<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#127" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="605" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.995284179688" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.602929626465" Y="-28.660416015625" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542362670898" Y="-28.467375" />
                  <Point X="0.526165893555" Y="-28.4440390625" />
                  <Point X="0.378634887695" Y="-28.231474609375" />
                  <Point X="0.356748809814" Y="-28.209017578125" />
                  <Point X="0.330493927002" Y="-28.189775390625" />
                  <Point X="0.30249041748" Y="-28.17566796875" />
                  <Point X="0.27742678833" Y="-28.167890625" />
                  <Point X="0.049131427765" Y="-28.09703515625" />
                  <Point X="0.020983259201" Y="-28.092767578125" />
                  <Point X="-0.008658372879" Y="-28.092765625" />
                  <Point X="-0.03682566452" Y="-28.09703515625" />
                  <Point X="-0.061889297485" Y="-28.104814453125" />
                  <Point X="-0.290184814453" Y="-28.17566796875" />
                  <Point X="-0.318184844971" Y="-28.18977734375" />
                  <Point X="-0.344440185547" Y="-28.209021484375" />
                  <Point X="-0.366324707031" Y="-28.2314765625" />
                  <Point X="-0.38252166748" Y="-28.254814453125" />
                  <Point X="-0.53005255127" Y="-28.467376953125" />
                  <Point X="-0.53818927002" Y="-28.481572265625" />
                  <Point X="-0.55099005127" Y="-28.5125234375" />
                  <Point X="-0.911943725586" Y="-29.85962109375" />
                  <Point X="-0.916584594727" Y="-29.87694140625" />
                  <Point X="-1.079332641602" Y="-29.8453515625" />
                  <Point X="-1.104369018555" Y="-29.83891015625" />
                  <Point X="-1.246417724609" Y="-29.802361328125" />
                  <Point X="-1.219733276367" Y="-29.599673828125" />
                  <Point X="-1.214962890625" Y="-29.56344140625" />
                  <Point X="-1.214201049805" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.222496337891" Y="-29.486123046875" />
                  <Point X="-1.277035888672" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.323645263672" Y="-29.131203125" />
                  <Point X="-1.346720825195" Y="-29.110966796875" />
                  <Point X="-1.55690625" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.61288671875" Y="-28.897994140625" />
                  <Point X="-1.643026123047" Y="-28.890966796875" />
                  <Point X="-1.67365234375" Y="-28.888958984375" />
                  <Point X="-1.952615112305" Y="-28.87067578125" />
                  <Point X="-1.983414794922" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.04265612793" Y="-28.89480078125" />
                  <Point X="-2.068175537109" Y="-28.9118515625" />
                  <Point X="-2.300622802734" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.480147949219" Y="-29.288490234375" />
                  <Point X="-2.801720458984" Y="-29.089380859375" />
                  <Point X="-2.836362548828" Y="-29.06270703125" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.497404541016" Y="-27.804173828125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412857177734" Y="-27.647642578125" />
                  <Point X="-2.406587646484" Y="-27.61610546875" />
                  <Point X="-2.405724853516" Y="-27.58410546875" />
                  <Point X="-2.415539550781" Y="-27.55363671875" />
                  <Point X="-2.431470703125" Y="-27.523103515625" />
                  <Point X="-2.448519042969" Y="-27.499875" />
                  <Point X="-2.464151367188" Y="-27.4842421875" />
                  <Point X="-2.489310791016" Y="-27.466212890625" />
                  <Point X="-2.518140136719" Y="-27.45199609375" />
                  <Point X="-2.5477578125" Y="-27.44301171875" />
                  <Point X="-2.578691650391" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-3.801029052734" Y="-28.131990234375" />
                  <Point X="-3.818024169922" Y="-28.141802734375" />
                  <Point X="-4.082859863281" Y="-27.793861328125" />
                  <Point X="-4.107701660156" Y="-27.752205078125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.234806640625" Y="-26.59738671875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083143554688" Y="-26.47336328125" />
                  <Point X="-3.064416503906" Y="-26.4432890625" />
                  <Point X="-3.053094238281" Y="-26.416890625" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.043347167969" Y="-26.3596640625" />
                  <Point X="-3.045553222656" Y="-26.328" />
                  <Point X="-3.053066894531" Y="-26.29703515625" />
                  <Point X="-3.070396728516" Y="-26.270296875" />
                  <Point X="-3.093854248047" Y="-26.2447890625" />
                  <Point X="-3.115598632812" Y="-26.227220703125" />
                  <Point X="-3.139461425781" Y="-26.213177734375" />
                  <Point X="-3.16872265625" Y="-26.20195703125" />
                  <Point X="-3.200608154297" Y="-26.1954765625" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-4.698648925781" Y="-26.38748046875" />
                  <Point X="-4.732102539062" Y="-26.391884765625" />
                  <Point X="-4.834077636719" Y="-25.99265625" />
                  <Point X="-4.840649902344" Y="-25.946703125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.679657714844" Y="-25.259740234375" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.515082275391" Y="-25.21366796875" />
                  <Point X="-3.496760742188" Y="-25.20453515625" />
                  <Point X="-3.4849765625" Y="-25.19755859375" />
                  <Point X="-3.459978271484" Y="-25.180208984375" />
                  <Point X="-3.435963623047" Y="-25.156134765625" />
                  <Point X="-3.415803710938" Y="-25.126931640625" />
                  <Point X="-3.40325390625" Y="-25.10112109375" />
                  <Point X="-3.39491796875" Y="-25.074263671875" />
                  <Point X="-3.390685546875" Y="-25.043439453125" />
                  <Point X="-3.391600097656" Y="-25.01084765625" />
                  <Point X="-3.395831787109" Y="-24.985353515625" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.420021728516" Y="-24.928185546875" />
                  <Point X="-3.442010498047" Y="-24.900025390625" />
                  <Point X="-3.462718261719" Y="-24.88044921875" />
                  <Point X="-3.487725585938" Y="-24.863091796875" />
                  <Point X="-3.501924804688" Y="-24.854953125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.869860351562" Y="-24.48390625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.824488769531" Y="-24.02303125" />
                  <Point X="-4.811256347656" Y="-23.97419921875" />
                  <Point X="-4.703550292969" Y="-23.576732421875" />
                  <Point X="-3.870883300781" Y="-23.68635546875" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744984619141" Y="-23.700658203125" />
                  <Point X="-3.723423583984" Y="-23.698771484375" />
                  <Point X="-3.703136230469" Y="-23.694736328125" />
                  <Point X="-3.697059814453" Y="-23.6928203125" />
                  <Point X="-3.641710205078" Y="-23.675369140625" />
                  <Point X="-3.622784912109" Y="-23.667041015625" />
                  <Point X="-3.604040527344" Y="-23.656220703125" />
                  <Point X="-3.58735546875" Y="-23.64398828125" />
                  <Point X="-3.573714355469" Y="-23.62843359375" />
                  <Point X="-3.561299804688" Y="-23.610703125" />
                  <Point X="-3.551350830078" Y="-23.592568359375" />
                  <Point X="-3.548912597656" Y="-23.586681640625" />
                  <Point X="-3.526703369141" Y="-23.533064453125" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532046875" Y="-23.41062890625" />
                  <Point X="-3.534988769531" Y="-23.4049765625" />
                  <Point X="-3.561786621094" Y="-23.3535" />
                  <Point X="-3.57328125" Y="-23.336294921875" />
                  <Point X="-3.587193359375" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.351860351563" Y="-22.730125" />
                  <Point X="-4.081156738281" Y="-22.26634765625" />
                  <Point X="-4.046107177734" Y="-22.221294921875" />
                  <Point X="-3.750503417969" Y="-21.841337890625" />
                  <Point X="-3.271278076172" Y="-22.11801953125" />
                  <Point X="-3.206656738281" Y="-22.155328125" />
                  <Point X="-3.187729248047" Y="-22.163658203125" />
                  <Point X="-3.167087158203" Y="-22.17016796875" />
                  <Point X="-3.146792236328" Y="-22.174205078125" />
                  <Point X="-3.138329101562" Y="-22.1749453125" />
                  <Point X="-3.061242919922" Y="-22.181689453125" />
                  <Point X="-3.040567626953" Y="-22.18123828125" />
                  <Point X="-3.019111328125" Y="-22.1784140625" />
                  <Point X="-2.999020996094" Y="-22.173498046875" />
                  <Point X="-2.980470458984" Y="-22.1643515625" />
                  <Point X="-2.962217285156" Y="-22.152724609375" />
                  <Point X="-2.946086181641" Y="-22.139779296875" />
                  <Point X="-2.940075439453" Y="-22.13376953125" />
                  <Point X="-2.885358886719" Y="-22.079052734375" />
                  <Point X="-2.872406738281" Y="-22.062916015625" />
                  <Point X="-2.860777587891" Y="-22.044662109375" />
                  <Point X="-2.851628662109" Y="-22.026111328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.96387890625" />
                  <Point X="-2.844176269531" Y="-21.955416015625" />
                  <Point X="-2.850920410156" Y="-21.878328125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.183333007812" Y="-21.275404296875" />
                  <Point X="-2.700626464844" Y="-20.90531640625" />
                  <Point X="-2.645432128906" Y="-20.87465234375" />
                  <Point X="-2.167037109375" Y="-20.608865234375" />
                  <Point X="-2.062977294922" Y="-20.74448046875" />
                  <Point X="-2.043194702148" Y="-20.770259765625" />
                  <Point X="-2.028887817383" Y="-20.785205078125" />
                  <Point X="-2.012309204102" Y="-20.799115234375" />
                  <Point X="-1.995120117188" Y="-20.810599609375" />
                  <Point X="-1.985700927734" Y="-20.81550390625" />
                  <Point X="-1.899904174805" Y="-20.86016796875" />
                  <Point X="-1.880625244141" Y="-20.86766796875" />
                  <Point X="-1.859718505859" Y="-20.873271484375" />
                  <Point X="-1.839268676758" Y="-20.876419921875" />
                  <Point X="-1.818622192383" Y="-20.87506640625" />
                  <Point X="-1.797306762695" Y="-20.871306640625" />
                  <Point X="-1.777448608398" Y="-20.865515625" />
                  <Point X="-1.767637695313" Y="-20.861451171875" />
                  <Point X="-1.678274658203" Y="-20.824435546875" />
                  <Point X="-1.660146118164" Y="-20.814490234375" />
                  <Point X="-1.642416625977" Y="-20.802076171875" />
                  <Point X="-1.626864501953" Y="-20.7884375" />
                  <Point X="-1.614633056641" Y="-20.771755859375" />
                  <Point X="-1.603811157227" Y="-20.75301171875" />
                  <Point X="-1.595480712891" Y="-20.734080078125" />
                  <Point X="-1.592287475586" Y="-20.723953125" />
                  <Point X="-1.563201416016" Y="-20.631703125" />
                  <Point X="-1.559165649414" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.557730224609" Y="-20.569173828125" />
                  <Point X="-1.584201904297" Y="-20.3681015625" />
                  <Point X="-0.949624145508" Y="-20.19019140625" />
                  <Point X="-0.882730285645" Y="-20.182361328125" />
                  <Point X="-0.294711212158" Y="-20.113541015625" />
                  <Point X="-0.153020172119" Y="-20.64233984375" />
                  <Point X="-0.133903381348" Y="-20.713685546875" />
                  <Point X="-0.12112991333" Y="-20.741876953125" />
                  <Point X="-0.103271614075" Y="-20.768603515625" />
                  <Point X="-0.082114082336" Y="-20.791193359375" />
                  <Point X="-0.054818141937" Y="-20.805783203125" />
                  <Point X="-0.02437991333" Y="-20.816115234375" />
                  <Point X="0.006155911922" Y="-20.82115625" />
                  <Point X="0.036691745758" Y="-20.816115234375" />
                  <Point X="0.067129974365" Y="-20.805783203125" />
                  <Point X="0.094426025391" Y="-20.791193359375" />
                  <Point X="0.115583602905" Y="-20.768603515625" />
                  <Point X="0.133441741943" Y="-20.741876953125" />
                  <Point X="0.146215209961" Y="-20.713685546875" />
                  <Point X="0.307419372559" Y="-20.1120625" />
                  <Point X="0.844034118652" Y="-20.168259765625" />
                  <Point X="0.89940032959" Y="-20.181626953125" />
                  <Point X="1.481032104492" Y="-20.322052734375" />
                  <Point X="1.515726074219" Y="-20.334634765625" />
                  <Point X="1.894663085938" Y="-20.472080078125" />
                  <Point X="1.929493041992" Y="-20.4883671875" />
                  <Point X="2.294549316406" Y="-20.65909375" />
                  <Point X="2.328271728516" Y="-20.678740234375" />
                  <Point X="2.680988525391" Y="-20.884232421875" />
                  <Point X="2.712729003906" Y="-20.9068046875" />
                  <Point X="2.943260009766" Y="-21.07074609375" />
                  <Point X="2.232817626953" Y="-22.301267578125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.140349609375" Y="-22.46455859375" />
                  <Point X="2.133224121094" Y="-22.4845859375" />
                  <Point X="2.130952880859" Y="-22.491888671875" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108400878906" Y="-22.58689453125" />
                  <Point X="2.107891357422" Y="-22.6126640625" />
                  <Point X="2.108555908203" Y="-22.625916015625" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140068603516" Y="-22.752525390625" />
                  <Point X="2.144318359375" Y="-22.7587890625" />
                  <Point X="2.183026367187" Y="-22.815833984375" />
                  <Point X="2.198198486328" Y="-22.83320703125" />
                  <Point X="2.217760498047" Y="-22.8507578125" />
                  <Point X="2.227860107422" Y="-22.858658203125" />
                  <Point X="2.284906005859" Y="-22.8973671875" />
                  <Point X="2.304954101562" Y="-22.90773046875" />
                  <Point X="2.327043212891" Y="-22.91599609375" />
                  <Point X="2.348964599609" Y="-22.921337890625" />
                  <Point X="2.355832519531" Y="-22.922166015625" />
                  <Point X="2.418389404297" Y="-22.929708984375" />
                  <Point X="2.441916015625" Y="-22.929611328125" />
                  <Point X="2.468760742188" Y="-22.9261484375" />
                  <Point X="2.4811484375" Y="-22.923705078125" />
                  <Point X="2.553492431641" Y="-22.904359375" />
                  <Point X="2.565286376953" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.933282714844" Y="-22.113462890625" />
                  <Point X="3.967325195312" Y="-22.09380859375" />
                  <Point X="4.123275390625" Y="-22.31054296875" />
                  <Point X="4.14097265625" Y="-22.3397890625" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.343005615234" Y="-23.2454375" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.217843994141" Y="-23.34354296875" />
                  <Point X="3.202883300781" Y="-23.36025" />
                  <Point X="3.198258056641" Y="-23.365830078125" />
                  <Point X="3.146191894531" Y="-23.43375390625" />
                  <Point X="3.134362548828" Y="-23.453912109375" />
                  <Point X="3.123763427734" Y="-23.4784765625" />
                  <Point X="3.119500244141" Y="-23.49052734375" />
                  <Point X="3.100105712891" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.099487304688" Y="-23.636703125" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.122996826172" Y="-23.736177734375" />
                  <Point X="3.13494921875" Y="-23.760783203125" />
                  <Point X="3.141037353516" Y="-23.77148828125" />
                  <Point X="3.184340820312" Y="-23.837306640625" />
                  <Point X="3.198892822266" Y="-23.85455078125" />
                  <Point X="3.216132080078" Y="-23.87063671875" />
                  <Point X="3.234339355469" Y="-23.8839609375" />
                  <Point X="3.241232910156" Y="-23.887841796875" />
                  <Point X="3.303985839844" Y="-23.923166015625" />
                  <Point X="3.326265869141" Y="-23.932216796875" />
                  <Point X="3.353556396484" Y="-23.9394453125" />
                  <Point X="3.365434082031" Y="-23.94179296875" />
                  <Point X="3.450280029297" Y="-23.953005859375" />
                  <Point X="3.462698242188" Y="-23.95382421875" />
                  <Point X="3.488203369141" Y="-23.953015625" />
                  <Point X="4.765625" Y="-23.78483984375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845937011719" Y="-24.06719140625" />
                  <Point X="4.851514160156" Y="-24.103017578125" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.845188476562" Y="-24.63594921875" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.699712646484" Y="-24.6766953125" />
                  <Point X="3.678480957031" Y="-24.6869921875" />
                  <Point X="3.672395263672" Y="-24.690220703125" />
                  <Point X="3.589036865234" Y="-24.73840234375" />
                  <Point X="3.570065185547" Y="-24.7528203125" />
                  <Point X="3.550261962891" Y="-24.77223828125" />
                  <Point X="3.542040283203" Y="-24.781419921875" />
                  <Point X="3.492025390625" Y="-24.845150390625" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.470526855469" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.461850585938" Y="-24.916953125" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443680175781" Y="-25.027994140625" />
                  <Point X="3.445510742188" Y="-25.056359375" />
                  <Point X="3.447009277344" Y="-25.068111328125" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526611328" Y="-25.1766640625" />
                  <Point X="3.480300537109" Y="-25.19812890625" />
                  <Point X="3.492020263672" Y="-25.21740234375" />
                  <Point X="3.497511230469" Y="-25.224400390625" />
                  <Point X="3.547526123047" Y="-25.288130859375" />
                  <Point X="3.565034423828" Y="-25.305314453125" />
                  <Point X="3.588497558594" Y="-25.3230234375" />
                  <Point X="3.598186279297" Y="-25.3294453125" />
                  <Point X="3.681544677734" Y="-25.37762890625" />
                  <Point X="3.692708251953" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.888033203125" Y="-25.706041015625" />
                  <Point X="4.89147265625" Y="-25.706962890625" />
                  <Point X="4.855022460938" Y="-25.948728515625" />
                  <Point X="4.847875488281" Y="-25.980046875" />
                  <Point X="4.801174316406" Y="-26.18469921875" />
                  <Point X="3.573532470703" Y="-26.023078125" />
                  <Point X="3.424382324219" Y="-26.00344140625" />
                  <Point X="3.399594970703" Y="-26.003439453125" />
                  <Point X="3.364487792969" Y="-26.008056640625" />
                  <Point X="3.356698486328" Y="-26.0094140625" />
                  <Point X="3.193095458984" Y="-26.04497265625" />
                  <Point X="3.162730224609" Y="-26.055689453125" />
                  <Point X="3.130097167969" Y="-26.077427734375" />
                  <Point X="3.106740478516" Y="-26.101259765625" />
                  <Point X="3.101540771484" Y="-26.107017578125" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.986627197266" Y="-26.250416015625" />
                  <Point X="2.974201904297" Y="-26.28416796875" />
                  <Point X="2.969050292969" Y="-26.315630859375" />
                  <Point X="2.968201660156" Y="-26.322275390625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.955109130859" Y="-26.508484375" />
                  <Point X="2.966159912109" Y="-26.546416015625" />
                  <Point X="2.982611328125" Y="-26.577041015625" />
                  <Point X="2.986390625" Y="-26.583458984375" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="4.197744140625" Y="-27.595083984375" />
                  <Point X="4.213123046875" Y="-27.606884765625" />
                  <Point X="4.124812011719" Y="-27.74978515625" />
                  <Point X="4.110030761719" Y="-27.77078515625" />
                  <Point X="4.028980712891" Y="-27.8859453125" />
                  <Point X="2.933792724609" Y="-27.253638671875" />
                  <Point X="2.800954345703" Y="-27.1769453125" />
                  <Point X="2.786129150391" Y="-27.170013671875" />
                  <Point X="2.754227294922" Y="-27.159828125" />
                  <Point X="2.732850585938" Y="-27.155966796875" />
                  <Point X="2.538136962891" Y="-27.120802734375" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.4448359375" Y="-27.13517578125" />
                  <Point X="2.427076904297" Y="-27.144521484375" />
                  <Point X="2.265317871094" Y="-27.229654296875" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531005859" Y="-27.29044140625" />
                  <Point X="2.195184570313" Y="-27.308201171875" />
                  <Point X="2.110052001953" Y="-27.469958984375" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.099535888672" Y="-27.584634765625" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.850400634766" Y="-29.036056640625" />
                  <Point X="2.861283691406" Y="-29.05490625" />
                  <Point X="2.781854736328" Y="-29.111640625" />
                  <Point X="2.765330078125" Y="-29.1223359375" />
                  <Point X="2.701764160156" Y="-29.16348046875" />
                  <Point X="1.85991394043" Y="-28.066361328125" />
                  <Point X="1.758546020508" Y="-27.934255859375" />
                  <Point X="1.747506469727" Y="-27.922181640625" />
                  <Point X="1.721924560547" Y="-27.900556640625" />
                  <Point X="1.700841308594" Y="-27.887001953125" />
                  <Point X="1.50880090332" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365722656" Y="-27.743435546875" />
                  <Point X="1.417102783203" Y="-27.7411171875" />
                  <Point X="1.394044433594" Y="-27.74323828125" />
                  <Point X="1.184015991211" Y="-27.762564453125" />
                  <Point X="1.156362426758" Y="-27.7693984375" />
                  <Point X="1.128976928711" Y="-27.7807421875" />
                  <Point X="1.104594604492" Y="-27.795462890625" />
                  <Point X="1.086789550781" Y="-27.810267578125" />
                  <Point X="0.924610717773" Y="-27.945115234375" />
                  <Point X="0.904141174316" Y="-27.96886328125" />
                  <Point X="0.887249023438" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.870300720215" Y="-28.050302734375" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724609375" Y="-28.289626953125" />
                  <Point X="0.819742370605" Y="-28.323119140625" />
                  <Point X="1.017714904785" Y="-29.82687109375" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.975714233398" Y="-29.870076171875" />
                  <Point X="0.960407287598" Y="-29.872857421875" />
                  <Point X="0.929315856934" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058429931641" Y="-29.752634765625" />
                  <Point X="-1.080698120117" Y="-29.74690625" />
                  <Point X="-1.14124609375" Y="-29.731328125" />
                  <Point X="-1.125546020508" Y="-29.61207421875" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077270508" Y="-29.5681015625" />
                  <Point X="-1.119451660156" Y="-29.54103515625" />
                  <Point X="-1.121759033203" Y="-29.509330078125" />
                  <Point X="-1.123333862305" Y="-29.497693359375" />
                  <Point X="-1.129321655273" Y="-29.46758984375" />
                  <Point X="-1.183861206055" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026855469" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230573852539" Y="-29.094861328125" />
                  <Point X="-1.250209106445" Y="-29.070935546875" />
                  <Point X="-1.261008056641" Y="-29.05977734375" />
                  <Point X="-1.284083496094" Y="-29.039541015625" />
                  <Point X="-1.494269042969" Y="-28.855212890625" />
                  <Point X="-1.506739868164" Y="-28.84596484375" />
                  <Point X="-1.533022705078" Y="-28.82962109375" />
                  <Point X="-1.546835083008" Y="-28.822525390625" />
                  <Point X="-1.576532592773" Y="-28.810224609375" />
                  <Point X="-1.591314941406" Y="-28.8054765625" />
                  <Point X="-1.621454223633" Y="-28.79844921875" />
                  <Point X="-1.636811279297" Y="-28.796169921875" />
                  <Point X="-1.66743762207" Y="-28.794162109375" />
                  <Point X="-1.946400390625" Y="-28.77587890625" />
                  <Point X="-1.96192578125" Y="-28.7761328125" />
                  <Point X="-1.992725463867" Y="-28.779166015625" />
                  <Point X="-2.007999633789" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053669189453" Y="-28.795494140625" />
                  <Point X="-2.081862060547" Y="-28.808267578125" />
                  <Point X="-2.09543359375" Y="-28.815810546875" />
                  <Point X="-2.120953125" Y="-28.832861328125" />
                  <Point X="-2.353400390625" Y="-28.988177734375" />
                  <Point X="-2.359678466797" Y="-28.99275390625" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.503201171875" Y="-29.162478515625" />
                  <Point X="-2.747604248047" Y="-29.011150390625" />
                  <Point X="-2.778404541016" Y="-28.987435546875" />
                  <Point X="-2.980863037109" Y="-28.83155078125" />
                  <Point X="-2.415132080078" Y="-27.851673828125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334848144531" Y="-27.710078125" />
                  <Point X="-2.323944091797" Y="-27.6811015625" />
                  <Point X="-2.319680664062" Y="-27.666166015625" />
                  <Point X="-2.313411132812" Y="-27.63462890625" />
                  <Point X="-2.311622070312" Y="-27.618666015625" />
                  <Point X="-2.310759277344" Y="-27.586666015625" />
                  <Point X="-2.315300537109" Y="-27.554978515625" />
                  <Point X="-2.325115234375" Y="-27.524509765625" />
                  <Point X="-2.331314941406" Y="-27.50969140625" />
                  <Point X="-2.34724609375" Y="-27.479158203125" />
                  <Point X="-2.354884277344" Y="-27.46689453125" />
                  <Point X="-2.371932617188" Y="-27.443666015625" />
                  <Point X="-2.381342773438" Y="-27.432701171875" />
                  <Point X="-2.396975097656" Y="-27.417068359375" />
                  <Point X="-2.408815429688" Y="-27.407021484375" />
                  <Point X="-2.433974853516" Y="-27.3889921875" />
                  <Point X="-2.447293945312" Y="-27.381009765625" />
                  <Point X="-2.476123291016" Y="-27.36679296875" />
                  <Point X="-2.490563232422" Y="-27.3610859375" />
                  <Point X="-2.520180908203" Y="-27.3521015625" />
                  <Point X="-2.550869140625" Y="-27.3480625" />
                  <Point X="-2.581802978516" Y="-27.349076171875" />
                  <Point X="-2.597226318359" Y="-27.3508515625" />
                  <Point X="-2.628753173828" Y="-27.357123046875" />
                  <Point X="-2.643683837891" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.686683837891" Y="-27.378927734375" />
                  <Point X="-3.793089111328" Y="-28.0177109375" />
                  <Point X="-4.004013671875" Y="-27.74059765625" />
                  <Point X="-4.026108886719" Y="-27.703546875" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.176974365234" Y="-26.672755859375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.035584228516" Y="-26.5623359375" />
                  <Point X="-3.012773193359" Y="-26.53718359375" />
                  <Point X="-3.002500244141" Y="-26.523580078125" />
                  <Point X="-2.983773193359" Y="-26.493505859375" />
                  <Point X="-2.977108154297" Y="-26.480736328125" />
                  <Point X="-2.965785888672" Y="-26.454337890625" />
                  <Point X="-2.961128662109" Y="-26.440708984375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552978516" Y="-26.398806640625" />
                  <Point X="-2.948748291016" Y="-26.368384765625" />
                  <Point X="-2.948576904297" Y="-26.353060546875" />
                  <Point X="-2.950782958984" Y="-26.321396484375" />
                  <Point X="-2.953232177734" Y="-26.30559765625" />
                  <Point X="-2.960745849609" Y="-26.2746328125" />
                  <Point X="-2.973346679688" Y="-26.245365234375" />
                  <Point X="-2.990676513672" Y="-26.218626953125" />
                  <Point X="-3.000469970703" Y="-26.205990234375" />
                  <Point X="-3.023927490234" Y="-26.180482421875" />
                  <Point X="-3.034150634766" Y="-26.17089453125" />
                  <Point X="-3.055895019531" Y="-26.153326171875" />
                  <Point X="-3.067416259766" Y="-26.145345703125" />
                  <Point X="-3.091279052734" Y="-26.131302734375" />
                  <Point X="-3.105447265625" Y="-26.1244765625" />
                  <Point X="-3.134708496094" Y="-26.113255859375" />
                  <Point X="-3.149801513672" Y="-26.108861328125" />
                  <Point X="-3.181687011719" Y="-26.102380859375" />
                  <Point X="-3.197298583984" Y="-26.10053515625" />
                  <Point X="-3.228619628906" Y="-26.099443359375" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-4.660920898438" Y="-26.286693359375" />
                  <Point X="-4.740762695312" Y="-25.9741171875" />
                  <Point X="-4.746606933594" Y="-25.93325390625" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.655069824219" Y="-25.35150390625" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.499215087891" Y="-25.30924609375" />
                  <Point X="-3.472700683594" Y="-25.298689453125" />
                  <Point X="-3.454379150391" Y="-25.289556640625" />
                  <Point X="-3.448363769531" Y="-25.286283203125" />
                  <Point X="-3.430810791016" Y="-25.275603515625" />
                  <Point X="-3.4058125" Y="-25.25825390625" />
                  <Point X="-3.392719970703" Y="-25.24730078125" />
                  <Point X="-3.368705322266" Y="-25.2232265625" />
                  <Point X="-3.357783203125" Y="-25.21010546875" />
                  <Point X="-3.337623291016" Y="-25.18090234375" />
                  <Point X="-3.330367675781" Y="-25.16847265625" />
                  <Point X="-3.317817871094" Y="-25.142662109375" />
                  <Point X="-3.312523681641" Y="-25.12928125" />
                  <Point X="-3.304187744141" Y="-25.102423828125" />
                  <Point X="-3.300801025391" Y="-25.0871875" />
                  <Point X="-3.296568603516" Y="-25.05636328125" />
                  <Point X="-3.295722900391" Y="-25.040775390625" />
                  <Point X="-3.296637451172" Y="-25.00818359375" />
                  <Point X="-3.297882324219" Y="-24.995291015625" />
                  <Point X="-3.302114013672" Y="-24.969796875" />
                  <Point X="-3.305100830078" Y="-24.9571953125" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.319988769531" Y="-24.9144609375" />
                  <Point X="-3.335842773438" Y="-24.88415234375" />
                  <Point X="-3.345144775391" Y="-24.86971875" />
                  <Point X="-3.367133544922" Y="-24.84155859375" />
                  <Point X="-3.376748046875" Y="-24.830990234375" />
                  <Point X="-3.397455810547" Y="-24.8114140625" />
                  <Point X="-3.408549072266" Y="-24.80240625" />
                  <Point X="-3.433556396484" Y="-24.785048828125" />
                  <Point X="-3.440483886719" Y="-24.780669921875" />
                  <Point X="-3.465612548828" Y="-24.767166015625" />
                  <Point X="-3.496563720703" Y="-24.75436328125" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731332519531" Y="-24.04248046875" />
                  <Point X="-4.719562988281" Y="-23.999046875" />
                  <Point X="-4.633584960938" Y="-23.681763671875" />
                  <Point X="-3.883283447266" Y="-23.78054296875" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747056640625" Y="-23.795634765625" />
                  <Point X="-3.736703125" Y="-23.795296875" />
                  <Point X="-3.715142089844" Y="-23.79341015625" />
                  <Point X="-3.704891113281" Y="-23.791947265625" />
                  <Point X="-3.684603759766" Y="-23.787912109375" />
                  <Point X="-3.668490966797" Y="-23.783423828125" />
                  <Point X="-3.613141357422" Y="-23.76597265625" />
                  <Point X="-3.603446289062" Y="-23.762322265625" />
                  <Point X="-3.584520996094" Y="-23.753994140625" />
                  <Point X="-3.575290771484" Y="-23.74931640625" />
                  <Point X="-3.556546386719" Y="-23.73849609375" />
                  <Point X="-3.547870849609" Y="-23.7328359375" />
                  <Point X="-3.531185791016" Y="-23.720603515625" />
                  <Point X="-3.515930664062" Y="-23.706626953125" />
                  <Point X="-3.502289550781" Y="-23.691072265625" />
                  <Point X="-3.495894042969" Y="-23.682921875" />
                  <Point X="-3.483479492188" Y="-23.66519140625" />
                  <Point X="-3.478010498047" Y="-23.656396484375" />
                  <Point X="-3.468061523438" Y="-23.63826171875" />
                  <Point X="-3.461143310547" Y="-23.62303515625" />
                  <Point X="-3.438934082031" Y="-23.56941796875" />
                  <Point X="-3.435498291016" Y="-23.559646484375" />
                  <Point X="-3.429710693359" Y="-23.5397890625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436011474609" Y="-23.39547265625" />
                  <Point X="-3.443504882812" Y="-23.376201171875" />
                  <Point X="-3.450719482422" Y="-23.3611171875" />
                  <Point X="-3.477517333984" Y="-23.309640625" />
                  <Point X="-3.482793945312" Y="-23.300724609375" />
                  <Point X="-3.494288574219" Y="-23.28351953125" />
                  <Point X="-3.500506591797" Y="-23.27523046875" />
                  <Point X="-3.514418701172" Y="-23.258650390625" />
                  <Point X="-3.521498535156" Y="-23.251091796875" />
                  <Point X="-3.536440917969" Y="-23.236787109375" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-4.227613769531" Y="-22.705716796875" />
                  <Point X="-4.002296142578" Y="-22.3196953125" />
                  <Point X="-3.971125488281" Y="-22.27962890625" />
                  <Point X="-3.726336181641" Y="-21.964986328125" />
                  <Point X="-3.318778076172" Y="-22.200291015625" />
                  <Point X="-3.254156738281" Y="-22.237599609375" />
                  <Point X="-3.244924560547" Y="-22.242279296875" />
                  <Point X="-3.225997070313" Y="-22.250609375" />
                  <Point X="-3.216301757812" Y="-22.254259765625" />
                  <Point X="-3.195659667969" Y="-22.26076953125" />
                  <Point X="-3.185621582031" Y="-22.263341796875" />
                  <Point X="-3.165326660156" Y="-22.26737890625" />
                  <Point X="-3.146606689453" Y="-22.269583984375" />
                  <Point X="-3.069520507812" Y="-22.276328125" />
                  <Point X="-3.059170410156" Y="-22.276666015625" />
                  <Point X="-3.038495117188" Y="-22.27621484375" />
                  <Point X="-3.028169921875" Y="-22.27542578125" />
                  <Point X="-3.006713623047" Y="-22.2726015625" />
                  <Point X="-2.996531494141" Y="-22.27069140625" />
                  <Point X="-2.976441162109" Y="-22.265775390625" />
                  <Point X="-2.957009521484" Y="-22.258703125" />
                  <Point X="-2.938458984375" Y="-22.249556640625" />
                  <Point X="-2.929431884766" Y="-22.2444765625" />
                  <Point X="-2.911178710938" Y="-22.232849609375" />
                  <Point X="-2.902758056641" Y="-22.22681640625" />
                  <Point X="-2.886626953125" Y="-22.21387109375" />
                  <Point X="-2.872905761719" Y="-22.20094921875" />
                  <Point X="-2.818189208984" Y="-22.146232421875" />
                  <Point X="-2.811272216797" Y="-22.138517578125" />
                  <Point X="-2.798320068359" Y="-22.122380859375" />
                  <Point X="-2.792284912109" Y="-22.113958984375" />
                  <Point X="-2.780655761719" Y="-22.095705078125" />
                  <Point X="-2.775575927734" Y="-22.086681640625" />
                  <Point X="-2.766427001953" Y="-22.068130859375" />
                  <Point X="-2.759351318359" Y="-22.04869140625" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965951171875" />
                  <Point X="-2.749537841797" Y="-21.947134765625" />
                  <Point X="-2.756281982422" Y="-21.870046875" />
                  <Point X="-2.75774609375" Y="-21.85979296875" />
                  <Point X="-2.76178125" Y="-21.8395078125" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.059386962891" Y="-21.300083984375" />
                  <Point X="-2.648372070312" Y="-20.984962890625" />
                  <Point X="-2.599295410156" Y="-20.957697265625" />
                  <Point X="-2.192525390625" Y="-20.731703125" />
                  <Point X="-2.138346191406" Y="-20.8023125" />
                  <Point X="-2.118563720703" Y="-20.828091796875" />
                  <Point X="-2.111819580078" Y="-20.835953125" />
                  <Point X="-2.097512695312" Y="-20.8508984375" />
                  <Point X="-2.089950195312" Y="-20.85798046875" />
                  <Point X="-2.073371582031" Y="-20.871890625" />
                  <Point X="-2.065085205078" Y="-20.878107421875" />
                  <Point X="-2.047896118164" Y="-20.889591796875" />
                  <Point X="-2.029573974609" Y="-20.899765625" />
                  <Point X="-1.94377722168" Y="-20.9444296875" />
                  <Point X="-1.934347167969" Y="-20.948705078125" />
                  <Point X="-1.915067993164" Y="-20.956205078125" />
                  <Point X="-1.905219604492" Y="-20.9594296875" />
                  <Point X="-1.884312744141" Y="-20.965033203125" />
                  <Point X="-1.874174316406" Y="-20.967166015625" />
                  <Point X="-1.853724487305" Y="-20.970314453125" />
                  <Point X="-1.833054077148" Y="-20.971216796875" />
                  <Point X="-1.812407714844" Y="-20.96986328125" />
                  <Point X="-1.802120117188" Y="-20.968623046875" />
                  <Point X="-1.7808046875" Y="-20.96486328125" />
                  <Point X="-1.77071081543" Y="-20.9625078125" />
                  <Point X="-1.750852661133" Y="-20.956716796875" />
                  <Point X="-1.731277832031" Y="-20.949216796875" />
                  <Point X="-1.641914794922" Y="-20.912201171875" />
                  <Point X="-1.63258203125" Y="-20.907724609375" />
                  <Point X="-1.614453369141" Y="-20.897779296875" />
                  <Point X="-1.605657104492" Y="-20.892310546875" />
                  <Point X="-1.587927612305" Y="-20.879896484375" />
                  <Point X="-1.579779174805" Y="-20.873501953125" />
                  <Point X="-1.564227050781" Y="-20.85986328125" />
                  <Point X="-1.550252197266" Y="-20.844611328125" />
                  <Point X="-1.538020751953" Y="-20.8279296875" />
                  <Point X="-1.532360595703" Y="-20.819255859375" />
                  <Point X="-1.521538696289" Y="-20.80051171875" />
                  <Point X="-1.516857055664" Y="-20.7912734375" />
                  <Point X="-1.508526611328" Y="-20.772341796875" />
                  <Point X="-1.501684814453" Y="-20.752521484375" />
                  <Point X="-1.472598876953" Y="-20.660271484375" />
                  <Point X="-1.470026855469" Y="-20.650236328125" />
                  <Point X="-1.465991088867" Y="-20.629947265625" />
                  <Point X="-1.46452722168" Y="-20.6196953125" />
                  <Point X="-1.46264074707" Y="-20.598134765625" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752807617" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.43734375" />
                  <Point X="-0.931166137695" Y="-20.2836796875" />
                  <Point X="-0.87168572998" Y="-20.276716796875" />
                  <Point X="-0.365222412109" Y="-20.21744140625" />
                  <Point X="-0.244783157349" Y="-20.666927734375" />
                  <Point X="-0.225666397095" Y="-20.7382734375" />
                  <Point X="-0.220435317993" Y="-20.752892578125" />
                  <Point X="-0.207661911011" Y="-20.781083984375" />
                  <Point X="-0.200119308472" Y="-20.79465625" />
                  <Point X="-0.182260925293" Y="-20.8213828125" />
                  <Point X="-0.172608901978" Y="-20.833544921875" />
                  <Point X="-0.151451400757" Y="-20.856134765625" />
                  <Point X="-0.126896362305" Y="-20.8749765625" />
                  <Point X="-0.09960043335" Y="-20.88956640625" />
                  <Point X="-0.085353881836" Y="-20.8957421875" />
                  <Point X="-0.054915763855" Y="-20.90607421875" />
                  <Point X="-0.03985357666" Y="-20.909845703125" />
                  <Point X="-0.009317810059" Y="-20.91488671875" />
                  <Point X="0.021629640579" Y="-20.91488671875" />
                  <Point X="0.052165405273" Y="-20.909845703125" />
                  <Point X="0.067227592468" Y="-20.90607421875" />
                  <Point X="0.097665718079" Y="-20.8957421875" />
                  <Point X="0.111912109375" Y="-20.88956640625" />
                  <Point X="0.139208190918" Y="-20.8749765625" />
                  <Point X="0.16376322937" Y="-20.856134765625" />
                  <Point X="0.184920883179" Y="-20.833544921875" />
                  <Point X="0.194573196411" Y="-20.8213828125" />
                  <Point X="0.212431289673" Y="-20.79465625" />
                  <Point X="0.219973754883" Y="-20.781083984375" />
                  <Point X="0.232747146606" Y="-20.752892578125" />
                  <Point X="0.237978225708" Y="-20.7382734375" />
                  <Point X="0.378190643311" Y="-20.2149921875" />
                  <Point X="0.827855041504" Y="-20.262083984375" />
                  <Point X="0.877104858398" Y="-20.273974609375" />
                  <Point X="1.453611816406" Y="-20.4131640625" />
                  <Point X="1.483337524414" Y="-20.423943359375" />
                  <Point X="1.858268188477" Y="-20.559935546875" />
                  <Point X="1.889251708984" Y="-20.574423828125" />
                  <Point X="2.250426757812" Y="-20.7433359375" />
                  <Point X="2.28044921875" Y="-20.760826171875" />
                  <Point X="2.629442138672" Y="-20.9641484375" />
                  <Point X="2.657672119141" Y="-20.984224609375" />
                  <Point X="2.817779541016" Y="-21.098083984375" />
                  <Point X="2.150545166016" Y="-22.253767578125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.061336425781" Y="-22.40906640625" />
                  <Point X="2.050845703125" Y="-22.43271484375" />
                  <Point X="2.043720458984" Y="-22.4527421875" />
                  <Point X="2.039177612305" Y="-22.46734765625" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017543945313" Y="-22.550923828125" />
                  <Point X="2.014337646484" Y="-22.5735859375" />
                  <Point X="2.013419433594" Y="-22.585015625" />
                  <Point X="2.01291003418" Y="-22.61078515625" />
                  <Point X="2.014239135742" Y="-22.6372890625" />
                  <Point X="2.021782348633" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.02914453125" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.045318847656" Y="-22.77611328125" />
                  <Point X="2.055678222656" Y="-22.79615234375" />
                  <Point X="2.065704589844" Y="-22.812126953125" />
                  <Point X="2.104412597656" Y="-22.869171875" />
                  <Point X="2.111471923828" Y="-22.87832421875" />
                  <Point X="2.126644042969" Y="-22.895697265625" />
                  <Point X="2.134756835938" Y="-22.90391796875" />
                  <Point X="2.154318847656" Y="-22.92146875" />
                  <Point X="2.174518066406" Y="-22.93726953125" />
                  <Point X="2.231563964844" Y="-22.975978515625" />
                  <Point X="2.241282226562" Y="-22.981759765625" />
                  <Point X="2.261330322266" Y="-22.992123046875" />
                  <Point X="2.27166015625" Y="-22.996705078125" />
                  <Point X="2.293749267578" Y="-23.004970703125" />
                  <Point X="2.304551757812" Y="-23.008294921875" />
                  <Point X="2.326473144531" Y="-23.01363671875" />
                  <Point X="2.344459960938" Y="-23.016482421875" />
                  <Point X="2.407016845703" Y="-23.024025390625" />
                  <Point X="2.418783691406" Y="-23.024708984375" />
                  <Point X="2.442310302734" Y="-23.024611328125" />
                  <Point X="2.454070068359" Y="-23.023830078125" />
                  <Point X="2.480914794922" Y="-23.0203671875" />
                  <Point X="2.505690185547" Y="-23.01548046875" />
                  <Point X="2.578034179688" Y="-22.996134765625" />
                  <Point X="2.583991943359" Y="-22.994330078125" />
                  <Point X="2.604414794922" Y="-22.9869296875" />
                  <Point X="2.627662353516" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.940403076172" Y="-22.219046875" />
                  <Point X="4.043959960938" Y="-22.362966796875" />
                  <Point X="4.059694824219" Y="-22.388970703125" />
                  <Point X="4.136884765625" Y="-22.516529296875" />
                  <Point X="3.285173339844" Y="-23.170068359375" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.166202392578" Y="-23.261876953125" />
                  <Point X="3.147072021484" Y="-23.28016796875" />
                  <Point X="3.132111328125" Y="-23.296875" />
                  <Point X="3.122860839844" Y="-23.30803515625" />
                  <Point X="3.070794677734" Y="-23.375958984375" />
                  <Point X="3.0642578125" Y="-23.385671875" />
                  <Point X="3.052428466797" Y="-23.405830078125" />
                  <Point X="3.047135986328" Y="-23.416275390625" />
                  <Point X="3.036536865234" Y="-23.44083984375" />
                  <Point X="3.028010498047" Y="-23.46494140625" />
                  <Point X="3.008615966797" Y="-23.53429296875" />
                  <Point X="3.006224853516" Y="-23.54533984375" />
                  <Point X="3.002771484375" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.006447265625" Y="-23.655900390625" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.025467773438" Y="-23.744453125" />
                  <Point X="3.033056152344" Y="-23.766765625" />
                  <Point X="3.037545166016" Y="-23.7776875" />
                  <Point X="3.049497558594" Y="-23.80229296875" />
                  <Point X="3.061673828125" Y="-23.823703125" />
                  <Point X="3.104977294922" Y="-23.889521484375" />
                  <Point X="3.111737792969" Y="-23.89857421875" />
                  <Point X="3.126289794922" Y="-23.915818359375" />
                  <Point X="3.134081298828" Y="-23.924009765625" />
                  <Point X="3.151320556641" Y="-23.940095703125" />
                  <Point X="3.160028564453" Y="-23.94730078125" />
                  <Point X="3.178235839844" Y="-23.960625" />
                  <Point X="3.194628662109" Y="-23.970625" />
                  <Point X="3.257381591797" Y="-24.00594921875" />
                  <Point X="3.268231689453" Y="-24.011181640625" />
                  <Point X="3.29051171875" Y="-24.020232421875" />
                  <Point X="3.301941650391" Y="-24.02405078125" />
                  <Point X="3.329232177734" Y="-24.031279296875" />
                  <Point X="3.352987548828" Y="-24.035974609375" />
                  <Point X="3.437833496094" Y="-24.0471875" />
                  <Point X="3.444033203125" Y="-24.04780078125" />
                  <Point X="3.465708496094" Y="-24.04877734375" />
                  <Point X="3.491213623047" Y="-24.04796875" />
                  <Point X="3.500603271484" Y="-24.047203125" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.75268359375" Y="-24.085763671875" />
                  <Point X="4.75764453125" Y="-24.117630859375" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="3.820600585938" Y="-24.544185546875" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.683407714844" Y="-24.581390625" />
                  <Point X="3.6582578125" Y="-24.591216796875" />
                  <Point X="3.637026123047" Y="-24.601513671875" />
                  <Point X="3.624854736328" Y="-24.607970703125" />
                  <Point X="3.541496337891" Y="-24.65615234375" />
                  <Point X="3.531555175781" Y="-24.662765625" />
                  <Point X="3.512583496094" Y="-24.67718359375" />
                  <Point X="3.503552978516" Y="-24.68498828125" />
                  <Point X="3.483749755859" Y="-24.70440625" />
                  <Point X="3.467306396484" Y="-24.72276953125" />
                  <Point X="3.417291503906" Y="-24.7865" />
                  <Point X="3.410854003906" Y="-24.79579296875" />
                  <Point X="3.399129638672" Y="-24.81507421875" />
                  <Point X="3.393842773438" Y="-24.8250625" />
                  <Point X="3.384068603516" Y="-24.84652734375" />
                  <Point X="3.380005126953" Y="-24.857072265625" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.368546386719" Y="-24.899083984375" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350363525391" Y="-24.998083984375" />
                  <Point X="3.348864990234" Y="-25.0220703125" />
                  <Point X="3.348877441406" Y="-25.034111328125" />
                  <Point X="3.350708007812" Y="-25.0624765625" />
                  <Point X="3.353705078125" Y="-25.08598046875" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373158691406" Y="-25.183986328125" />
                  <Point X="3.380004150391" Y="-25.205486328125" />
                  <Point X="3.384067871094" Y="-25.216033203125" />
                  <Point X="3.393841796875" Y="-25.237498046875" />
                  <Point X="3.399129394531" Y="-25.247486328125" />
                  <Point X="3.410849121094" Y="-25.266759765625" />
                  <Point X="3.422772216797" Y="-25.28304296875" />
                  <Point X="3.472787109375" Y="-25.3467734375" />
                  <Point X="3.480982666016" Y="-25.355931640625" />
                  <Point X="3.498490966797" Y="-25.373115234375" />
                  <Point X="3.507803710938" Y="-25.381140625" />
                  <Point X="3.531266845703" Y="-25.398849609375" />
                  <Point X="3.550644287109" Y="-25.411693359375" />
                  <Point X="3.634002685547" Y="-25.459876953125" />
                  <Point X="3.639487548828" Y="-25.4628125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027832031" Y="-25.481029296875" />
                  <Point X="3.6919921875" Y="-25.483916015625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761614257812" Y="-25.93105078125" />
                  <Point X="4.755256347656" Y="-25.95891015625" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.585932373047" Y="-25.928890625" />
                  <Point X="3.436782226562" Y="-25.90925390625" />
                  <Point X="3.424389892578" Y="-25.90844140625" />
                  <Point X="3.399602539062" Y="-25.908439453125" />
                  <Point X="3.387207519531" Y="-25.90925" />
                  <Point X="3.352100341797" Y="-25.9138671875" />
                  <Point X="3.336521728516" Y="-25.91658203125" />
                  <Point X="3.172918701172" Y="-25.952140625" />
                  <Point X="3.161478515625" Y="-25.955388671875" />
                  <Point X="3.13111328125" Y="-25.96610546875" />
                  <Point X="3.110062255859" Y="-25.976625" />
                  <Point X="3.077429199219" Y="-25.99836328125" />
                  <Point X="3.062248779297" Y="-26.01093359375" />
                  <Point X="3.038892089844" Y="-26.034765625" />
                  <Point X="3.028492675781" Y="-26.04628125" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.923183105469" Y="-26.173896484375" />
                  <Point X="2.907157226562" Y="-26.19836328125" />
                  <Point X="2.897476318359" Y="-26.217595703125" />
                  <Point X="2.885051025391" Y="-26.25134765625" />
                  <Point X="2.880450195312" Y="-26.268818359375" />
                  <Point X="2.875298583984" Y="-26.30028125" />
                  <Point X="2.873601318359" Y="-26.3135703125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.85908203125" Y="-26.479484375" />
                  <Point X="2.860162597656" Y="-26.511671875" />
                  <Point X="2.863900878906" Y="-26.535056640625" />
                  <Point X="2.874951660156" Y="-26.57298828125" />
                  <Point X="2.882470703125" Y="-26.591373046875" />
                  <Point X="2.898922119141" Y="-26.621998046875" />
                  <Point X="2.906480712891" Y="-26.634833984375" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="4.087171142578" Y="-27.629984375" />
                  <Point X="4.045490234375" Y="-27.6974296875" />
                  <Point X="4.032345214844" Y="-27.71610546875" />
                  <Point X="4.001273925781" Y="-27.76025390625" />
                  <Point X="2.981292724609" Y="-27.1713671875" />
                  <Point X="2.848454345703" Y="-27.094673828125" />
                  <Point X="2.84119140625" Y="-27.09088671875" />
                  <Point X="2.8150234375" Y="-27.079513671875" />
                  <Point X="2.783121582031" Y="-27.069328125" />
                  <Point X="2.771114013672" Y="-27.066341796875" />
                  <Point X="2.749737304688" Y="-27.06248046875" />
                  <Point X="2.555023681641" Y="-27.02731640625" />
                  <Point X="2.539367675781" Y="-27.025810546875" />
                  <Point X="2.508014404297" Y="-27.025404296875" />
                  <Point X="2.492317138672" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444849853516" Y="-27.035134765625" />
                  <Point X="2.415074707031" Y="-27.04495703125" />
                  <Point X="2.400594238281" Y="-27.05110546875" />
                  <Point X="2.382835205078" Y="-27.060451171875" />
                  <Point X="2.221076171875" Y="-27.145583984375" />
                  <Point X="2.208970458984" Y="-27.153169921875" />
                  <Point X="2.186038085938" Y="-27.170064453125" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144939697266" Y="-27.211162109375" />
                  <Point X="2.128044921875" Y="-27.234095703125" />
                  <Point X="2.120462158203" Y="-27.24619921875" />
                  <Point X="2.111115722656" Y="-27.263958984375" />
                  <Point X="2.025983154297" Y="-27.425716796875" />
                  <Point X="2.019835205078" Y="-27.440193359375" />
                  <Point X="2.010012084961" Y="-27.469966796875" />
                  <Point X="2.006337158203" Y="-27.485263671875" />
                  <Point X="2.001379394531" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.00218762207" Y="-27.580140625" />
                  <Point X="2.006048217773" Y="-27.601517578125" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723754150391" Y="-29.036083984375" />
                  <Point X="1.935282470703" Y="-28.008529296875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657958984" Y="-27.87015234375" />
                  <Point X="1.80883581543" Y="-27.849630859375" />
                  <Point X="1.78325402832" Y="-27.828005859375" />
                  <Point X="1.773299682617" Y="-27.820646484375" />
                  <Point X="1.752216430664" Y="-27.807091796875" />
                  <Point X="1.560176025391" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517469970703" Y="-27.663873046875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926513672" Y="-27.65115234375" />
                  <Point X="1.455391479492" Y="-27.6486953125" />
                  <Point X="1.424128417969" Y="-27.646376953125" />
                  <Point X="1.408400512695" Y="-27.646515625" />
                  <Point X="1.385342285156" Y="-27.64863671875" />
                  <Point X="1.175313720703" Y="-27.667962890625" />
                  <Point X="1.161224487305" Y="-27.67033984375" />
                  <Point X="1.133570922852" Y="-27.677173828125" />
                  <Point X="1.120006713867" Y="-27.681630859375" />
                  <Point X="1.09262121582" Y="-27.692974609375" />
                  <Point X="1.079876220703" Y="-27.6994140625" />
                  <Point X="1.055493896484" Y="-27.714134765625" />
                  <Point X="1.043856689453" Y="-27.722416015625" />
                  <Point X="1.026051635742" Y="-27.737220703125" />
                  <Point X="0.86387286377" Y="-27.872068359375" />
                  <Point X="0.852652404785" Y="-27.883091796875" />
                  <Point X="0.832182861328" Y="-27.90683984375" />
                  <Point X="0.822933349609" Y="-27.919564453125" />
                  <Point X="0.806041137695" Y="-27.947390625" />
                  <Point X="0.799018981934" Y="-27.96146875" />
                  <Point X="0.787394287109" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.777468383789" Y="-28.030125" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584716797" Y="-28.2612890625" />
                  <Point X="0.72472467041" Y="-28.289677734375" />
                  <Point X="0.72474230957" Y="-28.323169921875" />
                  <Point X="0.725555175781" Y="-28.33551953125" />
                  <Point X="0.833091491699" Y="-29.15233984375" />
                  <Point X="0.694692565918" Y="-28.635828125" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605957031" Y="-28.480123046875" />
                  <Point X="0.642146362305" Y="-28.45358203125" />
                  <Point X="0.626787353516" Y="-28.423814453125" />
                  <Point X="0.620406494141" Y="-28.41320703125" />
                  <Point X="0.604209716797" Y="-28.38987109375" />
                  <Point X="0.456678771973" Y="-28.177306640625" />
                  <Point X="0.446669311523" Y="-28.165169921875" />
                  <Point X="0.42478326416" Y="-28.142712890625" />
                  <Point X="0.412906799316" Y="-28.132392578125" />
                  <Point X="0.386651824951" Y="-28.113150390625" />
                  <Point X="0.373235168457" Y="-28.10493359375" />
                  <Point X="0.345231658936" Y="-28.090826171875" />
                  <Point X="0.330644897461" Y="-28.084935546875" />
                  <Point X="0.305581298828" Y="-28.077158203125" />
                  <Point X="0.077285949707" Y="-28.006302734375" />
                  <Point X="0.063371723175" Y="-28.003109375" />
                  <Point X="0.035223583221" Y="-27.998841796875" />
                  <Point X="0.020989521027" Y="-27.997767578125" />
                  <Point X="-0.00865212822" Y="-27.997765625" />
                  <Point X="-0.022895553589" Y="-27.998837890625" />
                  <Point X="-0.051062862396" Y="-28.003107421875" />
                  <Point X="-0.064986602783" Y="-28.0063046875" />
                  <Point X="-0.090050216675" Y="-28.014083984375" />
                  <Point X="-0.318345855713" Y="-28.0849375" />
                  <Point X="-0.332934967041" Y="-28.090830078125" />
                  <Point X="-0.360934936523" Y="-28.104939453125" />
                  <Point X="-0.374345794678" Y="-28.113154296875" />
                  <Point X="-0.400601196289" Y="-28.1323984375" />
                  <Point X="-0.412474090576" Y="-28.142716796875" />
                  <Point X="-0.434358673096" Y="-28.165171875" />
                  <Point X="-0.444370361328" Y="-28.177310546875" />
                  <Point X="-0.46056729126" Y="-28.2006484375" />
                  <Point X="-0.608098266602" Y="-28.4132109375" />
                  <Point X="-0.612472839355" Y="-28.420134765625" />
                  <Point X="-0.625977416992" Y="-28.445265625" />
                  <Point X="-0.638778198242" Y="-28.476216796875" />
                  <Point X="-0.642752929688" Y="-28.487935546875" />
                  <Point X="-0.985425231934" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853757622652" Y="-22.128768853018" />
                  <Point X="-4.150415351878" Y="-22.764953406364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.581150018126" Y="-23.688666879037" />
                  <Point X="-4.710795593825" Y="-23.966692713337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683969734378" Y="-21.989446701995" />
                  <Point X="-4.073216934224" Y="-22.824190015853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.482391944102" Y="-23.701668656236" />
                  <Point X="-4.773176349151" Y="-24.32525782524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.601383121186" Y="-22.037128288861" />
                  <Point X="-3.996018516571" Y="-22.883426625342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.383633870078" Y="-23.714670433435" />
                  <Point X="-4.715730502267" Y="-24.426853959442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.518796507993" Y="-22.084809875728" />
                  <Point X="-3.918820098917" Y="-22.942663234831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.284875796054" Y="-23.727672210634" />
                  <Point X="-4.622551942552" Y="-24.45182104369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.041744490766" Y="-21.286557673741" />
                  <Point X="-3.05311642548" Y="-21.310944866434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.4362098948" Y="-22.132491462594" />
                  <Point X="-3.841621681263" Y="-23.00189984432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.18611772203" Y="-23.740673987833" />
                  <Point X="-4.529373382837" Y="-24.476788127937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.878595685511" Y="-21.161473082198" />
                  <Point X="-2.995129615036" Y="-21.411380900538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.353623281608" Y="-22.18017304946" />
                  <Point X="-3.76442326361" Y="-23.061136453809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.087359648006" Y="-23.753675765032" />
                  <Point X="-4.436194823122" Y="-24.501755212185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.715446880256" Y="-21.036388490656" />
                  <Point X="-2.937142804592" Y="-21.511816934642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.27103647932" Y="-22.227854230811" />
                  <Point X="-3.687224845956" Y="-23.120373063298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.988601573982" Y="-23.766677542231" />
                  <Point X="-4.343016263407" Y="-24.526722296433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.565062988846" Y="-20.938678345193" />
                  <Point X="-2.879155994148" Y="-21.612252968746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.183006292293" Y="-22.263862035917" />
                  <Point X="-3.610026428303" Y="-23.179609672787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.889843499958" Y="-23.77967931943" />
                  <Point X="-4.249837703693" Y="-24.551689380681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760977527065" Y="-25.647832269252" />
                  <Point X="-4.781219860416" Y="-25.69124209321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.423590499314" Y="-20.86007876273" />
                  <Point X="-2.821169183704" Y="-21.71268900285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.083430926751" Y="-22.275111125799" />
                  <Point X="-3.533279210931" Y="-23.239813884402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.791085211963" Y="-23.792680637766" />
                  <Point X="-4.156659143978" Y="-24.576656464929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.641189424385" Y="-25.61573500446" />
                  <Point X="-4.756616320854" Y="-25.86326878275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.282118009782" Y="-20.781479180266" />
                  <Point X="-2.766956190936" Y="-21.821218015077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.973810181278" Y="-22.264817828901" />
                  <Point X="-3.468814516639" Y="-23.326358051766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683956654704" Y="-23.787731855739" />
                  <Point X="-4.063480584263" Y="-24.601623549177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.521401321705" Y="-25.583637739667" />
                  <Point X="-4.727569662537" Y="-26.02576717337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.168616203135" Y="-20.762862920821" />
                  <Point X="-2.751908370999" Y="-22.013737011485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808461719039" Y="-22.135016057734" />
                  <Point X="-3.422632044126" Y="-23.452108570255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.556015107505" Y="-23.738149472748" />
                  <Point X="-3.970302024548" Y="-24.626590633424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.401613219026" Y="-25.551540474875" />
                  <Point X="-4.690472382682" Y="-26.171000950388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.102444550479" Y="-20.845746504157" />
                  <Point X="-3.877123464833" Y="-24.651557717672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.281825116346" Y="-25.519443210082" />
                  <Point X="-4.638205221493" Y="-26.283702811903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.024133785812" Y="-20.902597677778" />
                  <Point X="-3.783944905118" Y="-24.67652480192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.162037013666" Y="-25.48734594529" />
                  <Point X="-4.526528485444" Y="-26.269000428985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.939684689477" Y="-20.946285156657" />
                  <Point X="-3.690766345403" Y="-24.701491886168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.042248910986" Y="-25.455248680497" />
                  <Point X="-4.414851749395" Y="-26.254298046067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.846221562736" Y="-20.970641984947" />
                  <Point X="-3.597587785689" Y="-24.726458970416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.922460808306" Y="-25.423151415705" />
                  <Point X="-4.303175013346" Y="-26.239595663149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.731438666388" Y="-20.949278419772" />
                  <Point X="-3.504520456257" Y="-24.751664588776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.802672705626" Y="-25.391054150912" />
                  <Point X="-4.191498277296" Y="-26.224893280232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.374157554698" Y="-20.407875753585" />
                  <Point X="-1.462622921973" Y="-20.597590345931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.597336481549" Y="-20.886484506729" />
                  <Point X="-3.4197390261" Y="-24.794639375473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.682884602946" Y="-25.35895688612" />
                  <Point X="-4.079821541247" Y="-26.210190897314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.253572168614" Y="-20.374068709015" />
                  <Point X="-3.348139697594" Y="-24.865883270389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.563096547328" Y="-25.326859722254" />
                  <Point X="-3.968144805198" Y="-26.195488514396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.132986782529" Y="-20.340261664444" />
                  <Point X="-3.299399737844" Y="-24.986149239798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435786025381" Y="-25.278630577284" />
                  <Point X="-3.856468069149" Y="-26.180786131478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.012401396445" Y="-20.306454619873" />
                  <Point X="-3.7447913331" Y="-26.16608374856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.894985374166" Y="-20.279444297918" />
                  <Point X="-3.633114597051" Y="-26.151381365642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.784113293964" Y="-20.266467505032" />
                  <Point X="-3.521437861002" Y="-26.136678982725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.102620278502" Y="-27.383028699134" />
                  <Point X="-4.152922197801" Y="-27.490901513184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.673241490063" Y="-20.253491304676" />
                  <Point X="-3.409761124952" Y="-26.121976599807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.93939606875" Y="-27.257782402125" />
                  <Point X="-4.094097578979" Y="-27.589540861423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.562369686162" Y="-20.24051510432" />
                  <Point X="-3.298084388903" Y="-26.107274216889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776171858998" Y="-27.132536105116" />
                  <Point X="-4.035272960156" Y="-27.688180209662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.451497882261" Y="-20.227538903964" />
                  <Point X="-3.190496035317" Y="-26.101339398457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.612947649246" Y="-27.007289808108" />
                  <Point X="-3.973554877886" Y="-27.780614505512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.356736450857" Y="-20.249111508921" />
                  <Point X="-3.098112117984" Y="-26.128010598791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.449723439494" Y="-26.882043511099" />
                  <Point X="-3.908555161945" Y="-27.866011315244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.318484659291" Y="-20.391869427586" />
                  <Point X="-3.019834593507" Y="-26.18493305623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.286499229742" Y="-26.75679721409" />
                  <Point X="-3.843555446003" Y="-27.951408124977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.280232867725" Y="-20.53462734625" />
                  <Point X="-2.959409485957" Y="-26.280140145316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.123275179574" Y="-26.631551259312" />
                  <Point X="-3.761017698" Y="-27.99919450358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.241981097064" Y="-20.677385309745" />
                  <Point X="-3.617580137395" Y="-27.916380812601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.195245682028" Y="-20.801950039166" />
                  <Point X="-3.474142576791" Y="-27.833567121623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.124960187437" Y="-20.876011460006" />
                  <Point X="-3.330705016186" Y="-27.750753430644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.036197870565" Y="-20.910449207592" />
                  <Point X="-3.187267455581" Y="-27.667939739665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.392234154245" Y="-20.216462915819" />
                  <Point X="0.358293909358" Y="-20.289248005862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.071309177033" Y="-20.904688750413" />
                  <Point X="-3.043829894976" Y="-27.585126048687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.492174491378" Y="-20.226929321598" />
                  <Point X="-2.900392334371" Y="-27.502312357708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.592114828511" Y="-20.237395727377" />
                  <Point X="-2.756954773766" Y="-27.419498666729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692055165645" Y="-20.247862133156" />
                  <Point X="-2.622464282692" Y="-27.355872028278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791995502778" Y="-20.258328538935" />
                  <Point X="-2.516417522228" Y="-27.353243166964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.888264169531" Y="-20.276668867254" />
                  <Point X="-2.429696353109" Y="-27.392058170033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.982478143248" Y="-20.299415499009" />
                  <Point X="-2.357873207368" Y="-27.462822087339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.076692116964" Y="-20.322162130765" />
                  <Point X="-2.310769244996" Y="-27.586596464447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.515193463652" Y="-28.024985616074" />
                  <Point X="-2.914961957782" Y="-28.88229191834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.170906090681" Y="-20.344908762521" />
                  <Point X="-2.837833198269" Y="-28.941677910193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.265120064398" Y="-20.367655394276" />
                  <Point X="-2.760704414554" Y="-29.001063850145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.359334038114" Y="-20.390402026032" />
                  <Point X="-2.680082275943" Y="-29.052958266347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.453548011831" Y="-20.413148657787" />
                  <Point X="-2.598745419286" Y="-29.103319964752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.543208873485" Y="-20.44565946987" />
                  <Point X="-2.517408562628" Y="-29.153681663157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.632865611514" Y="-20.478179125095" />
                  <Point X="-2.327270408561" Y="-28.970718226307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.722522349544" Y="-20.510698780321" />
                  <Point X="-2.175008053098" Y="-28.868979701683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.812179087573" Y="-20.543218435546" />
                  <Point X="-2.032686489471" Y="-28.788559273945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.900085947039" Y="-20.579490717461" />
                  <Point X="-1.922677643876" Y="-28.77743369365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.986140157684" Y="-20.619736017594" />
                  <Point X="-1.82096525288" Y="-28.784099917657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072194368328" Y="-20.659981317727" />
                  <Point X="-1.719252861884" Y="-28.790766141664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.158248578973" Y="-20.700226617859" />
                  <Point X="-1.618351934733" Y="-28.799172555502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.244302789618" Y="-20.740471917992" />
                  <Point X="-1.528919284875" Y="-28.832172769361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326988928198" Y="-20.787940071976" />
                  <Point X="-1.452090662466" Y="-28.892202407312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.409416715369" Y="-20.835962262345" />
                  <Point X="-1.377693752453" Y="-28.957446869324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.49184450254" Y="-20.883984452714" />
                  <Point X="-1.30329684244" Y="-29.022691331336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574272289711" Y="-20.932006643084" />
                  <Point X="-1.231565759561" Y="-29.093652678085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.655472872302" Y="-20.982660582167" />
                  <Point X="-1.18069066173" Y="-29.209339829105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.73419017287" Y="-21.038639936734" />
                  <Point X="2.239046903242" Y="-22.100478105096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013485223517" Y="-22.584196688267" />
                  <Point X="-1.149347436612" Y="-29.366913216329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.812907489828" Y="-21.094619256153" />
                  <Point X="2.7840477986" Y="-21.156509063716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.037572124584" Y="-22.757331312636" />
                  <Point X="-0.582833748428" Y="-28.376809841853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.653695224386" Y="-28.528772767443" />
                  <Point X="-1.120298009303" Y="-29.529405668827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.096013114781" Y="-22.856793355115" />
                  <Point X="-0.348259086342" Y="-28.098552006033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.795290605187" Y="-29.057214191882" />
                  <Point X="-1.113021339035" Y="-29.738589949478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.16630369744" Y="-22.930843864557" />
                  <Point X="-0.223340165781" Y="-28.055451666786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.936885985988" Y="-29.585655616322" />
                  <Point X="-1.018375414495" Y="-29.760410259702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.246197360556" Y="-22.984300501499" />
                  <Point X="-0.100782383367" Y="-28.017414804638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.336592040887" Y="-23.01523763435" />
                  <Point X="0.013200410444" Y="-27.997767064891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.437031700791" Y="-23.024633238993" />
                  <Point X="0.109394148854" Y="-28.016268077559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.551880368325" Y="-23.003128627053" />
                  <Point X="0.200962639575" Y="-28.044687965908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.684098613274" Y="-22.944374836144" />
                  <Point X="0.292531130295" Y="-28.073107854258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.827536206294" Y="-22.861561075651" />
                  <Point X="0.380450793942" Y="-28.109352677518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.970973799313" Y="-22.778747315158" />
                  <Point X="0.454701954433" Y="-28.174909700388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.114411392333" Y="-22.695933554666" />
                  <Point X="0.51752492706" Y="-28.264974551221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.257848985353" Y="-22.613119794173" />
                  <Point X="0.580222029876" Y="-28.355309330737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.401286578373" Y="-22.53030603368" />
                  <Point X="0.939833678481" Y="-27.808908812005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.7272262849" Y="-28.264846838893" />
                  <Point X="0.640607962622" Y="-28.45060043046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.544724171393" Y="-22.447492273187" />
                  <Point X="3.163979113641" Y="-23.264002684486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001160091686" Y="-23.613169203859" />
                  <Point X="1.100174562762" Y="-27.689845826425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.741745709822" Y="-28.458498982066" />
                  <Point X="0.681687799334" Y="-28.587293586738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.688161764413" Y="-22.364678512694" />
                  <Point X="3.327884907133" Y="-23.137294726429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.033665958568" Y="-23.768249297772" />
                  <Point X="1.216987804396" Y="-27.664128171732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764824039733" Y="-28.633796494256" />
                  <Point X="0.719939628234" Y="-28.730051425338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.831599357432" Y="-22.281864752202" />
                  <Point X="3.491108856706" Y="-23.012048987377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.091493462646" Y="-23.86902696548" />
                  <Point X="1.326508018997" Y="-27.654050463985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.787902369645" Y="-28.809094006446" />
                  <Point X="0.758191473879" Y="-28.87280922803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.955760318948" Y="-22.240389861374" />
                  <Point X="3.654332806279" Y="-22.886803248324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.159874231151" Y="-23.947173084592" />
                  <Point X="1.434546870726" Y="-27.647149549168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.810980699557" Y="-28.984391518636" />
                  <Point X="0.796443319524" Y="-29.015567030722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.019362961879" Y="-22.328782703844" />
                  <Point X="3.817556755852" Y="-22.761557509272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.241465239394" Y="-23.996989753163" />
                  <Point X="1.529217273156" Y="-27.668917366388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.08026592256" Y="-22.422965033584" />
                  <Point X="3.980780705425" Y="-22.636311770219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330206833789" Y="-24.031471940246" />
                  <Point X="1.611724409103" Y="-27.716769392759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.428287712011" Y="-24.045925968527" />
                  <Point X="1.692368932456" Y="-27.768615804727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.534600162214" Y="-24.042727333731" />
                  <Point X="1.773013366525" Y="-27.820462408163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.646276992587" Y="-24.028024748534" />
                  <Point X="2.177828325698" Y="-27.177123077092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.00058953914" Y="-27.557212881447" />
                  <Point X="1.845001459983" Y="-27.890872593949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.757953822961" Y="-24.013322163337" />
                  <Point X="2.322164809868" Y="-27.092381638307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.028019755189" Y="-27.723177743699" />
                  <Point X="1.910200549561" Y="-27.975841845537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.869630653334" Y="-23.99861957814" />
                  <Point X="3.570959161197" Y="-24.639122659988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356290602752" Y="-25.099480869189" />
                  <Point X="2.454794221194" Y="-27.032746098253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.066029840303" Y="-27.866454003523" />
                  <Point X="1.975399688574" Y="-28.060810991113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.981307483708" Y="-23.983916992943" />
                  <Point X="3.705692445614" Y="-24.574975349531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.395389624721" Y="-25.240421896391" />
                  <Point X="3.008269648838" Y="-26.070603363739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868347165904" Y="-26.370668096725" />
                  <Point X="2.561593750223" Y="-27.028502919544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.123749737714" Y="-27.967462434472" />
                  <Point X="2.040598858494" Y="-28.145780070407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.092984314081" Y="-23.969214407746" />
                  <Point X="3.825480617421" Y="-24.542877936495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.458880852339" Y="-25.329053669771" />
                  <Point X="3.167630520573" Y="-25.953642021843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876761312604" Y="-26.577413051297" />
                  <Point X="2.658273069823" Y="-27.04596259999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.181736579983" Y="-28.067898400327" />
                  <Point X="2.105798028414" Y="-28.230749149701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204661144455" Y="-23.954511822549" />
                  <Point X="3.945268775108" Y="-24.51078055374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.531184392756" Y="-25.398787377369" />
                  <Point X="3.28445588201" Y="-25.92789837615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.93449066364" Y="-26.678401208883" />
                  <Point X="2.754952302528" Y="-27.063422466784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239723422252" Y="-28.168334366182" />
                  <Point X="2.170997198334" Y="-28.315718228995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.316337974828" Y="-23.939809237352" />
                  <Point X="4.065056932795" Y="-24.478683170986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613141634696" Y="-25.447818655242" />
                  <Point X="3.398311253672" Y="-25.908523894084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.995245695729" Y="-26.772900772511" />
                  <Point X="2.845836882622" Y="-27.093309006205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.297710264521" Y="-28.268770332037" />
                  <Point X="2.236196368254" Y="-28.400687308289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.428014805201" Y="-23.925106652155" />
                  <Point X="4.184845090481" Y="-24.446585788231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.700115131591" Y="-25.486092539649" />
                  <Point X="3.498973637826" Y="-25.917441865031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.065844029316" Y="-26.846291307957" />
                  <Point X="2.928477541807" Y="-27.140874691066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.35569710679" Y="-28.369206297892" />
                  <Point X="2.301395538175" Y="-28.485656387583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.539691635575" Y="-23.910404066958" />
                  <Point X="4.304633248168" Y="-24.414488405477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.793293734972" Y="-25.511059530255" />
                  <Point X="3.597731546833" Y="-25.93044399611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.143042477457" Y="-26.905527852066" />
                  <Point X="3.011064352804" Y="-27.188555853741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.413683949059" Y="-28.469642263747" />
                  <Point X="2.366594708095" Y="-28.570625466878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.651368465948" Y="-23.895701481761" />
                  <Point X="4.424421405854" Y="-24.382391022722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.886472338353" Y="-25.53602652086" />
                  <Point X="3.696489706111" Y="-25.94344559048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220240925598" Y="-26.964764396175" />
                  <Point X="3.093651024561" Y="-27.236237315015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.471670791328" Y="-28.570078229602" />
                  <Point X="2.431793878015" Y="-28.655594546172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.723486724245" Y="-23.96583252815" />
                  <Point X="4.544209563541" Y="-24.350293639968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979650941734" Y="-25.560993511465" />
                  <Point X="3.79524786539" Y="-25.95644718485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.297439373739" Y="-27.024000940284" />
                  <Point X="3.176237696318" Y="-27.283918776289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.529657633597" Y="-28.670514195457" />
                  <Point X="2.496993047935" Y="-28.740563625466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.757614088858" Y="-24.117435308956" />
                  <Point X="4.663997721228" Y="-24.318196257213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.072829545116" Y="-25.585960502071" />
                  <Point X="3.894006024668" Y="-25.96944877922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.374637821879" Y="-27.083237484393" />
                  <Point X="3.258824368076" Y="-27.331600237563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.587644475867" Y="-28.770950161313" />
                  <Point X="2.562192217855" Y="-28.82553270476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783852047273" Y="-24.285956975955" />
                  <Point X="4.783785878914" Y="-24.286098874458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.166008148497" Y="-25.610927492676" />
                  <Point X="3.992764183947" Y="-25.98245037359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.45183627002" Y="-27.142474028502" />
                  <Point X="3.341411039833" Y="-27.379281698836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.645631318136" Y="-28.871386127168" />
                  <Point X="2.627391387775" Y="-28.910501784054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.259186751878" Y="-25.635894483281" />
                  <Point X="4.091522343225" Y="-25.99545196796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.529034718161" Y="-27.201710572611" />
                  <Point X="3.423997711591" Y="-27.42696316011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.703618160405" Y="-28.971822093023" />
                  <Point X="2.692590557696" Y="-28.995470863349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.352365355259" Y="-25.660861473886" />
                  <Point X="4.190280502503" Y="-26.00845356233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.606233166302" Y="-27.26094711672" />
                  <Point X="3.506584383348" Y="-27.474644621384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.44554395864" Y="-25.685828464492" />
                  <Point X="4.289038661782" Y="-26.0214551567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.683431614443" Y="-27.320183660829" />
                  <Point X="3.589171055106" Y="-27.522326082658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.538722562021" Y="-25.710795455097" />
                  <Point X="4.38779682106" Y="-26.03445675107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.760630062583" Y="-27.379420204938" />
                  <Point X="3.671757726863" Y="-27.570007543932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.631901165402" Y="-25.735762445702" />
                  <Point X="4.486554980339" Y="-26.04745834544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.837828510724" Y="-27.438656749047" />
                  <Point X="3.75434439862" Y="-27.617689005206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.725079768783" Y="-25.760729436308" />
                  <Point X="4.585313139617" Y="-26.060459939811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.915026958865" Y="-27.497893293156" />
                  <Point X="3.836931070378" Y="-27.665370466479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766934809761" Y="-25.895760161672" />
                  <Point X="4.684071298896" Y="-26.073461534181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.992225407006" Y="-27.557129837265" />
                  <Point X="3.919517742135" Y="-27.713051927753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.069423855147" Y="-27.616366381374" />
                  <Point X="4.004397713154" Y="-27.755815392892" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998374023438" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.511166687012" Y="-28.68500390625" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.448122070312" Y="-28.49820703125" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.27433605957" Y="-28.266400390625" />
                  <Point X="0.24927230835" Y="-28.258623046875" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="-0.008664604187" Y="-28.187765625" />
                  <Point X="-0.033728199005" Y="-28.195544921875" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.304475891113" Y="-28.30898046875" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.820180847168" Y="-29.884208984375" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-1.100230712891" Y="-29.938068359375" />
                  <Point X="-1.1280390625" Y="-29.9309140625" />
                  <Point X="-1.351589477539" Y="-29.873396484375" />
                  <Point X="-1.313920532227" Y="-29.5872734375" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.315671020508" Y="-29.50465625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.409358154297" Y="-29.182392578125" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.67986706543" Y="-28.983755859375" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.015397827148" Y="-28.990841796875" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.462103759766" Y="-29.4113984375" />
                  <Point X="-2.855837890625" Y="-29.167609375" />
                  <Point X="-2.894320556641" Y="-29.137978515625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.579677001953" Y="-27.756673828125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499764160156" Y="-27.59758203125" />
                  <Point X="-2.5156953125" Y="-27.567048828125" />
                  <Point X="-2.531327636719" Y="-27.551416015625" />
                  <Point X="-2.560156982422" Y="-27.53719921875" />
                  <Point X="-2.591683837891" Y="-27.543470703125" />
                  <Point X="-3.753529052734" Y="-28.21426171875" />
                  <Point X="-3.842959472656" Y="-28.26589453125" />
                  <Point X="-3.850635498047" Y="-28.255810546875" />
                  <Point X="-4.161703613281" Y="-27.84712890625" />
                  <Point X="-4.189294433594" Y="-27.80086328125" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.292638916016" Y="-26.522017578125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145059814453" Y="-26.393072265625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140323486328" Y="-26.334603515625" />
                  <Point X="-3.163781005859" Y="-26.309095703125" />
                  <Point X="-3.187643798828" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-4.686249023438" Y="-26.48166796875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.805732421875" Y="-26.48748828125" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.934692871094" Y="-25.960154296875" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.704245605469" Y="-25.1679765625" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.539142333984" Y="-25.119513671875" />
                  <Point X="-3.514144042969" Y="-25.1021640625" />
                  <Point X="-3.493984130859" Y="-25.0729609375" />
                  <Point X="-3.485648193359" Y="-25.046103515625" />
                  <Point X="-3.486562744141" Y="-25.01351171875" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.516887451172" Y="-24.9584921875" />
                  <Point X="-3.541894775391" Y="-24.941134765625" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.894448242188" Y="-24.575669921875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.996054199219" Y="-24.533462890625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.902949707031" Y="-23.9493515625" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.858483154297" Y="-23.59216796875" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731705078125" Y="-23.6041328125" />
                  <Point X="-3.725628662109" Y="-23.602216796875" />
                  <Point X="-3.670279052734" Y="-23.584765625" />
                  <Point X="-3.651534667969" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.636681884766" Y="-23.550328125" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.619258056641" Y="-23.4488359375" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968017578" Y="-23.380779296875" />
                  <Point X="-4.426865234375" Y="-22.792318359375" />
                  <Point X="-4.476105957031" Y="-22.754533203125" />
                  <Point X="-4.46468359375" Y="-22.73496484375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.121088378906" Y="-22.1629609375" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.223778076172" Y="-22.035748046875" />
                  <Point X="-3.159156738281" Y="-22.073056640625" />
                  <Point X="-3.138514648438" Y="-22.07956640625" />
                  <Point X="-3.130051513672" Y="-22.080306640625" />
                  <Point X="-3.052965332031" Y="-22.08705078125" />
                  <Point X="-3.031509033203" Y="-22.0842265625" />
                  <Point X="-3.013255859375" Y="-22.072599609375" />
                  <Point X="-3.007245117188" Y="-22.06658984375" />
                  <Point X="-2.952528564453" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.938814697266" Y="-21.963697265625" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.291902832031" Y="-21.27735546875" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.283513671875" Y="-21.23250390625" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.691568847656" Y="-20.791607421875" />
                  <Point X="-2.141548828125" Y="-20.48602734375" />
                  <Point X="-1.987608398438" Y="-20.6866484375" />
                  <Point X="-1.967825805664" Y="-20.712427734375" />
                  <Point X="-1.951247070312" Y="-20.726337890625" />
                  <Point X="-1.941827880859" Y="-20.7312421875" />
                  <Point X="-1.856031005859" Y="-20.77590625" />
                  <Point X="-1.835124267578" Y="-20.781509765625" />
                  <Point X="-1.813808837891" Y="-20.77775" />
                  <Point X="-1.803998046875" Y="-20.773685546875" />
                  <Point X="-1.714634887695" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.682890014648" Y="-20.695384765625" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.689137695313" Y="-20.298859375" />
                  <Point X="-1.655037475586" Y="-20.289298828125" />
                  <Point X="-0.968083435059" Y="-20.096703125" />
                  <Point X="-0.893773803711" Y="-20.088005859375" />
                  <Point X="-0.224199981689" Y="-20.009640625" />
                  <Point X="-0.061257141113" Y="-20.617751953125" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282178879" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594097137" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.228580078125" Y="-20.039244140625" />
                  <Point X="0.23664831543" Y="-20.0091328125" />
                  <Point X="0.260404785156" Y="-20.01162109375" />
                  <Point X="0.860210449219" Y="-20.074435546875" />
                  <Point X="0.921694946289" Y="-20.089279296875" />
                  <Point X="1.508456542969" Y="-20.230943359375" />
                  <Point X="1.548115112305" Y="-20.245326171875" />
                  <Point X="1.931044921875" Y="-20.38421875" />
                  <Point X="1.969736206055" Y="-20.4023125" />
                  <Point X="2.338685058594" Y="-20.574859375" />
                  <Point X="2.376094726562" Y="-20.596654296875" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.767786376953" Y="-20.82938671875" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.315090087891" Y="-22.348767578125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.222728027344" Y="-22.5164296875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202872802734" Y="-22.61454296875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.222932128906" Y="-22.705451171875" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.281202148438" Y="-22.780046875" />
                  <Point X="2.338248046875" Y="-22.818755859375" />
                  <Point X="2.360337158203" Y="-22.827021484375" />
                  <Point X="2.367205078125" Y="-22.827849609375" />
                  <Point X="2.429761962891" Y="-22.835392578125" />
                  <Point X="2.456606689453" Y="-22.8319296875" />
                  <Point X="2.528950683594" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.885782714844" Y="-22.03119140625" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.222250488281" Y="-22.290607421875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.400837890625" Y="-23.320806640625" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.273655273438" Y="-23.423625" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.210989990234" Y="-23.51611328125" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.19252734375" Y="-23.617505859375" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.220400878906" Y="-23.7192734375" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280943603516" Y="-23.801177734375" />
                  <Point X="3.287837158203" Y="-23.80505859375" />
                  <Point X="3.350590087891" Y="-23.8403828125" />
                  <Point X="3.377880615234" Y="-23.847611328125" />
                  <Point X="3.4627265625" Y="-23.85882421875" />
                  <Point X="3.475803466797" Y="-23.858828125" />
                  <Point X="4.753225097656" Y="-23.69065234375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.945383300781" Y="-24.088404296875" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="3.869776367188" Y="-24.727712890625" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.719935791016" Y="-24.772470703125" />
                  <Point X="3.636577392578" Y="-24.82065234375" />
                  <Point X="3.616774169922" Y="-24.8400703125" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.555154785156" Y="-24.934822265625" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.540313476562" Y="-25.0502421875" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759277344" Y="-25.158759765625" />
                  <Point X="3.572250244141" Y="-25.1657578125" />
                  <Point X="3.622265136719" Y="-25.22948828125" />
                  <Point X="3.645728271484" Y="-25.247197265625" />
                  <Point X="3.729086669922" Y="-25.295380859375" />
                  <Point X="3.741167480469" Y="-25.300388671875" />
                  <Point X="4.91262109375" Y="-25.61427734375" />
                  <Point X="4.998068359375" Y="-25.637173828125" />
                  <Point X="4.948431640625" Y="-25.966400390625" />
                  <Point X="4.940494628906" Y="-26.001181640625" />
                  <Point X="4.874545898438" Y="-26.290177734375" />
                  <Point X="3.561132568359" Y="-26.117265625" />
                  <Point X="3.411982421875" Y="-26.09762890625" />
                  <Point X="3.376875244141" Y="-26.10224609375" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.197945556641" Y="-26.143921875" />
                  <Point X="3.174588867188" Y="-26.16775390625" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.067953613281" Y="-26.299517578125" />
                  <Point X="3.062802001953" Y="-26.33098046875" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.049849121094" Y="-26.501458984375" />
                  <Point X="3.066300537109" Y="-26.532083984375" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.255576660156" Y="-27.51971484375" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.204134765625" Y="-27.802138671875" />
                  <Point X="4.187716308594" Y="-27.82546484375" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="2.886292724609" Y="-27.33591015625" />
                  <Point X="2.753454345703" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.715963867188" Y="-27.249453125" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.471318603516" Y="-27.228591796875" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.279253417969" Y="-27.352443359375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.193023681641" Y="-27.567751953125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.932673095703" Y="-28.988556640625" />
                  <Point X="2.986674072266" Y="-29.082087890625" />
                  <Point X="2.835298339844" Y="-29.190212890625" />
                  <Point X="2.816947265625" Y="-29.20208984375" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="1.784545410156" Y="-28.124193359375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.649465942383" Y="-27.966912109375" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.402746582031" Y="-27.83783984375" />
                  <Point X="1.192718139648" Y="-27.857166015625" />
                  <Point X="1.165332641602" Y="-27.868509765625" />
                  <Point X="1.147527709961" Y="-27.883314453125" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.963133117676" Y="-28.07048046875" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.111902099609" Y="-29.814470703125" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="0.994363769531" Y="-29.963244140625" />
                  <Point X="0.977389160156" Y="-29.966328125" />
                  <Point X="0.860200561523" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#126" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.019116756578" Y="4.426469184036" Z="0.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.25" />
                  <Point X="-0.905741999842" Y="4.992935613568" Z="0.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.25" />
                  <Point X="-1.674690981367" Y="4.790125262255" Z="0.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.25" />
                  <Point X="-1.746281347703" Y="4.736646270378" Z="0.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.25" />
                  <Point X="-1.736731914597" Y="4.350931711748" Z="0.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.25" />
                  <Point X="-1.829288251499" Y="4.303788378637" Z="0.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.25" />
                  <Point X="-1.924895927598" Y="4.344387924461" Z="0.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.25" />
                  <Point X="-1.954097725343" Y="4.375072419486" Z="0.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.25" />
                  <Point X="-2.72200787849" Y="4.283379987571" Z="0.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.25" />
                  <Point X="-3.320091864116" Y="3.83845708633" Z="0.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.25" />
                  <Point X="-3.34136016004" Y="3.728925181891" Z="0.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.25" />
                  <Point X="-2.994780435264" Y="3.063226542393" Z="0.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.25" />
                  <Point X="-3.048756466976" Y="3.000047100911" Z="0.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.25" />
                  <Point X="-3.131849923214" Y="3.000784263909" Z="0.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.25" />
                  <Point X="-3.204934166406" Y="3.038833788706" Z="0.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.25" />
                  <Point X="-4.166707661684" Y="2.899023067301" Z="0.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.25" />
                  <Point X="-4.514021692094" Y="2.321520262217" Z="0.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.25" />
                  <Point X="-4.4634597207" Y="2.199295151102" Z="0.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.25" />
                  <Point X="-3.669764454696" Y="1.55935595192" Z="0.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.25" />
                  <Point X="-3.689031651478" Y="1.500086582201" Z="0.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.25" />
                  <Point X="-3.746819475151" Y="1.476748476052" Z="0.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.25" />
                  <Point X="-3.858112894101" Y="1.488684596593" Z="0.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.25" />
                  <Point X="-4.957365313686" Y="1.095006824121" Z="0.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.25" />
                  <Point X="-5.051671466152" Y="0.505136655274" Z="0.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.25" />
                  <Point X="-4.913545175851" Y="0.407312938614" Z="0.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.25" />
                  <Point X="-3.551553318211" Y="0.031712456245" Z="0.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.25" />
                  <Point X="-3.540471910368" Y="0.002948658545" Z="0.25" />
                  <Point X="-3.539556741714" Y="0" Z="0.25" />
                  <Point X="-3.547892690767" Y="-0.026858292516" Z="0.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.25" />
                  <Point X="-3.573815276912" Y="-0.047163526889" Z="0.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.25" />
                  <Point X="-3.723342612921" Y="-0.088399118142" Z="0.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.25" />
                  <Point X="-4.990344782163" Y="-0.935951681418" Z="0.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.25" />
                  <Point X="-4.860333250768" Y="-1.468582225236" Z="0.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.25" />
                  <Point X="-4.685878382942" Y="-1.499960555699" Z="0.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.25" />
                  <Point X="-3.195296100457" Y="-1.320907906204" Z="0.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.25" />
                  <Point X="-3.199618633322" Y="-1.349254406087" Z="0.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.25" />
                  <Point X="-3.329232831526" Y="-1.451068805677" Z="0.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.25" />
                  <Point X="-4.238393946334" Y="-2.795193004879" Z="0.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.25" />
                  <Point X="-3.896577402472" Y="-3.254812298797" Z="0.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.25" />
                  <Point X="-3.734684794042" Y="-3.226282655422" Z="0.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.25" />
                  <Point X="-2.557207177176" Y="-2.571123291956" Z="0.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.25" />
                  <Point X="-2.629134357747" Y="-2.700393588451" Z="0.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.25" />
                  <Point X="-2.930980486536" Y="-4.146314673662" Z="0.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.25" />
                  <Point X="-2.494581307287" Y="-4.422630015827" Z="0.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.25" />
                  <Point X="-2.428869924288" Y="-4.420547642742" Z="0.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.25" />
                  <Point X="-1.993775362114" Y="-4.001135887661" Z="0.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.25" />
                  <Point X="-1.68929301696" Y="-4.002368587326" Z="0.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.25" />
                  <Point X="-1.448481356982" Y="-4.188703915187" Z="0.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.25" />
                  <Point X="-1.370866224661" Y="-4.483130111879" Z="0.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.25" />
                  <Point X="-1.369648759305" Y="-4.549465664335" Z="0.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.25" />
                  <Point X="-1.146653743667" Y="-4.948057311419" Z="0.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.25" />
                  <Point X="-0.847319301783" Y="-5.00800754434" Z="0.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.25" />
                  <Point X="-0.778040495755" Y="-4.865870781312" Z="0.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.25" />
                  <Point X="-0.269555874626" Y="-3.30620917091" Z="0.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.25" />
                  <Point X="-0.025063597175" Y="-3.212018184068" Z="0.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.25" />
                  <Point X="0.228295482187" Y="-3.27509386122" Z="0.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.25" />
                  <Point X="0.400889984807" Y="-3.495436584858" Z="0.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.25" />
                  <Point X="0.456714364646" Y="-3.666665248503" Z="0.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.25" />
                  <Point X="0.980169782685" Y="-4.984242322291" Z="0.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.25" />
                  <Point X="1.159341128603" Y="-4.945637721276" Z="0.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.25" />
                  <Point X="1.155318397258" Y="-4.776664808499" Z="0.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.25" />
                  <Point X="1.005836432452" Y="-3.049818673994" Z="0.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.25" />
                  <Point X="1.173338824455" Y="-2.890479652521" Z="0.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.25" />
                  <Point X="1.401172307812" Y="-2.856348421006" Z="0.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.25" />
                  <Point X="1.616270618451" Y="-2.977690669625" Z="0.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.25" />
                  <Point X="1.738721832971" Y="-3.123350508847" Z="0.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.25" />
                  <Point X="2.837960718078" Y="-4.212784846692" Z="0.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.25" />
                  <Point X="3.027792095669" Y="-4.078486598001" Z="0.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.25" />
                  <Point X="2.969818300661" Y="-3.932276600952" Z="0.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.25" />
                  <Point X="2.236071746353" Y="-2.527584971089" Z="0.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.25" />
                  <Point X="2.317343834867" Y="-2.3444490492" Z="0.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.25" />
                  <Point X="2.488449329172" Y="-2.2415574751" Z="0.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.25" />
                  <Point X="2.700921832423" Y="-2.267376263138" Z="0.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.25" />
                  <Point X="2.855136996155" Y="-2.347931199613" Z="0.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.25" />
                  <Point X="4.222449192394" Y="-2.8229623523" Z="0.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.25" />
                  <Point X="4.383431332441" Y="-2.565876769061" Z="0.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.25" />
                  <Point X="4.279858684945" Y="-2.448766479465" Z="0.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.25" />
                  <Point X="3.102202541878" Y="-1.473763569655" Z="0.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.25" />
                  <Point X="3.106435467438" Y="-1.304281541985" Z="0.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.25" />
                  <Point X="3.206879074862" Y="-1.16844112681" Z="0.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.25" />
                  <Point X="3.381338572542" Y="-1.119824384595" Z="0.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.25" />
                  <Point X="3.548450062182" Y="-1.135556419612" Z="0.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.25" />
                  <Point X="4.983085971276" Y="-0.981024214964" Z="0.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.25" />
                  <Point X="5.042418253526" Y="-0.606284112777" Z="0.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.25" />
                  <Point X="4.919406185107" Y="-0.53470063786" Z="0.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.25" />
                  <Point X="3.664594283302" Y="-0.172627978059" Z="0.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.25" />
                  <Point X="3.605427664875" Y="-0.103607280602" Z="0.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.25" />
                  <Point X="3.583265040437" Y="-0.009557215996" Z="0.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.25" />
                  <Point X="3.598106409987" Y="0.087053315246" Z="0.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.25" />
                  <Point X="3.649951773525" Y="0.160341457973" Z="0.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.25" />
                  <Point X="3.738801131051" Y="0.215520866183" Z="0.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.25" />
                  <Point X="3.8765616322" Y="0.255271294909" Z="0.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.25" />
                  <Point X="4.988632276004" Y="0.950567594062" Z="0.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.25" />
                  <Point X="4.890809300289" Y="1.367488253132" Z="0.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.25" />
                  <Point X="4.740542767826" Y="1.390199846582" Z="0.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.25" />
                  <Point X="3.378275494314" Y="1.233237505042" Z="0.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.25" />
                  <Point X="3.306207907303" Y="1.269792920688" Z="0.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.25" />
                  <Point X="3.256015099138" Y="1.339490375954" Z="0.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.25" />
                  <Point X="3.235339939004" Y="1.423877963189" Z="0.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.25" />
                  <Point X="3.252986724602" Y="1.501699974397" Z="0.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.25" />
                  <Point X="3.307182046088" Y="1.577237981017" Z="0.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.25" />
                  <Point X="3.425120261183" Y="1.670806145453" Z="0.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.25" />
                  <Point X="4.258871994499" Y="2.76656022489" Z="0.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.25" />
                  <Point X="4.025599872912" Y="3.096191071064" Z="0.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.25" />
                  <Point X="3.854626853641" Y="3.043389882309" Z="0.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.25" />
                  <Point X="2.437533990362" Y="2.247653058555" Z="0.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.25" />
                  <Point X="2.3670346569" Y="2.253072390184" Z="0.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.25" />
                  <Point X="2.303120899549" Y="2.292608488689" Z="0.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.25" />
                  <Point X="2.258150111546" Y="2.353903960833" Z="0.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.25" />
                  <Point X="2.246357312575" Y="2.422723786932" Z="0.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.25" />
                  <Point X="2.264874884102" Y="2.50193558552" Z="0.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.25" />
                  <Point X="2.35223547055" Y="2.657512228314" Z="0.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.25" />
                  <Point X="2.790607524574" Y="4.242641643271" Z="0.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.25" />
                  <Point X="2.395108745624" Y="4.477830317138" Z="0.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.25" />
                  <Point X="1.984761857027" Y="4.674258098072" Z="0.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.25" />
                  <Point X="1.559007690187" Y="4.832957331747" Z="0.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.25" />
                  <Point X="0.927273442038" Y="4.990603827457" Z="0.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.25" />
                  <Point X="0.259456591772" Y="5.069388864444" Z="0.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.25" />
                  <Point X="0.174127804975" Y="5.004978321915" Z="0.25" />
                  <Point X="0" Y="4.355124473572" Z="0.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>