<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#211" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3419" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563302001953" Y="-28.5125234375" />
                  <Point X="0.557721374512" Y="-28.497140625" />
                  <Point X="0.542363830566" Y="-28.467376953125" />
                  <Point X="0.390801757812" Y="-28.249005859375" />
                  <Point X="0.378636199951" Y="-28.2314765625" />
                  <Point X="0.356755157471" Y="-28.2090234375" />
                  <Point X="0.330501342773" Y="-28.189779296875" />
                  <Point X="0.302497070312" Y="-28.175669921875" />
                  <Point X="0.067963447571" Y="-28.102880859375" />
                  <Point X="0.04913809967" Y="-28.097037109375" />
                  <Point X="0.020985534668" Y="-28.092767578125" />
                  <Point X="-0.008656398773" Y="-28.092765625" />
                  <Point X="-0.036822170258" Y="-28.09703515625" />
                  <Point X="-0.271355957031" Y="-28.169826171875" />
                  <Point X="-0.290181304932" Y="-28.17566796875" />
                  <Point X="-0.31818069458" Y="-28.189775390625" />
                  <Point X="-0.344437561035" Y="-28.20901953125" />
                  <Point X="-0.366323150635" Y="-28.2314765625" />
                  <Point X="-0.517885253906" Y="-28.449849609375" />
                  <Point X="-0.53005065918" Y="-28.467376953125" />
                  <Point X="-0.53818737793" Y="-28.4815703125" />
                  <Point X="-0.55099017334" Y="-28.5125234375" />
                  <Point X="-0.580754577637" Y="-28.623607421875" />
                  <Point X="-0.916584777832" Y="-29.87694140625" />
                  <Point X="-1.061343139648" Y="-29.84884375" />
                  <Point X="-1.079348632812" Y="-29.84534765625" />
                  <Point X="-1.24641809082" Y="-29.80236328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.272538818359" Y="-29.23454296875" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287939086914" Y="-29.18296484375" />
                  <Point X="-1.304011962891" Y="-29.155126953125" />
                  <Point X="-1.323645507812" Y="-29.131205078125" />
                  <Point X="-1.53957421875" Y="-28.94183984375" />
                  <Point X="-1.55690625" Y="-28.926640625" />
                  <Point X="-1.583188720703" Y="-28.910296875" />
                  <Point X="-1.612884887695" Y="-28.89799609375" />
                  <Point X="-1.64302722168" Y="-28.890966796875" />
                  <Point X="-1.929612915039" Y="-28.87218359375" />
                  <Point X="-1.952616455078" Y="-28.87067578125" />
                  <Point X="-1.983415283203" Y="-28.873708984375" />
                  <Point X="-2.014462768555" Y="-28.88202734375" />
                  <Point X="-2.042656494141" Y="-28.89480078125" />
                  <Point X="-2.281455322266" Y="-29.054361328125" />
                  <Point X="-2.300623046875" Y="-29.06716796875" />
                  <Point X="-2.312785400391" Y="-29.076822265625" />
                  <Point X="-2.335102539062" Y="-29.099462890625" />
                  <Point X="-2.351812988281" Y="-29.121240234375" />
                  <Point X="-2.480147705078" Y="-29.288490234375" />
                  <Point X="-2.775341796875" Y="-29.105712890625" />
                  <Point X="-2.801709960938" Y="-29.08938671875" />
                  <Point X="-3.104721679688" Y="-28.856078125" />
                  <Point X="-2.423760986328" Y="-27.676619140625" />
                  <Point X="-2.412859130859" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616126953125" />
                  <Point X="-2.405575195312" Y="-27.5851953125" />
                  <Point X="-2.414559326172" Y="-27.555578125" />
                  <Point X="-2.428776367188" Y="-27.526748046875" />
                  <Point X="-2.446806640625" Y="-27.5015859375" />
                  <Point X="-2.462866210938" Y="-27.48552734375" />
                  <Point X="-2.464155273438" Y="-27.48423828125" />
                  <Point X="-2.489311035156" Y="-27.466212890625" />
                  <Point X="-2.518139892578" Y="-27.45199609375" />
                  <Point X="-2.547758056641" Y="-27.44301171875" />
                  <Point X="-2.578692138672" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.639184082031" Y="-27.46119921875" />
                  <Point X="-2.734990478516" Y="-27.516513671875" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.062027832031" Y="-27.82123046875" />
                  <Point X="-4.082861083984" Y="-27.793859375" />
                  <Point X="-4.306142089844" Y="-27.419451171875" />
                  <Point X="-3.105955078125" Y="-26.498515625" />
                  <Point X="-3.084574951172" Y="-26.475591796875" />
                  <Point X="-3.066609375" Y="-26.44845703125" />
                  <Point X="-3.053855712891" Y="-26.419830078125" />
                  <Point X="-3.046723632813" Y="-26.39229296875" />
                  <Point X="-3.046151123047" Y="-26.39008203125" />
                  <Point X="-3.043347167969" Y="-26.359650390625" />
                  <Point X="-3.045557128906" Y="-26.327982421875" />
                  <Point X="-3.052558837891" Y="-26.29823828125" />
                  <Point X="-3.068640380859" Y="-26.2722578125" />
                  <Point X="-3.089471679688" Y="-26.248302734375" />
                  <Point X="-3.112973144531" Y="-26.228767578125" />
                  <Point X="-3.137488037109" Y="-26.21433984375" />
                  <Point X="-3.139455810547" Y="-26.213181640625" />
                  <Point X="-3.168715820312" Y="-26.201958984375" />
                  <Point X="-3.200604492188" Y="-26.1954765625" />
                  <Point X="-3.231928466797" Y="-26.194384765625" />
                  <Point X="-3.352875" Y="-26.210306640625" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.825928710938" Y="-26.024556640625" />
                  <Point X="-4.834076660156" Y="-25.99265625" />
                  <Point X="-4.892424316406" Y="-25.58469921875" />
                  <Point X="-3.532875" Y="-25.220408203125" />
                  <Point X="-3.517490234375" Y="-25.214828125" />
                  <Point X="-3.487730712891" Y="-25.199470703125" />
                  <Point X="-3.462040039062" Y="-25.181640625" />
                  <Point X="-3.459954345703" Y="-25.180193359375" />
                  <Point X="-3.437512695312" Y="-25.15831640625" />
                  <Point X="-3.418272216797" Y="-25.1320625" />
                  <Point X="-3.404167480469" Y="-25.10406640625" />
                  <Point X="-3.395603759766" Y="-25.076474609375" />
                  <Point X="-3.394916503906" Y="-25.074259765625" />
                  <Point X="-3.390646972656" Y="-25.046099609375" />
                  <Point X="-3.390646972656" Y="-25.016462890625" />
                  <Point X="-3.394916015625" Y="-24.9883046875" />
                  <Point X="-3.403479736328" Y="-24.9607109375" />
                  <Point X="-3.404166992188" Y="-24.95849609375" />
                  <Point X="-3.418275390625" Y="-24.930494140625" />
                  <Point X="-3.437520996094" Y="-24.904236328125" />
                  <Point X="-3.459977783203" Y="-24.8823515625" />
                  <Point X="-3.485668457031" Y="-24.864521484375" />
                  <Point X="-3.497978759766" Y="-24.857283203125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-3.643124511719" Y="-24.812609375" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.82973828125" Y="-24.058505859375" />
                  <Point X="-4.824487792969" Y="-24.0230234375" />
                  <Point X="-4.703551757812" Y="-23.576732421875" />
                  <Point X="-3.765666503906" Y="-23.70020703125" />
                  <Point X="-3.744986572266" Y="-23.700658203125" />
                  <Point X="-3.723421386719" Y="-23.698771484375" />
                  <Point X="-3.703135253906" Y="-23.694736328125" />
                  <Point X="-3.6462734375" Y="-23.67680859375" />
                  <Point X="-3.641709228516" Y="-23.675369140625" />
                  <Point X="-3.622765869141" Y="-23.667033203125" />
                  <Point X="-3.604022705078" Y="-23.656208984375" />
                  <Point X="-3.587344238281" Y="-23.6439765625" />
                  <Point X="-3.573708496094" Y="-23.62842578125" />
                  <Point X="-3.561295410156" Y="-23.6106953125" />
                  <Point X="-3.551350585938" Y="-23.59256640625" />
                  <Point X="-3.528534423828" Y="-23.537484375" />
                  <Point X="-3.526703125" Y="-23.5330625" />
                  <Point X="-3.520913818359" Y="-23.51319921875" />
                  <Point X="-3.517156738281" Y="-23.491884765625" />
                  <Point X="-3.515804931641" Y="-23.471244140625" />
                  <Point X="-3.518952636719" Y="-23.450798828125" />
                  <Point X="-3.524555664062" Y="-23.429892578125" />
                  <Point X="-3.532051269531" Y="-23.41062109375" />
                  <Point X="-3.559581298828" Y="-23.357736328125" />
                  <Point X="-3.561791015625" Y="-23.3534921875" />
                  <Point X="-3.573283935547" Y="-23.336291015625" />
                  <Point X="-3.587195068359" Y="-23.319712890625" />
                  <Point X="-3.602135498047" Y="-23.30541015625" />
                  <Point X="-3.665374267578" Y="-23.256884765625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.101551757813" Y="-22.3012890625" />
                  <Point X="-4.081151123047" Y="-22.266337890625" />
                  <Point X="-3.750504882813" Y="-21.841337890625" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.1877265625" Y="-22.163658203125" />
                  <Point X="-3.167088378906" Y="-22.170166015625" />
                  <Point X="-3.146797607422" Y="-22.174203125" />
                  <Point X="-3.067604736328" Y="-22.181130859375" />
                  <Point X="-3.061248291016" Y="-22.1816875" />
                  <Point X="-3.040582519531" Y="-22.18123828125" />
                  <Point X="-3.019122314453" Y="-22.178416015625" />
                  <Point X="-2.999022705078" Y="-22.1735" />
                  <Point X="-2.980464355469" Y="-22.16434765625" />
                  <Point X="-2.962208251953" Y="-22.152716796875" />
                  <Point X="-2.946078369141" Y="-22.139771484375" />
                  <Point X="-2.889866699219" Y="-22.083560546875" />
                  <Point X="-2.885354736328" Y="-22.079048828125" />
                  <Point X="-2.872409179688" Y="-22.062919921875" />
                  <Point X="-2.860779296875" Y="-22.044666015625" />
                  <Point X="-2.851629638672" Y="-22.02611328125" />
                  <Point X="-2.846712646484" Y="-22.00601953125" />
                  <Point X="-2.843887207031" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.850364257812" Y="-21.8846875" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-2.897818359375" Y="-21.7699296875" />
                  <Point X="-3.183332763672" Y="-21.27540625" />
                  <Point X="-2.7361484375" Y="-20.932552734375" />
                  <Point X="-2.700622070312" Y="-20.90531640625" />
                  <Point X="-2.167035888672" Y="-20.6088671875" />
                  <Point X="-2.043194946289" Y="-20.7702578125" />
                  <Point X="-2.028897949219" Y="-20.7851953125" />
                  <Point X="-2.012321289063" Y="-20.799107421875" />
                  <Point X="-1.99511706543" Y="-20.810603515625" />
                  <Point X="-1.906975830078" Y="-20.85648828125" />
                  <Point X="-1.899901000977" Y="-20.860171875" />
                  <Point X="-1.880625366211" Y="-20.86766796875" />
                  <Point X="-1.859716186523" Y="-20.873271484375" />
                  <Point X="-1.839268676758" Y="-20.876419921875" />
                  <Point X="-1.818624633789" Y="-20.87506640625" />
                  <Point X="-1.797306518555" Y="-20.871306640625" />
                  <Point X="-1.777451293945" Y="-20.865517578125" />
                  <Point X="-1.685646118164" Y="-20.827490234375" />
                  <Point X="-1.67827722168" Y="-20.8244375" />
                  <Point X="-1.660135253906" Y="-20.814484375" />
                  <Point X="-1.642406982422" Y="-20.802068359375" />
                  <Point X="-1.626860229492" Y="-20.788431640625" />
                  <Point X="-1.614632446289" Y="-20.77175390625" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.595480102539" Y="-20.734078125" />
                  <Point X="-1.565599365234" Y="-20.63930859375" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165405273" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773046875" Y="-20.569173828125" />
                  <Point X="-1.560916503906" Y="-20.544974609375" />
                  <Point X="-1.584202026367" Y="-20.368103515625" />
                  <Point X="-0.995622314453" Y="-20.2030859375" />
                  <Point X="-0.949624938965" Y="-20.19019140625" />
                  <Point X="-0.294711364746" Y="-20.113541015625" />
                  <Point X="-0.133903366089" Y="-20.713685546875" />
                  <Point X="-0.121129745483" Y="-20.741876953125" />
                  <Point X="-0.103271453857" Y="-20.768603515625" />
                  <Point X="-0.082113975525" Y="-20.791193359375" />
                  <Point X="-0.054818138123" Y="-20.805783203125" />
                  <Point X="-0.024380064011" Y="-20.816115234375" />
                  <Point X="0.006155910969" Y="-20.82115625" />
                  <Point X="0.036691894531" Y="-20.816115234375" />
                  <Point X="0.067129966736" Y="-20.805783203125" />
                  <Point X="0.094425941467" Y="-20.791193359375" />
                  <Point X="0.115583587646" Y="-20.768603515625" />
                  <Point X="0.133441726685" Y="-20.741876953125" />
                  <Point X="0.146215194702" Y="-20.713685546875" />
                  <Point X="0.160573959351" Y="-20.66009765625" />
                  <Point X="0.307419555664" Y="-20.1120625" />
                  <Point X="0.803879150391" Y="-20.1640546875" />
                  <Point X="0.844029602051" Y="-20.168259765625" />
                  <Point X="1.440255371094" Y="-20.31220703125" />
                  <Point X="1.481040283203" Y="-20.3220546875" />
                  <Point X="1.868997192383" Y="-20.46276953125" />
                  <Point X="1.894630981445" Y="-20.47206640625" />
                  <Point X="2.269900146484" Y="-20.647568359375" />
                  <Point X="2.294575439453" Y="-20.659107421875" />
                  <Point X="2.657108642578" Y="-20.8703203125" />
                  <Point X="2.680989746094" Y="-20.884232421875" />
                  <Point X="2.943260498047" Y="-21.070744140625" />
                  <Point X="2.147580810547" Y="-22.44890234375" />
                  <Point X="2.142075683594" Y="-22.4600703125" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.113202392578" Y="-22.558265625" />
                  <Point X="2.110631591797" Y="-22.571677734375" />
                  <Point X="2.107698486328" Y="-22.596544921875" />
                  <Point X="2.107727783203" Y="-22.619046875" />
                  <Point X="2.115477294922" Y="-22.6833125" />
                  <Point X="2.116099121094" Y="-22.688470703125" />
                  <Point X="2.121442871094" Y="-22.710396484375" />
                  <Point X="2.129711669922" Y="-22.732490234375" />
                  <Point X="2.140072998047" Y="-22.75253125" />
                  <Point X="2.179838867188" Y="-22.81113671875" />
                  <Point X="2.188400390625" Y="-22.82196875" />
                  <Point X="2.204889892578" Y="-22.83996875" />
                  <Point X="2.221599365234" Y="-22.854408203125" />
                  <Point X="2.280196044922" Y="-22.89416796875" />
                  <Point X="2.284876953125" Y="-22.897345703125" />
                  <Point X="2.304937255859" Y="-22.907720703125" />
                  <Point X="2.327035400391" Y="-22.9159921875" />
                  <Point X="2.348967041016" Y="-22.921337890625" />
                  <Point X="2.413233398438" Y="-22.9290859375" />
                  <Point X="2.427425537109" Y="-22.929728515625" />
                  <Point X="2.451488525391" Y="-22.929013671875" />
                  <Point X="2.473210205078" Y="-22.925830078125" />
                  <Point X="2.547531005859" Y="-22.905955078125" />
                  <Point X="2.555609375" Y="-22.903404296875" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="2.699422607422" Y="-22.82583203125" />
                  <Point X="3.967325195312" Y="-22.09380859375" />
                  <Point X="4.109111816406" Y="-22.290859375" />
                  <Point X="4.123275878906" Y="-22.310544921875" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.23078515625" Y="-23.331548828125" />
                  <Point X="3.221419677734" Y="-23.339763671875" />
                  <Point X="3.203972900391" Y="-23.358375" />
                  <Point X="3.150484130859" Y="-23.428154296875" />
                  <Point X="3.143202392578" Y="-23.43916015625" />
                  <Point X="3.130440673828" Y="-23.4617109375" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.101705322266" Y="-23.55416015625" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.114095458984" Y="-23.707501953125" />
                  <Point X="3.117707763672" Y="-23.720361328125" />
                  <Point X="3.126218017578" Y="-23.7441015625" />
                  <Point X="3.136282226562" Y="-23.764259765625" />
                  <Point X="3.180769042969" Y="-23.831876953125" />
                  <Point X="3.18433984375" Y="-23.8373046875" />
                  <Point X="3.198894287109" Y="-23.85455078125" />
                  <Point X="3.216138916016" Y="-23.870640625" />
                  <Point X="3.23434765625" Y="-23.88396484375" />
                  <Point X="3.298814941406" Y="-23.92025390625" />
                  <Point X="3.31128515625" Y="-23.926126953125" />
                  <Point X="3.334435546875" Y="-23.9350390625" />
                  <Point X="3.356118896484" Y="-23.9405625" />
                  <Point X="3.443283203125" Y="-23.95208203125" />
                  <Point X="3.451343994141" Y="-23.952798828125" />
                  <Point X="3.471417724609" Y="-23.9537265625" />
                  <Point X="3.488203125" Y="-23.953015625" />
                  <Point X="3.593540039062" Y="-23.9391484375" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.839854492188" Y="-24.042212890625" />
                  <Point X="4.845937011719" Y="-24.0671953125" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.716580810547" Y="-24.67041015625" />
                  <Point X="3.704795166016" Y="-24.674412109375" />
                  <Point X="3.681549072266" Y="-24.6849296875" />
                  <Point X="3.595912841797" Y="-24.734427734375" />
                  <Point X="3.585315429688" Y="-24.741544921875" />
                  <Point X="3.564125732422" Y="-24.75794140625" />
                  <Point X="3.547529296875" Y="-24.774423828125" />
                  <Point X="3.496147705078" Y="-24.839896484375" />
                  <Point X="3.4920234375" Y="-24.84515234375" />
                  <Point X="3.480296875" Y="-24.8644375" />
                  <Point X="3.470524169922" Y="-24.88590234375" />
                  <Point X="3.463680664062" Y="-24.9073984375" />
                  <Point X="3.446553222656" Y="-24.996830078125" />
                  <Point X="3.444989990234" Y="-25.009681640625" />
                  <Point X="3.443615722656" Y="-25.035669921875" />
                  <Point X="3.445178955078" Y="-25.058556640625" />
                  <Point X="3.462306396484" Y="-25.14798828125" />
                  <Point X="3.463681152344" Y="-25.155166015625" />
                  <Point X="3.470526611328" Y="-25.176666015625" />
                  <Point X="3.480298828125" Y="-25.198126953125" />
                  <Point X="3.4920234375" Y="-25.217408203125" />
                  <Point X="3.543405029297" Y="-25.282880859375" />
                  <Point X="3.552393066406" Y="-25.2928046875" />
                  <Point X="3.570831298828" Y="-25.310482421875" />
                  <Point X="3.589036865234" Y="-25.32415625" />
                  <Point X="3.674665771484" Y="-25.373650390625" />
                  <Point X="3.681520996094" Y="-25.37725" />
                  <Point X="3.700482421875" Y="-25.386236328125" />
                  <Point X="3.716580566406" Y="-25.39215234375" />
                  <Point X="3.813179199219" Y="-25.41803515625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.858418457031" Y="-25.926203125" />
                  <Point X="4.855021484375" Y="-25.948734375" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.424381103516" Y="-26.00344140625" />
                  <Point X="3.40803515625" Y="-26.0027109375" />
                  <Point X="3.374660644531" Y="-26.005509765625" />
                  <Point X="3.206587158203" Y="-26.042041015625" />
                  <Point X="3.193096435547" Y="-26.04497265625" />
                  <Point X="3.163981445312" Y="-26.05659375" />
                  <Point X="3.136153564453" Y="-26.073484375" />
                  <Point X="3.112397949219" Y="-26.093958984375" />
                  <Point X="3.010808105469" Y="-26.216140625" />
                  <Point X="3.002653808594" Y="-26.225947265625" />
                  <Point X="2.987933349609" Y="-26.250328125" />
                  <Point X="2.976589355469" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.955197265625" Y="-26.463595703125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956346923828" Y="-26.507564453125" />
                  <Point X="2.964077880859" Y="-26.539185546875" />
                  <Point X="2.976449951172" Y="-26.567998046875" />
                  <Point X="3.069464355469" Y="-26.712673828125" />
                  <Point X="3.074732421875" Y="-26.72006640625" />
                  <Point X="3.093818847656" Y="-26.74430859375" />
                  <Point X="3.110627929688" Y="-26.76091015625" />
                  <Point X="3.200272216797" Y="-26.829697265625" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.134381347656" Y="-27.734298828125" />
                  <Point X="4.124810546875" Y="-27.749787109375" />
                  <Point X="4.028981445312" Y="-27.8859453125" />
                  <Point X="2.800953857422" Y="-27.176943359375" />
                  <Point X="2.786131835938" Y="-27.170013671875" />
                  <Point X="2.754221191406" Y="-27.15982421875" />
                  <Point X="2.554187011719" Y="-27.123697265625" />
                  <Point X="2.538130859375" Y="-27.120798828125" />
                  <Point X="2.506772216797" Y="-27.120396484375" />
                  <Point X="2.474604736328" Y="-27.12535546875" />
                  <Point X="2.444834716797" Y="-27.135177734375" />
                  <Point X="2.278655273438" Y="-27.22263671875" />
                  <Point X="2.265316650391" Y="-27.22965625" />
                  <Point X="2.242388671875" Y="-27.246546875" />
                  <Point X="2.221427978516" Y="-27.267505859375" />
                  <Point X="2.204532226563" Y="-27.290439453125" />
                  <Point X="2.117073242188" Y="-27.456619140625" />
                  <Point X="2.110053222656" Y="-27.46995703125" />
                  <Point X="2.100229980469" Y="-27.49973046875" />
                  <Point X="2.095271728516" Y="-27.53190234375" />
                  <Point X="2.095675537109" Y="-27.563255859375" />
                  <Point X="2.131801513672" Y="-27.763291015625" />
                  <Point X="2.133656982422" Y="-27.771478515625" />
                  <Point X="2.142459472656" Y="-27.803650390625" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.209424560547" Y="-27.92585546875" />
                  <Point X="2.861283935547" Y="-29.05490625" />
                  <Point X="2.793211914063" Y="-29.103529296875" />
                  <Point X="2.781858886719" Y="-29.111638671875" />
                  <Point X="2.701763427734" Y="-29.163482421875" />
                  <Point X="1.758545776367" Y="-27.934255859375" />
                  <Point X="1.747500976562" Y="-27.922177734375" />
                  <Point X="1.721922119141" Y="-27.900556640625" />
                  <Point X="1.524634155273" Y="-27.77371875" />
                  <Point X="1.508798461914" Y="-27.7635390625" />
                  <Point X="1.479985473633" Y="-27.75116796875" />
                  <Point X="1.448366577148" Y="-27.7434375" />
                  <Point X="1.417100219727" Y="-27.741119140625" />
                  <Point X="1.201332641602" Y="-27.76097265625" />
                  <Point X="1.184013305664" Y="-27.76256640625" />
                  <Point X="1.156359130859" Y="-27.7693984375" />
                  <Point X="1.128971435547" Y="-27.780744140625" />
                  <Point X="1.104593139648" Y="-27.795462890625" />
                  <Point X="0.93798260498" Y="-27.933994140625" />
                  <Point X="0.924609436035" Y="-27.945115234375" />
                  <Point X="0.904137145996" Y="-27.968869140625" />
                  <Point X="0.887247436523" Y="-27.996693359375" />
                  <Point X="0.875624267578" Y="-28.025810546875" />
                  <Point X="0.82580859375" Y="-28.255" />
                  <Point X="0.82446496582" Y="-28.262693359375" />
                  <Point X="0.819753540039" Y="-28.298234375" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="0.836067077637" Y="-28.44712109375" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.986423339844" Y="-29.867728515625" />
                  <Point X="0.975711547852" Y="-29.870076171875" />
                  <Point X="0.929315429688" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.043241455078" Y="-29.755583984375" />
                  <Point X="-1.05844152832" Y="-29.7526328125" />
                  <Point X="-1.14124621582" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.179364379883" Y="-29.216009765625" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188125" Y="-29.178470703125" />
                  <Point X="-1.199027832031" Y="-29.149501953125" />
                  <Point X="-1.205667480469" Y="-29.135462890625" />
                  <Point X="-1.221740234375" Y="-29.107625" />
                  <Point X="-1.23057800293" Y="-29.094857421875" />
                  <Point X="-1.250211547852" Y="-29.070935546875" />
                  <Point X="-1.261007446289" Y="-29.05978125" />
                  <Point X="-1.476936157227" Y="-28.870416015625" />
                  <Point X="-1.494268188477" Y="-28.855216796875" />
                  <Point X="-1.506739257812" Y="-28.845966796875" />
                  <Point X="-1.533021728516" Y="-28.829623046875" />
                  <Point X="-1.546833251953" Y="-28.822529296875" />
                  <Point X="-1.576529418945" Y="-28.810228515625" />
                  <Point X="-1.591309448242" Y="-28.805478515625" />
                  <Point X="-1.621451782227" Y="-28.79844921875" />
                  <Point X="-1.636813964844" Y="-28.796169921875" />
                  <Point X="-1.923399902344" Y="-28.77738671875" />
                  <Point X="-1.946403198242" Y="-28.77587890625" />
                  <Point X="-1.961927368164" Y="-28.7761328125" />
                  <Point X="-1.992726196289" Y="-28.779166015625" />
                  <Point X="-2.008000854492" Y="-28.7819453125" />
                  <Point X="-2.039048583984" Y="-28.790263671875" />
                  <Point X="-2.053667480469" Y="-28.795494140625" />
                  <Point X="-2.081861083984" Y="-28.808267578125" />
                  <Point X="-2.095435791016" Y="-28.815810546875" />
                  <Point X="-2.334234619141" Y="-28.97537109375" />
                  <Point X="-2.35340234375" Y="-28.988177734375" />
                  <Point X="-2.359686767578" Y="-28.992759765625" />
                  <Point X="-2.380442138672" Y="-29.0101328125" />
                  <Point X="-2.402759277344" Y="-29.0327734375" />
                  <Point X="-2.410470947266" Y="-29.041630859375" />
                  <Point X="-2.427181396484" Y="-29.063408203125" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.725330566406" Y="-29.02494140625" />
                  <Point X="-2.7475859375" Y="-29.011162109375" />
                  <Point X="-2.980862304688" Y="-28.831546875" />
                  <Point X="-2.341488525391" Y="-27.724119140625" />
                  <Point X="-2.334849365234" Y="-27.71008203125" />
                  <Point X="-2.323947509766" Y="-27.681115234375" />
                  <Point X="-2.319684570312" Y="-27.666185546875" />
                  <Point X="-2.313413574219" Y="-27.63466015625" />
                  <Point X="-2.311638916016" Y="-27.619236328125" />
                  <Point X="-2.310625976562" Y="-27.5883046875" />
                  <Point X="-2.314665771484" Y="-27.557619140625" />
                  <Point X="-2.323649902344" Y="-27.528001953125" />
                  <Point X="-2.329355957031" Y="-27.5135625" />
                  <Point X="-2.343572998047" Y="-27.484732421875" />
                  <Point X="-2.351554931641" Y="-27.4714140625" />
                  <Point X="-2.369585205078" Y="-27.446251953125" />
                  <Point X="-2.379633544922" Y="-27.434408203125" />
                  <Point X="-2.395693115234" Y="-27.418349609375" />
                  <Point X="-2.408821777344" Y="-27.407015625" />
                  <Point X="-2.433977539062" Y="-27.388990234375" />
                  <Point X="-2.447293701172" Y="-27.381009765625" />
                  <Point X="-2.476122558594" Y="-27.36679296875" />
                  <Point X="-2.490563476562" Y="-27.3610859375" />
                  <Point X="-2.520181640625" Y="-27.3521015625" />
                  <Point X="-2.550869384766" Y="-27.3480625" />
                  <Point X="-2.581803466797" Y="-27.349076171875" />
                  <Point X="-2.597227050781" Y="-27.3508515625" />
                  <Point X="-2.628753417969" Y="-27.357123046875" />
                  <Point X="-2.64368359375" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.686684570312" Y="-27.378927734375" />
                  <Point X="-2.782490966797" Y="-27.4342421875" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-3.986434326172" Y="-27.76369140625" />
                  <Point X="-4.004020019531" Y="-27.740587890625" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.048122802734" Y="-26.573884765625" />
                  <Point X="-3.036481445312" Y="-26.563310546875" />
                  <Point X="-3.015101318359" Y="-26.54038671875" />
                  <Point X="-3.005363037109" Y="-26.528037109375" />
                  <Point X="-2.987397460938" Y="-26.50090234375" />
                  <Point X="-2.979831787109" Y="-26.4871171875" />
                  <Point X="-2.967078125" Y="-26.458490234375" />
                  <Point X="-2.961890136719" Y="-26.4436484375" />
                  <Point X="-2.954758056641" Y="-26.416111328125" />
                  <Point X="-2.951551757812" Y="-26.398798828125" />
                  <Point X="-2.948747802734" Y="-26.3683671875" />
                  <Point X="-2.948577636719" Y="-26.353037109375" />
                  <Point X="-2.950787597656" Y="-26.321369140625" />
                  <Point X="-2.953084716797" Y="-26.30621484375" />
                  <Point X="-2.960086425781" Y="-26.276470703125" />
                  <Point X="-2.971781494141" Y="-26.24823828125" />
                  <Point X="-2.987863037109" Y="-26.2222578125" />
                  <Point X="-2.996954101562" Y="-26.209919921875" />
                  <Point X="-3.017785400391" Y="-26.18596484375" />
                  <Point X="-3.028744873047" Y="-26.17524609375" />
                  <Point X="-3.052246337891" Y="-26.1557109375" />
                  <Point X="-3.064788330078" Y="-26.14689453125" />
                  <Point X="-3.089303222656" Y="-26.132466796875" />
                  <Point X="-3.105435302734" Y="-26.124482421875" />
                  <Point X="-3.1346953125" Y="-26.113259765625" />
                  <Point X="-3.149791015625" Y="-26.10886328125" />
                  <Point X="-3.1816796875" Y="-26.102380859375" />
                  <Point X="-3.197295166016" Y="-26.10053515625" />
                  <Point X="-3.228619140625" Y="-26.099443359375" />
                  <Point X="-3.244327636719" Y="-26.100197265625" />
                  <Point X="-3.365274169922" Y="-26.116119140625" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.733883789062" Y="-26.001044921875" />
                  <Point X="-4.740761230469" Y="-25.974119140625" />
                  <Point X="-4.786452148438" Y="-25.65465625" />
                  <Point X="-3.508287109375" Y="-25.312171875" />
                  <Point X="-3.500483154297" Y="-25.30971484375" />
                  <Point X="-3.473924316406" Y="-25.29925" />
                  <Point X="-3.444164794922" Y="-25.283892578125" />
                  <Point X="-3.433564941406" Y="-25.277515625" />
                  <Point X="-3.407874267578" Y="-25.259685546875" />
                  <Point X="-3.393640625" Y="-25.24821875" />
                  <Point X="-3.371198974609" Y="-25.226341796875" />
                  <Point X="-3.360886962891" Y="-25.21447265625" />
                  <Point X="-3.341646484375" Y="-25.18821875" />
                  <Point X="-3.333431396484" Y="-25.174806640625" />
                  <Point X="-3.319326660156" Y="-25.146810546875" />
                  <Point X="-3.313437011719" Y="-25.1322265625" />
                  <Point X="-3.304873291016" Y="-25.104634765625" />
                  <Point X="-3.300989990234" Y="-25.0885" />
                  <Point X="-3.296720458984" Y="-25.06033984375" />
                  <Point X="-3.295646972656" Y="-25.046099609375" />
                  <Point X="-3.295646972656" Y="-25.016462890625" />
                  <Point X="-3.296720214844" Y="-25.00222265625" />
                  <Point X="-3.300989257812" Y="-24.974064453125" />
                  <Point X="-3.304185058594" Y="-24.960146484375" />
                  <Point X="-3.312748779297" Y="-24.932552734375" />
                  <Point X="-3.319326904297" Y="-24.91575" />
                  <Point X="-3.333435302734" Y="-24.887748046875" />
                  <Point X="-3.341652832031" Y="-24.874333984375" />
                  <Point X="-3.3608984375" Y="-24.848076171875" />
                  <Point X="-3.371218017578" Y="-24.836201171875" />
                  <Point X="-3.393674804688" Y="-24.81431640625" />
                  <Point X="-3.405812011719" Y="-24.804306640625" />
                  <Point X="-3.431502685547" Y="-24.7864765625" />
                  <Point X="-3.437516845703" Y="-24.78262890625" />
                  <Point X="-3.46018359375" Y="-24.770125" />
                  <Point X="-3.495080810547" Y="-24.7549921875" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-3.618536621094" Y="-24.720845703125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.73576171875" Y="-24.072412109375" />
                  <Point X="-4.731330566406" Y="-24.042466796875" />
                  <Point X="-4.633586914062" Y="-23.681763671875" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.74705859375" Y="-23.795634765625" />
                  <Point X="-3.736706787109" Y="-23.795296875" />
                  <Point X="-3.715141601563" Y="-23.79341015625" />
                  <Point X="-3.704887939453" Y="-23.7919453125" />
                  <Point X="-3.684601806641" Y="-23.78791015625" />
                  <Point X="-3.674569335938" Y="-23.78533984375" />
                  <Point X="-3.617707519531" Y="-23.767412109375" />
                  <Point X="-3.603445800781" Y="-23.762322265625" />
                  <Point X="-3.584502441406" Y="-23.753986328125" />
                  <Point X="-3.575256591797" Y="-23.74930078125" />
                  <Point X="-3.556513427734" Y="-23.7384765625" />
                  <Point X="-3.547838623047" Y="-23.732814453125" />
                  <Point X="-3.53116015625" Y="-23.72058203125" />
                  <Point X="-3.515915039062" Y="-23.706609375" />
                  <Point X="-3.502279296875" Y="-23.69105859375" />
                  <Point X="-3.495885009766" Y="-23.68291015625" />
                  <Point X="-3.483471923828" Y="-23.6651796875" />
                  <Point X="-3.478004394531" Y="-23.656384765625" />
                  <Point X="-3.468059570312" Y="-23.638255859375" />
                  <Point X="-3.463582275391" Y="-23.628921875" />
                  <Point X="-3.440766113281" Y="-23.57383984375" />
                  <Point X="-3.435498046875" Y="-23.55964453125" />
                  <Point X="-3.429708740234" Y="-23.53978125" />
                  <Point X="-3.427356201172" Y="-23.52969140625" />
                  <Point X="-3.423599121094" Y="-23.508376953125" />
                  <Point X="-3.422359863281" Y="-23.49809375" />
                  <Point X="-3.421008056641" Y="-23.477453125" />
                  <Point X="-3.421911132813" Y="-23.4567890625" />
                  <Point X="-3.425058837891" Y="-23.43634375" />
                  <Point X="-3.427190917969" Y="-23.426205078125" />
                  <Point X="-3.432793945312" Y="-23.405298828125" />
                  <Point X="-3.436017089844" Y="-23.395455078125" />
                  <Point X="-3.443512695312" Y="-23.37618359375" />
                  <Point X="-3.44778515625" Y="-23.366755859375" />
                  <Point X="-3.475315185547" Y="-23.31387109375" />
                  <Point X="-3.482800292969" Y="-23.30071484375" />
                  <Point X="-3.494293212891" Y="-23.283513671875" />
                  <Point X="-3.500510742188" Y="-23.275224609375" />
                  <Point X="-3.514421875" Y="-23.258646484375" />
                  <Point X="-3.521500488281" Y="-23.25108984375" />
                  <Point X="-3.536440917969" Y="-23.236787109375" />
                  <Point X="-3.544302734375" Y="-23.230041015625" />
                  <Point X="-3.607541503906" Y="-23.181515625" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.019505371094" Y="-22.349177734375" />
                  <Point X="-4.002291015625" Y="-22.319685546875" />
                  <Point X="-3.726338134766" Y="-21.964986328125" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244919921875" Y="-22.24228125" />
                  <Point X="-3.225989501953" Y="-22.250611328125" />
                  <Point X="-3.216296142578" Y="-22.254259765625" />
                  <Point X="-3.195657958984" Y="-22.260767578125" />
                  <Point X="-3.185626464844" Y="-22.26333984375" />
                  <Point X="-3.165335693359" Y="-22.267376953125" />
                  <Point X="-3.155076416016" Y="-22.268841796875" />
                  <Point X="-3.075883544922" Y="-22.27576953125" />
                  <Point X="-3.059183837891" Y="-22.276666015625" />
                  <Point X="-3.038518066406" Y="-22.276216796875" />
                  <Point X="-3.028195556641" Y="-22.275427734375" />
                  <Point X="-3.006735351562" Y="-22.27260546875" />
                  <Point X="-2.996552246094" Y="-22.2706953125" />
                  <Point X="-2.976452636719" Y="-22.265779296875" />
                  <Point X="-2.95700390625" Y="-22.258703125" />
                  <Point X="-2.938445556641" Y="-22.24955078125" />
                  <Point X="-2.929419433594" Y="-22.24446875" />
                  <Point X="-2.911163330078" Y="-22.232837890625" />
                  <Point X="-2.902746337891" Y="-22.226806640625" />
                  <Point X="-2.886616455078" Y="-22.213861328125" />
                  <Point X="-2.878903564453" Y="-22.206947265625" />
                  <Point X="-2.822691894531" Y="-22.150736328125" />
                  <Point X="-2.811267333984" Y="-22.138513671875" />
                  <Point X="-2.798321777344" Y="-22.122384765625" />
                  <Point X="-2.792288818359" Y="-22.113966796875" />
                  <Point X="-2.780658935547" Y="-22.095712890625" />
                  <Point X="-2.775577148438" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759352294922" Y="-22.048693359375" />
                  <Point X="-2.754435302734" Y="-22.028599609375" />
                  <Point X="-2.752525634766" Y="-22.018419921875" />
                  <Point X="-2.749700195312" Y="-21.9969609375" />
                  <Point X="-2.748909912109" Y="-21.986634765625" />
                  <Point X="-2.748458496094" Y="-21.965955078125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.755725830078" Y="-21.876408203125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.815546142578" Y="-21.7224296875" />
                  <Point X="-3.059386962891" Y="-21.3000859375" />
                  <Point X="-2.678346191406" Y="-21.0079453125" />
                  <Point X="-2.64837109375" Y="-20.98496484375" />
                  <Point X="-2.192522705078" Y="-20.731705078125" />
                  <Point X="-2.118563232422" Y="-20.82808984375" />
                  <Point X="-2.111825439453" Y="-20.8359453125" />
                  <Point X="-2.097528320313" Y="-20.8508828125" />
                  <Point X="-2.089969482422" Y="-20.857962890625" />
                  <Point X="-2.073392822266" Y="-20.871875" />
                  <Point X="-2.065102294922" Y="-20.878095703125" />
                  <Point X="-2.047898193359" Y="-20.889591796875" />
                  <Point X="-2.038984375" Y="-20.894869140625" />
                  <Point X="-1.950843017578" Y="-20.94075390625" />
                  <Point X="-1.934333496094" Y="-20.948712890625" />
                  <Point X="-1.915057739258" Y="-20.956208984375" />
                  <Point X="-1.905216918945" Y="-20.9594296875" />
                  <Point X="-1.884307739258" Y="-20.965033203125" />
                  <Point X="-1.874173583984" Y="-20.9671640625" />
                  <Point X="-1.853726074219" Y="-20.9703125" />
                  <Point X="-1.833053344727" Y="-20.971216796875" />
                  <Point X="-1.812409301758" Y="-20.96986328125" />
                  <Point X="-1.802124633789" Y="-20.968623046875" />
                  <Point X="-1.780806640625" Y="-20.96486328125" />
                  <Point X="-1.770715209961" Y="-20.962509765625" />
                  <Point X="-1.750859985352" Y="-20.956720703125" />
                  <Point X="-1.741096069336" Y="-20.95328515625" />
                  <Point X="-1.649290893555" Y="-20.9152578125" />
                  <Point X="-1.632582885742" Y="-20.9077265625" />
                  <Point X="-1.614440917969" Y="-20.8977734375" />
                  <Point X="-1.605637939453" Y="-20.892298828125" />
                  <Point X="-1.587909667969" Y="-20.8798828125" />
                  <Point X="-1.579762451172" Y="-20.873486328125" />
                  <Point X="-1.564215698242" Y="-20.859849609375" />
                  <Point X="-1.55024621582" Y="-20.844603515625" />
                  <Point X="-1.538018432617" Y="-20.82792578125" />
                  <Point X="-1.532360717773" Y="-20.819255859375" />
                  <Point X="-1.521539550781" Y="-20.800513671875" />
                  <Point X="-1.516857055664" Y="-20.7912734375" />
                  <Point X="-1.508525878906" Y="-20.77233984375" />
                  <Point X="-1.504877075195" Y="-20.76264453125" />
                  <Point X="-1.47499621582" Y="-20.667875" />
                  <Point X="-1.470026245117" Y="-20.650234375" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464526733398" Y="-20.619693359375" />
                  <Point X="-1.462640625" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462753173828" Y="-20.5671015625" />
                  <Point X="-1.463543334961" Y="-20.5567734375" />
                  <Point X="-1.466729370117" Y="-20.53257421875" />
                  <Point X="-1.479266357422" Y="-20.437345703125" />
                  <Point X="-0.969976501465" Y="-20.29455859375" />
                  <Point X="-0.931167236328" Y="-20.2836796875" />
                  <Point X="-0.365222625732" Y="-20.21744140625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.22043510437" Y="-20.752892578125" />
                  <Point X="-0.207661560059" Y="-20.781083984375" />
                  <Point X="-0.200119094849" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600669861" Y="-20.88956640625" />
                  <Point X="-0.085354125977" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.00931760788" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227790833" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912307739" Y="-20.88956640625" />
                  <Point X="0.139208236694" Y="-20.8749765625" />
                  <Point X="0.163763122559" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.1945730896" Y="-20.8213828125" />
                  <Point X="0.21243132019" Y="-20.79465625" />
                  <Point X="0.219973648071" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978118896" Y="-20.7382734375" />
                  <Point X="0.252336868286" Y="-20.684685546875" />
                  <Point X="0.378190765381" Y="-20.2149921875" />
                  <Point X="0.79398425293" Y="-20.258537109375" />
                  <Point X="0.827849487305" Y="-20.262083984375" />
                  <Point X="1.417960083008" Y="-20.4045546875" />
                  <Point X="1.45362109375" Y="-20.413166015625" />
                  <Point X="1.836604858398" Y="-20.552076171875" />
                  <Point X="1.858232299805" Y="-20.559919921875" />
                  <Point X="2.229655273438" Y="-20.733623046875" />
                  <Point X="2.25044921875" Y="-20.74334765625" />
                  <Point X="2.609285644531" Y="-20.95240625" />
                  <Point X="2.629449462891" Y="-20.96415234375" />
                  <Point X="2.817780029297" Y="-21.09808203125" />
                  <Point X="2.065308349609" Y="-22.40140234375" />
                  <Point X="2.062370849609" Y="-22.4068984375" />
                  <Point X="2.053180664062" Y="-22.426564453125" />
                  <Point X="2.044181640625" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.021427124023" Y="-22.533724609375" />
                  <Point X="2.019900878906" Y="-22.540380859375" />
                  <Point X="2.016285644531" Y="-22.560548828125" />
                  <Point X="2.013352539063" Y="-22.585416015625" />
                  <Point X="2.012698608398" Y="-22.59666796875" />
                  <Point X="2.012727905273" Y="-22.619169921875" />
                  <Point X="2.013410888672" Y="-22.630419921875" />
                  <Point X="2.021160644531" Y="-22.694685546875" />
                  <Point X="2.02380078125" Y="-22.71096484375" />
                  <Point X="2.02914440918" Y="-22.732890625" />
                  <Point X="2.032469848633" Y="-22.7436953125" />
                  <Point X="2.040738891602" Y="-22.7657890625" />
                  <Point X="2.045322753906" Y="-22.776119140625" />
                  <Point X="2.055684326172" Y="-22.79616015625" />
                  <Point X="2.061461425781" Y="-22.80587109375" />
                  <Point X="2.101227294922" Y="-22.8644765625" />
                  <Point X="2.105308105469" Y="-22.870044921875" />
                  <Point X="2.118350341797" Y="-22.886140625" />
                  <Point X="2.13483984375" Y="-22.904140625" />
                  <Point X="2.142774902344" Y="-22.911849609375" />
                  <Point X="2.159484375" Y="-22.9262890625" />
                  <Point X="2.168258789062" Y="-22.93301953125" />
                  <Point X="2.226837402344" Y="-22.972767578125" />
                  <Point X="2.241235107422" Y="-22.981728515625" />
                  <Point X="2.261295410156" Y="-22.992103515625" />
                  <Point X="2.271634521484" Y="-22.99669140625" />
                  <Point X="2.293732666016" Y="-23.004962890625" />
                  <Point X="2.304538330078" Y="-23.0082890625" />
                  <Point X="2.326469970703" Y="-23.013634765625" />
                  <Point X="2.337595947266" Y="-23.015654296875" />
                  <Point X="2.401862304688" Y="-23.02340234375" />
                  <Point X="2.408936523438" Y="-23.02398828125" />
                  <Point X="2.430246582031" Y="-23.0246875" />
                  <Point X="2.454309570312" Y="-23.02397265625" />
                  <Point X="2.465264892578" Y="-23.023009765625" />
                  <Point X="2.486986572266" Y="-23.019826171875" />
                  <Point X="2.497752929688" Y="-23.01760546875" />
                  <Point X="2.572073730469" Y="-22.99773046875" />
                  <Point X="2.591766113281" Y="-22.99125390625" />
                  <Point X="2.624690673828" Y="-22.977703125" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="2.746922607422" Y="-22.908103515625" />
                  <Point X="3.940402832031" Y="-22.219046875" />
                  <Point X="4.031999267578" Y="-22.346345703125" />
                  <Point X="4.043958496094" Y="-22.362966796875" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.172952880859" Y="-23.2561796875" />
                  <Point X="3.168140625" Y="-23.26012890625" />
                  <Point X="3.152111083984" Y="-23.274791015625" />
                  <Point X="3.134664306641" Y="-23.29340234375" />
                  <Point X="3.128575683594" Y="-23.300580078125" />
                  <Point X="3.075086914062" Y="-23.370359375" />
                  <Point X="3.0605234375" Y="-23.39237109375" />
                  <Point X="3.04776171875" Y="-23.414921875" />
                  <Point X="3.042713378906" Y="-23.425255859375" />
                  <Point X="3.033902587891" Y="-23.446458984375" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.010215576172" Y="-23.52857421875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.021055419922" Y="-23.72669921875" />
                  <Point X="3.022635498047" Y="-23.733193359375" />
                  <Point X="3.028280029297" Y="-23.75241796875" />
                  <Point X="3.036790283203" Y="-23.776158203125" />
                  <Point X="3.041222412109" Y="-23.786537109375" />
                  <Point X="3.051286621094" Y="-23.8066953125" />
                  <Point X="3.056918701172" Y="-23.816474609375" />
                  <Point X="3.101405517578" Y="-23.884091796875" />
                  <Point X="3.111738525391" Y="-23.89857421875" />
                  <Point X="3.12629296875" Y="-23.9158203125" />
                  <Point X="3.134085205078" Y="-23.92401171875" />
                  <Point X="3.151329833984" Y="-23.9401015625" />
                  <Point X="3.160038330078" Y="-23.947306640625" />
                  <Point X="3.178247070312" Y="-23.960630859375" />
                  <Point X="3.187747314453" Y="-23.96675" />
                  <Point X="3.252214599609" Y="-24.0030390625" />
                  <Point X="3.258337646484" Y="-24.00619921875" />
                  <Point X="3.277155029297" Y="-24.01478515625" />
                  <Point X="3.300305419922" Y="-24.023697265625" />
                  <Point X="3.310984863281" Y="-24.027099609375" />
                  <Point X="3.332668212891" Y="-24.032623046875" />
                  <Point X="3.343672119141" Y="-24.034744140625" />
                  <Point X="3.430836425781" Y="-24.046263671875" />
                  <Point X="3.446958007812" Y="-24.047697265625" />
                  <Point X="3.467031738281" Y="-24.048625" />
                  <Point X="3.475437744141" Y="-24.048640625" />
                  <Point X="3.500602539062" Y="-24.047203125" />
                  <Point X="3.605939453125" Y="-24.0333359375" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.747550292969" Y="-24.06468359375" />
                  <Point X="4.752684570312" Y="-24.085771484375" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="3.691992919922" Y="-24.578646484375" />
                  <Point X="3.686035400391" Y="-24.580455078125" />
                  <Point X="3.665634521484" Y="-24.587859375" />
                  <Point X="3.642388427734" Y="-24.598376953125" />
                  <Point X="3.634008789062" Y="-24.6026796875" />
                  <Point X="3.548372558594" Y="-24.652177734375" />
                  <Point X="3.527177734375" Y="-24.666412109375" />
                  <Point X="3.505988037109" Y="-24.68280859375" />
                  <Point X="3.497182617188" Y="-24.69053515625" />
                  <Point X="3.480586181641" Y="-24.707017578125" />
                  <Point X="3.472795166016" Y="-24.7157734375" />
                  <Point X="3.421413574219" Y="-24.78124609375" />
                  <Point X="3.410851806641" Y="-24.795794921875" />
                  <Point X="3.399125244141" Y="-24.815080078125" />
                  <Point X="3.393836181641" Y="-24.825072265625" />
                  <Point X="3.384063476562" Y="-24.846537109375" />
                  <Point X="3.380000976562" Y="-24.857083984375" />
                  <Point X="3.373157470703" Y="-24.878580078125" />
                  <Point X="3.370376464844" Y="-24.889529296875" />
                  <Point X="3.353249023438" Y="-24.9789609375" />
                  <Point X="3.350122558594" Y="-25.0046640625" />
                  <Point X="3.348748291016" Y="-25.03065234375" />
                  <Point X="3.348836669922" Y="-25.04214453125" />
                  <Point X="3.350399902344" Y="-25.06503125" />
                  <Point X="3.351874755859" Y="-25.07642578125" />
                  <Point X="3.369002197266" Y="-25.165857421875" />
                  <Point X="3.373158691406" Y="-25.18398828125" />
                  <Point X="3.380004150391" Y="-25.20548828125" />
                  <Point X="3.384067871094" Y="-25.21603515625" />
                  <Point X="3.393840087891" Y="-25.23749609375" />
                  <Point X="3.399127929688" Y="-25.247486328125" />
                  <Point X="3.410852539062" Y="-25.266767578125" />
                  <Point X="3.417289306641" Y="-25.27605859375" />
                  <Point X="3.468670898438" Y="-25.34153125" />
                  <Point X="3.4729921875" Y="-25.346654296875" />
                  <Point X="3.486646972656" Y="-25.36137890625" />
                  <Point X="3.505085205078" Y="-25.379056640625" />
                  <Point X="3.513778808594" Y="-25.386443359375" />
                  <Point X="3.531984375" Y="-25.4001171875" />
                  <Point X="3.541496337891" Y="-25.406404296875" />
                  <Point X="3.627125244141" Y="-25.4558984375" />
                  <Point X="3.640835693359" Y="-25.46309765625" />
                  <Point X="3.659797119141" Y="-25.472083984375" />
                  <Point X="3.667712890625" Y="-25.47540625" />
                  <Point X="3.691993408203" Y="-25.483916015625" />
                  <Point X="3.788592041016" Y="-25.509798828125" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.764479980469" Y="-25.912041015625" />
                  <Point X="4.76161328125" Y="-25.931056640625" />
                  <Point X="4.727801757812" Y="-26.07921875" />
                  <Point X="3.436781005859" Y="-25.90925390625" />
                  <Point X="3.428622314453" Y="-25.90853515625" />
                  <Point X="3.400096191406" Y="-25.90804296875" />
                  <Point X="3.366721679688" Y="-25.910841796875" />
                  <Point X="3.354483154297" Y="-25.912677734375" />
                  <Point X="3.186409667969" Y="-25.949208984375" />
                  <Point X="3.157879394531" Y="-25.9567421875" />
                  <Point X="3.128764404297" Y="-25.96836328125" />
                  <Point X="3.114688964844" Y="-25.9753828125" />
                  <Point X="3.086861083984" Y="-25.9922734375" />
                  <Point X="3.074131835938" Y="-26.0015234375" />
                  <Point X="3.050376220703" Y="-26.021998046875" />
                  <Point X="3.039349853516" Y="-26.03322265625" />
                  <Point X="2.937760009766" Y="-26.155404296875" />
                  <Point X="2.921327636719" Y="-26.176845703125" />
                  <Point X="2.906607177734" Y="-26.2012265625" />
                  <Point X="2.900164794922" Y="-26.21397265625" />
                  <Point X="2.888820800781" Y="-26.241359375" />
                  <Point X="2.884362792969" Y="-26.254927734375" />
                  <Point X="2.877531005859" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.860596923828" Y="-26.454890625" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861606933594" Y="-26.51458984375" />
                  <Point X="2.864064941406" Y="-26.530126953125" />
                  <Point X="2.871795898438" Y="-26.561748046875" />
                  <Point X="2.876785400391" Y="-26.57666796875" />
                  <Point X="2.889157470703" Y="-26.60548046875" />
                  <Point X="2.896540039062" Y="-26.619373046875" />
                  <Point X="2.989554443359" Y="-26.764048828125" />
                  <Point X="3.000090576172" Y="-26.778833984375" />
                  <Point X="3.019177001953" Y="-26.803076171875" />
                  <Point X="3.027062255859" Y="-26.811900390625" />
                  <Point X="3.043871337891" Y="-26.828501953125" />
                  <Point X="3.052795166016" Y="-26.836279296875" />
                  <Point X="3.142439453125" Y="-26.90506640625" />
                  <Point X="4.087170166016" Y="-27.629984375" />
                  <Point X="4.053567871094" Y="-27.684357421875" />
                  <Point X="4.0454921875" Y="-27.69742578125" />
                  <Point X="4.001274169922" Y="-27.760251953125" />
                  <Point X="2.848453857422" Y="-27.094671875" />
                  <Point X="2.841188720703" Y="-27.090884765625" />
                  <Point X="2.815029052734" Y="-27.079515625" />
                  <Point X="2.783118408203" Y="-27.069326171875" />
                  <Point X="2.77110546875" Y="-27.0663359375" />
                  <Point X="2.571071289062" Y="-27.030208984375" />
                  <Point X="2.539349609375" Y="-27.025806640625" />
                  <Point X="2.507990966797" Y="-27.025404296875" />
                  <Point X="2.492297851562" Y="-27.026505859375" />
                  <Point X="2.460130371094" Y="-27.03146484375" />
                  <Point X="2.444838867188" Y="-27.035138671875" />
                  <Point X="2.415068847656" Y="-27.0449609375" />
                  <Point X="2.400590332031" Y="-27.051109375" />
                  <Point X="2.234410888672" Y="-27.138568359375" />
                  <Point X="2.208970703125" Y="-27.153169921875" />
                  <Point X="2.186042724609" Y="-27.170060546875" />
                  <Point X="2.175216308594" Y="-27.179369140625" />
                  <Point X="2.154255615234" Y="-27.200328125" />
                  <Point X="2.144943359375" Y="-27.211158203125" />
                  <Point X="2.128047607422" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.033005126953" Y="-27.412375" />
                  <Point X="2.019836669922" Y="-27.44019140625" />
                  <Point X="2.010013427734" Y="-27.46996484375" />
                  <Point X="2.006338378906" Y="-27.485259765625" />
                  <Point X="2.001380249023" Y="-27.517431640625" />
                  <Point X="2.000279663086" Y="-27.533125" />
                  <Point X="2.00068347168" Y="-27.564478515625" />
                  <Point X="2.002187866211" Y="-27.580138671875" />
                  <Point X="2.038313842773" Y="-27.780173828125" />
                  <Point X="2.042024902344" Y="-27.796548828125" />
                  <Point X="2.050827392578" Y="-27.828720703125" />
                  <Point X="2.054786376953" Y="-27.840234375" />
                  <Point X="2.064145996094" Y="-27.8626640625" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.127152099609" Y="-27.97335546875" />
                  <Point X="2.735894042969" Y="-29.027724609375" />
                  <Point X="2.723752441406" Y="-29.036083984375" />
                  <Point X="1.833914428711" Y="-27.876423828125" />
                  <Point X="1.828652954102" Y="-27.870146484375" />
                  <Point X="1.80882824707" Y="-27.849625" />
                  <Point X="1.783249389648" Y="-27.82800390625" />
                  <Point X="1.773296875" Y="-27.820646484375" />
                  <Point X="1.576008789062" Y="-27.69380859375" />
                  <Point X="1.546278808594" Y="-27.67624609375" />
                  <Point X="1.517465820312" Y="-27.663875" />
                  <Point X="1.502547363281" Y="-27.65888671875" />
                  <Point X="1.470928466797" Y="-27.65115625" />
                  <Point X="1.455391357422" Y="-27.648697265625" />
                  <Point X="1.424125" Y="-27.64637890625" />
                  <Point X="1.408395751953" Y="-27.64651953125" />
                  <Point X="1.192628173828" Y="-27.666373046875" />
                  <Point X="1.161228393555" Y="-27.67033984375" />
                  <Point X="1.13357421875" Y="-27.677171875" />
                  <Point X="1.120000488281" Y="-27.681630859375" />
                  <Point X="1.092612915039" Y="-27.6929765625" />
                  <Point X="1.079869384766" Y="-27.69941796875" />
                  <Point X="1.055490966797" Y="-27.71413671875" />
                  <Point X="1.043856079102" Y="-27.7224140625" />
                  <Point X="0.877245483398" Y="-27.8609453125" />
                  <Point X="0.852647644043" Y="-27.883095703125" />
                  <Point X="0.832175476074" Y="-27.906849609375" />
                  <Point X="0.82292767334" Y="-27.91957421875" />
                  <Point X="0.806038024902" Y="-27.9473984375" />
                  <Point X="0.79901739502" Y="-27.96147265625" />
                  <Point X="0.787394165039" Y="-27.99058984375" />
                  <Point X="0.782791748047" Y="-28.0056328125" />
                  <Point X="0.732976196289" Y="-28.234822265625" />
                  <Point X="0.730288818359" Y="-28.250208984375" />
                  <Point X="0.725577453613" Y="-28.28575" />
                  <Point X="0.724753479004" Y="-28.29819140625" />
                  <Point X="0.724742370605" Y="-28.323076171875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.741879760742" Y="-28.459521484375" />
                  <Point X="0.833090759277" Y="-29.1523359375" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652606872559" Y="-28.480125" />
                  <Point X="0.642145324707" Y="-28.453580078125" />
                  <Point X="0.626787841797" Y="-28.42381640625" />
                  <Point X="0.620408203125" Y="-28.413208984375" />
                  <Point X="0.468846099854" Y="-28.194837890625" />
                  <Point X="0.456680419922" Y="-28.17730859375" />
                  <Point X="0.446672454834" Y="-28.165173828125" />
                  <Point X="0.424791442871" Y="-28.142720703125" />
                  <Point X="0.412918273926" Y="-28.13240234375" />
                  <Point X="0.386664489746" Y="-28.113158203125" />
                  <Point X="0.373246368408" Y="-28.104939453125" />
                  <Point X="0.345242095947" Y="-28.090830078125" />
                  <Point X="0.330655944824" Y="-28.084939453125" />
                  <Point X="0.096122314453" Y="-28.012150390625" />
                  <Point X="0.063382621765" Y="-28.003111328125" />
                  <Point X="0.035230026245" Y="-27.998841796875" />
                  <Point X="0.020991802216" Y="-27.997767578125" />
                  <Point X="-0.00865014267" Y="-27.997765625" />
                  <Point X="-0.022894458771" Y="-27.998837890625" />
                  <Point X="-0.051060134888" Y="-28.003107421875" />
                  <Point X="-0.064981788635" Y="-28.0063046875" />
                  <Point X="-0.299515563965" Y="-28.079095703125" />
                  <Point X="-0.332927490234" Y="-28.090828125" />
                  <Point X="-0.360926849365" Y="-28.104935546875" />
                  <Point X="-0.374339630127" Y="-28.11315234375" />
                  <Point X="-0.400596374512" Y="-28.132396484375" />
                  <Point X="-0.412472686768" Y="-28.14271484375" />
                  <Point X="-0.434358306885" Y="-28.165171875" />
                  <Point X="-0.444367614746" Y="-28.177310546875" />
                  <Point X="-0.59592980957" Y="-28.39568359375" />
                  <Point X="-0.608095214844" Y="-28.4132109375" />
                  <Point X="-0.612468139648" Y="-28.42012890625" />
                  <Point X="-0.625974365234" Y="-28.445259765625" />
                  <Point X="-0.63877722168" Y="-28.476212890625" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.672517578125" Y="-28.59901953125" />
                  <Point X="-0.985425415039" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.076856676534" Y="-20.322201917506" />
                  <Point X="0.352850245889" Y="-20.309564338251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.343796662935" Y="-20.297404321226" />
                  <Point X="-0.942827293776" Y="-20.28694820267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.484401166181" Y="-20.424330104199" />
                  <Point X="0.327509726396" Y="-20.404136489002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.31821802162" Y="-20.392865269235" />
                  <Point X="-1.261864676157" Y="-20.37639385561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.759605996165" Y="-20.524148293539" />
                  <Point X="0.302169206904" Y="-20.498708639753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.292639380304" Y="-20.488326217244" />
                  <Point X="-1.475272366391" Y="-20.467683281685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.993644211621" Y="-20.623247916949" />
                  <Point X="0.276828687412" Y="-20.593280790504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.267060738989" Y="-20.583787165253" />
                  <Point X="-1.463073800541" Y="-20.562910679608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.204687004374" Y="-20.721946153764" />
                  <Point X="0.251488172383" Y="-20.687852941334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.241482097674" Y="-20.679248113262" />
                  <Point X="-1.472148375166" Y="-20.657766753483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.382117337705" Y="-20.820057682916" />
                  <Point X="0.21929484545" Y="-20.782305475886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.210507364663" Y="-20.774803250402" />
                  <Point X="-1.501605054508" Y="-20.752267056397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.185910276924" Y="-20.740322464309" />
                  <Point X="-2.207359461212" Y="-20.739948067405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550241007779" Y="-20.918006763658" />
                  <Point X="0.137495312937" Y="-20.875892130899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.131829793452" Y="-20.87119104368" />
                  <Point X="-1.551895665695" Y="-20.846403701678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.111158881744" Y="-20.83664172593" />
                  <Point X="-2.373168941917" Y="-20.832068323317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.701890662468" Y="-21.015668289391" />
                  <Point X="-1.705954906324" Y="-20.938729058794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.963363497737" Y="-20.934235975118" />
                  <Point X="-2.538978422622" Y="-20.92418857923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.809421341634" Y="-21.112559715542" />
                  <Point X="-2.689600550809" Y="-21.016573931367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.755112070111" Y="-21.206626214845" />
                  <Point X="-2.810769865106" Y="-21.109473384283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.700802798588" Y="-21.300692714149" />
                  <Point X="-2.931939179403" Y="-21.202372837199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646493527064" Y="-21.394759213452" />
                  <Point X="-3.0531084937" Y="-21.295272290115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.592184255541" Y="-21.488825712756" />
                  <Point X="-3.006843128242" Y="-21.391094326237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.537874984018" Y="-21.582892212059" />
                  <Point X="-2.951427917317" Y="-21.487076073506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483565712495" Y="-21.676958711363" />
                  <Point X="-2.896012706392" Y="-21.583057820775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.429256440972" Y="-21.771025210666" />
                  <Point X="-2.840597495467" Y="-21.679039568043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.374947169449" Y="-21.86509170997" />
                  <Point X="-2.785470596117" Y="-21.775016282815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.320637897925" Y="-21.959158209273" />
                  <Point X="-2.75643965138" Y="-21.870537491005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266328626402" Y="-22.053224708577" />
                  <Point X="-2.74846713526" Y="-21.965691122955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212019354879" Y="-22.14729120788" />
                  <Point X="-2.763627959216" Y="-22.060440960953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.585864252741" Y="-22.046088773063" />
                  <Point X="-3.786707694198" Y="-22.042583037752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979335213551" Y="-22.273154292106" />
                  <Point X="3.850578803421" Y="-22.270906840608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.157710083356" Y="-22.241357707184" />
                  <Point X="-2.826316807832" Y="-22.154361194194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.416163390227" Y="-22.144065383801" />
                  <Point X="-3.859637737544" Y="-22.136324510274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.047829863539" Y="-22.369364341833" />
                  <Point X="3.690838469518" Y="-22.363133033872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103400811833" Y="-22.335424206487" />
                  <Point X="-2.934772234165" Y="-22.24748256885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.245353798161" Y="-22.242061347485" />
                  <Point X="-3.932567780891" Y="-22.230065982797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.10594008068" Y="-22.465393130611" />
                  <Point X="3.531098135614" Y="-22.455359227136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.052058175641" Y="-22.429542488603" />
                  <Point X="-4.004705018238" Y="-22.323821293799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.080284551945" Y="-22.559959782855" />
                  <Point X="3.371357801711" Y="-22.5475854204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02400960183" Y="-22.52406737009" />
                  <Point X="-4.059604626916" Y="-22.417877488729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.95921336539" Y="-22.652860948597" />
                  <Point X="3.211617467807" Y="-22.639811613664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012727534199" Y="-22.618884912032" />
                  <Point X="-4.114504146138" Y="-22.511933685221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.838142178835" Y="-22.745762114339" />
                  <Point X="3.051877133904" Y="-22.732037806929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.024566333523" Y="-22.714106030207" />
                  <Point X="-4.16940366536" Y="-22.605989881713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.717070992281" Y="-22.838663280081" />
                  <Point X="2.8921368" Y="-22.824264000193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.064134908232" Y="-22.809811173411" />
                  <Point X="-4.224303184583" Y="-22.700046078204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.595999805726" Y="-22.931564445823" />
                  <Point X="2.732396429158" Y="-22.916490192812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136851446919" Y="-22.90609491648" />
                  <Point X="-4.10854815478" Y="-22.797081060929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.474928619171" Y="-23.024465611565" />
                  <Point X="2.533545669167" Y="-23.00803371105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.290607751092" Y="-23.003793213916" />
                  <Point X="-3.981840096679" Y="-22.894307229475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353857432616" Y="-23.117366777306" />
                  <Point X="-3.855132038579" Y="-22.99153339802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.232786246062" Y="-23.210267943048" />
                  <Point X="-3.728423980478" Y="-23.088759566565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.126394668769" Y="-23.303425342323" />
                  <Point X="-3.60171601424" Y="-23.185985733507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.057766990902" Y="-23.397241912914" />
                  <Point X="-3.494778418442" Y="-23.282866807349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020553814005" Y="-23.491606825659" />
                  <Point X="-3.442497442767" Y="-23.378793846338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00145332956" Y="-23.586287896628" />
                  <Point X="-3.421151060969" Y="-23.474180919983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.01172552635" Y="-23.681481669654" />
                  <Point X="-3.438927344816" Y="-23.568885104958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.037123909981" Y="-23.776939471253" />
                  <Point X="-3.482206484745" Y="-23.663144135925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.707730762855" Y="-23.901114493504" />
                  <Point X="4.621663721105" Y="-23.899612187702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.094073775513" Y="-23.872948006018" />
                  <Point X="-3.589719259227" Y="-23.75628196463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.140600058211" Y="-23.746666304516" />
                  <Point X="-4.648770645683" Y="-23.737796153917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.730959949136" Y="-23.996534431623" />
                  <Point X="3.984447396982" Y="-23.983504006555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.19296786023" Y="-23.969688679852" />
                  <Point X="-4.674396541046" Y="-23.832363323414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.753645740507" Y="-24.091944884748" />
                  <Point X="-4.700022436409" Y="-23.92693049291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.768479371941" Y="-24.187218277912" />
                  <Point X="-4.725648331772" Y="-24.021497662407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783313003375" Y="-24.282491671076" />
                  <Point X="-4.742244519614" Y="-24.116222446035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.463486956997" Y="-24.371923557835" />
                  <Point X="-4.756267991667" Y="-24.210992136584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.130577669546" Y="-24.461127075772" />
                  <Point X="-4.77029146372" Y="-24.305761827133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.797668382096" Y="-24.550330593708" />
                  <Point X="-4.784314935773" Y="-24.400531517682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.567154962013" Y="-24.641321438158" />
                  <Point X="-4.436724771291" Y="-24.501613197735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.458151334352" Y="-24.734433243924" />
                  <Point X="-4.057415947675" Y="-24.603248529044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392366883503" Y="-24.828299443227" />
                  <Point X="-3.678107124059" Y="-24.704883860352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.36400105021" Y="-24.922818786929" />
                  <Point X="-3.405385112372" Y="-24.804658711937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349439608478" Y="-25.017579087183" />
                  <Point X="-3.326735301787" Y="-24.901046020652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358832804198" Y="-25.112757517188" />
                  <Point X="-3.297577293478" Y="-24.996569446744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.381033373732" Y="-25.208159500735" />
                  <Point X="-3.301714842674" Y="-25.091511696719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.439367986215" Y="-25.304192206348" />
                  <Point X="-3.340198312183" Y="-25.185854436424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.533081655766" Y="-25.400842455698" />
                  <Point X="-3.436349089004" Y="-25.279190589536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.750737938584" Y="-25.499656131411" />
                  <Point X="-3.721238810511" Y="-25.369232292114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.130048070685" Y="-25.601291485559" />
                  <Point X="-4.054149711865" Y="-25.458435781879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.509357180062" Y="-25.702926821855" />
                  <Point X="-4.387060613219" Y="-25.547639271645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.780967546508" Y="-25.802682269601" />
                  <Point X="-4.719971514573" Y="-25.636842761411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766680206453" Y="-25.897447354317" />
                  <Point X="-4.775549290926" Y="-25.73088711888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.747675933372" Y="-25.99213010466" />
                  <Point X="3.962142695428" Y="-25.978418570989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.139545453469" Y="-25.964060082721" />
                  <Point X="-4.761925913388" Y="-25.826139386984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019595751814" Y="-26.056980824054" />
                  <Point X="-4.748302535851" Y="-25.921391655087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.941724643534" Y="-26.150636049967" />
                  <Point X="-4.729877891776" Y="-26.01672772961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887720619881" Y="-26.244707877392" />
                  <Point X="-3.075449086431" Y="-26.140620362991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.495665847809" Y="-26.133285452137" />
                  <Point X="-4.705499423383" Y="-26.112167728523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.871221162874" Y="-26.339434349463" />
                  <Point X="-2.978535755644" Y="-26.237326462636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.132889429349" Y="-26.217177144312" />
                  <Point X="-4.68112095499" Y="-26.207607727436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.862491991134" Y="-26.434296452368" />
                  <Point X="-2.949987151715" Y="-26.332839251536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.863939841718" Y="-26.529336195858" />
                  <Point X="-2.957764164862" Y="-26.42771797443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.900146548602" Y="-26.624982657442" />
                  <Point X="-3.001347208763" Y="-26.521971700733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.961926191923" Y="-26.721075496291" />
                  <Point X="-3.102003926965" Y="-26.615229202346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.032552397212" Y="-26.817322752454" />
                  <Point X="-3.223075095576" Y="-26.708130368401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.154693406162" Y="-26.91446920286" />
                  <Point X="-3.344146264187" Y="-26.801031534456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.281400898811" Y="-27.011695361535" />
                  <Point X="-3.465217432798" Y="-26.893932700511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.408108391461" Y="-27.108921520211" />
                  <Point X="2.856456866136" Y="-27.099292407018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326611413769" Y="-27.090043920245" />
                  <Point X="-3.586288601409" Y="-26.986833866566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.534815884111" Y="-27.206147678886" />
                  <Point X="3.026157594913" Y="-27.197269015422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.172221696194" Y="-27.182363508865" />
                  <Point X="-3.70735977002" Y="-27.079735032622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.66152337676" Y="-27.303373837561" />
                  <Point X="3.19585832369" Y="-27.295245623825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.104673462149" Y="-27.276198921218" />
                  <Point X="-3.828430938631" Y="-27.172636198677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.78823086941" Y="-27.400599996236" />
                  <Point X="3.365559052467" Y="-27.393222232228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.055123328034" Y="-27.370348491574" />
                  <Point X="-3.949502107242" Y="-27.265537364732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.914938362059" Y="-27.497826154912" />
                  <Point X="3.535259781244" Y="-27.491198840632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.011781347168" Y="-27.464606425648" />
                  <Point X="-2.437388748561" Y="-27.386945872751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.692848981636" Y="-27.382486797796" />
                  <Point X="-4.070573275853" Y="-27.358438530787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.041645854709" Y="-27.595052313587" />
                  <Point X="3.704960510021" Y="-27.589175449035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.00061839986" Y="-27.559426046842" />
                  <Point X="-2.344260123275" Y="-27.483585910116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.85258839062" Y="-27.474713007204" />
                  <Point X="-4.176355708326" Y="-27.451606562724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.049950156409" Y="-27.690211736877" />
                  <Point X="3.874661238798" Y="-27.687152057438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015654038831" Y="-27.654702966061" />
                  <Point X="-2.311829035866" Y="-27.579166468016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.012328635877" Y="-27.566939202016" />
                  <Point X="-4.119097250225" Y="-27.547620483992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032867738961" Y="-27.750017903479" />
                  <Point X="1.653128715821" Y="-27.743389534174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.031675248961" Y="-27.73254202356" />
                  <Point X="-2.321917250029" Y="-27.674004848747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.172068881133" Y="-27.659165396828" />
                  <Point X="-4.061838792124" Y="-27.64363440526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056964032225" Y="-27.845452977006" />
                  <Point X="1.798556352211" Y="-27.840942454174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.919751611113" Y="-27.825602860359" />
                  <Point X="-2.366957755039" Y="-27.768233134972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.331809126389" Y="-27.751391591639" />
                  <Point X="-4.004580334023" Y="-27.739648326528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.108685385146" Y="-27.941370247744" />
                  <Point X="1.880695909504" Y="-27.937390676644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.823391968026" Y="-27.918935367696" />
                  <Point X="-2.421267079885" Y="-27.862299633345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.491549371646" Y="-27.843617786451" />
                  <Point X="-3.931442693151" Y="-27.835939419962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.164100610733" Y="-28.037351995269" />
                  <Point X="1.9545927899" Y="-28.033695022653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.781144283615" Y="-28.013212402786" />
                  <Point X="0.045294162756" Y="-28.000368091149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.024905585832" Y="-27.999142749979" />
                  <Point X="-2.475576404731" Y="-27.956366131718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.651289616902" Y="-27.935843981262" />
                  <Point X="-3.858148571915" Y="-27.932233244772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.219515847564" Y="-28.13333374299" />
                  <Point X="2.028489670297" Y="-28.129999368662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.760570437005" Y="-28.107867756122" />
                  <Point X="0.365366709234" Y="-28.100969449394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.327372831177" Y="-28.088877635737" />
                  <Point X="-2.529885729577" Y="-28.05043263009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.274931084395" Y="-28.229315490711" />
                  <Point X="2.102386550693" Y="-28.226303714672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.739996590395" Y="-28.202523109457" />
                  <Point X="0.470920272921" Y="-28.197826364865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.44748048756" Y="-28.181795619961" />
                  <Point X="-2.584195054424" Y="-28.144499128463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330346321226" Y="-28.325297238432" />
                  <Point X="2.17628343109" Y="-28.322608060681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724814331852" Y="-28.297272573313" />
                  <Point X="0.537674498045" Y="-28.294006035363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.512636105367" Y="-28.275672795586" />
                  <Point X="-2.63850437927" Y="-28.238565626836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.385761558056" Y="-28.421278986153" />
                  <Point X="2.250180311486" Y="-28.418912406691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733047313974" Y="-28.392430751715" />
                  <Point X="0.604428723168" Y="-28.390185705861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.577791723174" Y="-28.369549971211" />
                  <Point X="-2.692813704116" Y="-28.332632125209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.441176794887" Y="-28.517260733874" />
                  <Point X="2.324077191883" Y="-28.5152167527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.745584811834" Y="-28.487664065718" />
                  <Point X="0.654479030915" Y="-28.486073808397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.633556571327" Y="-28.46359106333" />
                  <Point X="-2.747123028962" Y="-28.426698623582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.496592031718" Y="-28.613242481595" />
                  <Point X="2.397974072279" Y="-28.61152109871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758122549762" Y="-28.582897383912" />
                  <Point X="0.680145221489" Y="-28.581536284584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.661557885444" Y="-28.558116769738" />
                  <Point X="-2.801432353808" Y="-28.520765121954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.552007268549" Y="-28.709224229316" />
                  <Point X="2.471870952676" Y="-28.707825444719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77066028769" Y="-28.678130702106" />
                  <Point X="0.705723944557" Y="-28.67699723402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.68689825861" Y="-28.652688923043" />
                  <Point X="-2.855741678654" Y="-28.614831620327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.607422505379" Y="-28.805205977037" />
                  <Point X="2.545767833073" Y="-28.804129790729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.783198025619" Y="-28.773364020299" />
                  <Point X="0.731302667625" Y="-28.772458183456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.712238808033" Y="-28.747261073272" />
                  <Point X="-2.910051003501" Y="-28.7088981187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.66283774221" Y="-28.901187724758" />
                  <Point X="2.619664713469" Y="-28.900434136738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795735763547" Y="-28.868597338493" />
                  <Point X="0.756881390693" Y="-28.867919132892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.737579357456" Y="-28.841833223501" />
                  <Point X="-1.536396316178" Y="-28.82788982162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.098821273031" Y="-28.818072657481" />
                  <Point X="-2.964360328347" Y="-28.802964617073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718252979041" Y="-28.997169472479" />
                  <Point X="2.693561593866" Y="-28.996738482748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808273501475" Y="-28.963830656687" />
                  <Point X="0.782460113761" Y="-28.963380082328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.762919906878" Y="-28.936405373729" />
                  <Point X="-1.414662062869" Y="-28.92502917208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.23740009932" Y="-28.910668226235" />
                  <Point X="-2.89296453431" Y="-28.899225306457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.820811239404" Y="-29.059063974881" />
                  <Point X="0.808038836829" Y="-29.058841031764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.788260456301" Y="-29.030977523958" />
                  <Point X="-1.30411909933" Y="-29.02197317785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.372312267818" Y="-29.003327796738" />
                  <Point X="-2.766701552288" Y="-28.996443706171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.813601005724" Y="-29.125549674187" />
                  <Point X="-1.215440822837" Y="-29.118535534088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.452907759287" Y="-29.096935468366" />
                  <Point X="-2.613585913603" Y="-29.09413082075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.838941555147" Y="-29.220121824415" />
                  <Point X="-1.179729649055" Y="-29.214173346109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.864282104569" Y="-29.314693974644" />
                  <Point X="-1.160764290357" Y="-29.309518858841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.889622653992" Y="-29.409266124873" />
                  <Point X="-1.141798914721" Y="-29.404864371868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.914963203415" Y="-29.503838275101" />
                  <Point X="-1.122993916988" Y="-29.500207085489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.940303752838" Y="-29.59841042533" />
                  <Point X="-1.123326541303" Y="-29.595215750674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.96564430226" Y="-29.692982575559" />
                  <Point X="-1.135806851791" Y="-29.690012377208" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998375" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.842355957031" Y="-29.92101953125" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464319580078" Y="-28.521544921875" />
                  <Point X="0.312757446289" Y="-28.303173828125" />
                  <Point X="0.300591949463" Y="-28.28564453125" />
                  <Point X="0.274338195801" Y="-28.266400390625" />
                  <Point X="0.039804515839" Y="-28.193611328125" />
                  <Point X="0.020979211807" Y="-28.187767578125" />
                  <Point X="-0.00866262722" Y="-28.187765625" />
                  <Point X="-0.243196456909" Y="-28.260556640625" />
                  <Point X="-0.262021759033" Y="-28.2663984375" />
                  <Point X="-0.288278564453" Y="-28.285642578125" />
                  <Point X="-0.439840698242" Y="-28.504015625" />
                  <Point X="-0.452006195068" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.488991607666" Y="-28.6481953125" />
                  <Point X="-0.84774395752" Y="-29.987076171875" />
                  <Point X="-1.079444580078" Y="-29.942103515625" />
                  <Point X="-1.100255615234" Y="-29.9380625" />
                  <Point X="-1.35158984375" Y="-29.8733984375" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.365713378906" Y="-29.253076171875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386283569336" Y="-29.20262890625" />
                  <Point X="-1.602212280273" Y="-29.013263671875" />
                  <Point X="-1.619544311523" Y="-28.998064453125" />
                  <Point X="-1.649240356445" Y="-28.985763671875" />
                  <Point X="-1.935826171875" Y="-28.96698046875" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989877197266" Y="-28.973791015625" />
                  <Point X="-2.228676025391" Y="-29.1333515625" />
                  <Point X="-2.24784375" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.276444580078" Y="-29.179072265625" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.825352783203" Y="-29.186484375" />
                  <Point X="-2.855833251953" Y="-29.167611328125" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-3.195155029297" Y="-28.822712890625" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.59759375" />
                  <Point X="-2.513979736328" Y="-27.568763671875" />
                  <Point X="-2.530039306641" Y="-27.552705078125" />
                  <Point X="-2.531328369141" Y="-27.551416015625" />
                  <Point X="-2.560157226562" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.687489990234" Y="-27.59878515625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.13762109375" Y="-27.87876953125" />
                  <Point X="-4.161703613281" Y="-27.84712890625" />
                  <Point X="-4.43101953125" Y="-27.39552734375" />
                  <Point X="-4.369525390625" Y="-27.348341796875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145821289062" Y="-26.39601171875" />
                  <Point X="-3.138689208984" Y="-26.368474609375" />
                  <Point X="-3.138116699219" Y="-26.366263671875" />
                  <Point X="-3.140326660156" Y="-26.334595703125" />
                  <Point X="-3.161157958984" Y="-26.310640625" />
                  <Point X="-3.185672851562" Y="-26.296212890625" />
                  <Point X="-3.187640625" Y="-26.2950546875" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.340475830078" Y="-26.304494140625" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.917973632812" Y="-26.048068359375" />
                  <Point X="-4.927392578125" Y="-26.011193359375" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-4.930981445312" Y="-25.4966796875" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541896484375" Y="-25.12142578125" />
                  <Point X="-3.516205810547" Y="-25.103595703125" />
                  <Point X="-3.514138427734" Y="-25.10216015625" />
                  <Point X="-3.494897949219" Y="-25.07590625" />
                  <Point X="-3.486334228516" Y="-25.048314453125" />
                  <Point X="-3.485646972656" Y="-25.046099609375" />
                  <Point X="-3.485646972656" Y="-25.016462890625" />
                  <Point X="-3.494210693359" Y="-24.988869140625" />
                  <Point X="-3.494897949219" Y="-24.986654296875" />
                  <Point X="-3.514143554688" Y="-24.960396484375" />
                  <Point X="-3.539834228516" Y="-24.94256640625" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.667712402344" Y="-24.904373046875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.92371484375" Y="-24.044599609375" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.773696777344" Y="-23.4723671875" />
                  <Point X="-4.773516113281" Y="-23.471701171875" />
                  <Point X="-4.737837402344" Y="-23.4763984375" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731701171875" Y="-23.6041328125" />
                  <Point X="-3.674839355469" Y="-23.586205078125" />
                  <Point X="-3.670275146484" Y="-23.584765625" />
                  <Point X="-3.651531982422" Y="-23.57394140625" />
                  <Point X="-3.639118896484" Y="-23.5562109375" />
                  <Point X="-3.616302734375" Y="-23.50112890625" />
                  <Point X="-3.614471435547" Y="-23.49670703125" />
                  <Point X="-3.610714355469" Y="-23.475392578125" />
                  <Point X="-3.616317382812" Y="-23.454486328125" />
                  <Point X="-3.643847412109" Y="-23.4016015625" />
                  <Point X="-3.646057128906" Y="-23.397357421875" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.72320703125" Y="-23.33225390625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.183598144531" Y="-22.253400390625" />
                  <Point X="-4.160010253906" Y="-22.21298828125" />
                  <Point X="-3.778697998047" Y="-21.722865234375" />
                  <Point X="-3.763853271484" Y="-21.723935546875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138518798828" Y="-22.079564453125" />
                  <Point X="-3.059325927734" Y="-22.0864921875" />
                  <Point X="-3.052969482422" Y="-22.087048828125" />
                  <Point X="-3.031509277344" Y="-22.0842265625" />
                  <Point X="-3.013253173828" Y="-22.072595703125" />
                  <Point X="-2.957041503906" Y="-22.016384765625" />
                  <Point X="-2.952529541016" Y="-22.011873046875" />
                  <Point X="-2.940899658203" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.945002685547" Y="-21.892966796875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-2.980090576172" Y="-21.8174296875" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.793950683594" Y="-20.85716015625" />
                  <Point X="-2.752873779297" Y="-20.82566796875" />
                  <Point X="-2.152314208984" Y="-20.492009765625" />
                  <Point X="-2.141548583984" Y="-20.486029296875" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.95124987793" Y="-20.726337890625" />
                  <Point X="-1.863108642578" Y="-20.77222265625" />
                  <Point X="-1.856033813477" Y="-20.77590625" />
                  <Point X="-1.835124633789" Y="-20.781509765625" />
                  <Point X="-1.813806640625" Y="-20.77775" />
                  <Point X="-1.722001586914" Y="-20.73972265625" />
                  <Point X="-1.714632568359" Y="-20.736669921875" />
                  <Point X="-1.696904296875" Y="-20.72425390625" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.656202514648" Y="-20.6107421875" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.655103637695" Y="-20.557375" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.021268249512" Y="-20.11161328125" />
                  <Point X="-0.96808380127" Y="-20.096703125" />
                  <Point X="-0.240028778076" Y="-20.011494140625" />
                  <Point X="-0.221026260376" Y="-20.021486328125" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282115936" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594036102" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.068810966492" Y="-20.635509765625" />
                  <Point X="0.236648391724" Y="-20.0091328125" />
                  <Point X="0.813773925781" Y="-20.069572265625" />
                  <Point X="0.860208862305" Y="-20.074435546875" />
                  <Point X="1.46255065918" Y="-20.219859375" />
                  <Point X="1.508457763672" Y="-20.230943359375" />
                  <Point X="1.901389526367" Y="-20.373462890625" />
                  <Point X="1.931029052734" Y="-20.384212890625" />
                  <Point X="2.310145019531" Y="-20.561513671875" />
                  <Point X="2.338703125" Y="-20.574869140625" />
                  <Point X="2.704931640625" Y="-20.788234375" />
                  <Point X="2.732529785156" Y="-20.8043125" />
                  <Point X="3.068740722656" Y="-21.04340625" />
                  <Point X="3.027459716797" Y="-21.114908203125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224851806641" Y="-22.508486328125" />
                  <Point X="2.204977539062" Y="-22.582806640625" />
                  <Point X="2.202044433594" Y="-22.607673828125" />
                  <Point X="2.209793945312" Y="-22.671939453125" />
                  <Point X="2.210415771484" Y="-22.67709765625" />
                  <Point X="2.218684570312" Y="-22.69919140625" />
                  <Point X="2.258450439453" Y="-22.757796875" />
                  <Point X="2.274939941406" Y="-22.775796875" />
                  <Point X="2.333536621094" Y="-22.815556640625" />
                  <Point X="2.338239990234" Y="-22.81875" />
                  <Point X="2.360338134766" Y="-22.827021484375" />
                  <Point X="2.424604492188" Y="-22.83476953125" />
                  <Point X="2.448667480469" Y="-22.8340546875" />
                  <Point X="2.52298828125" Y="-22.8141796875" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.651922607422" Y="-22.743560546875" />
                  <Point X="3.994247314453" Y="-21.9685703125" />
                  <Point X="4.186224121094" Y="-22.235373046875" />
                  <Point X="4.202593261719" Y="-22.258123046875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.338739257812" Y="-22.60112890625" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279370117188" Y="-23.416169921875" />
                  <Point X="3.225881347656" Y="-23.48594921875" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.193195068359" Y="-23.57974609375" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.207135498047" Y="-23.6883046875" />
                  <Point X="3.215645751953" Y="-23.712044921875" />
                  <Point X="3.260132568359" Y="-23.779662109375" />
                  <Point X="3.263703369141" Y="-23.78508984375" />
                  <Point X="3.280947998047" Y="-23.8011796875" />
                  <Point X="3.345415283203" Y="-23.83746875" />
                  <Point X="3.368565673828" Y="-23.846380859375" />
                  <Point X="3.455729980469" Y="-23.857900390625" />
                  <Point X="3.455735595703" Y="-23.85790234375" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.581140625" Y="-23.8449609375" />
                  <Point X="4.848975097656" Y="-23.678046875" />
                  <Point X="4.932158691406" Y="-24.0197421875" />
                  <Point X="4.939189453125" Y="-24.048619140625" />
                  <Point X="4.997858886719" Y="-24.425443359375" />
                  <Point X="4.944631347656" Y="-24.439705078125" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729089355469" Y="-24.7671796875" />
                  <Point X="3.643453125" Y="-24.816677734375" />
                  <Point X="3.622263427734" Y="-24.83307421875" />
                  <Point X="3.570881835938" Y="-24.898546875" />
                  <Point X="3.566757568359" Y="-24.903802734375" />
                  <Point X="3.556984863281" Y="-24.925267578125" />
                  <Point X="3.539857421875" Y="-25.01469921875" />
                  <Point X="3.538483154297" Y="-25.0406875" />
                  <Point X="3.555610595703" Y="-25.130119140625" />
                  <Point X="3.556985351562" Y="-25.137296875" />
                  <Point X="3.566757568359" Y="-25.1587578125" />
                  <Point X="3.618139160156" Y="-25.22423046875" />
                  <Point X="3.636577392578" Y="-25.241908203125" />
                  <Point X="3.722206298828" Y="-25.29140234375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="3.837766357422" Y="-25.326271484375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.952356933594" Y="-25.940365234375" />
                  <Point X="4.948430664062" Y="-25.96640625" />
                  <Point X="4.874545898438" Y="-26.290177734375" />
                  <Point X="4.807661621094" Y="-26.281373046875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394838134766" Y="-26.098341796875" />
                  <Point X="3.226764648438" Y="-26.134873046875" />
                  <Point X="3.213273925781" Y="-26.1378046875" />
                  <Point X="3.185446044922" Y="-26.1546953125" />
                  <Point X="3.083856201172" Y="-26.276876953125" />
                  <Point X="3.075701904297" Y="-26.28668359375" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.049797607422" Y="-26.47230078125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056359863281" Y="-26.516623046875" />
                  <Point X="3.149374267578" Y="-26.661298828125" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.258104980469" Y="-26.754328125" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.215194824219" Y="-27.784240234375" />
                  <Point X="4.204126953125" Y="-27.802150390625" />
                  <Point X="4.056688720703" Y="-28.011638671875" />
                  <Point X="3.996495361328" Y="-27.97688671875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737336914062" Y="-27.2533125" />
                  <Point X="2.537302734375" Y="-27.217185546875" />
                  <Point X="2.521246582031" Y="-27.214287109375" />
                  <Point X="2.489079101562" Y="-27.21924609375" />
                  <Point X="2.322899658203" Y="-27.306705078125" />
                  <Point X="2.309561035156" Y="-27.313724609375" />
                  <Point X="2.288600341797" Y="-27.33468359375" />
                  <Point X="2.201141357422" Y="-27.50086328125" />
                  <Point X="2.194121337891" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546373046875" />
                  <Point X="2.2252890625" Y="-27.746408203125" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.291697021484" Y="-27.87835546875" />
                  <Point X="2.986673828125" Y="-29.082087890625" />
                  <Point X="2.8484296875" Y="-29.180833984375" />
                  <Point X="2.835298828125" Y="-29.190212890625" />
                  <Point X="2.679774902344" Y="-29.29087890625" />
                  <Point X="2.631732421875" Y="-29.22826953125" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670547119141" Y="-27.980466796875" />
                  <Point X="1.473259277344" Y="-27.85362890625" />
                  <Point X="1.457423583984" Y="-27.84344921875" />
                  <Point X="1.4258046875" Y="-27.83571875" />
                  <Point X="1.210036987305" Y="-27.855572265625" />
                  <Point X="1.192717773438" Y="-27.857166015625" />
                  <Point X="1.165330078125" Y="-27.86851171875" />
                  <Point X="0.998719787598" Y="-28.00704296875" />
                  <Point X="0.985346496582" Y="-28.0181640625" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.918641052246" Y="-28.275177734375" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.930254394531" Y="-28.434720703125" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.006764038086" Y="-29.960525390625" />
                  <Point X="0.994356079102" Y="-29.963244140625" />
                  <Point X="0.860200622559" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#210" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.17888588467" Y="5.022735717989" Z="2.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.35" />
                  <Point X="-0.251997501656" Y="5.069446858853" Z="2.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.35" />
                  <Point X="-1.040921814484" Y="4.967811911153" Z="2.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.35" />
                  <Point X="-1.710831794447" Y="4.467379908761" Z="2.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.35" />
                  <Point X="-1.710044342814" Y="4.435573668988" Z="2.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.35" />
                  <Point X="-1.747294078111" Y="4.337751762799" Z="2.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.35" />
                  <Point X="-1.846173908459" Y="4.303407543494" Z="2.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.35" />
                  <Point X="-2.119431006362" Y="4.590539045953" Z="2.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.35" />
                  <Point X="-2.182753310146" Y="4.582978036368" Z="2.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.35" />
                  <Point X="-2.830528703279" Y="4.213799789576" Z="2.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.35" />
                  <Point X="-3.029547711361" Y="3.188850209572" Z="2.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.35" />
                  <Point X="-3.000968552032" Y="3.133956324779" Z="2.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.35" />
                  <Point X="-2.998551962028" Y="3.050251605858" Z="2.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.35" />
                  <Point X="-3.061120140828" Y="2.994596147141" Z="2.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.35" />
                  <Point X="-3.745009138725" Y="3.350646237385" Z="2.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.35" />
                  <Point X="-3.824317522902" Y="3.339117366544" Z="2.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.35" />
                  <Point X="-4.232936940108" Y="2.803085765564" Z="2.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.35" />
                  <Point X="-3.759801084296" Y="1.659358949662" Z="2.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.35" />
                  <Point X="-3.694352525582" Y="1.606589202827" Z="2.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.35" />
                  <Point X="-3.668653818099" Y="1.549283027099" Z="2.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.35" />
                  <Point X="-3.696034064265" Y="1.492761038834" Z="2.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.35" />
                  <Point X="-4.737467093253" Y="1.604453808156" Z="2.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.35" />
                  <Point X="-4.828112066969" Y="1.571990917284" Z="2.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.35" />
                  <Point X="-4.979332020417" Y="0.993999727868" Z="2.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.35" />
                  <Point X="-3.686809225746" Y="0.078610026401" Z="2.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.35" />
                  <Point X="-3.57449860997" Y="0.047637799053" Z="2.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.35" />
                  <Point X="-3.548120472366" Y="0.027592201131" Z="2.35" />
                  <Point X="-3.539556741714" Y="0" Z="2.35" />
                  <Point X="-3.540244128769" Y="-0.00221474993" Z="2.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.35" />
                  <Point X="-3.550869985154" Y="-0.03123818408" Z="2.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.35" />
                  <Point X="-4.950078563027" Y="-0.417102030355" Z="2.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.35" />
                  <Point X="-5.054556275591" Y="-0.48699169214" Z="2.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.35" />
                  <Point X="-4.972574449274" Y="-1.029162117895" Z="2.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.35" />
                  <Point X="-3.340105403614" Y="-1.322786215772" Z="2.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.35" />
                  <Point X="-3.217191148972" Y="-1.308021433436" Z="2.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.35" />
                  <Point X="-3.193248774011" Y="-1.324660125993" Z="2.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.35" />
                  <Point X="-4.406119295095" Y="-2.277392817281" Z="2.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.35" />
                  <Point X="-4.481089232743" Y="-2.388230055878" Z="2.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.35" />
                  <Point X="-4.183563070699" Y="-2.877772214814" Z="2.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.35" />
                  <Point X="-2.668645916043" Y="-2.610804830791" Z="2.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.35" />
                  <Point X="-2.571550448496" Y="-2.556780020635" Z="2.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.35" />
                  <Point X="-3.244612182378" Y="-3.766432466449" Z="2.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.35" />
                  <Point X="-3.269502584184" Y="-3.885663933607" Z="2.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.35" />
                  <Point X="-2.857829852465" Y="-4.19771553676" Z="2.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.35" />
                  <Point X="-2.242932715858" Y="-4.178229639661" Z="2.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.35" />
                  <Point X="-2.207054573177" Y="-4.143644710087" Z="2.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.35" />
                  <Point X="-1.945252552258" Y="-3.985592094797" Z="2.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.35" />
                  <Point X="-1.641334835058" Y="-4.01957572371" Z="2.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.35" />
                  <Point X="-1.420908600279" Y="-4.231550335332" Z="2.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.35" />
                  <Point X="-1.409516115033" Y="-4.852288164892" Z="2.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.35" />
                  <Point X="-1.391127820433" Y="-4.885156256632" Z="2.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.35" />
                  <Point X="-1.095130680207" Y="-4.959906482645" Z="2.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.35" />
                  <Point X="-0.446851226788" Y="-3.629855514263" Z="2.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.35" />
                  <Point X="-0.404921298506" Y="-3.501244939329" Z="2.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.35" />
                  <Point X="-0.234533705304" Y="-3.277029975463" Z="2.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.35" />
                  <Point X="0.018825374057" Y="-3.210082069825" Z="2.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.35" />
                  <Point X="0.265524560927" Y="-3.300400816439" Z="2.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.35" />
                  <Point X="0.787903633613" Y="-4.902680515553" Z="2.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.35" />
                  <Point X="0.831068062241" Y="-5.011328663234" Z="2.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.35" />
                  <Point X="1.011313601206" Y="-4.978085270126" Z="2.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.35" />
                  <Point X="0.973670716939" Y="-3.396913858298" Z="2.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.35" />
                  <Point X="0.96134434992" Y="-3.254517153334" Z="2.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.35" />
                  <Point X="1.024533371954" Y="-3.014206506676" Z="2.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.35" />
                  <Point X="1.208462824234" Y="-2.874081722975" Z="2.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.35" />
                  <Point X="1.440066196293" Y="-2.86440750682" Z="2.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.35" />
                  <Point X="2.585908784678" Y="-4.227425814576" Z="2.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.35" />
                  <Point X="2.676552642333" Y="-4.317261183741" Z="2.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.35" />
                  <Point X="2.871334631609" Y="-4.190240521488" Z="2.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.35" />
                  <Point X="2.328842312604" Y="-2.822074087832" Z="2.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.35" />
                  <Point X="2.268337158424" Y="-2.706242424786" Z="2.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.35" />
                  <Point X="2.239231205178" Y="-2.492869432224" Z="2.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.35" />
                  <Point X="2.340028946148" Y="-2.31967010479" Z="2.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.35" />
                  <Point X="2.522264378726" Y="-2.235110851067" Z="2.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.35" />
                  <Point X="3.965339509275" Y="-2.988907187671" Z="2.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.35" />
                  <Point X="4.078088843168" Y="-3.028078524101" Z="2.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.35" />
                  <Point X="4.251572583624" Y="-2.779244091748" Z="2.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.35" />
                  <Point X="3.282387010763" Y="-1.683379441626" Z="2.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.35" />
                  <Point X="3.185276821845" Y="-1.602980152475" Z="2.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.35" />
                  <Point X="3.093431216829" Y="-1.445601843084" Z="2.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.35" />
                  <Point X="3.116145785658" Y="-1.2775651056" Z="2.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.35" />
                  <Point X="3.231226328077" Y="-1.152451901929" Z="2.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.35" />
                  <Point X="4.79497939161" Y="-1.299665106759" Z="2.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.35" />
                  <Point X="4.913280276789" Y="-1.286922294118" Z="2.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.35" />
                  <Point X="4.995642217184" Y="-0.916539676377" Z="2.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.35" />
                  <Point X="3.844551413938" Y="-0.24669418317" Z="2.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.35" />
                  <Point X="3.741078917693" Y="-0.216837467691" Z="2.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.35" />
                  <Point X="3.65131844551" Y="-0.162082919244" Z="2.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.35" />
                  <Point X="3.598561967316" Y="-0.089432065802" Z="2.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.35" />
                  <Point X="3.582809483109" Y="0.007178465376" Z="2.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.35" />
                  <Point X="3.60406099289" Y="0.101865819331" Z="2.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.35" />
                  <Point X="3.662316496659" Y="0.171311376551" Z="2.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.35" />
                  <Point X="4.951416403369" Y="0.543277749599" Z="2.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.35" />
                  <Point X="5.043118375074" Y="0.60061227408" Z="2.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.35" />
                  <Point X="4.974583939535" Y="1.023367223599" Z="2.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.35" />
                  <Point X="3.568458298563" Y="1.235891951323" Z="2.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.35" />
                  <Point X="3.456124971839" Y="1.222948748224" Z="2.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.35" />
                  <Point X="3.363785995021" Y="1.237381444382" Z="2.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.35" />
                  <Point X="3.2957477249" Y="1.279098531259" Z="2.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.35" />
                  <Point X="3.249948136548" Y="1.353079189599" Z="2.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.35" />
                  <Point X="3.235191330549" Y="1.438067908525" Z="2.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.35" />
                  <Point X="3.259409382524" Y="1.514914776844" Z="2.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.35" />
                  <Point X="4.363021381676" Y="2.390482962493" Z="2.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.35" />
                  <Point X="4.431773019248" Y="2.48083946111" Z="2.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.35" />
                  <Point X="4.220654170397" Y="2.825109846546" Z="2.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.35" />
                  <Point X="2.620766674689" Y="2.331020416483" Z="2.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.35" />
                  <Point X="2.50391239895" Y="2.265403509767" Z="2.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.35" />
                  <Point X="2.42443317037" Y="2.246151192594" Z="2.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.35" />
                  <Point X="2.355462751998" Y="2.2570924082" Z="2.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.35" />
                  <Point X="2.293666192035" Y="2.301562108384" Z="2.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.35" />
                  <Point X="2.253278510165" Y="2.365325273462" Z="2.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.35" />
                  <Point X="2.247124432891" Y="2.435557176932" Z="2.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.35" />
                  <Point X="3.064604936376" Y="3.891372407266" Z="2.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.35" />
                  <Point X="3.100753342881" Y="4.02208307171" Z="2.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.35" />
                  <Point X="2.72394487246" Y="4.286249628217" Z="2.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.35" />
                  <Point X="2.325169779958" Y="4.515060697623" Z="2.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.35" />
                  <Point X="1.912281979878" Y="4.704822136772" Z="2.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.35" />
                  <Point X="1.46813003542" Y="4.860024372681" Z="2.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.35" />
                  <Point X="0.812826526386" Y="5.011436253047" Z="2.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.35" />
                  <Point X="0.014358676883" Y="4.408711787962" Z="2.35" />
                  <Point X="0" Y="4.355124473572" Z="2.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>