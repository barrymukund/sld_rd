<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#121" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="404" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475585938" Y="-24.995284179688" />
                  <Width Value="9.783895507812" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.579273193359" Y="-28.57212890625" />
                  <Point X="0.563302001953" Y="-28.5125234375" />
                  <Point X="0.554761169434" Y="-28.491294921875" />
                  <Point X="0.541013244629" Y="-28.466322265625" />
                  <Point X="0.535835510254" Y="-28.45797265625" />
                  <Point X="0.378635742188" Y="-28.2314765625" />
                  <Point X="0.357108215332" Y="-28.20751953125" />
                  <Point X="0.326243469238" Y="-28.1863984375" />
                  <Point X="0.301404327393" Y="-28.175837890625" />
                  <Point X="0.292393768311" Y="-28.172533203125" />
                  <Point X="0.049135974884" Y="-28.09703515625" />
                  <Point X="0.02039081955" Y="-28.091591796875" />
                  <Point X="-0.013893167496" Y="-28.09266015625" />
                  <Point X="-0.038815330505" Y="-28.098041015625" />
                  <Point X="-0.046925643921" Y="-28.100169921875" />
                  <Point X="-0.290183258057" Y="-28.17566796875" />
                  <Point X="-0.319509185791" Y="-28.188986328125" />
                  <Point X="-0.349137908936" Y="-28.2122890625" />
                  <Point X="-0.367183349609" Y="-28.233513671875" />
                  <Point X="-0.372851196289" Y="-28.240880859375" />
                  <Point X="-0.530051147461" Y="-28.467375" />
                  <Point X="-0.538189208984" Y="-28.481572265625" />
                  <Point X="-0.55099017334" Y="-28.5125234375" />
                  <Point X="-0.916584838867" Y="-29.87694140625" />
                  <Point X="-1.079319580078" Y="-29.845353515625" />
                  <Point X="-1.086903442383" Y="-29.84340234375" />
                  <Point X="-1.246417724609" Y="-29.802361328125" />
                  <Point X="-1.216885742188" Y="-29.57804296875" />
                  <Point X="-1.214963134766" Y="-29.56344140625" />
                  <Point X="-1.214656982422" Y="-29.541244140625" />
                  <Point X="-1.217603027344" Y="-29.512830078125" />
                  <Point X="-1.21892175293" Y="-29.50409375" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287037231445" Y="-29.1817578125" />
                  <Point X="-1.306946899414" Y="-29.149923828125" />
                  <Point X="-1.326581176758" Y="-29.12917578125" />
                  <Point X="-1.332945068359" Y="-29.123048828125" />
                  <Point X="-1.556905639648" Y="-28.926640625" />
                  <Point X="-1.583204223633" Y="-28.908791015625" />
                  <Point X="-1.618391357422" Y="-28.895650390625" />
                  <Point X="-1.646598754883" Y="-28.89114453125" />
                  <Point X="-1.655370605469" Y="-28.890158203125" />
                  <Point X="-1.952616699219" Y="-28.87067578125" />
                  <Point X="-1.984347045898" Y="-28.872525390625" />
                  <Point X="-2.020250976562" Y="-28.883515625" />
                  <Point X="-2.045373657227" Y="-28.897109375" />
                  <Point X="-2.052943115234" Y="-28.901671875" />
                  <Point X="-2.300624267578" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.480147949219" Y="-29.28848828125" />
                  <Point X="-2.801719482422" Y="-29.089380859375" />
                  <Point X="-2.8121796875" Y="-29.081326171875" />
                  <Point X="-3.104721435547" Y="-28.856080078125" />
                  <Point X="-2.45344140625" Y="-27.728029296875" />
                  <Point X="-2.423760742188" Y="-27.676619140625" />
                  <Point X="-2.412857421875" Y="-27.647646484375" />
                  <Point X="-2.406587646484" Y="-27.616115234375" />
                  <Point X="-2.405637695312" Y="-27.58473046875" />
                  <Point X="-2.414969970703" Y="-27.55475" />
                  <Point X="-2.429884277344" Y="-27.525228515625" />
                  <Point X="-2.447504638672" Y="-27.500888671875" />
                  <Point X="-2.464156005859" Y="-27.48423828125" />
                  <Point X="-2.489312011719" Y="-27.466212890625" />
                  <Point X="-2.518141113281" Y="-27.45199609375" />
                  <Point X="-2.547759277344" Y="-27.44301171875" />
                  <Point X="-2.578693603516" Y="-27.444025390625" />
                  <Point X="-2.610219482422" Y="-27.450296875" />
                  <Point X="-2.639184082031" Y="-27.46119921875" />
                  <Point X="-3.8180234375" Y="-28.141802734375" />
                  <Point X="-4.082861083984" Y="-27.793859375" />
                  <Point X="-4.090367431641" Y="-27.7812734375" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.157886474609" Y="-26.53836328125" />
                  <Point X="-3.105954833984" Y="-26.498515625" />
                  <Point X="-3.083961914062" Y="-26.47465234375" />
                  <Point X="-3.065689697266" Y="-26.446333984375" />
                  <Point X="-3.053549560547" Y="-26.4186484375" />
                  <Point X="-3.046152099609" Y="-26.390087890625" />
                  <Point X="-3.043348144531" Y="-26.35965234375" />
                  <Point X="-3.045559326172" Y="-26.327974609375" />
                  <Point X="-3.052778808594" Y="-26.2977109375" />
                  <Point X="-3.069388427734" Y="-26.271404296875" />
                  <Point X="-3.091274902344" Y="-26.24683203125" />
                  <Point X="-3.114029296875" Y="-26.22814453125" />
                  <Point X="-3.139456054688" Y="-26.2131796875" />
                  <Point X="-3.168721191406" Y="-26.201955078125" />
                  <Point X="-3.200607666016" Y="-26.195474609375" />
                  <Point X="-3.231928222656" Y="-26.194384765625" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.834076660156" Y="-25.992658203125" />
                  <Point X="-4.8360625" Y="-25.978771484375" />
                  <Point X="-4.892423339844" Y="-25.584701171875" />
                  <Point X="-3.592033691406" Y="-25.23626171875" />
                  <Point X="-3.532875732422" Y="-25.22041015625" />
                  <Point X="-3.516418457031" Y="-25.214322265625" />
                  <Point X="-3.486622070312" Y="-25.198703125" />
                  <Point X="-3.459975585938" Y="-25.180208984375" />
                  <Point X="-3.4368515625" Y="-25.157400390625" />
                  <Point X="-3.417239013672" Y="-25.12995703125" />
                  <Point X="-3.403800048828" Y="-25.102880859375" />
                  <Point X="-3.39491796875" Y="-25.074263671875" />
                  <Point X="-3.390655029297" Y="-25.04496875" />
                  <Point X="-3.3910234375" Y="-25.01413671875" />
                  <Point X="-3.395285888672" Y="-24.987111328125" />
                  <Point X="-3.404168212891" Y="-24.9584921875" />
                  <Point X="-3.419010742188" Y="-24.92950390625" />
                  <Point X="-3.439361083984" Y="-24.90248046875" />
                  <Point X="-3.46108203125" Y="-24.881583984375" />
                  <Point X="-3.487728515625" Y="-24.86308984375" />
                  <Point X="-3.501923095703" Y="-24.854953125" />
                  <Point X="-3.532875732422" Y="-24.842150390625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.820488769531" Y="-24.008267578125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-3.808071533203" Y="-23.694625" />
                  <Point X="-3.765665527344" Y="-23.70020703125" />
                  <Point X="-3.744996337891" Y="-23.700658203125" />
                  <Point X="-3.723447509766" Y="-23.698775390625" />
                  <Point X="-3.703167480469" Y="-23.69474609375" />
                  <Point X="-3.700688232422" Y="-23.69396484375" />
                  <Point X="-3.641711181641" Y="-23.675369140625" />
                  <Point X="-3.622774658203" Y="-23.66703515625" />
                  <Point X="-3.604028564453" Y="-23.6562109375" />
                  <Point X="-3.587347167969" Y="-23.643978515625" />
                  <Point X="-3.573708740234" Y="-23.62842578125" />
                  <Point X="-3.561294433594" Y="-23.610693359375" />
                  <Point X="-3.551352050781" Y="-23.5925703125" />
                  <Point X="-3.550371337891" Y="-23.590203125" />
                  <Point X="-3.526706542969" Y="-23.533072265625" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.532054199219" Y="-23.41061328125" />
                  <Point X="-3.533239990234" Y="-23.4083359375" />
                  <Point X="-3.561793945312" Y="-23.353484375" />
                  <Point X="-3.573282958984" Y="-23.33629296875" />
                  <Point X="-3.587196044922" Y="-23.319712890625" />
                  <Point X="-3.602136230469" Y="-23.30541015625" />
                  <Point X="-4.351858886719" Y="-22.730125" />
                  <Point X="-4.081150878906" Y="-22.266337890625" />
                  <Point X="-4.070562988281" Y="-22.252728515625" />
                  <Point X="-3.750504150391" Y="-21.841337890625" />
                  <Point X="-3.232701904297" Y="-22.140291015625" />
                  <Point X="-3.206657470703" Y="-22.155328125" />
                  <Point X="-3.187728759766" Y="-22.163658203125" />
                  <Point X="-3.167092041016" Y="-22.170166015625" />
                  <Point X="-3.146850341797" Y="-22.17419921875" />
                  <Point X="-3.143382568359" Y="-22.17450390625" />
                  <Point X="-3.061244140625" Y="-22.181689453125" />
                  <Point X="-3.040565429688" Y="-22.18123828125" />
                  <Point X="-3.019112792969" Y="-22.1784140625" />
                  <Point X="-2.999028076172" Y="-22.1735" />
                  <Point X="-2.980482421875" Y="-22.164357421875" />
                  <Point X="-2.962231201172" Y="-22.152734375" />
                  <Point X="-2.946119873047" Y="-22.1398125" />
                  <Point X="-2.943655761719" Y="-22.1373515625" />
                  <Point X="-2.885353271484" Y="-22.079048828125" />
                  <Point X="-2.872406738281" Y="-22.062916015625" />
                  <Point X="-2.860777587891" Y="-22.044662109375" />
                  <Point X="-2.851628662109" Y="-22.026111328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435302734" Y="-21.963884765625" />
                  <Point X="-2.843733642578" Y="-21.96047265625" />
                  <Point X="-2.850919921875" Y="-21.878333984375" />
                  <Point X="-2.854955322266" Y="-21.85804296875" />
                  <Point X="-2.861463623047" Y="-21.837400390625" />
                  <Point X="-2.869794677734" Y="-21.818466796875" />
                  <Point X="-3.183333251953" Y="-21.275404296875" />
                  <Point X="-2.700614746094" Y="-20.905310546875" />
                  <Point X="-2.68394921875" Y="-20.89605078125" />
                  <Point X="-2.167036865234" Y="-20.608865234375" />
                  <Point X="-2.051168212891" Y="-20.7598671875" />
                  <Point X="-2.04319519043" Y="-20.7702578125" />
                  <Point X="-2.028892211914" Y="-20.78519921875" />
                  <Point X="-2.012312744141" Y="-20.799111328125" />
                  <Point X="-1.995119506836" Y="-20.8106015625" />
                  <Point X="-1.991323242188" Y="-20.812578125" />
                  <Point X="-1.899903442383" Y="-20.860169921875" />
                  <Point X="-1.880612548828" Y="-20.867671875" />
                  <Point X="-1.859701293945" Y="-20.8732734375" />
                  <Point X="-1.839259277344" Y="-20.87641796875" />
                  <Point X="-1.81862097168" Y="-20.875064453125" />
                  <Point X="-1.797301513672" Y="-20.8713046875" />
                  <Point X="-1.777387207031" Y="-20.8654921875" />
                  <Point X="-1.773492797852" Y="-20.863876953125" />
                  <Point X="-1.678280639648" Y="-20.824439453125" />
                  <Point X="-1.660146484375" Y="-20.814490234375" />
                  <Point X="-1.642416748047" Y="-20.802076171875" />
                  <Point X="-1.626864257812" Y="-20.7884375" />
                  <Point X="-1.614632568359" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595480712891" Y="-20.734080078125" />
                  <Point X="-1.594193725586" Y="-20.729998046875" />
                  <Point X="-1.563201293945" Y="-20.631703125" />
                  <Point X="-1.559165405273" Y="-20.6114140625" />
                  <Point X="-1.557279052734" Y="-20.5898515625" />
                  <Point X="-1.557730224609" Y="-20.569171875" />
                  <Point X="-1.584201416016" Y="-20.368103515625" />
                  <Point X="-0.949624206543" Y="-20.19019140625" />
                  <Point X="-0.929424560547" Y="-20.187826171875" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.141608093262" Y="-20.684931640625" />
                  <Point X="-0.133903213501" Y="-20.713685546875" />
                  <Point X="-0.121129745483" Y="-20.741876953125" />
                  <Point X="-0.103271606445" Y="-20.768603515625" />
                  <Point X="-0.082114028931" Y="-20.791193359375" />
                  <Point X="-0.054818138123" Y="-20.805783203125" />
                  <Point X="-0.024380064011" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691894531" Y="-20.816115234375" />
                  <Point X="0.067129966736" Y="-20.805783203125" />
                  <Point X="0.094425842285" Y="-20.791193359375" />
                  <Point X="0.115583435059" Y="-20.768603515625" />
                  <Point X="0.133441574097" Y="-20.741876953125" />
                  <Point X="0.146215194702" Y="-20.713685546875" />
                  <Point X="0.307419464111" Y="-20.1120625" />
                  <Point X="0.844042358398" Y="-20.16826171875" />
                  <Point X="0.860765625" Y="-20.172298828125" />
                  <Point X="1.481020141602" Y="-20.322048828125" />
                  <Point X="1.490496704102" Y="-20.325484375" />
                  <Point X="1.894663574219" Y="-20.472080078125" />
                  <Point X="1.905181762695" Y="-20.476998046875" />
                  <Point X="2.294558105469" Y="-20.65909765625" />
                  <Point X="2.304783935547" Y="-20.6650546875" />
                  <Point X="2.680972900391" Y="-20.88422265625" />
                  <Point X="2.690571044922" Y="-20.891048828125" />
                  <Point X="2.943259277344" Y="-21.07074609375" />
                  <Point X="2.181933837891" Y="-22.389400390625" />
                  <Point X="2.147580810547" Y="-22.44890234375" />
                  <Point X="2.141143310547" Y="-22.46241015625" />
                  <Point X="2.132220947266" Y="-22.487146484375" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108484130859" Y="-22.584376953125" />
                  <Point X="2.107480224609" Y="-22.606046875" />
                  <Point X="2.108061523438" Y="-22.62181640625" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121439208984" Y="-22.710388671875" />
                  <Point X="2.129699951172" Y="-22.73246875" />
                  <Point X="2.140060058594" Y="-22.75251171875" />
                  <Point X="2.141784423828" Y="-22.7550546875" />
                  <Point X="2.183029296875" Y="-22.815837890625" />
                  <Point X="2.196255615234" Y="-22.831416015625" />
                  <Point X="2.212078857422" Y="-22.846427734375" />
                  <Point X="2.224121337891" Y="-22.856119140625" />
                  <Point X="2.284906005859" Y="-22.897365234375" />
                  <Point X="2.304974121094" Y="-22.907736328125" />
                  <Point X="2.327093994141" Y="-22.9160078125" />
                  <Point X="2.349053710938" Y="-22.921349609375" />
                  <Point X="2.351821777344" Y="-22.921681640625" />
                  <Point X="2.418385986328" Y="-22.929708984375" />
                  <Point X="2.439149414062" Y="-22.929927734375" />
                  <Point X="2.46125390625" Y="-22.927732421875" />
                  <Point X="2.476406738281" Y="-22.92497265625" />
                  <Point X="2.553491943359" Y="-22.904359375" />
                  <Point X="2.565284912109" Y="-22.900361328125" />
                  <Point X="2.588534179688" Y="-22.889853515625" />
                  <Point X="3.967325439453" Y="-22.09380859375" />
                  <Point X="4.123270507813" Y="-22.310537109375" />
                  <Point X="4.128623046875" Y="-22.3193828125" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.276011962891" Y="-23.29684375" />
                  <Point X="3.230782958984" Y="-23.33155078125" />
                  <Point X="3.219477783203" Y="-23.341765625" />
                  <Point X="3.201670166016" Y="-23.36137890625" />
                  <Point X="3.146191894531" Y="-23.43375390625" />
                  <Point X="3.135477783203" Y="-23.45142578125" />
                  <Point X="3.126149902344" Y="-23.4714453125" />
                  <Point X="3.120771484375" Y="-23.485982421875" />
                  <Point X="3.100105712891" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739013672" Y="-23.62823046875" />
                  <Point X="3.098443359375" Y="-23.63164453125" />
                  <Point X="3.115407958984" Y="-23.71386328125" />
                  <Point X="3.121788818359" Y="-23.733591796875" />
                  <Point X="3.130902832031" Y="-23.7538828125" />
                  <Point X="3.138198730469" Y="-23.767173828125" />
                  <Point X="3.184340332031" Y="-23.837306640625" />
                  <Point X="3.198900634766" Y="-23.854556640625" />
                  <Point X="3.216150146484" Y="-23.8706484375" />
                  <Point X="3.234364013672" Y="-23.883974609375" />
                  <Point X="3.237128662109" Y="-23.88553125" />
                  <Point X="3.303987792969" Y="-23.92316796875" />
                  <Point X="3.323387939453" Y="-23.931404296875" />
                  <Point X="3.345118896484" Y="-23.9378984375" />
                  <Point X="3.359874267578" Y="-23.94105859375" />
                  <Point X="3.450280761719" Y="-23.953005859375" />
                  <Point X="3.462698486328" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845936523438" Y="-24.067189453125" />
                  <Point X="4.847622558594" Y="-24.078021484375" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.768413330078" Y="-24.656521484375" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.701988769531" Y="-24.67562890625" />
                  <Point X="3.677857910156" Y="-24.687064453125" />
                  <Point X="3.589036376953" Y="-24.738404296875" />
                  <Point X="3.572171875" Y="-24.75081640625" />
                  <Point X="3.555646728516" Y="-24.766056640625" />
                  <Point X="3.545317871094" Y="-24.7772421875" />
                  <Point X="3.492024902344" Y="-24.845150390625" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463681640625" Y="-24.907392578125" />
                  <Point X="3.462943847656" Y="-24.911244140625" />
                  <Point X="3.445179443359" Y="-25.00400390625" />
                  <Point X="3.443533203125" Y="-25.02496875" />
                  <Point X="3.444271240234" Y="-25.04762890625" />
                  <Point X="3.445916503906" Y="-25.06240625" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.176662109375" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.492019287109" Y="-25.21740234375" />
                  <Point X="3.494232177734" Y="-25.22022265625" />
                  <Point X="3.547525146484" Y="-25.288130859375" />
                  <Point X="3.562542236328" Y="-25.3033671875" />
                  <Point X="3.580542236328" Y="-25.31791796875" />
                  <Point X="3.59272265625" Y="-25.326287109375" />
                  <Point X="3.681544189453" Y="-25.37762890625" />
                  <Point X="3.692709228516" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.855021972656" Y="-25.9487265625" />
                  <Point X="4.852861328125" Y="-25.958197265625" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.484494384766" Y="-26.01135546875" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.403118408203" Y="-26.00304296875" />
                  <Point X="3.378733154297" Y="-26.005328125" />
                  <Point X="3.367419189453" Y="-26.00708203125" />
                  <Point X="3.193093994141" Y="-26.04497265625" />
                  <Point X="3.16272265625" Y="-26.055693359375" />
                  <Point X="3.132492919922" Y="-26.07506640625" />
                  <Point X="3.115616699219" Y="-26.091107421875" />
                  <Point X="3.108018554688" Y="-26.0992265625" />
                  <Point X="3.002653564453" Y="-26.225947265625" />
                  <Point X="2.986627929688" Y="-26.2504140625" />
                  <Point X="2.974756347656" Y="-26.28109765625" />
                  <Point X="2.970533447266" Y="-26.30246484375" />
                  <Point X="2.969130371094" Y="-26.3121796875" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.955109130859" Y="-26.508484375" />
                  <Point X="2.964593994141" Y="-26.54337109375" />
                  <Point X="2.975111572266" Y="-26.564765625" />
                  <Point X="2.980456542969" Y="-26.574228515625" />
                  <Point X="3.076930664062" Y="-26.724287109375" />
                  <Point X="3.086930175781" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="4.213122070312" Y="-27.6068828125" />
                  <Point X="4.124818847656" Y="-27.7497734375" />
                  <Point X="4.120342285156" Y="-27.7561328125" />
                  <Point X="4.028980224609" Y="-27.8859453125" />
                  <Point X="2.854492675781" Y="-27.20785546875" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.780887939453" Y="-27.168263671875" />
                  <Point X="2.756158447266" Y="-27.1608046875" />
                  <Point X="2.745608398438" Y="-27.15826953125" />
                  <Point X="2.538133544922" Y="-27.12080078125" />
                  <Point X="2.505978515625" Y="-27.119083984375" />
                  <Point X="2.470131103516" Y="-27.125609375" />
                  <Point X="2.447298583984" Y="-27.134544921875" />
                  <Point X="2.437675537109" Y="-27.138943359375" />
                  <Point X="2.265314941406" Y="-27.22965625" />
                  <Point X="2.241145996094" Y="-27.24612890625" />
                  <Point X="2.218142578125" Y="-27.270376953125" />
                  <Point X="2.205484863281" Y="-27.289603515625" />
                  <Point X="2.200765136719" Y="-27.29759765625" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.098733398438" Y="-27.500109375" />
                  <Point X="2.094185058594" Y="-27.536544921875" />
                  <Point X="2.095953613281" Y="-27.561662109375" />
                  <Point X="2.097231445312" Y="-27.571873046875" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138986328125" Y="-27.795146484375" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.7818359375" Y="-29.111654296875" />
                  <Point X="2.776842773438" Y="-29.11488671875" />
                  <Point X="2.701764892578" Y="-29.163484375" />
                  <Point X="1.799400634766" Y="-27.987498046875" />
                  <Point X="1.758546264648" Y="-27.934255859375" />
                  <Point X="1.74291394043" Y="-27.91821875" />
                  <Point X="1.721788330078" Y="-27.901134765625" />
                  <Point X="1.713426391602" Y="-27.89509375" />
                  <Point X="1.508800048828" Y="-27.7635390625" />
                  <Point X="1.479751831055" Y="-27.7496484375" />
                  <Point X="1.443620605469" Y="-27.74194921875" />
                  <Point X="1.417865844727" Y="-27.74158203125" />
                  <Point X="1.407806396484" Y="-27.74197265625" />
                  <Point X="1.184012817383" Y="-27.76256640625" />
                  <Point X="1.155377685547" Y="-27.76853515625" />
                  <Point X="1.124454589844" Y="-27.78240234375" />
                  <Point X="1.104446044922" Y="-27.7961171875" />
                  <Point X="1.097419311523" Y="-27.8014296875" />
                  <Point X="0.924611572266" Y="-27.945115234375" />
                  <Point X="0.902615600586" Y="-27.968638671875" />
                  <Point X="0.884305603027" Y="-28.001056640625" />
                  <Point X="0.876041809082" Y="-28.02625390625" />
                  <Point X="0.873478820801" Y="-28.035681640625" />
                  <Point X="0.821809997559" Y="-28.273396484375" />
                  <Point X="0.81972454834" Y="-28.289626953125" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.975713745117" Y="-29.870076171875" />
                  <Point X="0.971048217773" Y="-29.870923828125" />
                  <Point X="0.929315551758" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058419555664" Y="-29.75263671875" />
                  <Point X="-1.063232788086" Y="-29.7513984375" />
                  <Point X="-1.14124609375" Y="-29.731326171875" />
                  <Point X="-1.122698364258" Y="-29.590443359375" />
                  <Point X="-1.119972290039" Y="-29.564751953125" />
                  <Point X="-1.119666015625" Y="-29.5425546875" />
                  <Point X="-1.120163452148" Y="-29.531447265625" />
                  <Point X="-1.123109619141" Y="-29.503033203125" />
                  <Point X="-1.125747070312" Y="-29.485560546875" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.186859863281" Y="-29.182046875" />
                  <Point X="-1.196860961914" Y="-29.15187109375" />
                  <Point X="-1.206492553711" Y="-29.1313828125" />
                  <Point X="-1.22640222168" Y="-29.099548828125" />
                  <Point X="-1.2379453125" Y="-29.084626953125" />
                  <Point X="-1.257579589844" Y="-29.06387890625" />
                  <Point X="-1.270307250977" Y="-29.051625" />
                  <Point X="-1.494267822266" Y="-28.855216796875" />
                  <Point X="-1.50355456543" Y="-28.848037109375" />
                  <Point X="-1.529853149414" Y="-28.8301875" />
                  <Point X="-1.549968505859" Y="-28.819794921875" />
                  <Point X="-1.585155639648" Y="-28.806654296875" />
                  <Point X="-1.60340612793" Y="-28.80183984375" />
                  <Point X="-1.63161340332" Y="-28.797333984375" />
                  <Point X="-1.649157348633" Y="-28.795361328125" />
                  <Point X="-1.946403442383" Y="-28.77587890625" />
                  <Point X="-1.958145019531" Y="-28.7758359375" />
                  <Point X="-1.989875366211" Y="-28.777685546875" />
                  <Point X="-2.012153076172" Y="-28.781685546875" />
                  <Point X="-2.048057128906" Y="-28.79267578125" />
                  <Point X="-2.0654609375" Y="-28.799962890625" />
                  <Point X="-2.090583496094" Y="-28.813556640625" />
                  <Point X="-2.105722412109" Y="-28.822681640625" />
                  <Point X="-2.353403564453" Y="-28.988177734375" />
                  <Point X="-2.359684326172" Y="-28.9927578125" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471191406" Y="-29.041630859375" />
                  <Point X="-2.503200927734" Y="-29.162478515625" />
                  <Point X="-2.747602294922" Y="-29.01115234375" />
                  <Point X="-2.754219238281" Y="-29.006056640625" />
                  <Point X="-2.980862304688" Y="-28.83155078125" />
                  <Point X="-2.371168945312" Y="-27.775529296875" />
                  <Point X="-2.34148828125" Y="-27.724119140625" />
                  <Point X="-2.334848388672" Y="-27.710080078125" />
                  <Point X="-2.323945068359" Y="-27.681107421875" />
                  <Point X="-2.319681640625" Y="-27.666173828125" />
                  <Point X="-2.313411865234" Y="-27.634642578125" />
                  <Point X="-2.311631103516" Y="-27.618990234375" />
                  <Point X="-2.310681152344" Y="-27.58760546875" />
                  <Point X="-2.314930664062" Y="-27.55649609375" />
                  <Point X="-2.324262939453" Y="-27.526515625" />
                  <Point X="-2.330176513672" Y="-27.511912109375" />
                  <Point X="-2.345090820312" Y="-27.482390625" />
                  <Point X="-2.352932128906" Y="-27.469521484375" />
                  <Point X="-2.370552490234" Y="-27.445181640625" />
                  <Point X="-2.380331542969" Y="-27.4337109375" />
                  <Point X="-2.396982910156" Y="-27.417060546875" />
                  <Point X="-2.408822998047" Y="-27.407015625" />
                  <Point X="-2.433979003906" Y="-27.388990234375" />
                  <Point X="-2.447294921875" Y="-27.381009765625" />
                  <Point X="-2.476124023438" Y="-27.36679296875" />
                  <Point X="-2.490564697266" Y="-27.3610859375" />
                  <Point X="-2.520182861328" Y="-27.3521015625" />
                  <Point X="-2.550870605469" Y="-27.3480625" />
                  <Point X="-2.581804931641" Y="-27.349076171875" />
                  <Point X="-2.597229003906" Y="-27.3508515625" />
                  <Point X="-2.628754882813" Y="-27.357123046875" />
                  <Point X="-2.643685546875" Y="-27.36138671875" />
                  <Point X="-2.672650146484" Y="-27.3722890625" />
                  <Point X="-2.686684082031" Y="-27.378927734375" />
                  <Point X="-3.793088134766" Y="-28.0177109375" />
                  <Point X="-4.004014404297" Y="-27.740595703125" />
                  <Point X="-4.008776611328" Y="-27.732611328125" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.100054199219" Y="-26.613732421875" />
                  <Point X="-3.048122558594" Y="-26.573884765625" />
                  <Point X="-3.036097900391" Y="-26.562896484375" />
                  <Point X="-3.014104980469" Y="-26.539033203125" />
                  <Point X="-3.00413671875" Y="-26.526158203125" />
                  <Point X="-2.985864501953" Y="-26.49783984375" />
                  <Point X="-2.978686767578" Y="-26.484484375" />
                  <Point X="-2.966546630859" Y="-26.456798828125" />
                  <Point X="-2.961584228516" Y="-26.44246875" />
                  <Point X="-2.954186767578" Y="-26.413908203125" />
                  <Point X="-2.951552734375" Y="-26.398802734375" />
                  <Point X="-2.948748779297" Y="-26.3683671875" />
                  <Point X="-2.948578857422" Y="-26.353037109375" />
                  <Point X="-2.950790039062" Y="-26.321359375" />
                  <Point X="-2.95315234375" Y="-26.3059296875" />
                  <Point X="-2.960371826172" Y="-26.275666015625" />
                  <Point X="-2.972450439453" Y="-26.2469921875" />
                  <Point X="-2.989060058594" Y="-26.220685546875" />
                  <Point X="-2.998448242188" Y="-26.20821875" />
                  <Point X="-3.020334716797" Y="-26.183646484375" />
                  <Point X="-3.030981689453" Y="-26.17341796875" />
                  <Point X="-3.053736083984" Y="-26.15473046875" />
                  <Point X="-3.065843505859" Y="-26.146271484375" />
                  <Point X="-3.091270263672" Y="-26.131306640625" />
                  <Point X="-3.105435546875" Y="-26.12448046875" />
                  <Point X="-3.134700683594" Y="-26.113255859375" />
                  <Point X="-3.149800537109" Y="-26.108857421875" />
                  <Point X="-3.181687011719" Y="-26.102376953125" />
                  <Point X="-3.197303955078" Y="-26.10053125" />
                  <Point X="-3.228624511719" Y="-26.09944140625" />
                  <Point X="-3.244328125" Y="-26.100197265625" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.740760253906" Y="-25.974123046875" />
                  <Point X="-4.742019042969" Y="-25.965322265625" />
                  <Point X="-4.786451171875" Y="-25.654658203125" />
                  <Point X="-3.567445800781" Y="-25.328025390625" />
                  <Point X="-3.508287841797" Y="-25.312173828125" />
                  <Point X="-3.499916015625" Y="-25.309509765625" />
                  <Point X="-3.472312255859" Y="-25.298462890625" />
                  <Point X="-3.442515869141" Y="-25.28284375" />
                  <Point X="-3.432454833984" Y="-25.276748046875" />
                  <Point X="-3.405808349609" Y="-25.25825390625" />
                  <Point X="-3.393263427734" Y="-25.24784375" />
                  <Point X="-3.370139404297" Y="-25.22503515625" />
                  <Point X="-3.359560302734" Y="-25.21263671875" />
                  <Point X="-3.339947753906" Y="-25.185193359375" />
                  <Point X="-3.332144287109" Y="-25.172193359375" />
                  <Point X="-3.318705322266" Y="-25.1451171875" />
                  <Point X="-3.313069824219" Y="-25.131041015625" />
                  <Point X="-3.304187744141" Y="-25.102423828125" />
                  <Point X="-3.300908203125" Y="-25.087943359375" />
                  <Point X="-3.296645263672" Y="-25.0586484375" />
                  <Point X="-3.295661865234" Y="-25.043833984375" />
                  <Point X="-3.296030273438" Y="-25.013001953125" />
                  <Point X="-3.297183349609" Y="-24.9993359375" />
                  <Point X="-3.301445800781" Y="-24.972310546875" />
                  <Point X="-3.304555175781" Y="-24.958951171875" />
                  <Point X="-3.3134375" Y="-24.93033203125" />
                  <Point X="-3.319607910156" Y="-24.9151953125" />
                  <Point X="-3.334450439453" Y="-24.88620703125" />
                  <Point X="-3.343122558594" Y="-24.87235546875" />
                  <Point X="-3.363472900391" Y="-24.84533203125" />
                  <Point X="-3.373497802734" Y="-24.83401953125" />
                  <Point X="-3.39521875" Y="-24.813123046875" />
                  <Point X="-3.406914794922" Y="-24.8035390625" />
                  <Point X="-3.433561279297" Y="-24.785044921875" />
                  <Point X="-3.440483642578" Y="-24.780669921875" />
                  <Point X="-3.465612304688" Y="-24.767166015625" />
                  <Point X="-3.496564941406" Y="-24.75436328125" />
                  <Point X="-3.508287841797" Y="-24.75038671875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731331054688" Y="-24.042470703125" />
                  <Point X="-4.728795410156" Y="-24.03311328125" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-3.820471679688" Y="-23.7888125" />
                  <Point X="-3.778065673828" Y="-23.79439453125" />
                  <Point X="-3.767738769531" Y="-23.79518359375" />
                  <Point X="-3.747069580078" Y="-23.795634765625" />
                  <Point X="-3.736727294922" Y="-23.795296875" />
                  <Point X="-3.715178466797" Y="-23.7934140625" />
                  <Point X="-3.704934570312" Y="-23.791953125" />
                  <Point X="-3.684654541016" Y="-23.787923828125" />
                  <Point X="-3.672120849609" Y="-23.784568359375" />
                  <Point X="-3.613143798828" Y="-23.76597265625" />
                  <Point X="-3.603443603516" Y="-23.7623203125" />
                  <Point X="-3.584507080078" Y="-23.753986328125" />
                  <Point X="-3.575270751953" Y="-23.7493046875" />
                  <Point X="-3.556524658203" Y="-23.73848046875" />
                  <Point X="-3.547850830078" Y="-23.7328203125" />
                  <Point X="-3.531169433594" Y="-23.720587890625" />
                  <Point X="-3.515920166016" Y="-23.70661328125" />
                  <Point X="-3.502281738281" Y="-23.691060546875" />
                  <Point X="-3.495885009766" Y="-23.68291015625" />
                  <Point X="-3.483470703125" Y="-23.665177734375" />
                  <Point X="-3.478004882812" Y="-23.65638671875" />
                  <Point X="-3.4680625" Y="-23.638263671875" />
                  <Point X="-3.462605224609" Y="-23.626564453125" />
                  <Point X="-3.438940429688" Y="-23.56943359375" />
                  <Point X="-3.435502441406" Y="-23.559658203125" />
                  <Point X="-3.429711669922" Y="-23.53979296875" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436013671875" Y="-23.39546484375" />
                  <Point X="-3.443514892578" Y="-23.376177734375" />
                  <Point X="-3.448978271484" Y="-23.3644609375" />
                  <Point X="-3.477532226562" Y="-23.309609375" />
                  <Point X="-3.482808837891" Y="-23.30069921875" />
                  <Point X="-3.494297851562" Y="-23.2835078125" />
                  <Point X="-3.500510253906" Y="-23.2752265625" />
                  <Point X="-3.514423339844" Y="-23.258646484375" />
                  <Point X="-3.521500732422" Y="-23.25108984375" />
                  <Point X="-3.536440917969" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-4.22761328125" Y="-22.705716796875" />
                  <Point X="-4.002292480469" Y="-22.3196875" />
                  <Point X="-3.99558203125" Y="-22.3110625" />
                  <Point X="-3.726337158203" Y="-21.964986328125" />
                  <Point X="-3.280201904297" Y="-22.2225625" />
                  <Point X="-3.254157470703" Y="-22.237599609375" />
                  <Point X="-3.244923095703" Y="-22.24228125" />
                  <Point X="-3.225994384766" Y="-22.250611328125" />
                  <Point X="-3.216300048828" Y="-22.254259765625" />
                  <Point X="-3.195663330078" Y="-22.260767578125" />
                  <Point X="-3.185656005859" Y="-22.263333984375" />
                  <Point X="-3.165414306641" Y="-22.2673671875" />
                  <Point X="-3.151661621094" Y="-22.269142578125" />
                  <Point X="-3.069523193359" Y="-22.276328125" />
                  <Point X="-3.059171875" Y="-22.276666015625" />
                  <Point X="-3.038493164062" Y="-22.27621484375" />
                  <Point X="-3.028165771484" Y="-22.27542578125" />
                  <Point X="-3.006713134766" Y="-22.2726015625" />
                  <Point X="-2.996535400391" Y="-22.27069140625" />
                  <Point X="-2.976450683594" Y="-22.26577734375" />
                  <Point X="-2.957022216797" Y="-22.258708984375" />
                  <Point X="-2.9384765625" Y="-22.24956640625" />
                  <Point X="-2.929452392578" Y="-22.24448828125" />
                  <Point X="-2.911201171875" Y="-22.232865234375" />
                  <Point X="-2.902793212891" Y="-22.22684375" />
                  <Point X="-2.886681884766" Y="-22.213921875" />
                  <Point X="-2.87648046875" Y="-22.20452734375" />
                  <Point X="-2.818177978516" Y="-22.146224609375" />
                  <Point X="-2.811260986328" Y="-22.1385078125" />
                  <Point X="-2.798314453125" Y="-22.122375" />
                  <Point X="-2.792284912109" Y="-22.113958984375" />
                  <Point X="-2.780655761719" Y="-22.095705078125" />
                  <Point X="-2.775575927734" Y="-22.086681640625" />
                  <Point X="-2.766427001953" Y="-22.068130859375" />
                  <Point X="-2.759351318359" Y="-22.04869140625" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.986634765625" />
                  <Point X="-2.748458007813" Y="-21.965958984375" />
                  <Point X="-2.749094726562" Y="-21.952197265625" />
                  <Point X="-2.756281005859" Y="-21.87005859375" />
                  <Point X="-2.757744628906" Y="-21.859802734375" />
                  <Point X="-2.761780029297" Y="-21.83951171875" />
                  <Point X="-2.764351806641" Y="-21.8294765625" />
                  <Point X="-2.770860107422" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799138671875" />
                  <Point X="-2.782840332031" Y="-21.780205078125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.059387207031" Y="-21.3000859375" />
                  <Point X="-2.648360351563" Y="-20.98495703125" />
                  <Point X="-2.637808837891" Y="-20.97909375" />
                  <Point X="-2.192524169922" Y="-20.731703125" />
                  <Point X="-2.126536621094" Y="-20.81769921875" />
                  <Point X="-2.111820556641" Y="-20.835951171875" />
                  <Point X="-2.097517578125" Y="-20.850892578125" />
                  <Point X="-2.089957763672" Y="-20.85797265625" />
                  <Point X="-2.073378417969" Y="-20.871884765625" />
                  <Point X="-2.065098632812" Y="-20.878095703125" />
                  <Point X="-2.047905273437" Y="-20.8895859375" />
                  <Point X="-2.035195556641" Y="-20.896841796875" />
                  <Point X="-1.943775756836" Y="-20.94443359375" />
                  <Point X="-1.934335571289" Y="-20.9487109375" />
                  <Point X="-1.915044677734" Y="-20.956212890625" />
                  <Point X="-1.905193969727" Y="-20.9594375" />
                  <Point X="-1.884282470703" Y="-20.9650390625" />
                  <Point X="-1.874144897461" Y="-20.967169921875" />
                  <Point X="-1.853702880859" Y="-20.970314453125" />
                  <Point X="-1.833042236328" Y="-20.97121484375" />
                  <Point X="-1.812403930664" Y="-20.969861328125" />
                  <Point X="-1.802121948242" Y="-20.96862109375" />
                  <Point X="-1.780802490234" Y="-20.964861328125" />
                  <Point X="-1.770683959961" Y="-20.9625" />
                  <Point X="-1.75076965332" Y="-20.9566875" />
                  <Point X="-1.737138427734" Y="-20.951646484375" />
                  <Point X="-1.641926269531" Y="-20.912208984375" />
                  <Point X="-1.632584960938" Y="-20.907728515625" />
                  <Point X="-1.614450805664" Y="-20.897779296875" />
                  <Point X="-1.605657958984" Y="-20.892310546875" />
                  <Point X="-1.587928222656" Y="-20.879896484375" />
                  <Point X="-1.579780029297" Y="-20.873501953125" />
                  <Point X="-1.564227539062" Y="-20.85986328125" />
                  <Point X="-1.550252441406" Y="-20.84461328125" />
                  <Point X="-1.538020751953" Y="-20.827931640625" />
                  <Point X="-1.532359863281" Y="-20.819255859375" />
                  <Point X="-1.521538085938" Y="-20.80051171875" />
                  <Point X="-1.516856079102" Y="-20.7912734375" />
                  <Point X="-1.508526000977" Y="-20.772341796875" />
                  <Point X="-1.503590209961" Y="-20.758564453125" />
                  <Point X="-1.47259777832" Y="-20.66026953125" />
                  <Point X="-1.470026855469" Y="-20.65023828125" />
                  <Point X="-1.465990966797" Y="-20.62994921875" />
                  <Point X="-1.464526855469" Y="-20.619693359375" />
                  <Point X="-1.462640380859" Y="-20.598130859375" />
                  <Point X="-1.462301513672" Y="-20.587779296875" />
                  <Point X="-1.462752807617" Y="-20.567099609375" />
                  <Point X="-1.46354296875" Y="-20.556771484375" />
                  <Point X="-1.479265625" Y="-20.437345703125" />
                  <Point X="-0.931165893555" Y="-20.2836796875" />
                  <Point X="-0.918376281738" Y="-20.282181640625" />
                  <Point X="-0.365222473145" Y="-20.21744140625" />
                  <Point X="-0.233371032715" Y="-20.70951953125" />
                  <Point X="-0.225666122437" Y="-20.7382734375" />
                  <Point X="-0.220435180664" Y="-20.752892578125" />
                  <Point X="-0.207661636353" Y="-20.781083984375" />
                  <Point X="-0.20011932373" Y="-20.79465625" />
                  <Point X="-0.182261245728" Y="-20.8213828125" />
                  <Point X="-0.172608932495" Y="-20.833544921875" />
                  <Point X="-0.151451278687" Y="-20.856134765625" />
                  <Point X="-0.126896400452" Y="-20.8749765625" />
                  <Point X="-0.099600616455" Y="-20.88956640625" />
                  <Point X="-0.085354072571" Y="-20.8957421875" />
                  <Point X="-0.054915958405" Y="-20.90607421875" />
                  <Point X="-0.039853622437" Y="-20.909845703125" />
                  <Point X="-0.009317713737" Y="-20.91488671875" />
                  <Point X="0.021629436493" Y="-20.91488671875" />
                  <Point X="0.052165493011" Y="-20.909845703125" />
                  <Point X="0.067227828979" Y="-20.90607421875" />
                  <Point X="0.09766594696" Y="-20.8957421875" />
                  <Point X="0.111912338257" Y="-20.88956640625" />
                  <Point X="0.139208267212" Y="-20.8749765625" />
                  <Point X="0.163763153076" Y="-20.856134765625" />
                  <Point X="0.184920654297" Y="-20.833544921875" />
                  <Point X="0.194572967529" Y="-20.8213828125" />
                  <Point X="0.21243119812" Y="-20.79465625" />
                  <Point X="0.219973373413" Y="-20.781083984375" />
                  <Point X="0.232746902466" Y="-20.752892578125" />
                  <Point X="0.237978134155" Y="-20.7382734375" />
                  <Point X="0.378190612793" Y="-20.2149921875" />
                  <Point X="0.827868164062" Y="-20.2620859375" />
                  <Point X="0.838472412109" Y="-20.264646484375" />
                  <Point X="1.453598388672" Y="-20.413158203125" />
                  <Point X="1.458118530273" Y="-20.414796875" />
                  <Point X="1.858264160156" Y="-20.55993359375" />
                  <Point X="1.864943969727" Y="-20.563056640625" />
                  <Point X="2.250436767578" Y="-20.74333984375" />
                  <Point X="2.256964355469" Y="-20.747142578125" />
                  <Point X="2.629430419922" Y="-20.964142578125" />
                  <Point X="2.635511962891" Y="-20.968466796875" />
                  <Point X="2.817778808594" Y="-21.098083984375" />
                  <Point X="2.099661376953" Y="-22.341900390625" />
                  <Point X="2.065308349609" Y="-22.40140234375" />
                  <Point X="2.061822021484" Y="-22.40803125" />
                  <Point X="2.051778808594" Y="-22.43017578125" />
                  <Point X="2.042856567383" Y="-22.454912109375" />
                  <Point X="2.040445800781" Y="-22.46260546875" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017728637695" Y="-22.549677734375" />
                  <Point X="2.01460559082" Y="-22.569822265625" />
                  <Point X="2.01358581543" Y="-22.57998046875" />
                  <Point X="2.01258203125" Y="-22.601650390625" />
                  <Point X="2.012544677734" Y="-22.609546875" />
                  <Point X="2.013744750977" Y="-22.633189453125" />
                  <Point X="2.021782470703" Y="-22.699845703125" />
                  <Point X="2.023799560547" Y="-22.710962890625" />
                  <Point X="2.029139648438" Y="-22.73287890625" />
                  <Point X="2.032462402344" Y="-22.743677734375" />
                  <Point X="2.040723022461" Y="-22.7657578125" />
                  <Point X="2.045307373047" Y="-22.776091796875" />
                  <Point X="2.055667480469" Y="-22.796134765625" />
                  <Point X="2.063173583984" Y="-22.808396484375" />
                  <Point X="2.104418457031" Y="-22.8691796875" />
                  <Point X="2.110610351562" Y="-22.87732421875" />
                  <Point X="2.123836669922" Y="-22.89290234375" />
                  <Point X="2.13087109375" Y="-22.9003359375" />
                  <Point X="2.146694335938" Y="-22.91534765625" />
                  <Point X="2.152517822266" Y="-22.9204375" />
                  <Point X="2.170779296875" Y="-22.93473046875" />
                  <Point X="2.231563964844" Y="-22.9759765625" />
                  <Point X="2.241290527344" Y="-22.98176171875" />
                  <Point X="2.261358642578" Y="-22.9921328125" />
                  <Point X="2.271700195312" Y="-22.99671875" />
                  <Point X="2.293820068359" Y="-23.004990234375" />
                  <Point X="2.304639648438" Y="-23.00831640625" />
                  <Point X="2.326599365234" Y="-23.013658203125" />
                  <Point X="2.3405078125" Y="-23.016005859375" />
                  <Point X="2.40701171875" Y="-23.024025390625" />
                  <Point X="2.417385253906" Y="-23.024703125" />
                  <Point X="2.438148681641" Y="-23.024921875" />
                  <Point X="2.448538085938" Y="-23.024462890625" />
                  <Point X="2.470642578125" Y="-23.022267578125" />
                  <Point X="2.478276123047" Y="-23.0211953125" />
                  <Point X="2.500948242188" Y="-23.016748046875" />
                  <Point X="2.578033447266" Y="-22.996134765625" />
                  <Point X="2.583993652344" Y="-22.994330078125" />
                  <Point X="2.604410888672" Y="-22.9869296875" />
                  <Point X="2.62766015625" Y="-22.976421875" />
                  <Point X="2.636034179688" Y="-22.972125" />
                  <Point X="3.940403076172" Y="-22.219048828125" />
                  <Point X="4.043952392578" Y="-22.362958984375" />
                  <Point X="4.047344726562" Y="-22.368564453125" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.2181796875" Y="-23.221474609375" />
                  <Point X="3.172950683594" Y="-23.256181640625" />
                  <Point X="3.167093017578" Y="-23.2610625" />
                  <Point X="3.149143066406" Y="-23.27790625" />
                  <Point X="3.131335449219" Y="-23.29751953125" />
                  <Point X="3.126272949219" Y="-23.303583984375" />
                  <Point X="3.070794677734" Y="-23.375958984375" />
                  <Point X="3.064956054688" Y="-23.384501953125" />
                  <Point X="3.054241943359" Y="-23.402173828125" />
                  <Point X="3.049366455078" Y="-23.411302734375" />
                  <Point X="3.040038574219" Y="-23.431322265625" />
                  <Point X="3.037052490234" Y="-23.43848046875" />
                  <Point X="3.029281738281" Y="-23.460396484375" />
                  <Point X="3.008615966797" Y="-23.53429296875" />
                  <Point X="3.006224853516" Y="-23.54533984375" />
                  <Point X="3.002771484375" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077392578" Y="-23.6362421875" />
                  <Point X="3.005402832031" Y="-23.65083984375" />
                  <Point X="3.022367431641" Y="-23.73305859375" />
                  <Point X="3.025018066406" Y="-23.74309765625" />
                  <Point X="3.031398925781" Y="-23.762826171875" />
                  <Point X="3.035129150391" Y="-23.772515625" />
                  <Point X="3.044243164063" Y="-23.792806640625" />
                  <Point X="3.047625" Y="-23.79959765625" />
                  <Point X="3.058834960938" Y="-23.819388671875" />
                  <Point X="3.1049765625" Y="-23.889521484375" />
                  <Point X="3.111744384766" Y="-23.898583984375" />
                  <Point X="3.1263046875" Y="-23.915833984375" />
                  <Point X="3.134097167969" Y="-23.924021484375" />
                  <Point X="3.151346679688" Y="-23.94011328125" />
                  <Point X="3.1600546875" Y="-23.947318359375" />
                  <Point X="3.178268554688" Y="-23.96064453125" />
                  <Point X="3.190519042969" Y="-23.968310546875" />
                  <Point X="3.257378173828" Y="-24.005947265625" />
                  <Point X="3.266862792969" Y="-24.01061328125" />
                  <Point X="3.286262939453" Y="-24.018849609375" />
                  <Point X="3.296186523438" Y="-24.02242578125" />
                  <Point X="3.317917480469" Y="-24.028919921875" />
                  <Point X="3.325223876953" Y="-24.030791015625" />
                  <Point X="3.347428222656" Y="-24.035240234375" />
                  <Point X="3.437834716797" Y="-24.0471875" />
                  <Point X="3.444033691406" Y="-24.04780078125" />
                  <Point X="3.465708740234" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.75268359375" Y="-24.085763671875" />
                  <Point X="4.753752929688" Y="-24.0926328125" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="3.743825439453" Y="-24.5647578125" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.684586181641" Y="-24.580958984375" />
                  <Point X="3.661305664062" Y="-24.58978125" />
                  <Point X="3.637174804688" Y="-24.601216796875" />
                  <Point X="3.630317138672" Y="-24.60481640625" />
                  <Point X="3.541495605469" Y="-24.65615625" />
                  <Point X="3.532724609375" Y="-24.661892578125" />
                  <Point X="3.515860107422" Y="-24.6743046875" />
                  <Point X="3.507766601562" Y="-24.68098046875" />
                  <Point X="3.491241455078" Y="-24.696220703125" />
                  <Point X="3.485852050781" Y="-24.701607421875" />
                  <Point X="3.470583740234" Y="-24.718591796875" />
                  <Point X="3.417290771484" Y="-24.7865" />
                  <Point X="3.410854003906" Y="-24.795791015625" />
                  <Point X="3.399129394531" Y="-24.815072265625" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.380004638672" Y="-24.857072265625" />
                  <Point X="3.373159912109" Y="-24.878568359375" />
                  <Point X="3.369640380859" Y="-24.89337109375" />
                  <Point X="3.351875976562" Y="-24.986130859375" />
                  <Point X="3.350470947266" Y="-24.99656640625" />
                  <Point X="3.348824707031" Y="-25.01753125" />
                  <Point X="3.348583496094" Y="-25.028060546875" />
                  <Point X="3.349321533203" Y="-25.050720703125" />
                  <Point X="3.349854736328" Y="-25.058140625" />
                  <Point X="3.352612060547" Y="-25.080275390625" />
                  <Point X="3.370376464844" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.18398828125" />
                  <Point X="3.380004638672" Y="-25.205486328125" />
                  <Point X="3.384067382812" Y="-25.216029296875" />
                  <Point X="3.393841064453" Y="-25.237494140625" />
                  <Point X="3.399125732422" Y="-25.24748046875" />
                  <Point X="3.410844970703" Y="-25.266755859375" />
                  <Point X="3.419492431641" Y="-25.278865234375" />
                  <Point X="3.472785400391" Y="-25.3467734375" />
                  <Point X="3.479864990234" Y="-25.354818359375" />
                  <Point X="3.494882080078" Y="-25.3700546875" />
                  <Point X="3.502819580078" Y="-25.37724609375" />
                  <Point X="3.520819580078" Y="-25.391796875" />
                  <Point X="3.526743408203" Y="-25.396216796875" />
                  <Point X="3.545180419922" Y="-25.40853515625" />
                  <Point X="3.634001953125" Y="-25.459876953125" />
                  <Point X="3.639491699219" Y="-25.462814453125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027099609" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.761612792969" Y="-25.9310546875" />
                  <Point X="4.760241210938" Y="-25.93706640625" />
                  <Point X="4.727801757812" Y="-26.07921875" />
                  <Point X="3.496894287109" Y="-25.91716796875" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.426161865234" Y="-25.908458984375" />
                  <Point X="3.404898193359" Y="-25.908060546875" />
                  <Point X="3.394254638672" Y="-25.90845703125" />
                  <Point X="3.369869384766" Y="-25.9107421875" />
                  <Point X="3.347241455078" Y="-25.91425" />
                  <Point X="3.172916259766" Y="-25.952140625" />
                  <Point X="3.161472412109" Y="-25.955390625" />
                  <Point X="3.131101074219" Y="-25.966111328125" />
                  <Point X="3.111463623047" Y="-25.975708984375" />
                  <Point X="3.081233886719" Y="-25.99508203125" />
                  <Point X="3.067043212891" Y="-26.006208984375" />
                  <Point X="3.050166992188" Y="-26.02225" />
                  <Point X="3.034970703125" Y="-26.03848828125" />
                  <Point X="2.929605712891" Y="-26.165208984375" />
                  <Point X="2.923183349609" Y="-26.17389453125" />
                  <Point X="2.907157714844" Y="-26.198361328125" />
                  <Point X="2.898028076172" Y="-26.216134765625" />
                  <Point X="2.886156494141" Y="-26.246818359375" />
                  <Point X="2.881559082031" Y="-26.262677734375" />
                  <Point X="2.877336181641" Y="-26.284044921875" />
                  <Point X="2.874530029297" Y="-26.303474609375" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.85908203125" Y="-26.479484375" />
                  <Point X="2.860162597656" Y="-26.511671875" />
                  <Point X="2.863436767578" Y="-26.533408203125" />
                  <Point X="2.872921630859" Y="-26.568294921875" />
                  <Point X="2.879338867188" Y="-26.585283203125" />
                  <Point X="2.889856445312" Y="-26.606677734375" />
                  <Point X="2.900546386719" Y="-26.625603515625" />
                  <Point X="2.997020507812" Y="-26.775662109375" />
                  <Point X="3.001739746094" Y="-26.782349609375" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043487304688" Y="-26.828119140625" />
                  <Point X="3.052795898438" Y="-26.836279296875" />
                  <Point X="4.087170410156" Y="-27.629982421875" />
                  <Point X="4.045497558594" Y="-27.697416015625" />
                  <Point X="4.042658935547" Y="-27.70144921875" />
                  <Point X="4.001272949219" Y="-27.76025390625" />
                  <Point X="2.901992675781" Y="-27.125583984375" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.838676513672" Y="-27.089755859375" />
                  <Point X="2.818609863281" Y="-27.08107421875" />
                  <Point X="2.808321289062" Y="-27.077310546875" />
                  <Point X="2.783591796875" Y="-27.0698515625" />
                  <Point X="2.762491699219" Y="-27.06478125" />
                  <Point X="2.555016845703" Y="-27.0273125" />
                  <Point X="2.543198486328" Y="-27.025935546875" />
                  <Point X="2.511043457031" Y="-27.02421875" />
                  <Point X="2.488965087891" Y="-27.025619140625" />
                  <Point X="2.453117675781" Y="-27.03214453125" />
                  <Point X="2.435509521484" Y="-27.037142578125" />
                  <Point X="2.412677001953" Y="-27.046078125" />
                  <Point X="2.393430908203" Y="-27.054875" />
                  <Point X="2.2210703125" Y="-27.145587890625" />
                  <Point X="2.211811523438" Y="-27.15115625" />
                  <Point X="2.187642578125" Y="-27.16762890625" />
                  <Point X="2.172225341797" Y="-27.18074609375" />
                  <Point X="2.149221923828" Y="-27.204994140625" />
                  <Point X="2.138794433594" Y="-27.218138671875" />
                  <Point X="2.12613671875" Y="-27.237365234375" />
                  <Point X="2.116697265625" Y="-27.253353515625" />
                  <Point X="2.025985229492" Y="-27.425712890625" />
                  <Point X="2.021113769531" Y="-27.436568359375" />
                  <Point X="2.009794189453" Y="-27.466720703125" />
                  <Point X="2.004465087891" Y="-27.488341796875" />
                  <Point X="1.999916748047" Y="-27.52477734375" />
                  <Point X="1.999419677734" Y="-27.543216796875" />
                  <Point X="2.001188232422" Y="-27.568333984375" />
                  <Point X="2.003743774414" Y="-27.588755859375" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051237792969" Y="-27.831548828125" />
                  <Point X="2.064070800781" Y="-27.862482421875" />
                  <Point X="2.069546875" Y="-27.873580078125" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.723753417969" Y="-29.0360859375" />
                  <Point X="1.874769287109" Y="-27.929666015625" />
                  <Point X="1.833914916992" Y="-27.876423828125" />
                  <Point X="1.826574462891" Y="-27.8679453125" />
                  <Point X="1.810942138672" Y="-27.851908203125" />
                  <Point X="1.802650512695" Y="-27.844349609375" />
                  <Point X="1.781524780273" Y="-27.827265625" />
                  <Point X="1.764801025391" Y="-27.81518359375" />
                  <Point X="1.560174560547" Y="-27.68362890625" />
                  <Point X="1.549783569336" Y="-27.677833984375" />
                  <Point X="1.520735229492" Y="-27.663943359375" />
                  <Point X="1.49955090332" Y="-27.656734375" />
                  <Point X="1.463419555664" Y="-27.64903515625" />
                  <Point X="1.444974975586" Y="-27.646958984375" />
                  <Point X="1.419220092773" Y="-27.646591796875" />
                  <Point X="1.399101074219" Y="-27.647373046875" />
                  <Point X="1.175307617188" Y="-27.667966796875" />
                  <Point X="1.164627563477" Y="-27.669564453125" />
                  <Point X="1.135992431641" Y="-27.675533203125" />
                  <Point X="1.116505615234" Y="-27.6818515625" />
                  <Point X="1.085582275391" Y="-27.69571875" />
                  <Point X="1.070743530273" Y="-27.70404296875" />
                  <Point X="1.050734985352" Y="-27.7177578125" />
                  <Point X="1.036681884766" Y="-27.7283828125" />
                  <Point X="0.863874145508" Y="-27.872068359375" />
                  <Point X="0.855221313477" Y="-27.88023046875" />
                  <Point X="0.833225280762" Y="-27.90375390625" />
                  <Point X="0.819897644043" Y="-27.92191796875" />
                  <Point X="0.801587768555" Y="-27.9543359375" />
                  <Point X="0.794036254883" Y="-27.971451171875" />
                  <Point X="0.785772521973" Y="-27.9966484375" />
                  <Point X="0.780646362305" Y="-28.01550390625" />
                  <Point X="0.728977478027" Y="-28.25321875" />
                  <Point X="0.727584716797" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.72474230957" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091491699" Y="-29.15233984375" />
                  <Point X="0.671036071777" Y="-28.547541015625" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.651436279297" Y="-28.477064453125" />
                  <Point X="0.642895568848" Y="-28.4558359375" />
                  <Point X="0.637983398438" Y="-28.445478515625" />
                  <Point X="0.624235473633" Y="-28.420505859375" />
                  <Point X="0.613880065918" Y="-28.403806640625" />
                  <Point X="0.456680267334" Y="-28.177310546875" />
                  <Point X="0.449298187256" Y="-28.16798046875" />
                  <Point X="0.427770751953" Y="-28.1440234375" />
                  <Point X="0.410758636475" Y="-28.129119140625" />
                  <Point X="0.379893829346" Y="-28.107998046875" />
                  <Point X="0.363413482666" Y="-28.09897265625" />
                  <Point X="0.338574279785" Y="-28.088412109375" />
                  <Point X="0.320553161621" Y="-28.081802734375" />
                  <Point X="0.077295394897" Y="-28.0063046875" />
                  <Point X="0.066811683655" Y="-28.003693359375" />
                  <Point X="0.038066532135" Y="-27.99825" />
                  <Point X="0.017431879044" Y="-27.996638671875" />
                  <Point X="-0.016852149963" Y="-27.99770703125" />
                  <Point X="-0.033942295074" Y="-27.99980078125" />
                  <Point X="-0.058864421844" Y="-28.005181640625" />
                  <Point X="-0.075084976196" Y="-28.009439453125" />
                  <Point X="-0.318342590332" Y="-28.0849375" />
                  <Point X="-0.329466125488" Y="-28.089169921875" />
                  <Point X="-0.35879208374" Y="-28.10248828125" />
                  <Point X="-0.378238067627" Y="-28.114314453125" />
                  <Point X="-0.407866790771" Y="-28.1376171875" />
                  <Point X="-0.421514526367" Y="-28.15075390625" />
                  <Point X="-0.439560028076" Y="-28.171978515625" />
                  <Point X="-0.450895477295" Y="-28.186712890625" />
                  <Point X="-0.608095397949" Y="-28.41320703125" />
                  <Point X="-0.612470825195" Y="-28.420130859375" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778137207" Y="-28.47621484375" />
                  <Point X="-0.642753051758" Y="-28.487935546875" />
                  <Point X="-0.985425292969" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667028768286" Y="-28.962160783672" />
                  <Point X="2.686939126373" Y="-28.942933574365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.945177322135" Y="-27.727867072684" />
                  <Point X="4.064529826629" Y="-27.612609698849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.608822341109" Y="-28.886304535872" />
                  <Point X="2.6379849422" Y="-28.858142539354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.859589385008" Y="-27.67845284168" />
                  <Point X="3.988324191327" Y="-27.554135084291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550615913932" Y="-28.810448288071" />
                  <Point X="2.589030758027" Y="-28.773351504344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.774001447881" Y="-27.629038610676" />
                  <Point X="3.912118556025" Y="-27.495660469732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.492409486756" Y="-28.734592040271" />
                  <Point X="2.540076573853" Y="-28.688560469333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.688413510754" Y="-27.579624379671" />
                  <Point X="3.835912920723" Y="-27.437185855174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.434203059579" Y="-28.65873579247" />
                  <Point X="2.49112238968" Y="-28.603769434323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.602825573627" Y="-27.530210148667" />
                  <Point X="3.759707285421" Y="-27.378711240615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375996632403" Y="-28.582879544669" />
                  <Point X="2.442168205506" Y="-28.518978399312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.5172376365" Y="-27.480795917663" />
                  <Point X="3.683501650119" Y="-27.320236626057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.317790205226" Y="-28.507023296869" />
                  <Point X="2.393214021333" Y="-28.434187364302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.431649699373" Y="-27.431381686659" />
                  <Point X="3.607296014817" Y="-27.261762011498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.25958377805" Y="-28.431167049068" />
                  <Point X="2.34425983716" Y="-28.349396329291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.346061762246" Y="-27.381967455655" />
                  <Point X="3.531090379515" Y="-27.20328739694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.699021169624" Y="-26.07542974318" />
                  <Point X="4.737046038447" Y="-26.038709554195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.201377350873" Y="-28.355310801268" />
                  <Point X="2.295305652986" Y="-28.264605294281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.260473825119" Y="-27.332553224651" />
                  <Point X="3.454884744213" Y="-27.144812782382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.578670584244" Y="-26.059585411377" />
                  <Point X="4.770106487878" Y="-25.874717908142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.143170923697" Y="-28.279454553467" />
                  <Point X="2.246351468813" Y="-28.17981425927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.174885887992" Y="-27.283138993647" />
                  <Point X="3.378679108911" Y="-27.086338167823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.458319998863" Y="-26.043741079574" />
                  <Point X="4.74567303513" Y="-25.766247478044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.08496449652" Y="-28.203598305666" />
                  <Point X="2.197397284639" Y="-28.09502322426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.089297950865" Y="-27.233724762643" />
                  <Point X="3.302473473609" Y="-27.027863553265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.337969413483" Y="-26.027896747771" />
                  <Point X="4.638619193292" Y="-25.73756263026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.026758069344" Y="-28.127742057866" />
                  <Point X="2.148443100466" Y="-28.010232189249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003710013738" Y="-27.184310531639" />
                  <Point X="3.226267838307" Y="-26.969388938706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.217618828102" Y="-26.012052415968" />
                  <Point X="4.531565351454" Y="-25.708877782477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.83225004162" Y="-29.149199510509" />
                  <Point X="0.832629780478" Y="-29.148832800957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.968551642167" Y="-28.051885810065" />
                  <Point X="2.099488916292" Y="-27.925441154239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.918122076611" Y="-27.134896300635" />
                  <Point X="3.150062203005" Y="-26.910914324148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.097268242722" Y="-25.996208084165" />
                  <Point X="4.424511509616" Y="-25.680192934693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804137461262" Y="-29.044281972645" />
                  <Point X="0.817204154974" Y="-29.031663613203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.910345214991" Y="-27.976029562265" />
                  <Point X="2.053732447719" Y="-27.837562121168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.831378223088" Y="-27.086598325118" />
                  <Point X="3.073856567703" Y="-26.85243970959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.976917657341" Y="-25.980363752361" />
                  <Point X="4.317457667778" Y="-25.651508086909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.776024880905" Y="-28.93936443478" />
                  <Point X="0.801778529471" Y="-28.91449442545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.852138696738" Y="-27.900173402415" />
                  <Point X="2.029119384216" Y="-27.72926513916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.72434621062" Y="-27.057892396957" />
                  <Point X="3.005240871178" Y="-26.786635576353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.85656707196" Y="-25.964519420558" />
                  <Point X="4.21040382594" Y="-25.622823239126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.747912300547" Y="-28.834446896916" />
                  <Point X="0.786352903967" Y="-28.797325237697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.786615771699" Y="-27.831382614472" />
                  <Point X="2.008810567112" Y="-27.61681159472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.6091342194" Y="-27.037085782455" />
                  <Point X="2.952138119992" Y="-26.705850765938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.73621648658" Y="-25.948675088755" />
                  <Point X="4.103349984102" Y="-25.594138391342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.71979972019" Y="-28.729529359052" />
                  <Point X="0.770927278463" Y="-28.680156049943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.705867275326" Y="-27.777294989855" />
                  <Point X="2.004379831222" Y="-27.489024765487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483155248128" Y="-27.026676719727" />
                  <Point X="2.899816617983" Y="-26.624311511962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.615865901199" Y="-25.932830756952" />
                  <Point X="3.996296142264" Y="-25.565453543558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.691687139832" Y="-28.624611821187" />
                  <Point X="0.75550165296" Y="-28.56298686219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.623767108082" Y="-27.724512658626" />
                  <Point X="2.123369548187" Y="-27.24205219035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.225885534793" Y="-27.143053652846" />
                  <Point X="2.862643726085" Y="-26.528143415249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.495515319771" Y="-25.916986421332" />
                  <Point X="3.889242300426" Y="-25.536768695775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.66357460141" Y="-28.519694242826" />
                  <Point X="0.740076027456" Y="-28.445817674437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.540127776578" Y="-27.673216681045" />
                  <Point X="2.866337011525" Y="-26.392511309811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.364335132107" Y="-25.91160011489" />
                  <Point X="3.782188458588" Y="-25.508083847991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.626072968086" Y="-28.423843608017" />
                  <Point X="0.725075800653" Y="-28.328237683933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.430770258396" Y="-27.646756467647" />
                  <Point X="2.88956122422" Y="-26.238018407161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.18927930741" Y="-25.948584018618" />
                  <Point X="3.67613913088" Y="-25.478428952188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.572220478442" Y="-28.343782811615" />
                  <Point X="0.745707502811" Y="-28.176248339608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.282237919875" Y="-27.658126938507" />
                  <Point X="3.586828859297" Y="-25.432609337785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770345829687" Y="-24.289700284685" />
                  <Point X="4.782593827783" Y="-24.27787253041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.029300964452" Y="-29.758288550484" />
                  <Point X="-0.967030579081" Y="-29.698154738329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.517341924049" Y="-28.264712874424" />
                  <Point X="0.782512167055" Y="-28.00864094734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.123140042781" Y="-27.679700431365" />
                  <Point X="3.505313580881" Y="-25.379262185979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.581068979847" Y="-24.340417272759" />
                  <Point X="4.764719361334" Y="-24.163068160869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.138792261114" Y="-29.731957525463" />
                  <Point X="-0.91929073549" Y="-29.519987366115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.462463369656" Y="-28.185642937232" />
                  <Point X="3.442353809334" Y="-25.307996189481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.391792130008" Y="-24.391134260834" />
                  <Point X="4.744180699095" Y="-24.050836575296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121978026928" Y="-29.583654667107" />
                  <Point X="-0.871550891899" Y="-29.341819993901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.395236767181" Y="-28.118497371464" />
                  <Point X="3.389187094833" Y="-25.22727314772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.202515280169" Y="-24.441851248908" />
                  <Point X="4.718149520846" Y="-23.943909050779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.130808242314" Y="-29.460116365837" />
                  <Point X="-0.823811048307" Y="-29.163652621688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.302333057075" Y="-28.076147900305" />
                  <Point X="3.360733983417" Y="-25.122684456877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.013238430329" Y="-24.492568236982" />
                  <Point X="4.628146130977" Y="-23.898758772924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.152844806787" Y="-29.349331287638" />
                  <Point X="-0.776071204716" Y="-28.985485249474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.198837561622" Y="-28.04402679736" />
                  <Point X="3.350134188941" Y="-25.000855018271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.82396158049" Y="-24.543285225056" />
                  <Point X="4.469800958134" Y="-23.919605387737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.174881371261" Y="-29.238546209439" />
                  <Point X="-0.728331361125" Y="-28.80731787726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.095342066169" Y="-28.011905694415" />
                  <Point X="3.392606822913" Y="-24.827774131261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613301815811" Y="-24.614651453963" />
                  <Point X="4.311455785291" Y="-23.940452002549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.2046740507" Y="-29.135251124398" />
                  <Point X="-0.680591517534" Y="-28.629150505047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.02814537407" Y="-27.999090588137" />
                  <Point X="4.153110612448" Y="-23.961298617362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.262559214149" Y="-29.059084635822" />
                  <Point X="-0.624950431875" Y="-28.443352992061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.223233107004" Y="-28.055419080788" />
                  <Point X="3.994765439606" Y="-23.982145232175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.333869365004" Y="-28.995882506886" />
                  <Point X="3.836420266763" Y="-24.002991846987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.405540291077" Y="-28.933028774528" />
                  <Point X="3.67807509392" Y="-24.0238384618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.477211217151" Y="-28.870175042171" />
                  <Point X="3.519729921078" Y="-24.044685076612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.558499754843" Y="-28.816608929394" />
                  <Point X="3.387296641614" Y="-24.040508866854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.671723485719" Y="-28.793882274196" />
                  <Point X="3.277027710382" Y="-24.014928794808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.799789295229" Y="-28.78548844773" />
                  <Point X="3.189320870888" Y="-23.967560764035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.927855104739" Y="-28.777094621264" />
                  <Point X="3.117243774401" Y="-23.905099265887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.125513890335" Y="-28.83590595061" />
                  <Point X="3.062878106119" Y="-23.825534040335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.562455375039" Y="-29.125789896489" />
                  <Point X="3.022286671734" Y="-23.732667191727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.645784869614" Y="-29.074194712863" />
                  <Point X="3.001740375407" Y="-23.620442978307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.729114364189" Y="-29.022599529238" />
                  <Point X="3.028751273351" Y="-23.462293316219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.510016809405" Y="-22.99754059035" />
                  <Point X="4.089390683922" Y="-22.438045743312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.806810069747" Y="-28.9655638588" />
                  <Point X="4.038391050667" Y="-22.35522997552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.882900130309" Y="-28.906977635013" />
                  <Point X="3.98232328432" Y="-22.277308446962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.958990190871" Y="-28.848391411225" />
                  <Point X="3.854591484882" Y="-22.26859207072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.858070558328" Y="-28.618868913774" />
                  <Point X="3.51451412432" Y="-22.464935419234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.685743275984" Y="-28.320388850475" />
                  <Point X="3.174436763758" Y="-22.661278767748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.51341599364" Y="-28.021908787175" />
                  <Point X="2.834359403196" Y="-22.857622116262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.341223141017" Y="-27.723558541149" />
                  <Point X="2.545027917722" Y="-23.004960742835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313771345619" Y="-27.564983109338" />
                  <Point X="2.390579898068" Y="-23.02204392056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.352466741371" Y="-27.470285277506" />
                  <Point X="2.277717108183" Y="-22.998968708699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.417367167739" Y="-27.400893349583" />
                  <Point X="2.19233401888" Y="-22.94935665845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.507563599296" Y="-27.355929489919" />
                  <Point X="2.119421414877" Y="-22.887702000531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.653988403141" Y="-27.365264738199" />
                  <Point X="2.063812212855" Y="-22.809337641554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.980641094242" Y="-27.548644034109" />
                  <Point X="2.02477573981" Y="-22.714969184234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.320719919145" Y="-27.74498879672" />
                  <Point X="2.012920079826" Y="-22.594352520853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.660798744047" Y="-27.941333559331" />
                  <Point X="2.056147822006" Y="-22.420542434322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.828487514648" Y="-27.971203181616" />
                  <Point X="2.224202981595" Y="-22.126187912013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.886423951888" Y="-27.895086207565" />
                  <Point X="2.396530691727" Y="-21.827707435603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.944360389129" Y="-27.818969233513" />
                  <Point X="2.568858401859" Y="-21.529226959193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.002296826369" Y="-27.742852259462" />
                  <Point X="2.741186111992" Y="-21.230746482784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.052510014742" Y="-27.659277030675" />
                  <Point X="2.774024929306" Y="-21.066968864379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.102486806397" Y="-27.57547351623" />
                  <Point X="-3.131798418827" Y="-26.638090636518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.985109240453" Y="-26.496434543577" />
                  <Point X="2.695265805043" Y="-21.010960125447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.152463598051" Y="-27.491670001784" />
                  <Point X="-3.797581508927" Y="-27.148964351937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950144730425" Y="-26.330604167579" />
                  <Point X="2.615433383953" Y="-20.955987857212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.983006666031" Y="-26.230273028766" />
                  <Point X="2.530135826895" Y="-20.906293209436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.047144088431" Y="-26.160144276476" />
                  <Point X="2.444838269836" Y="-20.856598561659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.13519767122" Y="-26.113111091811" />
                  <Point X="2.359540712778" Y="-20.806903913882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.260832970104" Y="-26.102370148516" />
                  <Point X="2.274243155719" Y="-20.757209266106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.419177806495" Y="-26.12321643842" />
                  <Point X="2.184014741579" Y="-20.712276291663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.577522642885" Y="-26.144062728324" />
                  <Point X="2.091877591738" Y="-20.669186561861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.735867479275" Y="-26.164909018228" />
                  <Point X="1.999740441897" Y="-20.626096832058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.894212315665" Y="-26.185755308132" />
                  <Point X="1.907603292055" Y="-20.583007102256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.052557152055" Y="-26.206601598036" />
                  <Point X="1.812083901774" Y="-20.543183564081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.210901988446" Y="-26.22744788794" />
                  <Point X="1.712666792124" Y="-20.507124009747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.369246824836" Y="-26.248294177845" />
                  <Point X="1.613249682474" Y="-20.471064455413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.527591661226" Y="-26.269140467749" />
                  <Point X="-3.547505586506" Y="-25.322682347047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300332998575" Y="-25.083990553442" />
                  <Point X="1.513832572824" Y="-20.435004901079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665195319048" Y="-26.269957234333" />
                  <Point X="-3.736782224112" Y="-25.373399130169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.305259341601" Y="-24.956682326456" />
                  <Point X="1.410478159907" Y="-20.402747556313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692254296603" Y="-26.16402224407" />
                  <Point X="-3.926058778772" Y="-25.424115833192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.347994638364" Y="-24.865885781682" />
                  <Point X="1.301072843618" Y="-20.376333501011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.719313274158" Y="-26.058087253806" />
                  <Point X="-4.115335333432" Y="-25.474832536215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.414639049737" Y="-24.798178000502" />
                  <Point X="1.191667527329" Y="-20.34991944571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744202354216" Y="-25.950056817886" />
                  <Point X="-4.304611888092" Y="-25.525549239238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.503566163071" Y="-24.751988374478" />
                  <Point X="1.082262211039" Y="-20.323505390408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760798573981" Y="-25.83401805987" />
                  <Point X="-4.493888442752" Y="-25.57626594226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.610347166183" Y="-24.723040049399" />
                  <Point X="0.97285689475" Y="-20.297091335106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.777394793745" Y="-25.717979301855" />
                  <Point X="-4.683164997412" Y="-25.626982645283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.717400929941" Y="-24.694355126214" />
                  <Point X="0.863451578461" Y="-20.270677279804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.824454693699" Y="-24.66567020303" />
                  <Point X="0.069530214046" Y="-20.905292688352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.235599116387" Y="-20.744921813517" />
                  <Point X="0.744618618646" Y="-20.253367394027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931508457457" Y="-24.636985279845" />
                  <Point X="-0.064624396698" Y="-20.90277874889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.28414496763" Y="-20.565976088762" />
                  <Point X="0.621240915339" Y="-20.240446316027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.038562221215" Y="-24.608300356661" />
                  <Point X="-0.152225267073" Y="-20.855308384928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331884614875" Y="-20.387808906157" />
                  <Point X="0.497863212031" Y="-20.227525238026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.145615984973" Y="-24.579615433476" />
                  <Point X="-0.2090191097" Y="-20.778088020085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.252669748732" Y="-24.550930510292" />
                  <Point X="-0.241881626689" Y="-20.677757442706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.35972351249" Y="-24.522245587107" />
                  <Point X="-0.269994114478" Y="-20.572839815449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.466777276248" Y="-24.493560663923" />
                  <Point X="-3.743948252289" Y="-23.795532789381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421442624457" Y="-23.484092724772" />
                  <Point X="-0.298106602267" Y="-20.467922188192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.573831040006" Y="-24.464875740738" />
                  <Point X="-3.867355379964" Y="-23.782640126161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.444426577519" Y="-23.374222529098" />
                  <Point X="-0.326219090056" Y="-20.363004560935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.680884803764" Y="-24.436190817554" />
                  <Point X="-3.987705811764" Y="-23.766795646047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.491532116016" Y="-23.287646277709" />
                  <Point X="-0.354331577845" Y="-20.258086933679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784914887823" Y="-24.404585960825" />
                  <Point X="-4.108056243564" Y="-23.750951165933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557863481253" Y="-23.21963619139" />
                  <Point X="-0.460553601697" Y="-20.228598808603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.762114427002" Y="-24.250502270604" />
                  <Point X="-4.228406675364" Y="-23.735106685819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.634068950665" Y="-23.161161416633" />
                  <Point X="-0.616171915681" Y="-20.246812126425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.739313966182" Y="-24.096418580382" />
                  <Point X="-4.348757107163" Y="-23.719262205704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710274420076" Y="-23.102686641875" />
                  <Point X="-0.771790229665" Y="-20.265025444247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.699830369899" Y="-23.926224173516" />
                  <Point X="-4.469107538963" Y="-23.70341772559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.786479889487" Y="-23.044211867118" />
                  <Point X="-2.981723182809" Y="-22.267067349028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761390936285" Y="-22.054294971832" />
                  <Point X="-0.927409518409" Y="-20.283239703384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.651358679303" Y="-23.747350064865" />
                  <Point X="-4.589457970763" Y="-23.687573245476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.862685358898" Y="-22.985737092361" />
                  <Point X="-3.123207639183" Y="-22.271631759212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752470285799" Y="-21.913614858647" />
                  <Point X="-1.766209686388" Y="-20.961194068761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.480479775127" Y="-20.68526790083" />
                  <Point X="-1.119219141225" Y="-20.336402561891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.93889082831" Y="-22.927262317603" />
                  <Point X="-3.234377793485" Y="-22.246921988168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.773489702869" Y="-21.801847532618" />
                  <Point X="-1.902027043187" Y="-20.9602858245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465870234947" Y="-20.539094090726" />
                  <Point X="-1.311923458411" Y="-20.390429416707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.015096297721" Y="-22.868787542846" />
                  <Point X="-3.321324801629" Y="-22.198820196789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.820006638018" Y="-21.714702873583" />
                  <Point X="-1.994841069404" Y="-20.917849746615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091301767132" Y="-22.810312768089" />
                  <Point X="-3.406912763875" Y="-22.149405990042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868960965208" Y="-21.629911976682" />
                  <Point X="-2.079062071359" Y="-20.86711548166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167507236543" Y="-22.751837993332" />
                  <Point X="-3.49250072612" Y="-22.099991783295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.917915292397" Y="-21.545121079781" />
                  <Point X="-2.14275749694" Y="-20.796559898003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.190290757933" Y="-22.641774243041" />
                  <Point X="-3.578088688366" Y="-22.050577576547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.966869619587" Y="-21.460330182879" />
                  <Point X="-2.239218085563" Y="-20.757645264501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.013627277018" Y="-22.339106761457" />
                  <Point X="-3.663676650612" Y="-22.0011633698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.015823946777" Y="-21.375539285978" />
                  <Point X="-2.561242614299" Y="-20.936555195968" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998374023438" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.487510314941" Y="-28.596716796875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.457791046143" Y="-28.512138671875" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.289073425293" Y="-28.27382421875" />
                  <Point X="0.264234313965" Y="-28.263263671875" />
                  <Point X="0.020976625443" Y="-28.187765625" />
                  <Point X="0.006155959129" Y="-28.18551953125" />
                  <Point X="-0.018766191483" Y="-28.190900390625" />
                  <Point X="-0.262023895264" Y="-28.2663984375" />
                  <Point X="-0.276761383057" Y="-28.27382421875" />
                  <Point X="-0.294806915283" Y="-28.295048828125" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.843837219238" Y="-29.97249609375" />
                  <Point X="-0.847744384766" Y="-29.987078125" />
                  <Point X="-1.100230712891" Y="-29.938068359375" />
                  <Point X="-1.110576660156" Y="-29.93540625" />
                  <Point X="-1.351589477539" Y="-29.873396484375" />
                  <Point X="-1.311072998047" Y="-29.565642578125" />
                  <Point X="-1.309150390625" Y="-29.551041015625" />
                  <Point X="-1.312096435547" Y="-29.522626953125" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.375948730469" Y="-29.215220703125" />
                  <Point X="-1.395582885742" Y="-29.19447265625" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.633376708984" Y="-28.9894609375" />
                  <Point X="-1.661583984375" Y="-28.984955078125" />
                  <Point X="-1.958829833984" Y="-28.96547265625" />
                  <Point X="-1.975041015625" Y="-28.967068359375" />
                  <Point X="-2.000163696289" Y="-28.980662109375" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.855837646484" Y="-29.167609375" />
                  <Point X="-2.870140380859" Y="-29.156595703125" />
                  <Point X="-3.228580566406" Y="-28.880609375" />
                  <Point X="-2.535713867188" Y="-27.680529296875" />
                  <Point X="-2.506033203125" Y="-27.629119140625" />
                  <Point X="-2.499763427734" Y="-27.597587890625" />
                  <Point X="-2.514677734375" Y="-27.56806640625" />
                  <Point X="-2.531329101562" Y="-27.551416015625" />
                  <Point X="-2.560158203125" Y="-27.53719921875" />
                  <Point X="-2.591684082031" Y="-27.543470703125" />
                  <Point X="-3.829675048828" Y="-28.258224609375" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.161704589844" Y="-27.84712890625" />
                  <Point X="-4.171959472656" Y="-27.82993359375" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.21571875" Y="-26.462994140625" />
                  <Point X="-3.163787109375" Y="-26.423146484375" />
                  <Point X="-3.145514892578" Y="-26.394828125" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140328613281" Y="-26.33458984375" />
                  <Point X="-3.162215087891" Y="-26.310017578125" />
                  <Point X="-3.187641845703" Y="-26.295052734375" />
                  <Point X="-3.219528320312" Y="-26.288572265625" />
                  <Point X="-4.782374511719" Y="-26.49432421875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.927393066406" Y="-26.01119140625" />
                  <Point X="-4.930105957031" Y="-25.992220703125" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-3.616621582031" Y="-25.144498046875" />
                  <Point X="-3.557463623047" Y="-25.128646484375" />
                  <Point X="-3.540789306641" Y="-25.120658203125" />
                  <Point X="-3.514142822266" Y="-25.1021640625" />
                  <Point X="-3.494530273438" Y="-25.074720703125" />
                  <Point X="-3.485648193359" Y="-25.046103515625" />
                  <Point X="-3.486016601562" Y="-25.015271484375" />
                  <Point X="-3.494898925781" Y="-24.98665234375" />
                  <Point X="-3.515249267578" Y="-24.95962890625" />
                  <Point X="-3.541895751953" Y="-24.941134765625" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.982071777344" Y="-24.55219140625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.912182128906" Y="-23.983421875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.795671386719" Y="-23.6004375" />
                  <Point X="-3.753265380859" Y="-23.60601953125" />
                  <Point X="-3.731716552734" Y="-23.60413671875" />
                  <Point X="-3.729255615234" Y="-23.603361328125" />
                  <Point X="-3.670278564453" Y="-23.584765625" />
                  <Point X="-3.651532470703" Y="-23.57394140625" />
                  <Point X="-3.639118164062" Y="-23.556208984375" />
                  <Point X="-3.638137451172" Y="-23.553841796875" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.617501708984" Y="-23.4522109375" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.65996875" Y="-23.380779296875" />
                  <Point X="-4.47610546875" Y="-22.754533203125" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.145544921875" Y="-22.194396484375" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.185201904297" Y="-22.05801953125" />
                  <Point X="-3.159157470703" Y="-22.073056640625" />
                  <Point X="-3.138520751953" Y="-22.079564453125" />
                  <Point X="-3.135103515625" Y="-22.079865234375" />
                  <Point X="-3.052965087891" Y="-22.08705078125" />
                  <Point X="-3.031512451172" Y="-22.0842265625" />
                  <Point X="-3.013261230469" Y="-22.072603515625" />
                  <Point X="-3.010831054688" Y="-22.07017578125" />
                  <Point X="-2.952528564453" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.938372558594" Y="-21.968748046875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067138672" Y="-21.865966796875" />
                  <Point X="-3.307279296875" Y="-21.25072265625" />
                  <Point X="-2.752874267578" Y="-20.82566796875" />
                  <Point X="-2.730087158203" Y="-20.8130078125" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-1.975799682617" Y="-20.70203515625" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247192383" Y="-20.726337890625" />
                  <Point X="-1.947450927734" Y="-20.728314453125" />
                  <Point X="-1.85603112793" Y="-20.77590625" />
                  <Point X="-1.835119995117" Y="-20.7815078125" />
                  <Point X="-1.813800537109" Y="-20.777748046875" />
                  <Point X="-1.809847167969" Y="-20.776107421875" />
                  <Point X="-1.714635009766" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.684796386719" Y="-20.7014296875" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.581572265625" />
                  <Point X="-1.689137207031" Y="-20.298861328125" />
                  <Point X="-0.968083190918" Y="-20.096703125" />
                  <Point X="-0.940469909668" Y="-20.093470703125" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.0498450737" Y="-20.66034375" />
                  <Point X="-0.049844921112" Y="-20.66034375" />
                  <Point X="-0.042140228271" Y="-20.68909765625" />
                  <Point X="-0.024282043457" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036593975067" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.236648239136" Y="-20.0091328125" />
                  <Point X="0.860210388184" Y="-20.074435546875" />
                  <Point X="0.883062438965" Y="-20.079953125" />
                  <Point X="1.508456542969" Y="-20.230943359375" />
                  <Point X="1.522881103516" Y="-20.236173828125" />
                  <Point X="1.931044433594" Y="-20.38421875" />
                  <Point X="1.945421508789" Y="-20.39094140625" />
                  <Point X="2.338687011719" Y="-20.574859375" />
                  <Point X="2.352606445312" Y="-20.58296875" />
                  <Point X="2.732520751953" Y="-20.804306640625" />
                  <Point X="2.745633056641" Y="-20.8136328125" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.264206298828" Y="-22.436900390625" />
                  <Point X="2.229853271484" Y="-22.49640234375" />
                  <Point X="2.22399609375" Y="-22.5116875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202378417969" Y="-22.610443359375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218676757812" Y="-22.6991796875" />
                  <Point X="2.220395263672" Y="-22.701712890625" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.277463378906" Y="-22.7775078125" />
                  <Point X="2.338248046875" Y="-22.81875390625" />
                  <Point X="2.360367919922" Y="-22.827025390625" />
                  <Point X="2.363135986328" Y="-22.827357421875" />
                  <Point X="2.429760742188" Y="-22.835392578125" />
                  <Point X="2.451865234375" Y="-22.833197265625" />
                  <Point X="2.528950439453" Y="-22.812583984375" />
                  <Point X="2.541034179688" Y="-22.80758203125" />
                  <Point X="3.973916015625" Y="-21.98030859375" />
                  <Point X="3.994248046875" Y="-21.9685703125" />
                  <Point X="4.202590332031" Y="-22.258119140625" />
                  <Point X="4.209900390625" Y="-22.27019921875" />
                  <Point X="4.387513183594" Y="-22.563705078125" />
                  <Point X="3.333844238281" Y="-23.372212890625" />
                  <Point X="3.288615234375" Y="-23.406919921875" />
                  <Point X="3.277067382812" Y="-23.419173828125" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.212261230469" Y="-23.511568359375" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.191483886719" Y="-23.61244921875" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.2175625" Y="-23.714958984375" />
                  <Point X="3.263704101562" Y="-23.785091796875" />
                  <Point X="3.280953613281" Y="-23.80118359375" />
                  <Point X="3.283730224609" Y="-23.80274609375" />
                  <Point X="3.350589355469" Y="-23.8403828125" />
                  <Point X="3.3723203125" Y="-23.846876953125" />
                  <Point X="3.462726806641" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.8369453125" Y="-23.679630859375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.941491699219" Y="-24.063408203125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="3.793001220703" Y="-24.74828515625" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.725398681641" Y="-24.7693125" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.620052001953" Y="-24.835892578125" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.556247314453" Y="-24.9291171875" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.539220947266" Y="-25.044537109375" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.568971923828" Y="-25.161580078125" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.640264892578" Y="-25.2440390625" />
                  <Point X="3.729086425781" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.989396484375" Y="-25.634849609375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.945480957031" Y="-25.979330078125" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.472094482422" Y="-26.10554296875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.387596923828" Y="-26.0999140625" />
                  <Point X="3.213271728516" Y="-26.1378046875" />
                  <Point X="3.197942626953" Y="-26.143923828125" />
                  <Point X="3.18106640625" Y="-26.15996484375" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.067953613281" Y="-26.299517578125" />
                  <Point X="3.063730712891" Y="-26.320884765625" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.049849121094" Y="-26.501458984375" />
                  <Point X="3.060366699219" Y="-26.522853515625" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460449219" Y="-26.685541015625" />
                  <Point X="4.326824707031" Y="-27.574384765625" />
                  <Point X="4.339073730469" Y="-27.583783203125" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.198027832031" Y="-27.810814453125" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="2.806992675781" Y="-27.290126953125" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.728725097656" Y="-27.2517578125" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.504752685547" Y="-27.214076171875" />
                  <Point X="2.481920166016" Y="-27.22301171875" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.297490722656" Y="-27.322615234375" />
                  <Point X="2.284833007812" Y="-27.341841796875" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.188950439453" Y="-27.529873046875" />
                  <Point X="2.190718994141" Y="-27.554990234375" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091796875" Y="-27.778580078125" />
                  <Point X="2.978457275391" Y="-29.067857421875" />
                  <Point X="2.986673828125" Y="-29.082087890625" />
                  <Point X="2.835296630859" Y="-29.190212890625" />
                  <Point X="2.8284765625" Y="-29.19462890625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.724031982422" Y="-28.045330078125" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.662051879883" Y="-27.97500390625" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.442266357422" Y="-27.836939453125" />
                  <Point X="1.41651159668" Y="-27.836572265625" />
                  <Point X="1.192718017578" Y="-27.857166015625" />
                  <Point X="1.178165405273" Y="-27.86076171875" />
                  <Point X="1.158156738281" Y="-27.8744765625" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.974574951172" Y="-28.030662109375" />
                  <Point X="0.966311279297" Y="-28.055859375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.124876953125" Y="-29.9130234375" />
                  <Point X="1.127642333984" Y="-29.934029296875" />
                  <Point X="0.994367492676" Y="-29.9632421875" />
                  <Point X="0.988039123535" Y="-29.964392578125" />
                  <Point X="0.860200378418" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#120" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.007704675999" Y="4.383878717325" Z="0.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.1" />
                  <Point X="-0.952438035427" Y="4.987470524619" Z="0.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.1" />
                  <Point X="-1.719960207573" Y="4.777433358763" Z="0.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.1" />
                  <Point X="-1.74881345865" Y="4.755879581922" Z="0.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.1" />
                  <Point X="-1.738638169725" Y="4.34488585766" Z="0.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.1" />
                  <Point X="-1.835144978169" Y="4.301362422626" Z="0.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.1" />
                  <Point X="-1.930518928965" Y="4.34731509453" Z="0.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.1" />
                  <Point X="-1.942288205271" Y="4.359681946167" Z="0.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.1" />
                  <Point X="-2.760526061943" Y="4.261980126942" Z="0.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.1" />
                  <Point X="-3.355060661319" Y="3.811646893241" Z="0.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.1" />
                  <Point X="-3.363632477803" Y="3.767501965628" Z="0.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.1" />
                  <Point X="-2.994338426924" Y="3.05817441508" Z="0.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.1" />
                  <Point X="-3.052342503043" Y="2.996461064843" Z="0.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.1" />
                  <Point X="-3.136902050527" Y="3.001226272249" Z="0.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.1" />
                  <Point X="-3.166357382669" Y="3.016561470943" Z="0.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.1" />
                  <Point X="-4.191164100168" Y="2.867587760212" Z="0.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.1" />
                  <Point X="-4.534099174379" Y="2.287122726264" Z="0.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.1" />
                  <Point X="-4.513721051871" Y="2.237862022633" Z="0.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.1" />
                  <Point X="-3.668008163918" Y="1.555982148284" Z="0.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.1" />
                  <Point X="-3.690487211005" Y="1.496572550423" Z="0.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.1" />
                  <Point X="-3.7504470045" Y="1.475604721567" Z="0.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.1" />
                  <Point X="-3.795301879876" Y="1.480415367196" Z="0.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.1" />
                  <Point X="-4.966597688451" Y="1.060936531753" Z="0.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.1" />
                  <Point X="-5.056838569419" Y="0.470217864375" Z="0.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.1" />
                  <Point X="-5.001169172287" Y="0.430791718058" Z="0.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.1" />
                  <Point X="-3.5499143688" Y="0.030574931759" Z="0.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.1" />
                  <Point X="-3.539925584511" Y="0.001188405503" Z="0.1" />
                  <Point X="-3.539556741714" Y="0" Z="0.1" />
                  <Point X="-3.548439016624" Y="-0.028618545557" Z="0.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.1" />
                  <Point X="-3.575454226323" Y="-0.048301051375" Z="0.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.1" />
                  <Point X="-3.635718616485" Y="-0.064920338698" Z="0.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.1" />
                  <Point X="-4.985758246918" Y="-0.968020252081" Z="0.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.1" />
                  <Point X="-4.852316022304" Y="-1.49996937576" Z="0.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.1" />
                  <Point X="-4.782005024322" Y="-1.512615865694" Z="0.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.1" />
                  <Point X="-3.19373216842" Y="-1.321828368544" Z="0.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.1" />
                  <Point X="-3.200073623273" Y="-1.351011140379" Z="0.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.1" />
                  <Point X="-3.252312369843" Y="-1.392045661991" Z="0.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.1" />
                  <Point X="-4.221058568734" Y="-2.824261786951" Z="0.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.1" />
                  <Point X="-3.87607842617" Y="-3.281743733367" Z="0.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.1" />
                  <Point X="-3.810830428184" Y="-3.270245357181" Z="0.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.1" />
                  <Point X="-2.556182657796" Y="-2.572147811336" Z="0.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.1" />
                  <Point X="-2.585171655987" Y="-2.624247954308" Z="0.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.1" />
                  <Point X="-2.906800336704" Y="-4.164932583666" Z="0.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.1" />
                  <Point X="-2.468634982632" Y="-4.43869533576" Z="0.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.1" />
                  <Point X="-2.442151153462" Y="-4.437856071534" Z="0.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.1" />
                  <Point X="-1.978541132752" Y="-3.990956686059" Z="0.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.1" />
                  <Point X="-1.67101019301" Y="-4.003566908221" Z="0.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.1" />
                  <Point X="-1.434706108548" Y="-4.200784500292" Z="0.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.1" />
                  <Point X="-1.36729176926" Y="-4.501100095918" Z="0.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.1" />
                  <Point X="-1.366801091039" Y="-4.527835485724" Z="0.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.1" />
                  <Point X="-1.129191309612" Y="-4.952550243903" Z="0.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.1" />
                  <Point X="-0.829618489039" Y="-5.011443334461" Z="0.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.1" />
                  <Point X="-0.80169687211" Y="-4.954157586102" Z="0.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.1" />
                  <Point X="-0.259886915777" Y="-3.292278044594" Z="0.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.1" />
                  <Point X="-0.010101446594" Y="-3.207374484683" Z="0.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.1" />
                  <Point X="0.243257632767" Y="-3.279737560605" Z="0.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.1" />
                  <Point X="0.410558943655" Y="-3.509367711174" Z="0.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.1" />
                  <Point X="0.433057988291" Y="-3.578378443714" Z="0.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.1" />
                  <Point X="0.990819905574" Y="-4.982307583652" Z="0.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.1" />
                  <Point X="1.169914523417" Y="-4.943320039215" Z="0.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.1" />
                  <Point X="1.168293231566" Y="-4.8752184478" Z="0.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.1" />
                  <Point X="1.009014438347" Y="-3.035197354041" Z="0.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.1" />
                  <Point X="1.183967785348" Y="-2.881642020081" Z="0.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.1" />
                  <Point X="1.414937270924" Y="-2.855081756579" Z="0.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.1" />
                  <Point X="1.628856648605" Y="-2.985782324111" Z="0.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.1" />
                  <Point X="1.678208479277" Y="-3.044487987009" Z="0.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.1" />
                  <Point X="2.849489866345" Y="-4.205322251188" Z="0.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.1" />
                  <Point X="3.038967628816" Y="-4.070504174894" Z="0.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.1" />
                  <Point X="3.015602299808" Y="-4.01157678046" Z="0.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.1" />
                  <Point X="2.233767074062" Y="-2.514823724397" Z="0.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.1" />
                  <Point X="2.322923308416" Y="-2.33384759327" Z="0.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.1" />
                  <Point X="2.499050785102" Y="-2.235978001551" Z="0.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.1" />
                  <Point X="2.713683079116" Y="-2.269680935429" Z="0.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.1" />
                  <Point X="2.775836816647" Y="-2.302147200466" Z="0.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.1" />
                  <Point X="4.23276064591" Y="-2.808311197171" Z="0.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.1" />
                  <Point X="4.392849814499" Y="-2.550636246012" Z="0.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.1" />
                  <Point X="4.351106661673" Y="-2.503436982168" Z="0.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.1" />
                  <Point X="3.096268664738" Y="-1.46453381374" Z="0.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.1" />
                  <Point X="3.107364342481" Y="-1.294187234764" Z="0.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.1" />
                  <Point X="3.213360024091" Y="-1.160646556897" Z="0.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.1" />
                  <Point X="3.392060875718" Y="-1.117493847642" Z="0.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.1" />
                  <Point X="3.459412252937" Y="-1.12383437053" Z="0.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.1" />
                  <Point X="4.98807209231" Y="-0.959174352167" Z="0.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.1" />
                  <Point X="5.045759398979" Y="-0.584123001091" Z="0.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.1" />
                  <Point X="4.996181525904" Y="-0.555272527481" Z="0.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.1" />
                  <Point X="3.659131095131" Y="-0.169470157371" Z="0.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.1" />
                  <Point X="3.602149751973" Y="-0.09943044927" Z="0.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.1" />
                  <Point X="3.582172402803" Y="-0.003851869581" Z="0.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.1" />
                  <Point X="3.599199047621" Y="0.092758661666" Z="0.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.1" />
                  <Point X="3.653229686427" Y="0.164518289305" Z="0.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.1" />
                  <Point X="3.744264319222" Y="0.218678686871" Z="0.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.1" />
                  <Point X="3.799786291402" Y="0.234699405288" Z="0.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.1" />
                  <Point X="4.984740411784" Y="0.975564402632" Z="0.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.1" />
                  <Point X="4.884825397486" Y="1.39206832667" Z="0.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.1" />
                  <Point X="4.82426308706" Y="1.401221839101" Z="0.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.1" />
                  <Point X="3.372714817348" Y="1.233972416243" Z="0.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.1" />
                  <Point X="3.302095186752" Y="1.272108026139" Z="0.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.1" />
                  <Point X="3.253177054441" Y="1.343804079146" Z="0.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.1" />
                  <Point X="3.234296496322" Y="1.428935018446" Z="0.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.1" />
                  <Point X="3.254257824177" Y="1.50624512196" Z="0.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.1" />
                  <Point X="3.310594379199" Y="1.581689638458" Z="0.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.1" />
                  <Point X="3.358127324005" Y="1.619400658521" Z="0.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.1" />
                  <Point X="4.246521921302" Y="2.786968850875" Z="0.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.1" />
                  <Point X="4.011667423091" Y="3.115554015673" Z="0.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.1" />
                  <Point X="3.942759723567" Y="3.094273415583" Z="0.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.1" />
                  <Point X="2.432792675463" Y="2.246385169183" Z="0.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.1" />
                  <Point X="2.362934763081" Y="2.25356676144" Z="0.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.1" />
                  <Point X="2.299382195803" Y="2.295145351581" Z="0.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.1" />
                  <Point X="2.255613248654" Y="2.357642664579" Z="0.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.1" />
                  <Point X="2.245862941319" Y="2.426823680751" Z="0.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.1" />
                  <Point X="2.266142773475" Y="2.506676900419" Z="0.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.1" />
                  <Point X="2.301351937277" Y="2.569379358388" Z="0.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.1" />
                  <Point X="2.768454251838" Y="4.258395826954" Z="0.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.1" />
                  <Point X="2.37162045085" Y="4.491514652061" Z="0.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.1" />
                  <Point X="1.960447005389" Y="4.685629340961" Z="0.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.1" />
                  <Point X="1.533773812352" Y="4.842109845674" Z="0.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.1" />
                  <Point X="0.888640828225" Y="4.99993093137" Z="0.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.1" />
                  <Point X="0.219930167871" Y="5.073528336686" Z="0.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.1" />
                  <Point X="0.185539885554" Y="5.047568788626" Z="0.1" />
                  <Point X="0" Y="4.355124473572" Z="0.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>