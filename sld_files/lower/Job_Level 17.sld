<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#149" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1342" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.689669677734" Y="-28.984134765625" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542362854004" Y="-28.467375" />
                  <Point X="0.490713317871" Y="-28.392958984375" />
                  <Point X="0.378635223389" Y="-28.231474609375" />
                  <Point X="0.356748565674" Y="-28.209017578125" />
                  <Point X="0.330493225098" Y="-28.189775390625" />
                  <Point X="0.302494262695" Y="-28.175669921875" />
                  <Point X="0.222569488525" Y="-28.150865234375" />
                  <Point X="0.049135276794" Y="-28.097037109375" />
                  <Point X="0.020983165741" Y="-28.092767578125" />
                  <Point X="-0.008658161163" Y="-28.092765625" />
                  <Point X="-0.03682484436" Y="-28.09703515625" />
                  <Point X="-0.11674962616" Y="-28.121841796875" />
                  <Point X="-0.290183837891" Y="-28.17566796875" />
                  <Point X="-0.318184143066" Y="-28.189775390625" />
                  <Point X="-0.34443963623" Y="-28.20901953125" />
                  <Point X="-0.366323577881" Y="-28.2314765625" />
                  <Point X="-0.417973297119" Y="-28.30589453125" />
                  <Point X="-0.530051391602" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.825203613281" Y="-29.535904296875" />
                  <Point X="-0.916584594727" Y="-29.87694140625" />
                  <Point X="-1.079322143555" Y="-29.845353515625" />
                  <Point X="-1.168396118164" Y="-29.822435546875" />
                  <Point X="-1.246417602539" Y="-29.802361328125" />
                  <Point X="-1.230174682617" Y="-29.678984375" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.235602783203" Y="-29.420234375" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323645141602" Y="-29.131203125" />
                  <Point X="-1.397229736328" Y="-29.066671875" />
                  <Point X="-1.55690612793" Y="-28.926638671875" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886108398" Y="-28.897994140625" />
                  <Point X="-1.643028198242" Y="-28.890966796875" />
                  <Point X="-1.74069128418" Y="-28.88456640625" />
                  <Point X="-1.9526171875" Y="-28.87067578125" />
                  <Point X="-1.98341418457" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042657348633" Y="-28.89480078125" />
                  <Point X="-2.124035888672" Y="-28.94917578125" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102539062" Y="-29.099462890625" />
                  <Point X="-2.480147460938" Y="-29.28848828125" />
                  <Point X="-2.507229003906" Y="-29.271720703125" />
                  <Point X="-2.801725341797" Y="-29.089376953125" />
                  <Point X="-2.925023681641" Y="-28.99444140625" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.658601318359" Y="-28.083375" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406587890625" Y="-27.616125" />
                  <Point X="-2.405575683594" Y="-27.585189453125" />
                  <Point X="-2.414561523438" Y="-27.5555703125" />
                  <Point X="-2.428779541016" Y="-27.5267421875" />
                  <Point X="-2.446807373047" Y="-27.5015859375" />
                  <Point X="-2.464156005859" Y="-27.48423828125" />
                  <Point X="-2.489312011719" Y="-27.466212890625" />
                  <Point X="-2.518140869141" Y="-27.45199609375" />
                  <Point X="-2.547759277344" Y="-27.44301171875" />
                  <Point X="-2.578693847656" Y="-27.444025390625" />
                  <Point X="-2.610219238281" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.521828125" Y="-27.970794921875" />
                  <Point X="-3.818022705078" Y="-28.141802734375" />
                  <Point X="-3.850204833984" Y="-28.099521484375" />
                  <Point X="-4.082864013672" Y="-27.79385546875" />
                  <Point X="-4.171265625" Y="-27.64562109375" />
                  <Point X="-4.306143066406" Y="-27.419451171875" />
                  <Point X="-3.516848388672" Y="-26.8138046875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083064941406" Y="-26.4758828125" />
                  <Point X="-3.063553466797" Y="-26.444357421875" />
                  <Point X="-3.054409423828" Y="-26.419904296875" />
                  <Point X="-3.051426269531" Y="-26.410451171875" />
                  <Point X="-3.046152099609" Y="-26.390087890625" />
                  <Point X="-3.042037353516" Y="-26.35860546875" />
                  <Point X="-3.042736083984" Y="-26.33573828125" />
                  <Point X="-3.048881835938" Y="-26.313701171875" />
                  <Point X="-3.060884765625" Y="-26.284720703125" />
                  <Point X="-3.073869873047" Y="-26.262486328125" />
                  <Point X="-3.092348388672" Y="-26.244556640625" />
                  <Point X="-3.113209960938" Y="-26.229205078125" />
                  <Point X="-3.121335205078" Y="-26.223845703125" />
                  <Point X="-3.139463623047" Y="-26.213177734375" />
                  <Point X="-3.168722900391" Y="-26.20195703125" />
                  <Point X="-3.200607666016" Y="-26.1954765625" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-4.346184570313" Y="-26.341078125" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.743083984375" Y="-26.348890625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.857467285156" Y="-25.8291171875" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.000945800781" Y="-25.345828125" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.511893798828" Y="-25.21200390625" />
                  <Point X="-3.487571044922" Y="-25.19870703125" />
                  <Point X="-3.478974365234" Y="-25.19339453125" />
                  <Point X="-3.459976318359" Y="-25.180208984375" />
                  <Point X="-3.436020507812" Y="-25.158681640625" />
                  <Point X="-3.415003173828" Y="-25.1280625" />
                  <Point X="-3.404659423828" Y="-25.103921875" />
                  <Point X="-3.401251464844" Y="-25.09466796875" />
                  <Point X="-3.394918701172" Y="-25.074265625" />
                  <Point X="-3.389474609375" Y="-25.0455234375" />
                  <Point X="-3.390489501953" Y="-25.011470703125" />
                  <Point X="-3.395654785156" Y="-24.98724609375" />
                  <Point X="-3.397835449219" Y="-24.978896484375" />
                  <Point X="-3.404168212891" Y="-24.9584921875" />
                  <Point X="-3.417484130859" Y="-24.929169921875" />
                  <Point X="-3.440581542969" Y="-24.89971484375" />
                  <Point X="-3.461155029297" Y="-24.88212109375" />
                  <Point X="-3.468731201172" Y="-24.876275390625" />
                  <Point X="-3.487729248047" Y="-24.86308984375" />
                  <Point X="-3.501922851562" Y="-24.854953125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.548572265625" Y="-24.56999609375" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.883131347656" Y="-24.41933203125" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.777404785156" Y="-23.849275390625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.101189941406" Y="-23.65603515625" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744984863281" Y="-23.700658203125" />
                  <Point X="-3.723423583984" Y="-23.698771484375" />
                  <Point X="-3.703139160156" Y="-23.694736328125" />
                  <Point X="-3.68376171875" Y="-23.688626953125" />
                  <Point X="-3.641713134766" Y="-23.675369140625" />
                  <Point X="-3.622784179688" Y="-23.667041015625" />
                  <Point X="-3.604040039063" Y="-23.656220703125" />
                  <Point X="-3.587355224609" Y="-23.64398828125" />
                  <Point X="-3.573714355469" Y="-23.62843359375" />
                  <Point X="-3.561299804688" Y="-23.610703125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.543576171875" Y="-23.573798828125" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.53205078125" Y="-23.41062109375" />
                  <Point X="-3.541432617188" Y="-23.392599609375" />
                  <Point X="-3.561790527344" Y="-23.3534921875" />
                  <Point X="-3.573281982422" Y="-23.33629296875" />
                  <Point X="-3.587194580078" Y="-23.319712890625" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.184741699219" Y="-22.858359375" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.30901953125" Y="-22.65673046875" />
                  <Point X="-4.081156738281" Y="-22.26634765625" />
                  <Point X="-3.956433837891" Y="-22.10603125" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.412726318359" Y="-22.036353515625" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187728759766" Y="-22.163658203125" />
                  <Point X="-3.167086181641" Y="-22.17016796875" />
                  <Point X="-3.146794921875" Y="-22.174205078125" />
                  <Point X="-3.119807373047" Y="-22.17656640625" />
                  <Point X="-3.061245605469" Y="-22.181689453125" />
                  <Point X="-3.040560302734" Y="-22.18123828125" />
                  <Point X="-3.019102050781" Y="-22.178412109375" />
                  <Point X="-2.999012695312" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946077392578" Y="-22.13976953125" />
                  <Point X="-2.926921386719" Y="-22.12061328125" />
                  <Point X="-2.885353759766" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.845796875" Y="-21.936892578125" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.127965332031" Y="-21.371302734375" />
                  <Point X="-3.183333251953" Y="-21.275404296875" />
                  <Point X="-3.097492431641" Y="-21.20958984375" />
                  <Point X="-2.700629150391" Y="-20.905318359375" />
                  <Point X="-2.504198242188" Y="-20.796185546875" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.106279541016" Y="-20.688044921875" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028892456055" Y="-20.78519921875" />
                  <Point X="-2.012312988281" Y="-20.799111328125" />
                  <Point X="-1.995114868164" Y="-20.810603515625" />
                  <Point X="-1.965077880859" Y="-20.826240234375" />
                  <Point X="-1.899898681641" Y="-20.860171875" />
                  <Point X="-1.880625244141" Y="-20.86766796875" />
                  <Point X="-1.859718017578" Y="-20.873271484375" />
                  <Point X="-1.839268554688" Y="-20.876419921875" />
                  <Point X="-1.818622436523" Y="-20.87506640625" />
                  <Point X="-1.797306762695" Y="-20.871306640625" />
                  <Point X="-1.777453491211" Y="-20.865517578125" />
                  <Point X="-1.74616796875" Y="-20.85255859375" />
                  <Point X="-1.678279296875" Y="-20.8244375" />
                  <Point X="-1.660145141602" Y="-20.814490234375" />
                  <Point X="-1.642415771484" Y="-20.802076171875" />
                  <Point X="-1.626863647461" Y="-20.7884375" />
                  <Point X="-1.614632324219" Y="-20.771755859375" />
                  <Point X="-1.603810546875" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734080078125" />
                  <Point X="-1.585297607422" Y="-20.70178515625" />
                  <Point X="-1.563201171875" Y="-20.631703125" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584201660156" Y="-20.368103515625" />
                  <Point X="-1.463404296875" Y="-20.334236328125" />
                  <Point X="-0.949623596191" Y="-20.19019140625" />
                  <Point X="-0.711511657715" Y="-20.162322265625" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.194864471436" Y="-20.48617578125" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113983154" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425941467" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.278498779297" Y="-20.21999609375" />
                  <Point X="0.307419433594" Y="-20.1120625" />
                  <Point X="0.395440093994" Y="-20.12128125" />
                  <Point X="0.844044555664" Y="-20.16826171875" />
                  <Point X="1.041052001953" Y="-20.215826171875" />
                  <Point X="1.481023803711" Y="-20.322048828125" />
                  <Point X="1.608247314453" Y="-20.368193359375" />
                  <Point X="1.894647216797" Y="-20.472072265625" />
                  <Point X="2.018646484375" Y="-20.5300625" />
                  <Point X="2.294555419922" Y="-20.65909765625" />
                  <Point X="2.414395751953" Y="-20.728916015625" />
                  <Point X="2.680975341797" Y="-20.884224609375" />
                  <Point X="2.793958251953" Y="-20.964572265625" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.419390625" Y="-21.978115234375" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.126303710938" Y="-22.5092734375" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.110368896484" Y="-22.64094921875" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140069580078" Y="-22.75252734375" />
                  <Point X="2.15362109375" Y="-22.7725" />
                  <Point X="2.18302734375" Y="-22.8158359375" />
                  <Point X="2.194462646484" Y="-22.82966796875" />
                  <Point X="2.221594970703" Y="-22.85440625" />
                  <Point X="2.241566162109" Y="-22.867958984375" />
                  <Point X="2.284903564453" Y="-22.897365234375" />
                  <Point X="2.304954101562" Y="-22.90773046875" />
                  <Point X="2.327042724609" Y="-22.91599609375" />
                  <Point X="2.348965332031" Y="-22.921337890625" />
                  <Point X="2.370866210938" Y="-22.923978515625" />
                  <Point X="2.418390136719" Y="-22.929708984375" />
                  <Point X="2.436468994141" Y="-22.93015625" />
                  <Point X="2.473208740234" Y="-22.925830078125" />
                  <Point X="2.498535888672" Y="-22.919056640625" />
                  <Point X="2.553495117188" Y="-22.904359375" />
                  <Point X="2.565288085938" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.61012890625" Y="-22.30003515625" />
                  <Point X="3.967326416016" Y="-22.09380859375" />
                  <Point X="4.123268554688" Y="-22.310533203125" />
                  <Point X="4.186256835938" Y="-22.414623046875" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.588646728516" Y="-23.056951171875" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221422363281" Y="-23.33976171875" />
                  <Point X="3.203974609375" Y="-23.358373046875" />
                  <Point X="3.185746582031" Y="-23.38215234375" />
                  <Point X="3.146192382812" Y="-23.43375390625" />
                  <Point X="3.136606689453" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.11483984375" Y="-23.507193359375" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739257812" Y="-23.628232421875" />
                  <Point X="3.103312988281" Y="-23.65524609375" />
                  <Point X="3.115408203125" Y="-23.713865234375" />
                  <Point X="3.120679443359" Y="-23.731021484375" />
                  <Point X="3.136282714844" Y="-23.76426171875" />
                  <Point X="3.151442871094" Y="-23.7873046875" />
                  <Point X="3.184340332031" Y="-23.837306640625" />
                  <Point X="3.198896972656" Y="-23.8545546875" />
                  <Point X="3.216140380859" Y="-23.870642578125" />
                  <Point X="3.234345703125" Y="-23.88396484375" />
                  <Point X="3.256314941406" Y="-23.89633203125" />
                  <Point X="3.303987792969" Y="-23.92316796875" />
                  <Point X="3.320521728516" Y="-23.930498046875" />
                  <Point X="3.356119140625" Y="-23.9405625" />
                  <Point X="3.385822998047" Y="-23.94448828125" />
                  <Point X="3.450279785156" Y="-23.953005859375" />
                  <Point X="3.462698730469" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.458650390625" Y="-23.82525390625" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.778020996094" Y="-23.788216796875" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.865784667969" Y="-24.194673828125" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.126698242188" Y="-24.560517578125" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704787841797" Y="-24.674416015625" />
                  <Point X="3.681548828125" Y="-24.684931640625" />
                  <Point X="3.652365478516" Y="-24.701798828125" />
                  <Point X="3.589038818359" Y="-24.73840234375" />
                  <Point X="3.574312988281" Y="-24.74890234375" />
                  <Point X="3.547530029297" Y="-24.774423828125" />
                  <Point X="3.530020019531" Y="-24.796736328125" />
                  <Point X="3.492024169922" Y="-24.84515234375" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.457843994141" Y="-24.937873046875" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.451015380859" Y="-25.08903125" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.176662109375" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.492023925781" Y="-25.217408203125" />
                  <Point X="3.509533691406" Y="-25.239720703125" />
                  <Point X="3.547529785156" Y="-25.28813671875" />
                  <Point X="3.559998291016" Y="-25.301236328125" />
                  <Point X="3.589037841797" Y="-25.324158203125" />
                  <Point X="3.618221191406" Y="-25.341025390625" />
                  <Point X="3.681547851562" Y="-25.377630859375" />
                  <Point X="3.692709716797" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.606523925781" Y="-25.630611328125" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.855022460938" Y="-25.948728515625" />
                  <Point X="4.829593261719" Y="-26.060162109375" />
                  <Point X="4.801174316406" Y="-26.18469921875" />
                  <Point X="3.900004150391" Y="-26.06605859375" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374658935547" Y="-26.005509765625" />
                  <Point X="3.317382568359" Y="-26.017958984375" />
                  <Point X="3.193094726563" Y="-26.04497265625" />
                  <Point X="3.163974365234" Y="-26.05659765625" />
                  <Point X="3.136147460938" Y="-26.073490234375" />
                  <Point X="3.112397705078" Y="-26.093958984375" />
                  <Point X="3.077777587891" Y="-26.135595703125" />
                  <Point X="3.002653564453" Y="-26.225947265625" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.964795654297" Y="-26.359287109375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.008148193359" Y="-26.61730078125" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.936501708984" Y="-27.394625" />
                  <Point X="4.213122558594" Y="-27.6068828125" />
                  <Point X="4.124811035156" Y="-27.74978515625" />
                  <Point X="4.072220214844" Y="-27.8245078125" />
                  <Point X="4.028980224609" Y="-27.8859453125" />
                  <Point X="3.224560302734" Y="-27.421513671875" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754223876953" Y="-27.159826171875" />
                  <Point X="2.686055908203" Y="-27.147515625" />
                  <Point X="2.538133544922" Y="-27.12080078125" />
                  <Point X="2.506783935547" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444833251953" Y="-27.135177734375" />
                  <Point X="2.388202392578" Y="-27.164982421875" />
                  <Point X="2.265315185547" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.174727294922" Y="-27.3470703125" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.107986572266" Y="-27.63142578125" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.682525878906" Y="-28.7452890625" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.78184375" Y="-29.111646484375" />
                  <Point X="2.723051757812" Y="-29.149703125" />
                  <Point X="2.701764648438" Y="-29.163482421875" />
                  <Point X="2.081796386719" Y="-28.3555234375" />
                  <Point X="1.758546020508" Y="-27.934255859375" />
                  <Point X="1.747507446289" Y="-27.92218359375" />
                  <Point X="1.721922973633" Y="-27.900556640625" />
                  <Point X="1.654690917969" Y="-27.857333984375" />
                  <Point X="1.508799560547" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.751166015625" />
                  <Point X="1.448365478516" Y="-27.743435546875" />
                  <Point X="1.417100708008" Y="-27.741119140625" />
                  <Point X="1.343570800781" Y="-27.747884765625" />
                  <Point X="1.184013916016" Y="-27.76256640625" />
                  <Point X="1.156362792969" Y="-27.7693984375" />
                  <Point X="1.128977050781" Y="-27.7807421875" />
                  <Point X="1.104595336914" Y="-27.7954609375" />
                  <Point X="1.047817626953" Y="-27.842669921875" />
                  <Point X="0.92461151123" Y="-27.94511328125" />
                  <Point X="0.90414074707" Y="-27.968865234375" />
                  <Point X="0.887248779297" Y="-27.99669140625" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.858648132324" Y="-28.1039140625" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.970140441895" Y="-29.4655078125" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.975680480957" Y="-29.87008203125" />
                  <Point X="0.92931628418" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058415405273" Y="-29.752638671875" />
                  <Point X="-1.141245849609" Y="-29.731326171875" />
                  <Point X="-1.135987426758" Y="-29.691384765625" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.54103515625" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.142428222656" Y="-29.401701171875" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026245117" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230573730469" Y="-29.094861328125" />
                  <Point X="-1.250209106445" Y="-29.070935546875" />
                  <Point X="-1.26100769043" Y="-29.05977734375" />
                  <Point X="-1.334592285156" Y="-28.99524609375" />
                  <Point X="-1.494268554688" Y="-28.855212890625" />
                  <Point X="-1.506739746094" Y="-28.84596484375" />
                  <Point X="-1.533022827148" Y="-28.82962109375" />
                  <Point X="-1.546834472656" Y="-28.822525390625" />
                  <Point X="-1.576531494141" Y="-28.810224609375" />
                  <Point X="-1.591316040039" Y="-28.805474609375" />
                  <Point X="-1.621458251953" Y="-28.798447265625" />
                  <Point X="-1.636815673828" Y="-28.796169921875" />
                  <Point X="-1.734478759766" Y="-28.78976953125" />
                  <Point X="-1.946404541016" Y="-28.77587890625" />
                  <Point X="-1.961928710938" Y="-28.7761328125" />
                  <Point X="-1.992725708008" Y="-28.779166015625" />
                  <Point X="-2.007998779297" Y="-28.7819453125" />
                  <Point X="-2.039047729492" Y="-28.790263671875" />
                  <Point X="-2.053667236328" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436279297" Y="-28.815810546875" />
                  <Point X="-2.176814697266" Y="-28.870185546875" />
                  <Point X="-2.353402832031" Y="-28.988177734375" />
                  <Point X="-2.359683349609" Y="-28.9927578125" />
                  <Point X="-2.380451416016" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410470947266" Y="-29.041630859375" />
                  <Point X="-2.503201171875" Y="-29.162478515625" />
                  <Point X="-2.747596435547" Y="-29.01115625" />
                  <Point X="-2.86706640625" Y="-28.91916796875" />
                  <Point X="-2.980862548828" Y="-28.831548828125" />
                  <Point X="-2.576328857422" Y="-28.130875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413085938" Y="-27.634658203125" />
                  <Point X="-2.311638671875" Y="-27.619232421875" />
                  <Point X="-2.310626464844" Y="-27.588296875" />
                  <Point X="-2.314667236328" Y="-27.557609375" />
                  <Point X="-2.323653076172" Y="-27.527990234375" />
                  <Point X="-2.329360351562" Y="-27.513548828125" />
                  <Point X="-2.343578369141" Y="-27.484720703125" />
                  <Point X="-2.351560791016" Y="-27.471404296875" />
                  <Point X="-2.369588623047" Y="-27.446248046875" />
                  <Point X="-2.379634033203" Y="-27.434408203125" />
                  <Point X="-2.396982666016" Y="-27.417060546875" />
                  <Point X="-2.408822998047" Y="-27.407015625" />
                  <Point X="-2.433979003906" Y="-27.388990234375" />
                  <Point X="-2.447294677734" Y="-27.381009765625" />
                  <Point X="-2.476123535156" Y="-27.36679296875" />
                  <Point X="-2.490564697266" Y="-27.3610859375" />
                  <Point X="-2.520183105469" Y="-27.3521015625" />
                  <Point X="-2.550870605469" Y="-27.3480625" />
                  <Point X="-2.581805175781" Y="-27.349076171875" />
                  <Point X="-2.597229492188" Y="-27.3508515625" />
                  <Point X="-2.628754882813" Y="-27.357123046875" />
                  <Point X="-2.643685546875" Y="-27.36138671875" />
                  <Point X="-2.672649902344" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.569328125" Y="-27.8885234375" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-4.004020019531" Y="-27.740587890625" />
                  <Point X="-4.089673095703" Y="-27.596962890625" />
                  <Point X="-4.181266113281" Y="-27.443375" />
                  <Point X="-3.459016113281" Y="-26.889173828125" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039159423828" Y="-26.566068359375" />
                  <Point X="-3.016269775391" Y="-26.543435546875" />
                  <Point X="-3.002284912109" Y="-26.52587890625" />
                  <Point X="-2.9827734375" Y="-26.494353515625" />
                  <Point X="-2.974571289062" Y="-26.477630859375" />
                  <Point X="-2.965427246094" Y="-26.453177734375" />
                  <Point X="-2.9594609375" Y="-26.434271484375" />
                  <Point X="-2.954186767578" Y="-26.413908203125" />
                  <Point X="-2.951953369141" Y="-26.402400390625" />
                  <Point X="-2.947838623047" Y="-26.37091796875" />
                  <Point X="-2.947081787109" Y="-26.355703125" />
                  <Point X="-2.947780517578" Y="-26.3328359375" />
                  <Point X="-2.951228027344" Y="-26.31021875" />
                  <Point X="-2.957373779297" Y="-26.288181640625" />
                  <Point X="-2.961112060547" Y="-26.277349609375" />
                  <Point X="-2.973114990234" Y="-26.248369140625" />
                  <Point X="-2.978849853516" Y="-26.236810546875" />
                  <Point X="-2.991834960938" Y="-26.214576171875" />
                  <Point X="-3.00771484375" Y="-26.194306640625" />
                  <Point X="-3.026193359375" Y="-26.176376953125" />
                  <Point X="-3.036042236328" Y="-26.168041015625" />
                  <Point X="-3.056903808594" Y="-26.152689453125" />
                  <Point X="-3.073154296875" Y="-26.141970703125" />
                  <Point X="-3.091282714844" Y="-26.131302734375" />
                  <Point X="-3.105447509766" Y="-26.1244765625" />
                  <Point X="-3.134706787109" Y="-26.113255859375" />
                  <Point X="-3.149801269531" Y="-26.108861328125" />
                  <Point X="-3.181686035156" Y="-26.102380859375" />
                  <Point X="-3.197298095703" Y="-26.10053515625" />
                  <Point X="-3.228619628906" Y="-26.099443359375" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-4.358584472656" Y="-26.246890625" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.740762695312" Y="-25.974115234375" />
                  <Point X="-4.763424316406" Y="-25.815666015625" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.976357910156" Y="-25.437591796875" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.497545410156" Y="-25.308595703125" />
                  <Point X="-3.476563232422" Y="-25.300189453125" />
                  <Point X="-3.466323730469" Y="-25.295361328125" />
                  <Point X="-3.442000976562" Y="-25.282064453125" />
                  <Point X="-3.424807617188" Y="-25.271439453125" />
                  <Point X="-3.405809570312" Y="-25.25825390625" />
                  <Point X="-3.396478271484" Y="-25.25087109375" />
                  <Point X="-3.372522460938" Y="-25.22934375" />
                  <Point X="-3.357696777344" Y="-25.212443359375" />
                  <Point X="-3.336679443359" Y="-25.18182421875" />
                  <Point X="-3.327681396484" Y="-25.165478515625" />
                  <Point X="-3.317337646484" Y="-25.141337890625" />
                  <Point X="-3.310521728516" Y="-25.122830078125" />
                  <Point X="-3.304188964844" Y="-25.102427734375" />
                  <Point X="-3.301578369141" Y="-25.0919453125" />
                  <Point X="-3.296134277344" Y="-25.063203125" />
                  <Point X="-3.294516845703" Y="-25.042693359375" />
                  <Point X="-3.295531738281" Y="-25.008640625" />
                  <Point X="-3.297578125" Y="-24.99166015625" />
                  <Point X="-3.302743408203" Y="-24.967435546875" />
                  <Point X="-3.307104736328" Y="-24.950736328125" />
                  <Point X="-3.3134375" Y="-24.93033203125" />
                  <Point X="-3.317669677734" Y="-24.9192109375" />
                  <Point X="-3.330985595703" Y="-24.889888671875" />
                  <Point X="-3.342727294922" Y="-24.870548828125" />
                  <Point X="-3.365824707031" Y="-24.84109375" />
                  <Point X="-3.378838623047" Y="-24.827515625" />
                  <Point X="-3.399412109375" Y="-24.809921875" />
                  <Point X="-3.414564453125" Y="-24.79823046875" />
                  <Point X="-3.4335625" Y="-24.785044921875" />
                  <Point X="-3.440481933594" Y="-24.780671875" />
                  <Point X="-3.465612548828" Y="-24.767166015625" />
                  <Point X="-3.496565673828" Y="-24.75436328125" />
                  <Point X="-3.508288330078" Y="-24.75038671875" />
                  <Point X="-4.523984375" Y="-24.478232421875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.685711914062" Y="-23.874123046875" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.11358984375" Y="-23.75022265625" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747056884766" Y="-23.795634765625" />
                  <Point X="-3.736703613281" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704888427734" Y="-23.7919453125" />
                  <Point X="-3.684604003906" Y="-23.78791015625" />
                  <Point X="-3.674573486328" Y="-23.78533984375" />
                  <Point X="-3.655196044922" Y="-23.77923046875" />
                  <Point X="-3.613147460938" Y="-23.76597265625" />
                  <Point X="-3.603455322266" Y="-23.76232421875" />
                  <Point X="-3.584526367188" Y="-23.75399609375" />
                  <Point X="-3.575289550781" Y="-23.74931640625" />
                  <Point X="-3.556545410156" Y="-23.73849609375" />
                  <Point X="-3.547869873047" Y="-23.7328359375" />
                  <Point X="-3.531185058594" Y="-23.720603515625" />
                  <Point X="-3.515929931641" Y="-23.706625" />
                  <Point X="-3.5022890625" Y="-23.6910703125" />
                  <Point X="-3.495894042969" Y="-23.682921875" />
                  <Point X="-3.483479492188" Y="-23.66519140625" />
                  <Point X="-3.478011230469" Y="-23.6563984375" />
                  <Point X="-3.468062988281" Y="-23.638265625" />
                  <Point X="-3.463583007813" Y="-23.62892578125" />
                  <Point X="-3.455807617188" Y="-23.610154296875" />
                  <Point X="-3.438935546875" Y="-23.569421875" />
                  <Point X="-3.435499267578" Y="-23.559650390625" />
                  <Point X="-3.4297109375" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436012939453" Y="-23.395466796875" />
                  <Point X="-3.443510742188" Y="-23.3761875" />
                  <Point X="-3.447785644531" Y="-23.36675390625" />
                  <Point X="-3.457167480469" Y="-23.348732421875" />
                  <Point X="-3.477525390625" Y="-23.309625" />
                  <Point X="-3.482799560547" Y="-23.30071484375" />
                  <Point X="-3.494291015625" Y="-23.283515625" />
                  <Point X="-3.500508300781" Y="-23.2752265625" />
                  <Point X="-3.514420898438" Y="-23.258646484375" />
                  <Point X="-3.521501708984" Y="-23.251087890625" />
                  <Point X="-3.536442871094" Y="-23.23678515625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-4.126909179688" Y="-22.782990234375" />
                  <Point X="-4.227614257812" Y="-22.705716796875" />
                  <Point X="-4.226973144531" Y="-22.704619140625" />
                  <Point X="-4.002298339844" Y="-22.319697265625" />
                  <Point X="-3.881452636719" Y="-22.164365234375" />
                  <Point X="-3.726336914062" Y="-21.964986328125" />
                  <Point X="-3.460226318359" Y="-22.118625" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244923583984" Y="-22.242279296875" />
                  <Point X="-3.225995361328" Y="-22.250609375" />
                  <Point X="-3.216300537109" Y="-22.254259765625" />
                  <Point X="-3.195657958984" Y="-22.26076953125" />
                  <Point X="-3.185623779297" Y="-22.263341796875" />
                  <Point X="-3.165332519531" Y="-22.26737890625" />
                  <Point X="-3.155075439453" Y="-22.26884375" />
                  <Point X="-3.128087890625" Y="-22.271205078125" />
                  <Point X="-3.069526123047" Y="-22.276328125" />
                  <Point X="-3.059174072266" Y="-22.276666015625" />
                  <Point X="-3.038488769531" Y="-22.27621484375" />
                  <Point X="-3.028155517578" Y="-22.27542578125" />
                  <Point X="-3.006697265625" Y="-22.272599609375" />
                  <Point X="-2.996512695312" Y="-22.2706875" />
                  <Point X="-2.976423339844" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.938449462891" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.902742431641" Y="-22.2268046875" />
                  <Point X="-2.886610107422" Y="-22.21385546875" />
                  <Point X="-2.878901855469" Y="-22.206943359375" />
                  <Point X="-2.859745849609" Y="-22.187787109375" />
                  <Point X="-2.818178222656" Y="-22.146220703125" />
                  <Point X="-2.811268310547" Y="-22.138513671875" />
                  <Point X="-2.798323486328" Y="-22.12238671875" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.751158447266" Y="-21.92861328125" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.045692871094" Y="-21.323802734375" />
                  <Point X="-3.059387695312" Y="-21.30008203125" />
                  <Point X="-3.039689697266" Y="-21.28498046875" />
                  <Point X="-2.648384521484" Y="-20.984970703125" />
                  <Point X="-2.458060791016" Y="-20.87923046875" />
                  <Point X="-2.192523681641" Y="-20.731703125" />
                  <Point X="-2.181648193359" Y="-20.745876953125" />
                  <Point X="-2.118563964844" Y="-20.82808984375" />
                  <Point X="-2.111820800781" Y="-20.835951171875" />
                  <Point X="-2.097517822266" Y="-20.850892578125" />
                  <Point X="-2.089958007813" Y="-20.85797265625" />
                  <Point X="-2.073378662109" Y="-20.871884765625" />
                  <Point X="-2.065094726563" Y="-20.878099609375" />
                  <Point X="-2.047896484375" Y="-20.889591796875" />
                  <Point X="-2.038981933594" Y="-20.894869140625" />
                  <Point X="-2.008944946289" Y="-20.910505859375" />
                  <Point X="-1.94376574707" Y="-20.9444375" />
                  <Point X="-1.934334594727" Y="-20.9487109375" />
                  <Point X="-1.915061157227" Y="-20.95620703125" />
                  <Point X="-1.905218994141" Y="-20.9594296875" />
                  <Point X="-1.884311767578" Y="-20.965033203125" />
                  <Point X="-1.874174072266" Y="-20.967166015625" />
                  <Point X="-1.853724487305" Y="-20.970314453125" />
                  <Point X="-1.833053955078" Y="-20.971216796875" />
                  <Point X="-1.812407836914" Y="-20.96986328125" />
                  <Point X="-1.802120605469" Y="-20.968623046875" />
                  <Point X="-1.780804931641" Y="-20.96486328125" />
                  <Point X="-1.770713012695" Y="-20.9625078125" />
                  <Point X="-1.750859863281" Y="-20.95671875" />
                  <Point X="-1.741098388672" Y="-20.95328515625" />
                  <Point X="-1.709812866211" Y="-20.940326171875" />
                  <Point X="-1.641924072266" Y="-20.912205078125" />
                  <Point X="-1.632590576172" Y="-20.907728515625" />
                  <Point X="-1.614456420898" Y="-20.89778125" />
                  <Point X="-1.605655883789" Y="-20.892310546875" />
                  <Point X="-1.587926513672" Y="-20.879896484375" />
                  <Point X="-1.579778320312" Y="-20.873501953125" />
                  <Point X="-1.564226196289" Y="-20.85986328125" />
                  <Point X="-1.550251098633" Y="-20.844611328125" />
                  <Point X="-1.538019775391" Y="-20.8279296875" />
                  <Point X="-1.532359619141" Y="-20.819255859375" />
                  <Point X="-1.521537841797" Y="-20.80051171875" />
                  <Point X="-1.516855834961" Y="-20.7912734375" />
                  <Point X="-1.508525756836" Y="-20.772341796875" />
                  <Point X="-1.504877563477" Y="-20.7626484375" />
                  <Point X="-1.494694702148" Y="-20.730353515625" />
                  <Point X="-1.472598144531" Y="-20.660271484375" />
                  <Point X="-1.470026489258" Y="-20.650236328125" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-1.437758666992" Y="-20.425708984375" />
                  <Point X="-0.931164001465" Y="-20.2836796875" />
                  <Point X="-0.700468078613" Y="-20.256677734375" />
                  <Point X="-0.365222686768" Y="-20.21744140625" />
                  <Point X="-0.286627441406" Y="-20.510763671875" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.37026171875" Y="-20.244583984375" />
                  <Point X="0.378190460205" Y="-20.214994140625" />
                  <Point X="0.385544464111" Y="-20.215763671875" />
                  <Point X="0.827879577637" Y="-20.262087890625" />
                  <Point X="1.018756347656" Y="-20.308171875" />
                  <Point X="1.453591186523" Y="-20.413154296875" />
                  <Point X="1.575855224609" Y="-20.4575" />
                  <Point X="1.858249755859" Y="-20.55992578125" />
                  <Point X="1.978401733398" Y="-20.6161171875" />
                  <Point X="2.250429443359" Y="-20.743337890625" />
                  <Point X="2.366573242188" Y="-20.811001953125" />
                  <Point X="2.629430664063" Y="-20.964142578125" />
                  <Point X="2.738901611328" Y="-21.0419921875" />
                  <Point X="2.817779052734" Y="-21.098083984375" />
                  <Point X="2.337118164062" Y="-21.930615234375" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301269531" Y="-22.459404296875" />
                  <Point X="2.034528320312" Y="-22.484732421875" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017912231445" Y="-22.5485390625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.016052124023" Y="-22.652322265625" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144287109" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.045318847656" Y="-22.77611328125" />
                  <Point X="2.055679199219" Y="-22.796154296875" />
                  <Point X="2.061456787109" Y="-22.8058671875" />
                  <Point X="2.075008300781" Y="-22.82583984375" />
                  <Point X="2.104414550781" Y="-22.86917578125" />
                  <Point X="2.109809082031" Y="-22.8763671875" />
                  <Point X="2.130456054688" Y="-22.899869140625" />
                  <Point X="2.157588378906" Y="-22.924607421875" />
                  <Point X="2.16825" Y="-22.933015625" />
                  <Point X="2.188221191406" Y="-22.946568359375" />
                  <Point X="2.23155859375" Y="-22.975974609375" />
                  <Point X="2.24127734375" Y="-22.981755859375" />
                  <Point X="2.261327880859" Y="-22.99212109375" />
                  <Point X="2.271659667969" Y="-22.996705078125" />
                  <Point X="2.293748291016" Y="-23.004970703125" />
                  <Point X="2.304552490234" Y="-23.008294921875" />
                  <Point X="2.326475097656" Y="-23.01363671875" />
                  <Point X="2.337593505859" Y="-23.015654296875" />
                  <Point X="2.359494384766" Y="-23.018294921875" />
                  <Point X="2.407018310547" Y="-23.024025390625" />
                  <Point X="2.416040527344" Y="-23.0246796875" />
                  <Point X="2.447578613281" Y="-23.02450390625" />
                  <Point X="2.484318359375" Y="-23.020177734375" />
                  <Point X="2.497752685547" Y="-23.01760546875" />
                  <Point X="2.523079833984" Y="-23.01083203125" />
                  <Point X="2.5780390625" Y="-22.996134765625" />
                  <Point X="2.584010253906" Y="-22.99432421875" />
                  <Point X="2.604412841797" Y="-22.9869296875" />
                  <Point X="2.627658691406" Y="-22.976423828125" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.65762890625" Y="-22.382306640625" />
                  <Point X="3.940403808594" Y="-22.219046875" />
                  <Point X="4.043952148438" Y="-22.36295703125" />
                  <Point X="4.104979492188" Y="-22.463806640625" />
                  <Point X="4.136883789062" Y="-22.516529296875" />
                  <Point X="3.530814453125" Y="-22.98158203125" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168133056641" Y="-23.260134765625" />
                  <Point X="3.152115722656" Y="-23.274787109375" />
                  <Point X="3.13466796875" Y="-23.2933984375" />
                  <Point X="3.128577880859" Y="-23.300578125" />
                  <Point X="3.110349853516" Y="-23.324357421875" />
                  <Point X="3.070795654297" Y="-23.375958984375" />
                  <Point X="3.065637207031" Y="-23.383396484375" />
                  <Point X="3.049740234375" Y="-23.410626953125" />
                  <Point X="3.034763427734" Y="-23.444453125" />
                  <Point X="3.030140380859" Y="-23.457328125" />
                  <Point X="3.023350341797" Y="-23.481607421875" />
                  <Point X="3.008616455078" Y="-23.53429296875" />
                  <Point X="3.006225341797" Y="-23.545337890625" />
                  <Point X="3.002771728516" Y="-23.56763671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077636719" Y="-23.636244140625" />
                  <Point X="3.004698974609" Y="-23.6474296875" />
                  <Point X="3.010272705078" Y="-23.674443359375" />
                  <Point X="3.022367919922" Y="-23.7330625" />
                  <Point X="3.024597900391" Y="-23.741767578125" />
                  <Point X="3.034682617188" Y="-23.771388671875" />
                  <Point X="3.050285888672" Y="-23.80462890625" />
                  <Point X="3.056918701172" Y="-23.8164765625" />
                  <Point X="3.072078857422" Y="-23.83951953125" />
                  <Point X="3.104976318359" Y="-23.889521484375" />
                  <Point X="3.111740234375" Y="-23.898578125" />
                  <Point X="3.126296875" Y="-23.915826171875" />
                  <Point X="3.134089599609" Y="-23.924017578125" />
                  <Point X="3.151333007812" Y="-23.94010546875" />
                  <Point X="3.160038330078" Y="-23.94730859375" />
                  <Point X="3.178243652344" Y="-23.960630859375" />
                  <Point X="3.187743652344" Y="-23.96675" />
                  <Point X="3.209712890625" Y="-23.9791171875" />
                  <Point X="3.257385742188" Y="-24.005953125" />
                  <Point X="3.265485107422" Y="-24.010015625" />
                  <Point X="3.294675537109" Y="-24.0219140625" />
                  <Point X="3.330272949219" Y="-24.031978515625" />
                  <Point X="3.343671875" Y="-24.034744140625" />
                  <Point X="3.373375732422" Y="-24.038669921875" />
                  <Point X="3.437832519531" Y="-24.0471875" />
                  <Point X="3.444033203125" Y="-24.04780078125" />
                  <Point X="3.465708984375" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.471050292969" Y="-23.91944140625" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.771915527344" Y="-24.2092890625" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.102110351562" Y="-24.46875390625" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686022460938" Y="-24.580458984375" />
                  <Point X="3.665623535156" Y="-24.587865234375" />
                  <Point X="3.642384521484" Y="-24.598380859375" />
                  <Point X="3.634010498047" Y="-24.602681640625" />
                  <Point X="3.604827148438" Y="-24.619548828125" />
                  <Point X="3.541500488281" Y="-24.65615234375" />
                  <Point X="3.533885498047" Y="-24.661052734375" />
                  <Point X="3.508777099609" Y="-24.680126953125" />
                  <Point X="3.481994140625" Y="-24.7056484375" />
                  <Point X="3.472795166016" Y="-24.715775390625" />
                  <Point X="3.45528515625" Y="-24.738087890625" />
                  <Point X="3.417289306641" Y="-24.78650390625" />
                  <Point X="3.410854003906" Y="-24.79579296875" />
                  <Point X="3.399130126953" Y="-24.815072265625" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.380004150391" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.364539550781" Y="-24.92000390625" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.99503515625" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280029297" Y="-25.062943359375" />
                  <Point X="3.351874267578" Y="-25.076423828125" />
                  <Point X="3.3577109375" Y="-25.106900390625" />
                  <Point X="3.370376464844" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.18398828125" />
                  <Point X="3.380004638672" Y="-25.205486328125" />
                  <Point X="3.384067382812" Y="-25.216029296875" />
                  <Point X="3.393841064453" Y="-25.237494140625" />
                  <Point X="3.399127685547" Y="-25.247484375" />
                  <Point X="3.4108515625" Y="-25.266765625" />
                  <Point X="3.417288818359" Y="-25.276056640625" />
                  <Point X="3.434798583984" Y="-25.298369140625" />
                  <Point X="3.472794677734" Y="-25.34678515625" />
                  <Point X="3.478717529297" Y="-25.353634765625" />
                  <Point X="3.501138427734" Y="-25.3758046875" />
                  <Point X="3.530177978516" Y="-25.3987265625" />
                  <Point X="3.541499511719" Y="-25.406408203125" />
                  <Point X="3.570682861328" Y="-25.423275390625" />
                  <Point X="3.634009521484" Y="-25.459880859375" />
                  <Point X="3.639497558594" Y="-25.462818359375" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026367188" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.581936035156" Y="-25.722375" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.76161328125" Y="-25.9310546875" />
                  <Point X="4.736974121094" Y="-26.03902734375" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.912404052734" Y="-25.97187109375" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720458984" Y="-25.910841796875" />
                  <Point X="3.354481445312" Y="-25.912677734375" />
                  <Point X="3.297205078125" Y="-25.925126953125" />
                  <Point X="3.172917236328" Y="-25.952140625" />
                  <Point X="3.157873046875" Y="-25.956744140625" />
                  <Point X="3.128752685547" Y="-25.968369140625" />
                  <Point X="3.114676513672" Y="-25.975390625" />
                  <Point X="3.086849609375" Y="-25.992283203125" />
                  <Point X="3.074127197266" Y="-26.001529296875" />
                  <Point X="3.050377441406" Y="-26.021998046875" />
                  <Point X="3.039350097656" Y="-26.033220703125" />
                  <Point X="3.004729980469" Y="-26.074857421875" />
                  <Point X="2.929605957031" Y="-26.165208984375" />
                  <Point X="2.921325927734" Y="-26.17684765625" />
                  <Point X="2.906605224609" Y="-26.20123046875" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.8701953125" Y="-26.35058203125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.92823828125" Y="-26.66867578125" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.878669433594" Y="-27.469994140625" />
                  <Point X="4.087170654297" Y="-27.629982421875" />
                  <Point X="4.045491455078" Y="-27.69742578125" />
                  <Point X="4.001273193359" Y="-27.76025390625" />
                  <Point X="3.272060302734" Y="-27.3392421875" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841191650391" Y="-27.09088671875" />
                  <Point X="2.815025878906" Y="-27.079515625" />
                  <Point X="2.783120361328" Y="-27.069328125" />
                  <Point X="2.771106933594" Y="-27.066337890625" />
                  <Point X="2.702938964844" Y="-27.05402734375" />
                  <Point X="2.555016601562" Y="-27.0273125" />
                  <Point X="2.539358642578" Y="-27.02580859375" />
                  <Point X="2.508009033203" Y="-27.025404296875" />
                  <Point X="2.492317382813" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444847412109" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400588623047" Y="-27.051109375" />
                  <Point X="2.343957763672" Y="-27.0809140625" />
                  <Point X="2.221070556641" Y="-27.145587890625" />
                  <Point X="2.208966552734" Y="-27.153171875" />
                  <Point X="2.186037597656" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144942138672" Y="-27.21116015625" />
                  <Point X="2.128047607422" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.090659179688" Y="-27.302826171875" />
                  <Point X="2.025984741211" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.44019140625" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.014498901367" Y="-27.64830859375" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.600253417969" Y="-28.7927890625" />
                  <Point X="2.735892089844" Y="-29.02772265625" />
                  <Point X="2.723752685547" Y="-29.036083984375" />
                  <Point X="2.157165039062" Y="-28.29769140625" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828655639648" Y="-27.8701484375" />
                  <Point X="1.808836547852" Y="-27.8496328125" />
                  <Point X="1.783252075195" Y="-27.828005859375" />
                  <Point X="1.773296630859" Y="-27.820646484375" />
                  <Point X="1.706064575195" Y="-27.777423828125" />
                  <Point X="1.560173217773" Y="-27.68362890625" />
                  <Point X="1.546284912109" Y="-27.676248046875" />
                  <Point X="1.517471679688" Y="-27.663875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926025391" Y="-27.65115234375" />
                  <Point X="1.455384765625" Y="-27.6486953125" />
                  <Point X="1.424119995117" Y="-27.64637890625" />
                  <Point X="1.408396362305" Y="-27.64651953125" />
                  <Point X="1.334866455078" Y="-27.65328515625" />
                  <Point X="1.175309570312" Y="-27.667966796875" />
                  <Point X="1.1612265625" Y="-27.67033984375" />
                  <Point X="1.133575439453" Y="-27.677171875" />
                  <Point X="1.120007324219" Y="-27.681630859375" />
                  <Point X="1.092621582031" Y="-27.692974609375" />
                  <Point X="1.079880126953" Y="-27.699412109375" />
                  <Point X="1.055498413086" Y="-27.714130859375" />
                  <Point X="1.043858154297" Y="-27.722412109375" />
                  <Point X="0.987080505371" Y="-27.76962109375" />
                  <Point X="0.863874328613" Y="-27.872064453125" />
                  <Point X="0.852650024414" Y="-27.88309375" />
                  <Point X="0.832179138184" Y="-27.906845703125" />
                  <Point X="0.822932739258" Y="-27.919568359375" />
                  <Point X="0.806040710449" Y="-27.94739453125" />
                  <Point X="0.799019287109" Y="-27.961470703125" />
                  <Point X="0.787394775391" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.765815612793" Y="-28.083736328125" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091308594" Y="-29.15233984375" />
                  <Point X="0.781432617188" Y="-28.959546875" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146484375" Y="-28.453583984375" />
                  <Point X="0.626787841797" Y="-28.42381640625" />
                  <Point X="0.620406860352" Y="-28.41320703125" />
                  <Point X="0.568757263184" Y="-28.338791015625" />
                  <Point X="0.456679229736" Y="-28.177306640625" />
                  <Point X="0.446668762207" Y="-28.165169921875" />
                  <Point X="0.424782104492" Y="-28.142712890625" />
                  <Point X="0.412905792236" Y="-28.132392578125" />
                  <Point X="0.386650512695" Y="-28.113150390625" />
                  <Point X="0.373235198975" Y="-28.10493359375" />
                  <Point X="0.345236297607" Y="-28.090828125" />
                  <Point X="0.330652679443" Y="-28.084939453125" />
                  <Point X="0.250727874756" Y="-28.060134765625" />
                  <Point X="0.07729372406" Y="-28.006306640625" />
                  <Point X="0.063380096436" Y="-28.003111328125" />
                  <Point X="0.03522794342" Y="-27.998841796875" />
                  <Point X="0.020989425659" Y="-27.997767578125" />
                  <Point X="-0.008651926041" Y="-27.997765625" />
                  <Point X="-0.022895647049" Y="-27.998837890625" />
                  <Point X="-0.051062362671" Y="-28.003107421875" />
                  <Point X="-0.064985206604" Y="-28.0063046875" />
                  <Point X="-0.144910003662" Y="-28.031111328125" />
                  <Point X="-0.318344299316" Y="-28.0849375" />
                  <Point X="-0.332928955078" Y="-28.090828125" />
                  <Point X="-0.360929229736" Y="-28.104935546875" />
                  <Point X="-0.374344970703" Y="-28.11315234375" />
                  <Point X="-0.400600402832" Y="-28.132396484375" />
                  <Point X="-0.412477294922" Y="-28.14271875" />
                  <Point X="-0.43436126709" Y="-28.16517578125" />
                  <Point X="-0.44436819458" Y="-28.177310546875" />
                  <Point X="-0.496017913818" Y="-28.251728515625" />
                  <Point X="-0.608095947266" Y="-28.4132109375" />
                  <Point X="-0.612468994141" Y="-28.42012890625" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.916966552734" Y="-29.51131640625" />
                  <Point X="-0.985424987793" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.588260529853" Y="-23.68773089242" />
                  <Point X="-4.692993733857" Y="-24.432946361123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.49406968652" Y="-23.700131388404" />
                  <Point X="-4.600541666933" Y="-24.457718894122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.768068210424" Y="-25.649732189374" />
                  <Point X="-4.777528898718" Y="-25.717048484409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.399878843187" Y="-23.712531884387" />
                  <Point X="-4.508089596879" Y="-24.48249140485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.66838053213" Y="-25.623020672303" />
                  <Point X="-4.725900807869" Y="-26.032298700718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.141779346915" Y="-22.558661714012" />
                  <Point X="-4.168790199522" Y="-22.750853916834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.305687999853" Y="-23.724932380371" />
                  <Point X="-4.415637511751" Y="-24.507263808315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.568692853835" Y="-25.596309155233" />
                  <Point X="-4.66401651796" Y="-26.274572268774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.015421185794" Y="-22.342179850957" />
                  <Point X="-4.082195065974" Y="-22.817300696236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.21149715652" Y="-23.737332876354" />
                  <Point X="-4.323185426622" Y="-24.532036211781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.469005175541" Y="-25.569597638162" />
                  <Point X="-4.568068431819" Y="-26.274469332485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.897373400462" Y="-22.184829384173" />
                  <Point X="-3.995599961298" Y="-22.883747681081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.117306313187" Y="-23.749733372338" />
                  <Point X="-4.230733341493" Y="-24.556808615246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.369317497246" Y="-25.542886121091" />
                  <Point X="-4.470326359513" Y="-26.261601521356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.780289034668" Y="-22.034334003603" />
                  <Point X="-3.909004856622" Y="-22.950194665927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.023115441854" Y="-23.762133669087" />
                  <Point X="-4.138281256365" Y="-24.581581018711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.269629818952" Y="-25.51617460402" />
                  <Point X="-4.372584287207" Y="-26.248733710227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.678491467387" Y="-21.992609846321" />
                  <Point X="-3.822409751947" Y="-23.016641650772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.92892456937" Y="-23.774533957653" />
                  <Point X="-4.045829171236" Y="-24.606353422176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.169942140657" Y="-25.489463086949" />
                  <Point X="-4.274842205512" Y="-26.235865832293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.589757795949" Y="-22.043840137976" />
                  <Point X="-3.735814647271" Y="-23.083088635617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.834733696886" Y="-23.786934246218" />
                  <Point X="-3.953377086107" Y="-24.631125825642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.070254462362" Y="-25.462751569879" />
                  <Point X="-4.177100122247" Y="-26.222997943191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501024124511" Y="-22.09507042963" />
                  <Point X="-3.649219542595" Y="-23.149535620463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739990444059" Y="-23.795404144408" />
                  <Point X="-3.860925000979" Y="-24.655898229107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.970566788218" Y="-25.436040082339" />
                  <Point X="-4.079358038983" Y="-26.210130054089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.412290474105" Y="-22.146300870936" />
                  <Point X="-3.56262443792" Y="-23.215982605308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.641161878409" Y="-23.774805531437" />
                  <Point X="-3.76847291585" Y="-24.680670632572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.870879181366" Y="-25.409329073609" />
                  <Point X="-3.981615955718" Y="-26.197262164987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153781764148" Y="-27.422285545521" />
                  <Point X="-4.161422225554" Y="-27.476650253272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.323556841599" Y="-22.197531439607" />
                  <Point X="-3.479405101099" Y="-23.30644942654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.538348792826" Y="-23.725855585978" />
                  <Point X="-3.676020830721" Y="-24.705443036037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.771191574514" Y="-25.382618064879" />
                  <Point X="-3.883873872454" Y="-26.184394275885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.046252075333" Y="-27.339775224229" />
                  <Point X="-4.08378503658" Y="-27.606836120278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234553515221" Y="-22.246843036668" />
                  <Point X="-3.583568745593" Y="-24.730215439502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.671503967662" Y="-25.355907056149" />
                  <Point X="-3.786131789189" Y="-26.171526386784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.938722386518" Y="-27.257264902938" />
                  <Point X="-4.006147610291" Y="-27.7370202987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.999061150902" Y="-21.253830968496" />
                  <Point X="-3.016098913547" Y="-21.375060948956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.141874228341" Y="-22.269998815667" />
                  <Point X="-3.491333056398" Y="-24.756527580042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.571816360811" Y="-25.329196047419" />
                  <Point X="-3.688389705925" Y="-26.158658497682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.831192697703" Y="-27.174754581647" />
                  <Point X="-3.925258147387" Y="-27.844065034247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.89154223347" Y="-21.171397289579" />
                  <Point X="-2.938946117465" Y="-21.508693450482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.046839813753" Y="-22.276396990288" />
                  <Point X="-3.40256194739" Y="-24.807491489546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.471468515006" Y="-25.297787194437" />
                  <Point X="-3.59064762266" Y="-26.14579060858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723663008888" Y="-27.092244260355" />
                  <Point X="-3.844277053994" Y="-27.950457784997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.784023316038" Y="-21.088963610662" />
                  <Point X="-2.861793321383" Y="-21.642325952007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947780094966" Y="-22.254153637281" />
                  <Point X="-3.321228192551" Y="-24.911374923721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.364655435372" Y="-25.220375812416" />
                  <Point X="-3.492905539396" Y="-26.132922719478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.616133320073" Y="-27.009733939064" />
                  <Point X="-3.754678736012" Y="-27.99553579681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.676504398606" Y="-21.006529931745" />
                  <Point X="-2.784717056973" Y="-21.776503004667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.839702432501" Y="-22.167744280876" />
                  <Point X="-3.395163456131" Y="-26.120054830376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.508603631258" Y="-26.927223617772" />
                  <Point X="-3.650273533303" Y="-27.935257349358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.571540724719" Y="-20.942277755376" />
                  <Point X="-3.297421372867" Y="-26.107186941274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.401073951652" Y="-26.844713362011" />
                  <Point X="-3.545868330567" Y="-27.874978901715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.467482013085" Y="-20.884464720029" />
                  <Point X="-3.200537038698" Y="-26.100422254119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.293544279928" Y="-26.762203162331" />
                  <Point X="-3.441463127739" Y="-27.814700453414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.363423301957" Y="-20.826651688282" />
                  <Point X="-3.107854314027" Y="-26.123553571964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.186014608205" Y="-26.67969296265" />
                  <Point X="-3.337057924911" Y="-27.754422005113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.25936459088" Y="-20.768838656893" />
                  <Point X="-3.020166411413" Y="-26.182224895446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.078484936481" Y="-26.59718276297" />
                  <Point X="-3.232652722083" Y="-27.694143556812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.163523554633" Y="-20.769497420179" />
                  <Point X="-2.947355346293" Y="-26.346750418004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959811956678" Y="-26.435383806381" />
                  <Point X="-3.128247519255" Y="-27.633865108511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.081072228015" Y="-20.865428917953" />
                  <Point X="-3.023842316427" Y="-27.57358666021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.992664877119" Y="-20.91898110091" />
                  <Point X="-2.919437113599" Y="-27.513308211909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.902517687301" Y="-20.960153686689" />
                  <Point X="-2.815031910771" Y="-27.453029763608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.807871805362" Y="-20.96931641475" />
                  <Point X="-2.710626707943" Y="-27.392751315307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890949500811" Y="-28.675814655939" />
                  <Point X="-2.919478933063" Y="-28.878812114383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.707743398495" Y="-20.939468950936" />
                  <Point X="-2.609137388101" Y="-27.353220452518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.764149756951" Y="-28.456190768427" />
                  <Point X="-2.832912796771" Y="-28.945465219984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.605130385238" Y="-20.891942594048" />
                  <Point X="-2.513338321103" Y="-27.354177842532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.63735001309" Y="-28.236566880915" />
                  <Point X="-2.746322321332" Y="-29.01194514356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.443914449782" Y="-20.427434778903" />
                  <Point X="-1.463031536452" Y="-20.563459918573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.479886046375" Y="-20.683385988164" />
                  <Point X="-2.423366030277" Y="-27.396594899298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.510550250461" Y="-28.016942859866" />
                  <Point X="-2.658068439766" Y="-29.066589317544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.344045797829" Y="-20.399435567339" />
                  <Point X="-2.340651669515" Y="-27.490654811884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.383750470423" Y="-27.797318714937" />
                  <Point X="-2.5698145582" Y="-29.121233491527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.244177139175" Y="-20.371436308102" />
                  <Point X="-2.474403014226" Y="-29.124948251131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.144308480522" Y="-20.343437048865" />
                  <Point X="-2.359918951529" Y="-28.992954988479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.044439821868" Y="-20.315437789627" />
                  <Point X="-2.253977254824" Y="-28.921743818168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.944571163215" Y="-20.28743853039" />
                  <Point X="-2.148101231075" Y="-28.850999935222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.846720198298" Y="-20.273795908079" />
                  <Point X="-2.043874378938" Y="-28.791990518025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.749182107289" Y="-20.262379499289" />
                  <Point X="-1.945683066202" Y="-28.775926195145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.651644072155" Y="-20.250963488081" />
                  <Point X="-1.850625093823" Y="-28.782156747369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.554106092772" Y="-20.239547873559" />
                  <Point X="-1.755567121444" Y="-28.788387299592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.456568113389" Y="-20.228132259037" />
                  <Point X="-1.660509052752" Y="-28.794617166513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.3612274974" Y="-20.232351697472" />
                  <Point X="-1.567305965041" Y="-28.814045908943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.29829986267" Y="-20.467201481379" />
                  <Point X="-1.47903548986" Y="-28.868572013226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.235372165136" Y="-20.702050818407" />
                  <Point X="-1.393628440672" Y="-28.943472452111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.159835538691" Y="-20.847182964431" />
                  <Point X="-1.308221341713" Y="-29.018372536865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.071392517584" Y="-20.90048134065" />
                  <Point X="-1.224309495546" Y="-29.103911898064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.022537626526" Y="-20.91473680799" />
                  <Point X="-1.158717906475" Y="-29.319806661905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.122828746021" Y="-20.88373158367" />
                  <Point X="-1.121339429353" Y="-29.736448148282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.240558544824" Y="-20.728643708611" />
                  <Point X="-1.028497402599" Y="-29.75844597291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.408238730453" Y="-20.218140363497" />
                  <Point X="-0.876725065168" Y="-29.361132849223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.502780845613" Y="-20.228041430561" />
                  <Point X="-0.674969157414" Y="-28.60816814264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.597322960773" Y="-20.237942497625" />
                  <Point X="-0.53730062216" Y="-28.311208785929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.691865075932" Y="-20.247843564689" />
                  <Point X="-0.418565289599" Y="-28.148966166404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.786407191092" Y="-20.257744631753" />
                  <Point X="-0.313418162062" Y="-28.083408649495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.879963146242" Y="-20.274662591892" />
                  <Point X="-0.213109318971" Y="-28.052277315243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.972748447194" Y="-20.297064041575" />
                  <Point X="-0.112800381801" Y="-28.021145311577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.065533764718" Y="-20.319465373343" />
                  <Point X="-0.013633668064" Y="-27.998140649948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.158319098542" Y="-20.341866589135" />
                  <Point X="0.080991023149" Y="-28.007454157863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.251104432365" Y="-20.364267804927" />
                  <Point X="0.17291499725" Y="-28.035984266544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.343889766189" Y="-20.386669020719" />
                  <Point X="0.264839001849" Y="-28.06451415822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.436675100012" Y="-20.40907023651" />
                  <Point X="0.356291684507" Y="-28.096397679762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.528230043048" Y="-20.440226137662" />
                  <Point X="0.443077821374" Y="-28.161485399937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.619510652684" Y="-20.473334022375" />
                  <Point X="0.523297997926" Y="-28.27329235534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.710791261293" Y="-20.506441914402" />
                  <Point X="0.603077002208" Y="-28.38823841455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.802071869901" Y="-20.539549806429" />
                  <Point X="0.674700256005" Y="-28.561215653824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.892866584551" Y="-20.57611501362" />
                  <Point X="0.867965886607" Y="-27.868662408219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.762700266927" Y="-28.617666211304" />
                  <Point X="0.737627916968" Y="-28.796065251071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.98288367798" Y="-20.618213283302" />
                  <Point X="0.976593361269" Y="-27.778340934754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809100718087" Y="-28.970113016772" />
                  <Point X="0.800555584599" Y="-29.030914800866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072900702558" Y="-20.66031204288" />
                  <Point X="1.083905535572" Y="-27.697378309638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162917727136" Y="-20.702410802458" />
                  <Point X="1.184086171442" Y="-27.667159217159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.252897353113" Y="-20.744775666912" />
                  <Point X="1.281276646107" Y="-27.658216227182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.341570626602" Y="-20.796435712304" />
                  <Point X="2.132319235735" Y="-22.285336723244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059601738082" Y="-22.802748604338" />
                  <Point X="1.378467100864" Y="-27.649273378862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.430243809895" Y="-20.848096399473" />
                  <Point X="2.259118850012" Y="-22.065713757775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.140587687773" Y="-22.909106800723" />
                  <Point X="1.47403000166" Y="-27.651911178715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.51891695777" Y="-20.899757338659" />
                  <Point X="2.385918534434" Y="-21.846090293188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.227509758095" Y="-22.973227304113" />
                  <Point X="1.56506407192" Y="-27.686773282248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.607590105644" Y="-20.951418277846" />
                  <Point X="2.512718330975" Y="-21.626466030841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.318052632293" Y="-23.011584449223" />
                  <Point X="1.653047909819" Y="-27.743338916758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.695165579941" Y="-21.010889570375" />
                  <Point X="2.639518127515" Y="-21.406841768494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.412185130594" Y="-23.024400091686" />
                  <Point X="1.741031845785" Y="-27.799903853491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.782382440244" Y="-21.072912534053" />
                  <Point X="2.766317924055" Y="-21.187217506147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.509515799605" Y="-23.014459567107" />
                  <Point X="1.827291663992" Y="-27.868736525524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.609651216085" Y="-22.984562227306" />
                  <Point X="1.908486581528" Y="-27.973607838443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.713628934425" Y="-22.92732548919" />
                  <Point X="2.101619157594" Y="-27.282001325052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.034541134677" Y="-27.759286258352" />
                  <Point X="1.989569390308" Y="-28.079276846599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.818034129141" Y="-22.867047098611" />
                  <Point X="2.216304767003" Y="-27.148573983031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.105628610618" Y="-27.936075755162" />
                  <Point X="2.070652199089" Y="-28.184945854755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.922439323856" Y="-22.806768708032" />
                  <Point X="2.319973369927" Y="-27.093536715388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.182781434857" Y="-28.069708056337" />
                  <Point X="2.15173500787" Y="-28.290614862911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.026844518572" Y="-22.746490317453" />
                  <Point X="2.423106525705" Y="-27.042309352154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.259934259096" Y="-28.203340357511" />
                  <Point X="2.232817779567" Y="-28.396284134931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.131249713288" Y="-22.686211926874" />
                  <Point X="3.01553819596" Y="-23.509542153798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00113212541" Y="-23.612046672006" />
                  <Point X="2.521391739922" Y="-27.025576885517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.337087083335" Y="-28.336972658686" />
                  <Point X="2.313900548603" Y="-28.501953425889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.235654908004" Y="-22.625933536295" />
                  <Point X="3.143115184188" Y="-23.284387885247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.066325531643" Y="-23.830774653959" />
                  <Point X="2.615545115807" Y="-27.038243976246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.414239907574" Y="-28.47060495986" />
                  <Point X="2.394983317638" Y="-28.607622716847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.340060102719" Y="-22.565655145716" />
                  <Point X="3.251481973309" Y="-23.195921285789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147408323081" Y="-23.936443785517" />
                  <Point X="2.709104054724" Y="-27.055140705778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.491392731813" Y="-28.604237261035" />
                  <Point X="2.476066086674" Y="-28.713292007805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.444465297435" Y="-22.505376755137" />
                  <Point X="3.359011673789" Y="-23.113410881496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.235318823306" Y="-23.993531244695" />
                  <Point X="2.930211830878" Y="-26.16448030071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.87361509669" Y="-26.567186989538" />
                  <Point X="2.802188097902" Y="-27.075416494069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.568545556052" Y="-28.73786956221" />
                  <Point X="2.55714885571" Y="-28.818961298763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.548870492151" Y="-22.445098364559" />
                  <Point X="3.466541374269" Y="-23.030900477203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.326018098073" Y="-24.030775541943" />
                  <Point X="3.045467798738" Y="-26.026994647437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.950433505572" Y="-26.703198779615" />
                  <Point X="2.89189081185" Y="-27.119751689991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.645698339839" Y="-28.871502151217" />
                  <Point X="2.638231624745" Y="-28.924630589722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.653275686866" Y="-22.38481997398" />
                  <Point X="3.574071070075" Y="-22.948390106169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419976777397" Y="-24.044827970687" />
                  <Point X="3.150882267899" Y="-25.959534896024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.030617561531" Y="-26.815262746387" />
                  <Point X="2.980624481991" Y="-27.170981990875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.722851095402" Y="-29.00513494105" />
                  <Point X="2.719314393781" Y="-29.03029988068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.7576808565" Y="-22.324541761873" />
                  <Point X="3.681600758936" Y="-22.86587978455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.515858853241" Y="-24.045194722067" />
                  <Point X="3.410223411712" Y="-24.796829944331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362864215539" Y="-25.133808134856" />
                  <Point X="3.250216277989" Y="-25.935339858992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.116705466551" Y="-26.88531864431" />
                  <Point X="3.069358152132" Y="-27.22221229176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.862086025041" Y="-22.264263557531" />
                  <Point X="3.789130447798" Y="-22.783369462932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613600950529" Y="-24.032326733183" />
                  <Point X="3.524205819364" Y="-24.668406142801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435537896114" Y="-25.299311199244" />
                  <Point X="3.349172684343" Y="-25.913831612141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.203300599924" Y="-26.951765424963" />
                  <Point X="3.158091822273" Y="-27.273442592644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.960457508865" Y="-22.246917250747" />
                  <Point X="3.896660136659" Y="-22.700859141313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.711343047817" Y="-24.019458744298" />
                  <Point X="3.628966714652" Y="-24.605596811137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518765611372" Y="-25.389718404795" />
                  <Point X="3.445586746808" Y="-25.910413082026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.289895733297" Y="-27.018212205615" />
                  <Point X="3.246825492414" Y="-27.324672893528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.040715072341" Y="-22.358458184359" />
                  <Point X="4.00418982552" Y="-22.618348819694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.809085145105" Y="-24.006590755414" />
                  <Point X="3.730123944843" Y="-24.568428888997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.607030292991" Y="-25.444285732408" />
                  <Point X="3.539777585463" Y="-25.922813611297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.37649086667" Y="-27.084658986268" />
                  <Point X="3.335559147351" Y="-27.375903302593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.118664602198" Y="-22.486421630501" />
                  <Point X="4.111719514381" Y="-22.535838498076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.906827242393" Y="-23.993722766529" />
                  <Point X="3.829811641339" Y="-24.541717242418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.697198201986" Y="-25.485310893575" />
                  <Point X="3.633968424118" Y="-25.935214140568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463086000043" Y="-27.151105766921" />
                  <Point X="3.424292796246" Y="-27.42713375465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.004569339681" Y="-23.980854777645" />
                  <Point X="3.929499337835" Y="-24.51500559584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789650288893" Y="-25.510083284387" />
                  <Point X="3.728159262773" Y="-25.947614669839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.549681133416" Y="-27.217552547573" />
                  <Point X="3.513026445141" Y="-27.478364206707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102311436969" Y="-23.967986788761" />
                  <Point X="4.029187034331" Y="-24.488293949261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.8821023758" Y="-25.5348556752" />
                  <Point X="3.822350101428" Y="-25.96001519911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.636276266789" Y="-27.283999328226" />
                  <Point X="3.601760094036" Y="-27.529594658763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.200053534257" Y="-23.955118799876" />
                  <Point X="4.12887471838" Y="-24.461582391243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974554462707" Y="-25.559628066012" />
                  <Point X="3.916540941594" Y="-25.972415717624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.722871400162" Y="-27.350446108879" />
                  <Point X="3.690493742931" Y="-27.58082511082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.297795631545" Y="-23.942250810992" />
                  <Point X="4.228562368518" Y="-24.434871074518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.067006549613" Y="-25.584400456824" />
                  <Point X="4.010731814669" Y="-25.984816001985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.809466533535" Y="-27.416892889531" />
                  <Point X="3.779227391826" Y="-27.632055562877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.395537728833" Y="-23.929382822108" />
                  <Point X="4.328250018655" Y="-24.408159757794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.15945863652" Y="-25.609172847636" />
                  <Point X="4.104922687743" Y="-25.997216286345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.896061670369" Y="-27.483339645555" />
                  <Point X="3.867961040721" Y="-27.683286014933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.493279834525" Y="-23.916514773428" />
                  <Point X="4.427937668793" Y="-24.381448441069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.251910723427" Y="-25.633945238449" />
                  <Point X="4.199113560818" Y="-26.009616570706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.982656820976" Y="-27.549786303582" />
                  <Point X="3.956694689616" Y="-27.73451646699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.591021968763" Y="-23.903646521626" />
                  <Point X="4.527625318931" Y="-24.354737124344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.344362810334" Y="-25.658717629261" />
                  <Point X="4.293304433893" Y="-26.022016855066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.069251971583" Y="-27.616232961608" />
                  <Point X="4.061476281427" Y="-27.671559871919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.688764103002" Y="-23.890778269824" />
                  <Point X="4.627312969069" Y="-24.32802580762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.436814897241" Y="-25.683490020073" />
                  <Point X="4.387495306968" Y="-26.034417139427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.755106787416" Y="-24.101328712602" />
                  <Point X="4.727000619206" Y="-24.301314490895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.529266984148" Y="-25.708262410886" />
                  <Point X="4.481686180042" Y="-26.046817423788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.621719090852" Y="-25.733034660833" />
                  <Point X="4.575877053117" Y="-26.059217708148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.714171223766" Y="-25.757806724289" />
                  <Point X="4.670067926192" Y="-26.071617992509" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.597906738281" Y="-29.00872265625" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.412669250488" Y="-28.447126953125" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.274335906982" Y="-28.266400390625" />
                  <Point X="0.194411117554" Y="-28.241595703125" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.088589248657" Y="-28.212572265625" />
                  <Point X="-0.2620234375" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.339928710938" Y="-28.360060546875" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.733440673828" Y="-29.5604921875" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-0.896538818359" Y="-29.97760546875" />
                  <Point X="-1.100230224609" Y="-29.938068359375" />
                  <Point X="-1.192067749023" Y="-29.914439453125" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.324361938477" Y="-29.666583984375" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.32877734375" Y="-29.438767578125" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.4598671875" Y="-29.13809765625" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240600586" Y="-28.985763671875" />
                  <Point X="-1.746903808594" Y="-28.97936328125" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.071256835938" Y="-29.028166015625" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.413683837891" Y="-29.35792578125" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.557240234375" Y="-29.3524921875" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-2.982981201172" Y="-29.069712890625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.740873779297" Y="-28.035875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.597591796875" />
                  <Point X="-2.513980712891" Y="-27.568763671875" />
                  <Point X="-2.531329345703" Y="-27.551416015625" />
                  <Point X="-2.560158203125" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.474328125" Y="-28.05306640625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.925798339844" Y="-28.157060546875" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.252857910156" Y="-27.694279296875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.574680664062" Y="-26.738435546875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535644531" Y="-26.411083984375" />
                  <Point X="-3.143391601562" Y="-26.386630859375" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.136651611328" Y="-26.350052734375" />
                  <Point X="-3.148654541016" Y="-26.321072265625" />
                  <Point X="-3.169516113281" Y="-26.305720703125" />
                  <Point X="-3.18764453125" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-4.333784667969" Y="-26.435265625" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.83512890625" Y="-26.37240234375" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.951510253906" Y="-25.842568359375" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.025533691406" Y="-25.254064453125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.533141113281" Y="-25.115349609375" />
                  <Point X="-3.514143066406" Y="-25.1021640625" />
                  <Point X="-3.502324951172" Y="-25.090646484375" />
                  <Point X="-3.491981201172" Y="-25.066505859375" />
                  <Point X="-3.4856484375" Y="-25.046103515625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.488566162109" Y="-25.007056640625" />
                  <Point X="-3.494898925781" Y="-24.98665234375" />
                  <Point X="-3.502324462891" Y="-24.9719140625" />
                  <Point X="-3.522897949219" Y="-24.9543203125" />
                  <Point X="-3.541895996094" Y="-24.941134765625" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.57316015625" Y="-24.661759765625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.977107910156" Y="-24.40542578125" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.86909765625" Y="-23.824427734375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.088790039063" Y="-23.56184765625" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731704833984" Y="-23.6041328125" />
                  <Point X="-3.712327392578" Y="-23.5980234375" />
                  <Point X="-3.670278808594" Y="-23.584765625" />
                  <Point X="-3.651534667969" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.631344726563" Y="-23.537443359375" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.625697753906" Y="-23.436466796875" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.24257421875" Y="-22.933728515625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.391065917969" Y="-22.608841796875" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.031414794922" Y="-22.047697265625" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.365226318359" Y="-21.95408203125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514404297" Y="-22.07956640625" />
                  <Point X="-3.111526855469" Y="-22.081927734375" />
                  <Point X="-3.052965087891" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.994096923828" Y="-22.053439453125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.940435302734" Y="-21.945171875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.210237792969" Y="-21.418802734375" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.155294921875" Y="-21.13419921875" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.550335693359" Y="-20.713140625" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.030911010742" Y="-20.630212890625" />
                  <Point X="-1.967826904297" Y="-20.71242578125" />
                  <Point X="-1.951247436523" Y="-20.726337890625" />
                  <Point X="-1.921210571289" Y="-20.741974609375" />
                  <Point X="-1.85603137207" Y="-20.77590625" />
                  <Point X="-1.835124267578" Y="-20.781509765625" />
                  <Point X="-1.81380847168" Y="-20.77775" />
                  <Point X="-1.782522949219" Y="-20.764791015625" />
                  <Point X="-1.714634399414" Y="-20.736669921875" />
                  <Point X="-1.696905029297" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.675900268555" Y="-20.673216796875" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.681268676758" Y="-20.358630859375" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.489050048828" Y="-20.242763671875" />
                  <Point X="-0.968082946777" Y="-20.096703125" />
                  <Point X="-0.722555053711" Y="-20.067966796875" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.103101448059" Y="-20.461587890625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282115936" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594036102" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.186735778809" Y="-20.195408203125" />
                  <Point X="0.236648361206" Y="-20.0091328125" />
                  <Point X="0.405334960938" Y="-20.026798828125" />
                  <Point X="0.860210144043" Y="-20.074435546875" />
                  <Point X="1.063347900391" Y="-20.12348046875" />
                  <Point X="1.508455932617" Y="-20.230943359375" />
                  <Point X="1.640639282227" Y="-20.27888671875" />
                  <Point X="1.931043945312" Y="-20.38421875" />
                  <Point X="2.058890869141" Y="-20.4440078125" />
                  <Point X="2.338684570312" Y="-20.574859375" />
                  <Point X="2.462218505859" Y="-20.646830078125" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.849015136719" Y="-20.88715234375" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.501663085938" Y="-22.025615234375" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.218079101562" Y="-22.533814453125" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.204685546875" Y="-22.629576171875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.232233886719" Y="-22.71916015625" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.274939941406" Y="-22.775796875" />
                  <Point X="2.294911132812" Y="-22.789349609375" />
                  <Point X="2.338248535156" Y="-22.818755859375" />
                  <Point X="2.360337158203" Y="-22.827021484375" />
                  <Point X="2.382238037109" Y="-22.829662109375" />
                  <Point X="2.429761962891" Y="-22.835392578125" />
                  <Point X="2.448664794922" Y="-22.8340546875" />
                  <Point X="2.473991943359" Y="-22.82728125" />
                  <Point X="2.528951171875" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.56262890625" Y="-22.217763671875" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.042255859375" Y="-22.0352890625" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.267534179688" Y="-22.365439453125" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.646479003906" Y="-23.1323203125" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.261143310547" Y="-23.439947265625" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.206329345703" Y="-23.532779296875" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.196353271484" Y="-23.636048828125" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646728516" Y="-23.712046875" />
                  <Point X="3.230806884766" Y="-23.73508984375" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280947753906" Y="-23.8011796875" />
                  <Point X="3.302916992188" Y="-23.813546875" />
                  <Point X="3.35058984375" Y="-23.8403828125" />
                  <Point X="3.36856640625" Y="-23.846380859375" />
                  <Point X="3.398270263672" Y="-23.850306640625" />
                  <Point X="3.462727050781" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.446250488281" Y="-23.73106640625" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.870325195312" Y="-23.76574609375" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.959653808594" Y="-24.18005859375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.151286132813" Y="-24.65228125" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.699903808594" Y="-24.784048828125" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.622264892578" Y="-24.833072265625" />
                  <Point X="3.604754882812" Y="-24.855384765625" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.5511484375" Y="-24.9557421875" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.544319824219" Y="-25.071162109375" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.584268798828" Y="-25.181072265625" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.636576171875" Y="-25.241908203125" />
                  <Point X="3.665759521484" Y="-25.258775390625" />
                  <Point X="3.729086181641" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.631111816406" Y="-25.53884765625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.986881835938" Y="-25.711369140625" />
                  <Point X="4.948431640625" Y="-25.966400390625" />
                  <Point X="4.922212402344" Y="-26.081296875" />
                  <Point X="4.874545898438" Y="-26.290177734375" />
                  <Point X="3.887604248047" Y="-26.16024609375" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836425781" Y="-26.098341796875" />
                  <Point X="3.337560058594" Y="-26.110791015625" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.150825195312" Y="-26.196333984375" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.059395996094" Y="-26.3679921875" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.088058105469" Y="-26.56592578125" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.994333984375" Y="-27.319255859375" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.312519042969" Y="-27.62675390625" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.149907714844" Y="-27.879185546875" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="3.177060302734" Y="-27.50378515625" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.669172851562" Y="-27.24100390625" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.432447021484" Y="-27.24905078125" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.258795410156" Y="-27.391314453125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.201474121094" Y="-27.61454296875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.764798339844" Y="-28.6977890625" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.963909423828" Y="-29.09834765625" />
                  <Point X="2.835295898438" Y="-29.190212890625" />
                  <Point X="2.774673828125" Y="-29.229453125" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="2.006427734375" Y="-28.41335546875" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670549316406" Y="-27.980466796875" />
                  <Point X="1.603317260742" Y="-27.937244140625" />
                  <Point X="1.45742590332" Y="-27.84344921875" />
                  <Point X="1.425805053711" Y="-27.83571875" />
                  <Point X="1.352275146484" Y="-27.842484375" />
                  <Point X="1.192718261719" Y="-27.857166015625" />
                  <Point X="1.165332519531" Y="-27.868509765625" />
                  <Point X="1.10855480957" Y="-27.91571875" />
                  <Point X="0.985348754883" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.951480529785" Y="-28.124091796875" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.064327758789" Y="-29.453107421875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.116022338867" Y="-29.936576171875" />
                  <Point X="0.994364868164" Y="-29.9632421875" />
                  <Point X="0.938338745117" Y="-29.973421875" />
                  <Point X="0.860200561523" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#148" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.06096105203" Y="4.582634228643" Z="0.8" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.8" />
                  <Point X="-0.734523202698" Y="5.012974273047" Z="0.8" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.8" />
                  <Point X="-1.508703818612" Y="4.836662241728" Z="0.8" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.8" />
                  <Point X="-1.736996940898" Y="4.66612412805" Z="0.8" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.8" />
                  <Point X="-1.729742312464" Y="4.373099843406" Z="0.8" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.8" />
                  <Point X="-1.80781358704" Y="4.31268355068" Z="0.8" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.8" />
                  <Point X="-1.904278255919" Y="4.333654967541" Z="0.8" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.8" />
                  <Point X="-1.997399298944" Y="4.431504154989" Z="0.8" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.8" />
                  <Point X="-2.580774539162" Y="4.361846143208" Z="0.8" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.8" />
                  <Point X="-3.19187294104" Y="3.936761127657" Z="0.8" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.8" />
                  <Point X="-3.25969499491" Y="3.587476974855" Z="0.8" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.8" />
                  <Point X="-2.996401132513" Y="3.081751009208" Z="0.8" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.8" />
                  <Point X="-3.035607668061" Y="3.013195899825" Z="0.8" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.8" />
                  <Point X="-3.113325456399" Y="2.99916356666" Z="0.8" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.8" />
                  <Point X="-3.346382373442" Y="3.120498953836" Z="0.8" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.8" />
                  <Point X="-4.077034053908" Y="3.01428585996" Z="0.8" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.8" />
                  <Point X="-4.44040425705" Y="2.447644560713" Z="0.8" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.8" />
                  <Point X="-4.27916817307" Y="2.05788328882" Z="0.8" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.8" />
                  <Point X="-3.676204187547" Y="1.571726565253" Z="0.8" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.8" />
                  <Point X="-3.683694599879" Y="1.512971365389" Z="0.8" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.8" />
                  <Point X="-3.733518534205" Y="1.480942242495" Z="0.8" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.8" />
                  <Point X="-4.08841994626" Y="1.519005104384" Z="0.8" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.8" />
                  <Point X="-4.923513272879" Y="1.219931229473" Z="0.8" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.8" />
                  <Point X="-5.03272542084" Y="0.633172221906" Z="0.8" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.8" />
                  <Point X="-4.592257188919" Y="0.321224080653" Z="0.8" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.8" />
                  <Point X="-3.557562799386" Y="0.035883379362" Z="0.8" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.8" />
                  <Point X="-3.542475105177" Y="0.009402919699" Z="0.8" />
                  <Point X="-3.539556741714" Y="0" Z="0.8" />
                  <Point X="-3.545889495958" Y="-0.020404031362" Z="0.8" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.8" />
                  <Point X="-3.567805795737" Y="-0.042992603772" Z="0.8" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.8" />
                  <Point X="-4.044630599854" Y="-0.174487976103" Z="0.8" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.8" />
                  <Point X="-5.007162078061" Y="-0.818366922321" Z="0.8" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.8" />
                  <Point X="-4.889729755139" Y="-1.353496006647" Z="0.8" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.8" />
                  <Point X="-4.333414031213" Y="-1.453557752385" Z="0.8" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.8" />
                  <Point X="-3.201030517925" Y="-1.317532877622" Z="0.8" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.8" />
                  <Point X="-3.197950336836" Y="-1.342813047014" Z="0.8" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.8" />
                  <Point X="-3.611274524366" Y="-1.667486999192" Z="0.8" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.8" />
                  <Point X="-4.301956997537" Y="-2.688607470617" Z="0.8" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.8" />
                  <Point X="-3.971740315579" Y="-3.156063705373" Z="0.8" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.8" />
                  <Point X="-3.455484135518" Y="-3.065086082304" Z="0.8" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.8" />
                  <Point X="-2.560963748236" Y="-2.567366720896" Z="0.8" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.8" />
                  <Point X="-2.790330930864" Y="-2.979594246974" Z="0.8" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.8" />
                  <Point X="-3.01964103592" Y="-4.078049003647" Z="0.8" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.8" />
                  <Point X="-2.589717831024" Y="-4.363723842738" Z="0.8" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.8" />
                  <Point X="-2.380172083985" Y="-4.35708340384" Z="0.8" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.8" />
                  <Point X="-2.049634203106" Y="-4.038459626868" Z="0.8" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.8" />
                  <Point X="-1.756330038109" Y="-3.997974744045" Z="0.8" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.8" />
                  <Point X="-1.49899060124" Y="-4.144408436467" Z="0.8" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.8" />
                  <Point X="-1.383972561133" Y="-4.417240170402" Z="0.8" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.8" />
                  <Point X="-1.380090209615" Y="-4.628776319243" Z="0.8" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.8" />
                  <Point X="-1.210682668534" Y="-4.931583225641" Z="0.8" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.8" />
                  <Point X="-0.912222281846" Y="-4.995409647229" Z="0.8" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.8" />
                  <Point X="-0.691300449121" Y="-4.542152497085" Z="0.8" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.8" />
                  <Point X="-0.305008723737" Y="-3.357289967401" Z="0.8" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.8" />
                  <Point X="-0.07992481597" Y="-3.229045081815" Z="0.8" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.8" />
                  <Point X="0.173434263391" Y="-3.258066963473" Z="0.8" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.8" />
                  <Point X="0.365437135695" Y="-3.444355788367" Z="0.8" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.8" />
                  <Point X="0.54345441128" Y="-3.99038353273" Z="0.8" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.8" />
                  <Point X="0.941119332093" Y="-4.991336363966" Z="0.8" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.8" />
                  <Point X="1.120572014285" Y="-4.954135888832" Z="0.8" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.8" />
                  <Point X="1.107744004794" Y="-4.415301464399" Z="0.8" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.8" />
                  <Point X="0.99418374417" Y="-3.103430180487" Z="0.8" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.8" />
                  <Point X="1.134365967848" Y="-2.9228843048" Z="0.8" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.8" />
                  <Point X="1.350700776399" Y="-2.860992857236" Z="0.8" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.8" />
                  <Point X="1.570121841219" Y="-2.948021269843" Z="0.8" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.8" />
                  <Point X="1.960604129846" Y="-3.412513088919" Z="0.8" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.8" />
                  <Point X="2.79568717443" Y="-4.240147696871" Z="0.8" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.8" />
                  <Point X="2.986815140796" Y="-4.107755482724" Z="0.8" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.8" />
                  <Point X="2.801943637122" Y="-3.641509276087" Z="0.8" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.8" />
                  <Point X="2.244522211419" Y="-2.574376208962" Z="0.8" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.8" />
                  <Point X="2.296885765186" Y="-2.383321054278" Z="0.8" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.8" />
                  <Point X="2.449577324094" Y="-2.262015544781" Z="0.8" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.8" />
                  <Point X="2.65413059455" Y="-2.258925798072" Z="0.8" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.8" />
                  <Point X="3.14590432102" Y="-2.515805863152" Z="0.8" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.8" />
                  <Point X="4.184640529501" Y="-2.876683254438" Z="0.8" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.8" />
                  <Point X="4.348896898227" Y="-2.621758686907" Z="0.8" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.8" />
                  <Point X="4.018616103612" Y="-2.248307969555" Z="0.8" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.8" />
                  <Point X="3.123960091393" Y="-1.507606008013" Z="0.8" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.8" />
                  <Point X="3.103029592278" Y="-1.341294001797" Z="0.8" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.8" />
                  <Point X="3.183115594356" Y="-1.197021216493" Z="0.8" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.8" />
                  <Point X="3.342023460896" Y="-1.128369686754" Z="0.8" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.8" />
                  <Point X="3.874922029413" Y="-1.178537266246" Z="0.8" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.8" />
                  <Point X="4.964803527481" Y="-1.061140378552" Z="0.8" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.8" />
                  <Point X="5.030167386865" Y="-0.687541522291" Z="0.8" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.8" />
                  <Point X="4.637896602181" Y="-0.459270375917" Z="0.8" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.8" />
                  <Point X="3.684625973261" Y="-0.184206653915" Z="0.8" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.8" />
                  <Point X="3.617446678851" Y="-0.118922328818" Z="0.8" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.8" />
                  <Point X="3.587271378429" Y="-0.030476819516" Z="0.8" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.8" />
                  <Point X="3.594100071995" Y="0.066133711709" Z="0.8" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.8" />
                  <Point X="3.637932759549" Y="0.145026409757" Z="0.8" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.8" />
                  <Point X="3.718769441091" Y="0.203942190327" Z="0.8" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.8" />
                  <Point X="4.158071215125" Y="0.330701556851" Z="0.8" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.8" />
                  <Point X="5.002902444808" Y="0.858912629305" Z="0.8" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.8" />
                  <Point X="4.912750277234" Y="1.277361316826" Z="0.8" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.8" />
                  <Point X="4.433568263972" Y="1.349785874014" Z="0.8" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.8" />
                  <Point X="3.39866464319" Y="1.230542830637" Z="0.8" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.8" />
                  <Point X="3.321287882657" Y="1.261304200703" Z="0.8" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.8" />
                  <Point X="3.266421263028" Y="1.323673464248" Z="0.8" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.8" />
                  <Point X="3.239165895503" Y="1.405335427249" Z="0.8" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.8" />
                  <Point X="3.24832602616" Y="1.485034433335" Z="0.8" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.8" />
                  <Point X="3.294670158012" Y="1.560915237067" Z="0.8" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.8" />
                  <Point X="3.670761030836" Y="1.859292930868" Z="0.8" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.8" />
                  <Point X="4.304155596219" Y="2.691728596281" Z="0.8" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.8" />
                  <Point X="4.076685522253" Y="3.0251936075" Z="0.8" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.8" />
                  <Point X="3.531472997249" Y="2.856816926974" Z="0.8" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.8" />
                  <Point X="2.454918811659" Y="2.252301986253" Z="0.8" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.8" />
                  <Point X="2.382067600904" Y="2.251259695577" Z="0.8" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.8" />
                  <Point X="2.316829479953" Y="2.283306658084" Z="0.8" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.8" />
                  <Point X="2.26745194215" Y="2.34019538043" Z="0.8" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.8" />
                  <Point X="2.248170007182" Y="2.407690842928" Z="0.8" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.8" />
                  <Point X="2.260225956404" Y="2.484550764223" Z="0.8" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.8" />
                  <Point X="2.538808425886" Y="2.980666084706" Z="0.8" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.8" />
                  <Point X="2.871836191273" Y="4.1848763031" Z="0.8" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.8" />
                  <Point X="2.481232493129" Y="4.42765442242" Z="0.8" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.8" />
                  <Point X="2.073916313032" Y="4.632563540811" Z="0.8" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.8" />
                  <Point X="1.651531908916" Y="4.799398114016" Z="0.8" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.8" />
                  <Point X="1.068926359353" Y="4.956404446444" Z="0.8" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.8" />
                  <Point X="0.404386812742" Y="5.054210799554" Z="0.8" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.8" />
                  <Point X="0.132283509523" Y="4.848813277308" Z="0.8" />
                  <Point X="0" Y="4.355124473572" Z="0.8" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>