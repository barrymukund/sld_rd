<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#181" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2414" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995282226562" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.815836975098" Y="-29.454998046875" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557720153809" Y="-28.497138671875" />
                  <Point X="0.542363037109" Y="-28.467376953125" />
                  <Point X="0.439145568848" Y="-28.318662109375" />
                  <Point X="0.378635375977" Y="-28.2314765625" />
                  <Point X="0.356752807617" Y="-28.2090234375" />
                  <Point X="0.330497924805" Y="-28.189779296875" />
                  <Point X="0.302494873047" Y="-28.175669921875" />
                  <Point X="0.14277204895" Y="-28.126099609375" />
                  <Point X="0.049135883331" Y="-28.097037109375" />
                  <Point X="0.020983621597" Y="-28.092767578125" />
                  <Point X="-0.008658161163" Y="-28.092765625" />
                  <Point X="-0.036823932648" Y="-28.09703515625" />
                  <Point X="-0.196546905518" Y="-28.146607421875" />
                  <Point X="-0.290183074951" Y="-28.17566796875" />
                  <Point X="-0.318184295654" Y="-28.189775390625" />
                  <Point X="-0.344439788818" Y="-28.20901953125" />
                  <Point X="-0.366323577881" Y="-28.2314765625" />
                  <Point X="-0.469540893555" Y="-28.380193359375" />
                  <Point X="-0.530051208496" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.699036315918" Y="-29.065041015625" />
                  <Point X="-0.916584594727" Y="-29.87694140625" />
                  <Point X="-0.97284161377" Y="-29.866021484375" />
                  <Point X="-1.07935546875" Y="-29.845345703125" />
                  <Point X="-1.246418212891" Y="-29.802365234375" />
                  <Point X="-1.245362304688" Y="-29.79434375" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.254666625977" Y="-29.32439453125" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937744141" Y="-29.182966796875" />
                  <Point X="-1.304009643555" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131203125" />
                  <Point X="-1.470697265625" Y="-29.0022421875" />
                  <Point X="-1.556905639648" Y="-28.926638671875" />
                  <Point X="-1.583189697266" Y="-28.910294921875" />
                  <Point X="-1.612887451172" Y="-28.897994140625" />
                  <Point X="-1.643027587891" Y="-28.890966796875" />
                  <Point X="-1.83819909668" Y="-28.878173828125" />
                  <Point X="-1.952616821289" Y="-28.87067578125" />
                  <Point X="-1.983415771484" Y="-28.873708984375" />
                  <Point X="-2.014463745117" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.205284912109" Y="-29.00346484375" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312789306641" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.418219482422" Y="-29.207783203125" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.645609863281" Y="-29.1860390625" />
                  <Point X="-2.801726318359" Y="-29.089376953125" />
                  <Point X="-3.053984619141" Y="-28.89514453125" />
                  <Point X="-3.104721923828" Y="-28.856080078125" />
                  <Point X="-2.893069091797" Y="-28.489484375" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405575927734" Y="-27.585189453125" />
                  <Point X="-2.414561767578" Y="-27.5555703125" />
                  <Point X="-2.428780761719" Y="-27.526740234375" />
                  <Point X="-2.446809082031" Y="-27.501583984375" />
                  <Point X="-2.464154052734" Y="-27.484240234375" />
                  <Point X="-2.489305664062" Y="-27.466216796875" />
                  <Point X="-2.518135253906" Y="-27.451998046875" />
                  <Point X="-2.547754882812" Y="-27.44301171875" />
                  <Point X="-2.578691162109" Y="-27.444025390625" />
                  <Point X="-2.610218261719" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.115718017578" Y="-27.736326171875" />
                  <Point X="-3.818022705078" Y="-28.141802734375" />
                  <Point X="-3.959532714844" Y="-27.95588671875" />
                  <Point X="-4.082861328125" Y="-27.793859375" />
                  <Point X="-4.263720214844" Y="-27.4905859375" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.927090820312" Y="-27.12859375" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084576171875" Y="-26.47559375" />
                  <Point X="-3.066611083984" Y="-26.4484609375" />
                  <Point X="-3.053856201172" Y="-26.41983203125" />
                  <Point X="-3.046151611328" Y="-26.390083984375" />
                  <Point X="-3.043347412109" Y="-26.359654296875" />
                  <Point X="-3.045556884766" Y="-26.327982421875" />
                  <Point X="-3.052559814453" Y="-26.298234375" />
                  <Point X="-3.068645019531" Y="-26.27225" />
                  <Point X="-3.089479492188" Y="-26.248294921875" />
                  <Point X="-3.112976074219" Y="-26.228765625" />
                  <Point X="-3.139458740234" Y="-26.2131796875" />
                  <Point X="-3.168722412109" Y="-26.20195703125" />
                  <Point X="-3.200607666016" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-3.833508789062" Y="-26.273583984375" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.785842773438" Y="-26.1814921875" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.881928710938" Y="-25.6580859375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.468273925781" Y="-25.471048828125" />
                  <Point X="-3.532875732422" Y="-25.22041015625" />
                  <Point X="-3.517493896484" Y="-25.214828125" />
                  <Point X="-3.487729736328" Y="-25.199470703125" />
                  <Point X="-3.470233642578" Y="-25.187328125" />
                  <Point X="-3.459976806641" Y="-25.180208984375" />
                  <Point X="-3.437517578125" Y="-25.158322265625" />
                  <Point X="-3.418274658203" Y="-25.13206640625" />
                  <Point X="-3.404168457031" Y="-25.104068359375" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.390647949219" Y="-25.046103515625" />
                  <Point X="-3.390647216797" Y="-25.01646484375" />
                  <Point X="-3.394916259766" Y="-24.9883046875" />
                  <Point X="-3.404167236328" Y="-24.95849609375" />
                  <Point X="-3.418272949219" Y="-24.930498046875" />
                  <Point X="-3.437516601563" Y="-24.904240234375" />
                  <Point X="-3.459976806641" Y="-24.8823515625" />
                  <Point X="-3.477472900391" Y="-24.870208984375" />
                  <Point X="-3.485521240234" Y="-24.865197265625" />
                  <Point X="-3.511347167969" Y="-24.850857421875" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-4.081244873047" Y="-24.695216796875" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.855573730469" Y="-24.233099609375" />
                  <Point X="-4.824488769531" Y="-24.023029296875" />
                  <Point X="-4.728165527344" Y="-23.66756640625" />
                  <Point X="-4.703551269531" Y="-23.576732421875" />
                  <Point X="-4.436182617188" Y="-23.61193359375" />
                  <Point X="-3.765666503906" Y="-23.70020703125" />
                  <Point X="-3.744985107422" Y="-23.700658203125" />
                  <Point X="-3.723422851562" Y="-23.698771484375" />
                  <Point X="-3.703138427734" Y="-23.694736328125" />
                  <Point X="-3.6644140625" Y="-23.68252734375" />
                  <Point X="-3.641712402344" Y="-23.675369140625" />
                  <Point X="-3.622776367188" Y="-23.667037109375" />
                  <Point X="-3.604032470703" Y="-23.65621484375" />
                  <Point X="-3.587351318359" Y="-23.643984375" />
                  <Point X="-3.573713134766" Y="-23.628431640625" />
                  <Point X="-3.561298828125" Y="-23.610701171875" />
                  <Point X="-3.5513515625" Y="-23.592568359375" />
                  <Point X="-3.535813232422" Y="-23.555056640625" />
                  <Point X="-3.526704101563" Y="-23.533064453125" />
                  <Point X="-3.520915283203" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532050048828" Y="-23.410623046875" />
                  <Point X="-3.550798583984" Y="-23.374607421875" />
                  <Point X="-3.561789794922" Y="-23.353494140625" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-3.916681396484" Y="-23.06405078125" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.201939941406" Y="-22.473275390625" />
                  <Point X="-4.08115625" Y="-22.266345703125" />
                  <Point X="-3.825999755859" Y="-21.938376953125" />
                  <Point X="-3.750504150391" Y="-21.841337890625" />
                  <Point X="-3.618469238281" Y="-21.917568359375" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187728271484" Y="-22.163658203125" />
                  <Point X="-3.167085205078" Y="-22.17016796875" />
                  <Point X="-3.146794189453" Y="-22.174205078125" />
                  <Point X="-3.092862060547" Y="-22.178923828125" />
                  <Point X="-3.061244873047" Y="-22.181689453125" />
                  <Point X="-3.040559570312" Y="-22.18123828125" />
                  <Point X="-3.0191015625" Y="-22.178412109375" />
                  <Point X="-2.999012207031" Y="-22.173494140625" />
                  <Point X="-2.980462890625" Y="-22.16434765625" />
                  <Point X="-2.962209228516" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.907796386719" Y="-22.101490234375" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.848154296875" Y="-21.909947265625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-3.009179931641" Y="-21.577046875" />
                  <Point X="-3.183333007812" Y="-21.27540625" />
                  <Point X="-2.910992431641" Y="-21.066603515625" />
                  <Point X="-2.700620849609" Y="-20.905314453125" />
                  <Point X="-2.298768066406" Y="-20.6820546875" />
                  <Point X="-2.167036376953" Y="-20.6088671875" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.02889453125" Y="-20.785197265625" />
                  <Point X="-2.012316162109" Y="-20.799109375" />
                  <Point X="-1.995114868164" Y="-20.810603515625" />
                  <Point X="-1.935088745117" Y="-20.8418515625" />
                  <Point X="-1.899898803711" Y="-20.860171875" />
                  <Point X="-1.880626342773" Y="-20.86766796875" />
                  <Point X="-1.859719238281" Y="-20.873271484375" />
                  <Point X="-1.839269775391" Y="-20.876419921875" />
                  <Point X="-1.818623657227" Y="-20.87506640625" />
                  <Point X="-1.797307617188" Y="-20.871306640625" />
                  <Point X="-1.777452880859" Y="-20.865517578125" />
                  <Point X="-1.714931518555" Y="-20.839619140625" />
                  <Point X="-1.678278930664" Y="-20.8244375" />
                  <Point X="-1.660147094727" Y="-20.814490234375" />
                  <Point X="-1.642417236328" Y="-20.802076171875" />
                  <Point X="-1.626864868164" Y="-20.7884375" />
                  <Point X="-1.614633300781" Y="-20.771755859375" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734078125" />
                  <Point X="-1.575130737305" Y="-20.669537109375" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.573577026367" Y="-20.44880859375" />
                  <Point X="-1.584202026367" Y="-20.368103515625" />
                  <Point X="-1.221968139648" Y="-20.266544921875" />
                  <Point X="-0.949623657227" Y="-20.19019140625" />
                  <Point X="-0.462466247559" Y="-20.133173828125" />
                  <Point X="-0.294711608887" Y="-20.113541015625" />
                  <Point X="-0.255728775024" Y="-20.25902734375" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113960266" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155894279" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425964355" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.21763432312" Y="-20.447146484375" />
                  <Point X="0.307419647217" Y="-20.112060546875" />
                  <Point X="0.606247375488" Y="-20.143357421875" />
                  <Point X="0.844030944824" Y="-20.168259765625" />
                  <Point X="1.247093017578" Y="-20.265572265625" />
                  <Point X="1.481039916992" Y="-20.3220546875" />
                  <Point X="1.742827880859" Y="-20.417005859375" />
                  <Point X="1.894645751953" Y="-20.472072265625" />
                  <Point X="2.148325195313" Y="-20.590708984375" />
                  <Point X="2.294557373047" Y="-20.65909765625" />
                  <Point X="2.539666748047" Y="-20.8018984375" />
                  <Point X="2.680990478516" Y="-20.884232421875" />
                  <Point X="2.912109130859" Y="-21.048591796875" />
                  <Point X="2.943260742188" Y="-21.07074609375" />
                  <Point X="2.69076953125" Y="-21.508072265625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142077148438" Y="-22.46006640625" />
                  <Point X="2.133076660156" Y="-22.483943359375" />
                  <Point X="2.119541748047" Y="-22.53455859375" />
                  <Point X="2.111606933594" Y="-22.56423046875" />
                  <Point X="2.108619140625" Y="-22.58206640625" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.113005615234" Y="-22.662814453125" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121443115234" Y="-22.7103984375" />
                  <Point X="2.129709716797" Y="-22.732486328125" />
                  <Point X="2.140070800781" Y="-22.75252734375" />
                  <Point X="2.16715234375" Y="-22.792439453125" />
                  <Point X="2.183028564453" Y="-22.8158359375" />
                  <Point X="2.194464599609" Y="-22.829669921875" />
                  <Point X="2.221596923828" Y="-22.85440625" />
                  <Point X="2.261508056641" Y="-22.88148828125" />
                  <Point X="2.284905517578" Y="-22.897365234375" />
                  <Point X="2.304947509766" Y="-22.9077265625" />
                  <Point X="2.327037109375" Y="-22.915994140625" />
                  <Point X="2.348964599609" Y="-22.921337890625" />
                  <Point X="2.392731445312" Y="-22.926615234375" />
                  <Point X="2.418389404297" Y="-22.929708984375" />
                  <Point X="2.43646875" Y="-22.93015625" />
                  <Point X="2.473207763672" Y="-22.925830078125" />
                  <Point X="2.523822021484" Y="-22.912294921875" />
                  <Point X="2.553494140625" Y="-22.904359375" />
                  <Point X="2.565290771484" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.140086914062" Y="-22.5714140625" />
                  <Point X="3.967325683594" Y="-22.09380859375" />
                  <Point X="4.039450195312" Y="-22.194044921875" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.252124511719" Y="-22.52346875" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.945942382812" Y="-22.782787109375" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221421386719" Y="-23.33976171875" />
                  <Point X="3.203974121094" Y="-23.358373046875" />
                  <Point X="3.167546875" Y="-23.40589453125" />
                  <Point X="3.146191894531" Y="-23.43375390625" />
                  <Point X="3.136606689453" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.108060791016" Y="-23.53143359375" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.108878417969" Y="-23.682216796875" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120678955078" Y="-23.73101953125" />
                  <Point X="3.136282714844" Y="-23.76426171875" />
                  <Point X="3.166579101562" Y="-23.810310546875" />
                  <Point X="3.184340332031" Y="-23.837306640625" />
                  <Point X="3.198897705078" Y="-23.8545546875" />
                  <Point X="3.216141601562" Y="-23.870642578125" />
                  <Point X="3.234345947266" Y="-23.88396484375" />
                  <Point X="3.278249755859" Y="-23.9086796875" />
                  <Point X="3.303988037109" Y="-23.92316796875" />
                  <Point X="3.32051953125" Y="-23.930498046875" />
                  <Point X="3.356120117188" Y="-23.9405625" />
                  <Point X="3.415480957031" Y="-23.94840625" />
                  <Point X="3.450280761719" Y="-23.953005859375" />
                  <Point X="3.462697265625" Y="-23.95382421875" />
                  <Point X="3.488203369141" Y="-23.953015625" />
                  <Point X="4.012141845703" Y="-23.8840390625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.809935058594" Y="-23.919310546875" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.886541015625" Y="-24.327990234375" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.536166992188" Y="-24.45080078125" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704787353516" Y="-24.674416015625" />
                  <Point X="3.681547607422" Y="-24.684931640625" />
                  <Point X="3.623227294922" Y="-24.718640625" />
                  <Point X="3.589037597656" Y="-24.73840234375" />
                  <Point X="3.574312744141" Y="-24.74890234375" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.512538085938" Y="-24.819013671875" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480301269531" Y="-24.864431640625" />
                  <Point X="3.470527099609" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.452016845703" Y="-24.968302734375" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443483154297" Y="-25.021876953125" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.456843017578" Y="-25.119458984375" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.1766640625" />
                  <Point X="3.480299316406" Y="-25.198126953125" />
                  <Point X="3.492024658203" Y="-25.217408203125" />
                  <Point X="3.527016845703" Y="-25.26199609375" />
                  <Point X="3.547530517578" Y="-25.28813671875" />
                  <Point X="3.559998291016" Y="-25.301236328125" />
                  <Point X="3.589035888672" Y="-25.32415625" />
                  <Point X="3.647356201172" Y="-25.357865234375" />
                  <Point X="3.681545898438" Y="-25.37762890625" />
                  <Point X="3.692698486328" Y="-25.383134765625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.197055175781" Y="-25.52089453125" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.875124023438" Y="-25.8153984375" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.803000488281" Y="-26.176693359375" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.374872558594" Y="-26.128576171875" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374658935547" Y="-26.005509765625" />
                  <Point X="3.260197021484" Y="-26.030388671875" />
                  <Point X="3.193094726563" Y="-26.04497265625" />
                  <Point X="3.163974609375" Y="-26.05659765625" />
                  <Point X="3.136147705078" Y="-26.073490234375" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.043212158203" Y="-26.177169921875" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.959841552734" Y="-26.413123046875" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.507564453125" />
                  <Point X="2.964078613281" Y="-26.539185546875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.039795654297" Y="-26.666525390625" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086931396484" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.556512695312" Y="-27.103048828125" />
                  <Point X="4.213122558594" Y="-27.6068828125" />
                  <Point X="4.181473632812" Y="-27.658095703125" />
                  <Point X="4.124810546875" Y="-27.749787109375" />
                  <Point X="4.028980957031" Y="-27.885947265625" />
                  <Point X="3.647494628906" Y="-27.6656953125" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786137451172" Y="-27.170015625" />
                  <Point X="2.754224365234" Y="-27.15982421875" />
                  <Point X="2.617996337891" Y="-27.135220703125" />
                  <Point X="2.538134033203" Y="-27.120798828125" />
                  <Point X="2.506777587891" Y="-27.120396484375" />
                  <Point X="2.474605224609" Y="-27.12535546875" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.331661621094" Y="-27.19473828125" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242385009766" Y="-27.246548828125" />
                  <Point X="2.221425537109" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.144970214844" Y="-27.403611328125" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.120278076172" Y="-27.699486328125" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.438344482422" Y="-28.32235546875" />
                  <Point X="2.861283691406" Y="-29.05490625" />
                  <Point X="2.849088623047" Y="-29.0636171875" />
                  <Point X="2.781858642578" Y="-29.11163671875" />
                  <Point X="2.701763916016" Y="-29.16348046875" />
                  <Point X="2.404534179688" Y="-28.776125" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506469727" Y="-27.922181640625" />
                  <Point X="1.721923706055" Y="-27.900556640625" />
                  <Point X="1.587566162109" Y="-27.814177734375" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986694336" Y="-27.75116796875" />
                  <Point X="1.448366699219" Y="-27.7434375" />
                  <Point X="1.417100708008" Y="-27.741119140625" />
                  <Point X="1.270157714844" Y="-27.754640625" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.156363769531" Y="-27.7693984375" />
                  <Point X="1.128977539062" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.795462890625" />
                  <Point X="0.991129577637" Y="-27.889806640625" />
                  <Point X="0.924611328125" Y="-27.945115234375" />
                  <Point X="0.90414074707" Y="-27.968865234375" />
                  <Point X="0.887248779297" Y="-27.99669140625" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.841698730469" Y="-28.18189453125" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.900941162109" Y="-28.939888671875" />
                  <Point X="1.022065429688" Y="-29.859916015625" />
                  <Point X="0.975713806152" Y="-29.87007421875" />
                  <Point X="0.929315246582" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058457763672" Y="-29.75262890625" />
                  <Point X="-1.141246459961" Y="-29.731330078125" />
                  <Point X="-1.120775512695" Y="-29.575837890625" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.1614921875" Y="-29.305861328125" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.17847265625" />
                  <Point X="-1.199026000977" Y="-29.149505859375" />
                  <Point X="-1.205664916992" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230573242188" Y="-29.094861328125" />
                  <Point X="-1.250208374023" Y="-29.070935546875" />
                  <Point X="-1.261007080078" Y="-29.05977734375" />
                  <Point X="-1.408059570312" Y="-28.93081640625" />
                  <Point X="-1.494268066406" Y="-28.855212890625" />
                  <Point X="-1.506740844727" Y="-28.845962890625" />
                  <Point X="-1.533024902344" Y="-28.829619140625" />
                  <Point X="-1.54683581543" Y="-28.822525390625" />
                  <Point X="-1.576533569336" Y="-28.810224609375" />
                  <Point X="-1.59131628418" Y="-28.8054765625" />
                  <Point X="-1.621456420898" Y="-28.79844921875" />
                  <Point X="-1.636813964844" Y="-28.796169921875" />
                  <Point X="-1.831985473633" Y="-28.783376953125" />
                  <Point X="-1.946403198242" Y="-28.77587890625" />
                  <Point X="-1.961927612305" Y="-28.7761328125" />
                  <Point X="-1.99272668457" Y="-28.779166015625" />
                  <Point X="-2.008001098633" Y="-28.7819453125" />
                  <Point X="-2.039049072266" Y="-28.790263671875" />
                  <Point X="-2.053668212891" Y="-28.795494140625" />
                  <Point X="-2.081862060547" Y="-28.808267578125" />
                  <Point X="-2.095436767578" Y="-28.815810546875" />
                  <Point X="-2.258063964844" Y="-28.924474609375" />
                  <Point X="-2.353402832031" Y="-28.988177734375" />
                  <Point X="-2.359685302734" Y="-28.9927578125" />
                  <Point X="-2.380448242188" Y="-29.01013671875" />
                  <Point X="-2.402761962891" Y="-29.032775390625" />
                  <Point X="-2.410471923828" Y="-29.041630859375" />
                  <Point X="-2.493588378906" Y="-29.149951171875" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.595599121094" Y="-29.105267578125" />
                  <Point X="-2.747611572266" Y="-29.011146484375" />
                  <Point X="-2.980862548828" Y="-28.831548828125" />
                  <Point X="-2.810796630859" Y="-28.536984375" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.619232421875" />
                  <Point X="-2.310626708984" Y="-27.588296875" />
                  <Point X="-2.314667480469" Y="-27.557609375" />
                  <Point X="-2.323653320313" Y="-27.527990234375" />
                  <Point X="-2.329360595703" Y="-27.513548828125" />
                  <Point X="-2.343579589844" Y="-27.48471875" />
                  <Point X="-2.351562744141" Y="-27.47140234375" />
                  <Point X="-2.369591064453" Y="-27.44624609375" />
                  <Point X="-2.379636230469" Y="-27.43440625" />
                  <Point X="-2.396981201172" Y="-27.4170625" />
                  <Point X="-2.408818603516" Y="-27.40701953125" />
                  <Point X="-2.433970214844" Y="-27.38899609375" />
                  <Point X="-2.447284423828" Y="-27.381015625" />
                  <Point X="-2.476114013672" Y="-27.366796875" />
                  <Point X="-2.490554443359" Y="-27.36108984375" />
                  <Point X="-2.520174072266" Y="-27.352103515625" />
                  <Point X="-2.550865966797" Y="-27.3480625" />
                  <Point X="-2.581802246094" Y="-27.349076171875" />
                  <Point X="-2.597225830078" Y="-27.3508515625" />
                  <Point X="-2.628752929688" Y="-27.357123046875" />
                  <Point X="-2.64368359375" Y="-27.36138671875" />
                  <Point X="-2.672648925781" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.163218017578" Y="-27.6540546875" />
                  <Point X="-3.793087646484" Y="-28.0177109375" />
                  <Point X="-3.883939208984" Y="-27.898349609375" />
                  <Point X="-4.004014892578" Y="-27.740595703125" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.869258300781" Y="-27.203962890625" />
                  <Point X="-3.048122070312" Y="-26.573884765625" />
                  <Point X="-3.036481201172" Y="-26.563310546875" />
                  <Point X="-3.015102783203" Y="-26.540388671875" />
                  <Point X="-3.005365478516" Y="-26.528041015625" />
                  <Point X="-2.987400390625" Y="-26.500908203125" />
                  <Point X="-2.979833740234" Y="-26.487123046875" />
                  <Point X="-2.967078857422" Y="-26.458494140625" />
                  <Point X="-2.961890625" Y="-26.443650390625" />
                  <Point X="-2.954186035156" Y="-26.41390234375" />
                  <Point X="-2.951552490234" Y="-26.39880078125" />
                  <Point X="-2.948748291016" Y="-26.36837109375" />
                  <Point X="-2.948577636719" Y="-26.35304296875" />
                  <Point X="-2.950787109375" Y="-26.32137109375" />
                  <Point X="-2.953084716797" Y="-26.306212890625" />
                  <Point X="-2.960087646484" Y="-26.27646484375" />
                  <Point X="-2.971784179688" Y="-26.248232421875" />
                  <Point X="-2.987869384766" Y="-26.222248046875" />
                  <Point X="-2.996963378906" Y="-26.20990625" />
                  <Point X="-3.017797851562" Y="-26.185951171875" />
                  <Point X="-3.028755859375" Y="-26.175236328125" />
                  <Point X="-3.052252441406" Y="-26.15570703125" />
                  <Point X="-3.064791015625" Y="-26.146892578125" />
                  <Point X="-3.091273681641" Y="-26.131306640625" />
                  <Point X="-3.105441894531" Y="-26.124478515625" />
                  <Point X="-3.134705566406" Y="-26.113255859375" />
                  <Point X="-3.149801025391" Y="-26.108861328125" />
                  <Point X="-3.181686279297" Y="-26.102380859375" />
                  <Point X="-3.197298095703" Y="-26.10053515625" />
                  <Point X="-3.228619384766" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-3.845908691406" Y="-26.179396484375" />
                  <Point X="-4.660920898438" Y="-26.286693359375" />
                  <Point X="-4.693798339844" Y="-26.15798046875" />
                  <Point X="-4.74076171875" Y="-25.97412109375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.443686035156" Y="-25.5628125" />
                  <Point X="-3.508287597656" Y="-25.312173828125" />
                  <Point X="-3.500468505859" Y="-25.3097109375" />
                  <Point X="-3.473933349609" Y="-25.299251953125" />
                  <Point X="-3.444169189453" Y="-25.28389453125" />
                  <Point X="-3.433564697266" Y="-25.277515625" />
                  <Point X="-3.416068603516" Y="-25.265373046875" />
                  <Point X="-3.393674316406" Y="-25.24824609375" />
                  <Point X="-3.371215087891" Y="-25.226359375" />
                  <Point X="-3.360893310547" Y="-25.21448046875" />
                  <Point X="-3.341650390625" Y="-25.188224609375" />
                  <Point X="-3.333434326172" Y="-25.174810546875" />
                  <Point X="-3.319328125" Y="-25.1468125" />
                  <Point X="-3.313437988281" Y="-25.132228515625" />
                  <Point X="-3.304187011719" Y="-25.102421875" />
                  <Point X="-3.300990966797" Y="-25.08850390625" />
                  <Point X="-3.296721435547" Y="-25.060345703125" />
                  <Point X="-3.295647949219" Y="-25.04610546875" />
                  <Point X="-3.295647216797" Y="-25.016466796875" />
                  <Point X="-3.296720458984" Y="-25.0022265625" />
                  <Point X="-3.300989501953" Y="-24.97406640625" />
                  <Point X="-3.304185302734" Y="-24.960146484375" />
                  <Point X="-3.313436279297" Y="-24.930337890625" />
                  <Point X="-3.319326416016" Y="-24.915751953125" />
                  <Point X="-3.333432128906" Y="-24.88775390625" />
                  <Point X="-3.341647705078" Y="-24.874341796875" />
                  <Point X="-3.360891357422" Y="-24.848083984375" />
                  <Point X="-3.371212646484" Y="-24.836205078125" />
                  <Point X="-3.393672851562" Y="-24.81431640625" />
                  <Point X="-3.405811767578" Y="-24.804306640625" />
                  <Point X="-3.423307861328" Y="-24.7921640625" />
                  <Point X="-3.439404541016" Y="-24.782140625" />
                  <Point X="-3.46523046875" Y="-24.76780078125" />
                  <Point X="-3.475728759766" Y="-24.762787109375" />
                  <Point X="-3.4972578125" Y="-24.754080078125" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-4.056657226562" Y="-24.603453125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.761597167969" Y="-24.247005859375" />
                  <Point X="-4.731332519531" Y="-24.042478515625" />
                  <Point X="-4.636472167969" Y="-23.6924140625" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.448583007812" Y="-23.70612109375" />
                  <Point X="-3.778066894531" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747057128906" Y="-23.795634765625" />
                  <Point X="-3.736704101562" Y="-23.795296875" />
                  <Point X="-3.715141845703" Y="-23.79341015625" />
                  <Point X="-3.704887695313" Y="-23.7919453125" />
                  <Point X="-3.684603271484" Y="-23.78791015625" />
                  <Point X="-3.674572998047" Y="-23.78533984375" />
                  <Point X="-3.635848632812" Y="-23.773130859375" />
                  <Point X="-3.613146972656" Y="-23.76597265625" />
                  <Point X="-3.603451660156" Y="-23.76232421875" />
                  <Point X="-3.584515625" Y="-23.7539921875" />
                  <Point X="-3.575274902344" Y="-23.74930859375" />
                  <Point X="-3.556531005859" Y="-23.738486328125" />
                  <Point X="-3.547860107422" Y="-23.732828125" />
                  <Point X="-3.531178955078" Y="-23.72059765625" />
                  <Point X="-3.515923828125" Y="-23.706619140625" />
                  <Point X="-3.502285644531" Y="-23.69106640625" />
                  <Point X="-3.495892333984" Y="-23.682919921875" />
                  <Point X="-3.483478027344" Y="-23.665189453125" />
                  <Point X="-3.478008300781" Y="-23.656392578125" />
                  <Point X="-3.468061035156" Y="-23.638259765625" />
                  <Point X="-3.463583496094" Y="-23.628923828125" />
                  <Point X="-3.448045166016" Y="-23.591412109375" />
                  <Point X="-3.438936035156" Y="-23.569419921875" />
                  <Point X="-3.435499755859" Y="-23.559650390625" />
                  <Point X="-3.4297109375" Y="-23.539791015625" />
                  <Point X="-3.427358398438" Y="-23.529701171875" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436012695312" Y="-23.39546875" />
                  <Point X="-3.443509277344" Y="-23.37619140625" />
                  <Point X="-3.447783935547" Y="-23.3667578125" />
                  <Point X="-3.466532470703" Y="-23.3307421875" />
                  <Point X="-3.477523681641" Y="-23.30962890625" />
                  <Point X="-3.482799804688" Y="-23.30071484375" />
                  <Point X="-3.494291748047" Y="-23.283515625" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521498046875" Y="-23.251091796875" />
                  <Point X="-3.536439941406" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-3.858849121094" Y="-22.988681640625" />
                  <Point X="-4.227614746094" Y="-22.70571875" />
                  <Point X="-4.119893554687" Y="-22.5211640625" />
                  <Point X="-4.002296386719" Y="-22.319693359375" />
                  <Point X="-3.751019042969" Y="-21.9967109375" />
                  <Point X="-3.726337402344" Y="-21.964986328125" />
                  <Point X="-3.665969238281" Y="-21.99983984375" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244922607422" Y="-22.24228125" />
                  <Point X="-3.225993896484" Y="-22.250611328125" />
                  <Point X="-3.216299560547" Y="-22.254259765625" />
                  <Point X="-3.195656494141" Y="-22.26076953125" />
                  <Point X="-3.185623046875" Y="-22.263341796875" />
                  <Point X="-3.16533203125" Y="-22.26737890625" />
                  <Point X="-3.155074462891" Y="-22.26884375" />
                  <Point X="-3.101142333984" Y="-22.2735625" />
                  <Point X="-3.069525146484" Y="-22.276328125" />
                  <Point X="-3.059173339844" Y="-22.276666015625" />
                  <Point X="-3.038488037109" Y="-22.27621484375" />
                  <Point X="-3.028154541016" Y="-22.27542578125" />
                  <Point X="-3.006696533203" Y="-22.272599609375" />
                  <Point X="-2.996512207031" Y="-22.2706875" />
                  <Point X="-2.976422851562" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.93844921875" Y="-22.249552734375" />
                  <Point X="-2.929419189453" Y="-22.244470703125" />
                  <Point X="-2.911165527344" Y="-22.232841796875" />
                  <Point X="-2.902745117188" Y="-22.226806640625" />
                  <Point X="-2.886613769531" Y="-22.213859375" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.840621337891" Y="-22.168666015625" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.753515869141" Y="-21.90166796875" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.926907714844" Y="-21.529546875" />
                  <Point X="-3.059387939453" Y="-21.3000859375" />
                  <Point X="-2.853189941406" Y="-21.141994140625" />
                  <Point X="-2.648377441406" Y="-20.984966796875" />
                  <Point X="-2.252630615234" Y="-20.765099609375" />
                  <Point X="-2.192523193359" Y="-20.731705078125" />
                  <Point X="-2.118563720703" Y="-20.82808984375" />
                  <Point X="-2.111821289062" Y="-20.835951171875" />
                  <Point X="-2.097520263672" Y="-20.850890625" />
                  <Point X="-2.089962402344" Y="-20.85796875" />
                  <Point X="-2.073384033203" Y="-20.871880859375" />
                  <Point X="-2.065097167969" Y="-20.87809765625" />
                  <Point X="-2.047895996094" Y="-20.889591796875" />
                  <Point X="-2.038981445312" Y="-20.894869140625" />
                  <Point X="-1.978955444336" Y="-20.9261171875" />
                  <Point X="-1.943765380859" Y="-20.9444375" />
                  <Point X="-1.93433605957" Y="-20.9487109375" />
                  <Point X="-1.915063720703" Y="-20.95620703125" />
                  <Point X="-1.905220092773" Y="-20.9594296875" />
                  <Point X="-1.884313110352" Y="-20.965033203125" />
                  <Point X="-1.874175292969" Y="-20.967166015625" />
                  <Point X="-1.853725830078" Y="-20.970314453125" />
                  <Point X="-1.833055053711" Y="-20.971216796875" />
                  <Point X="-1.812409057617" Y="-20.96986328125" />
                  <Point X="-1.802122070312" Y="-20.968623046875" />
                  <Point X="-1.780806030273" Y="-20.96486328125" />
                  <Point X="-1.770715698242" Y="-20.962509765625" />
                  <Point X="-1.750860961914" Y="-20.956720703125" />
                  <Point X="-1.741096435547" Y="-20.95328515625" />
                  <Point X="-1.678575073242" Y="-20.92738671875" />
                  <Point X="-1.641922485352" Y="-20.912205078125" />
                  <Point X="-1.632585693359" Y="-20.9077265625" />
                  <Point X="-1.614453857422" Y="-20.897779296875" />
                  <Point X="-1.605658813477" Y="-20.892310546875" />
                  <Point X="-1.587928955078" Y="-20.879896484375" />
                  <Point X="-1.579780395508" Y="-20.873501953125" />
                  <Point X="-1.564228027344" Y="-20.85986328125" />
                  <Point X="-1.550252807617" Y="-20.844611328125" />
                  <Point X="-1.538021240234" Y="-20.8279296875" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.52153918457" Y="-20.80051171875" />
                  <Point X="-1.516856445312" Y="-20.791271484375" />
                  <Point X="-1.508525512695" Y="-20.772337890625" />
                  <Point X="-1.504877319336" Y="-20.76264453125" />
                  <Point X="-1.484527587891" Y="-20.698103515625" />
                  <Point X="-1.47259777832" Y="-20.660267578125" />
                  <Point X="-1.470026611328" Y="-20.650236328125" />
                  <Point X="-1.465991333008" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266357422" Y="-20.437345703125" />
                  <Point X="-1.196322143555" Y="-20.358017578125" />
                  <Point X="-0.931164794922" Y="-20.2836796875" />
                  <Point X="-0.451422729492" Y="-20.227529296875" />
                  <Point X="-0.365222991943" Y="-20.21744140625" />
                  <Point X="-0.347491790771" Y="-20.283615234375" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451187134" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.309397216797" Y="-20.471734375" />
                  <Point X="0.378190795898" Y="-20.2149921875" />
                  <Point X="0.596351928711" Y="-20.23783984375" />
                  <Point X="0.827866333008" Y="-20.2620859375" />
                  <Point X="1.224797485352" Y="-20.35791796875" />
                  <Point X="1.453607421875" Y="-20.41316015625" />
                  <Point X="1.710435913086" Y="-20.5063125" />
                  <Point X="1.858248168945" Y="-20.55992578125" />
                  <Point X="2.108080566406" Y="-20.676763671875" />
                  <Point X="2.250429199219" Y="-20.7433359375" />
                  <Point X="2.491843994141" Y="-20.883984375" />
                  <Point X="2.629446533203" Y="-20.964150390625" />
                  <Point X="2.817780761719" Y="-21.098083984375" />
                  <Point X="2.608497070312" Y="-21.460572265625" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.062373291016" Y="-22.40689453125" />
                  <Point X="2.053183105469" Y="-22.426556640625" />
                  <Point X="2.044182617188" Y="-22.45043359375" />
                  <Point X="2.041301269531" Y="-22.45940234375" />
                  <Point X="2.027766235352" Y="-22.510017578125" />
                  <Point X="2.019831665039" Y="-22.539689453125" />
                  <Point X="2.017912475586" Y="-22.54853515625" />
                  <Point X="2.013646606445" Y="-22.57977734375" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.018688842773" Y="-22.6741875" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.023801025391" Y="-22.710966796875" />
                  <Point X="2.029144775391" Y="-22.732892578125" />
                  <Point X="2.032470214844" Y="-22.743697265625" />
                  <Point X="2.040736694336" Y="-22.76578515625" />
                  <Point X="2.045320556641" Y="-22.776115234375" />
                  <Point X="2.055681640625" Y="-22.79615625" />
                  <Point X="2.061458984375" Y="-22.8058671875" />
                  <Point X="2.088540527344" Y="-22.845779296875" />
                  <Point X="2.104416748047" Y="-22.86917578125" />
                  <Point X="2.109807861328" Y="-22.876365234375" />
                  <Point X="2.130460693359" Y="-22.899873046875" />
                  <Point X="2.157593017578" Y="-22.924609375" />
                  <Point X="2.168254882812" Y="-22.933017578125" />
                  <Point X="2.208166015625" Y="-22.960099609375" />
                  <Point X="2.231563476562" Y="-22.9759765625" />
                  <Point X="2.241277587891" Y="-22.98175390625" />
                  <Point X="2.261319580078" Y="-22.992115234375" />
                  <Point X="2.271647460938" Y="-22.99669921875" />
                  <Point X="2.293737060547" Y="-23.004966796875" />
                  <Point X="2.304543945312" Y="-23.00829296875" />
                  <Point X="2.326471435547" Y="-23.01363671875" />
                  <Point X="2.337592041016" Y="-23.015654296875" />
                  <Point X="2.381358886719" Y="-23.020931640625" />
                  <Point X="2.407016845703" Y="-23.024025390625" />
                  <Point X="2.416039794922" Y="-23.0246796875" />
                  <Point X="2.447578613281" Y="-23.02450390625" />
                  <Point X="2.484317626953" Y="-23.020177734375" />
                  <Point X="2.49775" Y="-23.01760546875" />
                  <Point X="2.548364257812" Y="-23.0040703125" />
                  <Point X="2.578036376953" Y="-22.996134765625" />
                  <Point X="2.584000732422" Y="-22.994328125" />
                  <Point X="2.604419189453" Y="-22.986927734375" />
                  <Point X="2.627662353516" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.187586914062" Y="-22.653685546875" />
                  <Point X="3.940403564453" Y="-22.219046875" />
                  <Point X="3.962337890625" Y="-22.24953125" />
                  <Point X="4.043955810547" Y="-22.362962890625" />
                  <Point X="4.136884765625" Y="-22.51652734375" />
                  <Point X="3.888110351562" Y="-22.70741796875" />
                  <Point X="3.172951904297" Y="-23.2561796875" />
                  <Point X="3.16813671875" Y="-23.2601328125" />
                  <Point X="3.152113769531" Y="-23.2747890625" />
                  <Point X="3.134666503906" Y="-23.293400390625" />
                  <Point X="3.128576904297" Y="-23.300578125" />
                  <Point X="3.092149658203" Y="-23.348099609375" />
                  <Point X="3.070794677734" Y="-23.375958984375" />
                  <Point X="3.065635498047" Y="-23.3833984375" />
                  <Point X="3.049740234375" Y="-23.410626953125" />
                  <Point X="3.034763427734" Y="-23.444453125" />
                  <Point X="3.030140380859" Y="-23.457328125" />
                  <Point X="3.016571289062" Y="-23.50584765625" />
                  <Point X="3.008616455078" Y="-23.53429296875" />
                  <Point X="3.006225341797" Y="-23.545337890625" />
                  <Point X="3.002771728516" Y="-23.56763671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.015838378906" Y="-23.7014140625" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024597900391" Y="-23.741765625" />
                  <Point X="3.034681640625" Y="-23.77138671875" />
                  <Point X="3.050285400391" Y="-23.80462890625" />
                  <Point X="3.056918945312" Y="-23.8164765625" />
                  <Point X="3.087215332031" Y="-23.862525390625" />
                  <Point X="3.1049765625" Y="-23.889521484375" />
                  <Point X="3.111741699219" Y="-23.898580078125" />
                  <Point X="3.126299072266" Y="-23.915828125" />
                  <Point X="3.134091308594" Y="-23.924017578125" />
                  <Point X="3.151335205078" Y="-23.94010546875" />
                  <Point X="3.160037597656" Y="-23.947306640625" />
                  <Point X="3.178241943359" Y="-23.96062890625" />
                  <Point X="3.187743896484" Y="-23.96675" />
                  <Point X="3.231647705078" Y="-23.99146484375" />
                  <Point X="3.257385986328" Y="-24.005953125" />
                  <Point X="3.26548046875" Y="-24.010013671875" />
                  <Point X="3.294675537109" Y="-24.021916015625" />
                  <Point X="3.330276123047" Y="-24.03198046875" />
                  <Point X="3.343675292969" Y="-24.034744140625" />
                  <Point X="3.403036132813" Y="-24.042587890625" />
                  <Point X="3.4378359375" Y="-24.0471875" />
                  <Point X="3.444032958984" Y="-24.04780078125" />
                  <Point X="3.465707519531" Y="-24.04877734375" />
                  <Point X="3.491213623047" Y="-24.04796875" />
                  <Point X="3.500603027344" Y="-24.047203125" />
                  <Point X="4.024541503906" Y="-23.9782265625" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.717630859375" Y="-23.94178125" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.511579101562" Y="-24.359037109375" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686023681641" Y="-24.580458984375" />
                  <Point X="3.665624023438" Y="-24.58786328125" />
                  <Point X="3.642384277344" Y="-24.59837890625" />
                  <Point X="3.6340078125" Y="-24.602681640625" />
                  <Point X="3.5756875" Y="-24.636390625" />
                  <Point X="3.541497802734" Y="-24.65615234375" />
                  <Point X="3.533881835938" Y="-24.661052734375" />
                  <Point X="3.508776123047" Y="-24.680126953125" />
                  <Point X="3.481993652344" Y="-24.7056484375" />
                  <Point X="3.472795166016" Y="-24.715775390625" />
                  <Point X="3.437802978516" Y="-24.760365234375" />
                  <Point X="3.417289306641" Y="-24.78650390625" />
                  <Point X="3.410853027344" Y="-24.795794921875" />
                  <Point X="3.399129882813" Y="-24.81507421875" />
                  <Point X="3.393843017578" Y="-24.8250625" />
                  <Point X="3.384068847656" Y="-24.84652734375" />
                  <Point X="3.380005615234" Y="-24.857072265625" />
                  <Point X="3.373159423828" Y="-24.878572265625" />
                  <Point X="3.370376464844" Y="-24.88952734375" />
                  <Point X="3.358712402344" Y="-24.95043359375" />
                  <Point X="3.351874267578" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584472656" Y="-25.026263671875" />
                  <Point X="3.350280273438" Y="-25.06294140625" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.363538818359" Y="-25.137328125" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373158447266" Y="-25.183984375" />
                  <Point X="3.380003662109" Y="-25.205484375" />
                  <Point X="3.384067382812" Y="-25.216033203125" />
                  <Point X="3.393840332031" Y="-25.23749609375" />
                  <Point X="3.399129638672" Y="-25.24748828125" />
                  <Point X="3.410854980469" Y="-25.26676953125" />
                  <Point X="3.417291015625" Y="-25.27605859375" />
                  <Point X="3.452283203125" Y="-25.320646484375" />
                  <Point X="3.472796875" Y="-25.346787109375" />
                  <Point X="3.478716308594" Y="-25.353630859375" />
                  <Point X="3.501139160156" Y="-25.375806640625" />
                  <Point X="3.530176757813" Y="-25.3987265625" />
                  <Point X="3.54149609375" Y="-25.40640625" />
                  <Point X="3.59981640625" Y="-25.440115234375" />
                  <Point X="3.634006103516" Y="-25.45987890625" />
                  <Point X="3.639491455078" Y="-25.462814453125" />
                  <Point X="3.659139648438" Y="-25.472009765625" />
                  <Point X="3.683021240234" Y="-25.48102734375" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.172467285156" Y="-25.612658203125" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.781185546875" Y="-25.801236328125" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.727801757812" Y="-26.079220703125" />
                  <Point X="4.387272460938" Y="-26.034388671875" />
                  <Point X="3.436781738281" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720458984" Y="-25.910841796875" />
                  <Point X="3.354481201172" Y="-25.912677734375" />
                  <Point X="3.240019287109" Y="-25.937556640625" />
                  <Point X="3.172916992188" Y="-25.952140625" />
                  <Point X="3.157872802734" Y="-25.956744140625" />
                  <Point X="3.128752685547" Y="-25.968369140625" />
                  <Point X="3.114676757812" Y="-25.975390625" />
                  <Point X="3.086849853516" Y="-25.992283203125" />
                  <Point X="3.074125244141" Y="-26.00153125" />
                  <Point X="3.050374755859" Y="-26.022001953125" />
                  <Point X="3.039348876953" Y="-26.033224609375" />
                  <Point X="2.970163818359" Y="-26.11643359375" />
                  <Point X="2.929604736328" Y="-26.165212890625" />
                  <Point X="2.921325683594" Y="-26.17684765625" />
                  <Point X="2.906605957031" Y="-26.201228515625" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.865241210938" Y="-26.40441796875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607177734" Y="-26.51458984375" />
                  <Point X="2.864065429688" Y="-26.530126953125" />
                  <Point X="2.871796875" Y="-26.561748046875" />
                  <Point X="2.876786132812" Y="-26.57666796875" />
                  <Point X="2.889158203125" Y="-26.60548046875" />
                  <Point X="2.896541015625" Y="-26.619373046875" />
                  <Point X="2.959885986328" Y="-26.717900390625" />
                  <Point X="2.997021240234" Y="-26.775662109375" />
                  <Point X="3.001742675781" Y="-26.782353515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486083984" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.498680419922" Y="-27.17841796875" />
                  <Point X="4.087170898438" Y="-27.629982421875" />
                  <Point X="4.045493896484" Y="-27.697423828125" />
                  <Point X="4.0012734375" Y="-27.76025390625" />
                  <Point X="3.694994628906" Y="-27.583423828125" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841200439453" Y="-27.090890625" />
                  <Point X="2.815037597656" Y="-27.079517578125" />
                  <Point X="2.783124511719" Y="-27.069326171875" />
                  <Point X="2.771108642578" Y="-27.0663359375" />
                  <Point X="2.634880615234" Y="-27.041732421875" />
                  <Point X="2.555018310547" Y="-27.027310546875" />
                  <Point X="2.539353027344" Y="-27.025806640625" />
                  <Point X="2.507996582031" Y="-27.025404296875" />
                  <Point X="2.492305419922" Y="-27.026505859375" />
                  <Point X="2.460133056641" Y="-27.03146484375" />
                  <Point X="2.444840820312" Y="-27.035138671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400590087891" Y="-27.051109375" />
                  <Point X="2.28741796875" Y="-27.110669921875" />
                  <Point X="2.221072021484" Y="-27.145587890625" />
                  <Point X="2.208969726562" Y="-27.153169921875" />
                  <Point X="2.1860390625" Y="-27.1700625" />
                  <Point X="2.175210693359" Y="-27.179373046875" />
                  <Point X="2.154251220703" Y="-27.20033203125" />
                  <Point X="2.144939697266" Y="-27.211162109375" />
                  <Point X="2.128046142578" Y="-27.23409375" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.06090234375" Y="-27.3593671875" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.026790405273" Y="-27.716369140625" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.356072021484" Y="-28.36985546875" />
                  <Point X="2.735892822266" Y="-29.02772265625" />
                  <Point X="2.723752197266" Y="-29.03608203125" />
                  <Point X="2.479902587891" Y="-28.71829296875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657714844" Y="-27.87015234375" />
                  <Point X="1.808834594727" Y="-27.84962890625" />
                  <Point X="1.783251831055" Y="-27.82800390625" />
                  <Point X="1.773298339844" Y="-27.820646484375" />
                  <Point X="1.638940795898" Y="-27.734267578125" />
                  <Point X="1.560174804688" Y="-27.68362890625" />
                  <Point X="1.546280151367" Y="-27.676244140625" />
                  <Point X="1.517466552734" Y="-27.663873046875" />
                  <Point X="1.502547851562" Y="-27.65888671875" />
                  <Point X="1.470927856445" Y="-27.65115625" />
                  <Point X="1.455391601563" Y="-27.648697265625" />
                  <Point X="1.424125610352" Y="-27.64637890625" />
                  <Point X="1.408395751953" Y="-27.64651953125" />
                  <Point X="1.261452758789" Y="-27.660041015625" />
                  <Point X="1.17530871582" Y="-27.667966796875" />
                  <Point X="1.161225463867" Y="-27.67033984375" />
                  <Point X="1.133575561523" Y="-27.677171875" />
                  <Point X="1.120008789062" Y="-27.681630859375" />
                  <Point X="1.092622558594" Y="-27.692974609375" />
                  <Point X="1.079876708984" Y="-27.6994140625" />
                  <Point X="1.055494384766" Y="-27.714134765625" />
                  <Point X="1.043857910156" Y="-27.722416015625" />
                  <Point X="0.930392272949" Y="-27.816759765625" />
                  <Point X="0.863874023438" Y="-27.872068359375" />
                  <Point X="0.852651977539" Y="-27.883091796875" />
                  <Point X="0.832181396484" Y="-27.906841796875" />
                  <Point X="0.822932739258" Y="-27.919568359375" />
                  <Point X="0.806040710449" Y="-27.94739453125" />
                  <Point X="0.799019287109" Y="-27.961470703125" />
                  <Point X="0.787394775391" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.748866210938" Y="-28.161716796875" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.806753967285" Y="-28.9522890625" />
                  <Point X="0.833091125488" Y="-29.15233984375" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642143554688" Y="-28.453576171875" />
                  <Point X="0.626786315918" Y="-28.423814453125" />
                  <Point X="0.620407165527" Y="-28.413208984375" />
                  <Point X="0.517189575195" Y="-28.264494140625" />
                  <Point X="0.456679534912" Y="-28.17730859375" />
                  <Point X="0.446669342041" Y="-28.165171875" />
                  <Point X="0.424786834717" Y="-28.14271875" />
                  <Point X="0.412914398193" Y="-28.13240234375" />
                  <Point X="0.386659576416" Y="-28.113158203125" />
                  <Point X="0.373244415283" Y="-28.104939453125" />
                  <Point X="0.345241333008" Y="-28.090830078125" />
                  <Point X="0.330653411865" Y="-28.084939453125" />
                  <Point X="0.170930618286" Y="-28.035369140625" />
                  <Point X="0.077294471741" Y="-28.006306640625" />
                  <Point X="0.06338053894" Y="-28.003111328125" />
                  <Point X="0.035228240967" Y="-27.998841796875" />
                  <Point X="0.020989871979" Y="-27.997767578125" />
                  <Point X="-0.008651926041" Y="-27.997765625" />
                  <Point X="-0.022896093369" Y="-27.998837890625" />
                  <Point X="-0.051061916351" Y="-28.003107421875" />
                  <Point X="-0.064983421326" Y="-28.0063046875" />
                  <Point X="-0.224706375122" Y="-28.055876953125" />
                  <Point X="-0.318342529297" Y="-28.0849375" />
                  <Point X="-0.332927032471" Y="-28.090828125" />
                  <Point X="-0.360928344727" Y="-28.104935546875" />
                  <Point X="-0.374345123291" Y="-28.11315234375" />
                  <Point X="-0.400600524902" Y="-28.132396484375" />
                  <Point X="-0.412477752686" Y="-28.14271875" />
                  <Point X="-0.434361419678" Y="-28.16517578125" />
                  <Point X="-0.444368041992" Y="-28.177310546875" />
                  <Point X="-0.547585327148" Y="-28.32602734375" />
                  <Point X="-0.60809564209" Y="-28.4132109375" />
                  <Point X="-0.612468444824" Y="-28.42012890625" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.790799255371" Y="-29.040453125" />
                  <Point X="-0.985424865723" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.538352808235" Y="-23.694302032807" />
                  <Point X="-4.746077090621" Y="-24.418722695535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.086947264963" Y="-22.464719572629" />
                  <Point X="-4.168958590522" Y="-22.750727053943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.443119664376" Y="-23.706840342842" />
                  <Point X="-4.654300192148" Y="-24.443314365653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.913983642234" Y="-22.206179487927" />
                  <Point X="-4.087953443599" Y="-22.812884286002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.347886280704" Y="-23.71937781655" />
                  <Point X="-4.562523293676" Y="-24.467906035771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.75746793447" Y="-22.005000099445" />
                  <Point X="-4.006948296677" Y="-22.875041518061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.252652897032" Y="-23.731915290259" />
                  <Point X="-4.470746395204" Y="-24.492497705889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.658411079957" Y="-22.004203545719" />
                  <Point X="-3.925943149755" Y="-22.937198750121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.157419513361" Y="-23.744452763967" />
                  <Point X="-4.378969496731" Y="-24.517089376007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.698396369511" Y="-25.631063265888" />
                  <Point X="-4.759399608483" Y="-25.843806842603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.573620001773" Y="-22.053157666412" />
                  <Point X="-3.844938013044" Y="-22.999356017793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.062186129689" Y="-23.756990237676" />
                  <Point X="-4.287192598259" Y="-24.541681046125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.591342468725" Y="-25.602377697482" />
                  <Point X="-4.720586622686" Y="-26.053105626788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.488828923589" Y="-22.102111787105" />
                  <Point X="-3.763932925586" Y="-23.061513457226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.966952746017" Y="-23.769527711385" />
                  <Point X="-4.195415699787" Y="-24.566272716243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.48428856794" Y="-25.573692129076" />
                  <Point X="-4.674026352641" Y="-26.235386419985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.404037845405" Y="-22.151065907798" />
                  <Point X="-3.682927838127" Y="-23.12367089666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.871719362345" Y="-23.782065185093" />
                  <Point X="-4.103638801314" Y="-24.590864386361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.377234788427" Y="-25.545006983594" />
                  <Point X="-4.587124089147" Y="-26.276977962534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.31924676722" Y="-22.200020028491" />
                  <Point X="-3.601922750668" Y="-23.185828336093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776461468573" Y="-23.794517181922" />
                  <Point X="-4.011861857457" Y="-24.615455898204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.270181083012" Y="-25.516322096525" />
                  <Point X="-4.484418484359" Y="-26.263456704393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.93502564619" Y="-21.204737492841" />
                  <Point X="-2.994562300881" Y="-21.412366482349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.233924394122" Y="-22.247121303627" />
                  <Point X="-3.521741475279" Y="-23.250858749637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.675035443304" Y="-23.78545834788" />
                  <Point X="-3.920084866" Y="-24.640047244045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.163127377597" Y="-25.487637209456" />
                  <Point X="-4.38171287957" Y="-26.249935446253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808347470119" Y="-21.107613943355" />
                  <Point X="-2.928529383004" Y="-21.526738082236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.141661276593" Y="-22.270017326384" />
                  <Point X="-3.453176272516" Y="-23.35639922264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.563969502599" Y="-23.742781133511" />
                  <Point X="-3.828307874542" Y="-24.664638589886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056073672182" Y="-25.458952322386" />
                  <Point X="-4.279007274782" Y="-26.236414188113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.681669616136" Y="-21.010491517122" />
                  <Point X="-2.862496469458" Y="-21.64110969723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.044648468692" Y="-22.276349210334" />
                  <Point X="-3.736530883085" Y="-24.689229935728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.949019966767" Y="-25.430267435317" />
                  <Point X="-4.176301669994" Y="-26.222892929972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.561716184128" Y="-20.936819937209" />
                  <Point X="-2.796463556021" Y="-21.755481312604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.938076032333" Y="-22.249342707921" />
                  <Point X="-3.644753891628" Y="-24.713821281569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.841966261352" Y="-25.401582548248" />
                  <Point X="-4.073596065206" Y="-26.209371671832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.44416002152" Y="-20.87150862923" />
                  <Point X="-2.750251766603" Y="-21.938977402173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.805356490768" Y="-22.131150413151" />
                  <Point X="-3.552976900171" Y="-24.738412627411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.734912555937" Y="-25.372897661178" />
                  <Point X="-3.970890460417" Y="-26.195850413692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.326603858912" Y="-20.806197321251" />
                  <Point X="-3.462940042312" Y="-24.769072540297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.627858850522" Y="-25.344212774109" />
                  <Point X="-3.868184855629" Y="-26.182329155552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.20904761509" Y="-20.740885730042" />
                  <Point X="-3.38070806013" Y="-24.826951289351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.520805145107" Y="-25.31552788704" />
                  <Point X="-3.765479216924" Y="-26.16880777913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11676335822" Y="-27.393881167376" />
                  <Point X="-4.147290980773" Y="-27.500343639206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.130692243066" Y="-20.812283825355" />
                  <Point X="-3.312442994261" Y="-24.933538464091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.405214783228" Y="-25.257072140917" />
                  <Point X="-3.662773568826" Y="-26.155286369948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990055576998" Y="-27.296654372459" />
                  <Point X="-4.080552346009" Y="-27.612254111827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.053044922413" Y="-20.886151189246" />
                  <Point X="-3.560067920727" Y="-26.141764960767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.863347809199" Y="-27.199427624349" />
                  <Point X="-4.013813711244" Y="-27.724164584449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.967401364908" Y="-20.932132361241" />
                  <Point X="-3.457362272629" Y="-26.128243551586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.736640315707" Y="-27.102201832866" />
                  <Point X="-3.942769373708" Y="-27.821059287032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.878365857193" Y="-20.966284397085" />
                  <Point X="-3.35465662453" Y="-26.114722142404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.609932822216" Y="-27.004976041383" />
                  <Point X="-3.870984114831" Y="-27.915370089833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.779009775601" Y="-20.964444314518" />
                  <Point X="-3.251950976431" Y="-26.101200733223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.483225328725" Y="-26.907750249901" />
                  <Point X="-3.79919908531" Y="-28.009681692489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.668339551161" Y="-20.923147126765" />
                  <Point X="-3.155015287653" Y="-26.107801563516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.356517835234" Y="-26.810524458418" />
                  <Point X="-3.684734870137" Y="-27.955153286654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.425921162715" Y="-20.422389488909" />
                  <Point X="-1.463830078758" Y="-20.554593590266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.544896733984" Y="-20.837306614615" />
                  <Point X="-3.067019869529" Y="-26.14558082282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.229810341742" Y="-26.713298666935" />
                  <Point X="-3.566299024519" Y="-27.88677415944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.318452931809" Y="-20.392258979653" />
                  <Point X="-2.989529397321" Y="-26.219995182242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103102848251" Y="-26.616072875453" />
                  <Point X="-3.447863178902" Y="-27.818395032227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.210984700902" Y="-20.362128470397" />
                  <Point X="-3.329427333284" Y="-27.750015905014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.103516839999" Y="-20.331999251495" />
                  <Point X="-3.210991487667" Y="-27.6816367778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.996049037553" Y="-20.301870236458" />
                  <Point X="-3.092555691373" Y="-27.613257822599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.890644617041" Y="-20.278937089383" />
                  <Point X="-2.974119928426" Y="-27.544878983692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.788384155716" Y="-20.266968230984" />
                  <Point X="-2.855684165479" Y="-27.476500144785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.68612369439" Y="-20.254999372586" />
                  <Point X="-2.737248402532" Y="-27.408121305878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.583863233065" Y="-20.243030514188" />
                  <Point X="-2.623496618439" Y="-27.356077442482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.481602771739" Y="-20.231061655789" />
                  <Point X="-2.523406624964" Y="-27.351677905015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.915082384457" Y="-28.717613605971" />
                  <Point X="-2.953740894273" Y="-28.852431851482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.379342601808" Y="-20.219093813601" />
                  <Point X="-2.435087043491" Y="-27.388326672373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.718737430204" Y="-28.377533127997" />
                  <Point X="-2.872786197916" Y="-28.91476502557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.324074895128" Y="-20.37100816651" />
                  <Point X="-2.357646105734" Y="-27.462913778957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.522393027708" Y="-28.037454574228" />
                  <Point X="-2.791831501558" Y="-28.977098199658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.276335073947" Y="-20.549175376039" />
                  <Point X="-2.709527767363" Y="-29.034726719704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.228595252766" Y="-20.727342585568" />
                  <Point X="-2.625600112218" Y="-29.086691954376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.163123749717" Y="-20.843672071637" />
                  <Point X="-2.541672581273" Y="-29.138657622186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.07976970986" Y="-20.897637740547" />
                  <Point X="-2.417737149683" Y="-29.051099159415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.014112671638" Y="-20.91488671875" />
                  <Point X="-2.288414808023" Y="-28.944754308662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.121702018983" Y="-20.884333826278" />
                  <Point X="-2.166163349148" Y="-28.863068556663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.411461946764" Y="-20.218476620351" />
                  <Point X="-2.047305482992" Y="-28.793217668926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.507409065489" Y="-20.228525004124" />
                  <Point X="-1.943558677945" Y="-28.776065313964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.603356184456" Y="-20.238573387056" />
                  <Point X="-1.846553069755" Y="-28.782422306291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.699303306486" Y="-20.248621759306" />
                  <Point X="-1.749547811981" Y="-28.788780520664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795250428515" Y="-20.258670131557" />
                  <Point X="-1.652542616129" Y="-28.795138950984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.888875749613" Y="-20.276815585914" />
                  <Point X="-1.56000321705" Y="-28.817071465474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.981305331225" Y="-20.299131079423" />
                  <Point X="-1.476563770049" Y="-28.870739284275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.073734912837" Y="-20.321446572931" />
                  <Point X="-1.397593931453" Y="-28.939994479989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.16616449445" Y="-20.34376206644" />
                  <Point X="-1.318623898757" Y="-29.009248998798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.258594073652" Y="-20.366077568355" />
                  <Point X="-1.240778594628" Y="-29.082425912254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.351023648671" Y="-20.388393084855" />
                  <Point X="-1.179728971852" Y="-29.214176327455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.443453223691" Y="-20.410708601355" />
                  <Point X="-1.139251679127" Y="-29.417670983622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.533291312305" Y="-20.442061704975" />
                  <Point X="-1.131111210403" Y="-29.733937546874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.622809566875" Y="-20.474530202462" />
                  <Point X="-1.038739876423" Y="-29.756456174018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.712327816784" Y="-20.506998716204" />
                  <Point X="-0.517181210192" Y="-28.282220699556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.801845850827" Y="-20.539467982762" />
                  <Point X="-0.368920586739" Y="-28.109830211334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.890485117156" Y="-20.575001876534" />
                  <Point X="-0.257543579218" Y="-28.066068178056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.977627676116" Y="-20.615755409206" />
                  <Point X="-0.149060726464" Y="-28.032399261913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.064770235076" Y="-20.656508941879" />
                  <Point X="-0.0414136021" Y="-28.00164487703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.151912733351" Y="-20.697262686183" />
                  <Point X="0.0572604908" Y="-28.002183171681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239055171664" Y="-20.7380166396" />
                  <Point X="0.148563654575" Y="-28.028426951024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.324058041434" Y="-20.786232155256" />
                  <Point X="0.239315729136" Y="-28.056592606852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408739728388" Y="-20.835567768506" />
                  <Point X="0.330067949654" Y="-28.084757753669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.493421419075" Y="-20.884903368739" />
                  <Point X="2.045562573675" Y="-22.446772774988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032452178531" Y="-22.492494156378" />
                  <Point X="0.414771745828" Y="-28.134016262907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578103306388" Y="-20.934238283253" />
                  <Point X="2.248269909262" Y="-22.084504036451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.04830068947" Y="-22.781879581878" />
                  <Point X="0.488174145808" Y="-28.222687424465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.661764385024" Y="-20.987133180693" />
                  <Point X="2.444614278203" Y="-21.7444255997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.117519013315" Y="-22.885142350985" />
                  <Point X="0.558109188261" Y="-28.323450698744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.74385336656" Y="-21.045510632265" />
                  <Point X="2.640958495131" Y="-21.404347693082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.19702211846" Y="-22.952537825231" />
                  <Point X="0.627668538874" Y="-28.425524166173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.282069030173" Y="-23.000599748382" />
                  <Point X="0.891873637548" Y="-27.848787240383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733972218926" Y="-28.399454928388" />
                  <Point X="0.680925795597" Y="-28.584449791302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375277660804" Y="-23.020198375089" />
                  <Point X="1.021641607865" Y="-27.740888297815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765069368703" Y="-28.635662030554" />
                  <Point X="0.72866552817" Y="-28.762617309843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.473755386911" Y="-23.021421482131" />
                  <Point X="1.139134292211" Y="-27.675798364841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.796166518481" Y="-28.87186913272" />
                  <Point X="0.776405260743" Y="-28.940784828384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580005735083" Y="-22.995538234714" />
                  <Point X="1.241967006096" Y="-27.661833824599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.827263701497" Y="-29.108076118969" />
                  <Point X="0.824144993316" Y="-29.118952346925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.695371270039" Y="-22.937866553248" />
                  <Point X="1.343473729405" Y="-27.652493563047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.813807074926" Y="-22.869487568077" />
                  <Point X="1.443640596948" Y="-27.647825933847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.932242879814" Y="-22.801108582906" />
                  <Point X="1.53563127787" Y="-27.671672055963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.050678684701" Y="-22.732729597736" />
                  <Point X="1.62000207507" Y="-27.722091870627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.169114489589" Y="-22.664350612565" />
                  <Point X="1.7034474254" Y="-27.775739102079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.287550272271" Y="-22.595971704832" />
                  <Point X="3.058017261094" Y="-23.396448443351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001160925621" Y="-23.594730048903" />
                  <Point X="1.78650150848" Y="-27.830750844588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.40598605085" Y="-22.527592811409" />
                  <Point X="3.203869965524" Y="-23.232455366709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.043777436289" Y="-23.790764365514" />
                  <Point X="1.861809319592" Y="-27.912777047841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.52442182943" Y="-22.459213917986" />
                  <Point X="3.330577652218" Y="-23.135228901448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.111704505368" Y="-23.898530275142" />
                  <Point X="2.169220302153" Y="-27.185363298527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022990368695" Y="-27.69532768059" />
                  <Point X="1.93375286736" Y="-28.006535831678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.642857608009" Y="-22.390835024563" />
                  <Point X="3.457285338912" Y="-23.038002436187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.190522709811" Y="-23.968314281992" />
                  <Point X="2.289830856593" Y="-27.109400060353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.070321111711" Y="-27.874921515218" />
                  <Point X="2.005696415127" Y="-28.100294615514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.761293386588" Y="-22.32245613114" />
                  <Point X="3.583993025606" Y="-22.940775970926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.276147104712" Y="-24.014362281932" />
                  <Point X="2.406037136856" Y="-27.04879635156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136353932779" Y="-27.989293452721" />
                  <Point X="2.077639962894" Y="-28.194053399351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879729165167" Y="-22.254077237717" />
                  <Point X="3.7107007123" Y="-22.843549505666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.368201845649" Y="-24.037985000224" />
                  <Point X="2.511560035941" Y="-27.025450020598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.202386753846" Y="-28.103665390224" />
                  <Point X="2.149583510662" Y="-28.287812183188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974867793201" Y="-22.266945163605" />
                  <Point X="3.837408398994" Y="-22.746323040405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463958237226" Y="-24.048698528611" />
                  <Point X="2.60715524575" Y="-27.036725656611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.268419574914" Y="-28.218037327726" />
                  <Point X="2.221527058429" Y="-28.381570967024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.045453789811" Y="-22.365438290959" />
                  <Point X="3.964115955006" Y="-22.649097030885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.565671835386" Y="-24.038636808714" />
                  <Point X="2.969720212426" Y="-26.116967106454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860394200892" Y="-26.498232218166" />
                  <Point X="2.701117752269" Y="-27.053695205656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.334452395982" Y="-28.332409265229" />
                  <Point X="2.293470606196" Y="-28.475329750861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.112508644661" Y="-22.476245973084" />
                  <Point X="4.090823423844" Y="-22.551871325381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668377424349" Y="-24.025115605761" />
                  <Point X="3.468916778911" Y="-24.720717541641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.357899335823" Y="-25.107881376184" />
                  <Point X="3.107979209931" Y="-25.979456433026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.915866696594" Y="-26.649432386881" />
                  <Point X="2.794428925704" Y="-27.072936223111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.400485222007" Y="-28.446781185444" />
                  <Point X="2.365414153964" Y="-28.569088534698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.771083013313" Y="-24.011594402809" />
                  <Point X="3.595151506295" Y="-24.625140481509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.411075905588" Y="-25.267088390173" />
                  <Point X="3.217413404535" Y="-25.942469793575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.98421232835" Y="-26.755738594981" />
                  <Point X="2.881545870435" Y="-27.113779083213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.466518050444" Y="-28.561153097245" />
                  <Point X="2.437357701731" Y="-28.662847318534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.873788602276" Y="-23.998073199856" />
                  <Point X="3.708586948748" Y="-24.574199832515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.483680830281" Y="-25.358540678567" />
                  <Point X="3.322810702483" Y="-25.919561485833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.05865663268" Y="-26.840776204261" />
                  <Point X="2.966336943906" Y="-27.162733220344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.532550878882" Y="-28.675525009045" />
                  <Point X="2.50930130904" Y="-28.756605894724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.976494191239" Y="-23.984551996904" />
                  <Point X="3.815640742187" Y="-24.545514638472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.564904397912" Y="-25.419936187091" />
                  <Point X="3.424819714056" Y="-25.908469536932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.139661747509" Y="-26.902933548242" />
                  <Point X="3.051128017377" Y="-27.211687357475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.59858370732" Y="-28.789896920846" />
                  <Point X="2.581245002516" Y="-28.850364170414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079199862886" Y="-23.971030505602" />
                  <Point X="3.922694535625" Y="-24.516829444428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.650024239454" Y="-25.467743773702" />
                  <Point X="3.52027144037" Y="-25.920245559355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220666862339" Y="-26.965090892224" />
                  <Point X="3.135919090847" Y="-27.260641494605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.664616535757" Y="-28.904268832646" />
                  <Point X="2.653188695992" Y="-28.944122446103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.181905607214" Y="-23.957508760826" />
                  <Point X="4.029748329063" Y="-24.488144250385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.740489225648" Y="-25.496910625648" />
                  <Point X="3.615504749999" Y="-25.932783291282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.301671977168" Y="-27.027248236205" />
                  <Point X="3.220710164318" Y="-27.309595631736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730649364195" Y="-29.018640744447" />
                  <Point X="2.726114577693" Y="-29.034455424392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.284611351543" Y="-23.94398701605" />
                  <Point X="4.136802122502" Y="-24.459459056342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.832266190863" Y="-25.521502063009" />
                  <Point X="3.710738059628" Y="-25.94532102321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382677091997" Y="-27.089405580186" />
                  <Point X="3.305501237789" Y="-27.358549768867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.387317095872" Y="-23.930465271274" />
                  <Point X="4.24385591594" Y="-24.430773862299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.924043156077" Y="-25.54609350037" />
                  <Point X="3.805971369257" Y="-25.957858755137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463682206826" Y="-27.151562924168" />
                  <Point X="3.39029231126" Y="-27.407503905997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.490022840201" Y="-23.916943526498" />
                  <Point X="4.350909709378" Y="-24.402088668256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.015820121291" Y="-25.570684937731" />
                  <Point X="3.901204678885" Y="-25.970396487064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.544687315922" Y="-27.213720288143" />
                  <Point X="3.475083384731" Y="-27.456458043128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.592728584529" Y="-23.903421781722" />
                  <Point X="4.457963502817" Y="-24.373403474212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.107597086506" Y="-25.595276375092" />
                  <Point X="3.996437988514" Y="-25.982934218992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.625692420657" Y="-27.275877667328" />
                  <Point X="3.559874458201" Y="-27.505412180259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.695434328858" Y="-23.889900036946" />
                  <Point X="4.565017302238" Y="-24.344718259306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.199374052689" Y="-25.619867809073" />
                  <Point X="4.091671298143" Y="-25.995471950919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.706697525391" Y="-27.338035046513" />
                  <Point X="3.644665531672" Y="-27.55436631739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.745987112013" Y="-24.058257282259" />
                  <Point X="4.672071107661" Y="-24.316033023466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.29115102121" Y="-25.644459234904" />
                  <Point X="4.186904607772" Y="-26.008009682846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.787702630126" Y="-27.400192425698" />
                  <Point X="3.729456606008" Y="-27.603320451503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.782328922036" Y="-24.276174080531" />
                  <Point X="4.779124913084" Y="-24.287347787627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.38292798973" Y="-25.669050660736" />
                  <Point X="4.282137917401" Y="-26.020547414774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.868707734861" Y="-27.462349804883" />
                  <Point X="3.814247681608" Y="-27.652274581211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.474704958251" Y="-25.693642086567" />
                  <Point X="4.37737122703" Y="-26.033085146701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.949712839596" Y="-27.524507184068" />
                  <Point X="3.899038757207" Y="-27.701228710918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.566481926771" Y="-25.718233512398" />
                  <Point X="4.472604509266" Y="-26.045622974158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.03071794433" Y="-27.586664563253" />
                  <Point X="3.983829832806" Y="-27.750182840626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.658258895292" Y="-25.742824938229" />
                  <Point X="4.567837788323" Y="-26.058160812699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.750035863812" Y="-25.76741636406" />
                  <Point X="4.663071067381" Y="-26.07069865124" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.724073974609" Y="-29.4795859375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318969727" Y="-28.521544921875" />
                  <Point X="0.361101531982" Y="-28.372830078125" />
                  <Point X="0.300591339111" Y="-28.28564453125" />
                  <Point X="0.274336364746" Y="-28.266400390625" />
                  <Point X="0.114613487244" Y="-28.216830078125" />
                  <Point X="0.020977386475" Y="-28.187767578125" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.168387481689" Y="-28.237337890625" />
                  <Point X="-0.262023590088" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.391496459961" Y="-28.434359375" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.6072734375" Y="-29.08962890625" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-0.99094329834" Y="-29.95928125" />
                  <Point X="-1.100256713867" Y="-29.9380625" />
                  <Point X="-1.285200195312" Y="-29.89048046875" />
                  <Point X="-1.35158996582" Y="-29.8733984375" />
                  <Point X="-1.339549560547" Y="-29.781943359375" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.347841186523" Y="-29.342927734375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282470703" Y="-29.20262890625" />
                  <Point X="-1.533335083008" Y="-29.07366796875" />
                  <Point X="-1.619543334961" Y="-28.998064453125" />
                  <Point X="-1.649241210938" Y="-28.985763671875" />
                  <Point X="-1.844412719727" Y="-28.972970703125" />
                  <Point X="-1.958830444336" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.152505859375" Y="-29.082455078125" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.342850585938" Y="-29.265615234375" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.695620849609" Y="-29.266810546875" />
                  <Point X="-2.855839599609" Y="-29.167607421875" />
                  <Point X="-3.111942138672" Y="-28.970416015625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.975341552734" Y="-28.441984375" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513981933594" Y="-27.56876171875" />
                  <Point X="-2.531326904297" Y="-27.55141796875" />
                  <Point X="-2.560156494141" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.068218017578" Y="-27.81859765625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.035126220703" Y="-28.01342578125" />
                  <Point X="-4.161701660156" Y="-27.8471328125" />
                  <Point X="-4.345312988281" Y="-27.539244140625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.984923095703" Y="-27.053224609375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145821777344" Y="-26.396013671875" />
                  <Point X="-3.1381171875" Y="-26.366265625" />
                  <Point X="-3.140326660156" Y="-26.33459375" />
                  <Point X="-3.161161132812" Y="-26.310638671875" />
                  <Point X="-3.187643798828" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-3.821108886719" Y="-26.367771484375" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.877887695312" Y="-26.20500390625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.975971679688" Y="-25.671537109375" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.492861816406" Y="-25.37928515625" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541894775391" Y="-25.12142578125" />
                  <Point X="-3.524398681641" Y="-25.109283203125" />
                  <Point X="-3.514141845703" Y="-25.1021640625" />
                  <Point X="-3.494898925781" Y="-25.075908203125" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.485647216797" Y="-25.016462890625" />
                  <Point X="-3.494898193359" Y="-24.986654296875" />
                  <Point X="-3.514141845703" Y="-24.960396484375" />
                  <Point X="-3.531637939453" Y="-24.94825390625" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.105832519531" Y="-24.78698046875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.949550292969" Y="-24.219193359375" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.819858886719" Y="-23.64271875" />
                  <Point X="-4.773516113281" Y="-23.471701171875" />
                  <Point X="-4.423782226562" Y="-23.51774609375" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731703857422" Y="-23.6041328125" />
                  <Point X="-3.692979492188" Y="-23.591923828125" />
                  <Point X="-3.670277832031" Y="-23.584765625" />
                  <Point X="-3.651533935547" Y="-23.573943359375" />
                  <Point X="-3.639119628906" Y="-23.556212890625" />
                  <Point X="-3.623581298828" Y="-23.518701171875" />
                  <Point X="-3.614472167969" Y="-23.496708984375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.635064697266" Y="-23.41847265625" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.974513671875" Y="-23.139419921875" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.283986328125" Y="-22.42538671875" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.90098046875" Y="-21.88004296875" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.570969238281" Y="-21.835296875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138513916016" Y="-22.07956640625" />
                  <Point X="-3.084581787109" Y="-22.08428515625" />
                  <Point X="-3.052964599609" Y="-22.08705078125" />
                  <Point X="-3.031506591797" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.974971435547" Y="-22.034314453125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.942792724609" Y="-21.9182265625" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.091452148438" Y="-21.624546875" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-2.968794677734" Y="-20.9912109375" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.344905273438" Y="-20.599009765625" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.093895263672" Y="-20.548130859375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951248291016" Y="-20.726337890625" />
                  <Point X="-1.891222045898" Y="-20.7575859375" />
                  <Point X="-1.856032226562" Y="-20.77590625" />
                  <Point X="-1.835125244141" Y="-20.781509765625" />
                  <Point X="-1.813809204102" Y="-20.77775" />
                  <Point X="-1.751287963867" Y="-20.7518515625" />
                  <Point X="-1.714635375977" Y="-20.736669921875" />
                  <Point X="-1.696905517578" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.665733886719" Y="-20.640970703125" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.667764282227" Y="-20.461208984375" />
                  <Point X="-1.689137695313" Y="-20.298861328125" />
                  <Point X="-1.247614257812" Y="-20.175072265625" />
                  <Point X="-0.968083129883" Y="-20.096703125" />
                  <Point X="-0.473509643555" Y="-20.038818359375" />
                  <Point X="-0.224200210571" Y="-20.009640625" />
                  <Point X="-0.163965759277" Y="-20.234439453125" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282100677" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594055176" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.125871307373" Y="-20.42255859375" />
                  <Point X="0.2366484375" Y="-20.009130859375" />
                  <Point X="0.616142456055" Y="-20.048875" />
                  <Point X="0.860210266113" Y="-20.074435546875" />
                  <Point X="1.269388549805" Y="-20.173224609375" />
                  <Point X="1.508455078125" Y="-20.230943359375" />
                  <Point X="1.775219726562" Y="-20.32769921875" />
                  <Point X="1.931044067383" Y="-20.38421875" />
                  <Point X="2.188569824219" Y="-20.504654296875" />
                  <Point X="2.338685302734" Y="-20.574859375" />
                  <Point X="2.587489501953" Y="-20.7198125" />
                  <Point X="2.732533935547" Y="-20.804314453125" />
                  <Point X="2.967165771484" Y="-20.971171875" />
                  <Point X="3.068740722656" Y="-21.043408203125" />
                  <Point X="2.773041992188" Y="-21.555572265625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508484375" />
                  <Point X="2.211317138672" Y="-22.559099609375" />
                  <Point X="2.203382324219" Y="-22.588771484375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.207322265625" Y="-22.65144140625" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682617188" Y="-22.6991875" />
                  <Point X="2.245764160156" Y="-22.739099609375" />
                  <Point X="2.261640380859" Y="-22.76249609375" />
                  <Point X="2.274938964844" Y="-22.775794921875" />
                  <Point X="2.314850097656" Y="-22.802876953125" />
                  <Point X="2.338247558594" Y="-22.81875390625" />
                  <Point X="2.360337158203" Y="-22.827021484375" />
                  <Point X="2.404104003906" Y="-22.832298828125" />
                  <Point X="2.429761962891" Y="-22.835392578125" />
                  <Point X="2.448665527344" Y="-22.8340546875" />
                  <Point X="2.499279785156" Y="-22.82051953125" />
                  <Point X="2.528951904297" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.092586914062" Y="-22.489142578125" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.1165625" Y="-22.13855859375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.333401367188" Y="-22.47428515625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.003774414062" Y="-22.85815625" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.242944091797" Y="-23.463689453125" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.199550292969" Y="-23.55701953125" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.201918457031" Y="-23.66301953125" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712046875" />
                  <Point X="3.245942871094" Y="-23.758095703125" />
                  <Point X="3.263704101562" Y="-23.785091796875" />
                  <Point X="3.280947998047" Y="-23.8011796875" />
                  <Point X="3.324851806641" Y="-23.82589453125" />
                  <Point X="3.350590087891" Y="-23.8403828125" />
                  <Point X="3.368564941406" Y="-23.846380859375" />
                  <Point X="3.42792578125" Y="-23.854224609375" />
                  <Point X="3.462725585938" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.9997421875" Y="-23.7898515625" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.902239257812" Y="-23.89683984375" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.98041015625" Y="-24.313375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.560754882812" Y="-24.542564453125" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.729087402344" Y="-24.767181640625" />
                  <Point X="3.670767089844" Y="-24.800890625" />
                  <Point X="3.636577392578" Y="-24.82065234375" />
                  <Point X="3.622265380859" Y="-24.833072265625" />
                  <Point X="3.587273193359" Y="-24.877662109375" />
                  <Point X="3.566759521484" Y="-24.90380078125" />
                  <Point X="3.556985351562" Y="-24.925265625" />
                  <Point X="3.545321289062" Y="-24.986171875" />
                  <Point X="3.538483154297" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.550147216797" Y="-25.10158984375" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758300781" Y="-25.1587578125" />
                  <Point X="3.601750488281" Y="-25.203345703125" />
                  <Point X="3.622264160156" Y="-25.229486328125" />
                  <Point X="3.636575683594" Y="-25.24190625" />
                  <Point X="3.694895996094" Y="-25.275615234375" />
                  <Point X="3.729085693359" Y="-25.29537890625" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.221643066406" Y="-25.429130859375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.9690625" Y="-25.829560546875" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.895619628906" Y="-26.197828125" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.36247265625" Y="-26.222763671875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836669922" Y="-26.098341796875" />
                  <Point X="3.280374755859" Y="-26.123220703125" />
                  <Point X="3.213272460938" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.116260498047" Y="-26.23790625" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.054441894531" Y="-26.421828125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.119705322266" Y="-26.615150390625" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.614344970703" Y="-27.0276796875" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.262287109375" Y="-27.708037109375" />
                  <Point X="4.204126953125" Y="-27.802150390625" />
                  <Point X="4.094913818359" Y="-27.957326171875" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.599994628906" Y="-27.747966796875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340087891" Y="-27.2533125" />
                  <Point X="2.601112060547" Y="-27.228708984375" />
                  <Point X="2.521249755859" Y="-27.214287109375" />
                  <Point X="2.489077392578" Y="-27.21924609375" />
                  <Point X="2.375905273438" Y="-27.278806640625" />
                  <Point X="2.309559326172" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.229038085938" Y="-27.44785546875" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.213765625" Y="-27.682603515625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.520616943359" Y="-28.27485546875" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.904306640625" Y="-29.140921875" />
                  <Point X="2.835297851563" Y="-29.190212890625" />
                  <Point X="2.713184814453" Y="-29.26925390625" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="2.329165771484" Y="-28.83395703125" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.53619152832" Y="-27.894087890625" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425805541992" Y="-27.83571875" />
                  <Point X="1.278862670898" Y="-27.849240234375" />
                  <Point X="1.19271875" Y="-27.857166015625" />
                  <Point X="1.165332519531" Y="-27.868509765625" />
                  <Point X="1.051866943359" Y="-27.962853515625" />
                  <Point X="0.985348754883" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.93453112793" Y="-28.202072265625" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.995128479004" Y="-28.92748828125" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.059631225586" Y="-29.9489375" />
                  <Point X="0.994366333008" Y="-29.9632421875" />
                  <Point X="0.881538085938" Y="-29.983740234375" />
                  <Point X="0.860200561523" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#180" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.12182548178" Y="4.809783384435" Z="1.6" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.6" />
                  <Point X="-0.48547767958" Y="5.042121414108" Z="1.6" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.6" />
                  <Point X="-1.267267945514" Y="4.904352393689" Z="1.6" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.6" />
                  <Point X="-1.723492349181" Y="4.563546466482" Z="1.6" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.6" />
                  <Point X="-1.719575618451" Y="4.405344398545" Z="1.6" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.6" />
                  <Point X="-1.776577711464" Y="4.325621982742" Z="1.6" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.6" />
                  <Point X="-1.874288915294" Y="4.318043393839" Z="1.6" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.6" />
                  <Point X="-2.060383405998" Y="4.513586679358" Z="1.6" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.6" />
                  <Point X="-2.375344227412" Y="4.475978733226" Z="1.6" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.6" />
                  <Point X="-3.005372689292" Y="4.079748824131" Z="1.6" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.6" />
                  <Point X="-3.140909300175" Y="3.381734128258" Z="1.6" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.6" />
                  <Point X="-2.998758510329" Y="3.108695688212" Z="1.6" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.6" />
                  <Point X="-3.016482142367" Y="3.03232142552" Z="1.6" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.6" />
                  <Point X="-3.086380777394" Y="2.996806188844" Z="1.6" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.6" />
                  <Point X="-3.552125220039" Y="3.239284648571" Z="1.6" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.6" />
                  <Point X="-3.946599715324" Y="3.1819408311" Z="1.6" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.6" />
                  <Point X="-4.333324351532" Y="2.631098085797" Z="1.6" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.6" />
                  <Point X="-4.011107740154" Y="1.852193307319" Z="1.6" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.6" />
                  <Point X="-3.685571071694" Y="1.589720184646" Z="1.6" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.6" />
                  <Point X="-3.675931615734" Y="1.531712868207" Z="1.6" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.6" />
                  <Point X="-3.71417171101" Y="1.487042266412" Z="1.6" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.6" />
                  <Point X="-4.423412022128" Y="1.56310766117" Z="1.6" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.6" />
                  <Point X="-4.874273940796" Y="1.40163945544" Z="1.6" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.6" />
                  <Point X="-5.005167536751" Y="0.81940577337" Z="1.6" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.6" />
                  <Point X="-4.124929207926" Y="0.19600392362" Z="1.6" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.6" />
                  <Point X="-3.566303862913" Y="0.041950176622" Z="1.6" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.6" />
                  <Point X="-3.545388843081" Y="0.018790935922" Z="1.6" />
                  <Point X="-3.539556741714" Y="0" Z="1.6" />
                  <Point X="-3.542975758054" Y="-0.011016015139" Z="1.6" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.6" />
                  <Point X="-3.55906473221" Y="-0.036925806512" Z="1.6" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.6" />
                  <Point X="-4.511958580846" Y="-0.299708133136" Z="1.6" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.6" />
                  <Point X="-5.031623599367" Y="-0.647334545453" Z="1.6" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.6" />
                  <Point X="-4.932488306951" Y="-1.186097870517" Z="1.6" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.6" />
                  <Point X="-3.820738610517" Y="-1.386062765746" Z="1.6" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.6" />
                  <Point X="-3.209371488788" Y="-1.312623745139" Z="1.6" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.6" />
                  <Point X="-3.195523723765" Y="-1.333443797455" Z="1.6" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.6" />
                  <Point X="-4.021516986678" Y="-1.982277098851" Z="1.6" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.6" />
                  <Point X="-4.39441234474" Y="-2.533573966236" Z="1.6" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.6" />
                  <Point X="-4.08106818919" Y="-3.012429387665" Z="1.6" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.6" />
                  <Point X="-3.049374086757" Y="-2.830618339587" Z="1.6" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.6" />
                  <Point X="-2.566427851596" Y="-2.561902617535" Z="1.6" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.6" />
                  <Point X="-3.024798673581" Y="-3.385704295735" Z="1.6" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.6" />
                  <Point X="-3.148601835024" Y="-3.978753483626" Z="1.6" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.6" />
                  <Point X="-2.728098229187" Y="-4.278042136427" Z="1.6" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.6" />
                  <Point X="-2.309338861726" Y="-4.264771783619" Z="1.6" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.6" />
                  <Point X="-2.130883426369" Y="-4.092748702078" Z="1.6" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.6" />
                  <Point X="-1.853838432508" Y="-3.991583699272" Z="1.6" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.6" />
                  <Point X="-1.572458592888" Y="-4.079978649237" Z="1.6" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.6" />
                  <Point X="-1.403036323273" Y="-4.321400255527" Z="1.6" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.6" />
                  <Point X="-1.395277773702" Y="-4.744137271836" Z="1.6" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.6" />
                  <Point X="-1.303815650159" Y="-4.907620919056" Z="1.6" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.6" />
                  <Point X="-1.006626616484" Y="-4.97708543325" Z="1.6" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.6" />
                  <Point X="-0.565133108562" Y="-4.071289538209" Z="1.6" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.6" />
                  <Point X="-0.356576504263" Y="-3.431589307751" Z="1.6" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.6" />
                  <Point X="-0.1597229524" Y="-3.253811478536" Z="1.6" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.6" />
                  <Point X="0.09363612696" Y="-3.233300566752" Z="1.6" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.6" />
                  <Point X="0.313869355169" Y="-3.370056448017" Z="1.6" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.6" />
                  <Point X="0.669621751839" Y="-4.461246491606" Z="1.6" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.6" />
                  <Point X="0.884318676686" Y="-5.00165497004" Z="1.6" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.6" />
                  <Point X="1.064180575276" Y="-4.966496859822" Z="1.6" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.6" />
                  <Point X="1.038544888482" Y="-3.889682054799" Z="1.6" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.6" />
                  <Point X="0.977234379396" Y="-3.181410553569" Z="1.6" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.6" />
                  <Point X="1.077678176419" Y="-2.970018344478" Z="1.6" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.6" />
                  <Point X="1.277287639797" Y="-2.867748400843" Z="1.6" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.6" />
                  <Point X="1.502996347064" Y="-2.904865779251" Z="1.6" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.6" />
                  <Point X="2.283342016211" Y="-3.833113205387" Z="1.6" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.6" />
                  <Point X="2.73419838367" Y="-4.279948206223" Z="1.6" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.6" />
                  <Point X="2.927212297344" Y="-4.150328405957" Z="1.6" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.6" />
                  <Point X="2.557762308339" Y="-3.218574985375" Z="1.6" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.6" />
                  <Point X="2.25681379697" Y="-2.642436191323" Z="1.6" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.6" />
                  <Point X="2.267128572924" Y="-2.439862152573" Z="1.6" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.6" />
                  <Point X="2.393036225799" Y="-2.291772737043" Z="1.6" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.6" />
                  <Point X="2.586070612189" Y="-2.246634212521" Z="1.6" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.6" />
                  <Point X="3.568838611732" Y="-2.759987191936" Z="1.6" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.6" />
                  <Point X="4.129646110749" Y="-2.954822748458" Z="1.6" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.6" />
                  <Point X="4.298664993916" Y="-2.703041476502" Z="1.6" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.6" />
                  <Point X="3.638626894399" Y="-1.95673195514" Z="1.6" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.6" />
                  <Point X="3.155607436142" Y="-1.556831372897" Z="1.6" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.6" />
                  <Point X="3.098075592046" Y="-1.395130306977" Z="1.6" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.6" />
                  <Point X="3.148550531802" Y="-1.238592256032" Z="1.6" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.6" />
                  <Point X="3.284837843957" Y="-1.140799217167" Z="1.6" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.6" />
                  <Point X="4.349790345386" Y="-1.241054861349" Z="1.6" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.6" />
                  <Point X="4.938210881963" Y="-1.177672980134" Z="1.6" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.6" />
                  <Point X="5.012347944449" Y="-0.805734117949" Z="1.6" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.6" />
                  <Point X="4.228428117927" Y="-0.349553631273" Z="1.6" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.6" />
                  <Point X="3.713762976839" Y="-0.201048364251" Z="1.6" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.6" />
                  <Point X="3.634928880998" Y="-0.141198762586" Z="1.6" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.6" />
                  <Point X="3.593098779145" Y="-0.060905333728" Z="1.6" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.6" />
                  <Point X="3.588272671279" Y="0.035705197472" Z="1.6" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.6" />
                  <Point X="3.620450557402" Y="0.122749975989" Z="1.6" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.6" />
                  <Point X="3.689632437513" Y="0.187100479991" Z="1.6" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.6" />
                  <Point X="4.56753969938" Y="0.440418301495" Z="1.6" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.6" />
                  <Point X="5.023659053977" Y="0.725596316931" Z="1.6" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.6" />
                  <Point X="4.944664425519" Y="1.14626759129" Z="1.6" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.6" />
                  <Point X="3.987059894729" Y="1.291001913915" Z="1.6" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.6" />
                  <Point X="3.428321587009" Y="1.226623304231" Z="1.6" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.6" />
                  <Point X="3.343222392264" Y="1.248956971634" Z="1.6" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.6" />
                  <Point X="3.281557501414" Y="1.300667047221" Z="1.6" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.6" />
                  <Point X="3.244730923139" Y="1.378364465881" Z="1.6" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.6" />
                  <Point X="3.241546828425" Y="1.460793646336" Z="1.6" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.6" />
                  <Point X="3.276471048083" Y="1.537173064049" Z="1.6" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.6" />
                  <Point X="4.028056695786" Y="2.133455527836" Z="1.6" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.6" />
                  <Point X="4.370022653266" Y="2.582882591032" Z="1.6" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.6" />
                  <Point X="4.150991921295" Y="2.921924569588" Z="1.6" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.6" />
                  <Point X="3.061431024315" Y="2.58543808285" Z="1.6" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.6" />
                  <Point X="2.480205824454" Y="2.259064062905" Z="1.6" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.6" />
                  <Point X="2.403933701274" Y="2.248623048876" Z="1.6" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.6" />
                  <Point X="2.336769233266" Y="2.26977672266" Z="1.6" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.6" />
                  <Point X="2.280981877575" Y="2.320255627116" Z="1.6" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.6" />
                  <Point X="2.250806653883" Y="2.385824742559" Z="1.6" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.6" />
                  <Point X="2.253463879752" Y="2.459263751428" Z="1.6" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.6" />
                  <Point X="2.81018727001" Y="3.45070805764" Z="1.6" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.6" />
                  <Point X="2.9899869792" Y="4.100853990124" Z="1.6" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.6" />
                  <Point X="2.60650339859" Y="4.354671302832" Z="1.6" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.6" />
                  <Point X="2.203595521768" Y="4.571916912069" Z="1.6" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.6" />
                  <Point X="1.786112590703" Y="4.750584706406" Z="1.6" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.6" />
                  <Point X="1.274966966355" Y="4.906659892244" Z="1.6" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.6" />
                  <Point X="0.615194406881" Y="5.03213361426" Z="1.6" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.6" />
                  <Point X="0.071419079773" Y="4.621664121516" Z="1.6" />
                  <Point X="0" Y="4.355124473572" Z="1.6" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>