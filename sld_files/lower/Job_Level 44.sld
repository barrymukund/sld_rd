<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#203" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3151" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995282226562" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.902576843262" Y="-29.778716796875" />
                  <Point X="0.563301940918" Y="-28.5125234375" />
                  <Point X="0.557720153809" Y="-28.497138671875" />
                  <Point X="0.542363342285" Y="-28.467376953125" />
                  <Point X="0.403693084717" Y="-28.267580078125" />
                  <Point X="0.378635528564" Y="-28.2314765625" />
                  <Point X="0.356753570557" Y="-28.2090234375" />
                  <Point X="0.330499145508" Y="-28.189779296875" />
                  <Point X="0.302495788574" Y="-28.175669921875" />
                  <Point X="0.087911506653" Y="-28.109072265625" />
                  <Point X="0.049136642456" Y="-28.097037109375" />
                  <Point X="0.020983924866" Y="-28.092767578125" />
                  <Point X="-0.008657250404" Y="-28.092765625" />
                  <Point X="-0.036823177338" Y="-28.09703515625" />
                  <Point X="-0.251407455444" Y="-28.163634765625" />
                  <Point X="-0.290182312012" Y="-28.17566796875" />
                  <Point X="-0.318182952881" Y="-28.189775390625" />
                  <Point X="-0.344438720703" Y="-28.20901953125" />
                  <Point X="-0.366323120117" Y="-28.2314765625" />
                  <Point X="-0.504993377686" Y="-28.431275390625" />
                  <Point X="-0.53005090332" Y="-28.467376953125" />
                  <Point X="-0.538187805176" Y="-28.4815703125" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.612296447754" Y="-28.741322265625" />
                  <Point X="-0.916584716797" Y="-29.87694140625" />
                  <Point X="-1.037744262695" Y="-29.853423828125" />
                  <Point X="-1.079352172852" Y="-29.845345703125" />
                  <Point X="-1.24641796875" Y="-29.80236328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.267772705078" Y="-29.25850390625" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287938354492" Y="-29.182966796875" />
                  <Point X="-1.304010620117" Y="-29.15512890625" />
                  <Point X="-1.323645019531" Y="-29.131205078125" />
                  <Point X="-1.52120690918" Y="-28.957947265625" />
                  <Point X="-1.556905883789" Y="-28.926640625" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612885864258" Y="-28.897994140625" />
                  <Point X="-1.643027587891" Y="-28.890966796875" />
                  <Point X="-1.905236206055" Y="-28.87378125" />
                  <Point X="-1.952616699219" Y="-28.87067578125" />
                  <Point X="-1.983415161133" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042657104492" Y="-28.89480078125" />
                  <Point X="-2.261143310547" Y="-29.0407890625" />
                  <Point X="-2.300623535156" Y="-29.06716796875" />
                  <Point X="-2.312787353516" Y="-29.076822265625" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.369521484375" Y="-29.144318359375" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.740746582031" Y="-29.1271328125" />
                  <Point X="-2.801726806641" Y="-29.089375" />
                  <Point X="-3.104721923828" Y="-28.856078125" />
                  <Point X="-3.054265625" Y="-28.768685546875" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412859130859" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616126953125" />
                  <Point X="-2.405575439453" Y="-27.585193359375" />
                  <Point X="-2.414559570312" Y="-27.555576171875" />
                  <Point X="-2.428776855469" Y="-27.52674609375" />
                  <Point X="-2.446805419922" Y="-27.501587890625" />
                  <Point X="-2.461499023438" Y="-27.48689453125" />
                  <Point X="-2.464176513672" Y="-27.48421875" />
                  <Point X="-2.489323486328" Y="-27.466205078125" />
                  <Point X="-2.518150878906" Y="-27.4519921875" />
                  <Point X="-2.547766601562" Y="-27.44301171875" />
                  <Point X="-2.578697509766" Y="-27.444025390625" />
                  <Point X="-2.610220458984" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-2.836517822266" Y="-27.575130859375" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.034696289063" Y="-27.857138671875" />
                  <Point X="-4.082857910156" Y="-27.793865234375" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-4.2091328125" Y="-27.345013671875" />
                  <Point X="-3.105954833984" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.047331054688" Y="-26.394638671875" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.359658203125" />
                  <Point X="-3.045556396484" Y="-26.327986328125" />
                  <Point X="-3.052558105469" Y="-26.29823828125" />
                  <Point X="-3.068642089844" Y="-26.27225390625" />
                  <Point X="-3.089474853516" Y="-26.248298828125" />
                  <Point X="-3.112972412109" Y="-26.228767578125" />
                  <Point X="-3.135402099609" Y="-26.21556640625" />
                  <Point X="-3.139460205078" Y="-26.213177734375" />
                  <Point X="-3.168729248047" Y="-26.201953125" />
                  <Point X="-3.200611572266" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-3.481044677734" Y="-26.227181640625" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.815239257812" Y="-26.06640625" />
                  <Point X="-4.834076660156" Y="-25.992658203125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.789562011719" Y="-25.557138671875" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.5174921875" Y="-25.214828125" />
                  <Point X="-3.487727783203" Y="-25.199470703125" />
                  <Point X="-3.464222167969" Y="-25.18315625" />
                  <Point X="-3.459979003906" Y="-25.1802109375" />
                  <Point X="-3.437529785156" Y="-25.158333984375" />
                  <Point X="-3.418282958984" Y="-25.132080078125" />
                  <Point X="-3.404168945312" Y="-25.1040703125" />
                  <Point X="-3.396333740234" Y="-25.07882421875" />
                  <Point X="-3.39491796875" Y="-25.074263671875" />
                  <Point X="-3.390648925781" Y="-25.046111328125" />
                  <Point X="-3.390647216797" Y="-25.01646875" />
                  <Point X="-3.394916259766" Y="-24.9883046875" />
                  <Point X="-3.402751464844" Y="-24.96305859375" />
                  <Point X="-3.404167236328" Y="-24.95849609375" />
                  <Point X="-3.418274902344" Y="-24.930494140625" />
                  <Point X="-3.437521484375" Y="-24.904234375" />
                  <Point X="-3.459981445312" Y="-24.88234765625" />
                  <Point X="-3.483487060547" Y="-24.86603515625" />
                  <Point X="-3.494275878906" Y="-24.8595625" />
                  <Point X="-3.514089355469" Y="-24.84939453125" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-3.759956787109" Y="-24.7813046875" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.836627441406" Y="-24.105064453125" />
                  <Point X="-4.824487792969" Y="-24.023025390625" />
                  <Point X="-4.703551757812" Y="-23.576732421875" />
                  <Point X="-4.666489257812" Y="-23.581611328125" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744986572266" Y="-23.700658203125" />
                  <Point X="-3.723421386719" Y="-23.698771484375" />
                  <Point X="-3.703135986328" Y="-23.694736328125" />
                  <Point X="-3.651110839844" Y="-23.678333984375" />
                  <Point X="-3.641709960938" Y="-23.675369140625" />
                  <Point X="-3.622771728516" Y="-23.66703515625" />
                  <Point X="-3.604028808594" Y="-23.656212890625" />
                  <Point X="-3.587347167969" Y="-23.64398046875" />
                  <Point X="-3.573708740234" Y="-23.628427734375" />
                  <Point X="-3.561295654297" Y="-23.610697265625" />
                  <Point X="-3.551350830078" Y="-23.592568359375" />
                  <Point X="-3.530475341797" Y="-23.542171875" />
                  <Point X="-3.526703369141" Y="-23.533064453125" />
                  <Point X="-3.5209140625" Y="-23.513201171875" />
                  <Point X="-3.517156494141" Y="-23.491884765625" />
                  <Point X="-3.5158046875" Y="-23.471244140625" />
                  <Point X="-3.518952392578" Y="-23.450798828125" />
                  <Point X="-3.524555419922" Y="-23.429892578125" />
                  <Point X="-3.53205078125" Y="-23.41062109375" />
                  <Point X="-3.557239013672" Y="-23.362234375" />
                  <Point X="-3.561790527344" Y="-23.3534921875" />
                  <Point X="-3.573284179688" Y="-23.336291015625" />
                  <Point X="-3.587195556641" Y="-23.319712890625" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-3.732389892578" Y="-23.205462890625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.128322265625" Y="-22.34715234375" />
                  <Point X="-4.081155761719" Y="-22.26634375" />
                  <Point X="-3.750504882813" Y="-21.841337890625" />
                  <Point X="-3.206656494141" Y="-22.155330078125" />
                  <Point X="-3.187724853516" Y="-22.16366015625" />
                  <Point X="-3.167079101562" Y="-22.170169921875" />
                  <Point X="-3.146791992188" Y="-22.174205078125" />
                  <Point X="-3.074335449219" Y="-22.180544921875" />
                  <Point X="-3.061242675781" Y="-22.181689453125" />
                  <Point X="-3.040555908203" Y="-22.181236328125" />
                  <Point X="-3.019099609375" Y="-22.17841015625" />
                  <Point X="-2.999009521484" Y="-22.173494140625" />
                  <Point X="-2.980459716797" Y="-22.164345703125" />
                  <Point X="-2.962207275391" Y="-22.152716796875" />
                  <Point X="-2.946078369141" Y="-22.139771484375" />
                  <Point X="-2.894648193359" Y="-22.088341796875" />
                  <Point X="-2.885354736328" Y="-22.079048828125" />
                  <Point X="-2.872409179688" Y="-22.062919921875" />
                  <Point X="-2.860779296875" Y="-22.044666015625" />
                  <Point X="-2.851629638672" Y="-22.02611328125" />
                  <Point X="-2.846712646484" Y="-22.00601953125" />
                  <Point X="-2.843887207031" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.849774902344" Y="-21.891423828125" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-2.927514648438" Y="-21.718494140625" />
                  <Point X="-3.183332519531" Y="-21.27540625" />
                  <Point X="-2.7827734375" Y="-20.96830078125" />
                  <Point X="-2.700620849609" Y="-20.905314453125" />
                  <Point X="-2.16703515625" Y="-20.6088671875" />
                  <Point X="-2.04319921875" Y="-20.770251953125" />
                  <Point X="-2.028891723633" Y="-20.78519921875" />
                  <Point X="-2.012312011719" Y="-20.799111328125" />
                  <Point X="-1.995114624023" Y="-20.810603515625" />
                  <Point X="-1.914470825195" Y="-20.8525859375" />
                  <Point X="-1.89989855957" Y="-20.860171875" />
                  <Point X="-1.88062487793" Y="-20.86766796875" />
                  <Point X="-1.859718017578" Y="-20.873271484375" />
                  <Point X="-1.839268310547" Y="-20.876419921875" />
                  <Point X="-1.818621826172" Y="-20.87506640625" />
                  <Point X="-1.797306396484" Y="-20.871306640625" />
                  <Point X="-1.777452880859" Y="-20.865517578125" />
                  <Point X="-1.693456665039" Y="-20.830724609375" />
                  <Point X="-1.678278930664" Y="-20.8244375" />
                  <Point X="-1.660144897461" Y="-20.814490234375" />
                  <Point X="-1.642415649414" Y="-20.802076171875" />
                  <Point X="-1.626863525391" Y="-20.7884375" />
                  <Point X="-1.614632324219" Y="-20.771755859375" />
                  <Point X="-1.603810546875" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734078125" />
                  <Point X="-1.568141235352" Y="-20.647369140625" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.564292602539" Y="-20.519330078125" />
                  <Point X="-1.584202026367" Y="-20.3681015625" />
                  <Point X="-1.055981567383" Y="-20.2200078125" />
                  <Point X="-0.949625427246" Y="-20.19019140625" />
                  <Point X="-0.294711212158" Y="-20.11354296875" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113983154" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907154" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425941467" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.17578994751" Y="-20.603310546875" />
                  <Point X="0.307419647217" Y="-20.112060546875" />
                  <Point X="0.751177307129" Y="-20.15853515625" />
                  <Point X="0.844030517578" Y="-20.168259765625" />
                  <Point X="1.388745849609" Y="-20.299771484375" />
                  <Point X="1.481038818359" Y="-20.3220546875" />
                  <Point X="1.835352050781" Y="-20.45056640625" />
                  <Point X="1.894646972656" Y="-20.472072265625" />
                  <Point X="2.237479736328" Y="-20.632404296875" />
                  <Point X="2.294558349609" Y="-20.65909765625" />
                  <Point X="2.625790527344" Y="-20.85207421875" />
                  <Point X="2.680990966797" Y="-20.884234375" />
                  <Point X="2.943260253906" Y="-21.070744140625" />
                  <Point X="2.877342529297" Y="-21.18491796875" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142076416016" Y="-22.460068359375" />
                  <Point X="2.133076904297" Y="-22.483943359375" />
                  <Point X="2.114893066406" Y="-22.551943359375" />
                  <Point X="2.112695556641" Y="-22.562552734375" />
                  <Point X="2.108071533203" Y="-22.5937421875" />
                  <Point X="2.107727783203" Y="-22.619046875" />
                  <Point X="2.114818115234" Y="-22.677845703125" />
                  <Point X="2.116099121094" Y="-22.688470703125" />
                  <Point X="2.121442138672" Y="-22.71039453125" />
                  <Point X="2.129710449219" Y="-22.73248828125" />
                  <Point X="2.140072265625" Y="-22.75253125" />
                  <Point X="2.176455566406" Y="-22.80615234375" />
                  <Point X="2.183204345703" Y="-22.814947265625" />
                  <Point X="2.203077148438" Y="-22.837931640625" />
                  <Point X="2.221599609375" Y="-22.854408203125" />
                  <Point X="2.275209472656" Y="-22.89078515625" />
                  <Point X="2.284892578125" Y="-22.897357421875" />
                  <Point X="2.30494140625" Y="-22.907724609375" />
                  <Point X="2.327034912109" Y="-22.915994140625" />
                  <Point X="2.348964355469" Y="-22.921337890625" />
                  <Point X="2.407764160156" Y="-22.928427734375" />
                  <Point X="2.419318603516" Y="-22.929111328125" />
                  <Point X="2.448846435547" Y="-22.9290546875" />
                  <Point X="2.473205810547" Y="-22.925830078125" />
                  <Point X="2.541204833984" Y="-22.907646484375" />
                  <Point X="2.547254150391" Y="-22.905810546875" />
                  <Point X="2.571624755859" Y="-22.897521484375" />
                  <Point X="2.588533691406" Y="-22.88985546875" />
                  <Point X="2.816932861328" Y="-22.757990234375" />
                  <Point X="3.967326416016" Y="-22.093810546875" />
                  <Point X="4.090535400391" Y="-22.26504296875" />
                  <Point X="4.123274414062" Y="-22.310541015625" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="4.191583496094" Y="-22.59430078125" />
                  <Point X="3.230784179688" Y="-23.331548828125" />
                  <Point X="3.221424316406" Y="-23.339759765625" />
                  <Point X="3.203973632812" Y="-23.358373046875" />
                  <Point X="3.155034667969" Y="-23.422216796875" />
                  <Point X="3.14924609375" Y="-23.430677734375" />
                  <Point X="3.131933349609" Y="-23.45916796875" />
                  <Point X="3.121629638672" Y="-23.482916015625" />
                  <Point X="3.103399658203" Y="-23.5481015625" />
                  <Point X="3.100105712891" Y="-23.559880859375" />
                  <Point X="3.096652099609" Y="-23.582181640625" />
                  <Point X="3.095836425781" Y="-23.60575" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.112704345703" Y="-23.700759765625" />
                  <Point X="3.115391601562" Y="-23.710912109375" />
                  <Point X="3.125293212891" Y="-23.74139453125" />
                  <Point X="3.136282470703" Y="-23.764259765625" />
                  <Point X="3.176985107422" Y="-23.826125" />
                  <Point X="3.184340087891" Y="-23.8373046875" />
                  <Point X="3.198892578125" Y="-23.854548828125" />
                  <Point X="3.216136230469" Y="-23.870638671875" />
                  <Point X="3.234345947266" Y="-23.88396484375" />
                  <Point X="3.293329589844" Y="-23.91716796875" />
                  <Point X="3.303218994141" Y="-23.922001953125" />
                  <Point X="3.331854492188" Y="-23.934" />
                  <Point X="3.356120361328" Y="-23.9405625" />
                  <Point X="3.435870361328" Y="-23.9511015625" />
                  <Point X="3.441737304688" Y="-23.95169140625" />
                  <Point X="3.469224609375" Y="-23.953599609375" />
                  <Point X="3.488203125" Y="-23.953015625" />
                  <Point X="3.705167236328" Y="-23.924453125" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.831875976562" Y="-24.009439453125" />
                  <Point X="4.845937011719" Y="-24.067193359375" />
                  <Point X="4.890864257812" Y="-24.355759765625" />
                  <Point X="4.817676269531" Y="-24.37537109375" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704788818359" Y="-24.674416015625" />
                  <Point X="3.681547363281" Y="-24.684931640625" />
                  <Point X="3.603195556641" Y="-24.73021875" />
                  <Point X="3.595040039062" Y="-24.7355078125" />
                  <Point X="3.566569580078" Y="-24.756111328125" />
                  <Point X="3.547530517578" Y="-24.774421875" />
                  <Point X="3.500519287109" Y="-24.834326171875" />
                  <Point X="3.492024658203" Y="-24.845150390625" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.448010253906" Y="-24.989220703125" />
                  <Point X="3.446650390625" Y="-24.999111328125" />
                  <Point X="3.443818847656" Y="-25.032708984375" />
                  <Point X="3.445178710938" Y="-25.058556640625" />
                  <Point X="3.460849121094" Y="-25.140380859375" />
                  <Point X="3.463680908203" Y="-25.155166015625" />
                  <Point X="3.470528320312" Y="-25.176669921875" />
                  <Point X="3.480302490234" Y="-25.1981328125" />
                  <Point X="3.492024902344" Y="-25.217408203125" />
                  <Point X="3.539035888672" Y="-25.2773125" />
                  <Point X="3.545959960938" Y="-25.285197265625" />
                  <Point X="3.568766357422" Y="-25.30844140625" />
                  <Point X="3.589035644531" Y="-25.32415625" />
                  <Point X="3.667387695312" Y="-25.3694453125" />
                  <Point X="3.672257324219" Y="-25.37207421875" />
                  <Point X="3.69849609375" Y="-25.385265625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="3.915545898438" Y="-25.44546484375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.862873046875" Y="-25.89665625" />
                  <Point X="4.855022460938" Y="-25.94873046875" />
                  <Point X="4.801174316406" Y="-26.18469921875" />
                  <Point X="4.701344238281" Y="-26.171556640625" />
                  <Point X="3.424381591797" Y="-26.00344140625" />
                  <Point X="3.408035400391" Y="-26.0027109375" />
                  <Point X="3.374659667969" Y="-26.005509765625" />
                  <Point X="3.220882568359" Y="-26.03893359375" />
                  <Point X="3.193095458984" Y="-26.04497265625" />
                  <Point X="3.163975341797" Y="-26.056595703125" />
                  <Point X="3.136148193359" Y="-26.07348828125" />
                  <Point X="3.112397705078" Y="-26.0939609375" />
                  <Point X="3.01944921875" Y="-26.20575" />
                  <Point X="3.002653564453" Y="-26.22594921875" />
                  <Point X="2.987933837891" Y="-26.250328125" />
                  <Point X="2.976590087891" Y="-26.277712890625" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.956435791016" Y="-26.45013671875" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079589844" Y="-26.539189453125" />
                  <Point X="2.976450927734" Y="-26.568" />
                  <Point X="3.061553466797" Y="-26.70037109375" />
                  <Point X="3.076931152344" Y="-26.7242890625" />
                  <Point X="3.086935058594" Y="-26.7372421875" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.295269775391" Y="-26.902591796875" />
                  <Point X="4.213121582031" Y="-27.6068828125" />
                  <Point X="4.146938964844" Y="-27.7139765625" />
                  <Point X="4.124807128906" Y="-27.749791015625" />
                  <Point X="4.028981689453" Y="-27.8859453125" />
                  <Point X="3.93826171875" Y="-27.833568359375" />
                  <Point X="2.800954345703" Y="-27.176943359375" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224365234" Y="-27.159826171875" />
                  <Point X="2.571205078125" Y="-27.1267734375" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.506783935547" Y="-27.120396484375" />
                  <Point X="2.474611572266" Y="-27.125353515625" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.292789550781" Y="-27.215197265625" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242385986328" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204532226563" Y="-27.290439453125" />
                  <Point X="2.124512451172" Y="-27.442484375" />
                  <Point X="2.110053222656" Y="-27.46995703125" />
                  <Point X="2.100229736328" Y="-27.499732421875" />
                  <Point X="2.095271484375" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.128728515625" Y="-27.74627734375" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.270470214844" Y="-28.031587890625" />
                  <Point X="2.861284423828" Y="-29.05490625" />
                  <Point X="2.808111572266" Y="-29.09288671875" />
                  <Point X="2.781883789062" Y="-29.11162109375" />
                  <Point X="2.701763671875" Y="-29.16348046875" />
                  <Point X="2.626416503906" Y="-29.065287109375" />
                  <Point X="1.758546020508" Y="-27.934255859375" />
                  <Point X="1.747504516602" Y="-27.9221796875" />
                  <Point X="1.721923095703" Y="-27.900556640625" />
                  <Point X="1.541416748047" Y="-27.7845078125" />
                  <Point X="1.508799804688" Y="-27.7635390625" />
                  <Point X="1.479987426758" Y="-27.75116796875" />
                  <Point X="1.448369140625" Y="-27.7434375" />
                  <Point X="1.41710168457" Y="-27.741119140625" />
                  <Point X="1.219687011719" Y="-27.75928515625" />
                  <Point X="1.184014770508" Y="-27.76256640625" />
                  <Point X="1.1563671875" Y="-27.769396484375" />
                  <Point X="1.128978881836" Y="-27.780740234375" />
                  <Point X="1.104594604492" Y="-27.7954609375" />
                  <Point X="0.95215612793" Y="-27.922208984375" />
                  <Point X="0.924610900879" Y="-27.94511328125" />
                  <Point X="0.904138916016" Y="-27.9688671875" />
                  <Point X="0.887247680664" Y="-27.996693359375" />
                  <Point X="0.875624511719" Y="-28.025810546875" />
                  <Point X="0.830046081543" Y="-28.235505859375" />
                  <Point X="0.821810241699" Y="-28.273396484375" />
                  <Point X="0.81972479248" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.853367004395" Y="-28.578525390625" />
                  <Point X="1.022065490723" Y="-29.859916015625" />
                  <Point X="1.00052142334" Y="-29.864638671875" />
                  <Point X="0.97571270752" Y="-29.870076171875" />
                  <Point X="0.929315246582" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.019642272949" Y="-29.7601640625" />
                  <Point X="-1.05844934082" Y="-29.75262890625" />
                  <Point X="-1.14124609375" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.174598144531" Y="-29.239970703125" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188125" Y="-29.178470703125" />
                  <Point X="-1.19902734375" Y="-29.14950390625" />
                  <Point X="-1.205666015625" Y="-29.135466796875" />
                  <Point X="-1.22173840332" Y="-29.10762890625" />
                  <Point X="-1.230575561523" Y="-29.094861328125" />
                  <Point X="-1.250209838867" Y="-29.0709375" />
                  <Point X="-1.261007080078" Y="-29.05978125" />
                  <Point X="-1.458568969727" Y="-28.8865234375" />
                  <Point X="-1.494268066406" Y="-28.855216796875" />
                  <Point X="-1.506735595703" Y="-28.84596875" />
                  <Point X="-1.533018798828" Y="-28.829623046875" />
                  <Point X="-1.546834350586" Y="-28.822525390625" />
                  <Point X="-1.576530883789" Y="-28.810224609375" />
                  <Point X="-1.591315673828" Y="-28.805474609375" />
                  <Point X="-1.621457397461" Y="-28.798447265625" />
                  <Point X="-1.636814453125" Y="-28.796169921875" />
                  <Point X="-1.899023071289" Y="-28.778984375" />
                  <Point X="-1.946403564453" Y="-28.77587890625" />
                  <Point X="-1.961927612305" Y="-28.7761328125" />
                  <Point X="-1.992726196289" Y="-28.779166015625" />
                  <Point X="-2.008000488281" Y="-28.7819453125" />
                  <Point X="-2.039048583984" Y="-28.790263671875" />
                  <Point X="-2.053667480469" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436279297" Y="-28.815810546875" />
                  <Point X="-2.313922607422" Y="-28.961798828125" />
                  <Point X="-2.353402832031" Y="-28.988177734375" />
                  <Point X="-2.359682861328" Y="-28.9927578125" />
                  <Point X="-2.380446533203" Y="-29.010134765625" />
                  <Point X="-2.402761962891" Y="-29.032775390625" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.444890136719" Y="-29.086486328125" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.690735595703" Y="-29.046361328125" />
                  <Point X="-2.747613037109" Y="-29.01114453125" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.971993164062" Y="-28.816185546875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684570312" Y="-27.666185546875" />
                  <Point X="-2.313413574219" Y="-27.63466015625" />
                  <Point X="-2.311638916016" Y="-27.619236328125" />
                  <Point X="-2.310626220703" Y="-27.588302734375" />
                  <Point X="-2.314666015625" Y="-27.5576171875" />
                  <Point X="-2.323650146484" Y="-27.528" />
                  <Point X="-2.329356445312" Y="-27.51355859375" />
                  <Point X="-2.343573730469" Y="-27.484728515625" />
                  <Point X="-2.351557128906" Y="-27.47141015625" />
                  <Point X="-2.369585693359" Y="-27.446251953125" />
                  <Point X="-2.379630859375" Y="-27.434412109375" />
                  <Point X="-2.394324462891" Y="-27.41971875" />
                  <Point X="-2.408854003906" Y="-27.40698828125" />
                  <Point X="-2.434000976562" Y="-27.388974609375" />
                  <Point X="-2.447313720703" Y="-27.380998046875" />
                  <Point X="-2.476141113281" Y="-27.36678515625" />
                  <Point X="-2.490583251953" Y="-27.361080078125" />
                  <Point X="-2.520198974609" Y="-27.352099609375" />
                  <Point X="-2.550878173828" Y="-27.3480625" />
                  <Point X="-2.581809082031" Y="-27.349076171875" />
                  <Point X="-2.597234375" Y="-27.3508515625" />
                  <Point X="-2.628757324219" Y="-27.357123046875" />
                  <Point X="-2.643687744141" Y="-27.36138671875" />
                  <Point X="-2.672651123047" Y="-27.3722890625" />
                  <Point X="-2.686684082031" Y="-27.378927734375" />
                  <Point X="-2.884018066406" Y="-27.492859375" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-3.959103027344" Y="-27.799599609375" />
                  <Point X="-4.004018066406" Y="-27.740591796875" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-4.15130078125" Y="-27.4203828125" />
                  <Point X="-3.048122802734" Y="-26.573884765625" />
                  <Point X="-3.036482421875" Y="-26.5633125" />
                  <Point X="-3.015105224609" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.955365478516" Y="-26.41845703125" />
                  <Point X="-2.951552734375" Y="-26.3988046875" />
                  <Point X="-2.948748535156" Y="-26.368376953125" />
                  <Point X="-2.948577880859" Y="-26.353048828125" />
                  <Point X="-2.950786621094" Y="-26.321376953125" />
                  <Point X="-2.953083251953" Y="-26.306220703125" />
                  <Point X="-2.960084960938" Y="-26.27647265625" />
                  <Point X="-2.971780761719" Y="-26.24823828125" />
                  <Point X="-2.987864746094" Y="-26.22225390625" />
                  <Point X="-2.996958007812" Y="-26.209912109375" />
                  <Point X="-3.017790771484" Y="-26.18595703125" />
                  <Point X="-3.028749267578" Y="-26.1752421875" />
                  <Point X="-3.052246826172" Y="-26.1557109375" />
                  <Point X="-3.064785888672" Y="-26.14689453125" />
                  <Point X="-3.087215576172" Y="-26.133693359375" />
                  <Point X="-3.105443603516" Y="-26.1244765625" />
                  <Point X="-3.134712646484" Y="-26.113251953125" />
                  <Point X="-3.149811767578" Y="-26.10885546875" />
                  <Point X="-3.181694091797" Y="-26.102376953125" />
                  <Point X="-3.197307617188" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244329345703" Y="-26.100197265625" />
                  <Point X="-3.493444824219" Y="-26.132994140625" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.723194335938" Y="-26.04289453125" />
                  <Point X="-4.740761230469" Y="-25.974119140625" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.764973632813" Y="-25.64890234375" />
                  <Point X="-3.508287597656" Y="-25.312173828125" />
                  <Point X="-3.500472412109" Y="-25.309712890625" />
                  <Point X="-3.473931884766" Y="-25.299251953125" />
                  <Point X="-3.444167480469" Y="-25.28389453125" />
                  <Point X="-3.433560058594" Y="-25.277515625" />
                  <Point X="-3.410054443359" Y="-25.261201171875" />
                  <Point X="-3.393676757812" Y="-25.248248046875" />
                  <Point X="-3.371227539062" Y="-25.22637109375" />
                  <Point X="-3.360912841797" Y="-25.214501953125" />
                  <Point X="-3.341666015625" Y="-25.188248046875" />
                  <Point X="-3.333445068359" Y="-25.174830078125" />
                  <Point X="-3.319331054688" Y="-25.1468203125" />
                  <Point X="-3.313437988281" Y="-25.132228515625" />
                  <Point X="-3.305602783203" Y="-25.106982421875" />
                  <Point X="-3.300991699219" Y="-25.088505859375" />
                  <Point X="-3.29672265625" Y="-25.060353515625" />
                  <Point X="-3.295648925781" Y="-25.0461171875" />
                  <Point X="-3.295647216797" Y="-25.016474609375" />
                  <Point X="-3.296720214844" Y="-25.002232421875" />
                  <Point X="-3.300989257812" Y="-24.974068359375" />
                  <Point X="-3.304185302734" Y="-24.960146484375" />
                  <Point X="-3.312020507812" Y="-24.934900390625" />
                  <Point X="-3.319326416016" Y="-24.915751953125" />
                  <Point X="-3.333434082031" Y="-24.88775" />
                  <Point X="-3.341651611328" Y="-24.874333984375" />
                  <Point X="-3.360898193359" Y="-24.84807421875" />
                  <Point X="-3.371220214844" Y="-24.836197265625" />
                  <Point X="-3.393680175781" Y="-24.814310546875" />
                  <Point X="-3.405818115234" Y="-24.80430078125" />
                  <Point X="-3.429323730469" Y="-24.78798828125" />
                  <Point X="-3.450901367188" Y="-24.77504296875" />
                  <Point X="-3.47071484375" Y="-24.764875" />
                  <Point X="-3.479910644531" Y="-24.760755859375" />
                  <Point X="-3.498697509766" Y="-24.75351171875" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.735369140625" Y="-24.689541015625" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.742650878906" Y="-24.118970703125" />
                  <Point X="-4.731331054688" Y="-24.04247265625" />
                  <Point X="-4.633586425781" Y="-23.681763671875" />
                  <Point X="-3.778065185547" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.74705859375" Y="-23.795634765625" />
                  <Point X="-3.736706787109" Y="-23.795296875" />
                  <Point X="-3.715141601563" Y="-23.79341015625" />
                  <Point X="-3.704887207031" Y="-23.7919453125" />
                  <Point X="-3.684601806641" Y="-23.78791015625" />
                  <Point X="-3.674570800781" Y="-23.78533984375" />
                  <Point X="-3.622545654297" Y="-23.7689375" />
                  <Point X="-3.6034453125" Y="-23.762322265625" />
                  <Point X="-3.584507080078" Y="-23.75398828125" />
                  <Point X="-3.575268310547" Y="-23.7493046875" />
                  <Point X="-3.556525390625" Y="-23.738482421875" />
                  <Point X="-3.5478515625" Y="-23.732822265625" />
                  <Point X="-3.531169921875" Y="-23.72058984375" />
                  <Point X="-3.515920166016" Y="-23.706615234375" />
                  <Point X="-3.502281738281" Y="-23.6910625" />
                  <Point X="-3.495885253906" Y="-23.682912109375" />
                  <Point X="-3.483472167969" Y="-23.665181640625" />
                  <Point X="-3.478004638672" Y="-23.65638671875" />
                  <Point X="-3.468059814453" Y="-23.6382578125" />
                  <Point X="-3.463582519531" Y="-23.628923828125" />
                  <Point X="-3.44270703125" Y="-23.57852734375" />
                  <Point X="-3.435498291016" Y="-23.559646484375" />
                  <Point X="-3.429708984375" Y="-23.539783203125" />
                  <Point X="-3.427356445313" Y="-23.529693359375" />
                  <Point X="-3.423598876953" Y="-23.508376953125" />
                  <Point X="-3.422359619141" Y="-23.49809375" />
                  <Point X="-3.4210078125" Y="-23.477453125" />
                  <Point X="-3.421910888672" Y="-23.4567890625" />
                  <Point X="-3.42505859375" Y="-23.43634375" />
                  <Point X="-3.427190673828" Y="-23.426205078125" />
                  <Point X="-3.432793701172" Y="-23.405298828125" />
                  <Point X="-3.436016357422" Y="-23.39545703125" />
                  <Point X="-3.44351171875" Y="-23.376185546875" />
                  <Point X="-3.447784423828" Y="-23.366755859375" />
                  <Point X="-3.47297265625" Y="-23.318369140625" />
                  <Point X="-3.482801269531" Y="-23.300712890625" />
                  <Point X="-3.494294921875" Y="-23.28351171875" />
                  <Point X="-3.500511474609" Y="-23.275224609375" />
                  <Point X="-3.514422851562" Y="-23.258646484375" />
                  <Point X="-3.521500976563" Y="-23.25108984375" />
                  <Point X="-3.53644140625" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-3.674557617188" Y="-23.13009375" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.046275878906" Y="-22.395041015625" />
                  <Point X="-4.002295654297" Y="-22.31969140625" />
                  <Point X="-3.726338378906" Y="-21.964986328125" />
                  <Point X="-3.254156738281" Y="-22.2376015625" />
                  <Point X="-3.244917236328" Y="-22.24228515625" />
                  <Point X="-3.225985595703" Y="-22.250615234375" />
                  <Point X="-3.216292724609" Y="-22.254263671875" />
                  <Point X="-3.195646972656" Y="-22.2607734375" />
                  <Point X="-3.185611816406" Y="-22.26334375" />
                  <Point X="-3.165324707031" Y="-22.26737890625" />
                  <Point X="-3.155072753906" Y="-22.26884375" />
                  <Point X="-3.082616210938" Y="-22.27518359375" />
                  <Point X="-3.059162353516" Y="-22.276666015625" />
                  <Point X="-3.038475585938" Y="-22.276212890625" />
                  <Point X="-3.028149902344" Y="-22.275421875" />
                  <Point X="-3.006693603516" Y="-22.272595703125" />
                  <Point X="-2.99651953125" Y="-22.2706875" />
                  <Point X="-2.976429443359" Y="-22.265771484375" />
                  <Point X="-2.956989501953" Y="-22.2586953125" />
                  <Point X="-2.938439697266" Y="-22.249546875" />
                  <Point X="-2.929413818359" Y="-22.244466796875" />
                  <Point X="-2.911161376953" Y="-22.232837890625" />
                  <Point X="-2.902743164062" Y="-22.2268046875" />
                  <Point X="-2.886614257812" Y="-22.213859375" />
                  <Point X="-2.878903564453" Y="-22.206947265625" />
                  <Point X="-2.827473388672" Y="-22.155517578125" />
                  <Point X="-2.811267333984" Y="-22.138513671875" />
                  <Point X="-2.798321777344" Y="-22.122384765625" />
                  <Point X="-2.792288818359" Y="-22.113966796875" />
                  <Point X="-2.780658935547" Y="-22.095712890625" />
                  <Point X="-2.775577148438" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759352294922" Y="-22.048693359375" />
                  <Point X="-2.754435302734" Y="-22.028599609375" />
                  <Point X="-2.752525634766" Y="-22.018419921875" />
                  <Point X="-2.749700195312" Y="-21.9969609375" />
                  <Point X="-2.748909912109" Y="-21.986634765625" />
                  <Point X="-2.748458496094" Y="-21.965955078125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.755136474609" Y="-21.88314453125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.845242431641" Y="-21.670994140625" />
                  <Point X="-3.059386474609" Y="-21.300087890625" />
                  <Point X="-2.724971191406" Y="-21.043693359375" />
                  <Point X="-2.648368408203" Y="-20.984962890625" />
                  <Point X="-2.192522460938" Y="-20.731705078125" />
                  <Point X="-2.118567626953" Y="-20.828083984375" />
                  <Point X="-2.111826904297" Y="-20.83594140625" />
                  <Point X="-2.09751953125" Y="-20.850888671875" />
                  <Point X="-2.089956787109" Y="-20.85797265625" />
                  <Point X="-2.073376953125" Y="-20.871884765625" />
                  <Point X="-2.065095214844" Y="-20.87809765625" />
                  <Point X="-2.047897827148" Y="-20.88958984375" />
                  <Point X="-2.038982299805" Y="-20.894869140625" />
                  <Point X="-1.958338623047" Y="-20.9368515625" />
                  <Point X="-1.934333984375" Y="-20.9487109375" />
                  <Point X="-1.915060302734" Y="-20.95620703125" />
                  <Point X="-1.905218994141" Y="-20.9594296875" />
                  <Point X="-1.884312133789" Y="-20.965033203125" />
                  <Point X="-1.874174072266" Y="-20.967166015625" />
                  <Point X="-1.853724243164" Y="-20.970314453125" />
                  <Point X="-1.833053710938" Y="-20.971216796875" />
                  <Point X="-1.812407226562" Y="-20.96986328125" />
                  <Point X="-1.802119750977" Y="-20.968623046875" />
                  <Point X="-1.780804321289" Y="-20.96486328125" />
                  <Point X="-1.770712890625" Y="-20.9625078125" />
                  <Point X="-1.750859375" Y="-20.95671875" />
                  <Point X="-1.741097412109" Y="-20.95328515625" />
                  <Point X="-1.657101196289" Y="-20.9184921875" />
                  <Point X="-1.63258996582" Y="-20.907728515625" />
                  <Point X="-1.614455932617" Y="-20.89778125" />
                  <Point X="-1.605655395508" Y="-20.892310546875" />
                  <Point X="-1.587926147461" Y="-20.879896484375" />
                  <Point X="-1.579778198242" Y="-20.873501953125" />
                  <Point X="-1.564225952148" Y="-20.85986328125" />
                  <Point X="-1.550250488281" Y="-20.844611328125" />
                  <Point X="-1.53801953125" Y="-20.8279296875" />
                  <Point X="-1.532359619141" Y="-20.819255859375" />
                  <Point X="-1.521537841797" Y="-20.80051171875" />
                  <Point X="-1.516854370117" Y="-20.79126953125" />
                  <Point X="-1.508524169922" Y="-20.7723359375" />
                  <Point X="-1.504877319336" Y="-20.76264453125" />
                  <Point X="-1.477538085938" Y="-20.675935546875" />
                  <Point X="-1.470026611328" Y="-20.650236328125" />
                  <Point X="-1.465991333008" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.47010534668" Y="-20.5069296875" />
                  <Point X="-1.479266357422" Y="-20.43734375" />
                  <Point X="-1.0303359375" Y="-20.31148046875" />
                  <Point X="-0.931167419434" Y="-20.2836796875" />
                  <Point X="-0.365222381592" Y="-20.217443359375" />
                  <Point X="-0.225666183472" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.267552886963" Y="-20.6278984375" />
                  <Point X="0.378190856934" Y="-20.214990234375" />
                  <Point X="0.741282165527" Y="-20.253017578125" />
                  <Point X="0.827851318359" Y="-20.262083984375" />
                  <Point X="1.366450439453" Y="-20.392119140625" />
                  <Point X="1.453621948242" Y="-20.413166015625" />
                  <Point X="1.802959838867" Y="-20.539873046875" />
                  <Point X="1.858248535156" Y="-20.55992578125" />
                  <Point X="2.197234863281" Y="-20.718458984375" />
                  <Point X="2.250431396484" Y="-20.7433359375" />
                  <Point X="2.577967529297" Y="-20.93416015625" />
                  <Point X="2.629450683594" Y="-20.964154296875" />
                  <Point X="2.817779785156" Y="-21.09808203125" />
                  <Point X="2.795069824219" Y="-21.13741796875" />
                  <Point X="2.065308349609" Y="-22.40140234375" />
                  <Point X="2.062372558594" Y="-22.406896484375" />
                  <Point X="2.053182128906" Y="-22.426560546875" />
                  <Point X="2.044182373047" Y="-22.450435546875" />
                  <Point X="2.041301635742" Y="-22.45940234375" />
                  <Point X="2.023117797852" Y="-22.52740234375" />
                  <Point X="2.01872277832" Y="-22.54862109375" />
                  <Point X="2.014098632812" Y="-22.579810546875" />
                  <Point X="2.013080200195" Y="-22.592451171875" />
                  <Point X="2.012736694336" Y="-22.617755859375" />
                  <Point X="2.013410888672" Y="-22.630419921875" />
                  <Point X="2.020501342773" Y="-22.68921875" />
                  <Point X="2.023800537109" Y="-22.71096484375" />
                  <Point X="2.029143554687" Y="-22.732888671875" />
                  <Point X="2.032468505859" Y="-22.74369140625" />
                  <Point X="2.040736938477" Y="-22.76578515625" />
                  <Point X="2.045320678711" Y="-22.776115234375" />
                  <Point X="2.055682617188" Y="-22.796158203125" />
                  <Point X="2.061460449219" Y="-22.80587109375" />
                  <Point X="2.09784375" Y="-22.8594921875" />
                  <Point X="2.111341308594" Y="-22.87708203125" />
                  <Point X="2.131214111328" Y="-22.90006640625" />
                  <Point X="2.139936523438" Y="-22.908912109375" />
                  <Point X="2.158458984375" Y="-22.925388671875" />
                  <Point X="2.168258056641" Y="-22.93301953125" />
                  <Point X="2.221867919922" Y="-22.969396484375" />
                  <Point X="2.221868408203" Y="-22.969396484375" />
                  <Point X="2.241257080078" Y="-22.9817421875" />
                  <Point X="2.261305908203" Y="-22.992109375" />
                  <Point X="2.271639648438" Y="-22.996697265625" />
                  <Point X="2.293733154297" Y="-23.004966796875" />
                  <Point X="2.304543457031" Y="-23.00829296875" />
                  <Point X="2.326472900391" Y="-23.01363671875" />
                  <Point X="2.337592041016" Y="-23.015654296875" />
                  <Point X="2.396391845703" Y="-23.022744140625" />
                  <Point X="2.419500732422" Y="-23.024111328125" />
                  <Point X="2.449028564453" Y="-23.0240546875" />
                  <Point X="2.461313476562" Y="-23.023232421875" />
                  <Point X="2.485672851562" Y="-23.0200078125" />
                  <Point X="2.497747314453" Y="-23.01760546875" />
                  <Point X="2.565746337891" Y="-22.999421875" />
                  <Point X="2.577844970703" Y="-22.99575" />
                  <Point X="2.602215576172" Y="-22.9874609375" />
                  <Point X="2.610851806641" Y="-22.984044921875" />
                  <Point X="2.636033447266" Y="-22.97212890625" />
                  <Point X="2.864432617188" Y="-22.840263671875" />
                  <Point X="3.940405029297" Y="-22.21905078125" />
                  <Point X="4.013422851562" Y="-22.320529296875" />
                  <Point X="4.043957763672" Y="-22.36296484375" />
                  <Point X="4.136884277344" Y="-22.51652734375" />
                  <Point X="4.133751953125" Y="-22.518931640625" />
                  <Point X="3.172952880859" Y="-23.2561796875" />
                  <Point X="3.168135253906" Y="-23.2601328125" />
                  <Point X="3.152119628906" Y="-23.274783203125" />
                  <Point X="3.134668945312" Y="-23.293396484375" />
                  <Point X="3.128576416016" Y="-23.300578125" />
                  <Point X="3.079637451172" Y="-23.364421875" />
                  <Point X="3.068060302734" Y="-23.38134375" />
                  <Point X="3.050747558594" Y="-23.409833984375" />
                  <Point X="3.044782714844" Y="-23.42135546875" />
                  <Point X="3.034479003906" Y="-23.445103515625" />
                  <Point X="3.030140136719" Y="-23.457330078125" />
                  <Point X="3.01191015625" Y="-23.522515625" />
                  <Point X="3.006224853516" Y="-23.545341796875" />
                  <Point X="3.002771240234" Y="-23.567642578125" />
                  <Point X="3.001708984375" Y="-23.578896484375" />
                  <Point X="3.000893310547" Y="-23.60246484375" />
                  <Point X="3.001175048828" Y="-23.613763671875" />
                  <Point X="3.003078125" Y="-23.63624609375" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.019664306641" Y="-23.71995703125" />
                  <Point X="3.025038818359" Y="-23.74026171875" />
                  <Point X="3.034940429688" Y="-23.770744140625" />
                  <Point X="3.039668945312" Y="-23.782546875" />
                  <Point X="3.050658203125" Y="-23.805412109375" />
                  <Point X="3.056918945312" Y="-23.816474609375" />
                  <Point X="3.097621582031" Y="-23.87833984375" />
                  <Point X="3.111738037109" Y="-23.89857421875" />
                  <Point X="3.126290527344" Y="-23.915818359375" />
                  <Point X="3.134081542969" Y="-23.9240078125" />
                  <Point X="3.151325195312" Y="-23.94009765625" />
                  <Point X="3.160032226562" Y="-23.947302734375" />
                  <Point X="3.178241943359" Y="-23.96062890625" />
                  <Point X="3.187744628906" Y="-23.96675" />
                  <Point X="3.246728271484" Y="-23.999953125" />
                  <Point X="3.266507080078" Y="-24.00962109375" />
                  <Point X="3.295142578125" Y="-24.021619140625" />
                  <Point X="3.307053466797" Y="-24.025705078125" />
                  <Point X="3.331319335938" Y="-24.032267578125" />
                  <Point X="3.343674316406" Y="-24.034744140625" />
                  <Point X="3.423424316406" Y="-24.045283203125" />
                  <Point X="3.435158203125" Y="-24.046462890625" />
                  <Point X="3.462645507813" Y="-24.04837109375" />
                  <Point X="3.472146484375" Y="-24.0485546875" />
                  <Point X="3.500602539062" Y="-24.047203125" />
                  <Point X="3.717566650391" Y="-24.018640625" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.739571777344" Y="-24.03191015625" />
                  <Point X="4.752684570312" Y="-24.08576953125" />
                  <Point X="4.783870605469" Y="-24.286078125" />
                  <Point X="3.691991210938" Y="-24.578646484375" />
                  <Point X="3.686020263672" Y="-24.580458984375" />
                  <Point X="3.665627929688" Y="-24.58786328125" />
                  <Point X="3.642386474609" Y="-24.59837890625" />
                  <Point X="3.634007568359" Y="-24.602681640625" />
                  <Point X="3.555655761719" Y="-24.64796875" />
                  <Point X="3.539344726562" Y="-24.658546875" />
                  <Point X="3.510874267578" Y="-24.679150390625" />
                  <Point X="3.500717285156" Y="-24.687638671875" />
                  <Point X="3.481678222656" Y="-24.70594921875" />
                  <Point X="3.472796142578" Y="-24.715771484375" />
                  <Point X="3.425784912109" Y="-24.77567578125" />
                  <Point X="3.410853271484" Y="-24.79579296875" />
                  <Point X="3.39912890625" Y="-24.81507421875" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.380004150391" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.354705810547" Y="-24.9713515625" />
                  <Point X="3.351986083984" Y="-24.9911328125" />
                  <Point X="3.349154541016" Y="-25.02473046875" />
                  <Point X="3.348949951172" Y="-25.03769921875" />
                  <Point X="3.350309814453" Y="-25.063546875" />
                  <Point X="3.351874267578" Y="-25.07642578125" />
                  <Point X="3.367544677734" Y="-25.15825" />
                  <Point X="3.373159423828" Y="-25.183990234375" />
                  <Point X="3.380006835938" Y="-25.205494140625" />
                  <Point X="3.384071289062" Y="-25.21604296875" />
                  <Point X="3.393845458984" Y="-25.237505859375" />
                  <Point X="3.399134033203" Y="-25.24749609375" />
                  <Point X="3.410856445312" Y="-25.266771484375" />
                  <Point X="3.417290283203" Y="-25.276056640625" />
                  <Point X="3.464301269531" Y="-25.3359609375" />
                  <Point X="3.478149414062" Y="-25.35173046875" />
                  <Point X="3.500955810547" Y="-25.374974609375" />
                  <Point X="3.510557861328" Y="-25.38351953125" />
                  <Point X="3.530827148438" Y="-25.399234375" />
                  <Point X="3.541494384766" Y="-25.406404296875" />
                  <Point X="3.619846435547" Y="-25.451693359375" />
                  <Point X="3.629585693359" Y="-25.456951171875" />
                  <Point X="3.655824462891" Y="-25.470142578125" />
                  <Point X="3.664687011719" Y="-25.474046875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="3.890958251953" Y="-25.537228515625" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.768934570312" Y="-25.882494140625" />
                  <Point X="4.76161328125" Y="-25.93105859375" />
                  <Point X="4.727802734375" Y="-26.079220703125" />
                  <Point X="4.713744140625" Y="-26.077369140625" />
                  <Point X="3.43678125" Y="-25.90925390625" />
                  <Point X="3.428622558594" Y="-25.90853515625" />
                  <Point X="3.400096679688" Y="-25.90804296875" />
                  <Point X="3.366720947266" Y="-25.910841796875" />
                  <Point X="3.354482177734" Y="-25.912677734375" />
                  <Point X="3.200705078125" Y="-25.9461015625" />
                  <Point X="3.17291796875" Y="-25.952140625" />
                  <Point X="3.157878662109" Y="-25.9567421875" />
                  <Point X="3.128758544922" Y="-25.968365234375" />
                  <Point X="3.114677734375" Y="-25.97538671875" />
                  <Point X="3.086850585938" Y="-25.992279296875" />
                  <Point X="3.074122314453" Y="-26.00153125" />
                  <Point X="3.050371826172" Y="-26.02200390625" />
                  <Point X="3.039349609375" Y="-26.033224609375" />
                  <Point X="2.946401123047" Y="-26.145013671875" />
                  <Point X="2.92960546875" Y="-26.165212890625" />
                  <Point X="2.921328125" Y="-26.176845703125" />
                  <Point X="2.906608398438" Y="-26.201224609375" />
                  <Point X="2.900166015625" Y="-26.213970703125" />
                  <Point X="2.888822265625" Y="-26.24135546875" />
                  <Point X="2.884363769531" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.861835449219" Y="-26.441431640625" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871798095703" Y="-26.56175390625" />
                  <Point X="2.876787109375" Y="-26.576673828125" />
                  <Point X="2.889158447266" Y="-26.605484375" />
                  <Point X="2.896540771484" Y="-26.619375" />
                  <Point X="2.981643310547" Y="-26.75174609375" />
                  <Point X="2.997020996094" Y="-26.7756640625" />
                  <Point X="3.001744140625" Y="-26.782357421875" />
                  <Point X="3.019795654297" Y="-26.804453125" />
                  <Point X="3.043488769531" Y="-26.82812109375" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.237437255859" Y="-26.9779609375" />
                  <Point X="4.087169677734" Y="-27.629982421875" />
                  <Point X="4.066125488281" Y="-27.66403515625" />
                  <Point X="4.045490234375" Y="-27.697427734375" />
                  <Point X="4.001274902344" Y="-27.760251953125" />
                  <Point X="3.985761474609" Y="-27.751294921875" />
                  <Point X="2.848454101562" Y="-27.094669921875" />
                  <Point X="2.841182617188" Y="-27.090880859375" />
                  <Point X="2.815026123047" Y="-27.079515625" />
                  <Point X="2.78312109375" Y="-27.069328125" />
                  <Point X="2.771107910156" Y="-27.066337890625" />
                  <Point X="2.588088623047" Y="-27.03328515625" />
                  <Point X="2.555017578125" Y="-27.0273125" />
                  <Point X="2.539359130859" Y="-27.02580859375" />
                  <Point X="2.508009033203" Y="-27.025404296875" />
                  <Point X="2.492317382813" Y="-27.02650390625" />
                  <Point X="2.460145019531" Y="-27.0314609375" />
                  <Point X="2.444847412109" Y="-27.03513671875" />
                  <Point X="2.415069580078" Y="-27.0449609375" />
                  <Point X="2.400589355469" Y="-27.051109375" />
                  <Point X="2.248545166016" Y="-27.13112890625" />
                  <Point X="2.221071289062" Y="-27.145587890625" />
                  <Point X="2.208968261719" Y="-27.153171875" />
                  <Point X="2.186038574219" Y="-27.170064453125" />
                  <Point X="2.175211914062" Y="-27.179373046875" />
                  <Point X="2.154252197266" Y="-27.20033203125" />
                  <Point X="2.144941162109" Y="-27.21116015625" />
                  <Point X="2.128047119141" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.040444213867" Y="-27.398240234375" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836303711" Y="-27.440193359375" />
                  <Point X="2.010012817383" Y="-27.46996875" />
                  <Point X="2.006338134766" Y="-27.485263671875" />
                  <Point X="2.001379882812" Y="-27.5174375" />
                  <Point X="2.000279418945" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.035240844727" Y="-27.76316015625" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236816406" Y="-27.831546875" />
                  <Point X="2.0640703125" Y="-27.862482421875" />
                  <Point X="2.069547119141" Y="-27.873580078125" />
                  <Point X="2.188197998047" Y="-28.079087890625" />
                  <Point X="2.73589453125" Y="-29.027724609375" />
                  <Point X="2.723752197266" Y="-29.036083984375" />
                  <Point X="2.701784912109" Y="-29.007455078125" />
                  <Point X="1.833914306641" Y="-27.876423828125" />
                  <Point X="1.828657470703" Y="-27.87015234375" />
                  <Point X="1.808831420898" Y="-27.849626953125" />
                  <Point X="1.78325" Y="-27.82800390625" />
                  <Point X="1.773297851562" Y="-27.820646484375" />
                  <Point X="1.592791503906" Y="-27.70459765625" />
                  <Point X="1.560174560547" Y="-27.68362890625" />
                  <Point X="1.546280883789" Y="-27.67624609375" />
                  <Point X="1.517468505859" Y="-27.663875" />
                  <Point X="1.502549804688" Y="-27.65888671875" />
                  <Point X="1.470931518555" Y="-27.65115625" />
                  <Point X="1.455393676758" Y="-27.648697265625" />
                  <Point X="1.424126220703" Y="-27.64637890625" />
                  <Point X="1.408396484375" Y="-27.64651953125" />
                  <Point X="1.210981933594" Y="-27.664685546875" />
                  <Point X="1.175309814453" Y="-27.667966796875" />
                  <Point X="1.161230834961" Y="-27.67033984375" />
                  <Point X="1.133583251953" Y="-27.677169921875" />
                  <Point X="1.120014648438" Y="-27.681626953125" />
                  <Point X="1.092626342773" Y="-27.692970703125" />
                  <Point X="1.079880981445" Y="-27.699412109375" />
                  <Point X="1.055496459961" Y="-27.7141328125" />
                  <Point X="1.043857421875" Y="-27.722412109375" />
                  <Point X="0.891418945312" Y="-27.84916015625" />
                  <Point X="0.863873718262" Y="-27.872064453125" />
                  <Point X="0.852648681641" Y="-27.88309375" />
                  <Point X="0.832176635742" Y="-27.90684765625" />
                  <Point X="0.822929931641" Y="-27.9195703125" />
                  <Point X="0.806038635254" Y="-27.947396484375" />
                  <Point X="0.799017700195" Y="-27.96147265625" />
                  <Point X="0.787394470215" Y="-27.99058984375" />
                  <Point X="0.782792053223" Y="-28.0056328125" />
                  <Point X="0.737213562012" Y="-28.215328125" />
                  <Point X="0.728977783203" Y="-28.25321875" />
                  <Point X="0.727584899902" Y="-28.2612890625" />
                  <Point X="0.72472479248" Y="-28.28967578125" />
                  <Point X="0.7247421875" Y="-28.32316796875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.759179748535" Y="-28.59092578125" />
                  <Point X="0.833091674805" Y="-29.152341796875" />
                  <Point X="0.655064819336" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642143859863" Y="-28.453576171875" />
                  <Point X="0.62678692627" Y="-28.423814453125" />
                  <Point X="0.620407592773" Y="-28.413208984375" />
                  <Point X="0.481737365723" Y="-28.213412109375" />
                  <Point X="0.456679840088" Y="-28.17730859375" />
                  <Point X="0.446670379639" Y="-28.165171875" />
                  <Point X="0.424788482666" Y="-28.14271875" />
                  <Point X="0.412915893555" Y="-28.13240234375" />
                  <Point X="0.386661346436" Y="-28.113158203125" />
                  <Point X="0.373245330811" Y="-28.104939453125" />
                  <Point X="0.345241943359" Y="-28.090830078125" />
                  <Point X="0.330654754639" Y="-28.084939453125" />
                  <Point X="0.116070480347" Y="-28.018341796875" />
                  <Point X="0.077295509338" Y="-28.006306640625" />
                  <Point X="0.063381134033" Y="-28.003111328125" />
                  <Point X="0.03522838974" Y="-27.998841796875" />
                  <Point X="0.020990167618" Y="-27.997767578125" />
                  <Point X="-0.008651034355" Y="-27.997765625" />
                  <Point X="-0.022895202637" Y="-27.998837890625" />
                  <Point X="-0.051061172485" Y="-28.003107421875" />
                  <Point X="-0.064982826233" Y="-28.0063046875" />
                  <Point X="-0.279567108154" Y="-28.072904296875" />
                  <Point X="-0.318341918945" Y="-28.0849375" />
                  <Point X="-0.332927032471" Y="-28.090828125" />
                  <Point X="-0.360927581787" Y="-28.104935546875" />
                  <Point X="-0.374343322754" Y="-28.11315234375" />
                  <Point X="-0.400599060059" Y="-28.132396484375" />
                  <Point X="-0.412475646973" Y="-28.142716796875" />
                  <Point X="-0.434360076904" Y="-28.165173828125" />
                  <Point X="-0.444367767334" Y="-28.177310546875" />
                  <Point X="-0.583037963867" Y="-28.377109375" />
                  <Point X="-0.60809552002" Y="-28.4132109375" />
                  <Point X="-0.612467834473" Y="-28.42012890625" />
                  <Point X="-0.625975280762" Y="-28.44526171875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.704059387207" Y="-28.716734375" />
                  <Point X="-0.985425170898" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.720289706671" Y="-24.001726545965" />
                  <Point X="-4.712855633645" Y="-24.427624304402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691880010715" Y="-25.629316937227" />
                  <Point X="-4.681834672862" Y="-26.204813957411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.630853928999" Y="-23.682123409829" />
                  <Point X="-4.617394684897" Y="-24.45320298802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.597307860834" Y="-25.603976367858" />
                  <Point X="-4.585564157164" Y="-26.276772700521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.535620612659" Y="-23.694661041575" />
                  <Point X="-4.52193373615" Y="-24.478781671639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.502735710954" Y="-25.578635798489" />
                  <Point X="-4.49076752639" Y="-26.264292632964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.440387296319" Y="-23.70719867332" />
                  <Point X="-4.426472787402" Y="-24.504360355257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.408163561073" Y="-25.55329522912" />
                  <Point X="-4.395970895615" Y="-26.251812565406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.345153979978" Y="-23.719736305066" />
                  <Point X="-4.331011838655" Y="-24.529939038875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.313591411193" Y="-25.527954659751" />
                  <Point X="-4.30117426484" Y="-26.239332497849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.249920663638" Y="-23.732273936811" />
                  <Point X="-4.235550889907" Y="-24.555517722494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.219019261312" Y="-25.502614090383" />
                  <Point X="-4.206377634066" Y="-26.226852430292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.174415676391" Y="-22.614576351752" />
                  <Point X="-4.172080982023" Y="-22.748330902503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.154687347298" Y="-23.744811568557" />
                  <Point X="-4.14008994116" Y="-24.581096406112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.124447111432" Y="-25.477273521014" />
                  <Point X="-4.111581003291" Y="-26.214372362735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091333202707" Y="-27.374368081331" />
                  <Point X="-4.087380729977" Y="-27.600805092351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.082160096047" Y="-22.456519602499" />
                  <Point X="-4.075776636129" Y="-22.822227776326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.059454030957" Y="-23.757349200302" />
                  <Point X="-4.044628992412" Y="-24.606675089731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.029874961551" Y="-25.451932951645" />
                  <Point X="-4.016784372516" Y="-26.201892295177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.997574511026" Y="-27.302424522875" />
                  <Point X="-3.989595537455" Y="-27.759539612615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.989814009152" Y="-22.303647970142" />
                  <Point X="-3.979472290234" Y="-22.896124650148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.964220714617" Y="-23.769886832048" />
                  <Point X="-3.949168043665" Y="-24.632253773349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.935302811671" Y="-25.426592382276" />
                  <Point X="-3.921987741742" Y="-26.18941222762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.903815819346" Y="-27.230480964419" />
                  <Point X="-3.892351030923" Y="-27.887298253304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.896884511846" Y="-22.184199897772" />
                  <Point X="-3.883167944339" Y="-22.970021523971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.868987398277" Y="-23.782424463794" />
                  <Point X="-3.853707094917" Y="-24.657832456967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.84073066179" Y="-25.401251812907" />
                  <Point X="-3.827191110967" Y="-26.176932160063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.810057127666" Y="-27.158537405963" />
                  <Point X="-3.795106506452" Y="-28.015057921672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803955014541" Y="-22.064751825402" />
                  <Point X="-3.786863598444" Y="-23.043918397794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.77375824458" Y="-23.794723617829" />
                  <Point X="-3.75824614617" Y="-24.683411140586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.74615851191" Y="-25.375911243538" />
                  <Point X="-3.732394480192" Y="-26.164452092505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716298435986" Y="-27.086593847507" />
                  <Point X="-3.700974014593" Y="-27.964529361109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710522569232" Y="-21.974117624834" />
                  <Point X="-3.69055925255" Y="-23.117815271616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.678888257418" Y="-23.786446134876" />
                  <Point X="-3.662785199384" Y="-24.708989711787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.651586362029" Y="-25.35057067417" />
                  <Point X="-3.637597849418" Y="-26.151972024948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.622539744306" Y="-27.014650289051" />
                  <Point X="-3.606907516601" Y="-27.910220014442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.614540820421" Y="-22.029532924153" />
                  <Point X="-3.594254906704" Y="-23.191712142616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.584440925587" Y="-23.753954744234" />
                  <Point X="-3.567324253218" Y="-24.734568247557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557014212149" Y="-25.325230104801" />
                  <Point X="-3.542801218643" Y="-26.139491957391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.528781052626" Y="-26.942706730595" />
                  <Point X="-3.512841018609" Y="-27.855910667776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.518559071609" Y="-22.084948223471" />
                  <Point X="-3.497717712376" Y="-23.278948894229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.490793458281" Y="-23.675639145692" />
                  <Point X="-3.471789175678" Y="-24.764393766838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.462555648207" Y="-25.293382201376" />
                  <Point X="-3.44800458972" Y="-26.127011783769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435022360945" Y="-26.870763172138" />
                  <Point X="-3.418774520617" Y="-27.80160132111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422577322797" Y="-22.14036352279" />
                  <Point X="-3.375595796508" Y="-24.83193336127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.368760413903" Y="-25.223532168409" />
                  <Point X="-3.353207962808" Y="-26.114531494942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.341263669265" Y="-26.798819613682" />
                  <Point X="-3.324708022626" Y="-27.747291974444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.326595573985" Y="-22.195778822109" />
                  <Point X="-3.258411335895" Y="-26.102051206114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.247504977585" Y="-26.726876055226" />
                  <Point X="-3.230641524634" Y="-27.692982627777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.230659829542" Y="-22.248558532908" />
                  <Point X="-3.163326029406" Y="-26.106109359156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.153746285905" Y="-26.65493249677" />
                  <Point X="-3.136575026642" Y="-27.638673281111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.135261020271" Y="-22.270577248322" />
                  <Point X="-3.067628857134" Y="-26.145221279436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.059987594225" Y="-26.582988938314" />
                  <Point X="-3.04250852865" Y="-27.584363934445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.057215556626" Y="-21.298423458625" />
                  <Point X="-3.057117918868" Y="-21.30401712205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.040147539354" Y="-22.276249513258" />
                  <Point X="-2.970773785985" Y="-26.250669181966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.967143653498" Y="-26.458639332832" />
                  <Point X="-2.948442030658" Y="-27.530054587779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.927342459597" Y="-28.738848204288" />
                  <Point X="-2.924973210998" Y="-28.874582365608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963455844619" Y="-21.226538354631" />
                  <Point X="-2.959141332633" Y="-21.473716580809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945538058015" Y="-22.253047661707" />
                  <Point X="-2.854375534616" Y="-27.475745129373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.835116270227" Y="-28.579107647267" />
                  <Point X="-2.828664359331" Y="-28.948737374965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.869696132613" Y="-21.154653250638" />
                  <Point X="-2.861164746398" Y="-21.643416039568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.851801338874" Y="-22.179845297356" />
                  <Point X="-2.760309042814" Y="-27.421435428118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.742890080858" Y="-28.419367090245" />
                  <Point X="-2.732396107958" Y="-29.020566394993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.775936420606" Y="-21.082768146644" />
                  <Point X="-2.762796630665" Y="-21.835546208213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.759094520785" Y="-22.047639941176" />
                  <Point X="-2.666194845012" Y="-27.369858801766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.650663891488" Y="-28.259626533223" />
                  <Point X="-2.636343527937" Y="-29.080039611599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.682176703755" Y="-21.010883320176" />
                  <Point X="-2.571549005173" Y="-27.348739927259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.558437702118" Y="-28.099885976201" />
                  <Point X="-2.540290943117" Y="-29.1395131031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.588198184758" Y="-20.951533660292" />
                  <Point X="-2.476220097986" Y="-27.366753954921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466211512749" Y="-27.94014541918" />
                  <Point X="-2.44617287795" Y="-29.088158037917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.494096280331" Y="-20.899252746948" />
                  <Point X="-2.380031644966" Y="-27.434011330444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.373985323379" Y="-27.780404862158" />
                  <Point X="-2.352909325093" Y="-28.987847995287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.399994375904" Y="-20.846971833604" />
                  <Point X="-2.258990227442" Y="-28.925094088716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.305892471476" Y="-20.79469092026" />
                  <Point X="-2.16507114693" Y="-28.862339200306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.211790567049" Y="-20.742410006915" />
                  <Point X="-2.071085715795" Y="-28.803385536521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.115212382725" Y="-20.831995073844" />
                  <Point X="-1.976521854252" Y="-28.777570128589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.018918118219" Y="-20.905314385309" />
                  <Point X="-1.881462607607" Y="-28.780135314179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.923069686568" Y="-20.953091949595" />
                  <Point X="-1.786339312427" Y="-28.786369837894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.82774491985" Y="-20.970868769988" />
                  <Point X="-1.691216017246" Y="-28.792604361609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.733095229575" Y="-20.949970486843" />
                  <Point X="-1.595995941576" Y="-28.804383435867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.638770718782" Y="-20.910442683602" />
                  <Point X="-1.500170594691" Y="-28.850838474839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.545029629783" Y="-20.837490668229" />
                  <Point X="-1.40369321633" Y="-28.934648371968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.457108185537" Y="-20.431131428232" />
                  <Point X="-1.30720167762" Y="-29.019269515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.362556426728" Y="-20.404622655194" />
                  <Point X="-1.210299044476" Y="-29.127442242412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.268004667918" Y="-20.378113882155" />
                  <Point X="-1.104579049248" Y="-29.740761305244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.173452909109" Y="-20.351605109117" />
                  <Point X="-1.009190485208" Y="-29.762193071751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.078901150299" Y="-20.325096336079" />
                  <Point X="-0.918457946088" Y="-29.516881349225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.984349372494" Y="-20.298588651275" />
                  <Point X="-0.829254478102" Y="-29.183969200132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.889679892418" Y="-20.278824125089" />
                  <Point X="-0.740051010116" Y="-28.851057051038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.794859128964" Y="-20.267726617767" />
                  <Point X="-0.650847542884" Y="-28.518144858707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.700038365511" Y="-20.256629110445" />
                  <Point X="-0.558901868654" Y="-28.342333600106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.605217602058" Y="-20.245531603124" />
                  <Point X="-0.466218351894" Y="-28.208793311723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.510396838604" Y="-20.234434095802" />
                  <Point X="-0.372888849488" Y="-28.11226151622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.415576075151" Y="-20.22333658848" />
                  <Point X="-0.278566780373" Y="-28.072593829403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.317559567396" Y="-20.395323149563" />
                  <Point X="-0.184064272212" Y="-28.043263488559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.216138157135" Y="-20.762376444645" />
                  <Point X="-0.089561764052" Y="-28.013933147716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.11908536741" Y="-20.879151636775" />
                  <Point X="0.005170517851" Y="-27.997766535733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.023487970481" Y="-20.912547431476" />
                  <Point X="0.100459554905" Y="-28.013496405067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.071388855229" Y="-20.904661728649" />
                  <Point X="0.195991558793" Y="-28.043145834948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.165523480583" Y="-20.854255395969" />
                  <Point X="0.291523555006" Y="-28.072794825144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257257553471" Y="-20.666321504573" />
                  <Point X="0.38725010428" Y="-28.113589752704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.346461000452" Y="-20.3334081521" />
                  <Point X="0.484065533079" Y="-28.216766546533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.439520596498" Y="-20.221413431608" />
                  <Point X="0.58153120345" Y="-28.35719565503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.534709082398" Y="-20.231382729106" />
                  <Point X="0.680483611547" Y="-28.58279991084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.629897568297" Y="-20.241352026604" />
                  <Point X="0.766714707552" Y="-28.079600684955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.778155851594" Y="-28.735063388114" />
                  <Point X="0.78211894204" Y="-28.962108687722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725086054196" Y="-20.251321324102" />
                  <Point X="0.858203862242" Y="-27.877635439429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.820274537387" Y="-20.261290466452" />
                  <Point X="0.951844319918" Y="-27.798918259416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.915672958667" Y="-20.283286953842" />
                  <Point X="1.045502939757" Y="-27.721241589014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.011089535731" Y="-20.306323585395" />
                  <Point X="1.139721667578" Y="-27.675653483412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.106506112794" Y="-20.329360216948" />
                  <Point X="1.23450690668" Y="-27.662520787353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.201922689858" Y="-20.352396848501" />
                  <Point X="1.329369009503" Y="-27.65379161093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.297339266922" Y="-20.375433480053" />
                  <Point X="1.424254257133" Y="-27.64638839965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.392755848578" Y="-20.398470374708" />
                  <Point X="1.519589856792" Y="-27.664785838784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.488246040344" Y="-20.425724389731" />
                  <Point X="1.615554694032" Y="-27.71923227477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.583865886577" Y="-20.460406304204" />
                  <Point X="1.711647517582" Y="-27.781011041606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.67948573281" Y="-20.495088218677" />
                  <Point X="1.807845132101" Y="-27.848793278991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.775105579043" Y="-20.52977013315" />
                  <Point X="1.904957990095" Y="-27.969009779989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.870748458159" Y="-20.565771600589" />
                  <Point X="2.002184151146" Y="-28.095717408722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.96654493346" Y="-20.610572587583" />
                  <Point X="2.083593031671" Y="-27.316253643038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.094062366151" Y="-27.916041413704" />
                  <Point X="2.099410312197" Y="-28.222425037455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.062341408761" Y="-20.655373574577" />
                  <Point X="2.092011146773" Y="-22.35515172686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.100883590156" Y="-22.863453667867" />
                  <Point X="2.176203363867" Y="-27.178520613778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.192038947629" Y="-28.085740599903" />
                  <Point X="2.196636473247" Y="-28.349132666187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.158137884062" Y="-20.700174561571" />
                  <Point X="2.18423733712" Y="-22.195411225859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.197458188075" Y="-22.952833269756" />
                  <Point X="2.27019175443" Y="-27.119736495502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.290015546419" Y="-28.255440777932" />
                  <Point X="2.293862634298" Y="-28.47584029492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.253941434864" Y="-20.745380903733" />
                  <Point X="2.276463527468" Y="-22.035670724859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293380348325" Y="-23.004834742686" />
                  <Point X="2.36434132541" Y="-27.070186397177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.387992145209" Y="-28.425140955961" />
                  <Point X="2.391088795348" Y="-28.602547923653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.349932074167" Y="-20.801305538963" />
                  <Point X="2.368689717815" Y="-21.875930223858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.388691221987" Y="-23.021815630416" />
                  <Point X="2.458685960724" Y="-27.031811527031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.485968743999" Y="-28.59484113399" />
                  <Point X="2.488314956399" Y="-28.729255552386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44592271347" Y="-20.857230174194" />
                  <Point X="2.460915908163" Y="-21.716189722857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483678745236" Y="-23.020271785363" />
                  <Point X="2.553619557352" Y="-27.027178227927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.583945342789" Y="-28.764541312019" />
                  <Point X="2.58554111745" Y="-28.855963181118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.541913352773" Y="-20.913154809424" />
                  <Point X="2.55314209851" Y="-21.556449221857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578262706981" Y="-22.995607917272" />
                  <Point X="2.648932425234" Y="-27.044273364454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.681921941579" Y="-28.934241490048" />
                  <Point X="2.6827672785" Y="-28.982670809851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.637923192017" Y="-20.970179408492" />
                  <Point X="2.645368288858" Y="-21.396708720856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.672499856478" Y="-22.951075188764" />
                  <Point X="2.744247360756" Y="-27.061486955958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734131895773" Y="-21.038596947865" />
                  <Point X="2.737594479205" Y="-21.236968219856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.766566362708" Y="-22.896766314107" />
                  <Point X="2.839764146183" Y="-27.090264520835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860632868939" Y="-22.842457439451" />
                  <Point X="2.918904886445" Y="-26.180859086479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.927396960095" Y="-26.667369660093" />
                  <Point X="2.935735104869" Y="-27.145061654273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.954699367844" Y="-22.788148145054" />
                  <Point X="3.011918259792" Y="-26.066216269317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.024893114097" Y="-26.809545174655" />
                  <Point X="3.031716850384" Y="-27.200476764683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.048765866439" Y="-22.733838832988" />
                  <Point X="3.060291267984" Y="-23.394128645283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.067956167331" Y="-23.833250434778" />
                  <Point X="3.105445125498" Y="-25.980991409691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.121291656398" Y="-26.888838556932" />
                  <Point X="3.127698595898" Y="-27.255891875094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.142832365035" Y="-22.679529520922" />
                  <Point X="3.15320522289" Y="-23.273790149409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.16502520867" Y="-23.950956681247" />
                  <Point X="3.199853821412" Y="-25.946286568879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.217596013847" Y="-26.962736092738" />
                  <Point X="3.223680341413" Y="-27.311306985505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.236898863631" Y="-22.625220208855" />
                  <Point X="3.246921583224" Y="-23.199421429793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.26101682751" Y="-24.006937434115" />
                  <Point X="3.294509179804" Y="-25.92571301193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313900361015" Y="-27.036633039494" />
                  <Point X="3.319662086927" Y="-27.366722095916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330965362227" Y="-22.570910896789" />
                  <Point X="3.340680272967" Y="-23.127477760315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356546358594" Y="-24.036445197141" />
                  <Point X="3.371368758208" Y="-24.885619902312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.376775026916" Y="-25.195344829163" />
                  <Point X="3.389231123674" Y="-25.908954134485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.410204705515" Y="-27.110529833397" />
                  <Point X="3.415643832441" Y="-27.422137206327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.425031860823" Y="-22.516601584723" />
                  <Point X="3.434438962709" Y="-23.055534090836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.451755801429" Y="-24.047615116691" />
                  <Point X="3.463622555018" Y="-24.727460974464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.474445599214" Y="-25.347512761181" />
                  <Point X="3.484360163778" Y="-25.915517784629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.506509050015" Y="-27.184426627301" />
                  <Point X="3.511625577956" Y="-27.477552316737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.519098359419" Y="-22.462292272657" />
                  <Point X="3.528197652451" Y="-22.983590421358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.546657252453" Y="-24.041140197158" />
                  <Point X="3.557233565816" Y="-24.647056783961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.570783535294" Y="-25.423334015442" />
                  <Point X="3.579593481225" Y="-25.928055479812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.602813394514" Y="-27.258323421204" />
                  <Point X="3.60760732347" Y="-27.532967427148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613164858014" Y="-22.407982960591" />
                  <Point X="3.621956342193" Y="-22.91164675188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.641453891093" Y="-24.028660580255" />
                  <Point X="3.651327747177" Y="-24.594333416455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.666695876391" Y="-25.474772949457" />
                  <Point X="3.674826798673" Y="-25.940593174995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.699117739014" Y="-27.332220215108" />
                  <Point X="3.703589068985" Y="-27.588382537559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.70723135661" Y="-22.353673648525" />
                  <Point X="3.715715031936" Y="-22.839703082402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.736250527061" Y="-24.016180810183" />
                  <Point X="3.745816656382" Y="-24.564223991978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.762198296731" Y="-25.502727539015" />
                  <Point X="3.770060116121" Y="-25.953130870179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.795422083514" Y="-27.406117009011" />
                  <Point X="3.799570814499" Y="-27.64379764797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.801297855206" Y="-22.299364336459" />
                  <Point X="3.809473721678" Y="-22.767759412924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.831047152137" Y="-24.003700416144" />
                  <Point X="3.840388806689" Y="-24.538883447045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857659243545" Y="-25.528306111853" />
                  <Point X="3.865293433568" Y="-25.965668565362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.891726428014" Y="-27.480013802915" />
                  <Point X="3.895552560013" Y="-27.69921275838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.895364353802" Y="-22.245055024393" />
                  <Point X="3.90323241142" Y="-22.695815743445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.925843777212" Y="-23.991220022106" />
                  <Point X="3.934960956997" Y="-24.513542902113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953120190129" Y="-25.553884671523" />
                  <Point X="3.960526751016" Y="-25.978206260545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.988030772513" Y="-27.553910596819" />
                  <Point X="3.991534307796" Y="-27.754627998751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.991156071174" Y="-22.289583429805" />
                  <Point X="3.996991101162" Y="-22.623872073967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020640402288" Y="-23.978739628067" />
                  <Point X="4.029533107304" Y="-24.48820235718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.04858113659" Y="-25.579463224139" />
                  <Point X="4.055760068464" Y="-25.990743955728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.084335117013" Y="-27.627807390722" />
                  <Point X="4.084449902322" Y="-27.634383436695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.088743246387" Y="-22.436973546087" />
                  <Point X="4.090749790905" Y="-22.551928404489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.115437027364" Y="-23.966259234029" />
                  <Point X="4.124105257611" Y="-24.462861812248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.144042083051" Y="-25.605041776756" />
                  <Point X="4.150993385911" Y="-26.003281650912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.21023365244" Y="-23.953778839991" />
                  <Point X="4.218677407918" Y="-24.437521267316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.239503029512" Y="-25.630620329372" />
                  <Point X="4.246226703359" Y="-26.015819346095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.305030277516" Y="-23.941298445952" />
                  <Point X="4.313249558225" Y="-24.412180722383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.334963975973" Y="-25.656198881989" />
                  <Point X="4.341460020807" Y="-26.028357041278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.399826902592" Y="-23.928818051914" />
                  <Point X="4.407821708532" Y="-24.386840177451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.430424922433" Y="-25.681777434605" />
                  <Point X="4.436693338254" Y="-26.040894736462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.494623527668" Y="-23.916337657876" />
                  <Point X="4.502393858839" Y="-24.361499632518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.525885868894" Y="-25.707355987222" />
                  <Point X="4.531926655702" Y="-26.053432431645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.589420152744" Y="-23.903857263837" />
                  <Point X="4.596966009146" Y="-24.336159087586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.621346815355" Y="-25.732934539838" />
                  <Point X="4.627159973149" Y="-26.065970126828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.68421677782" Y="-23.891376869799" />
                  <Point X="4.691538159453" Y="-24.310818542653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.716807761816" Y="-25.758513092455" />
                  <Point X="4.722393298291" Y="-26.078508262807" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998373046875" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.810814025879" Y="-29.8033046875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318969727" Y="-28.521544921875" />
                  <Point X="0.325648681641" Y="-28.321748046875" />
                  <Point X="0.300591186523" Y="-28.28564453125" />
                  <Point X="0.27433682251" Y="-28.266400390625" />
                  <Point X="0.059752590179" Y="-28.199802734375" />
                  <Point X="0.020977689743" Y="-28.187767578125" />
                  <Point X="-0.008663539886" Y="-28.187765625" />
                  <Point X="-0.223247772217" Y="-28.254365234375" />
                  <Point X="-0.262022674561" Y="-28.2663984375" />
                  <Point X="-0.288278564453" Y="-28.285642578125" />
                  <Point X="-0.426948822021" Y="-28.48544140625" />
                  <Point X="-0.452006347656" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.52053338623" Y="-28.76591015625" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-1.055845947266" Y="-29.94668359375" />
                  <Point X="-1.100256103516" Y="-29.9380625" />
                  <Point X="-1.349228881836" Y="-29.874005859375" />
                  <Point X="-1.349990966797" Y="-29.86125390625" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.360947265625" Y="-29.277037109375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282958984" Y="-29.20262890625" />
                  <Point X="-1.583844848633" Y="-29.02937109375" />
                  <Point X="-1.619543823242" Y="-28.998064453125" />
                  <Point X="-1.649240722656" Y="-28.985763671875" />
                  <Point X="-1.91144934082" Y="-28.968578125" />
                  <Point X="-1.958829833984" Y="-28.96547265625" />
                  <Point X="-1.989877807617" Y="-28.973791015625" />
                  <Point X="-2.208364013672" Y="-29.119779296875" />
                  <Point X="-2.247844238281" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.294152832031" Y="-29.202150390625" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.790757568359" Y="-29.207904296875" />
                  <Point X="-2.855840087891" Y="-29.167607421875" />
                  <Point X="-3.200603027344" Y="-28.902150390625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-3.136538085938" Y="-28.721185546875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.59759375" />
                  <Point X="-2.513979980469" Y="-27.568763671875" />
                  <Point X="-2.528673583984" Y="-27.5540703125" />
                  <Point X="-2.531333251953" Y="-27.551412109375" />
                  <Point X="-2.560160644531" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.789017578125" Y="-27.65740234375" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.110289550781" Y="-27.914677734375" />
                  <Point X="-4.161699707031" Y="-27.84713671875" />
                  <Point X="-4.408875488281" Y="-27.43266015625" />
                  <Point X="-4.43101953125" Y="-27.39552734375" />
                  <Point X="-4.26696484375" Y="-27.26964453125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.139296630859" Y="-26.3708203125" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140326171875" Y="-26.334595703125" />
                  <Point X="-3.161158935547" Y="-26.310640625" />
                  <Point X="-3.183588623047" Y="-26.297439453125" />
                  <Point X="-3.187646728516" Y="-26.29505078125" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-3.46864453125" Y="-26.321369140625" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.907284179688" Y="-26.08991796875" />
                  <Point X="-4.927392578125" Y="-26.011193359375" />
                  <Point X="-4.992788574219" Y="-25.553953125" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.814149902344" Y="-25.465375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541895507813" Y="-25.12142578125" />
                  <Point X="-3.518389892578" Y="-25.105111328125" />
                  <Point X="-3.514146728516" Y="-25.102166015625" />
                  <Point X="-3.494899902344" Y="-25.075912109375" />
                  <Point X="-3.487064697266" Y="-25.050666015625" />
                  <Point X="-3.485648925781" Y="-25.04610546875" />
                  <Point X="-3.485647216797" Y="-25.016462890625" />
                  <Point X="-3.493482421875" Y="-24.991216796875" />
                  <Point X="-3.494898193359" Y="-24.986654296875" />
                  <Point X="-3.514144775391" Y="-24.96039453125" />
                  <Point X="-3.537650390625" Y="-24.94408203125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.784544433594" Y="-24.873068359375" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.930604003906" Y="-24.091158203125" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.786006835938" Y="-23.517794921875" />
                  <Point X="-4.773516113281" Y="-23.471701171875" />
                  <Point X="-4.654089355469" Y="-23.487423828125" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731701171875" Y="-23.6041328125" />
                  <Point X="-3.679676025391" Y="-23.58773046875" />
                  <Point X="-3.670275146484" Y="-23.584765625" />
                  <Point X="-3.651532226562" Y="-23.573943359375" />
                  <Point X="-3.639119140625" Y="-23.556212890625" />
                  <Point X="-3.618243652344" Y="-23.50581640625" />
                  <Point X="-3.614471679688" Y="-23.496708984375" />
                  <Point X="-3.610714111328" Y="-23.475392578125" />
                  <Point X="-3.616317138672" Y="-23.454486328125" />
                  <Point X="-3.641505371094" Y="-23.406099609375" />
                  <Point X="-3.646056884766" Y="-23.397357421875" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.790222167969" Y="-23.28083203125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.210368652344" Y="-22.299263671875" />
                  <Point X="-4.160016113281" Y="-22.21299609375" />
                  <Point X="-3.811306640625" Y="-21.764779296875" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.712417480469" Y="-21.753630859375" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138511230469" Y="-22.07956640625" />
                  <Point X="-3.0660546875" Y="-22.08590625" />
                  <Point X="-3.052961914062" Y="-22.08705078125" />
                  <Point X="-3.031505615234" Y="-22.084224609375" />
                  <Point X="-3.013253173828" Y="-22.072595703125" />
                  <Point X="-2.961822998047" Y="-22.021166015625" />
                  <Point X="-2.952529541016" Y="-22.011873046875" />
                  <Point X="-2.940899658203" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.944413330078" Y="-21.899703125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.009786865234" Y="-21.765994140625" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.840575439453" Y="-20.892908203125" />
                  <Point X="-2.752872314453" Y="-20.825666015625" />
                  <Point X="-2.203671630859" Y="-20.52054296875" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.137196777344" Y="-20.49169921875" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246948242" Y="-20.726337890625" />
                  <Point X="-1.870603149414" Y="-20.7683203125" />
                  <Point X="-1.856030761719" Y="-20.77590625" />
                  <Point X="-1.835123901367" Y="-20.781509765625" />
                  <Point X="-1.813808349609" Y="-20.77775" />
                  <Point X="-1.729812255859" Y="-20.74295703125" />
                  <Point X="-1.714634399414" Y="-20.736669921875" />
                  <Point X="-1.696905151367" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.658744384766" Y="-20.618802734375" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.658479858398" Y="-20.53173046875" />
                  <Point X="-1.689137695313" Y="-20.298859375" />
                  <Point X="-1.081627319336" Y="-20.12853515625" />
                  <Point X="-0.96808404541" Y="-20.096703125" />
                  <Point X="-0.302290283203" Y="-20.01878125" />
                  <Point X="-0.224200027466" Y="-20.009640625" />
                  <Point X="-0.205810073853" Y="-20.0782734375" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282117844" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594036102" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.084027008057" Y="-20.57872265625" />
                  <Point X="0.236648406982" Y="-20.009130859375" />
                  <Point X="0.76107232666" Y="-20.064052734375" />
                  <Point X="0.860210083008" Y="-20.074435546875" />
                  <Point X="1.411041259766" Y="-20.207423828125" />
                  <Point X="1.50845703125" Y="-20.230943359375" />
                  <Point X="1.867744384766" Y="-20.361259765625" />
                  <Point X="1.931044067383" Y="-20.38421875" />
                  <Point X="2.277724609375" Y="-20.546349609375" />
                  <Point X="2.338686279297" Y="-20.574859375" />
                  <Point X="2.673613525391" Y="-20.76998828125" />
                  <Point X="2.732533203125" Y="-20.804314453125" />
                  <Point X="3.048394287109" Y="-21.0289375" />
                  <Point X="3.068740722656" Y="-21.04340625" />
                  <Point X="2.959614990234" Y="-21.23241796875" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852294922" Y="-22.508484375" />
                  <Point X="2.206668457031" Y="-22.576484375" />
                  <Point X="2.202044433594" Y="-22.607673828125" />
                  <Point X="2.209134765625" Y="-22.66647265625" />
                  <Point X="2.210415771484" Y="-22.67709765625" />
                  <Point X="2.218684082031" Y="-22.69919140625" />
                  <Point X="2.255067382812" Y="-22.7528125" />
                  <Point X="2.274940185547" Y="-22.775796875" />
                  <Point X="2.328559814453" Y="-22.8121796875" />
                  <Point X="2.338243164062" Y="-22.818751953125" />
                  <Point X="2.360336669922" Y="-22.827021484375" />
                  <Point X="2.419136474609" Y="-22.834111328125" />
                  <Point X="2.448664306641" Y="-22.8340546875" />
                  <Point X="2.516663330078" Y="-22.81587109375" />
                  <Point X="2.516668212891" Y="-22.815869140625" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.769433105469" Y="-22.675716796875" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.167647949219" Y="-22.209556640625" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.378685058594" Y="-22.5491171875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.249415527344" Y="-22.669669921875" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279370849609" Y="-23.41616796875" />
                  <Point X="3.230431884766" Y="-23.48001171875" />
                  <Point X="3.213119140625" Y="-23.508501953125" />
                  <Point X="3.194889160156" Y="-23.5736875" />
                  <Point X="3.191595214844" Y="-23.585466796875" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.205744384766" Y="-23.6815625" />
                  <Point X="3.215645996094" Y="-23.712044921875" />
                  <Point X="3.256348632812" Y="-23.77391015625" />
                  <Point X="3.263703613281" Y="-23.78508984375" />
                  <Point X="3.280947265625" Y="-23.8011796875" />
                  <Point X="3.339930908203" Y="-23.8343828125" />
                  <Point X="3.36856640625" Y="-23.846380859375" />
                  <Point X="3.44831640625" Y="-23.856919921875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.692767822266" Y="-23.830265625" />
                  <Point X="4.848975097656" Y="-23.678046875" />
                  <Point X="4.924180175781" Y="-23.98696875" />
                  <Point X="4.939189453125" Y="-24.048619140625" />
                  <Point X="4.994680175781" Y="-24.40503125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.842264160156" Y="-24.467134765625" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.650735351562" Y="-24.81246875" />
                  <Point X="3.622264892578" Y="-24.833072265625" />
                  <Point X="3.575253662109" Y="-24.8929765625" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.541314697266" Y="-25.00708984375" />
                  <Point X="3.538483154297" Y="-25.0406875" />
                  <Point X="3.554153564453" Y="-25.12251171875" />
                  <Point X="3.556985351562" Y="-25.137296875" />
                  <Point X="3.566759521484" Y="-25.158759765625" />
                  <Point X="3.613770507812" Y="-25.2186640625" />
                  <Point X="3.636576904297" Y="-25.241908203125" />
                  <Point X="3.714928955078" Y="-25.287197265625" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="3.940133544922" Y="-25.353701171875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.956811523438" Y="-25.910818359375" />
                  <Point X="4.948431640625" Y="-25.96640234375" />
                  <Point X="4.877337890625" Y="-26.2779453125" />
                  <Point X="4.874546386719" Y="-26.290177734375" />
                  <Point X="4.688944824219" Y="-26.265744140625" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394837158203" Y="-26.098341796875" />
                  <Point X="3.241060058594" Y="-26.131765625" />
                  <Point X="3.213272949219" Y="-26.1378046875" />
                  <Point X="3.185445800781" Y="-26.154697265625" />
                  <Point X="3.092497314453" Y="-26.266486328125" />
                  <Point X="3.075701660156" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.051036132812" Y="-26.458841796875" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056361083984" Y="-26.516625" />
                  <Point X="3.141463623047" Y="-26.64899609375" />
                  <Point X="3.156841308594" Y="-26.6729140625" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.353102294922" Y="-26.82722265625" />
                  <Point X="4.339073730469" Y="-27.583783203125" />
                  <Point X="4.227752441406" Y="-27.76391796875" />
                  <Point X="4.204123046875" Y="-27.80215625" />
                  <Point X="4.057105224609" Y="-28.011046875" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.890761962891" Y="-27.915841796875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.554321533203" Y="-27.22026171875" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489078125" Y="-27.21924609375" />
                  <Point X="2.337033935547" Y="-27.299265625" />
                  <Point X="2.309560058594" Y="-27.313724609375" />
                  <Point X="2.288600341797" Y="-27.33468359375" />
                  <Point X="2.208580566406" Y="-27.486728515625" />
                  <Point X="2.194121337891" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.222216064453" Y="-27.72939453125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.352742431641" Y="-27.984087890625" />
                  <Point X="2.986674072266" Y="-29.082087890625" />
                  <Point X="2.863329101562" Y="-29.17019140625" />
                  <Point X="2.835318603516" Y="-29.19019921875" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="2.551048095703" Y="-29.123119140625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670548339844" Y="-27.980466796875" />
                  <Point X="1.490041870117" Y="-27.86441796875" />
                  <Point X="1.457425048828" Y="-27.84344921875" />
                  <Point X="1.425806762695" Y="-27.83571875" />
                  <Point X="1.228392089844" Y="-27.853884765625" />
                  <Point X="1.192719848633" Y="-27.857166015625" />
                  <Point X="1.165331665039" Y="-27.868509765625" />
                  <Point X="1.012893371582" Y="-27.9952578125" />
                  <Point X="0.985348022461" Y="-28.018162109375" />
                  <Point X="0.96845690918" Y="-28.04598828125" />
                  <Point X="0.922878601074" Y="-28.25568359375" />
                  <Point X="0.914642700195" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.947554260254" Y="-28.566125" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.020862487793" Y="-29.957435546875" />
                  <Point X="0.99436920166" Y="-29.9632421875" />
                  <Point X="0.860200500488" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#202" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.163669777233" Y="4.965948429041" Z="2.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.15" />
                  <Point X="-0.314258882436" Y="5.062160073588" Z="2.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.15" />
                  <Point X="-1.101280782759" Y="4.950889373162" Z="2.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.15" />
                  <Point X="-1.714207942376" Y="4.493024324154" Z="2.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.15" />
                  <Point X="-1.712586016317" Y="4.427512530203" Z="2.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.15" />
                  <Point X="-1.755103047005" Y="4.334517154784" Z="2.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.15" />
                  <Point X="-1.853671243615" Y="4.30731043692" Z="2.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.15" />
                  <Point X="-2.103684979599" Y="4.570018414861" Z="2.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.15" />
                  <Point X="-2.234110888084" Y="4.554444888864" Z="2.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.15" />
                  <Point X="-2.877153766216" Y="4.178052865458" Z="2.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.15" />
                  <Point X="-3.059244135045" Y="3.240285921222" Z="2.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.15" />
                  <Point X="-3.000379207578" Y="3.127220155028" Z="2.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.15" />
                  <Point X="-3.003333343452" Y="3.045470224434" Z="2.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.15" />
                  <Point X="-3.067856310579" Y="2.995185491595" Z="2.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.15" />
                  <Point X="-3.693573427075" Y="3.320949813701" Z="2.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.15" />
                  <Point X="-3.856926107548" Y="3.297203623759" Z="2.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.15" />
                  <Point X="-4.259706916488" Y="2.757222384293" Z="2.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.15" />
                  <Point X="-3.826816192525" Y="1.710781445037" Z="2.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.15" />
                  <Point X="-3.692010804545" Y="1.602090797978" Z="2.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.15" />
                  <Point X="-3.670594564135" Y="1.544597651394" Z="2.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.15" />
                  <Point X="-3.700870770064" Y="1.491236032855" Z="2.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.15" />
                  <Point X="-4.653719074286" Y="1.59342816896" Z="2.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.15" />
                  <Point X="-4.84042189999" Y="1.526563860792" Z="2.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.15" />
                  <Point X="-4.986221491439" Y="0.947441340002" Z="2.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.15" />
                  <Point X="-3.803641220994" Y="0.10991506566" Z="2.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.15" />
                  <Point X="-3.572313344088" Y="0.046121099738" Z="2.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.15" />
                  <Point X="-3.54739203789" Y="0.025245197075" Z="2.15" />
                  <Point X="-3.539556741714" Y="0" Z="2.15" />
                  <Point X="-3.540972563245" Y="-0.004561753986" Z="2.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.15" />
                  <Point X="-3.553055251035" Y="-0.032754883395" Z="2.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.15" />
                  <Point X="-4.833246567779" Y="-0.385796991096" Z="2.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.15" />
                  <Point X="-5.048440895264" Y="-0.529749786357" Z="2.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.15" />
                  <Point X="-4.961884811321" Y="-1.071011651928" Z="2.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.15" />
                  <Point X="-3.468274258788" Y="-1.339659962431" Z="2.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.15" />
                  <Point X="-3.215105906256" Y="-1.309248716557" Z="2.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.15" />
                  <Point X="-3.193855427279" Y="-1.327002438382" Z="2.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.15" />
                  <Point X="-4.303558679517" Y="-2.198695292367" Z="2.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.15" />
                  <Point X="-4.457975395942" Y="-2.426988431973" Z="2.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.15" />
                  <Point X="-4.156231102297" Y="-2.913680794241" Z="2.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.15" />
                  <Point X="-2.770173428233" Y="-2.66942176647" Z="2.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.15" />
                  <Point X="-2.570184422656" Y="-2.558146046475" Z="2.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.15" />
                  <Point X="-3.185995246699" Y="-3.664904954259" Z="2.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.15" />
                  <Point X="-3.237262384408" Y="-3.910487813612" Z="2.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.15" />
                  <Point X="-2.823234752924" Y="-4.219135963338" Z="2.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.15" />
                  <Point X="-2.260641021423" Y="-4.201307544716" Z="2.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.15" />
                  <Point X="-2.186742267362" Y="-4.130072441285" Z="2.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.15" />
                  <Point X="-1.920875453658" Y="-3.987189855991" Z="2.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.15" />
                  <Point X="-1.622967837146" Y="-4.035683170517" Z="2.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.15" />
                  <Point X="-1.416142659744" Y="-4.255510314051" Z="2.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.15" />
                  <Point X="-1.405719224011" Y="-4.823447926743" Z="2.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.15" />
                  <Point X="-1.367844575027" Y="-4.891146833279" Z="2.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.15" />
                  <Point X="-1.071529596547" Y="-4.964487536139" Z="2.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.15" />
                  <Point X="-0.478393061928" Y="-3.747571253982" Z="2.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.15" />
                  <Point X="-0.392029353374" Y="-3.482670104242" Z="2.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.15" />
                  <Point X="-0.214584171196" Y="-3.270838376283" Z="2.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.15" />
                  <Point X="0.038774908164" Y="-3.216273669005" Z="2.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.15" />
                  <Point X="0.278416506058" Y="-3.318975651526" Z="2.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.15" />
                  <Point X="0.756361798474" Y="-4.784964775834" Z="2.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.15" />
                  <Point X="0.845268226093" Y="-5.008749011715" Z="2.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.15" />
                  <Point X="1.025411460958" Y="-4.974995027378" Z="2.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.15" />
                  <Point X="0.990970496017" Y="-3.528318710698" Z="2.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.15" />
                  <Point X="0.965581691113" Y="-3.235022060063" Z="2.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.15" />
                  <Point X="1.038705319811" Y="-3.002422996756" Z="2.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.15" />
                  <Point X="1.226816108384" Y="-2.872392837073" Z="2.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.15" />
                  <Point X="1.456847569832" Y="-2.875196379468" Z="2.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.15" />
                  <Point X="2.505224313087" Y="-4.122275785459" Z="2.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.15" />
                  <Point X="2.691924840023" Y="-4.307311056403" Z="2.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.15" />
                  <Point X="2.886235342472" Y="-4.17959729068" Z="2.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.15" />
                  <Point X="2.3898876448" Y="-2.92780766051" Z="2.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.15" />
                  <Point X="2.265264262037" Y="-2.689227429196" Z="2.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.15" />
                  <Point X="2.246670503243" Y="-2.478734157651" Z="2.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.15" />
                  <Point X="2.354164220721" Y="-2.312230806724" Z="2.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.15" />
                  <Point X="2.539279374316" Y="-2.238183747454" Z="2.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.15" />
                  <Point X="3.859605936597" Y="-2.927861855475" Z="2.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.15" />
                  <Point X="4.091837447857" Y="-3.008543650596" Z="2.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.15" />
                  <Point X="4.264130559702" Y="-2.758923394349" Z="2.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.15" />
                  <Point X="3.377384313066" Y="-1.75627344523" Z="2.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.15" />
                  <Point X="3.177364985657" Y="-1.590673811254" Z="2.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.15" />
                  <Point X="3.094669716887" Y="-1.432142766789" Z="2.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.15" />
                  <Point X="3.124787051296" Y="-1.267172345715" Z="2.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.15" />
                  <Point X="3.245522732311" Y="-1.149344519326" Z="2.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.15" />
                  <Point X="4.676262312617" Y="-1.284035707983" Z="2.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.15" />
                  <Point X="4.919928438168" Y="-1.257789143722" Z="2.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.15" />
                  <Point X="5.000097077788" Y="-0.886991527463" Z="2.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.15" />
                  <Point X="3.946918535001" Y="-0.274123369331" Z="2.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.15" />
                  <Point X="3.733794666799" Y="-0.212627040107" Z="2.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.15" />
                  <Point X="3.646947894974" Y="-0.156513810802" Z="2.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.15" />
                  <Point X="3.597105117137" Y="-0.081824937249" Z="2.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.15" />
                  <Point X="3.584266333288" Y="0.014785593935" Z="2.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.15" />
                  <Point X="3.608431543426" Y="0.107434927773" Z="2.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.15" />
                  <Point X="3.669600747553" Y="0.175521804135" Z="2.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.15" />
                  <Point X="4.849049282305" Y="0.515848563438" Z="2.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.15" />
                  <Point X="5.037929222781" Y="0.633941352173" Z="2.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.15" />
                  <Point X="4.966605402464" Y="1.056140654983" Z="2.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.15" />
                  <Point X="3.680085390874" Y="1.250587941347" Z="2.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.15" />
                  <Point X="3.448710735884" Y="1.223928629826" Z="2.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.15" />
                  <Point X="3.358302367619" Y="1.240468251649" Z="2.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.15" />
                  <Point X="3.291963665304" Y="1.284850135515" Z="2.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.15" />
                  <Point X="3.248556879639" Y="1.359821929941" Z="2.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.15" />
                  <Point X="3.236886129982" Y="1.444128105274" Z="2.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.15" />
                  <Point X="3.263959160006" Y="1.520850320099" Z="2.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.15" />
                  <Point X="4.273697465438" Y="2.321942313252" Z="2.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.15" />
                  <Point X="4.415306254986" Y="2.508050962422" Z="2.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.15" />
                  <Point X="4.202077570637" Y="2.850927106024" Z="2.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.15" />
                  <Point X="2.738277167922" Y="2.398865127514" Z="2.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.15" />
                  <Point X="2.497590645751" Y="2.263712990604" Z="2.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.15" />
                  <Point X="2.418966645278" Y="2.246810354269" Z="2.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.15" />
                  <Point X="2.350477813669" Y="2.260474892056" Z="2.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.15" />
                  <Point X="2.290283708179" Y="2.306547046713" Z="2.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.15" />
                  <Point X="2.25261934849" Y="2.370791798555" Z="2.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.15" />
                  <Point X="2.248814952054" Y="2.441878930131" Z="2.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.15" />
                  <Point X="2.996760225345" Y="3.773861914033" Z="2.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.15" />
                  <Point X="3.071215645899" Y="4.043088649953" Z="2.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.15" />
                  <Point X="2.692627146094" Y="4.304495408114" Z="2.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.15" />
                  <Point X="2.292749977774" Y="4.530222354808" Z="2.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.15" />
                  <Point X="1.878636809431" Y="4.717025488674" Z="2.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.15" />
                  <Point X="1.416619883669" Y="4.872460511231" Z="2.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.15" />
                  <Point X="0.760124627852" Y="5.01695554937" Z="2.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.15" />
                  <Point X="0.02957478432" Y="4.46549907691" Z="2.15" />
                  <Point X="0" Y="4.355124473572" Z="2.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>