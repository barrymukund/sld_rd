<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#171" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2079" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.995283203125" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.776409606934" Y="-29.307853515625" />
                  <Point X="0.563302001953" Y="-28.5125234375" />
                  <Point X="0.557721679688" Y="-28.497142578125" />
                  <Point X="0.54236328125" Y="-28.467375" />
                  <Point X="0.455260620117" Y="-28.341876953125" />
                  <Point X="0.378635437012" Y="-28.231474609375" />
                  <Point X="0.356752288818" Y="-28.209021484375" />
                  <Point X="0.330497253418" Y="-28.18977734375" />
                  <Point X="0.3024949646" Y="-28.17566796875" />
                  <Point X="0.167708862305" Y="-28.1338359375" />
                  <Point X="0.049135822296" Y="-28.09703515625" />
                  <Point X="0.020976730347" Y="-28.092765625" />
                  <Point X="-0.008664747238" Y="-28.092765625" />
                  <Point X="-0.036823841095" Y="-28.09703515625" />
                  <Point X="-0.171609924316" Y="-28.1388671875" />
                  <Point X="-0.290182983398" Y="-28.17566796875" />
                  <Point X="-0.318184967041" Y="-28.18977734375" />
                  <Point X="-0.344440307617" Y="-28.209021484375" />
                  <Point X="-0.366323608398" Y="-28.231474609375" />
                  <Point X="-0.453425964355" Y="-28.35697265625" />
                  <Point X="-0.530051269531" Y="-28.467375" />
                  <Point X="-0.538189208984" Y="-28.481572265625" />
                  <Point X="-0.55099017334" Y="-28.5125234375" />
                  <Point X="-0.738463745117" Y="-29.212185546875" />
                  <Point X="-0.916584655762" Y="-29.87694140625" />
                  <Point X="-0.943340393066" Y="-29.871748046875" />
                  <Point X="-1.079355224609" Y="-29.845345703125" />
                  <Point X="-1.23242578125" Y="-29.80596484375" />
                  <Point X="-1.24641809082" Y="-29.802365234375" />
                  <Point X="-1.240616088867" Y="-29.758294921875" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.248709106445" Y="-29.35434375" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287938354492" Y="-29.182966796875" />
                  <Point X="-1.304010620117" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131205078125" />
                  <Point X="-1.44773840332" Y="-29.022376953125" />
                  <Point X="-1.556905395508" Y="-28.926640625" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.64302746582" Y="-28.890966796875" />
                  <Point X="-1.807727905273" Y="-28.880171875" />
                  <Point X="-1.952616699219" Y="-28.87067578125" />
                  <Point X="-1.983414916992" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.179894775391" Y="-28.9865" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312789306641" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.440354736328" Y="-29.236630859375" />
                  <Point X="-2.480147216797" Y="-29.28848828125" />
                  <Point X="-2.602365722656" Y="-29.212814453125" />
                  <Point X="-2.801726318359" Y="-29.089376953125" />
                  <Point X="-3.013684082031" Y="-28.92617578125" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.819797851562" Y="-28.362576171875" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412859130859" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616126953125" />
                  <Point X="-2.405575195312" Y="-27.5851953125" />
                  <Point X="-2.414559326172" Y="-27.555578125" />
                  <Point X="-2.428776367188" Y="-27.526748046875" />
                  <Point X="-2.446806396484" Y="-27.5015859375" />
                  <Point X="-2.464155029297" Y="-27.48423828125" />
                  <Point X="-2.489311035156" Y="-27.466212890625" />
                  <Point X="-2.518139892578" Y="-27.45199609375" />
                  <Point X="-2.547758056641" Y="-27.44301171875" />
                  <Point X="-2.578692138672" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.242627441406" Y="-27.80959765625" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.925368164062" Y="-28.0007734375" />
                  <Point X="-4.082862792969" Y="-27.793857421875" />
                  <Point X="-4.234828125" Y="-27.53903515625" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.798889892578" Y="-27.03022265625" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.359658203125" />
                  <Point X="-3.045556152344" Y="-26.32798828125" />
                  <Point X="-3.052556152344" Y="-26.29824609375" />
                  <Point X="-3.068636230469" Y="-26.272263671875" />
                  <Point X="-3.089467041016" Y="-26.248306640625" />
                  <Point X="-3.112971191406" Y="-26.228767578125" />
                  <Point X="-3.139460205078" Y="-26.213177734375" />
                  <Point X="-3.168729003906" Y="-26.201953125" />
                  <Point X="-3.200612548828" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-3.993719970703" Y="-26.29467578125" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.772480957031" Y="-26.233802734375" />
                  <Point X="-4.834077636719" Y="-25.99265625" />
                  <Point X="-4.874284179688" Y="-25.711533203125" />
                  <Point X="-4.892423828125" Y="-25.584701171875" />
                  <Point X="-4.322233886719" Y="-25.43191796875" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.510053955078" Y="-25.210970703125" />
                  <Point X="-3.479723876953" Y="-25.19350390625" />
                  <Point X="-3.466561523438" Y="-25.184365234375" />
                  <Point X="-3.441752929688" Y="-25.16383203125" />
                  <Point X="-3.425646728516" Y="-25.14673046875" />
                  <Point X="-3.414229248047" Y="-25.126201171875" />
                  <Point X="-3.401881347656" Y="-25.09560546875" />
                  <Point X="-3.397366210938" Y="-25.081220703125" />
                  <Point X="-3.390789794922" Y="-25.052451171875" />
                  <Point X="-3.388401123047" Y="-25.031052734375" />
                  <Point X="-3.390892578125" Y="-25.009666015625" />
                  <Point X="-3.398060791016" Y="-24.97898828125" />
                  <Point X="-3.402662841797" Y="-24.96458203125" />
                  <Point X="-3.414418701172" Y="-24.935892578125" />
                  <Point X="-3.425945800781" Y="-24.915423828125" />
                  <Point X="-3.442143798828" Y="-24.898408203125" />
                  <Point X="-3.468728759766" Y="-24.876642578125" />
                  <Point X="-3.481956054688" Y="-24.8675625" />
                  <Point X="-3.510510009766" Y="-24.851328125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.227284667969" Y="-24.656083984375" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.864185546875" Y="-24.291296875" />
                  <Point X="-4.82448828125" Y="-24.023029296875" />
                  <Point X="-4.743552734375" Y="-23.7243515625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.331497558594" Y="-23.62571484375" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985107422" Y="-23.700658203125" />
                  <Point X="-3.723423095703" Y="-23.698771484375" />
                  <Point X="-3.703138916016" Y="-23.694736328125" />
                  <Point X="-3.670460449219" Y="-23.68443359375" />
                  <Point X="-3.641712890625" Y="-23.675369140625" />
                  <Point X="-3.622782470703" Y="-23.667041015625" />
                  <Point X="-3.604038818359" Y="-23.656220703125" />
                  <Point X="-3.587354248047" Y="-23.64398828125" />
                  <Point X="-3.573713623047" Y="-23.62843359375" />
                  <Point X="-3.561299316406" Y="-23.610703125" />
                  <Point X="-3.551351318359" Y="-23.5925703125" />
                  <Point X="-3.538239013672" Y="-23.5609140625" />
                  <Point X="-3.526703857422" Y="-23.53306640625" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532050292969" Y="-23.41062109375" />
                  <Point X="-3.547871826172" Y="-23.380228515625" />
                  <Point X="-3.561790039062" Y="-23.3534921875" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.000450195312" Y="-22.999771484375" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.23540234375" Y="-22.53060546875" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.866760253906" Y="-21.99076953125" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.554174804688" Y="-21.954689453125" />
                  <Point X="-3.206657226562" Y="-22.155328125" />
                  <Point X="-3.187727783203" Y="-22.163658203125" />
                  <Point X="-3.167084472656" Y="-22.17016796875" />
                  <Point X="-3.146794677734" Y="-22.174205078125" />
                  <Point X="-3.101282714844" Y="-22.1781875" />
                  <Point X="-3.061245361328" Y="-22.181689453125" />
                  <Point X="-3.040559082031" Y="-22.18123828125" />
                  <Point X="-3.019101318359" Y="-22.178412109375" />
                  <Point X="-2.999012207031" Y="-22.173494140625" />
                  <Point X="-2.980462890625" Y="-22.16434765625" />
                  <Point X="-2.962209228516" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.913773193359" Y="-22.107466796875" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.847417724609" Y="-21.918369140625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.046300048828" Y="-21.512751953125" />
                  <Point X="-3.183333496094" Y="-21.275404296875" />
                  <Point X="-2.969273681641" Y="-21.11128515625" />
                  <Point X="-2.700629638672" Y="-20.905318359375" />
                  <Point X="-2.362965087891" Y="-20.717720703125" />
                  <Point X="-2.167035888672" Y="-20.608865234375" />
                  <Point X="-2.149581298828" Y="-20.63161328125" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028890869141" Y="-20.785201171875" />
                  <Point X="-2.012310791016" Y="-20.79911328125" />
                  <Point X="-1.995115356445" Y="-20.810603515625" />
                  <Point X="-1.94446081543" Y="-20.836974609375" />
                  <Point X="-1.899899291992" Y="-20.860171875" />
                  <Point X="-1.880625488281" Y="-20.86766796875" />
                  <Point X="-1.859719360352" Y="-20.873271484375" />
                  <Point X="-1.839268920898" Y="-20.876419921875" />
                  <Point X="-1.818621948242" Y="-20.87506640625" />
                  <Point X="-1.797307006836" Y="-20.871306640625" />
                  <Point X="-1.777452514648" Y="-20.865517578125" />
                  <Point X="-1.724692260742" Y="-20.843662109375" />
                  <Point X="-1.678278564453" Y="-20.8244375" />
                  <Point X="-1.660147460938" Y="-20.814490234375" />
                  <Point X="-1.642417480469" Y="-20.802076171875" />
                  <Point X="-1.626864990234" Y="-20.7884375" />
                  <Point X="-1.614633300781" Y="-20.771755859375" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.578307739258" Y="-20.67961328125" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.557730224609" Y="-20.569173828125" />
                  <Point X="-1.57779699707" Y="-20.416751953125" />
                  <Point X="-1.584201660156" Y="-20.368103515625" />
                  <Point X="-1.297416992188" Y="-20.28769921875" />
                  <Point X="-0.949623657227" Y="-20.19019140625" />
                  <Point X="-0.540293029785" Y="-20.142283203125" />
                  <Point X="-0.294711242676" Y="-20.113541015625" />
                  <Point X="-0.236708724976" Y="-20.330009765625" />
                  <Point X="-0.133903366089" Y="-20.713685546875" />
                  <Point X="-0.121129745483" Y="-20.741876953125" />
                  <Point X="-0.103271606445" Y="-20.768603515625" />
                  <Point X="-0.082114013672" Y="-20.791193359375" />
                  <Point X="-0.054818138123" Y="-20.805783203125" />
                  <Point X="-0.024380064011" Y="-20.816115234375" />
                  <Point X="0.006155910969" Y="-20.82115625" />
                  <Point X="0.036691894531" Y="-20.816115234375" />
                  <Point X="0.067129966736" Y="-20.805783203125" />
                  <Point X="0.094425956726" Y="-20.791193359375" />
                  <Point X="0.115583587646" Y="-20.768603515625" />
                  <Point X="0.133441726685" Y="-20.741876953125" />
                  <Point X="0.146215194702" Y="-20.713685546875" />
                  <Point X="0.236654342651" Y="-20.376162109375" />
                  <Point X="0.307419555664" Y="-20.1120625" />
                  <Point X="0.540370239258" Y="-20.136458984375" />
                  <Point X="0.844045166016" Y="-20.16826171875" />
                  <Point X="1.182705322266" Y="-20.250025390625" />
                  <Point X="1.48102331543" Y="-20.322048828125" />
                  <Point X="1.700771484375" Y="-20.401751953125" />
                  <Point X="1.894645385742" Y="-20.472072265625" />
                  <Point X="2.107800537109" Y="-20.5717578125" />
                  <Point X="2.294559326172" Y="-20.659099609375" />
                  <Point X="2.50051953125" Y="-20.779091796875" />
                  <Point X="2.680977050781" Y="-20.8842265625" />
                  <Point X="2.875187255859" Y="-21.022337890625" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.605963623047" Y="-21.6549609375" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.121654785156" Y="-22.526658203125" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.112181640625" Y="-22.655982421875" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.140071044922" Y="-22.752529296875" />
                  <Point X="2.162924316406" Y="-22.786208984375" />
                  <Point X="2.183028808594" Y="-22.815837890625" />
                  <Point X="2.19446484375" Y="-22.829671875" />
                  <Point X="2.221597167969" Y="-22.85440625" />
                  <Point X="2.255277099609" Y="-22.877259765625" />
                  <Point X="2.284905761719" Y="-22.897365234375" />
                  <Point X="2.304947265625" Y="-22.9077265625" />
                  <Point X="2.327036865234" Y="-22.915994140625" />
                  <Point X="2.348965087891" Y="-22.921337890625" />
                  <Point X="2.385898925781" Y="-22.925791015625" />
                  <Point X="2.418389892578" Y="-22.929708984375" />
                  <Point X="2.436468505859" Y="-22.93015625" />
                  <Point X="2.473207519531" Y="-22.925830078125" />
                  <Point X="2.515919677734" Y="-22.914408203125" />
                  <Point X="2.553493896484" Y="-22.904359375" />
                  <Point X="2.565290771484" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.88985546875" />
                  <Point X="3.286975097656" Y="-22.486611328125" />
                  <Point X="3.967326904297" Y="-22.093810546875" />
                  <Point X="4.016229248047" Y="-22.1617734375" />
                  <Point X="4.1232734375" Y="-22.310541015625" />
                  <Point X="4.231541015625" Y="-22.489453125" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.834287353516" Y="-22.868462890625" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221422119141" Y="-23.33976171875" />
                  <Point X="3.203974365234" Y="-23.358373046875" />
                  <Point X="3.173234375" Y="-23.398474609375" />
                  <Point X="3.146192138672" Y="-23.43375390625" />
                  <Point X="3.13660546875" Y="-23.44908984375" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.110179199219" Y="-23.523859375" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739257812" Y="-23.628232421875" />
                  <Point X="3.107138916016" Y="-23.6737890625" />
                  <Point X="3.115408203125" Y="-23.713865234375" />
                  <Point X="3.120680419922" Y="-23.7310234375" />
                  <Point X="3.136282714844" Y="-23.764259765625" />
                  <Point X="3.161849121094" Y="-23.803119140625" />
                  <Point X="3.184340332031" Y="-23.8373046875" />
                  <Point X="3.198891601562" Y="-23.854546875" />
                  <Point X="3.216134521484" Y="-23.87063671875" />
                  <Point X="3.234346435547" Y="-23.88396484375" />
                  <Point X="3.271395751953" Y="-23.9048203125" />
                  <Point X="3.303988525391" Y="-23.92316796875" />
                  <Point X="3.320520751953" Y="-23.930498046875" />
                  <Point X="3.356120605469" Y="-23.9405625" />
                  <Point X="3.406213623047" Y="-23.947181640625" />
                  <Point X="3.45028125" Y="-23.953005859375" />
                  <Point X="3.462697753906" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.15167578125" Y="-23.86566796875" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.799961914062" Y="-23.87834375" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.8800546875" Y="-24.286330078125" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.408208007812" Y="-24.485087890625" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704785644531" Y="-24.674416015625" />
                  <Point X="3.681546875" Y="-24.68493359375" />
                  <Point X="3.63233203125" Y="-24.713380859375" />
                  <Point X="3.589036865234" Y="-24.738404296875" />
                  <Point X="3.574312255859" Y="-24.74890234375" />
                  <Point X="3.547530517578" Y="-24.774423828125" />
                  <Point X="3.518001708984" Y="-24.81205078125" />
                  <Point X="3.492024658203" Y="-24.84515234375" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.47052734375" Y="-24.88589453125" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.453837646484" Y="-24.95879296875" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.455021728516" Y="-25.109951171875" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.47052734375" Y="-25.1766640625" />
                  <Point X="3.48030078125" Y="-25.198126953125" />
                  <Point X="3.492024658203" Y="-25.217408203125" />
                  <Point X="3.521553466797" Y="-25.25503515625" />
                  <Point X="3.547530517578" Y="-25.28813671875" />
                  <Point X="3.559994873047" Y="-25.301232421875" />
                  <Point X="3.589035644531" Y="-25.32415625" />
                  <Point X="3.638250732422" Y="-25.352603515625" />
                  <Point X="3.681545654297" Y="-25.37762890625" />
                  <Point X="3.692708496094" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.325014160156" Y="-25.555181640625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.880692382812" Y="-25.77846484375" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.811311035156" Y="-26.14027734375" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.226476074219" Y="-26.1090390625" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374658935547" Y="-26.005509765625" />
                  <Point X="3.278067382812" Y="-26.02650390625" />
                  <Point X="3.193094726563" Y="-26.04497265625" />
                  <Point X="3.163974121094" Y="-26.05659765625" />
                  <Point X="3.136147216797" Y="-26.073490234375" />
                  <Point X="3.112397460938" Y="-26.093958984375" />
                  <Point X="3.054013916016" Y="-26.16417578125" />
                  <Point X="3.002653320312" Y="-26.225947265625" />
                  <Point X="2.987932128906" Y="-26.250330078125" />
                  <Point X="2.976589111328" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.961389892578" Y="-26.39630078125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450927734" Y="-26.567998046875" />
                  <Point X="3.029906005859" Y="-26.651142578125" />
                  <Point X="3.076931152344" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.675259033203" Y="-27.19416796875" />
                  <Point X="4.213122558594" Y="-27.606884765625" />
                  <Point X="4.197171875" Y="-27.6326953125" />
                  <Point X="4.124814453125" Y="-27.74978125" />
                  <Point X="4.034411376953" Y="-27.87823046875" />
                  <Point X="4.028980957031" Y="-27.8859453125" />
                  <Point X="3.515327636719" Y="-27.589388671875" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754224609375" Y="-27.159826171875" />
                  <Point X="2.639265380859" Y="-27.139064453125" />
                  <Point X="2.538134277344" Y="-27.12080078125" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.349330810547" Y="-27.185439453125" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.15426953125" Y="-27.38594140625" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.116437011719" Y="-27.678216796875" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.514651123047" Y="-28.4545234375" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.781861083984" Y="-29.111634765625" />
                  <Point X="2.701763183594" Y="-29.163482421875" />
                  <Point X="2.303678710938" Y="-28.6446875" />
                  <Point X="1.758546264648" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721923583984" Y="-27.900556640625" />
                  <Point X="1.60854284668" Y="-27.8276640625" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986572266" Y="-27.75116796875" />
                  <Point X="1.448366210938" Y="-27.7434375" />
                  <Point X="1.417100585938" Y="-27.741119140625" />
                  <Point X="1.293099243164" Y="-27.752529296875" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.156368164062" Y="-27.769396484375" />
                  <Point X="1.128982666016" Y="-27.78073828125" />
                  <Point X="1.104594848633" Y="-27.7954609375" />
                  <Point X="1.008844299316" Y="-27.875076171875" />
                  <Point X="0.92461126709" Y="-27.94511328125" />
                  <Point X="0.904139892578" Y="-27.968865234375" />
                  <Point X="0.887247619629" Y="-27.996693359375" />
                  <Point X="0.875624267578" Y="-28.025810546875" />
                  <Point X="0.846995361328" Y="-28.157525390625" />
                  <Point X="0.821809997559" Y="-28.273396484375" />
                  <Point X="0.81972454834" Y="-28.289626953125" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="0.922566040039" Y="-29.10414453125" />
                  <Point X="1.022065307617" Y="-29.859916015625" />
                  <Point X="0.97572277832" Y="-29.870072265625" />
                  <Point X="0.929315490723" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058456054687" Y="-29.75262890625" />
                  <Point X="-1.14124621582" Y="-29.731330078125" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.54103515625" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.155534545898" Y="-29.335810546875" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188125" Y="-29.178470703125" />
                  <Point X="-1.19902722168" Y="-29.14950390625" />
                  <Point X="-1.205666015625" Y="-29.135466796875" />
                  <Point X="-1.22173840332" Y="-29.10762890625" />
                  <Point X="-1.230575195312" Y="-29.094861328125" />
                  <Point X="-1.250209350586" Y="-29.0709375" />
                  <Point X="-1.261006591797" Y="-29.05978125" />
                  <Point X="-1.385100219727" Y="-28.950953125" />
                  <Point X="-1.494267211914" Y="-28.855216796875" />
                  <Point X="-1.506735839844" Y="-28.84596875" />
                  <Point X="-1.53301965332" Y="-28.829623046875" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576531860352" Y="-28.810224609375" />
                  <Point X="-1.591315795898" Y="-28.805474609375" />
                  <Point X="-1.62145690918" Y="-28.798447265625" />
                  <Point X="-1.636814086914" Y="-28.796169921875" />
                  <Point X="-1.801514648438" Y="-28.785375" />
                  <Point X="-1.946403564453" Y="-28.77587890625" />
                  <Point X="-1.961927856445" Y="-28.7761328125" />
                  <Point X="-1.992726196289" Y="-28.779166015625" />
                  <Point X="-2.008" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053666992188" Y="-28.795494140625" />
                  <Point X="-2.081861328125" Y="-28.808267578125" />
                  <Point X="-2.095436767578" Y="-28.815810546875" />
                  <Point X="-2.232674072266" Y="-28.907509765625" />
                  <Point X="-2.353403320312" Y="-28.988177734375" />
                  <Point X="-2.359686279297" Y="-28.992759765625" />
                  <Point X="-2.380448730469" Y="-29.01013671875" />
                  <Point X="-2.402762207031" Y="-29.032775390625" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.503200195312" Y="-29.162478515625" />
                  <Point X="-2.552354980469" Y="-29.13204296875" />
                  <Point X="-2.747612548828" Y="-29.011146484375" />
                  <Point X="-2.9557265625" Y="-28.850904296875" />
                  <Point X="-2.98086328125" Y="-28.83155078125" />
                  <Point X="-2.737525390625" Y="-28.410076171875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684570312" Y="-27.666185546875" />
                  <Point X="-2.313413574219" Y="-27.63466015625" />
                  <Point X="-2.311638916016" Y="-27.619236328125" />
                  <Point X="-2.310625976562" Y="-27.5883046875" />
                  <Point X="-2.314665771484" Y="-27.557619140625" />
                  <Point X="-2.323649902344" Y="-27.528001953125" />
                  <Point X="-2.329355957031" Y="-27.5135625" />
                  <Point X="-2.343572998047" Y="-27.484732421875" />
                  <Point X="-2.3515546875" Y="-27.4714140625" />
                  <Point X="-2.369584716797" Y="-27.446251953125" />
                  <Point X="-2.379633056641" Y="-27.434408203125" />
                  <Point X="-2.396981689453" Y="-27.417060546875" />
                  <Point X="-2.408822021484" Y="-27.407015625" />
                  <Point X="-2.433978027344" Y="-27.388990234375" />
                  <Point X="-2.447293701172" Y="-27.381009765625" />
                  <Point X="-2.476122558594" Y="-27.36679296875" />
                  <Point X="-2.490563476562" Y="-27.3610859375" />
                  <Point X="-2.520181640625" Y="-27.3521015625" />
                  <Point X="-2.550869384766" Y="-27.3480625" />
                  <Point X="-2.581803466797" Y="-27.349076171875" />
                  <Point X="-2.597227050781" Y="-27.3508515625" />
                  <Point X="-2.628753417969" Y="-27.357123046875" />
                  <Point X="-2.643684082031" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.290127441406" Y="-27.727326171875" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-3.849774902344" Y="-27.943234375" />
                  <Point X="-4.004022460937" Y="-27.740583984375" />
                  <Point X="-4.153235351563" Y="-27.490376953125" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.741057617188" Y="-27.105591796875" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.3988046875" />
                  <Point X="-2.948748535156" Y="-26.368376953125" />
                  <Point X="-2.948577880859" Y="-26.353048828125" />
                  <Point X="-2.950786376953" Y="-26.32137890625" />
                  <Point X="-2.953082763672" Y="-26.306224609375" />
                  <Point X="-2.960082763672" Y="-26.276482421875" />
                  <Point X="-2.971774902344" Y="-26.248251953125" />
                  <Point X="-2.987854980469" Y="-26.22226953125" />
                  <Point X="-2.996946533203" Y="-26.2099296875" />
                  <Point X="-3.01777734375" Y="-26.18597265625" />
                  <Point X="-3.028737060547" Y="-26.175251953125" />
                  <Point X="-3.052241210938" Y="-26.155712890625" />
                  <Point X="-3.064785644531" Y="-26.14689453125" />
                  <Point X="-3.091274658203" Y="-26.1313046875" />
                  <Point X="-3.105443359375" Y="-26.1244765625" />
                  <Point X="-3.134712158203" Y="-26.113251953125" />
                  <Point X="-3.149812255859" Y="-26.10885546875" />
                  <Point X="-3.181695800781" Y="-26.102376953125" />
                  <Point X="-3.19730859375" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-4.006119873047" Y="-26.20048828125" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.680436035156" Y="-26.210291015625" />
                  <Point X="-4.740762695312" Y="-25.9741171875" />
                  <Point X="-4.780241210938" Y="-25.698083984375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.297645996094" Y="-25.523681640625" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.496566162109" Y="-25.308197265625" />
                  <Point X="-3.473744140625" Y="-25.2987578125" />
                  <Point X="-3.462644042969" Y="-25.293294921875" />
                  <Point X="-3.432313964844" Y="-25.275828125" />
                  <Point X="-3.425543701172" Y="-25.2715390625" />
                  <Point X="-3.405989257812" Y="-25.25755078125" />
                  <Point X="-3.381180664062" Y="-25.237017578125" />
                  <Point X="-3.372595458984" Y="-25.22896484375" />
                  <Point X="-3.356489257812" Y="-25.21186328125" />
                  <Point X="-3.342623046875" Y="-25.192904296875" />
                  <Point X="-3.331205566406" Y="-25.172375" />
                  <Point X="-3.326133300781" Y="-25.161755859375" />
                  <Point X="-3.313785400391" Y="-25.13116015625" />
                  <Point X="-3.311241455078" Y="-25.124056640625" />
                  <Point X="-3.304755126953" Y="-25.102390625" />
                  <Point X="-3.298178710938" Y="-25.07362109375" />
                  <Point X="-3.296376220703" Y="-25.062990234375" />
                  <Point X="-3.293987548828" Y="-25.041591796875" />
                  <Point X="-3.294039306641" Y="-25.020060546875" />
                  <Point X="-3.296530761719" Y="-24.998673828125" />
                  <Point X="-3.298384277344" Y="-24.98805078125" />
                  <Point X="-3.305552490234" Y="-24.957373046875" />
                  <Point X="-3.307565917969" Y="-24.950080078125" />
                  <Point X="-3.314756591797" Y="-24.928560546875" />
                  <Point X="-3.326512451172" Y="-24.89987109375" />
                  <Point X="-3.331642333984" Y="-24.88927734375" />
                  <Point X="-3.343169433594" Y="-24.86880859375" />
                  <Point X="-3.357137939453" Y="-24.849921875" />
                  <Point X="-3.3733359375" Y="-24.83290625" />
                  <Point X="-3.381962646484" Y="-24.82490234375" />
                  <Point X="-3.408547607422" Y="-24.80313671875" />
                  <Point X="-3.414963623047" Y="-24.7983203125" />
                  <Point X="-3.435002197266" Y="-24.7849765625" />
                  <Point X="-3.463556152344" Y="-24.7687421875" />
                  <Point X="-3.474445556641" Y="-24.763439453125" />
                  <Point X="-3.496811523438" Y="-24.75426171875" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.202696777344" Y="-24.5643203125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.770208984375" Y="-24.305203125" />
                  <Point X="-4.73133203125" Y="-24.042478515625" />
                  <Point X="-4.651859375" Y="-23.74919921875" />
                  <Point X="-4.633585449219" Y="-23.681763671875" />
                  <Point X="-4.343897949219" Y="-23.71990234375" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057128906" Y="-23.795634765625" />
                  <Point X="-3.736704101562" Y="-23.795296875" />
                  <Point X="-3.715142089844" Y="-23.79341015625" />
                  <Point X="-3.704887939453" Y="-23.7919453125" />
                  <Point X="-3.684603759766" Y="-23.78791015625" />
                  <Point X="-3.674573730469" Y="-23.78533984375" />
                  <Point X="-3.641895263672" Y="-23.775037109375" />
                  <Point X="-3.613147705078" Y="-23.76597265625" />
                  <Point X="-3.603457519531" Y="-23.762326171875" />
                  <Point X="-3.584527099609" Y="-23.753998046875" />
                  <Point X="-3.575286865234" Y="-23.74931640625" />
                  <Point X="-3.556543212891" Y="-23.73849609375" />
                  <Point X="-3.547867919922" Y="-23.7328359375" />
                  <Point X="-3.531183349609" Y="-23.720603515625" />
                  <Point X="-3.515928466797" Y="-23.706625" />
                  <Point X="-3.502287841797" Y="-23.6910703125" />
                  <Point X="-3.495892822266" Y="-23.682921875" />
                  <Point X="-3.483478515625" Y="-23.66519140625" />
                  <Point X="-3.478010253906" Y="-23.656396484375" />
                  <Point X="-3.468062255859" Y="-23.638263671875" />
                  <Point X="-3.463582519531" Y="-23.62892578125" />
                  <Point X="-3.450470214844" Y="-23.59726953125" />
                  <Point X="-3.438935058594" Y="-23.569421875" />
                  <Point X="-3.435498535156" Y="-23.5596484375" />
                  <Point X="-3.429710449219" Y="-23.5397890625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436011962891" Y="-23.395470703125" />
                  <Point X="-3.443508789062" Y="-23.37619140625" />
                  <Point X="-3.447784423828" Y="-23.36675390625" />
                  <Point X="-3.463605957031" Y="-23.336361328125" />
                  <Point X="-3.477524169922" Y="-23.309625" />
                  <Point X="-3.482802246094" Y="-23.3007109375" />
                  <Point X="-3.494293945312" Y="-23.283513671875" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521497558594" Y="-23.25109375" />
                  <Point X="-3.536439208984" Y="-23.2367890625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.942617675781" Y="-22.92440234375" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.153355957031" Y="-22.578494140625" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.791779541016" Y="-22.049103515625" />
                  <Point X="-3.726337402344" Y="-21.964986328125" />
                  <Point X="-3.601675048828" Y="-22.0369609375" />
                  <Point X="-3.254157470703" Y="-22.237599609375" />
                  <Point X="-3.244921630859" Y="-22.24228125" />
                  <Point X="-3.2259921875" Y="-22.250611328125" />
                  <Point X="-3.216298583984" Y="-22.254259765625" />
                  <Point X="-3.195655273438" Y="-22.26076953125" />
                  <Point X="-3.185623535156" Y="-22.263341796875" />
                  <Point X="-3.165333740234" Y="-22.26737890625" />
                  <Point X="-3.155075683594" Y="-22.26884375" />
                  <Point X="-3.109563720703" Y="-22.272826171875" />
                  <Point X="-3.069526367188" Y="-22.276328125" />
                  <Point X="-3.059173828125" Y="-22.276666015625" />
                  <Point X="-3.038487548828" Y="-22.27621484375" />
                  <Point X="-3.028153808594" Y="-22.27542578125" />
                  <Point X="-3.006696044922" Y="-22.272599609375" />
                  <Point X="-2.99651171875" Y="-22.2706875" />
                  <Point X="-2.976422607422" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.93844921875" Y="-22.249552734375" />
                  <Point X="-2.929419189453" Y="-22.244470703125" />
                  <Point X="-2.911165527344" Y="-22.232841796875" />
                  <Point X="-2.902745117188" Y="-22.226806640625" />
                  <Point X="-2.886613769531" Y="-22.213859375" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.846598144531" Y="-22.174642578125" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.752779296875" Y="-21.91008984375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-2.964027587891" Y="-21.465251953125" />
                  <Point X="-3.059388183594" Y="-21.300083984375" />
                  <Point X="-2.911471191406" Y="-21.18667578125" />
                  <Point X="-2.648385498047" Y="-20.984970703125" />
                  <Point X="-2.316827880859" Y="-20.800765625" />
                  <Point X="-2.192524414062" Y="-20.731705078125" />
                  <Point X="-2.118564941406" Y="-20.82808984375" />
                  <Point X="-2.111821533203" Y="-20.83594921875" />
                  <Point X="-2.097517089844" Y="-20.850892578125" />
                  <Point X="-2.089955078125" Y="-20.8579765625" />
                  <Point X="-2.073375" Y="-20.871888671875" />
                  <Point X="-2.065091796875" Y="-20.8781015625" />
                  <Point X="-2.047896484375" Y="-20.889591796875" />
                  <Point X="-2.038984130859" Y="-20.894869140625" />
                  <Point X="-1.988329589844" Y="-20.921240234375" />
                  <Point X="-1.943768188477" Y="-20.9444375" />
                  <Point X="-1.934334594727" Y="-20.9487109375" />
                  <Point X="-1.915060668945" Y="-20.95620703125" />
                  <Point X="-1.905220458984" Y="-20.9594296875" />
                  <Point X="-1.884314331055" Y="-20.965033203125" />
                  <Point X="-1.874174682617" Y="-20.967166015625" />
                  <Point X="-1.853724243164" Y="-20.970314453125" />
                  <Point X="-1.8330546875" Y="-20.971216796875" />
                  <Point X="-1.812407592773" Y="-20.96986328125" />
                  <Point X="-1.802119506836" Y="-20.968623046875" />
                  <Point X="-1.78080456543" Y="-20.96486328125" />
                  <Point X="-1.77071472168" Y="-20.962509765625" />
                  <Point X="-1.750860229492" Y="-20.956720703125" />
                  <Point X="-1.741095581055" Y="-20.95328515625" />
                  <Point X="-1.688335327148" Y="-20.9314296875" />
                  <Point X="-1.641921630859" Y="-20.912205078125" />
                  <Point X="-1.632583862305" Y="-20.9077265625" />
                  <Point X="-1.614452758789" Y="-20.897779296875" />
                  <Point X="-1.605659423828" Y="-20.892310546875" />
                  <Point X="-1.587929443359" Y="-20.879896484375" />
                  <Point X="-1.579780883789" Y="-20.873501953125" />
                  <Point X="-1.564228393555" Y="-20.85986328125" />
                  <Point X="-1.550253173828" Y="-20.84461328125" />
                  <Point X="-1.538021484375" Y="-20.827931640625" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.5215390625" Y="-20.80051171875" />
                  <Point X="-1.516856689453" Y="-20.7912734375" />
                  <Point X="-1.508525634766" Y="-20.77233984375" />
                  <Point X="-1.504876953125" Y="-20.76264453125" />
                  <Point X="-1.487704467773" Y="-20.7081796875" />
                  <Point X="-1.47259753418" Y="-20.660267578125" />
                  <Point X="-1.470026367188" Y="-20.650236328125" />
                  <Point X="-1.465991210938" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752807617" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-1.271771240234" Y="-20.379171875" />
                  <Point X="-0.931164794922" Y="-20.2836796875" />
                  <Point X="-0.529249572754" Y="-20.236638671875" />
                  <Point X="-0.36522253418" Y="-20.21744140625" />
                  <Point X="-0.32847164917" Y="-20.35459765625" />
                  <Point X="-0.225666366577" Y="-20.7382734375" />
                  <Point X="-0.220435150146" Y="-20.752892578125" />
                  <Point X="-0.207661453247" Y="-20.781083984375" />
                  <Point X="-0.200119293213" Y="-20.79465625" />
                  <Point X="-0.182261199951" Y="-20.8213828125" />
                  <Point X="-0.172608886719" Y="-20.833544921875" />
                  <Point X="-0.15145123291" Y="-20.856134765625" />
                  <Point X="-0.126896499634" Y="-20.8749765625" />
                  <Point X="-0.099600570679" Y="-20.88956640625" />
                  <Point X="-0.085354026794" Y="-20.8957421875" />
                  <Point X="-0.054916057587" Y="-20.90607421875" />
                  <Point X="-0.039853721619" Y="-20.909845703125" />
                  <Point X="-0.009317661285" Y="-20.91488671875" />
                  <Point X="0.021629489899" Y="-20.91488671875" />
                  <Point X="0.052165550232" Y="-20.909845703125" />
                  <Point X="0.067227882385" Y="-20.90607421875" />
                  <Point X="0.097665855408" Y="-20.8957421875" />
                  <Point X="0.111912246704" Y="-20.88956640625" />
                  <Point X="0.139208175659" Y="-20.8749765625" />
                  <Point X="0.163763214111" Y="-20.856134765625" />
                  <Point X="0.184920715332" Y="-20.833544921875" />
                  <Point X="0.194573181152" Y="-20.8213828125" />
                  <Point X="0.212431259155" Y="-20.79465625" />
                  <Point X="0.219973724365" Y="-20.781083984375" />
                  <Point X="0.232747116089" Y="-20.752892578125" />
                  <Point X="0.23797819519" Y="-20.7382734375" />
                  <Point X="0.328417327881" Y="-20.40075" />
                  <Point X="0.378190704346" Y="-20.2149921875" />
                  <Point X="0.530475219727" Y="-20.23094140625" />
                  <Point X="0.827864990234" Y="-20.2620859375" />
                  <Point X="1.160409790039" Y="-20.342373046875" />
                  <Point X="1.453608520508" Y="-20.41316015625" />
                  <Point X="1.668379638672" Y="-20.49105859375" />
                  <Point X="1.858246704102" Y="-20.55992578125" />
                  <Point X="2.067555664063" Y="-20.6578125" />
                  <Point X="2.250432373047" Y="-20.743337890625" />
                  <Point X="2.452696777344" Y="-20.861177734375" />
                  <Point X="2.629434570312" Y="-20.96414453125" />
                  <Point X="2.817778808594" Y="-21.0980859375" />
                  <Point X="2.523691162109" Y="-21.6074609375" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.029879516602" Y="-22.5021171875" />
                  <Point X="2.019831542969" Y="-22.53969140625" />
                  <Point X="2.017912231445" Y="-22.5485390625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.017864868164" Y="-22.66735546875" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029143920898" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045318359375" Y="-22.776111328125" />
                  <Point X="2.055680908203" Y="-22.79615625" />
                  <Point X="2.061459960938" Y="-22.80587109375" />
                  <Point X="2.084313232422" Y="-22.83955078125" />
                  <Point X="2.104417724609" Y="-22.8691796875" />
                  <Point X="2.109808105469" Y="-22.8763671875" />
                  <Point X="2.130463867188" Y="-22.899876953125" />
                  <Point X="2.157596191406" Y="-22.924611328125" />
                  <Point X="2.168255615234" Y="-22.933017578125" />
                  <Point X="2.201935546875" Y="-22.95587109375" />
                  <Point X="2.231564208984" Y="-22.9759765625" />
                  <Point X="2.241277099609" Y="-22.98175390625" />
                  <Point X="2.261318603516" Y="-22.992115234375" />
                  <Point X="2.271647216797" Y="-22.99669921875" />
                  <Point X="2.293736816406" Y="-23.004966796875" />
                  <Point X="2.304544189453" Y="-23.00829296875" />
                  <Point X="2.326472412109" Y="-23.01363671875" />
                  <Point X="2.337593261719" Y="-23.015654296875" />
                  <Point X="2.374527099609" Y="-23.020107421875" />
                  <Point X="2.407018066406" Y="-23.024025390625" />
                  <Point X="2.416040283203" Y="-23.0246796875" />
                  <Point X="2.447578369141" Y="-23.02450390625" />
                  <Point X="2.484317382813" Y="-23.020177734375" />
                  <Point X="2.497749511719" Y="-23.01760546875" />
                  <Point X="2.540461669922" Y="-23.00618359375" />
                  <Point X="2.578035888672" Y="-22.996134765625" />
                  <Point X="2.583999755859" Y="-22.994328125" />
                  <Point X="2.604413085938" Y="-22.9869296875" />
                  <Point X="2.62765625" Y="-22.97642578125" />
                  <Point X="2.636033935547" Y="-22.97212890625" />
                  <Point X="3.334475097656" Y="-22.568884765625" />
                  <Point X="3.940405761719" Y="-22.21905078125" />
                  <Point X="4.043956787109" Y="-22.362962890625" />
                  <Point X="4.136885253906" Y="-22.51652734375" />
                  <Point X="3.776455322266" Y="-22.79309375" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.1681328125" Y="-23.260134765625" />
                  <Point X="3.152115478516" Y="-23.274787109375" />
                  <Point X="3.134667724609" Y="-23.2933984375" />
                  <Point X="3.128577636719" Y="-23.300578125" />
                  <Point X="3.097837646484" Y="-23.3406796875" />
                  <Point X="3.070795410156" Y="-23.375958984375" />
                  <Point X="3.065636230469" Y="-23.3833984375" />
                  <Point X="3.049738769531" Y="-23.41062890625" />
                  <Point X="3.034763183594" Y="-23.444453125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.018689453125" Y="-23.4982734375" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077636719" Y="-23.636244140625" />
                  <Point X="3.004698974609" Y="-23.6474296875" />
                  <Point X="3.014098632812" Y="-23.692986328125" />
                  <Point X="3.022367919922" Y="-23.7330625" />
                  <Point X="3.024598388672" Y="-23.741767578125" />
                  <Point X="3.034684570313" Y="-23.771392578125" />
                  <Point X="3.050286865234" Y="-23.80462890625" />
                  <Point X="3.056918945312" Y="-23.816474609375" />
                  <Point X="3.082485351562" Y="-23.855333984375" />
                  <Point X="3.1049765625" Y="-23.88951953125" />
                  <Point X="3.111739257813" Y="-23.898576171875" />
                  <Point X="3.126290527344" Y="-23.915818359375" />
                  <Point X="3.134079101562" Y="-23.92400390625" />
                  <Point X="3.151322021484" Y="-23.94009375" />
                  <Point X="3.160029785156" Y="-23.94730078125" />
                  <Point X="3.178241699219" Y="-23.96062890625" />
                  <Point X="3.187745849609" Y="-23.96675" />
                  <Point X="3.224795166016" Y="-23.98760546875" />
                  <Point X="3.257387939453" Y="-24.005953125" />
                  <Point X="3.265482421875" Y="-24.010013671875" />
                  <Point X="3.294676269531" Y="-24.0219140625" />
                  <Point X="3.330276123047" Y="-24.031978515625" />
                  <Point X="3.34367578125" Y="-24.034744140625" />
                  <Point X="3.393768798828" Y="-24.04136328125" />
                  <Point X="3.437836425781" Y="-24.0471875" />
                  <Point X="3.444033447266" Y="-24.04780078125" />
                  <Point X="3.465708007812" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.164075683594" Y="-23.95985546875" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.707657714844" Y="-23.900814453125" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.383620117188" Y="-24.39332421875" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686027587891" Y="-24.58045703125" />
                  <Point X="3.665614746094" Y="-24.5878671875" />
                  <Point X="3.642375976562" Y="-24.598384765625" />
                  <Point X="3.634005371094" Y="-24.602685546875" />
                  <Point X="3.584790527344" Y="-24.6311328125" />
                  <Point X="3.541495361328" Y="-24.65615625" />
                  <Point X="3.533887207031" Y="-24.66105078125" />
                  <Point X="3.508774658203" Y="-24.68012890625" />
                  <Point X="3.481992919922" Y="-24.705650390625" />
                  <Point X="3.472796386719" Y="-24.7157734375" />
                  <Point X="3.443267578125" Y="-24.753400390625" />
                  <Point X="3.417290527344" Y="-24.786501953125" />
                  <Point X="3.410854248047" Y="-24.79579296875" />
                  <Point X="3.399130615234" Y="-24.815072265625" />
                  <Point X="3.393843261719" Y="-24.825060546875" />
                  <Point X="3.384069580078" Y="-24.8465234375" />
                  <Point X="3.380005615234" Y="-24.8570703125" />
                  <Point X="3.373158935547" Y="-24.878572265625" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.360533203125" Y="-24.940923828125" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.99503515625" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280029297" Y="-25.062943359375" />
                  <Point X="3.351874267578" Y="-25.076423828125" />
                  <Point X="3.361717285156" Y="-25.1278203125" />
                  <Point X="3.370376464844" Y="-25.173033203125" />
                  <Point X="3.373159667969" Y="-25.183990234375" />
                  <Point X="3.380006103516" Y="-25.205490234375" />
                  <Point X="3.384069335938" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.23749609375" />
                  <Point X="3.399128417969" Y="-25.247484375" />
                  <Point X="3.410852294922" Y="-25.266765625" />
                  <Point X="3.417290527344" Y="-25.27605859375" />
                  <Point X="3.446819335938" Y="-25.313685546875" />
                  <Point X="3.472796386719" Y="-25.346787109375" />
                  <Point X="3.478717041016" Y="-25.3536328125" />
                  <Point X="3.501133544922" Y="-25.37580078125" />
                  <Point X="3.530174316406" Y="-25.398724609375" />
                  <Point X="3.541494384766" Y="-25.406404296875" />
                  <Point X="3.590709472656" Y="-25.4348515625" />
                  <Point X="3.634004394531" Y="-25.459876953125" />
                  <Point X="3.639486328125" Y="-25.462810546875" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.68302734375" Y="-25.481029296875" />
                  <Point X="3.691991943359" Y="-25.483916015625" />
                  <Point X="4.300426269531" Y="-25.6469453125" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="4.238875976563" Y="-26.0148515625" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720458984" Y="-25.910841796875" />
                  <Point X="3.354481689453" Y="-25.912677734375" />
                  <Point X="3.257890136719" Y="-25.933671875" />
                  <Point X="3.172917480469" Y="-25.952140625" />
                  <Point X="3.157873291016" Y="-25.9567421875" />
                  <Point X="3.128752685547" Y="-25.9683671875" />
                  <Point X="3.114676269531" Y="-25.975390625" />
                  <Point X="3.086849365234" Y="-25.992283203125" />
                  <Point X="3.074126953125" Y="-26.001529296875" />
                  <Point X="3.050377197266" Y="-26.021998046875" />
                  <Point X="3.039349853516" Y="-26.033220703125" />
                  <Point X="2.980966308594" Y="-26.1034375" />
                  <Point X="2.929605712891" Y="-26.165208984375" />
                  <Point X="2.921326416016" Y="-26.176845703125" />
                  <Point X="2.906605224609" Y="-26.201228515625" />
                  <Point X="2.900163330078" Y="-26.213974609375" />
                  <Point X="2.8888203125" Y="-26.241359375" />
                  <Point X="2.884362304688" Y="-26.254927734375" />
                  <Point X="2.877530761719" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.866789550781" Y="-26.387595703125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876787109375" Y="-26.576671875" />
                  <Point X="2.889158935547" Y="-26.605482421875" />
                  <Point X="2.896541259766" Y="-26.619373046875" />
                  <Point X="2.949996337891" Y="-26.702517578125" />
                  <Point X="2.997021484375" Y="-26.775662109375" />
                  <Point X="3.001740966797" Y="-26.7823515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486328125" Y="-26.828119140625" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.617426513672" Y="-27.269537109375" />
                  <Point X="4.087170898438" Y="-27.629984375" />
                  <Point X="4.045498046875" Y="-27.69741796875" />
                  <Point X="4.001274658203" Y="-27.760251953125" />
                  <Point X="3.562827392578" Y="-27.507115234375" />
                  <Point X="2.848454345703" Y="-27.094671875" />
                  <Point X="2.841199951172" Y="-27.090890625" />
                  <Point X="2.815021240234" Y="-27.07951171875" />
                  <Point X="2.783116943359" Y="-27.069326171875" />
                  <Point X="2.771108642578" Y="-27.066337890625" />
                  <Point X="2.656149414062" Y="-27.045576171875" />
                  <Point X="2.555018310547" Y="-27.0273125" />
                  <Point X="2.539359375" Y="-27.02580859375" />
                  <Point X="2.508008789062" Y="-27.025404296875" />
                  <Point X="2.492317138672" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444846435547" Y="-27.03513671875" />
                  <Point X="2.415069091797" Y="-27.0449609375" />
                  <Point X="2.40058984375" Y="-27.051109375" />
                  <Point X="2.305086914062" Y="-27.10137109375" />
                  <Point X="2.221071777344" Y="-27.145587890625" />
                  <Point X="2.208968994141" Y="-27.153169921875" />
                  <Point X="2.186038818359" Y="-27.1700625" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144940429687" Y="-27.21116015625" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.070201660156" Y="-27.341697265625" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.022949462891" Y="-27.695099609375" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.432378662109" Y="-28.5020234375" />
                  <Point X="2.735893066406" Y="-29.027724609375" />
                  <Point X="2.723751464844" Y="-29.036083984375" />
                  <Point X="2.379047363281" Y="-28.58685546875" />
                  <Point X="1.833914916992" Y="-27.876423828125" />
                  <Point X="1.828658569336" Y="-27.87015234375" />
                  <Point X="1.808834350586" Y="-27.84962890625" />
                  <Point X="1.783251098633" Y="-27.82800390625" />
                  <Point X="1.773297973633" Y="-27.820646484375" />
                  <Point X="1.659917236328" Y="-27.74775390625" />
                  <Point X="1.560174560547" Y="-27.68362890625" />
                  <Point X="1.546280029297" Y="-27.676244140625" />
                  <Point X="1.517466308594" Y="-27.663873046875" />
                  <Point X="1.502547485352" Y="-27.65888671875" />
                  <Point X="1.470927124023" Y="-27.65115625" />
                  <Point X="1.455391235352" Y="-27.648697265625" />
                  <Point X="1.424125610352" Y="-27.64637890625" />
                  <Point X="1.408395629883" Y="-27.64651953125" />
                  <Point X="1.28439440918" Y="-27.6579296875" />
                  <Point X="1.175308959961" Y="-27.667966796875" />
                  <Point X="1.161228027344" Y="-27.67033984375" />
                  <Point X="1.133582641602" Y="-27.677169921875" />
                  <Point X="1.120017822266" Y="-27.681626953125" />
                  <Point X="1.092632202148" Y="-27.69296875" />
                  <Point X="1.079885131836" Y="-27.699408203125" />
                  <Point X="1.055497314453" Y="-27.714130859375" />
                  <Point X="1.043857055664" Y="-27.7224140625" />
                  <Point X="0.948106384277" Y="-27.802029296875" />
                  <Point X="0.863873352051" Y="-27.87206640625" />
                  <Point X="0.852650695801" Y="-27.883091796875" />
                  <Point X="0.832179199219" Y="-27.90684375" />
                  <Point X="0.822930725098" Y="-27.9195703125" />
                  <Point X="0.806038391113" Y="-27.9473984375" />
                  <Point X="0.79901776123" Y="-27.96147265625" />
                  <Point X="0.78739440918" Y="-27.99058984375" />
                  <Point X="0.782791809082" Y="-28.0056328125" />
                  <Point X="0.754162902832" Y="-28.13734765625" />
                  <Point X="0.728977539063" Y="-28.25321875" />
                  <Point X="0.727584655762" Y="-28.2612890625" />
                  <Point X="0.72472454834" Y="-28.289677734375" />
                  <Point X="0.724742248535" Y="-28.323169921875" />
                  <Point X="0.725555053711" Y="-28.33551953125" />
                  <Point X="0.82837878418" Y="-29.116544921875" />
                  <Point X="0.833091369629" Y="-29.15233984375" />
                  <Point X="0.655064880371" Y="-28.487935546875" />
                  <Point X="0.652606079102" Y="-28.480123046875" />
                  <Point X="0.642147033691" Y="-28.453583984375" />
                  <Point X="0.626788635254" Y="-28.42381640625" />
                  <Point X="0.620407653809" Y="-28.41320703125" />
                  <Point X="0.533304992676" Y="-28.287708984375" />
                  <Point X="0.456679748535" Y="-28.177306640625" />
                  <Point X="0.446668518066" Y="-28.16516796875" />
                  <Point X="0.424785430908" Y="-28.14271484375" />
                  <Point X="0.412913726807" Y="-28.132400390625" />
                  <Point X="0.386658599854" Y="-28.11315625" />
                  <Point X="0.373244659424" Y="-28.1049375" />
                  <Point X="0.34524230957" Y="-28.090828125" />
                  <Point X="0.330654083252" Y="-28.0849375" />
                  <Point X="0.195867980957" Y="-28.04310546875" />
                  <Point X="0.07729486084" Y="-28.0063046875" />
                  <Point X="0.063377067566" Y="-28.003109375" />
                  <Point X="0.035218082428" Y="-27.99883984375" />
                  <Point X="0.020976737976" Y="-27.997765625" />
                  <Point X="-0.00866476059" Y="-27.997765625" />
                  <Point X="-0.022906103134" Y="-27.99883984375" />
                  <Point X="-0.051065090179" Y="-28.003109375" />
                  <Point X="-0.064982879639" Y="-28.0063046875" />
                  <Point X="-0.199768981934" Y="-28.04813671875" />
                  <Point X="-0.318342102051" Y="-28.0849375" />
                  <Point X="-0.332930786133" Y="-28.090830078125" />
                  <Point X="-0.360932678223" Y="-28.104939453125" />
                  <Point X="-0.37434588623" Y="-28.113154296875" />
                  <Point X="-0.400601318359" Y="-28.1323984375" />
                  <Point X="-0.412473175049" Y="-28.14271484375" />
                  <Point X="-0.434356536865" Y="-28.16516796875" />
                  <Point X="-0.44436807251" Y="-28.177306640625" />
                  <Point X="-0.531470458984" Y="-28.3028046875" />
                  <Point X="-0.60809564209" Y="-28.41320703125" />
                  <Point X="-0.612471252441" Y="-28.420130859375" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778259277" Y="-28.47621484375" />
                  <Point X="-0.642753051758" Y="-28.487935546875" />
                  <Point X="-0.83022668457" Y="-29.18759765625" />
                  <Point X="-0.985424926758" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.698891849119" Y="-25.631196250035" />
                  <Point X="-4.596416310565" Y="-26.278201336798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.606623478301" Y="-25.606472759999" />
                  <Point X="-4.502196738652" Y="-26.265797245632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74428624928" Y="-24.130021175173" />
                  <Point X="-4.696454512895" Y="-24.432018873221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.514355107483" Y="-25.581749269963" />
                  <Point X="-4.407977166739" Y="-26.253393154466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.687554773949" Y="-23.880926557429" />
                  <Point X="-4.596007473387" Y="-24.458933465016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.422086736666" Y="-25.557025779926" />
                  <Point X="-4.313757594826" Y="-26.240989063301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.129597658724" Y="-27.403729138809" />
                  <Point X="-4.102362990683" Y="-27.575682065405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.62268764789" Y="-23.683198416795" />
                  <Point X="-4.49556043388" Y="-24.485848056812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.329818365848" Y="-25.53230228989" />
                  <Point X="-4.219538022913" Y="-26.228584972135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.043836282447" Y="-27.337922102133" />
                  <Point X="-3.973764708285" Y="-27.780336609638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.524455113263" Y="-23.696131175045" />
                  <Point X="-4.395113394372" Y="-24.512762648607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.237549969813" Y="-25.507578959072" />
                  <Point X="-4.125318451" Y="-26.216180880969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.958074906171" Y="-27.272115065456" />
                  <Point X="-3.852306927458" Y="-27.939907801256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.426222578636" Y="-23.709063933295" />
                  <Point X="-4.294666354864" Y="-24.539677240403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.145281560277" Y="-25.482855713491" />
                  <Point X="-4.031098879087" Y="-26.203776789804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.872313529894" Y="-27.20630802878" />
                  <Point X="-3.747929327279" Y="-27.991638976444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.327990053553" Y="-23.721996631284" />
                  <Point X="-4.194219313482" Y="-24.566591844027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.053013150742" Y="-25.458132467911" />
                  <Point X="-3.936879315314" Y="-26.19137264725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.786552153618" Y="-27.140500992103" />
                  <Point X="-3.659803664895" Y="-27.94075945476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.229757577864" Y="-23.734929017413" />
                  <Point X="-4.093772251777" Y="-24.593506575972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.960744741206" Y="-25.43340922233" />
                  <Point X="-3.842659754476" Y="-26.178968486158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.700790768528" Y="-27.074694011073" />
                  <Point X="-3.571678002511" Y="-27.889879933076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.131525102175" Y="-23.747861403542" />
                  <Point X="-3.993325190071" Y="-24.620421307918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.868476331671" Y="-25.40868597675" />
                  <Point X="-3.748440193639" Y="-26.166564325065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.61502937348" Y="-27.008887092914" />
                  <Point X="-3.483552340127" Y="-27.839000411392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.206208630822" Y="-22.669045105388" />
                  <Point X="-4.196635157867" Y="-22.729489634756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.033292626486" Y="-23.760793789671" />
                  <Point X="-3.892878128366" Y="-24.647336039863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776207922135" Y="-25.383962731169" />
                  <Point X="-3.654220632801" Y="-26.154160163973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.529267978433" Y="-26.943080174755" />
                  <Point X="-3.395426677743" Y="-27.788120889708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.130553692581" Y="-22.539428530255" />
                  <Point X="-4.087144386955" Y="-22.813504099402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.935060150797" Y="-23.7737261758" />
                  <Point X="-3.79243106666" Y="-24.674250771809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683939512599" Y="-25.359239485589" />
                  <Point X="-3.560001071963" Y="-26.141756002881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443506583385" Y="-26.877273256595" />
                  <Point X="-3.307301015359" Y="-27.737241368024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.054898508028" Y="-22.409813510278" />
                  <Point X="-3.977653616043" Y="-22.897518564049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836827675109" Y="-23.786658561928" />
                  <Point X="-3.691984004954" Y="-24.701165503755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.591671103064" Y="-25.334516240008" />
                  <Point X="-3.465781511126" Y="-26.129351841788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.357745188337" Y="-26.811466338436" />
                  <Point X="-3.219175335307" Y="-27.686361957892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.977945320216" Y="-22.288393760344" />
                  <Point X="-3.868162710231" Y="-22.981533880421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73926209108" Y="-23.795380359825" />
                  <Point X="-3.591536943249" Y="-24.7280802357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.499497907781" Y="-25.309191835294" />
                  <Point X="-3.371561950288" Y="-26.116947680696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.271983793289" Y="-26.745659420277" />
                  <Point X="-3.131049650979" Y="-27.63548257476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.9500610507" Y="-28.778199623912" />
                  <Point X="-2.936159527508" Y="-28.865970387018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.898030323634" Y="-22.185674135014" />
                  <Point X="-3.758671740939" Y="-23.065549597586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646090471585" Y="-23.776359757498" />
                  <Point X="-3.490816380021" Y="-24.756721788297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.410932759585" Y="-25.26108711782" />
                  <Point X="-3.277342389451" Y="-26.104543519604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.186222398242" Y="-26.679852502118" />
                  <Point X="-3.04292396665" Y="-27.584603191628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874582842869" Y="-28.647467216886" />
                  <Point X="-2.826616358912" Y="-28.95031567762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.818115327052" Y="-22.082954509684" />
                  <Point X="-3.649180771648" Y="-23.149565314752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.555963233305" Y="-23.738117688653" />
                  <Point X="-3.384112190544" Y="-24.823142470187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.329398421004" Y="-25.168591615497" />
                  <Point X="-3.181494886987" Y="-26.102417777417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.100461003194" Y="-26.614045583958" />
                  <Point X="-2.954798282322" Y="-27.533723808495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.799104635038" Y="-28.516734809861" />
                  <Point X="-2.717881959749" Y="-29.029554598991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.738200374673" Y="-21.980234605274" />
                  <Point X="-3.53961305223" Y="-23.234065613141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.473907007678" Y="-23.648917251454" />
                  <Point X="-3.079652055325" Y="-26.138145054041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.015820720446" Y="-26.541160241316" />
                  <Point X="-2.866672597994" Y="-27.482844425363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.723626426434" Y="-28.386002407717" />
                  <Point X="-2.611239807109" Y="-29.095583595709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.636187593919" Y="-22.017034898232" />
                  <Point X="-2.962470486422" Y="-26.270717306153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948633875261" Y="-26.358078230831" />
                  <Point X="-2.778546913665" Y="-27.431965042231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.648148214404" Y="-28.2552700272" />
                  <Point X="-2.504597540738" Y="-29.161613310497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.530322766941" Y="-22.078156053875" />
                  <Point X="-2.690421229337" Y="-27.381085659099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.572670002375" Y="-28.124537646682" />
                  <Point X="-2.424517435236" Y="-29.059936141861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.42445798897" Y="-22.139276900092" />
                  <Point X="-2.59897071499" Y="-27.351198426532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.497191790346" Y="-27.993805266165" />
                  <Point X="-2.341010203885" Y="-28.979896994249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.318593211" Y="-22.200397746309" />
                  <Point X="-2.501758350502" Y="-27.357690084018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.421713578317" Y="-27.863072885647" />
                  <Point X="-2.254030876843" Y="-28.921779796062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.213750870827" Y="-22.255063174337" />
                  <Point X="-2.396018215296" Y="-27.418023966798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.346235366287" Y="-27.73234050513" />
                  <Point X="-2.167051619644" Y="-28.863662156904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.114826235029" Y="-22.272365687404" />
                  <Point X="-2.079789722273" Y="-28.807329037564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.018361647112" Y="-22.274136069433" />
                  <Point X="-1.988137694177" Y="-28.77871411273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.927109087381" Y="-22.242999000611" />
                  <Point X="-1.891836117121" Y="-28.779455284691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988835441882" Y="-21.245991080343" />
                  <Point X="-2.950357954424" Y="-21.488928375059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.842414150872" Y="-22.17045872824" />
                  <Point X="-1.794643005217" Y="-28.785825386149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.903066474533" Y="-21.180231971813" />
                  <Point X="-2.817812783762" Y="-21.718502591052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.763646788841" Y="-22.060493223524" />
                  <Point X="-1.697449861476" Y="-28.792195688617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.81729737598" Y="-21.114473691674" />
                  <Point X="-1.599463365678" Y="-28.803575018838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.731528277426" Y="-21.048715411536" />
                  <Point X="-1.495210768525" Y="-28.854516955977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.64567846429" Y="-20.983466743214" />
                  <Point X="-1.383535212437" Y="-28.952325611337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.557273426802" Y="-20.934351126521" />
                  <Point X="-1.271835925439" Y="-29.05028409777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.468868389313" Y="-20.885235509828" />
                  <Point X="-1.064664209175" Y="-29.751031779084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.380463351824" Y="-20.836119893136" />
                  <Point X="-0.973205008849" Y="-29.721199387632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.292058333631" Y="-20.787004154612" />
                  <Point X="-0.912753557903" Y="-29.495591771567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.203653365012" Y="-20.737888103092" />
                  <Point X="-0.852302106956" Y="-29.269984155501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.08821820778" Y="-20.859433945875" />
                  <Point X="-0.791850693193" Y="-29.044376304673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.981698100348" Y="-20.924692379474" />
                  <Point X="-0.731399300818" Y="-28.818768318798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.878945697577" Y="-20.966162462059" />
                  <Point X="-0.670947908444" Y="-28.593160332924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.782908501716" Y="-20.965234396853" />
                  <Point X="-0.604162968177" Y="-28.407540794648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.691847991122" Y="-20.932884777497" />
                  <Point X="-0.525849959994" Y="-28.294706612644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.602446439933" Y="-20.890060900692" />
                  <Point X="-0.447536956316" Y="-28.181872402191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.520705828327" Y="-20.798867754983" />
                  <Point X="-0.363307399539" Y="-28.106393837829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.46687952926" Y="-20.53143057621" />
                  <Point X="-0.272762126832" Y="-28.070791134485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.389579463266" Y="-20.412200928921" />
                  <Point X="-0.181084500807" Y="-28.042337828619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.297484752932" Y="-20.386380989742" />
                  <Point X="-0.089406820038" Y="-28.013884868399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.205390006541" Y="-20.36056127822" />
                  <Point X="0.004224329568" Y="-27.997765625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.113295246182" Y="-20.334741654884" />
                  <Point X="0.103025821994" Y="-28.014290641415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.021200485824" Y="-20.308922031547" />
                  <Point X="0.204182539513" Y="-28.045685963826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.929053433101" Y="-20.283432569205" />
                  <Point X="0.305339180201" Y="-28.077080801145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.834619823637" Y="-20.272379857953" />
                  <Point X="0.409939895638" Y="-28.130220670629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.740186214173" Y="-20.261327146702" />
                  <Point X="0.530406292652" Y="-28.283532511203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.645752604709" Y="-20.25027443545" />
                  <Point X="0.664602301079" Y="-28.523529706632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.551318995245" Y="-20.239221724198" />
                  <Point X="0.72472781782" Y="-28.295864222984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.456885330271" Y="-20.22816936342" />
                  <Point X="0.778251986242" Y="-28.026519466387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.358320496243" Y="-20.243200177515" />
                  <Point X="0.851864024498" Y="-27.884004528381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.16782608903" Y="-20.838651473549" />
                  <Point X="0.936582243577" Y="-27.811611256367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.061306771524" Y="-20.903904919752" />
                  <Point X="1.021573587827" Y="-27.740942428817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.036234878219" Y="-20.912475602518" />
                  <Point X="1.109080557965" Y="-27.68615663803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.127473249814" Y="-20.881248953331" />
                  <Point X="1.201994858683" Y="-27.665511388879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.210419392076" Y="-20.79766722863" />
                  <Point X="1.296797460294" Y="-27.656788402355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.275196708784" Y="-20.599372054072" />
                  <Point X="1.391599997045" Y="-27.648065006318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.33564811779" Y="-20.373764173207" />
                  <Point X="1.488972533146" Y="-27.655567947572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.407165936177" Y="-20.218026851331" />
                  <Point X="1.592937232671" Y="-27.70469217063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.504972547248" Y="-20.228270434083" />
                  <Point X="1.700025803385" Y="-27.773539740133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.602779079982" Y="-20.238513522236" />
                  <Point X="1.808172756712" Y="-27.84906967447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.700585585086" Y="-20.248756435936" />
                  <Point X="1.928138118541" Y="-27.999218103382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.798392090189" Y="-20.258999349636" />
                  <Point X="2.049339626482" Y="-28.157171251684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.897736990182" Y="-20.278955306395" />
                  <Point X="2.028819237522" Y="-27.420327558766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.112349483432" Y="-27.947716775397" />
                  <Point X="2.170541134423" Y="-28.315124399986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.997745413761" Y="-20.30310058621" />
                  <Point X="2.102753447765" Y="-27.279846734626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.244894806411" Y="-28.177291953078" />
                  <Point X="2.291742642364" Y="-28.473077548288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.097753837341" Y="-20.327245866025" />
                  <Point X="2.182087652486" Y="-27.173460133808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.37744012939" Y="-28.406867130759" />
                  <Point X="2.41294416252" Y="-28.631030773707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.197762248718" Y="-20.351391068794" />
                  <Point X="2.269795818253" Y="-27.11994464223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.509985392717" Y="-28.636441931805" />
                  <Point X="2.534145714134" Y="-28.788984197748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.297770639625" Y="-20.375536142322" />
                  <Point X="2.35857936544" Y="-27.073218841717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.642530613814" Y="-28.866016466229" />
                  <Point X="2.655347265748" Y="-28.94693762179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.397779030533" Y="-20.39968121585" />
                  <Point X="2.448589490417" Y="-27.034237348582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.498687725234" Y="-20.429510583821" />
                  <Point X="2.543501704653" Y="-27.026206428931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.600734146329" Y="-20.466523273533" />
                  <Point X="2.642359315421" Y="-27.043085762615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.702780614932" Y="-20.503536263202" />
                  <Point X="2.023250777698" Y="-22.526905238774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.069253854537" Y="-22.817357234849" />
                  <Point X="2.741375788907" Y="-27.060968116023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.804827176955" Y="-20.540549842701" />
                  <Point X="2.092184420594" Y="-22.354852074976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.185623384866" Y="-22.944802477183" />
                  <Point X="2.842398154101" Y="-27.09151517124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.907746729654" Y="-20.583075268402" />
                  <Point X="2.167662677782" Y="-22.224119979583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.29118541889" Y="-23.004011873352" />
                  <Point X="2.948203723552" Y="-27.152262189578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01162531161" Y="-20.631655766522" />
                  <Point X="2.243140934971" Y="-22.093387884189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390218582059" Y="-23.021999601269" />
                  <Point X="2.896990845622" Y="-26.221633747932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980725757569" Y="-26.75031517507" />
                  <Point X="3.05406853557" Y="-27.213383250765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.115503899904" Y="-20.680236304665" />
                  <Point X="2.318619192159" Y="-21.962655788795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.486061318717" Y="-23.019843768969" />
                  <Point X="2.97549652167" Y="-26.110016022949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.095745099866" Y="-26.869235665671" />
                  <Point X="3.159933347588" Y="-27.274504311952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.219382495592" Y="-20.728816889493" />
                  <Point X="2.394097449348" Y="-21.831923693402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578469561029" Y="-22.996003392797" />
                  <Point X="3.056855691739" Y="-26.016414550163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.205236053594" Y="-26.953251284572" />
                  <Point X="3.265798159606" Y="-27.335625373139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.32472164394" Y="-20.786619040883" />
                  <Point X="2.469575706536" Y="-21.701191598008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667953571577" Y="-22.953700143888" />
                  <Point X="3.144438172079" Y="-25.96210551203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.314727007321" Y="-27.037266903472" />
                  <Point X="3.371662971624" Y="-27.396746434325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.430683485947" Y="-20.84835272531" />
                  <Point X="2.545053958581" Y="-21.570459470139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.756079274529" Y="-22.902820878344" />
                  <Point X="3.236843440638" Y="-25.938246360316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.424217961049" Y="-27.121282522372" />
                  <Point X="3.477527783642" Y="-27.457867495512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.536645231221" Y="-20.910085798989" />
                  <Point X="2.620532197596" Y="-21.439727260004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.844204977482" Y="-22.8519416128" />
                  <Point X="3.329826698302" Y="-25.918036488186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.533708914777" Y="-27.205298141273" />
                  <Point X="3.583392588631" Y="-27.518988512321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.642909201084" Y="-20.973727043626" />
                  <Point X="2.696010436611" Y="-21.308995049869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.932330680434" Y="-22.801062347256" />
                  <Point X="3.034395813525" Y="-23.445476235898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.104645715291" Y="-23.889016659581" />
                  <Point X="3.424494740373" Y="-25.908463926162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.643199854923" Y="-27.289313674421" />
                  <Point X="3.689257364466" Y="-27.580109345057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.751302290498" Y="-21.050811020053" />
                  <Point X="2.771488675626" Y="-21.178262839733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020456383387" Y="-22.750183081711" />
                  <Point X="3.11121735658" Y="-23.323225313684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.215627478022" Y="-23.982444876086" />
                  <Point X="3.364311166899" Y="-24.92119674194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.421331267592" Y="-25.281207489054" />
                  <Point X="3.522593361256" Y="-25.920551186308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.752690750952" Y="-27.373328929024" />
                  <Point X="3.795122140301" Y="-27.641230177794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.108582086339" Y="-22.699303816167" />
                  <Point X="3.194200165121" Y="-23.239875090759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.319159242783" Y="-24.028835656617" />
                  <Point X="3.435488933822" Y="-24.76331241962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.5368451163" Y="-25.403250170258" />
                  <Point X="3.620825863153" Y="-25.933483737909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.862181646981" Y="-27.457344183627" />
                  <Point X="3.900986916136" Y="-27.70235101053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.196707789292" Y="-22.648424550622" />
                  <Point X="3.279961518066" Y="-23.174067906772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.417831309999" Y="-24.044543514415" />
                  <Point X="3.517453822425" Y="-24.673535303145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.642701071531" Y="-25.464315311895" />
                  <Point X="3.71905836505" Y="-25.946416289509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.97167254301" Y="-27.54135943823" />
                  <Point X="4.005410901247" Y="-27.75437504865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.284833492244" Y="-22.597545285078" />
                  <Point X="3.365722871011" Y="-23.108260722784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.514154183682" Y="-24.045419147989" />
                  <Point X="3.605065909451" Y="-24.619413194261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.744205596738" Y="-25.497906605622" />
                  <Point X="3.817290866948" Y="-25.95934884111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.081163439039" Y="-27.625374692833" />
                  <Point X="4.082970158399" Y="-27.63678186993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.372959189065" Y="-22.546665980818" />
                  <Point X="3.451484223955" Y="-23.042453538796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.608373734661" Y="-24.033014924649" />
                  <Point X="3.694679237204" Y="-24.57792642206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.844652649485" Y="-25.524821281008" />
                  <Point X="3.915523368845" Y="-25.972281392711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.461084877976" Y="-22.495786626618" />
                  <Point X="3.5372455769" Y="-22.976646354809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.702593285639" Y="-24.02061070131" />
                  <Point X="3.786947625319" Y="-24.553203041235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.945099702232" Y="-25.551735956394" />
                  <Point X="4.013755870742" Y="-25.985213944312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.549210566887" Y="-22.444907272418" />
                  <Point X="3.623006929845" Y="-22.910839170821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.796812836618" Y="-24.008206477971" />
                  <Point X="3.879216013434" Y="-24.52847966041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.04554675498" Y="-25.57865063178" />
                  <Point X="4.111988372639" Y="-25.998146495913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.637336255797" Y="-22.394027918218" />
                  <Point X="3.708768282789" Y="-22.845031986834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.891032387596" Y="-23.995802254631" />
                  <Point X="3.971484401549" Y="-24.503756279585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.145993807727" Y="-25.605565307166" />
                  <Point X="4.210220874536" Y="-26.011079047513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.725461944708" Y="-22.343148564018" />
                  <Point X="3.794529649742" Y="-22.779224891293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.985251938575" Y="-23.983398031292" />
                  <Point X="4.063752789664" Y="-24.479032898759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.246440860475" Y="-25.632479982552" />
                  <Point X="4.308453349639" Y="-26.024011429944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.813587633619" Y="-22.292269209818" />
                  <Point X="3.880291069157" Y="-22.713418126981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079471489554" Y="-23.970993807953" />
                  <Point X="4.156021177779" Y="-24.454309517934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.346887891363" Y="-25.659394519927" />
                  <Point X="4.406685813707" Y="-26.036943742702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.90171332253" Y="-22.241389855618" />
                  <Point X="3.966052488572" Y="-22.647611362669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.173691038173" Y="-23.958589569716" />
                  <Point X="4.248289565894" Y="-24.429586137109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.447334896853" Y="-25.686308896942" />
                  <Point X="4.504918277776" Y="-26.04987605546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009587486847" Y="-22.315197467928" />
                  <Point X="4.051813907987" Y="-22.581804598357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.26791056603" Y="-23.946185200396" />
                  <Point X="4.340557954009" Y="-24.404862756284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.547781902343" Y="-25.713223273957" />
                  <Point X="4.603150741844" Y="-26.062808368218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.362130093888" Y="-23.933780831077" />
                  <Point X="4.432826330146" Y="-24.380139299834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.648228907833" Y="-25.740137650972" />
                  <Point X="4.701383205912" Y="-26.075740680976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.456349621746" Y="-23.921376461757" />
                  <Point X="4.525094695801" Y="-24.355415777204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.748675913324" Y="-25.767052027987" />
                  <Point X="4.767971725971" Y="-25.888880994319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.550569149603" Y="-23.908972092438" />
                  <Point X="4.617363061457" Y="-24.330692254573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.644788677461" Y="-23.896567723118" />
                  <Point X="4.709631427112" Y="-24.305968731943" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998374023438" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.68464666748" Y="-29.33244140625" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.377216278076" Y="-28.396044921875" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.274335906982" Y="-28.2663984375" />
                  <Point X="0.139549758911" Y="-28.22456640625" />
                  <Point X="0.02097677803" Y="-28.187765625" />
                  <Point X="-0.008664756775" Y="-28.187765625" />
                  <Point X="-0.143450897217" Y="-28.22959765625" />
                  <Point X="-0.262023895264" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.375381561279" Y="-28.411140625" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.646700744629" Y="-29.2367734375" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-0.961441833496" Y="-29.9650078125" />
                  <Point X="-1.100257202148" Y="-29.9380625" />
                  <Point X="-1.256096313477" Y="-29.89796875" />
                  <Point X="-1.35158972168" Y="-29.8733984375" />
                  <Point X="-1.334803344727" Y="-29.74589453125" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.341883666992" Y="-29.372876953125" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282958984" Y="-29.20262890625" />
                  <Point X="-1.510376586914" Y="-29.09380078125" />
                  <Point X="-1.619543701172" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.813941040039" Y="-28.97496875" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878295898" Y="-28.973791015625" />
                  <Point X="-2.127115478516" Y="-29.065490234375" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.364986083984" Y="-29.294462890625" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.652376953125" Y="-29.2935859375" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-3.071641601562" Y="-29.001447265625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.9020703125" Y="-28.315076171875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.59759375" />
                  <Point X="-2.513979736328" Y="-27.568763671875" />
                  <Point X="-2.531328369141" Y="-27.551416015625" />
                  <Point X="-2.560157226562" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.195127441406" Y="-27.891869140625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.000961425781" Y="-28.0583125" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.316420898438" Y="-27.587693359375" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.856722167969" Y="-26.954853515625" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140325927734" Y="-26.33459765625" />
                  <Point X="-3.161156738281" Y="-26.310640625" />
                  <Point X="-3.187645751953" Y="-26.29505078125" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.981320068359" Y="-26.38886328125" />
                  <Point X="-4.803283691406" Y="-26.497076171875" />
                  <Point X="-4.864525878906" Y="-26.257314453125" />
                  <Point X="-4.927392578125" Y="-26.0111953125" />
                  <Point X="-4.968327148438" Y="-25.724982421875" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-4.346821777344" Y="-25.340154296875" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.527133789062" Y="-25.1111796875" />
                  <Point X="-3.502325195312" Y="-25.090646484375" />
                  <Point X="-3.489977294922" Y="-25.06005078125" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.490569091797" Y="-25.000603515625" />
                  <Point X="-3.502324951172" Y="-24.9719140625" />
                  <Point X="-3.528909912109" Y="-24.9501484375" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.251872558594" Y="-24.74784765625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.958162109375" Y="-24.277390625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.835245605469" Y="-23.69950390625" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.319097167969" Y="-23.53152734375" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731704101562" Y="-23.6041328125" />
                  <Point X="-3.699025634766" Y="-23.593830078125" />
                  <Point X="-3.670278076172" Y="-23.584765625" />
                  <Point X="-3.651534423828" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.6260078125" Y="-23.52455859375" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.632137695312" Y="-23.424095703125" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.058282714844" Y="-23.075140625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.317448730469" Y="-22.482716796875" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.941740966797" Y="-21.932435546875" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.506674560547" Y="-21.87241796875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138513671875" Y="-22.07956640625" />
                  <Point X="-3.093001708984" Y="-22.083548828125" />
                  <Point X="-3.052964355469" Y="-22.08705078125" />
                  <Point X="-3.031506591797" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.980948242188" Y="-22.040291015625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.942056152344" Y="-21.9266484375" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.128572509766" Y="-21.560251953125" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-3.027075927734" Y="-21.03589453125" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.409102294922" Y="-20.63467578125" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.074212646484" Y="-20.57378125" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246582031" Y="-20.726337890625" />
                  <Point X="-1.900592041016" Y="-20.752708984375" />
                  <Point X="-1.856030517578" Y="-20.77590625" />
                  <Point X="-1.835124389648" Y="-20.781509765625" />
                  <Point X="-1.813809448242" Y="-20.77775" />
                  <Point X="-1.761049194336" Y="-20.75589453125" />
                  <Point X="-1.714635498047" Y="-20.736669921875" />
                  <Point X="-1.696905517578" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.668911010742" Y="-20.651046875" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.67198425293" Y="-20.42915234375" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.323062866211" Y="-20.1962265625" />
                  <Point X="-0.968082946777" Y="-20.096703125" />
                  <Point X="-0.551336303711" Y="-20.047927734375" />
                  <Point X="-0.224199996948" Y="-20.009640625" />
                  <Point X="-0.144945755005" Y="-20.305421875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.02428212738" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594047546" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.144891479492" Y="-20.35157421875" />
                  <Point X="0.236648422241" Y="-20.0091328125" />
                  <Point X="0.550265136719" Y="-20.0419765625" />
                  <Point X="0.860210205078" Y="-20.074435546875" />
                  <Point X="1.205000976562" Y="-20.1576796875" />
                  <Point X="1.508455566406" Y="-20.230943359375" />
                  <Point X="1.733163330078" Y="-20.3124453125" />
                  <Point X="1.931044921875" Y="-20.38421875" />
                  <Point X="2.148045410156" Y="-20.485703125" />
                  <Point X="2.338686279297" Y="-20.574859375" />
                  <Point X="2.548342529297" Y="-20.697005859375" />
                  <Point X="2.732519287109" Y="-20.804306640625" />
                  <Point X="2.930243896484" Y="-20.94491796875" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.688236083984" Y="-21.7024609375" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.213430175781" Y="-22.55119921875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.206498291016" Y="-22.644609375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.241535400391" Y="-22.7328671875" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.274938720703" Y="-22.775794921875" />
                  <Point X="2.308618652344" Y="-22.7986484375" />
                  <Point X="2.338247314453" Y="-22.81875390625" />
                  <Point X="2.360336914062" Y="-22.827021484375" />
                  <Point X="2.397270751953" Y="-22.831474609375" />
                  <Point X="2.42976171875" Y="-22.835392578125" />
                  <Point X="2.448665527344" Y="-22.8340546875" />
                  <Point X="2.491377685547" Y="-22.8226328125" />
                  <Point X="2.528951904297" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.239475097656" Y="-22.404337890625" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.093341552734" Y="-22.106287109375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.312817871094" Y="-22.44026953125" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.892119628906" Y="-22.94383203125" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.27937109375" Y="-23.41616796875" />
                  <Point X="3.248631103516" Y="-23.45626953125" />
                  <Point X="3.221588867188" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.201668945312" Y="-23.5494453125" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.200179199219" Y="-23.654591796875" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.241212890625" Y="-23.750904296875" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280947021484" Y="-23.8011796875" />
                  <Point X="3.317996337891" Y="-23.82203515625" />
                  <Point X="3.350589111328" Y="-23.8403828125" />
                  <Point X="3.368565429688" Y="-23.846380859375" />
                  <Point X="3.418658447266" Y="-23.853" />
                  <Point X="3.462726074219" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.139275878906" Y="-23.77148046875" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.892266113281" Y="-23.855873046875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.973923828125" Y="-24.27171484375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.432795898438" Y="-24.5768515625" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729088378906" Y="-24.767181640625" />
                  <Point X="3.679873535156" Y="-24.79562890625" />
                  <Point X="3.636578369141" Y="-24.82065234375" />
                  <Point X="3.622264648438" Y="-24.83307421875" />
                  <Point X="3.592735839844" Y="-24.870701171875" />
                  <Point X="3.566758789062" Y="-24.903802734375" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.547142089844" Y="-24.976662109375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.548326171875" Y="-25.09208203125" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.596287597656" Y="-25.196384765625" />
                  <Point X="3.622264648438" Y="-25.229486328125" />
                  <Point X="3.636576904297" Y="-25.241908203125" />
                  <Point X="3.685791992188" Y="-25.27035546875" />
                  <Point X="3.729086914062" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.349602050781" Y="-25.46341796875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.974630859375" Y="-25.792626953125" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.903930175781" Y="-26.161412109375" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.214076171875" Y="-26.2032265625" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836181641" Y="-26.098341796875" />
                  <Point X="3.298244628906" Y="-26.1193359375" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.185445068359" Y="-26.154697265625" />
                  <Point X="3.127061523438" Y="-26.2249140625" />
                  <Point X="3.075700927734" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.055990234375" Y="-26.405005859375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.109815673828" Y="-26.599767578125" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.733091552734" Y="-27.118798828125" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.277985351562" Y="-27.68263671875" />
                  <Point X="4.204130859375" Y="-27.80214453125" />
                  <Point X="4.112099609375" Y="-27.932908203125" />
                  <Point X="4.056688476562" Y="-28.011638671875" />
                  <Point X="3.467827636719" Y="-27.67166015625" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.622381347656" Y="-27.232552734375" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.393574707031" Y="-27.2695078125" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.238337402344" Y="-27.430185546875" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.209924560547" Y="-27.661333984375" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.596923583984" Y="-28.4070234375" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.922932373047" Y="-29.1276171875" />
                  <Point X="2.835294433594" Y="-29.19021484375" />
                  <Point X="2.732400390625" Y="-29.25681640625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="2.228310058594" Y="-28.70251953125" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549194336" Y="-27.980466796875" />
                  <Point X="1.557168457031" Y="-27.90757421875" />
                  <Point X="1.45742578125" Y="-27.84344921875" />
                  <Point X="1.425805297852" Y="-27.83571875" />
                  <Point X="1.301804077148" Y="-27.84712890625" />
                  <Point X="1.192718383789" Y="-27.857166015625" />
                  <Point X="1.165332763672" Y="-27.8685078125" />
                  <Point X="1.06958215332" Y="-27.948123046875" />
                  <Point X="0.985349060059" Y="-28.01816015625" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.939827819824" Y="-28.177703125" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.016753356934" Y="-29.091744140625" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.077253173828" Y="-29.94507421875" />
                  <Point X="0.99436505127" Y="-29.9632421875" />
                  <Point X="0.899288208008" Y="-29.980515625" />
                  <Point X="0.860200561523" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#170" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.102805347483" Y="4.73879927325" Z="1.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.35" />
                  <Point X="-0.563304405554" Y="5.033012932527" Z="1.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.35" />
                  <Point X="-1.342716655857" Y="4.883199221201" Z="1.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.35" />
                  <Point X="-1.727712534092" Y="4.595601985722" Z="1.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.35" />
                  <Point X="-1.72275271033" Y="4.395267975064" Z="1.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.35" />
                  <Point X="-1.786338922581" Y="4.321578722722" Z="1.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.35" />
                  <Point X="-1.883660584239" Y="4.322922010621" Z="1.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.35" />
                  <Point X="-2.040700872544" Y="4.487935890493" Z="1.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.35" />
                  <Point X="-2.439541199834" Y="4.440312298846" Z="1.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.35" />
                  <Point X="-3.063654017963" Y="4.035065168983" Z="1.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.35" />
                  <Point X="-3.17802982978" Y="3.446028767819" Z="1.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.35" />
                  <Point X="-2.998021829762" Y="3.100275476023" Z="1.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.35" />
                  <Point X="-3.022458869146" Y="3.02634469874" Z="1.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.35" />
                  <Point X="-3.094800989583" Y="2.997542869411" Z="1.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.35" />
                  <Point X="-3.487830580478" Y="3.202164118966" Z="1.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.35" />
                  <Point X="-3.987360446131" Y="3.129548652619" Z="1.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.35" />
                  <Point X="-4.366786822006" Y="2.573768859208" Z="1.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.35" />
                  <Point X="-4.094876625441" Y="1.916471426538" Z="1.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.35" />
                  <Point X="-3.682643920398" Y="1.584097178585" Z="1.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.35" />
                  <Point X="-3.678357548279" Y="1.525856148576" Z="1.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.35" />
                  <Point X="-3.720217593258" Y="1.485136008938" Z="1.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.35" />
                  <Point X="-4.318726998419" Y="1.549325612174" Z="1.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.35" />
                  <Point X="-4.889661232072" Y="1.344855634826" Z="1.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.35" />
                  <Point X="-5.013779375529" Y="0.761207788538" Z="1.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.35" />
                  <Point X="-4.270969201986" Y="0.235135222693" Z="1.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.35" />
                  <Point X="-3.563572280561" Y="0.040054302478" Z="1.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.35" />
                  <Point X="-3.544478299986" Y="0.015857180852" Z="1.35" />
                  <Point X="-3.539556741714" Y="0" Z="1.35" />
                  <Point X="-3.543886301149" Y="-0.013949770209" Z="1.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.35" />
                  <Point X="-3.561796314562" Y="-0.038821680656" Z="1.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.35" />
                  <Point X="-4.365918586786" Y="-0.260576834063" Z="1.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.35" />
                  <Point X="-5.023979373959" Y="-0.700782163225" Z="1.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.35" />
                  <Point X="-4.919126259509" Y="-1.238409788058" Z="1.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.35" />
                  <Point X="-3.980949679484" Y="-1.407154949071" Z="1.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.35" />
                  <Point X="-3.206764935393" Y="-1.31415784904" Z="1.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.35" />
                  <Point X="-3.19628204035" Y="-1.336371687942" Z="1.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.35" />
                  <Point X="-3.893316217205" Y="-1.883905192708" Z="1.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.35" />
                  <Point X="-4.365520048739" Y="-2.582021936355" Z="1.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.35" />
                  <Point X="-4.046903228686" Y="-3.057315111949" Z="1.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.35" />
                  <Point X="-3.176283476995" Y="-2.903889509186" Z="1.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.35" />
                  <Point X="-2.564720319296" Y="-2.563610149835" Z="1.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.35" />
                  <Point X="-2.951527503982" Y="-3.258794905497" Z="1.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.35" />
                  <Point X="-3.108301585304" Y="-4.009783333633" Z="1.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.35" />
                  <Point X="-2.684854354761" Y="-4.304817669649" Z="1.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.35" />
                  <Point X="-2.331474243682" Y="-4.293619164938" Z="1.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.35" />
                  <Point X="-2.105493044099" Y="-4.075783366075" Z="1.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.35" />
                  <Point X="-1.823367059259" Y="-3.993580900763" Z="1.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.35" />
                  <Point X="-1.549499845498" Y="-4.100112957746" Z="1.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.35" />
                  <Point X="-1.397078897604" Y="-4.351350228926" Z="1.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.35" />
                  <Point X="-1.390531659925" Y="-4.70808697415" Z="1.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.35" />
                  <Point X="-1.274711593401" Y="-4.915109139864" Z="1.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.35" />
                  <Point X="-0.97712526191" Y="-4.982811750118" Z="1.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.35" />
                  <Point X="-0.604560402486" Y="-4.218434212858" Z="1.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.35" />
                  <Point X="-0.340461572849" Y="-3.408370763891" Z="1.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.35" />
                  <Point X="-0.134786034766" Y="-3.246071979561" Z="1.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.35" />
                  <Point X="0.118573044595" Y="-3.241040065727" Z="1.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.35" />
                  <Point X="0.329984286584" Y="-3.393274991876" Z="1.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.35" />
                  <Point X="0.630194457915" Y="-4.314101816958" Z="1.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.35" />
                  <Point X="0.9020688815" Y="-4.998430405642" Z="1.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.35" />
                  <Point X="1.081802899966" Y="-4.962634056388" Z="1.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.35" />
                  <Point X="1.060169612329" Y="-4.053938120299" Z="1.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.35" />
                  <Point X="0.982531055887" Y="-3.157041686981" Z="1.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.35" />
                  <Point X="1.09539311124" Y="-2.955288957078" Z="1.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.35" />
                  <Point X="1.300229244985" Y="-2.865637293466" Z="1.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.35" />
                  <Point X="1.523973063987" Y="-2.918351870061" Z="1.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.35" />
                  <Point X="2.182486426722" Y="-3.701675668991" Z="1.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.35" />
                  <Point X="2.753413630783" Y="-4.267510547051" Z="1.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.35" />
                  <Point X="2.945838185923" Y="-4.137024367446" Z="1.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.35" />
                  <Point X="2.634068973584" Y="-3.350741951222" Z="1.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.35" />
                  <Point X="2.252972676485" Y="-2.621167446835" Z="1.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.35" />
                  <Point X="2.276427695506" Y="-2.422193059356" Z="1.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.35" />
                  <Point X="2.410705319016" Y="-2.282473614461" Z="1.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.35" />
                  <Point X="2.607339356677" Y="-2.250475333006" Z="1.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.35" />
                  <Point X="3.436671645885" Y="-2.683680526691" Z="1.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.35" />
                  <Point X="4.146831866609" Y="-2.930404156577" Z="1.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.35" />
                  <Point X="4.314362464013" Y="-2.677640604754" Z="1.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.35" />
                  <Point X="3.757373522278" Y="-2.047849459645" Z="1.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.35" />
                  <Point X="3.145717640908" Y="-1.541448446371" Z="1.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.35" />
                  <Point X="3.099623717119" Y="-1.378306461608" Z="1.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.35" />
                  <Point X="3.15935211385" Y="-1.225601306176" Z="1.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.35" />
                  <Point X="3.30270834925" Y="-1.136914988913" Z="1.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.35" />
                  <Point X="4.201393996645" Y="-1.22151811288" Z="1.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.35" />
                  <Point X="4.946521083687" Y="-1.14125654214" Z="1.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.35" />
                  <Point X="5.017916520204" Y="-0.768798931806" Z="1.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.35" />
                  <Point X="4.356387019256" Y="-0.383840113975" Z="1.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.35" />
                  <Point X="3.704657663221" Y="-0.195785329771" Z="1.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.35" />
                  <Point X="3.629465692827" Y="-0.134237377034" Z="1.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.35" />
                  <Point X="3.591277716421" Y="-0.051396423037" Z="1.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.35" />
                  <Point X="3.590093734003" Y="0.045214108171" Z="1.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.35" />
                  <Point X="3.625913745573" Y="0.129711361541" Z="1.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.35" />
                  <Point X="3.698737751131" Y="0.192363514471" Z="1.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.35" />
                  <Point X="4.43958079805" Y="0.406131818794" Z="1.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.35" />
                  <Point X="5.017172613612" Y="0.767257664548" Z="1.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.35" />
                  <Point X="4.93469125418" Y="1.18723438052" Z="1.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.35" />
                  <Point X="4.126593760117" Y="1.309371901446" Z="1.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.35" />
                  <Point X="3.419053792065" Y="1.227848156233" Z="1.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.35" />
                  <Point X="3.336367858012" Y="1.252815480718" Z="1.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.35" />
                  <Point X="3.276827426919" Y="1.307856552542" Z="1.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.35" />
                  <Point X="3.242991852003" Y="1.386792891309" Z="1.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.35" />
                  <Point X="3.243665327717" Y="1.468368892274" Z="1.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.35" />
                  <Point X="3.282158269935" Y="1.544592493117" Z="1.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.35" />
                  <Point X="3.916401800489" Y="2.047779716284" Z="1.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.35" />
                  <Point X="4.349439197939" Y="2.616896967672" Z="1.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.35" />
                  <Point X="4.127771171594" Y="2.954196143936" Z="1.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.35" />
                  <Point X="3.208319140857" Y="2.670243971639" Z="1.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.35" />
                  <Point X="2.472303632956" Y="2.256950913952" Z="1.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.35" />
                  <Point X="2.397100544908" Y="2.24944700097" Z="1.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.35" />
                  <Point X="2.330538060356" Y="2.27400482748" Z="1.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.35" />
                  <Point X="2.276753772755" Y="2.326486800026" Z="1.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.35" />
                  <Point X="2.249982701789" Y="2.392657898924" Z="1.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.35" />
                  <Point X="2.255577028706" Y="2.467165942926" Z="1.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.35" />
                  <Point X="2.725381381221" Y="3.303819941098" Z="1.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.35" />
                  <Point X="2.953064857973" Y="4.127110962929" Z="1.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.35" />
                  <Point X="2.567356240633" Y="4.377478527703" Z="1.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.35" />
                  <Point X="2.163070769038" Y="4.590868983551" Z="1.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.35" />
                  <Point X="1.744056127644" Y="4.765838896284" Z="1.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.35" />
                  <Point X="1.210579276667" Y="4.922205065431" Z="1.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.35" />
                  <Point X="0.549317033713" Y="5.039032734664" Z="1.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.35" />
                  <Point X="0.09043921407" Y="4.692648232701" Z="1.35" />
                  <Point X="0" Y="4.355124473572" Z="1.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>