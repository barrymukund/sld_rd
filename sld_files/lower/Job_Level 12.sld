<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#139" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1007" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.65024230957" Y="-28.836990234375" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542362548828" Y="-28.467375" />
                  <Point X="0.506827850342" Y="-28.416177734375" />
                  <Point X="0.378634918213" Y="-28.231474609375" />
                  <Point X="0.356748718262" Y="-28.209017578125" />
                  <Point X="0.330493682861" Y="-28.189775390625" />
                  <Point X="0.302492889404" Y="-28.17566796875" />
                  <Point X="0.247505004883" Y="-28.158603515625" />
                  <Point X="0.049133758545" Y="-28.09703515625" />
                  <Point X="0.020983165741" Y="-28.092767578125" />
                  <Point X="-0.008658312798" Y="-28.092765625" />
                  <Point X="-0.036824390411" Y="-28.09703515625" />
                  <Point X="-0.091812286377" Y="-28.1141015625" />
                  <Point X="-0.290183532715" Y="-28.17566796875" />
                  <Point X="-0.31818460083" Y="-28.189775390625" />
                  <Point X="-0.344440093994" Y="-28.20901953125" />
                  <Point X="-0.366323730469" Y="-28.2314765625" />
                  <Point X="-0.401858459473" Y="-28.28267578125" />
                  <Point X="-0.530051391602" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.864630981445" Y="-29.683048828125" />
                  <Point X="-0.916584777832" Y="-29.87694140625" />
                  <Point X="-1.079314086914" Y="-29.84535546875" />
                  <Point X="-1.139292114258" Y="-29.829923828125" />
                  <Point X="-1.246417602539" Y="-29.802361328125" />
                  <Point X="-1.225428588867" Y="-29.64293359375" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.229645141602" Y="-29.45018359375" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323645751953" Y="-29.131203125" />
                  <Point X="-1.374271484375" Y="-29.086806640625" />
                  <Point X="-1.556906616211" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886474609" Y="-28.897994140625" />
                  <Point X="-1.643027099609" Y="-28.890966796875" />
                  <Point X="-1.710218994141" Y="-28.8865625" />
                  <Point X="-1.952616210938" Y="-28.87067578125" />
                  <Point X="-1.983414916992" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042657836914" Y="-28.89480078125" />
                  <Point X="-2.098645751953" Y="-28.9322109375" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312788574219" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.480148681641" Y="-29.288490234375" />
                  <Point X="-2.801711669922" Y="-29.08938671875" />
                  <Point X="-2.884723144531" Y="-29.025470703125" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.585330078125" Y="-27.95646484375" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858154297" Y="-27.6476484375" />
                  <Point X="-2.406587890625" Y="-27.6161171875" />
                  <Point X="-2.405577392578" Y="-27.585177734375" />
                  <Point X="-2.414567138672" Y="-27.555556640625" />
                  <Point X="-2.428791259766" Y="-27.526724609375" />
                  <Point X="-2.446816162109" Y="-27.501576171875" />
                  <Point X="-2.464156005859" Y="-27.48423828125" />
                  <Point X="-2.489305908203" Y="-27.466216796875" />
                  <Point X="-2.518135253906" Y="-27.451998046875" />
                  <Point X="-2.547755126953" Y="-27.443013671875" />
                  <Point X="-2.57869140625" Y="-27.444025390625" />
                  <Point X="-2.610218261719" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.648737548828" Y="-28.04406640625" />
                  <Point X="-3.818023925781" Y="-28.141802734375" />
                  <Point X="-4.082861083984" Y="-27.793859375" />
                  <Point X="-4.142372558594" Y="-27.694068359375" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.388647460938" Y="-26.71543359375" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083064941406" Y="-26.4758828125" />
                  <Point X="-3.064015625" Y="-26.445568359375" />
                  <Point X="-3.055629882812" Y="-26.42404296875" />
                  <Point X="-3.052184570312" Y="-26.41337890625" />
                  <Point X="-3.046152099609" Y="-26.390087890625" />
                  <Point X="-3.042037597656" Y="-26.358603515625" />
                  <Point X="-3.042736816406" Y="-26.335736328125" />
                  <Point X="-3.0488828125" Y="-26.31369921875" />
                  <Point X="-3.060888183594" Y="-26.28471484375" />
                  <Point X="-3.073480712891" Y="-26.262986328125" />
                  <Point X="-3.091326171875" Y="-26.245318359375" />
                  <Point X="-3.109576416016" Y="-26.231505859375" />
                  <Point X="-3.118722412109" Y="-26.2253828125" />
                  <Point X="-3.139457275391" Y="-26.2131796875" />
                  <Point X="-3.168721923828" Y="-26.201955078125" />
                  <Point X="-3.200608886719" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-4.506395507812" Y="-26.362169921875" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.849822753906" Y="-25.88256640625" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.854905761719" Y="-25.306697265625" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.513108154297" Y="-25.21265625" />
                  <Point X="-3.491515869141" Y="-25.201255859375" />
                  <Point X="-3.481703125" Y="-25.1952890625" />
                  <Point X="-3.459973632812" Y="-25.18020703125" />
                  <Point X="-3.43601953125" Y="-25.158681640625" />
                  <Point X="-3.415523193359" Y="-25.129255859375" />
                  <Point X="-3.406090332031" Y="-25.108048828125" />
                  <Point X="-3.402160400391" Y="-25.097599609375" />
                  <Point X="-3.394917236328" Y="-25.07426171875" />
                  <Point X="-3.389474365234" Y="-25.0455234375" />
                  <Point X="-3.390242919922" Y="-25.0126640625" />
                  <Point X="-3.394497802734" Y="-24.991373046875" />
                  <Point X="-3.396925048828" Y="-24.981830078125" />
                  <Point X="-3.404168212891" Y="-24.9584921875" />
                  <Point X="-3.417486816406" Y="-24.929166015625" />
                  <Point X="-3.43957421875" Y="-24.900587890625" />
                  <Point X="-3.457413818359" Y="-24.884892578125" />
                  <Point X="-3.465997070312" Y="-24.878173828125" />
                  <Point X="-3.4877265625" Y="-24.863091796875" />
                  <Point X="-3.501924072266" Y="-24.854953125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.694612304688" Y="-24.530865234375" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.891743164062" Y="-24.47753125" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.792791992188" Y="-23.90605859375" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-3.996505126953" Y="-23.66981640625" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985107422" Y="-23.700658203125" />
                  <Point X="-3.723423583984" Y="-23.698771484375" />
                  <Point X="-3.703139648438" Y="-23.694736328125" />
                  <Point X="-3.689808105469" Y="-23.690533203125" />
                  <Point X="-3.641713623047" Y="-23.675369140625" />
                  <Point X="-3.622784667969" Y="-23.667041015625" />
                  <Point X="-3.604040283203" Y="-23.656220703125" />
                  <Point X="-3.587355224609" Y="-23.64398828125" />
                  <Point X="-3.573714111328" Y="-23.62843359375" />
                  <Point X="-3.561299560547" Y="-23.610703125" />
                  <Point X="-3.551351806641" Y="-23.5925703125" />
                  <Point X="-3.546002441406" Y="-23.57965625" />
                  <Point X="-3.526704345703" Y="-23.53306640625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.532051025391" Y="-23.41062109375" />
                  <Point X="-3.538505615234" Y="-23.39822265625" />
                  <Point X="-3.561790771484" Y="-23.3534921875" />
                  <Point X="-3.573281982422" Y="-23.33629296875" />
                  <Point X="-3.587194580078" Y="-23.319712890625" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-4.268510742188" Y="-22.79408203125" />
                  <Point X="-4.351860839844" Y="-22.730125" />
                  <Point X="-4.342482421875" Y="-22.71405859375" />
                  <Point X="-4.081155761719" Y="-22.266345703125" />
                  <Point X="-3.997194335938" Y="-22.158423828125" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.348431884766" Y="-22.073474609375" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187728515625" Y="-22.163658203125" />
                  <Point X="-3.167085693359" Y="-22.17016796875" />
                  <Point X="-3.146796875" Y="-22.174205078125" />
                  <Point X="-3.128229492188" Y="-22.175830078125" />
                  <Point X="-3.061247558594" Y="-22.181689453125" />
                  <Point X="-3.040559814453" Y="-22.18123828125" />
                  <Point X="-3.019101806641" Y="-22.178412109375" />
                  <Point X="-2.999012695312" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946076660156" Y="-22.13976953125" />
                  <Point X="-2.932897460938" Y="-22.12658984375" />
                  <Point X="-2.885353027344" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.845060302734" Y="-21.9453125" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-3.165086181641" Y="-21.307009765625" />
                  <Point X="-3.183333740234" Y="-21.275404296875" />
                  <Point X="-3.155773681641" Y="-21.2542734375" />
                  <Point X="-2.700627685547" Y="-20.90531640625" />
                  <Point X="-2.568395751953" Y="-20.831853515625" />
                  <Point X="-2.167037109375" Y="-20.6088671875" />
                  <Point X="-2.086596679688" Y="-20.71369921875" />
                  <Point X="-2.043194824219" Y="-20.770259765625" />
                  <Point X="-2.028887817383" Y="-20.785205078125" />
                  <Point X="-2.012308959961" Y="-20.799115234375" />
                  <Point X="-1.995114013672" Y="-20.810603515625" />
                  <Point X="-1.974448730469" Y="-20.821361328125" />
                  <Point X="-1.899897949219" Y="-20.860171875" />
                  <Point X="-1.880625366211" Y="-20.86766796875" />
                  <Point X="-1.859718383789" Y="-20.873271484375" />
                  <Point X="-1.839268798828" Y="-20.876419921875" />
                  <Point X="-1.818622680664" Y="-20.87506640625" />
                  <Point X="-1.797306884766" Y="-20.871306640625" />
                  <Point X="-1.777452026367" Y="-20.865517578125" />
                  <Point X="-1.755927978516" Y="-20.8566015625" />
                  <Point X="-1.678278076172" Y="-20.8244375" />
                  <Point X="-1.660145996094" Y="-20.814490234375" />
                  <Point X="-1.642416381836" Y="-20.802076171875" />
                  <Point X="-1.626864135742" Y="-20.7884375" />
                  <Point X="-1.61463269043" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734080078125" />
                  <Point X="-1.588474731445" Y="-20.711861328125" />
                  <Point X="-1.563201171875" Y="-20.631703125" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.538852294922" Y="-20.355388671875" />
                  <Point X="-0.949622619629" Y="-20.19019140625" />
                  <Point X="-0.789337768555" Y="-20.1714296875" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.175844223022" Y="-20.55716015625" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113975525" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425926208" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.297518890381" Y="-20.14901171875" />
                  <Point X="0.307419555664" Y="-20.1120625" />
                  <Point X="0.329561981201" Y="-20.114380859375" />
                  <Point X="0.844032348633" Y="-20.168259765625" />
                  <Point X="0.976665161133" Y="-20.20028125" />
                  <Point X="1.481037475586" Y="-20.3220546875" />
                  <Point X="1.566190673828" Y="-20.352939453125" />
                  <Point X="1.894645141602" Y="-20.472072265625" />
                  <Point X="1.97812109375" Y="-20.511111328125" />
                  <Point X="2.294564941406" Y="-20.6591015625" />
                  <Point X="2.375249511719" Y="-20.706107421875" />
                  <Point X="2.680984130859" Y="-20.884228515625" />
                  <Point X="2.757036132812" Y="-20.9383125" />
                  <Point X="2.943260498047" Y="-21.07074609375" />
                  <Point X="2.334584716797" Y="-22.125001953125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.139568847656" Y="-22.46684375" />
                  <Point X="2.129907714844" Y="-22.496353515625" />
                  <Point X="2.128417236328" Y="-22.501369140625" />
                  <Point X="2.111607177734" Y="-22.56423046875" />
                  <Point X="2.108391845703" Y="-22.59011328125" />
                  <Point X="2.10887109375" Y="-22.62408203125" />
                  <Point X="2.109544677734" Y="-22.634115234375" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.140071289062" Y="-22.752529296875" />
                  <Point X="2.149394775391" Y="-22.76626953125" />
                  <Point X="2.183029052734" Y="-22.815837890625" />
                  <Point X="2.200676757812" Y="-22.83535546875" />
                  <Point X="2.227716796875" Y="-22.85798046875" />
                  <Point X="2.235338378906" Y="-22.863732421875" />
                  <Point X="2.284906738281" Y="-22.8973671875" />
                  <Point X="2.304953857422" Y="-22.90773046875" />
                  <Point X="2.327042480469" Y="-22.91599609375" />
                  <Point X="2.348967041016" Y="-22.921337890625" />
                  <Point X="2.364034667969" Y="-22.923154296875" />
                  <Point X="2.418391845703" Y="-22.929708984375" />
                  <Point X="2.445237304688" Y="-22.929123046875" />
                  <Point X="2.481565429688" Y="-22.923125" />
                  <Point X="2.490632568359" Y="-22.921169921875" />
                  <Point X="2.553493896484" Y="-22.904359375" />
                  <Point X="2.565288085938" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.757017089844" Y="-22.21523046875" />
                  <Point X="3.967325927734" Y="-22.09380859375" />
                  <Point X="4.123276855469" Y="-22.310544921875" />
                  <Point X="4.165673828125" Y="-22.380607421875" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.476991943359" Y="-23.142626953125" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221423339844" Y="-23.339759765625" />
                  <Point X="3.203975341797" Y="-23.35837109375" />
                  <Point X="3.191434570312" Y="-23.37473046875" />
                  <Point X="3.146193115234" Y="-23.433751953125" />
                  <Point X="3.133095703125" Y="-23.45699609375" />
                  <Point X="3.119954833984" Y="-23.490650390625" />
                  <Point X="3.116958251953" Y="-23.4996171875" />
                  <Point X="3.100105712891" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739257812" Y="-23.628232421875" />
                  <Point X="3.101573974609" Y="-23.646818359375" />
                  <Point X="3.115408203125" Y="-23.713865234375" />
                  <Point X="3.124524902344" Y="-23.739185546875" />
                  <Point X="3.142153320312" Y="-23.77241796875" />
                  <Point X="3.146713134766" Y="-23.780115234375" />
                  <Point X="3.184340576172" Y="-23.837306640625" />
                  <Point X="3.198895996094" Y="-23.854552734375" />
                  <Point X="3.216138916016" Y="-23.870640625" />
                  <Point X="3.234348388672" Y="-23.883966796875" />
                  <Point X="3.249463134766" Y="-23.892474609375" />
                  <Point X="3.303990478516" Y="-23.923169921875" />
                  <Point X="3.320521972656" Y="-23.9305" />
                  <Point X="3.356117919922" Y="-23.9405625" />
                  <Point X="3.376553955078" Y="-23.943263671875" />
                  <Point X="3.450278564453" Y="-23.953005859375" />
                  <Point X="3.462698730469" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.598184570312" Y="-23.806884765625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.859297851562" Y="-24.153013671875" />
                  <Point X="4.890864257812" Y="-24.355759765625" />
                  <Point X="3.998739257813" Y="-24.5948046875" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704785888672" Y="-24.674416015625" />
                  <Point X="3.681546386719" Y="-24.68493359375" />
                  <Point X="3.661468505859" Y="-24.6965390625" />
                  <Point X="3.589036376953" Y="-24.738404296875" />
                  <Point X="3.567673828125" Y="-24.755251953125" />
                  <Point X="3.541314453125" Y="-24.7830234375" />
                  <Point X="3.535484619141" Y="-24.7897734375" />
                  <Point X="3.492025390625" Y="-24.845150390625" />
                  <Point X="3.480300537109" Y="-24.864431640625" />
                  <Point X="3.470526611328" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.459665283203" Y="-24.928365234375" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443483154297" Y="-25.021876953125" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.449194580078" Y="-25.079521484375" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526611328" Y="-25.1766640625" />
                  <Point X="3.480300537109" Y="-25.19812890625" />
                  <Point X="3.492022949219" Y="-25.21740625" />
                  <Point X="3.504069580078" Y="-25.2327578125" />
                  <Point X="3.547528808594" Y="-25.288134765625" />
                  <Point X="3.559999755859" Y="-25.30123828125" />
                  <Point X="3.589035400391" Y="-25.32415625" />
                  <Point X="3.60911328125" Y="-25.33576171875" />
                  <Point X="3.681545410156" Y="-25.37762890625" />
                  <Point X="3.692708496094" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.734482421875" Y="-25.6648984375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.855021484375" Y="-25.948732421875" />
                  <Point X="4.837903320312" Y="-26.02374609375" />
                  <Point X="4.801174316406" Y="-26.18469921875" />
                  <Point X="3.751607910156" Y="-26.046521484375" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659912109" Y="-26.005509765625" />
                  <Point X="3.335254150391" Y="-26.01407421875" />
                  <Point X="3.193095703125" Y="-26.04497265625" />
                  <Point X="3.163973632812" Y="-26.05659765625" />
                  <Point X="3.136146972656" Y="-26.073490234375" />
                  <Point X="3.112396972656" Y="-26.0939609375" />
                  <Point X="3.088578613281" Y="-26.122607421875" />
                  <Point X="3.002652832031" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.96634375" Y="-26.342462890625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450927734" Y="-26.567998046875" />
                  <Point X="2.998258789062" Y="-26.60191796875" />
                  <Point X="3.076931152344" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="4.055248291016" Y="-27.4857421875" />
                  <Point X="4.213122558594" Y="-27.606884765625" />
                  <Point X="4.124817382812" Y="-27.749775390625" />
                  <Point X="4.089405761719" Y="-27.80008984375" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="3.092393310547" Y="-27.34520703125" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754225830078" Y="-27.159826171875" />
                  <Point X="2.707326660156" Y="-27.15135546875" />
                  <Point X="2.538135498047" Y="-27.12080078125" />
                  <Point X="2.506783935547" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.444835449219" Y="-27.135177734375" />
                  <Point X="2.405873535156" Y="-27.155681640625" />
                  <Point X="2.265317382812" Y="-27.22965625" />
                  <Point X="2.242385009766" Y="-27.246548828125" />
                  <Point X="2.221425537109" Y="-27.2675078125" />
                  <Point X="2.204532226563" Y="-27.2904375" />
                  <Point X="2.184026855469" Y="-27.3293984375" />
                  <Point X="2.110053222656" Y="-27.469955078125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.104145263672" Y="-27.610158203125" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.758832519531" Y="-28.87745703125" />
                  <Point X="2.861283935547" Y="-29.05490625" />
                  <Point X="2.781853271484" Y="-29.111640625" />
                  <Point X="2.742266845703" Y="-29.137265625" />
                  <Point X="2.701764160156" Y="-29.163482421875" />
                  <Point X="1.980940673828" Y="-28.2240859375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721924316406" Y="-27.900556640625" />
                  <Point X="1.675668945312" Y="-27.870818359375" />
                  <Point X="1.50880078125" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365722656" Y="-27.743435546875" />
                  <Point X="1.417101318359" Y="-27.741119140625" />
                  <Point X="1.366513183594" Y="-27.7457734375" />
                  <Point X="1.184014526367" Y="-27.76256640625" />
                  <Point X="1.156362426758" Y="-27.7693984375" />
                  <Point X="1.128976928711" Y="-27.7807421875" />
                  <Point X="1.104594604492" Y="-27.795462890625" />
                  <Point X="1.065531616211" Y="-27.827943359375" />
                  <Point X="0.924610900879" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.863944824219" Y="-28.079544921875" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.991765075684" Y="-29.629763671875" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.975706237793" Y="-29.870078125" />
                  <Point X="0.939108886719" Y="-29.8767265625" />
                  <Point X="0.929315917969" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058409423828" Y="-29.752640625" />
                  <Point X="-1.115620727539" Y="-29.737919921875" />
                  <Point X="-1.14124597168" Y="-29.731326171875" />
                  <Point X="-1.131241333008" Y="-29.655333984375" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.541037109375" />
                  <Point X="-1.121759155273" Y="-29.50933203125" />
                  <Point X="-1.123333740234" Y="-29.497693359375" />
                  <Point X="-1.136470458984" Y="-29.431650390625" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026367188" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230574707031" Y="-29.094861328125" />
                  <Point X="-1.250210693359" Y="-29.070935546875" />
                  <Point X="-1.261008789063" Y="-29.05977734375" />
                  <Point X="-1.311634521484" Y="-29.015380859375" />
                  <Point X="-1.49426965332" Y="-28.855212890625" />
                  <Point X="-1.506739746094" Y="-28.84596484375" />
                  <Point X="-1.533022338867" Y="-28.82962109375" />
                  <Point X="-1.546834838867" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591315551758" Y="-28.805474609375" />
                  <Point X="-1.621456176758" Y="-28.798447265625" />
                  <Point X="-1.636813232422" Y="-28.796169921875" />
                  <Point X="-1.704005249023" Y="-28.791765625" />
                  <Point X="-1.94640246582" Y="-28.77587890625" />
                  <Point X="-1.961927246094" Y="-28.7761328125" />
                  <Point X="-1.992725952148" Y="-28.779166015625" />
                  <Point X="-2.00799987793" Y="-28.7819453125" />
                  <Point X="-2.039048339844" Y="-28.790263671875" />
                  <Point X="-2.053667236328" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095437255859" Y="-28.815810546875" />
                  <Point X="-2.151425048828" Y="-28.853220703125" />
                  <Point X="-2.353403320312" Y="-28.988177734375" />
                  <Point X="-2.359688476562" Y="-28.99276171875" />
                  <Point X="-2.380446533203" Y="-29.010134765625" />
                  <Point X="-2.402760986328" Y="-29.0327734375" />
                  <Point X="-2.410471679688" Y="-29.041630859375" />
                  <Point X="-2.503201904297" Y="-29.162478515625" />
                  <Point X="-2.747586669922" Y="-29.011162109375" />
                  <Point X="-2.826765869141" Y="-28.950197265625" />
                  <Point X="-2.980862548828" Y="-28.831548828125" />
                  <Point X="-2.503057617188" Y="-28.00396484375" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849365234" Y="-27.710080078125" />
                  <Point X="-2.323946289062" Y="-27.681109375" />
                  <Point X="-2.319682617188" Y="-27.666177734375" />
                  <Point X="-2.313412353516" Y="-27.634646484375" />
                  <Point X="-2.311638427734" Y="-27.61921875" />
                  <Point X="-2.310627929688" Y="-27.588279296875" />
                  <Point X="-2.314671630859" Y="-27.557587890625" />
                  <Point X="-2.323661376953" Y="-27.527966796875" />
                  <Point X="-2.329370849609" Y="-27.513525390625" />
                  <Point X="-2.343594970703" Y="-27.484693359375" />
                  <Point X="-2.351576416016" Y="-27.471380859375" />
                  <Point X="-2.369601318359" Y="-27.446232421875" />
                  <Point X="-2.379644775391" Y="-27.434396484375" />
                  <Point X="-2.396984619141" Y="-27.41705859375" />
                  <Point X="-2.408822021484" Y="-27.407017578125" />
                  <Point X="-2.433971923828" Y="-27.38899609375" />
                  <Point X="-2.447284423828" Y="-27.381015625" />
                  <Point X="-2.476113769531" Y="-27.366796875" />
                  <Point X="-2.490560302734" Y="-27.361087890625" />
                  <Point X="-2.520180175781" Y="-27.352103515625" />
                  <Point X="-2.550860351562" Y="-27.348064453125" />
                  <Point X="-2.581796630859" Y="-27.349076171875" />
                  <Point X="-2.597226074219" Y="-27.3508515625" />
                  <Point X="-2.628752929688" Y="-27.357123046875" />
                  <Point X="-2.64368359375" Y="-27.36138671875" />
                  <Point X="-2.672648925781" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.696237548828" Y="-27.961794921875" />
                  <Point X="-3.793088378906" Y="-28.0177109375" />
                  <Point X="-4.004015869141" Y="-27.74059375" />
                  <Point X="-4.060780029297" Y="-27.64541015625" />
                  <Point X="-4.181265625" Y="-27.443375" />
                  <Point X="-3.330815185547" Y="-26.790802734375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039159423828" Y="-26.566068359375" />
                  <Point X="-3.016269775391" Y="-26.543435546875" />
                  <Point X="-3.002627929688" Y="-26.526427734375" />
                  <Point X="-2.983578613281" Y="-26.49611328125" />
                  <Point X="-2.975495605469" Y="-26.480052734375" />
                  <Point X="-2.967109863281" Y="-26.45852734375" />
                  <Point X="-2.960219238281" Y="-26.43719921875" />
                  <Point X="-2.954186767578" Y="-26.413908203125" />
                  <Point X="-2.951953125" Y="-26.4023984375" />
                  <Point X="-2.947838623047" Y="-26.3709140625" />
                  <Point X="-2.94708203125" Y="-26.35569921875" />
                  <Point X="-2.94778125" Y="-26.33283203125" />
                  <Point X="-2.951229003906" Y="-26.31021484375" />
                  <Point X="-2.957375" Y="-26.288177734375" />
                  <Point X="-2.961113769531" Y="-26.277345703125" />
                  <Point X="-2.973119140625" Y="-26.248361328125" />
                  <Point X="-2.978693847656" Y="-26.237080078125" />
                  <Point X="-2.991286376953" Y="-26.2153515625" />
                  <Point X="-3.006642089844" Y="-26.1954765625" />
                  <Point X="-3.024487548828" Y="-26.17780859375" />
                  <Point X="-3.033995117188" Y="-26.169568359375" />
                  <Point X="-3.052245361328" Y="-26.155755859375" />
                  <Point X="-3.070537353516" Y="-26.143509765625" />
                  <Point X="-3.091272216797" Y="-26.131306640625" />
                  <Point X="-3.105436279297" Y="-26.12448046875" />
                  <Point X="-3.134700927734" Y="-26.113255859375" />
                  <Point X="-3.149801513672" Y="-26.108857421875" />
                  <Point X="-3.181688476562" Y="-26.102376953125" />
                  <Point X="-3.197305175781" Y="-26.10053125" />
                  <Point X="-3.228625488281" Y="-26.09944140625" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-4.518795410156" Y="-26.267982421875" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.740763183594" Y="-25.97411328125" />
                  <Point X="-4.755779785156" Y="-25.8691171875" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.830317871094" Y="-25.3984609375" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.498185546875" Y="-25.308849609375" />
                  <Point X="-3.478417724609" Y="-25.301095703125" />
                  <Point X="-3.468752441406" Y="-25.296666015625" />
                  <Point X="-3.44716015625" Y="-25.285265625" />
                  <Point X="-3.427534667969" Y="-25.27333203125" />
                  <Point X="-3.405805175781" Y="-25.25825" />
                  <Point X="-3.396476318359" Y="-25.250869140625" />
                  <Point X="-3.372522216797" Y="-25.22934375" />
                  <Point X="-3.358066162109" Y="-25.21298046875" />
                  <Point X="-3.337569824219" Y="-25.1835546875" />
                  <Point X="-3.328722412109" Y="-25.167865234375" />
                  <Point X="-3.319289550781" Y="-25.146658203125" />
                  <Point X="-3.3114296875" Y="-25.125759765625" />
                  <Point X="-3.304186523438" Y="-25.102421875" />
                  <Point X="-3.301576660156" Y="-25.091939453125" />
                  <Point X="-3.296133789062" Y="-25.063201171875" />
                  <Point X="-3.294500244141" Y="-25.043302734375" />
                  <Point X="-3.295268798828" Y="-25.010443359375" />
                  <Point X="-3.297084960938" Y="-24.994046875" />
                  <Point X="-3.30133984375" Y="-24.972755859375" />
                  <Point X="-3.306194335938" Y="-24.953669921875" />
                  <Point X="-3.3134375" Y="-24.93033203125" />
                  <Point X="-3.317670654297" Y="-24.919208984375" />
                  <Point X="-3.330989257812" Y="-24.8898828125" />
                  <Point X="-3.3423203125" Y="-24.871072265625" />
                  <Point X="-3.364407714844" Y="-24.842494140625" />
                  <Point X="-3.376822509766" Y="-24.829263671875" />
                  <Point X="-3.394662109375" Y="-24.813568359375" />
                  <Point X="-3.411828613281" Y="-24.800130859375" />
                  <Point X="-3.433558105469" Y="-24.785048828125" />
                  <Point X="-3.44048046875" Y="-24.780673828125" />
                  <Point X="-3.465612548828" Y="-24.767166015625" />
                  <Point X="-3.496564453125" Y="-24.75436328125" />
                  <Point X="-3.508288330078" Y="-24.75038671875" />
                  <Point X="-4.670024414062" Y="-24.4391015625" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.701099121094" Y="-23.93090625" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.008905029297" Y="-23.76400390625" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057128906" Y="-23.795634765625" />
                  <Point X="-3.736703857422" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704888183594" Y="-23.7919453125" />
                  <Point X="-3.684604248047" Y="-23.78791015625" />
                  <Point X="-3.661242919922" Y="-23.78113671875" />
                  <Point X="-3.6131484375" Y="-23.76597265625" />
                  <Point X="-3.603455810547" Y="-23.76232421875" />
                  <Point X="-3.584526855469" Y="-23.75399609375" />
                  <Point X="-3.575290527344" Y="-23.74931640625" />
                  <Point X="-3.556546142578" Y="-23.73849609375" />
                  <Point X="-3.547870605469" Y="-23.7328359375" />
                  <Point X="-3.531185546875" Y="-23.720603515625" />
                  <Point X="-3.515930419922" Y="-23.706626953125" />
                  <Point X="-3.502289306641" Y="-23.691072265625" />
                  <Point X="-3.495893798828" Y="-23.682921875" />
                  <Point X="-3.483479248047" Y="-23.66519140625" />
                  <Point X="-3.478010009766" Y="-23.656396484375" />
                  <Point X="-3.468062255859" Y="-23.638263671875" />
                  <Point X="-3.458234375" Y="-23.61601171875" />
                  <Point X="-3.438936279297" Y="-23.569421875" />
                  <Point X="-3.435499267578" Y="-23.5596484375" />
                  <Point X="-3.429710449219" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436013427734" Y="-23.395466796875" />
                  <Point X="-3.443511474609" Y="-23.3761875" />
                  <Point X="-3.454240722656" Y="-23.35435546875" />
                  <Point X="-3.477525878906" Y="-23.309625" />
                  <Point X="-3.482799072266" Y="-23.300716796875" />
                  <Point X="-3.494290283203" Y="-23.283517578125" />
                  <Point X="-3.500508300781" Y="-23.2752265625" />
                  <Point X="-3.514420898438" Y="-23.258646484375" />
                  <Point X="-3.521502197266" Y="-23.251087890625" />
                  <Point X="-3.536443603516" Y="-23.23678515625" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-4.210678222656" Y="-22.718712890625" />
                  <Point X="-4.227614257812" Y="-22.705716796875" />
                  <Point X="-4.002293457031" Y="-22.31969140625" />
                  <Point X="-3.922213378906" Y="-22.2167578125" />
                  <Point X="-3.726337158203" Y="-21.964986328125" />
                  <Point X="-3.395931884766" Y="-22.15574609375" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244923095703" Y="-22.24228125" />
                  <Point X="-3.225994628906" Y="-22.250611328125" />
                  <Point X="-3.216300048828" Y="-22.254259765625" />
                  <Point X="-3.195657226562" Y="-22.26076953125" />
                  <Point X="-3.185625488281" Y="-22.263341796875" />
                  <Point X="-3.165336669922" Y="-22.26737890625" />
                  <Point X="-3.136512207031" Y="-22.27046875" />
                  <Point X="-3.069530273438" Y="-22.276328125" />
                  <Point X="-3.059176269531" Y="-22.276666015625" />
                  <Point X="-3.038488525391" Y="-22.27621484375" />
                  <Point X="-3.028154785156" Y="-22.27542578125" />
                  <Point X="-3.006696777344" Y="-22.272599609375" />
                  <Point X="-2.996512207031" Y="-22.2706875" />
                  <Point X="-2.976423095703" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.938449462891" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.902743896484" Y="-22.2268046875" />
                  <Point X="-2.886610839844" Y="-22.21385546875" />
                  <Point X="-2.865721191406" Y="-22.193763671875" />
                  <Point X="-2.818176757812" Y="-22.146220703125" />
                  <Point X="-2.811265869141" Y="-22.13851171875" />
                  <Point X="-2.798321777344" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.750421875" Y="-21.937033203125" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-3.059388427734" Y="-21.300083984375" />
                  <Point X="-2.648377441406" Y="-20.98496484375" />
                  <Point X="-2.522259277344" Y="-20.9148984375" />
                  <Point X="-2.192524902344" Y="-20.731705078125" />
                  <Point X="-2.161965332031" Y="-20.77153125" />
                  <Point X="-2.118563476562" Y="-20.828091796875" />
                  <Point X="-2.111819335938" Y="-20.835953125" />
                  <Point X="-2.097512451172" Y="-20.8508984375" />
                  <Point X="-2.089949707031" Y="-20.857982421875" />
                  <Point X="-2.073370849609" Y="-20.871892578125" />
                  <Point X="-2.065084960938" Y="-20.878107421875" />
                  <Point X="-2.047890014648" Y="-20.889595703125" />
                  <Point X="-2.03898059082" Y="-20.894869140625" />
                  <Point X="-2.018315307617" Y="-20.905626953125" />
                  <Point X="-1.943764404297" Y="-20.9444375" />
                  <Point X="-1.934335083008" Y="-20.9487109375" />
                  <Point X="-1.9150625" Y="-20.95620703125" />
                  <Point X="-1.905219360352" Y="-20.9594296875" />
                  <Point X="-1.88431237793" Y="-20.965033203125" />
                  <Point X="-1.874174316406" Y="-20.967166015625" />
                  <Point X="-1.853724731445" Y="-20.970314453125" />
                  <Point X="-1.833054199219" Y="-20.971216796875" />
                  <Point X="-1.812408081055" Y="-20.96986328125" />
                  <Point X="-1.80212109375" Y="-20.968623046875" />
                  <Point X="-1.780805297852" Y="-20.96486328125" />
                  <Point X="-1.770715087891" Y="-20.962509765625" />
                  <Point X="-1.750860229492" Y="-20.956720703125" />
                  <Point X="-1.741095458984" Y="-20.95328515625" />
                  <Point X="-1.719571411133" Y="-20.944369140625" />
                  <Point X="-1.641921508789" Y="-20.912205078125" />
                  <Point X="-1.632585205078" Y="-20.9077265625" />
                  <Point X="-1.61445324707" Y="-20.897779296875" />
                  <Point X="-1.605657226562" Y="-20.892310546875" />
                  <Point X="-1.587927612305" Y="-20.879896484375" />
                  <Point X="-1.579779296875" Y="-20.873501953125" />
                  <Point X="-1.564227050781" Y="-20.85986328125" />
                  <Point X="-1.550251831055" Y="-20.844611328125" />
                  <Point X="-1.538020385742" Y="-20.8279296875" />
                  <Point X="-1.532360229492" Y="-20.819255859375" />
                  <Point X="-1.521538330078" Y="-20.80051171875" />
                  <Point X="-1.516856567383" Y="-20.7912734375" />
                  <Point X="-1.508526245117" Y="-20.772341796875" />
                  <Point X="-1.504877563477" Y="-20.7626484375" />
                  <Point X="-1.497871826172" Y="-20.7404296875" />
                  <Point X="-1.472598144531" Y="-20.660271484375" />
                  <Point X="-1.470026489258" Y="-20.650236328125" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-0.931162658691" Y="-20.2836796875" />
                  <Point X="-0.778293212891" Y="-20.26578515625" />
                  <Point X="-0.365222686768" Y="-20.21744140625" />
                  <Point X="-0.267607116699" Y="-20.581748046875" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451187134" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.378190307617" Y="-20.214994140625" />
                  <Point X="0.827866149902" Y="-20.262087890625" />
                  <Point X="0.954369934082" Y="-20.29262890625" />
                  <Point X="1.453623046875" Y="-20.413166015625" />
                  <Point X="1.533799194336" Y="-20.44224609375" />
                  <Point X="1.858242919922" Y="-20.559923828125" />
                  <Point X="1.937876220703" Y="-20.597166015625" />
                  <Point X="2.250443603516" Y="-20.74334375" />
                  <Point X="2.327427490234" Y="-20.788193359375" />
                  <Point X="2.629438232422" Y="-20.96414453125" />
                  <Point X="2.701979736328" Y="-21.015732421875" />
                  <Point X="2.817780273438" Y="-21.098083984375" />
                  <Point X="2.252312255859" Y="-22.077501953125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.060837646484" Y="-22.4101640625" />
                  <Point X="2.049284179688" Y="-22.43728515625" />
                  <Point X="2.039622924805" Y="-22.466794921875" />
                  <Point X="2.036642089844" Y="-22.476826171875" />
                  <Point X="2.01983203125" Y="-22.5396875" />
                  <Point X="2.017331787109" Y="-22.55251953125" />
                  <Point X="2.014116455078" Y="-22.57840234375" />
                  <Point X="2.013401245117" Y="-22.591453125" />
                  <Point X="2.013880493164" Y="-22.625421875" />
                  <Point X="2.015227905273" Y="-22.64548828125" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144042969" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045318847656" Y="-22.776111328125" />
                  <Point X="2.055681640625" Y="-22.79615625" />
                  <Point X="2.070783935547" Y="-22.819611328125" />
                  <Point X="2.104418212891" Y="-22.8691796875" />
                  <Point X="2.112563232422" Y="-22.879552734375" />
                  <Point X="2.1302109375" Y="-22.8990703125" />
                  <Point X="2.139713623047" Y="-22.90821484375" />
                  <Point X="2.166753662109" Y="-22.93083984375" />
                  <Point X="2.181996826172" Y="-22.94234375" />
                  <Point X="2.231565185547" Y="-22.975978515625" />
                  <Point X="2.24128125" Y="-22.9817578125" />
                  <Point X="2.261328369141" Y="-22.99212109375" />
                  <Point X="2.271659423828" Y="-22.996705078125" />
                  <Point X="2.293748046875" Y="-23.004970703125" />
                  <Point X="2.304554199219" Y="-23.008296875" />
                  <Point X="2.326478759766" Y="-23.013638671875" />
                  <Point X="2.352664794922" Y="-23.017470703125" />
                  <Point X="2.407021972656" Y="-23.024025390625" />
                  <Point X="2.42046484375" Y="-23.024685546875" />
                  <Point X="2.447310302734" Y="-23.024099609375" />
                  <Point X="2.460712890625" Y="-23.022853515625" />
                  <Point X="2.497041015625" Y="-23.01685546875" />
                  <Point X="2.515175292969" Y="-23.0129453125" />
                  <Point X="2.578036621094" Y="-22.996134765625" />
                  <Point X="2.584006103516" Y="-22.994326171875" />
                  <Point X="2.604412841797" Y="-22.9869296875" />
                  <Point X="2.627658691406" Y="-22.976423828125" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.804517089844" Y="-22.297501953125" />
                  <Point X="3.940403564453" Y="-22.219048828125" />
                  <Point X="4.043958740234" Y="-22.362966796875" />
                  <Point X="4.084396484375" Y="-22.429791015625" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.419159667969" Y="-23.0672578125" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168137939453" Y="-23.260130859375" />
                  <Point X="3.1521171875" Y="-23.27478515625" />
                  <Point X="3.134669189453" Y="-23.293396484375" />
                  <Point X="3.128579589844" Y="-23.30057421875" />
                  <Point X="3.116038818359" Y="-23.31693359375" />
                  <Point X="3.070797363281" Y="-23.375955078125" />
                  <Point X="3.063427734375" Y="-23.387115234375" />
                  <Point X="3.050330322266" Y="-23.410359375" />
                  <Point X="3.044602539062" Y="-23.422443359375" />
                  <Point X="3.031461669922" Y="-23.45609765625" />
                  <Point X="3.025468505859" Y="-23.47403125" />
                  <Point X="3.008615966797" Y="-23.53429296875" />
                  <Point X="3.006224853516" Y="-23.54533984375" />
                  <Point X="3.002771484375" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077636719" Y="-23.636244140625" />
                  <Point X="3.008533691406" Y="-23.666015625" />
                  <Point X="3.022367919922" Y="-23.7330625" />
                  <Point X="3.026025390625" Y="-23.746048828125" />
                  <Point X="3.035142089844" Y="-23.771369140625" />
                  <Point X="3.040601318359" Y="-23.783703125" />
                  <Point X="3.058229736328" Y="-23.816935546875" />
                  <Point X="3.067349365234" Y="-23.832330078125" />
                  <Point X="3.104976806641" Y="-23.889521484375" />
                  <Point X="3.111741210938" Y="-23.898580078125" />
                  <Point X="3.126296630859" Y="-23.915826171875" />
                  <Point X="3.134087646484" Y="-23.924013671875" />
                  <Point X="3.151330566406" Y="-23.9401015625" />
                  <Point X="3.160034423828" Y="-23.9473046875" />
                  <Point X="3.178243896484" Y="-23.960630859375" />
                  <Point X="3.202864257812" Y="-23.97526171875" />
                  <Point X="3.257391601562" Y="-24.00595703125" />
                  <Point X="3.265482910156" Y="-24.010015625" />
                  <Point X="3.294679443359" Y="-24.02191796875" />
                  <Point X="3.330275390625" Y="-24.03198046875" />
                  <Point X="3.343669433594" Y="-24.034744140625" />
                  <Point X="3.36410546875" Y="-24.0374453125" />
                  <Point X="3.437830078125" Y="-24.0471875" />
                  <Point X="3.444032714844" Y="-24.04780078125" />
                  <Point X="3.465708984375" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.610584472656" Y="-23.901072265625" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.765428710938" Y="-24.16762890625" />
                  <Point X="4.783870117188" Y="-24.286076171875" />
                  <Point X="3.974151367188" Y="-24.503041015625" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.686026123047" Y="-24.58045703125" />
                  <Point X="3.665616210938" Y="-24.5878671875" />
                  <Point X="3.642376708984" Y="-24.598384765625" />
                  <Point X="3.634004882812" Y="-24.602685546875" />
                  <Point X="3.613927001953" Y="-24.614291015625" />
                  <Point X="3.541494873047" Y="-24.65615625" />
                  <Point X="3.530207763672" Y="-24.663810546875" />
                  <Point X="3.508845214844" Y="-24.680658203125" />
                  <Point X="3.498769775391" Y="-24.6898515625" />
                  <Point X="3.472410400391" Y="-24.717623046875" />
                  <Point X="3.460750732422" Y="-24.731123046875" />
                  <Point X="3.417291503906" Y="-24.7865" />
                  <Point X="3.410854980469" Y="-24.795791015625" />
                  <Point X="3.399130126953" Y="-24.815072265625" />
                  <Point X="3.393841796875" Y="-24.8250625" />
                  <Point X="3.384067871094" Y="-24.84652734375" />
                  <Point X="3.380004394531" Y="-24.85707421875" />
                  <Point X="3.373158691406" Y="-24.87857421875" />
                  <Point X="3.370376464844" Y="-24.88952734375" />
                  <Point X="3.366360839844" Y="-24.91049609375" />
                  <Point X="3.351874267578" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584472656" Y="-25.026263671875" />
                  <Point X="3.350280273438" Y="-25.06294140625" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.355890380859" Y="-25.097390625" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373158691406" Y="-25.183986328125" />
                  <Point X="3.380004150391" Y="-25.205486328125" />
                  <Point X="3.384067871094" Y="-25.216033203125" />
                  <Point X="3.393841796875" Y="-25.237498046875" />
                  <Point X="3.399129882813" Y="-25.24748828125" />
                  <Point X="3.410852294922" Y="-25.266765625" />
                  <Point X="3.429333251953" Y="-25.291404296875" />
                  <Point X="3.472792480469" Y="-25.34678125" />
                  <Point X="3.478713134766" Y="-25.35362890625" />
                  <Point X="3.501141357422" Y="-25.37580859375" />
                  <Point X="3.530177001953" Y="-25.3987265625" />
                  <Point X="3.541493896484" Y="-25.406404296875" />
                  <Point X="3.561571777344" Y="-25.418009765625" />
                  <Point X="3.63400390625" Y="-25.459876953125" />
                  <Point X="3.639486816406" Y="-25.4628125" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.68302734375" Y="-25.481029296875" />
                  <Point X="3.691991943359" Y="-25.483916015625" />
                  <Point X="4.70989453125" Y="-25.756662109375" />
                  <Point X="4.784876464844" Y="-25.77675390625" />
                  <Point X="4.761612304688" Y="-25.931060546875" />
                  <Point X="4.745284179688" Y="-26.002611328125" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.7640078125" Y="-25.952333984375" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400096923828" Y="-25.90804296875" />
                  <Point X="3.366721191406" Y="-25.910841796875" />
                  <Point X="3.354483642578" Y="-25.912677734375" />
                  <Point X="3.315077880859" Y="-25.9212421875" />
                  <Point X="3.172919433594" Y="-25.952140625" />
                  <Point X="3.157875732422" Y="-25.9567421875" />
                  <Point X="3.128753662109" Y="-25.9683671875" />
                  <Point X="3.114675292969" Y="-25.975390625" />
                  <Point X="3.086848632812" Y="-25.992283203125" />
                  <Point X="3.074123779297" Y="-26.00153125" />
                  <Point X="3.050373779297" Y="-26.022001953125" />
                  <Point X="3.039348632812" Y="-26.033224609375" />
                  <Point X="3.015530273438" Y="-26.06187109375" />
                  <Point X="2.929604492188" Y="-26.165212890625" />
                  <Point X="2.921325927734" Y="-26.17684765625" />
                  <Point X="2.906605957031" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.871743408203" Y="-26.3337578125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876787109375" Y="-26.576671875" />
                  <Point X="2.889158935547" Y="-26.605482421875" />
                  <Point X="2.896541259766" Y="-26.619373046875" />
                  <Point X="2.918349121094" Y="-26.65329296875" />
                  <Point X="2.997021484375" Y="-26.775662109375" />
                  <Point X="3.001740966797" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.997416015625" Y="-27.561111328125" />
                  <Point X="4.087170898438" Y="-27.629984375" />
                  <Point X="4.0455" Y="-27.697412109375" />
                  <Point X="4.011718017578" Y="-27.745412109375" />
                  <Point X="4.001273193359" Y="-27.760251953125" />
                  <Point X="3.139893310547" Y="-27.262935546875" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841199951172" Y="-27.090890625" />
                  <Point X="2.815022216797" Y="-27.07951171875" />
                  <Point X="2.783119140625" Y="-27.069326171875" />
                  <Point X="2.771111083984" Y="-27.066337890625" />
                  <Point X="2.724211914062" Y="-27.0578671875" />
                  <Point X="2.555020751953" Y="-27.0273125" />
                  <Point X="2.539360595703" Y="-27.02580859375" />
                  <Point X="2.508009033203" Y="-27.025404296875" />
                  <Point X="2.492317626953" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444844970703" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400593505859" Y="-27.051109375" />
                  <Point X="2.361631591797" Y="-27.07161328125" />
                  <Point X="2.221075439453" Y="-27.145587890625" />
                  <Point X="2.208974121094" Y="-27.15316796875" />
                  <Point X="2.186041748047" Y="-27.170060546875" />
                  <Point X="2.175210693359" Y="-27.179373046875" />
                  <Point X="2.154251220703" Y="-27.20033203125" />
                  <Point X="2.144941650391" Y="-27.211158203125" />
                  <Point X="2.128048339844" Y="-27.234087890625" />
                  <Point X="2.120464599609" Y="-27.24619140625" />
                  <Point X="2.099959228516" Y="-27.28515234375" />
                  <Point X="2.025985595703" Y="-27.425708984375" />
                  <Point X="2.019836303711" Y="-27.44019140625" />
                  <Point X="2.010012207031" Y="-27.46996875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.002187744141" Y="-27.580140625" />
                  <Point X="2.010657592773" Y="-27.627041015625" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.676560058594" Y="-28.92495703125" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.723752441406" Y="-29.036083984375" />
                  <Point X="2.056309326172" Y="-28.16625390625" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828658813477" Y="-27.87015234375" />
                  <Point X="1.808835327148" Y="-27.84962890625" />
                  <Point X="1.783252929688" Y="-27.82800390625" />
                  <Point X="1.773299560547" Y="-27.820646484375" />
                  <Point X="1.727044189453" Y="-27.790908203125" />
                  <Point X="1.560176025391" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517470092773" Y="-27.663873046875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926513672" Y="-27.65115234375" />
                  <Point X="1.455385131836" Y="-27.6486953125" />
                  <Point X="1.424120727539" Y="-27.64637890625" />
                  <Point X="1.408397705078" Y="-27.64651953125" />
                  <Point X="1.357809570312" Y="-27.651173828125" />
                  <Point X="1.175310913086" Y="-27.667966796875" />
                  <Point X="1.161228027344" Y="-27.67033984375" />
                  <Point X="1.133575805664" Y="-27.677171875" />
                  <Point X="1.120006713867" Y="-27.681630859375" />
                  <Point X="1.09262121582" Y="-27.692974609375" />
                  <Point X="1.079876220703" Y="-27.6994140625" />
                  <Point X="1.055493896484" Y="-27.714134765625" />
                  <Point X="1.043856567383" Y="-27.722416015625" />
                  <Point X="1.004793640137" Y="-27.754896484375" />
                  <Point X="0.86387298584" Y="-27.872068359375" />
                  <Point X="0.852652709961" Y="-27.88308984375" />
                  <Point X="0.832183166504" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.771112365723" Y="-28.0593671875" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091369629" Y="-29.15233984375" />
                  <Point X="0.742005187988" Y="-28.81240234375" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146179199" Y="-28.45358203125" />
                  <Point X="0.62678704834" Y="-28.423814453125" />
                  <Point X="0.62040625" Y="-28.41320703125" />
                  <Point X="0.584871520996" Y="-28.362009765625" />
                  <Point X="0.456678649902" Y="-28.177306640625" />
                  <Point X="0.446669189453" Y="-28.165169921875" />
                  <Point X="0.424782989502" Y="-28.142712890625" />
                  <Point X="0.41290637207" Y="-28.132392578125" />
                  <Point X="0.386651397705" Y="-28.113150390625" />
                  <Point X="0.373238189697" Y="-28.104935546875" />
                  <Point X="0.345237335205" Y="-28.090828125" />
                  <Point X="0.330649688721" Y="-28.0849375" />
                  <Point X="0.275661743164" Y="-28.067873046875" />
                  <Point X="0.077290603638" Y="-28.0063046875" />
                  <Point X="0.063372959137" Y="-28.003107421875" />
                  <Point X="0.035222297668" Y="-27.99883984375" />
                  <Point X="0.020989425659" Y="-27.997767578125" />
                  <Point X="-0.008652074814" Y="-27.997765625" />
                  <Point X="-0.022896093369" Y="-27.998837890625" />
                  <Point X="-0.051062213898" Y="-28.003107421875" />
                  <Point X="-0.064984169006" Y="-28.0063046875" />
                  <Point X="-0.119971961975" Y="-28.02337109375" />
                  <Point X="-0.318343261719" Y="-28.0849375" />
                  <Point X="-0.332927764893" Y="-28.090828125" />
                  <Point X="-0.360928771973" Y="-28.104935546875" />
                  <Point X="-0.374345397949" Y="-28.11315234375" />
                  <Point X="-0.400600830078" Y="-28.132396484375" />
                  <Point X="-0.412478179932" Y="-28.14271875" />
                  <Point X="-0.434361877441" Y="-28.16517578125" />
                  <Point X="-0.444368347168" Y="-28.177310546875" />
                  <Point X="-0.479903045654" Y="-28.228509765625" />
                  <Point X="-0.608095947266" Y="-28.4132109375" />
                  <Point X="-0.612468994141" Y="-28.42012890625" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.956393859863" Y="-29.6584609375" />
                  <Point X="-0.985425109863" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.757207492114" Y="-25.646822064246" />
                  <Point X="-4.565422955278" Y="-26.274121018743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665388655456" Y="-25.622219102672" />
                  <Point X="-4.469925982651" Y="-26.261548697833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.122412149126" Y="-27.398215230178" />
                  <Point X="-4.032169884409" Y="-27.693384378023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.573569818797" Y="-25.597616141099" />
                  <Point X="-4.374429004271" Y="-26.24897639574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.041947930257" Y="-27.336472987275" />
                  <Point X="-3.860917507762" Y="-27.928596818719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.481750982139" Y="-25.573013179525" />
                  <Point X="-4.278932025891" Y="-26.236404093648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.961483711388" Y="-27.274730744373" />
                  <Point X="-3.743147064415" Y="-27.988877737834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770146809533" Y="-24.304784088449" />
                  <Point X="-4.734351656627" Y="-24.421864758059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.38993214548" Y="-25.548410217951" />
                  <Point X="-4.183435047511" Y="-26.223831791555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.881019492519" Y="-27.212988501471" />
                  <Point X="-3.658710452332" Y="-27.940128607678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.737747227856" Y="-24.08582950113" />
                  <Point X="-4.62614665354" Y="-24.450858531857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.298113308822" Y="-25.523807256378" />
                  <Point X="-4.087938069131" Y="-26.211259489462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.80055527365" Y="-27.151246258568" />
                  <Point X="-3.574273956968" Y="-27.89137909575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.693896983646" Y="-23.904328343342" />
                  <Point X="-4.517941794333" Y="-24.479851835043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.206294472164" Y="-25.499204294804" />
                  <Point X="-3.992441090751" Y="-26.198687187369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.720091054781" Y="-27.089504015666" />
                  <Point X="-3.489837461605" Y="-27.842629583822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.647219403568" Y="-23.732074984482" />
                  <Point X="-4.409736935126" Y="-24.508845138229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.114475635505" Y="-25.47460133323" />
                  <Point X="-3.896944112371" Y="-26.186114885276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.639626835912" Y="-27.027761772764" />
                  <Point X="-3.405400966241" Y="-27.793880071894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.560311091145" Y="-23.691410421854" />
                  <Point X="-4.301532075919" Y="-24.537838441415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.022656798847" Y="-25.449998371657" />
                  <Point X="-3.801447133991" Y="-26.173542583183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.559162617043" Y="-26.966019529861" />
                  <Point X="-3.320964470877" Y="-27.745130559966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.456804230366" Y="-23.70503726458" />
                  <Point X="-4.193327216711" Y="-24.566831744602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.930837962188" Y="-25.425395410083" />
                  <Point X="-3.705950155611" Y="-26.16097028109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.478698398174" Y="-26.904277286959" />
                  <Point X="-3.236527975513" Y="-27.696381048038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921112514021" Y="-28.728058536171" />
                  <Point X="-2.861335636673" Y="-28.923579891968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.353297369587" Y="-23.718664107305" />
                  <Point X="-4.085122357504" Y="-24.595825047788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.83901912553" Y="-25.400792448509" />
                  <Point X="-3.610453177231" Y="-26.148397978997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.398234179305" Y="-26.842535044057" />
                  <Point X="-3.15209148015" Y="-27.64763153611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.856164502185" Y="-28.615565066865" />
                  <Point X="-2.732330319711" Y="-29.020608426867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.249790508808" Y="-23.732290950031" />
                  <Point X="-3.976917498297" Y="-24.624818350974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.747200205919" Y="-25.37618975826" />
                  <Point X="-3.514956198852" Y="-26.135825676904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.317769966543" Y="-26.78079278118" />
                  <Point X="-3.067654984786" Y="-27.598882024182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.79121649035" Y="-28.503071597558" />
                  <Point X="-2.609793335816" Y="-29.096479997619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146283648029" Y="-23.745917792757" />
                  <Point X="-3.86871263909" Y="-24.65381165416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.655381277625" Y="-25.351587096414" />
                  <Point X="-3.419459220472" Y="-26.123253374811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.237305785342" Y="-26.719050415072" />
                  <Point X="-2.983218489422" Y="-27.550132512254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.726268478514" Y="-28.390578128251" />
                  <Point X="-2.493957949257" Y="-29.150431631174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.04277678725" Y="-23.759544635483" />
                  <Point X="-3.760507779883" Y="-24.682804957346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.56356234933" Y="-25.326984434568" />
                  <Point X="-3.323962242092" Y="-26.110681072718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.156841604141" Y="-26.657308048963" />
                  <Point X="-2.898781994058" Y="-27.501383000326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.661320466679" Y="-28.278084658945" />
                  <Point X="-2.422920813441" Y="-29.057854788982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.9392698956" Y="-23.773171579185" />
                  <Point X="-3.652302920676" Y="-24.711798260533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.472908552541" Y="-25.298570799288" />
                  <Point X="-3.228051732256" Y="-26.099461371071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.07637742294" Y="-26.595565682855" />
                  <Point X="-2.814345498695" Y="-27.452633488398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.596372454843" Y="-28.165591189638" />
                  <Point X="-2.34632789192" Y="-28.983450103013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.187364895701" Y="-22.636760554586" />
                  <Point X="-4.147484587496" Y="-22.767203165103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.835762988933" Y="-23.786798572003" />
                  <Point X="-3.544098061469" Y="-24.740791563719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.389945833342" Y="-25.245000782732" />
                  <Point X="-3.123137654385" Y="-26.117691013507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.999631937427" Y="-26.521660011218" />
                  <Point X="-2.729909003331" Y="-27.40388397647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.531424443008" Y="-28.053097720332" />
                  <Point X="-2.263838304693" Y="-28.928332541508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.122171529552" Y="-22.525069603078" />
                  <Point X="-4.017696008375" Y="-22.866793635089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.733899114398" Y="-23.79505144887" />
                  <Point X="-3.430598508974" Y="-24.787103028307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.320108534867" Y="-25.148499449433" />
                  <Point X="-2.995676207964" Y="-26.209669775405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947634410825" Y="-26.366807413372" />
                  <Point X="-2.643570845607" Y="-27.361354521851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466476418514" Y="-27.940604292428" />
                  <Point X="-2.181348717466" Y="-28.873214980003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056978163403" Y="-22.41337865157" />
                  <Point X="-3.887907429253" Y="-22.966384105075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640784653924" Y="-23.774686281847" />
                  <Point X="-2.548185654996" Y="-27.348416578441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.401528384204" Y="-27.828110896631" />
                  <Point X="-2.098859239244" Y="-28.818097061958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990798006693" Y="-22.304915346551" />
                  <Point X="-3.758118850131" Y="-23.065974575061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.553179851862" Y="-23.736299834176" />
                  <Point X="-2.436992055778" Y="-27.38718560972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.33692893291" Y="-27.714477337166" />
                  <Point X="-2.010376548435" Y="-28.782582058998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.919482799555" Y="-22.213248034672" />
                  <Point X="-3.628330271009" Y="-23.165565045047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.478182994811" Y="-23.656674656559" />
                  <Point X="-1.91240394237" Y="-28.778107170201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.84816724388" Y="-22.121581862807" />
                  <Point X="-3.491778434731" Y="-23.287277132412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423812226291" Y="-23.509584583254" />
                  <Point X="-1.81103197504" Y="-28.784751091098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776851688205" Y="-22.029915690941" />
                  <Point X="-1.709660007711" Y="-28.791395011995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.691151030069" Y="-21.985301069129" />
                  <Point X="-1.607142928022" Y="-28.801784426651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.570516676772" Y="-22.054949415603" />
                  <Point X="-1.490440930573" Y="-28.858570616706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.449882323476" Y="-22.124597762076" />
                  <Point X="-1.354707184615" Y="-28.977606851204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.136136791328" Y="-29.692518394408" />
                  <Point X="-1.122822725165" Y="-29.736066742581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.329248030394" Y="-22.194245911599" />
                  <Point X="-1.20872342271" Y="-29.130169377203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.173562799047" Y="-29.245174595178" />
                  <Point X="-1.015891812719" Y="-29.760893153668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.211053391398" Y="-22.255914312166" />
                  <Point X="-0.952411572926" Y="-29.643598818333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.106459186748" Y="-22.273097696438" />
                  <Point X="-0.906012448457" Y="-29.470434672212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.007248532894" Y="-22.272672279496" />
                  <Point X="-0.859613323989" Y="-29.297270526092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.918631298528" Y="-22.237597348681" />
                  <Point X="-0.81321419952" Y="-29.124106379971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.840425356202" Y="-22.168468616035" />
                  <Point X="-0.766815075052" Y="-28.95094223385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.016604970802" Y="-21.267282218412" />
                  <Point X="-2.947132699094" Y="-21.49451578024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.769719570657" Y="-22.074807975942" />
                  <Point X="-0.720415950583" Y="-28.77777808773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.936128142736" Y="-21.205581218333" />
                  <Point X="-0.674016826114" Y="-28.604613941609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.855651314671" Y="-21.143880218253" />
                  <Point X="-0.62433091927" Y="-28.442200376227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.775174486606" Y="-21.082179218174" />
                  <Point X="-0.556556309963" Y="-28.338952290663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.694697658541" Y="-21.020478218094" />
                  <Point X="-0.487593587386" Y="-28.239590348499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.612336161254" Y="-20.964941693273" />
                  <Point X="-0.416581966161" Y="-28.14693005184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.52741883992" Y="-20.917764892227" />
                  <Point X="-0.334197715633" Y="-28.091467949518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.442501873197" Y="-20.870586931304" />
                  <Point X="-0.243915719414" Y="-28.061838209368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.357584929413" Y="-20.823408895349" />
                  <Point X="-0.153184191028" Y="-28.033678822685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.272667985629" Y="-20.776230859393" />
                  <Point X="-0.062394421127" Y="-28.005709935412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.183241183184" Y="-20.74380390645" />
                  <Point X="0.034837023958" Y="-27.998810818283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.037475673464" Y="-20.895652561718" />
                  <Point X="0.142672943522" Y="-28.026597374271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.920236908571" Y="-20.954194438971" />
                  <Point X="0.252428289287" Y="-28.060662090477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.816033089636" Y="-20.970100929108" />
                  <Point X="0.363860241225" Y="-28.100210738371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.723998681163" Y="-20.946203071168" />
                  <Point X="0.510464317108" Y="-28.254802219968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.635930768474" Y="-20.909331390107" />
                  <Point X="0.794431351135" Y="-28.858687692895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.554844548256" Y="-20.849623621945" />
                  <Point X="0.724733000221" Y="-28.305785815419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.493310597882" Y="-20.725963260767" />
                  <Point X="0.761814413834" Y="-28.102144810348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.477040162783" Y="-20.45425261213" />
                  <Point X="0.811229253841" Y="-27.938844625294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.390478761746" Y="-20.412453353488" />
                  <Point X="0.884827603814" Y="-27.854645137135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.298980753047" Y="-20.386801010946" />
                  <Point X="0.964033592448" Y="-27.788787408574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.207482744347" Y="-20.361148668403" />
                  <Point X="1.043239423638" Y="-27.722929165036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.115984735647" Y="-20.33549632586" />
                  <Point X="1.129045845358" Y="-27.678660480319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.024486726947" Y="-20.309843983318" />
                  <Point X="1.223754344948" Y="-27.663509180311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.932988718248" Y="-20.284191640775" />
                  <Point X="1.320376838872" Y="-27.654618273683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.837168397364" Y="-20.272676944356" />
                  <Point X="1.417217404915" Y="-27.646440648815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.741259811865" Y="-20.261450948487" />
                  <Point X="1.522555342677" Y="-27.666056674486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.645350835252" Y="-20.250226231896" />
                  <Point X="1.643681809527" Y="-27.737314651865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.549441858638" Y="-20.239001515304" />
                  <Point X="1.767325337632" Y="-27.816805565643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.453532882025" Y="-20.227776798712" />
                  <Point X="1.918657858485" Y="-27.986863093853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.167236637012" Y="-20.839280777489" />
                  <Point X="2.083795246284" Y="-28.202074307259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.046858277216" Y="-20.908091806952" />
                  <Point X="2.015459233063" Y="-27.653628435594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.097516238626" Y="-27.922024807104" />
                  <Point X="2.248932804816" Y="-28.41728607911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.052957988555" Y="-20.909647257333" />
                  <Point X="2.03808768294" Y="-27.402713916242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.308673025303" Y="-28.287758691834" />
                  <Point X="2.414070363348" Y="-28.632497850961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.141225581877" Y="-20.873428702195" />
                  <Point X="2.100925129191" Y="-27.283317097967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.51982981198" Y="-28.653492576564" />
                  <Point X="2.579207921881" Y="-28.847709622813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.215045121319" Y="-20.78995269219" />
                  <Point X="2.170061262739" Y="-27.184522357532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730986290494" Y="-29.019225453335" />
                  <Point X="2.73398620013" Y="-29.029037715624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.266286310366" Y="-20.632626225673" />
                  <Point X="2.252450144754" Y="-27.129075404119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.312685431051" Y="-20.459462067178" />
                  <Point X="2.338021870075" Y="-27.084039062069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.359084551737" Y="-20.286297908683" />
                  <Point X="2.424467211065" Y="-27.04186018812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.438558423347" Y="-20.221316385858" />
                  <Point X="2.518819478102" Y="-27.025543703933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.541185105962" Y="-20.232064295529" />
                  <Point X="2.622422398924" Y="-27.0394847449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.643811788576" Y="-20.242812205201" />
                  <Point X="2.727568544303" Y="-27.058473445954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746438471191" Y="-20.253560114872" />
                  <Point X="2.836148934308" Y="-27.088695055035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.850021706613" Y="-20.267436767799" />
                  <Point X="2.956365186023" Y="-27.156975852855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.957279176058" Y="-20.293331298702" />
                  <Point X="2.864088931544" Y="-26.530224980384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.882001914639" Y="-26.588815708045" />
                  <Point X="3.076999435662" Y="-27.226623860283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.064537029099" Y="-20.319227084291" />
                  <Point X="2.882083592699" Y="-26.264154021058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.058288270631" Y="-26.840493553261" />
                  <Point X="3.197633685101" Y="-27.296271867053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.17179488214" Y="-20.34512286988" />
                  <Point X="2.945375864569" Y="-26.146244870348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.188076757707" Y="-26.940083722179" />
                  <Point X="3.31826793432" Y="-27.365919873107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.279052735182" Y="-20.371018655468" />
                  <Point X="3.018009359154" Y="-26.058889482418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.317865244783" Y="-27.039673891098" />
                  <Point X="3.438902183539" Y="-27.43556787916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.386310588223" Y="-20.396914441057" />
                  <Point X="2.029905379008" Y="-22.502018147738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.159034737648" Y="-22.924381248571" />
                  <Point X="3.095399472063" Y="-25.987092291989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.44765373186" Y="-27.139264060016" />
                  <Point X="3.559536432758" Y="-27.505215885213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.495234131645" Y="-20.42825845439" />
                  <Point X="2.086998854306" Y="-22.363833647032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281627464749" Y="-23.000435147132" />
                  <Point X="3.183360566109" Y="-25.949871222888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.577442218936" Y="-27.238854228934" />
                  <Point X="3.680170681978" Y="-27.574863891267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.606964644981" Y="-20.468783652616" />
                  <Point X="2.151946870759" Y="-22.251340192829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.387459229291" Y="-23.021666407419" />
                  <Point X="3.276511304041" Y="-25.929624714084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707230706012" Y="-27.338444397853" />
                  <Point X="3.800804931197" Y="-27.64451189732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.71869520853" Y="-20.509309015081" />
                  <Point X="2.216894887212" Y="-22.138846738625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.485891885271" Y="-23.01869627409" />
                  <Point X="3.370024808631" Y="-25.910564761552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.837019193089" Y="-27.438034566771" />
                  <Point X="3.921439180416" Y="-27.714159903374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.830425772079" Y="-20.549834377546" />
                  <Point X="2.281842935197" Y="-22.026353387559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578309573665" Y="-22.996052068285" />
                  <Point X="3.470314444774" Y="-25.913668536652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.966807680165" Y="-27.537624735689" />
                  <Point X="4.024696662979" Y="-27.726971066708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.945298457509" Y="-20.600637157593" />
                  <Point X="2.346791021" Y="-21.913860160189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.665188822655" Y="-22.95529244345" />
                  <Point X="3.573821333518" Y="-25.927295470848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.061212632839" Y="-20.654846497605" />
                  <Point X="2.411739106803" Y="-21.80136693282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.749625357537" Y="-22.906543060782" />
                  <Point X="3.364794991942" Y="-24.918672270286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.505641555273" Y="-25.379360620763" />
                  <Point X="3.677328222262" Y="-25.940922405043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.177126808169" Y="-20.709055837618" />
                  <Point X="2.476687192607" Y="-21.68887370545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.83406189242" Y="-22.857793678114" />
                  <Point X="3.024033466498" Y="-23.479162698625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.169212312906" Y="-23.954021308547" />
                  <Point X="3.421922493563" Y="-24.780599064666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.628652978337" Y="-25.456784012111" />
                  <Point X="3.780835104773" Y="-25.954549318851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294861910516" Y="-20.769221161533" />
                  <Point X="2.54163527841" Y="-21.576380478081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.918498427302" Y="-22.809044295446" />
                  <Point X="3.085826167407" Y="-23.356348672311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288546776686" Y="-24.019417907995" />
                  <Point X="3.494798196497" Y="-24.694035904549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.740241388423" Y="-25.496844411547" />
                  <Point X="3.884341955175" Y="-25.968176127635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.415731597007" Y="-20.839639248205" />
                  <Point X="2.606583364213" Y="-21.463887250712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002934962185" Y="-22.760294912778" />
                  <Point X="3.158457360006" Y="-23.268985754923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.394632313466" Y="-24.041479219869" />
                  <Point X="3.576391377065" Y="-24.635986328977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.84844627849" Y="-25.525837815672" />
                  <Point X="3.987848805577" Y="-25.981802936419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.536601461312" Y="-20.910057916477" />
                  <Point X="2.671531450016" Y="-21.351394023342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.087371497067" Y="-22.71154553011" />
                  <Point X="3.238504490639" Y="-23.205879277873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.495841711032" Y="-24.04759139903" />
                  <Point X="3.661579181755" Y="-24.589694239125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956651168557" Y="-25.554831219797" />
                  <Point X="4.091355655979" Y="-25.995429745203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658879176962" Y="-20.985081459009" />
                  <Point X="2.736479535819" Y="-21.238900795973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.17180803195" Y="-22.662796147442" />
                  <Point X="3.318968692936" Y="-23.144136980767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.591408811253" Y="-24.035248455146" />
                  <Point X="3.752578875491" Y="-24.562411981778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.064856058624" Y="-25.583824623922" />
                  <Point X="4.194862506381" Y="-26.009056553987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.785819255562" Y="-21.075354903605" />
                  <Point X="2.801427621622" Y="-21.126407568604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.256244566832" Y="-22.614046764774" />
                  <Point X="3.399432895233" Y="-23.08239468366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.686905779146" Y="-24.022676118749" />
                  <Point X="3.844397669369" Y="-24.537808880275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.173060948691" Y="-25.612818028047" />
                  <Point X="4.298369356783" Y="-26.022683362772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.340681101715" Y="-22.565297382106" />
                  <Point X="3.479897106745" Y="-23.020652416693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.782402747038" Y="-24.010103782352" />
                  <Point X="3.936216463246" Y="-24.513205778772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.281265838758" Y="-25.641811432172" />
                  <Point X="4.401876207185" Y="-26.036310171556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.425117636597" Y="-22.516547999438" />
                  <Point X="3.560361321249" Y="-22.958910159516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.87789971493" Y="-23.997531445955" />
                  <Point X="4.028035286273" Y="-24.488602772611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.389470728826" Y="-25.670804836297" />
                  <Point X="4.505383057587" Y="-26.04993698034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.50955417148" Y="-22.46779861677" />
                  <Point X="3.640825535754" Y="-22.897167902338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.973396682822" Y="-23.984959109559" />
                  <Point X="4.119854129822" Y="-24.463999833574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.497675618893" Y="-25.699798240421" />
                  <Point X="4.608889907989" Y="-26.063563789124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.593990706362" Y="-22.419049234102" />
                  <Point X="3.721289750258" Y="-22.83542564516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068893650714" Y="-23.972386773162" />
                  <Point X="4.21167297337" Y="-24.439396894536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.60588050896" Y="-25.728791644546" />
                  <Point X="4.712396758391" Y="-26.077190597908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.678427241245" Y="-22.370299851434" />
                  <Point X="3.801753964763" Y="-22.773683387982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.164390618606" Y="-23.959814436765" />
                  <Point X="4.303491816919" Y="-24.414793955499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.714085407485" Y="-25.757785076335" />
                  <Point X="4.763411841267" Y="-25.919124571433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.762863776127" Y="-22.321550468766" />
                  <Point X="3.882218179267" Y="-22.711941130804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.259887586499" Y="-23.947242100368" />
                  <Point X="4.395310660467" Y="-24.390191016461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.847300380508" Y="-22.272801313418" />
                  <Point X="3.962682393772" Y="-22.650198873626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.355384554391" Y="-23.934669763972" />
                  <Point X="4.487129504016" Y="-24.365588077424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.931737052553" Y="-22.224052379386" />
                  <Point X="4.043146608276" Y="-22.588456616448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.450881522283" Y="-23.922097427575" />
                  <Point X="4.578948347564" Y="-24.340985138386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.103762962627" Y="-22.461794933916" />
                  <Point X="4.123610822781" Y="-22.52671435927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.546378490175" Y="-23.909525091178" />
                  <Point X="4.670767191112" Y="-24.316382199349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.641875304592" Y="-23.896952252787" />
                  <Point X="4.762586034661" Y="-24.291779260311" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998373046875" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.558479431152" Y="-28.861578125" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.42878414917" Y="-28.470345703125" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.27433605957" Y="-28.266400390625" />
                  <Point X="0.219348144531" Y="-28.2493359375" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="-0.008664604187" Y="-28.187765625" />
                  <Point X="-0.063652515411" Y="-28.20483203125" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.323813842773" Y="-28.336841796875" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.772867980957" Y="-29.70763671875" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-0.867037536621" Y="-29.98333203125" />
                  <Point X="-1.10023059082" Y="-29.938068359375" />
                  <Point X="-1.162963745117" Y="-29.921927734375" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.319615844727" Y="-29.630533203125" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.322819824219" Y="-29.468716796875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.436908447266" Y="-29.158232421875" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.716432617188" Y="-28.981359375" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.045866333008" Y="-29.011201171875" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734375" Y="-29.157294921875" />
                  <Point X="-2.435819580078" Y="-29.3867734375" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.513996337891" Y="-29.379267578125" />
                  <Point X="-2.855837890625" Y="-29.167609375" />
                  <Point X="-2.942680908203" Y="-29.1007421875" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.667602539062" Y="-27.90896484375" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499763427734" Y="-27.597587890625" />
                  <Point X="-2.513987548828" Y="-27.568755859375" />
                  <Point X="-2.531327392578" Y="-27.55141796875" />
                  <Point X="-2.560156738281" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.601237548828" Y="-28.126337890625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.891633544922" Y="-28.201947265625" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.223965332031" Y="-27.7427265625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.446479736328" Y="-26.640064453125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535644531" Y="-26.411083984375" />
                  <Point X="-3.144149902344" Y="-26.38955859375" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.136651855469" Y="-26.350052734375" />
                  <Point X="-3.148657226562" Y="-26.321068359375" />
                  <Point X="-3.166907470703" Y="-26.307255859375" />
                  <Point X="-3.187642333984" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-4.493995605469" Y="-26.456357421875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.821767089844" Y="-26.424712890625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.943865722656" Y="-25.896017578125" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.879493652344" Y="-25.21493359375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.535871582031" Y="-25.11724609375" />
                  <Point X="-3.514142089844" Y="-25.1021640625" />
                  <Point X="-3.502323974609" Y="-25.090646484375" />
                  <Point X="-3.492891113281" Y="-25.069439453125" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.487655761719" Y="-25.009990234375" />
                  <Point X="-3.494898925781" Y="-24.98665234375" />
                  <Point X="-3.502325927734" Y="-24.971912109375" />
                  <Point X="-3.520165527344" Y="-24.956216796875" />
                  <Point X="-3.541895019531" Y="-24.941134765625" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.719200195312" Y="-24.62262890625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.985719726562" Y="-24.463625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.884484863281" Y="-23.8812109375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.984105224609" Y="-23.57562890625" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731704833984" Y="-23.6041328125" />
                  <Point X="-3.718373291016" Y="-23.5999296875" />
                  <Point X="-3.670278808594" Y="-23.584765625" />
                  <Point X="-3.651534423828" Y="-23.5739453125" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.633770507812" Y="-23.54330078125" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.622770507812" Y="-23.44208984375" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.326343261719" Y="-22.869451171875" />
                  <Point X="-4.476105957031" Y="-22.75453515625" />
                  <Point X="-4.424528808594" Y="-22.666169921875" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.072175292969" Y="-22.10008984375" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.300931884766" Y="-21.991203125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514160156" Y="-22.07956640625" />
                  <Point X="-3.119946777344" Y="-22.08119140625" />
                  <Point X="-3.05296484375" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-3.000073730469" Y="-22.059416015625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.939698730469" Y="-21.953591796875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.247358398438" Y="-21.354509765625" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-3.213576171875" Y="-21.1788828125" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.614532470703" Y="-20.74880859375" />
                  <Point X="-2.141549316406" Y="-20.486029296875" />
                  <Point X="-2.011228027344" Y="-20.6558671875" />
                  <Point X="-1.967826416016" Y="-20.712427734375" />
                  <Point X="-1.951247436523" Y="-20.726337890625" />
                  <Point X="-1.93058215332" Y="-20.737095703125" />
                  <Point X="-1.85603137207" Y="-20.77590625" />
                  <Point X="-1.835124389648" Y="-20.781509765625" />
                  <Point X="-1.81380859375" Y="-20.77775" />
                  <Point X="-1.792284545898" Y="-20.768833984375" />
                  <Point X="-1.714634765625" Y="-20.736669921875" />
                  <Point X="-1.696905151367" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.679077514648" Y="-20.68329296875" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.685488769531" Y="-20.326576171875" />
                  <Point X="-1.689137695313" Y="-20.298861328125" />
                  <Point X="-1.564498657227" Y="-20.263916015625" />
                  <Point X="-0.968082824707" Y="-20.096703125" />
                  <Point X="-0.800381896973" Y="-20.07707421875" />
                  <Point X="-0.224200027466" Y="-20.009640625" />
                  <Point X="-0.084081283569" Y="-20.532572265625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282108307" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594028473" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.205755950928" Y="-20.124423828125" />
                  <Point X="0.236648422241" Y="-20.009130859375" />
                  <Point X="0.339457489014" Y="-20.0198984375" />
                  <Point X="0.860210205078" Y="-20.074435546875" />
                  <Point X="0.998960144043" Y="-20.10793359375" />
                  <Point X="1.508456176758" Y="-20.230943359375" />
                  <Point X="1.598582885742" Y="-20.2636328125" />
                  <Point X="1.931044433594" Y="-20.38421875" />
                  <Point X="2.018366210938" Y="-20.425056640625" />
                  <Point X="2.338685058594" Y="-20.574859375" />
                  <Point X="2.423071289062" Y="-20.624021484375" />
                  <Point X="2.732533691406" Y="-20.804314453125" />
                  <Point X="2.812092773438" Y="-20.860892578125" />
                  <Point X="3.068740722656" Y="-21.043408203125" />
                  <Point X="2.416857177734" Y="-22.172501953125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.220192382812" Y="-22.525912109375" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.203861572266" Y="-22.6227421875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.228005615234" Y="-22.712927734375" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.288679931641" Y="-22.78512109375" />
                  <Point X="2.338248291016" Y="-22.818755859375" />
                  <Point X="2.360336914062" Y="-22.827021484375" />
                  <Point X="2.375404541016" Y="-22.828837890625" />
                  <Point X="2.42976171875" Y="-22.835392578125" />
                  <Point X="2.46608984375" Y="-22.82939453125" />
                  <Point X="2.528951171875" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.709517089844" Y="-22.132958984375" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.01903515625" Y="-22.003017578125" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.246950683594" Y="-22.331423828125" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.53482421875" Y="-23.21799609375" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.27937109375" Y="-23.41616796875" />
                  <Point X="3.266830322266" Y="-23.43252734375" />
                  <Point X="3.221588867188" Y="-23.491548828125" />
                  <Point X="3.208447998047" Y="-23.525203125" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.194614257812" Y="-23.62762109375" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.226076904297" Y="-23.727900390625" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280947265625" Y="-23.8011796875" />
                  <Point X="3.296062011719" Y="-23.8096875" />
                  <Point X="3.350589355469" Y="-23.8403828125" />
                  <Point X="3.36856640625" Y="-23.846380859375" />
                  <Point X="3.389002441406" Y="-23.84908203125" />
                  <Point X="3.462727050781" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.585784667969" Y="-23.712697265625" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.860352050781" Y="-23.724779296875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.953166992188" Y="-24.1383984375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.023327148437" Y="-24.686568359375" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.729087890625" Y="-24.767181640625" />
                  <Point X="3.709010009766" Y="-24.778787109375" />
                  <Point X="3.636577880859" Y="-24.82065234375" />
                  <Point X="3.610218505859" Y="-24.848423828125" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985351562" Y="-24.925265625" />
                  <Point X="3.552969726562" Y="-24.946234375" />
                  <Point X="3.538483154297" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.542498779297" Y="-25.06165234375" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759277344" Y="-25.158759765625" />
                  <Point X="3.578805908203" Y="-25.174111328125" />
                  <Point X="3.622265136719" Y="-25.22948828125" />
                  <Point X="3.636576904297" Y="-25.241908203125" />
                  <Point X="3.656654785156" Y="-25.253513671875" />
                  <Point X="3.729086914062" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.7590703125" Y="-25.573134765625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.992450683594" Y="-25.674431640625" />
                  <Point X="4.948430664062" Y="-25.966404296875" />
                  <Point X="4.930522460938" Y="-26.044880859375" />
                  <Point X="4.874545898438" Y="-26.290177734375" />
                  <Point X="3.739208007812" Y="-26.140708984375" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836181641" Y="-26.098341796875" />
                  <Point X="3.355430419922" Y="-26.10690625" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.161626953125" Y="-26.18334375" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.060944091797" Y="-26.35116796875" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.078168457031" Y="-26.55054296875" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.113080566406" Y="-27.410373046875" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.328217285156" Y="-27.601353515625" />
                  <Point X="4.204134277344" Y="-27.802138671875" />
                  <Point X="4.16709375" Y="-27.854767578125" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.044893310547" Y="-27.427478515625" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.69044140625" Y="-27.24484375" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077392578" Y="-27.21924609375" />
                  <Point X="2.450115478516" Y="-27.23975" />
                  <Point X="2.309559326172" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.268094482422" Y="-27.37364453125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.197633056641" Y="-27.593275390625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.841104980469" Y="-28.82995703125" />
                  <Point X="2.986673828125" Y="-29.082087890625" />
                  <Point X="2.982535400391" Y="-29.085044921875" />
                  <Point X="2.835296142578" Y="-29.190212890625" />
                  <Point X="2.793889160156" Y="-29.217015625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.905572021484" Y="-28.28191796875" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.624293701172" Y="-27.950728515625" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.375216796875" Y="-27.840373046875" />
                  <Point X="1.192718139648" Y="-27.857166015625" />
                  <Point X="1.165332641602" Y="-27.868509765625" />
                  <Point X="1.126269775391" Y="-27.900990234375" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.95677722168" Y="-28.09972265625" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.085952392578" Y="-29.61736328125" />
                  <Point X="1.127642333984" Y="-29.934029296875" />
                  <Point X="0.994363708496" Y="-29.963244140625" />
                  <Point X="0.956088928223" Y="-29.970197265625" />
                  <Point X="0.860200561523" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#138" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.041940917734" Y="4.511650117458" Z="0.55" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.55" />
                  <Point X="-0.812349928673" Y="5.003865791466" Z="0.55" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.55" />
                  <Point X="-1.584152528955" Y="4.815509069241" Z="0.55" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.55" />
                  <Point X="-1.741217125809" Y="4.69817964729" Z="0.55" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.55" />
                  <Point X="-1.732919404343" Y="4.363023419925" Z="0.55" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.55" />
                  <Point X="-1.817574798158" Y="4.30864029066" Z="0.55" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.55" />
                  <Point X="-1.913649924864" Y="4.338533584323" Z="0.55" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.55" />
                  <Point X="-1.977716765489" Y="4.405853366124" Z="0.55" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.55" />
                  <Point X="-2.644971511584" Y="4.326179708828" Z="0.55" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.55" />
                  <Point X="-3.250154269711" Y="3.892077472508" Z="0.55" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.55" />
                  <Point X="-3.296815524515" Y="3.651771614417" Z="0.55" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.55" />
                  <Point X="-2.995664451945" Y="3.073330797019" Z="0.55" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.55" />
                  <Point X="-3.04158439484" Y="3.007219173046" Z="0.55" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.55" />
                  <Point X="-3.121745668587" Y="2.999900247228" Z="0.55" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.55" />
                  <Point X="-3.28208773388" Y="3.083378424231" Z="0.55" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.55" />
                  <Point X="-4.117794784715" Y="2.961893681479" Z="0.55" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.55" />
                  <Point X="-4.473866727525" Y="2.390315334124" Z="0.55" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.55" />
                  <Point X="-4.362937058356" Y="2.122161408039" Z="0.55" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.55" />
                  <Point X="-3.673277036251" Y="1.566103559192" Z="0.55" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.55" />
                  <Point X="-3.686120532424" Y="1.507114645758" Z="0.55" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.55" />
                  <Point X="-3.739564416453" Y="1.479035985021" Z="0.55" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.55" />
                  <Point X="-3.983734922552" Y="1.505223055388" Z="0.55" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.55" />
                  <Point X="-4.938900564155" Y="1.163147408859" Z="0.55" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.55" />
                  <Point X="-5.041337259618" Y="0.574974237073" Z="0.55" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.55" />
                  <Point X="-4.738297182979" Y="0.360355379726" Z="0.55" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.55" />
                  <Point X="-3.554831217034" Y="0.033987505218" Z="0.55" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.55" />
                  <Point X="-3.541564562082" Y="0.006469164629" Z="0.55" />
                  <Point X="-3.539556741714" Y="0" Z="0.55" />
                  <Point X="-3.546800039053" Y="-0.023337786432" Z="0.55" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.55" />
                  <Point X="-3.570537378089" Y="-0.044888477916" Z="0.55" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.55" />
                  <Point X="-3.898590605794" Y="-0.13535667703" Z="0.55" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.55" />
                  <Point X="-4.999517852653" Y="-0.871814540093" Z="0.55" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.55" />
                  <Point X="-4.876367707698" Y="-1.405807924187" Z="0.55" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.55" />
                  <Point X="-4.49362510018" Y="-1.47464993571" Z="0.55" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.55" />
                  <Point X="-3.19842396453" Y="-1.319066981523" Z="0.55" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.55" />
                  <Point X="-3.19870865342" Y="-1.345740937502" Z="0.55" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.55" />
                  <Point X="-3.483073754893" Y="-1.569115093049" Z="0.55" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.55" />
                  <Point X="-4.273064701536" Y="-2.737055440736" Z="0.55" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.55" />
                  <Point X="-3.937575355076" Y="-3.200949429657" Z="0.55" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.55" />
                  <Point X="-3.582393525756" Y="-3.138357251903" Z="0.55" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.55" />
                  <Point X="-2.559256215936" Y="-2.569074253196" Z="0.55" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.55" />
                  <Point X="-2.717059761265" Y="-2.852684856736" Z="0.55" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.55" />
                  <Point X="-2.9793407862" Y="-4.109078853654" Z="0.55" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.55" />
                  <Point X="-2.546473956598" Y="-4.39049937596" Z="0.55" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.55" />
                  <Point X="-2.402307465941" Y="-4.385930785159" Z="0.55" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.55" />
                  <Point X="-2.024243820837" Y="-4.021494290865" Z="0.55" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.55" />
                  <Point X="-1.72585866486" Y="-3.999971945536" Z="0.55" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.55" />
                  <Point X="-1.47603185385" Y="-4.164542744976" Z="0.55" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.55" />
                  <Point X="-1.378015135464" Y="-4.447190143801" Z="0.55" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.55" />
                  <Point X="-1.375344095838" Y="-4.592726021557" Z="0.55" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.55" />
                  <Point X="-1.181578611776" Y="-4.939071446449" Z="0.55" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.55" />
                  <Point X="-0.882720927272" Y="-5.001135964097" Z="0.55" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.55" />
                  <Point X="-0.730727743045" Y="-4.689297171734" Z="0.55" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.55" />
                  <Point X="-0.288893792323" Y="-3.334071423541" Z="0.55" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.55" />
                  <Point X="-0.054987898336" Y="-3.221305582839" Z="0.55" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.55" />
                  <Point X="0.198371181025" Y="-3.265806462449" Z="0.55" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.55" />
                  <Point X="0.381552067109" Y="-3.467574332227" Z="0.55" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.55" />
                  <Point X="0.504027117356" Y="-3.843238858082" Z="0.55" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.55" />
                  <Point X="0.958869536907" Y="-4.988111799568" Z="0.55" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.55" />
                  <Point X="1.138194338975" Y="-4.950273085397" Z="0.55" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.55" />
                  <Point X="1.129368728641" Y="-4.579557529899" Z="0.55" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.55" />
                  <Point X="0.999480420662" Y="-3.079061313899" Z="0.55" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.55" />
                  <Point X="1.152080902669" Y="-2.9081549174" Z="0.55" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.55" />
                  <Point X="1.373642381586" Y="-2.858881749858" Z="0.55" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.55" />
                  <Point X="1.591098558143" Y="-2.961507360653" Z="0.55" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.55" />
                  <Point X="1.859748540357" Y="-3.281075552522" Z="0.55" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.55" />
                  <Point X="2.814902421543" Y="-4.227710037699" Z="0.55" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.55" />
                  <Point X="3.005441029374" Y="-4.094451444213" Z="0.55" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.55" />
                  <Point X="2.878250302367" Y="-3.773676241935" Z="0.55" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.55" />
                  <Point X="2.240681090934" Y="-2.553107464475" Z="0.55" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.55" />
                  <Point X="2.306184887768" Y="-2.365651961061" Z="0.55" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.55" />
                  <Point X="2.467246417311" Y="-2.252716422199" Z="0.55" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.55" />
                  <Point X="2.675399339038" Y="-2.262766918557" Z="0.55" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.55" />
                  <Point X="3.013737355172" Y="-2.439499197907" Z="0.55" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.55" />
                  <Point X="4.201826285361" Y="-2.852264662557" Z="0.55" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.55" />
                  <Point X="4.364594368324" Y="-2.596357815159" Z="0.55" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.55" />
                  <Point X="4.137362731491" Y="-2.33942547406" Z="0.55" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.55" />
                  <Point X="3.114070296159" Y="-1.492223081487" Z="0.55" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.55" />
                  <Point X="3.104577717351" Y="-1.324470156428" Z="0.55" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.55" />
                  <Point X="3.193917176404" Y="-1.184030266637" Z="0.55" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.55" />
                  <Point X="3.359893966189" Y="-1.1244854585" Z="0.55" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.55" />
                  <Point X="3.726525680672" Y="-1.159000517776" Z="0.55" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.55" />
                  <Point X="4.973113729206" Y="-1.024723940557" Z="0.55" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.55" />
                  <Point X="5.03573596262" Y="-0.650606336148" Z="0.55" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.55" />
                  <Point X="4.765855503511" Y="-0.493556858619" Z="0.55" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.55" />
                  <Point X="3.675520659643" Y="-0.178943619435" Z="0.55" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.55" />
                  <Point X="3.61198349068" Y="-0.111960943265" Z="0.55" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.55" />
                  <Point X="3.585450315706" Y="-0.020967908825" Z="0.55" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.55" />
                  <Point X="3.595921134719" Y="0.075642622408" Z="0.55" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.55" />
                  <Point X="3.64339594772" Y="0.15198779531" Z="0.55" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.55" />
                  <Point X="3.727874754709" Y="0.209205224807" Z="0.55" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.55" />
                  <Point X="4.030112313795" Y="0.29641507415" Z="0.55" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.55" />
                  <Point X="4.996416004442" Y="0.900573976922" Z="0.55" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.55" />
                  <Point X="4.902777105896" Y="1.318328106056" Z="0.55" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.55" />
                  <Point X="4.57310212936" Y="1.368155861545" Z="0.55" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.55" />
                  <Point X="3.389396848246" Y="1.23176768264" Z="0.55" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.55" />
                  <Point X="3.314433348405" Y="1.265162709787" Z="0.55" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.55" />
                  <Point X="3.261691188533" Y="1.330862969569" Z="0.55" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.55" />
                  <Point X="3.237426824367" Y="1.413763852676" Z="0.55" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.55" />
                  <Point X="3.250444525452" Y="1.492609679273" Z="0.55" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.55" />
                  <Point X="3.300357379864" Y="1.568334666135" Z="0.55" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.55" />
                  <Point X="3.559106135539" Y="1.773617119316" Z="0.55" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.55" />
                  <Point X="4.283572140891" Y="2.725742972922" Z="0.55" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.55" />
                  <Point X="4.053464772552" Y="3.057465181847" Z="0.55" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.55" />
                  <Point X="3.678361113791" Y="2.941622815763" Z="0.55" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.55" />
                  <Point X="2.44701662016" Y="2.2501888373" Z="0.55" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.55" />
                  <Point X="2.375234444539" Y="2.252083647671" Z="0.55" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.55" />
                  <Point X="2.310598307042" Y="2.287534762905" Z="0.55" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.55" />
                  <Point X="2.26322383733" Y="2.34642655334" Z="0.55" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.55" />
                  <Point X="2.247346055088" Y="2.414523999293" Z="0.55" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.55" />
                  <Point X="2.262339105358" Y="2.492452955722" Z="0.55" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.55" />
                  <Point X="2.454002537097" Y="2.833777968164" Z="0.55" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.55" />
                  <Point X="2.834914070046" Y="4.211133275905" Z="0.55" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.55" />
                  <Point X="2.442085335172" Y="4.450461647292" Z="0.55" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.55" />
                  <Point X="2.033391560302" Y="4.651515612293" Z="0.55" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.55" />
                  <Point X="1.609475445857" Y="4.814652303894" Z="0.55" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.55" />
                  <Point X="1.004538669664" Y="4.971949619632" Z="0.55" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.55" />
                  <Point X="0.338509439574" Y="5.061109919958" Z="0.55" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.55" />
                  <Point X="0.151303643819" Y="4.919797388493" Z="0.55" />
                  <Point X="0" Y="4.355124473572" Z="0.55" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>