<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#137" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="940" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.99528515625" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.642356811523" Y="-28.807560546875" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542363037109" Y="-28.467375" />
                  <Point X="0.510051269531" Y="-28.4208203125" />
                  <Point X="0.378635223389" Y="-28.231474609375" />
                  <Point X="0.356752349854" Y="-28.209021484375" />
                  <Point X="0.330497161865" Y="-28.18977734375" />
                  <Point X="0.302494110107" Y="-28.17566796875" />
                  <Point X="0.252493438721" Y="-28.160150390625" />
                  <Point X="0.049134975433" Y="-28.09703515625" />
                  <Point X="0.020976791382" Y="-28.092765625" />
                  <Point X="-0.008664384842" Y="-28.092765625" />
                  <Point X="-0.036825908661" Y="-28.09703515625" />
                  <Point X="-0.086826431274" Y="-28.1125546875" />
                  <Point X="-0.290184906006" Y="-28.17566796875" />
                  <Point X="-0.318184143066" Y="-28.189775390625" />
                  <Point X="-0.34443963623" Y="-28.20901953125" />
                  <Point X="-0.366324188232" Y="-28.2314765625" />
                  <Point X="-0.398635955811" Y="-28.278033203125" />
                  <Point X="-0.530051818848" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.872516479492" Y="-29.7124765625" />
                  <Point X="-0.916584777832" Y="-29.87694140625" />
                  <Point X="-1.079326660156" Y="-29.845353515625" />
                  <Point X="-1.133471679688" Y="-29.831421875" />
                  <Point X="-1.246417602539" Y="-29.802361328125" />
                  <Point X="-1.224479248047" Y="-29.635724609375" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.228453735352" Y="-29.456173828125" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.32364453125" Y="-29.131205078125" />
                  <Point X="-1.369678833008" Y="-29.090833984375" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.643028442383" Y="-28.890966796875" />
                  <Point X="-1.704125976562" Y="-28.886962890625" />
                  <Point X="-1.952617553711" Y="-28.87067578125" />
                  <Point X="-1.983414550781" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.04265637207" Y="-28.89480078125" />
                  <Point X="-2.093566162109" Y="-28.92881640625" />
                  <Point X="-2.300622802734" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.801724853516" Y="-29.089376953125" />
                  <Point X="-2.876663330078" Y="-29.03167578125" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.57067578125" Y="-27.931083984375" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405575927734" Y="-27.585189453125" />
                  <Point X="-2.414560791016" Y="-27.555572265625" />
                  <Point X="-2.428778808594" Y="-27.5267421875" />
                  <Point X="-2.446807617188" Y="-27.501583984375" />
                  <Point X="-2.46415625" Y="-27.484236328125" />
                  <Point X="-2.489316650391" Y="-27.466208984375" />
                  <Point X="-2.518145263672" Y="-27.451994140625" />
                  <Point X="-2.54776171875" Y="-27.44301171875" />
                  <Point X="-2.578693847656" Y="-27.444025390625" />
                  <Point X="-2.610219238281" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.674119384766" Y="-28.058720703125" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.082862304687" Y="-27.793857421875" />
                  <Point X="-4.136594238281" Y="-27.7037578125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.363007324219" Y="-26.6957578125" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083065429688" Y="-26.4758828125" />
                  <Point X="-3.064125" Y="-26.445849609375" />
                  <Point X="-3.055891113281" Y="-26.42491015625" />
                  <Point X="-3.0523359375" Y="-26.413962890625" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.042037597656" Y="-26.358603515625" />
                  <Point X="-3.042736816406" Y="-26.335736328125" />
                  <Point X="-3.048883300781" Y="-26.313697265625" />
                  <Point X="-3.060888916016" Y="-26.284712890625" />
                  <Point X="-3.073389892578" Y="-26.26310546875" />
                  <Point X="-3.091088623047" Y="-26.245498046875" />
                  <Point X="-3.108817626953" Y="-26.2319921875" />
                  <Point X="-3.118202392578" Y="-26.225689453125" />
                  <Point X="-3.139458740234" Y="-26.2131796875" />
                  <Point X="-3.168722167969" Y="-26.201955078125" />
                  <Point X="-3.200608398438" Y="-26.195474609375" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.5384375" Y="-26.366388671875" />
                  <Point X="-4.732102539062" Y="-26.391884765625" />
                  <Point X="-4.834077636719" Y="-25.99265625" />
                  <Point X="-4.848293945312" Y="-25.893255859375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.825697753906" Y="-25.29887109375" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.513390380859" Y="-25.2128046875" />
                  <Point X="-3.492344970703" Y="-25.201783203125" />
                  <Point X="-3.482250732422" Y="-25.195669921875" />
                  <Point X="-3.459974853516" Y="-25.180208984375" />
                  <Point X="-3.436020507812" Y="-25.158681640625" />
                  <Point X="-3.415646972656" Y="-25.12953125" />
                  <Point X="-3.406395996094" Y="-25.10891015625" />
                  <Point X="-3.402342773438" Y="-25.098185546875" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.389474365234" Y="-25.0455234375" />
                  <Point X="-3.390187255859" Y="-25.0129453125" />
                  <Point X="-3.394259765625" Y="-24.9922421875" />
                  <Point X="-3.396742431641" Y="-24.982419921875" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.417485351562" Y="-24.92916796875" />
                  <Point X="-3.439332519531" Y="-24.900802734375" />
                  <Point X="-3.456628173828" Y="-24.885482421875" />
                  <Point X="-3.465451904297" Y="-24.87855078125" />
                  <Point X="-3.487727783203" Y="-24.86308984375" />
                  <Point X="-3.501923339844" Y="-24.854953125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.723819824219" Y="-24.5230390625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.795869628906" Y="-23.917416015625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-3.975568115234" Y="-23.67257421875" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744984619141" Y="-23.700658203125" />
                  <Point X="-3.723423583984" Y="-23.698771484375" />
                  <Point X="-3.703137207031" Y="-23.694736328125" />
                  <Point X="-3.691014892578" Y="-23.6909140625" />
                  <Point X="-3.641711181641" Y="-23.675369140625" />
                  <Point X="-3.622785400391" Y="-23.667041015625" />
                  <Point X="-3.604040771484" Y="-23.656220703125" />
                  <Point X="-3.58735546875" Y="-23.64398828125" />
                  <Point X="-3.573714111328" Y="-23.62843359375" />
                  <Point X="-3.561299560547" Y="-23.610703125" />
                  <Point X="-3.551352539062" Y="-23.592572265625" />
                  <Point X="-3.54648828125" Y="-23.580830078125" />
                  <Point X="-3.526705078125" Y="-23.533068359375" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532048828125" Y="-23.410625" />
                  <Point X="-3.53791796875" Y="-23.399349609375" />
                  <Point X="-3.561788574219" Y="-23.35349609375" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-4.285264160156" Y="-22.7812265625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.349174804688" Y="-22.725525390625" />
                  <Point X="-4.081155517578" Y="-22.266345703125" />
                  <Point X="-4.005346679688" Y="-22.16890234375" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.335572753906" Y="-22.0808984375" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187729492188" Y="-22.163658203125" />
                  <Point X="-3.167087402344" Y="-22.17016796875" />
                  <Point X="-3.146791748047" Y="-22.174205078125" />
                  <Point X="-3.129908447266" Y="-22.175681640625" />
                  <Point X="-3.061242431641" Y="-22.181689453125" />
                  <Point X="-3.040560791016" Y="-22.18123828125" />
                  <Point X="-3.019102294922" Y="-22.178412109375" />
                  <Point X="-2.999013427734" Y="-22.173494140625" />
                  <Point X="-2.980464355469" Y="-22.16434765625" />
                  <Point X="-2.962210205078" Y="-22.15271875" />
                  <Point X="-2.946076416016" Y="-22.13976953125" />
                  <Point X="-2.934092529297" Y="-22.12778515625" />
                  <Point X="-2.885352783203" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435546875" Y="-21.963880859375" />
                  <Point X="-2.844912597656" Y="-21.94699609375" />
                  <Point X="-2.850920166016" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.172510009766" Y="-21.294150390625" />
                  <Point X="-3.183332763672" Y="-21.275404296875" />
                  <Point X="-3.167430175781" Y="-21.263212890625" />
                  <Point X="-2.700620117188" Y="-20.9053125" />
                  <Point X="-2.581235107422" Y="-20.838986328125" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.082660400391" Y="-20.718826171875" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028891723633" Y="-20.78519921875" />
                  <Point X="-2.012312011719" Y="-20.799111328125" />
                  <Point X="-1.995117553711" Y="-20.8106015625" />
                  <Point X="-1.976326660156" Y="-20.820384765625" />
                  <Point X="-1.899901367188" Y="-20.860169921875" />
                  <Point X="-1.88062512207" Y="-20.86766796875" />
                  <Point X="-1.859718505859" Y="-20.873271484375" />
                  <Point X="-1.839268676758" Y="-20.876419921875" />
                  <Point X="-1.818622192383" Y="-20.87506640625" />
                  <Point X="-1.797306762695" Y="-20.871306640625" />
                  <Point X="-1.777451660156" Y="-20.865517578125" />
                  <Point X="-1.757879882812" Y="-20.85741015625" />
                  <Point X="-1.678277832031" Y="-20.8244375" />
                  <Point X="-1.660145751953" Y="-20.814490234375" />
                  <Point X="-1.642416259766" Y="-20.802076171875" />
                  <Point X="-1.626864013672" Y="-20.7884375" />
                  <Point X="-1.614632568359" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.589109741211" Y="-20.713873046875" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.553941894531" Y="-20.359619140625" />
                  <Point X="-0.949622436523" Y="-20.19019140625" />
                  <Point X="-0.804903442383" Y="-20.173251953125" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.172040039062" Y="-20.571357421875" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129692078" Y="-20.741876953125" />
                  <Point X="-0.103271400452" Y="-20.768603515625" />
                  <Point X="-0.082113822937" Y="-20.791193359375" />
                  <Point X="-0.054817928314" Y="-20.805783203125" />
                  <Point X="-0.024379852295" Y="-20.816115234375" />
                  <Point X="0.006156134129" Y="-20.82115625" />
                  <Point X="0.036692108154" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094426101685" Y="-20.791193359375" />
                  <Point X="0.115583656311" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.301322906494" Y="-20.13481640625" />
                  <Point X="0.307419677734" Y="-20.1120625" />
                  <Point X="0.316387481689" Y="-20.113001953125" />
                  <Point X="0.844043457031" Y="-20.16826171875" />
                  <Point X="0.963786621094" Y="-20.197171875" />
                  <Point X="1.481024780273" Y="-20.322048828125" />
                  <Point X="1.557779418945" Y="-20.349888671875" />
                  <Point X="1.894647705078" Y="-20.472072265625" />
                  <Point X="1.970016601562" Y="-20.5073203125" />
                  <Point X="2.294558837891" Y="-20.659099609375" />
                  <Point X="2.367419677734" Y="-20.701546875" />
                  <Point X="2.680983642578" Y="-20.884228515625" />
                  <Point X="2.749651123047" Y="-20.9330625" />
                  <Point X="2.943260009766" Y="-21.07074609375" />
                  <Point X="2.317623535156" Y="-22.154380859375" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.139659667969" Y="-22.466568359375" />
                  <Point X="2.130421142578" Y="-22.494498046875" />
                  <Point X="2.12883984375" Y="-22.4997890625" />
                  <Point X="2.111607177734" Y="-22.56423046875" />
                  <Point X="2.10838671875" Y="-22.589689453125" />
                  <Point X="2.108701171875" Y="-22.62229296875" />
                  <Point X="2.109379882812" Y="-22.63275" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.140070800781" Y="-22.752529296875" />
                  <Point X="2.148548583984" Y="-22.7650234375" />
                  <Point X="2.183028564453" Y="-22.815837890625" />
                  <Point X="2.200351074219" Y="-22.83508203125" />
                  <Point X="2.226144775391" Y="-22.856861328125" />
                  <Point X="2.234092041016" Y="-22.86288671875" />
                  <Point X="2.284906738281" Y="-22.8973671875" />
                  <Point X="2.304961914062" Y="-22.907734375" />
                  <Point X="2.327056640625" Y="-22.916" />
                  <Point X="2.348975585938" Y="-22.92133984375" />
                  <Point X="2.362669189453" Y="-22.922990234375" />
                  <Point X="2.418393066406" Y="-22.9297109375" />
                  <Point X="2.444812988281" Y="-22.929193359375" />
                  <Point X="2.479561279297" Y="-22.9236171875" />
                  <Point X="2.489050537109" Y="-22.921591796875" />
                  <Point X="2.553492431641" Y="-22.904359375" />
                  <Point X="2.565286376953" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.786394775391" Y="-22.19826953125" />
                  <Point X="3.967326171875" Y="-22.09380859375" />
                  <Point X="4.12326953125" Y="-22.310533203125" />
                  <Point X="4.161556640625" Y="-22.3738046875" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.454660888672" Y="-23.15976171875" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.216439208984" Y="-23.3451484375" />
                  <Point X="3.195791259766" Y="-23.369275390625" />
                  <Point X="3.192571289062" Y="-23.37325" />
                  <Point X="3.146192382812" Y="-23.43375390625" />
                  <Point X="3.133253173828" Y="-23.456595703125" />
                  <Point X="3.120535644531" Y="-23.488736328125" />
                  <Point X="3.117382080078" Y="-23.498103515625" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.101226318359" Y="-23.645130859375" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.124324951172" Y="-23.738806640625" />
                  <Point X="3.141007324219" Y="-23.7706015625" />
                  <Point X="3.145767333984" Y="-23.778677734375" />
                  <Point X="3.184340820312" Y="-23.837306640625" />
                  <Point X="3.198895996094" Y="-23.854552734375" />
                  <Point X="3.216138916016" Y="-23.870640625" />
                  <Point X="3.234347900391" Y="-23.88396484375" />
                  <Point X="3.248091796875" Y="-23.891701171875" />
                  <Point X="3.303989990234" Y="-23.92316796875" />
                  <Point X="3.329188232422" Y="-23.93294140625" />
                  <Point X="3.365747070312" Y="-23.94139453125" />
                  <Point X="3.374701416016" Y="-23.943017578125" />
                  <Point X="3.450279541016" Y="-23.953005859375" />
                  <Point X="3.462697998047" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.626091308594" Y="-23.8032109375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.858000976562" Y="-24.144681640625" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.973147460938" Y="-24.601662109375" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704786621094" Y="-24.674416015625" />
                  <Point X="3.681546630859" Y="-24.68493359375" />
                  <Point X="3.663289794922" Y="-24.695486328125" />
                  <Point X="3.589036621094" Y="-24.738404296875" />
                  <Point X="3.567971923828" Y="-24.754939453125" />
                  <Point X="3.542705078125" Y="-24.781318359375" />
                  <Point X="3.536576660156" Y="-24.788380859375" />
                  <Point X="3.492024902344" Y="-24.845150390625" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.460029296875" Y="-24.926462890625" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443921386719" Y="-25.030994140625" />
                  <Point X="3.447572998047" Y="-25.0688671875" />
                  <Point X="3.448830078125" Y="-25.077619140625" />
                  <Point X="3.463680664062" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.1766640625" />
                  <Point X="3.480300292969" Y="-25.19812890625" />
                  <Point X="3.492023193359" Y="-25.217408203125" />
                  <Point X="3.502977294922" Y="-25.2313671875" />
                  <Point X="3.547529052734" Y="-25.28813671875" />
                  <Point X="3.560000244141" Y="-25.30123828125" />
                  <Point X="3.589035888672" Y="-25.32415625" />
                  <Point X="3.607292724609" Y="-25.334708984375" />
                  <Point X="3.681545898438" Y="-25.37762890625" />
                  <Point X="3.692709228516" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.76007421875" Y="-25.671755859375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.855021484375" Y="-25.948732421875" />
                  <Point X="4.839565429688" Y="-26.016462890625" />
                  <Point X="4.801174316406" Y="-26.184697265625" />
                  <Point X="3.721928466797" Y="-26.04261328125" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035400391" Y="-26.0027109375" />
                  <Point X="3.374661132812" Y="-26.0055078125" />
                  <Point X="3.338829345703" Y="-26.013294921875" />
                  <Point X="3.193096923828" Y="-26.044970703125" />
                  <Point X="3.163973144531" Y="-26.05659765625" />
                  <Point X="3.136146728516" Y="-26.073490234375" />
                  <Point X="3.112396484375" Y="-26.0939609375" />
                  <Point X="3.09073828125" Y="-26.120009765625" />
                  <Point X="3.00265234375" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.966653320312" Y="-26.339099609375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="2.996280517578" Y="-26.598841796875" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="4.078997802734" Y="-27.503966796875" />
                  <Point X="4.213121582031" Y="-27.606884765625" />
                  <Point X="4.124810546875" Y="-27.74978515625" />
                  <Point X="4.092842773438" Y="-27.79520703125" />
                  <Point X="4.028980712891" Y="-27.8859453125" />
                  <Point X="3.065959960938" Y="-27.3299453125" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754223388672" Y="-27.159826171875" />
                  <Point X="2.711577880859" Y="-27.152125" />
                  <Point X="2.538133056641" Y="-27.12080078125" />
                  <Point X="2.506784179688" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444834960938" Y="-27.135177734375" />
                  <Point X="2.409406982422" Y="-27.153822265625" />
                  <Point X="2.265316894531" Y="-27.22965625" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.185886474609" Y="-27.3258671875" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.103376953125" Y="-27.605904296875" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.774093994141" Y="-28.903890625" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.781859619141" Y="-29.11163671875" />
                  <Point X="2.746108886719" Y="-29.134779296875" />
                  <Point X="2.701764404297" Y="-29.163482421875" />
                  <Point X="1.960769775391" Y="-28.197798828125" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506469727" Y="-27.922181640625" />
                  <Point X="1.721924316406" Y="-27.900556640625" />
                  <Point X="1.679864257812" Y="-27.873515625" />
                  <Point X="1.50880078125" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.75116796875" />
                  <Point X="1.448365844727" Y="-27.7434375" />
                  <Point X="1.417100830078" Y="-27.741119140625" />
                  <Point X="1.371101196289" Y="-27.7453515625" />
                  <Point X="1.184014038086" Y="-27.76256640625" />
                  <Point X="1.156367797852" Y="-27.769396484375" />
                  <Point X="1.128982299805" Y="-27.78073828125" />
                  <Point X="1.10459375" Y="-27.7954609375" />
                  <Point X="1.069073974609" Y="-27.82499609375" />
                  <Point X="0.924609985352" Y="-27.94511328125" />
                  <Point X="0.904140014648" Y="-27.968865234375" />
                  <Point X="0.887247680664" Y="-27.996693359375" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.86500402832" Y="-28.074671875" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.996090087891" Y="-29.662615234375" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.975685668945" Y="-29.87008203125" />
                  <Point X="0.942653625488" Y="-29.876083984375" />
                  <Point X="0.929315734863" Y="-29.8785078125" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058415283203" Y="-29.752638671875" />
                  <Point X="-1.109798950195" Y="-29.73941796875" />
                  <Point X="-1.141245849609" Y="-29.731326171875" />
                  <Point X="-1.130291992188" Y="-29.648125" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.54103515625" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123333984375" Y="-29.497693359375" />
                  <Point X="-1.135279052734" Y="-29.437640625" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124633789" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230575195312" Y="-29.094859375" />
                  <Point X="-1.250209716797" Y="-29.070935546875" />
                  <Point X="-1.261006591797" Y="-29.05978125" />
                  <Point X="-1.307040893555" Y="-29.01941015625" />
                  <Point X="-1.494267578125" Y="-28.855216796875" />
                  <Point X="-1.506735717773" Y="-28.84596875" />
                  <Point X="-1.53301940918" Y="-28.829623046875" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576531982422" Y="-28.810224609375" />
                  <Point X="-1.59131640625" Y="-28.805474609375" />
                  <Point X="-1.621458496094" Y="-28.798447265625" />
                  <Point X="-1.636816162109" Y="-28.796169921875" />
                  <Point X="-1.697913696289" Y="-28.792166015625" />
                  <Point X="-1.946405395508" Y="-28.77587890625" />
                  <Point X="-1.961929199219" Y="-28.7761328125" />
                  <Point X="-1.992726074219" Y="-28.779166015625" />
                  <Point X="-2.007999267578" Y="-28.7819453125" />
                  <Point X="-2.039047973633" Y="-28.790263671875" />
                  <Point X="-2.053668701172" Y="-28.795494140625" />
                  <Point X="-2.081861816406" Y="-28.808267578125" />
                  <Point X="-2.095434326172" Y="-28.815810546875" />
                  <Point X="-2.146343994141" Y="-28.849826171875" />
                  <Point X="-2.353400634766" Y="-28.988177734375" />
                  <Point X="-2.359678466797" Y="-28.99275390625" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471191406" Y="-29.041630859375" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.747609619141" Y="-29.011146484375" />
                  <Point X="-2.818705322266" Y="-28.956404296875" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.488403320312" Y="-27.978583984375" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.619232421875" />
                  <Point X="-2.310626708984" Y="-27.588296875" />
                  <Point X="-2.314666992188" Y="-27.557611328125" />
                  <Point X="-2.323651855469" Y="-27.527994140625" />
                  <Point X="-2.329358642578" Y="-27.513552734375" />
                  <Point X="-2.343576660156" Y="-27.48472265625" />
                  <Point X="-2.351559326172" Y="-27.47140625" />
                  <Point X="-2.369588134766" Y="-27.446248046875" />
                  <Point X="-2.379634277344" Y="-27.43440625" />
                  <Point X="-2.396982910156" Y="-27.41705859375" />
                  <Point X="-2.408825683594" Y="-27.40701171875" />
                  <Point X="-2.433986083984" Y="-27.388984375" />
                  <Point X="-2.447303710938" Y="-27.38100390625" />
                  <Point X="-2.476132324219" Y="-27.3667890625" />
                  <Point X="-2.490572753906" Y="-27.361083984375" />
                  <Point X="-2.520189208984" Y="-27.3521015625" />
                  <Point X="-2.550873291016" Y="-27.3480625" />
                  <Point X="-2.581805419922" Y="-27.349076171875" />
                  <Point X="-2.597229492188" Y="-27.3508515625" />
                  <Point X="-2.628754882813" Y="-27.357123046875" />
                  <Point X="-2.643685546875" Y="-27.36138671875" />
                  <Point X="-2.672649902344" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.721619384766" Y="-27.97644921875" />
                  <Point X="-3.793087890625" Y="-28.0177109375" />
                  <Point X="-4.004017089844" Y="-27.740591796875" />
                  <Point X="-4.055001708984" Y="-27.655099609375" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.305175048828" Y="-26.771126953125" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039158691406" Y="-26.566068359375" />
                  <Point X="-3.01626953125" Y="-26.543435546875" />
                  <Point X="-3.002710205078" Y="-26.52655859375" />
                  <Point X="-2.983769775391" Y="-26.496525390625" />
                  <Point X="-2.975714599609" Y="-26.480615234375" />
                  <Point X="-2.967480712891" Y="-26.45967578125" />
                  <Point X="-2.960370361328" Y="-26.43778125" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951952880859" Y="-26.402396484375" />
                  <Point X="-2.947838623047" Y="-26.3709140625" />
                  <Point X="-2.94708203125" Y="-26.35569921875" />
                  <Point X="-2.94778125" Y="-26.33283203125" />
                  <Point X="-2.951229003906" Y="-26.31021484375" />
                  <Point X="-2.957375488281" Y="-26.28817578125" />
                  <Point X="-2.961114746094" Y="-26.277341796875" />
                  <Point X="-2.973120361328" Y="-26.248357421875" />
                  <Point X="-2.978659179688" Y="-26.237138671875" />
                  <Point X="-2.99116015625" Y="-26.21553125" />
                  <Point X="-3.006388671875" Y="-26.195755859375" />
                  <Point X="-3.024087402344" Y="-26.1781484375" />
                  <Point X="-3.033519775391" Y="-26.169927734375" />
                  <Point X="-3.051248779297" Y="-26.156421875" />
                  <Point X="-3.070018310547" Y="-26.14381640625" />
                  <Point X="-3.091274658203" Y="-26.131306640625" />
                  <Point X="-3.105436523438" Y="-26.12448046875" />
                  <Point X="-3.134699951172" Y="-26.113255859375" />
                  <Point X="-3.149801513672" Y="-26.108857421875" />
                  <Point X="-3.181687744141" Y="-26.102376953125" />
                  <Point X="-3.1973046875" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.550837402344" Y="-26.272201171875" />
                  <Point X="-4.660920898438" Y="-26.286693359375" />
                  <Point X="-4.740762695312" Y="-25.9741171875" />
                  <Point X="-4.754250976562" Y="-25.879806640625" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.801109863281" Y="-25.390634765625" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.498334228516" Y="-25.308908203125" />
                  <Point X="-3.478848632812" Y="-25.301302734375" />
                  <Point X="-3.469316894531" Y="-25.296962890625" />
                  <Point X="-3.448271484375" Y="-25.28594140625" />
                  <Point X="-3.428083007812" Y="-25.27371484375" />
                  <Point X="-3.405807128906" Y="-25.25825390625" />
                  <Point X="-3.396474609375" Y="-25.250869140625" />
                  <Point X="-3.372520263672" Y="-25.229341796875" />
                  <Point X="-3.358153564453" Y="-25.213103515625" />
                  <Point X="-3.337780029297" Y="-25.183953125" />
                  <Point X="-3.328969726562" Y="-25.168416015625" />
                  <Point X="-3.31971875" Y="-25.147794921875" />
                  <Point X="-3.311612304688" Y="-25.126345703125" />
                  <Point X="-3.304187011719" Y="-25.102421875" />
                  <Point X="-3.301576904297" Y="-25.09194140625" />
                  <Point X="-3.296133789062" Y="-25.063203125" />
                  <Point X="-3.294497070312" Y="-25.0434453125" />
                  <Point X="-3.295209960938" Y="-25.0108671875" />
                  <Point X="-3.296973632812" Y="-24.994609375" />
                  <Point X="-3.301046142578" Y="-24.97390625" />
                  <Point X="-3.306011474609" Y="-24.95426171875" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.317669189453" Y="-24.919212890625" />
                  <Point X="-3.330986816406" Y="-24.88988671875" />
                  <Point X="-3.342221679688" Y="-24.87119921875" />
                  <Point X="-3.364068847656" Y="-24.842833984375" />
                  <Point X="-3.376341064453" Y="-24.829689453125" />
                  <Point X="-3.39363671875" Y="-24.814369140625" />
                  <Point X="-3.411284179688" Y="-24.800505859375" />
                  <Point X="-3.433560058594" Y="-24.785044921875" />
                  <Point X="-3.440485351562" Y="-24.780669921875" />
                  <Point X="-3.465612548828" Y="-24.767166015625" />
                  <Point X="-3.496565185547" Y="-24.75436328125" />
                  <Point X="-3.508288330078" Y="-24.75038671875" />
                  <Point X="-4.699231933594" Y="-24.431275390625" />
                  <Point X="-4.785446289062" Y="-24.408173828125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.704176757812" Y="-23.942263671875" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-3.987968261719" Y="-23.76676171875" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747056640625" Y="-23.795634765625" />
                  <Point X="-3.736703125" Y="-23.795296875" />
                  <Point X="-3.715142089844" Y="-23.79341015625" />
                  <Point X="-3.704890136719" Y="-23.7919453125" />
                  <Point X="-3.684603759766" Y="-23.78791015625" />
                  <Point X="-3.662447021484" Y="-23.781517578125" />
                  <Point X="-3.613143310547" Y="-23.76597265625" />
                  <Point X="-3.603447998047" Y="-23.762322265625" />
                  <Point X="-3.584522216797" Y="-23.753994140625" />
                  <Point X="-3.575291748047" Y="-23.74931640625" />
                  <Point X="-3.556547119141" Y="-23.73849609375" />
                  <Point X="-3.547871582031" Y="-23.7328359375" />
                  <Point X="-3.531186279297" Y="-23.720603515625" />
                  <Point X="-3.515931396484" Y="-23.706626953125" />
                  <Point X="-3.502290039063" Y="-23.691072265625" />
                  <Point X="-3.495893798828" Y="-23.682921875" />
                  <Point X="-3.483479248047" Y="-23.66519140625" />
                  <Point X="-3.478010742188" Y="-23.656396484375" />
                  <Point X="-3.468063720703" Y="-23.638265625" />
                  <Point X="-3.458720947266" Y="-23.6171875" />
                  <Point X="-3.438937744141" Y="-23.56942578125" />
                  <Point X="-3.435500244141" Y="-23.55965234375" />
                  <Point X="-3.429710693359" Y="-23.5397890625" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436011962891" Y="-23.395470703125" />
                  <Point X="-3.443507324219" Y="-23.3761953125" />
                  <Point X="-3.453650634766" Y="-23.355486328125" />
                  <Point X="-3.477521240234" Y="-23.3096328125" />
                  <Point X="-3.482798339844" Y="-23.30071875" />
                  <Point X="-3.494291503906" Y="-23.283517578125" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521498046875" Y="-23.251091796875" />
                  <Point X="-3.536439941406" Y="-23.236787109375" />
                  <Point X="-3.544303466797" Y="-23.230041015625" />
                  <Point X="-4.227431640625" Y="-22.705857421875" />
                  <Point X="-4.227613769531" Y="-22.70571875" />
                  <Point X="-4.002293457031" Y="-22.31969140625" />
                  <Point X="-3.930365478516" Y="-22.227236328125" />
                  <Point X="-3.726336914062" Y="-21.964986328125" />
                  <Point X="-3.383072753906" Y="-22.163169921875" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244924804688" Y="-22.242279296875" />
                  <Point X="-3.225997314453" Y="-22.250609375" />
                  <Point X="-3.216302001953" Y="-22.254259765625" />
                  <Point X="-3.195659912109" Y="-22.26076953125" />
                  <Point X="-3.18562109375" Y="-22.263341796875" />
                  <Point X="-3.165325439453" Y="-22.26737890625" />
                  <Point X="-3.138185302734" Y="-22.2703203125" />
                  <Point X="-3.069519287109" Y="-22.276328125" />
                  <Point X="-3.059170410156" Y="-22.276666015625" />
                  <Point X="-3.038488769531" Y="-22.27621484375" />
                  <Point X="-3.028156005859" Y="-22.27542578125" />
                  <Point X="-3.006697509766" Y="-22.272599609375" />
                  <Point X="-2.996512451172" Y="-22.2706875" />
                  <Point X="-2.976423583984" Y="-22.26576953125" />
                  <Point X="-2.956999267578" Y="-22.25869921875" />
                  <Point X="-2.938450195312" Y="-22.249552734375" />
                  <Point X="-2.929421630859" Y="-22.244470703125" />
                  <Point X="-2.911167480469" Y="-22.232841796875" />
                  <Point X="-2.90274609375" Y="-22.226806640625" />
                  <Point X="-2.886612304688" Y="-22.213857421875" />
                  <Point X="-2.866916015625" Y="-22.194958984375" />
                  <Point X="-2.818176269531" Y="-22.146220703125" />
                  <Point X="-2.811265136719" Y="-22.13851171875" />
                  <Point X="-2.798321289062" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.986634765625" />
                  <Point X="-2.748458251953" Y="-21.965955078125" />
                  <Point X="-2.750273925781" Y="-21.938716796875" />
                  <Point X="-2.756281494141" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761781005859" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.05938671875" Y="-21.3000859375" />
                  <Point X="-2.6483671875" Y="-20.984958984375" />
                  <Point X="-2.535098388672" Y="-20.92203125" />
                  <Point X="-2.192523925781" Y="-20.731703125" />
                  <Point X="-2.158029052734" Y="-20.776658203125" />
                  <Point X="-2.118563964844" Y="-20.82808984375" />
                  <Point X="-2.111819091797" Y="-20.835953125" />
                  <Point X="-2.097515625" Y="-20.85089453125" />
                  <Point X="-2.089956787109" Y="-20.85797265625" />
                  <Point X="-2.073376953125" Y="-20.871884765625" />
                  <Point X="-2.065095214844" Y="-20.87809765625" />
                  <Point X="-2.047900756836" Y="-20.889587890625" />
                  <Point X="-2.03898815918" Y="-20.894865234375" />
                  <Point X="-2.020197143555" Y="-20.9046484375" />
                  <Point X="-1.943772094727" Y="-20.94443359375" />
                  <Point X="-1.934340698242" Y="-20.94870703125" />
                  <Point X="-1.915064453125" Y="-20.956205078125" />
                  <Point X="-1.905219604492" Y="-20.9594296875" />
                  <Point X="-1.884312866211" Y="-20.965033203125" />
                  <Point X="-1.874174316406" Y="-20.967166015625" />
                  <Point X="-1.853724487305" Y="-20.970314453125" />
                  <Point X="-1.833054077148" Y="-20.971216796875" />
                  <Point X="-1.812407470703" Y="-20.96986328125" />
                  <Point X="-1.802120117188" Y="-20.968623046875" />
                  <Point X="-1.7808046875" Y="-20.96486328125" />
                  <Point X="-1.770715209961" Y="-20.962509765625" />
                  <Point X="-1.750860107422" Y="-20.956720703125" />
                  <Point X="-1.741094726562" Y="-20.95328515625" />
                  <Point X="-1.721522949219" Y="-20.945177734375" />
                  <Point X="-1.641920898438" Y="-20.912205078125" />
                  <Point X="-1.632585083008" Y="-20.9077265625" />
                  <Point X="-1.61445300293" Y="-20.897779296875" />
                  <Point X="-1.605656738281" Y="-20.892310546875" />
                  <Point X="-1.587927246094" Y="-20.879896484375" />
                  <Point X="-1.579779296875" Y="-20.873501953125" />
                  <Point X="-1.564226928711" Y="-20.85986328125" />
                  <Point X="-1.550251708984" Y="-20.844611328125" />
                  <Point X="-1.538020263672" Y="-20.8279296875" />
                  <Point X="-1.532359863281" Y="-20.819255859375" />
                  <Point X="-1.521538085938" Y="-20.80051171875" />
                  <Point X="-1.51685546875" Y="-20.791271484375" />
                  <Point X="-1.508525024414" Y="-20.772337890625" />
                  <Point X="-1.504877075195" Y="-20.76264453125" />
                  <Point X="-1.498506469727" Y="-20.742439453125" />
                  <Point X="-1.472597412109" Y="-20.660267578125" />
                  <Point X="-1.470026489258" Y="-20.650236328125" />
                  <Point X="-1.465991210938" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-0.93116015625" Y="-20.283677734375" />
                  <Point X="-0.793858947754" Y="-20.267607421875" />
                  <Point X="-0.36522265625" Y="-20.21744140625" />
                  <Point X="-0.263802978516" Y="-20.5959453125" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.22043510437" Y="-20.752892578125" />
                  <Point X="-0.207661407471" Y="-20.781083984375" />
                  <Point X="-0.200119094849" Y="-20.79465625" />
                  <Point X="-0.18226071167" Y="-20.8213828125" />
                  <Point X="-0.172608703613" Y="-20.833544921875" />
                  <Point X="-0.151451034546" Y="-20.856134765625" />
                  <Point X="-0.126896308899" Y="-20.8749765625" />
                  <Point X="-0.099600372314" Y="-20.88956640625" />
                  <Point X="-0.08535382843" Y="-20.8957421875" />
                  <Point X="-0.054915859222" Y="-20.90607421875" />
                  <Point X="-0.039853370667" Y="-20.909845703125" />
                  <Point X="-0.009317459106" Y="-20.91488671875" />
                  <Point X="0.021629692078" Y="-20.91488671875" />
                  <Point X="0.052165752411" Y="-20.909845703125" />
                  <Point X="0.067228240967" Y="-20.90607421875" />
                  <Point X="0.097666061401" Y="-20.8957421875" />
                  <Point X="0.111912162781" Y="-20.88956640625" />
                  <Point X="0.139208236694" Y="-20.8749765625" />
                  <Point X="0.163763427734" Y="-20.856134765625" />
                  <Point X="0.184920928955" Y="-20.833544921875" />
                  <Point X="0.194573242188" Y="-20.8213828125" />
                  <Point X="0.21243132019" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.378190795898" Y="-20.214994140625" />
                  <Point X="0.827880249023" Y="-20.262087890625" />
                  <Point X="0.941490844727" Y="-20.289517578125" />
                  <Point X="1.45358972168" Y="-20.413154296875" />
                  <Point X="1.52538671875" Y="-20.4391953125" />
                  <Point X="1.858249633789" Y="-20.55992578125" />
                  <Point X="1.929771362305" Y="-20.593375" />
                  <Point X="2.250440429688" Y="-20.74334375" />
                  <Point X="2.319598144531" Y="-20.7836328125" />
                  <Point X="2.629443847656" Y="-20.9641484375" />
                  <Point X="2.694593505859" Y="-21.01048046875" />
                  <Point X="2.817780029297" Y="-21.098083984375" />
                  <Point X="2.235351074219" Y="-22.106880859375" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.060896484375" Y="-22.410033203125" />
                  <Point X="2.049465820313" Y="-22.436734375" />
                  <Point X="2.040227294922" Y="-22.4646640625" />
                  <Point X="2.037064697266" Y="-22.47524609375" />
                  <Point X="2.01983203125" Y="-22.5396875" />
                  <Point X="2.017358276367" Y="-22.55230859375" />
                  <Point X="2.014137817383" Y="-22.577767578125" />
                  <Point X="2.013391235352" Y="-22.59060546875" />
                  <Point X="2.013705566406" Y="-22.623208984375" />
                  <Point X="2.015062988281" Y="-22.644123046875" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144042969" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045317871094" Y="-22.776111328125" />
                  <Point X="2.055680175781" Y="-22.79615625" />
                  <Point X="2.069937255859" Y="-22.818365234375" />
                  <Point X="2.104417236328" Y="-22.8691796875" />
                  <Point X="2.112420654297" Y="-22.87939453125" />
                  <Point X="2.129743164062" Y="-22.898638671875" />
                  <Point X="2.139062255859" Y="-22.90766796875" />
                  <Point X="2.164855957031" Y="-22.929447265625" />
                  <Point X="2.180750488281" Y="-22.941498046875" />
                  <Point X="2.231565185547" Y="-22.975978515625" />
                  <Point X="2.241281982422" Y="-22.9817578125" />
                  <Point X="2.261337158203" Y="-22.992125" />
                  <Point X="2.271675537109" Y="-22.996712890625" />
                  <Point X="2.293770263672" Y="-23.004978515625" />
                  <Point X="2.304570556641" Y="-23.00830078125" />
                  <Point X="2.326489501953" Y="-23.013640625" />
                  <Point X="2.351301757812" Y="-23.01730859375" />
                  <Point X="2.407025634766" Y="-23.024029296875" />
                  <Point X="2.42025390625" Y="-23.024693359375" />
                  <Point X="2.446673828125" Y="-23.02417578125" />
                  <Point X="2.459865478516" Y="-23.022994140625" />
                  <Point X="2.494613769531" Y="-23.01741796875" />
                  <Point X="2.513592285156" Y="-23.0133671875" />
                  <Point X="2.578034179688" Y="-22.996134765625" />
                  <Point X="2.583991943359" Y="-22.994330078125" />
                  <Point X="2.604414794922" Y="-22.9869296875" />
                  <Point X="2.627662353516" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.833894775391" Y="-22.280541015625" />
                  <Point X="3.940404296875" Y="-22.219046875" />
                  <Point X="4.043947998047" Y="-22.36294921875" />
                  <Point X="4.080279052734" Y="-22.42298828125" />
                  <Point X="4.136883789062" Y="-22.516529296875" />
                  <Point X="3.396828613281" Y="-23.084392578125" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.165423095703" Y="-23.262607421875" />
                  <Point X="3.144262207031" Y="-23.28337890625" />
                  <Point X="3.123614257812" Y="-23.307505859375" />
                  <Point X="3.117174316406" Y="-23.315455078125" />
                  <Point X="3.070795410156" Y="-23.375958984375" />
                  <Point X="3.063533447266" Y="-23.3869296875" />
                  <Point X="3.050594238281" Y="-23.409771484375" />
                  <Point X="3.044916992188" Y="-23.421642578125" />
                  <Point X="3.032199462891" Y="-23.453783203125" />
                  <Point X="3.025892333984" Y="-23.472517578125" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.008186279297" Y="-23.664328125" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.025953125" Y="-23.745845703125" />
                  <Point X="3.034869628906" Y="-23.770787109375" />
                  <Point X="3.040201416016" Y="-23.7829453125" />
                  <Point X="3.056883789063" Y="-23.814740234375" />
                  <Point X="3.066403808594" Y="-23.830892578125" />
                  <Point X="3.104977294922" Y="-23.889521484375" />
                  <Point X="3.111740966797" Y="-23.898578125" />
                  <Point X="3.126296142578" Y="-23.91582421875" />
                  <Point X="3.134087646484" Y="-23.924013671875" />
                  <Point X="3.151330566406" Y="-23.9401015625" />
                  <Point X="3.160038818359" Y="-23.947306640625" />
                  <Point X="3.178247802734" Y="-23.960630859375" />
                  <Point X="3.201492431641" Y="-23.974486328125" />
                  <Point X="3.257390625" Y="-24.005953125" />
                  <Point X="3.26963671875" Y="-24.01173828125" />
                  <Point X="3.294834960938" Y="-24.02151171875" />
                  <Point X="3.307787109375" Y="-24.0255" />
                  <Point X="3.344345947266" Y="-24.033953125" />
                  <Point X="3.362254638672" Y="-24.03719921875" />
                  <Point X="3.437832763672" Y="-24.0471875" />
                  <Point X="3.444032714844" Y="-24.04780078125" />
                  <Point X="3.465708251953" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.638491210938" Y="-23.8973984375" />
                  <Point X="4.704704101562" Y="-23.888681640625" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.764131835938" Y="-24.159296875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="3.948559570313" Y="-24.5098984375" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.686024414062" Y="-24.580458984375" />
                  <Point X="3.665617431641" Y="-24.5878671875" />
                  <Point X="3.642377441406" Y="-24.598384765625" />
                  <Point X="3.634005615234" Y="-24.602685546875" />
                  <Point X="3.615748779297" Y="-24.61323828125" />
                  <Point X="3.541495605469" Y="-24.65615625" />
                  <Point X="3.530377929688" Y="-24.663677734375" />
                  <Point X="3.509313232422" Y="-24.680212890625" />
                  <Point X="3.499366210938" Y="-24.6892265625" />
                  <Point X="3.474099365234" Y="-24.71560546875" />
                  <Point X="3.461842529297" Y="-24.72973046875" />
                  <Point X="3.417290771484" Y="-24.7865" />
                  <Point X="3.410854003906" Y="-24.795791015625" />
                  <Point X="3.399129394531" Y="-24.815072265625" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.380004150391" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.366724853516" Y="-24.90859375" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.35028125" Y="-24.999587890625" />
                  <Point X="3.349024169922" Y="-25.02657421875" />
                  <Point X="3.349359863281" Y="-25.040111328125" />
                  <Point X="3.353011474609" Y="-25.077984375" />
                  <Point X="3.355525634766" Y="-25.09548828125" />
                  <Point X="3.370376220703" Y="-25.173033203125" />
                  <Point X="3.373158447266" Y="-25.183986328125" />
                  <Point X="3.380004150391" Y="-25.205486328125" />
                  <Point X="3.384067626953" Y="-25.216033203125" />
                  <Point X="3.393841552734" Y="-25.237498046875" />
                  <Point X="3.399128417969" Y="-25.247486328125" />
                  <Point X="3.410851318359" Y="-25.266765625" />
                  <Point X="3.428241455078" Y="-25.290015625" />
                  <Point X="3.472793212891" Y="-25.34678515625" />
                  <Point X="3.478718994141" Y="-25.35363671875" />
                  <Point X="3.501141845703" Y="-25.37580859375" />
                  <Point X="3.530177490234" Y="-25.3987265625" />
                  <Point X="3.541494873047" Y="-25.406404296875" />
                  <Point X="3.559751708984" Y="-25.41695703125" />
                  <Point X="3.634004882812" Y="-25.459876953125" />
                  <Point X="3.639488037109" Y="-25.4628125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027099609" Y="-25.481029296875" />
                  <Point X="3.6919921875" Y="-25.483916015625" />
                  <Point X="4.735486328125" Y="-25.76351953125" />
                  <Point X="4.784876464844" Y="-25.77675390625" />
                  <Point X="4.761612304688" Y="-25.931060546875" />
                  <Point X="4.746946289063" Y="-25.995328125" />
                  <Point X="4.727802734375" Y="-26.07921875" />
                  <Point X="3.734328369141" Y="-25.94842578125" />
                  <Point X="3.436781738281" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400101806641" Y="-25.90804296875" />
                  <Point X="3.366727539062" Y="-25.91083984375" />
                  <Point X="3.354486328125" Y="-25.912673828125" />
                  <Point X="3.318654541016" Y="-25.9204609375" />
                  <Point X="3.172922119141" Y="-25.95213671875" />
                  <Point X="3.157873779297" Y="-25.9567421875" />
                  <Point X="3.12875" Y="-25.968369140625" />
                  <Point X="3.114674560547" Y="-25.975390625" />
                  <Point X="3.086848144531" Y="-25.992283203125" />
                  <Point X="3.074123779297" Y="-26.00153125" />
                  <Point X="3.050373535156" Y="-26.022001953125" />
                  <Point X="3.03934765625" Y="-26.033224609375" />
                  <Point X="3.017689453125" Y="-26.0592734375" />
                  <Point X="2.929603515625" Y="-26.165212890625" />
                  <Point X="2.921324707031" Y="-26.176849609375" />
                  <Point X="2.906605224609" Y="-26.20123046875" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.872052978516" Y="-26.33039453125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.916370605469" Y="-26.650216796875" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="4.021165527344" Y="-27.5793359375" />
                  <Point X="4.087169921875" Y="-27.629984375" />
                  <Point X="4.045486572266" Y="-27.69743359375" />
                  <Point X="4.015154541016" Y="-27.74053125" />
                  <Point X="4.001274169922" Y="-27.76025390625" />
                  <Point X="3.113459960938" Y="-27.247673828125" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841191650391" Y="-27.09088671875" />
                  <Point X="2.815025390625" Y="-27.079515625" />
                  <Point X="2.783119384766" Y="-27.069328125" />
                  <Point X="2.771105957031" Y="-27.066337890625" />
                  <Point X="2.728460449219" Y="-27.05863671875" />
                  <Point X="2.555015625" Y="-27.0273125" />
                  <Point X="2.539358154297" Y="-27.02580859375" />
                  <Point X="2.508009277344" Y="-27.025404296875" />
                  <Point X="2.492317871094" Y="-27.02650390625" />
                  <Point X="2.460145019531" Y="-27.0314609375" />
                  <Point X="2.444845947266" Y="-27.03513671875" />
                  <Point X="2.415069580078" Y="-27.0449609375" />
                  <Point X="2.400592285156" Y="-27.051109375" />
                  <Point X="2.365164306641" Y="-27.06975390625" />
                  <Point X="2.22107421875" Y="-27.145587890625" />
                  <Point X="2.208972167969" Y="-27.153169921875" />
                  <Point X="2.186040771484" Y="-27.1700625" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144940429687" Y="-27.21116015625" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.101818603516" Y="-27.281623046875" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.0021875" Y="-27.580140625" />
                  <Point X="2.009889160156" Y="-27.622787109375" />
                  <Point X="2.041213134766" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.691821533203" Y="-28.951390625" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.723752441406" Y="-29.036083984375" />
                  <Point X="2.036138305664" Y="-28.139966796875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657714844" Y="-27.87015234375" />
                  <Point X="1.808835571289" Y="-27.849630859375" />
                  <Point X="1.783253417969" Y="-27.828005859375" />
                  <Point X="1.773299560547" Y="-27.820646484375" />
                  <Point X="1.731239501953" Y="-27.79360546875" />
                  <Point X="1.560176025391" Y="-27.68362890625" />
                  <Point X="1.546279541016" Y="-27.676244140625" />
                  <Point X="1.517465087891" Y="-27.663873046875" />
                  <Point X="1.502547119141" Y="-27.65888671875" />
                  <Point X="1.470926635742" Y="-27.65115625" />
                  <Point X="1.455390991211" Y="-27.648697265625" />
                  <Point X="1.424126098633" Y="-27.64637890625" />
                  <Point X="1.408396484375" Y="-27.64651953125" />
                  <Point X="1.362396972656" Y="-27.650751953125" />
                  <Point X="1.175309814453" Y="-27.667966796875" />
                  <Point X="1.161229125977" Y="-27.67033984375" />
                  <Point X="1.133582885742" Y="-27.677169921875" />
                  <Point X="1.120017333984" Y="-27.681626953125" />
                  <Point X="1.092631835938" Y="-27.69296875" />
                  <Point X="1.079885864258" Y="-27.699408203125" />
                  <Point X="1.055497314453" Y="-27.714130859375" />
                  <Point X="1.043854736328" Y="-27.7224140625" />
                  <Point X="1.008334899902" Y="-27.75194921875" />
                  <Point X="0.863870910645" Y="-27.87206640625" />
                  <Point X="0.852647216797" Y="-27.88309375" />
                  <Point X="0.832177368164" Y="-27.906845703125" />
                  <Point X="0.822930969238" Y="-27.9195703125" />
                  <Point X="0.806038635254" Y="-27.9473984375" />
                  <Point X="0.799017822266" Y="-27.96147265625" />
                  <Point X="0.787394470215" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.772171630859" Y="-28.054494140625" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091369629" Y="-29.15233984375" />
                  <Point X="0.734119689941" Y="-28.78297265625" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146789551" Y="-28.453583984375" />
                  <Point X="0.626788085938" Y="-28.42381640625" />
                  <Point X="0.620407287598" Y="-28.41320703125" />
                  <Point X="0.588095458984" Y="-28.36665234375" />
                  <Point X="0.456679382324" Y="-28.177306640625" />
                  <Point X="0.446668762207" Y="-28.165169921875" />
                  <Point X="0.424785949707" Y="-28.142716796875" />
                  <Point X="0.412913513184" Y="-28.132400390625" />
                  <Point X="0.38665838623" Y="-28.11315625" />
                  <Point X="0.373243682861" Y="-28.1049375" />
                  <Point X="0.345240600586" Y="-28.090828125" />
                  <Point X="0.33065222168" Y="-28.0849375" />
                  <Point X="0.280651611328" Y="-28.069419921875" />
                  <Point X="0.077293128967" Y="-28.0063046875" />
                  <Point X="0.063376678467" Y="-28.003109375" />
                  <Point X="0.035218582153" Y="-27.99883984375" />
                  <Point X="0.020976791382" Y="-27.997765625" />
                  <Point X="-0.008664410591" Y="-27.997765625" />
                  <Point X="-0.022904417038" Y="-27.99883984375" />
                  <Point X="-0.051066078186" Y="-28.003109375" />
                  <Point X="-0.064987289429" Y="-28.0063046875" />
                  <Point X="-0.11498789978" Y="-28.02182421875" />
                  <Point X="-0.318346374512" Y="-28.0849375" />
                  <Point X="-0.332931335449" Y="-28.090828125" />
                  <Point X="-0.36093057251" Y="-28.104935546875" />
                  <Point X="-0.374344970703" Y="-28.11315234375" />
                  <Point X="-0.400600402832" Y="-28.132396484375" />
                  <Point X="-0.412476409912" Y="-28.142716796875" />
                  <Point X="-0.434360961914" Y="-28.165173828125" />
                  <Point X="-0.444369384766" Y="-28.177310546875" />
                  <Point X="-0.476681182861" Y="-28.2238671875" />
                  <Point X="-0.608096984863" Y="-28.4132109375" />
                  <Point X="-0.612471557617" Y="-28.4201328125" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.96427935791" Y="-29.687888671875" />
                  <Point X="-0.985425292969" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.315929711249" Y="-20.800265129196" />
                  <Point X="-3.011153827175" Y="-21.383627428455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.713098911695" Y="-21.972629289983" />
                  <Point X="-3.768354978219" Y="-22.018994635023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.150968368055" Y="-20.78585981945" />
                  <Point X="-2.9629209356" Y="-21.46716891941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.625546427032" Y="-22.02317772488" />
                  <Point X="-4.02473225052" Y="-22.35813440214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.08953606162" Y="-20.858325686264" />
                  <Point X="-2.914688044024" Y="-21.550710410366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.537993942369" Y="-22.073726159777" />
                  <Point X="-4.166601870857" Y="-22.601190840726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.420354125852" Y="-20.420829063258" />
                  <Point X="-1.475363591221" Y="-20.46698748536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.005849118502" Y="-20.912117695647" />
                  <Point X="-2.866455152449" Y="-21.634251901321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.450441457706" Y="-22.124274594675" />
                  <Point X="-4.183614332368" Y="-22.739479683393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.198400827142" Y="-20.358601824658" />
                  <Point X="-1.462467618629" Y="-20.580180172002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.911851017161" Y="-20.957257615967" />
                  <Point X="-2.818222260874" Y="-21.717793392276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.362888990202" Y="-22.17482304397" />
                  <Point X="-4.106415916827" Y="-22.798716213871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.976447528431" Y="-20.296374586058" />
                  <Point X="-1.49507428457" Y="-20.731554105853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.770104217825" Y="-20.96233162141" />
                  <Point X="-2.77279675051" Y="-21.80369055577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.27533657997" Y="-22.225371541322" />
                  <Point X="-4.029217501287" Y="-22.85795274435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.794453298306" Y="-20.26767698717" />
                  <Point X="-2.752711891287" Y="-21.91085105029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.175252062747" Y="-22.26540435232" />
                  <Point X="-3.952019085746" Y="-22.917189274829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.622704234423" Y="-20.247576103498" />
                  <Point X="-2.756809210554" Y="-22.038302801863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.040391201014" Y="-22.276256345466" />
                  <Point X="-3.874820670205" Y="-22.976425805308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.450955177038" Y="-20.227475225277" />
                  <Point X="-3.797622254665" Y="-23.035662335787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.576562724666" Y="-23.689270996874" />
                  <Point X="-4.653000862232" Y="-23.753410209914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.351635416749" Y="-20.268149743537" />
                  <Point X="-3.720423839124" Y="-23.094898866266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.448812790174" Y="-23.706089766446" />
                  <Point X="-4.696496400969" Y="-23.913920992912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.324505801108" Y="-20.369398985545" />
                  <Point X="-3.643225423583" Y="-23.154135396745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.321062855682" Y="-23.722908536017" />
                  <Point X="-4.735503376709" Y="-24.070665424356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.297376185467" Y="-20.470648227553" />
                  <Point X="-3.566027008042" Y="-23.213371927224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.19331292119" Y="-23.739727305589" />
                  <Point X="-4.756455938361" Y="-24.212260403596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.270246569826" Y="-20.571897469561" />
                  <Point X="-3.49720328578" Y="-23.279635659743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.065562986698" Y="-23.75654607516" />
                  <Point X="-4.777408500012" Y="-24.353855382837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.243116959366" Y="-20.673146715917" />
                  <Point X="-3.449617638436" Y="-23.363720253094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.937812665013" Y="-23.773364519838" />
                  <Point X="-4.716398425503" Y="-24.426675544305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.442638315409" Y="-20.221743410262" />
                  <Point X="0.357164756759" Y="-20.293464241801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.212150547136" Y="-20.771176503322" />
                  <Point X="-3.42158658456" Y="-23.464213098612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.810061744303" Y="-23.790182461874" />
                  <Point X="-4.604376557371" Y="-24.456691728558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.574033155039" Y="-20.235503741276" />
                  <Point X="0.31429711714" Y="-20.453448154881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.157680093429" Y="-20.849484058194" />
                  <Point X="-3.460272632662" Y="-23.620688239793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.645618195079" Y="-23.776211632858" />
                  <Point X="-4.492354586451" Y="-24.486707826561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.705427994669" Y="-20.249264072291" />
                  <Point X="0.271429477521" Y="-20.613432067962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.070872735898" Y="-20.900657728992" />
                  <Point X="-4.38033261553" Y="-24.516723924565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.835691405327" Y="-20.263973784939" />
                  <Point X="0.220311352757" Y="-20.780338960084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.072665467944" Y="-20.904228567575" />
                  <Point X="-4.268310644609" Y="-24.546740022568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.950462016278" Y="-20.291683500106" />
                  <Point X="-4.156288673689" Y="-24.576756120572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.065233083136" Y="-20.319392832722" />
                  <Point X="-4.044266702768" Y="-24.606772218575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.180004149995" Y="-20.347102165338" />
                  <Point X="-3.932244731848" Y="-24.636788316578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.294775216853" Y="-20.374811497954" />
                  <Point X="-3.820222760927" Y="-24.666804414582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.409546283711" Y="-20.40252083057" />
                  <Point X="-3.708200790007" Y="-24.696820512585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.517180335985" Y="-20.436218829492" />
                  <Point X="-3.596178819086" Y="-24.726836610589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.662252748154" Y="-25.621378851277" />
                  <Point X="-4.777393787667" Y="-25.717993655066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.620370044492" Y="-20.473646075628" />
                  <Point X="-3.486266613161" Y="-24.758623011621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.445122420216" Y="-25.563198565674" />
                  <Point X="-4.76155760134" Y="-25.828719209446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.723559753931" Y="-20.511073320983" />
                  <Point X="-3.399458307335" Y="-24.809795886707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.227992092278" Y="-25.50501828007" />
                  <Point X="-4.74572152706" Y="-25.939444857845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.82674946337" Y="-20.548500566338" />
                  <Point X="-3.336394136861" Y="-24.880892457007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.01086176434" Y="-25.446837994466" />
                  <Point X="-4.722845231002" Y="-26.044263058747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.924180225633" Y="-20.590760142145" />
                  <Point X="-3.300819995" Y="-24.975055900179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.793731478166" Y="-25.388657743907" />
                  <Point X="-4.696759292502" Y="-26.146388049859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01908083817" Y="-20.635142765653" />
                  <Point X="-3.304021086038" Y="-25.101755626975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.576602379241" Y="-25.330478489567" />
                  <Point X="-4.670673354002" Y="-26.248513040971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.113981470399" Y="-20.679525372637" />
                  <Point X="-4.551160733292" Y="-26.272243737499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.208882102629" Y="-20.723907979622" />
                  <Point X="-4.375863494426" Y="-26.249165581507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299471827842" Y="-20.771907867093" />
                  <Point X="-4.200566253575" Y="-26.226087423849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.386701535702" Y="-20.822727143887" />
                  <Point X="-4.025269012724" Y="-26.203009266191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.473930810692" Y="-20.873546783902" />
                  <Point X="-3.849971771872" Y="-26.179931108533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.561160085681" Y="-20.924366423917" />
                  <Point X="-3.674674531021" Y="-26.156852950874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646818192863" Y="-20.97650443026" />
                  <Point X="-3.49937729017" Y="-26.133774793216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.726813838195" Y="-21.033393806252" />
                  <Point X="-3.324080049319" Y="-26.110696635558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.806810031696" Y="-21.090282722277" />
                  <Point X="-3.169357714572" Y="-26.104882874024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.697944316259" Y="-21.305645596435" />
                  <Point X="-3.068876778522" Y="-26.14458305013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.559063696176" Y="-21.546193966011" />
                  <Point X="-2.996842259819" Y="-26.208152604542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420183076093" Y="-21.786742335587" />
                  <Point X="-2.954917949458" Y="-26.296987623667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281302456009" Y="-22.027290705163" />
                  <Point X="-2.956331618534" Y="-26.422187525353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.088557957202" Y="-27.372238228539" />
                  <Point X="-4.178620491853" Y="-27.447809668148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.142421328885" Y="-22.267839500196" />
                  <Point X="-4.129329450169" Y="-27.530463265737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.035336720469" Y="-22.48170784811" />
                  <Point X="-4.080038408485" Y="-27.613116863326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013746391078" Y="-22.623837978025" />
                  <Point X="-4.030747124301" Y="-27.695770257434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029643254565" Y="-22.734512618223" />
                  <Point X="-3.977651278204" Y="-27.775231245043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072682971758" Y="-22.822411699887" />
                  <Point X="-3.920048223823" Y="-27.850910235844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.129695746477" Y="-22.898585994134" />
                  <Point X="-3.862445169443" Y="-27.926589226645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205961542654" Y="-22.958605085178" />
                  <Point X="-3.804842115062" Y="-28.002268217446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.297223623186" Y="-23.00604079955" />
                  <Point X="-3.415980312618" Y="-27.799988114923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.422848712463" Y="-23.024642525957" />
                  <Point X="-2.942190571059" Y="-27.526445010012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.628814157372" Y="-22.975830689585" />
                  <Point X="-2.583209459382" Y="-27.349237784091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.100832649311" Y="-22.703773839577" />
                  <Point X="-2.463661021096" Y="-27.372938426104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.574618639652" Y="-22.430233882311" />
                  <Point X="-2.383963126439" Y="-27.430077644578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953087873609" Y="-22.236674180172" />
                  <Point X="-2.331200408257" Y="-27.509818159698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008727498375" Y="-22.314000684039" />
                  <Point X="-2.311577866054" Y="-27.617366584259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.062213631944" Y="-22.393134181575" />
                  <Point X="-2.388925763358" Y="-27.806282868846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.111985574887" Y="-22.475384255095" />
                  <Point X="3.272539802689" Y="-23.17976289294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.132052828802" Y="-23.297645460913" />
                  <Point X="-2.527805464301" Y="-28.046830467172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0119261473" Y="-23.522457407543" />
                  <Point X="-2.666686301667" Y="-28.28737901907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.005863480394" Y="-23.651558281594" />
                  <Point X="-2.805567139032" Y="-28.527927570967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.029492160495" Y="-23.755745157323" />
                  <Point X="-2.944447976398" Y="-28.768476122865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.073989020494" Y="-23.842421550996" />
                  <Point X="-2.923999594014" Y="-28.875331585235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.129851443032" Y="-23.919561105334" />
                  <Point X="-2.846927591531" Y="-28.934674188864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.20789353708" Y="-23.978089705489" />
                  <Point X="-2.769856064866" Y="-28.994017191752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.301494946244" Y="-24.023562490068" />
                  <Point X="-2.687115263861" Y="-29.048603108631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.423405773259" Y="-24.04528085257" />
                  <Point X="-2.602073910816" Y="-29.101258633143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.58161981216" Y="-24.036537203367" />
                  <Point X="-2.187045819353" Y="-28.877022407155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.459741402516" Y="-29.105841170411" />
                  <Point X="-2.517032557771" Y="-29.153914157655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.756917048981" Y="-24.013459049091" />
                  <Point X="-1.920720247396" Y="-28.777562410439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932214285802" Y="-23.990380894815" />
                  <Point X="-1.783634543069" Y="-28.786547538985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.107511522623" Y="-23.967302740538" />
                  <Point X="-1.64654822258" Y="-28.79553215051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.282808759444" Y="-23.944224586262" />
                  <Point X="-1.536966024694" Y="-28.827595461167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.458105996265" Y="-23.921146431986" />
                  <Point X="3.66110583646" Y="-24.589908972127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.405758252019" Y="-24.804171036052" />
                  <Point X="-1.458975371068" Y="-28.886167224961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.633403233086" Y="-23.898068277709" />
                  <Point X="3.88346582445" Y="-24.527340480702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.354780980584" Y="-24.970959738199" />
                  <Point X="-1.386709515903" Y="-28.949542665031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.719576786218" Y="-23.949773773546" />
                  <Point X="4.100596296653" Y="-24.469160074045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.355379543716" Y="-25.094471176582" />
                  <Point X="-0.144054851798" Y="-28.030845287187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.616152120614" Y="-28.426981931331" />
                  <Point X="-1.314443660738" Y="-29.012918105102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.744646400028" Y="-24.05275156233" />
                  <Point X="4.317726556138" Y="-24.410979845881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.378078414805" Y="-25.199438254709" />
                  <Point X="0.040861776473" Y="-27.999695505093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.672266389511" Y="-28.598081086153" />
                  <Point X="-1.24433013639" Y="-29.078099565168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.764285810654" Y="-24.160285832605" />
                  <Point X="4.534856815622" Y="-24.352799617717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.424263876831" Y="-25.284697743045" />
                  <Point X="0.152837011078" Y="-28.029750819521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.715133908999" Y="-28.758064898431" />
                  <Point X="-1.1948518769" Y="-29.160596068365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.7813629837" Y="-24.269970075486" />
                  <Point X="4.751987075107" Y="-24.294619389552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.483851339096" Y="-25.358711617922" />
                  <Point X="0.260725286471" Y="-28.063235499917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.758001428487" Y="-28.918048710709" />
                  <Point X="-1.169885800373" Y="-29.263660735246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.561219690028" Y="-25.417805555676" />
                  <Point X="0.364231844183" Y="-28.100396878003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.800868947976" Y="-29.078032522988" />
                  <Point X="-1.148746285028" Y="-29.369936268203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.649687430366" Y="-25.467585999874" />
                  <Point X="0.441343680409" Y="-28.159706057153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.843736467464" Y="-29.238016335266" />
                  <Point X="-1.127606858732" Y="-29.476211875882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.757197770697" Y="-25.501387805442" />
                  <Point X="3.236479754725" Y="-25.938322100591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.896006399918" Y="-26.224013167035" />
                  <Point X="1.175134202619" Y="-27.667996393092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.78625883116" Y="-27.994301573857" />
                  <Point X="0.497709470934" Y="-28.2364232356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.886603986952" Y="-29.398000147545" />
                  <Point X="-1.123536157652" Y="-29.596809844594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.869219642495" Y="-25.531403986619" />
                  <Point X="3.419950570377" Y="-25.908385499332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868290433136" Y="-26.371283317026" />
                  <Point X="1.341149057359" Y="-27.652707082196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.752062980483" Y="-28.147008992034" />
                  <Point X="0.552103578067" Y="-28.314794852853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.92947150644" Y="-29.557983959823" />
                  <Point X="-1.137271480539" Y="-29.732348841449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.981241514294" Y="-25.561420167795" />
                  <Point X="3.549089080704" Y="-25.924039115433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860652120215" Y="-26.501706315068" />
                  <Point X="1.486309223339" Y="-27.654916932948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724726750694" Y="-28.293960504855" />
                  <Point X="0.60649782116" Y="-28.393166356021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.972338994399" Y="-29.717967745646" />
                  <Point X="-1.022066691672" Y="-29.759694238087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.093263386092" Y="-25.591436348972" />
                  <Point X="3.676839582133" Y="-25.940857409287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887996131466" Y="-26.602775657798" />
                  <Point X="2.274787401413" Y="-27.117318877021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.119883339761" Y="-27.247298818021" />
                  <Point X="1.582659251021" Y="-27.698083352742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.735248065232" Y="-28.409145766193" />
                  <Point X="0.652122434118" Y="-28.478896452603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.205285257891" Y="-25.621452530148" />
                  <Point X="3.804589899058" Y="-25.957675857959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.938405722522" Y="-26.684490681022" />
                  <Point X="2.531755720793" Y="-27.025710547491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012960740882" Y="-27.461031223792" />
                  <Point X="1.666339274674" Y="-27.751881168244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749950588451" Y="-28.52082257687" />
                  <Point X="0.679676973503" Y="-28.579789141254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.317307129689" Y="-25.651468711325" />
                  <Point X="3.932340065021" Y="-25.974494433304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.990196335666" Y="-26.765046889121" />
                  <Point X="2.655922118652" Y="-27.04553626133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.004374328205" Y="-27.592249771988" />
                  <Point X="1.75001910551" Y="-27.80567914554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764653111669" Y="-28.632499387546" />
                  <Point X="0.706806561442" Y="-28.681038406507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.429329001488" Y="-25.681484892502" />
                  <Point X="4.060090230984" Y="-25.991313008648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052954139045" Y="-26.836400531939" />
                  <Point X="2.777136566245" Y="-27.067838955548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.023823521365" Y="-27.699943653668" />
                  <Point X="1.8252191739" Y="-27.866592488376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779355634887" Y="-28.744176198223" />
                  <Point X="0.73393614938" Y="-28.78228767176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.541350873286" Y="-25.711501073678" />
                  <Point X="4.187840396946" Y="-26.008131583993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.130152584968" Y="-26.895637036924" />
                  <Point X="2.874813435534" Y="-27.109892023039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043887249842" Y="-27.807121878989" />
                  <Point X="1.883807122814" Y="-27.941445054537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.794058158106" Y="-28.8558530089" />
                  <Point X="0.76106580374" Y="-28.88353688128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.653372745085" Y="-25.741517254855" />
                  <Point X="4.315590562909" Y="-26.024950159337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.20735103089" Y="-26.95487354191" />
                  <Point X="2.962366124251" Y="-27.160440286715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.083557768708" Y="-27.897848053726" />
                  <Point X="1.941694575904" Y="-28.016885406486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808760681324" Y="-28.967529819576" />
                  <Point X="0.788195458551" Y="-28.98478609042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765394440281" Y="-25.771533584219" />
                  <Point X="4.443340728871" Y="-26.041768734682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.284549476813" Y="-27.014110046896" />
                  <Point X="3.049918812967" Y="-27.210988550391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.131790568956" Y="-27.981389621314" />
                  <Point X="1.999582028995" Y="-28.092325758435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.823463204543" Y="-29.079206630253" />
                  <Point X="0.815325113363" Y="-29.08603529956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767194169635" Y="-25.894037124468" />
                  <Point X="4.571090894834" Y="-26.058587310026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361747922735" Y="-27.073346551881" />
                  <Point X="3.137471436848" Y="-27.26153686847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.180023369204" Y="-28.064931188902" />
                  <Point X="2.057469459759" Y="-28.167766129117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.735737791031" Y="-26.04444585264" />
                  <Point X="4.698841060797" Y="-26.075405885371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.438946368657" Y="-27.132583056867" />
                  <Point X="3.225023889159" Y="-27.312085330515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.228256169452" Y="-28.14847275649" />
                  <Point X="2.115356852263" Y="-28.243206531904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.51614481458" Y="-27.191819561852" />
                  <Point X="3.312576341469" Y="-27.362633792559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.2764889697" Y="-28.232014324078" />
                  <Point X="2.173244244767" Y="-28.31864693469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.593343260502" Y="-27.251056066838" />
                  <Point X="3.400128793779" Y="-27.413182254603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.324721769948" Y="-28.315555891666" />
                  <Point X="2.231131637271" Y="-28.394087337477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.670541706425" Y="-27.310292571824" />
                  <Point X="3.48768124609" Y="-27.463730716648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.372954570195" Y="-28.399097459254" />
                  <Point X="2.289019029774" Y="-28.469527740264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.747740152347" Y="-27.369529076809" />
                  <Point X="3.5752336984" Y="-27.514279178692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.421187370443" Y="-28.482639026842" />
                  <Point X="2.346906422278" Y="-28.544968143051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.824938598269" Y="-27.428765581795" />
                  <Point X="3.66278615071" Y="-27.564827640736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.469420170691" Y="-28.56618059443" />
                  <Point X="2.404793814782" Y="-28.620408545837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.902137044192" Y="-27.488002086781" />
                  <Point X="3.750338603021" Y="-27.615376102781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.517652970939" Y="-28.649722162018" />
                  <Point X="2.462681207286" Y="-28.695848948624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979335490114" Y="-27.547238591766" />
                  <Point X="3.837891055331" Y="-27.665924564825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.565885771187" Y="-28.733263729606" />
                  <Point X="2.52056859979" Y="-28.771289351411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.056533456348" Y="-27.606475499258" />
                  <Point X="3.925443507641" Y="-27.716473026869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.614118571435" Y="-28.816805297194" />
                  <Point X="2.578455992294" Y="-28.846729754198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662351371682" Y="-28.900346864782" />
                  <Point X="2.636343384798" Y="-28.922170156985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.710584252759" Y="-28.983888364546" />
                  <Point X="2.694230777301" Y="-28.997610559771" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998375" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.55059387207" Y="-28.8321484375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.432007049561" Y="-28.47498828125" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.274335906982" Y="-28.2663984375" />
                  <Point X="0.224335281372" Y="-28.250880859375" />
                  <Point X="0.02097677803" Y="-28.187765625" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.058664928436" Y="-28.20328515625" />
                  <Point X="-0.2620234375" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.320590789795" Y="-28.33219921875" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.780753540039" Y="-29.737064453125" />
                  <Point X="-0.84774432373" Y="-29.987078125" />
                  <Point X="-0.861136962891" Y="-29.984478515625" />
                  <Point X="-1.100231201172" Y="-29.938068359375" />
                  <Point X="-1.157142944336" Y="-29.92342578125" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.318666625977" Y="-29.62332421875" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.321628417969" Y="-29.47470703125" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.432316894531" Y="-29.1622578125" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240722656" Y="-28.985763671875" />
                  <Point X="-1.710338256836" Y="-28.981759765625" />
                  <Point X="-1.958829833984" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.040788330078" Y="-29.007806640625" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.440246337891" Y="-29.39254296875" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.50534765625" Y="-29.384623046875" />
                  <Point X="-2.855839355469" Y="-29.167607421875" />
                  <Point X="-2.93462109375" Y="-29.106947265625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.652948242188" Y="-27.883583984375" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513980957031" Y="-27.56876171875" />
                  <Point X="-2.531329589844" Y="-27.5514140625" />
                  <Point X="-2.560158203125" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.626619384766" Y="-28.1409921875" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.884800537109" Y="-28.210923828125" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.218187011719" Y="-27.752416015625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.420839599609" Y="-26.620388671875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535400391" Y="-26.411083984375" />
                  <Point X="-3.144301513672" Y="-26.39014453125" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.136651855469" Y="-26.350052734375" />
                  <Point X="-3.148657470703" Y="-26.321068359375" />
                  <Point X="-3.166386474609" Y="-26.3075625" />
                  <Point X="-3.187642822266" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.526037597656" Y="-26.460576171875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.819094726562" Y="-26.43517578125" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.942336914062" Y="-25.90670703125" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.850285644531" Y="-25.207107421875" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.536418457031" Y="-25.117625" />
                  <Point X="-3.514142578125" Y="-25.1021640625" />
                  <Point X="-3.50232421875" Y="-25.090646484375" />
                  <Point X="-3.493073242188" Y="-25.070025390625" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.487473388672" Y="-25.010578125" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.502323974609" Y="-24.971916015625" />
                  <Point X="-3.519619628906" Y="-24.956595703125" />
                  <Point X="-3.541895507813" Y="-24.941134765625" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.748407714844" Y="-24.614802734375" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.987442382813" Y="-24.475263671875" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.8875625" Y="-23.892568359375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.96316796875" Y="-23.57838671875" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731705078125" Y="-23.6041328125" />
                  <Point X="-3.719582763672" Y="-23.600310546875" />
                  <Point X="-3.670279052734" Y="-23.584765625" />
                  <Point X="-3.651534423828" Y="-23.5739453125" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.634255615234" Y="-23.54447265625" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.622185302734" Y="-23.443212890625" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.343096679688" Y="-22.856595703125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.431221191406" Y="-22.67763671875" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.080327636719" Y="-22.110568359375" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.288072753906" Y="-21.998626953125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514892578" Y="-22.07956640625" />
                  <Point X="-3.121631591797" Y="-22.08104296875" />
                  <Point X="-3.052965576172" Y="-22.08705078125" />
                  <Point X="-3.031507080078" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-3.001269042969" Y="-22.060611328125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.939551269531" Y="-21.955275390625" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.254782470703" Y="-21.341650390625" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-3.225232421875" Y="-21.1878203125" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.627372070312" Y="-20.75594140625" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.007291870117" Y="-20.660994140625" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246948242" Y="-20.726337890625" />
                  <Point X="-1.932456054688" Y="-20.73612109375" />
                  <Point X="-1.856030761719" Y="-20.77590625" />
                  <Point X="-1.835124145508" Y="-20.781509765625" />
                  <Point X="-1.81380859375" Y="-20.77775" />
                  <Point X="-1.794236816406" Y="-20.769642578125" />
                  <Point X="-1.714634765625" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.679713012695" Y="-20.685306640625" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.686332885742" Y="-20.3201640625" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.579588500977" Y="-20.268146484375" />
                  <Point X="-0.968082946777" Y="-20.096703125" />
                  <Point X="-0.81594720459" Y="-20.078896484375" />
                  <Point X="-0.224200027466" Y="-20.009640625" />
                  <Point X="-0.080277153015" Y="-20.54676953125" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282051086" Y="-20.71582421875" />
                  <Point X="0.006156086445" Y="-20.72615625" />
                  <Point X="0.036594120026" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.209559921265" Y="-20.110228515625" />
                  <Point X="0.2366484375" Y="-20.0091328125" />
                  <Point X="0.326282104492" Y="-20.01851953125" />
                  <Point X="0.860210144043" Y="-20.074435546875" />
                  <Point X="0.986082763672" Y="-20.104826171875" />
                  <Point X="1.508455932617" Y="-20.230943359375" />
                  <Point X="1.590171508789" Y="-20.26058203125" />
                  <Point X="1.931043945312" Y="-20.38421875" />
                  <Point X="2.010261108398" Y="-20.421265625" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.415241943359" Y="-20.6194609375" />
                  <Point X="2.732519775391" Y="-20.804306640625" />
                  <Point X="2.804708496094" Y="-20.85564453125" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.399895996094" Y="-22.201880859375" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.220614990234" Y="-22.52433203125" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.203696777344" Y="-22.621376953125" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.227159912109" Y="-22.711681640625" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.28743359375" Y="-22.784275390625" />
                  <Point X="2.338248291016" Y="-22.818755859375" />
                  <Point X="2.360343017578" Y="-22.827021484375" />
                  <Point X="2.374036621094" Y="-22.828671875" />
                  <Point X="2.429760498047" Y="-22.835392578125" />
                  <Point X="2.464508789062" Y="-22.82981640625" />
                  <Point X="2.528950683594" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.738894775391" Y="-22.115998046875" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.014391113281" Y="-21.996564453125" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.242833984375" Y="-22.32462109375" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.512493164062" Y="-23.235130859375" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.267968261719" Y="-23.431044921875" />
                  <Point X="3.221589355469" Y="-23.491548828125" />
                  <Point X="3.208871826172" Y="-23.523689453125" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.194266357422" Y="-23.62593359375" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.225130859375" Y="-23.726462890625" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280947265625" Y="-23.8011796875" />
                  <Point X="3.294691162109" Y="-23.808916015625" />
                  <Point X="3.350589355469" Y="-23.8403828125" />
                  <Point X="3.387148193359" Y="-23.8488359375" />
                  <Point X="3.462726318359" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.61369140625" Y="-23.7090234375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.858357421875" Y="-23.7165859375" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.951870117188" Y="-24.13006640625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="3.997735351562" Y="-24.69342578125" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.729087646484" Y="-24.767181640625" />
                  <Point X="3.710830810547" Y="-24.777734375" />
                  <Point X="3.636577636719" Y="-24.82065234375" />
                  <Point X="3.611310791016" Y="-24.84703125" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.553333740234" Y="-24.94433203125" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.542134521484" Y="-25.05975" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.577713134766" Y="-25.17271875" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.636576904297" Y="-25.241908203125" />
                  <Point X="3.654833740234" Y="-25.2524609375" />
                  <Point X="3.729086914062" Y="-25.295380859375" />
                  <Point X="3.74116796875" Y="-25.300388671875" />
                  <Point X="4.784662109375" Y="-25.5799921875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.993564453125" Y="-25.667044921875" />
                  <Point X="4.948430664062" Y="-25.966404296875" />
                  <Point X="4.932184570312" Y="-26.03759765625" />
                  <Point X="4.874545898438" Y="-26.290177734375" />
                  <Point X="3.709528564453" Y="-26.13680078125" />
                  <Point X="3.411981933594" Y="-26.09762890625" />
                  <Point X="3.3948359375" Y="-26.098341796875" />
                  <Point X="3.359004150391" Y="-26.10612890625" />
                  <Point X="3.213271728516" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.163787109375" Y="-26.18074609375" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.061253662109" Y="-26.3478046875" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.076190429688" Y="-26.547466796875" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.136830078125" Y="-27.42859765625" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.331355957031" Y="-27.5962734375" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.170530761719" Y="-27.849884765625" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.018459960938" Y="-27.412216796875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.6946953125" Y="-27.24561328125" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.453649658203" Y="-27.237890625" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.269954345703" Y="-27.370111328125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.196864746094" Y="-27.589021484375" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.856366455078" Y="-28.856390625" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.986260498047" Y="-29.0823828125" />
                  <Point X="2.835291259766" Y="-29.190216796875" />
                  <Point X="2.797732421875" Y="-29.214529296875" />
                  <Point X="2.679775634766" Y="-29.290880859375" />
                  <Point X="1.885401123047" Y="-28.255630859375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.628489013672" Y="-27.95342578125" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425805053711" Y="-27.83571875" />
                  <Point X="1.379805419922" Y="-27.839951171875" />
                  <Point X="1.192718261719" Y="-27.857166015625" />
                  <Point X="1.165332763672" Y="-27.8685078125" />
                  <Point X="1.129812988281" Y="-27.89804296875" />
                  <Point X="0.985349060059" Y="-28.01816015625" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.957836547852" Y="-28.094849609375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.09027734375" Y="-29.65021484375" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="0.994365539551" Y="-29.9632421875" />
                  <Point X="0.959638977051" Y="-29.969552734375" />
                  <Point X="0.860200439453" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#136" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.038136890874" Y="4.497453295221" Z="0.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.5" />
                  <Point X="-0.827915273868" Y="5.00204409515" Z="0.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.5" />
                  <Point X="-1.599242271024" Y="4.811278434743" Z="0.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.5" />
                  <Point X="-1.742061162791" Y="4.704590751138" Z="0.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.5" />
                  <Point X="-1.733554822718" Y="4.361008135229" Z="0.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.5" />
                  <Point X="-1.819527040381" Y="4.307831638657" Z="0.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.5" />
                  <Point X="-1.915524258653" Y="4.339509307679" Z="0.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.5" />
                  <Point X="-1.973780258798" Y="4.400723208351" Z="0.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.5" />
                  <Point X="-2.657810906068" Y="4.319046421952" Z="0.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.5" />
                  <Point X="-3.261810535445" Y="3.883140741479" Z="0.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.5" />
                  <Point X="-3.304239630436" Y="3.66463054233" Z="0.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.5" />
                  <Point X="-2.995517115832" Y="3.071646754582" Z="0.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.5" />
                  <Point X="-3.042779740196" Y="3.00602382769" Z="0.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.5" />
                  <Point X="-3.123429711025" Y="3.000047583341" Z="0.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.5" />
                  <Point X="-3.269228805968" Y="3.07595431831" Z="0.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.5" />
                  <Point X="-4.125946930877" Y="2.951415245782" Z="0.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.5" />
                  <Point X="-4.48055922162" Y="2.378849488806" Z="0.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.5" />
                  <Point X="-4.379690835413" Y="2.135017031883" Z="0.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.5" />
                  <Point X="-3.672691605992" Y="1.56497895798" Z="0.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.5" />
                  <Point X="-3.686605718933" Y="1.505943301832" Z="0.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.5" />
                  <Point X="-3.740773592902" Y="1.478654733526" Z="0.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.5" />
                  <Point X="-3.96279791781" Y="1.502466645589" Z="0.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.5" />
                  <Point X="-4.94197802241" Y="1.151790644736" Z="0.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.5" />
                  <Point X="-5.043059627374" Y="0.563334640107" Z="0.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.5" />
                  <Point X="-4.767505181791" Y="0.368181639541" Z="0.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.5" />
                  <Point X="-3.554284900563" Y="0.033608330389" Z="0.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.5" />
                  <Point X="-3.541382453463" Y="0.005882413615" Z="0.5" />
                  <Point X="-3.539556741714" Y="0" Z="0.5" />
                  <Point X="-3.546982147672" Y="-0.023924537446" Z="0.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.5" />
                  <Point X="-3.57108369456" Y="-0.045267652745" Z="0.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.5" />
                  <Point X="-3.869382606982" Y="-0.127530417215" Z="0.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.5" />
                  <Point X="-4.997989007571" Y="-0.882504063647" Z="0.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.5" />
                  <Point X="-4.87369529821" Y="-1.416270307696" Z="0.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.5" />
                  <Point X="-4.525667313974" Y="-1.478868372375" Z="0.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.5" />
                  <Point X="-3.197902653851" Y="-1.319373802303" Z="0.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.5" />
                  <Point X="-3.198860316737" Y="-1.346326515599" Z="0.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.5" />
                  <Point X="-3.457433600999" Y="-1.54944071182" Z="0.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.5" />
                  <Point X="-4.267286242335" Y="-2.74674503476" Z="0.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.5" />
                  <Point X="-3.930742362975" Y="-3.209926574513" Z="0.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.5" />
                  <Point X="-3.607775403804" Y="-3.153011485823" Z="0.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.5" />
                  <Point X="-2.558914709476" Y="-2.569415759656" Z="0.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.5" />
                  <Point X="-2.702405527345" Y="-2.827302978688" Z="0.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.5" />
                  <Point X="-2.971280736256" Y="-4.115284823655" Z="0.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.5" />
                  <Point X="-2.537825181713" Y="-4.395854482605" Z="0.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.5" />
                  <Point X="-2.406734542332" Y="-4.391700261423" Z="0.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.5" />
                  <Point X="-2.019165744383" Y="-4.018101223664" Z="0.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.5" />
                  <Point X="-1.71976439021" Y="-4.000371385834" Z="0.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.5" />
                  <Point X="-1.471440104372" Y="-4.168569606677" Z="0.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.5" />
                  <Point X="-1.37682365033" Y="-4.453180138481" Z="0.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.5" />
                  <Point X="-1.374394873083" Y="-4.58551596202" Z="0.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.5" />
                  <Point X="-1.175757800425" Y="-4.940569090611" Z="0.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.5" />
                  <Point X="-0.876820656357" Y="-5.002281227471" Z="0.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.5" />
                  <Point X="-0.73861320183" Y="-4.718726106664" Z="0.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.5" />
                  <Point X="-0.28567080604" Y="-3.329427714769" Z="0.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.5" />
                  <Point X="-0.050000514809" Y="-3.219757683044" Z="0.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.5" />
                  <Point X="0.203358564552" Y="-3.267354362244" Z="0.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.5" />
                  <Point X="0.384775053392" Y="-3.472218040999" Z="0.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.5" />
                  <Point X="0.496141658571" Y="-3.813809923152" Z="0.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.5" />
                  <Point X="0.96241957787" Y="-4.987466886689" Z="0.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.5" />
                  <Point X="1.141718803913" Y="-4.94950052471" Z="0.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.5" />
                  <Point X="1.133693673411" Y="-4.612408742999" Z="0.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.5" />
                  <Point X="1.00053975596" Y="-3.074187540582" Z="0.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.5" />
                  <Point X="1.155623889634" Y="-2.90520903992" Z="0.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.5" />
                  <Point X="1.378230702624" Y="-2.858459528383" Z="0.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.5" />
                  <Point X="1.595293901527" Y="-2.964204578815" Z="0.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.5" />
                  <Point X="1.83957742246" Y="-3.254788045243" Z="0.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.5" />
                  <Point X="2.818745470965" Y="-4.225222505864" Z="0.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.5" />
                  <Point X="3.00916620709" Y="-4.091790636511" Z="0.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.5" />
                  <Point X="2.893511635416" Y="-3.800109635104" Z="0.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.5" />
                  <Point X="2.239912866837" Y="-2.548853715577" Z="0.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.5" />
                  <Point X="2.308044712285" Y="-2.362118142417" Z="0.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.5" />
                  <Point X="2.470780235955" Y="-2.250856597682" Z="0.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.5" />
                  <Point X="2.679653087935" Y="-2.263535142654" Z="0.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.5" />
                  <Point X="2.987303962003" Y="-2.424237864858" Z="0.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.5" />
                  <Point X="4.205263436533" Y="-2.847380944181" Z="0.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.5" />
                  <Point X="4.367733862343" Y="-2.591277640809" Z="0.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.5" />
                  <Point X="4.161112057066" Y="-2.35764897496" Z="0.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.5" />
                  <Point X="3.112092337112" Y="-1.489146496182" Z="0.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.5" />
                  <Point X="3.104887342365" Y="-1.321105387354" Z="0.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.5" />
                  <Point X="3.196077492814" Y="-1.181432076666" Z="0.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.5" />
                  <Point X="3.363468067248" Y="-1.123708612849" Z="0.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.5" />
                  <Point X="3.696846410924" Y="-1.155093168082" Z="0.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.5" />
                  <Point X="4.974775769551" Y="-1.017440652959" Z="0.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.5" />
                  <Point X="5.036849677771" Y="-0.64321929892" Z="0.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.5" />
                  <Point X="4.791447283777" Y="-0.500414155159" Z="0.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.5" />
                  <Point X="3.67369959692" Y="-0.177891012539" Z="0.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.5" />
                  <Point X="3.610890853046" Y="-0.110568666154" Z="0.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.5" />
                  <Point X="3.585086103161" Y="-0.019066126687" Z="0.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.5" />
                  <Point X="3.596285347263" Y="0.077544404547" Z="0.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.5" />
                  <Point X="3.644488585354" Y="0.153380072421" Z="0.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.5" />
                  <Point X="3.729695817433" Y="0.210257831703" Z="0.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.5" />
                  <Point X="4.004520533529" Y="0.28955777761" Z="0.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.5" />
                  <Point X="4.995118716369" Y="0.908906246445" Z="0.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.5" />
                  <Point X="4.900782471628" Y="1.326521463902" Z="0.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.5" />
                  <Point X="4.601008902438" Y="1.371829859051" Z="0.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.5" />
                  <Point X="3.387543289257" Y="1.23201265304" Z="0.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.5" />
                  <Point X="3.313062441555" Y="1.265934411604" Z="0.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.5" />
                  <Point X="3.260745173634" Y="1.332300870633" Z="0.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.5" />
                  <Point X="3.23707901014" Y="1.415449537762" Z="0.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.5" />
                  <Point X="3.25086822531" Y="1.49412472846" Z="0.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.5" />
                  <Point X="3.301494824235" Y="1.569818551949" Z="0.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.5" />
                  <Point X="3.53677515648" Y="1.756481957005" Z="0.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.5" />
                  <Point X="4.279455449826" Y="2.73254584825" Z="0.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.5" />
                  <Point X="4.048820622612" Y="3.063919496717" Z="0.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.5" />
                  <Point X="3.707738737099" Y="2.958583993521" Z="0.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.5" />
                  <Point X="2.445436181861" Y="2.249766207509" Z="0.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.5" />
                  <Point X="2.373867813266" Y="2.25224843809" Z="0.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.5" />
                  <Point X="2.30935207246" Y="2.288380383869" Z="0.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.5" />
                  <Point X="2.262378216366" Y="2.347672787922" Z="0.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.5" />
                  <Point X="2.247181264669" Y="2.415890630567" Z="0.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.5" />
                  <Point X="2.262761735149" Y="2.494033394021" Z="0.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.5" />
                  <Point X="2.437041359339" Y="2.804400344856" Z="0.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.5" />
                  <Point X="2.827529645801" Y="4.216384670466" Z="0.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.5" />
                  <Point X="2.434255903581" Y="4.455023092266" Z="0.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.5" />
                  <Point X="2.025286609756" Y="4.65530602659" Z="0.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.5" />
                  <Point X="1.601064153245" Y="4.817703141869" Z="0.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.5" />
                  <Point X="0.991661131727" Y="4.975058654269" Z="0.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.5" />
                  <Point X="0.32533396494" Y="5.062489744039" Z="0.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.5" />
                  <Point X="0.155107670679" Y="4.93399421073" Z="0.5" />
                  <Point X="0" Y="4.355124473572" Z="0.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>