<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#147" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1275" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.99528515625" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.681784179688" Y="-28.954705078125" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.54236315918" Y="-28.467375" />
                  <Point X="0.493936431885" Y="-28.3976015625" />
                  <Point X="0.378635375977" Y="-28.231474609375" />
                  <Point X="0.356752044678" Y="-28.209021484375" />
                  <Point X="0.330496856689" Y="-28.18977734375" />
                  <Point X="0.302495178223" Y="-28.17566796875" />
                  <Point X="0.227557617188" Y="-28.15241015625" />
                  <Point X="0.049136035919" Y="-28.09703515625" />
                  <Point X="0.020976638794" Y="-28.092765625" />
                  <Point X="-0.008664536476" Y="-28.092765625" />
                  <Point X="-0.036824085236" Y="-28.09703515625" />
                  <Point X="-0.111761489868" Y="-28.12029296875" />
                  <Point X="-0.290183227539" Y="-28.17566796875" />
                  <Point X="-0.318184906006" Y="-28.18977734375" />
                  <Point X="-0.344440093994" Y="-28.209021484375" />
                  <Point X="-0.366324035645" Y="-28.2314765625" />
                  <Point X="-0.414750793457" Y="-28.301251953125" />
                  <Point X="-0.530051696777" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.833089111328" Y="-29.56533203125" />
                  <Point X="-0.916584594727" Y="-29.87694140625" />
                  <Point X="-1.079328735352" Y="-29.8453515625" />
                  <Point X="-1.162575683594" Y="-29.82393359375" />
                  <Point X="-1.24641784668" Y="-29.802361328125" />
                  <Point X="-1.229225463867" Y="-29.6717734375" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.234411132812" Y="-29.42622265625" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287938354492" Y="-29.182966796875" />
                  <Point X="-1.304010620117" Y="-29.15512890625" />
                  <Point X="-1.32364453125" Y="-29.131205078125" />
                  <Point X="-1.392637451172" Y="-29.07069921875" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.64302734375" Y="-28.890966796875" />
                  <Point X="-1.734596313477" Y="-28.88496484375" />
                  <Point X="-1.952616577148" Y="-28.87067578125" />
                  <Point X="-1.983414916992" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042657836914" Y="-28.89480078125" />
                  <Point X="-2.118958007812" Y="-28.945783203125" />
                  <Point X="-2.300624267578" Y="-29.06716796875" />
                  <Point X="-2.312789306641" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.480147949219" Y="-29.28848828125" />
                  <Point X="-2.498580566406" Y="-29.277076171875" />
                  <Point X="-2.801722900391" Y="-29.08937890625" />
                  <Point X="-2.916963378906" Y="-29.000646484375" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.643947021484" Y="-28.0579921875" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406587890625" Y="-27.616125" />
                  <Point X="-2.405575683594" Y="-27.585189453125" />
                  <Point X="-2.414561035156" Y="-27.555572265625" />
                  <Point X="-2.428778564453" Y="-27.526744140625" />
                  <Point X="-2.446806884766" Y="-27.5015859375" />
                  <Point X="-2.464155517578" Y="-27.48423828125" />
                  <Point X="-2.489311523438" Y="-27.466212890625" />
                  <Point X="-2.518140380859" Y="-27.45199609375" />
                  <Point X="-2.547758789062" Y="-27.44301171875" />
                  <Point X="-2.578693115234" Y="-27.444025390625" />
                  <Point X="-2.610218994141" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.547209960938" Y="-27.98544921875" />
                  <Point X="-3.8180234375" Y="-28.141802734375" />
                  <Point X="-3.843372314453" Y="-28.1085" />
                  <Point X="-4.082862304687" Y="-27.793857421875" />
                  <Point X="-4.165486328125" Y="-27.655310546875" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.491208251953" Y="-26.794130859375" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083065429688" Y="-26.4758828125" />
                  <Point X="-3.063635742188" Y="-26.444578125" />
                  <Point X="-3.054643554688" Y="-26.4207109375" />
                  <Point X="-3.051577636719" Y="-26.41103515625" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.042037597656" Y="-26.358603515625" />
                  <Point X="-3.042736816406" Y="-26.335736328125" />
                  <Point X="-3.048883300781" Y="-26.313697265625" />
                  <Point X="-3.060888183594" Y="-26.28471484375" />
                  <Point X="-3.073803222656" Y="-26.262572265625" />
                  <Point X="-3.09216796875" Y="-26.244689453125" />
                  <Point X="-3.112502929688" Y="-26.229650390625" />
                  <Point X="-3.1208046875" Y="-26.22416015625" />
                  <Point X="-3.139454345703" Y="-26.21318359375" />
                  <Point X="-3.168716064453" Y="-26.201958984375" />
                  <Point X="-3.200603759766" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.3782265625" Y="-26.345296875" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.740411621094" Y="-26.3593515625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.855937988281" Y="-25.83980859375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.971737792969" Y="-25.338001953125" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.512110839844" Y="-25.21212109375" />
                  <Point X="-3.488334472656" Y="-25.199203125" />
                  <Point X="-3.479521240234" Y="-25.1937734375" />
                  <Point X="-3.459977050781" Y="-25.180208984375" />
                  <Point X="-3.436021972656" Y="-25.15868359375" />
                  <Point X="-3.415095703125" Y="-25.128279296875" />
                  <Point X="-3.404933837891" Y="-25.104724609375" />
                  <Point X="-3.401432128906" Y="-25.095251953125" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.389474365234" Y="-25.0455234375" />
                  <Point X="-3.390444091797" Y="-25.011685546875" />
                  <Point X="-3.395427001953" Y="-24.988048828125" />
                  <Point X="-3.397652832031" Y="-24.979486328125" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.417484619141" Y="-24.92916796875" />
                  <Point X="-3.440399169922" Y="-24.89987109375" />
                  <Point X="-3.460426757812" Y="-24.88265625" />
                  <Point X="-3.468185791016" Y="-24.876654296875" />
                  <Point X="-3.487729980469" Y="-24.86308984375" />
                  <Point X="-3.501922607422" Y="-24.854953125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.577780273438" Y="-24.562169921875" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.884854003906" Y="-24.43097265625" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.780481933594" Y="-23.860630859375" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.080253173828" Y="-23.658791015625" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985107422" Y="-23.700658203125" />
                  <Point X="-3.723423828125" Y="-23.698771484375" />
                  <Point X="-3.703137451172" Y="-23.694736328125" />
                  <Point X="-3.684969238281" Y="-23.6890078125" />
                  <Point X="-3.641711425781" Y="-23.675369140625" />
                  <Point X="-3.622784912109" Y="-23.667041015625" />
                  <Point X="-3.604040527344" Y="-23.656220703125" />
                  <Point X="-3.58735546875" Y="-23.64398828125" />
                  <Point X="-3.573714355469" Y="-23.62843359375" />
                  <Point X="-3.561299804688" Y="-23.610703125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.544061523438" Y="-23.574970703125" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532049316406" Y="-23.410623046875" />
                  <Point X="-3.540845703125" Y="-23.393724609375" />
                  <Point X="-3.5617890625" Y="-23.353494140625" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.201495605469" Y="-22.84550390625" />
                  <Point X="-4.351860351563" Y="-22.730125" />
                  <Point X="-4.315712402344" Y="-22.6681953125" />
                  <Point X="-4.081155761719" Y="-22.266345703125" />
                  <Point X="-3.9645859375" Y="-22.116509765625" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.399867431641" Y="-22.04377734375" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187729980469" Y="-22.163658203125" />
                  <Point X="-3.167088378906" Y="-22.17016796875" />
                  <Point X="-3.146791992188" Y="-22.174205078125" />
                  <Point X="-3.121488525391" Y="-22.17641796875" />
                  <Point X="-3.061242675781" Y="-22.181689453125" />
                  <Point X="-3.040561523438" Y="-22.18123828125" />
                  <Point X="-3.019102783203" Y="-22.178412109375" />
                  <Point X="-2.999013916016" Y="-22.17349609375" />
                  <Point X="-2.980465087891" Y="-22.16434765625" />
                  <Point X="-2.962210693359" Y="-22.15271875" />
                  <Point X="-2.946077392578" Y="-22.13976953125" />
                  <Point X="-2.928116699219" Y="-22.12180859375" />
                  <Point X="-2.885353759766" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.845649658203" Y="-21.938576171875" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.135389404297" Y="-21.3584453125" />
                  <Point X="-3.183332519531" Y="-21.27540625" />
                  <Point X="-3.109148925781" Y="-21.218529296875" />
                  <Point X="-2.700619873047" Y="-20.9053125" />
                  <Point X="-2.517038085938" Y="-20.8033203125" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.102343017578" Y="-20.69317578125" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028891479492" Y="-20.78519921875" />
                  <Point X="-2.012311645508" Y="-20.799111328125" />
                  <Point X="-1.995116577148" Y="-20.8106015625" />
                  <Point X="-1.966954101562" Y="-20.825263671875" />
                  <Point X="-1.899900634766" Y="-20.860169921875" />
                  <Point X="-1.88062512207" Y="-20.86766796875" />
                  <Point X="-1.859718261719" Y="-20.873271484375" />
                  <Point X="-1.839268310547" Y="-20.876419921875" />
                  <Point X="-1.818621826172" Y="-20.87506640625" />
                  <Point X="-1.797306518555" Y="-20.871306640625" />
                  <Point X="-1.77745324707" Y="-20.865517578125" />
                  <Point X="-1.748119995117" Y="-20.8533671875" />
                  <Point X="-1.678279296875" Y="-20.8244375" />
                  <Point X="-1.660145385742" Y="-20.814490234375" />
                  <Point X="-1.642416015625" Y="-20.802076171875" />
                  <Point X="-1.626863891602" Y="-20.7884375" />
                  <Point X="-1.614632568359" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.585932739258" Y="-20.703796875" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584201904297" Y="-20.368103515625" />
                  <Point X="-1.478493896484" Y="-20.338466796875" />
                  <Point X="-0.949623474121" Y="-20.19019140625" />
                  <Point X="-0.727077331543" Y="-20.16414453125" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.191060287476" Y="-20.500373046875" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113975525" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907154" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425926208" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441650391" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.282302642822" Y="-20.205798828125" />
                  <Point X="0.307419464111" Y="-20.1120625" />
                  <Point X="0.382263916016" Y="-20.119900390625" />
                  <Point X="0.844045288086" Y="-20.16826171875" />
                  <Point X="1.028174804688" Y="-20.212716796875" />
                  <Point X="1.48102355957" Y="-20.322048828125" />
                  <Point X="1.59983605957" Y="-20.365142578125" />
                  <Point X="1.894649414062" Y="-20.47207421875" />
                  <Point X="2.010541748047" Y="-20.526271484375" />
                  <Point X="2.294556884766" Y="-20.65909765625" />
                  <Point X="2.406566894531" Y="-20.724353515625" />
                  <Point X="2.680973632812" Y="-20.88422265625" />
                  <Point X="2.786573974609" Y="-20.9593203125" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.402429443359" Y="-22.0074921875" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133077148438" Y="-22.483943359375" />
                  <Point X="2.126726806641" Y="-22.507689453125" />
                  <Point X="2.111607421875" Y="-22.56423046875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.110204101563" Y="-22.63958203125" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121443115234" Y="-22.7103984375" />
                  <Point X="2.129709716797" Y="-22.732486328125" />
                  <Point X="2.140069580078" Y="-22.752525390625" />
                  <Point X="2.152775390625" Y="-22.771251953125" />
                  <Point X="2.18302734375" Y="-22.815833984375" />
                  <Point X="2.194461425781" Y="-22.82966796875" />
                  <Point X="2.221594970703" Y="-22.85440625" />
                  <Point X="2.240320068359" Y="-22.86711328125" />
                  <Point X="2.284903564453" Y="-22.897365234375" />
                  <Point X="2.304952392578" Y="-22.90773046875" />
                  <Point X="2.327040039062" Y="-22.91599609375" />
                  <Point X="2.348969482422" Y="-22.92133984375" />
                  <Point X="2.369503662109" Y="-22.923814453125" />
                  <Point X="2.418394287109" Y="-22.9297109375" />
                  <Point X="2.436467529297" Y="-22.93015625" />
                  <Point X="2.4732109375" Y="-22.925828125" />
                  <Point X="2.496957763672" Y="-22.9194765625" />
                  <Point X="2.553497314453" Y="-22.904357421875" />
                  <Point X="2.565285644531" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.639506591797" Y="-22.28307421875" />
                  <Point X="3.967325927734" Y="-22.09380859375" />
                  <Point X="4.123270996094" Y="-22.310537109375" />
                  <Point X="4.182140136719" Y="-22.4078203125" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.566315673828" Y="-23.0740859375" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221423339844" Y="-23.339759765625" />
                  <Point X="3.203974853516" Y="-23.35837109375" />
                  <Point X="3.186884277344" Y="-23.380666015625" />
                  <Point X="3.146192626953" Y="-23.433751953125" />
                  <Point X="3.136605957031" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.115263671875" Y="-23.505677734375" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.102965576172" Y="-23.653560546875" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679443359" Y="-23.731021484375" />
                  <Point X="3.136281982422" Y="-23.764259765625" />
                  <Point X="3.15049609375" Y="-23.785865234375" />
                  <Point X="3.184339599609" Y="-23.8373046875" />
                  <Point X="3.198896972656" Y="-23.8545546875" />
                  <Point X="3.216140380859" Y="-23.870642578125" />
                  <Point X="3.234345214844" Y="-23.883962890625" />
                  <Point X="3.254943603516" Y="-23.89555859375" />
                  <Point X="3.303987304688" Y="-23.923166015625" />
                  <Point X="3.320520751953" Y="-23.930498046875" />
                  <Point X="3.356122070312" Y="-23.9405625" />
                  <Point X="3.383972412109" Y="-23.9442421875" />
                  <Point X="3.450282714844" Y="-23.953005859375" />
                  <Point X="3.462697998047" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.486557128906" Y="-23.821580078125" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.864487304688" Y="-24.186341796875" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.101106445313" Y="-24.567375" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704787841797" Y="-24.674416015625" />
                  <Point X="3.681548828125" Y="-24.684931640625" />
                  <Point X="3.654186767578" Y="-24.70074609375" />
                  <Point X="3.589038818359" Y="-24.73840234375" />
                  <Point X="3.574312988281" Y="-24.74890234375" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.531113037109" Y="-24.79534375" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.458208496094" Y="-24.935970703125" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.021876953125" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.450651367188" Y="-25.08712890625" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.1766640625" />
                  <Point X="3.480300292969" Y="-25.19812890625" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.508441650391" Y="-25.238328125" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.559998291016" Y="-25.301236328125" />
                  <Point X="3.589038330078" Y="-25.324158203125" />
                  <Point X="3.616400634766" Y="-25.33997265625" />
                  <Point X="3.681548339844" Y="-25.377630859375" />
                  <Point X="3.692709716797" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.632115722656" Y="-25.63746875" />
                  <Point X="4.89147265625" Y="-25.706962890625" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.831255371094" Y="-26.052876953125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.870324951172" Y="-26.062150390625" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659667969" Y="-26.005509765625" />
                  <Point X="3.320957275391" Y="-26.017181640625" />
                  <Point X="3.193095458984" Y="-26.04497265625" />
                  <Point X="3.163973632812" Y="-26.05659765625" />
                  <Point X="3.136146972656" Y="-26.073490234375" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.0799375" Y="-26.133" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.965105224609" Y="-26.355921875" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.006170410156" Y="-26.614224609375" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.960251220703" Y="-27.412849609375" />
                  <Point X="4.213121582031" Y="-27.606884765625" />
                  <Point X="4.124811523438" Y="-27.749783203125" />
                  <Point X="4.075657226562" Y="-27.819625" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="3.198126953125" Y="-27.406251953125" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754224609375" Y="-27.159826171875" />
                  <Point X="2.690310302734" Y="-27.148283203125" />
                  <Point X="2.538134277344" Y="-27.12080078125" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.444834960938" Y="-27.135177734375" />
                  <Point X="2.391737792969" Y="-27.16312109375" />
                  <Point X="2.265316894531" Y="-27.22965625" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531494141" Y="-27.290439453125" />
                  <Point X="2.176586914063" Y="-27.343537109375" />
                  <Point X="2.110052490234" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.107218261719" Y="-27.627171875" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.697787109375" Y="-28.77172265625" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.781834960938" Y="-29.111654296875" />
                  <Point X="2.72689453125" Y="-29.147216796875" />
                  <Point X="2.701764404297" Y="-29.163482421875" />
                  <Point X="2.061625244141" Y="-28.329236328125" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506225586" Y="-27.922181640625" />
                  <Point X="1.721924316406" Y="-27.900556640625" />
                  <Point X="1.658887573242" Y="-27.860029296875" />
                  <Point X="1.50880090332" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.75116796875" />
                  <Point X="1.448366088867" Y="-27.7434375" />
                  <Point X="1.417100341797" Y="-27.741119140625" />
                  <Point X="1.348158935547" Y="-27.747462890625" />
                  <Point X="1.184013549805" Y="-27.76256640625" />
                  <Point X="1.156363037109" Y="-27.7693984375" />
                  <Point X="1.128977050781" Y="-27.7807421875" />
                  <Point X="1.104594970703" Y="-27.795462890625" />
                  <Point X="1.051360351563" Y="-27.8397265625" />
                  <Point X="0.924611328125" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624206543" Y="-28.025810546875" />
                  <Point X="0.859707275391" Y="-28.099041015625" />
                  <Point X="0.821809936523" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.974465393066" Y="-29.498359375" />
                  <Point X="1.022065429688" Y="-29.859916015625" />
                  <Point X="0.975680480957" Y="-29.87008203125" />
                  <Point X="0.929315795898" Y="-29.8785078125" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058427246094" Y="-29.752634765625" />
                  <Point X="-1.138904785156" Y="-29.7319296875" />
                  <Point X="-1.14124621582" Y="-29.731328125" />
                  <Point X="-1.135038208008" Y="-29.684173828125" />
                  <Point X="-1.120775756836" Y="-29.57583984375" />
                  <Point X="-1.120077392578" Y="-29.568099609375" />
                  <Point X="-1.119451660156" Y="-29.541033203125" />
                  <Point X="-1.121759033203" Y="-29.509330078125" />
                  <Point X="-1.123333862305" Y="-29.497693359375" />
                  <Point X="-1.141236450195" Y="-29.407689453125" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188125" Y="-29.178470703125" />
                  <Point X="-1.19902734375" Y="-29.14950390625" />
                  <Point X="-1.205666137695" Y="-29.135466796875" />
                  <Point X="-1.22173828125" Y="-29.10762890625" />
                  <Point X="-1.230574829102" Y="-29.094861328125" />
                  <Point X="-1.250208618164" Y="-29.0709375" />
                  <Point X="-1.261006225586" Y="-29.05978125" />
                  <Point X="-1.329999145508" Y="-28.999275390625" />
                  <Point X="-1.494267211914" Y="-28.855216796875" />
                  <Point X="-1.506735839844" Y="-28.84596875" />
                  <Point X="-1.53301953125" Y="-28.829623046875" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576531860352" Y="-28.810224609375" />
                  <Point X="-1.591315673828" Y="-28.805474609375" />
                  <Point X="-1.621456665039" Y="-28.798447265625" />
                  <Point X="-1.636813842773" Y="-28.796169921875" />
                  <Point X="-1.7283828125" Y="-28.79016796875" />
                  <Point X="-1.946403076172" Y="-28.77587890625" />
                  <Point X="-1.961927612305" Y="-28.7761328125" />
                  <Point X="-1.992726074219" Y="-28.779166015625" />
                  <Point X="-2.00799987793" Y="-28.7819453125" />
                  <Point X="-2.039048339844" Y="-28.790263671875" />
                  <Point X="-2.053667236328" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095437255859" Y="-28.815810546875" />
                  <Point X="-2.171737304688" Y="-28.86679296875" />
                  <Point X="-2.353403564453" Y="-28.988177734375" />
                  <Point X="-2.359687255859" Y="-28.992759765625" />
                  <Point X="-2.380448730469" Y="-29.01013671875" />
                  <Point X="-2.402762207031" Y="-29.032775390625" />
                  <Point X="-2.410471191406" Y="-29.041630859375" />
                  <Point X="-2.503202148438" Y="-29.162478515625" />
                  <Point X="-2.747597167969" Y="-29.01115625" />
                  <Point X="-2.859005615234" Y="-28.925375" />
                  <Point X="-2.98086328125" Y="-28.83155078125" />
                  <Point X="-2.561674560547" Y="-28.1054921875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413085938" Y="-27.634658203125" />
                  <Point X="-2.311638671875" Y="-27.619232421875" />
                  <Point X="-2.310626464844" Y="-27.588296875" />
                  <Point X="-2.314667236328" Y="-27.557609375" />
                  <Point X="-2.323652587891" Y="-27.5279921875" />
                  <Point X="-2.329359375" Y="-27.513552734375" />
                  <Point X="-2.343576904297" Y="-27.484724609375" />
                  <Point X="-2.351558349609" Y="-27.471408203125" />
                  <Point X="-2.369586669922" Y="-27.44625" />
                  <Point X="-2.379633544922" Y="-27.434408203125" />
                  <Point X="-2.396982177734" Y="-27.417060546875" />
                  <Point X="-2.408822509766" Y="-27.407015625" />
                  <Point X="-2.433978515625" Y="-27.388990234375" />
                  <Point X="-2.447294189453" Y="-27.381009765625" />
                  <Point X="-2.476123046875" Y="-27.36679296875" />
                  <Point X="-2.490564208984" Y="-27.3610859375" />
                  <Point X="-2.520182617188" Y="-27.3521015625" />
                  <Point X="-2.550870117188" Y="-27.3480625" />
                  <Point X="-2.581804443359" Y="-27.349076171875" />
                  <Point X="-2.597228515625" Y="-27.3508515625" />
                  <Point X="-2.628754394531" Y="-27.357123046875" />
                  <Point X="-2.643685058594" Y="-27.36138671875" />
                  <Point X="-2.672649658203" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.594709960938" Y="-27.903177734375" />
                  <Point X="-3.793087646484" Y="-28.0177109375" />
                  <Point X="-4.004021240234" Y="-27.7405859375" />
                  <Point X="-4.083893798828" Y="-27.60665234375" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.433375976562" Y="-26.8695" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039158691406" Y="-26.566068359375" />
                  <Point X="-3.01626953125" Y="-26.543435546875" />
                  <Point X="-3.002348632812" Y="-26.52598046875" />
                  <Point X="-2.982918945312" Y="-26.49467578125" />
                  <Point X="-2.974736083984" Y="-26.478072265625" />
                  <Point X="-2.965743896484" Y="-26.454205078125" />
                  <Point X="-2.959612060547" Y="-26.434853515625" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951952880859" Y="-26.402396484375" />
                  <Point X="-2.947838623047" Y="-26.3709140625" />
                  <Point X="-2.94708203125" Y="-26.35569921875" />
                  <Point X="-2.94778125" Y="-26.33283203125" />
                  <Point X="-2.951229003906" Y="-26.31021484375" />
                  <Point X="-2.957375488281" Y="-26.28817578125" />
                  <Point X="-2.961114746094" Y="-26.277341796875" />
                  <Point X="-2.973119628906" Y="-26.248359375" />
                  <Point X="-2.978826904297" Y="-26.2368515625" />
                  <Point X="-2.991741943359" Y="-26.214708984375" />
                  <Point X="-3.007527099609" Y="-26.194509765625" />
                  <Point X="-3.025891845703" Y="-26.176626953125" />
                  <Point X="-3.035679199219" Y="-26.16830859375" />
                  <Point X="-3.056014160156" Y="-26.15326953125" />
                  <Point X="-3.072617675781" Y="-26.1422890625" />
                  <Point X="-3.091267333984" Y="-26.1313125" />
                  <Point X="-3.105430175781" Y="-26.124486328125" />
                  <Point X="-3.134691894531" Y="-26.11326171875" />
                  <Point X="-3.149790771484" Y="-26.10886328125" />
                  <Point X="-3.181678466797" Y="-26.102380859375" />
                  <Point X="-3.197294677734" Y="-26.10053515625" />
                  <Point X="-3.228619873047" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.390626464844" Y="-26.251109375" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.740762695312" Y="-25.9741171875" />
                  <Point X="-4.761895019531" Y="-25.826359375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.947149902344" Y="-25.429765625" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.497656005859" Y="-25.308640625" />
                  <Point X="-3.476890869141" Y="-25.3003515625" />
                  <Point X="-3.4667578125" Y="-25.295595703125" />
                  <Point X="-3.442981445313" Y="-25.282677734375" />
                  <Point X="-3.425354980469" Y="-25.271818359375" />
                  <Point X="-3.405810791016" Y="-25.25825390625" />
                  <Point X="-3.396481201172" Y="-25.25087109375" />
                  <Point X="-3.372526123047" Y="-25.229345703125" />
                  <Point X="-3.357766113281" Y="-25.212544921875" />
                  <Point X="-3.33683984375" Y="-25.182140625" />
                  <Point X="-3.327866943359" Y="-25.165912109375" />
                  <Point X="-3.317705078125" Y="-25.142357421875" />
                  <Point X="-3.310701660156" Y="-25.123412109375" />
                  <Point X="-3.304187011719" Y="-25.102421875" />
                  <Point X="-3.301576904297" Y="-25.09194140625" />
                  <Point X="-3.296133789062" Y="-25.063203125" />
                  <Point X="-3.294513427734" Y="-25.042802734375" />
                  <Point X="-3.295483154297" Y="-25.00896484375" />
                  <Point X="-3.297487304688" Y="-24.99208984375" />
                  <Point X="-3.302470214844" Y="-24.968453125" />
                  <Point X="-3.306921875" Y="-24.951328125" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.317668212891" Y="-24.91921484375" />
                  <Point X="-3.330985107422" Y="-24.889888671875" />
                  <Point X="-3.342655029297" Y="-24.870640625" />
                  <Point X="-3.365569580078" Y="-24.84134375" />
                  <Point X="-3.378473876953" Y="-24.827828125" />
                  <Point X="-3.398501464844" Y="-24.81061328125" />
                  <Point X="-3.41401953125" Y="-24.798609375" />
                  <Point X="-3.433563720703" Y="-24.785044921875" />
                  <Point X="-3.440480224609" Y="-24.780673828125" />
                  <Point X="-3.465612548828" Y="-24.767166015625" />
                  <Point X="-3.496565917969" Y="-24.75436328125" />
                  <Point X="-3.508288330078" Y="-24.75038671875" />
                  <Point X="-4.553192382812" Y="-24.47040625" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.6887890625" Y="-23.885478515625" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.092653076172" Y="-23.752978515625" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057128906" Y="-23.795634765625" />
                  <Point X="-3.736703857422" Y="-23.795296875" />
                  <Point X="-3.715142578125" Y="-23.79341015625" />
                  <Point X="-3.704890380859" Y="-23.7919453125" />
                  <Point X="-3.684604003906" Y="-23.78791015625" />
                  <Point X="-3.656401611328" Y="-23.779611328125" />
                  <Point X="-3.613143798828" Y="-23.76597265625" />
                  <Point X="-3.603449462891" Y="-23.76232421875" />
                  <Point X="-3.584522949219" Y="-23.75399609375" />
                  <Point X="-3.575290771484" Y="-23.74931640625" />
                  <Point X="-3.556546386719" Y="-23.73849609375" />
                  <Point X="-3.547870849609" Y="-23.7328359375" />
                  <Point X="-3.531185791016" Y="-23.720603515625" />
                  <Point X="-3.515930664062" Y="-23.706626953125" />
                  <Point X="-3.502289550781" Y="-23.691072265625" />
                  <Point X="-3.495894042969" Y="-23.682921875" />
                  <Point X="-3.483479492188" Y="-23.66519140625" />
                  <Point X="-3.478011230469" Y="-23.6563984375" />
                  <Point X="-3.468062988281" Y="-23.638265625" />
                  <Point X="-3.45629296875" Y="-23.611326171875" />
                  <Point X="-3.438935546875" Y="-23.569421875" />
                  <Point X="-3.435499267578" Y="-23.559650390625" />
                  <Point X="-3.4297109375" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.43601171875" Y="-23.395470703125" />
                  <Point X="-3.443507568359" Y="-23.376193359375" />
                  <Point X="-3.456578857422" Y="-23.349859375" />
                  <Point X="-3.477522216797" Y="-23.30962890625" />
                  <Point X="-3.482800537109" Y="-23.300712890625" />
                  <Point X="-3.494293212891" Y="-23.283513671875" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521497558594" Y="-23.25109375" />
                  <Point X="-3.536439208984" Y="-23.2367890625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-4.143663085938" Y="-22.770134765625" />
                  <Point X="-4.227614257812" Y="-22.705716796875" />
                  <Point X="-4.002292724609" Y="-22.319689453125" />
                  <Point X="-3.889604980469" Y="-22.17484375" />
                  <Point X="-3.726337890625" Y="-21.96498828125" />
                  <Point X="-3.447367431641" Y="-22.12605078125" />
                  <Point X="-3.254156982422" Y="-22.2376015625" />
                  <Point X="-3.244925537109" Y="-22.242279296875" />
                  <Point X="-3.225998535156" Y="-22.250609375" />
                  <Point X="-3.216302978516" Y="-22.254259765625" />
                  <Point X="-3.195661376953" Y="-22.26076953125" />
                  <Point X="-3.185621582031" Y="-22.263341796875" />
                  <Point X="-3.165325195312" Y="-22.26737890625" />
                  <Point X="-3.155068603516" Y="-22.26884375" />
                  <Point X="-3.129765136719" Y="-22.271056640625" />
                  <Point X="-3.069519287109" Y="-22.276328125" />
                  <Point X="-3.059170654297" Y="-22.276666015625" />
                  <Point X="-3.038489501953" Y="-22.27621484375" />
                  <Point X="-3.028156982422" Y="-22.27542578125" />
                  <Point X="-3.006698242188" Y="-22.272599609375" />
                  <Point X="-2.996521240234" Y="-22.270689453125" />
                  <Point X="-2.976432373047" Y="-22.2657734375" />
                  <Point X="-2.9569921875" Y="-22.258697265625" />
                  <Point X="-2.938443359375" Y="-22.249548828125" />
                  <Point X="-2.929422851563" Y="-22.244470703125" />
                  <Point X="-2.911168457031" Y="-22.232841796875" />
                  <Point X="-2.902745605469" Y="-22.226806640625" />
                  <Point X="-2.886612304688" Y="-22.213857421875" />
                  <Point X="-2.878901855469" Y="-22.206943359375" />
                  <Point X="-2.860941162109" Y="-22.188982421875" />
                  <Point X="-2.818178222656" Y="-22.146220703125" />
                  <Point X="-2.811268310547" Y="-22.138513671875" />
                  <Point X="-2.798323486328" Y="-22.12238671875" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.751011230469" Y="-21.930296875" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.053116943359" Y="-21.3109453125" />
                  <Point X="-3.051346191406" Y="-21.293919921875" />
                  <Point X="-2.648374511719" Y="-20.984962890625" />
                  <Point X="-2.470901123047" Y="-20.886365234375" />
                  <Point X="-2.192524169922" Y="-20.731703125" />
                  <Point X="-2.177711914062" Y="-20.7510078125" />
                  <Point X="-2.118564208984" Y="-20.82808984375" />
                  <Point X="-2.111818603516" Y="-20.835953125" />
                  <Point X="-2.097514892578" Y="-20.85089453125" />
                  <Point X="-2.089956298828" Y="-20.85797265625" />
                  <Point X="-2.073376464844" Y="-20.871884765625" />
                  <Point X="-2.065093505859" Y="-20.878099609375" />
                  <Point X="-2.0478984375" Y="-20.88958984375" />
                  <Point X="-2.038986572266" Y="-20.894865234375" />
                  <Point X="-2.01082409668" Y="-20.90952734375" />
                  <Point X="-1.943770751953" Y="-20.94443359375" />
                  <Point X="-1.934341064453" Y="-20.94870703125" />
                  <Point X="-1.915065551758" Y="-20.956205078125" />
                  <Point X="-1.905219238281" Y="-20.9594296875" />
                  <Point X="-1.88431237793" Y="-20.965033203125" />
                  <Point X="-1.874173950195" Y="-20.967166015625" />
                  <Point X="-1.853723999023" Y="-20.970314453125" />
                  <Point X="-1.833053710938" Y="-20.971216796875" />
                  <Point X="-1.812407226562" Y="-20.96986328125" />
                  <Point X="-1.802119750977" Y="-20.968623046875" />
                  <Point X="-1.780804443359" Y="-20.96486328125" />
                  <Point X="-1.770712646484" Y="-20.9625078125" />
                  <Point X="-1.75085949707" Y="-20.95671875" />
                  <Point X="-1.741097900391" Y="-20.95328515625" />
                  <Point X="-1.711764648438" Y="-20.941134765625" />
                  <Point X="-1.641923950195" Y="-20.912205078125" />
                  <Point X="-1.632590087891" Y="-20.907728515625" />
                  <Point X="-1.614456176758" Y="-20.89778125" />
                  <Point X="-1.60565612793" Y="-20.892310546875" />
                  <Point X="-1.587926757812" Y="-20.879896484375" />
                  <Point X="-1.579778686523" Y="-20.873501953125" />
                  <Point X="-1.56422644043" Y="-20.85986328125" />
                  <Point X="-1.550251220703" Y="-20.844611328125" />
                  <Point X="-1.538020019531" Y="-20.8279296875" />
                  <Point X="-1.532359863281" Y="-20.819255859375" />
                  <Point X="-1.521538085938" Y="-20.80051171875" />
                  <Point X="-1.51685534668" Y="-20.791271484375" />
                  <Point X="-1.508524902344" Y="-20.772337890625" />
                  <Point X="-1.504876953125" Y="-20.76264453125" />
                  <Point X="-1.495329467773" Y="-20.73236328125" />
                  <Point X="-1.47259753418" Y="-20.660267578125" />
                  <Point X="-1.470026367188" Y="-20.650236328125" />
                  <Point X="-1.465991088867" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-1.452848144531" Y="-20.429939453125" />
                  <Point X="-0.931164794922" Y="-20.2836796875" />
                  <Point X="-0.716033874512" Y="-20.2585" />
                  <Point X="-0.365222625732" Y="-20.21744140625" />
                  <Point X="-0.28282333374" Y="-20.5249609375" />
                  <Point X="-0.225666244507" Y="-20.7382734375" />
                  <Point X="-0.220435317993" Y="-20.752892578125" />
                  <Point X="-0.207661773682" Y="-20.781083984375" />
                  <Point X="-0.200119155884" Y="-20.79465625" />
                  <Point X="-0.182260925293" Y="-20.8213828125" />
                  <Point X="-0.172608764648" Y="-20.833544921875" />
                  <Point X="-0.151451248169" Y="-20.856134765625" />
                  <Point X="-0.126896362305" Y="-20.8749765625" />
                  <Point X="-0.09960043335" Y="-20.88956640625" />
                  <Point X="-0.085354034424" Y="-20.8957421875" />
                  <Point X="-0.054915912628" Y="-20.90607421875" />
                  <Point X="-0.03985357666" Y="-20.909845703125" />
                  <Point X="-0.009317661285" Y="-20.91488671875" />
                  <Point X="0.021629491806" Y="-20.91488671875" />
                  <Point X="0.052165405273" Y="-20.909845703125" />
                  <Point X="0.067227592468" Y="-20.90607421875" />
                  <Point X="0.097665863037" Y="-20.8957421875" />
                  <Point X="0.111912406921" Y="-20.88956640625" />
                  <Point X="0.139208343506" Y="-20.8749765625" />
                  <Point X="0.16376322937" Y="-20.856134765625" />
                  <Point X="0.18492074585" Y="-20.833544921875" />
                  <Point X="0.194573059082" Y="-20.8213828125" />
                  <Point X="0.212431289673" Y="-20.79465625" />
                  <Point X="0.219973449707" Y="-20.781083984375" />
                  <Point X="0.232746994019" Y="-20.752892578125" />
                  <Point X="0.237978225708" Y="-20.7382734375" />
                  <Point X="0.374065673828" Y="-20.23038671875" />
                  <Point X="0.378190582275" Y="-20.2149921875" />
                  <Point X="0.827879760742" Y="-20.262087890625" />
                  <Point X="1.005879211426" Y="-20.3050625" />
                  <Point X="1.453591430664" Y="-20.413154296875" />
                  <Point X="1.567443969727" Y="-20.45444921875" />
                  <Point X="1.858254516602" Y="-20.5599296875" />
                  <Point X="1.970297973633" Y="-20.612326171875" />
                  <Point X="2.250427734375" Y="-20.7433359375" />
                  <Point X="2.358744628906" Y="-20.806439453125" />
                  <Point X="2.629427490234" Y="-20.964138671875" />
                  <Point X="2.731517333984" Y="-21.036740234375" />
                  <Point X="2.817779052734" Y="-21.098083984375" />
                  <Point X="2.320156982422" Y="-21.9599921875" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053180419922" Y="-22.426564453125" />
                  <Point X="2.044182495117" Y="-22.450435546875" />
                  <Point X="2.041302368164" Y="-22.459400390625" />
                  <Point X="2.034951904297" Y="-22.483146484375" />
                  <Point X="2.019832519531" Y="-22.5396875" />
                  <Point X="2.017912719727" Y="-22.54853515625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.015887329102" Y="-22.650955078125" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.023801025391" Y="-22.710966796875" />
                  <Point X="2.029144775391" Y="-22.732892578125" />
                  <Point X="2.032470214844" Y="-22.743697265625" />
                  <Point X="2.040736816406" Y="-22.76578515625" />
                  <Point X="2.04532019043" Y="-22.776115234375" />
                  <Point X="2.055679931641" Y="-22.796154296875" />
                  <Point X="2.061456542969" Y="-22.80586328125" />
                  <Point X="2.074162353516" Y="-22.82458984375" />
                  <Point X="2.104414306641" Y="-22.869171875" />
                  <Point X="2.109801513672" Y="-22.876357421875" />
                  <Point X="2.130456298828" Y="-22.89987109375" />
                  <Point X="2.15758984375" Y="-22.924609375" />
                  <Point X="2.168250244141" Y="-22.933015625" />
                  <Point X="2.186975341797" Y="-22.94572265625" />
                  <Point X="2.231558837891" Y="-22.975974609375" />
                  <Point X="2.241274414062" Y="-22.98175390625" />
                  <Point X="2.261323242188" Y="-22.992119140625" />
                  <Point X="2.271656494141" Y="-22.996705078125" />
                  <Point X="2.293744140625" Y="-23.004970703125" />
                  <Point X="2.304548583984" Y="-23.008294921875" />
                  <Point X="2.326478027344" Y="-23.013638671875" />
                  <Point X="2.337603027344" Y="-23.015658203125" />
                  <Point X="2.358137207031" Y="-23.0181328125" />
                  <Point X="2.407027832031" Y="-23.024029296875" />
                  <Point X="2.416054199219" Y="-23.024681640625" />
                  <Point X="2.447581054688" Y="-23.02450390625" />
                  <Point X="2.484324462891" Y="-23.02017578125" />
                  <Point X="2.4977578125" Y="-23.0176015625" />
                  <Point X="2.521504638672" Y="-23.01125" />
                  <Point X="2.578044189453" Y="-22.996130859375" />
                  <Point X="2.583996337891" Y="-22.994328125" />
                  <Point X="2.604412841797" Y="-22.9869296875" />
                  <Point X="2.627661132812" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.687006591797" Y="-22.365345703125" />
                  <Point X="3.940403320312" Y="-22.219046875" />
                  <Point X="4.043951904297" Y="-22.36295703125" />
                  <Point X="4.100862792969" Y="-22.45700390625" />
                  <Point X="4.136883789062" Y="-22.516529296875" />
                  <Point X="3.508483398438" Y="-22.998716796875" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168137939453" Y="-23.260130859375" />
                  <Point X="3.152117919922" Y="-23.27478515625" />
                  <Point X="3.134669433594" Y="-23.293396484375" />
                  <Point X="3.128578613281" Y="-23.30057421875" />
                  <Point X="3.111488037109" Y="-23.322869140625" />
                  <Point X="3.070796386719" Y="-23.375955078125" />
                  <Point X="3.06563671875" Y="-23.383396484375" />
                  <Point X="3.049738769531" Y="-23.41062890625" />
                  <Point X="3.034762695312" Y="-23.444455078125" />
                  <Point X="3.030140380859" Y="-23.457328125" />
                  <Point X="3.023774169922" Y="-23.480091796875" />
                  <Point X="3.008616455078" Y="-23.53429296875" />
                  <Point X="3.006225341797" Y="-23.545337890625" />
                  <Point X="3.002771728516" Y="-23.56763671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.009925537109" Y="-23.6727578125" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034682861328" Y="-23.771388671875" />
                  <Point X="3.050285400391" Y="-23.804626953125" />
                  <Point X="3.056917236328" Y="-23.81647265625" />
                  <Point X="3.071131347656" Y="-23.838078125" />
                  <Point X="3.104974853516" Y="-23.889517578125" />
                  <Point X="3.111737548828" Y="-23.89857421875" />
                  <Point X="3.126294921875" Y="-23.91582421875" />
                  <Point X="3.134089599609" Y="-23.924017578125" />
                  <Point X="3.151333007812" Y="-23.94010546875" />
                  <Point X="3.160042724609" Y="-23.947310546875" />
                  <Point X="3.178247558594" Y="-23.960630859375" />
                  <Point X="3.187742675781" Y="-23.96674609375" />
                  <Point X="3.208341064453" Y="-23.978341796875" />
                  <Point X="3.257384765625" Y="-24.00594921875" />
                  <Point X="3.265475097656" Y="-24.010009765625" />
                  <Point X="3.294677246094" Y="-24.021916015625" />
                  <Point X="3.330278564453" Y="-24.03198046875" />
                  <Point X="3.343678466797" Y="-24.034744140625" />
                  <Point X="3.371528808594" Y="-24.038423828125" />
                  <Point X="3.437839111328" Y="-24.0471875" />
                  <Point X="3.444034423828" Y="-24.04780078125" />
                  <Point X="3.465708251953" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.49895703125" Y="-23.915767578125" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.770618164062" Y="-24.20095703125" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.076518554688" Y="-24.475611328125" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.686021728516" Y="-24.580458984375" />
                  <Point X="3.665623535156" Y="-24.587865234375" />
                  <Point X="3.642384521484" Y="-24.598380859375" />
                  <Point X="3.634010498047" Y="-24.602681640625" />
                  <Point X="3.6066484375" Y="-24.61849609375" />
                  <Point X="3.541500488281" Y="-24.65615234375" />
                  <Point X="3.533885498047" Y="-24.661052734375" />
                  <Point X="3.508776611328" Y="-24.680126953125" />
                  <Point X="3.481993896484" Y="-24.7056484375" />
                  <Point X="3.472795654297" Y="-24.715775390625" />
                  <Point X="3.456378417969" Y="-24.7366953125" />
                  <Point X="3.417289794922" Y="-24.78650390625" />
                  <Point X="3.410854736328" Y="-24.795791015625" />
                  <Point X="3.399130615234" Y="-24.8150703125" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.38000390625" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.370376708984" Y="-24.88952734375" />
                  <Point X="3.364904296875" Y="-24.9181015625" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350603759766" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026263671875" />
                  <Point X="3.350280029297" Y="-25.06294140625" />
                  <Point X="3.351874511719" Y="-25.076423828125" />
                  <Point X="3.357347167969" Y="-25.104998046875" />
                  <Point X="3.370376708984" Y="-25.173033203125" />
                  <Point X="3.373158447266" Y="-25.183986328125" />
                  <Point X="3.38000390625" Y="-25.205486328125" />
                  <Point X="3.384067626953" Y="-25.216033203125" />
                  <Point X="3.393841552734" Y="-25.237498046875" />
                  <Point X="3.399130615234" Y="-25.247490234375" />
                  <Point X="3.410854736328" Y="-25.26676953125" />
                  <Point X="3.417289794922" Y="-25.276056640625" />
                  <Point X="3.43370703125" Y="-25.2969765625" />
                  <Point X="3.472795654297" Y="-25.34678515625" />
                  <Point X="3.478716796875" Y="-25.3536328125" />
                  <Point X="3.501139160156" Y="-25.375806640625" />
                  <Point X="3.530179199219" Y="-25.398728515625" />
                  <Point X="3.541500488281" Y="-25.406408203125" />
                  <Point X="3.568862792969" Y="-25.42222265625" />
                  <Point X="3.634010498047" Y="-25.459880859375" />
                  <Point X="3.639496582031" Y="-25.46281640625" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026367188" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.607527832031" Y="-25.729232421875" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761613769531" Y="-25.93105078125" />
                  <Point X="4.738636230469" Y="-26.0317421875" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.882724853516" Y="-25.967962890625" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400096923828" Y="-25.90804296875" />
                  <Point X="3.366720947266" Y="-25.910841796875" />
                  <Point X="3.354483154297" Y="-25.912677734375" />
                  <Point X="3.300780761719" Y="-25.924349609375" />
                  <Point X="3.172918945312" Y="-25.952140625" />
                  <Point X="3.157875244141" Y="-25.9567421875" />
                  <Point X="3.128753417969" Y="-25.9683671875" />
                  <Point X="3.114675292969" Y="-25.975390625" />
                  <Point X="3.086848632812" Y="-25.992283203125" />
                  <Point X="3.074123291016" Y="-26.00153125" />
                  <Point X="3.050373535156" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="3.006889404297" Y="-26.072263671875" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606201172" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.870504882812" Y="-26.347216796875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.926260498047" Y="-26.665599609375" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.902418945312" Y="-27.48821875" />
                  <Point X="4.087169677734" Y="-27.629984375" />
                  <Point X="4.045488769531" Y="-27.6974296875" />
                  <Point X="4.001273925781" Y="-27.76025390625" />
                  <Point X="3.245626953125" Y="-27.32398046875" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841199951172" Y="-27.090890625" />
                  <Point X="2.815021240234" Y="-27.07951171875" />
                  <Point X="2.783116943359" Y="-27.069326171875" />
                  <Point X="2.771108642578" Y="-27.066337890625" />
                  <Point X="2.707194335938" Y="-27.054794921875" />
                  <Point X="2.555018310547" Y="-27.0273125" />
                  <Point X="2.539359375" Y="-27.02580859375" />
                  <Point X="2.508008789062" Y="-27.025404296875" />
                  <Point X="2.492317138672" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444845458984" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400592285156" Y="-27.051109375" />
                  <Point X="2.347495117188" Y="-27.079052734375" />
                  <Point X="2.22107421875" Y="-27.145587890625" />
                  <Point X="2.208972167969" Y="-27.153169921875" />
                  <Point X="2.186040771484" Y="-27.1700625" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144941162109" Y="-27.21116015625" />
                  <Point X="2.128046875" Y="-27.234091796875" />
                  <Point X="2.120463134766" Y="-27.2461953125" />
                  <Point X="2.092518554688" Y="-27.29929296875" />
                  <Point X="2.02598425293" Y="-27.425712890625" />
                  <Point X="2.019835571289" Y="-27.440193359375" />
                  <Point X="2.010012084961" Y="-27.46996875" />
                  <Point X="2.006337158203" Y="-27.485263671875" />
                  <Point X="2.001379394531" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.01373059082" Y="-27.6440546875" />
                  <Point X="2.041213378906" Y="-27.796232421875" />
                  <Point X="2.043014770508" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.615514648438" Y="-28.81922265625" />
                  <Point X="2.735893066406" Y="-29.027724609375" />
                  <Point X="2.72375390625" Y="-29.0360859375" />
                  <Point X="2.136993896484" Y="-28.271404296875" />
                  <Point X="1.833914672852" Y="-27.876423828125" />
                  <Point X="1.828657104492" Y="-27.870150390625" />
                  <Point X="1.808835693359" Y="-27.849630859375" />
                  <Point X="1.78325390625" Y="-27.828005859375" />
                  <Point X="1.773299682617" Y="-27.820646484375" />
                  <Point X="1.710262939453" Y="-27.780119140625" />
                  <Point X="1.560176269531" Y="-27.68362890625" />
                  <Point X="1.546279663086" Y="-27.676244140625" />
                  <Point X="1.517465209961" Y="-27.663873046875" />
                  <Point X="1.502547363281" Y="-27.65888671875" />
                  <Point X="1.470927001953" Y="-27.65115625" />
                  <Point X="1.455391113281" Y="-27.648697265625" />
                  <Point X="1.424125366211" Y="-27.64637890625" />
                  <Point X="1.408395507812" Y="-27.64651953125" />
                  <Point X="1.339454101562" Y="-27.65286328125" />
                  <Point X="1.17530871582" Y="-27.667966796875" />
                  <Point X="1.161225708008" Y="-27.67033984375" />
                  <Point X="1.133575317383" Y="-27.677171875" />
                  <Point X="1.1200078125" Y="-27.681630859375" />
                  <Point X="1.092621826172" Y="-27.692974609375" />
                  <Point X="1.079875854492" Y="-27.699416015625" />
                  <Point X="1.055493896484" Y="-27.71413671875" />
                  <Point X="1.043857299805" Y="-27.722416015625" />
                  <Point X="0.990622619629" Y="-27.7666796875" />
                  <Point X="0.863873596191" Y="-27.872068359375" />
                  <Point X="0.852653869629" Y="-27.88308984375" />
                  <Point X="0.832183776855" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041259766" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394287109" Y="-27.99058984375" />
                  <Point X="0.782791748047" Y="-28.0056328125" />
                  <Point X="0.766874816895" Y="-28.07886328125" />
                  <Point X="0.728977478027" Y="-28.25321875" />
                  <Point X="0.727584594727" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091247559" Y="-29.15233984375" />
                  <Point X="0.773547119141" Y="-28.9301171875" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605957031" Y="-28.480123046875" />
                  <Point X="0.642146911621" Y="-28.453583984375" />
                  <Point X="0.626788391113" Y="-28.42381640625" />
                  <Point X="0.620407409668" Y="-28.41320703125" />
                  <Point X="0.571980712891" Y="-28.34343359375" />
                  <Point X="0.456679656982" Y="-28.177306640625" />
                  <Point X="0.446668273926" Y="-28.16516796875" />
                  <Point X="0.424784881592" Y="-28.14271484375" />
                  <Point X="0.41291317749" Y="-28.132400390625" />
                  <Point X="0.386658050537" Y="-28.11315625" />
                  <Point X="0.373244995117" Y="-28.104939453125" />
                  <Point X="0.345243377686" Y="-28.090830078125" />
                  <Point X="0.330654571533" Y="-28.0849375" />
                  <Point X="0.255716964722" Y="-28.0616796875" />
                  <Point X="0.077295455933" Y="-28.0063046875" />
                  <Point X="0.063377220154" Y="-28.003109375" />
                  <Point X="0.035217788696" Y="-27.99883984375" />
                  <Point X="0.02097659111" Y="-27.997765625" />
                  <Point X="-0.008664463997" Y="-27.997765625" />
                  <Point X="-0.022905660629" Y="-27.99883984375" />
                  <Point X="-0.051065093994" Y="-28.003109375" />
                  <Point X="-0.064983482361" Y="-28.0063046875" />
                  <Point X="-0.139920928955" Y="-28.0295625" />
                  <Point X="-0.31834274292" Y="-28.0849375" />
                  <Point X="-0.332931427002" Y="-28.090830078125" />
                  <Point X="-0.360933013916" Y="-28.104939453125" />
                  <Point X="-0.374346069336" Y="-28.11315625" />
                  <Point X="-0.400601348877" Y="-28.132400390625" />
                  <Point X="-0.412474853516" Y="-28.142716796875" />
                  <Point X="-0.434358825684" Y="-28.165171875" />
                  <Point X="-0.444369018555" Y="-28.177310546875" />
                  <Point X="-0.49279586792" Y="-28.2470859375" />
                  <Point X="-0.608096618652" Y="-28.4132109375" />
                  <Point X="-0.612471191406" Y="-28.4201328125" />
                  <Point X="-0.625976257324" Y="-28.445263671875" />
                  <Point X="-0.638777709961" Y="-28.47621484375" />
                  <Point X="-0.642753112793" Y="-28.487935546875" />
                  <Point X="-0.924851989746" Y="-29.540744140625" />
                  <Point X="-0.985424926758" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.36198619459" Y="-20.229519978331" />
                  <Point X="-1.467624158133" Y="-20.525774777783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.337398473857" Y="-20.321282955556" />
                  <Point X="-1.465061447143" Y="-20.623439338582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.13736874526" Y="-20.803583536178" />
                  <Point X="-2.493806949793" Y="-20.899090865234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.412806183514" Y="-20.218617460594" />
                  <Point X="0.374466572575" Y="-20.228890528383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.312810753124" Y="-20.413045932781" />
                  <Point X="-1.494481415618" Y="-20.729673632515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.060319762532" Y="-20.881289560618" />
                  <Point X="-2.756436148584" Y="-21.067813384098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.676709783833" Y="-20.246255941148" />
                  <Point X="0.346075106519" Y="-20.334849235924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.28822303239" Y="-20.504808910005" />
                  <Point X="-1.548752024886" Y="-20.842566635581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.938254490608" Y="-20.946933506721" />
                  <Point X="-2.953632622255" Y="-21.219003257108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.910359220945" Y="-20.282001000301" />
                  <Point X="0.317683637113" Y="-20.440807944362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.263635187074" Y="-20.596571853848" />
                  <Point X="-3.036583929275" Y="-21.339581229974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.103439330342" Y="-20.328616578052" />
                  <Point X="0.289292167706" Y="-20.546766652801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.239047306698" Y="-20.688334788297" />
                  <Point X="-2.98740815764" Y="-21.424755858816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.296519386246" Y="-20.375232170137" />
                  <Point X="0.260900698299" Y="-20.652725361239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.208796209067" Y="-20.778580268255" />
                  <Point X="-2.938232386005" Y="-21.509930487658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.482675156408" Y="-20.423703118995" />
                  <Point X="0.22979310663" Y="-20.759411852444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.146205143282" Y="-20.860160279864" />
                  <Point X="-2.88905661437" Y="-21.5951051165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.638625460517" Y="-20.480267598089" />
                  <Point X="0.112727536434" Y="-20.889130714579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.016604820267" Y="-20.91488671875" />
                  <Point X="-2.839880842735" Y="-21.680279745343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.794574869982" Y="-20.536832316901" />
                  <Point X="-2.7907050711" Y="-21.765454374185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.665805017428" Y="-21.9999366981" />
                  <Point X="-3.77663086817" Y="-22.029632395307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.93736200873" Y="-20.596923855523" />
                  <Point X="-2.758656320339" Y="-21.855218174439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.549453677565" Y="-22.067111687684" />
                  <Point X="-3.873299917795" Y="-22.153886026226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.071061009513" Y="-20.659450553373" />
                  <Point X="-2.749196904854" Y="-21.951034768838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.433102467532" Y="-22.134286712056" />
                  <Point X="-3.969967262763" Y="-22.278139200383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.204758623157" Y="-20.721977622906" />
                  <Point X="-2.760728602987" Y="-22.052475915179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.316752186608" Y="-22.201461985383" />
                  <Point X="-4.047586773377" Y="-22.397288522708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326563555995" Y="-20.787691326657" />
                  <Point X="-2.845499834899" Y="-22.17354153545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.182761849597" Y="-22.263910619926" />
                  <Point X="-4.115636744798" Y="-22.513873694734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.442197072104" Y="-20.855058656537" />
                  <Point X="-4.183686716218" Y="-22.63045886676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.557830008181" Y="-20.922426141836" />
                  <Point X="-4.193938539723" Y="-22.731557071728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667681102542" Y="-20.991342866953" />
                  <Point X="-4.09893861193" Y="-22.804453154934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.768132156001" Y="-21.062778325438" />
                  <Point X="-4.003938803519" Y="-22.877349270128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.783804204422" Y="-21.156930249859" />
                  <Point X="-3.908938995109" Y="-22.950245385323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.716629197992" Y="-21.273280975723" />
                  <Point X="-3.813939186699" Y="-23.023141500517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.649454191563" Y="-21.389631701586" />
                  <Point X="-3.718939378289" Y="-23.096037615711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.582279185134" Y="-21.505982427449" />
                  <Point X="-3.623939569879" Y="-23.168933730906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.515104178704" Y="-21.622333153312" />
                  <Point X="-3.530685398322" Y="-23.242297588085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.447929172275" Y="-21.738683879176" />
                  <Point X="-3.469858485471" Y="-23.324350303048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.380754165846" Y="-21.855034605039" />
                  <Point X="-3.430926503555" Y="-23.412269747072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.50156002685" Y="-23.699145135029" />
                  <Point X="-4.64900167098" Y="-23.738652004504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.313579142378" Y="-21.971385335468" />
                  <Point X="-3.423650536338" Y="-23.508671394672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255436488087" Y="-23.731547768718" />
                  <Point X="-4.677739778962" Y="-23.844703594469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.24640396195" Y="-22.087736107954" />
                  <Point X="-3.458491513001" Y="-23.616358243371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.009312981075" Y="-23.763950410915" />
                  <Point X="-4.70647781562" Y="-23.950755165323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179228781523" Y="-22.20408688044" />
                  <Point X="-3.564587300273" Y="-23.74313776103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.75949604967" Y="-23.795363403028" />
                  <Point X="-4.733379815881" Y="-24.056314771706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.112053601095" Y="-22.320437652926" />
                  <Point X="-4.748534099763" Y="-24.158726586973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.049826358176" Y="-22.435462629552" />
                  <Point X="-4.763688383644" Y="-24.26113840224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.019334167033" Y="-22.541984224683" />
                  <Point X="-4.778842667526" Y="-24.363550217507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01475469283" Y="-22.641562528236" />
                  <Point X="-4.681887372858" Y="-24.435922361738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.943353672042" Y="-22.223147226372" />
                  <Point X="3.9245957288" Y="-22.228173402115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030044784315" Y="-22.73581679771" />
                  <Point X="-4.49836171104" Y="-24.485098046002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.002682424547" Y="-22.305601372189" />
                  <Point X="3.606720002194" Y="-22.411699183492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072874567043" Y="-22.822691829155" />
                  <Point X="-4.314835565331" Y="-24.534273600608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.059540448658" Y="-22.388717547685" />
                  <Point X="3.288843758754" Y="-22.595225103354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.135324594685" Y="-22.90430963182" />
                  <Point X="-4.131309419622" Y="-24.583449155215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.110752353788" Y="-22.473346596201" />
                  <Point X="2.970967515314" Y="-22.778751023215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.23260238696" Y="-22.976595363077" />
                  <Point X="-3.947783273913" Y="-24.632624709821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.040430151096" Y="-22.590540610761" />
                  <Point X="2.653091271874" Y="-22.962276943077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420282736909" Y="-23.024657802012" />
                  <Point X="-3.764257128204" Y="-24.681800264427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.843481916349" Y="-22.741663968351" />
                  <Point X="-3.580730982495" Y="-24.730975819033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.646533681602" Y="-22.892787325942" />
                  <Point X="-3.428523347707" Y="-24.788543143349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449585766248" Y="-23.043910597951" />
                  <Point X="-3.347022759315" Y="-24.865056363646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.252638599519" Y="-23.195033669366" />
                  <Point X="-3.306597565939" Y="-24.952575702866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.103445770583" Y="-23.333361004535" />
                  <Point X="-3.29490997377" Y="-25.047795259122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.03251371957" Y="-23.450718427461" />
                  <Point X="-3.322532885003" Y="-25.153548033018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.004484153185" Y="-23.556580164277" />
                  <Point X="-3.448412948975" Y="-25.285628731641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.006154953745" Y="-23.654483711755" />
                  <Point X="-4.774351594218" Y="-25.739264157987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.026509413846" Y="-23.747380987747" />
                  <Point X="-4.760804342484" Y="-25.833985419964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.068725066787" Y="-23.834420574773" />
                  <Point X="-4.747257298435" Y="-25.92870673759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.127448587481" Y="-23.917036891965" />
                  <Point X="-4.728522159155" Y="-26.022037909289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.226841396347" Y="-23.988755906235" />
                  <Point X="-4.705009295418" Y="-26.114088893578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.396308048748" Y="-24.041698690719" />
                  <Point X="-4.681496431682" Y="-26.206139877867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.976776673378" Y="-23.984513828657" />
                  <Point X="-4.570792538806" Y="-26.27482809631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.698378107651" Y="-23.889512544225" />
                  <Point X="-3.849203132651" Y="-26.179830034803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.726984147111" Y="-23.980198816192" />
                  <Point X="-3.189608951134" Y="-26.101443543673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.749461236259" Y="-24.072527335446" />
                  <Point X="-3.045365562001" Y="-26.16114488118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765275438135" Y="-24.166641169963" />
                  <Point X="-2.97671877611" Y="-26.241102267476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.779974789872" Y="-24.261053727675" />
                  <Point X="-2.947947085614" Y="-26.331744153382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.480133838887" Y="-24.707696297919" />
                  <Point X="-2.959157716494" Y="-26.433099270012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.391711577978" Y="-24.829740208461" />
                  <Point X="-3.020872456965" Y="-26.547986922021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361437628611" Y="-24.936203325885" />
                  <Point X="-3.207831246248" Y="-26.696433615766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349120156298" Y="-25.037855019783" />
                  <Point X="-3.404779108658" Y="-26.847556873589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362630615729" Y="-25.132586140228" />
                  <Point X="-3.601726952375" Y="-26.998680126403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.387785093586" Y="-25.224197255339" />
                  <Point X="-3.798674792917" Y="-27.149803378366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.442309812561" Y="-25.307938638061" />
                  <Point X="-3.995622633458" Y="-27.300926630329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.515061785198" Y="-25.386796042884" />
                  <Point X="-4.178362204691" Y="-27.448242788005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.626202166511" Y="-25.455367304604" />
                  <Point X="-4.12779085242" Y="-27.533043472143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789350698161" Y="-25.510003024441" />
                  <Point X="-4.077219462203" Y="-27.617844146113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.972876816472" Y="-25.559178586389" />
                  <Point X="-4.026647822412" Y="-27.70264475321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.156402934783" Y="-25.608354148336" />
                  <Point X="-2.451499066173" Y="-27.378936153156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890386048048" Y="-27.496535565518" />
                  <Point X="-3.969662312613" Y="-27.785726769018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.339929053094" Y="-25.657529710284" />
                  <Point X="3.404704172224" Y="-25.908122461854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.084650706689" Y="-25.993880529479" />
                  <Point X="-2.364122645982" Y="-27.453874949068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.208261414709" Y="-27.680061250448" />
                  <Point X="-3.907483614137" Y="-27.867417274114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523455171405" Y="-25.706705272231" />
                  <Point X="3.658564883889" Y="-25.938451926312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.963236485662" Y="-26.124764609092" />
                  <Point X="-2.319895669794" Y="-27.540375603653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.526136781371" Y="-27.863586935377" />
                  <Point X="-3.845304915661" Y="-27.94910777921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.70698224521" Y="-25.755880578155" />
                  <Point X="3.904688744416" Y="-25.970854473785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.88821027286" Y="-26.243219059362" />
                  <Point X="-2.313902972823" Y="-27.637121102478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.775983028553" Y="-25.83574311112" />
                  <Point X="4.150812423464" Y="-26.003257069885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.870590018959" Y="-26.346291629304" />
                  <Point X="-2.35429169707" Y="-27.746294465663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.759938165758" Y="-25.938393556287" />
                  <Point X="4.396936102511" Y="-26.035659665985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86131115517" Y="-26.447129130502" />
                  <Point X="-2.421466826753" Y="-27.862645224551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.73603296865" Y="-26.043150171686" />
                  <Point X="4.643059781559" Y="-26.068062262085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.867418877959" Y="-26.543843808252" />
                  <Point X="-2.488641956435" Y="-27.97899598344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.904777631547" Y="-26.632184797537" />
                  <Point X="-2.555817086117" Y="-28.095346742329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.958716905817" Y="-26.716083049695" />
                  <Point X="-2.622991986132" Y="-28.211697439679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015516658872" Y="-26.799214838873" />
                  <Point X="-2.690166864207" Y="-28.328048131149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.102345931636" Y="-26.874300242495" />
                  <Point X="2.537021581464" Y="-27.025778445586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.357137552125" Y="-27.073978225978" />
                  <Point X="-2.757341742282" Y="-28.44439882262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.197345829357" Y="-26.947196333759" />
                  <Point X="2.760113997549" Y="-27.064352249997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.128393550101" Y="-27.233621233733" />
                  <Point X="-2.824516620358" Y="-28.560749514091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.292345727079" Y="-27.020092425022" />
                  <Point X="2.900931840297" Y="-27.124971459892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.06660616972" Y="-27.348528349547" />
                  <Point X="-2.891691498433" Y="-28.677100205562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.387345624801" Y="-27.092988516286" />
                  <Point X="3.017282925461" Y="-27.192146517722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012880185018" Y="-27.4612754209" />
                  <Point X="1.276113595083" Y="-27.658691433683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.047220468358" Y="-27.720023162142" />
                  <Point X="0.010671485553" Y="-27.997765625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.325603589709" Y="-28.087870259851" />
                  <Point X="-2.958866376508" Y="-28.793450897033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.482345522522" Y="-27.165884607549" />
                  <Point X="3.133634010626" Y="-27.259321575553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000663068313" Y="-27.562900224594" />
                  <Point X="1.556797804673" Y="-27.681833563534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.871976706884" Y="-27.865330823647" />
                  <Point X="0.201727621994" Y="-28.044923524671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.479098607592" Y="-28.227350363074" />
                  <Point X="-2.917133148341" Y="-28.880619749387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.577345420244" Y="-27.238780698812" />
                  <Point X="3.249985080614" Y="-27.32649663745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.016089761657" Y="-27.657117891709" />
                  <Point X="1.665189237798" Y="-27.751141403701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789291367623" Y="-27.985837530667" />
                  <Point X="0.363368896598" Y="-28.099963112816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.56295485809" Y="-28.348170814814" />
                  <Point X="-2.822373927179" Y="-28.95358032974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.672345317966" Y="-27.311676790076" />
                  <Point X="3.366335760621" Y="-27.393671803842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.033031826212" Y="-27.750929516333" />
                  <Point X="1.77316579904" Y="-27.820560408454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764259411079" Y="-28.090896060346" />
                  <Point X="0.453981240419" Y="-28.174034845604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.634416392763" Y="-28.465670112459" />
                  <Point X="-1.822450122826" Y="-28.78400279101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.1987247037" Y="-28.884825261088" />
                  <Point X="-2.724219333795" Y="-29.025631122849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.767345215687" Y="-27.384572881339" />
                  <Point X="3.482686440628" Y="-27.460846970235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056036200389" Y="-27.843116749988" />
                  <Point X="1.850596593481" Y="-27.898164126753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.741560083647" Y="-28.195329563939" />
                  <Point X="0.511898897606" Y="-28.256867093272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.665362010702" Y="-28.572313202934" />
                  <Point X="-1.56739053183" Y="-28.81401101672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.4085216948" Y="-29.039391432566" />
                  <Point X="-2.613353571196" Y="-29.094275968431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.862345113409" Y="-27.457468972603" />
                  <Point X="3.599037120635" Y="-27.528022136627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.101680582912" Y="-27.929237611691" />
                  <Point X="1.913193871234" Y="-27.97974247387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724728986288" Y="-28.298190680023" />
                  <Point X="0.569455994139" Y="-28.339795952876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.69375353094" Y="-28.678271924992" />
                  <Point X="-1.461468358209" Y="-28.883980492977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.957344667668" Y="-27.530365155896" />
                  <Point X="3.715387800642" Y="-27.595197303019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.150856281087" Y="-28.014412260217" />
                  <Point X="1.975791148987" Y="-28.061320820986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733286840873" Y="-28.394248846937" />
                  <Point X="0.626254161804" Y="-28.422928166858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.722145051178" Y="-28.784230647051" />
                  <Point X="-1.375566214019" Y="-28.959314319952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.052343971339" Y="-27.603261406335" />
                  <Point X="3.831738480649" Y="-27.662372469412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.200031979262" Y="-28.099586908743" />
                  <Point X="2.03838842674" Y="-28.142899168103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.745793807997" Y="-28.489248852336" />
                  <Point X="0.6614709476" Y="-28.511843094683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.750536571416" Y="-28.89018936911" />
                  <Point X="-1.289664559419" Y="-29.034648278112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.040267489661" Y="-27.704848526987" />
                  <Point X="3.948089160655" Y="-27.729547635804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.249207677438" Y="-28.184761557268" />
                  <Point X="2.100985704493" Y="-28.22447751522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758300775121" Y="-28.584248857734" />
                  <Point X="0.686058749957" Y="-28.603606050036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.778928091654" Y="-28.996148091168" />
                  <Point X="-1.218153385979" Y="-29.113838154078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.298383375613" Y="-28.269936205794" />
                  <Point X="2.16358294663" Y="-28.306055871879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.770807742244" Y="-28.679248863133" />
                  <Point X="0.710646552314" Y="-28.69536900539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.807319611892" Y="-29.102106813227" />
                  <Point X="-1.182048301407" Y="-29.202515062963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347559073788" Y="-28.35511085432" />
                  <Point X="2.226180140535" Y="-28.387634241463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.783314709368" Y="-28.774248868531" />
                  <Point X="0.735234354671" Y="-28.787131960744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.83571113213" Y="-29.208065535285" />
                  <Point X="-1.163474898747" Y="-29.295889571859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.396734771963" Y="-28.440285502845" />
                  <Point X="2.288777334439" Y="-28.469212611047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795821676492" Y="-28.869248873929" />
                  <Point X="0.759822157028" Y="-28.878894916097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.864102652368" Y="-29.314024257344" />
                  <Point X="-1.144901496087" Y="-29.389264080754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.445910470138" Y="-28.525460151371" />
                  <Point X="2.351374528344" Y="-28.550790980631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808328643615" Y="-28.964248879328" />
                  <Point X="0.784409919406" Y="-28.970657882164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.892494172606" Y="-29.419982979403" />
                  <Point X="-1.126328376557" Y="-29.482638665514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.495086168314" Y="-28.610634799897" />
                  <Point X="2.413971722249" Y="-28.632369350214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.820835610739" Y="-29.059248884726" />
                  <Point X="0.808997631269" Y="-29.062420861765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.920885692844" Y="-29.525941701461" />
                  <Point X="-1.12127553024" Y="-29.579635996563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544261866489" Y="-28.695809448422" />
                  <Point X="2.476568916154" Y="-28.713947719798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.949277329544" Y="-29.631900454726" />
                  <Point X="-1.134697192089" Y="-29.681583557155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.593437564664" Y="-28.780984096948" />
                  <Point X="2.539166110058" Y="-28.795526089382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.977668985155" Y="-29.737859213058" />
                  <Point X="-1.043572716728" Y="-29.755518064711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.642613172028" Y="-28.866158769806" />
                  <Point X="2.601763303963" Y="-28.877104458965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69178870541" Y="-28.951333462488" />
                  <Point X="2.664360497868" Y="-28.958682828549" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.590021179199" Y="-28.97929296875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.415892150879" Y="-28.45176953125" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.274335754395" Y="-28.2663984375" />
                  <Point X="0.199398239136" Y="-28.243140625" />
                  <Point X="0.020976625443" Y="-28.187765625" />
                  <Point X="-0.008664604187" Y="-28.187765625" />
                  <Point X="-0.083602111816" Y="-28.2110234375" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.336705688477" Y="-28.35541796875" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.74132623291" Y="-29.589919921875" />
                  <Point X="-0.84774420166" Y="-29.987078125" />
                  <Point X="-0.890638427734" Y="-29.978751953125" />
                  <Point X="-1.100231079102" Y="-29.938068359375" />
                  <Point X="-1.186247070312" Y="-29.9159375" />
                  <Point X="-1.351589599609" Y="-29.873396484375" />
                  <Point X="-1.323412719727" Y="-29.659373046875" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.32758581543" Y="-29.444755859375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282958984" Y="-29.20262890625" />
                  <Point X="-1.455275756836" Y="-29.142123046875" />
                  <Point X="-1.619543701172" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.740809814453" Y="-28.97976171875" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.066178466797" Y="-29.0247734375" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.418110839844" Y="-29.3636953125" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.548591552734" Y="-29.35784765625" />
                  <Point X="-2.855837890625" Y="-29.167609375" />
                  <Point X="-2.974921142578" Y="-29.07591796875" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.726219482422" Y="-28.0104921875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.597591796875" />
                  <Point X="-2.513980224609" Y="-27.568763671875" />
                  <Point X="-2.531328857422" Y="-27.551416015625" />
                  <Point X="-2.560157714844" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.499709960938" Y="-28.067720703125" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.918965332031" Y="-28.1660390625" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.247079101562" Y="-27.70396875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.549040527344" Y="-26.71876171875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535400391" Y="-26.411083984375" />
                  <Point X="-3.143543212891" Y="-26.387216796875" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.136651855469" Y="-26.350052734375" />
                  <Point X="-3.148656738281" Y="-26.3210703125" />
                  <Point X="-3.168991699219" Y="-26.30603125" />
                  <Point X="-3.187641357422" Y="-26.2950546875" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.365826660156" Y="-26.439484375" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.832456542969" Y="-26.38286328125" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.949980957031" Y="-25.853259765625" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.996325683594" Y="-25.24623828125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.5336875" Y="-25.115728515625" />
                  <Point X="-3.514143310547" Y="-25.1021640625" />
                  <Point X="-3.502324462891" Y="-25.090646484375" />
                  <Point X="-3.492162597656" Y="-25.067091796875" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.488383789062" Y="-25.00764453125" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.502324462891" Y="-24.9719140625" />
                  <Point X="-3.522352050781" Y="-24.95469921875" />
                  <Point X="-3.541896240234" Y="-24.941134765625" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.602368164062" Y="-24.65393359375" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.978830566406" Y="-24.41706640625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.872174804688" Y="-23.835783203125" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.067853271484" Y="-23.564603515625" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731705078125" Y="-23.6041328125" />
                  <Point X="-3.713536865234" Y="-23.598404296875" />
                  <Point X="-3.670279052734" Y="-23.584765625" />
                  <Point X="-3.651534667969" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.631830078125" Y="-23.538615234375" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.625112548828" Y="-23.43758984375" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.259328125" Y="-22.920873046875" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.397758789063" Y="-22.620306640625" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.039566894531" Y="-22.05817578125" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.352367431641" Y="-21.961505859375" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138515380859" Y="-22.07956640625" />
                  <Point X="-3.113211914062" Y="-22.081779296875" />
                  <Point X="-3.052966064453" Y="-22.08705078125" />
                  <Point X="-3.031507324219" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.995292236328" Y="-22.054634765625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.940288085938" Y="-21.94685546875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.217661865234" Y="-21.4059453125" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.166951171875" Y="-21.14313671875" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.563175048828" Y="-20.720275390625" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.026974365234" Y="-20.63534375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246826172" Y="-20.726337890625" />
                  <Point X="-1.923084228516" Y="-20.741" />
                  <Point X="-1.856030761719" Y="-20.77590625" />
                  <Point X="-1.835124023438" Y="-20.781509765625" />
                  <Point X="-1.81380859375" Y="-20.77775" />
                  <Point X="-1.784475341797" Y="-20.765599609375" />
                  <Point X="-1.714634643555" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.676536010742" Y="-20.67523046875" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.682112792969" Y="-20.352220703125" />
                  <Point X="-1.689137695313" Y="-20.298861328125" />
                  <Point X="-1.504139770508" Y="-20.246994140625" />
                  <Point X="-0.968083007812" Y="-20.096703125" />
                  <Point X="-0.738120361328" Y="-20.0697890625" />
                  <Point X="-0.224199996948" Y="-20.009640625" />
                  <Point X="-0.099297317505" Y="-20.47578515625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.0242821064" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594024658" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.190539749146" Y="-20.1812109375" />
                  <Point X="0.236648376465" Y="-20.009130859375" />
                  <Point X="0.392159423828" Y="-20.02541796875" />
                  <Point X="0.860210266113" Y="-20.074435546875" />
                  <Point X="1.050470581055" Y="-20.12037109375" />
                  <Point X="1.508455932617" Y="-20.230943359375" />
                  <Point X="1.632228027344" Y="-20.2758359375" />
                  <Point X="1.931043334961" Y="-20.38421875" />
                  <Point X="2.050785644531" Y="-20.440216796875" />
                  <Point X="2.338685302734" Y="-20.574859375" />
                  <Point X="2.454389160156" Y="-20.642267578125" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.841630615234" Y="-20.881900390625" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.484701904297" Y="-22.0549921875" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.218501708984" Y="-22.532232421875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.204520751953" Y="-22.628208984375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682617188" Y="-22.6991875" />
                  <Point X="2.231388427734" Y="-22.7179140625" />
                  <Point X="2.261640380859" Y="-22.76249609375" />
                  <Point X="2.274939697266" Y="-22.775796875" />
                  <Point X="2.293664794922" Y="-22.78850390625" />
                  <Point X="2.338248291016" Y="-22.818755859375" />
                  <Point X="2.3603359375" Y="-22.827021484375" />
                  <Point X="2.380870117188" Y="-22.82949609375" />
                  <Point X="2.429760742188" Y="-22.835392578125" />
                  <Point X="2.4486640625" Y="-22.8340546875" />
                  <Point X="2.472410888672" Y="-22.827703125" />
                  <Point X="2.528950439453" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.592006591797" Y="-22.200802734375" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.037611816406" Y="-22.0288359375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.263417480469" Y="-22.35863671875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.624147949219" Y="-23.149455078125" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.27937109375" Y="-23.41616796875" />
                  <Point X="3.262280517578" Y="-23.438462890625" />
                  <Point X="3.221588867188" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.206753173828" Y="-23.531263671875" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.196005615234" Y="-23.63436328125" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646728516" Y="-23.712046875" />
                  <Point X="3.229860839844" Y="-23.73365234375" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280947753906" Y="-23.8011796875" />
                  <Point X="3.301546142578" Y="-23.812775390625" />
                  <Point X="3.35058984375" Y="-23.8403828125" />
                  <Point X="3.368565673828" Y="-23.846380859375" />
                  <Point X="3.396416015625" Y="-23.850060546875" />
                  <Point X="3.462726318359" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.474157226562" Y="-23.727392578125" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.868330566406" Y="-23.757552734375" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.958356445312" Y="-24.1717265625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.125694335938" Y="-24.659138671875" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.701725097656" Y="-24.78299609375" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.622264892578" Y="-24.833072265625" />
                  <Point X="3.60584765625" Y="-24.8539921875" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.551512695313" Y="-24.95383984375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538482910156" Y="-25.040685546875" />
                  <Point X="3.543955566406" Y="-25.069259765625" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.583176269531" Y="-25.1796796875" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.636576171875" Y="-25.241908203125" />
                  <Point X="3.663938476562" Y="-25.25772265625" />
                  <Point X="3.729086181641" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.656703613281" Y="-25.545705078125" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.987995605469" Y="-25.703982421875" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.923874511719" Y="-26.07401171875" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.857925048828" Y="-26.156337890625" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836181641" Y="-26.098341796875" />
                  <Point X="3.341133789062" Y="-26.110013671875" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.152985595703" Y="-26.193736328125" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.059705566406" Y="-26.364626953125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.086080322266" Y="-26.562849609375" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.018083496094" Y="-27.33748046875" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.315658691406" Y="-27.621673828125" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.153345214844" Y="-27.874302734375" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.150626953125" Y="-27.4885234375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.673426269531" Y="-27.241771484375" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.43598046875" Y="-27.247189453125" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.260655273438" Y="-27.38778125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.200705810547" Y="-27.6102890625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.780059570312" Y="-28.72422265625" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.967634521484" Y="-29.0956875" />
                  <Point X="2.835294189453" Y="-29.19021484375" />
                  <Point X="2.778517089844" Y="-29.226966796875" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.986256713867" Y="-28.387068359375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670548950195" Y="-27.980466796875" />
                  <Point X="1.607512207031" Y="-27.939939453125" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425805175781" Y="-27.83571875" />
                  <Point X="1.356863769531" Y="-27.8420625" />
                  <Point X="1.192718383789" Y="-27.857166015625" />
                  <Point X="1.165332641602" Y="-27.868509765625" />
                  <Point X="1.112097900391" Y="-27.9127734375" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.952539855957" Y="-28.11921875" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.068652709961" Y="-29.485958984375" />
                  <Point X="1.127642578125" Y="-29.934029296875" />
                  <Point X="1.119547119141" Y="-29.9358046875" />
                  <Point X="0.994366821289" Y="-29.9632421875" />
                  <Point X="0.94188885498" Y="-29.97277734375" />
                  <Point X="0.860200439453" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#146" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.057157025171" Y="4.568437406406" Z="0.75" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.75" />
                  <Point X="-0.750088547893" Y="5.011152576731" Z="0.75" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.75" />
                  <Point X="-1.523793560681" Y="4.832431607231" Z="0.75" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.75" />
                  <Point X="-1.73784097788" Y="4.672535231898" Z="0.75" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.75" />
                  <Point X="-1.730377730839" Y="4.37108455871" Z="0.75" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.75" />
                  <Point X="-1.809765829264" Y="4.311874898676" Z="0.75" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.75" />
                  <Point X="-1.906152589708" Y="4.334630690897" Z="0.75" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.75" />
                  <Point X="-1.993462792253" Y="4.426373997216" Z="0.75" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.75" />
                  <Point X="-2.593613933646" Y="4.354712856332" Z="0.75" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.75" />
                  <Point X="-3.203529206774" Y="3.927824396627" Z="0.75" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.75" />
                  <Point X="-3.267119100831" Y="3.600335902768" Z="0.75" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.75" />
                  <Point X="-2.996253796399" Y="3.08006696677" Z="0.75" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.75" />
                  <Point X="-3.036803013417" Y="3.01200055447" Z="0.75" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.75" />
                  <Point X="-3.115009498836" Y="2.999310902774" Z="0.75" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.75" />
                  <Point X="-3.333523445529" Y="3.113074847915" Z="0.75" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.75" />
                  <Point X="-4.085186200069" Y="3.003807424264" Z="0.75" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.75" />
                  <Point X="-4.447096751145" Y="2.436178715395" Z="0.75" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.75" />
                  <Point X="-4.295921950127" Y="2.070738912664" Z="0.75" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.75" />
                  <Point X="-3.675618757288" Y="1.57060196404" Z="0.75" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.75" />
                  <Point X="-3.684179786388" Y="1.511800021463" Z="0.75" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.75" />
                  <Point X="-3.734727710654" Y="1.480560991" Z="0.75" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.75" />
                  <Point X="-4.067482941519" Y="1.516248694585" Z="0.75" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.75" />
                  <Point X="-4.926590731134" Y="1.20857446535" Z="0.75" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.75" />
                  <Point X="-5.034447788596" Y="0.621532624939" Z="0.75" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.75" />
                  <Point X="-4.621465187731" Y="0.329050340468" Z="0.75" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.75" />
                  <Point X="-3.557016482916" Y="0.035504204533" Z="0.75" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.75" />
                  <Point X="-3.542292996558" Y="0.008816168685" Z="0.75" />
                  <Point X="-3.539556741714" Y="0" Z="0.75" />
                  <Point X="-3.546071604577" Y="-0.020990782376" Z="0.75" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.75" />
                  <Point X="-3.568352112208" Y="-0.043371778601" Z="0.75" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.75" />
                  <Point X="-4.015422601042" Y="-0.166661716288" Z="0.75" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.75" />
                  <Point X="-5.005633232979" Y="-0.829056445876" Z="0.75" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.75" />
                  <Point X="-4.887057345651" Y="-1.363958390155" Z="0.75" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.75" />
                  <Point X="-4.365456245006" Y="-1.45777618905" Z="0.75" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.75" />
                  <Point X="-3.200509207246" Y="-1.317839698402" Z="0.75" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.75" />
                  <Point X="-3.198102000153" Y="-1.343398625112" Z="0.75" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.75" />
                  <Point X="-3.585634370471" Y="-1.647812617964" Z="0.75" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.75" />
                  <Point X="-4.296178538336" Y="-2.698297064641" Z="0.75" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.75" />
                  <Point X="-3.964907323479" Y="-3.16504085023" Z="0.75" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.75" />
                  <Point X="-3.480866013566" Y="-3.079740316224" Z="0.75" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.75" />
                  <Point X="-2.560622241776" Y="-2.567708227356" Z="0.75" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.75" />
                  <Point X="-2.775676696944" Y="-2.954212368926" Z="0.75" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.75" />
                  <Point X="-3.011580985976" Y="-4.084254973649" Z="0.75" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.75" />
                  <Point X="-2.581069056139" Y="-4.369078949383" Z="0.75" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.75" />
                  <Point X="-2.384599160376" Y="-4.362852880104" Z="0.75" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.75" />
                  <Point X="-2.044556126652" Y="-4.035066559667" Z="0.75" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.75" />
                  <Point X="-1.750235763459" Y="-3.998374184343" Z="0.75" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.75" />
                  <Point X="-1.494398851762" Y="-4.148435298168" Z="0.75" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.75" />
                  <Point X="-1.382781075999" Y="-4.423230165082" Z="0.75" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.75" />
                  <Point X="-1.37914098686" Y="-4.621566259706" Z="0.75" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.75" />
                  <Point X="-1.204861857182" Y="-4.933080869803" Z="0.75" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.75" />
                  <Point X="-0.906322010932" Y="-4.996554910603" Z="0.75" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.75" />
                  <Point X="-0.699185907906" Y="-4.571581432015" Z="0.75" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.75" />
                  <Point X="-0.301785737454" Y="-3.352646258629" Z="0.75" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.75" />
                  <Point X="-0.074937432443" Y="-3.22749718202" Z="0.75" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.75" />
                  <Point X="0.178421646918" Y="-3.259614863269" Z="0.75" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.75" />
                  <Point X="0.368660121978" Y="-3.448999497139" Z="0.75" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.75" />
                  <Point X="0.535568952495" Y="-3.960954597801" Z="0.75" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.75" />
                  <Point X="0.944669373056" Y="-4.990691451087" Z="0.75" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.75" />
                  <Point X="1.124096479223" Y="-4.953363328145" Z="0.75" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.75" />
                  <Point X="1.112068949563" Y="-4.448152677499" Z="0.75" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.75" />
                  <Point X="0.995243079468" Y="-3.09855640717" Z="0.75" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.75" />
                  <Point X="1.137908954812" Y="-2.91993842732" Z="0.75" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.75" />
                  <Point X="1.355289097436" Y="-2.86057063576" Z="0.75" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.75" />
                  <Point X="1.574317184604" Y="-2.950718488005" Z="0.75" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.75" />
                  <Point X="1.940433011949" Y="-3.386225581639" Z="0.75" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.75" />
                  <Point X="2.799530223853" Y="-4.237660165037" Z="0.75" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.75" />
                  <Point X="2.990540318512" Y="-4.105094675021" Z="0.75" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.75" />
                  <Point X="2.817204970171" Y="-3.667942669256" Z="0.75" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.75" />
                  <Point X="2.243753987322" Y="-2.570122460065" Z="0.75" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.75" />
                  <Point X="2.298745589703" Y="-2.379787235635" Z="0.75" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.75" />
                  <Point X="2.453111142738" Y="-2.260155720264" Z="0.75" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.75" />
                  <Point X="2.658384343448" Y="-2.259694022169" Z="0.75" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.75" />
                  <Point X="3.11947092785" Y="-2.500544530103" Z="0.75" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.75" />
                  <Point X="4.188077680673" Y="-2.871799536062" Z="0.75" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.75" />
                  <Point X="4.352036392246" Y="-2.616678512558" Z="0.75" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.75" />
                  <Point X="4.042365429188" Y="-2.266531470456" Z="0.75" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.75" />
                  <Point X="3.121982132346" Y="-1.504529422708" Z="0.75" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.75" />
                  <Point X="3.103339217293" Y="-1.337929232723" Z="0.75" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.75" />
                  <Point X="3.185275910766" Y="-1.194423026522" Z="0.75" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.75" />
                  <Point X="3.345597561955" Y="-1.127592841103" Z="0.75" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.75" />
                  <Point X="3.845242759665" Y="-1.174629916552" Z="0.75" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.75" />
                  <Point X="4.966465567826" Y="-1.053857090953" Z="0.75" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.75" />
                  <Point X="5.031281102016" Y="-0.680154485062" Z="0.75" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.75" />
                  <Point X="4.663488382447" Y="-0.466127672458" Z="0.75" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.75" />
                  <Point X="3.682804910538" Y="-0.183154047019" Z="0.75" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.75" />
                  <Point X="3.616354041217" Y="-0.117530051707" Z="0.75" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.75" />
                  <Point X="3.586907165884" Y="-0.028575037378" Z="0.75" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.75" />
                  <Point X="3.59446428454" Y="0.068035493849" Z="0.75" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.75" />
                  <Point X="3.639025397183" Y="0.146418686868" Z="0.75" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.75" />
                  <Point X="3.720590503815" Y="0.204994797223" Z="0.75" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.75" />
                  <Point X="4.132479434859" Y="0.323844260311" Z="0.75" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.75" />
                  <Point X="5.001605156735" Y="0.867244898828" Z="0.75" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.75" />
                  <Point X="4.910755642967" Y="1.285554674672" Z="0.75" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.75" />
                  <Point X="4.461475037049" Y="1.35345987152" Z="0.75" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.75" />
                  <Point X="3.396811084201" Y="1.230787801038" Z="0.75" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.75" />
                  <Point X="3.319916975807" Y="1.26207590252" Z="0.75" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.75" />
                  <Point X="3.265475248129" Y="1.325111365312" Z="0.75" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.75" />
                  <Point X="3.238818081276" Y="1.407021112334" Z="0.75" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.75" />
                  <Point X="3.248749726018" Y="1.486549482523" Z="0.75" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.75" />
                  <Point X="3.295807602382" Y="1.562399122881" Z="0.75" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.75" />
                  <Point X="3.648430051777" Y="1.842157768558" Z="0.75" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.75" />
                  <Point X="4.300038905153" Y="2.698531471609" Z="0.75" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.75" />
                  <Point X="4.072041372313" Y="3.031647922369" Z="0.75" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.75" />
                  <Point X="3.560850620557" Y="2.873778104732" Z="0.75" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.75" />
                  <Point X="2.453338373359" Y="2.251879356463" Z="0.75" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.75" />
                  <Point X="2.380700969631" Y="2.251424485995" Z="0.75" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.75" />
                  <Point X="2.31558324537" Y="2.284152279049" Z="0.75" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.75" />
                  <Point X="2.266606321186" Y="2.341441615012" Z="0.75" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.75" />
                  <Point X="2.248005216763" Y="2.409057474201" Z="0.75" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.75" />
                  <Point X="2.260648586195" Y="2.486131202523" Z="0.75" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.75" />
                  <Point X="2.521847248128" Y="2.951288461398" Z="0.75" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.75" />
                  <Point X="2.864451767028" Y="4.190127697661" Z="0.75" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.75" />
                  <Point X="2.473403061537" Y="4.432215867395" Z="0.75" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.75" />
                  <Point X="2.065811362486" Y="4.636353955108" Z="0.75" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.75" />
                  <Point X="1.643120616304" Y="4.802448951991" Z="0.75" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.75" />
                  <Point X="1.056048821415" Y="4.959513481082" Z="0.75" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.75" />
                  <Point X="0.391211338109" Y="5.055590623635" Z="0.75" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.75" />
                  <Point X="0.136087536382" Y="4.863010099545" Z="0.75" />
                  <Point X="0" Y="4.355124473572" Z="0.75" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>