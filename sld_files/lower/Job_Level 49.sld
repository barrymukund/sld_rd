<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#213" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3486" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563302124023" Y="-28.5125234375" />
                  <Point X="0.557720214844" Y="-28.497138671875" />
                  <Point X="0.54236340332" Y="-28.467376953125" />
                  <Point X="0.387578186035" Y="-28.244361328125" />
                  <Point X="0.378635742188" Y="-28.2314765625" />
                  <Point X="0.356752593994" Y="-28.2090234375" />
                  <Point X="0.330497558594" Y="-28.189779296875" />
                  <Point X="0.302495391846" Y="-28.175669921875" />
                  <Point X="0.062974403381" Y="-28.10133203125" />
                  <Point X="0.049136428833" Y="-28.097037109375" />
                  <Point X="0.020977186203" Y="-28.092767578125" />
                  <Point X="-0.008665203094" Y="-28.092767578125" />
                  <Point X="-0.036824447632" Y="-28.097037109375" />
                  <Point X="-0.276345458984" Y="-28.171375" />
                  <Point X="-0.290183410645" Y="-28.175669921875" />
                  <Point X="-0.318180847168" Y="-28.189775390625" />
                  <Point X="-0.344434082031" Y="-28.209015625" />
                  <Point X="-0.366321929932" Y="-28.231474609375" />
                  <Point X="-0.521107177734" Y="-28.4544921875" />
                  <Point X="-0.525663452148" Y="-28.461732421875" />
                  <Point X="-0.541827941895" Y="-28.490185546875" />
                  <Point X="-0.55099017334" Y="-28.5125234375" />
                  <Point X="-0.57286920166" Y="-28.594177734375" />
                  <Point X="-0.916584533691" Y="-29.87694140625" />
                  <Point X="-1.067245605469" Y="-29.847697265625" />
                  <Point X="-1.079348510742" Y="-29.84534765625" />
                  <Point X="-1.24641784668" Y="-29.80236328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.273730102539" Y="-29.2285546875" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.2879375" Y="-29.18296875" />
                  <Point X="-1.304008056641" Y="-29.1551328125" />
                  <Point X="-1.323643310547" Y="-29.131205078125" />
                  <Point X="-1.5441640625" Y="-28.937814453125" />
                  <Point X="-1.556904174805" Y="-28.926640625" />
                  <Point X="-1.583186767578" Y="-28.910298828125" />
                  <Point X="-1.612887817383" Y="-28.89799609375" />
                  <Point X="-1.643029785156" Y="-28.890966796875" />
                  <Point X="-1.935709838867" Y="-28.871783203125" />
                  <Point X="-1.952618896484" Y="-28.87067578125" />
                  <Point X="-1.983419433594" Y="-28.873708984375" />
                  <Point X="-2.01446484375" Y="-28.88202734375" />
                  <Point X="-2.042656860352" Y="-28.89480078125" />
                  <Point X="-2.286533447266" Y="-29.05775390625" />
                  <Point X="-2.29269140625" Y="-29.062236328125" />
                  <Point X="-2.318671386719" Y="-29.082787109375" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.347385986328" Y="-29.115470703125" />
                  <Point X="-2.480148925781" Y="-29.2884921875" />
                  <Point X="-2.783990966797" Y="-29.100359375" />
                  <Point X="-2.801713134766" Y="-29.08938671875" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406587890625" Y="-27.616125" />
                  <Point X="-2.405575683594" Y="-27.585189453125" />
                  <Point X="-2.414561279297" Y="-27.5555703125" />
                  <Point X="-2.428779052734" Y="-27.5267421875" />
                  <Point X="-2.446806884766" Y="-27.501587890625" />
                  <Point X="-2.463208007813" Y="-27.4851875" />
                  <Point X="-2.464159179688" Y="-27.484236328125" />
                  <Point X="-2.489323242188" Y="-27.466205078125" />
                  <Point X="-2.518150878906" Y="-27.4519921875" />
                  <Point X="-2.547766357422" Y="-27.44301171875" />
                  <Point X="-2.578697265625" Y="-27.444025390625" />
                  <Point X="-2.610220458984" Y="-27.450296875" />
                  <Point X="-2.639184082031" Y="-27.46119921875" />
                  <Point X="-2.709608642578" Y="-27.501859375" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.068860839844" Y="-27.81225390625" />
                  <Point X="-4.082859619141" Y="-27.793861328125" />
                  <Point X="-4.306142089844" Y="-27.419451171875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046572753906" Y="-26.3917109375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.359658203125" />
                  <Point X="-3.045556640625" Y="-26.327986328125" />
                  <Point X="-3.052558105469" Y="-26.298240234375" />
                  <Point X="-3.068640869141" Y="-26.272255859375" />
                  <Point X="-3.089474365234" Y="-26.248298828125" />
                  <Point X="-3.112975341797" Y="-26.228765625" />
                  <Point X="-3.138011474609" Y="-26.21403125" />
                  <Point X="-3.139473876953" Y="-26.213169921875" />
                  <Point X="-3.168708984375" Y="-26.2019609375" />
                  <Point X="-3.200601074219" Y="-26.1954765625" />
                  <Point X="-3.231927978516" Y="-26.194384765625" />
                  <Point X="-3.320832275391" Y="-26.206087890625" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.8286015625" Y="-26.01409375" />
                  <Point X="-4.834076660156" Y="-25.99266015625" />
                  <Point X="-4.892424316406" Y="-25.58469921875" />
                  <Point X="-3.532874023438" Y="-25.220408203125" />
                  <Point X="-3.517493896484" Y="-25.214828125" />
                  <Point X="-3.487725830078" Y="-25.19946875" />
                  <Point X="-3.461488769531" Y="-25.1812578125" />
                  <Point X="-3.459981689453" Y="-25.1802109375" />
                  <Point X="-3.437561767578" Y="-25.15836328125" />
                  <Point X="-3.418293701172" Y="-25.132091796875" />
                  <Point X="-3.404168212891" Y="-25.10406640625" />
                  <Point X="-3.395435302734" Y="-25.075927734375" />
                  <Point X="-3.394929443359" Y="-25.07430078125" />
                  <Point X="-3.390649414062" Y="-25.046111328125" />
                  <Point X="-3.390647949219" Y="-25.01646484375" />
                  <Point X="-3.394917236328" Y="-24.988302734375" />
                  <Point X="-3.403653320312" Y="-24.960154296875" />
                  <Point X="-3.404157958984" Y="-24.958525390625" />
                  <Point X="-3.418266845703" Y="-24.93051171875" />
                  <Point X="-3.437515380859" Y="-24.904244140625" />
                  <Point X="-3.459977294922" Y="-24.8823515625" />
                  <Point X="-3.486202148438" Y="-24.864150390625" />
                  <Point X="-3.498952392578" Y="-24.85669921875" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-3.613916503906" Y="-24.820435546875" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.828015625" Y="-24.046865234375" />
                  <Point X="-4.824487792969" Y="-24.0230234375" />
                  <Point X="-4.703551757812" Y="-23.576732421875" />
                  <Point X="-3.765670166016" Y="-23.70020703125" />
                  <Point X="-3.744984863281" Y="-23.700658203125" />
                  <Point X="-3.723424072266" Y="-23.698771484375" />
                  <Point X="-3.703140136719" Y="-23.694736328125" />
                  <Point X="-3.645069091797" Y="-23.676427734375" />
                  <Point X="-3.641718017578" Y="-23.67537109375" />
                  <Point X="-3.622756591797" Y="-23.667029296875" />
                  <Point X="-3.604018310547" Y="-23.65620703125" />
                  <Point X="-3.587342041016" Y="-23.643974609375" />
                  <Point X="-3.573707763672" Y="-23.62842578125" />
                  <Point X="-3.561297119141" Y="-23.61069921875" />
                  <Point X="-3.551351318359" Y="-23.5925703125" />
                  <Point X="-3.528050048828" Y="-23.53631640625" />
                  <Point X="-3.526703857422" Y="-23.53306640625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.532049804688" Y="-23.410623046875" />
                  <Point X="-3.560165283203" Y="-23.35661328125" />
                  <Point X="-3.561800292969" Y="-23.35347265625" />
                  <Point X="-3.5732890625" Y="-23.336283203125" />
                  <Point X="-3.587198486328" Y="-23.319708984375" />
                  <Point X="-3.602135253906" Y="-23.305412109375" />
                  <Point X="-3.648620361328" Y="-23.2697421875" />
                  <Point X="-4.351860351563" Y="-22.730126953125" />
                  <Point X="-4.094859863281" Y="-22.289822265625" />
                  <Point X="-4.081152099609" Y="-22.266337890625" />
                  <Point X="-3.750504638672" Y="-21.841337890625" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187729492188" Y="-22.163658203125" />
                  <Point X="-3.167087402344" Y="-22.17016796875" />
                  <Point X="-3.146795166016" Y="-22.174205078125" />
                  <Point X="-3.065918212891" Y="-22.18128125" />
                  <Point X="-3.061217041016" Y="-22.18169140625" />
                  <Point X="-3.040552734375" Y="-22.181236328125" />
                  <Point X="-3.019097900391" Y="-22.17841015625" />
                  <Point X="-2.999009765625" Y="-22.173494140625" />
                  <Point X="-2.980461914062" Y="-22.16434765625" />
                  <Point X="-2.9622109375" Y="-22.152720703125" />
                  <Point X="-2.946078857422" Y="-22.1397734375" />
                  <Point X="-2.888671875" Y="-22.0823671875" />
                  <Point X="-2.885355224609" Y="-22.07905078125" />
                  <Point X="-2.872409667969" Y="-22.062921875" />
                  <Point X="-2.860779785156" Y="-22.04466796875" />
                  <Point X="-2.851630371094" Y="-22.0261171875" />
                  <Point X="-2.846713378906" Y="-22.006025390625" />
                  <Point X="-2.843887451172" Y="-21.98456640625" />
                  <Point X="-2.843435791016" Y="-21.9638828125" />
                  <Point X="-2.85051171875" Y="-21.883005859375" />
                  <Point X="-2.850920410156" Y="-21.87833203125" />
                  <Point X="-2.854954833984" Y="-21.858046875" />
                  <Point X="-2.861463378906" Y="-21.83740234375" />
                  <Point X="-2.869795410156" Y="-21.818466796875" />
                  <Point X="-2.890394287109" Y="-21.7827890625" />
                  <Point X="-3.183332763672" Y="-21.27540625" />
                  <Point X="-2.724491699219" Y="-20.9236171875" />
                  <Point X="-2.700618896484" Y="-20.9053125" />
                  <Point X="-2.167035400391" Y="-20.6088671875" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028893554688" Y="-20.78519921875" />
                  <Point X="-2.01231237793" Y="-20.79911328125" />
                  <Point X="-1.99511328125" Y="-20.81060546875" />
                  <Point X="-1.90509765625" Y="-20.857466796875" />
                  <Point X="-1.899897216797" Y="-20.860173828125" />
                  <Point X="-1.880614746094" Y="-20.867671875" />
                  <Point X="-1.85970690918" Y="-20.8732734375" />
                  <Point X="-1.83926184082" Y="-20.876419921875" />
                  <Point X="-1.818620361328" Y="-20.87506640625" />
                  <Point X="-1.797304199219" Y="-20.871306640625" />
                  <Point X="-1.777450317383" Y="-20.865517578125" />
                  <Point X="-1.683693237305" Y="-20.826681640625" />
                  <Point X="-1.678276489258" Y="-20.8244375" />
                  <Point X="-1.660133300781" Y="-20.814482421875" />
                  <Point X="-1.642405517578" Y="-20.80206640625" />
                  <Point X="-1.626859130859" Y="-20.7884296875" />
                  <Point X="-1.614631591797" Y="-20.77175390625" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.564963867188" Y="-20.63729296875" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165405273" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773046875" Y="-20.569173828125" />
                  <Point X="-1.560072265625" Y="-20.55138671875" />
                  <Point X="-1.584201904297" Y="-20.368103515625" />
                  <Point X="-0.980532531738" Y="-20.19885546875" />
                  <Point X="-0.949624633789" Y="-20.19019140625" />
                  <Point X="-0.294711242676" Y="-20.113541015625" />
                  <Point X="-0.133903213501" Y="-20.713685546875" />
                  <Point X="-0.121129745483" Y="-20.741876953125" />
                  <Point X="-0.103271606445" Y="-20.768603515625" />
                  <Point X="-0.082113998413" Y="-20.791193359375" />
                  <Point X="-0.054817985535" Y="-20.805783203125" />
                  <Point X="-0.024379911423" Y="-20.816115234375" />
                  <Point X="0.00615600872" Y="-20.82115625" />
                  <Point X="0.036691894531" Y="-20.816115234375" />
                  <Point X="0.067129966736" Y="-20.805783203125" />
                  <Point X="0.09442591095" Y="-20.791193359375" />
                  <Point X="0.115583435059" Y="-20.768603515625" />
                  <Point X="0.133441726685" Y="-20.741876953125" />
                  <Point X="0.14621534729" Y="-20.713685546875" />
                  <Point X="0.156769943237" Y="-20.674294921875" />
                  <Point X="0.307419647217" Y="-20.1120625" />
                  <Point X="0.817055603027" Y="-20.165435546875" />
                  <Point X="0.844030212402" Y="-20.168259765625" />
                  <Point X="1.45313293457" Y="-20.31531640625" />
                  <Point X="1.481038452148" Y="-20.3220546875" />
                  <Point X="1.877408569336" Y="-20.4658203125" />
                  <Point X="1.894640625" Y="-20.4720703125" />
                  <Point X="2.278003417969" Y="-20.65135546875" />
                  <Point X="2.294561279297" Y="-20.659099609375" />
                  <Point X="2.6649375" Y="-20.874880859375" />
                  <Point X="2.680978759766" Y="-20.8842265625" />
                  <Point X="2.943259765625" Y="-21.07074609375" />
                  <Point X="2.147580566406" Y="-22.44890234375" />
                  <Point X="2.142076416016" Y="-22.460068359375" />
                  <Point X="2.133076904297" Y="-22.483943359375" />
                  <Point X="2.112779785156" Y="-22.559845703125" />
                  <Point X="2.110102539062" Y="-22.574203125" />
                  <Point X="2.107591796875" Y="-22.597490234375" />
                  <Point X="2.107727783203" Y="-22.619046875" />
                  <Point X="2.115642089844" Y="-22.6846796875" />
                  <Point X="2.116099121094" Y="-22.688470703125" />
                  <Point X="2.121441162109" Y="-22.710392578125" />
                  <Point X="2.129707275391" Y="-22.732482421875" />
                  <Point X="2.1400703125" Y="-22.75252734375" />
                  <Point X="2.180681640625" Y="-22.81237890625" />
                  <Point X="2.18985546875" Y="-22.823873046875" />
                  <Point X="2.205502197266" Y="-22.840630859375" />
                  <Point X="2.221598876953" Y="-22.854408203125" />
                  <Point X="2.281438232422" Y="-22.89501171875" />
                  <Point X="2.28487109375" Y="-22.897341796875" />
                  <Point X="2.304933105469" Y="-22.90771875" />
                  <Point X="2.327035644531" Y="-22.9159921875" />
                  <Point X="2.348966552734" Y="-22.921337890625" />
                  <Point X="2.414599609375" Y="-22.929251953125" />
                  <Point X="2.429657470703" Y="-22.92986328125" />
                  <Point X="2.452349365234" Y="-22.928982421875" />
                  <Point X="2.473206054688" Y="-22.925830078125" />
                  <Point X="2.549107421875" Y="-22.905533203125" />
                  <Point X="2.557923339844" Y="-22.902708984375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="2.670044921875" Y="-22.84279296875" />
                  <Point X="3.967325439453" Y="-22.09380859375" />
                  <Point X="4.113756347656" Y="-22.297314453125" />
                  <Point X="4.1232734375" Y="-22.310541015625" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.230785400391" Y="-23.331548828125" />
                  <Point X="3.221421630859" Y="-23.33976171875" />
                  <Point X="3.203974365234" Y="-23.358373046875" />
                  <Point X="3.149348144531" Y="-23.42963671875" />
                  <Point X="3.141568115234" Y="-23.44153515625" />
                  <Point X="3.129942626953" Y="-23.462603515625" />
                  <Point X="3.121630126953" Y="-23.4829140625" />
                  <Point X="3.101281738281" Y="-23.55567578125" />
                  <Point X="3.100106201172" Y="-23.55987890625" />
                  <Point X="3.096652832031" Y="-23.58217578125" />
                  <Point X="3.095836181641" Y="-23.60574609375" />
                  <Point X="3.097739257812" Y="-23.628232421875" />
                  <Point X="3.114443115234" Y="-23.7091875" />
                  <Point X="3.118389160156" Y="-23.722962890625" />
                  <Point X="3.126551513672" Y="-23.745017578125" />
                  <Point X="3.136281982422" Y="-23.764259765625" />
                  <Point X="3.181714599609" Y="-23.833314453125" />
                  <Point X="3.184339599609" Y="-23.8373046875" />
                  <Point X="3.198890625" Y="-23.854546875" />
                  <Point X="3.216135742188" Y="-23.870638671875" />
                  <Point X="3.234348144531" Y="-23.883966796875" />
                  <Point X="3.300186523438" Y="-23.92102734375" />
                  <Point X="3.313531494141" Y="-23.927232421875" />
                  <Point X="3.335310302734" Y="-23.93537109375" />
                  <Point X="3.356119384766" Y="-23.9405625" />
                  <Point X="3.445137207031" Y="-23.952326171875" />
                  <Point X="3.454021484375" Y="-23.953078125" />
                  <Point X="3.488203125" Y="-23.953015625" />
                  <Point X="3.565633300781" Y="-23.942822265625" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.841849121094" Y="-24.05040625" />
                  <Point X="4.8459375" Y="-24.067197265625" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.71658203125" Y="-24.67041015625" />
                  <Point X="3.704790771484" Y="-24.6744140625" />
                  <Point X="3.681550537109" Y="-24.6849296875" />
                  <Point X="3.594093261719" Y="-24.73548046875" />
                  <Point X="3.582646484375" Y="-24.74326171875" />
                  <Point X="3.563275878906" Y="-24.75860546875" />
                  <Point X="3.547529052734" Y="-24.774423828125" />
                  <Point X="3.4950546875" Y="-24.8412890625" />
                  <Point X="3.492023193359" Y="-24.84515234375" />
                  <Point X="3.480298339844" Y="-24.864435546875" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.90739453125" />
                  <Point X="3.446189453125" Y="-24.998728515625" />
                  <Point X="3.444577392578" Y="-25.012619140625" />
                  <Point X="3.443567382812" Y="-25.0367109375" />
                  <Point X="3.445179443359" Y="-25.05855859375" />
                  <Point X="3.462670898438" Y="-25.149892578125" />
                  <Point X="3.463681640625" Y="-25.15516796875" />
                  <Point X="3.47052734375" Y="-25.17666796875" />
                  <Point X="3.480298583984" Y="-25.198126953125" />
                  <Point X="3.492023193359" Y="-25.217408203125" />
                  <Point X="3.544497558594" Y="-25.2842734375" />
                  <Point X="3.554207275391" Y="-25.2948828125" />
                  <Point X="3.571552978516" Y="-25.31116796875" />
                  <Point X="3.589036865234" Y="-25.324158203125" />
                  <Point X="3.676494140625" Y="-25.374708984375" />
                  <Point X="3.684132324219" Y="-25.378671875" />
                  <Point X="3.716581054688" Y="-25.39215234375" />
                  <Point X="3.787588134766" Y="-25.411177734375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.8573046875" Y="-25.93358984375" />
                  <Point X="4.855021484375" Y="-25.948734375" />
                  <Point X="4.801174804688" Y="-26.18469921875" />
                  <Point X="3.4243828125" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374658203125" Y="-26.005509765625" />
                  <Point X="3.203010498047" Y="-26.042818359375" />
                  <Point X="3.193099365234" Y="-26.04497265625" />
                  <Point X="3.163986083984" Y="-26.056591796875" />
                  <Point X="3.136153808594" Y="-26.073484375" />
                  <Point X="3.112396972656" Y="-26.0939609375" />
                  <Point X="3.008646728516" Y="-26.218740234375" />
                  <Point X="3.002652832031" Y="-26.22594921875" />
                  <Point X="2.987931640625" Y="-26.25033203125" />
                  <Point X="2.976588867188" Y="-26.277716796875" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.954887695312" Y="-26.4669609375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956348144531" Y="-26.507568359375" />
                  <Point X="2.964081298828" Y="-26.53919140625" />
                  <Point X="2.976451904297" Y="-26.568" />
                  <Point X="3.071444091797" Y="-26.71575390625" />
                  <Point X="3.077473632812" Y="-26.724099609375" />
                  <Point X="3.094580078125" Y="-26.74526171875" />
                  <Point X="3.110628662109" Y="-26.76091015625" />
                  <Point X="3.176523681641" Y="-26.81147265625" />
                  <Point X="4.213123046875" Y="-27.606884765625" />
                  <Point X="4.131241210938" Y="-27.739380859375" />
                  <Point X="4.124819824219" Y="-27.7497734375" />
                  <Point X="4.028980957031" Y="-27.8859453125" />
                  <Point X="2.800954101562" Y="-27.176943359375" />
                  <Point X="2.786134521484" Y="-27.170013671875" />
                  <Point X="2.754222167969" Y="-27.15982421875" />
                  <Point X="2.549934082031" Y="-27.1229296875" />
                  <Point X="2.538131835938" Y="-27.120798828125" />
                  <Point X="2.506774658203" Y="-27.120396484375" />
                  <Point X="2.474604736328" Y="-27.12535546875" />
                  <Point X="2.444833984375" Y="-27.135177734375" />
                  <Point X="2.275120849609" Y="-27.22449609375" />
                  <Point X="2.265315917969" Y="-27.22965625" />
                  <Point X="2.242386962891" Y="-27.246546875" />
                  <Point X="2.221426757812" Y="-27.267505859375" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.115213134766" Y="-27.46015234375" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.132569580078" Y="-27.767546875" />
                  <Point X="2.134762939453" Y="-27.776939453125" />
                  <Point X="2.142797363281" Y="-27.80485546875" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.194163574219" Y="-27.899421875" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.789486083984" Y="-29.106189453125" />
                  <Point X="2.781818847656" Y="-29.111666015625" />
                  <Point X="2.701765380859" Y="-29.163482421875" />
                  <Point X="1.758546630859" Y="-27.934255859375" />
                  <Point X="1.74750390625" Y="-27.9221796875" />
                  <Point X="1.721920898438" Y="-27.9005546875" />
                  <Point X="1.520437866211" Y="-27.77101953125" />
                  <Point X="1.508797363281" Y="-27.763537109375" />
                  <Point X="1.479979858398" Y="-27.7511640625" />
                  <Point X="1.448362182617" Y="-27.743435546875" />
                  <Point X="1.417099365234" Y="-27.741119140625" />
                  <Point X="1.196776123047" Y="-27.76139453125" />
                  <Point X="1.184045043945" Y="-27.762560546875" />
                  <Point X="1.156370117188" Y="-27.76939453125" />
                  <Point X="1.128978393555" Y="-27.780740234375" />
                  <Point X="1.104594848633" Y="-27.795462890625" />
                  <Point X="0.934441650391" Y="-27.93694140625" />
                  <Point X="0.92461126709" Y="-27.945115234375" />
                  <Point X="0.904141723633" Y="-27.96886328125" />
                  <Point X="0.88725" Y="-27.9966875" />
                  <Point X="0.875624572754" Y="-28.02580859375" />
                  <Point X="0.824749389648" Y="-28.259873046875" />
                  <Point X="0.823248657227" Y="-28.26881640625" />
                  <Point X="0.819596130371" Y="-28.299484375" />
                  <Point X="0.819742126465" Y="-28.323119140625" />
                  <Point X="0.831742248535" Y="-28.41426953125" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.982903503418" Y="-29.8685" />
                  <Point X="0.975691589355" Y="-29.870080078125" />
                  <Point X="0.929315490723" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.049143432617" Y="-29.7544375" />
                  <Point X="-1.058444580078" Y="-29.7526328125" />
                  <Point X="-1.14124609375" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.541037109375" />
                  <Point X="-1.121759155273" Y="-29.50933203125" />
                  <Point X="-1.123333984375" Y="-29.497693359375" />
                  <Point X="-1.180555541992" Y="-29.210021484375" />
                  <Point X="-1.18812487793" Y="-29.178470703125" />
                  <Point X="-1.199026367188" Y="-29.149505859375" />
                  <Point X="-1.205664428711" Y="-29.135470703125" />
                  <Point X="-1.221734985352" Y="-29.107634765625" />
                  <Point X="-1.230569580078" Y="-29.094869140625" />
                  <Point X="-1.250204711914" Y="-29.07094140625" />
                  <Point X="-1.261005615234" Y="-29.059779296875" />
                  <Point X="-1.481526367188" Y="-28.866388671875" />
                  <Point X="-1.506741699219" Y="-28.84596484375" />
                  <Point X="-1.533024291992" Y="-28.829623046875" />
                  <Point X="-1.546831420898" Y="-28.82253125" />
                  <Point X="-1.576532470703" Y="-28.810228515625" />
                  <Point X="-1.591312133789" Y="-28.805478515625" />
                  <Point X="-1.621454101563" Y="-28.79844921875" />
                  <Point X="-1.63681640625" Y="-28.796169921875" />
                  <Point X="-1.929496459961" Y="-28.776986328125" />
                  <Point X="-1.961929321289" Y="-28.7761328125" />
                  <Point X="-1.992729858398" Y="-28.779166015625" />
                  <Point X="-2.008006591797" Y="-28.7819453125" />
                  <Point X="-2.039052001953" Y="-28.790263671875" />
                  <Point X="-2.053671386719" Y="-28.795494140625" />
                  <Point X="-2.081863525391" Y="-28.808267578125" />
                  <Point X="-2.095436035156" Y="-28.815810546875" />
                  <Point X="-2.3393125" Y="-28.978763671875" />
                  <Point X="-2.351628662109" Y="-28.987728515625" />
                  <Point X="-2.377608642578" Y="-29.008279296875" />
                  <Point X="-2.386340576172" Y="-29.016109375" />
                  <Point X="-2.402771972656" Y="-29.03278515625" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.422754638672" Y="-29.057638671875" />
                  <Point X="-2.503203125" Y="-29.162482421875" />
                  <Point X="-2.733979492188" Y="-29.01958984375" />
                  <Point X="-2.747577880859" Y="-29.011169921875" />
                  <Point X="-2.980862792969" Y="-28.831548828125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413085938" Y="-27.634658203125" />
                  <Point X="-2.311638671875" Y="-27.619232421875" />
                  <Point X="-2.310626464844" Y="-27.588296875" />
                  <Point X="-2.314666992188" Y="-27.557611328125" />
                  <Point X="-2.323652587891" Y="-27.5279921875" />
                  <Point X="-2.329359863281" Y="-27.513548828125" />
                  <Point X="-2.343577636719" Y="-27.484720703125" />
                  <Point X="-2.351562255859" Y="-27.47140234375" />
                  <Point X="-2.369590087891" Y="-27.446248046875" />
                  <Point X="-2.379633300781" Y="-27.434412109375" />
                  <Point X="-2.396034423828" Y="-27.41801171875" />
                  <Point X="-2.408825927734" Y="-27.407013671875" />
                  <Point X="-2.433989990234" Y="-27.388982421875" />
                  <Point X="-2.447313720703" Y="-27.380998046875" />
                  <Point X="-2.476141357422" Y="-27.36678515625" />
                  <Point X="-2.490583007812" Y="-27.361080078125" />
                  <Point X="-2.520198486328" Y="-27.352099609375" />
                  <Point X="-2.550877929688" Y="-27.3480625" />
                  <Point X="-2.581808837891" Y="-27.349076171875" />
                  <Point X="-2.597234130859" Y="-27.3508515625" />
                  <Point X="-2.628757324219" Y="-27.357123046875" />
                  <Point X="-2.6436875" Y="-27.36138671875" />
                  <Point X="-2.672651123047" Y="-27.3722890625" />
                  <Point X="-2.686684570312" Y="-27.378927734375" />
                  <Point X="-2.757109130859" Y="-27.419587890625" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-3.993267578125" Y="-27.75471484375" />
                  <Point X="-4.004018310547" Y="-27.74058984375" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954607177734" Y="-26.415529296875" />
                  <Point X="-2.951552734375" Y="-26.3988046875" />
                  <Point X="-2.948748535156" Y="-26.368376953125" />
                  <Point X="-2.948577880859" Y="-26.353048828125" />
                  <Point X="-2.950786865234" Y="-26.321376953125" />
                  <Point X="-2.953083740234" Y="-26.306220703125" />
                  <Point X="-2.960085205078" Y="-26.276474609375" />
                  <Point X="-2.971779052734" Y="-26.2482421875" />
                  <Point X="-2.987861816406" Y="-26.2222578125" />
                  <Point X="-2.996955322266" Y="-26.209916015625" />
                  <Point X="-3.017788818359" Y="-26.185958984375" />
                  <Point X="-3.028750244141" Y="-26.175240234375" />
                  <Point X="-3.052251220703" Y="-26.15570703125" />
                  <Point X="-3.064790771484" Y="-26.146892578125" />
                  <Point X="-3.089799316406" Y="-26.132173828125" />
                  <Point X="-3.105464111328" Y="-26.124466796875" />
                  <Point X="-3.13469921875" Y="-26.1132578125" />
                  <Point X="-3.149780761719" Y="-26.108865234375" />
                  <Point X="-3.181672851562" Y="-26.102380859375" />
                  <Point X="-3.197292236328" Y="-26.10053515625" />
                  <Point X="-3.228619140625" Y="-26.099443359375" />
                  <Point X="-3.244326660156" Y="-26.100197265625" />
                  <Point X="-3.333230957031" Y="-26.111900390625" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.736556640625" Y="-25.99058203125" />
                  <Point X="-4.740761230469" Y="-25.97412109375" />
                  <Point X="-4.786452148438" Y="-25.65465625" />
                  <Point X="-3.508286132812" Y="-25.312171875" />
                  <Point X="-3.500473632812" Y="-25.309712890625" />
                  <Point X="-3.47393359375" Y="-25.299251953125" />
                  <Point X="-3.444165527344" Y="-25.283892578125" />
                  <Point X="-3.433556884766" Y="-25.27751171875" />
                  <Point X="-3.407319824219" Y="-25.25930078125" />
                  <Point X="-3.393680419922" Y="-25.248248046875" />
                  <Point X="-3.371260498047" Y="-25.226400390625" />
                  <Point X="-3.360956542969" Y="-25.214546875" />
                  <Point X="-3.341688476562" Y="-25.188275390625" />
                  <Point X="-3.333460205078" Y="-25.174849609375" />
                  <Point X="-3.319334716797" Y="-25.14682421875" />
                  <Point X="-3.313437255859" Y="-25.132224609375" />
                  <Point X="-3.304704345703" Y="-25.1040859375" />
                  <Point X="-3.301005859375" Y="-25.088560546875" />
                  <Point X="-3.296725830078" Y="-25.06037109375" />
                  <Point X="-3.295649414062" Y="-25.046115234375" />
                  <Point X="-3.295647949219" Y="-25.01646875" />
                  <Point X="-3.296721191406" Y="-25.0022265625" />
                  <Point X="-3.300990478516" Y="-24.974064453125" />
                  <Point X="-3.304186523438" Y="-24.96014453125" />
                  <Point X="-3.312908691406" Y="-24.932041015625" />
                  <Point X="-3.319311279297" Y="-24.91579296875" />
                  <Point X="-3.333420166016" Y="-24.887779296875" />
                  <Point X="-3.341638427734" Y="-24.874359375" />
                  <Point X="-3.360886962891" Y="-24.848091796875" />
                  <Point X="-3.371208007812" Y="-24.836212890625" />
                  <Point X="-3.393669921875" Y="-24.8143203125" />
                  <Point X="-3.405810791016" Y="-24.804306640625" />
                  <Point X="-3.432035644531" Y="-24.78610546875" />
                  <Point X="-3.43826953125" Y="-24.78212890625" />
                  <Point X="-3.461508056641" Y="-24.769390625" />
                  <Point X="-3.495431640625" Y="-24.754841796875" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-3.589328613281" Y="-24.728671875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.7340390625" Y="-24.060771484375" />
                  <Point X="-4.731330566406" Y="-24.042466796875" />
                  <Point X="-4.633586914062" Y="-23.681763671875" />
                  <Point X="-3.778070068359" Y="-23.79439453125" />
                  <Point X="-3.767741699219" Y="-23.79518359375" />
                  <Point X="-3.747056396484" Y="-23.795634765625" />
                  <Point X="-3.736703369141" Y="-23.795296875" />
                  <Point X="-3.715142578125" Y="-23.79341015625" />
                  <Point X="-3.704888671875" Y="-23.7919453125" />
                  <Point X="-3.684604736328" Y="-23.78791015625" />
                  <Point X="-3.674574707031" Y="-23.78533984375" />
                  <Point X="-3.616503662109" Y="-23.76703125" />
                  <Point X="-3.603462646484" Y="-23.762328125" />
                  <Point X="-3.584501220703" Y="-23.753986328125" />
                  <Point X="-3.575244384766" Y="-23.749294921875" />
                  <Point X="-3.556506103516" Y="-23.73847265625" />
                  <Point X="-3.547829345703" Y="-23.73280859375" />
                  <Point X="-3.531153076172" Y="-23.720576171875" />
                  <Point X="-3.515913330078" Y="-23.706607421875" />
                  <Point X="-3.502279052734" Y="-23.69105859375" />
                  <Point X="-3.495885009766" Y="-23.68291015625" />
                  <Point X="-3.483474365234" Y="-23.66518359375" />
                  <Point X="-3.478008056641" Y="-23.656392578125" />
                  <Point X="-3.468062255859" Y="-23.638263671875" />
                  <Point X="-3.463582763672" Y="-23.62892578125" />
                  <Point X="-3.440281494141" Y="-23.572671875" />
                  <Point X="-3.435498291016" Y="-23.559646484375" />
                  <Point X="-3.429709960938" Y="-23.53978515625" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436012695312" Y="-23.395466796875" />
                  <Point X="-3.443509521484" Y="-23.376189453125" />
                  <Point X="-3.447783691406" Y="-23.3667578125" />
                  <Point X="-3.475899169922" Y="-23.312748046875" />
                  <Point X="-3.482817382812" Y="-23.30068359375" />
                  <Point X="-3.494306152344" Y="-23.283494140625" />
                  <Point X="-3.500519287109" Y="-23.275212890625" />
                  <Point X="-3.514428710938" Y="-23.258638671875" />
                  <Point X="-3.521509521484" Y="-23.251080078125" />
                  <Point X="-3.536446289062" Y="-23.236783203125" />
                  <Point X="-3.544302246094" Y="-23.230044921875" />
                  <Point X="-3.590787353516" Y="-23.194375" />
                  <Point X="-4.227615234375" Y="-22.70571875" />
                  <Point X="-4.012813476562" Y="-22.3377109375" />
                  <Point X="-4.002292480469" Y="-22.319685546875" />
                  <Point X="-3.726337890625" Y="-21.964986328125" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244924804688" Y="-22.242279296875" />
                  <Point X="-3.225997314453" Y="-22.250609375" />
                  <Point X="-3.216302001953" Y="-22.254259765625" />
                  <Point X="-3.195659912109" Y="-22.26076953125" />
                  <Point X="-3.185624267578" Y="-22.263341796875" />
                  <Point X="-3.16533203125" Y="-22.26737890625" />
                  <Point X="-3.155075439453" Y="-22.26884375" />
                  <Point X="-3.074198486328" Y="-22.275919921875" />
                  <Point X="-3.059125488281" Y="-22.27666796875" />
                  <Point X="-3.038461181641" Y="-22.276212890625" />
                  <Point X="-3.028145996094" Y="-22.275421875" />
                  <Point X="-3.006691162109" Y="-22.272595703125" />
                  <Point X="-2.996515625" Y="-22.2706875" />
                  <Point X="-2.976427490234" Y="-22.265771484375" />
                  <Point X="-2.956993408203" Y="-22.258697265625" />
                  <Point X="-2.938445556641" Y="-22.24955078125" />
                  <Point X="-2.929419189453" Y="-22.244470703125" />
                  <Point X="-2.911168212891" Y="-22.23284375" />
                  <Point X="-2.902748535156" Y="-22.226810546875" />
                  <Point X="-2.886616455078" Y="-22.21386328125" />
                  <Point X="-2.878904052734" Y="-22.20694921875" />
                  <Point X="-2.821497070312" Y="-22.14954296875" />
                  <Point X="-2.811267822266" Y="-22.138515625" />
                  <Point X="-2.798322265625" Y="-22.12238671875" />
                  <Point X="-2.792289306641" Y="-22.11396875" />
                  <Point X="-2.780659423828" Y="-22.09571484375" />
                  <Point X="-2.775579101562" Y="-22.086689453125" />
                  <Point X="-2.7664296875" Y="-22.068138671875" />
                  <Point X="-2.759353515625" Y="-22.04869921875" />
                  <Point X="-2.754436523438" Y="-22.028607421875" />
                  <Point X="-2.752526611328" Y="-22.0184296875" />
                  <Point X="-2.749700683594" Y="-21.996970703125" />
                  <Point X="-2.74891015625" Y="-21.986640625" />
                  <Point X="-2.748458496094" Y="-21.96595703125" />
                  <Point X="-2.748797363281" Y="-21.955603515625" />
                  <Point X="-2.755873291016" Y="-21.8747265625" />
                  <Point X="-2.757745361328" Y="-21.85980078125" />
                  <Point X="-2.761779785156" Y="-21.839515625" />
                  <Point X="-2.764350830078" Y="-21.829482421875" />
                  <Point X="-2.770859375" Y="-21.808837890625" />
                  <Point X="-2.774509033203" Y="-21.799140625" />
                  <Point X="-2.782841064453" Y="-21.780205078125" />
                  <Point X="-2.7875234375" Y="-21.770966796875" />
                  <Point X="-2.808122314453" Y="-21.7352890625" />
                  <Point X="-3.059386962891" Y="-21.300087890625" />
                  <Point X="-2.666689453125" Y="-20.999009765625" />
                  <Point X="-2.648363769531" Y="-20.984958984375" />
                  <Point X="-2.192522949219" Y="-20.731705078125" />
                  <Point X="-2.118563720703" Y="-20.82808984375" />
                  <Point X="-2.111823486328" Y="-20.835947265625" />
                  <Point X="-2.097521728516" Y="-20.850888671875" />
                  <Point X="-2.089960449219" Y="-20.85797265625" />
                  <Point X="-2.073379150391" Y="-20.87188671875" />
                  <Point X="-2.065092041016" Y="-20.878103515625" />
                  <Point X="-2.047892822266" Y="-20.889595703125" />
                  <Point X="-2.038980957031" Y="-20.89487109375" />
                  <Point X="-1.948965332031" Y="-20.941732421875" />
                  <Point X="-1.934326904297" Y="-20.94871484375" />
                  <Point X="-1.915044433594" Y="-20.956212890625" />
                  <Point X="-1.905199829102" Y="-20.959435546875" />
                  <Point X="-1.884291992188" Y="-20.965037109375" />
                  <Point X="-1.874157226562" Y="-20.96716796875" />
                  <Point X="-1.853712158203" Y="-20.970314453125" />
                  <Point X="-1.833045776367" Y="-20.971216796875" />
                  <Point X="-1.812404296875" Y="-20.96986328125" />
                  <Point X="-1.802118896484" Y="-20.968623046875" />
                  <Point X="-1.780802734375" Y="-20.96486328125" />
                  <Point X="-1.770711181641" Y="-20.9625078125" />
                  <Point X="-1.750857299805" Y="-20.95671875" />
                  <Point X="-1.741094970703" Y="-20.95328515625" />
                  <Point X="-1.647337890625" Y="-20.91444921875" />
                  <Point X="-1.632577636719" Y="-20.907724609375" />
                  <Point X="-1.614434448242" Y="-20.89776953125" />
                  <Point X="-1.605635009766" Y="-20.892296875" />
                  <Point X="-1.587907226562" Y="-20.879880859375" />
                  <Point X="-1.579760131836" Y="-20.873484375" />
                  <Point X="-1.564213867188" Y="-20.85984765625" />
                  <Point X="-1.550247680664" Y="-20.84460546875" />
                  <Point X="-1.538020019531" Y="-20.8279296875" />
                  <Point X="-1.532359130859" Y="-20.81925390625" />
                  <Point X="-1.521538330078" Y="-20.80051171875" />
                  <Point X="-1.51685546875" Y="-20.791271484375" />
                  <Point X="-1.508525024414" Y="-20.772337890625" />
                  <Point X="-1.504877197266" Y="-20.76264453125" />
                  <Point X="-1.474360839844" Y="-20.665859375" />
                  <Point X="-1.470026489258" Y="-20.650236328125" />
                  <Point X="-1.465990966797" Y="-20.62994921875" />
                  <Point X="-1.464526733398" Y="-20.619693359375" />
                  <Point X="-1.462640625" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462753173828" Y="-20.5671015625" />
                  <Point X="-1.465885009766" Y="-20.538986328125" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-0.954886657715" Y="-20.290328125" />
                  <Point X="-0.931166748047" Y="-20.2836796875" />
                  <Point X="-0.365222503662" Y="-20.21744140625" />
                  <Point X="-0.225666183472" Y="-20.7382734375" />
                  <Point X="-0.22043510437" Y="-20.752892578125" />
                  <Point X="-0.207661712646" Y="-20.781083984375" />
                  <Point X="-0.200119384766" Y="-20.79465625" />
                  <Point X="-0.182261154175" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451187134" Y="-20.856134765625" />
                  <Point X="-0.126896308899" Y="-20.8749765625" />
                  <Point X="-0.099600227356" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054915859222" Y="-20.90607421875" />
                  <Point X="-0.03985351944" Y="-20.909845703125" />
                  <Point X="-0.00931760788" Y="-20.91488671875" />
                  <Point X="0.021629692078" Y="-20.91488671875" />
                  <Point X="0.052165603638" Y="-20.909845703125" />
                  <Point X="0.067227790833" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912307739" Y="-20.88956640625" />
                  <Point X="0.139208236694" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572799683" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.219973495483" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978408813" Y="-20.7382734375" />
                  <Point X="0.248532867432" Y="-20.6988828125" />
                  <Point X="0.378190856934" Y="-20.2149921875" />
                  <Point X="0.807160583496" Y="-20.25991796875" />
                  <Point X="0.827852600098" Y="-20.262083984375" />
                  <Point X="1.430837524414" Y="-20.4076640625" />
                  <Point X="1.453619506836" Y="-20.413166015625" />
                  <Point X="1.845016235352" Y="-20.555126953125" />
                  <Point X="1.858244750977" Y="-20.55992578125" />
                  <Point X="2.237758789062" Y="-20.73741015625" />
                  <Point X="2.250431884766" Y="-20.743337890625" />
                  <Point X="2.617114746094" Y="-20.956966796875" />
                  <Point X="2.629436035156" Y="-20.96414453125" />
                  <Point X="2.817779296875" Y="-21.098083984375" />
                  <Point X="2.065308105469" Y="-22.40140234375" />
                  <Point X="2.062370605469" Y="-22.4068984375" />
                  <Point X="2.053182128906" Y="-22.426560546875" />
                  <Point X="2.044182373047" Y="-22.450435546875" />
                  <Point X="2.041301635742" Y="-22.45940234375" />
                  <Point X="2.021004516602" Y="-22.5353046875" />
                  <Point X="2.019389648438" Y="-22.542431640625" />
                  <Point X="2.015649902344" Y="-22.56401953125" />
                  <Point X="2.013139160156" Y="-22.587306640625" />
                  <Point X="2.01259362793" Y="-22.59808984375" />
                  <Point X="2.012729614258" Y="-22.619646484375" />
                  <Point X="2.013410888672" Y="-22.630419921875" />
                  <Point X="2.021325317383" Y="-22.696052734375" />
                  <Point X="2.023799926758" Y="-22.710962890625" />
                  <Point X="2.029142089844" Y="-22.732884765625" />
                  <Point X="2.032466674805" Y="-22.7436875" />
                  <Point X="2.040732788086" Y="-22.76577734375" />
                  <Point X="2.045318115234" Y="-22.776111328125" />
                  <Point X="2.055680908203" Y="-22.79615625" />
                  <Point X="2.061458740234" Y="-22.8058671875" />
                  <Point X="2.102070068359" Y="-22.86571875" />
                  <Point X="2.106431396484" Y="-22.871640625" />
                  <Point X="2.120417724609" Y="-22.88870703125" />
                  <Point X="2.136064453125" Y="-22.90546484375" />
                  <Point X="2.143728271484" Y="-22.9128046875" />
                  <Point X="2.159824951172" Y="-22.92658203125" />
                  <Point X="2.1682578125" Y="-22.93301953125" />
                  <Point X="2.228085693359" Y="-22.973615234375" />
                  <Point X="2.241225830078" Y="-22.98172265625" />
                  <Point X="2.261287841797" Y="-22.992099609375" />
                  <Point X="2.271629394531" Y="-22.996689453125" />
                  <Point X="2.293731933594" Y="-23.004962890625" />
                  <Point X="2.304537841797" Y="-23.0082890625" />
                  <Point X="2.32646875" Y="-23.013634765625" />
                  <Point X="2.33759375" Y="-23.015654296875" />
                  <Point X="2.403226806641" Y="-23.023568359375" />
                  <Point X="2.410745849609" Y="-23.024173828125" />
                  <Point X="2.433342529297" Y="-23.024791015625" />
                  <Point X="2.456034423828" Y="-23.02391015625" />
                  <Point X="2.466546630859" Y="-23.022916015625" />
                  <Point X="2.487403320312" Y="-23.019763671875" />
                  <Point X="2.497747802734" Y="-23.01760546875" />
                  <Point X="2.573649169922" Y="-22.99730859375" />
                  <Point X="2.594708007812" Y="-22.990298828125" />
                  <Point X="2.625318603516" Y="-22.977443359375" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="2.717544921875" Y="-22.925064453125" />
                  <Point X="3.940403076172" Y="-22.219048828125" />
                  <Point X="4.036643798828" Y="-22.35280078125" />
                  <Point X="4.043955810547" Y="-22.362962890625" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.172953125" Y="-23.2561796875" />
                  <Point X="3.168142822266" Y="-23.26012890625" />
                  <Point X="3.152114013672" Y="-23.2747890625" />
                  <Point X="3.134666748047" Y="-23.293400390625" />
                  <Point X="3.128577148438" Y="-23.300578125" />
                  <Point X="3.073950927734" Y="-23.371841796875" />
                  <Point X="3.069836914062" Y="-23.377646484375" />
                  <Point X="3.058390869141" Y="-23.395638671875" />
                  <Point X="3.046765380859" Y="-23.41670703125" />
                  <Point X="3.042021240234" Y="-23.426619140625" />
                  <Point X="3.033708740234" Y="-23.4469296875" />
                  <Point X="3.030140380859" Y="-23.457328125" />
                  <Point X="3.009791992188" Y="-23.53008984375" />
                  <Point X="3.006225585938" Y="-23.545337890625" />
                  <Point X="3.002772216797" Y="-23.567634765625" />
                  <Point X="3.001709716797" Y="-23.57888671875" />
                  <Point X="3.000893066406" Y="-23.60245703125" />
                  <Point X="3.001174560547" Y="-23.6137578125" />
                  <Point X="3.003077636719" Y="-23.636244140625" />
                  <Point X="3.00469921875" Y="-23.6474296875" />
                  <Point X="3.021403076172" Y="-23.728384765625" />
                  <Point X="3.023116210938" Y="-23.735349609375" />
                  <Point X="3.029295166016" Y="-23.755935546875" />
                  <Point X="3.037457519531" Y="-23.777990234375" />
                  <Point X="3.041774658203" Y="-23.787888671875" />
                  <Point X="3.051505126953" Y="-23.807130859375" />
                  <Point X="3.056918457031" Y="-23.816474609375" />
                  <Point X="3.102351074219" Y="-23.885529296875" />
                  <Point X="3.111738037109" Y="-23.89857421875" />
                  <Point X="3.1262890625" Y="-23.91581640625" />
                  <Point X="3.134078125" Y="-23.92400390625" />
                  <Point X="3.151323242188" Y="-23.940095703125" />
                  <Point X="3.160031982422" Y="-23.947302734375" />
                  <Point X="3.178244384766" Y="-23.960630859375" />
                  <Point X="3.187748046875" Y="-23.966751953125" />
                  <Point X="3.253586425781" Y="-24.0038125" />
                  <Point X="3.260132080078" Y="-24.007169921875" />
                  <Point X="3.280276367188" Y="-24.01622265625" />
                  <Point X="3.302055175781" Y="-24.024361328125" />
                  <Point X="3.312314697266" Y="-24.027546875" />
                  <Point X="3.333123779297" Y="-24.03273828125" />
                  <Point X="3.343673339844" Y="-24.034744140625" />
                  <Point X="3.432691162109" Y="-24.0465078125" />
                  <Point X="3.454195068359" Y="-24.048078125" />
                  <Point X="3.488376708984" Y="-24.048015625" />
                  <Point X="3.500602539062" Y="-24.047203125" />
                  <Point X="3.578032714844" Y="-24.037009765625" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.749544921875" Y="-24.072876953125" />
                  <Point X="4.752685058594" Y="-24.0857734375" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="3.691994140625" Y="-24.578646484375" />
                  <Point X="3.686036376953" Y="-24.580455078125" />
                  <Point X="3.665628173828" Y="-24.587861328125" />
                  <Point X="3.642387939453" Y="-24.598376953125" />
                  <Point X="3.634010009766" Y="-24.6026796875" />
                  <Point X="3.546552734375" Y="-24.65323046875" />
                  <Point X="3.540685791016" Y="-24.6569140625" />
                  <Point X="3.523659179688" Y="-24.66879296875" />
                  <Point X="3.504288574219" Y="-24.68413671875" />
                  <Point X="3.495948730469" Y="-24.69158203125" />
                  <Point X="3.480201904297" Y="-24.707400390625" />
                  <Point X="3.472794921875" Y="-24.7157734375" />
                  <Point X="3.420320556641" Y="-24.782638671875" />
                  <Point X="3.410850585938" Y="-24.795796875" />
                  <Point X="3.399125732422" Y="-24.815080078125" />
                  <Point X="3.393839355469" Y="-24.825068359375" />
                  <Point X="3.384067382812" Y="-24.846529296875" />
                  <Point X="3.380004638672" Y="-24.857072265625" />
                  <Point X="3.373159179688" Y="-24.8785703125" />
                  <Point X="3.370376464844" Y="-24.889525390625" />
                  <Point X="3.352885009766" Y="-24.980859375" />
                  <Point X="3.351822753906" Y="-24.98777734375" />
                  <Point X="3.349660888672" Y="-25.008640625" />
                  <Point X="3.348650878906" Y="-25.032732421875" />
                  <Point X="3.348824951172" Y="-25.043701171875" />
                  <Point X="3.350437011719" Y="-25.065548828125" />
                  <Point X="3.351875" Y="-25.076427734375" />
                  <Point X="3.369366455078" Y="-25.16776171875" />
                  <Point X="3.373159423828" Y="-25.183990234375" />
                  <Point X="3.380005126953" Y="-25.205490234375" />
                  <Point X="3.384068603516" Y="-25.216037109375" />
                  <Point X="3.39383984375" Y="-25.23749609375" />
                  <Point X="3.399127685547" Y="-25.247486328125" />
                  <Point X="3.410852294922" Y="-25.266767578125" />
                  <Point X="3.4172890625" Y="-25.27605859375" />
                  <Point X="3.469763427734" Y="-25.342923828125" />
                  <Point X="3.474416748047" Y="-25.348412109375" />
                  <Point X="3.489182861328" Y="-25.364142578125" />
                  <Point X="3.506528564453" Y="-25.380427734375" />
                  <Point X="3.514895996094" Y="-25.387423828125" />
                  <Point X="3.532379882812" Y="-25.4004140625" />
                  <Point X="3.541496337891" Y="-25.406408203125" />
                  <Point X="3.628953613281" Y="-25.456958984375" />
                  <Point X="3.647685546875" Y="-25.46640234375" />
                  <Point X="3.680134277344" Y="-25.4798828125" />
                  <Point X="3.691994384766" Y="-25.483916015625" />
                  <Point X="3.763001464844" Y="-25.50294140625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.763366210938" Y="-25.919427734375" />
                  <Point X="4.761612304688" Y="-25.9310625" />
                  <Point X="4.727803222656" Y="-26.07921875" />
                  <Point X="3.436782714844" Y="-25.90925390625" />
                  <Point X="3.428623535156" Y="-25.90853515625" />
                  <Point X="3.400097412109" Y="-25.90804296875" />
                  <Point X="3.366719970703" Y="-25.910841796875" />
                  <Point X="3.35448046875" Y="-25.912677734375" />
                  <Point X="3.182832763672" Y="-25.949986328125" />
                  <Point X="3.157885742188" Y="-25.956740234375" />
                  <Point X="3.128772460938" Y="-25.968359375" />
                  <Point X="3.114695068359" Y="-25.97537890625" />
                  <Point X="3.086862792969" Y="-25.992271484375" />
                  <Point X="3.074130615234" Y="-26.001525390625" />
                  <Point X="3.050373779297" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.935598876953" Y="-26.15800390625" />
                  <Point X="2.921325927734" Y="-26.17684765625" />
                  <Point X="2.906604736328" Y="-26.20123046875" />
                  <Point X="2.900162597656" Y="-26.213978515625" />
                  <Point X="2.888819824219" Y="-26.24136328125" />
                  <Point X="2.884362304688" Y="-26.2549296875" />
                  <Point X="2.877531005859" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.860287353516" Y="-26.458255859375" />
                  <Point X="2.859288818359" Y="-26.48332421875" />
                  <Point X="2.861608398438" Y="-26.514595703125" />
                  <Point X="2.864067382812" Y="-26.530134765625" />
                  <Point X="2.871800537109" Y="-26.5617578125" />
                  <Point X="2.8767890625" Y="-26.57667578125" />
                  <Point X="2.889159667969" Y="-26.605484375" />
                  <Point X="2.896541748047" Y="-26.619375" />
                  <Point X="2.991533935547" Y="-26.76712890625" />
                  <Point X="3.003593017578" Y="-26.7838203125" />
                  <Point X="3.020699462891" Y="-26.804982421875" />
                  <Point X="3.028258300781" Y="-26.813279296875" />
                  <Point X="3.044306884766" Y="-26.828927734375" />
                  <Point X="3.052796630859" Y="-26.836279296875" />
                  <Point X="3.118691650391" Y="-26.886841796875" />
                  <Point X="4.087171142578" Y="-27.629984375" />
                  <Point X="4.050427978516" Y="-27.689439453125" />
                  <Point X="4.045501708984" Y="-27.697412109375" />
                  <Point X="4.001273925781" Y="-27.76025390625" />
                  <Point X="2.848454101562" Y="-27.094671875" />
                  <Point X="2.841194335938" Y="-27.09088671875" />
                  <Point X="2.815030273438" Y="-27.079515625" />
                  <Point X="2.783117919922" Y="-27.069326171875" />
                  <Point X="2.771105957031" Y="-27.0663359375" />
                  <Point X="2.566817871094" Y="-27.02944140625" />
                  <Point X="2.539350585938" Y="-27.025806640625" />
                  <Point X="2.507993408203" Y="-27.025404296875" />
                  <Point X="2.492301269531" Y="-27.026505859375" />
                  <Point X="2.460131347656" Y="-27.03146484375" />
                  <Point X="2.444839599609" Y="-27.035138671875" />
                  <Point X="2.415068847656" Y="-27.0449609375" />
                  <Point X="2.40058984375" Y="-27.051109375" />
                  <Point X="2.230876708984" Y="-27.140427734375" />
                  <Point X="2.208971435547" Y="-27.15316796875" />
                  <Point X="2.186042480469" Y="-27.17005859375" />
                  <Point X="2.175213867187" Y="-27.179369140625" />
                  <Point X="2.154253662109" Y="-27.200328125" />
                  <Point X="2.144940673828" Y="-27.21116015625" />
                  <Point X="2.128045898438" Y="-27.23409375" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.031145019531" Y="-27.415908203125" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.039081787109" Y="-27.7844296875" />
                  <Point X="2.04346875" Y="-27.80321484375" />
                  <Point X="2.051503173828" Y="-27.831130859375" />
                  <Point X="2.055368164062" Y="-27.84201953125" />
                  <Point X="2.064390136719" Y="-27.863244140625" />
                  <Point X="2.069547119141" Y="-27.873580078125" />
                  <Point X="2.111891357422" Y="-27.946921875" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723754394531" Y="-29.036083984375" />
                  <Point X="1.833915161133" Y="-27.876423828125" />
                  <Point X="1.828654541016" Y="-27.8701484375" />
                  <Point X="1.808831787109" Y="-27.849626953125" />
                  <Point X="1.783248779297" Y="-27.828001953125" />
                  <Point X="1.773295776367" Y="-27.82064453125" />
                  <Point X="1.571812744141" Y="-27.691109375" />
                  <Point X="1.546277709961" Y="-27.676244140625" />
                  <Point X="1.517460205078" Y="-27.66387109375" />
                  <Point X="1.502537231445" Y="-27.658880859375" />
                  <Point X="1.470919555664" Y="-27.65115234375" />
                  <Point X="1.455381835938" Y="-27.6486953125" />
                  <Point X="1.424119262695" Y="-27.64637890625" />
                  <Point X="1.408393676758" Y="-27.64651953125" />
                  <Point X="1.188111572266" Y="-27.666791015625" />
                  <Point X="1.161269897461" Y="-27.670330078125" />
                  <Point X="1.133594970703" Y="-27.6771640625" />
                  <Point X="1.120016113281" Y="-27.681625" />
                  <Point X="1.092624389648" Y="-27.692970703125" />
                  <Point X="1.079874511719" Y="-27.6994140625" />
                  <Point X="1.055490966797" Y="-27.71413671875" />
                  <Point X="1.043857421875" Y="-27.722416015625" />
                  <Point X="0.873704101562" Y="-27.86389453125" />
                  <Point X="0.852653015137" Y="-27.883091796875" />
                  <Point X="0.83218347168" Y="-27.90683984375" />
                  <Point X="0.822934814453" Y="-27.919564453125" />
                  <Point X="0.80604309082" Y="-27.947388671875" />
                  <Point X="0.799020629883" Y="-27.96146484375" />
                  <Point X="0.787395202637" Y="-27.9905859375" />
                  <Point X="0.782792053223" Y="-28.005630859375" />
                  <Point X="0.731916992188" Y="-28.2396953125" />
                  <Point X="0.728915405273" Y="-28.25758203125" />
                  <Point X="0.725262817383" Y="-28.28825" />
                  <Point X="0.724598022461" Y="-28.3000703125" />
                  <Point X="0.72474395752" Y="-28.323705078125" />
                  <Point X="0.725554870605" Y="-28.33551953125" />
                  <Point X="0.737554992676" Y="-28.426669921875" />
                  <Point X="0.833091125488" Y="-29.152337890625" />
                  <Point X="0.655065124512" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642143859863" Y="-28.453576171875" />
                  <Point X="0.62678704834" Y="-28.423814453125" />
                  <Point X="0.620407714844" Y="-28.413208984375" />
                  <Point X="0.465622619629" Y="-28.190193359375" />
                  <Point X="0.446668884277" Y="-28.165169921875" />
                  <Point X="0.424785675049" Y="-28.142716796875" />
                  <Point X="0.412913970947" Y="-28.13240234375" />
                  <Point X="0.386658996582" Y="-28.113158203125" />
                  <Point X="0.373245178223" Y="-28.104939453125" />
                  <Point X="0.345242980957" Y="-28.090830078125" />
                  <Point X="0.330654602051" Y="-28.084939453125" />
                  <Point X="0.091133636475" Y="-28.0106015625" />
                  <Point X="0.063377716064" Y="-28.003111328125" />
                  <Point X="0.03521843338" Y="-27.998841796875" />
                  <Point X="0.020977237701" Y="-27.997767578125" />
                  <Point X="-0.008665153503" Y="-27.997767578125" />
                  <Point X="-0.022906497955" Y="-27.998841796875" />
                  <Point X="-0.051065631866" Y="-28.003111328125" />
                  <Point X="-0.064983718872" Y="-28.006306640625" />
                  <Point X="-0.304504699707" Y="-28.08064453125" />
                  <Point X="-0.332927337646" Y="-28.090830078125" />
                  <Point X="-0.36092477417" Y="-28.104935546875" />
                  <Point X="-0.374337371826" Y="-28.113150390625" />
                  <Point X="-0.400590576172" Y="-28.132390625" />
                  <Point X="-0.412468658447" Y="-28.1427109375" />
                  <Point X="-0.434356506348" Y="-28.165169921875" />
                  <Point X="-0.444366577148" Y="-28.17730859375" />
                  <Point X="-0.599151672363" Y="-28.400326171875" />
                  <Point X="-0.608264343262" Y="-28.414806640625" />
                  <Point X="-0.624428833008" Y="-28.443259765625" />
                  <Point X="-0.629721740723" Y="-28.454134765625" />
                  <Point X="-0.638883911133" Y="-28.47647265625" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.664632263184" Y="-28.56958984375" />
                  <Point X="-0.985425048828" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.88413034447" Y="-22.167805422872" />
                  <Point X="-4.150105096226" Y="-22.765194496247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.562362816527" Y="-23.691140496369" />
                  <Point X="-4.737576430865" Y="-24.084676717461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.697302608985" Y="-21.981749825468" />
                  <Point X="-4.072594958077" Y="-22.824670242493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.464130310822" Y="-23.704073043041" />
                  <Point X="-4.778450684236" Y="-24.410048160518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.614576985568" Y="-22.029511400007" />
                  <Point X="-3.995084819928" Y="-22.88414598874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.365897805116" Y="-23.717005589714" />
                  <Point X="-4.685543875827" Y="-24.434942419164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.53185136215" Y="-22.077272974547" />
                  <Point X="-3.917574681779" Y="-22.943621734987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.267665299411" Y="-23.729938136386" />
                  <Point X="-4.592637067418" Y="-24.45983667781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.449125738732" Y="-22.125034549086" />
                  <Point X="-3.84006454363" Y="-23.003097481234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.169432793706" Y="-23.742870683059" />
                  <Point X="-4.499730259008" Y="-24.484730936456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.935585241428" Y="-21.20517007413" />
                  <Point X="-3.013348317702" Y="-21.379828803094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.366400115315" Y="-22.172796123626" />
                  <Point X="-3.762554405482" Y="-23.06257322748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.071200288" Y="-23.755803229731" />
                  <Point X="-4.406823450599" Y="-24.509625195102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777700206436" Y="-21.084120846369" />
                  <Point X="-2.954634973326" Y="-21.481522839386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.283674491897" Y="-22.220557698165" />
                  <Point X="-3.685044267333" Y="-23.122048973727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.972967782295" Y="-23.768735776404" />
                  <Point X="-4.31391664219" Y="-24.534519453748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.623380156271" Y="-20.971078705628" />
                  <Point X="-2.89592162895" Y="-21.583216875679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.197350178473" Y="-22.260236482612" />
                  <Point X="-3.607534129184" Y="-23.181524719974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.874735276589" Y="-23.781668323076" />
                  <Point X="-4.22100983378" Y="-24.559413712394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.69810330424" Y="-25.630983191636" />
                  <Point X="-4.767534254322" Y="-25.786927658768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.48521293257" Y="-20.894316407126" />
                  <Point X="-2.837208284574" Y="-21.684910911971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.099362210798" Y="-22.273718270694" />
                  <Point X="-3.530626483874" Y="-23.242353687293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776465482101" Y="-23.794517117771" />
                  <Point X="-4.128103025371" Y="-24.58430797104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.580026407582" Y="-25.599344506473" />
                  <Point X="-4.7422507744" Y="-25.96370639997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347045708868" Y="-20.817554108624" />
                  <Point X="-2.779264465639" Y="-21.788333330703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.993717502054" Y="-22.270002736766" />
                  <Point X="-3.466239434868" Y="-23.331304374343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.667378975416" Y="-23.783071179099" />
                  <Point X="-4.035196216961" Y="-24.609202229686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.461949510924" Y="-25.567705821311" />
                  <Point X="-4.70508411624" Y="-26.113795085859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.208878485167" Y="-20.740791810123" />
                  <Point X="-2.748953407772" Y="-21.953819946957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.847810059615" Y="-22.17585562234" />
                  <Point X="-3.421572091609" Y="-23.46454624567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.53770310118" Y="-23.725380763755" />
                  <Point X="-3.942289408552" Y="-24.634096488332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.343872614266" Y="-25.536067136148" />
                  <Point X="-4.667173511572" Y="-26.262212840533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.134505658554" Y="-20.807314073448" />
                  <Point X="-3.849382600143" Y="-24.658990746978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.225795717608" Y="-25.504428450986" />
                  <Point X="-4.568675538505" Y="-26.27454913775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.062734156965" Y="-20.879679008443" />
                  <Point X="-3.756475791733" Y="-24.683885005624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.10771882095" Y="-25.472789765823" />
                  <Point X="-4.458210135282" Y="-26.260006146746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.979332991374" Y="-20.925923290418" />
                  <Point X="-3.663568983324" Y="-24.70877926427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.989641924292" Y="-25.441151080661" />
                  <Point X="-4.347744732058" Y="-26.245463155741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.891854992096" Y="-20.963010854012" />
                  <Point X="-3.570662187714" Y="-24.733673551665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.871565027635" Y="-25.409512395498" />
                  <Point X="-4.237279328835" Y="-26.230920164736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.789361411186" Y="-20.966372869078" />
                  <Point X="-3.479196481732" Y="-24.761804579358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.753488130977" Y="-25.377873710335" />
                  <Point X="-4.126813925611" Y="-26.216377173732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.44563481896" Y="-20.42791666965" />
                  <Point X="-1.472549087637" Y="-20.48836710684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.66562575094" Y="-20.922024392802" />
                  <Point X="-3.397266726369" Y="-24.811353702813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.635411234319" Y="-25.346235025173" />
                  <Point X="-4.016348522388" Y="-26.201834182727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.326812157626" Y="-20.394602969598" />
                  <Point X="-3.330173369101" Y="-24.894225921986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.517334337661" Y="-25.31459634001" />
                  <Point X="-3.905883119164" Y="-26.187291191723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.207989496291" Y="-20.361289269547" />
                  <Point X="-3.296025397296" Y="-25.051094688436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.376234617262" Y="-25.231247546086" />
                  <Point X="-3.795417715941" Y="-26.172748200718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.089166834956" Y="-20.327975569496" />
                  <Point X="-3.684952312717" Y="-26.158205209714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.970344173622" Y="-20.294661869445" />
                  <Point X="-3.574486909494" Y="-26.143662218709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.138570458379" Y="-27.410614613059" />
                  <Point X="-4.165171296996" Y="-27.47036107481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.857632286239" Y="-20.275073192406" />
                  <Point X="-3.46402150627" Y="-26.129119227705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.980617691026" Y="-27.289413255924" />
                  <Point X="-4.105631828214" Y="-27.570199605307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.747925027828" Y="-20.262233022531" />
                  <Point X="-3.353556103047" Y="-26.1145762367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.822664923673" Y="-27.168211898789" />
                  <Point X="-4.046092359432" Y="-27.670038135804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.638217769417" Y="-20.249392852656" />
                  <Point X="-3.243138332316" Y="-26.100140230026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.66471215632" Y="-27.047010541655" />
                  <Point X="-3.984771884129" Y="-27.765876460159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.528510511007" Y="-20.23655268278" />
                  <Point X="-3.143807138707" Y="-26.110605083263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506759388968" Y="-26.92580918452" />
                  <Point X="-3.919160288621" Y="-27.852076770733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.418803252596" Y="-20.223712512905" />
                  <Point X="-3.058074795105" Y="-26.151613453699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.348806621615" Y="-26.804607827385" />
                  <Point X="-3.853548693113" Y="-27.938277081308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.345233998732" Y="-20.292039630179" />
                  <Point X="-2.986509802495" Y="-26.224442215454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.190853854262" Y="-26.68340647025" />
                  <Point X="-3.782100176325" Y="-28.011367452039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.306163618336" Y="-20.437852485918" />
                  <Point X="-2.949308509683" Y="-26.37445311064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.030574007354" Y="-26.556978406858" />
                  <Point X="-3.64212992874" Y="-27.93055549559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.26709323794" Y="-20.583665341657" />
                  <Point X="-3.502159681154" Y="-27.849743539141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.228022857544" Y="-20.729478197397" />
                  <Point X="-3.362189433569" Y="-27.768931582691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.171088582445" Y="-20.835168088708" />
                  <Point X="-3.222219185984" Y="-27.688119626242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.092657773575" Y="-20.892575974659" />
                  <Point X="-3.082248938399" Y="-27.607307669793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.001399289623" Y="-20.91488671875" />
                  <Point X="-2.942278690814" Y="-27.526495713344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.415273736309" Y="-20.218875858593" />
                  <Point X="0.319528563311" Y="-20.433923038073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.118146713931" Y="-20.886234077375" />
                  <Point X="-2.802308443229" Y="-27.445683756895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.514631282117" Y="-20.229281523824" />
                  <Point X="-2.664229124106" Y="-27.369118895309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.613988827925" Y="-20.239687189054" />
                  <Point X="-2.550864550313" Y="-27.348064260592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.713346373733" Y="-20.250092854284" />
                  <Point X="-2.458976959664" Y="-27.375247719808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812704039061" Y="-20.260498251069" />
                  <Point X="-2.380806069592" Y="-27.433239392937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.907434053021" Y="-20.281297523" />
                  <Point X="-2.321768045766" Y="-27.534204187244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.624807984029" Y="-28.214843032546" />
                  <Point X="-2.920184303799" Y="-28.878269108889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.001331244728" Y="-20.30396734434" />
                  <Point X="-2.842741906407" Y="-28.937897003366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.095228436435" Y="-20.326637165679" />
                  <Point X="-2.765299509014" Y="-28.997524897842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.189125628142" Y="-20.349306987018" />
                  <Point X="-2.684714315118" Y="-29.0500939558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.283022819849" Y="-20.371976808357" />
                  <Point X="-2.603196503267" Y="-29.100568319534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.376920011556" Y="-20.394646629696" />
                  <Point X="-2.521678691416" Y="-29.151042683267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.470017112808" Y="-20.419113483621" />
                  <Point X="-2.341786437672" Y="-28.980564432898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.559549357594" Y="-20.451587136261" />
                  <Point X="-2.193665298906" Y="-28.881445275116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.64908160238" Y="-20.484060788901" />
                  <Point X="-2.050977888429" Y="-28.794530470891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.738613847166" Y="-20.516534441541" />
                  <Point X="-1.939064183168" Y="-28.776734540249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.828146091952" Y="-20.549008094181" />
                  <Point X="-1.837860005254" Y="-28.782992601861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.915379306258" Y="-20.586645453824" />
                  <Point X="-1.736818194372" Y="-28.7896153458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.001448677985" Y="-20.626896846697" />
                  <Point X="-1.635812358904" Y="-28.796318891839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.087518049713" Y="-20.66714823957" />
                  <Point X="-1.544113867557" Y="-28.823927075042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.17358742144" Y="-20.707399632442" />
                  <Point X="-1.465346157991" Y="-28.88057826964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.259281600356" Y="-20.748493722167" />
                  <Point X="-1.390557276552" Y="-28.946166058528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.341853702969" Y="-20.796600110079" />
                  <Point X="-1.315768395113" Y="-29.011753847416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.424425805582" Y="-20.844706497992" />
                  <Point X="-1.242376988052" Y="-29.080480415149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.506997908195" Y="-20.892812885904" />
                  <Point X="-1.185985298865" Y="-29.187388974371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.589570010807" Y="-20.940919273817" />
                  <Point X="-1.153231426001" Y="-29.347388938309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.67028591984" Y="-20.993194740771" />
                  <Point X="-1.121700644737" Y="-29.510136010962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.749268664019" Y="-21.049362959719" />
                  <Point X="2.423608148977" Y="-21.780808452312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02021492506" Y="-22.686844467575" />
                  <Point X="-0.406637686175" Y="-28.137644677255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.763904664468" Y="-28.940079448602" />
                  <Point X="-1.118766462078" Y="-29.737112095687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.067348441212" Y="-22.814547223894" />
                  <Point X="-0.272902269357" Y="-28.070836379986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.921081128325" Y="-29.526669933297" />
                  <Point X="-1.02460985286" Y="-29.759199255758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.132540251846" Y="-22.901690386732" />
                  <Point X="-0.152238297905" Y="-28.0333870297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.209978106527" Y="-22.961328484305" />
                  <Point X="-0.033588419408" Y="-28.000461406256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294443716573" Y="-23.005181984891" />
                  <Point X="0.068595312817" Y="-28.004519352863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390909297848" Y="-23.02208310881" />
                  <Point X="0.160317813201" Y="-28.032073610887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.496805817559" Y="-23.017801998191" />
                  <Point X="0.251683248645" Y="-28.060429849893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.617257879194" Y="-22.980828605145" />
                  <Point X="0.342611690592" Y="-28.089767592368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.755947626346" Y="-22.902892699759" />
                  <Point X="0.423518001955" Y="-28.141615408684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.89591768865" Y="-22.822081159457" />
                  <Point X="0.490147626423" Y="-28.225529188776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.035887750954" Y="-22.741269619156" />
                  <Point X="0.55349891252" Y="-28.316806237408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.175857813259" Y="-22.660458078854" />
                  <Point X="0.848388876378" Y="-27.888038901207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.770161782024" Y="-28.063739831844" />
                  <Point X="0.616850198617" Y="-28.40808328604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.315827875563" Y="-22.579646538553" />
                  <Point X="1.015692984983" Y="-27.745834087735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.731852018586" Y="-28.383351336203" />
                  <Point X="0.666420254304" Y="-28.530313484963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.455797937867" Y="-22.498834998251" />
                  <Point X="3.048679092959" Y="-23.413238895265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034047396445" Y="-23.446102223699" />
                  <Point X="1.152315502392" Y="-27.67254125637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.755584118003" Y="-28.563614535071" />
                  <Point X="0.705490660344" Y="-28.676126283105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.595768000172" Y="-22.41802345795" />
                  <Point X="3.248357731046" Y="-23.198319698038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.018538572822" Y="-23.714501978756" />
                  <Point X="1.261888943492" Y="-27.660001645097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779316243509" Y="-28.743877675342" />
                  <Point X="0.744561066384" Y="-28.821939081247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.735738062476" Y="-22.337211917648" />
                  <Point X="3.406310542053" Y="-23.077118242853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.068971412044" Y="-23.834794334131" />
                  <Point X="1.370322138417" Y="-27.650023068664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.803048369015" Y="-28.924140815612" />
                  <Point X="0.783631472424" Y="-28.967751879388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.87570812478" Y="-22.256400377347" />
                  <Point X="3.564263353061" Y="-22.955916787668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.133509375054" Y="-23.923406062776" />
                  <Point X="1.473526125761" Y="-27.651789484755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826780494521" Y="-29.104403955882" />
                  <Point X="0.822701878465" Y="-29.11356467753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974951353689" Y="-22.267062802537" />
                  <Point X="3.722216164068" Y="-22.834715332484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.212098320635" Y="-23.98045876786" />
                  <Point X="1.562439644156" Y="-27.685652819622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.039192024087" Y="-22.356342261322" />
                  <Point X="3.880168975076" Y="-22.713513877299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.297328512711" Y="-24.022594989088" />
                  <Point X="1.643485089522" Y="-27.737188135852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.099424015209" Y="-22.454625361176" />
                  <Point X="4.038121786083" Y="-22.592312422114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.393007163867" Y="-24.041263586994" />
                  <Point X="1.724333407451" Y="-27.789166207553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.494162607428" Y="-24.047631107756" />
                  <Point X="1.803435767615" Y="-27.845065764604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.604429235346" Y="-24.033534573398" />
                  <Point X="2.217752898866" Y="-27.148060618633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.008880066093" Y="-27.617196682112" />
                  <Point X="1.8715991917" Y="-27.925534574353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.714894712932" Y="-24.018991415371" />
                  <Point X="2.353989436185" Y="-27.075634712745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038888960608" Y="-27.783361968367" />
                  <Point X="1.937406231179" Y="-28.011295910581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.825360190519" Y="-24.004448257344" />
                  <Point X="3.532240369128" Y="-24.662806155349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351007200739" Y="-25.0698625162" />
                  <Point X="2.478936186733" Y="-27.028566083115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.088271985922" Y="-27.906012244383" />
                  <Point X="2.003213270657" Y="-28.097057246809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.935825668106" Y="-23.989905099317" />
                  <Point X="3.670363520681" Y="-24.586142844513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.38702540964" Y="-25.22253066136" />
                  <Point X="3.010787001501" Y="-26.067575961794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866394581204" Y="-26.391886647654" />
                  <Point X="2.581367037235" Y="-27.032068992986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.146985306472" Y="-28.007706334189" />
                  <Point X="2.069020310136" Y="-28.182818583037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.046291145692" Y="-23.97536194129" />
                  <Point X="3.78930014839" Y="-24.552573171793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449123200247" Y="-25.316623106957" />
                  <Point X="3.16497978614" Y="-25.954819664105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.8823479228" Y="-26.589621222643" />
                  <Point X="2.677618079375" Y="-27.049451979692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205698553742" Y="-28.109400588586" />
                  <Point X="2.134827349615" Y="-28.268579919266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.156756623279" Y="-23.960818783263" />
                  <Point X="3.907377157741" Y="-24.520934233517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.51992691846" Y="-25.391161719002" />
                  <Point X="3.280581618212" Y="-25.928740065019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.941769721149" Y="-26.689724045258" />
                  <Point X="2.773793438256" Y="-27.067004953781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.264411801011" Y="-28.211094842983" />
                  <Point X="2.200634389094" Y="-28.354341255494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.267222100866" Y="-23.946275625236" />
                  <Point X="4.025454167093" Y="-24.489295295241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.6016491828" Y="-25.441176874925" />
                  <Point X="3.393542275133" Y="-25.908592642451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003768923732" Y="-26.784037923184" />
                  <Point X="2.861987080107" Y="-27.102485157838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.323125048281" Y="-28.31278909738" />
                  <Point X="2.266441428573" Y="-28.440102591722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.377687578453" Y="-23.931732467208" />
                  <Point X="4.143531176444" Y="-24.457656356965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.687318845079" Y="-25.482326029919" />
                  <Point X="3.493890908681" Y="-25.916772288169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.076427083272" Y="-26.854411391813" />
                  <Point X="2.944712642537" Y="-27.150246869358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.38183829555" Y="-28.414483351777" />
                  <Point X="2.332248468052" Y="-28.52586392795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.488153056039" Y="-23.917189309181" />
                  <Point X="4.261608185795" Y="-24.426017418689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.780091872963" Y="-25.507520764545" />
                  <Point X="3.592123450381" Y="-25.929704753997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.153937349496" Y="-26.913886850397" />
                  <Point X="3.027438204967" Y="-27.198008580878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44055154282" Y="-28.516177606174" />
                  <Point X="2.398055507531" Y="-28.611625264178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.598618533626" Y="-23.902646151154" />
                  <Point X="4.379685195147" Y="-24.394378480413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.872998669185" Y="-25.532415050565" />
                  <Point X="3.69035599208" Y="-25.942637219825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.231447472784" Y="-26.973362630024" />
                  <Point X="3.110163767397" Y="-27.245770292398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.49926479009" Y="-28.617871860571" />
                  <Point X="2.46386254701" Y="-28.697386600406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.706161299608" Y="-23.89466751087" />
                  <Point X="4.497762204498" Y="-24.362739542137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965905465407" Y="-25.557309336584" />
                  <Point X="3.78858853378" Y="-25.955569685654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.308957596071" Y="-27.032838409651" />
                  <Point X="3.192889329827" Y="-27.293532003917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.557978037359" Y="-28.719566114968" />
                  <Point X="2.529669586489" Y="-28.783147936634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.742921433914" Y="-24.045669264286" />
                  <Point X="4.615839213849" Y="-24.331100603861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.058812261628" Y="-25.582203622604" />
                  <Point X="3.886821075479" Y="-25.968502151482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386467719358" Y="-27.092314189278" />
                  <Point X="3.275614892257" Y="-27.341293715437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.616691284629" Y="-28.821260369364" />
                  <Point X="2.595476625968" Y="-28.868909272863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.772472264256" Y="-24.212863379518" />
                  <Point X="4.7339162232" Y="-24.299461665584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.15171905785" Y="-25.607097908624" />
                  <Point X="3.985053617179" Y="-25.981434617311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463977842645" Y="-27.151789968904" />
                  <Point X="3.358340454687" Y="-27.389055426957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.675404531898" Y="-28.922954623761" />
                  <Point X="2.661283665447" Y="-28.954670609091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.244625854072" Y="-25.631992194644" />
                  <Point X="4.083286158878" Y="-25.994367083139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.541487965932" Y="-27.211265748531" />
                  <Point X="3.441066017117" Y="-27.436817138476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734117779168" Y="-29.024648878158" />
                  <Point X="2.731357692315" Y="-29.030848134729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.337532650294" Y="-25.656886480664" />
                  <Point X="4.181518700578" Y="-26.007299548968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.618998089219" Y="-27.270741528158" />
                  <Point X="3.523791579547" Y="-27.484578849996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.430439446516" Y="-25.681780766683" />
                  <Point X="4.279751242277" Y="-26.020232014796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696508212506" Y="-27.330217307785" />
                  <Point X="3.606517141978" Y="-27.532340561516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523346242737" Y="-25.706675052703" />
                  <Point X="4.377983783977" Y="-26.033164480625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.774018335794" Y="-27.389693087411" />
                  <Point X="3.689242704408" Y="-27.580102273035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.616253038959" Y="-25.731569338723" />
                  <Point X="4.476216325676" Y="-26.046096946453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.851528459081" Y="-27.449168867038" />
                  <Point X="3.771968266838" Y="-27.627863984555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.709159835181" Y="-25.756463624743" />
                  <Point X="4.574448867376" Y="-26.059029412282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.929038582368" Y="-27.508644646665" />
                  <Point X="3.854693829268" Y="-27.675625696075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.775025745851" Y="-25.842092734111" />
                  <Point X="4.672681409075" Y="-26.07196187811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.006548705655" Y="-27.568120426292" />
                  <Point X="3.937419391698" Y="-27.723387407594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.084058828942" Y="-27.627596205918" />
                  <Point X="4.072235033785" Y="-27.65415288465" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998375" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.850241394043" Y="-29.95044921875" />
                  <Point X="0.4715390625" Y="-28.537111328125" />
                  <Point X="0.464319000244" Y="-28.521544921875" />
                  <Point X="0.309533843994" Y="-28.298529296875" />
                  <Point X="0.300591369629" Y="-28.28564453125" />
                  <Point X="0.274336242676" Y="-28.266400390625" />
                  <Point X="0.034815158844" Y="-28.1920625" />
                  <Point X="0.020977140427" Y="-28.187767578125" />
                  <Point X="-0.00866515255" Y="-28.187767578125" />
                  <Point X="-0.248186248779" Y="-28.26210546875" />
                  <Point X="-0.262024261475" Y="-28.266400390625" />
                  <Point X="-0.288277404785" Y="-28.285640625" />
                  <Point X="-0.443062591553" Y="-28.508658203125" />
                  <Point X="-0.459227081299" Y="-28.537111328125" />
                  <Point X="-0.481106109619" Y="-28.618765625" />
                  <Point X="-0.84774395752" Y="-29.987076171875" />
                  <Point X="-1.085347412109" Y="-29.94095703125" />
                  <Point X="-1.100251586914" Y="-29.938064453125" />
                  <Point X="-1.351589599609" Y="-29.8733984375" />
                  <Point X="-1.309150268555" Y="-29.5510390625" />
                  <Point X="-1.309683105469" Y="-29.534759765625" />
                  <Point X="-1.366904663086" Y="-29.247087890625" />
                  <Point X="-1.370210571289" Y="-29.230466796875" />
                  <Point X="-1.38628112793" Y="-29.202630859375" />
                  <Point X="-1.606801757812" Y="-29.009240234375" />
                  <Point X="-1.619541992188" Y="-28.99806640625" />
                  <Point X="-1.649243164062" Y="-28.985763671875" />
                  <Point X="-1.941923217773" Y="-28.966580078125" />
                  <Point X="-1.958832275391" Y="-28.96547265625" />
                  <Point X="-1.989877685547" Y="-28.973791015625" />
                  <Point X="-2.233754150391" Y="-29.136744140625" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.272017333984" Y="-29.173302734375" />
                  <Point X="-2.457094726562" Y="-29.414501953125" />
                  <Point X="-2.834002197266" Y="-29.18112890625" />
                  <Point X="-2.855835693359" Y="-29.167611328125" />
                  <Point X="-3.228580810547" Y="-28.880609375" />
                  <Point X="-3.209809082031" Y="-28.848095703125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.597591796875" />
                  <Point X="-2.51398046875" Y="-27.568763671875" />
                  <Point X="-2.530381591797" Y="-27.55236328125" />
                  <Point X="-2.531332763672" Y="-27.551412109375" />
                  <Point X="-2.560160400391" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.662108154297" Y="-27.584130859375" />
                  <Point X="-3.842958740234" Y="-28.26589453125" />
                  <Point X="-4.144454101563" Y="-27.86979296875" />
                  <Point X="-4.161701660156" Y="-27.8471328125" />
                  <Point X="-4.43101953125" Y="-27.39552734375" />
                  <Point X="-4.395165527344" Y="-27.368015625" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138538330078" Y="-26.367892578125" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140326416016" Y="-26.334595703125" />
                  <Point X="-3.161159912109" Y="-26.310638671875" />
                  <Point X="-3.186196044922" Y="-26.295904296875" />
                  <Point X="-3.187637207031" Y="-26.295056640625" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.30843359375" Y="-26.300275390625" />
                  <Point X="-4.803283691406" Y="-26.497076171875" />
                  <Point X="-4.920646484375" Y="-26.03760546875" />
                  <Point X="-4.927392089844" Y="-26.011197265625" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-4.960189453125" Y="-25.504505859375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541894775391" Y="-25.12142578125" />
                  <Point X="-3.515657714844" Y="-25.10321484375" />
                  <Point X="-3.514166992188" Y="-25.1021796875" />
                  <Point X="-3.494898925781" Y="-25.075908203125" />
                  <Point X="-3.486155029297" Y="-25.047734375" />
                  <Point X="-3.485649414063" Y="-25.046107421875" />
                  <Point X="-3.485647949219" Y="-25.0164609375" />
                  <Point X="-3.494390136719" Y="-24.98829296875" />
                  <Point X="-3.494895263672" Y="-24.9866640625" />
                  <Point X="-3.514143798828" Y="-24.960396484375" />
                  <Point X="-3.540368652344" Y="-24.9421953125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.638504394531" Y="-24.91219921875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.9219921875" Y="-24.032958984375" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.773516113281" Y="-23.471701171875" />
                  <Point X="-4.758774414062" Y="-23.473642578125" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731705566406" Y="-23.6041328125" />
                  <Point X="-3.673634521484" Y="-23.58582421875" />
                  <Point X="-3.670268798828" Y="-23.584763671875" />
                  <Point X="-3.651530517578" Y="-23.57394140625" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.615818603516" Y="-23.4999609375" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.644431396484" Y="-23.400478515625" />
                  <Point X="-3.646058837891" Y="-23.397353515625" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.706453369141" Y="-23.345109375" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.17690625" Y="-22.24193359375" />
                  <Point X="-4.160012207031" Y="-22.212990234375" />
                  <Point X="-3.774671386719" Y="-21.717689453125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514892578" Y="-22.07956640625" />
                  <Point X="-3.057637939453" Y="-22.086642578125" />
                  <Point X="-3.052959472656" Y="-22.08705078125" />
                  <Point X="-3.031504638672" Y="-22.084224609375" />
                  <Point X="-3.013253662109" Y="-22.07259765625" />
                  <Point X="-2.955846679688" Y="-22.01519140625" />
                  <Point X="-2.952530029297" Y="-22.011875" />
                  <Point X="-2.940900146484" Y="-21.99362109375" />
                  <Point X="-2.93807421875" Y="-21.972162109375" />
                  <Point X="-2.945150146484" Y="-21.89128515625" />
                  <Point X="-2.945558837891" Y="-21.886611328125" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-2.972666259766" Y="-21.8302890625" />
                  <Point X="-3.307278320312" Y="-21.250724609375" />
                  <Point X="-2.782293701172" Y="-20.848224609375" />
                  <Point X="-2.752872314453" Y="-20.825666015625" />
                  <Point X="-2.141547851563" Y="-20.486029296875" />
                  <Point X="-1.967826904297" Y="-20.71242578125" />
                  <Point X="-1.951245605469" Y="-20.72633984375" />
                  <Point X="-1.861229980469" Y="-20.773201171875" />
                  <Point X="-1.856029541016" Y="-20.775908203125" />
                  <Point X="-1.835121826172" Y="-20.781509765625" />
                  <Point X="-1.813805664062" Y="-20.77775" />
                  <Point X="-1.720048461914" Y="-20.7389140625" />
                  <Point X="-1.714631713867" Y="-20.736669921875" />
                  <Point X="-1.696904052734" Y="-20.72425390625" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.655566894531" Y="-20.6087265625" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917724609" Y="-20.58157421875" />
                  <Point X="-1.654259521484" Y="-20.563787109375" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.006178405762" Y="-20.1073828125" />
                  <Point X="-0.968083190918" Y="-20.096703125" />
                  <Point X="-0.224463867188" Y="-20.009671875" />
                  <Point X="-0.224200134277" Y="-20.009640625" />
                  <Point X="-0.042140319824" Y="-20.68909765625" />
                  <Point X="-0.024282102585" Y="-20.71582421875" />
                  <Point X="0.006155994415" Y="-20.72615625" />
                  <Point X="0.036594051361" Y="-20.71582421875" />
                  <Point X="0.054452308655" Y="-20.68909765625" />
                  <Point X="0.065006896973" Y="-20.64970703125" />
                  <Point X="0.236648422241" Y="-20.0091328125" />
                  <Point X="0.826950561523" Y="-20.070953125" />
                  <Point X="0.860208496094" Y="-20.074435546875" />
                  <Point X="1.475428344727" Y="-20.22296875" />
                  <Point X="1.508455810547" Y="-20.230943359375" />
                  <Point X="1.90980078125" Y="-20.376513671875" />
                  <Point X="1.931037719727" Y="-20.384216796875" />
                  <Point X="2.318248046875" Y="-20.56530078125" />
                  <Point X="2.338689941406" Y="-20.574861328125" />
                  <Point X="2.712760253906" Y="-20.792794921875" />
                  <Point X="2.732522705078" Y="-20.80430859375" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="3.044420898438" Y="-21.08553125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852294922" Y="-22.508484375" />
                  <Point X="2.204555175781" Y="-22.58438671875" />
                  <Point X="2.202044433594" Y="-22.607673828125" />
                  <Point X="2.209958740234" Y="-22.673306640625" />
                  <Point X="2.210415771484" Y="-22.67709765625" />
                  <Point X="2.218681884766" Y="-22.6991875" />
                  <Point X="2.259293212891" Y="-22.7590390625" />
                  <Point X="2.259297119141" Y="-22.759044921875" />
                  <Point X="2.274939941406" Y="-22.775796875" />
                  <Point X="2.334779296875" Y="-22.816400390625" />
                  <Point X="2.338236816406" Y="-22.818748046875" />
                  <Point X="2.360339355469" Y="-22.827021484375" />
                  <Point X="2.425972412109" Y="-22.834935546875" />
                  <Point X="2.448664306641" Y="-22.8340546875" />
                  <Point X="2.524565673828" Y="-22.8137578125" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.622544921875" Y="-22.760521484375" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.190868652344" Y="-22.241828125" />
                  <Point X="4.202590332031" Y="-22.258119140625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.3610703125" Y="-22.583994140625" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371582031" Y="-23.41616796875" />
                  <Point X="3.224745361328" Y="-23.487431640625" />
                  <Point X="3.213119873047" Y="-23.5085" />
                  <Point X="3.192771484375" Y="-23.58126171875" />
                  <Point X="3.191595947266" Y="-23.58546484375" />
                  <Point X="3.190779296875" Y="-23.60903515625" />
                  <Point X="3.207483154297" Y="-23.689990234375" />
                  <Point X="3.215645507812" Y="-23.712044921875" />
                  <Point X="3.261078125" Y="-23.781099609375" />
                  <Point X="3.263703125" Y="-23.78508984375" />
                  <Point X="3.280948242188" Y="-23.801181640625" />
                  <Point X="3.346786621094" Y="-23.8382421875" />
                  <Point X="3.346786865234" Y="-23.8382421875" />
                  <Point X="3.368565429688" Y="-23.846380859375" />
                  <Point X="3.457583251953" Y="-23.85814453125" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.553233886719" Y="-23.848634765625" />
                  <Point X="4.848975097656" Y="-23.678046875" />
                  <Point X="4.934153320312" Y="-24.027935546875" />
                  <Point X="4.939189941406" Y="-24.04862109375" />
                  <Point X="4.997858886719" Y="-24.425443359375" />
                  <Point X="4.970223144531" Y="-24.43284765625" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729091064453" Y="-24.7671796875" />
                  <Point X="3.641633789062" Y="-24.81773046875" />
                  <Point X="3.622263183594" Y="-24.83307421875" />
                  <Point X="3.569788818359" Y="-24.899939453125" />
                  <Point X="3.566757324219" Y="-24.903802734375" />
                  <Point X="3.556985351562" Y="-24.925263671875" />
                  <Point X="3.539493896484" Y="-25.01659765625" />
                  <Point X="3.538483886719" Y="-25.040689453125" />
                  <Point X="3.555975341797" Y="-25.1320234375" />
                  <Point X="3.556986083984" Y="-25.137298828125" />
                  <Point X="3.566757324219" Y="-25.1587578125" />
                  <Point X="3.619231689453" Y="-25.225623046875" />
                  <Point X="3.636577392578" Y="-25.241908203125" />
                  <Point X="3.724034667969" Y="-25.292458984375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="3.812174804688" Y="-25.3194140625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.951243164062" Y="-25.947751953125" />
                  <Point X="4.948430664062" Y="-25.96640625" />
                  <Point X="4.874546386719" Y="-26.2901796875" />
                  <Point X="4.837340820312" Y="-26.28528125" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.3948359375" Y="-26.098341796875" />
                  <Point X="3.223188232422" Y="-26.135650390625" />
                  <Point X="3.213277099609" Y="-26.1378046875" />
                  <Point X="3.185444824219" Y="-26.154697265625" />
                  <Point X="3.081694580078" Y="-26.2794765625" />
                  <Point X="3.075700683594" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.049488037109" Y="-26.475666015625" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056362060547" Y="-26.516625" />
                  <Point X="3.151354248047" Y="-26.66437890625" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.234355712891" Y="-26.736103515625" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.2120546875" Y="-27.789322265625" />
                  <Point X="4.204137695313" Y="-27.802134765625" />
                  <Point X="4.0566875" Y="-28.011638671875" />
                  <Point X="4.022928955078" Y="-27.9921484375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737338378906" Y="-27.2533125" />
                  <Point X="2.533050292969" Y="-27.21641796875" />
                  <Point X="2.521248046875" Y="-27.214287109375" />
                  <Point X="2.489078125" Y="-27.21924609375" />
                  <Point X="2.319364990234" Y="-27.308564453125" />
                  <Point X="2.309560058594" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.199281005859" Y="-27.504396484375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.226057128906" Y="-27.7506640625" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.276435791016" Y="-27.851921875" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.844703369141" Y="-29.183494140625" />
                  <Point X="2.835274169922" Y="-29.190228515625" />
                  <Point X="2.679775634766" Y="-29.290880859375" />
                  <Point X="2.651903564453" Y="-29.254556640625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670546020508" Y="-27.98046484375" />
                  <Point X="1.469062988281" Y="-27.8509296875" />
                  <Point X="1.457422485352" Y="-27.843447265625" />
                  <Point X="1.42580480957" Y="-27.83571875" />
                  <Point X="1.205455078125" Y="-27.85599609375" />
                  <Point X="1.192724121094" Y="-27.8571640625" />
                  <Point X="1.165332397461" Y="-27.868509765625" />
                  <Point X="0.995179016113" Y="-28.00998828125" />
                  <Point X="0.985348632813" Y="-28.018162109375" />
                  <Point X="0.96845703125" Y="-28.045986328125" />
                  <Point X="0.91758190918" Y="-28.28005078125" />
                  <Point X="0.913929443359" Y="-28.31071875" />
                  <Point X="0.925929443359" Y="-28.401869140625" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.00324407959" Y="-29.961296875" />
                  <Point X="0.994339538574" Y="-29.963248046875" />
                  <Point X="0.860200439453" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#212" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.18268991153" Y="5.036932540226" Z="2.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.4" />
                  <Point X="-0.236432156461" Y="5.071268555169" Z="2.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.4" />
                  <Point X="-1.025832072416" Y="4.97204254565" Z="2.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.4" />
                  <Point X="-1.709987757464" Y="4.460968804913" Z="2.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.4" />
                  <Point X="-1.709408924438" Y="4.437588953684" Z="2.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.4" />
                  <Point X="-1.745341835888" Y="4.338560414803" Z="2.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.4" />
                  <Point X="-1.84429957467" Y="4.302431820138" Z="2.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.4" />
                  <Point X="-2.123367513053" Y="4.595669203726" Z="2.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.4" />
                  <Point X="-2.169913915662" Y="4.590111323244" Z="2.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.4" />
                  <Point X="-2.818872437545" Y="4.222736520606" Z="2.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.4" />
                  <Point X="-3.02212360544" Y="3.17599128166" Z="2.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.4" />
                  <Point X="-3.001115888145" Y="3.135640367216" Z="2.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.4" />
                  <Point X="-2.997356616673" Y="3.051446951214" Z="2.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.4" />
                  <Point X="-3.05943609839" Y="2.994448811028" Z="2.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.4" />
                  <Point X="-3.757868066637" Y="3.358070343305" Z="2.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.4" />
                  <Point X="-3.81616537674" Y="3.34959580224" Z="2.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.4" />
                  <Point X="-4.226244446013" Y="2.814551610882" Z="2.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.4" />
                  <Point X="-3.743047307239" Y="1.646503325818" Z="2.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.4" />
                  <Point X="-3.694937955841" Y="1.607713804039" Z="2.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.4" />
                  <Point X="-3.66816863159" Y="1.550454371025" Z="2.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.4" />
                  <Point X="-3.694824887816" Y="1.493142290329" Z="2.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.4" />
                  <Point X="-4.758404097995" Y="1.607210217955" Z="2.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.4" />
                  <Point X="-4.825034608714" Y="1.583347681407" Z="2.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.4" />
                  <Point X="-4.977609652661" Y="1.005639324835" Z="2.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.4" />
                  <Point X="-3.657601226934" Y="0.070783766587" Z="2.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.4" />
                  <Point X="-3.57504492644" Y="0.048016973882" Z="2.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.4" />
                  <Point X="-3.548302580985" Y="0.028178952145" Z="2.4" />
                  <Point X="-3.539556741714" Y="0" Z="2.4" />
                  <Point X="-3.54006202015" Y="-0.001627998916" Z="2.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.4" />
                  <Point X="-3.550323668683" Y="-0.030859009252" Z="2.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.4" />
                  <Point X="-4.979286561839" Y="-0.424928290169" Z="2.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.4" />
                  <Point X="-5.056085120672" Y="-0.476302168585" Z="2.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.4" />
                  <Point X="-4.975246858762" Y="-1.018699734387" Z="2.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.4" />
                  <Point X="-3.308063189821" Y="-1.318567779107" Z="2.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.4" />
                  <Point X="-3.217712459651" Y="-1.307714612656" Z="2.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.4" />
                  <Point X="-3.193097110694" Y="-1.324074547895" Z="2.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.4" />
                  <Point X="-4.43175944899" Y="-2.29706719851" Z="2.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.4" />
                  <Point X="-4.486867691943" Y="-2.378540461854" Z="2.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.4" />
                  <Point X="-4.1903960628" Y="-2.868795069957" Z="2.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.4" />
                  <Point X="-2.643264037996" Y="-2.596150596871" Z="2.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.4" />
                  <Point X="-2.571891954956" Y="-2.556438514175" Z="2.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.4" />
                  <Point X="-3.259266416298" Y="-3.791814344497" Z="2.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.4" />
                  <Point X="-3.277562634128" Y="-3.879457963605" Z="2.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.4" />
                  <Point X="-2.86647862735" Y="-4.192360430116" Z="2.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.4" />
                  <Point X="-2.238505639467" Y="-4.172460163397" Z="2.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.4" />
                  <Point X="-2.212132649631" Y="-4.147037777288" Z="2.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.4" />
                  <Point X="-1.951346826908" Y="-3.985192654499" Z="2.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.4" />
                  <Point X="-1.645926584536" Y="-4.015548862008" Z="2.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.4" />
                  <Point X="-1.422100085413" Y="-4.225560340652" Z="2.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.4" />
                  <Point X="-1.410465337788" Y="-4.859498224429" Z="2.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.4" />
                  <Point X="-1.396948631784" Y="-4.883658612471" Z="2.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.4" />
                  <Point X="-1.101030951122" Y="-4.958761219271" Z="2.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.4" />
                  <Point X="-0.438965768003" Y="-3.600426579333" Z="2.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.4" />
                  <Point X="-0.408144284789" Y="-3.505888648101" Z="2.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.4" />
                  <Point X="-0.23952108883" Y="-3.278577875258" Z="2.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.4" />
                  <Point X="0.01383799053" Y="-3.20853417003" Z="2.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.4" />
                  <Point X="0.262301574644" Y="-3.295757107667" Z="2.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.4" />
                  <Point X="0.795789092398" Y="-4.932109450482" Z="2.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.4" />
                  <Point X="0.827518021279" Y="-5.011973576113" Z="2.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.4" />
                  <Point X="1.007789136268" Y="-4.978857830813" Z="2.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.4" />
                  <Point X="0.96934577217" Y="-3.364062645198" Z="2.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.4" />
                  <Point X="0.960285014622" Y="-3.259390926651" Z="2.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.4" />
                  <Point X="1.020990384989" Y="-3.017152384156" Z="2.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.4" />
                  <Point X="1.203874503196" Y="-2.874503944451" Z="2.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.4" />
                  <Point X="1.435870852908" Y="-2.861710288658" Z="2.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.4" />
                  <Point X="2.606079902576" Y="-4.253713321856" Z="2.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.4" />
                  <Point X="2.672709592911" Y="-4.319748715576" Z="2.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.4" />
                  <Point X="2.867609453893" Y="-4.19290132919" Z="2.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.4" />
                  <Point X="2.313580979555" Y="-2.795640694662" Z="2.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.4" />
                  <Point X="2.269105382521" Y="-2.710496173684" Z="2.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.4" />
                  <Point X="2.237371380661" Y="-2.496403250868" Z="2.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.4" />
                  <Point X="2.336495127504" Y="-2.321529929306" Z="2.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.4" />
                  <Point X="2.518010629829" Y="-2.23434262697" Z="2.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.4" />
                  <Point X="3.991772902444" Y="-3.00416852072" Z="2.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.4" />
                  <Point X="4.074651691996" Y="-3.032962242477" Z="2.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.4" />
                  <Point X="4.248433089604" Y="-2.784324266097" Z="2.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.4" />
                  <Point X="3.258637685187" Y="-1.665155940725" Z="2.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.4" />
                  <Point X="3.187254780891" Y="-1.60605673778" Z="2.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.4" />
                  <Point X="3.093121591814" Y="-1.448966612158" Z="2.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.4" />
                  <Point X="3.113985469248" Y="-1.280163295571" Z="2.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.4" />
                  <Point X="3.227652227018" Y="-1.15322874758" Z="2.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.4" />
                  <Point X="4.824658661359" Y="-1.303572456453" Z="2.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.4" />
                  <Point X="4.911618236444" Y="-1.294205581716" Z="2.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.4" />
                  <Point X="4.994528502033" Y="-0.923926713606" Z="2.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.4" />
                  <Point X="3.818959633672" Y="-0.239836886629" Z="2.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.4" />
                  <Point X="3.742899980417" Y="-0.217890074587" Z="2.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.4" />
                  <Point X="3.652411083145" Y="-0.163475196355" Z="2.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.4" />
                  <Point X="3.59892617986" Y="-0.09133384794" Z="2.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.4" />
                  <Point X="3.582445270564" Y="0.005276683236" Z="2.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.4" />
                  <Point X="3.602968355256" Y="0.10047354222" Z="2.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.4" />
                  <Point X="3.660495433935" Y="0.170258769655" Z="2.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.4" />
                  <Point X="4.977008183635" Y="0.550135046139" Z="2.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.4" />
                  <Point X="5.044415663147" Y="0.592280004557" Z="2.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.4" />
                  <Point X="4.976578573803" Y="1.015173865753" Z="2.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.4" />
                  <Point X="3.540551525485" Y="1.232217953817" Z="2.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.4" />
                  <Point X="3.457978530828" Y="1.222703777824" Z="2.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.4" />
                  <Point X="3.365156901871" Y="1.236609742565" Z="2.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.4" />
                  <Point X="3.2966937398" Y="1.277660630194" Z="2.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.4" />
                  <Point X="3.250295950775" Y="1.351393504514" Z="2.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.4" />
                  <Point X="3.23476763069" Y="1.436552859337" Z="2.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.4" />
                  <Point X="3.258271938154" Y="1.51343089103" Z="2.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.4" />
                  <Point X="4.385352360735" Y="2.407618124804" Z="2.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.4" />
                  <Point X="4.435889710313" Y="2.474036585782" Z="2.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.4" />
                  <Point X="4.225298320337" Y="2.818655531677" Z="2.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.4" />
                  <Point X="2.59138905138" Y="2.314059238726" Z="2.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.4" />
                  <Point X="2.50549283725" Y="2.265826139558" Z="2.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.4" />
                  <Point X="2.425799801643" Y="2.245986402175" Z="2.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.4" />
                  <Point X="2.35670898658" Y="2.256246787236" Z="2.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.4" />
                  <Point X="2.294511812999" Y="2.300315873802" Z="2.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.4" />
                  <Point X="2.253443300584" Y="2.363958642189" Z="2.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.4" />
                  <Point X="2.2467018031" Y="2.433976738632" Z="2.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.4" />
                  <Point X="3.081566114134" Y="3.920750030575" Z="2.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.4" />
                  <Point X="3.108137767126" Y="4.016831677149" Z="2.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.4" />
                  <Point X="2.731774304051" Y="4.281688183243" Z="2.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.4" />
                  <Point X="2.333274730504" Y="4.511270283326" Z="2.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.4" />
                  <Point X="1.92069327249" Y="4.701771298796" Z="2.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.4" />
                  <Point X="1.481007573357" Y="4.856915338043" Z="2.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.4" />
                  <Point X="0.82600200102" Y="5.010056428966" Z="2.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.4" />
                  <Point X="0.010554650023" Y="4.394514965725" Z="2.4" />
                  <Point X="0" Y="4.355124473572" Z="2.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>