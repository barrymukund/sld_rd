<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#145" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1208" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.673898681641" Y="-28.92527734375" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542362854004" Y="-28.467375" />
                  <Point X="0.497159240723" Y="-28.40224609375" />
                  <Point X="0.378635223389" Y="-28.231474609375" />
                  <Point X="0.356748565674" Y="-28.209017578125" />
                  <Point X="0.330493225098" Y="-28.189775390625" />
                  <Point X="0.302494262695" Y="-28.175669921875" />
                  <Point X="0.232544082642" Y="-28.1539609375" />
                  <Point X="0.049135128021" Y="-28.097037109375" />
                  <Point X="0.02098301506" Y="-28.092767578125" />
                  <Point X="-0.008658161163" Y="-28.092765625" />
                  <Point X="-0.036825149536" Y="-28.09703515625" />
                  <Point X="-0.106775177002" Y="-28.11874609375" />
                  <Point X="-0.290184143066" Y="-28.17566796875" />
                  <Point X="-0.318184143066" Y="-28.189775390625" />
                  <Point X="-0.34443963623" Y="-28.20901953125" />
                  <Point X="-0.366323730469" Y="-28.2314765625" />
                  <Point X="-0.411527374268" Y="-28.296607421875" />
                  <Point X="-0.530051696777" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.840974609375" Y="-29.59476171875" />
                  <Point X="-0.916584899902" Y="-29.87694140625" />
                  <Point X="-1.079319091797" Y="-29.845353515625" />
                  <Point X="-1.156755371094" Y="-29.825431640625" />
                  <Point X="-1.24641784668" Y="-29.802361328125" />
                  <Point X="-1.228276367188" Y="-29.664564453125" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.233219604492" Y="-29.432212890625" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.32364453125" Y="-29.131205078125" />
                  <Point X="-1.388045654297" Y="-29.0747265625" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886108398" Y="-28.897994140625" />
                  <Point X="-1.643028198242" Y="-28.890966796875" />
                  <Point X="-1.728502685547" Y="-28.885365234375" />
                  <Point X="-1.9526171875" Y="-28.87067578125" />
                  <Point X="-1.98341418457" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042656982422" Y="-28.89480078125" />
                  <Point X="-2.11387890625" Y="-28.942388671875" />
                  <Point X="-2.300623291016" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102539062" Y="-29.099462890625" />
                  <Point X="-2.480147949219" Y="-29.288490234375" />
                  <Point X="-2.489932128906" Y="-29.282431640625" />
                  <Point X="-2.801723632812" Y="-29.08937890625" />
                  <Point X="-2.908903564453" Y="-29.006853515625" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.629292724609" Y="-28.032611328125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412859130859" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616126953125" />
                  <Point X="-2.405575439453" Y="-27.585193359375" />
                  <Point X="-2.414559570312" Y="-27.555576171875" />
                  <Point X="-2.428776855469" Y="-27.52674609375" />
                  <Point X="-2.446806640625" Y="-27.5015859375" />
                  <Point X="-2.464155273438" Y="-27.48423828125" />
                  <Point X="-2.489311279297" Y="-27.466212890625" />
                  <Point X="-2.518140136719" Y="-27.45199609375" />
                  <Point X="-2.547758300781" Y="-27.44301171875" />
                  <Point X="-2.578692626953" Y="-27.444025390625" />
                  <Point X="-2.61021875" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.572592041016" Y="-28.000103515625" />
                  <Point X="-3.8180234375" Y="-28.141802734375" />
                  <Point X="-3.836539550781" Y="-28.1174765625" />
                  <Point X="-4.082862304687" Y="-27.793857421875" />
                  <Point X="-4.159708007812" Y="-27.665" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.465567871094" Y="-26.774455078125" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083060302734" Y="-26.475875" />
                  <Point X="-3.063723388672" Y="-26.444806640625" />
                  <Point X="-3.054882568359" Y="-26.421525390625" />
                  <Point X="-3.051728759766" Y="-26.411619140625" />
                  <Point X="-3.046151367188" Y="-26.390083984375" />
                  <Point X="-3.042037353516" Y="-26.3586015625" />
                  <Point X="-3.042736816406" Y="-26.335732421875" />
                  <Point X="-3.048884033203" Y="-26.313693359375" />
                  <Point X="-3.060889160156" Y="-26.284712890625" />
                  <Point X="-3.073729248047" Y="-26.26266796875" />
                  <Point X="-3.09197265625" Y="-26.244833984375" />
                  <Point X="-3.111786621094" Y="-26.2301015625" />
                  <Point X="-3.120284179688" Y="-26.22446484375" />
                  <Point X="-3.139455078125" Y="-26.213181640625" />
                  <Point X="-3.168716064453" Y="-26.201958984375" />
                  <Point X="-3.200603515625" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.410268554688" Y="-26.349515625" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.737739257812" Y="-26.369814453125" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.854409179688" Y="-25.850498046875" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.942529785156" Y="-25.33017578125" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.512338378906" Y="-25.212244140625" />
                  <Point X="-3.489108886719" Y="-25.199705078125" />
                  <Point X="-3.480068847656" Y="-25.19415234375" />
                  <Point X="-3.459978271484" Y="-25.180208984375" />
                  <Point X="-3.436022460938" Y="-25.15868359375" />
                  <Point X="-3.415195556641" Y="-25.1285078125" />
                  <Point X="-3.405215576172" Y="-25.105541015625" />
                  <Point X="-3.401614746094" Y="-25.09583984375" />
                  <Point X="-3.39491796875" Y="-25.074263671875" />
                  <Point X="-3.389474365234" Y="-25.045521484375" />
                  <Point X="-3.390396728516" Y="-25.01191015625" />
                  <Point X="-3.395197753906" Y="-24.988859375" />
                  <Point X="-3.397471191406" Y="-24.9800703125" />
                  <Point X="-3.404168212891" Y="-24.9584921875" />
                  <Point X="-3.417486816406" Y="-24.9291640625" />
                  <Point X="-3.440219726562" Y="-24.9000234375" />
                  <Point X="-3.459704589844" Y="-24.883189453125" />
                  <Point X="-3.467641601562" Y="-24.877033203125" />
                  <Point X="-3.487724121094" Y="-24.86309375" />
                  <Point X="-3.501916259766" Y="-24.854958984375" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.60698828125" Y="-24.55434375" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.886576171875" Y="-24.442611328125" />
                  <Point X="-4.82448828125" Y="-24.023029296875" />
                  <Point X="-4.783559570312" Y="-23.87198828125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.059316162109" Y="-23.661548828125" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985351562" Y="-23.700658203125" />
                  <Point X="-3.723422851562" Y="-23.698771484375" />
                  <Point X="-3.703143554688" Y="-23.69473828125" />
                  <Point X="-3.686184570312" Y="-23.689392578125" />
                  <Point X="-3.641717529297" Y="-23.67537109375" />
                  <Point X="-3.622775634766" Y="-23.667037109375" />
                  <Point X="-3.604031982422" Y="-23.65621484375" />
                  <Point X="-3.587352539062" Y="-23.643984375" />
                  <Point X="-3.573715332031" Y="-23.62843359375" />
                  <Point X="-3.561301269531" Y="-23.610705078125" />
                  <Point X="-3.551352050781" Y="-23.592572265625" />
                  <Point X="-3.544547119141" Y="-23.57614453125" />
                  <Point X="-3.526704589844" Y="-23.533068359375" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532049804688" Y="-23.410623046875" />
                  <Point X="-3.540260742188" Y="-23.394849609375" />
                  <Point X="-3.561789550781" Y="-23.353494140625" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.218249023438" Y="-22.8326484375" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.322404785156" Y="-22.679662109375" />
                  <Point X="-4.081156738281" Y="-22.26634765625" />
                  <Point X="-3.972738037109" Y="-22.12698828125" />
                  <Point X="-3.750504638672" Y="-21.84133984375" />
                  <Point X="-3.387008789062" Y="-22.051203125" />
                  <Point X="-3.206657226562" Y="-22.155330078125" />
                  <Point X="-3.187729003906" Y="-22.163658203125" />
                  <Point X="-3.167086425781" Y="-22.17016796875" />
                  <Point X="-3.146794433594" Y="-22.174205078125" />
                  <Point X="-3.123175048828" Y="-22.176271484375" />
                  <Point X="-3.061245117188" Y="-22.181689453125" />
                  <Point X="-3.040560546875" Y="-22.18123828125" />
                  <Point X="-3.019102050781" Y="-22.178412109375" />
                  <Point X="-2.999012939453" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946077392578" Y="-22.13976953125" />
                  <Point X="-2.929312011719" Y="-22.12300390625" />
                  <Point X="-2.885353759766" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.845502197266" Y="-21.94026171875" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.142813476562" Y="-21.3455859375" />
                  <Point X="-3.183333496094" Y="-21.275404296875" />
                  <Point X="-3.120804931641" Y="-21.227462890625" />
                  <Point X="-2.700629150391" Y="-20.905318359375" />
                  <Point X="-2.529877197266" Y="-20.810453125" />
                  <Point X="-2.167036376953" Y="-20.608865234375" />
                  <Point X="-2.098406494141" Y="-20.698306640625" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028892333984" Y="-20.78519921875" />
                  <Point X="-2.012312988281" Y="-20.799111328125" />
                  <Point X="-1.995115234375" Y="-20.810603515625" />
                  <Point X="-1.968827026367" Y="-20.8242890625" />
                  <Point X="-1.899899169922" Y="-20.860171875" />
                  <Point X="-1.880625244141" Y="-20.86766796875" />
                  <Point X="-1.859718383789" Y="-20.873271484375" />
                  <Point X="-1.839268676758" Y="-20.876419921875" />
                  <Point X="-1.818622436523" Y="-20.87506640625" />
                  <Point X="-1.797306762695" Y="-20.871306640625" />
                  <Point X="-1.77745300293" Y="-20.865517578125" />
                  <Point X="-1.750072021484" Y="-20.85417578125" />
                  <Point X="-1.678279052734" Y="-20.8244375" />
                  <Point X="-1.660145507812" Y="-20.814490234375" />
                  <Point X="-1.642416137695" Y="-20.802076171875" />
                  <Point X="-1.626863769531" Y="-20.7884375" />
                  <Point X="-1.614632324219" Y="-20.771755859375" />
                  <Point X="-1.603810546875" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734078125" />
                  <Point X="-1.586568237305" Y="-20.7058125" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.493583496094" Y="-20.342697265625" />
                  <Point X="-0.949623474121" Y="-20.19019140625" />
                  <Point X="-0.742642028809" Y="-20.16596484375" />
                  <Point X="-0.294711303711" Y="-20.113541015625" />
                  <Point X="-0.187256271362" Y="-20.514568359375" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082114013672" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425964355" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.286106811523" Y="-20.191603515625" />
                  <Point X="0.307419616699" Y="-20.1120625" />
                  <Point X="0.369089111328" Y="-20.118521484375" />
                  <Point X="0.844044921875" Y="-20.16826171875" />
                  <Point X="1.015297180176" Y="-20.209607421875" />
                  <Point X="1.481024291992" Y="-20.322048828125" />
                  <Point X="1.591424560547" Y="-20.362091796875" />
                  <Point X="1.894645263672" Y="-20.472072265625" />
                  <Point X="2.002435913086" Y="-20.522482421875" />
                  <Point X="2.294558349609" Y="-20.659099609375" />
                  <Point X="2.398736816406" Y="-20.71979296875" />
                  <Point X="2.680971435547" Y="-20.88422265625" />
                  <Point X="2.779188964844" Y="-20.9540703125" />
                  <Point X="2.943259033203" Y="-21.070748046875" />
                  <Point X="2.385468261719" Y="-22.036869140625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.4839453125" />
                  <Point X="2.127149414062" Y="-22.506111328125" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108413330078" Y="-22.591203125" />
                  <Point X="2.109386962891" Y="-22.629271484375" />
                  <Point X="2.1100390625" Y="-22.63821484375" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442138672" Y="-22.710396484375" />
                  <Point X="2.129708007812" Y="-22.732484375" />
                  <Point X="2.140071533203" Y="-22.752529296875" />
                  <Point X="2.151931884766" Y="-22.7700078125" />
                  <Point X="2.183029296875" Y="-22.815837890625" />
                  <Point X="2.194462646484" Y="-22.829669921875" />
                  <Point X="2.221594726562" Y="-22.85440625" />
                  <Point X="2.239073486328" Y="-22.866267578125" />
                  <Point X="2.284903320312" Y="-22.897365234375" />
                  <Point X="2.304953369141" Y="-22.90773046875" />
                  <Point X="2.327041748047" Y="-22.91599609375" />
                  <Point X="2.348967285156" Y="-22.921337890625" />
                  <Point X="2.368134765625" Y="-22.9236484375" />
                  <Point X="2.418392089844" Y="-22.929708984375" />
                  <Point X="2.436468261719" Y="-22.93015625" />
                  <Point X="2.473207763672" Y="-22.925830078125" />
                  <Point X="2.495374023438" Y="-22.91990234375" />
                  <Point X="2.553494140625" Y="-22.904359375" />
                  <Point X="2.5652890625" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.668884277344" Y="-22.26611328125" />
                  <Point X="3.967326171875" Y="-22.09380859375" />
                  <Point X="4.123272460938" Y="-22.3105390625" />
                  <Point X="4.178023925781" Y="-22.401015625" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.543984619141" Y="-23.091220703125" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221420410156" Y="-23.339763671875" />
                  <Point X="3.203973632812" Y="-23.358375" />
                  <Point X="3.188020507812" Y="-23.3791875" />
                  <Point X="3.14619140625" Y="-23.433755859375" />
                  <Point X="3.136606933594" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.115687255859" Y="-23.5041640625" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739257812" Y="-23.628232421875" />
                  <Point X="3.102617431641" Y="-23.651875" />
                  <Point X="3.115408203125" Y="-23.713865234375" />
                  <Point X="3.120679931641" Y="-23.7310234375" />
                  <Point X="3.136283691406" Y="-23.76426171875" />
                  <Point X="3.149552001953" Y="-23.784427734375" />
                  <Point X="3.184341308594" Y="-23.837306640625" />
                  <Point X="3.198893798828" Y="-23.854548828125" />
                  <Point X="3.216137939453" Y="-23.870638671875" />
                  <Point X="3.234344726562" Y="-23.883962890625" />
                  <Point X="3.253572265625" Y="-23.894787109375" />
                  <Point X="3.303986816406" Y="-23.923166015625" />
                  <Point X="3.320521240234" Y="-23.930498046875" />
                  <Point X="3.356119873047" Y="-23.9405625" />
                  <Point X="3.382116699219" Y="-23.943998046875" />
                  <Point X="3.450280517578" Y="-23.953005859375" />
                  <Point X="3.462698486328" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.514463867188" Y="-23.81790625" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.863189941406" Y="-24.178009765625" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.075514648438" Y="-24.574232421875" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704787841797" Y="-24.674416015625" />
                  <Point X="3.681549560547" Y="-24.684931640625" />
                  <Point X="3.656008544922" Y="-24.699693359375" />
                  <Point X="3.589039550781" Y="-24.73840234375" />
                  <Point X="3.574308105469" Y="-24.74890625" />
                  <Point X="3.547530517578" Y="-24.774423828125" />
                  <Point X="3.532205810547" Y="-24.793951171875" />
                  <Point X="3.492024658203" Y="-24.84515234375" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.47052734375" Y="-24.88589453125" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.458572265625" Y="-24.9340703125" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.021876953125" />
                  <Point X="3.445178710938" Y="-25.058556640625" />
                  <Point X="3.450287109375" Y="-25.085228515625" />
                  <Point X="3.463680908203" Y="-25.155166015625" />
                  <Point X="3.47052734375" Y="-25.176666015625" />
                  <Point X="3.480301025391" Y="-25.19812890625" />
                  <Point X="3.492024658203" Y="-25.217408203125" />
                  <Point X="3.507349365234" Y="-25.236935546875" />
                  <Point X="3.547530517578" Y="-25.28813671875" />
                  <Point X="3.559995605469" Y="-25.301232421875" />
                  <Point X="3.58903515625" Y="-25.32415625" />
                  <Point X="3.614576416016" Y="-25.338919921875" />
                  <Point X="3.681545166016" Y="-25.37762890625" />
                  <Point X="3.692708251953" Y="-25.383138671875" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.657707519531" Y="-25.644326171875" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.832917480469" Y="-26.04559375" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.840645751953" Y="-26.058244140625" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.37466015625" Y="-26.005509765625" />
                  <Point X="3.324531982422" Y="-26.016404296875" />
                  <Point X="3.193095947266" Y="-26.04497265625" />
                  <Point X="3.163973144531" Y="-26.05659765625" />
                  <Point X="3.136146728516" Y="-26.073490234375" />
                  <Point X="3.112396728516" Y="-26.0939609375" />
                  <Point X="3.082097412109" Y="-26.13040234375" />
                  <Point X="3.002652587891" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.965415039062" Y="-26.35255859375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.507564453125" />
                  <Point X="2.964078613281" Y="-26.539185546875" />
                  <Point X="2.976451171875" Y="-26.568" />
                  <Point X="3.004192871094" Y="-26.6111484375" />
                  <Point X="3.076931396484" Y="-26.7242890625" />
                  <Point X="3.086931396484" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.984000488281" Y="-27.431072265625" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.124801269531" Y="-27.74980078125" />
                  <Point X="4.079094482422" Y="-27.814744140625" />
                  <Point X="4.028981689453" Y="-27.885947265625" />
                  <Point X="3.171693603516" Y="-27.3909921875" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754225585938" Y="-27.159826171875" />
                  <Point X="2.694564941406" Y="-27.14905078125" />
                  <Point X="2.538135253906" Y="-27.12080078125" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.444834228516" Y="-27.135177734375" />
                  <Point X="2.395270996094" Y="-27.16126171875" />
                  <Point X="2.265316162109" Y="-27.22965625" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.178447265625" Y="-27.340001953125" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.563259765625" />
                  <Point X="2.106450195312" Y="-27.622919921875" />
                  <Point X="2.134701171875" Y="-27.7793515625" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.713048583984" Y="-28.79815625" />
                  <Point X="2.861283691406" Y="-29.05490625" />
                  <Point X="2.781840576172" Y="-29.111650390625" />
                  <Point X="2.730737548828" Y="-29.144728515625" />
                  <Point X="2.701764160156" Y="-29.163482421875" />
                  <Point X="2.041454223633" Y="-28.30294921875" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506225586" Y="-27.922181640625" />
                  <Point X="1.721924194336" Y="-27.900556640625" />
                  <Point X="1.663082763672" Y="-27.8627265625" />
                  <Point X="1.50880078125" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.75116796875" />
                  <Point X="1.448366333008" Y="-27.7434375" />
                  <Point X="1.417100097656" Y="-27.741119140625" />
                  <Point X="1.352746826172" Y="-27.747041015625" />
                  <Point X="1.184013061523" Y="-27.76256640625" />
                  <Point X="1.156362792969" Y="-27.7693984375" />
                  <Point X="1.128977050781" Y="-27.7807421875" />
                  <Point X="1.104594604492" Y="-27.795462890625" />
                  <Point X="1.054902832031" Y="-27.83678125" />
                  <Point X="0.924610900879" Y="-27.945115234375" />
                  <Point X="0.904141479492" Y="-27.96886328125" />
                  <Point X="0.887249206543" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.860766784668" Y="-28.094166015625" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.978790222168" Y="-29.5312109375" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.975717041016" Y="-29.87007421875" />
                  <Point X="0.929315551758" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058423095703" Y="-29.75263671875" />
                  <Point X="-1.133085693359" Y="-29.733427734375" />
                  <Point X="-1.14124621582" Y="-29.731328125" />
                  <Point X="-1.134089233398" Y="-29.67696484375" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.541037109375" />
                  <Point X="-1.121759155273" Y="-29.50933203125" />
                  <Point X="-1.123333740234" Y="-29.497693359375" />
                  <Point X="-1.140044921875" Y="-29.4136796875" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188124633789" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230575195312" Y="-29.094859375" />
                  <Point X="-1.250209716797" Y="-29.070935546875" />
                  <Point X="-1.261006469727" Y="-29.05978125" />
                  <Point X="-1.325407592773" Y="-29.003302734375" />
                  <Point X="-1.494267456055" Y="-28.855216796875" />
                  <Point X="-1.506735717773" Y="-28.84596875" />
                  <Point X="-1.53301940918" Y="-28.829623046875" />
                  <Point X="-1.546834472656" Y="-28.822525390625" />
                  <Point X="-1.576531494141" Y="-28.810224609375" />
                  <Point X="-1.591316040039" Y="-28.805474609375" />
                  <Point X="-1.621458251953" Y="-28.798447265625" />
                  <Point X="-1.636815673828" Y="-28.796169921875" />
                  <Point X="-1.722290161133" Y="-28.790568359375" />
                  <Point X="-1.946404541016" Y="-28.77587890625" />
                  <Point X="-1.961928710938" Y="-28.7761328125" />
                  <Point X="-1.992725708008" Y="-28.779166015625" />
                  <Point X="-2.007998779297" Y="-28.7819453125" />
                  <Point X="-2.039047729492" Y="-28.790263671875" />
                  <Point X="-2.053667724609" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095435302734" Y="-28.815810546875" />
                  <Point X="-2.166657226563" Y="-28.8633984375" />
                  <Point X="-2.353401611328" Y="-28.988177734375" />
                  <Point X="-2.359680419922" Y="-28.992755859375" />
                  <Point X="-2.380451416016" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471191406" Y="-29.041630859375" />
                  <Point X="-2.503201171875" Y="-29.162478515625" />
                  <Point X="-2.747609375" Y="-29.0111484375" />
                  <Point X="-2.850946044922" Y="-28.93158203125" />
                  <Point X="-2.98086328125" Y="-28.83155078125" />
                  <Point X="-2.547020263672" Y="-28.080111328125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684570312" Y="-27.666185546875" />
                  <Point X="-2.313413574219" Y="-27.63466015625" />
                  <Point X="-2.311638916016" Y="-27.619236328125" />
                  <Point X="-2.310626220703" Y="-27.588302734375" />
                  <Point X="-2.314666015625" Y="-27.5576171875" />
                  <Point X="-2.323650146484" Y="-27.528" />
                  <Point X="-2.329356445312" Y="-27.51355859375" />
                  <Point X="-2.343573730469" Y="-27.484728515625" />
                  <Point X="-2.351556884766" Y="-27.47141015625" />
                  <Point X="-2.369586669922" Y="-27.44625" />
                  <Point X="-2.379633300781" Y="-27.434408203125" />
                  <Point X="-2.396981933594" Y="-27.417060546875" />
                  <Point X="-2.408822265625" Y="-27.407015625" />
                  <Point X="-2.433978271484" Y="-27.388990234375" />
                  <Point X="-2.447293945312" Y="-27.381009765625" />
                  <Point X="-2.476122802734" Y="-27.36679296875" />
                  <Point X="-2.490563720703" Y="-27.3610859375" />
                  <Point X="-2.520181884766" Y="-27.3521015625" />
                  <Point X="-2.550869628906" Y="-27.3480625" />
                  <Point X="-2.581803955078" Y="-27.349076171875" />
                  <Point X="-2.597227783203" Y="-27.3508515625" />
                  <Point X="-2.62875390625" Y="-27.357123046875" />
                  <Point X="-2.643684570313" Y="-27.36138671875" />
                  <Point X="-2.672649414062" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.620092041016" Y="-27.91783203125" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-4.004020263672" Y="-27.740587890625" />
                  <Point X="-4.078115478516" Y="-27.616341796875" />
                  <Point X="-4.181265625" Y="-27.443375" />
                  <Point X="-3.407735595703" Y="-26.84982421875" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039154785156" Y="-26.566064453125" />
                  <Point X="-3.016260498047" Y="-26.543423828125" />
                  <Point X="-3.00240625" Y="-26.52607421875" />
                  <Point X="-2.983069335938" Y="-26.495005859375" />
                  <Point X="-2.974911376953" Y="-26.47853125" />
                  <Point X="-2.966070556641" Y="-26.45525" />
                  <Point X="-2.959762939453" Y="-26.4354375" />
                  <Point X="-2.954185546875" Y="-26.41390234375" />
                  <Point X="-2.951952148438" Y="-26.40239453125" />
                  <Point X="-2.947838134766" Y="-26.370912109375" />
                  <Point X="-2.947081787109" Y="-26.355697265625" />
                  <Point X="-2.94778125" Y="-26.332828125" />
                  <Point X="-2.951229736328" Y="-26.310208984375" />
                  <Point X="-2.957376953125" Y="-26.288169921875" />
                  <Point X="-2.961116455078" Y="-26.2773359375" />
                  <Point X="-2.973121582031" Y="-26.24835546875" />
                  <Point X="-2.978798583984" Y="-26.2368984375" />
                  <Point X="-2.991638671875" Y="-26.214853515625" />
                  <Point X="-3.007320800781" Y="-26.194734375" />
                  <Point X="-3.025564208984" Y="-26.176900390625" />
                  <Point X="-3.035288574219" Y="-26.16859765625" />
                  <Point X="-3.055102539062" Y="-26.153865234375" />
                  <Point X="-3.07209765625" Y="-26.142591796875" />
                  <Point X="-3.091268554688" Y="-26.13130859375" />
                  <Point X="-3.105435546875" Y="-26.124482421875" />
                  <Point X="-3.134696533203" Y="-26.113259765625" />
                  <Point X="-3.149790527344" Y="-26.10886328125" />
                  <Point X="-3.181677978516" Y="-26.102380859375" />
                  <Point X="-3.197294433594" Y="-26.10053515625" />
                  <Point X="-3.228619873047" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.422668457031" Y="-26.255328125" />
                  <Point X="-4.660920898438" Y="-26.286693359375" />
                  <Point X="-4.740762207031" Y="-25.974119140625" />
                  <Point X="-4.760366210938" Y="-25.837048828125" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.917941894531" Y="-25.421939453125" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.497775634766" Y="-25.3086875" />
                  <Point X="-3.477238037109" Y="-25.300521484375" />
                  <Point X="-3.467212890625" Y="-25.295841796875" />
                  <Point X="-3.443983398438" Y="-25.283302734375" />
                  <Point X="-3.425903320312" Y="-25.272197265625" />
                  <Point X="-3.405812744141" Y="-25.25825390625" />
                  <Point X="-3.396483398438" Y="-25.250873046875" />
                  <Point X="-3.372527587891" Y="-25.22934765625" />
                  <Point X="-3.357836669922" Y="-25.212646484375" />
                  <Point X="-3.337009765625" Y="-25.182470703125" />
                  <Point X="-3.328066162109" Y="-25.166369140625" />
                  <Point X="-3.318086181641" Y="-25.14340234375" />
                  <Point X="-3.310884521484" Y="-25.124" />
                  <Point X="-3.304187744141" Y="-25.102423828125" />
                  <Point X="-3.301577392578" Y="-25.09194140625" />
                  <Point X="-3.296133789062" Y="-25.06319921875" />
                  <Point X="-3.294510009766" Y="-25.042916015625" />
                  <Point X="-3.295432373047" Y="-25.0093046875" />
                  <Point X="-3.297392578125" Y="-24.9925390625" />
                  <Point X="-3.302193603516" Y="-24.96948828125" />
                  <Point X="-3.306740478516" Y="-24.95191015625" />
                  <Point X="-3.3134375" Y="-24.93033203125" />
                  <Point X="-3.317669677734" Y="-24.9192109375" />
                  <Point X="-3.33098828125" Y="-24.8898828125" />
                  <Point X="-3.342583007813" Y="-24.87073046875" />
                  <Point X="-3.365315917969" Y="-24.84158984375" />
                  <Point X="-3.378112792969" Y="-24.82813671875" />
                  <Point X="-3.39759765625" Y="-24.811302734375" />
                  <Point X="-3.413471679688" Y="-24.798990234375" />
                  <Point X="-3.433554199219" Y="-24.78505078125" />
                  <Point X="-3.440481689453" Y="-24.780673828125" />
                  <Point X="-3.465598388672" Y="-24.76717578125" />
                  <Point X="-3.496558105469" Y="-24.7543671875" />
                  <Point X="-3.508288330078" Y="-24.75038671875" />
                  <Point X="-4.582400390625" Y="-24.462580078125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.73133203125" Y="-24.042478515625" />
                  <Point X="-4.691866210938" Y="-23.8968359375" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.071716308594" Y="-23.755736328125" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057373047" Y="-23.795634765625" />
                  <Point X="-3.736704589844" Y="-23.795296875" />
                  <Point X="-3.715142089844" Y="-23.79341015625" />
                  <Point X="-3.704891845703" Y="-23.791947265625" />
                  <Point X="-3.684612548828" Y="-23.7879140625" />
                  <Point X="-3.657624511719" Y="-23.779998046875" />
                  <Point X="-3.613157470703" Y="-23.7659765625" />
                  <Point X="-3.603458984375" Y="-23.762326171875" />
                  <Point X="-3.584517089844" Y="-23.7539921875" />
                  <Point X="-3.575273681641" Y="-23.74930859375" />
                  <Point X="-3.556530029297" Y="-23.738486328125" />
                  <Point X="-3.547855712891" Y="-23.732826171875" />
                  <Point X="-3.531176269531" Y="-23.720595703125" />
                  <Point X="-3.515926757812" Y="-23.70662109375" />
                  <Point X="-3.502289550781" Y="-23.6910703125" />
                  <Point X="-3.495896728516" Y="-23.682923828125" />
                  <Point X="-3.483482666016" Y="-23.6651953125" />
                  <Point X="-3.478014648438" Y="-23.65640234375" />
                  <Point X="-3.468065429688" Y="-23.63826953125" />
                  <Point X="-3.456779296875" Y="-23.612501953125" />
                  <Point X="-3.438936767578" Y="-23.56942578125" />
                  <Point X="-3.435499267578" Y="-23.559650390625" />
                  <Point X="-3.429710205078" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436012451172" Y="-23.39546875" />
                  <Point X="-3.443508789062" Y="-23.37619140625" />
                  <Point X="-3.455994384766" Y="-23.350984375" />
                  <Point X="-3.477523193359" Y="-23.30962890625" />
                  <Point X="-3.482800048828" Y="-23.30071484375" />
                  <Point X="-3.494292236328" Y="-23.283515625" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521497558594" Y="-23.25109375" />
                  <Point X="-3.536439208984" Y="-23.2367890625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-4.160416503906" Y="-22.757279296875" />
                  <Point X="-4.227614257812" Y="-22.705716796875" />
                  <Point X="-4.002296142578" Y="-22.3196953125" />
                  <Point X="-3.897756835938" Y="-22.185322265625" />
                  <Point X="-3.726338623047" Y="-21.964990234375" />
                  <Point X="-3.434508544922" Y="-22.1334765625" />
                  <Point X="-3.254156982422" Y="-22.237603515625" />
                  <Point X="-3.244916259766" Y="-22.24228515625" />
                  <Point X="-3.225988037109" Y="-22.25061328125" />
                  <Point X="-3.21630078125" Y="-22.254259765625" />
                  <Point X="-3.195658203125" Y="-22.26076953125" />
                  <Point X="-3.185623535156" Y="-22.263341796875" />
                  <Point X="-3.165331542969" Y="-22.26737890625" />
                  <Point X="-3.15507421875" Y="-22.26884375" />
                  <Point X="-3.131454833984" Y="-22.27091015625" />
                  <Point X="-3.069524902344" Y="-22.276328125" />
                  <Point X="-3.059173583984" Y="-22.276666015625" />
                  <Point X="-3.038489013672" Y="-22.27621484375" />
                  <Point X="-3.028155761719" Y="-22.27542578125" />
                  <Point X="-3.006697265625" Y="-22.272599609375" />
                  <Point X="-2.996512451172" Y="-22.2706875" />
                  <Point X="-2.976423339844" Y="-22.26576953125" />
                  <Point X="-2.956999267578" Y="-22.25869921875" />
                  <Point X="-2.938449951172" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.902742431641" Y="-22.2268046875" />
                  <Point X="-2.886610107422" Y="-22.21385546875" />
                  <Point X="-2.878901855469" Y="-22.206943359375" />
                  <Point X="-2.862136474609" Y="-22.190177734375" />
                  <Point X="-2.818178222656" Y="-22.146220703125" />
                  <Point X="-2.811268310547" Y="-22.138513671875" />
                  <Point X="-2.798323486328" Y="-22.12238671875" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.750863769531" Y="-21.931982421875" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.059388183594" Y="-21.30008203125" />
                  <Point X="-2.648381591797" Y="-20.98496875" />
                  <Point X="-2.483739990234" Y="-20.893498046875" />
                  <Point X="-2.192524414062" Y="-20.731703125" />
                  <Point X="-2.173775390625" Y="-20.756138671875" />
                  <Point X="-2.118564208984" Y="-20.82808984375" />
                  <Point X="-2.111820556641" Y="-20.835951171875" />
                  <Point X="-2.097517578125" Y="-20.850892578125" />
                  <Point X="-2.089958251953" Y="-20.85797265625" />
                  <Point X="-2.07337890625" Y="-20.871884765625" />
                  <Point X="-2.065095458984" Y="-20.878099609375" />
                  <Point X="-2.047897583008" Y="-20.889591796875" />
                  <Point X="-2.038983276367" Y="-20.894869140625" />
                  <Point X="-2.012695068359" Y="-20.9085546875" />
                  <Point X="-1.943767333984" Y="-20.9444375" />
                  <Point X="-1.934334228516" Y="-20.9487109375" />
                  <Point X="-1.915060302734" Y="-20.95620703125" />
                  <Point X="-1.905219360352" Y="-20.9594296875" />
                  <Point X="-1.8843125" Y="-20.965033203125" />
                  <Point X="-1.874174316406" Y="-20.967166015625" />
                  <Point X="-1.853724487305" Y="-20.970314453125" />
                  <Point X="-1.833054077148" Y="-20.971216796875" />
                  <Point X="-1.812407836914" Y="-20.96986328125" />
                  <Point X="-1.802120605469" Y="-20.968623046875" />
                  <Point X="-1.780804931641" Y="-20.96486328125" />
                  <Point X="-1.770713623047" Y="-20.9625078125" />
                  <Point X="-1.750859863281" Y="-20.95671875" />
                  <Point X="-1.741097412109" Y="-20.95328515625" />
                  <Point X="-1.713716430664" Y="-20.941943359375" />
                  <Point X="-1.641923583984" Y="-20.912205078125" />
                  <Point X="-1.632589111328" Y="-20.907728515625" />
                  <Point X="-1.614455444336" Y="-20.89778125" />
                  <Point X="-1.60565625" Y="-20.892310546875" />
                  <Point X="-1.587926879883" Y="-20.879896484375" />
                  <Point X="-1.579779296875" Y="-20.873501953125" />
                  <Point X="-1.564226928711" Y="-20.85986328125" />
                  <Point X="-1.550251464844" Y="-20.844611328125" />
                  <Point X="-1.538020019531" Y="-20.8279296875" />
                  <Point X="-1.532359619141" Y="-20.819255859375" />
                  <Point X="-1.521537841797" Y="-20.80051171875" />
                  <Point X="-1.516854370117" Y="-20.79126953125" />
                  <Point X="-1.508524169922" Y="-20.7723359375" />
                  <Point X="-1.504877441406" Y="-20.762646484375" />
                  <Point X="-1.495965087891" Y="-20.734380859375" />
                  <Point X="-1.472597900391" Y="-20.66026953125" />
                  <Point X="-1.470026611328" Y="-20.650236328125" />
                  <Point X="-1.465991333008" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-1.467937744141" Y="-20.434169921875" />
                  <Point X="-0.931164489746" Y="-20.2836796875" />
                  <Point X="-0.731598022461" Y="-20.2603203125" />
                  <Point X="-0.36522253418" Y="-20.21744140625" />
                  <Point X="-0.279019287109" Y="-20.53915625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896308899" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.377869720459" Y="-20.21619140625" />
                  <Point X="0.378190734863" Y="-20.214994140625" />
                  <Point X="0.827866760254" Y="-20.2620859375" />
                  <Point X="0.99300177002" Y="-20.301955078125" />
                  <Point X="1.453606079102" Y="-20.41316015625" />
                  <Point X="1.559032226562" Y="-20.4513984375" />
                  <Point X="1.858246826172" Y="-20.55992578125" />
                  <Point X="1.962191162109" Y="-20.608537109375" />
                  <Point X="2.250435302734" Y="-20.74333984375" />
                  <Point X="2.350914550781" Y="-20.80187890625" />
                  <Point X="2.629424316406" Y="-20.964138671875" />
                  <Point X="2.724132080078" Y="-21.031490234375" />
                  <Point X="2.817778564453" Y="-21.0980859375" />
                  <Point X="2.303195800781" Y="-21.989369140625" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.0531796875" Y="-22.42656640625" />
                  <Point X="2.044181640625" Y="-22.450439453125" />
                  <Point X="2.041301635742" Y="-22.459404296875" />
                  <Point X="2.035374145508" Y="-22.4815703125" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017266357422" Y="-22.553060546875" />
                  <Point X="2.014072509766" Y="-22.58003125" />
                  <Point X="2.013444335937" Y="-22.5936328125" />
                  <Point X="2.01441796875" Y="-22.631701171875" />
                  <Point X="2.015722290039" Y="-22.649587890625" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.023800537109" Y="-22.710966796875" />
                  <Point X="2.029143554687" Y="-22.732890625" />
                  <Point X="2.032468261719" Y="-22.743693359375" />
                  <Point X="2.040734130859" Y="-22.76578125" />
                  <Point X="2.045319580078" Y="-22.776115234375" />
                  <Point X="2.055683105469" Y="-22.79616015625" />
                  <Point X="2.061461181641" Y="-22.80587109375" />
                  <Point X="2.073321533203" Y="-22.823349609375" />
                  <Point X="2.104418945313" Y="-22.8691796875" />
                  <Point X="2.109805908203" Y="-22.87636328125" />
                  <Point X="2.130458496094" Y="-22.899873046875" />
                  <Point X="2.157590576172" Y="-22.924609375" />
                  <Point X="2.168249755859" Y="-22.933015625" />
                  <Point X="2.185728515625" Y="-22.944876953125" />
                  <Point X="2.231558349609" Y="-22.975974609375" />
                  <Point X="2.241276367188" Y="-22.981755859375" />
                  <Point X="2.261326416016" Y="-22.99212109375" />
                  <Point X="2.271658447266" Y="-22.996705078125" />
                  <Point X="2.293746826172" Y="-23.004970703125" />
                  <Point X="2.304554443359" Y="-23.008296875" />
                  <Point X="2.326479980469" Y="-23.013638671875" />
                  <Point X="2.356765380859" Y="-23.01796484375" />
                  <Point X="2.407022705078" Y="-23.024025390625" />
                  <Point X="2.416042236328" Y="-23.0246796875" />
                  <Point X="2.447577880859" Y="-23.02450390625" />
                  <Point X="2.484317382813" Y="-23.020177734375" />
                  <Point X="2.497750488281" Y="-23.01760546875" />
                  <Point X="2.519916748047" Y="-23.011677734375" />
                  <Point X="2.578036865234" Y="-22.996134765625" />
                  <Point X="2.584004638672" Y="-22.994326171875" />
                  <Point X="2.604415039063" Y="-22.986927734375" />
                  <Point X="2.627659912109" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.716384277344" Y="-22.348384765625" />
                  <Point X="3.940403564453" Y="-22.219046875" />
                  <Point X="4.043956542969" Y="-22.362962890625" />
                  <Point X="4.096747070313" Y="-22.45019921875" />
                  <Point X="4.136884765625" Y="-22.516529296875" />
                  <Point X="3.48615234375" Y="-23.0158515625" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168130859375" Y="-23.26013671875" />
                  <Point X="3.152111816406" Y="-23.274791015625" />
                  <Point X="3.134665039062" Y="-23.29340234375" />
                  <Point X="3.128575683594" Y="-23.30058203125" />
                  <Point X="3.112622558594" Y="-23.32139453125" />
                  <Point X="3.070793457031" Y="-23.375962890625" />
                  <Point X="3.065636230469" Y="-23.3833984375" />
                  <Point X="3.049740722656" Y="-23.410626953125" />
                  <Point X="3.034763671875" Y="-23.444453125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.024197509766" Y="-23.478578125" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077636719" Y="-23.636244140625" />
                  <Point X="3.004698974609" Y="-23.6474296875" />
                  <Point X="3.009577148438" Y="-23.671072265625" />
                  <Point X="3.022367919922" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034684570313" Y="-23.77139453125" />
                  <Point X="3.050288330078" Y="-23.8046328125" />
                  <Point X="3.056921142578" Y="-23.816478515625" />
                  <Point X="3.070189453125" Y="-23.83664453125" />
                  <Point X="3.104978759766" Y="-23.8895234375" />
                  <Point X="3.111742675781" Y="-23.898580078125" />
                  <Point X="3.126295166016" Y="-23.915822265625" />
                  <Point X="3.134083740234" Y="-23.9240078125" />
                  <Point X="3.151327880859" Y="-23.94009765625" />
                  <Point X="3.160033447266" Y="-23.947302734375" />
                  <Point X="3.178240234375" Y="-23.960626953125" />
                  <Point X="3.187741455078" Y="-23.96674609375" />
                  <Point X="3.206968994141" Y="-23.9775703125" />
                  <Point X="3.257383544922" Y="-24.00594921875" />
                  <Point X="3.2654765625" Y="-24.010009765625" />
                  <Point X="3.29467578125" Y="-24.0219140625" />
                  <Point X="3.330274414062" Y="-24.031978515625" />
                  <Point X="3.343673583984" Y="-24.034744140625" />
                  <Point X="3.369670410156" Y="-24.0381796875" />
                  <Point X="3.437834228516" Y="-24.0471875" />
                  <Point X="3.444033447266" Y="-24.04780078125" />
                  <Point X="3.465708740234" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.526863769531" Y="-23.91209375" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.769320800781" Y="-24.192625" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.050926757813" Y="-24.48246875" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.686021728516" Y="-24.580458984375" />
                  <Point X="3.665622314453" Y="-24.587865234375" />
                  <Point X="3.642384033203" Y="-24.598380859375" />
                  <Point X="3.634011962891" Y="-24.602681640625" />
                  <Point X="3.608470947266" Y="-24.617443359375" />
                  <Point X="3.541501953125" Y="-24.65615234375" />
                  <Point X="3.533886474609" Y="-24.66105078125" />
                  <Point X="3.508770507812" Y="-24.6801328125" />
                  <Point X="3.481992919922" Y="-24.705650390625" />
                  <Point X="3.472796386719" Y="-24.7157734375" />
                  <Point X="3.457471679688" Y="-24.73530078125" />
                  <Point X="3.417290527344" Y="-24.786501953125" />
                  <Point X="3.410854248047" Y="-24.79579296875" />
                  <Point X="3.399130615234" Y="-24.815072265625" />
                  <Point X="3.393843261719" Y="-24.825060546875" />
                  <Point X="3.384069580078" Y="-24.8465234375" />
                  <Point X="3.380005615234" Y="-24.8570703125" />
                  <Point X="3.373158935547" Y="-24.878572265625" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.365267822266" Y="-24.916201171875" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026263671875" />
                  <Point X="3.350280029297" Y="-25.062943359375" />
                  <Point X="3.351874511719" Y="-25.076427734375" />
                  <Point X="3.356982910156" Y="-25.103099609375" />
                  <Point X="3.370376708984" Y="-25.173037109375" />
                  <Point X="3.373159667969" Y="-25.1839921875" />
                  <Point X="3.380006103516" Y="-25.2054921875" />
                  <Point X="3.384069580078" Y="-25.216037109375" />
                  <Point X="3.393843261719" Y="-25.2375" />
                  <Point X="3.399130615234" Y="-25.24748828125" />
                  <Point X="3.410854248047" Y="-25.266767578125" />
                  <Point X="3.417290527344" Y="-25.27605859375" />
                  <Point X="3.432615234375" Y="-25.2955859375" />
                  <Point X="3.472796386719" Y="-25.346787109375" />
                  <Point X="3.478718994141" Y="-25.353634765625" />
                  <Point X="3.5011328125" Y="-25.375798828125" />
                  <Point X="3.530172363281" Y="-25.39872265625" />
                  <Point X="3.541493164062" Y="-25.406404296875" />
                  <Point X="3.567034423828" Y="-25.42116796875" />
                  <Point X="3.634003173828" Y="-25.459876953125" />
                  <Point X="3.639498535156" Y="-25.462818359375" />
                  <Point X="3.659150146484" Y="-25.472013671875" />
                  <Point X="3.683021972656" Y="-25.48102734375" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.633119628906" Y="-25.73608984375" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.76161328125" Y="-25.9310546875" />
                  <Point X="4.740298339844" Y="-26.024458984375" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.853045654297" Y="-25.964056640625" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400096923828" Y="-25.90804296875" />
                  <Point X="3.366721435547" Y="-25.910841796875" />
                  <Point X="3.354484375" Y="-25.912677734375" />
                  <Point X="3.304356201172" Y="-25.923572265625" />
                  <Point X="3.172920166016" Y="-25.952140625" />
                  <Point X="3.157876953125" Y="-25.9567421875" />
                  <Point X="3.128754150391" Y="-25.9683671875" />
                  <Point X="3.114674560547" Y="-25.975390625" />
                  <Point X="3.086848144531" Y="-25.992283203125" />
                  <Point X="3.074123535156" Y="-26.00153125" />
                  <Point X="3.050373535156" Y="-26.022001953125" />
                  <Point X="3.039348144531" Y="-26.033224609375" />
                  <Point X="3.009048828125" Y="-26.069666015625" />
                  <Point X="2.929604003906" Y="-26.165212890625" />
                  <Point X="2.921325195312" Y="-26.17684765625" />
                  <Point X="2.90660546875" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.870814697266" Y="-26.343853515625" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607177734" Y="-26.51458984375" />
                  <Point X="2.864065429688" Y="-26.530126953125" />
                  <Point X="2.871796875" Y="-26.561748046875" />
                  <Point X="2.876785644531" Y="-26.57666796875" />
                  <Point X="2.889158203125" Y="-26.605482421875" />
                  <Point X="2.896541992188" Y="-26.619376953125" />
                  <Point X="2.924283691406" Y="-26.662525390625" />
                  <Point X="2.997022216797" Y="-26.775666015625" />
                  <Point X="3.00174609375" Y="-26.782359375" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486083984" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.926168212891" Y="-27.50644140625" />
                  <Point X="4.087170410156" Y="-27.629984375" />
                  <Point X="4.045477294922" Y="-27.69744921875" />
                  <Point X="4.00140625" Y="-27.760068359375" />
                  <Point X="4.001274658203" Y="-27.760255859375" />
                  <Point X="3.219193603516" Y="-27.308720703125" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841199951172" Y="-27.090890625" />
                  <Point X="2.815021972656" Y="-27.07951171875" />
                  <Point X="2.783118652344" Y="-27.069326171875" />
                  <Point X="2.771110595703" Y="-27.066337890625" />
                  <Point X="2.711449951172" Y="-27.0555625" />
                  <Point X="2.555020263672" Y="-27.0273125" />
                  <Point X="2.539360351562" Y="-27.02580859375" />
                  <Point X="2.508008789062" Y="-27.025404296875" />
                  <Point X="2.492317138672" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444846191406" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400590820312" Y="-27.051109375" />
                  <Point X="2.351027587891" Y="-27.077193359375" />
                  <Point X="2.221072753906" Y="-27.145587890625" />
                  <Point X="2.208970214844" Y="-27.153169921875" />
                  <Point X="2.186039550781" Y="-27.1700625" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144940429687" Y="-27.21116015625" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.094379394531" Y="-27.2957578125" />
                  <Point X="2.025985229492" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564484375" />
                  <Point X="2.002187866211" Y="-27.58014453125" />
                  <Point X="2.012962524414" Y="-27.6398046875" />
                  <Point X="2.041213500977" Y="-27.796236328125" />
                  <Point X="2.043015380859" Y="-27.8042265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.630776123047" Y="-28.84565625" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723752685547" Y="-29.036083984375" />
                  <Point X="2.116822753906" Y="-28.2451171875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657104492" Y="-27.870150390625" />
                  <Point X="1.808835327148" Y="-27.849630859375" />
                  <Point X="1.783253417969" Y="-27.828005859375" />
                  <Point X="1.773299560547" Y="-27.820646484375" />
                  <Point X="1.714458007812" Y="-27.78281640625" />
                  <Point X="1.560176025391" Y="-27.68362890625" />
                  <Point X="1.546279663086" Y="-27.676244140625" />
                  <Point X="1.517465332031" Y="-27.663873046875" />
                  <Point X="1.502547485352" Y="-27.65888671875" />
                  <Point X="1.470927368164" Y="-27.65115625" />
                  <Point X="1.455391235352" Y="-27.648697265625" />
                  <Point X="1.424125" Y="-27.64637890625" />
                  <Point X="1.408394897461" Y="-27.64651953125" />
                  <Point X="1.344041625977" Y="-27.65244140625" />
                  <Point X="1.175307861328" Y="-27.667966796875" />
                  <Point X="1.161225097656" Y="-27.67033984375" />
                  <Point X="1.133574829102" Y="-27.677171875" />
                  <Point X="1.120007324219" Y="-27.681630859375" />
                  <Point X="1.092621582031" Y="-27.692974609375" />
                  <Point X="1.079876342773" Y="-27.6994140625" />
                  <Point X="1.055493896484" Y="-27.714134765625" />
                  <Point X="1.043856323242" Y="-27.722416015625" />
                  <Point X="0.994164611816" Y="-27.763734375" />
                  <Point X="0.863872680664" Y="-27.872068359375" />
                  <Point X="0.852652404785" Y="-27.883091796875" />
                  <Point X="0.832183044434" Y="-27.90683984375" />
                  <Point X="0.822933776855" Y="-27.919564453125" />
                  <Point X="0.806041625977" Y="-27.947390625" />
                  <Point X="0.799019287109" Y="-27.96146875" />
                  <Point X="0.787394470215" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.767934387207" Y="-28.07398828125" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091064453" Y="-29.15233984375" />
                  <Point X="0.765661621094" Y="-28.900689453125" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146484375" Y="-28.453583984375" />
                  <Point X="0.626787841797" Y="-28.42381640625" />
                  <Point X="0.620406982422" Y="-28.41320703125" />
                  <Point X="0.575203308105" Y="-28.348078125" />
                  <Point X="0.456679229736" Y="-28.177306640625" />
                  <Point X="0.446668762207" Y="-28.165169921875" />
                  <Point X="0.424782104492" Y="-28.142712890625" />
                  <Point X="0.412905792236" Y="-28.132392578125" />
                  <Point X="0.386650512695" Y="-28.113150390625" />
                  <Point X="0.373235198975" Y="-28.10493359375" />
                  <Point X="0.345236297607" Y="-28.090828125" />
                  <Point X="0.330652526855" Y="-28.084939453125" />
                  <Point X="0.260702423096" Y="-28.06323046875" />
                  <Point X="0.077293426514" Y="-28.006306640625" />
                  <Point X="0.063379947662" Y="-28.003111328125" />
                  <Point X="0.035227794647" Y="-27.998841796875" />
                  <Point X="0.020989276886" Y="-27.997767578125" />
                  <Point X="-0.008651926041" Y="-27.997765625" />
                  <Point X="-0.022895498276" Y="-27.998837890625" />
                  <Point X="-0.051062511444" Y="-28.003107421875" />
                  <Point X="-0.064985801697" Y="-28.0063046875" />
                  <Point X="-0.134935760498" Y="-28.028015625" />
                  <Point X="-0.31834475708" Y="-28.0849375" />
                  <Point X="-0.33292956543" Y="-28.090828125" />
                  <Point X="-0.360929656982" Y="-28.104935546875" />
                  <Point X="-0.374344970703" Y="-28.11315234375" />
                  <Point X="-0.400600402832" Y="-28.132396484375" />
                  <Point X="-0.412477142334" Y="-28.14271875" />
                  <Point X="-0.434361114502" Y="-28.16517578125" />
                  <Point X="-0.444368499756" Y="-28.177310546875" />
                  <Point X="-0.489572143555" Y="-28.24244140625" />
                  <Point X="-0.608096557617" Y="-28.4132109375" />
                  <Point X="-0.612470092773" Y="-28.420130859375" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.932737548828" Y="-29.570173828125" />
                  <Point X="-0.985425170898" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.083981199206" Y="-22.557123445493" />
                  <Point X="3.857322671873" Y="-22.267013760067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008609632767" Y="-22.614957818044" />
                  <Point X="3.774241779292" Y="-22.314980645134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.933238066327" Y="-22.672792190596" />
                  <Point X="3.691160864732" Y="-22.362947502069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.768979230492" Y="-21.182608835961" />
                  <Point X="2.572281571957" Y="-20.930847313848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857866499888" Y="-22.730626563148" />
                  <Point X="3.608079899754" Y="-22.410914294472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717748820687" Y="-21.271342479938" />
                  <Point X="2.351004722468" Y="-20.801931440267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.733049788266" Y="-24.005115668064" />
                  <Point X="4.647920884154" Y="-23.896155639587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.782494933449" Y="-22.788460935699" />
                  <Point X="3.524998934776" Y="-22.458881086875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.666518410882" Y="-21.360076123915" />
                  <Point X="2.146807354372" Y="-20.694876305977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.771891531518" Y="-24.209136410639" />
                  <Point X="4.538608300509" Y="-23.910547491178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.70712336701" Y="-22.846295308251" />
                  <Point X="3.441917969798" Y="-22.506847879277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.615288001076" Y="-21.448809767892" />
                  <Point X="1.956839740429" Y="-20.606034426445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.723983783793" Y="-24.302122868143" />
                  <Point X="4.42929532845" Y="-23.924938845622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.63175180057" Y="-22.904129680803" />
                  <Point X="3.35883700482" Y="-22.55481467168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.564057591271" Y="-21.53754341187" />
                  <Point X="1.777328513523" Y="-20.530576112003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.624296223098" Y="-24.328834187317" />
                  <Point X="4.319982309637" Y="-23.939330140223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.556380234131" Y="-22.961964053354" />
                  <Point X="3.275756039842" Y="-22.602781464083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.512827181466" Y="-21.626277055847" />
                  <Point X="1.609099320051" Y="-20.469558141848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.524608662402" Y="-24.355545506492" />
                  <Point X="4.210669290823" Y="-23.953721434824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.481008681326" Y="-23.019798443357" />
                  <Point X="3.192675074864" Y="-22.650748256486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.461596771661" Y="-21.715010699824" />
                  <Point X="1.442357938362" Y="-20.410444483937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.424921101707" Y="-24.382256825667" />
                  <Point X="4.10135627201" Y="-23.968112729425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.40563731467" Y="-23.07763307162" />
                  <Point X="3.109594109886" Y="-22.698715048889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.410366361856" Y="-21.803744343801" />
                  <Point X="1.293774120296" Y="-20.374571447644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.325233541011" Y="-24.408968144842" />
                  <Point X="3.992043253197" Y="-23.982504024026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330265948014" Y="-23.135467699882" />
                  <Point X="3.026513144909" Y="-22.746681841292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.359135952051" Y="-21.892477987778" />
                  <Point X="1.14519030223" Y="-20.338698411352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.225545980316" Y="-24.435679464017" />
                  <Point X="3.882730234383" Y="-23.996895318627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.254894581359" Y="-23.193302328145" />
                  <Point X="2.943432179931" Y="-22.794648633695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.307905542246" Y="-21.981211631755" />
                  <Point X="0.996606484163" Y="-20.302825375059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.12585841962" Y="-24.462390783192" />
                  <Point X="3.77341721557" Y="-24.011286613228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.179523214703" Y="-23.251136956408" />
                  <Point X="2.860351214953" Y="-22.842615426098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.256675171873" Y="-22.069945326203" />
                  <Point X="0.848022581102" Y="-20.266952229977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.026170901306" Y="-24.489102156613" />
                  <Point X="3.664104196757" Y="-24.025677907829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.113234245184" Y="-23.320596522887" />
                  <Point X="2.777270249975" Y="-22.890582218501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205444805492" Y="-22.15867902576" />
                  <Point X="0.71437835272" Y="-20.25020099649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.926483511275" Y="-24.515813694228" />
                  <Point X="3.554791177943" Y="-24.04006920243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055418424269" Y="-23.400901225019" />
                  <Point X="2.694189284997" Y="-22.938549010904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.154214439111" Y="-22.247412725317" />
                  <Point X="0.583078794124" Y="-20.236450803476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.826796121244" Y="-24.542525231843" />
                  <Point X="3.439960190165" Y="-24.047397818827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.016646016417" Y="-23.50558038435" />
                  <Point X="2.609599465296" Y="-22.984584557329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.10298407273" Y="-22.336146424875" />
                  <Point X="0.451779235528" Y="-20.222700610461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766379255719" Y="-25.899442380886" />
                  <Point X="4.640246124415" Y="-25.737999334931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.727108731212" Y="-24.569236769458" />
                  <Point X="3.300859191198" Y="-24.02366223749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.004194395281" Y="-23.64394861439" />
                  <Point X="2.51188776148" Y="-23.013824857984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053144373983" Y="-22.426660097833" />
                  <Point X="0.364658424839" Y="-20.26549663615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.741021236697" Y="-26.02129117495" />
                  <Point X="4.487769232203" Y="-25.697143390962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.633095726891" Y="-24.60321118958" />
                  <Point X="2.398497339095" Y="-23.022997314002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02010359775" Y="-22.538675411093" />
                  <Point X="0.333871153664" Y="-20.380396304353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.658605104359" Y="-26.070108914327" />
                  <Point X="4.335292296343" Y="-25.656287391126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.550043741703" Y="-24.651215074423" />
                  <Point X="2.248729619152" Y="-22.985608952409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021109786606" Y="-22.69426885242" />
                  <Point X="0.303083882489" Y="-20.495295972556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.524226662449" Y="-26.052417930378" />
                  <Point X="4.182815360483" Y="-25.615431391291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.476629266918" Y="-24.711554410061" />
                  <Point X="0.272296611315" Y="-20.610195640758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.389848220539" Y="-26.034726946429" />
                  <Point X="4.030338424623" Y="-25.574575391456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.416039219396" Y="-24.788308264062" />
                  <Point X="0.24150934014" Y="-20.725095308961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.255469778629" Y="-26.01703596248" />
                  <Point X="3.877861488764" Y="-25.53371939162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.371403575123" Y="-24.885482822998" />
                  <Point X="0.19531407742" Y="-20.820273647316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.121091336719" Y="-25.999344978531" />
                  <Point X="3.725384552904" Y="-25.492863391785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349520626755" Y="-25.011779504668" />
                  <Point X="0.123892073035" Y="-20.88316322877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.349039044315" Y="-20.277839002515" />
                  <Point X="-0.393629353028" Y="-20.22076601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.986712894809" Y="-25.981653994582" />
                  <Point X="3.532523370925" Y="-25.400317914057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.379797826754" Y="-25.204838131773" />
                  <Point X="0.027378905768" Y="-20.913937586252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.2861113332" Y="-20.51268837811" />
                  <Point X="-0.504086127499" Y="-20.233693364118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.852334450771" Y="-25.96396300791" />
                  <Point X="-0.121266466399" Y="-20.877985764303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.221648066679" Y="-20.749503174998" />
                  <Point X="-0.61454290197" Y="-20.246620718236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.717955606928" Y="-25.94627150951" />
                  <Point X="-0.72499967644" Y="-20.259548072354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583576763086" Y="-25.928580011111" />
                  <Point X="-0.835455321678" Y="-20.272476871825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449197919243" Y="-25.910888512711" />
                  <Point X="-0.944367450958" Y="-20.287381281629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.333587381745" Y="-25.917219350968" />
                  <Point X="-1.043262104288" Y="-20.315107475951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.230530873975" Y="-25.939618614526" />
                  <Point X="-1.142156757619" Y="-20.342833670273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.131559952798" Y="-25.967247190456" />
                  <Point X="-1.241051410949" Y="-20.370559864595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052410572165" Y="-26.020246181342" />
                  <Point X="-1.33994606428" Y="-20.398286058917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.989156469807" Y="-26.093590200648" />
                  <Point X="-1.43884071761" Y="-20.426012253239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079128798696" Y="-27.642996740653" />
                  <Point X="4.041706197104" Y="-27.59509799489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.927198616494" Y="-26.168593343073" />
                  <Point X="-1.464821337339" Y="-20.547064154739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.024453548054" Y="-27.727321189426" />
                  <Point X="3.740687123651" Y="-27.364116729014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.8818958168" Y="-26.264913982009" />
                  <Point X="-1.47959472771" Y="-20.682460655675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.870753046364" Y="-27.684899096744" />
                  <Point X="3.439670304268" Y="-27.133138348216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.865777856408" Y="-26.398589511799" />
                  <Point X="-1.51623698444" Y="-20.78986628411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.651129361136" Y="-27.558099176926" />
                  <Point X="3.138653484884" Y="-26.902159967417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.871387989669" Y="-26.560075733242" />
                  <Point X="-1.574836751575" Y="-20.869167580838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.431505675908" Y="-27.431299257108" />
                  <Point X="-1.656916722138" Y="-20.918415587665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.211881979651" Y="-27.304499323174" />
                  <Point X="-1.748261692111" Y="-20.955804936027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.992257963139" Y="-27.177698979332" />
                  <Point X="-1.857996169071" Y="-20.9696567888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.788331347203" Y="-27.070990392004" />
                  <Point X="-2.035612336551" Y="-20.896624039812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.64656632416" Y="-27.043845015344" />
                  <Point X="-2.257022639838" Y="-20.76753735316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.511638697275" Y="-27.025451106681" />
                  <Point X="-2.341088760082" Y="-20.814243204324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408503223729" Y="-27.047749298654" />
                  <Point X="-2.425154880326" Y="-20.860949055487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.322628169979" Y="-27.092139820513" />
                  <Point X="-2.509221139996" Y="-20.907654728194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.237198826341" Y="-27.137100825302" />
                  <Point X="-2.59328772023" Y="-20.954359990596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.160871759043" Y="-27.193712212524" />
                  <Point X="-2.674365732924" Y="-21.004890445015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.104708438886" Y="-27.276132019174" />
                  <Point X="-2.749760750694" Y="-21.062694801232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.05618505558" Y="-27.368330499067" />
                  <Point X="-2.825155768465" Y="-21.120499157448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.011503886706" Y="-27.46544678917" />
                  <Point X="-2.900550786235" Y="-21.178303513665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.008690794762" Y="-27.616151773997" />
                  <Point X="-2.975945804006" Y="-21.236107869881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.047990017934" Y="-27.820758064169" />
                  <Point X="-3.051340821776" Y="-21.293912226098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.355043376419" Y="-28.368074019319" />
                  <Point X="-2.772036060708" Y="-21.805711596179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69634745778" Y="-28.959228900612" />
                  <Point X="-2.749117626003" Y="-21.989351433224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.598364187741" Y="-27.708179980158" />
                  <Point X="-2.783316480249" Y="-22.099884474222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.429855121255" Y="-27.646803788881" />
                  <Point X="-2.84596363814" Y="-22.174005347019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.315737667007" Y="-27.65504568655" />
                  <Point X="-2.917433186658" Y="-22.236834074759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.203266174989" Y="-27.665394319803" />
                  <Point X="-3.009734311452" Y="-22.272999600757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.101412551515" Y="-27.689333205049" />
                  <Point X="-3.131957879463" Y="-22.270866145945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.02133413289" Y="-27.741143081532" />
                  <Point X="-3.298508108384" Y="-22.21199715242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.948252754303" Y="-27.801908760862" />
                  <Point X="-3.51813185946" Y="-22.085197148319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.875171082072" Y="-27.862674064343" />
                  <Point X="-3.729464970971" Y="-21.969008678956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812495740832" Y="-27.936758864099" />
                  <Point X="-3.789616302601" Y="-22.046324063692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.774729496565" Y="-28.042725854091" />
                  <Point X="-3.849767634231" Y="-22.123639448428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.748490134797" Y="-28.163446580883" />
                  <Point X="-3.909918799244" Y="-22.200955046423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724933698186" Y="-28.28760129528" />
                  <Point X="-3.970069306823" Y="-22.278271485896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.742525434547" Y="-28.464423269352" />
                  <Point X="-4.026228385851" Y="-22.360696720944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766956963084" Y="-28.649999778184" />
                  <Point X="0.662751740951" Y="-28.516623176085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.323828922317" Y="-28.082821750415" />
                  <Point X="-4.077780939163" Y="-22.449018040035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.79138849162" Y="-28.835576287017" />
                  <Point X="0.725679092893" Y="-28.75147209196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.164682498389" Y="-28.033429195135" />
                  <Point X="-3.424593530553" Y="-23.43936537626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.6537483702" Y="-23.146060556777" />
                  <Point X="-4.129333492474" Y="-22.537339359127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.815820020157" Y="-29.02115279585" />
                  <Point X="0.788606530781" Y="-28.986321117841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.016263615889" Y="-27.997767266741" />
                  <Point X="-3.440530263938" Y="-23.57327286604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.954766395347" Y="-22.915080632672" />
                  <Point X="-4.180886045786" Y="-22.625660678218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.09125325203" Y="-28.014457529649" />
                  <Point X="-3.486217692575" Y="-23.669101202381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.188282052684" Y="-28.044571906492" />
                  <Point X="-3.553903722519" Y="-23.736772613058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.285311492327" Y="-28.074685465464" />
                  <Point X="-3.644036540056" Y="-23.775713445786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.375279465379" Y="-28.113837289512" />
                  <Point X="-3.749063218104" Y="-23.795591006383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.44525148004" Y="-28.17858277318" />
                  <Point X="-3.881158146202" Y="-23.78082278683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.501965503121" Y="-28.260297712231" />
                  <Point X="-4.015536212645" Y="-23.763132283456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.558680109611" Y="-28.342011904551" />
                  <Point X="-4.149914771507" Y="-23.745441149815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.614806740036" Y="-28.424478671916" />
                  <Point X="-3.297850845355" Y="-24.990338820509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.47536027356" Y="-24.763137113242" />
                  <Point X="-4.28429368414" Y="-23.727749563369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.653742450444" Y="-28.528948813507" />
                  <Point X="-3.311881492802" Y="-25.126685989035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.631717995592" Y="-24.717313933619" />
                  <Point X="-4.418672596773" Y="-23.710057976922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.684529698273" Y="-28.643848511591" />
                  <Point X="-3.361777924775" Y="-25.217127046777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.784194887766" Y="-24.676457989699" />
                  <Point X="-4.553051509405" Y="-23.692366390476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.715316946103" Y="-28.758748209674" />
                  <Point X="-3.434961910581" Y="-25.277761394854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.93667177994" Y="-24.635602045779" />
                  <Point X="-4.646025736597" Y="-23.727670384693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.746104193932" Y="-28.873647907758" />
                  <Point X="-3.525110947207" Y="-25.316681468095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.089148672114" Y="-24.594746101859" />
                  <Point X="-4.677071565902" Y="-23.84223911358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.776891441761" Y="-28.988547605841" />
                  <Point X="-3.62479870697" Y="-25.343392532476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.241625564288" Y="-24.553890157939" />
                  <Point X="-4.708117273114" Y="-23.95680799874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.807678689591" Y="-29.103447303925" />
                  <Point X="-2.947522938796" Y="-26.364571562957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.14657162462" Y="-26.109800863138" />
                  <Point X="-3.724486466732" Y="-25.370103596857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.394102456462" Y="-24.513034214019" />
                  <Point X="-4.736174318457" Y="-24.07520219665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.83846593742" Y="-29.218347002008" />
                  <Point X="-2.976514789302" Y="-26.481769264822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.271805335522" Y="-26.103814601121" />
                  <Point X="-3.824174226495" Y="-25.396814661237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.546579348636" Y="-24.472178270099" />
                  <Point X="-4.755371713941" Y="-24.204936229261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.869253185249" Y="-29.333246700092" />
                  <Point X="-3.034674499941" Y="-26.56163380818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.381118360208" Y="-26.118205888205" />
                  <Point X="-3.92386196958" Y="-25.423525746964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.699056553661" Y="-24.431321925748" />
                  <Point X="-4.774569109426" Y="-24.334670261871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900040433078" Y="-29.448146398175" />
                  <Point X="-2.314119303553" Y="-27.63820798065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.539652304855" Y="-27.349538902851" />
                  <Point X="-3.109102324704" Y="-26.620676114992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.490431384895" Y="-26.132597175289" />
                  <Point X="-4.023549448514" Y="-25.450237170789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.930827680908" Y="-29.563046096259" />
                  <Point X="-1.171645494662" Y="-29.254813350661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.461161147635" Y="-28.884250213249" />
                  <Point X="-2.35256550214" Y="-27.7433046688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.6493008871" Y="-27.363500695844" />
                  <Point X="-3.184473917844" Y="-26.678510453368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.599744409581" Y="-26.146988462373" />
                  <Point X="-4.123236927448" Y="-25.476948594614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.961615173363" Y="-29.677945481235" />
                  <Point X="-1.130468777997" Y="-29.461822722918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.651274033091" Y="-28.795222394658" />
                  <Point X="-2.403795803352" Y="-27.83203845177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.735696256501" Y="-27.40722524404" />
                  <Point X="-3.259845510984" Y="-26.736344791744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.709057434267" Y="-26.161379749457" />
                  <Point X="-4.222924406382" Y="-25.503660018438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.017631115381" Y="-29.760553923301" />
                  <Point X="-1.126712616689" Y="-29.620935968474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.778336992931" Y="-28.786894800771" />
                  <Point X="-2.455026104563" Y="-27.920772234741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.818777175449" Y="-27.455192095359" />
                  <Point X="-3.335217104125" Y="-26.79417913012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.818370458953" Y="-26.175771036541" />
                  <Point X="-4.322611885316" Y="-25.530371442263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.905400510715" Y="-28.778566492746" />
                  <Point X="-2.506256405775" Y="-28.009506017711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.901858094397" Y="-27.503158946677" />
                  <Point X="-3.410588690798" Y="-26.852013476773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.92768348364" Y="-26.190162323625" />
                  <Point X="-4.42229936425" Y="-25.557082866088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.020665981036" Y="-28.785338996849" />
                  <Point X="-2.55748671678" Y="-28.098239788146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.984939013345" Y="-27.551125797996" />
                  <Point X="-3.485960113106" Y="-26.909848033804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036996508326" Y="-26.204553610709" />
                  <Point X="-4.521986843184" Y="-25.583794289913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.109876854184" Y="-28.825459864584" />
                  <Point X="-2.608717065927" Y="-28.186973509762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.068019932293" Y="-27.599092649314" />
                  <Point X="-3.561331535414" Y="-26.967682590836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146309533012" Y="-26.218944897793" />
                  <Point X="-4.621674322118" Y="-25.610505713737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.189084671584" Y="-28.87838405982" />
                  <Point X="-2.659947415074" Y="-28.275707231377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.151100851241" Y="-27.647059500632" />
                  <Point X="-3.636702957722" Y="-27.025517147867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255622557698" Y="-26.233336184877" />
                  <Point X="-4.721361801053" Y="-25.637217137562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.268291940652" Y="-28.931308956888" />
                  <Point X="-2.711177764222" Y="-28.364440952993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234181770189" Y="-27.695026351951" />
                  <Point X="-3.71207438003" Y="-27.083351704898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.364935582384" Y="-26.247727471961" />
                  <Point X="-4.777076635705" Y="-25.72021097948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.34749920972" Y="-28.984233853956" />
                  <Point X="-2.762408113369" Y="-28.453174674608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.317262689137" Y="-27.742993203269" />
                  <Point X="-3.787445802338" Y="-27.14118626193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.474248790366" Y="-26.262118524438" />
                  <Point X="-4.750062296263" Y="-25.909093335519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.416784426116" Y="-29.049858399316" />
                  <Point X="-2.813638462516" Y="-28.541908396224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.400343608085" Y="-27.790960054588" />
                  <Point X="-3.862817224646" Y="-27.199020818961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.583562203508" Y="-26.276509314321" />
                  <Point X="-4.702362484189" Y="-26.124451889162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.4765195653" Y="-29.127706486091" />
                  <Point X="-2.864868811663" Y="-28.630642117839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.483424527033" Y="-27.838926905906" />
                  <Point X="-3.938188646954" Y="-27.256855375992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.632417631512" Y="-29.082471639089" />
                  <Point X="-2.916099160811" Y="-28.719375839455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.566505445981" Y="-27.886893757225" />
                  <Point X="-4.013560069262" Y="-27.314689933024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.900931451516" Y="-28.893095200367" />
                  <Point X="-2.967329509958" Y="-28.80810956107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.649586377592" Y="-27.934860592335" />
                  <Point X="-4.08893149157" Y="-27.372524490055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73266733221" Y="-27.982827397998" />
                  <Point X="-4.164302913878" Y="-27.430359047086" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.582135803223" Y="-28.949865234375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.41911517334" Y="-28.4564140625" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.274335906982" Y="-28.266400390625" />
                  <Point X="0.20438583374" Y="-28.24469140625" />
                  <Point X="0.02097677803" Y="-28.187767578125" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.078614524841" Y="-28.2094765625" />
                  <Point X="-0.2620234375" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.333482635498" Y="-28.3507734375" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.749211608887" Y="-29.619349609375" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-0.88473828125" Y="-29.979896484375" />
                  <Point X="-1.100230224609" Y="-29.938068359375" />
                  <Point X="-1.180426147461" Y="-29.917435546875" />
                  <Point X="-1.351589477539" Y="-29.873396484375" />
                  <Point X="-1.322463623047" Y="-29.6521640625" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.326394287109" Y="-29.45074609375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.45068371582" Y="-29.146150390625" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240600586" Y="-28.985763671875" />
                  <Point X="-1.734715209961" Y="-28.980162109375" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.061100585938" Y="-29.02137890625" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.422538085938" Y="-29.36946484375" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.539942871094" Y="-29.363203125" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-2.966861083984" Y="-29.082125" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.711565185547" Y="-27.985111328125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.59759375" />
                  <Point X="-2.513979980469" Y="-27.568763671875" />
                  <Point X="-2.531328613281" Y="-27.551416015625" />
                  <Point X="-2.560157470703" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.525092041016" Y="-28.082375" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.912132568359" Y="-28.175015625" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.24130078125" Y="-27.713658203125" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.523400146484" Y="-26.6990859375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535400391" Y="-26.41108203125" />
                  <Point X="-3.143694580078" Y="-26.38780078125" />
                  <Point X="-3.1381171875" Y="-26.366265625" />
                  <Point X="-3.136651611328" Y="-26.35005078125" />
                  <Point X="-3.148656738281" Y="-26.3210703125" />
                  <Point X="-3.168470703125" Y="-26.306337890625" />
                  <Point X="-3.187641601562" Y="-26.2950546875" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.397868652344" Y="-26.443703125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.829784179688" Y="-26.393326171875" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.948452148438" Y="-25.86394921875" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.967117675781" Y="-25.238412109375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.534234375" Y="-25.116107421875" />
                  <Point X="-3.514143798828" Y="-25.1021640625" />
                  <Point X="-3.502324951172" Y="-25.090646484375" />
                  <Point X="-3.492344970703" Y="-25.0676796875" />
                  <Point X="-3.485648193359" Y="-25.046103515625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.488201904297" Y="-25.00823046875" />
                  <Point X="-3.494898925781" Y="-24.98665234375" />
                  <Point X="-3.502326660156" Y="-24.97191015625" />
                  <Point X="-3.521811523438" Y="-24.955076171875" />
                  <Point X="-3.541894042969" Y="-24.94113671875" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.631576171875" Y="-24.646107421875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.980552734375" Y="-24.428705078125" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.875252441406" Y="-23.847140625" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.046916015625" Y="-23.567361328125" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731703613281" Y="-23.6041328125" />
                  <Point X="-3.714744628906" Y="-23.598787109375" />
                  <Point X="-3.670277587891" Y="-23.584765625" />
                  <Point X="-3.651533935547" Y="-23.573943359375" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.632314941406" Y="-23.539787109375" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.624527099609" Y="-23.43871484375" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.276081542969" Y="-22.908017578125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.404451171875" Y="-22.6317734375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.047718994141" Y="-22.068654296875" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.339508789062" Y="-21.9689296875" />
                  <Point X="-3.159157226562" Y="-22.073056640625" />
                  <Point X="-3.138514648438" Y="-22.07956640625" />
                  <Point X="-3.114895263672" Y="-22.0816328125" />
                  <Point X="-3.052965332031" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.996487548828" Y="-22.055830078125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.940140625" Y="-21.948541015625" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.2250859375" Y="-21.3930859375" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-3.178607421875" Y="-21.152072265625" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.576014404297" Y="-20.727408203125" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.023037719727" Y="-20.640474609375" />
                  <Point X="-1.967826538086" Y="-20.71242578125" />
                  <Point X="-1.951247192383" Y="-20.726337890625" />
                  <Point X="-1.924958984375" Y="-20.7400234375" />
                  <Point X="-1.85603112793" Y="-20.77590625" />
                  <Point X="-1.835124267578" Y="-20.781509765625" />
                  <Point X="-1.81380859375" Y="-20.77775" />
                  <Point X="-1.786427612305" Y="-20.766408203125" />
                  <Point X="-1.714634643555" Y="-20.736669921875" />
                  <Point X="-1.696905151367" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.677171386719" Y="-20.67724609375" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.682956787109" Y="-20.34580859375" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.519229492188" Y="-20.251224609375" />
                  <Point X="-0.968082885742" Y="-20.096703125" />
                  <Point X="-0.753685791016" Y="-20.071609375" />
                  <Point X="-0.224199996948" Y="-20.009640625" />
                  <Point X="-0.095493347168" Y="-20.48998046875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282138824" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.03659405899" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.19434387207" Y="-20.167015625" />
                  <Point X="0.236648422241" Y="-20.0091328125" />
                  <Point X="0.378984008789" Y="-20.0240390625" />
                  <Point X="0.860210205078" Y="-20.074435546875" />
                  <Point X="1.037592773438" Y="-20.11726171875" />
                  <Point X="1.508456176758" Y="-20.230943359375" />
                  <Point X="1.623816650391" Y="-20.27278515625" />
                  <Point X="1.931044433594" Y="-20.38421875" />
                  <Point X="2.042680908203" Y="-20.436427734375" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.446559570312" Y="-20.63770703125" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.834246337891" Y="-20.876650390625" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.467740722656" Y="-22.084369140625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.218924560547" Y="-22.53065234375" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.204355957031" Y="-22.626841796875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218681884766" Y="-22.6991875" />
                  <Point X="2.230542236328" Y="-22.716666015625" />
                  <Point X="2.261639648438" Y="-22.76249609375" />
                  <Point X="2.274939697266" Y="-22.775796875" />
                  <Point X="2.292418457031" Y="-22.787658203125" />
                  <Point X="2.338248291016" Y="-22.818755859375" />
                  <Point X="2.360336669922" Y="-22.827021484375" />
                  <Point X="2.379504150391" Y="-22.82933203125" />
                  <Point X="2.429761474609" Y="-22.835392578125" />
                  <Point X="2.448665039062" Y="-22.8340546875" />
                  <Point X="2.470831298828" Y="-22.828126953125" />
                  <Point X="2.528951416016" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.621384277344" Y="-22.183841796875" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.032967529297" Y="-22.022380859375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.25930078125" Y="-22.35183203125" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.601816894531" Y="-23.16658984375" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.279371582031" Y="-23.41616796875" />
                  <Point X="3.263418457031" Y="-23.43698046875" />
                  <Point X="3.221589355469" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.207177001953" Y="-23.52975" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.195657714844" Y="-23.632677734375" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646240234" Y="-23.712044921875" />
                  <Point X="3.228914550781" Y="-23.7322109375" />
                  <Point X="3.263703857422" Y="-23.78508984375" />
                  <Point X="3.280947998047" Y="-23.8011796875" />
                  <Point X="3.300175537109" Y="-23.81200390625" />
                  <Point X="3.350590087891" Y="-23.8403828125" />
                  <Point X="3.368566162109" Y="-23.846380859375" />
                  <Point X="3.394562988281" Y="-23.84981640625" />
                  <Point X="3.462726806641" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.502063964844" Y="-23.72371875" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.8663359375" Y="-23.749359375" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.957059082031" Y="-24.16339453125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.100102539062" Y="-24.66599609375" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.703546142578" Y="-24.781943359375" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.622264648438" Y="-24.83307421875" />
                  <Point X="3.606939941406" Y="-24.8526015625" />
                  <Point X="3.566758789062" Y="-24.903802734375" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.551876708984" Y="-24.951939453125" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538482910156" Y="-25.040685546875" />
                  <Point X="3.543591308594" Y="-25.067357421875" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.582083496094" Y="-25.17828515625" />
                  <Point X="3.622264648438" Y="-25.229486328125" />
                  <Point X="3.636577148438" Y="-25.241908203125" />
                  <Point X="3.662118408203" Y="-25.256671875" />
                  <Point X="3.729087158203" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.682295410156" Y="-25.5525625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.989109375" Y="-25.69659375" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.925536621094" Y="-26.066728515625" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.828245849609" Y="-26.152431640625" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.3948359375" Y="-26.098341796875" />
                  <Point X="3.344707763672" Y="-26.109236328125" />
                  <Point X="3.213271728516" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.155145996094" Y="-26.191138671875" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.060015380859" Y="-26.361263671875" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.084102050781" Y="-26.559771484375" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.041832763672" Y="-27.355703125" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.318798828125" Y="-27.61659375" />
                  <Point X="4.204130859375" Y="-27.80214453125" />
                  <Point X="4.156782714844" Y="-27.869419921875" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.124193603516" Y="-27.473263671875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.677679931641" Y="-27.2425390625" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.439514404297" Y="-27.245330078125" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.262515136719" Y="-27.38424609375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.199937744141" Y="-27.60603515625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.795321044922" Y="-28.75065625" />
                  <Point X="2.986673828125" Y="-29.082087890625" />
                  <Point X="2.971359863281" Y="-29.09302734375" />
                  <Point X="2.835296142578" Y="-29.190212890625" />
                  <Point X="2.782360107422" Y="-29.224478515625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.966085571289" Y="-28.36078125" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670548950195" Y="-27.980466796875" />
                  <Point X="1.611707519531" Y="-27.94263671875" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425805297852" Y="-27.83571875" />
                  <Point X="1.361452026367" Y="-27.841640625" />
                  <Point X="1.192718383789" Y="-27.857166015625" />
                  <Point X="1.165332763672" Y="-27.868509765625" />
                  <Point X="1.115640991211" Y="-27.909828125" />
                  <Point X="0.985349060059" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.953599243164" Y="-28.11434375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.072977539062" Y="-29.518810546875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.123071289062" Y="-29.93503125" />
                  <Point X="0.994368469238" Y="-29.9632421875" />
                  <Point X="0.945438354492" Y="-29.972130859375" />
                  <Point X="0.860200439453" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#144" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.053352998312" Y="4.554240584169" Z="0.7" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.7" />
                  <Point X="-0.765653893088" Y="5.009330880415" Z="0.7" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.7" />
                  <Point X="-1.53888330275" Y="4.828200972733" Z="0.7" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.7" />
                  <Point X="-1.738685014862" Y="4.678946335746" Z="0.7" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.7" />
                  <Point X="-1.731013149215" Y="4.369069274014" Z="0.7" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.7" />
                  <Point X="-1.811718071487" Y="4.311066246672" Z="0.7" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.7" />
                  <Point X="-1.908026923497" Y="4.335606414254" Z="0.7" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.7" />
                  <Point X="-1.989526285562" Y="4.421243839443" Z="0.7" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.7" />
                  <Point X="-2.606453328131" Y="4.347579569456" Z="0.7" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.7" />
                  <Point X="-3.215185472508" Y="3.918887665597" Z="0.7" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.7" />
                  <Point X="-3.274543206752" Y="3.61319483068" Z="0.7" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.7" />
                  <Point X="-2.996106460286" Y="3.078382924333" Z="0.7" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.7" />
                  <Point X="-3.037998358773" Y="3.010805209114" Z="0.7" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.7" />
                  <Point X="-3.116693541274" Y="2.999458238887" Z="0.7" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.7" />
                  <Point X="-3.320664517617" Y="3.105650741994" Z="0.7" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.7" />
                  <Point X="-4.093338346231" Y="2.993328988567" Z="0.7" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.7" />
                  <Point X="-4.45378924524" Y="2.424712870077" Z="0.7" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.7" />
                  <Point X="-4.312675727185" Y="2.083594536508" Z="0.7" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.7" />
                  <Point X="-3.675033327029" Y="1.569477362828" Z="0.7" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.7" />
                  <Point X="-3.684664972897" Y="1.510628677537" Z="0.7" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.7" />
                  <Point X="-3.735936887104" Y="1.480179739505" Z="0.7" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.7" />
                  <Point X="-4.046545936777" Y="1.513492284786" Z="0.7" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.7" />
                  <Point X="-4.929668189389" Y="1.197217701228" Z="0.7" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.7" />
                  <Point X="-5.036170156351" Y="0.609893027973" Z="0.7" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.7" />
                  <Point X="-4.650673186543" Y="0.336876600282" Z="0.7" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.7" />
                  <Point X="-3.556470166445" Y="0.035125029704" Z="0.7" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.7" />
                  <Point X="-3.542110887939" Y="0.008229417671" Z="0.7" />
                  <Point X="-3.539556741714" Y="0" Z="0.7" />
                  <Point X="-3.546253713196" Y="-0.02157753339" Z="0.7" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.7" />
                  <Point X="-3.568898428678" Y="-0.04375095343" Z="0.7" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.7" />
                  <Point X="-3.98621460223" Y="-0.158835456474" Z="0.7" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.7" />
                  <Point X="-5.004104387898" Y="-0.83974596943" Z="0.7" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.7" />
                  <Point X="-4.884384936162" Y="-1.374420773663" Z="0.7" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.7" />
                  <Point X="-4.3974984588" Y="-1.461994625715" Z="0.7" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.7" />
                  <Point X="-3.199987896567" Y="-1.318146519182" Z="0.7" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.7" />
                  <Point X="-3.19825366347" Y="-1.343984203209" Z="0.7" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.7" />
                  <Point X="-3.559994216577" Y="-1.628138236735" Z="0.7" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.7" />
                  <Point X="-4.290400079136" Y="-2.707986658665" Z="0.7" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.7" />
                  <Point X="-3.958074331378" Y="-3.174017995086" Z="0.7" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.7" />
                  <Point X="-3.506247891613" Y="-3.094394550144" Z="0.7" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.7" />
                  <Point X="-2.560280735316" Y="-2.568049733816" Z="0.7" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.7" />
                  <Point X="-2.761022463025" Y="-2.928830490879" Z="0.7" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.7" />
                  <Point X="-3.003520936032" Y="-4.09046094365" Z="0.7" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.7" />
                  <Point X="-2.572420281254" Y="-4.374434056027" Z="0.7" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.7" />
                  <Point X="-2.389026236767" Y="-4.368622356368" Z="0.7" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.7" />
                  <Point X="-2.039478050199" Y="-4.031673492466" Z="0.7" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.7" />
                  <Point X="-1.744141488809" Y="-3.998773624641" Z="0.7" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.7" />
                  <Point X="-1.489807102284" Y="-4.15246215987" Z="0.7" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.7" />
                  <Point X="-1.381589590865" Y="-4.429220159762" Z="0.7" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.7" />
                  <Point X="-1.378191764104" Y="-4.614356200169" Z="0.7" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.7" />
                  <Point X="-1.199041045831" Y="-4.934578513964" Z="0.7" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.7" />
                  <Point X="-0.900421740017" Y="-4.997700173976" Z="0.7" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.7" />
                  <Point X="-0.707071366691" Y="-4.601010366945" Z="0.7" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.7" />
                  <Point X="-0.298562751171" Y="-3.348002549857" Z="0.7" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.7" />
                  <Point X="-0.069950048917" Y="-3.225949282224" Z="0.7" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.7" />
                  <Point X="0.183409030445" Y="-3.261162763064" Z="0.7" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.7" />
                  <Point X="0.371883108261" Y="-3.453643205911" Z="0.7" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.7" />
                  <Point X="0.52768349371" Y="-3.931525662871" Z="0.7" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.7" />
                  <Point X="0.948219414019" Y="-4.990046538207" Z="0.7" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.7" />
                  <Point X="1.127620944161" Y="-4.952590767458" Z="0.7" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.7" />
                  <Point X="1.116393894333" Y="-4.481003890599" Z="0.7" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.7" />
                  <Point X="0.996302414766" Y="-3.093682633852" Z="0.7" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.7" />
                  <Point X="1.141451941776" Y="-2.91699254984" Z="0.7" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.7" />
                  <Point X="1.359877418474" Y="-2.860148414285" Z="0.7" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.7" />
                  <Point X="1.578512527988" Y="-2.953415706167" Z="0.7" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.7" />
                  <Point X="1.920261894051" Y="-3.35993807436" Z="0.7" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.7" />
                  <Point X="2.803373273275" Y="-4.235172633202" Z="0.7" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.7" />
                  <Point X="2.994265496227" Y="-4.102433867319" Z="0.7" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.7" />
                  <Point X="2.83246630322" Y="-3.694376062426" Z="0.7" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.7" />
                  <Point X="2.242985763225" Y="-2.565868711167" Z="0.7" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.7" />
                  <Point X="2.300605414219" Y="-2.376253416991" Z="0.7" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.7" />
                  <Point X="2.456644961381" Y="-2.258295895748" Z="0.7" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.7" />
                  <Point X="2.662638092345" Y="-2.260462246266" Z="0.7" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.7" />
                  <Point X="3.093037534681" Y="-2.485283197054" Z="0.7" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.7" />
                  <Point X="4.191514831845" Y="-2.866915817686" Z="0.7" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.7" />
                  <Point X="4.355175886266" Y="-2.611598338208" Z="0.7" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.7" />
                  <Point X="4.066114754763" Y="-2.284754971357" Z="0.7" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.7" />
                  <Point X="3.1200041733" Y="-1.501452837403" Z="0.7" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.7" />
                  <Point X="3.103648842307" Y="-1.334564463649" Z="0.7" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.7" />
                  <Point X="3.187436227175" Y="-1.191824836551" Z="0.7" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.7" />
                  <Point X="3.349171663013" Y="-1.126815995452" Z="0.7" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.7" />
                  <Point X="3.815563489917" Y="-1.170722566858" Z="0.7" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.7" />
                  <Point X="4.968127608171" Y="-1.046573803354" Z="0.7" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.7" />
                  <Point X="5.032394817167" Y="-0.672767447834" Z="0.7" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.7" />
                  <Point X="4.689080162713" Y="-0.472984968998" Z="0.7" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.7" />
                  <Point X="3.680983847814" Y="-0.182101440123" Z="0.7" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.7" />
                  <Point X="3.615261403583" Y="-0.116137774597" Z="0.7" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.7" />
                  <Point X="3.58654295334" Y="-0.02667325524" Z="0.7" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.7" />
                  <Point X="3.594828497084" Y="0.069937275988" Z="0.7" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.7" />
                  <Point X="3.640118034817" Y="0.147810963978" Z="0.7" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.7" />
                  <Point X="3.722411566538" Y="0.206047404119" Z="0.7" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.7" />
                  <Point X="4.106887654593" Y="0.316986963771" Z="0.7" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.7" />
                  <Point X="5.000307868661" Y="0.875577168352" Z="0.7" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.7" />
                  <Point X="4.908761008699" Y="1.293748032518" Z="0.7" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.7" />
                  <Point X="4.489381810127" Y="1.357133869027" Z="0.7" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.7" />
                  <Point X="3.394957525212" Y="1.231032771438" Z="0.7" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.7" />
                  <Point X="3.318546068957" Y="1.262847604337" Z="0.7" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.7" />
                  <Point X="3.26452923323" Y="1.326549266376" Z="0.7" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.7" />
                  <Point X="3.238470267049" Y="1.40870679742" Z="0.7" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.7" />
                  <Point X="3.249173425876" Y="1.48806453171" Z="0.7" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.7" />
                  <Point X="3.296945046753" Y="1.563883008694" Z="0.7" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.7" />
                  <Point X="3.626099072717" Y="1.825022606247" Z="0.7" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.7" />
                  <Point X="4.295922214088" Y="2.705334346938" Z="0.7" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.7" />
                  <Point X="4.067397222373" Y="3.038102237239" Z="0.7" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.7" />
                  <Point X="3.590228243866" Y="2.890739282489" Z="0.7" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.7" />
                  <Point X="2.451757935059" Y="2.251456726672" Z="0.7" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.7" />
                  <Point X="2.379334338358" Y="2.251589276414" Z="0.7" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.7" />
                  <Point X="2.314337010788" Y="2.284997900013" Z="0.7" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.7" />
                  <Point X="2.265760700222" Y="2.342687849594" Z="0.7" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.7" />
                  <Point X="2.247840426344" Y="2.410424105474" Z="0.7" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.7" />
                  <Point X="2.261071215986" Y="2.487711640823" Z="0.7" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.7" />
                  <Point X="2.50488607037" Y="2.921910838089" Z="0.7" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.7" />
                  <Point X="2.857067342783" Y="4.195379092222" Z="0.7" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.7" />
                  <Point X="2.465573629946" Y="4.436777312369" Z="0.7" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.7" />
                  <Point X="2.05770641194" Y="4.640144369404" Z="0.7" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.7" />
                  <Point X="1.634709323692" Y="4.805499789967" Z="0.7" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.7" />
                  <Point X="1.043171283477" Y="4.962622515719" Z="0.7" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.7" />
                  <Point X="0.378035863475" Y="5.056970447716" Z="0.7" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.7" />
                  <Point X="0.139891563241" Y="4.877206921782" Z="0.7" />
                  <Point X="0" Y="4.355124473572" Z="0.7" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>