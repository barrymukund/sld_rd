<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#159" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1677" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.729096801758" Y="-29.131279296875" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542363037109" Y="-28.467375" />
                  <Point X="0.474598480225" Y="-28.369740234375" />
                  <Point X="0.378635375977" Y="-28.231474609375" />
                  <Point X="0.356752044678" Y="-28.20901953125" />
                  <Point X="0.330496551514" Y="-28.189775390625" />
                  <Point X="0.302495025635" Y="-28.17566796875" />
                  <Point X="0.197633346558" Y="-28.143123046875" />
                  <Point X="0.049135883331" Y="-28.09703515625" />
                  <Point X="0.020976638794" Y="-28.092765625" />
                  <Point X="-0.00866468811" Y="-28.092765625" />
                  <Point X="-0.036823932648" Y="-28.09703515625" />
                  <Point X="-0.14168560791" Y="-28.129580078125" />
                  <Point X="-0.290182922363" Y="-28.17566796875" />
                  <Point X="-0.318184906006" Y="-28.18977734375" />
                  <Point X="-0.344440093994" Y="-28.209021484375" />
                  <Point X="-0.366323577881" Y="-28.2314765625" />
                  <Point X="-0.434088287354" Y="-28.32911328125" />
                  <Point X="-0.530051391602" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.785776489258" Y="-29.388759765625" />
                  <Point X="-0.916584533691" Y="-29.87694140625" />
                  <Point X="-1.079323974609" Y="-29.845353515625" />
                  <Point X="-1.197500244141" Y="-29.814947265625" />
                  <Point X="-1.24641784668" Y="-29.802361328125" />
                  <Point X="-1.234920776367" Y="-29.715033203125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.241560180664" Y="-29.390283203125" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131203125" />
                  <Point X="-1.420188110352" Y="-29.046537109375" />
                  <Point X="-1.556905639648" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886474609" Y="-28.897994140625" />
                  <Point X="-1.64302734375" Y="-28.890966796875" />
                  <Point X="-1.771162231445" Y="-28.882568359375" />
                  <Point X="-1.952616577148" Y="-28.87067578125" />
                  <Point X="-1.983414794922" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042657348633" Y="-28.89480078125" />
                  <Point X="-2.149426025391" Y="-28.966140625" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102539062" Y="-29.099462890625" />
                  <Point X="-2.466916748047" Y="-29.27124609375" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.550473144531" Y="-29.2449453125" />
                  <Point X="-2.801727294922" Y="-29.089375" />
                  <Point X="-2.96532421875" Y="-28.96341015625" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.731872314453" Y="-28.210283203125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412859130859" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616126953125" />
                  <Point X="-2.405575439453" Y="-27.585193359375" />
                  <Point X="-2.414559570312" Y="-27.555576171875" />
                  <Point X="-2.428776855469" Y="-27.52674609375" />
                  <Point X="-2.446806640625" Y="-27.5015859375" />
                  <Point X="-2.464155273438" Y="-27.48423828125" />
                  <Point X="-2.489311523438" Y="-27.466212890625" />
                  <Point X="-2.518140625" Y="-27.45199609375" />
                  <Point X="-2.547758789062" Y="-27.44301171875" />
                  <Point X="-2.578693115234" Y="-27.444025390625" />
                  <Point X="-2.610218994141" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.394918701172" Y="-27.8975234375" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.884370117188" Y="-28.05463671875" />
                  <Point X="-4.082862548828" Y="-27.793857421875" />
                  <Point X="-4.200157226562" Y="-27.597171875" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.645049072266" Y="-26.91217578125" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.35965625" />
                  <Point X="-3.045556884766" Y="-26.327984375" />
                  <Point X="-3.05255859375" Y="-26.298240234375" />
                  <Point X="-3.068641357422" Y="-26.272255859375" />
                  <Point X="-3.089475097656" Y="-26.248298828125" />
                  <Point X="-3.112975341797" Y="-26.228765625" />
                  <Point X="-3.139458007813" Y="-26.2131796875" />
                  <Point X="-3.168722167969" Y="-26.201955078125" />
                  <Point X="-3.200608154297" Y="-26.195474609375" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.185973144531" Y="-26.319986328125" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.756446289062" Y="-26.296578125" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.865111328125" Y="-25.775669921875" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.146985839844" Y="-25.3849609375" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.5109453125" Y="-25.211478515625" />
                  <Point X="-3.483891845703" Y="-25.19628515625" />
                  <Point X="-3.470523925781" Y="-25.18719921875" />
                  <Point X="-3.442438720703" Y="-25.164392578125" />
                  <Point X="-3.426203369141" Y="-25.147484375" />
                  <Point X="-3.414603027344" Y="-25.12711328125" />
                  <Point X="-3.403348144531" Y="-25.1000390625" />
                  <Point X="-3.398641601563" Y="-25.085525390625" />
                  <Point X="-3.390972167969" Y="-25.053234375" />
                  <Point X="-3.388402832031" Y="-25.031904296875" />
                  <Point X="-3.390692382812" Y="-25.010541015625" />
                  <Point X="-3.396768066406" Y="-24.9833828125" />
                  <Point X="-3.401238037109" Y="-24.968923828125" />
                  <Point X="-3.414087158203" Y="-24.936712890625" />
                  <Point X="-3.425393798828" Y="-24.91617578125" />
                  <Point X="-3.441385498047" Y="-24.899033203125" />
                  <Point X="-3.464687011719" Y="-24.879548828125" />
                  <Point X="-3.477870605469" Y="-24.8703046875" />
                  <Point X="-3.509707275391" Y="-24.851791015625" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.402532226562" Y="-24.609126953125" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.87451953125" Y="-24.361134765625" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.762017578125" Y="-23.7924921875" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.205875976562" Y="-23.64225390625" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744984863281" Y="-23.700658203125" />
                  <Point X="-3.723423583984" Y="-23.698771484375" />
                  <Point X="-3.703138916016" Y="-23.694736328125" />
                  <Point X="-3.677715576172" Y="-23.686720703125" />
                  <Point X="-3.641712890625" Y="-23.675369140625" />
                  <Point X="-3.622784667969" Y="-23.667041015625" />
                  <Point X="-3.604040283203" Y="-23.656220703125" />
                  <Point X="-3.587355224609" Y="-23.64398828125" />
                  <Point X="-3.573714111328" Y="-23.62843359375" />
                  <Point X="-3.561299560547" Y="-23.610703125" />
                  <Point X="-3.551351806641" Y="-23.5925703125" />
                  <Point X="-3.541150634766" Y="-23.567943359375" />
                  <Point X="-3.526704345703" Y="-23.53306640625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.532050292969" Y="-23.41062109375" />
                  <Point X="-3.544359130859" Y="-23.3869765625" />
                  <Point X="-3.561790039062" Y="-23.3534921875" />
                  <Point X="-3.573281982422" Y="-23.33629296875" />
                  <Point X="-3.587194580078" Y="-23.319712890625" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-4.10097265625" Y="-22.922638671875" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.275557128906" Y="-22.599400390625" />
                  <Point X="-4.081156982422" Y="-22.26634765625" />
                  <Point X="-3.915673095703" Y="-22.053638671875" />
                  <Point X="-3.750504882813" Y="-21.84133984375" />
                  <Point X="-3.477020751953" Y="-21.999234375" />
                  <Point X="-3.206656738281" Y="-22.155330078125" />
                  <Point X="-3.187729003906" Y="-22.163658203125" />
                  <Point X="-3.167086669922" Y="-22.17016796875" />
                  <Point X="-3.146794189453" Y="-22.174205078125" />
                  <Point X="-3.111386474609" Y="-22.177302734375" />
                  <Point X="-3.061244873047" Y="-22.181689453125" />
                  <Point X="-3.040560546875" Y="-22.18123828125" />
                  <Point X="-3.019102050781" Y="-22.178412109375" />
                  <Point X="-2.999012939453" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.920945068359" Y="-22.114638671875" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.846533691406" Y="-21.92847265625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.090844726562" Y="-21.43559765625" />
                  <Point X="-3.183332519531" Y="-21.275404296875" />
                  <Point X="-3.039211181641" Y="-21.164908203125" />
                  <Point X="-2.70062109375" Y="-20.905314453125" />
                  <Point X="-2.440001220703" Y="-20.76051953125" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.125962158203" Y="-20.66239453125" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028892333984" Y="-20.78519921875" />
                  <Point X="-2.012312866211" Y="-20.799111328125" />
                  <Point X="-1.995114868164" Y="-20.810603515625" />
                  <Point X="-1.955706176758" Y="-20.831119140625" />
                  <Point X="-1.899898803711" Y="-20.860171875" />
                  <Point X="-1.880625854492" Y="-20.86766796875" />
                  <Point X="-1.859719482422" Y="-20.873271484375" />
                  <Point X="-1.83926940918" Y="-20.876419921875" />
                  <Point X="-1.818622802734" Y="-20.87506640625" />
                  <Point X="-1.797307495117" Y="-20.871306640625" />
                  <Point X="-1.777451416016" Y="-20.865517578125" />
                  <Point X="-1.736404663086" Y="-20.848513671875" />
                  <Point X="-1.678277587891" Y="-20.8244375" />
                  <Point X="-1.660147583008" Y="-20.814490234375" />
                  <Point X="-1.642417602539" Y="-20.802076171875" />
                  <Point X="-1.626865234375" Y="-20.7884375" />
                  <Point X="-1.614633666992" Y="-20.771755859375" />
                  <Point X="-1.603811523438" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.582120239258" Y="-20.691705078125" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.582861206055" Y="-20.37828515625" />
                  <Point X="-1.584201660156" Y="-20.368103515625" />
                  <Point X="-1.387955200195" Y="-20.31308203125" />
                  <Point X="-0.94962322998" Y="-20.19019140625" />
                  <Point X="-0.633684936523" Y="-20.153212890625" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.213884414673" Y="-20.41519140625" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113990784" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425941467" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.259478546143" Y="-20.29098046875" />
                  <Point X="0.307419586182" Y="-20.1120625" />
                  <Point X="0.461317565918" Y="-20.1281796875" />
                  <Point X="0.844044616699" Y="-20.16826171875" />
                  <Point X="1.105439819336" Y="-20.23137109375" />
                  <Point X="1.4810234375" Y="-20.322048828125" />
                  <Point X="1.650303833008" Y="-20.383447265625" />
                  <Point X="1.894643920898" Y="-20.4720703125" />
                  <Point X="2.059170410156" Y="-20.549015625" />
                  <Point X="2.294560302734" Y="-20.659099609375" />
                  <Point X="2.453543212891" Y="-20.75172265625" />
                  <Point X="2.680972167969" Y="-20.88422265625" />
                  <Point X="2.830880126953" Y="-20.990830078125" />
                  <Point X="2.943259033203" Y="-21.070748046875" />
                  <Point X="2.504196533203" Y="-21.8312265625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.124190673828" Y="-22.51717578125" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.111192871094" Y="-22.64778125" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140070556641" Y="-22.75252734375" />
                  <Point X="2.157850097656" Y="-22.77873046875" />
                  <Point X="2.183028320312" Y="-22.8158359375" />
                  <Point X="2.194464355469" Y="-22.829669921875" />
                  <Point X="2.221597900391" Y="-22.85440625" />
                  <Point X="2.247800292969" Y="-22.872185546875" />
                  <Point X="2.284906494141" Y="-22.897365234375" />
                  <Point X="2.304947753906" Y="-22.9077265625" />
                  <Point X="2.327037841797" Y="-22.915994140625" />
                  <Point X="2.348964355469" Y="-22.921337890625" />
                  <Point X="2.377698242188" Y="-22.924802734375" />
                  <Point X="2.418389160156" Y="-22.929708984375" />
                  <Point X="2.436469238281" Y="-22.93015625" />
                  <Point X="2.473208496094" Y="-22.925830078125" />
                  <Point X="2.506437988281" Y="-22.916943359375" />
                  <Point X="2.553494873047" Y="-22.904359375" />
                  <Point X="2.565288085938" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.463240966797" Y="-22.384841796875" />
                  <Point X="3.967325195312" Y="-22.09380859375" />
                  <Point X="3.988364013672" Y="-22.123048828125" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.206840820313" Y="-22.44863671875" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.700301513672" Y="-22.971275390625" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221422363281" Y="-23.33976171875" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.18005859375" Y="-23.389572265625" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.136606201172" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.112721435547" Y="-23.51476953125" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.105052490234" Y="-23.663673828125" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679443359" Y="-23.731021484375" />
                  <Point X="3.136282958984" Y="-23.76426171875" />
                  <Point X="3.156173339844" Y="-23.794494140625" />
                  <Point X="3.184340576172" Y="-23.837306640625" />
                  <Point X="3.198895019531" Y="-23.854552734375" />
                  <Point X="3.216137451172" Y="-23.870640625" />
                  <Point X="3.234347412109" Y="-23.88396484375" />
                  <Point X="3.263171142578" Y="-23.900189453125" />
                  <Point X="3.303989501953" Y="-23.92316796875" />
                  <Point X="3.320522216797" Y="-23.9305" />
                  <Point X="3.356119628906" Y="-23.9405625" />
                  <Point X="3.395091308594" Y="-23.945712890625" />
                  <Point X="3.450280273438" Y="-23.953005859375" />
                  <Point X="3.462698730469" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.319116699219" Y="-23.843625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.787994140625" Y="-23.82918359375" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.872270996094" Y="-24.2363359375" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.254657226562" Y="-24.526232421875" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704787597656" Y="-24.674416015625" />
                  <Point X="3.681547363281" Y="-24.684931640625" />
                  <Point X="3.643258789062" Y="-24.7070625" />
                  <Point X="3.589037353516" Y="-24.73840234375" />
                  <Point X="3.574309814453" Y="-24.748904296875" />
                  <Point X="3.547530517578" Y="-24.774423828125" />
                  <Point X="3.524557373047" Y="-24.803697265625" />
                  <Point X="3.492024658203" Y="-24.84515234375" />
                  <Point X="3.48030078125" Y="-24.86443359375" />
                  <Point X="3.47052734375" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.456023193359" Y="-24.9473828125" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443483154297" Y="-25.021876953125" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.452836669922" Y="-25.0985390625" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.47052734375" Y="-25.1766640625" />
                  <Point X="3.48030078125" Y="-25.198126953125" />
                  <Point X="3.492024658203" Y="-25.217408203125" />
                  <Point X="3.514997802734" Y="-25.246681640625" />
                  <Point X="3.547530517578" Y="-25.28813671875" />
                  <Point X="3.559993652344" Y="-25.30123046875" />
                  <Point X="3.589036132812" Y="-25.324158203125" />
                  <Point X="3.627324707031" Y="-25.3462890625" />
                  <Point X="3.681546142578" Y="-25.377630859375" />
                  <Point X="3.692709228516" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.478564941406" Y="-25.59632421875" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.887375" Y="-25.734140625" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.821283203125" Y="-26.096578125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.048400390625" Y="-26.085595703125" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374658691406" Y="-26.005509765625" />
                  <Point X="3.29951171875" Y="-26.02184375" />
                  <Point X="3.193094482422" Y="-26.04497265625" />
                  <Point X="3.163974609375" Y="-26.05659765625" />
                  <Point X="3.136147705078" Y="-26.073490234375" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.066975585938" Y="-26.14858984375" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.963247558594" Y="-26.376111328125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.018038085938" Y="-26.63268359375" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.817755126953" Y="-27.3035078125" />
                  <Point X="4.213122558594" Y="-27.6068828125" />
                  <Point X="4.124813964844" Y="-27.74978125" />
                  <Point X="4.055034179688" Y="-27.848927734375" />
                  <Point X="4.028980712891" Y="-27.8859453125" />
                  <Point X="3.356727294922" Y="-27.4978203125" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754224609375" Y="-27.159826171875" />
                  <Point X="2.664787841797" Y="-27.143673828125" />
                  <Point X="2.538134277344" Y="-27.12080078125" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444833496094" Y="-27.135177734375" />
                  <Point X="2.370533447266" Y="-27.17428125" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531494141" Y="-27.290439453125" />
                  <Point X="2.165427978516" Y="-27.364740234375" />
                  <Point X="2.110052490234" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.111827392578" Y="-27.6526953125" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.606219238281" Y="-28.613123046875" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.781833496094" Y="-29.111654296875" />
                  <Point X="2.703836914063" Y="-29.162140625" />
                  <Point X="2.701764648438" Y="-29.163482421875" />
                  <Point X="2.182652099609" Y="-28.4869609375" />
                  <Point X="1.758546020508" Y="-27.934255859375" />
                  <Point X="1.747507446289" Y="-27.92218359375" />
                  <Point X="1.721923461914" Y="-27.900556640625" />
                  <Point X="1.63371472168" Y="-27.84384765625" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.751166015625" />
                  <Point X="1.448365844727" Y="-27.743435546875" />
                  <Point X="1.417100341797" Y="-27.741119140625" />
                  <Point X="1.320629150391" Y="-27.74999609375" />
                  <Point X="1.184013549805" Y="-27.76256640625" />
                  <Point X="1.156362792969" Y="-27.7693984375" />
                  <Point X="1.128977050781" Y="-27.7807421875" />
                  <Point X="1.104594970703" Y="-27.795462890625" />
                  <Point X="1.030102294922" Y="-27.85740234375" />
                  <Point X="0.924611206055" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.853351379395" Y="-28.128283203125" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.94851574707" Y="-29.301251953125" />
                  <Point X="1.022065429688" Y="-29.859916015625" />
                  <Point X="0.975718505859" Y="-29.87007421875" />
                  <Point X="0.929315246582" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058415405273" Y="-29.752638671875" />
                  <Point X="-1.14124609375" Y="-29.731326171875" />
                  <Point X="-1.140733520508" Y="-29.72743359375" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759155273" Y="-29.509330078125" />
                  <Point X="-1.123333984375" Y="-29.497693359375" />
                  <Point X="-1.148385620117" Y="-29.37175" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026367188" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230573242188" Y="-29.094861328125" />
                  <Point X="-1.250208251953" Y="-29.070935546875" />
                  <Point X="-1.261006958008" Y="-29.05977734375" />
                  <Point X="-1.357550292969" Y="-28.975111328125" />
                  <Point X="-1.494267822266" Y="-28.855212890625" />
                  <Point X="-1.506740112305" Y="-28.84596484375" />
                  <Point X="-1.533023681641" Y="-28.82962109375" />
                  <Point X="-1.546834838867" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591315795898" Y="-28.805474609375" />
                  <Point X="-1.621456665039" Y="-28.798447265625" />
                  <Point X="-1.636813964844" Y="-28.796169921875" />
                  <Point X="-1.764948852539" Y="-28.787771484375" />
                  <Point X="-1.946403198242" Y="-28.77587890625" />
                  <Point X="-1.961927612305" Y="-28.7761328125" />
                  <Point X="-1.992725952148" Y="-28.779166015625" />
                  <Point X="-2.007999511719" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053667724609" Y="-28.795494140625" />
                  <Point X="-2.081861816406" Y="-28.808267578125" />
                  <Point X="-2.095436279297" Y="-28.815810546875" />
                  <Point X="-2.202204833984" Y="-28.887150390625" />
                  <Point X="-2.353402587891" Y="-28.988177734375" />
                  <Point X="-2.359682373047" Y="-28.992755859375" />
                  <Point X="-2.380451416016" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410470947266" Y="-29.041630859375" />
                  <Point X="-2.503200927734" Y="-29.162478515625" />
                  <Point X="-2.747611572266" Y="-29.01114453125" />
                  <Point X="-2.907366699219" Y="-28.888138671875" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.649599853516" Y="-28.257783203125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684570312" Y="-27.666185546875" />
                  <Point X="-2.313413574219" Y="-27.63466015625" />
                  <Point X="-2.311638916016" Y="-27.619236328125" />
                  <Point X="-2.310626220703" Y="-27.588302734375" />
                  <Point X="-2.314666015625" Y="-27.5576171875" />
                  <Point X="-2.323650146484" Y="-27.528" />
                  <Point X="-2.329356445312" Y="-27.51355859375" />
                  <Point X="-2.343573730469" Y="-27.484728515625" />
                  <Point X="-2.351556884766" Y="-27.47141015625" />
                  <Point X="-2.369586669922" Y="-27.44625" />
                  <Point X="-2.379633300781" Y="-27.434408203125" />
                  <Point X="-2.396981933594" Y="-27.417060546875" />
                  <Point X="-2.408822509766" Y="-27.407015625" />
                  <Point X="-2.433978759766" Y="-27.388990234375" />
                  <Point X="-2.447294433594" Y="-27.381009765625" />
                  <Point X="-2.476123535156" Y="-27.36679296875" />
                  <Point X="-2.490564208984" Y="-27.3610859375" />
                  <Point X="-2.520182373047" Y="-27.3521015625" />
                  <Point X="-2.550870117188" Y="-27.3480625" />
                  <Point X="-2.581804443359" Y="-27.349076171875" />
                  <Point X="-2.597228515625" Y="-27.3508515625" />
                  <Point X="-2.628754394531" Y="-27.357123046875" />
                  <Point X="-2.643685058594" Y="-27.36138671875" />
                  <Point X="-2.672649658203" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.442418701172" Y="-27.815251953125" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-3.808776855469" Y="-27.99709765625" />
                  <Point X="-4.004020019531" Y="-27.740587890625" />
                  <Point X="-4.118564453125" Y="-27.548513671875" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.587216796875" Y="-26.987544921875" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.398802734375" />
                  <Point X="-2.948748535156" Y="-26.368373046875" />
                  <Point X="-2.948577880859" Y="-26.353044921875" />
                  <Point X="-2.950787109375" Y="-26.321373046875" />
                  <Point X="-2.953084472656" Y="-26.306216796875" />
                  <Point X="-2.960086181641" Y="-26.27647265625" />
                  <Point X="-2.971779541016" Y="-26.2482421875" />
                  <Point X="-2.987862304688" Y="-26.2222578125" />
                  <Point X="-2.996956054688" Y="-26.209916015625" />
                  <Point X="-3.017789794922" Y="-26.185958984375" />
                  <Point X="-3.02875" Y="-26.175240234375" />
                  <Point X="-3.052250244141" Y="-26.15570703125" />
                  <Point X="-3.064790283203" Y="-26.146892578125" />
                  <Point X="-3.091272949219" Y="-26.131306640625" />
                  <Point X="-3.105436523438" Y="-26.12448046875" />
                  <Point X="-3.134700683594" Y="-26.113255859375" />
                  <Point X="-3.149801269531" Y="-26.108857421875" />
                  <Point X="-3.181687255859" Y="-26.102376953125" />
                  <Point X="-3.197304443359" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.198373046875" Y="-26.225798828125" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.664401367188" Y="-26.27306640625" />
                  <Point X="-4.740762207031" Y="-25.974115234375" />
                  <Point X="-4.771068359375" Y="-25.76221875" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.122397949219" Y="-25.476724609375" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.497043457031" Y="-25.308392578125" />
                  <Point X="-3.475112792969" Y="-25.2994609375" />
                  <Point X="-3.464426757812" Y="-25.294310546875" />
                  <Point X="-3.437373291016" Y="-25.2791171875" />
                  <Point X="-3.430489501953" Y="-25.27485546875" />
                  <Point X="-3.410637451172" Y="-25.2609453125" />
                  <Point X="-3.382552246094" Y="-25.238138671875" />
                  <Point X="-3.3739140625" Y="-25.23019140625" />
                  <Point X="-3.357678710938" Y="-25.213283203125" />
                  <Point X="-3.343650146484" Y="-25.194494140625" />
                  <Point X="-3.332049804688" Y="-25.174123046875" />
                  <Point X="-3.326880859375" Y="-25.163580078125" />
                  <Point X="-3.315625976562" Y="-25.136505859375" />
                  <Point X="-3.312980957031" Y="-25.12934375" />
                  <Point X="-3.306212890625" Y="-25.107478515625" />
                  <Point X="-3.298543457031" Y="-25.0751875" />
                  <Point X="-3.296654052734" Y="-25.064595703125" />
                  <Point X="-3.294084716797" Y="-25.043265625" />
                  <Point X="-3.293943847656" Y="-25.02178125" />
                  <Point X="-3.296233398438" Y="-25.00041796875" />
                  <Point X="-3.297983886719" Y="-24.98980078125" />
                  <Point X="-3.304059570312" Y="-24.962642578125" />
                  <Point X="-3.306006347656" Y="-24.95532421875" />
                  <Point X="-3.312999511719" Y="-24.933724609375" />
                  <Point X="-3.325848632812" Y="-24.901513671875" />
                  <Point X="-3.330865966797" Y="-24.890896484375" />
                  <Point X="-3.342172607422" Y="-24.870359375" />
                  <Point X="-3.355927246094" Y="-24.851373046875" />
                  <Point X="-3.371918945313" Y="-24.83423046875" />
                  <Point X="-3.3804453125" Y="-24.826154296875" />
                  <Point X="-3.403746826172" Y="-24.806669921875" />
                  <Point X="-3.410146240234" Y="-24.801765625" />
                  <Point X="-3.430114013672" Y="-24.788181640625" />
                  <Point X="-3.461950683594" Y="-24.76966796875" />
                  <Point X="-3.473210693359" Y="-24.76408203125" />
                  <Point X="-3.496379394531" Y="-24.75444140625" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.377944335938" Y="-24.51736328125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.78054296875" Y="-24.375041015625" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.670324707031" Y="-23.81733984375" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.218276367188" Y="-23.73644140625" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747056884766" Y="-23.795634765625" />
                  <Point X="-3.736703613281" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704888671875" Y="-23.7919453125" />
                  <Point X="-3.684604003906" Y="-23.78791015625" />
                  <Point X="-3.674572998047" Y="-23.78533984375" />
                  <Point X="-3.649149658203" Y="-23.77732421875" />
                  <Point X="-3.613146972656" Y="-23.76597265625" />
                  <Point X="-3.603453857422" Y="-23.76232421875" />
                  <Point X="-3.584525634766" Y="-23.75399609375" />
                  <Point X="-3.575290527344" Y="-23.74931640625" />
                  <Point X="-3.556546142578" Y="-23.73849609375" />
                  <Point X="-3.547870605469" Y="-23.7328359375" />
                  <Point X="-3.531185546875" Y="-23.720603515625" />
                  <Point X="-3.515930419922" Y="-23.706626953125" />
                  <Point X="-3.502289306641" Y="-23.691072265625" />
                  <Point X="-3.495893798828" Y="-23.682921875" />
                  <Point X="-3.483479248047" Y="-23.66519140625" />
                  <Point X="-3.478010009766" Y="-23.656396484375" />
                  <Point X="-3.468062255859" Y="-23.638263671875" />
                  <Point X="-3.463583740234" Y="-23.62892578125" />
                  <Point X="-3.453382568359" Y="-23.604298828125" />
                  <Point X="-3.438936279297" Y="-23.569421875" />
                  <Point X="-3.435499267578" Y="-23.5596484375" />
                  <Point X="-3.429710449219" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436012207031" Y="-23.39546875" />
                  <Point X="-3.443509521484" Y="-23.376189453125" />
                  <Point X="-3.447784667969" Y="-23.36675390625" />
                  <Point X="-3.460093505859" Y="-23.343109375" />
                  <Point X="-3.477524414062" Y="-23.309625" />
                  <Point X="-3.482800048828" Y="-23.300712890625" />
                  <Point X="-3.494291992188" Y="-23.283513671875" />
                  <Point X="-3.500508300781" Y="-23.2752265625" />
                  <Point X="-3.514420898438" Y="-23.258646484375" />
                  <Point X="-3.521502197266" Y="-23.251087890625" />
                  <Point X="-3.536443603516" Y="-23.23678515625" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-4.043140380859" Y="-22.84726953125" />
                  <Point X="-4.227614746094" Y="-22.70571875" />
                  <Point X="-4.193510742187" Y="-22.6472890625" />
                  <Point X="-4.002296875" Y="-22.3196953125" />
                  <Point X="-3.840692138672" Y="-22.11197265625" />
                  <Point X="-3.726339111328" Y="-21.96498828125" />
                  <Point X="-3.524520507812" Y="-22.0815078125" />
                  <Point X="-3.254156494141" Y="-22.237603515625" />
                  <Point X="-3.244916503906" Y="-22.24228515625" />
                  <Point X="-3.225988769531" Y="-22.25061328125" />
                  <Point X="-3.216301025391" Y="-22.254259765625" />
                  <Point X="-3.195658691406" Y="-22.26076953125" />
                  <Point X="-3.185623291016" Y="-22.263341796875" />
                  <Point X="-3.165330810547" Y="-22.26737890625" />
                  <Point X="-3.155073730469" Y="-22.26884375" />
                  <Point X="-3.119666015625" Y="-22.27194140625" />
                  <Point X="-3.069524414062" Y="-22.276328125" />
                  <Point X="-3.059173095703" Y="-22.276666015625" />
                  <Point X="-3.038488769531" Y="-22.27621484375" />
                  <Point X="-3.028155761719" Y="-22.27542578125" />
                  <Point X="-3.006697265625" Y="-22.272599609375" />
                  <Point X="-2.996512451172" Y="-22.2706875" />
                  <Point X="-2.976423339844" Y="-22.26576953125" />
                  <Point X="-2.956999267578" Y="-22.25869921875" />
                  <Point X="-2.938449951172" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.902746826172" Y="-22.226806640625" />
                  <Point X="-2.886614990234" Y="-22.213859375" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.853770019531" Y="-22.181814453125" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.751895263672" Y="-21.920193359375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.008572265625" Y="-21.38809765625" />
                  <Point X="-3.059386474609" Y="-21.3000859375" />
                  <Point X="-2.981409179688" Y="-21.24030078125" />
                  <Point X="-2.648369384766" Y="-20.984962890625" />
                  <Point X="-2.393863769531" Y="-20.843564453125" />
                  <Point X="-2.192524169922" Y="-20.731703125" />
                  <Point X="-2.118564208984" Y="-20.82808984375" />
                  <Point X="-2.111820556641" Y="-20.835951171875" />
                  <Point X="-2.097517578125" Y="-20.850892578125" />
                  <Point X="-2.089958007813" Y="-20.85797265625" />
                  <Point X="-2.073378417969" Y="-20.871884765625" />
                  <Point X="-2.065094726563" Y="-20.878099609375" />
                  <Point X="-2.047896850586" Y="-20.889591796875" />
                  <Point X="-2.038982299805" Y="-20.894869140625" />
                  <Point X="-1.999573486328" Y="-20.915384765625" />
                  <Point X="-1.943766235352" Y="-20.9444375" />
                  <Point X="-1.934335571289" Y="-20.9487109375" />
                  <Point X="-1.9150625" Y="-20.95620703125" />
                  <Point X="-1.905220458984" Y="-20.9594296875" />
                  <Point X="-1.884314086914" Y="-20.965033203125" />
                  <Point X="-1.874175048828" Y="-20.967166015625" />
                  <Point X="-1.853724975586" Y="-20.970314453125" />
                  <Point X="-1.833054931641" Y="-20.971216796875" />
                  <Point X="-1.812408325195" Y="-20.96986328125" />
                  <Point X="-1.802120727539" Y="-20.968623046875" />
                  <Point X="-1.780805419922" Y="-20.96486328125" />
                  <Point X="-1.770717163086" Y="-20.962509765625" />
                  <Point X="-1.750861206055" Y="-20.956720703125" />
                  <Point X="-1.741093261719" Y="-20.95328515625" />
                  <Point X="-1.700046508789" Y="-20.93628125" />
                  <Point X="-1.641919311523" Y="-20.912205078125" />
                  <Point X="-1.632580810547" Y="-20.907724609375" />
                  <Point X="-1.614450683594" Y="-20.89777734375" />
                  <Point X="-1.605659545898" Y="-20.892310546875" />
                  <Point X="-1.58792956543" Y="-20.879896484375" />
                  <Point X="-1.579780761719" Y="-20.873501953125" />
                  <Point X="-1.564228393555" Y="-20.85986328125" />
                  <Point X="-1.550253173828" Y="-20.844611328125" />
                  <Point X="-1.538021606445" Y="-20.8279296875" />
                  <Point X="-1.532361694336" Y="-20.819255859375" />
                  <Point X="-1.521539550781" Y="-20.80051171875" />
                  <Point X="-1.516857299805" Y="-20.7912734375" />
                  <Point X="-1.508526123047" Y="-20.77233984375" />
                  <Point X="-1.504877075195" Y="-20.76264453125" />
                  <Point X="-1.491516967773" Y="-20.720271484375" />
                  <Point X="-1.472597412109" Y="-20.660267578125" />
                  <Point X="-1.470026489258" Y="-20.650236328125" />
                  <Point X="-1.465991210938" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-1.362308959961" Y="-20.4045546875" />
                  <Point X="-0.931163696289" Y="-20.2836796875" />
                  <Point X="-0.622641235352" Y="-20.247568359375" />
                  <Point X="-0.36522265625" Y="-20.21744140625" />
                  <Point X="-0.30564730835" Y="-20.439779296875" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.351241546631" Y="-20.315568359375" />
                  <Point X="0.378190734863" Y="-20.2149921875" />
                  <Point X="0.451422607422" Y="-20.222662109375" />
                  <Point X="0.827865356445" Y="-20.2620859375" />
                  <Point X="1.083144287109" Y="-20.32371875" />
                  <Point X="1.453606933594" Y="-20.41316015625" />
                  <Point X="1.617911865234" Y="-20.47275390625" />
                  <Point X="1.858240966797" Y="-20.559921875" />
                  <Point X="2.018924804688" Y="-20.6350703125" />
                  <Point X="2.250437744141" Y="-20.743341796875" />
                  <Point X="2.405720458984" Y="-20.83380859375" />
                  <Point X="2.629427246094" Y="-20.964140625" />
                  <Point X="2.775823242188" Y="-21.06825" />
                  <Point X="2.817778564453" Y="-21.0980859375" />
                  <Point X="2.421924072266" Y="-21.7837265625" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301269531" Y="-22.459404296875" />
                  <Point X="2.032415405273" Y="-22.492634765625" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017912231445" Y="-22.5485390625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411376953" Y="-22.630421875" />
                  <Point X="2.016876220703" Y="-22.659154296875" />
                  <Point X="2.021782714844" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144287109" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.04532043457" Y="-22.776115234375" />
                  <Point X="2.055681884766" Y="-22.79615625" />
                  <Point X="2.061458740234" Y="-22.8058671875" />
                  <Point X="2.07923828125" Y="-22.8320703125" />
                  <Point X="2.104416503906" Y="-22.86917578125" />
                  <Point X="2.109807617188" Y="-22.876365234375" />
                  <Point X="2.130462158203" Y="-22.899875" />
                  <Point X="2.157595703125" Y="-22.924611328125" />
                  <Point X="2.168257080078" Y="-22.933017578125" />
                  <Point X="2.194459472656" Y="-22.950796875" />
                  <Point X="2.231565673828" Y="-22.9759765625" />
                  <Point X="2.24127734375" Y="-22.98175390625" />
                  <Point X="2.261318603516" Y="-22.992115234375" />
                  <Point X="2.271648193359" Y="-22.99669921875" />
                  <Point X="2.29373828125" Y="-23.004966796875" />
                  <Point X="2.304543701172" Y="-23.00829296875" />
                  <Point X="2.326470214844" Y="-23.01363671875" />
                  <Point X="2.337591308594" Y="-23.015654296875" />
                  <Point X="2.366325195312" Y="-23.019119140625" />
                  <Point X="2.407016113281" Y="-23.024025390625" />
                  <Point X="2.416039794922" Y="-23.0246796875" />
                  <Point X="2.447579101562" Y="-23.02450390625" />
                  <Point X="2.484318359375" Y="-23.020177734375" />
                  <Point X="2.497752197266" Y="-23.01760546875" />
                  <Point X="2.530981689453" Y="-23.00871875" />
                  <Point X="2.578038574219" Y="-22.996134765625" />
                  <Point X="2.584009277344" Y="-22.994326171875" />
                  <Point X="2.604412841797" Y="-22.9869296875" />
                  <Point X="2.627658691406" Y="-22.976423828125" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.510740966797" Y="-22.46711328125" />
                  <Point X="3.940402832031" Y="-22.219048828125" />
                  <Point X="4.043957275391" Y="-22.36296484375" />
                  <Point X="4.125563964844" Y="-22.4978203125" />
                  <Point X="4.136884765625" Y="-22.516529296875" />
                  <Point X="3.642469238281" Y="-22.89590625" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168133056641" Y="-23.260134765625" />
                  <Point X="3.152116943359" Y="-23.274787109375" />
                  <Point X="3.134668457031" Y="-23.2933984375" />
                  <Point X="3.128576416016" Y="-23.300578125" />
                  <Point X="3.104661132812" Y="-23.33177734375" />
                  <Point X="3.070794189453" Y="-23.375958984375" />
                  <Point X="3.065635742188" Y="-23.3833984375" />
                  <Point X="3.049739257812" Y="-23.41062890625" />
                  <Point X="3.034762939453" Y="-23.444455078125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.021231689453" Y="-23.48918359375" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.012012451172" Y="-23.68287109375" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034682861328" Y="-23.771390625" />
                  <Point X="3.050286376953" Y="-23.804630859375" />
                  <Point X="3.056919189453" Y="-23.8164765625" />
                  <Point X="3.076809570312" Y="-23.846708984375" />
                  <Point X="3.104976806641" Y="-23.889521484375" />
                  <Point X="3.111739257813" Y="-23.898576171875" />
                  <Point X="3.126293701172" Y="-23.915822265625" />
                  <Point X="3.134085693359" Y="-23.924013671875" />
                  <Point X="3.151328125" Y="-23.9401015625" />
                  <Point X="3.160039306641" Y="-23.94730859375" />
                  <Point X="3.178249267578" Y="-23.9606328125" />
                  <Point X="3.187748046875" Y="-23.96675" />
                  <Point X="3.216571777344" Y="-23.982974609375" />
                  <Point X="3.257390136719" Y="-24.005953125" />
                  <Point X="3.265475830078" Y="-24.01001171875" />
                  <Point X="3.294680664062" Y="-24.02191796875" />
                  <Point X="3.330278076172" Y="-24.03198046875" />
                  <Point X="3.343672851563" Y="-24.034744140625" />
                  <Point X="3.38264453125" Y="-24.03989453125" />
                  <Point X="3.437833496094" Y="-24.0471875" />
                  <Point X="3.444033447266" Y="-24.04780078125" />
                  <Point X="3.465708984375" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.331516601562" Y="-23.9378125" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.778401855469" Y="-24.250951171875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.230069335938" Y="-24.43446875" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686022949219" Y="-24.580458984375" />
                  <Point X="3.665625" Y="-24.58786328125" />
                  <Point X="3.642384765625" Y="-24.59837890625" />
                  <Point X="3.634007080078" Y="-24.602681640625" />
                  <Point X="3.595718505859" Y="-24.6248125" />
                  <Point X="3.541497070312" Y="-24.65615234375" />
                  <Point X="3.533881347656" Y="-24.661052734375" />
                  <Point X="3.508771728516" Y="-24.680130859375" />
                  <Point X="3.481992431641" Y="-24.705650390625" />
                  <Point X="3.472796386719" Y="-24.7157734375" />
                  <Point X="3.449823242188" Y="-24.745046875" />
                  <Point X="3.417290527344" Y="-24.786501953125" />
                  <Point X="3.410852294922" Y="-24.795794921875" />
                  <Point X="3.399128417969" Y="-24.815076171875" />
                  <Point X="3.393842773438" Y="-24.825064453125" />
                  <Point X="3.384069335938" Y="-24.84652734375" />
                  <Point X="3.380006103516" Y="-24.8570703125" />
                  <Point X="3.373159667969" Y="-24.8785703125" />
                  <Point X="3.370376464844" Y="-24.88952734375" />
                  <Point X="3.36271875" Y="-24.929513671875" />
                  <Point X="3.351874267578" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584472656" Y="-25.026263671875" />
                  <Point X="3.350280273438" Y="-25.06294140625" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.359532470703" Y="-25.116408203125" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159667969" Y="-25.18398828125" />
                  <Point X="3.380005859375" Y="-25.20548828125" />
                  <Point X="3.384069335938" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.23749609375" />
                  <Point X="3.399128417969" Y="-25.247484375" />
                  <Point X="3.410852294922" Y="-25.266765625" />
                  <Point X="3.417290527344" Y="-25.27605859375" />
                  <Point X="3.440263671875" Y="-25.30533203125" />
                  <Point X="3.472796386719" Y="-25.346787109375" />
                  <Point X="3.47871875" Y="-25.353634765625" />
                  <Point X="3.501128417969" Y="-25.375794921875" />
                  <Point X="3.530170898438" Y="-25.39872265625" />
                  <Point X="3.541495849609" Y="-25.406408203125" />
                  <Point X="3.579784423828" Y="-25.4285390625" />
                  <Point X="3.634005859375" Y="-25.459880859375" />
                  <Point X="3.639499511719" Y="-25.4628203125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027099609" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.453977050781" Y="-25.688087890625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.76161328125" Y="-25.9310546875" />
                  <Point X="4.7286640625" Y="-26.075443359375" />
                  <Point X="4.727802246094" Y="-26.079220703125" />
                  <Point X="4.060800292969" Y="-25.991408203125" />
                  <Point X="3.436781738281" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720214844" Y="-25.910841796875" />
                  <Point X="3.354480712891" Y="-25.912677734375" />
                  <Point X="3.279333740234" Y="-25.92901171875" />
                  <Point X="3.172916503906" Y="-25.952140625" />
                  <Point X="3.157872314453" Y="-25.956744140625" />
                  <Point X="3.128752441406" Y="-25.968369140625" />
                  <Point X="3.114676757812" Y="-25.975390625" />
                  <Point X="3.086849853516" Y="-25.992283203125" />
                  <Point X="3.074125244141" Y="-26.00153125" />
                  <Point X="3.050374755859" Y="-26.022001953125" />
                  <Point X="3.039348876953" Y="-26.033224609375" />
                  <Point X="2.993927246094" Y="-26.087853515625" />
                  <Point X="2.929604736328" Y="-26.165212890625" />
                  <Point X="2.921325683594" Y="-26.17684765625" />
                  <Point X="2.906605957031" Y="-26.201228515625" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.868647216797" Y="-26.36740625" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.938128173828" Y="-26.68405859375" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.759922851562" Y="-27.378876953125" />
                  <Point X="4.087170898438" Y="-27.629982421875" />
                  <Point X="4.045496337891" Y="-27.697419921875" />
                  <Point X="4.0012734375" Y="-27.76025390625" />
                  <Point X="3.404227294922" Y="-27.415548828125" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841199951172" Y="-27.090890625" />
                  <Point X="2.815021240234" Y="-27.07951171875" />
                  <Point X="2.783116943359" Y="-27.069326171875" />
                  <Point X="2.771108642578" Y="-27.066337890625" />
                  <Point X="2.681671875" Y="-27.050185546875" />
                  <Point X="2.555018310547" Y="-27.0273125" />
                  <Point X="2.539359375" Y="-27.02580859375" />
                  <Point X="2.508008789062" Y="-27.025404296875" />
                  <Point X="2.492317138672" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444847167969" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400589111328" Y="-27.051109375" />
                  <Point X="2.3262890625" Y="-27.090212890625" />
                  <Point X="2.221071044922" Y="-27.145587890625" />
                  <Point X="2.208967285156" Y="-27.153171875" />
                  <Point X="2.186038085938" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144942382812" Y="-27.211158203125" />
                  <Point X="2.128047607422" Y="-27.23408984375" />
                  <Point X="2.120463134766" Y="-27.2461953125" />
                  <Point X="2.081359619141" Y="-27.32049609375" />
                  <Point X="2.02598425293" Y="-27.425712890625" />
                  <Point X="2.019835571289" Y="-27.440193359375" />
                  <Point X="2.010012084961" Y="-27.46996875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.002187744141" Y="-27.580140625" />
                  <Point X="2.01833972168" Y="-27.669578125" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.523946777344" Y="-28.660623046875" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.723753662109" Y="-29.036083984375" />
                  <Point X="2.258020751953" Y="-28.42912890625" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828655639648" Y="-27.8701484375" />
                  <Point X="1.808837158203" Y="-27.8496328125" />
                  <Point X="1.783253173828" Y="-27.828005859375" />
                  <Point X="1.773297607422" Y="-27.820646484375" />
                  <Point X="1.685088867188" Y="-27.7639375" />
                  <Point X="1.560174316406" Y="-27.68362890625" />
                  <Point X="1.546284667969" Y="-27.67624609375" />
                  <Point X="1.517470947266" Y="-27.663873046875" />
                  <Point X="1.502547241211" Y="-27.6588828125" />
                  <Point X="1.470926635742" Y="-27.65115234375" />
                  <Point X="1.455385009766" Y="-27.6486953125" />
                  <Point X="1.424119506836" Y="-27.64637890625" />
                  <Point X="1.408395507812" Y="-27.64651953125" />
                  <Point X="1.311924438477" Y="-27.655396484375" />
                  <Point X="1.17530871582" Y="-27.667966796875" />
                  <Point X="1.161225952148" Y="-27.67033984375" />
                  <Point X="1.133575317383" Y="-27.677171875" />
                  <Point X="1.120007324219" Y="-27.681630859375" />
                  <Point X="1.092621582031" Y="-27.692974609375" />
                  <Point X="1.079875854492" Y="-27.699416015625" />
                  <Point X="1.055493896484" Y="-27.71413671875" />
                  <Point X="1.043857055664" Y="-27.722416015625" />
                  <Point X="0.969364501953" Y="-27.78435546875" />
                  <Point X="0.863873474121" Y="-27.872068359375" />
                  <Point X="0.852653625488" Y="-27.88308984375" />
                  <Point X="0.83218359375" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.760518859863" Y="-28.10810546875" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091308594" Y="-29.15233984375" />
                  <Point X="0.820859741211" Y="-29.10669140625" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146789551" Y="-28.453583984375" />
                  <Point X="0.626788085938" Y="-28.42381640625" />
                  <Point X="0.620407165527" Y="-28.41320703125" />
                  <Point X="0.552642700195" Y="-28.315572265625" />
                  <Point X="0.456679534912" Y="-28.177306640625" />
                  <Point X="0.446671112061" Y="-28.165171875" />
                  <Point X="0.424787750244" Y="-28.142716796875" />
                  <Point X="0.412912780762" Y="-28.132396484375" />
                  <Point X="0.386657348633" Y="-28.11315234375" />
                  <Point X="0.373240112305" Y="-28.10493359375" />
                  <Point X="0.345238677979" Y="-28.090826171875" />
                  <Point X="0.330654296875" Y="-28.0849375" />
                  <Point X="0.225792526245" Y="-28.052392578125" />
                  <Point X="0.077295059204" Y="-28.0063046875" />
                  <Point X="0.063377120972" Y="-28.003109375" />
                  <Point X="0.035217838287" Y="-27.99883984375" />
                  <Point X="0.020976644516" Y="-27.997765625" />
                  <Point X="-0.008664708138" Y="-27.997765625" />
                  <Point X="-0.022905902863" Y="-27.99883984375" />
                  <Point X="-0.051065185547" Y="-28.003109375" />
                  <Point X="-0.064983123779" Y="-28.0063046875" />
                  <Point X="-0.169844909668" Y="-28.038849609375" />
                  <Point X="-0.318342071533" Y="-28.0849375" />
                  <Point X="-0.332930755615" Y="-28.090830078125" />
                  <Point X="-0.360932647705" Y="-28.104939453125" />
                  <Point X="-0.374346160889" Y="-28.11315625" />
                  <Point X="-0.400601287842" Y="-28.132400390625" />
                  <Point X="-0.412475646973" Y="-28.14271875" />
                  <Point X="-0.434359039307" Y="-28.165173828125" />
                  <Point X="-0.44436819458" Y="-28.177310546875" />
                  <Point X="-0.5121328125" Y="-28.274947265625" />
                  <Point X="-0.608095947266" Y="-28.4132109375" />
                  <Point X="-0.612468994141" Y="-28.42012890625" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.877539428711" Y="-29.364171875" />
                  <Point X="-0.985425109863" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.67712497766" Y="-21.993401954152" />
                  <Point X="-4.168944486956" Y="-22.750737585038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.594739562871" Y="-22.040966993974" />
                  <Point X="-4.093342620324" Y="-22.808748372997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.941770478169" Y="-21.20991023157" />
                  <Point X="-3.031593084011" Y="-21.348224915265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.512354182697" Y="-22.088532087097" />
                  <Point X="-4.017740850149" Y="-22.866759309485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.553829856637" Y="-23.692263988063" />
                  <Point X="-4.69558042396" Y="-23.910540720285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.716171877932" Y="-21.036946304764" />
                  <Point X="-2.978282424413" Y="-21.440561151936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429969002303" Y="-22.136097487855" />
                  <Point X="-3.942139270618" Y="-22.92477053954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.449477279975" Y="-23.706002564961" />
                  <Point X="-4.750025644503" Y="-24.168806461429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.524417186245" Y="-20.916097426971" />
                  <Point X="-2.924971902172" Y="-21.532897600119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.347583821909" Y="-22.183662888612" />
                  <Point X="-3.866537691086" Y="-22.982781769594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.345124703313" Y="-23.719741141858" />
                  <Point X="-4.783453159866" Y="-24.394707774747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347204473125" Y="-20.817641232479" />
                  <Point X="-2.871661379932" Y="-21.625234048302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.265198641515" Y="-22.23122828937" />
                  <Point X="-3.790936111555" Y="-23.040792999648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.24077212665" Y="-23.733479718756" />
                  <Point X="-4.694711999433" Y="-24.432485824532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.184723236563" Y="-20.741869522604" />
                  <Point X="-2.818350857692" Y="-21.717570496486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.174248477191" Y="-22.265604771458" />
                  <Point X="-3.715334532023" Y="-23.098804229702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.136419229658" Y="-23.747217802389" />
                  <Point X="-4.598226689429" Y="-24.458338929718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.123371942887" Y="-20.821824268571" />
                  <Point X="-2.768730775004" Y="-21.815589723237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.067970726871" Y="-22.276378840891" />
                  <Point X="-3.639732952492" Y="-23.156815459757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.032066244633" Y="-23.760955750464" />
                  <Point X="-4.501741379425" Y="-24.484192034904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.052227059364" Y="-20.886698208663" />
                  <Point X="-2.748681403427" Y="-21.959143851984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.936598387833" Y="-22.248510632375" />
                  <Point X="-3.56413137296" Y="-23.214826689811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.927713259609" Y="-23.774693698538" />
                  <Point X="-4.405256069421" Y="-24.510045140089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.96819134778" Y="-20.93172201427" />
                  <Point X="-3.494919472533" Y="-23.282677162848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.823360274584" Y="-23.788431646613" />
                  <Point X="-4.308770770406" Y="-24.535898262197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.877482603155" Y="-20.966470250094" />
                  <Point X="-3.443083681503" Y="-23.377284497953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.713132369862" Y="-23.793123011682" />
                  <Point X="-4.21228547573" Y="-24.561751390986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.407529259397" Y="-20.417233014998" />
                  <Point X="-1.46937592152" Y="-20.51246852313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.759515174165" Y="-20.959243792905" />
                  <Point X="-3.426825794668" Y="-23.526677001213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.569082153332" Y="-23.745732583801" />
                  <Point X="-4.115800181054" Y="-24.587604519775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.269040225015" Y="-20.378406056664" />
                  <Point X="-1.491957424967" Y="-20.721668442702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.600367354233" Y="-20.888605094508" />
                  <Point X="-4.019314886378" Y="-24.613457648565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676313273854" Y="-25.625146446721" />
                  <Point X="-4.770032492732" Y="-25.769461388307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.130551553701" Y="-20.339579657407" />
                  <Point X="-3.922829591703" Y="-24.639310777354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.539175560622" Y="-25.588400340481" />
                  <Point X="-4.749587930808" Y="-25.912406977283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.992062882388" Y="-20.30075325815" />
                  <Point X="-3.826344297027" Y="-24.665163906143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.402037847389" Y="-25.55165423424" />
                  <Point X="-4.722589596868" Y="-26.045260642353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.862479973667" Y="-20.275640530685" />
                  <Point X="-3.729859002351" Y="-24.691017034932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.264900134156" Y="-25.514908127999" />
                  <Point X="-4.690612987067" Y="-26.170448434842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.739887106083" Y="-20.261291522662" />
                  <Point X="-3.633373707675" Y="-24.716870163721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.127762420924" Y="-25.478162021758" />
                  <Point X="-4.652072348057" Y="-26.285528508731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.617294280378" Y="-20.246942579129" />
                  <Point X="-3.536888412999" Y="-24.742723292511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990624742663" Y="-25.441415969369" />
                  <Point X="-4.528208091659" Y="-26.269221733618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.494702372983" Y="-20.232595049667" />
                  <Point X="-3.446824344712" Y="-24.77846424284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853487065826" Y="-25.404669919173" />
                  <Point X="-4.404343835261" Y="-26.252914958506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.372110465588" Y="-20.218247520206" />
                  <Point X="-3.370648945082" Y="-24.835591867428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716349388989" Y="-25.367923868976" />
                  <Point X="-4.280479578863" Y="-26.236608183393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.333995403426" Y="-20.333982924974" />
                  <Point X="-3.316082902249" Y="-24.92599498344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.579211712152" Y="-25.331177818779" />
                  <Point X="-4.156615282291" Y="-26.220301346419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.300909224532" Y="-20.457462130891" />
                  <Point X="-3.298107296614" Y="-25.072742431703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.428418795319" Y="-25.273404542942" />
                  <Point X="-4.032750906729" Y="-26.20399438781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.267823149013" Y="-20.580941495993" />
                  <Point X="-3.908886531167" Y="-26.187687429201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.234737073494" Y="-20.704420861096" />
                  <Point X="-3.785022155605" Y="-26.171380470592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.189947730592" Y="-20.809878774792" />
                  <Point X="-3.661157780043" Y="-26.155073511982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.120996249489" Y="-20.878130258422" />
                  <Point X="-3.537293404481" Y="-26.138766553373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.420076292987" Y="-20.219379060965" />
                  <Point X="0.346767982042" Y="-20.332263960547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.029434950305" Y="-20.911565675351" />
                  <Point X="-3.413429028918" Y="-26.122459594764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.526137366481" Y="-20.230486783451" />
                  <Point X="0.267200409942" Y="-20.629214730662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.093111497981" Y="-20.89728814678" />
                  <Point X="-3.289564653356" Y="-26.106152636155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.134825959838" Y="-27.407740907274" />
                  <Point X="-4.170112099679" Y="-27.462076797724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.632198582208" Y="-20.241594286917" />
                  <Point X="-3.174753353096" Y="-26.103786191018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.909040631578" Y="-27.234489444528" />
                  <Point X="-4.115886517521" Y="-27.5530041772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.738259797935" Y="-20.252701790384" />
                  <Point X="-3.082647471075" Y="-26.136383023916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683255303319" Y="-27.061237981781" />
                  <Point X="-4.061661237024" Y="-27.643932021193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.843058122431" Y="-20.26575397581" />
                  <Point X="-3.008503001972" Y="-26.196638007268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.457470683544" Y="-26.887987610006" />
                  <Point X="-4.007435956526" Y="-27.734859865185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.940979658438" Y="-20.289395486892" />
                  <Point X="-2.956624282666" Y="-26.291179238624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.231686588191" Y="-26.714738045768" />
                  <Point X="-3.946746459098" Y="-27.815833688008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.038901194446" Y="-20.313036997975" />
                  <Point X="-3.885622369206" Y="-27.896138297122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.136822777704" Y="-20.336678436299" />
                  <Point X="-3.824498279314" Y="-27.976442906236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.234744399905" Y="-20.360319814655" />
                  <Point X="-3.704994209495" Y="-27.966850229673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.332666022107" Y="-20.38396119301" />
                  <Point X="-3.523773478497" Y="-27.862222228876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.430587644309" Y="-20.407602571365" />
                  <Point X="-3.342552792612" Y="-27.757594297547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.523734957728" Y="-20.438595740542" />
                  <Point X="-3.161332143477" Y="-27.652966422808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.615415011174" Y="-20.471848291944" />
                  <Point X="-2.980111494342" Y="-27.548338548069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.707095047109" Y="-20.505100870309" />
                  <Point X="-2.798890845207" Y="-27.44371067333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.798775082554" Y="-20.53835344943" />
                  <Point X="-2.629529391969" Y="-27.357344358852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.888770609517" Y="-20.574199944145" />
                  <Point X="-2.514056843553" Y="-27.353959680847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.97565660906" Y="-20.614834691186" />
                  <Point X="-2.426849062657" Y="-27.394098928057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.062542817705" Y="-20.655469116239" />
                  <Point X="-2.357969616086" Y="-27.46246133514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.149429233775" Y="-20.696103221884" />
                  <Point X="-2.3132879135" Y="-27.568085000388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.236315649846" Y="-20.736737327529" />
                  <Point X="-2.663150200697" Y="-28.281253132186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.319262093694" Y="-20.783438458357" />
                  <Point X="-2.931783032432" Y="-28.869338871489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.40144386802" Y="-20.831317076993" />
                  <Point X="-2.856267788039" Y="-28.927483045997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483625351588" Y="-20.879196143357" />
                  <Point X="-2.780752509912" Y="-28.985627168562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.565806819194" Y="-20.927075234298" />
                  <Point X="-2.702277843416" Y="-29.03921423266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646928334981" Y="-20.97658650791" />
                  <Point X="-2.621488641652" Y="-29.089237224993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.724416725433" Y="-21.031692303935" />
                  <Point X="-2.540699439887" Y="-29.139260217326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.801905267912" Y="-21.086797865858" />
                  <Point X="-2.310843537446" Y="-28.959740620014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.096099848137" Y="-22.348070356624" />
                  <Point X="-2.110740492815" Y="-28.826036406018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015384950773" Y="-22.646787852716" />
                  <Point X="-1.965272113401" Y="-28.776462198799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.045057715364" Y="-22.775523255727" />
                  <Point X="-1.855488358779" Y="-28.781837495045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.100889270969" Y="-22.863977652959" />
                  <Point X="-1.746838292279" Y="-28.788958517908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.16900094859" Y="-22.933522320548" />
                  <Point X="-1.638188416961" Y="-28.796079835167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.248521368357" Y="-22.985499065825" />
                  <Point X="-1.543275424348" Y="-28.824354096816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.341877223351" Y="-23.016171109136" />
                  <Point X="-1.466089104857" Y="-28.87992504133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.449919310057" Y="-23.024228338784" />
                  <Point X="-1.393917530059" Y="-28.943218015499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.582270471532" Y="-22.994852875893" />
                  <Point X="-1.321745880576" Y="-29.006510874663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.754854740216" Y="-22.903523860824" />
                  <Point X="-1.250268686127" Y="-29.070873100802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.936075168183" Y="-22.798896326655" />
                  <Point X="-1.19500189001" Y="-29.160197151383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.117295596149" Y="-22.694268792486" />
                  <Point X="-0.439970040992" Y="-28.171977514101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.640126879569" Y="-28.480192017093" />
                  <Point X="-1.164969219895" Y="-29.288378348487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.298516024115" Y="-22.589641258317" />
                  <Point X="-0.258011330609" Y="-28.066213124705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.720632935194" Y="-28.778587925109" />
                  <Point X="-1.138408896221" Y="-29.421906490218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.479736452081" Y="-22.485013724148" />
                  <Point X="-0.116143137421" Y="-28.022182718119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.800200539078" Y="-29.075538744168" />
                  <Point X="-1.120078193599" Y="-29.568107137072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.660956649292" Y="-22.380386545312" />
                  <Point X="0.012988021073" Y="-27.997765625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.879768149811" Y="-29.372489573775" />
                  <Point X="-1.116872105136" Y="-29.737597647362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.842176798874" Y="-22.275759439816" />
                  <Point X="3.237797775196" Y="-23.206421523243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002695372221" Y="-23.568447476492" />
                  <Point X="0.113433466532" Y="-28.017520656348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.959335998264" Y="-29.669440769437" />
                  <Point X="-1.018408424905" Y="-29.760404329549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.967670208778" Y="-22.2569439883" />
                  <Point X="3.46358268758" Y="-23.033170700888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019161419793" Y="-23.717519440327" />
                  <Point X="0.207707038335" Y="-28.046779539699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.027209346075" Y="-22.339689210384" />
                  <Point X="3.689367434333" Y="-22.859920133583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.062582425411" Y="-23.825084408667" />
                  <Point X="0.301980661312" Y="-28.076038344248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.083226617288" Y="-22.427857630659" />
                  <Point X="3.915151549309" Y="-22.686670539131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.121002613372" Y="-23.90955266163" />
                  <Point X="0.389703247406" Y="-28.115384860969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.194620533785" Y="-23.970618458861" />
                  <Point X="0.459823522369" Y="-28.181836559885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.27878828001" Y="-24.01543894895" />
                  <Point X="0.518342778717" Y="-28.266152260911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.376692019914" Y="-24.039107863628" />
                  <Point X="0.91531493655" Y="-27.829296197037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.773732136712" Y="-28.047314589987" />
                  <Point X="0.576862228741" Y="-28.3504676637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.484065021942" Y="-24.048195393331" />
                  <Point X="1.125699210486" Y="-27.679760278249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724728463299" Y="-28.297201083357" />
                  <Point X="0.63374595836" Y="-28.437301855032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.607087821173" Y="-24.033184348628" />
                  <Point X="1.251165182845" Y="-27.660987076846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.740314379817" Y="-28.447628330165" />
                  <Point X="0.672319632354" Y="-28.552331059508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.730952142174" Y="-24.016877474035" />
                  <Point X="1.371638568416" Y="-27.649901784917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.759407480414" Y="-28.59265498709" />
                  <Point X="0.705405746229" Y="-28.675810365546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854816463175" Y="-24.000570599443" />
                  <Point X="1.482295921489" Y="-27.653931857516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77850058101" Y="-28.737681644015" />
                  <Point X="0.738491860105" Y="-28.799289671584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.978680784176" Y="-23.984263724851" />
                  <Point X="3.542864477586" Y="-24.655361986028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361836434159" Y="-24.934120727568" />
                  <Point X="1.571539763378" Y="-27.690935845738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.797593681606" Y="-28.88270830094" />
                  <Point X="0.771577973981" Y="-28.922768977622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102545105177" Y="-23.967956850258" />
                  <Point X="3.708900929357" Y="-24.574115724813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.35889469101" Y="-25.11307806836" />
                  <Point X="1.651450701187" Y="-27.742311245965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.816686782202" Y="-29.027734957865" />
                  <Point X="0.804664087856" Y="-29.046248283659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.226409426178" Y="-23.951649975666" />
                  <Point X="3.846038577945" Y="-24.537369718116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392824038673" Y="-25.235258908233" />
                  <Point X="1.731361913305" Y="-27.793686223792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.350273830358" Y="-23.935342972989" />
                  <Point X="3.983176226533" Y="-24.50062371142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.451387457105" Y="-25.319506605612" />
                  <Point X="2.288594151474" Y="-27.110051277112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000543118427" Y="-27.55361097069" />
                  <Point X="1.808493140321" Y="-27.849342003277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.474138700638" Y="-23.919035252582" />
                  <Point X="4.120313875121" Y="-24.463877704723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518963997988" Y="-25.389875311515" />
                  <Point X="3.148372145379" Y="-25.960536721221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866088196448" Y="-26.395215884029" />
                  <Point X="2.451567388876" Y="-27.03352195238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022728000368" Y="-27.693876701847" />
                  <Point X="1.871971789896" Y="-27.926020908431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.598003570917" Y="-23.902727532175" />
                  <Point X="4.257451587226" Y="-24.427131600219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.599666615486" Y="-25.440031631925" />
                  <Point X="3.282577044651" Y="-25.928306752445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.871637680176" Y="-26.561097882051" />
                  <Point X="2.56741990786" Y="-27.02955217101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.049768007602" Y="-27.82666619567" />
                  <Point X="1.933323193056" Y="-28.005975485807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.708983645396" Y="-23.906260657388" />
                  <Point X="4.394589553925" Y="-24.390385103674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.68574799276" Y="-25.481905388608" />
                  <Point X="3.408912239419" Y="-25.908195065908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.921677420169" Y="-26.658470893221" />
                  <Point X="2.668804084248" Y="-27.04786168349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.099210364717" Y="-27.924959095804" />
                  <Point X="1.994674596217" Y="-28.085930063182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.739869222882" Y="-24.033128492312" />
                  <Point X="4.531727520624" Y="-24.35363860713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.782046702769" Y="-25.508045832587" />
                  <Point X="3.514826547503" Y="-25.919528787306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.978029997353" Y="-26.746122987579" />
                  <Point X="2.770187985696" Y="-27.066171619338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.152520903026" Y="-28.017295519243" />
                  <Point X="2.056025999378" Y="-28.165884640558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765500356699" Y="-24.168087460948" />
                  <Point X="4.668865487322" Y="-24.316892110585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.878532072195" Y="-25.533898846272" />
                  <Point X="3.619179233782" Y="-25.93326719541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.040192683131" Y="-26.824828299277" />
                  <Point X="2.860453962455" Y="-27.101601657886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205831441336" Y="-28.109631942682" />
                  <Point X="2.117377402539" Y="-28.245839217934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.975017441621" Y="-25.559751859956" />
                  <Point X="3.723531920061" Y="-25.947005603513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.11502278496" Y="-26.884027500816" />
                  <Point X="2.942839318502" Y="-27.149166788163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.259141979645" Y="-28.201968366122" />
                  <Point X="2.1787288057" Y="-28.325793795309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.071502811047" Y="-25.58560487364" />
                  <Point X="3.82788460634" Y="-25.960744011616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.190624395341" Y="-26.942038683366" />
                  <Point X="3.025224674548" Y="-27.19673191844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.312452517954" Y="-28.294304789561" />
                  <Point X="2.24008020886" Y="-28.405748372685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.167988180473" Y="-25.611457887324" />
                  <Point X="3.932237292618" Y="-25.974482419719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.266226005721" Y="-27.000049865917" />
                  <Point X="3.107610030594" Y="-27.244297048717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.365763056264" Y="-28.386641213" />
                  <Point X="2.301431575846" Y="-28.485703005766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.264473549899" Y="-25.637310901008" />
                  <Point X="4.036589978897" Y="-25.988220827822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.341827616102" Y="-27.058061048468" />
                  <Point X="3.18999538664" Y="-27.291862178993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.419073594573" Y="-28.478977636439" />
                  <Point X="2.36278292788" Y="-28.565657661869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.360958919324" Y="-25.663163914692" />
                  <Point X="4.140942716683" Y="-26.001959156611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.417429226483" Y="-27.116072231018" />
                  <Point X="3.272380742686" Y="-27.33942730927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.472384132882" Y="-28.571314059878" />
                  <Point X="2.424134279915" Y="-28.645612317972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.457444287241" Y="-25.6890169307" />
                  <Point X="4.245295470029" Y="-26.015697461439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.493030836863" Y="-27.174083413569" />
                  <Point X="3.354766098733" Y="-27.386992439547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.52569467133" Y="-28.663650483105" />
                  <Point X="2.48548563195" Y="-28.725566974075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.553929614672" Y="-25.714870009051" />
                  <Point X="4.349648223376" Y="-26.029435766268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.568632447244" Y="-27.232094596119" />
                  <Point X="3.437151431688" Y="-27.434557605381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.579005213857" Y="-28.755986900048" />
                  <Point X="2.546836983984" Y="-28.805521630177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.650414942103" Y="-25.740723087402" />
                  <Point X="4.454000976722" Y="-26.043174071096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.644234057625" Y="-27.29010577867" />
                  <Point X="3.519536729953" Y="-27.482122824633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632315756385" Y="-28.848323316992" />
                  <Point X="2.608188336019" Y="-28.88547628628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.746900269533" Y="-25.766576165753" />
                  <Point X="4.558353730068" Y="-26.056912375925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.719835668005" Y="-27.348116961221" />
                  <Point X="3.601922028219" Y="-27.529688043884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.685626298913" Y="-28.940659733935" />
                  <Point X="2.669539688054" Y="-28.965430942383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.7641083412" Y="-25.914505512683" />
                  <Point X="4.662706483414" Y="-26.070650680753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.795437320927" Y="-27.406128078265" />
                  <Point X="3.684307326484" Y="-27.577253263136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.871039021866" Y="-27.464139121367" />
                  <Point X="3.766692624749" Y="-27.624818482387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.946640722805" Y="-27.52215016447" />
                  <Point X="3.849077923015" Y="-27.672383701638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.022242423744" Y="-27.580161207573" />
                  <Point X="3.93146322128" Y="-27.71994892089" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998375" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.63733392334" Y="-29.1558671875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.396554351807" Y="-28.423908203125" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.274335754395" Y="-28.2663984375" />
                  <Point X="0.169474075317" Y="-28.233853515625" />
                  <Point X="0.020976625443" Y="-28.187765625" />
                  <Point X="-0.008664756775" Y="-28.187765625" />
                  <Point X="-0.113526428223" Y="-28.220310546875" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.356043609619" Y="-28.383279296875" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.69401348877" Y="-29.41334765625" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-0.926040283203" Y="-29.97187890625" />
                  <Point X="-1.100230102539" Y="-29.938068359375" />
                  <Point X="-1.221171875" Y="-29.906951171875" />
                  <Point X="-1.351589599609" Y="-29.873396484375" />
                  <Point X="-1.329108032227" Y="-29.7026328125" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.334734741211" Y="-29.40881640625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.482825927734" Y="-29.117962890625" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.777375488281" Y="-28.977365234375" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.096647216797" Y="-29.045130859375" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.391548339844" Y="-29.329078125" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.600484130859" Y="-29.325716796875" />
                  <Point X="-2.855839599609" Y="-29.167607421875" />
                  <Point X="-3.023281738281" Y="-29.038681640625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.814144775391" Y="-28.162783203125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.59759375" />
                  <Point X="-2.513979980469" Y="-27.568763671875" />
                  <Point X="-2.531328613281" Y="-27.551416015625" />
                  <Point X="-2.560157714844" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.347418701172" Y="-27.979794921875" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.959963378906" Y="-28.11217578125" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.28175" Y="-27.645830078125" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.702881347656" Y="-26.836806640625" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140326660156" Y="-26.334595703125" />
                  <Point X="-3.161160400391" Y="-26.310638671875" />
                  <Point X="-3.187643066406" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.173573242188" Y="-26.414173828125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.848491210938" Y="-26.32008984375" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.959154296875" Y="-25.78912109375" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.171573730469" Y="-25.293197265625" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.530410400391" Y="-25.113453125" />
                  <Point X="-3.502325195312" Y="-25.090646484375" />
                  <Point X="-3.4910703125" Y="-25.063572265625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.4894765625" Y="-25.004123046875" />
                  <Point X="-3.502325683594" Y="-24.971912109375" />
                  <Point X="-3.525627197266" Y="-24.952427734375" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.427120117188" Y="-24.700890625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.96849609375" Y="-24.347228515625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.853710449219" Y="-23.76764453125" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.193475585937" Y="-23.54806640625" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731704833984" Y="-23.6041328125" />
                  <Point X="-3.706281494141" Y="-23.5961171875" />
                  <Point X="-3.670278808594" Y="-23.584765625" />
                  <Point X="-3.651534423828" Y="-23.5739453125" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.628918701172" Y="-23.531587890625" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.628624755859" Y="-23.43084375" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.158805175781" Y="-22.9980078125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.357603515625" Y="-22.55151171875" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.990654052734" Y="-21.9953046875" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.429520996094" Y="-21.9169609375" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514648438" Y="-22.07956640625" />
                  <Point X="-3.103106933594" Y="-22.0826640625" />
                  <Point X="-3.052965332031" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.988120117188" Y="-22.047462890625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.941172119141" Y="-21.936751953125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.1731171875" Y="-21.48309765625" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.097013427734" Y="-21.089515625" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.486138671875" Y="-20.677474609375" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.050593505859" Y="-20.6045625" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247192383" Y="-20.726337890625" />
                  <Point X="-1.911838745117" Y="-20.746853515625" />
                  <Point X="-1.85603125" Y="-20.77590625" />
                  <Point X="-1.83512487793" Y="-20.781509765625" />
                  <Point X="-1.813809570312" Y="-20.77775" />
                  <Point X="-1.772762817383" Y="-20.76074609375" />
                  <Point X="-1.714635620117" Y="-20.736669921875" />
                  <Point X="-1.696905639648" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.672723510742" Y="-20.663138671875" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.677048461914" Y="-20.390685546875" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.41360144043" Y="-20.221609375" />
                  <Point X="-0.968082946777" Y="-20.096703125" />
                  <Point X="-0.644728393555" Y="-20.058857421875" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.122121459961" Y="-20.390603515625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282119751" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594039917" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.167715606689" Y="-20.266392578125" />
                  <Point X="0.236648406982" Y="-20.0091328125" />
                  <Point X="0.471212280273" Y="-20.033697265625" />
                  <Point X="0.860210144043" Y="-20.074435546875" />
                  <Point X="1.127735595703" Y="-20.139025390625" />
                  <Point X="1.508455566406" Y="-20.230943359375" />
                  <Point X="1.682695678711" Y="-20.294140625" />
                  <Point X="1.931044921875" Y="-20.38421875" />
                  <Point X="2.099415527344" Y="-20.4629609375" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.501365722656" Y="-20.66963671875" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.885937255859" Y="-20.91341015625" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.586468994141" Y="-21.8787265625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.215966064453" Y="-22.541716796875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.205509521484" Y="-22.636408203125" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.236461914062" Y="-22.725390625" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.274938720703" Y="-22.775794921875" />
                  <Point X="2.301141113281" Y="-22.79357421875" />
                  <Point X="2.338247314453" Y="-22.81875390625" />
                  <Point X="2.360337402344" Y="-22.827021484375" />
                  <Point X="2.389071289062" Y="-22.830486328125" />
                  <Point X="2.429762207031" Y="-22.835392578125" />
                  <Point X="2.448664794922" Y="-22.8340546875" />
                  <Point X="2.481894287109" Y="-22.82516796875" />
                  <Point X="2.528951171875" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.415740966797" Y="-22.3025703125" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.0654765625" Y="-22.0675625" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.288117675781" Y="-22.399453125" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.758133789062" Y="-23.04664453125" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.255456054688" Y="-23.4473671875" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.204211181641" Y="-23.54035546875" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.198092529297" Y="-23.6444765625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646728516" Y="-23.712046875" />
                  <Point X="3.235537109375" Y="-23.742279296875" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280946777344" Y="-23.8011796875" />
                  <Point X="3.309770507812" Y="-23.817404296875" />
                  <Point X="3.350588867188" Y="-23.8403828125" />
                  <Point X="3.36856640625" Y="-23.846380859375" />
                  <Point X="3.407538085938" Y="-23.85153125" />
                  <Point X="3.462727050781" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.306716796875" Y="-23.7494375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.880298339844" Y="-23.806712890625" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.966140136719" Y="-24.221720703125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.279245117188" Y="-24.61799609375" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087646484" Y="-24.767181640625" />
                  <Point X="3.690799072266" Y="-24.7893125" />
                  <Point X="3.636577636719" Y="-24.82065234375" />
                  <Point X="3.622264648438" Y="-24.83307421875" />
                  <Point X="3.599291503906" Y="-24.86234765625" />
                  <Point X="3.566758789062" Y="-24.903802734375" />
                  <Point X="3.556985351562" Y="-24.925265625" />
                  <Point X="3.549327636719" Y="-24.965251953125" />
                  <Point X="3.538483154297" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.546140869141" Y="-25.080669921875" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.589731933594" Y="-25.18803125" />
                  <Point X="3.622264648438" Y="-25.229486328125" />
                  <Point X="3.636576416016" Y="-25.241908203125" />
                  <Point X="3.674864990234" Y="-25.2640390625" />
                  <Point X="3.729086425781" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.503152832031" Y="-25.504560546875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.981313476562" Y="-25.748302734375" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.91390234375" Y="-26.117712890625" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.036000732422" Y="-26.179783203125" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836669922" Y="-26.098341796875" />
                  <Point X="3.319689697266" Y="-26.11467578125" />
                  <Point X="3.213272460938" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.140023925781" Y="-26.209326171875" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.057847900391" Y="-26.38481640625" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.097947998047" Y="-26.58130859375" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.875587402344" Y="-27.228138671875" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.296821777344" Y="-27.652154296875" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.132722167969" Y="-27.90360546875" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.309227294922" Y="-27.580091796875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.647903808594" Y="-27.237162109375" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.414777832031" Y="-27.258349609375" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.249496337891" Y="-27.408984375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.205315185547" Y="-27.6358125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.688491699219" Y="-28.565623046875" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.945283447266" Y="-29.11165234375" />
                  <Point X="2.835296142578" Y="-29.190212890625" />
                  <Point X="2.755458496094" Y="-29.241890625" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="2.107283447266" Y="-28.54479296875" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670549316406" Y="-27.980466796875" />
                  <Point X="1.582340576172" Y="-27.9237578125" />
                  <Point X="1.45742590332" Y="-27.84344921875" />
                  <Point X="1.425805175781" Y="-27.83571875" />
                  <Point X="1.329333984375" Y="-27.844595703125" />
                  <Point X="1.192718383789" Y="-27.857166015625" />
                  <Point X="1.165332763672" Y="-27.868509765625" />
                  <Point X="1.090840087891" Y="-27.93044921875" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.946183837891" Y="-28.1484609375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.04270300293" Y="-29.2888515625" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.098400268555" Y="-29.940439453125" />
                  <Point X="0.994366088867" Y="-29.9632421875" />
                  <Point X="0.920588623047" Y="-29.976646484375" />
                  <Point X="0.860200561523" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#158" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.079981186327" Y="4.653618339828" Z="1.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.05" />
                  <Point X="-0.656696476724" Y="5.022082754629" Z="1.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.05" />
                  <Point X="-1.433255108269" Y="4.857815414216" Z="1.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.05" />
                  <Point X="-1.732776755986" Y="4.63406860881" Z="1.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.05" />
                  <Point X="-1.726565220585" Y="4.383176266887" Z="1.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.05" />
                  <Point X="-1.798052375923" Y="4.316726810699" Z="1.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.05" />
                  <Point X="-1.894906586973" Y="4.328776350759" Z="1.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.05" />
                  <Point X="-2.017081832398" Y="4.457154943855" Z="1.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.05" />
                  <Point X="-2.51657756674" Y="4.397512577589" Z="1.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.05" />
                  <Point X="-3.133591612369" Y="3.981444782805" Z="1.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.05" />
                  <Point X="-3.222574465305" Y="3.523182335294" Z="1.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.05" />
                  <Point X="-2.99713781308" Y="3.090171221397" Z="1.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.05" />
                  <Point X="-3.029630941282" Y="3.019172626605" Z="1.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.05" />
                  <Point X="-3.10490524421" Y="2.998426886092" Z="1.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.05" />
                  <Point X="-3.410677013004" Y="3.15761948344" Z="1.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.05" />
                  <Point X="-4.0362733231" Y="3.066678038441" Z="1.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.05" />
                  <Point X="-4.406941786576" Y="2.504973787302" Z="1.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.05" />
                  <Point X="-4.195399287784" Y="1.993605169601" Z="1.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.05" />
                  <Point X="-3.679131338843" Y="1.577349571313" Z="1.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.05" />
                  <Point X="-3.681268667334" Y="1.518828085019" Z="1.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.05" />
                  <Point X="-3.727472651956" Y="1.482848499969" Z="1.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.05" />
                  <Point X="-4.193104969969" Y="1.532787153379" Z="1.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.05" />
                  <Point X="-4.908125981603" Y="1.276715050088" Z="1.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.05" />
                  <Point X="-5.024113582062" Y="0.691370206739" Z="1.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.05" />
                  <Point X="-4.446217194859" Y="0.28209278158" Z="1.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.05" />
                  <Point X="-3.560294381738" Y="0.037779253506" Z="1.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.05" />
                  <Point X="-3.543385648272" Y="0.012336674768" Z="1.05" />
                  <Point X="-3.539556741714" Y="0" Z="1.05" />
                  <Point X="-3.544978952863" Y="-0.017470276292" Z="1.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.05" />
                  <Point X="-3.565074213385" Y="-0.041096729628" Z="1.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.05" />
                  <Point X="-4.190670593914" Y="-0.213619275176" Z="1.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.05" />
                  <Point X="-5.014806303469" Y="-0.76491930455" Z="1.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.05" />
                  <Point X="-4.90309180258" Y="-1.301184089106" Z="1.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.05" />
                  <Point X="-4.173202962245" Y="-1.43246556906" Z="1.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.05" />
                  <Point X="-3.20363707132" Y="-1.315998773721" Z="1.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.05" />
                  <Point X="-3.197192020251" Y="-1.339885156527" Z="1.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.05" />
                  <Point X="-3.739475293838" Y="-1.765858905336" Z="1.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.05" />
                  <Point X="-4.330849293538" Y="-2.640159500498" Z="1.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.05" />
                  <Point X="-4.005905276083" Y="-3.111177981089" Z="1.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.05" />
                  <Point X="-3.32857474528" Y="-2.991814912705" Z="1.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.05" />
                  <Point X="-2.562671280536" Y="-2.565659188596" Z="1.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.05" />
                  <Point X="-2.863602100463" Y="-3.106503637212" Z="1.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.05" />
                  <Point X="-3.05994128564" Y="-4.047019153641" Z="1.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.05" />
                  <Point X="-2.63296170545" Y="-4.336948309516" Z="1.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.05" />
                  <Point X="-2.358036702029" Y="-4.328236022521" Z="1.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.05" />
                  <Point X="-2.075024585376" Y="-4.055424962871" Z="1.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.05" />
                  <Point X="-1.786801411359" Y="-3.995977542553" Z="1.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.05" />
                  <Point X="-1.52194934863" Y="-4.124274127957" Z="1.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.05" />
                  <Point X="-1.389929986802" Y="-4.387290197004" Z="1.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.05" />
                  <Point X="-1.384836323392" Y="-4.664826616928" Z="1.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.05" />
                  <Point X="-1.239786725292" Y="-4.924095004833" Z="1.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.05" />
                  <Point X="-0.941723636421" Y="-4.989683330361" Z="1.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.05" />
                  <Point X="-0.651873155196" Y="-4.395007822436" Z="1.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.05" />
                  <Point X="-0.321123655151" Y="-3.38050851126" Z="1.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.05" />
                  <Point X="-0.104861733605" Y="-3.23678458079" Z="1.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.05" />
                  <Point X="0.148497345756" Y="-3.250327464498" Z="1.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.05" />
                  <Point X="0.349322204281" Y="-3.421137244508" Z="1.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.05" />
                  <Point X="0.582881705205" Y="-4.137528207379" Z="1.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.05" />
                  <Point X="0.923369127278" Y="-4.994560928364" Z="1.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.05" />
                  <Point X="1.102949689595" Y="-4.957998692266" Z="1.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.05" />
                  <Point X="1.086119280946" Y="-4.251045398899" Z="1.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.05" />
                  <Point X="0.988887067678" Y="-3.127799047075" Z="1.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.05" />
                  <Point X="1.116651033026" Y="-2.937613692199" Z="1.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.05" />
                  <Point X="1.327759171211" Y="-2.863103964613" Z="1.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.05" />
                  <Point X="1.549145124295" Y="-2.934535179033" Z="1.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.05" />
                  <Point X="2.061459719335" Y="-3.543950625315" Z="1.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.05" />
                  <Point X="2.776471927318" Y="-4.252585356044" Z="1.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.05" />
                  <Point X="2.968189252217" Y="-4.121059521234" Z="1.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.05" />
                  <Point X="2.725636971877" Y="-3.509342310239" Z="1.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.05" />
                  <Point X="2.248363331904" Y="-2.59564495345" Z="1.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.05" />
                  <Point X="2.287586642604" Y="-2.400990147495" Z="1.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.05" />
                  <Point X="2.431908230877" Y="-2.271314667363" Z="1.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.05" />
                  <Point X="2.632861850062" Y="-2.255084677587" Z="1.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.05" />
                  <Point X="3.278071286868" Y="-2.592112528397" Z="1.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.05" />
                  <Point X="4.167454773641" Y="-2.901101846319" Z="1.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.05" />
                  <Point X="4.33319942813" Y="-2.647159558656" Z="1.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.05" />
                  <Point X="3.899869475733" Y="-2.15719046505" Z="1.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.05" />
                  <Point X="3.133849886627" Y="-1.522988934539" Z="1.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.05" />
                  <Point X="3.101481467206" Y="-1.358117847166" Z="1.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.05" />
                  <Point X="3.172314012308" Y="-1.210012166349" Z="1.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.05" />
                  <Point X="3.324152955603" Y="-1.132253915008" Z="1.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.05" />
                  <Point X="4.023318378155" Y="-1.198074014716" Z="1.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.05" />
                  <Point X="4.956493325757" Y="-1.097556816546" Z="1.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.05" />
                  <Point X="5.02459881111" Y="-0.724476708434" Z="1.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.05" />
                  <Point X="4.509937700852" Y="-0.424983893216" Z="1.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.05" />
                  <Point X="3.693731286879" Y="-0.189469688395" Z="1.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.05" />
                  <Point X="3.622909867022" Y="-0.12588371437" Z="1.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.05" />
                  <Point X="3.589092441153" Y="-0.039985730208" Z="1.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.05" />
                  <Point X="3.592279009271" Y="0.05662480101" Z="1.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.05" />
                  <Point X="3.632469571378" Y="0.138065024205" Z="1.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.05" />
                  <Point X="3.709664127473" Y="0.198679155847" Z="1.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.05" />
                  <Point X="4.286030116455" Y="0.364988039553" Z="1.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.05" />
                  <Point X="5.009388885173" Y="0.817251281688" Z="1.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.05" />
                  <Point X="4.922723448573" Y="1.236394527596" Z="1.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.05" />
                  <Point X="4.294034398583" Y="1.331415886483" Z="1.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.05" />
                  <Point X="3.407932438133" Y="1.229317978635" Z="1.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.05" />
                  <Point X="3.32814241691" Y="1.257445691619" Z="1.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.05" />
                  <Point X="3.271151337524" Y="1.316483958927" Z="1.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.05" />
                  <Point X="3.24090496664" Y="1.396907001822" Z="1.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.05" />
                  <Point X="3.246207526867" Y="1.477459187398" Z="1.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.05" />
                  <Point X="3.288982936159" Y="1.553495807999" Z="1.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.05" />
                  <Point X="3.782415926133" Y="1.944968742421" Z="1.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.05" />
                  <Point X="4.324739051546" Y="2.657714219641" Z="1.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.05" />
                  <Point X="4.099906271954" Y="2.992922033153" Z="1.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.05" />
                  <Point X="3.384584880707" Y="2.772011038185" Z="1.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.05" />
                  <Point X="2.462821003157" Y="2.254415135207" Z="1.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.05" />
                  <Point X="2.38890075727" Y="2.250435743483" Z="1.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.05" />
                  <Point X="2.323060652863" Y="2.279078553264" Z="1.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.05" />
                  <Point X="2.271680046971" Y="2.333964207519" Z="1.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.05" />
                  <Point X="2.248993959276" Y="2.400857686563" Z="1.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.05" />
                  <Point X="2.25811280745" Y="2.476648572725" Z="1.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.05" />
                  <Point X="2.623614314674" Y="3.127554201248" Z="1.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.05" />
                  <Point X="2.908758312501" Y="4.158619330295" Z="1.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.05" />
                  <Point X="2.520379651085" Y="4.404847197549" Z="1.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.05" />
                  <Point X="2.114441065762" Y="4.613611469329" Z="1.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.05" />
                  <Point X="1.693588371974" Y="4.784143924138" Z="1.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.05" />
                  <Point X="1.133314049041" Y="4.940859273257" Z="1.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.05" />
                  <Point X="0.470264185911" Y="5.04731167915" Z="1.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.05" />
                  <Point X="0.113263375226" Y="4.777829166123" Z="1.05" />
                  <Point X="0" Y="4.355124473572" Z="1.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>