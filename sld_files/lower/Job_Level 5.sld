<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#125" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="538" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.995283203125" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.595044128418" Y="-28.63098828125" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542361328125" Y="-28.467373046875" />
                  <Point X="0.529387512207" Y="-28.448681640625" />
                  <Point X="0.378633666992" Y="-28.23147265625" />
                  <Point X="0.35675201416" Y="-28.20901953125" />
                  <Point X="0.330496673584" Y="-28.189775390625" />
                  <Point X="0.302493469238" Y="-28.17566796875" />
                  <Point X="0.282417205811" Y="-28.1694375" />
                  <Point X="0.049134460449" Y="-28.09703515625" />
                  <Point X="0.020976732254" Y="-28.092765625" />
                  <Point X="-0.008664748192" Y="-28.092765625" />
                  <Point X="-0.036822174072" Y="-28.09703515625" />
                  <Point X="-0.056898582458" Y="-28.103265625" />
                  <Point X="-0.290181335449" Y="-28.17566796875" />
                  <Point X="-0.318184997559" Y="-28.18977734375" />
                  <Point X="-0.344440338135" Y="-28.209021484375" />
                  <Point X="-0.366324554443" Y="-28.2314765625" />
                  <Point X="-0.379298248291" Y="-28.250169921875" />
                  <Point X="-0.530052246094" Y="-28.467376953125" />
                  <Point X="-0.53818927002" Y="-28.481572265625" />
                  <Point X="-0.55099005127" Y="-28.5125234375" />
                  <Point X="-0.916584716797" Y="-29.87694140625" />
                  <Point X="-1.079341796875" Y="-29.845349609375" />
                  <Point X="-1.098549560547" Y="-29.840408203125" />
                  <Point X="-1.24641784668" Y="-29.80236328125" />
                  <Point X="-1.218784057617" Y="-29.592462890625" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201293945" Y="-29.54793359375" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.221304931641" Y="-29.49211328125" />
                  <Point X="-1.277035888672" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323646362305" Y="-29.131203125" />
                  <Point X="-1.342130004883" Y="-29.114994140625" />
                  <Point X="-1.556907226563" Y="-28.926638671875" />
                  <Point X="-1.583188964844" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.643029296875" Y="-28.890966796875" />
                  <Point X="-1.667560913086" Y="-28.889359375" />
                  <Point X="-1.952618408203" Y="-28.87067578125" />
                  <Point X="-1.983414672852" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.042657226562" Y="-28.89480078125" />
                  <Point X="-2.063098388672" Y="-28.908458984375" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312789550781" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.480147949219" Y="-29.28848828125" />
                  <Point X="-2.801715820312" Y="-29.0893828125" />
                  <Point X="-2.828301269531" Y="-29.068912109375" />
                  <Point X="-3.104721435547" Y="-28.856078125" />
                  <Point X="-2.482750244141" Y="-27.77879296875" />
                  <Point X="-2.423760986328" Y="-27.676619140625" />
                  <Point X="-2.412855957031" Y="-27.647638671875" />
                  <Point X="-2.406587158203" Y="-27.61609765625" />
                  <Point X="-2.405697265625" Y="-27.584298828125" />
                  <Point X="-2.415354980469" Y="-27.55398828125" />
                  <Point X="-2.430943847656" Y="-27.52380078125" />
                  <Point X="-2.448176757812" Y="-27.500216796875" />
                  <Point X="-2.464150634766" Y="-27.4842421875" />
                  <Point X="-2.489310791016" Y="-27.466212890625" />
                  <Point X="-2.518140380859" Y="-27.45199609375" />
                  <Point X="-2.547758056641" Y="-27.44301171875" />
                  <Point X="-2.578691650391" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-3.8180234375" Y="-28.141802734375" />
                  <Point X="-4.082861816406" Y="-27.793857421875" />
                  <Point X="-4.101924316406" Y="-27.76189453125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.209166503906" Y="-26.577712890625" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083404296875" Y="-26.473779296875" />
                  <Point X="-3.064828613281" Y="-26.4442890625" />
                  <Point X="-3.053245605469" Y="-26.417474609375" />
                  <Point X="-3.046151611328" Y="-26.390083984375" />
                  <Point X="-3.043347900391" Y="-26.3596484375" />
                  <Point X="-3.045559814453" Y="-26.32797265625" />
                  <Point X="-3.052984375" Y="-26.297228515625" />
                  <Point X="-3.070089599609" Y="-26.270625" />
                  <Point X="-3.093030273438" Y="-26.24543359375" />
                  <Point X="-3.115083984375" Y="-26.227525390625" />
                  <Point X="-3.139455566406" Y="-26.213181640625" />
                  <Point X="-3.168716064453" Y="-26.201958984375" />
                  <Point X="-3.200603759766" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.730690917969" Y="-26.39169921875" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.83912109375" Y="-25.957392578125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.650449951172" Y="-25.2519140625" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.515483886719" Y="-25.2138671875" />
                  <Point X="-3.485522216797" Y="-25.197935546875" />
                  <Point X="-3.459981445312" Y="-25.1802109375" />
                  <Point X="-3.436248291016" Y="-25.156544921875" />
                  <Point X="-3.416270263672" Y="-25.127927734375" />
                  <Point X="-3.403436279297" Y="-25.101708984375" />
                  <Point X="-3.394918212891" Y="-25.074265625" />
                  <Point X="-3.390673095703" Y="-25.0439296875" />
                  <Point X="-3.391405761719" Y="-25.011923828125" />
                  <Point X="-3.395650390625" Y="-24.9859375" />
                  <Point X="-3.404168457031" Y="-24.9584921875" />
                  <Point X="-3.419694091797" Y="-24.928607421875" />
                  <Point X="-3.441136474609" Y="-24.900826171875" />
                  <Point X="-3.462172851562" Y="-24.880828125" />
                  <Point X="-3.4877265625" Y="-24.863091796875" />
                  <Point X="-3.501924560547" Y="-24.854953125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.824487792969" Y="-24.023025390625" />
                  <Point X="-4.814333984375" Y="-23.985556640625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-3.849946289063" Y="-23.689111328125" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744997314453" Y="-23.700658203125" />
                  <Point X="-3.723448242188" Y="-23.698775390625" />
                  <Point X="-3.7031796875" Y="-23.69475" />
                  <Point X="-3.698268066406" Y="-23.693203125" />
                  <Point X="-3.641709228516" Y="-23.675369140625" />
                  <Point X="-3.622775878906" Y="-23.667037109375" />
                  <Point X="-3.604032226562" Y="-23.65621484375" />
                  <Point X="-3.587353271484" Y="-23.643984375" />
                  <Point X="-3.573716064453" Y="-23.628435546875" />
                  <Point X="-3.561301757812" Y="-23.61070703125" />
                  <Point X="-3.551352539062" Y="-23.592572265625" />
                  <Point X="-3.549399414062" Y="-23.587857421875" />
                  <Point X="-3.526705078125" Y="-23.533068359375" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532047607422" Y="-23.410626953125" />
                  <Point X="-3.534404052734" Y="-23.406099609375" />
                  <Point X="-3.561787353516" Y="-23.353498046875" />
                  <Point X="-3.573282226562" Y="-23.33629296875" />
                  <Point X="-3.587194824219" Y="-23.319712890625" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-4.351859375" Y="-22.730125" />
                  <Point X="-4.081155761719" Y="-22.266345703125" />
                  <Point X="-4.054259277344" Y="-22.2317734375" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.258419189453" Y="-22.125443359375" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187727539062" Y="-22.163658203125" />
                  <Point X="-3.167090087891" Y="-22.170166015625" />
                  <Point X="-3.146829101562" Y="-22.174201171875" />
                  <Point X="-3.140013916016" Y="-22.174798828125" />
                  <Point X="-3.061243652344" Y="-22.181689453125" />
                  <Point X="-3.040566894531" Y="-22.18123828125" />
                  <Point X="-3.019111328125" Y="-22.1784140625" />
                  <Point X="-2.999020263672" Y="-22.173498046875" />
                  <Point X="-2.980469238281" Y="-22.1643515625" />
                  <Point X="-2.962216796875" Y="-22.152724609375" />
                  <Point X="-2.946095214844" Y="-22.1397890625" />
                  <Point X="-2.941265136719" Y="-22.1349609375" />
                  <Point X="-2.885353271484" Y="-22.079048828125" />
                  <Point X="-2.872406738281" Y="-22.062916015625" />
                  <Point X="-2.860777587891" Y="-22.044662109375" />
                  <Point X="-2.851628662109" Y="-22.026111328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435546875" Y="-21.963880859375" />
                  <Point X="-2.844028564453" Y="-21.9571015625" />
                  <Point X="-2.850920166016" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.183332763672" Y="-21.275404296875" />
                  <Point X="-2.700621337891" Y="-20.905314453125" />
                  <Point X="-2.658270996094" Y="-20.88178515625" />
                  <Point X="-2.167036621094" Y="-20.6088671875" />
                  <Point X="-2.059041015625" Y="-20.749609375" />
                  <Point X="-2.043194824219" Y="-20.770259765625" />
                  <Point X="-2.028887451172" Y="-20.785205078125" />
                  <Point X="-2.012308227539" Y="-20.799115234375" />
                  <Point X="-1.995114868164" Y="-20.810603515625" />
                  <Point X="-1.987569946289" Y="-20.81453125" />
                  <Point X="-1.899898803711" Y="-20.860171875" />
                  <Point X="-1.880625366211" Y="-20.86766796875" />
                  <Point X="-1.859718505859" Y="-20.873271484375" />
                  <Point X="-1.839268798828" Y="-20.876419921875" />
                  <Point X="-1.818622436523" Y="-20.87506640625" />
                  <Point X="-1.797306884766" Y="-20.871306640625" />
                  <Point X="-1.777447143555" Y="-20.865515625" />
                  <Point X="-1.769588500977" Y="-20.862259765625" />
                  <Point X="-1.67827331543" Y="-20.824435546875" />
                  <Point X="-1.660146118164" Y="-20.814490234375" />
                  <Point X="-1.642416625977" Y="-20.802076171875" />
                  <Point X="-1.626864257812" Y="-20.7884375" />
                  <Point X="-1.614632568359" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595479614258" Y="-20.734076171875" />
                  <Point X="-1.592921875" Y="-20.725962890625" />
                  <Point X="-1.563200195312" Y="-20.63169921875" />
                  <Point X="-1.559165649414" Y="-20.6114140625" />
                  <Point X="-1.557279052734" Y="-20.5898515625" />
                  <Point X="-1.557730224609" Y="-20.569171875" />
                  <Point X="-1.584201538086" Y="-20.368103515625" />
                  <Point X="-0.949622253418" Y="-20.19019140625" />
                  <Point X="-0.898293212891" Y="-20.184181640625" />
                  <Point X="-0.294711181641" Y="-20.113541015625" />
                  <Point X="-0.149215988159" Y="-20.656537109375" />
                  <Point X="-0.133903381348" Y="-20.713685546875" />
                  <Point X="-0.12112991333" Y="-20.741876953125" />
                  <Point X="-0.103271614075" Y="-20.768603515625" />
                  <Point X="-0.082114059448" Y="-20.791193359375" />
                  <Point X="-0.054818141937" Y="-20.805783203125" />
                  <Point X="-0.024380065918" Y="-20.816115234375" />
                  <Point X="0.006155892849" Y="-20.82115625" />
                  <Point X="0.036691745758" Y="-20.816115234375" />
                  <Point X="0.067129974365" Y="-20.805783203125" />
                  <Point X="0.094426002502" Y="-20.791193359375" />
                  <Point X="0.115583602905" Y="-20.768603515625" />
                  <Point X="0.133441741943" Y="-20.741876953125" />
                  <Point X="0.146215209961" Y="-20.713685546875" />
                  <Point X="0.307419525146" Y="-20.1120625" />
                  <Point X="0.844041442871" Y="-20.16826171875" />
                  <Point X="0.886520446777" Y="-20.178517578125" />
                  <Point X="1.481023193359" Y="-20.322048828125" />
                  <Point X="1.507315185547" Y="-20.331583984375" />
                  <Point X="1.894648803711" Y="-20.47207421875" />
                  <Point X="1.921385864258" Y="-20.484578125" />
                  <Point X="2.294573486328" Y="-20.65910546875" />
                  <Point X="2.320444824219" Y="-20.674177734375" />
                  <Point X="2.680977783203" Y="-20.884224609375" />
                  <Point X="2.705344970703" Y="-20.901552734375" />
                  <Point X="2.943260498047" Y="-21.07074609375" />
                  <Point X="2.215856445313" Y="-22.330646484375" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.140566162109" Y="-22.463955078125" />
                  <Point X="2.131375244141" Y="-22.490306640625" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108418457031" Y="-22.5861484375" />
                  <Point X="2.107744140625" Y="-22.61055078125" />
                  <Point X="2.108391113281" Y="-22.624548828125" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140066894531" Y="-22.752521484375" />
                  <Point X="2.143470947266" Y="-22.7575390625" />
                  <Point X="2.183024658203" Y="-22.815830078125" />
                  <Point X="2.197622070313" Y="-22.832685546875" />
                  <Point X="2.215937744141" Y="-22.849390625" />
                  <Point X="2.226613769531" Y="-22.8578125" />
                  <Point X="2.284906005859" Y="-22.8973671875" />
                  <Point X="2.304961669922" Y="-22.907734375" />
                  <Point X="2.327061767578" Y="-22.916001953125" />
                  <Point X="2.34899609375" Y="-22.921341796875" />
                  <Point X="2.354475830078" Y="-22.922001953125" />
                  <Point X="2.418387451172" Y="-22.929708984375" />
                  <Point X="2.441105957031" Y="-22.929712890625" />
                  <Point X="2.466372070312" Y="-22.926673828125" />
                  <Point X="2.4795703125" Y="-22.92412890625" />
                  <Point X="2.553494628906" Y="-22.904359375" />
                  <Point X="2.5652890625" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.962660400391" Y="-22.096501953125" />
                  <Point X="3.967325439453" Y="-22.09380859375" />
                  <Point X="4.123271972656" Y="-22.3105390625" />
                  <Point X="4.13685546875" Y="-22.332986328125" />
                  <Point X="4.262197753906" Y="-22.5401171875" />
                  <Point X="3.320674804688" Y="-23.262572265625" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.218286621094" Y="-23.343052734375" />
                  <Point X="3.204463134766" Y="-23.358275390625" />
                  <Point X="3.199394775391" Y="-23.364345703125" />
                  <Point X="3.14619140625" Y="-23.43375390625" />
                  <Point X="3.1346796875" Y="-23.453185546875" />
                  <Point X="3.124504638672" Y="-23.476236328125" />
                  <Point X="3.119924560547" Y="-23.489013671875" />
                  <Point X="3.100106201172" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739746094" Y="-23.628234375" />
                  <Point X="3.099139892578" Y="-23.63501953125" />
                  <Point X="3.115408691406" Y="-23.7138671875" />
                  <Point X="3.122641601562" Y="-23.7354375" />
                  <Point X="3.133647705078" Y="-23.7586015625" />
                  <Point X="3.140090576172" Y="-23.770046875" />
                  <Point X="3.184340087891" Y="-23.8373046875" />
                  <Point X="3.198884521484" Y="-23.854541015625" />
                  <Point X="3.216121826172" Y="-23.87062890625" />
                  <Point X="3.234348144531" Y="-23.883966796875" />
                  <Point X="3.239873046875" Y="-23.887076171875" />
                  <Point X="3.303996826172" Y="-23.923171875" />
                  <Point X="3.325436767578" Y="-23.9319921875" />
                  <Point X="3.350874267578" Y="-23.9389765625" />
                  <Point X="3.363581787109" Y="-23.941548828125" />
                  <Point X="3.45028125" Y="-23.953005859375" />
                  <Point X="3.462698730469" Y="-23.95382421875" />
                  <Point X="3.488203369141" Y="-23.953015625" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.845936523438" Y="-24.06719140625" />
                  <Point X="4.850216796875" Y="-24.094685546875" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.819596923828" Y="-24.642806640625" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.700321044922" Y="-24.676404296875" />
                  <Point X="3.68091015625" Y="-24.6856484375" />
                  <Point X="3.674216308594" Y="-24.689169921875" />
                  <Point X="3.589036865234" Y="-24.738404296875" />
                  <Point X="3.570666748047" Y="-24.752236328125" />
                  <Point X="3.551956054688" Y="-24.77026171875" />
                  <Point X="3.5431328125" Y="-24.78002734375" />
                  <Point X="3.492025146484" Y="-24.845150390625" />
                  <Point X="3.480300048828" Y="-24.86443359375" />
                  <Point X="3.470526367188" Y="-24.8858984375" />
                  <Point X="3.463680664062" Y="-24.9073984375" />
                  <Point X="3.462214599609" Y="-24.9150546875" />
                  <Point X="3.445178466797" Y="-25.004009765625" />
                  <Point X="3.443628662109" Y="-25.0271328125" />
                  <Point X="3.445094726562" Y="-25.053595703125" />
                  <Point X="3.446644775391" Y="-25.066208984375" />
                  <Point X="3.463680664062" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.1766640625" />
                  <Point X="3.480300292969" Y="-25.19812890625" />
                  <Point X="3.492019287109" Y="-25.21740234375" />
                  <Point X="3.496417480469" Y="-25.2230078125" />
                  <Point X="3.547525146484" Y="-25.288130859375" />
                  <Point X="3.56432421875" Y="-25.3047734375" />
                  <Point X="3.585966552734" Y="-25.3214296875" />
                  <Point X="3.596365234375" Y="-25.328392578125" />
                  <Point X="3.681544677734" Y="-25.37762890625" />
                  <Point X="3.692708984375" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.89147265625" Y="-25.706962890625" />
                  <Point X="4.855022460938" Y="-25.948728515625" />
                  <Point X="4.849537597656" Y="-25.972763671875" />
                  <Point X="4.801174316406" Y="-26.18469921875" />
                  <Point X="3.543853027344" Y="-26.019169921875" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.400498779297" Y="-26.003326171875" />
                  <Point X="3.368965576172" Y="-26.007166015625" />
                  <Point X="3.360271728516" Y="-26.00863671875" />
                  <Point X="3.193094726563" Y="-26.04497265625" />
                  <Point X="3.162729980469" Y="-26.055689453125" />
                  <Point X="3.130709960938" Y="-26.076806640625" />
                  <Point X="3.109513671875" Y="-26.098041015625" />
                  <Point X="3.103701171875" Y="-26.104419921875" />
                  <Point X="3.002653320312" Y="-26.22594921875" />
                  <Point X="2.986627197266" Y="-26.250416015625" />
                  <Point X="2.974333496094" Y="-26.283384765625" />
                  <Point X="2.969491455078" Y="-26.311482421875" />
                  <Point X="2.968511230469" Y="-26.31891015625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.955109130859" Y="-26.508484375" />
                  <Point X="2.965750488281" Y="-26.54564453125" />
                  <Point X="2.980223876953" Y="-26.57319140625" />
                  <Point X="2.984412353516" Y="-26.580380859375" />
                  <Point X="3.076930419922" Y="-26.724287109375" />
                  <Point X="3.086930664063" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="4.213122070312" Y="-27.6068828125" />
                  <Point X="4.124811035156" Y="-27.74978515625" />
                  <Point X="4.113466308594" Y="-27.765904296875" />
                  <Point X="4.028980957031" Y="-27.885947265625" />
                  <Point X="2.907359863281" Y="-27.23837890625" />
                  <Point X="2.800954833984" Y="-27.1769453125" />
                  <Point X="2.778269775391" Y="-27.167515625" />
                  <Point X="2.745032714844" Y="-27.158521484375" />
                  <Point X="2.7371015625" Y="-27.156734375" />
                  <Point X="2.538134277344" Y="-27.12080078125" />
                  <Point X="2.505974853516" Y="-27.119083984375" />
                  <Point X="2.467714355469" Y="-27.126591796875" />
                  <Point X="2.437817871094" Y="-27.139248046875" />
                  <Point X="2.430608398438" Y="-27.1426640625" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.241146240234" Y="-27.24612890625" />
                  <Point X="2.216854736328" Y="-27.27238671875" />
                  <Point X="2.200477050781" Y="-27.2986796875" />
                  <Point X="2.197045166016" Y="-27.3046640625" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.098733398438" Y="-27.500109375" />
                  <Point X="2.094406005859" Y="-27.539166015625" />
                  <Point X="2.0977109375" Y="-27.572791015625" />
                  <Point X="2.098767822266" Y="-27.580380859375" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.781835693359" Y="-29.111654296875" />
                  <Point X="2.769165771484" Y="-29.11985546875" />
                  <Point X="2.701764648438" Y="-29.163482421875" />
                  <Point X="1.839742919922" Y="-28.04007421875" />
                  <Point X="1.758546020508" Y="-27.934255859375" />
                  <Point X="1.747506469727" Y="-27.922181640625" />
                  <Point X="1.721924072266" Y="-27.900556640625" />
                  <Point X="1.705036132812" Y="-27.88969921875" />
                  <Point X="1.508800537109" Y="-27.7635390625" />
                  <Point X="1.479748535156" Y="-27.749646484375" />
                  <Point X="1.440955444336" Y="-27.741947265625" />
                  <Point X="1.406028442383" Y="-27.742427734375" />
                  <Point X="1.398630493164" Y="-27.742818359375" />
                  <Point X="1.184013549805" Y="-27.76256640625" />
                  <Point X="1.155378051758" Y="-27.76853515625" />
                  <Point X="1.122471313477" Y="-27.783798828125" />
                  <Point X="1.095376586914" Y="-27.80340625" />
                  <Point X="1.090333740234" Y="-27.8073203125" />
                  <Point X="0.924611816406" Y="-27.94511328125" />
                  <Point X="0.904141174316" Y="-27.96886328125" />
                  <Point X="0.887249023438" Y="-27.996689453125" />
                  <Point X="0.875624450684" Y="-28.025810546875" />
                  <Point X="0.871360351562" Y="-28.045427734375" />
                  <Point X="0.821810241699" Y="-28.273396484375" />
                  <Point X="0.819724609375" Y="-28.289626953125" />
                  <Point X="0.819742370605" Y="-28.32312109375" />
                  <Point X="1.022039855957" Y="-29.859724609375" />
                  <Point X="1.022065063477" Y="-29.859916015625" />
                  <Point X="0.975707824707" Y="-29.870078125" />
                  <Point X="0.96396105957" Y="-29.8722109375" />
                  <Point X="0.929315551758" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058448242188" Y="-29.752630859375" />
                  <Point X="-1.074880493164" Y="-29.748404296875" />
                  <Point X="-1.141246337891" Y="-29.731328125" />
                  <Point X="-1.124596801758" Y="-29.60486328125" />
                  <Point X="-1.120775756836" Y="-29.575841796875" />
                  <Point X="-1.120077392578" Y="-29.5681015625" />
                  <Point X="-1.119451660156" Y="-29.5410390625" />
                  <Point X="-1.121759033203" Y="-29.50933203125" />
                  <Point X="-1.123333862305" Y="-29.497693359375" />
                  <Point X="-1.128130249023" Y="-29.473580078125" />
                  <Point X="-1.183861206055" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736938477" Y="-29.10762890625" />
                  <Point X="-1.230575561523" Y="-29.094859375" />
                  <Point X="-1.250212036133" Y="-29.07093359375" />
                  <Point X="-1.261010131836" Y="-29.05977734375" />
                  <Point X="-1.279493774414" Y="-29.043568359375" />
                  <Point X="-1.494270996094" Y="-28.855212890625" />
                  <Point X="-1.506739257812" Y="-28.84596484375" />
                  <Point X="-1.533020996094" Y="-28.82962109375" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591317016602" Y="-28.805474609375" />
                  <Point X="-1.621459960938" Y="-28.798447265625" />
                  <Point X="-1.636817749023" Y="-28.796169921875" />
                  <Point X="-1.661349365234" Y="-28.7945625" />
                  <Point X="-1.946406860352" Y="-28.77587890625" />
                  <Point X="-1.961930175781" Y="-28.7761328125" />
                  <Point X="-1.99272644043" Y="-28.779166015625" />
                  <Point X="-2.007999633789" Y="-28.7819453125" />
                  <Point X="-2.039048095703" Y="-28.790263671875" />
                  <Point X="-2.053667724609" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436035156" Y="-28.815810546875" />
                  <Point X="-2.115877197266" Y="-28.82946875" />
                  <Point X="-2.353402587891" Y="-28.988177734375" />
                  <Point X="-2.359684570312" Y="-28.9927578125" />
                  <Point X="-2.380448974609" Y="-29.01013671875" />
                  <Point X="-2.402762451172" Y="-29.032775390625" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.503200927734" Y="-29.162478515625" />
                  <Point X="-2.747595458984" Y="-29.01115625" />
                  <Point X="-2.770342529297" Y="-28.993640625" />
                  <Point X="-2.980862060547" Y="-28.831546875" />
                  <Point X="-2.400477783203" Y="-27.82629296875" />
                  <Point X="-2.341488525391" Y="-27.724119140625" />
                  <Point X="-2.334847412109" Y="-27.710076171875" />
                  <Point X="-2.323942382812" Y="-27.681095703125" />
                  <Point X="-2.319678466797" Y="-27.666158203125" />
                  <Point X="-2.313409667969" Y="-27.6346171875" />
                  <Point X="-2.311624267578" Y="-27.618755859375" />
                  <Point X="-2.310734375" Y="-27.58695703125" />
                  <Point X="-2.315180908203" Y="-27.555458984375" />
                  <Point X="-2.324838623047" Y="-27.5251484375" />
                  <Point X="-2.3309453125" Y="-27.5103984375" />
                  <Point X="-2.346534179688" Y="-27.4802109375" />
                  <Point X="-2.354239257812" Y="-27.467751953125" />
                  <Point X="-2.371472167969" Y="-27.44416796875" />
                  <Point X="-2.381" Y="-27.43304296875" />
                  <Point X="-2.396973876953" Y="-27.417068359375" />
                  <Point X="-2.408815673828" Y="-27.407021484375" />
                  <Point X="-2.433975830078" Y="-27.3889921875" />
                  <Point X="-2.447294189453" Y="-27.381009765625" />
                  <Point X="-2.476123779297" Y="-27.36679296875" />
                  <Point X="-2.490563476562" Y="-27.3610859375" />
                  <Point X="-2.520181152344" Y="-27.3521015625" />
                  <Point X="-2.550869384766" Y="-27.3480625" />
                  <Point X="-2.581802978516" Y="-27.349076171875" />
                  <Point X="-2.597226318359" Y="-27.3508515625" />
                  <Point X="-2.628753173828" Y="-27.357123046875" />
                  <Point X="-2.643683837891" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.686683837891" Y="-27.378927734375" />
                  <Point X="-3.793088134766" Y="-28.0177109375" />
                  <Point X="-4.004018066406" Y="-27.74058984375" />
                  <Point X="-4.020333007812" Y="-27.713234375" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.151334228516" Y="-26.65308203125" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.035749023438" Y="-26.562517578125" />
                  <Point X="-3.013198730469" Y="-26.53778125" />
                  <Point X="-3.003021728516" Y="-26.524412109375" />
                  <Point X="-2.984446044922" Y="-26.494921875" />
                  <Point X="-2.977617431641" Y="-26.4819609375" />
                  <Point X="-2.966034423828" Y="-26.455146484375" />
                  <Point X="-2.961280029297" Y="-26.44129296875" />
                  <Point X="-2.954186035156" Y="-26.41390234375" />
                  <Point X="-2.951552246094" Y="-26.398798828125" />
                  <Point X="-2.948748535156" Y="-26.36836328125" />
                  <Point X="-2.948578613281" Y="-26.35303125" />
                  <Point X="-2.950790527344" Y="-26.32135546875" />
                  <Point X="-2.953214355469" Y="-26.305671875" />
                  <Point X="-2.960638916016" Y="-26.274927734375" />
                  <Point X="-2.973076416016" Y="-26.245849609375" />
                  <Point X="-2.990181640625" Y="-26.21924609375" />
                  <Point X="-2.999850097656" Y="-26.20666015625" />
                  <Point X="-3.022790771484" Y="-26.18146875" />
                  <Point X="-3.033145019531" Y="-26.171685546875" />
                  <Point X="-3.055198730469" Y="-26.15377734375" />
                  <Point X="-3.066898193359" Y="-26.14565234375" />
                  <Point X="-3.091269775391" Y="-26.13130859375" />
                  <Point X="-3.105435546875" Y="-26.124482421875" />
                  <Point X="-3.134696044922" Y="-26.113259765625" />
                  <Point X="-3.149790771484" Y="-26.10886328125" />
                  <Point X="-3.181678466797" Y="-26.102380859375" />
                  <Point X="-3.197294677734" Y="-26.10053515625" />
                  <Point X="-3.228619873047" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.740762695312" Y="-25.97411328125" />
                  <Point X="-4.745078125" Y="-25.94394140625" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.625862060547" Y="-25.343677734375" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.499425537109" Y="-25.309326171875" />
                  <Point X="-3.470882568359" Y="-25.29774609375" />
                  <Point X="-3.440920898438" Y="-25.281814453125" />
                  <Point X="-3.431359375" Y="-25.275982421875" />
                  <Point X="-3.405818603516" Y="-25.2582578125" />
                  <Point X="-3.392901611328" Y="-25.24748046875" />
                  <Point X="-3.369168457031" Y="-25.223814453125" />
                  <Point X="-3.358352294922" Y="-25.21092578125" />
                  <Point X="-3.338374267578" Y="-25.18230859375" />
                  <Point X="-3.330944335938" Y="-25.1696953125" />
                  <Point X="-3.318110351562" Y="-25.1434765625" />
                  <Point X="-3.312706298828" Y="-25.12987109375" />
                  <Point X="-3.304188232422" Y="-25.102427734375" />
                  <Point X="-3.300834960938" Y="-25.087431640625" />
                  <Point X="-3.29658984375" Y="-25.057095703125" />
                  <Point X="-3.295697998047" Y="-25.041755859375" />
                  <Point X="-3.296430664062" Y="-25.00975" />
                  <Point X="-3.297648193359" Y="-24.996609375" />
                  <Point X="-3.301892822266" Y="-24.970623046875" />
                  <Point X="-3.304919921875" Y="-24.95777734375" />
                  <Point X="-3.313437988281" Y="-24.93033203125" />
                  <Point X="-3.319866210938" Y="-24.9146953125" />
                  <Point X="-3.335391845703" Y="-24.884810546875" />
                  <Point X="-3.344489257812" Y="-24.8705625" />
                  <Point X="-3.365931640625" Y="-24.84278125" />
                  <Point X="-3.375682128906" Y="-24.83197265625" />
                  <Point X="-3.396718505859" Y="-24.811974609375" />
                  <Point X="-3.408004394531" Y="-24.80278515625" />
                  <Point X="-3.433558105469" Y="-24.785048828125" />
                  <Point X="-3.440481689453" Y="-24.780671875" />
                  <Point X="-3.465612548828" Y="-24.767166015625" />
                  <Point X="-3.496563964844" Y="-24.75436328125" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731330566406" Y="-24.042470703125" />
                  <Point X="-4.722641113281" Y="-24.010404296875" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-3.862346191406" Y="-23.783298828125" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.767739501953" Y="-23.79518359375" />
                  <Point X="-3.747070556641" Y="-23.795634765625" />
                  <Point X="-3.736728271484" Y="-23.795296875" />
                  <Point X="-3.715179199219" Y="-23.7934140625" />
                  <Point X="-3.704942382812" Y="-23.791955078125" />
                  <Point X="-3.684673828125" Y="-23.7879296875" />
                  <Point X="-3.669699462891" Y="-23.783806640625" />
                  <Point X="-3.613140625" Y="-23.76597265625" />
                  <Point X="-3.603443847656" Y="-23.762322265625" />
                  <Point X="-3.584510498047" Y="-23.753990234375" />
                  <Point X="-3.575273925781" Y="-23.74930859375" />
                  <Point X="-3.556530273438" Y="-23.738486328125" />
                  <Point X="-3.547854980469" Y="-23.73282421875" />
                  <Point X="-3.531176025391" Y="-23.72059375" />
                  <Point X="-3.515931396484" Y="-23.706625" />
                  <Point X="-3.502294189453" Y="-23.691076171875" />
                  <Point X="-3.495897949219" Y="-23.682927734375" />
                  <Point X="-3.483483642578" Y="-23.66519921875" />
                  <Point X="-3.478012939453" Y="-23.65640234375" />
                  <Point X="-3.468063720703" Y="-23.638267578125" />
                  <Point X="-3.461632080078" Y="-23.62421484375" />
                  <Point X="-3.438937744141" Y="-23.56942578125" />
                  <Point X="-3.435500244141" Y="-23.55965234375" />
                  <Point X="-3.429710693359" Y="-23.5397890625" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436011230469" Y="-23.39547265625" />
                  <Point X="-3.443505371094" Y="-23.37619921875" />
                  <Point X="-3.450135498047" Y="-23.36223828125" />
                  <Point X="-3.477518798828" Y="-23.30963671875" />
                  <Point X="-3.482795166016" Y="-23.30072265625" />
                  <Point X="-3.494290039062" Y="-23.283517578125" />
                  <Point X="-3.500508544922" Y="-23.2752265625" />
                  <Point X="-3.514421142578" Y="-23.258646484375" />
                  <Point X="-3.521501953125" Y="-23.251087890625" />
                  <Point X="-3.536443115234" Y="-23.23678515625" />
                  <Point X="-3.544303466797" Y="-23.230041015625" />
                  <Point X="-4.227613769531" Y="-22.705716796875" />
                  <Point X="-4.002297851562" Y="-22.319697265625" />
                  <Point X="-3.979278076172" Y="-22.290107421875" />
                  <Point X="-3.726336669922" Y="-21.964986328125" />
                  <Point X="-3.305919189453" Y="-22.20771484375" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244921386719" Y="-22.24228125" />
                  <Point X="-3.225991943359" Y="-22.250611328125" />
                  <Point X="-3.216298095703" Y="-22.254259765625" />
                  <Point X="-3.195660644531" Y="-22.260767578125" />
                  <Point X="-3.185645751953" Y="-22.2633359375" />
                  <Point X="-3.165384765625" Y="-22.26737109375" />
                  <Point X="-3.148292724609" Y="-22.2694375" />
                  <Point X="-3.069522460938" Y="-22.276328125" />
                  <Point X="-3.059171142578" Y="-22.276666015625" />
                  <Point X="-3.038494384766" Y="-22.27621484375" />
                  <Point X="-3.028168945313" Y="-22.27542578125" />
                  <Point X="-3.006713378906" Y="-22.2726015625" />
                  <Point X="-2.996532226562" Y="-22.27069140625" />
                  <Point X="-2.976441162109" Y="-22.265775390625" />
                  <Point X="-2.957009765625" Y="-22.258705078125" />
                  <Point X="-2.938458740234" Y="-22.24955859375" />
                  <Point X="-2.929429199219" Y="-22.2444765625" />
                  <Point X="-2.911176757812" Y="-22.232849609375" />
                  <Point X="-2.902763427734" Y="-22.2268203125" />
                  <Point X="-2.886641845703" Y="-22.213884765625" />
                  <Point X="-2.87408984375" Y="-22.20213671875" />
                  <Point X="-2.818177978516" Y="-22.146224609375" />
                  <Point X="-2.811260986328" Y="-22.1385078125" />
                  <Point X="-2.798314453125" Y="-22.122375" />
                  <Point X="-2.792284912109" Y="-22.113958984375" />
                  <Point X="-2.780655761719" Y="-22.095705078125" />
                  <Point X="-2.775575927734" Y="-22.086681640625" />
                  <Point X="-2.766427001953" Y="-22.068130859375" />
                  <Point X="-2.759351318359" Y="-22.04869140625" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.986634765625" />
                  <Point X="-2.748458251953" Y="-21.965955078125" />
                  <Point X="-2.749389892578" Y="-21.948822265625" />
                  <Point X="-2.756281494141" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761781005859" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.05938671875" Y="-21.300083984375" />
                  <Point X="-2.648361816406" Y="-20.98495703125" />
                  <Point X="-2.6121328125" Y="-20.964828125" />
                  <Point X="-2.192524902344" Y="-20.731703125" />
                  <Point X="-2.134409423828" Y="-20.80744140625" />
                  <Point X="-2.118563232422" Y="-20.828091796875" />
                  <Point X="-2.111818603516" Y="-20.835955078125" />
                  <Point X="-2.097511230469" Y="-20.850900390625" />
                  <Point X="-2.089948486328" Y="-20.857982421875" />
                  <Point X="-2.073369384766" Y="-20.871892578125" />
                  <Point X="-2.065087646484" Y="-20.87810546875" />
                  <Point X="-2.047894165039" Y="-20.88959375" />
                  <Point X="-2.031437011719" Y="-20.898796875" />
                  <Point X="-1.94376574707" Y="-20.9444375" />
                  <Point X="-1.934334838867" Y="-20.9487109375" />
                  <Point X="-1.915061157227" Y="-20.95620703125" />
                  <Point X="-1.905219604492" Y="-20.9594296875" />
                  <Point X="-1.88431262207" Y="-20.965033203125" />
                  <Point X="-1.874174438477" Y="-20.967166015625" />
                  <Point X="-1.853724731445" Y="-20.970314453125" />
                  <Point X="-1.833054199219" Y="-20.971216796875" />
                  <Point X="-1.812407836914" Y="-20.96986328125" />
                  <Point X="-1.802120483398" Y="-20.968623046875" />
                  <Point X="-1.780804931641" Y="-20.96486328125" />
                  <Point X="-1.770712890625" Y="-20.9625078125" />
                  <Point X="-1.750853149414" Y="-20.956716796875" />
                  <Point X="-1.733226928711" Y="-20.950025390625" />
                  <Point X="-1.641911743164" Y="-20.912201171875" />
                  <Point X="-1.63257800293" Y="-20.907724609375" />
                  <Point X="-1.614450927734" Y="-20.897779296875" />
                  <Point X="-1.605657104492" Y="-20.892310546875" />
                  <Point X="-1.587927612305" Y="-20.879896484375" />
                  <Point X="-1.579779785156" Y="-20.873501953125" />
                  <Point X="-1.564227416992" Y="-20.85986328125" />
                  <Point X="-1.550252441406" Y="-20.84461328125" />
                  <Point X="-1.538020751953" Y="-20.827931640625" />
                  <Point X="-1.532359863281" Y="-20.819255859375" />
                  <Point X="-1.521538085938" Y="-20.80051171875" />
                  <Point X="-1.516855102539" Y="-20.79126953125" />
                  <Point X="-1.508523925781" Y="-20.772333984375" />
                  <Point X="-1.502317504883" Y="-20.754525390625" />
                  <Point X="-1.472595947266" Y="-20.66026171875" />
                  <Point X="-1.470025268555" Y="-20.65023046875" />
                  <Point X="-1.465990722656" Y="-20.6299453125" />
                  <Point X="-1.46452722168" Y="-20.6196953125" />
                  <Point X="-1.462640625" Y="-20.5981328125" />
                  <Point X="-1.462301635742" Y="-20.587779296875" />
                  <Point X="-1.462752807617" Y="-20.567099609375" />
                  <Point X="-1.46354309082" Y="-20.556771484375" />
                  <Point X="-1.47926574707" Y="-20.437345703125" />
                  <Point X="-0.931164428711" Y="-20.2836796875" />
                  <Point X="-0.887245788574" Y="-20.278537109375" />
                  <Point X="-0.365222381592" Y="-20.21744140625" />
                  <Point X="-0.240979019165" Y="-20.681125" />
                  <Point X="-0.225666397095" Y="-20.7382734375" />
                  <Point X="-0.220435317993" Y="-20.752892578125" />
                  <Point X="-0.207661911011" Y="-20.781083984375" />
                  <Point X="-0.200119308472" Y="-20.79465625" />
                  <Point X="-0.182260925293" Y="-20.8213828125" />
                  <Point X="-0.172608901978" Y="-20.833544921875" />
                  <Point X="-0.151451400757" Y="-20.856134765625" />
                  <Point X="-0.126896514893" Y="-20.8749765625" />
                  <Point X="-0.099600578308" Y="-20.88956640625" />
                  <Point X="-0.085354034424" Y="-20.8957421875" />
                  <Point X="-0.054916061401" Y="-20.90607421875" />
                  <Point X="-0.039853725433" Y="-20.909845703125" />
                  <Point X="-0.009317661285" Y="-20.91488671875" />
                  <Point X="0.021629491806" Y="-20.91488671875" />
                  <Point X="0.052165405273" Y="-20.909845703125" />
                  <Point X="0.067227592468" Y="-20.90607421875" />
                  <Point X="0.097665718079" Y="-20.8957421875" />
                  <Point X="0.111912261963" Y="-20.88956640625" />
                  <Point X="0.139208190918" Y="-20.8749765625" />
                  <Point X="0.16376322937" Y="-20.856134765625" />
                  <Point X="0.184920883179" Y="-20.833544921875" />
                  <Point X="0.194573196411" Y="-20.8213828125" />
                  <Point X="0.212431289673" Y="-20.79465625" />
                  <Point X="0.219973754883" Y="-20.781083984375" />
                  <Point X="0.232747146606" Y="-20.752892578125" />
                  <Point X="0.237978225708" Y="-20.7382734375" />
                  <Point X="0.378190704346" Y="-20.2149921875" />
                  <Point X="0.827876220703" Y="-20.262087890625" />
                  <Point X="0.864224914551" Y="-20.27086328125" />
                  <Point X="1.453586547852" Y="-20.413154296875" />
                  <Point X="1.474926269531" Y="-20.420892578125" />
                  <Point X="1.858260375977" Y="-20.559931640625" />
                  <Point X="1.881141479492" Y="-20.5706328125" />
                  <Point X="2.250448486328" Y="-20.743345703125" />
                  <Point X="2.272622802734" Y="-20.756263671875" />
                  <Point X="2.629438232422" Y="-20.96414453125" />
                  <Point X="2.650289550781" Y="-20.97897265625" />
                  <Point X="2.817780517578" Y="-21.098083984375" />
                  <Point X="2.133583984375" Y="-22.283146484375" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.061472412109" Y="-22.4087734375" />
                  <Point X="2.050865478516" Y="-22.432669921875" />
                  <Point X="2.041674560547" Y="-22.459021484375" />
                  <Point X="2.039599853516" Y="-22.465765625" />
                  <Point X="2.019831542969" Y="-22.53969140625" />
                  <Point X="2.017596679688" Y="-22.5505546875" />
                  <Point X="2.014408203125" Y="-22.572470703125" />
                  <Point X="2.013454711914" Y="-22.5835234375" />
                  <Point X="2.012780395508" Y="-22.60792578125" />
                  <Point X="2.012845458984" Y="-22.6149375" />
                  <Point X="2.01407434082" Y="-22.635921875" />
                  <Point X="2.021782348633" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.02914453125" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.045319335938" Y="-22.77611328125" />
                  <Point X="2.055677001953" Y="-22.7961484375" />
                  <Point X="2.06485546875" Y="-22.810873046875" />
                  <Point X="2.104409179688" Y="-22.8691640625" />
                  <Point X="2.111211669922" Y="-22.8780234375" />
                  <Point X="2.125809082031" Y="-22.89487890625" />
                  <Point X="2.133604003906" Y="-22.902875" />
                  <Point X="2.151919677734" Y="-22.919580078125" />
                  <Point X="2.157099853516" Y="-22.9239765625" />
                  <Point X="2.173271728516" Y="-22.936423828125" />
                  <Point X="2.231563964844" Y="-22.975978515625" />
                  <Point X="2.241282226562" Y="-22.981759765625" />
                  <Point X="2.261337890625" Y="-22.992126953125" />
                  <Point X="2.271675292969" Y="-22.996712890625" />
                  <Point X="2.293775390625" Y="-23.00498046875" />
                  <Point X="2.304590576172" Y="-23.008306640625" />
                  <Point X="2.326524902344" Y="-23.013646484375" />
                  <Point X="2.343113037109" Y="-23.0163203125" />
                  <Point X="2.407013916016" Y="-23.024025390625" />
                  <Point X="2.41837109375" Y="-23.024708984375" />
                  <Point X="2.441089599609" Y="-23.024712890625" />
                  <Point X="2.452450927734" Y="-23.024033203125" />
                  <Point X="2.477717041016" Y="-23.020994140625" />
                  <Point X="2.484358886719" Y="-23.019955078125" />
                  <Point X="2.504113525391" Y="-23.015904296875" />
                  <Point X="2.578037841797" Y="-22.996134765625" />
                  <Point X="2.584006347656" Y="-22.994326171875" />
                  <Point X="2.604415039063" Y="-22.986927734375" />
                  <Point X="2.627659912109" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.940403076172" Y="-22.219046875" />
                  <Point X="4.043953369141" Y="-22.362958984375" />
                  <Point X="4.055578125" Y="-22.382169921875" />
                  <Point X="4.136883300781" Y="-22.516529296875" />
                  <Point X="3.262842529297" Y="-23.187203125" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.166443359375" Y="-23.261654296875" />
                  <Point X="3.147957275391" Y="-23.2791875" />
                  <Point X="3.134133789062" Y="-23.29441015625" />
                  <Point X="3.123997070312" Y="-23.30655078125" />
                  <Point X="3.070793701172" Y="-23.375958984375" />
                  <Point X="3.064457519531" Y="-23.385333984375" />
                  <Point X="3.052945800781" Y="-23.404765625" />
                  <Point X="3.047770263672" Y="-23.414822265625" />
                  <Point X="3.037595214844" Y="-23.437873046875" />
                  <Point X="3.035076416016" Y="-23.4441796875" />
                  <Point X="3.028435058594" Y="-23.463427734375" />
                  <Point X="3.008616699219" Y="-23.53429296875" />
                  <Point X="3.006225585938" Y="-23.545337890625" />
                  <Point X="3.002771972656" Y="-23.56763671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003078125" Y="-23.63624609375" />
                  <Point X="3.006100097656" Y="-23.65421875" />
                  <Point X="3.022368896484" Y="-23.73306640625" />
                  <Point X="3.025337646484" Y="-23.7440703125" />
                  <Point X="3.032570556641" Y="-23.765640625" />
                  <Point X="3.036834716797" Y="-23.77620703125" />
                  <Point X="3.047840820312" Y="-23.79937109375" />
                  <Point X="3.0607265625" Y="-23.82226171875" />
                  <Point X="3.104976074219" Y="-23.88951953125" />
                  <Point X="3.111735107422" Y="-23.8985703125" />
                  <Point X="3.126279541016" Y="-23.915806640625" />
                  <Point X="3.134064941406" Y="-23.9239921875" />
                  <Point X="3.151302246094" Y="-23.940080078125" />
                  <Point X="3.160019042969" Y="-23.94729296875" />
                  <Point X="3.178245361328" Y="-23.960630859375" />
                  <Point X="3.193279785156" Y="-23.969865234375" />
                  <Point X="3.257403564453" Y="-24.0059609375" />
                  <Point X="3.267853271484" Y="-24.01102734375" />
                  <Point X="3.289293212891" Y="-24.01984765625" />
                  <Point X="3.300283447266" Y="-24.0236015625" />
                  <Point X="3.325720947266" Y="-24.0305859375" />
                  <Point X="3.351135986328" Y="-24.03573046875" />
                  <Point X="3.437835449219" Y="-24.0471875" />
                  <Point X="3.444033935547" Y="-24.04780078125" />
                  <Point X="3.465708984375" Y="-24.04877734375" />
                  <Point X="3.491213623047" Y="-24.04796875" />
                  <Point X="3.500603271484" Y="-24.047203125" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.752684570312" Y="-24.085767578125" />
                  <Point X="4.75634765625" Y="-24.109298828125" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="3.795009033203" Y="-24.55104296875" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.683718261719" Y="-24.581275390625" />
                  <Point X="3.659474365234" Y="-24.590634765625" />
                  <Point X="3.640063476562" Y="-24.59987890625" />
                  <Point X="3.62667578125" Y="-24.606921875" />
                  <Point X="3.541496337891" Y="-24.65615625" />
                  <Point X="3.531893066406" Y="-24.66251171875" />
                  <Point X="3.513522949219" Y="-24.67634375" />
                  <Point X="3.504756103516" Y="-24.6838203125" />
                  <Point X="3.486045410156" Y="-24.701845703125" />
                  <Point X="3.468398925781" Y="-24.721376953125" />
                  <Point X="3.417291259766" Y="-24.7865" />
                  <Point X="3.410853027344" Y="-24.79579296875" />
                  <Point X="3.399127929688" Y="-24.815076171875" />
                  <Point X="3.393841064453" Y="-24.82506640625" />
                  <Point X="3.384067382812" Y="-24.84653125" />
                  <Point X="3.380004150391" Y="-24.857076171875" />
                  <Point X="3.373158447266" Y="-24.878576171875" />
                  <Point X="3.368909912109" Y="-24.8971875" />
                  <Point X="3.351873779297" Y="-24.986142578125" />
                  <Point X="3.350391113281" Y="-24.99765625" />
                  <Point X="3.348841308594" Y="-25.020779296875" />
                  <Point X="3.348774169922" Y="-25.032388671875" />
                  <Point X="3.350240234375" Y="-25.0588515625" />
                  <Point X="3.353340332031" Y="-25.084078125" />
                  <Point X="3.370376220703" Y="-25.173033203125" />
                  <Point X="3.373158447266" Y="-25.183986328125" />
                  <Point X="3.380004150391" Y="-25.205486328125" />
                  <Point X="3.384067626953" Y="-25.216033203125" />
                  <Point X="3.393841552734" Y="-25.237498046875" />
                  <Point X="3.399127685547" Y="-25.247484375" />
                  <Point X="3.410846679688" Y="-25.2667578125" />
                  <Point X="3.421677734375" Y="-25.281650390625" />
                  <Point X="3.472785400391" Y="-25.3467734375" />
                  <Point X="3.480665039062" Y="-25.355619140625" />
                  <Point X="3.497464111328" Y="-25.37226171875" />
                  <Point X="3.506383544922" Y="-25.38005859375" />
                  <Point X="3.528025878906" Y="-25.39671484375" />
                  <Point X="3.548823242188" Y="-25.410640625" />
                  <Point X="3.634002685547" Y="-25.459876953125" />
                  <Point X="3.639489746094" Y="-25.4628125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.68302734375" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761614257812" Y="-25.93105078125" />
                  <Point X="4.756918457031" Y="-25.951626953125" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.556252929688" Y="-25.924982421875" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.424840332031" Y="-25.908443359375" />
                  <Point X="3.40095703125" Y="-25.908328125" />
                  <Point X="3.389015380859" Y="-25.9090234375" />
                  <Point X="3.357482177734" Y="-25.91286328125" />
                  <Point X="3.340094482422" Y="-25.9158046875" />
                  <Point X="3.172917480469" Y="-25.952140625" />
                  <Point X="3.161477294922" Y="-25.955388671875" />
                  <Point X="3.131112548828" Y="-25.96610546875" />
                  <Point X="3.110427734375" Y="-25.9763828125" />
                  <Point X="3.078407714844" Y="-25.9975" />
                  <Point X="3.063474609375" Y="-26.00969140625" />
                  <Point X="3.042278320312" Y="-26.03092578125" />
                  <Point X="3.030653320312" Y="-26.04368359375" />
                  <Point X="2.92960546875" Y="-26.165212890625" />
                  <Point X="2.923183837891" Y="-26.17389453125" />
                  <Point X="2.907157714844" Y="-26.198361328125" />
                  <Point X="2.897614257812" Y="-26.217224609375" />
                  <Point X="2.885320556641" Y="-26.250193359375" />
                  <Point X="2.880713378906" Y="-26.267251953125" />
                  <Point X="2.875871337891" Y="-26.295349609375" />
                  <Point X="2.873910888672" Y="-26.310205078125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.85908203125" Y="-26.479484375" />
                  <Point X="2.860162597656" Y="-26.511671875" />
                  <Point X="2.863780029297" Y="-26.534638671875" />
                  <Point X="2.874421386719" Y="-26.571798828125" />
                  <Point X="2.881651855469" Y="-26.589830078125" />
                  <Point X="2.896125244141" Y="-26.617376953125" />
                  <Point X="2.904502197266" Y="-26.631755859375" />
                  <Point X="2.997020263672" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.782353515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486572266" Y="-26.828119140625" />
                  <Point X="3.052795898438" Y="-26.836279296875" />
                  <Point X="4.087170654297" Y="-27.629982421875" />
                  <Point X="4.045493408203" Y="-27.697423828125" />
                  <Point X="4.035778320312" Y="-27.7112265625" />
                  <Point X="4.001272949219" Y="-27.76025390625" />
                  <Point X="2.954859863281" Y="-27.156107421875" />
                  <Point X="2.848454833984" Y="-27.094673828125" />
                  <Point X="2.837419433594" Y="-27.08922265625" />
                  <Point X="2.814734375" Y="-27.07979296875" />
                  <Point X="2.803084716797" Y="-27.075814453125" />
                  <Point X="2.76984765625" Y="-27.0668203125" />
                  <Point X="2.753985351562" Y="-27.06324609375" />
                  <Point X="2.555018066406" Y="-27.0273125" />
                  <Point X="2.543198486328" Y="-27.025935546875" />
                  <Point X="2.5110390625" Y="-27.02421875" />
                  <Point X="2.487681884766" Y="-27.025861328125" />
                  <Point X="2.449421386719" Y="-27.033369140625" />
                  <Point X="2.430679443359" Y="-27.039107421875" />
                  <Point X="2.400782958984" Y="-27.051763671875" />
                  <Point X="2.386364013672" Y="-27.058595703125" />
                  <Point X="2.221071044922" Y="-27.145587890625" />
                  <Point X="2.2118125" Y="-27.151154296875" />
                  <Point X="2.187643310547" Y="-27.167626953125" />
                  <Point X="2.171410888672" Y="-27.181615234375" />
                  <Point X="2.147119384766" Y="-27.207873046875" />
                  <Point X="2.136218505859" Y="-27.222158203125" />
                  <Point X="2.119840820312" Y="-27.248451171875" />
                  <Point X="2.112977050781" Y="-27.260419921875" />
                  <Point X="2.025984741211" Y="-27.425712890625" />
                  <Point X="2.02111328125" Y="-27.436568359375" />
                  <Point X="2.009793945312" Y="-27.466720703125" />
                  <Point X="2.004311157227" Y="-27.4896484375" />
                  <Point X="1.999983764648" Y="-27.528705078125" />
                  <Point X="1.999861694336" Y="-27.548458984375" />
                  <Point X="2.003166503906" Y="-27.582083984375" />
                  <Point X="2.005280151367" Y="-27.597263671875" />
                  <Point X="2.041213378906" Y="-27.796232421875" />
                  <Point X="2.043014770508" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.735893066406" Y="-29.027724609375" />
                  <Point X="2.723754150391" Y="-29.0360859375" />
                  <Point X="1.915111450195" Y="-27.9822421875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657958984" Y="-27.87015234375" />
                  <Point X="1.808835327148" Y="-27.84962890625" />
                  <Point X="1.783252807617" Y="-27.82800390625" />
                  <Point X="1.773299072266" Y="-27.820646484375" />
                  <Point X="1.756411010742" Y="-27.8097890625" />
                  <Point X="1.560175415039" Y="-27.68362890625" />
                  <Point X="1.549784301758" Y="-27.677833984375" />
                  <Point X="1.520732299805" Y="-27.66394140625" />
                  <Point X="1.498242431641" Y="-27.65646484375" />
                  <Point X="1.45944921875" Y="-27.648765625" />
                  <Point X="1.439648681641" Y="-27.64695703125" />
                  <Point X="1.404721679688" Y="-27.6474375" />
                  <Point X="1.38992590332" Y="-27.64821875" />
                  <Point X="1.175308837891" Y="-27.667966796875" />
                  <Point X="1.164628540039" Y="-27.669564453125" />
                  <Point X="1.135993041992" Y="-27.675533203125" />
                  <Point X="1.115403686523" Y="-27.68235546875" />
                  <Point X="1.082496948242" Y="-27.697619140625" />
                  <Point X="1.066776977539" Y="-27.706837890625" />
                  <Point X="1.039682250977" Y="-27.7264453125" />
                  <Point X="1.029596557617" Y="-27.734271484375" />
                  <Point X="0.863874633789" Y="-27.872064453125" />
                  <Point X="0.852652709961" Y="-27.88308984375" />
                  <Point X="0.832181945801" Y="-27.90683984375" />
                  <Point X="0.822933349609" Y="-27.919564453125" />
                  <Point X="0.806041137695" Y="-27.947390625" />
                  <Point X="0.799018859863" Y="-27.96146875" />
                  <Point X="0.787394165039" Y="-27.99058984375" />
                  <Point X="0.782792175293" Y="-28.0056328125" />
                  <Point X="0.778528076172" Y="-28.02525" />
                  <Point X="0.728977905273" Y="-28.25321875" />
                  <Point X="0.727585021973" Y="-28.2612890625" />
                  <Point X="0.72472467041" Y="-28.289677734375" />
                  <Point X="0.72474230957" Y="-28.323171875" />
                  <Point X="0.725555175781" Y="-28.335521484375" />
                  <Point X="0.833091064453" Y="-29.15233984375" />
                  <Point X="0.686807067871" Y="-28.606400390625" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605957031" Y="-28.480123046875" />
                  <Point X="0.642145874023" Y="-28.45358203125" />
                  <Point X="0.626785705566" Y="-28.4238125" />
                  <Point X="0.620403869629" Y="-28.413203125" />
                  <Point X="0.607430114746" Y="-28.39451171875" />
                  <Point X="0.456676239014" Y="-28.177302734375" />
                  <Point X="0.446669006348" Y="-28.16516796875" />
                  <Point X="0.424787414551" Y="-28.14271484375" />
                  <Point X="0.412913024902" Y="-28.132396484375" />
                  <Point X="0.386657623291" Y="-28.11315234375" />
                  <Point X="0.373238311768" Y="-28.10493359375" />
                  <Point X="0.345235076904" Y="-28.090826171875" />
                  <Point X="0.330651000977" Y="-28.0849375" />
                  <Point X="0.310574707031" Y="-28.07870703125" />
                  <Point X="0.077292037964" Y="-28.0063046875" />
                  <Point X="0.063376476288" Y="-28.003109375" />
                  <Point X="0.035218677521" Y="-27.99883984375" />
                  <Point X="0.020976739883" Y="-27.997765625" />
                  <Point X="-0.008664761543" Y="-27.997765625" />
                  <Point X="-0.022906848907" Y="-27.99883984375" />
                  <Point X="-0.051064350128" Y="-28.003109375" />
                  <Point X="-0.064979469299" Y="-28.0063046875" />
                  <Point X="-0.085055892944" Y="-28.01253515625" />
                  <Point X="-0.3183387146" Y="-28.0849375" />
                  <Point X="-0.332927093506" Y="-28.090828125" />
                  <Point X="-0.360930786133" Y="-28.1049375" />
                  <Point X="-0.374345916748" Y="-28.113154296875" />
                  <Point X="-0.400601348877" Y="-28.1323984375" />
                  <Point X="-0.412474700928" Y="-28.142716796875" />
                  <Point X="-0.434358978271" Y="-28.165171875" />
                  <Point X="-0.444369903564" Y="-28.177310546875" />
                  <Point X="-0.457343658447" Y="-28.19600390625" />
                  <Point X="-0.60809765625" Y="-28.4132109375" />
                  <Point X="-0.612471618652" Y="-28.4201328125" />
                  <Point X="-0.625977416992" Y="-28.445265625" />
                  <Point X="-0.638778198242" Y="-28.476216796875" />
                  <Point X="-0.642752929688" Y="-28.487935546875" />
                  <Point X="-0.985425170898" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.13616839432" Y="-20.3365192584" />
                  <Point X="0.352956189899" Y="-20.309168885546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.347195431627" Y="-20.284719052162" />
                  <Point X="-0.803820720703" Y="-20.268773345698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.543611509143" Y="-20.445805392202" />
                  <Point X="0.327721675452" Y="-20.403345583592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.321484221069" Y="-20.380674814127" />
                  <Point X="-1.171282086951" Y="-20.350999218738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.833608667779" Y="-20.55099022284" />
                  <Point X="0.302487161005" Y="-20.497522281638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.295773010512" Y="-20.476630576093" />
                  <Point X="-1.472783904929" Y="-20.43552844996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059248420758" Y="-20.653927643351" />
                  <Point X="0.277252646558" Y="-20.591698979685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.270061799955" Y="-20.572586338058" />
                  <Point X="-1.466963660216" Y="-20.530789604092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.272938819144" Y="-20.756447783204" />
                  <Point X="0.252018132111" Y="-20.685875677731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.244350589398" Y="-20.668542100024" />
                  <Point X="-1.46541336592" Y="-20.62590164827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.446503579269" Y="-20.857566704892" />
                  <Point X="0.220539961655" Y="-20.779834342505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.215121349079" Y="-20.764620714296" />
                  <Point X="-1.491447466805" Y="-20.720050424143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.620068339393" Y="-20.958685626581" />
                  <Point X="0.142853540354" Y="-20.872179379602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.143577076069" Y="-20.862177002071" />
                  <Point X="-1.529204101459" Y="-20.813789840115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.146067237107" Y="-20.792248504748" />
                  <Point X="-2.292310079186" Y="-20.787141592169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.762420373427" Y="-21.058714575856" />
                  <Point X="-1.628316679211" Y="-20.905386659342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.045718028847" Y="-20.890810683026" />
                  <Point X="-2.453289038456" Y="-20.876577989748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.785170061855" Y="-21.15456691919" />
                  <Point X="-2.614267911685" Y="-20.966014390331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.731372955172" Y="-21.247746189537" />
                  <Point X="-2.741829963904" Y="-21.056617732018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.67757584849" Y="-21.340925459884" />
                  <Point X="-2.860414206071" Y="-21.147534585741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623778741808" Y="-21.43410473023" />
                  <Point X="-2.978998448237" Y="-21.238451439463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.569981635126" Y="-21.527284000577" />
                  <Point X="-3.041345594108" Y="-21.331332135862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.516184528444" Y="-21.620463270924" />
                  <Point X="-2.98533463272" Y="-21.428345988442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.462387421762" Y="-21.71364254127" />
                  <Point X="-2.929323671332" Y="-21.525359841022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408590315079" Y="-21.806821811617" />
                  <Point X="-2.873312709944" Y="-21.622373693602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.354793208397" Y="-21.900001081963" />
                  <Point X="-2.817301748556" Y="-21.719387546182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.300996101715" Y="-21.99318035231" />
                  <Point X="-2.76855483628" Y="-21.816147732578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.247198995033" Y="-22.086359622657" />
                  <Point X="-2.752632269913" Y="-21.911761667556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956270884335" Y="-22.241099634856" />
                  <Point X="3.905290134872" Y="-22.239319347856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.193401888351" Y="-22.179538893003" />
                  <Point X="-2.751005132488" Y="-22.006876395155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.711900274908" Y="-21.973321197381" />
                  <Point X="-3.732267780734" Y="-21.972609948405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.026431618337" Y="-22.338607608384" />
                  <Point X="3.750035564784" Y="-22.328955645509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.139604781668" Y="-22.27271816335" />
                  <Point X="-2.783892603934" Y="-22.100785846054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.536655837633" Y="-22.074498774688" />
                  <Point X="-3.80426596142" Y="-22.065153623242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.088041687656" Y="-22.435816986121" />
                  <Point X="3.594780994696" Y="-22.418591943163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085807468877" Y="-22.365897426499" />
                  <Point X="-2.864965779913" Y="-22.193012615072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361411400357" Y="-22.175676351995" />
                  <Point X="-3.876264142107" Y="-22.157697298079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.116875502685" Y="-22.531881791838" />
                  <Point X="3.439526424609" Y="-22.508228240817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.041555039747" Y="-22.45941000433" />
                  <Point X="-3.948262322793" Y="-22.250240972915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.998385828485" Y="-22.622801947947" />
                  <Point X="3.284271854521" Y="-22.597864538471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017151342583" Y="-22.553615715155" />
                  <Point X="-4.015863362835" Y="-22.342938199287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879896154286" Y="-22.713722104055" />
                  <Point X="3.129017284433" Y="-22.687500836125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015605451527" Y="-22.648619638158" />
                  <Point X="-4.070239413487" Y="-22.436097252465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.761406480087" Y="-22.804642260164" />
                  <Point X="2.973762714345" Y="-22.777137133778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032685443152" Y="-22.744273991317" />
                  <Point X="-4.124615464138" Y="-22.529256305642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.642916805887" Y="-22.895562416272" />
                  <Point X="2.818508144257" Y="-22.866773431432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085415873406" Y="-22.841173285226" />
                  <Point X="-4.17899151479" Y="-22.62241535882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.524427131688" Y="-22.986482572381" />
                  <Point X="2.663253574169" Y="-22.956409729086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.177739083546" Y="-22.939455189474" />
                  <Point X="-4.213880262048" Y="-22.716254923628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.405937457489" Y="-23.077402728489" />
                  <Point X="-4.084092221233" Y="-22.815845128592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.287447783289" Y="-23.168322884598" />
                  <Point X="-3.954304180418" Y="-22.915435333557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.169295773299" Y="-23.2592548322" />
                  <Point X="-3.824516139602" Y="-23.015025538521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.089521544227" Y="-23.351526961444" />
                  <Point X="-3.694728098787" Y="-23.114615743486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034904614974" Y="-23.444677602955" />
                  <Point X="-3.564940057971" Y="-23.21420594845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007644530458" Y="-23.538783566536" />
                  <Point X="-3.476097837691" Y="-23.312366293854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002860448776" Y="-23.633674409431" />
                  <Point X="-3.431810505167" Y="-23.408970748293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.021609728471" Y="-23.729387055414" />
                  <Point X="-3.423111931983" Y="-23.50433241587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.063116543658" Y="-23.825894412047" />
                  <Point X="-3.450946821948" Y="-23.598418306802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.726703262284" Y="-23.979046047086" />
                  <Point X="4.166815154627" Y="-23.959494323538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.133511759674" Y="-23.923410573868" />
                  <Point X="-3.502810701466" Y="-23.691665086929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.75004349198" Y="-24.074919012576" />
                  <Point X="3.596149678341" Y="-24.034624152692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.3031656333" Y="-24.02439292439" />
                  <Point X="-3.661386459324" Y="-23.78118540615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.956742825262" Y="-23.770871334578" />
                  <Point X="-4.651161196339" Y="-23.74662171071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765881071216" Y="-24.170529979738" />
                  <Point X="-4.6766785613" Y="-23.840788531399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.780762067919" Y="-24.266107542302" />
                  <Point X="-4.702195926261" Y="-23.934955352088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.535588489275" Y="-24.352603798985" />
                  <Point X="-4.727713340244" Y="-24.029122171064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.221733429774" Y="-24.436701645507" />
                  <Point X="-4.743340827418" Y="-24.123634353895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.907878370272" Y="-24.520799492029" />
                  <Point X="-4.757334790065" Y="-24.21820358066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.628252054644" Y="-24.606092632625" />
                  <Point X="-4.771328752712" Y="-24.312772807424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491716210901" Y="-24.696382602607" />
                  <Point X="-4.785322715358" Y="-24.407342034189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.415707349914" Y="-24.788786221401" />
                  <Point X="-4.381108991823" Y="-24.516515395162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.372301905831" Y="-24.882328376602" />
                  <Point X="-3.973184236238" Y="-24.62581834823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353675302582" Y="-24.976735827992" />
                  <Point X="-3.565259480653" Y="-24.735121301298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351822750602" Y="-25.07172904216" />
                  <Point X="-3.371186273564" Y="-24.836956393736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.36929685527" Y="-25.16739715805" />
                  <Point X="-3.312277405342" Y="-24.934071443453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.409075060572" Y="-25.263844150296" />
                  <Point X="-3.295974006492" Y="-25.029698677394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.486715587111" Y="-25.361613323935" />
                  <Point X="-3.310956426866" Y="-25.124233386454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.637877427316" Y="-25.461949918421" />
                  <Point X="-3.363823335094" Y="-25.217445140047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.013764400637" Y="-25.57013408748" />
                  <Point X="-3.495888631416" Y="-25.307891224984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.421691030517" Y="-25.679437106" />
                  <Point X="-3.806574679403" Y="-25.392099735827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783313352027" Y="-25.787123142441" />
                  <Point X="-4.120430687849" Y="-25.476197549211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.769057106087" Y="-25.881683210071" />
                  <Point X="-4.434286696295" Y="-25.560295362594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.751328726438" Y="-25.97612202812" />
                  <Point X="3.653498460636" Y="-25.937784950467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.296348032176" Y="-25.925312982681" />
                  <Point X="-4.748142704741" Y="-25.644393175978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729808182862" Y="-26.070428420887" />
                  <Point X="4.636203952089" Y="-26.067159689121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.061017465748" Y="-26.012152964924" />
                  <Point X="-4.774456275998" Y="-25.73853219253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980179526784" Y="-26.1043879486" />
                  <Point X="-4.760792608498" Y="-25.834067245022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.908096142026" Y="-26.196928648045" />
                  <Point X="-4.747128940998" Y="-25.929602297513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876640187304" Y="-26.29088808861" />
                  <Point X="-4.72767792257" Y="-26.025339448753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866972305005" Y="-26.385608385429" />
                  <Point X="-3.024478182551" Y="-26.179874400981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.676572804886" Y="-26.157102754987" />
                  <Point X="-4.703178615983" Y="-26.121252890099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859112495133" Y="-26.480391821528" />
                  <Point X="-2.960094854349" Y="-26.277180623052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.247244055295" Y="-26.232232382505" />
                  <Point X="-4.678679309396" Y="-26.217166331446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876123593245" Y="-26.576043768873" />
                  <Point X="-2.949140759662" Y="-26.372621055176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.931030525728" Y="-26.673019067914" />
                  <Point X="-2.971116565335" Y="-26.46691154984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.993547289347" Y="-26.770260108114" />
                  <Point X="-3.033270683565" Y="-26.559798986913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.095269250508" Y="-26.86887022398" />
                  <Point X="-3.148407441803" Y="-26.650836229427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.225057846199" Y="-26.968460448321" />
                  <Point X="-3.266897020166" Y="-26.741756388882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.354846441889" Y="-27.068050672663" />
                  <Point X="2.642933860528" Y="-27.043190137511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.440440306902" Y="-27.036118906801" />
                  <Point X="-3.385386600649" Y="-26.832676548263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48463503758" Y="-27.167640897004" />
                  <Point X="2.942016523069" Y="-27.148692240937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.260399557175" Y="-27.124889651989" />
                  <Point X="-3.503876181131" Y="-26.923596707645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.61442363327" Y="-27.267231121345" />
                  <Point X="3.117261218763" Y="-27.249869827268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.141084895134" Y="-27.215780998887" />
                  <Point X="-3.622365761614" Y="-27.014516867026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.74421222896" Y="-27.366821345686" />
                  <Point X="3.292506061623" Y="-27.351047418738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.087428092605" Y="-27.308965168763" />
                  <Point X="-3.740855342096" Y="-27.105437026407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.874000824651" Y="-27.466411570027" />
                  <Point X="3.467750904482" Y="-27.452225010209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038302760243" Y="-27.402307581064" />
                  <Point X="-3.859344922579" Y="-27.196357185788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.003789420341" Y="-27.566001794368" />
                  <Point X="3.642995747341" Y="-27.553402601679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.003590431594" Y="-27.496153306545" />
                  <Point X="-3.977834503061" Y="-27.287277345169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.066609925394" Y="-27.663253441453" />
                  <Point X="3.818240590201" Y="-27.65458019315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.004441534384" Y="-27.591240934417" />
                  <Point X="-2.376537279648" Y="-27.438253783104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.765889715143" Y="-27.424657296453" />
                  <Point X="-4.096324083544" Y="-27.378197504551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.004174603173" Y="-27.756131058666" />
                  <Point X="3.99348543306" Y="-27.75575778462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021467039363" Y="-27.686893384861" />
                  <Point X="1.5330630694" Y="-27.669837942407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.258988049085" Y="-27.660267031799" />
                  <Point X="-2.321626630692" Y="-27.535229211928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921144245816" Y="-27.514293595484" />
                  <Point X="-4.164873339962" Y="-27.470861618477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038743198515" Y="-27.782554588341" />
                  <Point X="1.695399649115" Y="-27.770564767395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.014542251186" Y="-27.746788703146" />
                  <Point X="-2.312956343043" Y="-27.630589891752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.076398776489" Y="-27.603929894514" />
                  <Point X="-4.106979379274" Y="-27.567941226841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072556478349" Y="-27.8787932808" />
                  <Point X="1.82876486492" Y="-27.870279890063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.904825246303" Y="-27.838015207617" />
                  <Point X="-2.341789766647" Y="-27.724640913121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.231653307162" Y="-27.693566193544" />
                  <Point X="-4.049085418586" Y="-27.665020835206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.1285675194" Y="-27.975807136162" />
                  <Point X="1.904160583916" Y="-27.967970673295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.816602984679" Y="-27.929992325063" />
                  <Point X="-2.395586102652" Y="-27.817820210381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.386907837835" Y="-27.783202492574" />
                  <Point X="-3.987548505478" Y="-27.762227658272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.184578560452" Y="-28.072820991524" />
                  <Point X="1.97910932561" Y="-28.065645847736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.778858004774" Y="-28.023732148029" />
                  <Point X="-2.449383167487" Y="-27.910999482189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.542162368508" Y="-27.872838791604" />
                  <Point X="-3.913219805101" Y="-27.859881180393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.240589601503" Y="-28.169834846886" />
                  <Point X="2.054058119759" Y="-28.163321024009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.75835231051" Y="-28.118073980115" />
                  <Point X="0.372581202097" Y="-28.104602556161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.305414852893" Y="-28.080926412209" />
                  <Point X="-2.503180305223" Y="-28.004178751451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.697416899181" Y="-27.962475090635" />
                  <Point X="-3.838891104725" Y="-27.957534702514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.296600642555" Y="-28.266848702247" />
                  <Point X="2.129006913907" Y="-28.260996200281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.73784663186" Y="-28.212415812746" />
                  <Point X="0.474667868134" Y="-28.203225407802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.439416935686" Y="-28.171304863072" />
                  <Point X="-2.556977442959" Y="-28.097358020713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.352611683606" Y="-28.363862557609" />
                  <Point X="2.203955708055" Y="-28.358671376554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724733801233" Y="-28.307015809318" />
                  <Point X="0.542281535608" Y="-28.300644435807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.504596932663" Y="-28.264086634131" />
                  <Point X="-2.610774580695" Y="-28.190537289975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408622724657" Y="-28.460876412971" />
                  <Point X="2.278904502203" Y="-28.456346552826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.734361180657" Y="-28.402409911524" />
                  <Point X="0.609895395112" Y="-28.398063470518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.569011300121" Y="-28.356895141561" />
                  <Point X="-2.664571718431" Y="-28.283716559237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464633765709" Y="-28.557890268333" />
                  <Point X="2.353853296351" Y="-28.554021729099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746933558101" Y="-28.497906855327" />
                  <Point X="0.656894223179" Y="-28.494762612467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.627892802869" Y="-28.449896860885" />
                  <Point X="-2.718368856167" Y="-28.3768958285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.52064480676" Y="-28.654904123695" />
                  <Point X="2.428802090499" Y="-28.651696905371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.759505935545" Y="-28.593403799131" />
                  <Point X="0.682605138823" Y="-28.590718364135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.65775178328" Y="-28.543912069021" />
                  <Point X="-2.772165993903" Y="-28.470075097762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.576655847812" Y="-28.751917979057" />
                  <Point X="2.503750884647" Y="-28.749372081644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.772078312989" Y="-28.688900742934" />
                  <Point X="0.708316344687" Y="-28.686674125936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.682986335763" Y="-28.638088765739" />
                  <Point X="-2.825963131639" Y="-28.563254367024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632666888863" Y="-28.848931834419" />
                  <Point X="2.578699678796" Y="-28.847047257916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784650690433" Y="-28.784397686737" />
                  <Point X="0.734027607247" Y="-28.782629889718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.708220888246" Y="-28.732265462457" />
                  <Point X="-2.879760269375" Y="-28.656433636286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.688677929914" Y="-28.945945689781" />
                  <Point X="2.653648472944" Y="-28.944722434189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.797223067878" Y="-28.87989463054" />
                  <Point X="0.759738869806" Y="-28.878585653499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.733455440729" Y="-28.826442159175" />
                  <Point X="-1.678431937317" Y="-28.793442852762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.007947915666" Y="-28.781935901239" />
                  <Point X="-2.933557407111" Y="-28.749612905548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809795445322" Y="-28.975391574343" />
                  <Point X="0.785450132366" Y="-28.974541417281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.758689993212" Y="-28.920618855893" />
                  <Point X="-1.447102248048" Y="-28.896578970226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.178110489908" Y="-28.871051599916" />
                  <Point X="-2.965254914908" Y="-28.843563910894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.822367822766" Y="-29.070888518146" />
                  <Point X="0.811161394926" Y="-29.070497181063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.783924545695" Y="-29.014795552611" />
                  <Point X="-1.334214900915" Y="-28.995578989962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313309208202" Y="-28.961388263347" />
                  <Point X="-2.83593311377" Y="-28.94313783441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.809159098178" Y="-29.108972249328" />
                  <Point X="-1.231085325147" Y="-29.094238260814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.419007356605" Y="-29.052755109379" />
                  <Point X="-2.696034922562" Y="-29.043081093606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.834393650661" Y="-29.203148946046" />
                  <Point X="-1.184569349217" Y="-29.190920541195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.490044239725" Y="-29.145332353467" />
                  <Point X="-2.533334543323" Y="-29.143820622754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.859628203144" Y="-29.297325642764" />
                  <Point X="-1.165312607362" Y="-29.286650908147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.884862755627" Y="-29.391502339482" />
                  <Point X="-1.146272241119" Y="-29.382373719096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.91009730811" Y="-29.4856790362" />
                  <Point X="-1.127231878989" Y="-29.478096529902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.935331860593" Y="-29.579855732918" />
                  <Point X="-1.12055432838" Y="-29.573387621816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.960566413076" Y="-29.674032429636" />
                  <Point X="-1.132910817329" Y="-29.668014030422" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998627929688" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.977974609375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.50328112793" Y="-28.655576171875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.451344970703" Y="-28.5028515625" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.274335754395" Y="-28.2663984375" />
                  <Point X="0.254259597778" Y="-28.26016796875" />
                  <Point X="0.02097677803" Y="-28.187765625" />
                  <Point X="-0.008664756775" Y="-28.187765625" />
                  <Point X="-0.028741065979" Y="-28.19399609375" />
                  <Point X="-0.262023895264" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.301252838135" Y="-28.3043359375" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.82806628418" Y="-29.913638671875" />
                  <Point X="-0.84774420166" Y="-29.987078125" />
                  <Point X="-1.100230957031" Y="-29.938068359375" />
                  <Point X="-1.122218017578" Y="-29.932412109375" />
                  <Point X="-1.351589477539" Y="-29.873396484375" />
                  <Point X="-1.312971313477" Y="-29.5800625" />
                  <Point X="-1.309150390625" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.314479492188" Y="-29.510646484375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.404766235352" Y="-29.186419921875" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240722656" Y="-28.985763671875" />
                  <Point X="-1.673772460938" Y="-28.98415625" />
                  <Point X="-1.958829833984" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.010319580078" Y="-28.98744921875" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734375" Y="-29.157294921875" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.855837646484" Y="-29.167609375" />
                  <Point X="-2.886260498047" Y="-29.14418359375" />
                  <Point X="-3.228580810547" Y="-28.880609375" />
                  <Point X="-2.565022705078" Y="-27.73129296875" />
                  <Point X="-2.506033447266" Y="-27.629119140625" />
                  <Point X="-2.499764648438" Y="-27.597578125" />
                  <Point X="-2.515353515625" Y="-27.567390625" />
                  <Point X="-2.531327392578" Y="-27.551416015625" />
                  <Point X="-2.560156982422" Y="-27.53719921875" />
                  <Point X="-2.591683837891" Y="-27.543470703125" />
                  <Point X="-3.778910888672" Y="-28.228916015625" />
                  <Point X="-3.842959228516" Y="-28.26589453125" />
                  <Point X="-3.843802490234" Y="-28.264787109375" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.183516113281" Y="-27.8105546875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.266998779297" Y="-26.50234375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145211181641" Y="-26.39365625" />
                  <Point X="-3.1381171875" Y="-26.366265625" />
                  <Point X="-3.140329101562" Y="-26.33458984375" />
                  <Point X="-3.163269775391" Y="-26.3093984375" />
                  <Point X="-3.187641357422" Y="-26.2950546875" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.718291015625" Y="-26.48588671875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.9331640625" Y="-25.97084375" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.675037841797" Y="-25.160150390625" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.539685058594" Y="-25.119888671875" />
                  <Point X="-3.514144287109" Y="-25.1021640625" />
                  <Point X="-3.494166259766" Y="-25.073546875" />
                  <Point X="-3.485648193359" Y="-25.046103515625" />
                  <Point X="-3.486380859375" Y="-25.01409765625" />
                  <Point X="-3.494898925781" Y="-24.98665234375" />
                  <Point X="-3.516341308594" Y="-24.95887109375" />
                  <Point X="-3.541895019531" Y="-24.941134765625" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.92365625" Y="-24.56784375" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.997776367188" Y="-24.5451015625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.906026855469" Y="-23.960708984375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.837546386719" Y="-23.594923828125" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731717285156" Y="-23.60413671875" />
                  <Point X="-3.726836669922" Y="-23.602599609375" />
                  <Point X="-3.670277832031" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.573943359375" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.637166748047" Y="-23.5515" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.618672607422" Y="-23.4499609375" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968505859" Y="-23.380779296875" />
                  <Point X="-4.443619628906" Y="-22.7794609375" />
                  <Point X="-4.47610546875" Y="-22.754533203125" />
                  <Point X="-4.471375976562" Y="-22.746431640625" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.129240234375" Y="-22.173439453125" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.210919189453" Y="-22.043171875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.13851953125" Y="-22.079564453125" />
                  <Point X="-3.131735107422" Y="-22.08016015625" />
                  <Point X="-3.05296484375" Y="-22.08705078125" />
                  <Point X="-3.031509277344" Y="-22.0842265625" />
                  <Point X="-3.013256835938" Y="-22.072599609375" />
                  <Point X="-3.008440429688" Y="-22.06778515625" />
                  <Point X="-2.952528564453" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.938667236328" Y="-21.965380859375" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.299327148438" Y="-21.26449609375" />
                  <Point X="-3.295169921875" Y="-21.241439453125" />
                  <Point X="-2.752873046875" Y="-20.825666015625" />
                  <Point X="-2.704408447266" Y="-20.798740234375" />
                  <Point X="-2.141549316406" Y="-20.486029296875" />
                  <Point X="-1.983672485352" Y="-20.69177734375" />
                  <Point X="-1.967826416016" Y="-20.712427734375" />
                  <Point X="-1.951247192383" Y="-20.726337890625" />
                  <Point X="-1.943702392578" Y="-20.730265625" />
                  <Point X="-1.85603125" Y="-20.77590625" />
                  <Point X="-1.835124389648" Y="-20.781509765625" />
                  <Point X="-1.813808837891" Y="-20.77775" />
                  <Point X="-1.805950195312" Y="-20.774494140625" />
                  <Point X="-1.714634887695" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.683525634766" Y="-20.6973984375" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.581572265625" />
                  <Point X="-1.689137329102" Y="-20.298861328125" />
                  <Point X="-1.670126953125" Y="-20.29353125" />
                  <Point X="-0.968082641602" Y="-20.096703125" />
                  <Point X="-0.909339294434" Y="-20.089826171875" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.057453018188" Y="-20.63194921875" />
                  <Point X="-0.03193029213" Y="-20.625109375" />
                  <Point X="-0.031930294037" Y="-20.625109375" />
                  <Point X="-0.057453018188" Y="-20.63194921875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282159805" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594078064" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.232384048462" Y="-20.025046875" />
                  <Point X="0.247229232788" Y="-20.010240234375" />
                  <Point X="0.860209960938" Y="-20.074435546875" />
                  <Point X="0.908817565918" Y="-20.086171875" />
                  <Point X="1.508456420898" Y="-20.230943359375" />
                  <Point X="1.539703613281" Y="-20.242275390625" />
                  <Point X="1.931044921875" Y="-20.38421875" />
                  <Point X="1.961631469727" Y="-20.3985234375" />
                  <Point X="2.338685058594" Y="-20.574859375" />
                  <Point X="2.368265380859" Y="-20.592091796875" />
                  <Point X="2.732533935547" Y="-20.804314453125" />
                  <Point X="2.760401855469" Y="-20.8241328125" />
                  <Point X="3.068740722656" Y="-21.043408203125" />
                  <Point X="2.29812890625" Y="-22.378146484375" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.223150634766" Y="-22.51484765625" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202708007812" Y="-22.61317578125" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.222086425781" Y="-22.704205078125" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.279955810547" Y="-22.779201171875" />
                  <Point X="2.338248046875" Y="-22.818755859375" />
                  <Point X="2.360348144531" Y="-22.8270234375" />
                  <Point X="2.365849365234" Y="-22.827685546875" />
                  <Point X="2.429760986328" Y="-22.835392578125" />
                  <Point X="2.455027099609" Y="-22.832353515625" />
                  <Point X="2.528951416016" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.915160400391" Y="-22.01423046875" />
                  <Point X="3.994248046875" Y="-21.9685703125" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.218133789062" Y="-22.2838046875" />
                  <Point X="4.387512207031" Y="-22.563705078125" />
                  <Point X="3.378507080078" Y="-23.33794140625" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.274792480469" Y="-23.422140625" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.2114140625" Y="-23.514599609375" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.1921796875" Y="-23.6158203125" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.219454589844" Y="-23.71783203125" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.28094140625" Y="-23.801177734375" />
                  <Point X="3.286466308594" Y="-23.804287109375" />
                  <Point X="3.350590087891" Y="-23.8403828125" />
                  <Point X="3.376027587891" Y="-23.8473671875" />
                  <Point X="3.462727050781" Y="-23.85882421875" />
                  <Point X="3.475803466797" Y="-23.858828125" />
                  <Point X="4.781131835938" Y="-23.686978515625" />
                  <Point X="4.848975097656" Y="-23.678046875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.9440859375" Y="-24.080072265625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="3.844184814453" Y="-24.7345703125" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.721756835938" Y="-24.77141796875" />
                  <Point X="3.636577392578" Y="-24.82065234375" />
                  <Point X="3.617866699219" Y="-24.838677734375" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985351562" Y="-24.925265625" />
                  <Point X="3.555519287109" Y="-24.932921875" />
                  <Point X="3.538483154297" Y="-25.021876953125" />
                  <Point X="3.53994921875" Y="-25.04833984375" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.571157226562" Y="-25.164365234375" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.643907226562" Y="-25.24614453125" />
                  <Point X="3.729086669922" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.938212890625" Y="-25.621134765625" />
                  <Point X="4.998068359375" Y="-25.637173828125" />
                  <Point X="4.948431640625" Y="-25.966400390625" />
                  <Point X="4.942156738281" Y="-25.9938984375" />
                  <Point X="4.874545898438" Y="-26.290177734375" />
                  <Point X="3.531453125" Y="-26.113357421875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.380448974609" Y="-26.10146875" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.1979453125" Y="-26.143921875" />
                  <Point X="3.176749023438" Y="-26.16515625" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.067953613281" Y="-26.299517578125" />
                  <Point X="3.063111572266" Y="-26.327615234375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.049849121094" Y="-26.501458984375" />
                  <Point X="3.064322509766" Y="-26.529005859375" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460449219" Y="-26.685541015625" />
                  <Point X="4.279326171875" Y="-27.5379375" />
                  <Point X="4.339073730469" Y="-27.583783203125" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.191153808594" Y="-27.82058203125" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="2.859859863281" Y="-27.320650390625" />
                  <Point X="2.753454833984" Y="-27.259216796875" />
                  <Point X="2.720217773438" Y="-27.25022265625" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.504749267578" Y="-27.214076171875" />
                  <Point X="2.474852783203" Y="-27.226732421875" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.297490966797" Y="-27.322615234375" />
                  <Point X="2.28111328125" Y="-27.348908203125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.188950439453" Y="-27.529873046875" />
                  <Point X="2.192255371094" Y="-27.563498046875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.947934326172" Y="-29.014990234375" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.835293945312" Y="-29.19021484375" />
                  <Point X="2.820790527344" Y="-29.199603515625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.764374389648" Y="-28.09790625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.653661132812" Y="-27.969609375" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.442262207031" Y="-27.8369375" />
                  <Point X="1.407335205078" Y="-27.83741796875" />
                  <Point X="1.192718261719" Y="-27.857166015625" />
                  <Point X="1.178165649414" Y="-27.86076171875" />
                  <Point X="1.151070800781" Y="-27.880369140625" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.964192626953" Y="-28.06560546875" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.310720703125" />
                  <Point X="1.116227050781" Y="-29.84732421875" />
                  <Point X="1.127642211914" Y="-29.934029296875" />
                  <Point X="0.994369140625" Y="-29.9632421875" />
                  <Point X="0.980938781738" Y="-29.965681640625" />
                  <Point X="0.860200439453" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#124" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.015312729718" Y="4.412272361799" Z="0.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.2" />
                  <Point X="-0.921307345037" Y="4.991113917252" Z="0.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.2" />
                  <Point X="-1.689780723436" Y="4.785894627758" Z="0.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.2" />
                  <Point X="-1.747125384685" Y="4.743057374226" Z="0.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.2" />
                  <Point X="-1.737367332973" Y="4.348916427052" Z="0.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.2" />
                  <Point X="-1.831240493722" Y="4.302979726633" Z="0.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.2" />
                  <Point X="-1.926770261387" Y="4.345363647817" Z="0.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.2" />
                  <Point X="-1.950161218653" Y="4.369942261713" Z="0.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.2" />
                  <Point X="-2.734847272975" Y="4.276246700695" Z="0.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.2" />
                  <Point X="-3.33174812985" Y="3.829520355301" Z="0.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.2" />
                  <Point X="-3.348784265961" Y="3.741784109804" Z="0.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.2" />
                  <Point X="-2.994633099151" Y="3.061542499955" Z="0.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.2" />
                  <Point X="-3.049951812332" Y="2.998851755555" Z="0.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.2" />
                  <Point X="-3.133533965652" Y="3.000931600022" Z="0.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.2" />
                  <Point X="-3.192075238493" Y="3.031409682785" Z="0.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.2" />
                  <Point X="-4.174859807845" Y="2.888544631605" Z="0.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.2" />
                  <Point X="-4.520714186189" Y="2.310054416899" Z="0.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.2" />
                  <Point X="-4.480213497757" Y="2.212150774946" Z="0.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.2" />
                  <Point X="-3.669179024437" Y="1.558231350708" Z="0.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.2" />
                  <Point X="-3.689516837987" Y="1.498915238275" Z="0.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.2" />
                  <Point X="-3.7480286516" Y="1.476367224557" Z="0.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.2" />
                  <Point X="-3.83717588936" Y="1.485928186794" Z="0.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.2" />
                  <Point X="-4.960442771941" Y="1.083650059998" Z="0.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.2" />
                  <Point X="-5.053393833907" Y="0.493497058308" Z="0.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.2" />
                  <Point X="-4.942753174663" Y="0.415139198428" Z="0.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.2" />
                  <Point X="-3.551007001741" Y="0.031333281416" Z="0.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.2" />
                  <Point X="-3.540289801749" Y="0.002361907531" Z="0.2" />
                  <Point X="-3.539556741714" Y="0" Z="0.2" />
                  <Point X="-3.548074799386" Y="-0.02744504353" Z="0.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.2" />
                  <Point X="-3.574361593383" Y="-0.047542701717" Z="0.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.2" />
                  <Point X="-3.694134614109" Y="-0.080572858328" Z="0.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.2" />
                  <Point X="-4.988815937082" Y="-0.946641204972" Z="0.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.2" />
                  <Point X="-4.85766084128" Y="-1.479044608744" Z="0.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.2" />
                  <Point X="-4.717920596735" Y="-1.504178992364" Z="0.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.2" />
                  <Point X="-3.194774789778" Y="-1.321214726984" Z="0.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.2" />
                  <Point X="-3.199770296639" Y="-1.349839984184" Z="0.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.2" />
                  <Point X="-3.303592677632" Y="-1.431394424448" Z="0.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.2" />
                  <Point X="-4.232615487134" Y="-2.804882598903" Z="0.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.2" />
                  <Point X="-3.889744410372" Y="-3.263789443654" Z="0.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.2" />
                  <Point X="-3.760066672089" Y="-3.240936889342" Z="0.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.2" />
                  <Point X="-2.556865670716" Y="-2.571464798416" Z="0.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.2" />
                  <Point X="-2.614480123827" Y="-2.675011710403" Z="0.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.2" />
                  <Point X="-2.922920436592" Y="-4.152520643663" Z="0.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.2" />
                  <Point X="-2.485932532402" Y="-4.427985122472" Z="0.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.2" />
                  <Point X="-2.433297000679" Y="-4.426317119006" Z="0.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.2" />
                  <Point X="-1.98869728566" Y="-3.99774282046" Z="0.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.2" />
                  <Point X="-1.68319874231" Y="-4.002768027624" Z="0.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.2" />
                  <Point X="-1.443889607504" Y="-4.192730776888" Z="0.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.2" />
                  <Point X="-1.369674739528" Y="-4.489120106559" Z="0.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.2" />
                  <Point X="-1.36869953655" Y="-4.542255604798" Z="0.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.2" />
                  <Point X="-1.140832932315" Y="-4.94955495558" Z="0.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.2" />
                  <Point X="-0.841419030868" Y="-5.009152807713" Z="0.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.2" />
                  <Point X="-0.78592595454" Y="-4.895299716242" Z="0.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.2" />
                  <Point X="-0.266332888343" Y="-3.301565462138" Z="0.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.2" />
                  <Point X="-0.020076213648" Y="-3.210470284273" Z="0.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.2" />
                  <Point X="0.233282865714" Y="-3.276641761015" Z="0.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.2" />
                  <Point X="0.404112971089" Y="-3.50008029363" Z="0.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.2" />
                  <Point X="0.448828905861" Y="-3.637236313573" Z="0.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.2" />
                  <Point X="0.983719823648" Y="-4.983597409411" Z="0.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.2" />
                  <Point X="1.162865593541" Y="-4.944865160589" Z="0.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.2" />
                  <Point X="1.159643342027" Y="-4.809516021599" Z="0.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.2" />
                  <Point X="1.00689576775" Y="-3.044944900676" Z="0.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.2" />
                  <Point X="1.17688181142" Y="-2.887533775041" Z="0.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.2" />
                  <Point X="1.405760628849" Y="-2.85592619953" Z="0.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.2" />
                  <Point X="1.620465961836" Y="-2.980387887787" Z="0.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.2" />
                  <Point X="1.718550715073" Y="-3.097063001567" Z="0.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.2" />
                  <Point X="2.8418037675" Y="-4.210297314857" Z="0.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.2" />
                  <Point X="3.031517273384" Y="-4.075825790299" Z="0.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.2" />
                  <Point X="2.98507963371" Y="-3.958709994121" Z="0.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.2" />
                  <Point X="2.235303522256" Y="-2.523331222192" Z="0.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.2" />
                  <Point X="2.319203659383" Y="-2.340915230557" Z="0.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.2" />
                  <Point X="2.491983147815" Y="-2.239697650584" Z="0.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.2" />
                  <Point X="2.705175581321" Y="-2.268144487235" Z="0.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.2" />
                  <Point X="2.828703602986" Y="-2.332669866564" Z="0.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.2" />
                  <Point X="4.225886343566" Y="-2.818078633924" Z="0.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.2" />
                  <Point X="4.38657082646" Y="-2.560796594711" Z="0.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.2" />
                  <Point X="4.303608010521" Y="-2.466989980366" Z="0.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.2" />
                  <Point X="3.100224582831" Y="-1.47068698435" Z="0.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.2" />
                  <Point X="3.106745092452" Y="-1.300916772912" Z="0.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.2" />
                  <Point X="3.209039391271" Y="-1.165842936839" Z="0.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.2" />
                  <Point X="3.3849126736" Y="-1.119047538944" Z="0.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.2" />
                  <Point X="3.518770792434" Y="-1.131649069918" Z="0.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.2" />
                  <Point X="4.984748011621" Y="-0.973740927365" Z="0.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.2" />
                  <Point X="5.043531968677" Y="-0.598897075548" Z="0.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.2" />
                  <Point X="4.944997965373" Y="-0.5415579344" Z="0.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.2" />
                  <Point X="3.662773220578" Y="-0.171575371163" Z="0.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.2" />
                  <Point X="3.604335027241" Y="-0.102215003491" Z="0.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.2" />
                  <Point X="3.582900827892" Y="-0.007655433858" Z="0.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.2" />
                  <Point X="3.598470622532" Y="0.088955097386" Z="0.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.2" />
                  <Point X="3.651044411159" Y="0.161733735084" Z="0.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.2" />
                  <Point X="3.740622193774" Y="0.216573473079" Z="0.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.2" />
                  <Point X="3.850969851934" Y="0.248413998369" Z="0.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.2" />
                  <Point X="4.98733498793" Y="0.958899863585" Z="0.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.2" />
                  <Point X="4.888814666021" Y="1.375681610978" Z="0.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.2" />
                  <Point X="4.768449540904" Y="1.393873844088" Z="0.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.2" />
                  <Point X="3.376421935325" Y="1.233482475443" Z="0.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.2" />
                  <Point X="3.304837000452" Y="1.270564622505" Z="0.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.2" />
                  <Point X="3.255069084239" Y="1.340928277018" Z="0.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.2" />
                  <Point X="3.234992124776" Y="1.425563648275" Z="0.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.2" />
                  <Point X="3.25341042446" Y="1.503215023585" Z="0.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.2" />
                  <Point X="3.308319490458" Y="1.578721866831" Z="0.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.2" />
                  <Point X="3.402789282124" Y="1.653670983142" Z="0.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.2" />
                  <Point X="4.254755303433" Y="2.773363100219" Z="0.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.2" />
                  <Point X="4.020955722971" Y="3.102645385934" Z="0.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.2" />
                  <Point X="3.88400447695" Y="3.060351060067" Z="0.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.2" />
                  <Point X="2.435953552062" Y="2.247230428764" Z="0.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.2" />
                  <Point X="2.365668025627" Y="2.253237180602" Z="0.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.2" />
                  <Point X="2.301874664967" Y="2.293454109653" Z="0.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.2" />
                  <Point X="2.257304490582" Y="2.355150195415" Z="0.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.2" />
                  <Point X="2.246192522156" Y="2.424090418205" Z="0.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.2" />
                  <Point X="2.265297513893" Y="2.50351602382" Z="0.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.2" />
                  <Point X="2.335274292792" Y="2.628134605005" Z="0.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.2" />
                  <Point X="2.783223100329" Y="4.247893037832" Z="0.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.2" />
                  <Point X="2.387279314033" Y="4.482391762112" Z="0.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.2" />
                  <Point X="1.976656906481" Y="4.678048512368" Z="0.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.2" />
                  <Point X="1.550596397575" Y="4.836008169723" Z="0.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.2" />
                  <Point X="0.914395904101" Y="4.993712862095" Z="0.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.2" />
                  <Point X="0.246281117138" Y="5.070768688525" Z="0.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.2" />
                  <Point X="0.177931831835" Y="5.019175144152" Z="0.2" />
                  <Point X="0" Y="4.355124473572" Z="0.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>