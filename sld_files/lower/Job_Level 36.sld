<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#187" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2615" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.839493225098" Y="-29.54328515625" />
                  <Point X="0.563301940918" Y="-28.5125234375" />
                  <Point X="0.557720153809" Y="-28.497138671875" />
                  <Point X="0.54236315918" Y="-28.467376953125" />
                  <Point X="0.429476806641" Y="-28.30473046875" />
                  <Point X="0.378635528564" Y="-28.2314765625" />
                  <Point X="0.356752349854" Y="-28.2090234375" />
                  <Point X="0.330497161865" Y="-28.189779296875" />
                  <Point X="0.302495025635" Y="-28.175669921875" />
                  <Point X="0.127810066223" Y="-28.121455078125" />
                  <Point X="0.049135883331" Y="-28.097037109375" />
                  <Point X="0.020983165741" Y="-28.092767578125" />
                  <Point X="-0.008657706261" Y="-28.092765625" />
                  <Point X="-0.036823932648" Y="-28.09703515625" />
                  <Point X="-0.211508895874" Y="-28.151251953125" />
                  <Point X="-0.290182922363" Y="-28.17566796875" />
                  <Point X="-0.318183105469" Y="-28.189775390625" />
                  <Point X="-0.344439025879" Y="-28.20901953125" />
                  <Point X="-0.366323425293" Y="-28.2314765625" />
                  <Point X="-0.479209960938" Y="-28.394125" />
                  <Point X="-0.530051086426" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.675380065918" Y="-28.97675390625" />
                  <Point X="-0.916584594727" Y="-29.87694140625" />
                  <Point X="-0.990542358398" Y="-29.8625859375" />
                  <Point X="-1.07932409668" Y="-29.845353515625" />
                  <Point X="-1.246417724609" Y="-29.802361328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.258240966797" Y="-29.306423828125" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287938354492" Y="-29.182966796875" />
                  <Point X="-1.304010620117" Y="-29.15512890625" />
                  <Point X="-1.323644897461" Y="-29.131205078125" />
                  <Point X="-1.48447265625" Y="-28.990162109375" />
                  <Point X="-1.556905639648" Y="-28.926640625" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.643027709961" Y="-28.890966796875" />
                  <Point X="-1.856482177734" Y="-28.8769765625" />
                  <Point X="-1.952616821289" Y="-28.87067578125" />
                  <Point X="-1.983414916992" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.220519287109" Y="-29.01364453125" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312789306641" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.404938232422" Y="-29.190474609375" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.671556152344" Y="-29.169974609375" />
                  <Point X="-2.801724609375" Y="-29.089376953125" />
                  <Point X="-3.078164550781" Y="-28.87652734375" />
                  <Point X="-3.104721923828" Y="-28.856080078125" />
                  <Point X="-2.937031738281" Y="-28.565630859375" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406587890625" Y="-27.616125" />
                  <Point X="-2.405575683594" Y="-27.585189453125" />
                  <Point X="-2.414561035156" Y="-27.555572265625" />
                  <Point X="-2.428779541016" Y="-27.5267421875" />
                  <Point X="-2.446807861328" Y="-27.501583984375" />
                  <Point X="-2.464156494141" Y="-27.484236328125" />
                  <Point X="-2.489316894531" Y="-27.466208984375" />
                  <Point X="-2.518145507812" Y="-27.451994140625" />
                  <Point X="-2.547761962891" Y="-27.44301171875" />
                  <Point X="-2.578694091797" Y="-27.444025390625" />
                  <Point X="-2.610219238281" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.461197265625" />
                  <Point X="-3.039572509766" Y="-27.692361328125" />
                  <Point X="-3.818024169922" Y="-28.14180078125" />
                  <Point X="-3.980031738281" Y="-27.92895703125" />
                  <Point X="-4.082862548828" Y="-27.793857421875" />
                  <Point X="-4.281055664063" Y="-27.461517578125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-4.004011230469" Y="-27.1876171875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084578613281" Y="-26.47559765625" />
                  <Point X="-3.066614257812" Y="-26.44846875" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.048544433594" Y="-26.399322265625" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.043347900391" Y="-26.359662109375" />
                  <Point X="-3.045556152344" Y="-26.327990234375" />
                  <Point X="-3.052556640625" Y="-26.29824609375" />
                  <Point X="-3.068637695312" Y="-26.27226171875" />
                  <Point X="-3.089470947266" Y="-26.248302734375" />
                  <Point X="-3.112974609375" Y="-26.228765625" />
                  <Point X="-3.131233886719" Y="-26.21801953125" />
                  <Point X="-3.139457275391" Y="-26.2131796875" />
                  <Point X="-3.168721923828" Y="-26.201955078125" />
                  <Point X="-3.200609130859" Y="-26.195474609375" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-3.737382080078" Y="-26.260927734375" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.793859863281" Y="-26.15010546875" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.886515136719" Y="-25.626017578125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.555897460938" Y="-25.49452734375" />
                  <Point X="-3.532875732422" Y="-25.22041015625" />
                  <Point X="-3.517494873047" Y="-25.214830078125" />
                  <Point X="-3.487729980469" Y="-25.199470703125" />
                  <Point X="-3.468594970703" Y="-25.186189453125" />
                  <Point X="-3.459977050781" Y="-25.180208984375" />
                  <Point X="-3.437524658203" Y="-25.158328125" />
                  <Point X="-3.418277832031" Y="-25.1320703125" />
                  <Point X="-3.404168212891" Y="-25.104068359375" />
                  <Point X="-3.397789794922" Y="-25.083517578125" />
                  <Point X="-3.394917236328" Y="-25.07426171875" />
                  <Point X="-3.390647705078" Y="-25.0461015625" />
                  <Point X="-3.390647705078" Y="-25.0164609375" />
                  <Point X="-3.394916503906" Y="-24.988302734375" />
                  <Point X="-3.401294921875" Y="-24.96775" />
                  <Point X="-3.404167480469" Y="-24.958494140625" />
                  <Point X="-3.418277099609" Y="-24.930490234375" />
                  <Point X="-3.4375234375" Y="-24.904232421875" />
                  <Point X="-3.459981933594" Y="-24.88234765625" />
                  <Point X="-3.479116943359" Y="-24.869068359375" />
                  <Point X="-3.487762451172" Y="-24.86373046875" />
                  <Point X="-3.511946289062" Y="-24.850529296875" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-3.993620605469" Y="-24.7186953125" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.85040625" Y="-24.1981796875" />
                  <Point X="-4.824487792969" Y="-24.023025390625" />
                  <Point X="-4.71893359375" Y="-23.63349609375" />
                  <Point X="-4.703551757812" Y="-23.576732421875" />
                  <Point X="-4.498993652344" Y="-23.6036640625" />
                  <Point X="-3.765666503906" Y="-23.70020703125" />
                  <Point X="-3.744984619141" Y="-23.700658203125" />
                  <Point X="-3.723424072266" Y="-23.698771484375" />
                  <Point X="-3.703138427734" Y="-23.694736328125" />
                  <Point X="-3.660786621094" Y="-23.6813828125" />
                  <Point X="-3.641712402344" Y="-23.675369140625" />
                  <Point X="-3.622785400391" Y="-23.667041015625" />
                  <Point X="-3.604041015625" Y="-23.656220703125" />
                  <Point X="-3.587356201172" Y="-23.64398828125" />
                  <Point X="-3.573715332031" Y="-23.62843359375" />
                  <Point X="-3.561300537109" Y="-23.610703125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.534357666016" Y="-23.55154296875" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520916503906" Y="-23.513208984375" />
                  <Point X="-3.517157714844" Y="-23.49189453125" />
                  <Point X="-3.515804443359" Y="-23.471255859375" />
                  <Point X="-3.518950683594" Y="-23.450810546875" />
                  <Point X="-3.524552490234" Y="-23.42990234375" />
                  <Point X="-3.532050292969" Y="-23.41062109375" />
                  <Point X="-3.552555175781" Y="-23.371232421875" />
                  <Point X="-3.561790039062" Y="-23.3534921875" />
                  <Point X="-3.573281982422" Y="-23.33629296875" />
                  <Point X="-3.587194580078" Y="-23.319712890625" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-3.866419921875" Y="-23.1026171875" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.181862304688" Y="-22.43887890625" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.801543212891" Y="-21.90694140625" />
                  <Point X="-3.750504638672" Y="-21.841337890625" />
                  <Point X="-3.657045898438" Y="-21.895296875" />
                  <Point X="-3.206656738281" Y="-22.155330078125" />
                  <Point X="-3.187729492188" Y="-22.163658203125" />
                  <Point X="-3.167087402344" Y="-22.17016796875" />
                  <Point X="-3.146794189453" Y="-22.174205078125" />
                  <Point X="-3.087809814453" Y="-22.179365234375" />
                  <Point X="-3.061244873047" Y="-22.181689453125" />
                  <Point X="-3.040560791016" Y="-22.18123828125" />
                  <Point X="-3.019102294922" Y="-22.178412109375" />
                  <Point X="-2.999013427734" Y="-22.173494140625" />
                  <Point X="-2.980464355469" Y="-22.16434765625" />
                  <Point X="-2.962210205078" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.904210449219" Y="-22.097904296875" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.848596191406" Y="-21.904896484375" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-2.986907226562" Y="-21.615623046875" />
                  <Point X="-3.183332763672" Y="-21.275404296875" />
                  <Point X="-2.8760234375" Y="-21.03979296875" />
                  <Point X="-2.70062109375" Y="-20.905314453125" />
                  <Point X="-2.260249755859" Y="-20.660654296875" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028893432617" Y="-20.78519921875" />
                  <Point X="-2.012314697266" Y="-20.799111328125" />
                  <Point X="-1.995114868164" Y="-20.810603515625" />
                  <Point X="-1.929465576172" Y="-20.844779296875" />
                  <Point X="-1.899898925781" Y="-20.860171875" />
                  <Point X="-1.880625976562" Y="-20.86766796875" />
                  <Point X="-1.859719116211" Y="-20.873271484375" />
                  <Point X="-1.83926953125" Y="-20.876419921875" />
                  <Point X="-1.818623291016" Y="-20.87506640625" />
                  <Point X="-1.797307495117" Y="-20.871306640625" />
                  <Point X="-1.777453125" Y="-20.865517578125" />
                  <Point X="-1.709075073242" Y="-20.837193359375" />
                  <Point X="-1.678279174805" Y="-20.8244375" />
                  <Point X="-1.660147216797" Y="-20.814490234375" />
                  <Point X="-1.642417236328" Y="-20.802076171875" />
                  <Point X="-1.626864746094" Y="-20.7884375" />
                  <Point X="-1.614633056641" Y="-20.771755859375" />
                  <Point X="-1.603810913086" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.573224487305" Y="-20.6634921875" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.571044677734" Y="-20.468041015625" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.176698974609" Y="-20.253853515625" />
                  <Point X="-0.949623535156" Y="-20.19019140625" />
                  <Point X="-0.415770202637" Y="-20.127708984375" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.267140838623" Y="-20.216435546875" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113990784" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425941467" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.206222259521" Y="-20.489736328125" />
                  <Point X="0.307419586182" Y="-20.1120625" />
                  <Point X="0.645774169922" Y="-20.147498046875" />
                  <Point X="0.84403112793" Y="-20.168259765625" />
                  <Point X="1.285725585938" Y="-20.2748984375" />
                  <Point X="1.481038574219" Y="-20.3220546875" />
                  <Point X="1.768061645508" Y="-20.426158203125" />
                  <Point X="1.894646240234" Y="-20.472072265625" />
                  <Point X="2.172640136719" Y="-20.602080078125" />
                  <Point X="2.294558837891" Y="-20.65909765625" />
                  <Point X="2.563155273438" Y="-20.81558203125" />
                  <Point X="2.680977050781" Y="-20.8842265625" />
                  <Point X="2.934262695312" Y="-21.064349609375" />
                  <Point X="2.943259033203" Y="-21.070748046875" />
                  <Point X="2.741653076172" Y="-21.419939453125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.4839453125" />
                  <Point X="2.118274169922" Y="-22.53930078125" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107727783203" Y="-22.619048828125" />
                  <Point X="2.113499755859" Y="-22.666916015625" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140070800781" Y="-22.75252734375" />
                  <Point X="2.169689208984" Y="-22.796177734375" />
                  <Point X="2.183028564453" Y="-22.8158359375" />
                  <Point X="2.194465576172" Y="-22.829671875" />
                  <Point X="2.221596923828" Y="-22.85440625" />
                  <Point X="2.265246582031" Y="-22.884025390625" />
                  <Point X="2.284905517578" Y="-22.897365234375" />
                  <Point X="2.304947021484" Y="-22.9077265625" />
                  <Point X="2.327036132812" Y="-22.915994140625" />
                  <Point X="2.34896484375" Y="-22.921337890625" />
                  <Point X="2.396831542969" Y="-22.927109375" />
                  <Point X="2.418389648438" Y="-22.929708984375" />
                  <Point X="2.436468261719" Y="-22.93015625" />
                  <Point X="2.473208007812" Y="-22.925830078125" />
                  <Point X="2.528563476563" Y="-22.91102734375" />
                  <Point X="2.553494384766" Y="-22.904359375" />
                  <Point X="2.565293457031" Y="-22.900359375" />
                  <Point X="2.588533691406" Y="-22.88985546875" />
                  <Point X="3.051953857422" Y="-22.62230078125" />
                  <Point X="3.967326660156" Y="-22.093810546875" />
                  <Point X="4.0533828125" Y="-22.213408203125" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.262198730469" Y="-22.540115234375" />
                  <Point X="4.012935302734" Y="-22.7313828125" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221424316406" Y="-23.339759765625" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.164134277344" Y="-23.410345703125" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.136603271484" Y="-23.44909375" />
                  <Point X="3.121629882812" Y="-23.482916015625" />
                  <Point X="3.106789550781" Y="-23.53598046875" />
                  <Point X="3.100105957031" Y="-23.559880859375" />
                  <Point X="3.09665234375" Y="-23.5821796875" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.109921875" Y="-23.6872734375" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.1206796875" Y="-23.7310234375" />
                  <Point X="3.136282958984" Y="-23.76426171875" />
                  <Point X="3.169417480469" Y="-23.814625" />
                  <Point X="3.184340576172" Y="-23.837306640625" />
                  <Point X="3.198895751953" Y="-23.854552734375" />
                  <Point X="3.216138427734" Y="-23.870640625" />
                  <Point X="3.234346191406" Y="-23.88396484375" />
                  <Point X="3.282362548828" Y="-23.910994140625" />
                  <Point X="3.30398828125" Y="-23.92316796875" />
                  <Point X="3.320523193359" Y="-23.9305" />
                  <Point X="3.356120117188" Y="-23.9405625" />
                  <Point X="3.421041748047" Y="-23.949142578125" />
                  <Point X="3.450280761719" Y="-23.953005859375" />
                  <Point X="3.462699462891" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="3.928421875" Y="-23.895060546875" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.815918945312" Y="-23.943890625" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.890433105469" Y="-24.35298828125" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.612942382812" Y="-24.43023046875" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704786132812" Y="-24.674416015625" />
                  <Point X="3.681547607422" Y="-24.684931640625" />
                  <Point X="3.617764160156" Y="-24.721798828125" />
                  <Point X="3.589037597656" Y="-24.73840234375" />
                  <Point X="3.574311523438" Y="-24.748904296875" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.509260253906" Y="-24.823189453125" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.47052734375" Y="-24.88589453125" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.450923828125" Y="-24.9740078125" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.021876953125" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.457935546875" Y="-25.1251640625" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.47052734375" Y="-25.176666015625" />
                  <Point X="3.480301025391" Y="-25.19812890625" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.530294433594" Y="-25.266173828125" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.559992431641" Y="-25.30123046875" />
                  <Point X="3.589035400391" Y="-25.324158203125" />
                  <Point X="3.652818847656" Y="-25.361025390625" />
                  <Point X="3.681545410156" Y="-25.377630859375" />
                  <Point X="3.692710205078" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.120280273438" Y="-25.500322265625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.871782714844" Y="-25.837560546875" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.46391015625" Y="-26.140296875" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.249474853516" Y="-26.03271875" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163973632812" Y="-26.05659765625" />
                  <Point X="3.136147216797" Y="-26.073490234375" />
                  <Point X="3.112397460938" Y="-26.0939609375" />
                  <Point X="3.036731445313" Y="-26.18496484375" />
                  <Point X="3.002653320312" Y="-26.22594921875" />
                  <Point X="2.987933837891" Y="-26.250328125" />
                  <Point X="2.976590087891" Y="-26.277712890625" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.958912841797" Y="-26.42321875" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.045729492188" Y="-26.675755859375" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.485264648438" Y="-27.04837890625" />
                  <Point X="4.213122558594" Y="-27.6068828125" />
                  <Point X="4.172055175781" Y="-27.6733359375" />
                  <Point X="4.124813964844" Y="-27.74978125" />
                  <Point X="4.028980224609" Y="-27.8859453125" />
                  <Point X="3.726794677734" Y="-27.711478515625" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786137207031" Y="-27.170015625" />
                  <Point X="2.754224121094" Y="-27.15982421875" />
                  <Point X="2.605234863281" Y="-27.132916015625" />
                  <Point X="2.538133789062" Y="-27.120798828125" />
                  <Point X="2.506777099609" Y="-27.120396484375" />
                  <Point X="2.47460546875" Y="-27.12535546875" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.321060302734" Y="-27.200318359375" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.139390625" Y="-27.414212890625" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.122582763672" Y="-27.712248046875" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.392560791016" Y="-28.2430546875" />
                  <Point X="2.861283935547" Y="-29.05490625" />
                  <Point X="2.837913330078" Y="-29.071599609375" />
                  <Point X="2.781859619141" Y="-29.11163671875" />
                  <Point X="2.701764160156" Y="-29.16348046875" />
                  <Point X="2.465047607422" Y="-28.854986328125" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506225586" Y="-27.922181640625" />
                  <Point X="1.721923583984" Y="-27.900556640625" />
                  <Point X="1.574979980469" Y="-27.8060859375" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365722656" Y="-27.743435546875" />
                  <Point X="1.417100708008" Y="-27.741119140625" />
                  <Point X="1.256392700195" Y="-27.75590625" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.156362548828" Y="-27.7693984375" />
                  <Point X="1.128976928711" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.795462890625" />
                  <Point X="0.980500610352" Y="-27.89864453125" />
                  <Point X="0.92461151123" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.838520690918" Y="-28.196515625" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.887966430664" Y="-28.841333984375" />
                  <Point X="1.022065551758" Y="-29.859916015625" />
                  <Point X="0.975716918945" Y="-29.87007421875" />
                  <Point X="0.929315368652" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058416870117" Y="-29.752638671875" />
                  <Point X="-1.14124609375" Y="-29.731326171875" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.16506640625" Y="-29.287890625" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188125" Y="-29.178470703125" />
                  <Point X="-1.19902734375" Y="-29.14950390625" />
                  <Point X="-1.205666015625" Y="-29.135466796875" />
                  <Point X="-1.22173840332" Y="-29.10762890625" />
                  <Point X="-1.230575317383" Y="-29.094861328125" />
                  <Point X="-1.250209594727" Y="-29.0709375" />
                  <Point X="-1.261006958008" Y="-29.05978125" />
                  <Point X="-1.421834594727" Y="-28.91873828125" />
                  <Point X="-1.494267578125" Y="-28.855216796875" />
                  <Point X="-1.506735839844" Y="-28.84596875" />
                  <Point X="-1.53301940918" Y="-28.829623046875" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576531860352" Y="-28.810224609375" />
                  <Point X="-1.591315917969" Y="-28.805474609375" />
                  <Point X="-1.621457275391" Y="-28.798447265625" />
                  <Point X="-1.636814575195" Y="-28.796169921875" />
                  <Point X="-1.850269042969" Y="-28.7821796875" />
                  <Point X="-1.946403686523" Y="-28.77587890625" />
                  <Point X="-1.961927978516" Y="-28.7761328125" />
                  <Point X="-1.992726074219" Y="-28.779166015625" />
                  <Point X="-2.00799987793" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053667236328" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436767578" Y="-28.815810546875" />
                  <Point X="-2.273298583984" Y="-28.934654296875" />
                  <Point X="-2.353403320312" Y="-28.988177734375" />
                  <Point X="-2.359686279297" Y="-28.992759765625" />
                  <Point X="-2.380448242188" Y="-29.01013671875" />
                  <Point X="-2.402761962891" Y="-29.032775390625" />
                  <Point X="-2.410471923828" Y="-29.041630859375" />
                  <Point X="-2.480307128906" Y="-29.132642578125" />
                  <Point X="-2.503200927734" Y="-29.162478515625" />
                  <Point X="-2.621545410156" Y="-29.089203125" />
                  <Point X="-2.747598388672" Y="-29.011154296875" />
                  <Point X="-2.980862060547" Y="-28.831548828125" />
                  <Point X="-2.854759277344" Y="-28.613130859375" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413085938" Y="-27.634658203125" />
                  <Point X="-2.311638671875" Y="-27.619232421875" />
                  <Point X="-2.310626464844" Y="-27.588296875" />
                  <Point X="-2.314667236328" Y="-27.557609375" />
                  <Point X="-2.323652587891" Y="-27.5279921875" />
                  <Point X="-2.329359375" Y="-27.513552734375" />
                  <Point X="-2.343577880859" Y="-27.48472265625" />
                  <Point X="-2.351559326172" Y="-27.47140625" />
                  <Point X="-2.369587646484" Y="-27.446248046875" />
                  <Point X="-2.379634521484" Y="-27.43440625" />
                  <Point X="-2.396983154297" Y="-27.41705859375" />
                  <Point X="-2.408825927734" Y="-27.40701171875" />
                  <Point X="-2.433986328125" Y="-27.388984375" />
                  <Point X="-2.447303955078" Y="-27.38100390625" />
                  <Point X="-2.476132568359" Y="-27.3667890625" />
                  <Point X="-2.490572998047" Y="-27.361083984375" />
                  <Point X="-2.520189453125" Y="-27.3521015625" />
                  <Point X="-2.550873535156" Y="-27.3480625" />
                  <Point X="-2.581805664062" Y="-27.349076171875" />
                  <Point X="-2.597229736328" Y="-27.3508515625" />
                  <Point X="-2.628754882813" Y="-27.357123046875" />
                  <Point X="-2.643680175781" Y="-27.361384765625" />
                  <Point X="-2.67264453125" Y="-27.37228515625" />
                  <Point X="-2.68668359375" Y="-27.378923828125" />
                  <Point X="-3.087072509766" Y="-27.610087890625" />
                  <Point X="-3.793089355469" Y="-28.01770703125" />
                  <Point X="-3.904438476562" Y="-27.87141796875" />
                  <Point X="-4.004020019531" Y="-27.740587890625" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.946178710938" Y="-27.262986328125" />
                  <Point X="-3.048122070312" Y="-26.573884765625" />
                  <Point X="-3.036482910156" Y="-26.5633125" />
                  <Point X="-3.015106933594" Y="-26.54039453125" />
                  <Point X="-3.005370361328" Y="-26.528048828125" />
                  <Point X="-2.987406005859" Y="-26.500919921875" />
                  <Point X="-2.979837402344" Y="-26.487130859375" />
                  <Point X="-2.967079589844" Y="-26.45849609375" />
                  <Point X="-2.961890380859" Y="-26.443650390625" />
                  <Point X="-2.956578369141" Y="-26.423138671875" />
                  <Point X="-2.951552734375" Y="-26.3988046875" />
                  <Point X="-2.948748779297" Y="-26.368380859375" />
                  <Point X="-2.948577880859" Y="-26.3530546875" />
                  <Point X="-2.950786132812" Y="-26.3213828125" />
                  <Point X="-2.953082763672" Y="-26.3062265625" />
                  <Point X="-2.960083251953" Y="-26.276482421875" />
                  <Point X="-2.971775146484" Y="-26.248251953125" />
                  <Point X="-2.987856201172" Y="-26.222267578125" />
                  <Point X="-2.99694921875" Y="-26.20992578125" />
                  <Point X="-3.017782470703" Y="-26.185966796875" />
                  <Point X="-3.028743896484" Y="-26.17524609375" />
                  <Point X="-3.052247558594" Y="-26.155708984375" />
                  <Point X="-3.064789794922" Y="-26.146892578125" />
                  <Point X="-3.083049072266" Y="-26.136146484375" />
                  <Point X="-3.105436279297" Y="-26.12448046875" />
                  <Point X="-3.134700927734" Y="-26.113255859375" />
                  <Point X="-3.149801757812" Y="-26.108857421875" />
                  <Point X="-3.181688964844" Y="-26.102376953125" />
                  <Point X="-3.197305419922" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244328613281" Y="-26.100197265625" />
                  <Point X="-3.749781738281" Y="-26.166740234375" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.701814941406" Y="-26.12659375" />
                  <Point X="-4.740762207031" Y="-25.974115234375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.531309082031" Y="-25.586291015625" />
                  <Point X="-3.508287597656" Y="-25.312173828125" />
                  <Point X="-3.5004765625" Y="-25.30971484375" />
                  <Point X="-3.473930908203" Y="-25.299251953125" />
                  <Point X="-3.444166015625" Y="-25.283892578125" />
                  <Point X="-3.433561523438" Y="-25.277513671875" />
                  <Point X="-3.414426513672" Y="-25.264232421875" />
                  <Point X="-3.393673339844" Y="-25.248244140625" />
                  <Point X="-3.371220947266" Y="-25.22636328125" />
                  <Point X="-3.360903808594" Y="-25.214490234375" />
                  <Point X="-3.341656982422" Y="-25.188232421875" />
                  <Point X="-3.333439208984" Y="-25.174818359375" />
                  <Point X="-3.319329589844" Y="-25.14681640625" />
                  <Point X="-3.313437744141" Y="-25.132228515625" />
                  <Point X="-3.307059326172" Y="-25.111677734375" />
                  <Point X="-3.300990722656" Y="-25.088501953125" />
                  <Point X="-3.296721191406" Y="-25.060341796875" />
                  <Point X="-3.295647705078" Y="-25.0461015625" />
                  <Point X="-3.295647705078" Y="-25.0164609375" />
                  <Point X="-3.296720947266" Y="-25.002220703125" />
                  <Point X="-3.300989746094" Y="-24.9740625" />
                  <Point X="-3.304185302734" Y="-24.96014453125" />
                  <Point X="-3.310563720703" Y="-24.939591796875" />
                  <Point X="-3.319327880859" Y="-24.915748046875" />
                  <Point X="-3.3334375" Y="-24.887744140625" />
                  <Point X="-3.341655517578" Y="-24.874328125" />
                  <Point X="-3.360901855469" Y="-24.8480703125" />
                  <Point X="-3.371222900391" Y="-24.836193359375" />
                  <Point X="-3.393681396484" Y="-24.81430859375" />
                  <Point X="-3.405818847656" Y="-24.80430078125" />
                  <Point X="-3.424953857422" Y="-24.791021484375" />
                  <Point X="-3.442244873047" Y="-24.780345703125" />
                  <Point X="-3.466428710938" Y="-24.76714453125" />
                  <Point X="-3.476638916016" Y="-24.762333984375" />
                  <Point X="-3.497568847656" Y="-24.753955078125" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.969032958984" Y="-24.626931640625" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.7564296875" Y="-24.2120859375" />
                  <Point X="-4.731331054688" Y="-24.04247265625" />
                  <Point X="-4.633586425781" Y="-23.681763671875" />
                  <Point X="-4.511394042969" Y="-23.6978515625" />
                  <Point X="-3.778066894531" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747056640625" Y="-23.795634765625" />
                  <Point X="-3.736703125" Y="-23.795296875" />
                  <Point X="-3.715142578125" Y="-23.79341015625" />
                  <Point X="-3.704890136719" Y="-23.7919453125" />
                  <Point X="-3.684604492188" Y="-23.78791015625" />
                  <Point X="-3.674571289062" Y="-23.78533984375" />
                  <Point X="-3.632219482422" Y="-23.771986328125" />
                  <Point X="-3.603451416016" Y="-23.76232421875" />
                  <Point X="-3.584524414062" Y="-23.75399609375" />
                  <Point X="-3.575291259766" Y="-23.74931640625" />
                  <Point X="-3.556546875" Y="-23.73849609375" />
                  <Point X="-3.547870849609" Y="-23.7328359375" />
                  <Point X="-3.531186035156" Y="-23.720603515625" />
                  <Point X="-3.515930908203" Y="-23.706625" />
                  <Point X="-3.502290039063" Y="-23.6910703125" />
                  <Point X="-3.495895507812" Y="-23.682921875" />
                  <Point X="-3.483480712891" Y="-23.66519140625" />
                  <Point X="-3.478013427734" Y="-23.656400390625" />
                  <Point X="-3.468064453125" Y="-23.638267578125" />
                  <Point X="-3.463582763672" Y="-23.62892578125" />
                  <Point X="-3.446588867188" Y="-23.5878984375" />
                  <Point X="-3.435499023438" Y="-23.5596484375" />
                  <Point X="-3.429711425781" Y="-23.539791015625" />
                  <Point X="-3.427360107422" Y="-23.52970703125" />
                  <Point X="-3.423601318359" Y="-23.508392578125" />
                  <Point X="-3.422361328125" Y="-23.498109375" />
                  <Point X="-3.421008056641" Y="-23.477470703125" />
                  <Point X="-3.421909667969" Y="-23.456806640625" />
                  <Point X="-3.425055908203" Y="-23.436361328125" />
                  <Point X="-3.427187255859" Y="-23.426224609375" />
                  <Point X="-3.4327890625" Y="-23.40531640625" />
                  <Point X="-3.436011230469" Y="-23.39547265625" />
                  <Point X="-3.443509033203" Y="-23.37619140625" />
                  <Point X="-3.447784667969" Y="-23.36675390625" />
                  <Point X="-3.468289550781" Y="-23.327365234375" />
                  <Point X="-3.482800048828" Y="-23.300712890625" />
                  <Point X="-3.494291992188" Y="-23.283513671875" />
                  <Point X="-3.500508300781" Y="-23.2752265625" />
                  <Point X="-3.514420898438" Y="-23.258646484375" />
                  <Point X="-3.521501708984" Y="-23.251087890625" />
                  <Point X="-3.536442871094" Y="-23.23678515625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.808587402344" Y="-23.027248046875" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.099815917969" Y="-22.486767578125" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.7265625" Y="-21.965275390625" />
                  <Point X="-3.726337890625" Y="-21.964986328125" />
                  <Point X="-3.704546386719" Y="-21.977568359375" />
                  <Point X="-3.254157226562" Y="-22.2376015625" />
                  <Point X="-3.244917480469" Y="-22.24228515625" />
                  <Point X="-3.225990234375" Y="-22.25061328125" />
                  <Point X="-3.216302001953" Y="-22.254259765625" />
                  <Point X="-3.195659912109" Y="-22.26076953125" />
                  <Point X="-3.185623291016" Y="-22.263341796875" />
                  <Point X="-3.165330078125" Y="-22.26737890625" />
                  <Point X="-3.155073486328" Y="-22.26884375" />
                  <Point X="-3.096089111328" Y="-22.27400390625" />
                  <Point X="-3.069524169922" Y="-22.276328125" />
                  <Point X="-3.059173095703" Y="-22.276666015625" />
                  <Point X="-3.038489013672" Y="-22.27621484375" />
                  <Point X="-3.028156005859" Y="-22.27542578125" />
                  <Point X="-3.006697509766" Y="-22.272599609375" />
                  <Point X="-2.996512451172" Y="-22.2706875" />
                  <Point X="-2.976423583984" Y="-22.26576953125" />
                  <Point X="-2.956999267578" Y="-22.25869921875" />
                  <Point X="-2.938450195312" Y="-22.249552734375" />
                  <Point X="-2.929421630859" Y="-22.244470703125" />
                  <Point X="-2.911167480469" Y="-22.232841796875" />
                  <Point X="-2.902748291016" Y="-22.22680859375" />
                  <Point X="-2.886615966797" Y="-22.213861328125" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.837035400391" Y="-22.165080078125" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.753957763672" Y="-21.8966171875" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-2.904634765625" Y="-21.568123046875" />
                  <Point X="-3.05938671875" Y="-21.3000859375" />
                  <Point X="-2.818221191406" Y="-21.115185546875" />
                  <Point X="-2.648368408203" Y="-20.984962890625" />
                  <Point X="-2.214112304688" Y="-20.74369921875" />
                  <Point X="-2.192523193359" Y="-20.731705078125" />
                  <Point X="-2.118563964844" Y="-20.82808984375" />
                  <Point X="-2.111822998047" Y="-20.83594921875" />
                  <Point X="-2.097520996094" Y="-20.850890625" />
                  <Point X="-2.089960449219" Y="-20.857970703125" />
                  <Point X="-2.073381835938" Y="-20.8718828125" />
                  <Point X="-2.065092773438" Y="-20.8781015625" />
                  <Point X="-2.047892822266" Y="-20.88959375" />
                  <Point X="-2.038981933594" Y="-20.894869140625" />
                  <Point X="-1.973332641602" Y="-20.929044921875" />
                  <Point X="-1.943765991211" Y="-20.9444375" />
                  <Point X="-1.934335571289" Y="-20.9487109375" />
                  <Point X="-1.9150625" Y="-20.95620703125" />
                  <Point X="-1.905220092773" Y="-20.9594296875" />
                  <Point X="-1.884313232422" Y="-20.965033203125" />
                  <Point X="-1.874175048828" Y="-20.967166015625" />
                  <Point X="-1.853725463867" Y="-20.970314453125" />
                  <Point X="-1.833054931641" Y="-20.971216796875" />
                  <Point X="-1.812408691406" Y="-20.96986328125" />
                  <Point X="-1.802121582031" Y="-20.968623046875" />
                  <Point X="-1.780805786133" Y="-20.96486328125" />
                  <Point X="-1.770715087891" Y="-20.962509765625" />
                  <Point X="-1.750860717773" Y="-20.956720703125" />
                  <Point X="-1.741097045898" Y="-20.95328515625" />
                  <Point X="-1.672718994141" Y="-20.9249609375" />
                  <Point X="-1.641923095703" Y="-20.912205078125" />
                  <Point X="-1.632586181641" Y="-20.9077265625" />
                  <Point X="-1.614454101562" Y="-20.897779296875" />
                  <Point X="-1.605659301758" Y="-20.892310546875" />
                  <Point X="-1.587929077148" Y="-20.879896484375" />
                  <Point X="-1.579780639648" Y="-20.873501953125" />
                  <Point X="-1.564228027344" Y="-20.85986328125" />
                  <Point X="-1.550252929688" Y="-20.84461328125" />
                  <Point X="-1.538021240234" Y="-20.827931640625" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.52153894043" Y="-20.80051171875" />
                  <Point X="-1.516855834961" Y="-20.791271484375" />
                  <Point X="-1.508525146484" Y="-20.772337890625" />
                  <Point X="-1.504877197266" Y="-20.76264453125" />
                  <Point X="-1.482621459961" Y="-20.69205859375" />
                  <Point X="-1.472597900391" Y="-20.660267578125" />
                  <Point X="-1.470026489258" Y="-20.650234375" />
                  <Point X="-1.465990966797" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.476857421875" Y="-20.455640625" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-1.151053100586" Y="-20.345326171875" />
                  <Point X="-0.931164855957" Y="-20.2836796875" />
                  <Point X="-0.404726745605" Y="-20.222064453125" />
                  <Point X="-0.36522265625" Y="-20.21744140625" />
                  <Point X="-0.358903778076" Y="-20.2410234375" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.297985198975" Y="-20.51432421875" />
                  <Point X="0.378190704346" Y="-20.2149921875" />
                  <Point X="0.635879089355" Y="-20.24198046875" />
                  <Point X="0.827852722168" Y="-20.262083984375" />
                  <Point X="1.263430419922" Y="-20.36724609375" />
                  <Point X="1.453622924805" Y="-20.413166015625" />
                  <Point X="1.735669799805" Y="-20.51546484375" />
                  <Point X="1.858247314453" Y="-20.55992578125" />
                  <Point X="2.132395507812" Y="-20.688134765625" />
                  <Point X="2.250430664063" Y="-20.7433359375" />
                  <Point X="2.515332519531" Y="-20.89766796875" />
                  <Point X="2.629432617188" Y="-20.96414453125" />
                  <Point X="2.817778808594" Y="-21.0980859375" />
                  <Point X="2.659380615234" Y="-21.372439453125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.0531796875" Y="-22.42656640625" />
                  <Point X="2.044181640625" Y="-22.450439453125" />
                  <Point X="2.041301635742" Y="-22.459404296875" />
                  <Point X="2.026498779297" Y="-22.514759765625" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017912719727" Y="-22.548537109375" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755371094" Y="-22.616759765625" />
                  <Point X="2.013410888672" Y="-22.630421875" />
                  <Point X="2.019182983398" Y="-22.6782890625" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.02914440918" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.045320922852" Y="-22.7761171875" />
                  <Point X="2.055682373047" Y="-22.796158203125" />
                  <Point X="2.061459228516" Y="-22.8058671875" />
                  <Point X="2.091077636719" Y="-22.849517578125" />
                  <Point X="2.104416992188" Y="-22.86917578125" />
                  <Point X="2.109806152344" Y="-22.87636328125" />
                  <Point X="2.130463134766" Y="-22.899876953125" />
                  <Point X="2.157594482422" Y="-22.924611328125" />
                  <Point X="2.168254638672" Y="-22.933017578125" />
                  <Point X="2.211904296875" Y="-22.96263671875" />
                  <Point X="2.231563232422" Y="-22.9759765625" />
                  <Point X="2.241276855469" Y="-22.98175390625" />
                  <Point X="2.261318359375" Y="-22.992115234375" />
                  <Point X="2.271646240234" Y="-22.99669921875" />
                  <Point X="2.293735351562" Y="-23.004966796875" />
                  <Point X="2.304543945312" Y="-23.00829296875" />
                  <Point X="2.32647265625" Y="-23.01363671875" />
                  <Point X="2.337592773438" Y="-23.015654296875" />
                  <Point X="2.385459472656" Y="-23.02142578125" />
                  <Point X="2.407017578125" Y="-23.024025390625" />
                  <Point X="2.416040039062" Y="-23.0246796875" />
                  <Point X="2.447577880859" Y="-23.02450390625" />
                  <Point X="2.484317626953" Y="-23.020177734375" />
                  <Point X="2.497749755859" Y="-23.01760546875" />
                  <Point X="2.553105224609" Y="-23.002802734375" />
                  <Point X="2.578036132812" Y="-22.996134765625" />
                  <Point X="2.583995361328" Y="-22.994330078125" />
                  <Point X="2.604419921875" Y="-22.986927734375" />
                  <Point X="2.62766015625" Y="-22.976423828125" />
                  <Point X="2.636033447266" Y="-22.97212890625" />
                  <Point X="3.099453613281" Y="-22.70457421875" />
                  <Point X="3.940405517578" Y="-22.21905078125" />
                  <Point X="3.976270507812" Y="-22.26889453125" />
                  <Point X="4.043958251953" Y="-22.36296484375" />
                  <Point X="4.136884765625" Y="-22.51652734375" />
                  <Point X="3.955102783203" Y="-22.656013671875" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168134033203" Y="-23.260134765625" />
                  <Point X="3.152119140625" Y="-23.27478515625" />
                  <Point X="3.134668701172" Y="-23.2933984375" />
                  <Point X="3.128576904297" Y="-23.300578125" />
                  <Point X="3.088737304688" Y="-23.35255078125" />
                  <Point X="3.070794677734" Y="-23.375958984375" />
                  <Point X="3.065634033203" Y="-23.383400390625" />
                  <Point X="3.049735351562" Y="-23.41063671875" />
                  <Point X="3.034761962891" Y="-23.444458984375" />
                  <Point X="3.030140380859" Y="-23.457330078125" />
                  <Point X="3.015300048828" Y="-23.51039453125" />
                  <Point X="3.008616455078" Y="-23.534294921875" />
                  <Point X="3.006225341797" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578892578125" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.016881835938" Y="-23.706470703125" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024597167969" Y="-23.741763671875" />
                  <Point X="3.034683837891" Y="-23.771392578125" />
                  <Point X="3.050287109375" Y="-23.804630859375" />
                  <Point X="3.056918945312" Y="-23.8164765625" />
                  <Point X="3.090053466797" Y="-23.86683984375" />
                  <Point X="3.1049765625" Y="-23.889521484375" />
                  <Point X="3.111740722656" Y="-23.898578125" />
                  <Point X="3.126295898438" Y="-23.91582421875" />
                  <Point X="3.134086914063" Y="-23.924013671875" />
                  <Point X="3.151329589844" Y="-23.9401015625" />
                  <Point X="3.160035888672" Y="-23.9473046875" />
                  <Point X="3.178243652344" Y="-23.96062890625" />
                  <Point X="3.187745117188" Y="-23.96675" />
                  <Point X="3.235761474609" Y="-23.993779296875" />
                  <Point X="3.257387207031" Y="-24.005953125" />
                  <Point X="3.265479003906" Y="-24.010013671875" />
                  <Point X="3.294681396484" Y="-24.02191796875" />
                  <Point X="3.330278320312" Y="-24.03198046875" />
                  <Point X="3.343673095703" Y="-24.034744140625" />
                  <Point X="3.408594726563" Y="-24.04332421875" />
                  <Point X="3.437833740234" Y="-24.0471875" />
                  <Point X="3.444034179688" Y="-24.04780078125" />
                  <Point X="3.465709960938" Y="-24.04877734375" />
                  <Point X="3.491214111328" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="3.940821777344" Y="-23.989248046875" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.723614746094" Y="-23.966361328125" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="4.588354003906" Y="-24.338466796875" />
                  <Point X="3.691991210938" Y="-24.578646484375" />
                  <Point X="3.686025634766" Y="-24.58045703125" />
                  <Point X="3.66562109375" Y="-24.587865234375" />
                  <Point X="3.642382568359" Y="-24.598380859375" />
                  <Point X="3.634007080078" Y="-24.602681640625" />
                  <Point X="3.570223632812" Y="-24.639548828125" />
                  <Point X="3.541497070312" Y="-24.65615234375" />
                  <Point X="3.533877929688" Y="-24.661056640625" />
                  <Point X="3.508775878906" Y="-24.68012890625" />
                  <Point X="3.481994628906" Y="-24.7056484375" />
                  <Point X="3.472795898438" Y="-24.7157734375" />
                  <Point X="3.434525878906" Y="-24.7645390625" />
                  <Point X="3.417290039063" Y="-24.786501953125" />
                  <Point X="3.410853515625" Y="-24.79579296875" />
                  <Point X="3.399130126953" Y="-24.815072265625" />
                  <Point X="3.393843261719" Y="-24.825060546875" />
                  <Point X="3.384069580078" Y="-24.8465234375" />
                  <Point X="3.380005615234" Y="-24.8570703125" />
                  <Point X="3.373158935547" Y="-24.878572265625" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.357619384766" Y="-24.956138671875" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026263671875" />
                  <Point X="3.350280029297" Y="-25.06294140625" />
                  <Point X="3.351874511719" Y="-25.076423828125" />
                  <Point X="3.364631347656" Y="-25.143033203125" />
                  <Point X="3.370376708984" Y="-25.173033203125" />
                  <Point X="3.373158935547" Y="-25.183986328125" />
                  <Point X="3.380005371094" Y="-25.20548828125" />
                  <Point X="3.384069580078" Y="-25.216037109375" />
                  <Point X="3.393843261719" Y="-25.2375" />
                  <Point X="3.399130126953" Y="-25.24748828125" />
                  <Point X="3.410853515625" Y="-25.266767578125" />
                  <Point X="3.417290039063" Y="-25.27605859375" />
                  <Point X="3.455560058594" Y="-25.32482421875" />
                  <Point X="3.472795898438" Y="-25.346787109375" />
                  <Point X="3.478716064453" Y="-25.353630859375" />
                  <Point X="3.501127685547" Y="-25.375794921875" />
                  <Point X="3.530170654297" Y="-25.39872265625" />
                  <Point X="3.541494873047" Y="-25.406408203125" />
                  <Point X="3.605278320313" Y="-25.443275390625" />
                  <Point X="3.634004882812" Y="-25.459880859375" />
                  <Point X="3.639504150391" Y="-25.462822265625" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683025878906" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.095692626953" Y="-25.5920859375" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.777844238281" Y="-25.8233984375" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.727801757812" Y="-26.07921875" />
                  <Point X="4.476310546875" Y="-26.046109375" />
                  <Point X="3.436782226562" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354481933594" Y="-25.912677734375" />
                  <Point X="3.229297607422" Y="-25.93988671875" />
                  <Point X="3.172917724609" Y="-25.952140625" />
                  <Point X="3.157874267578" Y="-25.9567421875" />
                  <Point X="3.128752929688" Y="-25.9683671875" />
                  <Point X="3.114675048828" Y="-25.975390625" />
                  <Point X="3.086848632812" Y="-25.992283203125" />
                  <Point X="3.074123535156" Y="-26.00153125" />
                  <Point X="3.050373779297" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.963683105469" Y="-26.124228515625" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921327392578" Y="-26.176845703125" />
                  <Point X="2.906607910156" Y="-26.201224609375" />
                  <Point X="2.900166015625" Y="-26.213970703125" />
                  <Point X="2.888822265625" Y="-26.24135546875" />
                  <Point X="2.884363769531" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.8643125" Y="-26.414513671875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.965819580078" Y="-26.727130859375" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.427432373047" Y="-27.123748046875" />
                  <Point X="4.087170654297" Y="-27.629982421875" />
                  <Point X="4.045496337891" Y="-27.697419921875" />
                  <Point X="4.001273193359" Y="-27.76025390625" />
                  <Point X="3.774294677734" Y="-27.62920703125" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841199707031" Y="-27.090890625" />
                  <Point X="2.815037353516" Y="-27.079517578125" />
                  <Point X="2.783124267578" Y="-27.069326171875" />
                  <Point X="2.771108398438" Y="-27.0663359375" />
                  <Point X="2.622119140625" Y="-27.039427734375" />
                  <Point X="2.555018066406" Y="-27.027310546875" />
                  <Point X="2.539352539062" Y="-27.025806640625" />
                  <Point X="2.507995849609" Y="-27.025404296875" />
                  <Point X="2.4923046875" Y="-27.026505859375" />
                  <Point X="2.460133056641" Y="-27.03146484375" />
                  <Point X="2.444841064453" Y="-27.035138671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400589599609" Y="-27.051109375" />
                  <Point X="2.276816162109" Y="-27.11625" />
                  <Point X="2.221071533203" Y="-27.145587890625" />
                  <Point X="2.208967773438" Y="-27.153171875" />
                  <Point X="2.186038330078" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144942138672" Y="-27.21116015625" />
                  <Point X="2.128047607422" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.055322509766" Y="-27.36996875" />
                  <Point X="2.025984741211" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.44019140625" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.029095092773" Y="-27.729130859375" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236816406" Y="-27.831546875" />
                  <Point X="2.0640703125" Y="-27.862482421875" />
                  <Point X="2.069547119141" Y="-27.873580078125" />
                  <Point X="2.310288574219" Y="-28.2905546875" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723752929688" Y="-29.03608203125" />
                  <Point X="2.540416015625" Y="-28.797154296875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657104492" Y="-27.870150390625" />
                  <Point X="1.808834594727" Y="-27.84962890625" />
                  <Point X="1.783251831055" Y="-27.82800390625" />
                  <Point X="1.773298217773" Y="-27.820646484375" />
                  <Point X="1.626354614258" Y="-27.72617578125" />
                  <Point X="1.560174804688" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517470703125" Y="-27.663873046875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926513672" Y="-27.65115234375" />
                  <Point X="1.455385009766" Y="-27.6486953125" />
                  <Point X="1.424119995117" Y="-27.64637890625" />
                  <Point X="1.408396362305" Y="-27.64651953125" />
                  <Point X="1.247688354492" Y="-27.661306640625" />
                  <Point X="1.175309326172" Y="-27.667966796875" />
                  <Point X="1.161226196289" Y="-27.67033984375" />
                  <Point X="1.133575317383" Y="-27.677171875" />
                  <Point X="1.120007080078" Y="-27.681630859375" />
                  <Point X="1.092621337891" Y="-27.692974609375" />
                  <Point X="1.07987512207" Y="-27.699416015625" />
                  <Point X="1.055493408203" Y="-27.71413671875" />
                  <Point X="1.043857910156" Y="-27.722416015625" />
                  <Point X="0.919763244629" Y="-27.82559765625" />
                  <Point X="0.863874206543" Y="-27.872068359375" />
                  <Point X="0.85265435791" Y="-27.88308984375" />
                  <Point X="0.832184082031" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.745688232422" Y="-28.176337890625" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.793779174805" Y="-28.853734375" />
                  <Point X="0.83309173584" Y="-29.152341796875" />
                  <Point X="0.655064819336" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642143676758" Y="-28.453576171875" />
                  <Point X="0.626786621094" Y="-28.423814453125" />
                  <Point X="0.620407287598" Y="-28.413208984375" />
                  <Point X="0.507521057129" Y="-28.2505625" />
                  <Point X="0.4566796875" Y="-28.17730859375" />
                  <Point X="0.446668609619" Y="-28.165169921875" />
                  <Point X="0.424785369873" Y="-28.142716796875" />
                  <Point X="0.412913513184" Y="-28.13240234375" />
                  <Point X="0.38665838623" Y="-28.113158203125" />
                  <Point X="0.373244720459" Y="-28.104939453125" />
                  <Point X="0.345242675781" Y="-28.090830078125" />
                  <Point X="0.330654022217" Y="-28.084939453125" />
                  <Point X="0.155969039917" Y="-28.030724609375" />
                  <Point X="0.077294914246" Y="-28.006306640625" />
                  <Point X="0.063380393982" Y="-28.003111328125" />
                  <Point X="0.035227645874" Y="-27.998841796875" />
                  <Point X="0.020989425659" Y="-27.997767578125" />
                  <Point X="-0.008651480675" Y="-27.997765625" />
                  <Point X="-0.022895498276" Y="-27.998837890625" />
                  <Point X="-0.051061767578" Y="-28.003107421875" />
                  <Point X="-0.06498387146" Y="-28.0063046875" />
                  <Point X="-0.239668838501" Y="-28.060521484375" />
                  <Point X="-0.318342803955" Y="-28.0849375" />
                  <Point X="-0.332928222656" Y="-28.090828125" />
                  <Point X="-0.360928344727" Y="-28.104935546875" />
                  <Point X="-0.374343322754" Y="-28.11315234375" />
                  <Point X="-0.400599212646" Y="-28.132396484375" />
                  <Point X="-0.412475952148" Y="-28.142716796875" />
                  <Point X="-0.43436038208" Y="-28.165173828125" />
                  <Point X="-0.444367889404" Y="-28.177310546875" />
                  <Point X="-0.557254455566" Y="-28.339958984375" />
                  <Point X="-0.60809564209" Y="-28.4132109375" />
                  <Point X="-0.612468994141" Y="-28.42012890625" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.767142944336" Y="-28.952166015625" />
                  <Point X="-0.985425109863" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.690692629394" Y="-24.311044209135" />
                  <Point X="4.654320849023" Y="-23.895312857148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.743866137932" Y="-26.008822951188" />
                  <Point X="4.722090691411" Y="-25.759928458529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.597514165038" Y="-24.336012246395" />
                  <Point X="4.560043870727" Y="-23.907724822622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.6538098432" Y="-26.069477550541" />
                  <Point X="4.624438600634" Y="-25.73376271181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.504335628004" Y="-24.360979452941" />
                  <Point X="4.465766892432" Y="-23.920136788095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.55733576281" Y="-26.056776524157" />
                  <Point X="4.526786509856" Y="-25.707596965091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.411157083047" Y="-24.385946568918" />
                  <Point X="4.371489914136" Y="-23.932548753569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.460861683389" Y="-26.044075508858" />
                  <Point X="4.429134419079" Y="-25.681431218373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.317978538089" Y="-24.410913684895" />
                  <Point X="4.277212935841" Y="-23.944960719042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.364387609055" Y="-26.031374551696" />
                  <Point X="4.331482328302" Y="-25.655265471654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.224799993131" Y="-24.435880800872" />
                  <Point X="4.182935957545" Y="-23.957372684516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.061911466644" Y="-22.574056423601" />
                  <Point X="4.043372073594" Y="-22.362150191376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.267913534721" Y="-26.018673594534" />
                  <Point X="4.233830237525" Y="-25.629099724935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.131621448174" Y="-24.460847916849" />
                  <Point X="4.088658979249" Y="-23.969784649989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.972547786159" Y="-22.642627640025" />
                  <Point X="3.935725983474" Y="-22.221752509461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.171439460386" Y="-26.005972637372" />
                  <Point X="4.136178146747" Y="-25.602933978216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.038442903216" Y="-24.485815032827" />
                  <Point X="3.994382000954" Y="-23.982196615463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.883184103428" Y="-22.711198830785" />
                  <Point X="3.8449484159" Y="-22.274162922508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.074965386052" Y="-25.993271680209" />
                  <Point X="4.038526068706" Y="-25.576768377065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.945264358258" Y="-24.510782148804" />
                  <Point X="3.90010501042" Y="-23.994608441055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.793820420152" Y="-22.779770015319" />
                  <Point X="3.754170848326" Y="-22.326573335554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.978491311718" Y="-25.980570723047" />
                  <Point X="3.940873999683" Y="-25.550602879005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.852085813301" Y="-24.535749264781" />
                  <Point X="3.805828003788" Y="-24.007020082643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.704456736877" Y="-22.848341199853" />
                  <Point X="3.663393280751" Y="-22.378983748601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.034678060306" Y="-27.712790956475" />
                  <Point X="4.023134345591" Y="-27.580845693514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.882017237384" Y="-25.967869765885" />
                  <Point X="3.843221930661" Y="-25.524437380946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.758907268343" Y="-24.560716380758" />
                  <Point X="3.711550997156" Y="-24.01943172423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.615093053602" Y="-22.916912384387" />
                  <Point X="3.572615713177" Y="-22.431394161648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.940392443467" Y="-27.725104182942" />
                  <Point X="3.920908823397" Y="-27.50240538649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.785543163049" Y="-25.955168808723" />
                  <Point X="3.745569861639" Y="-25.498271882886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.665910410774" Y="-24.587760193087" />
                  <Point X="3.617273990524" Y="-24.031843365818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.525729370326" Y="-22.985483568921" />
                  <Point X="3.481838145603" Y="-22.483804574694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.839956340572" Y="-27.667117032109" />
                  <Point X="3.818683301202" Y="-27.423965079465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.689069088715" Y="-25.94246785156" />
                  <Point X="3.647429996841" Y="-25.466530853603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.574844759233" Y="-24.636877791335" />
                  <Point X="3.522996983892" Y="-24.044255007405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.436365687051" Y="-23.054054753455" />
                  <Point X="3.391060578028" Y="-22.536214987741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.739520251359" Y="-27.609130037652" />
                  <Point X="3.716457779007" Y="-27.34552477244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592595014381" Y="-25.929766894398" />
                  <Point X="3.547090001627" Y="-25.409642218583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.485228895798" Y="-24.702566543451" />
                  <Point X="3.427774376268" Y="-24.045858380193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.347002003776" Y="-23.122625937989" />
                  <Point X="3.300283010454" Y="-22.588625400788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.639084187978" Y="-27.551143338469" />
                  <Point X="3.614232256812" Y="-27.267084465415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.496120940047" Y="-25.917065937236" />
                  <Point X="3.442894516708" Y="-25.30868513459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.399636177702" Y="-24.814240057217" />
                  <Point X="3.331214225868" Y="-24.032173569103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.2576383205" Y="-23.191197122523" />
                  <Point X="3.20950544288" Y="-22.641035813834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.538648124598" Y="-27.493156639286" />
                  <Point X="3.512006734617" Y="-27.18864415839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.399969584043" Y="-25.908053667471" />
                  <Point X="3.232322933042" Y="-23.991843678143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.168295127092" Y="-23.260002507313" />
                  <Point X="3.118727875305" Y="-22.693446226881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.438212061218" Y="-27.435169940103" />
                  <Point X="3.409781209737" Y="-27.110203820662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.305934421743" Y="-25.923229602423" />
                  <Point X="3.130715661778" Y="-23.920470011602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.081818834206" Y="-23.361576715009" />
                  <Point X="3.027950296606" Y="-22.74585651277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.337775997837" Y="-27.37718324092" />
                  <Point X="3.307555671985" Y="-27.031763335826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.212351088412" Y="-25.943569966122" />
                  <Point X="3.016445072799" Y="-23.704353961241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003865934306" Y="-23.560573750339" />
                  <Point X="2.937172714908" Y="-22.798266764383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.792288789032" Y="-21.142235913784" />
                  <Point X="2.786478768771" Y="-21.075827078331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.237339934457" Y="-27.319196541737" />
                  <Point X="3.205330134234" Y="-26.953322850989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.119558977125" Y="-25.972954039161" />
                  <Point X="2.84639513321" Y="-22.850677015995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.709475058172" Y="-21.285673397012" />
                  <Point X="2.684789043999" Y="-21.003510963864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.136903871076" Y="-27.261209842555" />
                  <Point X="3.103104596483" Y="-26.874882366152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.030409715824" Y="-26.043976078081" />
                  <Point X="2.755617551512" Y="-22.903087267608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.626661330358" Y="-21.429110915049" />
                  <Point X="2.583648310982" Y="-20.937469853879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.036467807696" Y="-27.203223143372" />
                  <Point X="2.99934979282" Y="-26.77896229196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.944125809963" Y="-26.147749279546" />
                  <Point X="2.664839969814" Y="-22.95549751922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.543847607206" Y="-21.57254848638" />
                  <Point X="2.483163528152" Y="-20.87892628884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.936031744316" Y="-27.145236444189" />
                  <Point X="2.888720071165" Y="-26.604461545551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868305427606" Y="-26.371121101923" />
                  <Point X="2.573146791931" Y="-22.997442458547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.461033884055" Y="-21.715986057711" />
                  <Point X="2.382678842193" Y="-20.820383831045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.835705250209" Y="-27.088502127537" />
                  <Point X="2.479819327513" Y="-23.020707417309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.378220160904" Y="-21.859423629042" />
                  <Point X="2.282194156235" Y="-20.76184137325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.73787800555" Y="-27.06033436279" />
                  <Point X="2.384509268066" Y="-23.021311211183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.295406437752" Y="-22.002861200373" />
                  <Point X="2.182429967978" Y="-20.711534241874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.640984111313" Y="-27.04283484218" />
                  <Point X="2.287512667532" Y="-23.002637752234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212592714601" Y="-22.146298771703" />
                  <Point X="2.082998818258" Y="-20.665033758386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.719525795551" Y="-29.030573159306" />
                  <Point X="2.716308806764" Y="-28.993802809209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544171930211" Y="-27.026269306982" />
                  <Point X="2.187182492437" Y="-22.945861361684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.129778991449" Y="-22.289736343034" />
                  <Point X="1.983567687443" Y="-20.618533490991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.611890715378" Y="-28.890301321654" />
                  <Point X="2.603914218718" Y="-28.799129547638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.449487355374" Y="-27.034022422661" />
                  <Point X="2.082252764819" Y="-22.836511845238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.047666827767" Y="-22.441192775799" />
                  <Point X="1.884136556629" Y="-20.572033223597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.504255605198" Y="-28.75002914102" />
                  <Point X="2.491519630672" Y="-28.604456286067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.357598865408" Y="-27.073734934665" />
                  <Point X="1.785402787743" Y="-20.533503839521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.396620435705" Y="-28.609756282445" />
                  <Point X="2.379125042627" Y="-28.409783024496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266433625652" Y="-27.121714234402" />
                  <Point X="1.686914581774" Y="-20.497781252434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.288985266213" Y="-28.469483423871" />
                  <Point X="2.266730493558" Y="-28.21511020843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.176052084164" Y="-27.178651246328" />
                  <Point X="1.588426445003" Y="-20.462059456275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.181350096721" Y="-28.329210565297" />
                  <Point X="2.154336006085" Y="-28.020438096415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.091425679844" Y="-27.301369777092" />
                  <Point X="1.489938308232" Y="-20.426337660117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.073714927229" Y="-28.188937706722" />
                  <Point X="2.037613862465" Y="-27.776300648285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01064528029" Y="-27.468048343498" />
                  <Point X="1.392124000573" Y="-20.398317765964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.966079757736" Y="-28.048664848148" />
                  <Point X="1.294703279515" Y="-20.374796587237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.858444588244" Y="-27.908391989574" />
                  <Point X="1.197282604762" Y="-20.351275937774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.754338544716" Y="-27.808457225354" />
                  <Point X="1.0998619519" Y="-20.327755538533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.653292116346" Y="-27.743494022411" />
                  <Point X="1.002441299038" Y="-20.304235139292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.552326770504" Y="-27.679457597007" />
                  <Point X="0.905020646177" Y="-20.280714740051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.454265276688" Y="-27.648612352145" />
                  <Point X="0.807844469494" Y="-20.25998871632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.35911600244" Y="-27.651053929263" />
                  <Point X="0.711599807735" Y="-20.249909956893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.264514661595" Y="-27.659758413832" />
                  <Point X="0.615355124145" Y="-20.239830947942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.169948940749" Y="-27.66887003685" />
                  <Point X="0.519110360015" Y="-20.229751018406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.077389764957" Y="-27.700916574791" />
                  <Point X="0.422865595885" Y="-20.21967108887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.987973155059" Y="-27.768882805252" />
                  <Point X="0.339670663729" Y="-20.358751421338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.899077002032" Y="-27.842797884979" />
                  <Point X="0.267780763887" Y="-20.627048864446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812005278725" Y="-27.937566291827" />
                  <Point X="0.189931559446" Y="-20.827231144291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.817990852202" Y="-29.09598446807" />
                  <Point X="0.802774403326" Y="-28.922059661552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.739872930425" Y="-28.203092536372" />
                  <Point X="0.100456769077" Y="-20.894532368937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.6763952241" Y="-28.567541791361" />
                  <Point X="0.006874659372" Y="-20.91488671875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.559903207177" Y="-28.326034703421" />
                  <Point X="-0.090352736376" Y="-20.893575258422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.450917174562" Y="-28.170321408698" />
                  <Point X="-0.193503713891" Y="-20.80455694868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.348754511503" Y="-28.092599584877" />
                  <Point X="-0.328117168252" Y="-20.355920883016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.250546304399" Y="-28.060077399459" />
                  <Point X="-0.434882186515" Y="-20.22559389851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.152521777445" Y="-28.029654687764" />
                  <Point X="-0.529278469593" Y="-20.236642204079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.054721765624" Y="-28.001798195772" />
                  <Point X="-0.623674752671" Y="-20.247690509647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.040664463524" Y="-28.001531365987" />
                  <Point X="-0.71807103575" Y="-20.258738815216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.133742675934" Y="-28.027645288232" />
                  <Point X="-0.812467318828" Y="-20.269787120784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.226584553675" Y="-28.056460528102" />
                  <Point X="-0.906863601906" Y="-20.280835426352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.319418033733" Y="-28.085371753931" />
                  <Point X="-1.000282347847" Y="-20.303057032528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.409955491193" Y="-28.140526638145" />
                  <Point X="-1.09336219132" Y="-20.329152311637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.495637428801" Y="-28.251180368217" />
                  <Point X="-1.18644199952" Y="-20.355247993913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.580325057996" Y="-28.373199095461" />
                  <Point X="-1.27952175022" Y="-20.381344333432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.660013897866" Y="-28.552354246146" />
                  <Point X="-1.372601500919" Y="-20.407440672951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.731903810751" Y="-28.820651540162" />
                  <Point X="-1.465681251618" Y="-20.43353701247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.80379373454" Y="-29.088948709558" />
                  <Point X="-1.527964354481" Y="-20.811640647501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.875683668811" Y="-29.357245759134" />
                  <Point X="-1.615729852401" Y="-20.898479174242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.947573603082" Y="-29.625542808709" />
                  <Point X="-1.70751486651" Y="-20.939374420705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.031357522085" Y="-29.757890990712" />
                  <Point X="-1.800346224473" Y="-20.968309902184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128763527043" Y="-29.734538017782" />
                  <Point X="-1.133915768545" Y="-29.675647627935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.170018040546" Y="-29.262996770721" />
                  <Point X="-1.896276304641" Y="-20.961826826798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.285000941763" Y="-29.038738954223" />
                  <Point X="-1.99551764922" Y="-20.917495825999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.388288681153" Y="-28.948157449102" />
                  <Point X="-2.096635158163" Y="-20.851720168399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.491576333858" Y="-28.857576934794" />
                  <Point X="-2.202035644358" Y="-20.736989856791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.591501363903" Y="-28.805431373355" />
                  <Point X="-2.292978114515" Y="-20.787515424695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.687967842662" Y="-28.792817234024" />
                  <Point X="-2.383920569679" Y="-20.838041163964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.783880709124" Y="-28.786530912189" />
                  <Point X="-2.474863024844" Y="-20.888566903233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.879793573525" Y="-28.780244613923" />
                  <Point X="-2.565805480008" Y="-20.939092642502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.975400111861" Y="-28.777459638592" />
                  <Point X="-2.656602902808" Y="-20.991276109284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.068593551774" Y="-28.802256504451" />
                  <Point X="-2.745971331256" Y="-21.059793056243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.159051791756" Y="-28.858316848578" />
                  <Point X="-2.75601106678" Y="-22.035041112431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777180903801" Y="-21.79306876804" />
                  <Point X="-2.835339738104" Y="-21.128310250087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.249147824799" Y="-28.918517236966" />
                  <Point X="-2.352044548052" Y="-27.742402308395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.378916689509" Y="-27.435252326063" />
                  <Point X="-2.839758754785" Y="-22.167803416639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890007449009" Y="-21.593458213515" />
                  <Point X="-2.92470805379" Y="-21.19682848593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.339243923433" Y="-28.978716875646" />
                  <Point X="-2.4348582571" Y="-27.885840040929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.480417438931" Y="-27.365096209732" />
                  <Point X="-2.928467310113" Y="-22.243862747879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.002401941036" Y="-21.398786049443" />
                  <Point X="-3.014076369476" Y="-21.265346721773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.427195727443" Y="-29.063425914023" />
                  <Point X="-2.517671966149" Y="-28.029277773462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.577195114002" Y="-27.348925080273" />
                  <Point X="-3.021149520115" Y="-22.274502998364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.514504976958" Y="-29.155479383891" />
                  <Point X="-2.600485675197" Y="-28.172715505995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.670582160939" Y="-27.371509007717" />
                  <Point X="-3.116713928002" Y="-22.272199576261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.615329568302" Y="-29.093051789768" />
                  <Point X="-2.683299384245" Y="-28.316153238529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761516416493" Y="-27.422128468967" />
                  <Point X="-3.213570992019" Y="-22.255121026996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.716154198907" Y="-29.030623746874" />
                  <Point X="-2.766113093293" Y="-28.459590971062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.852293991797" Y="-27.474538793658" />
                  <Point X="-2.948913707745" Y="-26.370170386892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.955351371908" Y="-26.296587548809" />
                  <Point X="-3.31346223243" Y="-22.203361682855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.817960256834" Y="-28.956977938371" />
                  <Point X="-2.848926802341" Y="-28.603028703596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.943071567101" Y="-27.52694911835" />
                  <Point X="-3.028159574259" Y="-26.554388746204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.063746721365" Y="-26.147625793477" />
                  <Point X="-3.413898322514" Y="-22.145374678451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.920211110215" Y="-28.878248094569" />
                  <Point X="-2.93174045268" Y="-28.746467107171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.033849142406" Y="-27.579359443041" />
                  <Point X="-3.117180715031" Y="-26.626875209464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.162731291331" Y="-26.106229739939" />
                  <Point X="-3.514334412597" Y="-22.087387674046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.124626712581" Y="-27.631769826363" />
                  <Point X="-3.206544412352" Y="-26.695446233461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.25845919448" Y="-26.102057558453" />
                  <Point X="-3.338814472575" Y="-25.183592527014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.368982392331" Y="-24.838771626343" />
                  <Point X="-3.473194823015" Y="-23.64761809302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506387883096" Y="-23.26821968021" />
                  <Point X="-3.614770502681" Y="-22.029400669641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.215404275486" Y="-27.684180292777" />
                  <Point X="-3.295908109672" Y="-26.764017257457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.352736206437" Y="-26.114469139171" />
                  <Point X="-3.426395474533" Y="-25.272539852254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.470791678821" Y="-24.765088915192" />
                  <Point X="-3.560411724798" Y="-23.740727102295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.609465480188" Y="-23.18004011254" />
                  <Point X="-3.715206622382" Y="-21.971413326715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.306181838391" Y="-27.736590759191" />
                  <Point X="-3.385271806993" Y="-26.832588281454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.447013218394" Y="-26.12688071989" />
                  <Point X="-3.51806170631" Y="-25.314792786979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568860792557" Y="-24.734156574239" />
                  <Point X="-3.652480867973" Y="-23.778374738679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.711691032522" Y="-23.101599461025" />
                  <Point X="-3.802560204822" Y="-22.062960068929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.396959401297" Y="-27.789001225605" />
                  <Point X="-3.474635504313" Y="-26.90115930545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.541290230351" Y="-26.139292300609" />
                  <Point X="-3.611240255894" Y="-25.339759850074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.666512868872" Y="-24.707990992826" />
                  <Point X="-3.746335754141" Y="-23.795611239241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.813916582177" Y="-23.023158840127" />
                  <Point X="-3.888283139289" Y="-22.17314520276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.487736964202" Y="-27.841411692019" />
                  <Point X="-3.563999201634" Y="-26.969730329446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.635567242309" Y="-26.151703881328" />
                  <Point X="-3.704418805479" Y="-25.364726913168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.764164945187" Y="-24.681825411412" />
                  <Point X="-3.842547773514" Y="-23.785905583994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.916142083127" Y="-22.944718775931" />
                  <Point X="-3.974006073757" Y="-22.283330336591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.578514527107" Y="-27.893822158433" />
                  <Point X="-3.653362898954" Y="-27.038301353443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.729844254266" Y="-26.164115462047" />
                  <Point X="-3.797597355063" Y="-25.389693976263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.861817021502" Y="-24.655659829998" />
                  <Point X="-3.939021840226" Y="-23.773204713956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.018367584077" Y="-22.866278711734" />
                  <Point X="-4.057859320527" Y="-22.41488609859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.669292090013" Y="-27.946232624847" />
                  <Point X="-3.742726596275" Y="-27.106872377439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.824121254918" Y="-26.176527171981" />
                  <Point X="-3.890775904647" Y="-25.414661039357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.959469097817" Y="-24.629494248584" />
                  <Point X="-4.035495906938" Y="-23.760503843917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.120593085028" Y="-22.787838647537" />
                  <Point X="-4.140791538069" Y="-22.556969272843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.760069652918" Y="-27.998643091261" />
                  <Point X="-3.832090293596" Y="-27.175443401435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.918398252539" Y="-26.188938916569" />
                  <Point X="-3.983954454232" Y="-25.439628102451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.057121197683" Y="-24.603328397978" />
                  <Point X="-4.13196997365" Y="-23.747802973879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.222818585978" Y="-22.70939858334" />
                  <Point X="-4.223723688677" Y="-22.699053212149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.861644488397" Y="-27.927640167434" />
                  <Point X="-3.921453990916" Y="-27.244014425432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.012675250159" Y="-26.201350661158" />
                  <Point X="-4.077133003816" Y="-25.464595165546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.154773300106" Y="-24.577162518146" />
                  <Point X="-4.228444040362" Y="-23.735102103841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.96939210905" Y="-27.786081986202" />
                  <Point X="-4.010817659174" Y="-27.312585781618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.10695224778" Y="-26.213762405747" />
                  <Point X="-4.1703115534" Y="-25.48956222864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.252425402529" Y="-24.550996638314" />
                  <Point X="-4.324918107074" Y="-23.722401233803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079861584529" Y="-27.613412861956" />
                  <Point X="-4.100181316315" Y="-27.381157264867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.2012292454" Y="-26.226174150335" />
                  <Point X="-4.263490102985" Y="-25.514529291735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.350077504952" Y="-24.524830758481" />
                  <Point X="-4.421392173786" Y="-23.709700363765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.295506243021" Y="-26.238585894924" />
                  <Point X="-4.356668652569" Y="-25.539496354829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.447729607376" Y="-24.498664878649" />
                  <Point X="-4.517866246069" Y="-23.696999430041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.389783240642" Y="-26.250997639513" />
                  <Point X="-4.449847202153" Y="-25.564463417924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.545381709799" Y="-24.472498998816" />
                  <Point X="-4.614340395833" Y="-23.684297610715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.484060238262" Y="-26.263409384101" />
                  <Point X="-4.543025744056" Y="-25.589430568821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.643033812222" Y="-24.446333118984" />
                  <Point X="-4.691293573182" Y="-23.894721527086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.578337235883" Y="-26.27582112869" />
                  <Point X="-4.63620423255" Y="-25.614398330177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.740685914645" Y="-24.420167239151" />
                  <Point X="-4.757976269513" Y="-24.222537578682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678910895081" Y="-26.216261702113" />
                  <Point X="-4.729382721044" Y="-25.639366091534" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998375" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.74773034668" Y="-29.567873046875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318969727" Y="-28.521544921875" />
                  <Point X="0.351432556152" Y="-28.3588984375" />
                  <Point X="0.300591339111" Y="-28.28564453125" />
                  <Point X="0.27433605957" Y="-28.266400390625" />
                  <Point X="0.099651023865" Y="-28.212185546875" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="-0.008663995743" Y="-28.187765625" />
                  <Point X="-0.183349029541" Y="-28.241982421875" />
                  <Point X="-0.262022979736" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.401165435791" Y="-28.448291015625" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.58361706543" Y="-29.001341796875" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-1.008643981934" Y="-29.955845703125" />
                  <Point X="-1.10023046875" Y="-29.938068359375" />
                  <Point X="-1.302663208008" Y="-29.885984375" />
                  <Point X="-1.351589599609" Y="-29.873396484375" />
                  <Point X="-1.342397216797" Y="-29.80357421875" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.351415527344" Y="-29.32495703125" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282958984" Y="-29.20262890625" />
                  <Point X="-1.547110717773" Y="-29.0615859375" />
                  <Point X="-1.619543701172" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.8626953125" Y="-28.9717734375" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878295898" Y="-28.973791015625" />
                  <Point X="-2.167739990234" Y="-29.092634765625" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.329569335938" Y="-29.248306640625" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.721567138672" Y="-29.25074609375" />
                  <Point X="-2.855837890625" Y="-29.167609375" />
                  <Point X="-3.136122070312" Y="-28.951798828125" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-3.019304199219" Y="-28.518130859375" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.597591796875" />
                  <Point X="-2.513981201172" Y="-27.56876171875" />
                  <Point X="-2.531329833984" Y="-27.5514140625" />
                  <Point X="-2.560158447266" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.992072509766" Y="-27.774634765625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.055625" Y="-27.98649609375" />
                  <Point X="-4.161703613281" Y="-27.84712890625" />
                  <Point X="-4.3626484375" Y="-27.51017578125" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-4.061843505859" Y="-27.112248046875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822509766" Y="-26.396017578125" />
                  <Point X="-3.140510498047" Y="-26.375505859375" />
                  <Point X="-3.138117919922" Y="-26.36626953125" />
                  <Point X="-3.140326171875" Y="-26.33459765625" />
                  <Point X="-3.161159423828" Y="-26.310638671875" />
                  <Point X="-3.179418701172" Y="-26.299892578125" />
                  <Point X="-3.187642089844" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.724982421875" Y="-26.355115234375" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.885904785156" Y="-26.1736171875" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.980558105469" Y="-25.63946875" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.580485839844" Y="-25.402763671875" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.5418984375" Y="-25.121427734375" />
                  <Point X="-3.522763427734" Y="-25.108146484375" />
                  <Point X="-3.514145507812" Y="-25.102166015625" />
                  <Point X="-3.494898681641" Y="-25.075908203125" />
                  <Point X="-3.488520263672" Y="-25.055357421875" />
                  <Point X="-3.485647705078" Y="-25.0461015625" />
                  <Point X="-3.485647705078" Y="-25.0164609375" />
                  <Point X="-3.492026123047" Y="-24.995908203125" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.514145019531" Y="-24.96039453125" />
                  <Point X="-3.533280029297" Y="-24.947115234375" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.018208251953" Y="-24.810458984375" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.9443828125" Y="-24.1842734375" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.810626464844" Y="-23.6086484375" />
                  <Point X="-4.773516113281" Y="-23.471701171875" />
                  <Point X="-4.486593261719" Y="-23.5094765625" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731705566406" Y="-23.6041328125" />
                  <Point X="-3.689353759766" Y="-23.590779296875" />
                  <Point X="-3.670279541016" Y="-23.584765625" />
                  <Point X="-3.65153515625" Y="-23.5739453125" />
                  <Point X="-3.639120361328" Y="-23.55621484375" />
                  <Point X="-3.622126464844" Y="-23.5151875" />
                  <Point X="-3.614472900391" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.475396484375" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.636820800781" Y="-23.415099609375" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.924252441406" Y="-23.177986328125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.263908691406" Y="-22.390990234375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.876523925781" Y="-21.848607421875" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.609546142578" Y="-21.8130234375" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514892578" Y="-22.07956640625" />
                  <Point X="-3.079530517578" Y="-22.0847265625" />
                  <Point X="-3.052965576172" Y="-22.08705078125" />
                  <Point X="-3.031507080078" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.971385498047" Y="-22.030728515625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.943234619141" Y="-21.91317578125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.0691796875" Y="-21.663123046875" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-2.933825683594" Y="-20.964400390625" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.306387207031" Y="-20.577609375" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.105704589844" Y="-20.532740234375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247802734" Y="-20.726337890625" />
                  <Point X="-1.885598510742" Y="-20.760513671875" />
                  <Point X="-1.856031860352" Y="-20.77590625" />
                  <Point X="-1.835125" Y="-20.781509765625" />
                  <Point X="-1.813809204102" Y="-20.77775" />
                  <Point X="-1.745431152344" Y="-20.74942578125" />
                  <Point X="-1.714635253906" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.663827514648" Y="-20.63492578125" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.665232055664" Y="-20.48044140625" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.202345092773" Y="-20.162380859375" />
                  <Point X="-0.968082946777" Y="-20.096703125" />
                  <Point X="-0.426813598633" Y="-20.033353515625" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.175377838135" Y="-20.19184765625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282119751" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594039917" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.114459243774" Y="-20.4651484375" />
                  <Point X="0.236648406982" Y="-20.0091328125" />
                  <Point X="0.655669128418" Y="-20.053015625" />
                  <Point X="0.860209655762" Y="-20.074435546875" />
                  <Point X="1.308020874023" Y="-20.18255078125" />
                  <Point X="1.508455322266" Y="-20.230943359375" />
                  <Point X="1.800453491211" Y="-20.3368515625" />
                  <Point X="1.931044555664" Y="-20.38421875" />
                  <Point X="2.212884765625" Y="-20.516025390625" />
                  <Point X="2.338685302734" Y="-20.574859375" />
                  <Point X="2.610977783203" Y="-20.73349609375" />
                  <Point X="2.732521728516" Y="-20.80430859375" />
                  <Point X="2.989319335938" Y="-20.9869296875" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.823925537109" Y="-21.467439453125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.210049316406" Y="-22.563841796875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.207816650391" Y="-22.65554296875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.24830078125" Y="-22.742837890625" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.274939208984" Y="-22.775794921875" />
                  <Point X="2.318588867188" Y="-22.8054140625" />
                  <Point X="2.338247802734" Y="-22.81875390625" />
                  <Point X="2.360336914062" Y="-22.827021484375" />
                  <Point X="2.408203613281" Y="-22.83279296875" />
                  <Point X="2.42976171875" Y="-22.835392578125" />
                  <Point X="2.448666259766" Y="-22.8340546875" />
                  <Point X="2.504021728516" Y="-22.819251953125" />
                  <Point X="2.528952636719" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.004454101562" Y="-22.54002734375" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.130495117188" Y="-22.157921875" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.345751464844" Y="-22.494693359375" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.070767578125" Y="-22.806751953125" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279370849609" Y="-23.41616796875" />
                  <Point X="3.23953125" Y="-23.468140625" />
                  <Point X="3.221588623047" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.508501953125" />
                  <Point X="3.198279052734" Y="-23.56156640625" />
                  <Point X="3.191595458984" Y="-23.585466796875" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.202961914062" Y="-23.668076171875" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646972656" Y="-23.712046875" />
                  <Point X="3.248781494141" Y="-23.76241015625" />
                  <Point X="3.263704589844" Y="-23.785091796875" />
                  <Point X="3.280947265625" Y="-23.8011796875" />
                  <Point X="3.328963623047" Y="-23.828208984375" />
                  <Point X="3.350589355469" Y="-23.8403828125" />
                  <Point X="3.368567138672" Y="-23.846380859375" />
                  <Point X="3.433488769531" Y="-23.8549609375" />
                  <Point X="3.462727783203" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.916021972656" Y="-23.800873046875" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.908223144531" Y="-23.921419921875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.984302246094" Y="-24.338373046875" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.637530273438" Y="-24.521994140625" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729088134766" Y="-24.767181640625" />
                  <Point X="3.6653046875" Y="-24.804048828125" />
                  <Point X="3.636578125" Y="-24.82065234375" />
                  <Point X="3.622264648438" Y="-24.83307421875" />
                  <Point X="3.583994628906" Y="-24.88183984375" />
                  <Point X="3.566758789062" Y="-24.903802734375" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.544228271484" Y="-24.991876953125" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538482910156" Y="-25.040685546875" />
                  <Point X="3.551239746094" Y="-25.107294921875" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.605028808594" Y="-25.2075234375" />
                  <Point X="3.622264648438" Y="-25.229486328125" />
                  <Point X="3.636575927734" Y="-25.241908203125" />
                  <Point X="3.700359375" Y="-25.278775390625" />
                  <Point X="3.7290859375" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.144867675781" Y="-25.40855859375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.965721191406" Y="-25.85172265625" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.890633789062" Y="-26.219677734375" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.451510253906" Y="-26.234484375" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836425781" Y="-26.098341796875" />
                  <Point X="3.269652099609" Y="-26.12555078125" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.185445800781" Y="-26.154697265625" />
                  <Point X="3.109779785156" Y="-26.245701171875" />
                  <Point X="3.075701660156" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.053513183594" Y="-26.431923828125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.125639404297" Y="-26.624380859375" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.543096923828" Y="-26.973009765625" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.252868652344" Y="-27.72327734375" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.084602050781" Y="-27.9719765625" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="3.679294677734" Y="-27.79375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.73733984375" Y="-27.2533125" />
                  <Point X="2.588350585938" Y="-27.226404296875" />
                  <Point X="2.521249511719" Y="-27.214287109375" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.365304443359" Y="-27.28438671875" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.223458740234" Y="-27.45845703125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.2160703125" Y="-27.695365234375" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.474833007813" Y="-28.1955546875" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.893131103516" Y="-29.148904296875" />
                  <Point X="2.835297851563" Y="-29.190212890625" />
                  <Point X="2.701655761719" Y="-29.276716796875" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="2.389678955078" Y="-28.912818359375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670548950195" Y="-27.980466796875" />
                  <Point X="1.52360534668" Y="-27.88599609375" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.265097045898" Y="-27.850505859375" />
                  <Point X="1.192718139648" Y="-27.857166015625" />
                  <Point X="1.165332519531" Y="-27.868509765625" />
                  <Point X="1.041238037109" Y="-27.97169140625" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.931353149414" Y="-28.216693359375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.982153747559" Y="-28.82893359375" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.04905847168" Y="-29.951255859375" />
                  <Point X="0.99436730957" Y="-29.9632421875" />
                  <Point X="0.87088873291" Y="-29.98567578125" />
                  <Point X="0.860200744629" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#186" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.133237562358" Y="4.852373851146" Z="1.75" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.75" />
                  <Point X="-0.438781643995" Y="5.047586503057" Z="1.75" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.75" />
                  <Point X="-1.221998719308" Y="4.917044297182" Z="1.75" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.75" />
                  <Point X="-1.720960238234" Y="4.544313154938" Z="1.75" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.75" />
                  <Point X="-1.717669363323" Y="4.411390252634" Z="1.75" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.75" />
                  <Point X="-1.770720984793" Y="4.328047938753" Z="1.75" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.75" />
                  <Point X="-1.868665913927" Y="4.31511622377" Z="1.75" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.75" />
                  <Point X="-2.072192926071" Y="4.528977152677" Z="1.75" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.75" />
                  <Point X="-2.336826043959" Y="4.497378593855" Z="1.75" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.75" />
                  <Point X="-2.97040389209" Y="4.10655901722" Z="1.75" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.75" />
                  <Point X="-3.118636982412" Y="3.343157344521" Z="1.75" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.75" />
                  <Point X="-2.99920051867" Y="3.113747815525" Z="1.75" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.75" />
                  <Point X="-3.012896106299" Y="3.035907461587" Z="1.75" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.75" />
                  <Point X="-3.081328650081" Y="2.996364180503" Z="1.75" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.75" />
                  <Point X="-3.590702003777" Y="3.261556966333" Z="1.75" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.75" />
                  <Point X="-3.92214327684" Y="3.213376138189" Z="1.75" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.75" />
                  <Point X="-4.313246869247" Y="2.665495621751" Z="1.75" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.75" />
                  <Point X="-3.960846408983" Y="1.813626435788" Z="1.75" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.75" />
                  <Point X="-3.687327362472" Y="1.593093988282" Z="1.75" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.75" />
                  <Point X="-3.674476056207" Y="1.535226899985" Z="1.75" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.75" />
                  <Point X="-3.710544181661" Y="1.488186020896" Z="1.75" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.75" />
                  <Point X="-4.486223036353" Y="1.571376890567" Z="1.75" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.75" />
                  <Point X="-4.865041566031" Y="1.435709747809" Z="1.75" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.75" />
                  <Point X="-5.000000433484" Y="0.85432456427" Z="1.75" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.75" />
                  <Point X="-4.03730521149" Y="0.172525144176" Z="1.75" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.75" />
                  <Point X="-3.567942812324" Y="0.043087701108" Z="1.75" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.75" />
                  <Point X="-3.545935168938" Y="0.020551188964" Z="1.75" />
                  <Point X="-3.539556741714" Y="0" Z="1.75" />
                  <Point X="-3.542429432197" Y="-0.009255762097" Z="1.75" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.75" />
                  <Point X="-3.557425782799" Y="-0.035788282026" Z="1.75" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.75" />
                  <Point X="-4.599582577282" Y="-0.32318691258" Z="1.75" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.75" />
                  <Point X="-5.036210134611" Y="-0.615265974791" Z="1.75" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.75" />
                  <Point X="-4.940505535415" Y="-1.154710719993" Z="1.75" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.75" />
                  <Point X="-3.724611969136" Y="-1.373407455751" Z="1.75" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.75" />
                  <Point X="-3.210935420825" Y="-1.311703282798" Z="1.75" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.75" />
                  <Point X="-3.195068733814" Y="-1.331687063162" Z="1.75" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.75" />
                  <Point X="-4.098437448361" Y="-2.041300242537" Z="1.75" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.75" />
                  <Point X="-4.41174772234" Y="-2.504505184164" Z="1.75" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.75" />
                  <Point X="-4.101567165492" Y="-2.985497953095" Z="1.75" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.75" />
                  <Point X="-2.973228452614" Y="-2.786655637828" Z="1.75" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.75" />
                  <Point X="-2.567452370976" Y="-2.560878098155" Z="1.75" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.75" />
                  <Point X="-3.06876137534" Y="-3.461849929878" Z="1.75" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.75" />
                  <Point X="-3.172781984856" Y="-3.960135573622" Z="1.75" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.75" />
                  <Point X="-2.754044553842" Y="-4.261976816494" Z="1.75" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.75" />
                  <Point X="-2.296057632552" Y="-4.247463354827" Z="1.75" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.75" />
                  <Point X="-2.14611765573" Y="-4.10292790368" Z="1.75" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.75" />
                  <Point X="-1.872121256458" Y="-3.990385378377" Z="1.75" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.75" />
                  <Point X="-1.586233841322" Y="-4.067898064132" Z="1.75" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.75" />
                  <Point X="-1.406610778674" Y="-4.303430271488" Z="1.75" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.75" />
                  <Point X="-1.398125441968" Y="-4.765767450447" Z="1.75" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.75" />
                  <Point X="-1.321278084214" Y="-4.903127986571" Z="1.75" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.75" />
                  <Point X="-1.024327429229" Y="-4.973649643129" Z="1.75" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.75" />
                  <Point X="-0.541476732207" Y="-3.98300273342" Z="1.75" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.75" />
                  <Point X="-0.366245463111" Y="-3.445520434067" Z="1.75" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.75" />
                  <Point X="-0.174685102981" Y="-3.258455177922" Z="1.75" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.75" />
                  <Point X="0.07867397638" Y="-3.228656867366" Z="1.75" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.75" />
                  <Point X="0.304200396321" Y="-3.356125321701" Z="1.75" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.75" />
                  <Point X="0.693278128194" Y="-4.549533296396" Z="1.75" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.75" />
                  <Point X="0.873668553797" Y="-5.003589708679" Z="1.75" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.75" />
                  <Point X="1.053607180462" Y="-4.968814541883" Z="1.75" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.75" />
                  <Point X="1.025570054173" Y="-3.791128415499" Z="1.75" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.75" />
                  <Point X="0.9740563735" Y="-3.196031873522" Z="1.75" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.75" />
                  <Point X="1.067049215526" Y="-2.978855976917" Z="1.75" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.75" />
                  <Point X="1.263522676685" Y="-2.86901506527" Z="1.75" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.75" />
                  <Point X="1.490410316909" Y="-2.896774124765" Z="1.75" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.75" />
                  <Point X="2.343855369905" Y="-3.911975727225" Z="1.75" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.75" />
                  <Point X="2.722669235403" Y="-4.287410801727" Z="1.75" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.75" />
                  <Point X="2.916036764197" Y="-4.158310829063" Z="1.75" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.75" />
                  <Point X="2.511978309192" Y="-3.139274805866" Z="1.75" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.75" />
                  <Point X="2.259118469261" Y="-2.655197438016" Z="1.75" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.75" />
                  <Point X="2.261549099374" Y="-2.450463608503" Z="1.75" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.75" />
                  <Point X="2.382434769869" Y="-2.297352210593" Z="1.75" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.75" />
                  <Point X="2.573309365497" Y="-2.24432954023" Z="1.75" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.75" />
                  <Point X="3.648138791241" Y="-2.805771191083" Z="1.75" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.75" />
                  <Point X="4.119334657233" Y="-2.969473903586" Z="1.75" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.75" />
                  <Point X="4.289246511857" Y="-2.718281999551" Z="1.75" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.75" />
                  <Point X="3.567378917672" Y="-1.902061452437" Z="1.75" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.75" />
                  <Point X="3.161541313283" Y="-1.566061128812" Z="1.75" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.75" />
                  <Point X="3.097146717003" Y="-1.405224614199" Z="1.75" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.75" />
                  <Point X="3.142069582573" Y="-1.246386825945" Z="1.75" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.75" />
                  <Point X="3.274115540781" Y="-1.143129754119" Z="1.75" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.75" />
                  <Point X="4.438828154631" Y="-1.252776910431" Z="1.75" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.75" />
                  <Point X="4.933224760928" Y="-1.199522842931" Z="1.75" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.75" />
                  <Point X="5.009006798996" Y="-0.827895229634" Z="1.75" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.75" />
                  <Point X="4.151652777129" Y="-0.328981741653" Z="1.75" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.75" />
                  <Point X="3.71922616501" Y="-0.204206184939" Z="1.75" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.75" />
                  <Point X="3.6382067939" Y="-0.145375593918" Z="1.75" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.75" />
                  <Point X="3.594191416779" Y="-0.066610680143" Z="1.75" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.75" />
                  <Point X="3.587180033645" Y="0.029999851053" Z="1.75" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.75" />
                  <Point X="3.6171726445" Y="0.118573144657" Z="1.75" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.75" />
                  <Point X="3.684169249342" Y="0.183942659303" Z="1.75" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.75" />
                  <Point X="4.644315040178" Y="0.460990191116" Z="1.75" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.75" />
                  <Point X="5.027550918197" Y="0.700599508361" Z="1.75" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.75" />
                  <Point X="4.950648328322" Y="1.121687517752" Z="1.75" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.75" />
                  <Point X="3.903339575496" Y="1.279979921397" Z="1.75" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.75" />
                  <Point X="3.433882263975" Y="1.225888393029" Z="1.75" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.75" />
                  <Point X="3.347335112815" Y="1.246641866184" Z="1.75" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.75" />
                  <Point X="3.284395546111" Y="1.296353344029" Z="1.75" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.75" />
                  <Point X="3.245774365821" Y="1.373307410625" Z="1.75" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.75" />
                  <Point X="3.24027572885" Y="1.456248498774" Z="1.75" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.75" />
                  <Point X="3.273058714971" Y="1.532721406608" Z="1.75" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.75" />
                  <Point X="4.095049632964" Y="2.184861014768" Z="1.75" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.75" />
                  <Point X="4.382372726462" Y="2.562473965047" Z="1.75" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.75" />
                  <Point X="4.164924371116" Y="2.90256162498" Z="1.75" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.75" />
                  <Point X="2.973298154389" Y="2.534554549576" Z="1.75" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.75" />
                  <Point X="2.484947139353" Y="2.260331952278" Z="1.75" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.75" />
                  <Point X="2.408033595093" Y="2.248128677619" Z="1.75" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.75" />
                  <Point X="2.340507937013" Y="2.267239859768" Z="1.75" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.75" />
                  <Point X="2.283518740467" Y="2.316516923369" Z="1.75" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.75" />
                  <Point X="2.251301025139" Y="2.381724848739" Z="1.75" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.75" />
                  <Point X="2.25219599038" Y="2.454522436529" Z="1.75" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.75" />
                  <Point X="2.861070803283" Y="3.538840927566" Z="1.75" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.75" />
                  <Point X="3.012140251936" Y="4.085099806441" Z="1.75" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.75" />
                  <Point X="2.629991693364" Y="4.340986967909" Z="1.75" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.75" />
                  <Point X="2.227910373406" Y="4.56054566918" Z="1.75" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.75" />
                  <Point X="1.811346468538" Y="4.741432192479" Z="1.75" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.75" />
                  <Point X="1.313599580168" Y="4.897332788331" Z="1.75" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.75" />
                  <Point X="0.654720830782" Y="5.027994142017" Z="1.75" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.75" />
                  <Point X="0.060006999195" Y="4.579073654805" Z="1.75" />
                  <Point X="0" Y="4.355124473572" Z="1.75" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>