<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#117" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="270" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.995282226562" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.766447265625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563502197266" Y="-28.513271484375" />
                  <Point X="0.558131713867" Y="-28.498345703125" />
                  <Point X="0.542240600586" Y="-28.467201171875" />
                  <Point X="0.3786355896" Y="-28.2314765625" />
                  <Point X="0.356650421143" Y="-28.2089453125" />
                  <Point X="0.330268341064" Y="-28.189662109375" />
                  <Point X="0.302368499756" Y="-28.17562890625" />
                  <Point X="0.049136127472" Y="-28.09703515625" />
                  <Point X="0.020852109909" Y="-28.092765625" />
                  <Point X="-0.008916111946" Y="-28.0928046875" />
                  <Point X="-0.036950737" Y="-28.09707421875" />
                  <Point X="-0.290183105469" Y="-28.17566796875" />
                  <Point X="-0.311769073486" Y="-28.18546484375" />
                  <Point X="-0.332859436035" Y="-28.198427734375" />
                  <Point X="-0.356658660889" Y="-28.2192265625" />
                  <Point X="-0.366476318359" Y="-28.231697265625" />
                  <Point X="-0.530050964355" Y="-28.467375" />
                  <Point X="-0.538189025879" Y="-28.481572265625" />
                  <Point X="-0.550989990234" Y="-28.5125234375" />
                  <Point X="-0.916584472656" Y="-29.87694140625" />
                  <Point X="-1.079336181641" Y="-29.8453515625" />
                  <Point X="-1.24641809082" Y="-29.802361328125" />
                  <Point X="-1.214994018555" Y="-29.563673828125" />
                  <Point X="-1.214229858398" Y="-29.548240234375" />
                  <Point X="-1.21653894043" Y="-29.51607421875" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.284377563477" Y="-29.189751953125" />
                  <Point X="-1.294839111328" Y="-29.167697265625" />
                  <Point X="-1.312682739258" Y="-29.142060546875" />
                  <Point X="-1.323866699219" Y="-29.13101171875" />
                  <Point X="-1.556905273438" Y="-28.926640625" />
                  <Point X="-1.583358764648" Y="-28.9102265625" />
                  <Point X="-1.613247314453" Y="-28.8979140625" />
                  <Point X="-1.64321875" Y="-28.890955078125" />
                  <Point X="-1.952616699219" Y="-28.87067578125" />
                  <Point X="-1.983555786133" Y="-28.87374609375" />
                  <Point X="-2.014733398438" Y="-28.882150390625" />
                  <Point X="-2.042786621094" Y="-28.89488671875" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.480147460938" Y="-29.28848828125" />
                  <Point X="-2.801709472656" Y="-29.08938671875" />
                  <Point X="-3.104721435547" Y="-28.856078125" />
                  <Point X="-2.424133544922" Y="-27.677265625" />
                  <Point X="-2.422786621094" Y="-27.67487890625" />
                  <Point X="-2.412240478516" Y="-27.64577734375" />
                  <Point X="-2.406430664062" Y="-27.614560546875" />
                  <Point X="-2.405744873047" Y="-27.58400390625" />
                  <Point X="-2.414797363281" Y="-27.554810546875" />
                  <Point X="-2.428958984375" Y="-27.526388671875" />
                  <Point X="-2.446813476563" Y="-27.501580078125" />
                  <Point X="-2.464153320312" Y="-27.484240234375" />
                  <Point X="-2.489311035156" Y="-27.466212890625" />
                  <Point X="-2.518139892578" Y="-27.45199609375" />
                  <Point X="-2.547758056641" Y="-27.44301171875" />
                  <Point X="-2.578692138672" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.8180234375" Y="-28.141802734375" />
                  <Point X="-4.082863037109" Y="-27.793857421875" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.106666503906" Y="-26.499060546875" />
                  <Point X="-3.106143798828" Y="-26.49866015625" />
                  <Point X="-3.089136474609" Y="-26.481873046875" />
                  <Point X="-3.073733398438" Y="-26.46226171875" />
                  <Point X="-3.058676757812" Y="-26.43467578125" />
                  <Point X="-3.053852539062" Y="-26.419818359375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.0421171875" Y="-26.36241015625" />
                  <Point X="-3.042044677734" Y="-26.34228125" />
                  <Point X="-3.046214599609" Y="-26.322587890625" />
                  <Point X="-3.054778076172" Y="-26.296291015625" />
                  <Point X="-3.064922119141" Y="-26.274765625" />
                  <Point X="-3.08009765625" Y="-26.256435546875" />
                  <Point X="-3.096161621094" Y="-26.241359375" />
                  <Point X="-3.112987792969" Y="-26.2287578125" />
                  <Point X="-3.13945703125" Y="-26.2131796875" />
                  <Point X="-3.168721923828" Y="-26.201955078125" />
                  <Point X="-3.200608886719" Y="-26.195474609375" />
                  <Point X="-3.231929443359" Y="-26.194384765625" />
                  <Point X="-4.7321015625" Y="-26.39188671875" />
                  <Point X="-4.834076660156" Y="-25.99265625" />
                  <Point X="-4.892423828125" Y="-25.584701171875" />
                  <Point X="-3.533617919922" Y="-25.220609375" />
                  <Point X="-3.518851074219" Y="-25.215310546875" />
                  <Point X="-3.487716064453" Y="-25.1994609375" />
                  <Point X="-3.459977050781" Y="-25.180208984375" />
                  <Point X="-3.442167236328" Y="-25.16416796875" />
                  <Point X="-3.425830078125" Y="-25.145203125" />
                  <Point X="-3.409555908203" Y="-25.118369140625" />
                  <Point X="-3.404163818359" Y="-25.104052734375" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.390647949219" Y="-25.046087890625" />
                  <Point X="-3.39065234375" Y="-25.016431640625" />
                  <Point X="-3.394921386719" Y="-24.988287109375" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.418300292969" Y="-24.930458984375" />
                  <Point X="-3.437576171875" Y="-24.90418359375" />
                  <Point X="-3.460005126953" Y="-24.882333984375" />
                  <Point X="-3.487724853516" Y="-24.86309375" />
                  <Point X="-3.501916503906" Y="-24.854958984375" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-3.766198486328" Y="-23.700138671875" />
                  <Point X="-3.765188964844" Y="-23.700267578125" />
                  <Point X="-3.744488525391" Y="-23.700646484375" />
                  <Point X="-3.7230625" Y="-23.698693359375" />
                  <Point X="-3.702920410156" Y="-23.69466796875" />
                  <Point X="-3.641712158203" Y="-23.675369140625" />
                  <Point X="-3.622966308594" Y="-23.667146484375" />
                  <Point X="-3.604391357422" Y="-23.656478515625" />
                  <Point X="-3.587844970703" Y="-23.64443359375" />
                  <Point X="-3.574262451172" Y="-23.629123046875" />
                  <Point X="-3.561855712891" Y="-23.611662109375" />
                  <Point X="-3.551740234375" Y="-23.593498046875" />
                  <Point X="-3.551338867188" Y="-23.592541015625" />
                  <Point X="-3.526703857422" Y="-23.53306640625" />
                  <Point X="-3.520955566406" Y="-23.51343359375" />
                  <Point X="-3.517187255859" Y="-23.492359375" />
                  <Point X="-3.515775634766" Y="-23.471951171875" />
                  <Point X="-3.518765136719" Y="-23.451716796875" />
                  <Point X="-3.524155761719" Y="-23.430998046875" />
                  <Point X="-3.531633300781" Y="-23.4114296875" />
                  <Point X="-3.532065429688" Y="-23.410591796875" />
                  <Point X="-3.561790283203" Y="-23.3534921875" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.081156494141" Y="-22.266345703125" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.207255859375" Y="-22.154982421875" />
                  <Point X="-3.206929931641" Y="-22.155171875" />
                  <Point X="-3.188078125" Y="-22.163525390625" />
                  <Point X="-3.167545654297" Y="-22.17006640625" />
                  <Point X="-3.147266601562" Y="-22.174162109375" />
                  <Point X="-3.146751220703" Y="-22.174208984375" />
                  <Point X="-3.061244628906" Y="-22.181689453125" />
                  <Point X="-3.041040527344" Y="-22.181298828125" />
                  <Point X="-3.020062011719" Y="-22.17864453125" />
                  <Point X="-3.000407226562" Y="-22.173994140625" />
                  <Point X="-2.982179931641" Y="-22.16529296875" />
                  <Point X="-2.964174072266" Y="-22.15420703125" />
                  <Point X="-2.947485595703" Y="-22.141158203125" />
                  <Point X="-2.946046386719" Y="-22.1397421875" />
                  <Point X="-2.885353271484" Y="-22.079048828125" />
                  <Point X="-2.872435058594" Y="-22.0629609375" />
                  <Point X="-2.860825195312" Y="-22.044759765625" />
                  <Point X="-2.851684326172" Y="-22.026263671875" />
                  <Point X="-2.846752441406" Y="-22.00623046875" />
                  <Point X="-2.8438984375" Y="-21.984830078125" />
                  <Point X="-2.843409667969" Y="-21.96418359375" />
                  <Point X="-2.843439453125" Y="-21.963837890625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.183332519531" Y="-21.275404296875" />
                  <Point X="-2.700620117188" Y="-20.905314453125" />
                  <Point X="-2.167036376953" Y="-20.608865234375" />
                  <Point X="-2.043295898438" Y="-20.77012890625" />
                  <Point X="-2.042829833984" Y="-20.770732421875" />
                  <Point X="-2.028868286133" Y="-20.78530078125" />
                  <Point X="-2.012905883789" Y="-20.798751953125" />
                  <Point X="-1.995984741211" Y="-20.810146484375" />
                  <Point X="-1.99506652832" Y="-20.81062890625" />
                  <Point X="-1.899898071289" Y="-20.860171875" />
                  <Point X="-1.864206054688" Y="-20.87251171875" />
                  <Point X="-1.843811523438" Y="-20.876140625" />
                  <Point X="-1.823115356445" Y="-20.87526171875" />
                  <Point X="-1.799407714844" Y="-20.871638671875" />
                  <Point X="-1.777404418945" Y="-20.865498046875" />
                  <Point X="-1.678280029297" Y="-20.824439453125" />
                  <Point X="-1.660180541992" Y="-20.814513671875" />
                  <Point X="-1.642481811523" Y="-20.8021328125" />
                  <Point X="-1.626954467773" Y="-20.78853515625" />
                  <Point X="-1.61473034668" Y="-20.771904296875" />
                  <Point X="-1.603905029297" Y="-20.75321484375" />
                  <Point X="-1.595503662109" Y="-20.734154296875" />
                  <Point X="-1.595460327148" Y="-20.734015625" />
                  <Point X="-1.563200683594" Y="-20.631701171875" />
                  <Point X="-1.559165283203" Y="-20.6114140625" />
                  <Point X="-1.557279052734" Y="-20.589853515625" />
                  <Point X="-1.557730224609" Y="-20.569173828125" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-0.949618896484" Y="-20.190189453125" />
                  <Point X="-0.294711364746" Y="-20.113541015625" />
                  <Point X="-0.133999908447" Y="-20.71332421875" />
                  <Point X="-0.121228111267" Y="-20.741513671875" />
                  <Point X="-0.103372398376" Y="-20.76823828125" />
                  <Point X="-0.082217926025" Y="-20.790826171875" />
                  <Point X="-0.054925758362" Y="-20.805416015625" />
                  <Point X="-0.024491477966" Y="-20.81575" />
                  <Point X="0.006040857315" Y="-20.820794921875" />
                  <Point X="0.036574562073" Y="-20.8157578125" />
                  <Point X="0.067011421204" Y="-20.805431640625" />
                  <Point X="0.094307868958" Y="-20.790849609375" />
                  <Point X="0.115468383789" Y="-20.768265625" />
                  <Point X="0.133330764771" Y="-20.741544921875" />
                  <Point X="0.14610925293" Y="-20.713359375" />
                  <Point X="0.307388641357" Y="-20.11205859375" />
                  <Point X="0.844036437988" Y="-20.16826171875" />
                  <Point X="1.481047241211" Y="-20.322056640625" />
                  <Point X="1.894652587891" Y="-20.472076171875" />
                  <Point X="2.294562744141" Y="-20.659099609375" />
                  <Point X="2.680968994141" Y="-20.884220703125" />
                  <Point X="2.943259277344" Y="-21.07074609375" />
                  <Point X="2.148009277344" Y="-22.44816015625" />
                  <Point X="2.142781494141" Y="-22.4586640625" />
                  <Point X="2.133066162109" Y="-22.483984375" />
                  <Point X="2.111607177734" Y="-22.56423046875" />
                  <Point X="2.108616943359" Y="-22.582099609375" />
                  <Point X="2.107731933594" Y="-22.61908203125" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121544677734" Y="-22.710669921875" />
                  <Point X="2.129885742188" Y="-22.732751953125" />
                  <Point X="2.129905029297" Y="-22.732744140625" />
                  <Point X="2.140092285156" Y="-22.752560546875" />
                  <Point X="2.183028564453" Y="-22.815837890625" />
                  <Point X="2.194491210938" Y="-22.829697265625" />
                  <Point X="2.221628417969" Y="-22.854427734375" />
                  <Point X="2.284905517578" Y="-22.897365234375" />
                  <Point X="2.305296630859" Y="-22.90785546875" />
                  <Point X="2.327430908203" Y="-22.916041015625" />
                  <Point X="2.327438720703" Y="-22.91601953125" />
                  <Point X="2.348998779297" Y="-22.921341796875" />
                  <Point X="2.418388916016" Y="-22.929708984375" />
                  <Point X="2.436512939453" Y="-22.93015234375" />
                  <Point X="2.473246582031" Y="-22.925818359375" />
                  <Point X="2.553492675781" Y="-22.904359375" />
                  <Point X="2.565286376953" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.967324707031" Y="-22.09380859375" />
                  <Point X="4.123278808594" Y="-22.31055078125" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.231351074219" Y="-23.33111328125" />
                  <Point X="3.222416015625" Y="-23.33890234375" />
                  <Point X="3.203944580078" Y="-23.35841015625" />
                  <Point X="3.14619140625" Y="-23.43375390625" />
                  <Point X="3.136588867188" Y="-23.449123046875" />
                  <Point X="3.121619384766" Y="-23.482953125" />
                  <Point X="3.100106201172" Y="-23.55987890625" />
                  <Point X="3.096655517578" Y="-23.582095703125" />
                  <Point X="3.095822021484" Y="-23.60558203125" />
                  <Point X="3.097711669922" Y="-23.62809765625" />
                  <Point X="3.097748291016" Y="-23.628275390625" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120694580078" Y="-23.73105859375" />
                  <Point X="3.136307128906" Y="-23.764298828125" />
                  <Point X="3.184340820312" Y="-23.837306640625" />
                  <Point X="3.1988828125" Y="-23.854541015625" />
                  <Point X="3.216110839844" Y="-23.87062109375" />
                  <Point X="3.234436523438" Y="-23.883884765625" />
                  <Point X="3.234380859375" Y="-23.883984375" />
                  <Point X="3.30398828125" Y="-23.92316796875" />
                  <Point X="3.320565185547" Y="-23.930513671875" />
                  <Point X="3.356166503906" Y="-23.940568359375" />
                  <Point X="3.450280273438" Y="-23.953005859375" />
                  <Point X="3.462698242188" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845935058594" Y="-24.067185546875" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.717229980469" Y="-24.670234375" />
                  <Point X="3.705953369141" Y="-24.67402734375" />
                  <Point X="3.681500244141" Y="-24.684958984375" />
                  <Point X="3.589036376953" Y="-24.738404296875" />
                  <Point X="3.574274902344" Y="-24.748935546875" />
                  <Point X="3.54750390625" Y="-24.774458984375" />
                  <Point X="3.492025634766" Y="-24.845150390625" />
                  <Point X="3.480273925781" Y="-24.864490234375" />
                  <Point X="3.470495361328" Y="-24.88600390625" />
                  <Point X="3.463657958984" Y="-24.907515625" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.021923828125" />
                  <Point X="3.445187744141" Y="-25.0586015625" />
                  <Point X="3.463680664062" Y="-25.1551640625" />
                  <Point X="3.470526123047" Y="-25.176662109375" />
                  <Point X="3.480299804688" Y="-25.198126953125" />
                  <Point X="3.492022460938" Y="-25.21740625" />
                  <Point X="3.492050048828" Y="-25.21744140625" />
                  <Point X="3.547528320312" Y="-25.288134765625" />
                  <Point X="3.560041748047" Y="-25.301275390625" />
                  <Point X="3.589080566406" Y="-25.324181640625" />
                  <Point X="3.681544189453" Y="-25.37762890625" />
                  <Point X="3.692709228516" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.855022949219" Y="-25.9487265625" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.425135986328" Y="-26.003541015625" />
                  <Point X="3.409396484375" Y="-26.002787109375" />
                  <Point X="3.374568115234" Y="-26.005529296875" />
                  <Point X="3.193094726563" Y="-26.04497265625" />
                  <Point X="3.169917480469" Y="-26.052359375" />
                  <Point X="3.142415039062" Y="-26.067572265625" />
                  <Point X="3.126290283203" Y="-26.080384765625" />
                  <Point X="3.112342529297" Y="-26.09402734375" />
                  <Point X="3.002653320312" Y="-26.22594921875" />
                  <Point X="2.987883300781" Y="-26.25044921875" />
                  <Point X="2.97652734375" Y="-26.277970703125" />
                  <Point X="2.969744873047" Y="-26.305501953125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956375244141" Y="-26.5076796875" />
                  <Point X="2.964173583984" Y="-26.539404296875" />
                  <Point X="2.976517333984" Y="-26.5681015625" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.124817871094" Y="-27.749775390625" />
                  <Point X="4.028980712891" Y="-27.8859453125" />
                  <Point X="2.801625976562" Y="-27.17733203125" />
                  <Point X="2.787360839844" Y="-27.170607421875" />
                  <Point X="2.754116210938" Y="-27.159806640625" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.506690185547" Y="-27.120412109375" />
                  <Point X="2.474427734375" Y="-27.125416015625" />
                  <Point X="2.444743652344" Y="-27.135224609375" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242318359375" Y="-27.246615234375" />
                  <Point X="2.221311035156" Y="-27.2676640625" />
                  <Point X="2.204484375" Y="-27.290529296875" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100213378906" Y="-27.4998359375" />
                  <Point X="2.095274902344" Y="-27.532119140625" />
                  <Point X="2.095694824219" Y="-27.563369140625" />
                  <Point X="2.134701171875" Y="-27.7793515625" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.861282958984" Y="-29.05490625" />
                  <Point X="2.781852294922" Y="-29.111642578125" />
                  <Point X="2.701764160156" Y="-29.163482421875" />
                  <Point X="1.759064697266" Y="-27.934931640625" />
                  <Point X="1.748503295898" Y="-27.92330078125" />
                  <Point X="1.721817382812" Y="-27.90048828125" />
                  <Point X="1.508800415039" Y="-27.7635390625" />
                  <Point X="1.479881347656" Y="-27.751140625" />
                  <Point X="1.448143920898" Y="-27.743419921875" />
                  <Point X="1.416983398438" Y="-27.74112890625" />
                  <Point X="1.184013183594" Y="-27.76256640625" />
                  <Point X="1.162787841797" Y="-27.76700390625" />
                  <Point X="1.141259765625" Y="-27.774150390625" />
                  <Point X="1.1155078125" Y="-27.787341796875" />
                  <Point X="1.104229492188" Y="-27.795767578125" />
                  <Point X="0.924611694336" Y="-27.945115234375" />
                  <Point X="0.904079956055" Y="-27.96896484375" />
                  <Point X="0.887160766602" Y="-27.9969140625" />
                  <Point X="0.875597351074" Y="-28.02593359375" />
                  <Point X="0.821810119629" Y="-28.273396484375" />
                  <Point X="0.81972454834" Y="-28.289630859375" />
                  <Point X="0.819742736816" Y="-28.323123046875" />
                  <Point X="1.022065185547" Y="-29.859916015625" />
                  <Point X="0.975691650391" Y="-29.870080078125" />
                  <Point X="0.929315307617" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058425415039" Y="-29.75263671875" />
                  <Point X="-1.141246337891" Y="-29.731328125" />
                  <Point X="-1.120806762695" Y="-29.57607421875" />
                  <Point X="-1.120110107422" Y="-29.56837109375" />
                  <Point X="-1.119473754883" Y="-29.5414375" />
                  <Point X="-1.121782958984" Y="-29.509271484375" />
                  <Point X="-1.123364379883" Y="-29.497541015625" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.18684753418" Y="-29.182083984375" />
                  <Point X="-1.194188842773" Y="-29.15990234375" />
                  <Point X="-1.198544433594" Y="-29.149037109375" />
                  <Point X="-1.209005981445" Y="-29.126982421875" />
                  <Point X="-1.216866455078" Y="-29.113427734375" />
                  <Point X="-1.234710205078" Y="-29.087791015625" />
                  <Point X="-1.245917114258" Y="-29.074478515625" />
                  <Point X="-1.261228515625" Y="-29.059587890625" />
                  <Point X="-1.494267089844" Y="-28.855216796875" />
                  <Point X="-1.506817504883" Y="-28.84591796875" />
                  <Point X="-1.533271118164" Y="-28.82950390625" />
                  <Point X="-1.547173828125" Y="-28.822388671875" />
                  <Point X="-1.57706237793" Y="-28.810076171875" />
                  <Point X="-1.591761108398" Y="-28.805375" />
                  <Point X="-1.621732543945" Y="-28.798416015625" />
                  <Point X="-1.637005371094" Y="-28.796158203125" />
                  <Point X="-1.946403320312" Y="-28.77587890625" />
                  <Point X="-1.961998168945" Y="-28.776140625" />
                  <Point X="-1.992937133789" Y="-28.7792109375" />
                  <Point X="-2.008281616211" Y="-28.78201953125" />
                  <Point X="-2.039459228516" Y="-28.790423828125" />
                  <Point X="-2.054006103516" Y="-28.7956484375" />
                  <Point X="-2.082059326172" Y="-28.808384765625" />
                  <Point X="-2.095565673828" Y="-28.815896484375" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359683349609" Y="-28.9927578125" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471191406" Y="-29.041630859375" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.747584960938" Y="-29.011162109375" />
                  <Point X="-2.980861816406" Y="-28.831546875" />
                  <Point X="-2.341861083984" Y="-27.724765625" />
                  <Point X="-2.333470458984" Y="-27.70724609375" />
                  <Point X="-2.322924316406" Y="-27.67814453125" />
                  <Point X="-2.318844238281" Y="-27.66316015625" />
                  <Point X="-2.313034423828" Y="-27.631943359375" />
                  <Point X="-2.311454589844" Y="-27.61669140625" />
                  <Point X="-2.310768798828" Y="-27.586134765625" />
                  <Point X="-2.315007080078" Y="-27.5558671875" />
                  <Point X="-2.324059570312" Y="-27.526673828125" />
                  <Point X="-2.329767822266" Y="-27.512443359375" />
                  <Point X="-2.343929443359" Y="-27.484021484375" />
                  <Point X="-2.351851806641" Y="-27.47089453125" />
                  <Point X="-2.369706298828" Y="-27.4460859375" />
                  <Point X="-2.379638427734" Y="-27.434404296875" />
                  <Point X="-2.396978271484" Y="-27.417064453125" />
                  <Point X="-2.408818847656" Y="-27.40701953125" />
                  <Point X="-2.4339765625" Y="-27.3889921875" />
                  <Point X="-2.447293701172" Y="-27.381009765625" />
                  <Point X="-2.476122558594" Y="-27.36679296875" />
                  <Point X="-2.490563476562" Y="-27.3610859375" />
                  <Point X="-2.520181640625" Y="-27.3521015625" />
                  <Point X="-2.550869384766" Y="-27.3480625" />
                  <Point X="-2.581803466797" Y="-27.349076171875" />
                  <Point X="-2.597227050781" Y="-27.3508515625" />
                  <Point X="-2.628753417969" Y="-27.357123046875" />
                  <Point X="-2.643684082031" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-4.004022460937" Y="-27.740583984375" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.048834228516" Y="-26.5744296875" />
                  <Point X="-3.039407714844" Y="-26.566271484375" />
                  <Point X="-3.022400390625" Y="-26.549484375" />
                  <Point X="-3.014425537109" Y="-26.540552734375" />
                  <Point X="-2.999022460938" Y="-26.52094140625" />
                  <Point X="-2.990345703125" Y="-26.507775390625" />
                  <Point X="-2.9752890625" Y="-26.480189453125" />
                  <Point X="-2.968320556641" Y="-26.464013671875" />
                  <Point X="-2.961886962891" Y="-26.44363671875" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.952145507812" Y="-26.403791015625" />
                  <Point X="-2.948110839844" Y="-26.376115234375" />
                  <Point X="-2.947117919922" Y="-26.362751953125" />
                  <Point X="-2.947045410156" Y="-26.342623046875" />
                  <Point X="-2.949105224609" Y="-26.3226015625" />
                  <Point X="-2.953275146484" Y="-26.302908203125" />
                  <Point X="-2.955883544922" Y="-26.293171875" />
                  <Point X="-2.964447021484" Y="-26.266875" />
                  <Point X="-2.968842529297" Y="-26.25579296875" />
                  <Point X="-2.978986572266" Y="-26.234267578125" />
                  <Point X="-2.99174609375" Y="-26.21418359375" />
                  <Point X="-3.006921630859" Y="-26.195853515625" />
                  <Point X="-3.015086181641" Y="-26.1871640625" />
                  <Point X="-3.031150146484" Y="-26.172087890625" />
                  <Point X="-3.039213867188" Y="-26.1653203125" />
                  <Point X="-3.056040039062" Y="-26.15271875" />
                  <Point X="-3.064802490234" Y="-26.146884765625" />
                  <Point X="-3.091271728516" Y="-26.131306640625" />
                  <Point X="-3.105436279297" Y="-26.12448046875" />
                  <Point X="-3.134701171875" Y="-26.113255859375" />
                  <Point X="-3.149801513672" Y="-26.108857421875" />
                  <Point X="-3.181688476562" Y="-26.102376953125" />
                  <Point X="-3.197305175781" Y="-26.10053125" />
                  <Point X="-3.228625732422" Y="-26.09944140625" />
                  <Point X="-3.244329345703" Y="-26.100197265625" />
                  <Point X="-4.660919921875" Y="-26.2866953125" />
                  <Point X="-4.740761230469" Y="-25.974119140625" />
                  <Point X="-4.786451660156" Y="-25.654658203125" />
                  <Point X="-3.509030029297" Y="-25.312373046875" />
                  <Point X="-3.501531982422" Y="-25.31002734375" />
                  <Point X="-3.475753173828" Y="-25.29997265625" />
                  <Point X="-3.444618164062" Y="-25.284123046875" />
                  <Point X="-3.433549804688" Y="-25.277505859375" />
                  <Point X="-3.405810791016" Y="-25.25825390625" />
                  <Point X="-3.396398681641" Y="-25.250798828125" />
                  <Point X="-3.378588867188" Y="-25.2347578125" />
                  <Point X="-3.370191162109" Y="-25.226171875" />
                  <Point X="-3.353854003906" Y="-25.20720703125" />
                  <Point X="-3.344601318359" Y="-25.194466796875" />
                  <Point X="-3.328327148438" Y="-25.1676328125" />
                  <Point X="-3.320652587891" Y="-25.151853515625" />
                  <Point X="-3.313433349609" Y="-25.132212890625" />
                  <Point X="-3.304187011719" Y="-25.102421875" />
                  <Point X="-3.300989990234" Y="-25.08849609375" />
                  <Point X="-3.296720458984" Y="-25.060322265625" />
                  <Point X="-3.295647949219" Y="-25.04607421875" />
                  <Point X="-3.29565234375" Y="-25.01641796875" />
                  <Point X="-3.296726806641" Y="-25.002185546875" />
                  <Point X="-3.300995849609" Y="-24.974041015625" />
                  <Point X="-3.304190429688" Y="-24.96012890625" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.319336669922" Y="-24.91573046875" />
                  <Point X="-3.333469238281" Y="-24.8876953125" />
                  <Point X="-3.341701904297" Y="-24.874265625" />
                  <Point X="-3.360977783203" Y="-24.847990234375" />
                  <Point X="-3.371285644531" Y="-24.836134765625" />
                  <Point X="-3.393714599609" Y="-24.81428515625" />
                  <Point X="-3.405835693359" Y="-24.804291015625" />
                  <Point X="-3.433555419922" Y="-24.78505078125" />
                  <Point X="-3.440481201172" Y="-24.780673828125" />
                  <Point X="-3.465598388672" Y="-24.76717578125" />
                  <Point X="-3.496557861328" Y="-24.7543671875" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731331054688" Y="-24.042470703125" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-3.778598632812" Y="-23.794326171875" />
                  <Point X="-3.766927490234" Y="-23.795251953125" />
                  <Point X="-3.746227050781" Y="-23.795630859375" />
                  <Point X="-3.735864501953" Y="-23.79525390625" />
                  <Point X="-3.714438476562" Y="-23.79330078125" />
                  <Point X="-3.704444824219" Y="-23.7918515625" />
                  <Point X="-3.684302734375" Y="-23.787826171875" />
                  <Point X="-3.674353515625" Y="-23.785271484375" />
                  <Point X="-3.613145263672" Y="-23.76597265625" />
                  <Point X="-3.603551269531" Y="-23.7623671875" />
                  <Point X="-3.584805419922" Y="-23.75414453125" />
                  <Point X="-3.575653564453" Y="-23.74952734375" />
                  <Point X="-3.557078613281" Y="-23.738859375" />
                  <Point X="-3.548481201172" Y="-23.733283203125" />
                  <Point X="-3.531934814453" Y="-23.72123828125" />
                  <Point X="-3.516779296875" Y="-23.707478515625" />
                  <Point X="-3.503196777344" Y="-23.69216796875" />
                  <Point X="-3.496820800781" Y="-23.6841484375" />
                  <Point X="-3.4844140625" Y="-23.6666875" />
                  <Point X="-3.478857910156" Y="-23.6578828125" />
                  <Point X="-3.468742431641" Y="-23.63971875" />
                  <Point X="-3.4635703125" Y="-23.628896484375" />
                  <Point X="-3.438935302734" Y="-23.569421875" />
                  <Point X="-3.435531494141" Y="-23.55976171875" />
                  <Point X="-3.429783203125" Y="-23.54012890625" />
                  <Point X="-3.427438720703" Y="-23.53015625" />
                  <Point X="-3.423670410156" Y="-23.50908203125" />
                  <Point X="-3.422413818359" Y="-23.4989140625" />
                  <Point X="-3.421002197266" Y="-23.478505859375" />
                  <Point X="-3.421795898438" Y="-23.45806640625" />
                  <Point X="-3.424785400391" Y="-23.43783203125" />
                  <Point X="-3.426826171875" Y="-23.427796875" />
                  <Point X="-3.432216796875" Y="-23.407078125" />
                  <Point X="-3.4354140625" Y="-23.397087890625" />
                  <Point X="-3.442891601562" Y="-23.37751953125" />
                  <Point X="-3.447799804688" Y="-23.366724609375" />
                  <Point X="-3.477524658203" Y="-23.309625" />
                  <Point X="-3.482802001953" Y="-23.3007109375" />
                  <Point X="-3.494293457031" Y="-23.283513671875" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521498046875" Y="-23.251091796875" />
                  <Point X="-3.536439941406" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.726336914062" Y="-21.964986328125" />
                  <Point X="-3.254997070312" Y="-22.237115234375" />
                  <Point X="-3.245416748047" Y="-22.24202734375" />
                  <Point X="-3.226564941406" Y="-22.250380859375" />
                  <Point X="-3.216914306641" Y="-22.25404296875" />
                  <Point X="-3.196381835938" Y="-22.260583984375" />
                  <Point X="-3.186352783203" Y="-22.263185546875" />
                  <Point X="-3.166073730469" Y="-22.26728125" />
                  <Point X="-3.155030517578" Y="-22.26884765625" />
                  <Point X="-3.069523925781" Y="-22.276328125" />
                  <Point X="-3.059408203125" Y="-22.276671875" />
                  <Point X="-3.039204101562" Y="-22.27628125" />
                  <Point X="-3.029115722656" Y="-22.275546875" />
                  <Point X="-3.008137207031" Y="-22.272892578125" />
                  <Point X="-2.998188476562" Y="-22.271091796875" />
                  <Point X="-2.978533691406" Y="-22.26644140625" />
                  <Point X="-2.959481201172" Y="-22.2597265625" />
                  <Point X="-2.94125390625" Y="-22.251025390625" />
                  <Point X="-2.932373046875" Y="-22.246189453125" />
                  <Point X="-2.9143671875" Y="-22.235103515625" />
                  <Point X="-2.905657470703" Y="-22.229044921875" />
                  <Point X="-2.888968994141" Y="-22.21599609375" />
                  <Point X="-2.87887109375" Y="-22.20691796875" />
                  <Point X="-2.818177978516" Y="-22.146224609375" />
                  <Point X="-2.811278564453" Y="-22.138529296875" />
                  <Point X="-2.798360351562" Y="-22.12244140625" />
                  <Point X="-2.792341552734" Y="-22.114048828125" />
                  <Point X="-2.780731689453" Y="-22.09584765625" />
                  <Point X="-2.775658203125" Y="-22.086849609375" />
                  <Point X="-2.766517333984" Y="-22.068353515625" />
                  <Point X="-2.759438476562" Y="-22.04897265625" />
                  <Point X="-2.754506591797" Y="-22.028939453125" />
                  <Point X="-2.752586181641" Y="-22.0187890625" />
                  <Point X="-2.749732177734" Y="-21.997388671875" />
                  <Point X="-2.748925048828" Y="-21.987078125" />
                  <Point X="-2.748436279297" Y="-21.966431640625" />
                  <Point X="-2.748801025391" Y="-21.95555859375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.059386474609" Y="-21.3000859375" />
                  <Point X="-2.648369140625" Y="-20.984962890625" />
                  <Point X="-2.192524414062" Y="-20.731703125" />
                  <Point X="-2.118664794922" Y="-20.8279609375" />
                  <Point X="-2.111418212891" Y="-20.83646484375" />
                  <Point X="-2.097456787109" Y="-20.851033203125" />
                  <Point X="-2.090085449219" Y="-20.857947265625" />
                  <Point X="-2.074123046875" Y="-20.8713984375" />
                  <Point X="-2.065968505859" Y="-20.87755078125" />
                  <Point X="-2.049047363281" Y="-20.8889453125" />
                  <Point X="-2.03893359375" Y="-20.89489453125" />
                  <Point X="-1.943765136719" Y="-20.9444375" />
                  <Point X="-1.930939697266" Y="-20.94995703125" />
                  <Point X="-1.895247680664" Y="-20.962296875" />
                  <Point X="-1.880848510742" Y="-20.96604296875" />
                  <Point X="-1.860453979492" Y="-20.969671875" />
                  <Point X="-1.839780761719" Y="-20.9710546875" />
                  <Point X="-1.819084472656" Y="-20.97017578125" />
                  <Point X="-1.808763793945" Y="-20.969171875" />
                  <Point X="-1.785056274414" Y="-20.965548828125" />
                  <Point X="-1.77387121582" Y="-20.963142578125" />
                  <Point X="-1.751867919922" Y="-20.957001953125" />
                  <Point X="-1.741049682617" Y="-20.953267578125" />
                  <Point X="-1.641925170898" Y="-20.912208984375" />
                  <Point X="-1.632599975586" Y="-20.907736328125" />
                  <Point X="-1.614500488281" Y="-20.897810546875" />
                  <Point X="-1.605725952148" Y="-20.892357421875" />
                  <Point X="-1.58802722168" Y="-20.8799765625" />
                  <Point X="-1.57989465332" Y="-20.8736015625" />
                  <Point X="-1.56436730957" Y="-20.86000390625" />
                  <Point X="-1.550407714844" Y="-20.844798828125" />
                  <Point X="-1.53818371582" Y="-20.82816796875" />
                  <Point X="-1.532524536133" Y="-20.81951953125" />
                  <Point X="-1.52169921875" Y="-20.800830078125" />
                  <Point X="-1.516974853516" Y="-20.79153125" />
                  <Point X="-1.508573486328" Y="-20.772470703125" />
                  <Point X="-1.504808227539" Y="-20.76242578125" />
                  <Point X="-1.47259753418" Y="-20.660267578125" />
                  <Point X="-1.470026123047" Y="-20.650234375" />
                  <Point X="-1.465990722656" Y="-20.629947265625" />
                  <Point X="-1.464526733398" Y="-20.619693359375" />
                  <Point X="-1.46264050293" Y="-20.5981328125" />
                  <Point X="-1.462301635742" Y="-20.58778125" />
                  <Point X="-1.462752807617" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-0.931160095215" Y="-20.283677734375" />
                  <Point X="-0.36522253418" Y="-20.21744140625" />
                  <Point X="-0.225762832642" Y="-20.737912109375" />
                  <Point X="-0.220532791138" Y="-20.752529296875" />
                  <Point X="-0.207761032104" Y="-20.78071875" />
                  <Point X="-0.200219161987" Y="-20.794291015625" />
                  <Point X="-0.182363464355" Y="-20.821015625" />
                  <Point X="-0.172711593628" Y="-20.833177734375" />
                  <Point X="-0.1515572052" Y="-20.855765625" />
                  <Point X="-0.127005142212" Y="-20.87460546875" />
                  <Point X="-0.099712928772" Y="-20.8891953125" />
                  <Point X="-0.085470245361" Y="-20.89537109375" />
                  <Point X="-0.055035995483" Y="-20.905705078125" />
                  <Point X="-0.039978565216" Y="-20.909478515625" />
                  <Point X="-0.009446219444" Y="-20.9145234375" />
                  <Point X="0.021503904343" Y="-20.91452734375" />
                  <Point X="0.052037586212" Y="-20.909490234375" />
                  <Point X="0.067096054077" Y="-20.905720703125" />
                  <Point X="0.097532981873" Y="-20.89539453125" />
                  <Point X="0.111774475098" Y="-20.889224609375" />
                  <Point X="0.139070861816" Y="-20.874642578125" />
                  <Point X="0.163632278442" Y="-20.8558046875" />
                  <Point X="0.184792755127" Y="-20.833220703125" />
                  <Point X="0.194446853638" Y="-20.821060546875" />
                  <Point X="0.212309249878" Y="-20.79433984375" />
                  <Point X="0.219853790283" Y="-20.780771484375" />
                  <Point X="0.232632385254" Y="-20.7525859375" />
                  <Point X="0.237865997314" Y="-20.737970703125" />
                  <Point X="0.378139434814" Y="-20.214986328125" />
                  <Point X="0.827857543945" Y="-20.2620859375" />
                  <Point X="1.45362902832" Y="-20.41316796875" />
                  <Point X="1.858258300781" Y="-20.559931640625" />
                  <Point X="2.250434326172" Y="-20.743337890625" />
                  <Point X="2.629427978516" Y="-20.964140625" />
                  <Point X="2.817778808594" Y="-21.098083984375" />
                  <Point X="2.065736816406" Y="-22.40066015625" />
                  <Point X="2.062960449219" Y="-22.40583203125" />
                  <Point X="2.054086425781" Y="-22.4246328125" />
                  <Point X="2.04437097168" Y="-22.449953125" />
                  <Point X="2.041291015625" Y="-22.45944140625" />
                  <Point X="2.01983203125" Y="-22.5396875" />
                  <Point X="2.01791003418" Y="-22.54855078125" />
                  <Point X="2.013644165039" Y="-22.579826171875" />
                  <Point X="2.012759033203" Y="-22.61680859375" />
                  <Point X="2.013415161133" Y="-22.630455078125" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.023834960938" Y="-22.711107421875" />
                  <Point X="2.029280395508" Y="-22.7333046875" />
                  <Point X="2.032673461914" Y="-22.744240234375" />
                  <Point X="2.041014526367" Y="-22.766322265625" />
                  <Point X="2.045415649414" Y="-22.776177734375" />
                  <Point X="2.055603027344" Y="-22.795994140625" />
                  <Point X="2.061480957031" Y="-22.80590234375" />
                  <Point X="2.104417236328" Y="-22.8691796875" />
                  <Point X="2.109822509766" Y="-22.876384765625" />
                  <Point X="2.130501953125" Y="-22.8999140625" />
                  <Point X="2.157639160156" Y="-22.92464453125" />
                  <Point X="2.168286132813" Y="-22.9330390625" />
                  <Point X="2.231563232422" Y="-22.9759765625" />
                  <Point X="2.241446289062" Y="-22.981841796875" />
                  <Point X="2.261837402344" Y="-22.99233203125" />
                  <Point X="2.272345458984" Y="-22.99695703125" />
                  <Point X="2.294479736328" Y="-23.005142578125" />
                  <Point X="2.304670654297" Y="-23.00825" />
                  <Point X="2.326230712891" Y="-23.013572265625" />
                  <Point X="2.337625976563" Y="-23.015658203125" />
                  <Point X="2.407016113281" Y="-23.024025390625" />
                  <Point X="2.416065673828" Y="-23.0246796875" />
                  <Point X="2.447644287109" Y="-23.024498046875" />
                  <Point X="2.484377929688" Y="-23.0201640625" />
                  <Point X="2.497788574219" Y="-23.01759375" />
                  <Point X="2.578034667969" Y="-22.996134765625" />
                  <Point X="2.583992675781" Y="-22.994330078125" />
                  <Point X="2.604414794922" Y="-22.9869296875" />
                  <Point X="2.627662353516" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.940401855469" Y="-22.219048828125" />
                  <Point X="4.043964599609" Y="-22.362978515625" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.173518798828" Y="-23.255744140625" />
                  <Point X="3.168925292969" Y="-23.25950390625" />
                  <Point X="3.15343359375" Y="-23.273583984375" />
                  <Point X="3.134962158203" Y="-23.293091796875" />
                  <Point X="3.128546875" Y="-23.300615234375" />
                  <Point X="3.070793701172" Y="-23.375958984375" />
                  <Point X="3.065624023438" Y="-23.383416015625" />
                  <Point X="3.049713867188" Y="-23.410681640625" />
                  <Point X="3.034744384766" Y="-23.44451171875" />
                  <Point X="3.030129882812" Y="-23.4573671875" />
                  <Point X="3.008616699219" Y="-23.53429296875" />
                  <Point X="3.006231689453" Y="-23.545298828125" />
                  <Point X="3.002781005859" Y="-23.567515625" />
                  <Point X="3.001715332031" Y="-23.5787265625" />
                  <Point X="3.000881835938" Y="-23.602212890625" />
                  <Point X="3.001154785156" Y="-23.61352734375" />
                  <Point X="3.003044433594" Y="-23.63604296875" />
                  <Point X="3.004708251953" Y="-23.64747265625" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024603271484" Y="-23.741783203125" />
                  <Point X="3.03470703125" Y="-23.7714453125" />
                  <Point X="3.050319580078" Y="-23.804685546875" />
                  <Point X="3.056943603516" Y="-23.816513671875" />
                  <Point X="3.104977294922" Y="-23.889521484375" />
                  <Point X="3.111734130859" Y="-23.8985703125" />
                  <Point X="3.126276123047" Y="-23.9158046875" />
                  <Point X="3.134061279297" Y="-23.923990234375" />
                  <Point X="3.151289306641" Y="-23.9400703125" />
                  <Point X="3.160410644531" Y="-23.947578125" />
                  <Point X="3.177148193359" Y="-23.95969140625" />
                  <Point X="3.187779541016" Y="-23.96676953125" />
                  <Point X="3.257386962891" Y="-24.005953125" />
                  <Point X="3.265500488281" Y="-24.010021484375" />
                  <Point X="3.294744873047" Y="-24.0219375" />
                  <Point X="3.330346191406" Y="-24.0319921875" />
                  <Point X="3.343720214844" Y="-24.03475" />
                  <Point X="3.437833984375" Y="-24.0471875" />
                  <Point X="3.444033203125" Y="-24.04780078125" />
                  <Point X="3.465708496094" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="3.692642089844" Y="-24.578470703125" />
                  <Point X="3.686943359375" Y="-24.58019140625" />
                  <Point X="3.667181884766" Y="-24.587298828125" />
                  <Point X="3.642728759766" Y="-24.59823046875" />
                  <Point X="3.633959472656" Y="-24.6027109375" />
                  <Point X="3.541495605469" Y="-24.65615625" />
                  <Point X="3.533862792969" Y="-24.661068359375" />
                  <Point X="3.508720947266" Y="-24.680177734375" />
                  <Point X="3.481949951172" Y="-24.705701171875" />
                  <Point X="3.472770263672" Y="-24.71580859375" />
                  <Point X="3.417291992188" Y="-24.7865" />
                  <Point X="3.410838867188" Y="-24.795818359375" />
                  <Point X="3.399087158203" Y="-24.815158203125" />
                  <Point X="3.393788574219" Y="-24.8251796875" />
                  <Point X="3.384010009766" Y="-24.846693359375" />
                  <Point X="3.379958496094" Y="-24.8572265625" />
                  <Point X="3.37312109375" Y="-24.87873828125" />
                  <Point X="3.370353515625" Y="-24.889646484375" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350601074219" Y="-24.995056640625" />
                  <Point X="3.348585449219" Y="-25.026333984375" />
                  <Point X="3.350290283203" Y="-25.06301171875" />
                  <Point X="3.351883300781" Y="-25.076470703125" />
                  <Point X="3.370376220703" Y="-25.173033203125" />
                  <Point X="3.373158935547" Y="-25.18398828125" />
                  <Point X="3.380004394531" Y="-25.205486328125" />
                  <Point X="3.384067138672" Y="-25.216029296875" />
                  <Point X="3.393840820312" Y="-25.237494140625" />
                  <Point X="3.399127441406" Y="-25.247482421875" />
                  <Point X="3.410850097656" Y="-25.26676171875" />
                  <Point X="3.417313720703" Y="-25.276087890625" />
                  <Point X="3.472791992188" Y="-25.34678125" />
                  <Point X="3.478731445312" Y="-25.3536484375" />
                  <Point X="3.501205810547" Y="-25.37586328125" />
                  <Point X="3.530244628906" Y="-25.39876953125" />
                  <Point X="3.541538330078" Y="-25.4064296875" />
                  <Point X="3.634001953125" Y="-25.459876953125" />
                  <Point X="3.639491699219" Y="-25.462814453125" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026611328" Y="-25.481029296875" />
                  <Point X="3.6919921875" Y="-25.483916015625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761613769531" Y="-25.9310546875" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.437535888672" Y="-25.909353515625" />
                  <Point X="3.429681152344" Y="-25.908650390625" />
                  <Point X="3.401939697266" Y="-25.908080078125" />
                  <Point X="3.367111328125" Y="-25.910822265625" />
                  <Point X="3.354390869141" Y="-25.912697265625" />
                  <Point X="3.172917480469" Y="-25.952140625" />
                  <Point X="3.164247314453" Y="-25.954458984375" />
                  <Point X="3.141070068359" Y="-25.961845703125" />
                  <Point X="3.123934570312" Y="-25.96923046875" />
                  <Point X="3.096432128906" Y="-25.984443359375" />
                  <Point X="3.083314941406" Y="-25.993193359375" />
                  <Point X="3.067190185547" Y="-26.006005859375" />
                  <Point X="3.059862304688" Y="-26.012470703125" />
                  <Point X="3.039294677734" Y="-26.033291015625" />
                  <Point X="2.92960546875" Y="-26.165212890625" />
                  <Point X="2.921294189453" Y="-26.176900390625" />
                  <Point X="2.906524169922" Y="-26.201400390625" />
                  <Point X="2.900065429688" Y="-26.214212890625" />
                  <Point X="2.888709472656" Y="-26.241734375" />
                  <Point X="2.88428515625" Y="-26.25524609375" />
                  <Point X="2.877502685547" Y="-26.28277734375" />
                  <Point X="2.87514453125" Y="-26.296796875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.85929296875" Y="-26.483380859375" />
                  <Point X="2.861639648438" Y="-26.514763671875" />
                  <Point X="2.864121582031" Y="-26.530357421875" />
                  <Point X="2.871919921875" Y="-26.56208203125" />
                  <Point X="2.876904296875" Y="-26.57694140625" />
                  <Point X="2.889248046875" Y="-26.605638671875" />
                  <Point X="2.896607421875" Y="-26.6194765625" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052795898438" Y="-26.836279296875" />
                  <Point X="4.087170410156" Y="-27.629984375" />
                  <Point X="4.045499023438" Y="-27.6974140625" />
                  <Point X="4.001273193359" Y="-27.760251953125" />
                  <Point X="2.849125976562" Y="-27.095060546875" />
                  <Point X="2.842134033203" Y="-27.091400390625" />
                  <Point X="2.81671484375" Y="-27.080255859375" />
                  <Point X="2.783470214844" Y="-27.069455078125" />
                  <Point X="2.770999755859" Y="-27.066318359375" />
                  <Point X="2.555017578125" Y="-27.0273125" />
                  <Point X="2.539308105469" Y="-27.02580859375" />
                  <Point X="2.507864257812" Y="-27.025419921875" />
                  <Point X="2.492129882813" Y="-27.02653515625" />
                  <Point X="2.459867431641" Y="-27.0315390625" />
                  <Point X="2.444621582031" Y="-27.035212890625" />
                  <Point X="2.4149375" Y="-27.045021484375" />
                  <Point X="2.400499267578" Y="-27.05115625" />
                  <Point X="2.221071044922" Y="-27.145587890625" />
                  <Point X="2.208931884766" Y="-27.153197265625" />
                  <Point X="2.185934814453" Y="-27.17015625" />
                  <Point X="2.175076904297" Y="-27.179505859375" />
                  <Point X="2.154069580078" Y="-27.2005546875" />
                  <Point X="2.144796630859" Y="-27.211357421875" />
                  <Point X="2.127969970703" Y="-27.23422265625" />
                  <Point X="2.120416259766" Y="-27.24628515625" />
                  <Point X="2.025984741211" Y="-27.425712890625" />
                  <Point X="2.019819458008" Y="-27.4402421875" />
                  <Point X="2.009980102539" Y="-27.47012109375" />
                  <Point X="2.006305786133" Y="-27.485470703125" />
                  <Point X="2.00136730957" Y="-27.51775390625" />
                  <Point X="2.000283325195" Y="-27.533396484375" />
                  <Point X="2.000703369141" Y="-27.564646484375" />
                  <Point X="2.00220715332" Y="-27.58025390625" />
                  <Point X="2.041213500977" Y="-27.796236328125" />
                  <Point X="2.043015380859" Y="-27.8042265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.735893066406" Y="-29.027724609375" />
                  <Point X="2.723753662109" Y="-29.0360859375" />
                  <Point X="1.834433105469" Y="-27.877099609375" />
                  <Point X="1.829395385742" Y="-27.871068359375" />
                  <Point X="1.810233032227" Y="-27.85108984375" />
                  <Point X="1.783547119141" Y="-27.82827734375" />
                  <Point X="1.773191894531" Y="-27.820578125" />
                  <Point X="1.560174926758" Y="-27.68362890625" />
                  <Point X="1.546234375" Y="-27.676224609375" />
                  <Point X="1.517315307617" Y="-27.663826171875" />
                  <Point X="1.502336914062" Y="-27.65883203125" />
                  <Point X="1.470599609375" Y="-27.651111328125" />
                  <Point X="1.455109985352" Y="-27.64867578125" />
                  <Point X="1.42394934082" Y="-27.646384765625" />
                  <Point X="1.408278442383" Y="-27.646529296875" />
                  <Point X="1.175308105469" Y="-27.667966796875" />
                  <Point X="1.164572265625" Y="-27.669576171875" />
                  <Point X="1.143346923828" Y="-27.674013671875" />
                  <Point X="1.132857543945" Y="-27.676841796875" />
                  <Point X="1.111329467773" Y="-27.68398828125" />
                  <Point X="1.097947998047" Y="-27.68959765625" />
                  <Point X="1.072195922852" Y="-27.7027890625" />
                  <Point X="1.0434921875" Y="-27.722720703125" />
                  <Point X="0.863874389648" Y="-27.872068359375" />
                  <Point X="0.852615600586" Y="-27.883134765625" />
                  <Point X="0.83208380127" Y="-27.906984375" />
                  <Point X="0.822810791016" Y="-27.919767578125" />
                  <Point X="0.805891540527" Y="-27.947716796875" />
                  <Point X="0.798908935547" Y="-27.961748046875" />
                  <Point X="0.78734564209" Y="-27.990767578125" />
                  <Point X="0.782764953613" Y="-28.005755859375" />
                  <Point X="0.728977722168" Y="-28.25321875" />
                  <Point X="0.727584533691" Y="-28.261291015625" />
                  <Point X="0.72472454834" Y="-28.289681640625" />
                  <Point X="0.724742675781" Y="-28.323173828125" />
                  <Point X="0.725555541992" Y="-28.3355234375" />
                  <Point X="0.833091552734" Y="-29.152341796875" />
                  <Point X="0.655265075684" Y="-28.48868359375" />
                  <Point X="0.652891845703" Y="-28.481107421875" />
                  <Point X="0.642752929688" Y="-28.45516796875" />
                  <Point X="0.626861755371" Y="-28.4240234375" />
                  <Point X="0.62028503418" Y="-28.41303515625" />
                  <Point X="0.456680053711" Y="-28.177310546875" />
                  <Point X="0.446629577637" Y="-28.165130859375" />
                  <Point X="0.42464453125" Y="-28.142599609375" />
                  <Point X="0.412709533691" Y="-28.132248046875" />
                  <Point X="0.386327484131" Y="-28.11296484375" />
                  <Point X="0.372956176758" Y="-28.10479296875" />
                  <Point X="0.345056396484" Y="-28.090759765625" />
                  <Point X="0.330527893066" Y="-28.0848984375" />
                  <Point X="0.077295448303" Y="-28.0063046875" />
                  <Point X="0.063315982819" Y="-28.003099609375" />
                  <Point X="0.035031856537" Y="-27.998830078125" />
                  <Point X="0.020727497101" Y="-27.997765625" />
                  <Point X="-0.009040776253" Y="-27.9978046875" />
                  <Point X="-0.023219251633" Y="-27.99888671875" />
                  <Point X="-0.0512538414" Y="-28.00315625" />
                  <Point X="-0.065110099792" Y="-28.00634375" />
                  <Point X="-0.318342407227" Y="-28.0849375" />
                  <Point X="-0.329444824219" Y="-28.08916015625" />
                  <Point X="-0.351030822754" Y="-28.09895703125" />
                  <Point X="-0.361514373779" Y="-28.104529296875" />
                  <Point X="-0.382604705811" Y="-28.1174921875" />
                  <Point X="-0.395373962402" Y="-28.12689453125" />
                  <Point X="-0.419173217773" Y="-28.147693359375" />
                  <Point X="-0.431302947998" Y="-28.160462890625" />
                  <Point X="-0.444520568848" Y="-28.177529296875" />
                  <Point X="-0.608095214844" Y="-28.41320703125" />
                  <Point X="-0.612470703125" Y="-28.420130859375" />
                  <Point X="-0.625977111816" Y="-28.445263671875" />
                  <Point X="-0.638777954102" Y="-28.47621484375" />
                  <Point X="-0.642752868652" Y="-28.487935546875" />
                  <Point X="-0.985425048828" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691451660156" Y="-23.895304497556" />
                  <Point X="-4.691451660156" Y="-24.433359446308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691451660156" Y="-25.629202950875" />
                  <Point X="-4.691451660156" Y="-26.167164532748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.596451660156" Y="-23.686652548085" />
                  <Point X="-4.596451660156" Y="-24.458814583313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.596451660156" Y="-25.603747698624" />
                  <Point X="-4.596451660156" Y="-26.278207888471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.501451660156" Y="-23.699159678239" />
                  <Point X="-4.501451660156" Y="-24.484269720319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.501451660156" Y="-25.578292446374" />
                  <Point X="-4.501451660156" Y="-26.265700877093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.406451660156" Y="-23.711666808393" />
                  <Point X="-4.406451660156" Y="-24.509724857325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.406451660156" Y="-25.552837194123" />
                  <Point X="-4.406451660156" Y="-26.253193865716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.311451660156" Y="-23.724173938548" />
                  <Point X="-4.311451660156" Y="-24.535179994331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.311451660156" Y="-25.527381941873" />
                  <Point X="-4.311451660156" Y="-26.240686854339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216451660156" Y="-22.686594472681" />
                  <Point X="-4.216451660156" Y="-22.714284106587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216451660156" Y="-23.736681068702" />
                  <Point X="-4.216451660156" Y="-24.560635131337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216451660156" Y="-25.501926689622" />
                  <Point X="-4.216451660156" Y="-26.228179842962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121451660156" Y="-22.523836089623" />
                  <Point X="-4.121451660156" Y="-22.78718012065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121451660156" Y="-23.749188198856" />
                  <Point X="-4.121451660156" Y="-24.586090268343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121451660156" Y="-25.476471437372" />
                  <Point X="-4.121451660156" Y="-26.215672831584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121451660156" Y="-27.397478478008" />
                  <Point X="-4.121451660156" Y="-27.543673094365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026451660156" Y="-22.361077706564" />
                  <Point X="-4.026451660156" Y="-22.860076134713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026451660156" Y="-23.761695329011" />
                  <Point X="-4.026451660156" Y="-24.611545405348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026451660156" Y="-25.451016185121" />
                  <Point X="-4.026451660156" Y="-26.203165820207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026451660156" Y="-27.324582370978" />
                  <Point X="-4.026451660156" Y="-27.702973631841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931451660156" Y="-22.228632675609" />
                  <Point X="-3.931451660156" Y="-22.932972148776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931451660156" Y="-23.774202459165" />
                  <Point X="-3.931451660156" Y="-24.637000542354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931451660156" Y="-25.425560932871" />
                  <Point X="-3.931451660156" Y="-26.19065880883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931451660156" Y="-27.251686263949" />
                  <Point X="-3.931451660156" Y="-27.835927554871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836451660156" Y="-22.106523449003" />
                  <Point X="-3.836451660156" Y="-23.005868162838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836451660156" Y="-23.786709589319" />
                  <Point X="-3.836451660156" Y="-24.66245567936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836451660156" Y="-25.40010568062" />
                  <Point X="-3.836451660156" Y="-26.178151797452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836451660156" Y="-27.17879015692" />
                  <Point X="-3.836451660156" Y="-27.960738623505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741451660156" Y="-21.984414222397" />
                  <Point X="-3.741451660156" Y="-23.078764176901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741451660156" Y="-23.795457147439" />
                  <Point X="-3.741451660156" Y="-24.687910816366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741451660156" Y="-25.37465042837" />
                  <Point X="-3.741451660156" Y="-26.165644786075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741451660156" Y="-27.105894049891" />
                  <Point X="-3.741451660156" Y="-27.987899131124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646451660156" Y="-22.0111082231" />
                  <Point X="-3.646451660156" Y="-23.151660190964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646451660156" Y="-23.7764740903" />
                  <Point X="-3.646451660156" Y="-24.713365953372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646451660156" Y="-25.349195176119" />
                  <Point X="-3.646451660156" Y="-26.153137774698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646451660156" Y="-27.032997942862" />
                  <Point X="-3.646451660156" Y="-27.933050786228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551451660156" Y="-22.065956643908" />
                  <Point X="-3.551451660156" Y="-23.224556205027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551451660156" Y="-23.735209804948" />
                  <Point X="-3.551451660156" Y="-24.738821090377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551451660156" Y="-25.323739923869" />
                  <Point X="-3.551451660156" Y="-26.140630763321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551451660156" Y="-26.960101835833" />
                  <Point X="-3.551451660156" Y="-27.878202441333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456451660156" Y="-22.120805064717" />
                  <Point X="-3.456451660156" Y="-23.350104928899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456451660156" Y="-23.611710411731" />
                  <Point X="-3.456451660156" Y="-24.772091258759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456451660156" Y="-25.290147014493" />
                  <Point X="-3.456451660156" Y="-26.128123751943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456451660156" Y="-26.887205728803" />
                  <Point X="-3.456451660156" Y="-27.823354096437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361451660156" Y="-22.175653485525" />
                  <Point X="-3.361451660156" Y="-24.84744521022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361451660156" Y="-25.216026702447" />
                  <Point X="-3.361451660156" Y="-26.115616740566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361451660156" Y="-26.814309621774" />
                  <Point X="-3.361451660156" Y="-27.768505751541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.266451660156" Y="-22.230501906334" />
                  <Point X="-3.266451660156" Y="-26.103109729189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.266451660156" Y="-26.741413514745" />
                  <Point X="-3.266451660156" Y="-27.713657406646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.171451660156" Y="-22.266195084699" />
                  <Point X="-3.171451660156" Y="-26.104457407417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.171451660156" Y="-26.668517407716" />
                  <Point X="-3.171451660156" Y="-27.65880906175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.076451660156" Y="-22.27572205827" />
                  <Point X="-3.076451660156" Y="-26.140028798587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.076451660156" Y="-26.595621300687" />
                  <Point X="-3.076451660156" Y="-27.603960716854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981451660156" Y="-21.240334062926" />
                  <Point X="-2.981451660156" Y="-21.435072587293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981451660156" Y="-22.267131807819" />
                  <Point X="-2.981451660156" Y="-26.230387433659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981451660156" Y="-26.491480220967" />
                  <Point X="-2.981451660156" Y="-27.549112371959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.886451660156" Y="-21.167498474307" />
                  <Point X="-2.886451660156" Y="-21.599616916042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.886451660156" Y="-22.21373298244" />
                  <Point X="-2.886451660156" Y="-27.494264027063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.886451660156" Y="-28.668023759227" />
                  <Point X="-2.886451660156" Y="-28.904239480137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.791451660156" Y="-21.094662885689" />
                  <Point X="-2.791451660156" Y="-21.764161244791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.791451660156" Y="-22.112653713698" />
                  <Point X="-2.791451660156" Y="-27.439415682168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.791451660156" Y="-28.503479004557" />
                  <Point X="-2.791451660156" Y="-28.977386244874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.696451660156" Y="-21.02182729707" />
                  <Point X="-2.696451660156" Y="-27.384567337272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.696451660156" Y="-28.338934249888" />
                  <Point X="-2.696451660156" Y="-29.04282252304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.601451660156" Y="-20.958896317042" />
                  <Point X="-2.601451660156" Y="-27.351691956559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.601451660156" Y="-28.174389495218" />
                  <Point X="-2.601451660156" Y="-29.101644059147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.506451660156" Y="-20.906115893507" />
                  <Point X="-2.506451660156" Y="-27.356266415291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.506451660156" Y="-28.009844740549" />
                  <Point X="-2.506451660156" Y="-29.160465595254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.411451660156" Y="-20.853335469971" />
                  <Point X="-2.411451660156" Y="-27.405132928422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.411451660156" Y="-27.845299985879" />
                  <Point X="-2.411451660156" Y="-29.042908633551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316451660156" Y="-20.800555046436" />
                  <Point X="-2.316451660156" Y="-27.55120856382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316451660156" Y="-27.650304560613" />
                  <Point X="-2.316451660156" Y="-28.963487615399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.221451660156" Y="-20.7477746229" />
                  <Point X="-2.221451660156" Y="-28.900010716387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.126451660156" Y="-20.817812677566" />
                  <Point X="-2.126451660156" Y="-28.836533817375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.031451660156" Y="-20.898789489872" />
                  <Point X="-2.031451660156" Y="-28.788265292664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.936451660156" Y="-20.94758491359" />
                  <Point X="-1.936451660156" Y="-28.776531181649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.841451660156" Y="-20.970942922642" />
                  <Point X="-1.841451660156" Y="-28.782757897786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.746451660156" Y="-20.955132300566" />
                  <Point X="-1.746451660156" Y="-28.788984613923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.651451660156" Y="-20.916154973571" />
                  <Point X="-1.651451660156" Y="-28.795211330059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.556451660156" Y="-20.851382017856" />
                  <Point X="-1.556451660156" Y="-28.818566696315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.461451660156" Y="-20.432351244777" />
                  <Point X="-1.461451660156" Y="-28.883995402647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.366451660156" Y="-20.405716871496" />
                  <Point X="-1.366451660156" Y="-28.967308879703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.271451660156" Y="-20.379082498216" />
                  <Point X="-1.271451660156" Y="-29.050622356758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.176451660156" Y="-20.352448124935" />
                  <Point X="-1.176451660156" Y="-29.23065208169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.081451660156" Y="-20.325813751655" />
                  <Point X="-1.081451660156" Y="-29.746712407768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.986451660156" Y="-20.299179378374" />
                  <Point X="-0.986451660156" Y="-29.76660736762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.891451660156" Y="-20.279030329582" />
                  <Point X="-0.891451660156" Y="-29.416092875964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.796451660156" Y="-20.267911698119" />
                  <Point X="-0.796451660156" Y="-29.061547742349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.701451660156" Y="-20.256793066657" />
                  <Point X="-0.701451660156" Y="-28.707002608735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.606451660156" Y="-20.245674435195" />
                  <Point X="-0.606451660156" Y="-28.410839003841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.511451660156" Y="-20.234555803732" />
                  <Point X="-0.511451660156" Y="-28.273963361352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.416451660156" Y="-20.22343717227" />
                  <Point X="-0.416451660156" Y="-28.14531491483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.321451660156" Y="-20.380796535568" />
                  <Point X="-0.321451660156" Y="-28.086120062889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.226451660156" Y="-20.735341370036" />
                  <Point X="-0.226451660156" Y="-28.056418080516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.131451660156" Y="-20.871193466128" />
                  <Point X="-0.131451660156" Y="-28.026933666282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.036451660156" Y="-20.910061273354" />
                  <Point X="-0.036451660156" Y="-28.000901950012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.058548339844" Y="-20.907860421203" />
                  <Point X="0.058548339844" Y="-28.002379926523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.153548339844" Y="-20.863538774474" />
                  <Point X="0.153548339844" Y="-28.029970694042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.248548339844" Y="-20.698143503259" />
                  <Point X="0.248548339844" Y="-28.059455092288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.343548339844" Y="-20.343953027068" />
                  <Point X="0.343548339844" Y="-28.090151360578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.438548339844" Y="-20.221313037617" />
                  <Point X="0.438548339844" Y="-28.156848848693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.533548339844" Y="-20.231262521082" />
                  <Point X="0.533548339844" Y="-28.288063573281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.628548339844" Y="-20.241212004546" />
                  <Point X="0.628548339844" Y="-28.427328912713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.723548339844" Y="-20.251161488011" />
                  <Point X="0.723548339844" Y="-28.743520495136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818548339844" Y="-20.261110971476" />
                  <Point X="0.818548339844" Y="-27.926808799029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818548339844" Y="-29.041874956871" />
                  <Point X="0.818548339844" Y="-29.098065725647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.913548339844" Y="-20.282774543208" />
                  <Point X="0.913548339844" Y="-27.830765726078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.008548339844" Y="-20.30571070014" />
                  <Point X="1.008548339844" Y="-27.751775628696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.103548339844" Y="-20.328646857071" />
                  <Point X="1.103548339844" Y="-27.687250050351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.198548339844" Y="-20.351583014002" />
                  <Point X="1.198548339844" Y="-27.665828273443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.293548339844" Y="-20.374519170934" />
                  <Point X="1.293548339844" Y="-27.657086548952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.388548339844" Y="-20.397455327865" />
                  <Point X="1.388548339844" Y="-27.64834482446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.483548339844" Y="-20.424020045745" />
                  <Point X="1.483548339844" Y="-27.654261353144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.578548339844" Y="-20.458477633905" />
                  <Point X="1.578548339844" Y="-27.695441226152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.673548339844" Y="-20.492935222066" />
                  <Point X="1.673548339844" Y="-27.756516998271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.768548339844" Y="-20.527392810226" />
                  <Point X="1.768548339844" Y="-27.81759277039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.863548339844" Y="-20.562405596614" />
                  <Point X="1.863548339844" Y="-27.915043361298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.958548339844" Y="-20.606833589714" />
                  <Point X="1.958548339844" Y="-28.038849901414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053548339844" Y="-20.651261582814" />
                  <Point X="2.053548339844" Y="-22.426035166311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053548339844" Y="-22.791997378897" />
                  <Point X="2.053548339844" Y="-27.373339762144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053548339844" Y="-27.837119569633" />
                  <Point X="2.053548339844" Y="-28.16265644153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148548339844" Y="-20.695689575914" />
                  <Point X="2.148548339844" Y="-22.257226254544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148548339844" Y="-22.916359955487" />
                  <Point X="2.148548339844" Y="-27.206986782647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148548339844" Y="-28.010414891849" />
                  <Point X="2.148548339844" Y="-28.286462981646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243548339844" Y="-20.740117569014" />
                  <Point X="2.243548339844" Y="-22.092681272198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243548339844" Y="-22.982923199595" />
                  <Point X="2.243548339844" Y="-27.133758269817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243548339844" Y="-28.174959526021" />
                  <Point X="2.243548339844" Y="-28.410269521762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338548339844" Y="-20.794673355046" />
                  <Point X="2.338548339844" Y="-21.928136289851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338548339844" Y="-23.015769423349" />
                  <Point X="2.338548339844" Y="-27.083760528511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338548339844" Y="-28.339504160193" />
                  <Point X="2.338548339844" Y="-28.534076061878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433548339844" Y="-20.850020613861" />
                  <Point X="2.433548339844" Y="-21.763591307505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433548339844" Y="-23.024579126961" />
                  <Point X="2.433548339844" Y="-27.038871852826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433548339844" Y="-28.504048794364" />
                  <Point X="2.433548339844" Y="-28.657882601995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528548339844" Y="-20.905367872676" />
                  <Point X="2.528548339844" Y="-21.599046325159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528548339844" Y="-23.009368136776" />
                  <Point X="2.528548339844" Y="-27.025675594178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528548339844" Y="-28.668593428536" />
                  <Point X="2.528548339844" Y="-28.781689142111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623548339844" Y="-20.960715131491" />
                  <Point X="2.623548339844" Y="-21.434501342813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623548339844" Y="-22.978281394317" />
                  <Point X="2.623548339844" Y="-27.039688989961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623548339844" Y="-28.833138062708" />
                  <Point X="2.623548339844" Y="-28.905495682227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718548339844" Y="-21.027517465867" />
                  <Point X="2.718548339844" Y="-21.269956360466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718548339844" Y="-22.924485349589" />
                  <Point X="2.718548339844" Y="-27.056845760392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718548339844" Y="-28.99768269688" />
                  <Point X="2.718548339844" Y="-29.029302222343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.813548339844" Y="-21.095075538934" />
                  <Point X="2.813548339844" Y="-21.10541137812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.813548339844" Y="-22.869637146689" />
                  <Point X="2.813548339844" Y="-27.07922710023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.908548339844" Y="-22.814788943789" />
                  <Point X="2.908548339844" Y="-26.198042767232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.908548339844" Y="-26.638049736671" />
                  <Point X="2.908548339844" Y="-27.129368007604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003548339844" Y="-22.759940740889" />
                  <Point X="3.003548339844" Y="-23.562575240638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003548339844" Y="-23.639504579168" />
                  <Point X="3.003548339844" Y="-26.07628270061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003548339844" Y="-26.784563507792" />
                  <Point X="3.003548339844" Y="-27.184216192516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098548339844" Y="-22.705092537989" />
                  <Point X="3.098548339844" Y="-23.339750782837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098548339844" Y="-23.879749927512" />
                  <Point X="3.098548339844" Y="-25.983272783823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098548339844" Y="-26.871386450686" />
                  <Point X="3.098548339844" Y="-27.239064377429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193548339844" Y="-22.650244335089" />
                  <Point X="3.193548339844" Y="-23.240374964168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193548339844" Y="-23.970016918721" />
                  <Point X="3.193548339844" Y="-25.947656494261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193548339844" Y="-26.944282661456" />
                  <Point X="3.193548339844" Y="-27.293912562342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288548339844" Y="-22.595396132189" />
                  <Point X="3.288548339844" Y="-23.167479046967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288548339844" Y="-24.019412639687" />
                  <Point X="3.288548339844" Y="-25.927008182307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288548339844" Y="-27.017178872227" />
                  <Point X="3.288548339844" Y="-27.348760747255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383548339844" Y="-22.540547929289" />
                  <Point X="3.383548339844" Y="-23.094583129765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383548339844" Y="-24.040013441334" />
                  <Point X="3.383548339844" Y="-24.847893617712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383548339844" Y="-25.214682995055" />
                  <Point X="3.383548339844" Y="-25.909528108902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383548339844" Y="-27.090075082997" />
                  <Point X="3.383548339844" Y="-27.403608932168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478548339844" Y="-22.485699726389" />
                  <Point X="3.478548339844" Y="-23.021687212564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478548339844" Y="-24.048370283709" />
                  <Point X="3.478548339844" Y="-24.709446563071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478548339844" Y="-25.35343673121" />
                  <Point X="3.478548339844" Y="-25.914752857854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478548339844" Y="-27.162971293768" />
                  <Point X="3.478548339844" Y="-27.458457117081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573548339844" Y="-22.430851523489" />
                  <Point X="3.573548339844" Y="-22.948791295363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573548339844" Y="-24.037599721083" />
                  <Point X="3.573548339844" Y="-24.637629354497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573548339844" Y="-25.424932612631" />
                  <Point X="3.573548339844" Y="-25.927259730697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573548339844" Y="-27.235867504538" />
                  <Point X="3.573548339844" Y="-27.513305301994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668548339844" Y="-22.376003320589" />
                  <Point X="3.668548339844" Y="-22.875895378162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668548339844" Y="-24.02509268742" />
                  <Point X="3.668548339844" Y="-24.586807368206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668548339844" Y="-25.475563406587" />
                  <Point X="3.668548339844" Y="-25.93976660354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668548339844" Y="-27.308763715309" />
                  <Point X="3.668548339844" Y="-27.568153486907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763548339844" Y="-22.321155117689" />
                  <Point X="3.763548339844" Y="-22.802999460961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763548339844" Y="-24.012585653756" />
                  <Point X="3.763548339844" Y="-24.559471383153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763548339844" Y="-25.503089320638" />
                  <Point X="3.763548339844" Y="-25.952273476383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763548339844" Y="-27.381659926079" />
                  <Point X="3.763548339844" Y="-27.62300167182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858548339844" Y="-22.266306914789" />
                  <Point X="3.858548339844" Y="-22.73010354376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858548339844" Y="-24.000078620093" />
                  <Point X="3.858548339844" Y="-24.534016146169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858548339844" Y="-25.528544350242" />
                  <Point X="3.858548339844" Y="-25.964780349225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858548339844" Y="-27.45455613685" />
                  <Point X="3.858548339844" Y="-27.677849856732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953548339844" Y="-22.237319581797" />
                  <Point X="3.953548339844" Y="-22.657207626559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953548339844" Y="-23.98757158643" />
                  <Point X="3.953548339844" Y="-24.508560909185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953548339844" Y="-25.553999379846" />
                  <Point X="3.953548339844" Y="-25.977287222068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953548339844" Y="-27.52745234762" />
                  <Point X="3.953548339844" Y="-27.732698041645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048548339844" Y="-22.370553196373" />
                  <Point X="4.048548339844" Y="-22.584311709358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048548339844" Y="-23.975064552766" />
                  <Point X="4.048548339844" Y="-24.483105672201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048548339844" Y="-25.579454409449" />
                  <Point X="4.048548339844" Y="-25.989794094911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048548339844" Y="-27.600348558391" />
                  <Point X="4.048548339844" Y="-27.692479874594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.143548339844" Y="-23.962557519103" />
                  <Point X="4.143548339844" Y="-24.457650435217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.143548339844" Y="-25.604909439053" />
                  <Point X="4.143548339844" Y="-26.002300967754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.238548339844" Y="-23.950050485439" />
                  <Point X="4.238548339844" Y="-24.432195198233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.238548339844" Y="-25.630364468657" />
                  <Point X="4.238548339844" Y="-26.014807840597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.333548339844" Y="-23.937543451776" />
                  <Point X="4.333548339844" Y="-24.40673996125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.333548339844" Y="-25.655819498261" />
                  <Point X="4.333548339844" Y="-26.027314713439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.428548339844" Y="-23.925036418112" />
                  <Point X="4.428548339844" Y="-24.381284724266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.428548339844" Y="-25.681274527865" />
                  <Point X="4.428548339844" Y="-26.039821586282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523548339844" Y="-23.912529384449" />
                  <Point X="4.523548339844" Y="-24.355829487282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523548339844" Y="-25.706729557468" />
                  <Point X="4.523548339844" Y="-26.052328459125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.618548339844" Y="-23.900022350785" />
                  <Point X="4.618548339844" Y="-24.330374250298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.618548339844" Y="-25.732184587072" />
                  <Point X="4.618548339844" Y="-26.064835331968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.713548339844" Y="-23.92500929092" />
                  <Point X="4.713548339844" Y="-24.304919013314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.713548339844" Y="-25.757639616676" />
                  <Point X="4.713548339844" Y="-26.077342204811" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998374023438" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.47173928833" Y="-28.537859375" />
                  <Point X="0.464196136475" Y="-28.5213671875" />
                  <Point X="0.300591094971" Y="-28.285642578125" />
                  <Point X="0.274209106445" Y="-28.266359375" />
                  <Point X="0.020976716995" Y="-28.187765625" />
                  <Point X="-0.008791377068" Y="-28.1878046875" />
                  <Point X="-0.262023773193" Y="-28.2663984375" />
                  <Point X="-0.283114105225" Y="-28.279361328125" />
                  <Point X="-0.288432128906" Y="-28.285865234375" />
                  <Point X="-0.45200668335" Y="-28.52154296875" />
                  <Point X="-0.459227050781" Y="-28.537111328125" />
                  <Point X="-0.847743774414" Y="-29.987076171875" />
                  <Point X="-1.100244140625" Y="-29.93806640625" />
                  <Point X="-1.35158972168" Y="-29.873396484375" />
                  <Point X="-1.309181274414" Y="-29.5512734375" />
                  <Point X="-1.309713500977" Y="-29.534607421875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.380672241211" Y="-29.208412109375" />
                  <Point X="-1.386504760742" Y="-29.202435546875" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649432128906" Y="-28.985751953125" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.990007568359" Y="-28.973876953125" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.457094238281" Y="-29.4145" />
                  <Point X="-2.855833251953" Y="-29.167611328125" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.506406005859" Y="-27.629765625" />
                  <Point X="-2.50563671875" Y="-27.62839453125" />
                  <Point X="-2.499826904297" Y="-27.597177734375" />
                  <Point X="-2.513988525391" Y="-27.568755859375" />
                  <Point X="-2.531328369141" Y="-27.551416015625" />
                  <Point X="-2.560157226562" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.842959228516" Y="-28.26589453125" />
                  <Point X="-4.161705078125" Y="-27.84712890625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.164498779297" Y="-26.42369140625" />
                  <Point X="-3.163847412109" Y="-26.423193359375" />
                  <Point X="-3.148444335938" Y="-26.40358203125" />
                  <Point X="-3.145818115234" Y="-26.396" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.136545654297" Y="-26.35200390625" />
                  <Point X="-3.145109130859" Y="-26.32570703125" />
                  <Point X="-3.161173095703" Y="-26.310630859375" />
                  <Point X="-3.187642333984" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-4.803283203125" Y="-26.497078125" />
                  <Point X="-4.927392578125" Y="-26.011193359375" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-3.558205810547" Y="-25.128845703125" />
                  <Point X="-3.541882324219" Y="-25.121416015625" />
                  <Point X="-3.514143310547" Y="-25.1021640625" />
                  <Point X="-3.497806152344" Y="-25.08319921875" />
                  <Point X="-3.494894287109" Y="-25.075892578125" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.48565234375" Y="-25.0164453125" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.514174560547" Y="-24.960376953125" />
                  <Point X="-3.541894287109" Y="-24.94113671875" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.917645019531" Y="-24.00358203125" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.753798339844" Y="-23.605951171875" />
                  <Point X="-3.753112548828" Y="-23.6060390625" />
                  <Point X="-3.731686523438" Y="-23.6040859375" />
                  <Point X="-3.731487304688" Y="-23.604064453125" />
                  <Point X="-3.670279052734" Y="-23.584765625" />
                  <Point X="-3.651704101563" Y="-23.57409765625" />
                  <Point X="-3.639297363281" Y="-23.55663671875" />
                  <Point X="-3.639107421875" Y="-23.556185546875" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610704101562" Y="-23.47563671875" />
                  <Point X="-3.616094726562" Y="-23.45491796875" />
                  <Point X="-3.616331054688" Y="-23.454458984375" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.160016601562" Y="-22.212998046875" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.159568359375" Y="-22.072818359375" />
                  <Point X="-3.159241943359" Y="-22.0730078125" />
                  <Point X="-3.138709472656" Y="-22.079548828125" />
                  <Point X="-3.138471923828" Y="-22.0795703125" />
                  <Point X="-3.052965332031" Y="-22.08705078125" />
                  <Point X="-3.031986816406" Y="-22.084396484375" />
                  <Point X="-3.013980957031" Y="-22.073310546875" />
                  <Point X="-3.013221679688" Y="-22.07256640625" />
                  <Point X="-2.952528564453" Y="-22.011873046875" />
                  <Point X="-2.940918701172" Y="-21.993671875" />
                  <Point X="-2.938064697266" Y="-21.972271484375" />
                  <Point X="-2.938077880859" Y="-21.9721171875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.75287109375" Y="-20.825666015625" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-1.967927124023" Y="-20.712296875" />
                  <Point X="-1.967651000977" Y="-20.712654296875" />
                  <Point X="-1.951688598633" Y="-20.72610546875" />
                  <Point X="-1.951199462891" Y="-20.72636328125" />
                  <Point X="-1.856030883789" Y="-20.77590625" />
                  <Point X="-1.837466796875" Y="-20.7813515625" />
                  <Point X="-1.813759277344" Y="-20.777728515625" />
                  <Point X="-1.714635009766" Y="-20.736669921875" />
                  <Point X="-1.696936157227" Y="-20.7242890625" />
                  <Point X="-1.686110839844" Y="-20.705599609375" />
                  <Point X="-1.686067138672" Y="-20.7054609375" />
                  <Point X="-1.653803833008" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-0.968078491211" Y="-20.096701171875" />
                  <Point X="-0.224200042725" Y="-20.009640625" />
                  <Point X="-0.042236881256" Y="-20.688736328125" />
                  <Point X="-0.02438120842" Y="-20.7154609375" />
                  <Point X="0.006053043365" Y="-20.725794921875" />
                  <Point X="0.036489959717" Y="-20.71546875" />
                  <Point X="0.054352401733" Y="-20.688748046875" />
                  <Point X="0.236637786865" Y="-20.009130859375" />
                  <Point X="0.860216796875" Y="-20.0744375" />
                  <Point X="1.508466064453" Y="-20.2309453125" />
                  <Point X="1.931048217773" Y="-20.384220703125" />
                  <Point X="2.33869140625" Y="-20.574861328125" />
                  <Point X="2.732510253906" Y="-20.80430078125" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.230281738281" Y="-22.49566015625" />
                  <Point X="2.224841308594" Y="-22.50852734375" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202048828125" Y="-22.607708984375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218757080078" Y="-22.699181640625" />
                  <Point X="2.218703613281" Y="-22.69921875" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.274970703125" Y="-22.77581640625" />
                  <Point X="2.338247802734" Y="-22.81875390625" />
                  <Point X="2.360382080078" Y="-22.826939453125" />
                  <Point X="2.360371582031" Y="-22.827025390625" />
                  <Point X="2.42976171875" Y="-22.835392578125" />
                  <Point X="2.448704589844" Y="-22.83404296875" />
                  <Point X="2.528950683594" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.994247070312" Y="-21.968568359375" />
                  <Point X="4.202593261719" Y="-22.258123046875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.289183349609" Y="-23.406482421875" />
                  <Point X="3.279342285156" Y="-23.416205078125" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213108886719" Y="-23.5085390625" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190762207031" Y="-23.608951171875" />
                  <Point X="3.190788330078" Y="-23.609078125" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215670654297" Y="-23.712083984375" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280932373047" Y="-23.801171875" />
                  <Point X="3.280982177734" Y="-23.80119921875" />
                  <Point X="3.350589599609" Y="-23.8403828125" />
                  <Point X="3.368612792969" Y="-23.84638671875" />
                  <Point X="3.4627265625" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.939187988281" Y="-24.048611328125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="3.741817871094" Y="-24.761998046875" />
                  <Point X="3.729041015625" Y="-24.76720703125" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.622237548828" Y="-24.833109375" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556980712891" Y="-24.925314453125" />
                  <Point X="3.556962402344" Y="-24.925384765625" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.5384921875" Y="-25.040732421875" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.158759765625" />
                  <Point X="3.566786376953" Y="-25.158794921875" />
                  <Point X="3.622264648438" Y="-25.22948828125" />
                  <Point X="3.636622802734" Y="-25.24193359375" />
                  <Point X="3.729086425781" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.948432128906" Y="-25.9663984375" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.412736083984" Y="-26.097728515625" />
                  <Point X="3.394745361328" Y="-26.098361328125" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.201515136719" Y="-26.141951171875" />
                  <Point X="3.185390380859" Y="-26.154763671875" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064345214844" Y="-26.31420703125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056427246094" Y="-26.5167265625" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.339073730469" Y="-27.58378515625" />
                  <Point X="4.204134277344" Y="-27.802138671875" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="2.754125976562" Y="-27.259603515625" />
                  <Point X="2.737232666016" Y="-27.253294921875" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.488988037109" Y="-27.21929296875" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288552490234" Y="-27.3347734375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189182373047" Y="-27.546484375" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.986673095703" Y="-29.082087890625" />
                  <Point X="2.835301757812" Y="-29.190208984375" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.683696044922" Y="-27.992763671875" />
                  <Point X="1.670442626953" Y="-27.9803984375" />
                  <Point X="1.45742578125" Y="-27.84344921875" />
                  <Point X="1.425688354492" Y="-27.835728515625" />
                  <Point X="1.192718139648" Y="-27.857166015625" />
                  <Point X="1.171190063477" Y="-27.8643125" />
                  <Point X="1.164966918945" Y="-27.868814453125" />
                  <Point X="0.985349121094" Y="-28.018162109375" />
                  <Point X="0.968429870605" Y="-28.046111328125" />
                  <Point X="0.914642578125" Y="-28.29357421875" />
                  <Point X="0.913930053711" Y="-28.31072265625" />
                  <Point X="1.127642211914" Y="-29.934029296875" />
                  <Point X="0.994338928223" Y="-29.963248046875" />
                  <Point X="0.860200500488" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#116" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-9.6622281E-05" Y="4.355485072851" Z="0" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0" />
                  <Point X="-0.983568725817" Y="4.983827131987" Z="0" />
                  <Point X="-0.983964085579" Y="4.983780860901" />
                  <Point X="-1.75013969171" Y="4.768972089767" Z="0" />
                  <Point X="-1.750522971153" Y="4.768864631653" />
                  <Point X="-1.750501532614" Y="4.768701789618" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0" />
                  <Point X="-1.739909006476" Y="4.340855288267" Z="0" />
                  <Point X="-1.739925146103" Y="4.340804100037" />
                  <Point X="-1.839049462616" Y="4.299745118618" Z="0" />
                  <Point X="-1.839099049568" Y="4.299724578857" />
                  <Point X="-1.934267596543" Y="4.349266541243" Z="0" />
                  <Point X="-1.93431520462" Y="4.349291324615" />
                  <Point X="-1.934415191889" Y="4.349421630621" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0" />
                  <Point X="-2.786204850912" Y="4.24771355319" Z="0" />
                  <Point X="-2.786530971527" Y="4.247532367706" />
                  <Point X="-3.378373192787" Y="3.793773431182" Z="0" />
                  <Point X="-3.378669261932" Y="3.793546438217" />
                  <Point X="-3.378480689645" Y="3.793219821453" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0" />
                  <Point X="-2.994043754697" Y="3.054806330204" Z="0" />
                  <Point X="-2.99404001236" Y="3.054763555527" />
                  <Point X="-3.054733193755" Y="2.994070374131" Z="0" />
                  <Point X="-3.054763555527" Y="2.99404001236" />
                  <Point X="-3.140270135403" Y="3.001520944476" Z="0" />
                  <Point X="-3.14031291008" Y="3.001524686813" />
                  <Point X="-3.140639526844" Y="3.001713259101" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0" />
                  <Point X="-4.207468392491" Y="2.84663088882" Z="0" />
                  <Point X="-4.207675457001" Y="2.846364736557" />
                  <Point X="-4.547484162569" Y="2.264191035628" Z="0" />
                  <Point X="-4.547654151917" Y="2.263899803162" />
                  <Point X="-4.547228605986" Y="2.263573270321" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0" />
                  <Point X="-3.6668373034" Y="1.553732945859" Z="0" />
                  <Point X="-3.666822433472" Y="1.553704380989" />
                  <Point X="-3.691457584023" Y="1.494229862571" Z="0" />
                  <Point X="-3.691469907761" Y="1.494200110435" />
                  <Point X="-3.752865357399" Y="1.474842218578" Z="0" />
                  <Point X="-3.75289607048" Y="1.47483253479" />
                  <Point X="-3.753427870393" Y="1.474902547598" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0" />
                  <Point X="-4.972752604961" Y="1.038223003507" Z="0" />
                  <Point X="-4.9728307724" Y="1.037934541702" />
                  <Point X="-5.06028330493" Y="0.446938670442" Z="0" />
                  <Point X="-5.06032705307" Y="0.446643024683" />
                  <Point X="-5.059585169911" Y="0.446444237687" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0" />
                  <Point X="-3.548821735859" Y="0.029816582101" Z="0" />
                  <Point X="-3.548807859421" Y="0.029806951061" />
                  <Point X="-3.539561367273" Y="1.4903476E-05" Z="0" />
                  <Point X="-3.539556741714" Y="0" />
                  <Point X="-3.548803233862" Y="-0.029792047585" Z="0" />
                  <Point X="-3.548807859421" Y="-0.029806951061" />
                  <Point X="-3.576546859264" Y="-0.049059401033" Z="0" />
                  <Point X="-3.576560735703" Y="-0.049069032073" />
                  <Point X="-3.577302618861" Y="-0.049267819069" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0" />
                  <Point X="-4.982700556755" Y="-0.989399299189" Z="0" />
                  <Point X="-4.982661724091" Y="-0.989670813084" />
                  <Point X="-4.846971203327" Y="-1.520894142777" Z="0" />
                  <Point X="-4.846903324127" Y="-1.521159887314" />
                  <Point X="-4.846089451909" Y="-1.521052739024" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0" />
                  <Point X="-3.192689547062" Y="-1.322442010105" Z="0" />
                  <Point X="-3.192676305771" Y="-1.322449803352" />
                  <Point X="-3.200376949906" Y="-1.352182296574" Z="0" />
                  <Point X="-3.200380802155" Y="-1.352197170258" />
                  <Point X="-3.201032062054" Y="-1.352696899533" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0" />
                  <Point X="-4.209501650333" Y="-2.843640974998" Z="0" />
                  <Point X="-4.209354877472" Y="-2.843887090683" />
                  <Point X="-3.862412441969" Y="-3.299698023081" Z="0" />
                  <Point X="-3.862238883972" Y="-3.299926042557" />
                  <Point X="-3.861594184279" Y="-3.299553825021" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0" />
                  <Point X="-2.555499644876" Y="-2.572830824256" Z="0" />
                  <Point X="-2.555490970612" Y="-2.57283949852" />
                  <Point X="-2.555863188148" Y="-2.573484198213" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0" />
                  <Point X="-2.890680236816" Y="-4.177344523668" Z="0" />
                  <Point X="-2.890475511551" Y="-4.177502155304" />
                  <Point X="-2.451337432861" Y="-4.449405549049" Z="0" />
                  <Point X="-2.451117753983" Y="-4.449541568756" />
                  <Point X="-2.451005306244" Y="-4.449395024061" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0" />
                  <Point X="-1.968384979844" Y="-3.984170551658" Z="0" />
                  <Point X="-1.968255996704" Y="-3.984084367752" />
                  <Point X="-1.65882164371" Y="-4.004365788817" Z="0" />
                  <Point X="-1.658666849136" Y="-4.004375934601" />
                  <Point X="-1.425522609591" Y="-4.208838223696" Z="0" />
                  <Point X="-1.425405979156" Y="-4.208940505981" />
                  <Point X="-1.364908798993" Y="-4.513080085278" Z="0" />
                  <Point X="-1.364878535271" Y="-4.51323223114" />
                  <Point X="-1.364902645528" Y="-4.51341536665" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0" />
                  <Point X="-1.117549686909" Y="-4.955545532227" Z="0" />
                  <Point X="-1.117401838303" Y="-4.955583572388" />
                  <Point X="-0.817817947209" Y="-5.013733861208" Z="0" />
                  <Point X="-0.81766808033" Y="-5.013762950897" />
                  <Point X="-0.81746778968" Y="-5.013015455961" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0" />
                  <Point X="-0.253440943211" Y="-3.28299062705" Z="0" />
                  <Point X="-0.253359079361" Y="-3.282872676849" />
                  <Point X="-0.00012667954" Y="-3.204278685093" Z="0" />
                  <Point X="-1E-12" Y="-3.204239368439" />
                  <Point X="0.253232399821" Y="-3.282833360195" Z="0" />
                  <Point X="0.253359079361" Y="-3.282872676849" />
                  <Point X="0.417004916221" Y="-3.518655128717" Z="0" />
                  <Point X="0.417086780071" Y="-3.518773078918" />
                  <Point X="0.417287070721" Y="-3.519520573854" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0" />
                  <Point X="0.9979199875" Y="-4.981017757893" Z="0" />
                  <Point X="0.998010158539" Y="-4.981001377106" />
                  <Point X="1.176963453293" Y="-4.941774917841" Z="0" />
                  <Point X="1.177052974701" Y="-4.9417552948" />
                  <Point X="1.176943121105" Y="-4.940920874" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0" />
                  <Point X="1.011133108944" Y="-3.025449807405" Z="0" />
                  <Point X="1.01116001606" Y="-3.025326013565" />
                  <Point X="1.191053759277" Y="-2.875750265121" Z="0" />
                  <Point X="1.191143751144" Y="-2.875675439835" />
                  <Point X="1.424113913" Y="-2.854237313628" Z="0" />
                  <Point X="1.424230456352" Y="-2.854226589203" />
                  <Point X="1.637247335374" Y="-2.991176760435" Z="0" />
                  <Point X="1.637353897095" Y="-2.991245269775" />
                  <Point X="1.637866243482" Y="-2.99191297245" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0" />
                  <Point X="2.85717596519" Y="-4.200347187519" Z="0" />
                  <Point X="2.857273578644" Y="-4.200284004211" />
                  <Point X="3.046417984247" Y="-4.06518255949" Z="0" />
                  <Point X="3.04651260376" Y="-4.065114974976" />
                  <Point X="3.046124965906" Y="-4.064443566799" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0" />
                  <Point X="2.232230625868" Y="-2.506316226602" Z="0" />
                  <Point X="2.232211112976" Y="-2.506208181381" />
                  <Point X="2.326642957449" Y="-2.326779955983" Z="0" />
                  <Point X="2.326690196991" Y="-2.326690196991" />
                  <Point X="2.506118422389" Y="-2.232258352518" Z="0" />
                  <Point X="2.506208181381" Y="-2.232211112976" />
                  <Point X="2.722190576911" Y="-2.271217383623" Z="0" />
                  <Point X="2.722298622131" Y="-2.271236896515" />
                  <Point X="2.722970030308" Y="-2.271624534369" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0" />
                  <Point X="4.239634948254" Y="-2.798543760419" Z="0" />
                  <Point X="4.239722251892" Y="-2.798419713974" />
                  <Point X="4.399128802538" Y="-2.540475897312" Z="0" />
                  <Point X="4.399208545685" Y="-2.540346860886" />
                  <Point X="4.398605312824" Y="-2.53988398397" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0" />
                  <Point X="3.092312746644" Y="-1.458380643129" Z="0" />
                  <Point X="3.092262506485" Y="-1.458302497864" />
                  <Point X="3.10798359251" Y="-1.287457696617" Z="0" />
                  <Point X="3.107991456985" Y="-1.287372231483" />
                  <Point X="3.21768065691" Y="-1.155450176954" Z="0" />
                  <Point X="3.217735528946" Y="-1.15538418293" />
                  <Point X="3.399209077835" Y="-1.115940156341" Z="0" />
                  <Point X="3.399299860001" Y="-1.115920424461" />
                  <Point X="3.400053713441" Y="-1.116019671142" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0" />
                  <Point X="4.991396173" Y="-0.94460777697" Z="0" />
                  <Point X="4.991438388824" Y="-0.944422781467" />
                  <Point X="5.047986829281" Y="-0.569348926634" Z="0" />
                  <Point X="5.048015117645" Y="-0.569161295891" />
                  <Point X="5.047365086436" Y="-0.568987120561" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0" />
                  <Point X="3.655488969684" Y="-0.167364943579" Z="0" />
                  <Point X="3.655442714691" Y="-0.167338207364" />
                  <Point X="3.599964476705" Y="-0.096645895049" Z="0" />
                  <Point X="3.599936723709" Y="-0.096610531211" />
                  <Point X="3.581443977714" Y="-4.8305305E-05" Z="0" />
                  <Point X="3.581434726715" Y="-3.9E-11" />
                  <Point X="3.599927472711" Y="0.096562225945" Z="0" />
                  <Point X="3.599936723709" Y="0.096610531211" />
                  <Point X="3.655414961696" Y="0.167302843526" Z="0" />
                  <Point X="3.655442714691" Y="0.167338207364" />
                  <Point X="3.747906444669" Y="0.220783900663" Z="0" />
                  <Point X="3.747952699661" Y="0.220810636878" />
                  <Point X="3.74860273087" Y="0.220984812208" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0" />
                  <Point X="4.982145835638" Y="0.992228941679" Z="0" />
                  <Point X="4.982112884521" Y="0.992440581322" />
                  <Point X="4.88083612895" Y="1.408455042362" Z="0" />
                  <Point X="4.88078546524" Y="1.408663153648" />
                  <Point X="4.880076633215" Y="1.408569834113" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0" />
                  <Point X="3.36900769937" Y="1.234462357044" Z="0" />
                  <Point X="3.368960618973" Y="1.234468579292" />
                  <Point X="3.299353373051" Y="1.273651429772" Z="0" />
                  <Point X="3.299318552017" Y="1.273671030998" />
                  <Point X="3.251285024643" Y="1.346679881275" Z="0" />
                  <Point X="3.251260995865" Y="1.346716403961" />
                  <Point X="3.233600867867" Y="1.432306388617" Z="0" />
                  <Point X="3.233592033386" Y="1.432349205017" />
                  <Point X="3.255105223894" Y="1.509275220335" Z="0" />
                  <Point X="3.25511598587" Y="1.509313702583" />
                  <Point X="3.312869267941" Y="1.584657410085" Z="0" />
                  <Point X="3.312898159027" Y="1.584695100784" />
                  <Point X="3.313465365887" Y="1.5851303339" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0" />
                  <Point X="4.238288539171" Y="2.800574601531" Z="0" />
                  <Point X="4.23818397522" Y="2.800747394562" />
                  <Point X="4.002379123211" Y="3.128462645411" Z="0" />
                  <Point X="4.002261161804" Y="3.128626585007" />
                  <Point X="4.001514970183" Y="3.128195771098" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0" />
                  <Point X="2.429631798863" Y="2.245539909601" Z="0" />
                  <Point X="2.429591655731" Y="2.245529174805" />
                  <Point X="2.360201500535" Y="2.253896342278" Z="0" />
                  <Point X="2.360166788101" Y="2.253900527954" />
                  <Point X="2.296889726639" Y="2.296836593509" Z="0" />
                  <Point X="2.296858072281" Y="2.296858072281" />
                  <Point X="2.253922006726" Y="2.360135133743" Z="0" />
                  <Point X="2.253900527954" Y="2.360166788101" />
                  <Point X="2.245533360481" Y="2.429556943297" Z="0" />
                  <Point X="2.245529174805" Y="2.429591655731" />
                  <Point X="2.266988033056" Y="2.509837777019" Z="0" />
                  <Point X="2.266998767853" Y="2.509877920151" />
                  <Point X="2.267429581761" Y="2.510624111772" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0" />
                  <Point X="2.753685403347" Y="4.268898616076" Z="0" />
                  <Point X="2.753497838974" Y="4.269032001495" />
                  <Point X="2.355961587667" Y="4.500637542009" Z="0" />
                  <Point X="2.355762720108" Y="4.50075340271" />
                  <Point X="1.944237104297" Y="4.693210169554" Z="0" />
                  <Point X="1.944031238556" Y="4.693306446075" />
                  <Point X="1.516951227129" Y="4.848211521626" Z="0" />
                  <Point X="1.516737580299" Y="4.848289012909" />
                  <Point X="0.86288575235" Y="5.006149000645" Z="0" />
                  <Point X="0.862558662891" Y="5.006227970123" />
                  <Point X="0.193579218604" Y="5.076287984848" Z="0" />
                  <Point X="0.193244561553" Y="5.076323032379" />
                  <Point X="0.193147939272" Y="5.0759624331" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>