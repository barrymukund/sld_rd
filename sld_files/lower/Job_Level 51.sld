<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#217" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3620" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.995282226562" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.55772265625" Y="-28.49714453125" />
                  <Point X="0.542365539551" Y="-28.46737890625" />
                  <Point X="0.381134399414" Y="-28.235076171875" />
                  <Point X="0.378637786865" Y="-28.231478515625" />
                  <Point X="0.356758331299" Y="-28.20902734375" />
                  <Point X="0.330502929688" Y="-28.18978125" />
                  <Point X="0.302497375488" Y="-28.175669921875" />
                  <Point X="0.05300151825" Y="-28.098236328125" />
                  <Point X="0.049138343811" Y="-28.097037109375" />
                  <Point X="0.02097927475" Y="-28.092767578125" />
                  <Point X="-0.008665021896" Y="-28.092767578125" />
                  <Point X="-0.036825004578" Y="-28.097037109375" />
                  <Point X="-0.286300628662" Y="-28.174466796875" />
                  <Point X="-0.290081787109" Y="-28.17563671875" />
                  <Point X="-0.318149261475" Y="-28.1897578125" />
                  <Point X="-0.344424133301" Y="-28.2090078125" />
                  <Point X="-0.366323669434" Y="-28.2314765625" />
                  <Point X="-0.527554931641" Y="-28.46378125" />
                  <Point X="-0.534241455078" Y="-28.474986328125" />
                  <Point X="-0.543958190918" Y="-28.494150390625" />
                  <Point X="-0.550990356445" Y="-28.512525390625" />
                  <Point X="-0.557098510742" Y="-28.535322265625" />
                  <Point X="-0.916584411621" Y="-29.876943359375" />
                  <Point X="-1.079046508789" Y="-29.845408203125" />
                  <Point X="-1.079332519531" Y="-29.845353515625" />
                  <Point X="-1.24641796875" Y="-29.80236328125" />
                  <Point X="-1.214962890625" Y="-29.563439453125" />
                  <Point X="-1.214200805664" Y="-29.547931640625" />
                  <Point X="-1.216508300781" Y="-29.516228515625" />
                  <Point X="-1.276112792969" Y="-29.216576171875" />
                  <Point X="-1.277035766602" Y="-29.211935546875" />
                  <Point X="-1.287937133789" Y="-29.182970703125" />
                  <Point X="-1.304009887695" Y="-29.155130859375" />
                  <Point X="-1.323645141602" Y="-29.131205078125" />
                  <Point X="-1.553349121094" Y="-28.929759765625" />
                  <Point X="-1.556906005859" Y="-28.926640625" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612885986328" Y="-28.897994140625" />
                  <Point X="-1.64302734375" Y="-28.890966796875" />
                  <Point X="-1.947895996094" Y="-28.870984375" />
                  <Point X="-1.952651977539" Y="-28.870673828125" />
                  <Point X="-1.983429321289" Y="-28.8737109375" />
                  <Point X="-2.014469482422" Y="-28.882029296875" />
                  <Point X="-2.042657348633" Y="-28.89480078125" />
                  <Point X="-2.296690185547" Y="-29.0645390625" />
                  <Point X="-2.296711669922" Y="-29.0645546875" />
                  <Point X="-2.315645751953" Y="-29.07961328125" />
                  <Point X="-2.330959960938" Y="-29.09521484375" />
                  <Point X="-2.338531982422" Y="-29.103931640625" />
                  <Point X="-2.4801484375" Y="-29.288490234375" />
                  <Point X="-2.801285888672" Y="-29.089650390625" />
                  <Point X="-2.801708251953" Y="-29.089388671875" />
                  <Point X="-3.104721679688" Y="-28.856078125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412859130859" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616126953125" />
                  <Point X="-2.405575195312" Y="-27.5851953125" />
                  <Point X="-2.414559326172" Y="-27.555578125" />
                  <Point X="-2.428776367188" Y="-27.526748046875" />
                  <Point X="-2.4468046875" Y="-27.501587890625" />
                  <Point X="-2.463888671875" Y="-27.48450390625" />
                  <Point X="-2.464393066406" Y="-27.484001953125" />
                  <Point X="-2.489493164062" Y="-27.466091796875" />
                  <Point X="-2.518302734375" Y="-27.45193359375" />
                  <Point X="-2.547890136719" Y="-27.442994140625" />
                  <Point X="-2.578780761719" Y="-27.44402734375" />
                  <Point X="-2.610261230469" Y="-27.4503046875" />
                  <Point X="-2.639185058594" Y="-27.46119921875" />
                  <Point X="-2.658845703125" Y="-27.47255078125" />
                  <Point X="-3.818024658203" Y="-28.1418046875" />
                  <Point X="-4.082527099609" Y="-27.79430078125" />
                  <Point X="-4.082853027344" Y="-27.793873046875" />
                  <Point X="-4.306141601563" Y="-27.419451171875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084576171875" Y="-26.47559375" />
                  <Point X="-3.066611083984" Y="-26.4484609375" />
                  <Point X="-3.053856201172" Y="-26.41983203125" />
                  <Point X="-3.046270996094" Y="-26.390544921875" />
                  <Point X="-3.046157226562" Y="-26.39010546875" />
                  <Point X="-3.043348632812" Y="-26.359673828125" />
                  <Point X="-3.045554199219" Y="-26.328" />
                  <Point X="-3.052553710938" Y="-26.29825" />
                  <Point X="-3.068636474609" Y="-26.272263671875" />
                  <Point X="-3.089469970703" Y="-26.2483046875" />
                  <Point X="-3.112969970703" Y="-26.22876953125" />
                  <Point X="-3.139048828125" Y="-26.213419921875" />
                  <Point X="-3.139772949219" Y="-26.21299609375" />
                  <Point X="-3.1689375" Y="-26.201873046875" />
                  <Point X="-3.200718994141" Y="-26.195453125" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-3.256749023438" Y="-26.19765234375" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.833946777344" Y="-25.993166015625" />
                  <Point X="-4.834077148438" Y="-25.99265625" />
                  <Point X="-4.892423828125" Y="-25.58469921875" />
                  <Point X="-3.532876220703" Y="-25.22041015625" />
                  <Point X="-3.5174921875" Y="-25.214828125" />
                  <Point X="-3.487727783203" Y="-25.19946875" />
                  <Point X="-3.460480957031" Y="-25.180556640625" />
                  <Point X="-3.460102294922" Y="-25.180296875" />
                  <Point X="-3.437595458984" Y="-25.158396484375" />
                  <Point X="-3.418306640625" Y="-25.132109375" />
                  <Point X="-3.404168212891" Y="-25.10406640625" />
                  <Point X="-3.395069824219" Y="-25.07475" />
                  <Point X="-3.394951660156" Y="-25.07437109375" />
                  <Point X="-3.390655029297" Y="-25.0461484375" />
                  <Point X="-3.390647705078" Y="-25.016484375" />
                  <Point X="-3.394916748047" Y="-24.988302734375" />
                  <Point X="-3.404026611328" Y="-24.95894921875" />
                  <Point X="-3.404169921875" Y="-24.958486328125" />
                  <Point X="-3.418267822266" Y="-24.930509765625" />
                  <Point X="-3.437517089844" Y="-24.9042421875" />
                  <Point X="-3.459980224609" Y="-24.882349609375" />
                  <Point X="-3.487225341797" Y="-24.86344140625" />
                  <Point X="-3.500895751953" Y="-24.85555078125" />
                  <Point X="-3.532875" Y="-24.84215234375" />
                  <Point X="-3.555499511719" Y="-24.83608984375" />
                  <Point X="-4.89181640625" Y="-24.478025390625" />
                  <Point X="-4.824570800781" Y="-24.0235859375" />
                  <Point X="-4.824487304688" Y="-24.023021484375" />
                  <Point X="-4.703551269531" Y="-23.576732421875" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985107422" Y="-23.700658203125" />
                  <Point X="-3.723423339844" Y="-23.698771484375" />
                  <Point X="-3.703138183594" Y="-23.694736328125" />
                  <Point X="-3.642696777344" Y="-23.6756796875" />
                  <Point X="-3.641760253906" Y="-23.675384765625" />
                  <Point X="-3.622813720703" Y="-23.6670546875" />
                  <Point X="-3.604059326172" Y="-23.656232421875" />
                  <Point X="-3.587372070312" Y="-23.644001953125" />
                  <Point X="-3.573728027344" Y="-23.62844921875" />
                  <Point X="-3.561307128906" Y="-23.61071484375" />
                  <Point X="-3.551351806641" Y="-23.5925703125" />
                  <Point X="-3.527080078125" Y="-23.533974609375" />
                  <Point X="-3.5266875" Y="-23.53302734375" />
                  <Point X="-3.520902587891" Y="-23.51316015625" />
                  <Point X="-3.517152587891" Y="-23.49185546875" />
                  <Point X="-3.515805419922" Y="-23.4712265625" />
                  <Point X="-3.518952392578" Y="-23.450794921875" />
                  <Point X="-3.524552001953" Y="-23.429900390625" />
                  <Point X="-3.532048095703" Y="-23.410625" />
                  <Point X="-3.561334472656" Y="-23.3543671875" />
                  <Point X="-3.561787841797" Y="-23.35349609375" />
                  <Point X="-3.573277099609" Y="-23.33630078125" />
                  <Point X="-3.587191650391" Y="-23.319716796875" />
                  <Point X="-3.602132324219" Y="-23.3054140625" />
                  <Point X="-3.615109863281" Y="-23.295455078125" />
                  <Point X="-4.351860351563" Y="-22.730126953125" />
                  <Point X="-4.081476074219" Y="-22.266892578125" />
                  <Point X="-4.081155761719" Y="-22.26634375" />
                  <Point X="-3.750503417969" Y="-21.841337890625" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187729736328" Y="-22.163658203125" />
                  <Point X="-3.167087890625" Y="-22.17016796875" />
                  <Point X="-3.146795654297" Y="-22.174205078125" />
                  <Point X="-3.06255078125" Y="-22.181576171875" />
                  <Point X="-3.061093261719" Y="-22.181701171875" />
                  <Point X="-3.040485595703" Y="-22.181234375" />
                  <Point X="-3.019043701172" Y="-22.178400390625" />
                  <Point X="-2.998971191406" Y="-22.17348046875" />
                  <Point X="-2.980437744141" Y="-22.164333984375" />
                  <Point X="-2.962198486328" Y="-22.1527109375" />
                  <Point X="-2.946077636719" Y="-22.139771484375" />
                  <Point X="-2.886280029297" Y="-22.079974609375" />
                  <Point X="-2.885340576172" Y="-22.07903515625" />
                  <Point X="-2.872416748047" Y="-22.0629296875" />
                  <Point X="-2.860783691406" Y="-22.044673828125" />
                  <Point X="-2.851631835938" Y="-22.02612109375" />
                  <Point X="-2.846713623047" Y="-22.006025390625" />
                  <Point X="-2.843887451172" Y="-21.984564453125" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.850806396484" Y="-21.879634765625" />
                  <Point X="-2.850920166016" Y="-21.878333984375" />
                  <Point X="-2.854954101562" Y="-21.85805078125" />
                  <Point X="-2.861462890625" Y="-21.837404296875" />
                  <Point X="-2.869798339844" Y="-21.8184609375" />
                  <Point X="-2.875549072266" Y="-21.808501953125" />
                  <Point X="-3.183332519531" Y="-21.27540625" />
                  <Point X="-2.701181396484" Y="-20.905744140625" />
                  <Point X="-2.700622070312" Y="-20.90531640625" />
                  <Point X="-2.167035888672" Y="-20.608865234375" />
                  <Point X="-2.04319519043" Y="-20.7702578125" />
                  <Point X="-2.028896850586" Y="-20.7851953125" />
                  <Point X="-2.012319335938" Y="-20.799107421875" />
                  <Point X="-1.995116699219" Y="-20.810603515625" />
                  <Point X="-1.901352416992" Y="-20.859416015625" />
                  <Point X="-1.899900634766" Y="-20.860171875" />
                  <Point X="-1.880626342773" Y="-20.86766796875" />
                  <Point X="-1.859718505859" Y="-20.873271484375" />
                  <Point X="-1.839269775391" Y="-20.876419921875" />
                  <Point X="-1.818624389648" Y="-20.87506640625" />
                  <Point X="-1.797307861328" Y="-20.871306640625" />
                  <Point X="-1.777453735352" Y="-20.865517578125" />
                  <Point X="-1.679791870117" Y="-20.825064453125" />
                  <Point X="-1.678251953125" Y="-20.824427734375" />
                  <Point X="-1.660111816406" Y="-20.814470703125" />
                  <Point X="-1.642391601562" Y="-20.802056640625" />
                  <Point X="-1.626848266602" Y="-20.788419921875" />
                  <Point X="-1.614623535156" Y="-20.771744140625" />
                  <Point X="-1.603807006836" Y="-20.753005859375" />
                  <Point X="-1.595479980469" Y="-20.734078125" />
                  <Point X="-1.563702270508" Y="-20.633291015625" />
                  <Point X="-1.563209838867" Y="-20.63173046875" />
                  <Point X="-1.559168457031" Y="-20.6114296875" />
                  <Point X="-1.557279785156" Y="-20.589861328125" />
                  <Point X="-1.557730712891" Y="-20.569171875" />
                  <Point X="-1.558384521484" Y="-20.56420703125" />
                  <Point X="-1.584202148438" Y="-20.3681015625" />
                  <Point X="-0.950351806641" Y="-20.190392578125" />
                  <Point X="-0.949633789063" Y="-20.19019140625" />
                  <Point X="-0.294711364746" Y="-20.11354296875" />
                  <Point X="-0.133903289795" Y="-20.713685546875" />
                  <Point X="-0.121765380859" Y="-20.743474609375" />
                  <Point X="-0.109663383484" Y="-20.763236328125" />
                  <Point X="-0.093137512207" Y="-20.779478515625" />
                  <Point X="-0.067945854187" Y="-20.79880859375" />
                  <Point X="-0.040650428772" Y="-20.8133984375" />
                  <Point X="-0.010113883972" Y="-20.818439453125" />
                  <Point X="0.022425701141" Y="-20.818439453125" />
                  <Point X="0.052962341309" Y="-20.8133984375" />
                  <Point X="0.080257827759" Y="-20.79880859375" />
                  <Point X="0.105449478149" Y="-20.779478515625" />
                  <Point X="0.123928771973" Y="-20.76069140625" />
                  <Point X="0.136536407471" Y="-20.737552734375" />
                  <Point X="0.146317962646" Y="-20.711544921875" />
                  <Point X="0.149161819458" Y="-20.702689453125" />
                  <Point X="0.307419555664" Y="-20.112060546875" />
                  <Point X="0.843407958984" Y="-20.1681953125" />
                  <Point X="0.844043273926" Y="-20.168263671875" />
                  <Point X="1.478888916016" Y="-20.32153515625" />
                  <Point X="1.481035888672" Y="-20.322052734375" />
                  <Point X="1.89423046875" Y="-20.471921875" />
                  <Point X="1.894643554688" Y="-20.472072265625" />
                  <Point X="2.294217285156" Y="-20.658939453125" />
                  <Point X="2.294578369141" Y="-20.659109375" />
                  <Point X="2.680596679688" Y="-20.88400390625" />
                  <Point X="2.680991455078" Y="-20.884234375" />
                  <Point X="2.943260498047" Y="-21.070744140625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074707031" Y="-22.460072265625" />
                  <Point X="2.133076171875" Y="-22.483947265625" />
                  <Point X="2.111933837891" Y="-22.563009765625" />
                  <Point X="2.109032226562" Y="-22.579720703125" />
                  <Point X="2.107367919922" Y="-22.599845703125" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.115968505859" Y="-22.68738671875" />
                  <Point X="2.116095947266" Y="-22.6884453125" />
                  <Point X="2.121437744141" Y="-22.710376953125" />
                  <Point X="2.129704833984" Y="-22.732474609375" />
                  <Point X="2.140070556641" Y="-22.752529296875" />
                  <Point X="2.182373291016" Y="-22.814873046875" />
                  <Point X="2.193074951172" Y="-22.827962890625" />
                  <Point X="2.221598632812" Y="-22.854408203125" />
                  <Point X="2.283921386719" Y="-22.896697265625" />
                  <Point X="2.284850585938" Y="-22.897328125" />
                  <Point X="2.304877197266" Y="-22.907689453125" />
                  <Point X="2.327002929688" Y="-22.91598046875" />
                  <Point X="2.348967285156" Y="-22.921337890625" />
                  <Point X="2.417333496094" Y="-22.929580078125" />
                  <Point X="2.434447998047" Y="-22.93008984375" />
                  <Point X="2.473206787109" Y="-22.925830078125" />
                  <Point X="2.552268798828" Y="-22.9046875" />
                  <Point X="2.563049316406" Y="-22.9011015625" />
                  <Point X="2.588530273438" Y="-22.889857421875" />
                  <Point X="2.611286132812" Y="-22.876720703125" />
                  <Point X="3.967326904297" Y="-22.09380859375" />
                  <Point X="4.12304296875" Y="-22.31021875" />
                  <Point X="4.123269042969" Y="-22.310533203125" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221423339844" Y="-23.339759765625" />
                  <Point X="3.203973388672" Y="-23.358373046875" />
                  <Point X="3.147085693359" Y="-23.432587890625" />
                  <Point X="3.138089111328" Y="-23.446763671875" />
                  <Point X="3.128725341797" Y="-23.464880859375" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.100434082031" Y="-23.558705078125" />
                  <Point X="3.100104248047" Y="-23.559884765625" />
                  <Point X="3.096650878906" Y="-23.5821875" />
                  <Point X="3.0958359375" Y="-23.60575" />
                  <Point X="3.097739013672" Y="-23.62823046875" />
                  <Point X="3.115138427734" Y="-23.712556640625" />
                  <Point X="3.119963134766" Y="-23.7286171875" />
                  <Point X="3.127431152344" Y="-23.747302734375" />
                  <Point X="3.136282470703" Y="-23.764259765625" />
                  <Point X="3.183607177734" Y="-23.83619140625" />
                  <Point X="3.184333007812" Y="-23.837294921875" />
                  <Point X="3.198913818359" Y="-23.85457421875" />
                  <Point X="3.216149169922" Y="-23.870650390625" />
                  <Point X="3.234345947266" Y="-23.88396484375" />
                  <Point X="3.302926025391" Y="-23.9225703125" />
                  <Point X="3.318428955078" Y="-23.92955078125" />
                  <Point X="3.337467285156" Y="-23.936146484375" />
                  <Point X="3.356119628906" Y="-23.9405625" />
                  <Point X="3.448844482422" Y="-23.95281640625" />
                  <Point X="3.460025146484" Y="-23.953626953125" />
                  <Point X="3.488202880859" Y="-23.953015625" />
                  <Point X="3.509819580078" Y="-23.950169921875" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845839355469" Y="-24.066794921875" />
                  <Point X="4.845935546875" Y="-24.067189453125" />
                  <Point X="4.890864746094" Y="-24.35576171875" />
                  <Point X="3.716580078125" Y="-24.67041015625" />
                  <Point X="3.704788818359" Y="-24.674416015625" />
                  <Point X="3.681546630859" Y="-24.68493359375" />
                  <Point X="3.590447265625" Y="-24.73758984375" />
                  <Point X="3.5768125" Y="-24.74715625" />
                  <Point X="3.561091308594" Y="-24.760388671875" />
                  <Point X="3.547532226562" Y="-24.774419921875" />
                  <Point X="3.492872558594" Y="-24.8440703125" />
                  <Point X="3.492026367188" Y="-24.8451484375" />
                  <Point X="3.480302978516" Y="-24.864427734375" />
                  <Point X="3.470527587891" Y="-24.88589453125" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.4454609375" Y="-25.002533203125" />
                  <Point X="3.443774658203" Y="-25.019080078125" />
                  <Point X="3.443492431641" Y="-25.03936328125" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.463398681641" Y="-25.15369140625" />
                  <Point X="3.463681884766" Y="-25.155169921875" />
                  <Point X="3.470531005859" Y="-25.176677734375" />
                  <Point X="3.480305419922" Y="-25.198138671875" />
                  <Point X="3.492026367188" Y="-25.217412109375" />
                  <Point X="3.546686035156" Y="-25.2870625" />
                  <Point X="3.558244628906" Y="-25.299361328125" />
                  <Point X="3.573401367188" Y="-25.312857421875" />
                  <Point X="3.589036376953" Y="-25.32415625" />
                  <Point X="3.680111816406" Y="-25.376798828125" />
                  <Point X="3.689981445312" Y="-25.38176171875" />
                  <Point X="3.716584960938" Y="-25.39215234375" />
                  <Point X="3.736408447266" Y="-25.397462890625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.855077148438" Y="-25.94836328125" />
                  <Point X="4.855020996094" Y="-25.948734375" />
                  <Point X="4.801174316406" Y="-26.18469921875" />
                  <Point X="3.424382324219" Y="-26.00344140625" />
                  <Point X="3.40803515625" Y="-26.0027109375" />
                  <Point X="3.374661132812" Y="-26.005509765625" />
                  <Point X="3.195865234375" Y="-26.04437109375" />
                  <Point X="3.193096923828" Y="-26.04497265625" />
                  <Point X="3.163978759766" Y="-26.056595703125" />
                  <Point X="3.136149902344" Y="-26.07348828125" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.004332763672" Y="-26.2239296875" />
                  <Point X="3.002675537109" Y="-26.225921875" />
                  <Point X="2.987940917969" Y="-26.250318359375" />
                  <Point X="2.976591064453" Y="-26.27771484375" />
                  <Point X="2.969757080078" Y="-26.305369140625" />
                  <Point X="2.954267822266" Y="-26.473693359375" />
                  <Point X="2.954028076172" Y="-26.47630078125" />
                  <Point X="2.95634765625" Y="-26.507572265625" />
                  <Point X="2.964080078125" Y="-26.53919140625" />
                  <Point X="2.976450927734" Y="-26.568" />
                  <Point X="3.075399169922" Y="-26.72190625" />
                  <Point X="3.083856933594" Y="-26.733138671875" />
                  <Point X="3.110625" Y="-26.76090625" />
                  <Point X="3.129021240234" Y="-26.7750234375" />
                  <Point X="4.213123046875" Y="-27.6068828125" />
                  <Point X="4.1249609375" Y="-27.74954296875" />
                  <Point X="4.124810058594" Y="-27.749787109375" />
                  <Point X="4.028979980469" Y="-27.885947265625" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129638672" Y="-27.170013671875" />
                  <Point X="2.754225341797" Y="-27.159826171875" />
                  <Point X="2.541429931641" Y="-27.12139453125" />
                  <Point X="2.538091064453" Y="-27.12079296875" />
                  <Point X="2.506745361328" Y="-27.120396484375" />
                  <Point X="2.474591796875" Y="-27.125357421875" />
                  <Point X="2.444833984375" Y="-27.135177734375" />
                  <Point X="2.268053222656" Y="-27.22821484375" />
                  <Point X="2.265272460938" Y="-27.2296796875" />
                  <Point X="2.242372314453" Y="-27.24655859375" />
                  <Point X="2.221424560547" Y="-27.2675078125" />
                  <Point X="2.204533935547" Y="-27.290435546875" />
                  <Point X="2.111495361328" Y="-27.467216796875" />
                  <Point X="2.110054931641" Y="-27.469953125" />
                  <Point X="2.100232177734" Y="-27.49972265625" />
                  <Point X="2.095271972656" Y="-27.5318984375" />
                  <Point X="2.095675292969" Y="-27.563255859375" />
                  <Point X="2.134105957031" Y="-27.776052734375" />
                  <Point X="2.137507568359" Y="-27.789328125" />
                  <Point X="2.144005615234" Y="-27.80873828125" />
                  <Point X="2.151820068359" Y="-27.82608203125" />
                  <Point X="2.163641601562" Y="-27.846556640625" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.782035644531" Y="-29.11151171875" />
                  <Point X="2.781881347656" Y="-29.11162109375" />
                  <Point X="2.701763916016" Y="-29.16348046875" />
                  <Point X="1.758545898438" Y="-27.934255859375" />
                  <Point X="1.747507202148" Y="-27.92218359375" />
                  <Point X="1.721924194336" Y="-27.900556640625" />
                  <Point X="1.512050415039" Y="-27.765626953125" />
                  <Point X="1.508764892578" Y="-27.763515625" />
                  <Point X="1.47996081543" Y="-27.75115625" />
                  <Point X="1.448352172852" Y="-27.74343359375" />
                  <Point X="1.417100341797" Y="-27.741119140625" />
                  <Point X="1.187579956055" Y="-27.76223828125" />
                  <Point X="1.184048583984" Y="-27.7625625" />
                  <Point X="1.15638293457" Y="-27.769392578125" />
                  <Point X="1.128985351563" Y="-27.78073828125" />
                  <Point X="1.104595581055" Y="-27.7954609375" />
                  <Point X="0.927356262207" Y="-27.942830078125" />
                  <Point X="0.924600952148" Y="-27.94512109375" />
                  <Point X="0.904125793457" Y="-27.96887890625" />
                  <Point X="0.887245361328" Y="-27.99669140625" />
                  <Point X="0.875625244141" Y="-28.0258046875" />
                  <Point X="0.822631652832" Y="-28.2696171875" />
                  <Point X="0.820718566895" Y="-28.282845703125" />
                  <Point X="0.819184020996" Y="-28.30376953125" />
                  <Point X="0.81974230957" Y="-28.3231171875" />
                  <Point X="0.823092224121" Y="-28.348564453125" />
                  <Point X="1.022065612793" Y="-29.859916015625" />
                  <Point X="0.97586328125" Y="-29.87004296875" />
                  <Point X="0.975732971191" Y="-29.870072265625" />
                  <Point X="0.929315368652" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058439453125" Y="-29.752634765625" />
                  <Point X="-1.14124621582" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451416016" Y="-29.54103515625" />
                  <Point X="-1.121759033203" Y="-29.50933203125" />
                  <Point X="-1.123333740234" Y="-29.4976953125" />
                  <Point X="-1.182938232422" Y="-29.19804296875" />
                  <Point X="-1.188124511719" Y="-29.17847265625" />
                  <Point X="-1.199026000977" Y="-29.1495078125" />
                  <Point X="-1.20566394043" Y="-29.13547265625" />
                  <Point X="-1.221736694336" Y="-29.1076328125" />
                  <Point X="-1.230573730469" Y="-29.09486328125" />
                  <Point X="-1.250208984375" Y="-29.0709375" />
                  <Point X="-1.261007202148" Y="-29.05978125" />
                  <Point X="-1.490711181641" Y="-28.8583359375" />
                  <Point X="-1.506735473633" Y="-28.84596875" />
                  <Point X="-1.533018554688" Y="-28.829623046875" />
                  <Point X="-1.546834228516" Y="-28.822525390625" />
                  <Point X="-1.57653125" Y="-28.810224609375" />
                  <Point X="-1.591315673828" Y="-28.805474609375" />
                  <Point X="-1.62145690918" Y="-28.798447265625" />
                  <Point X="-1.636813964844" Y="-28.796169921875" />
                  <Point X="-1.941682617188" Y="-28.7761875" />
                  <Point X="-1.941705932617" Y="-28.776185546875" />
                  <Point X="-1.961981323242" Y="-28.7761328125" />
                  <Point X="-1.992758666992" Y="-28.779169921875" />
                  <Point X="-2.008020385742" Y="-28.78194921875" />
                  <Point X="-2.039060546875" Y="-28.790267578125" />
                  <Point X="-2.053676025391" Y="-28.79549609375" />
                  <Point X="-2.081863769531" Y="-28.808267578125" />
                  <Point X="-2.095436279297" Y="-28.815810546875" />
                  <Point X="-2.349469238281" Y="-28.985548828125" />
                  <Point X="-2.355845214844" Y="-28.990203125" />
                  <Point X="-2.374779296875" Y="-29.00526171875" />
                  <Point X="-2.383442382812" Y="-29.013064453125" />
                  <Point X="-2.398756591797" Y="-29.028666015625" />
                  <Point X="-2.413900634766" Y="-29.046099609375" />
                  <Point X="-2.503201660156" Y="-29.162478515625" />
                  <Point X="-2.747581298828" Y="-29.011166015625" />
                  <Point X="-2.980862304688" Y="-28.831546875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684570312" Y="-27.666185546875" />
                  <Point X="-2.313413574219" Y="-27.63466015625" />
                  <Point X="-2.311638916016" Y="-27.619236328125" />
                  <Point X="-2.310625976562" Y="-27.5883046875" />
                  <Point X="-2.314665771484" Y="-27.557619140625" />
                  <Point X="-2.323649902344" Y="-27.528001953125" />
                  <Point X="-2.329355957031" Y="-27.5135625" />
                  <Point X="-2.343572998047" Y="-27.484732421875" />
                  <Point X="-2.351554199219" Y="-27.471416015625" />
                  <Point X="-2.369582519531" Y="-27.446255859375" />
                  <Point X="-2.379629638672" Y="-27.434412109375" />
                  <Point X="-2.396713623047" Y="-27.417328125" />
                  <Point X="-2.409213134766" Y="-27.406669921875" />
                  <Point X="-2.434313232422" Y="-27.388759765625" />
                  <Point X="-2.447592773438" Y="-27.38083203125" />
                  <Point X="-2.47640234375" Y="-27.366673828125" />
                  <Point X="-2.490826416016" Y="-27.360994140625" />
                  <Point X="-2.520413818359" Y="-27.3520546875" />
                  <Point X="-2.551065917969" Y="-27.348046875" />
                  <Point X="-2.581956542969" Y="-27.349080078125" />
                  <Point X="-2.597358398438" Y="-27.350861328125" />
                  <Point X="-2.628838867188" Y="-27.357138671875" />
                  <Point X="-2.643747558594" Y="-27.36140234375" />
                  <Point X="-2.672671386719" Y="-27.372296875" />
                  <Point X="-2.686686523438" Y="-27.378927734375" />
                  <Point X="-2.706347167969" Y="-27.390279296875" />
                  <Point X="-3.79308984375" Y="-28.017712890625" />
                  <Point X="-4.004007568359" Y="-27.740607421875" />
                  <Point X="-4.181264160156" Y="-27.443375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481201172" Y="-26.563310546875" />
                  <Point X="-3.015102783203" Y="-26.540388671875" />
                  <Point X="-3.005365478516" Y="-26.528041015625" />
                  <Point X="-2.987400390625" Y="-26.500908203125" />
                  <Point X="-2.979833740234" Y="-26.487123046875" />
                  <Point X="-2.967078857422" Y="-26.458494140625" />
                  <Point X="-2.961890625" Y="-26.443650390625" />
                  <Point X="-2.954305419922" Y="-26.41436328125" />
                  <Point X="-2.951559326172" Y="-26.3988359375" />
                  <Point X="-2.948750732422" Y="-26.368404296875" />
                  <Point X="-2.948578125" Y="-26.35307421875" />
                  <Point X="-2.950783691406" Y="-26.321400390625" />
                  <Point X="-2.953079345703" Y="-26.3062421875" />
                  <Point X="-2.960078857422" Y="-26.2764921875" />
                  <Point X="-2.971772949219" Y="-26.248255859375" />
                  <Point X="-2.987855712891" Y="-26.22226953125" />
                  <Point X="-2.996948242188" Y="-26.209927734375" />
                  <Point X="-3.017781738281" Y="-26.18596875" />
                  <Point X="-3.028740966797" Y="-26.17525" />
                  <Point X="-3.052240966797" Y="-26.15571484375" />
                  <Point X="-3.064781738281" Y="-26.1468984375" />
                  <Point X="-3.090860595703" Y="-26.131548828125" />
                  <Point X="-3.105919433594" Y="-26.124232421875" />
                  <Point X="-3.135083984375" Y="-26.113109375" />
                  <Point X="-3.150127197266" Y="-26.10875390625" />
                  <Point X="-3.181908691406" Y="-26.102333984375" />
                  <Point X="-3.197468994141" Y="-26.1005078125" />
                  <Point X="-3.228679199219" Y="-26.099439453125" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-3.269148925781" Y="-26.10346484375" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.740762207031" Y="-25.974115234375" />
                  <Point X="-4.786452148438" Y="-25.65465625" />
                  <Point X="-3.508288330078" Y="-25.312173828125" />
                  <Point X="-3.500472900391" Y="-25.309712890625" />
                  <Point X="-3.473927490234" Y="-25.29925" />
                  <Point X="-3.444163085938" Y="-25.283890625" />
                  <Point X="-3.433558105469" Y="-25.27751171875" />
                  <Point X="-3.406412597656" Y="-25.258669921875" />
                  <Point X="-3.393850585938" Y="-25.2483828125" />
                  <Point X="-3.37134375" Y="-25.226482421875" />
                  <Point X="-3.361003173828" Y="-25.21459765625" />
                  <Point X="-3.341714355469" Y="-25.188310546875" />
                  <Point X="-3.333478027344" Y="-25.174876953125" />
                  <Point X="-3.319339599609" Y="-25.146833984375" />
                  <Point X="-3.313437255859" Y="-25.132224609375" />
                  <Point X="-3.304377685547" Y="-25.103033203125" />
                  <Point X="-3.301033691406" Y="-25.088669921875" />
                  <Point X="-3.296737060547" Y="-25.060447265625" />
                  <Point X="-3.295655029297" Y="-25.046171875" />
                  <Point X="-3.295647705078" Y="-25.0165078125" />
                  <Point X="-3.296719238281" Y="-25.002255859375" />
                  <Point X="-3.30098828125" Y="-24.97407421875" />
                  <Point X="-3.304185791016" Y="-24.96014453125" />
                  <Point X="-3.313295654297" Y="-24.930791015625" />
                  <Point X="-3.319332763672" Y="-24.915734375" />
                  <Point X="-3.333430664062" Y="-24.8877578125" />
                  <Point X="-3.341640380859" Y="-24.87435546875" />
                  <Point X="-3.360889648438" Y="-24.848087890625" />
                  <Point X="-3.371211425781" Y="-24.836208984375" />
                  <Point X="-3.393674560547" Y="-24.81431640625" />
                  <Point X="-3.405815917969" Y="-24.804302734375" />
                  <Point X="-3.433061035156" Y="-24.78539453125" />
                  <Point X="-3.439734375" Y="-24.7811640625" />
                  <Point X="-3.464185302734" Y="-24.7679296875" />
                  <Point X="-3.496164550781" Y="-24.75453125" />
                  <Point X="-3.508286132812" Y="-24.750390625" />
                  <Point X="-3.530910644531" Y="-24.744328125" />
                  <Point X="-4.785446289062" Y="-24.408177734375" />
                  <Point X="-4.731330078125" Y="-24.04246484375" />
                  <Point X="-4.633586425781" Y="-23.681763671875" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057128906" Y="-23.795634765625" />
                  <Point X="-3.736703857422" Y="-23.795296875" />
                  <Point X="-3.715142089844" Y="-23.79341015625" />
                  <Point X="-3.704888916016" Y="-23.7919453125" />
                  <Point X="-3.684603759766" Y="-23.78791015625" />
                  <Point X="-3.674571777344" Y="-23.78533984375" />
                  <Point X="-3.614130371094" Y="-23.766283203125" />
                  <Point X="-3.603524658203" Y="-23.762349609375" />
                  <Point X="-3.584578125" Y="-23.75401953125" />
                  <Point X="-3.57533203125" Y="-23.749337890625" />
                  <Point X="-3.556577636719" Y="-23.738515625" />
                  <Point X="-3.547900146484" Y="-23.73285546875" />
                  <Point X="-3.531212890625" Y="-23.720625" />
                  <Point X="-3.515958007812" Y="-23.70665234375" />
                  <Point X="-3.502313964844" Y="-23.691099609375" />
                  <Point X="-3.495915039062" Y="-23.68294921875" />
                  <Point X="-3.483494140625" Y="-23.66521484375" />
                  <Point X="-3.478019775391" Y="-23.656412109375" />
                  <Point X="-3.468064453125" Y="-23.638267578125" />
                  <Point X="-3.463583496094" Y="-23.62892578125" />
                  <Point X="-3.439311767578" Y="-23.570330078125" />
                  <Point X="-3.435475585938" Y="-23.5595859375" />
                  <Point X="-3.429690673828" Y="-23.53971875" />
                  <Point X="-3.427340820312" Y="-23.52962890625" />
                  <Point X="-3.423590820312" Y="-23.50832421875" />
                  <Point X="-3.422354492188" Y="-23.498046875" />
                  <Point X="-3.421007324219" Y="-23.47741796875" />
                  <Point X="-3.421912597656" Y="-23.456765625" />
                  <Point X="-3.425059570312" Y="-23.436333984375" />
                  <Point X="-3.427190429688" Y="-23.426203125" />
                  <Point X="-3.432790039062" Y="-23.40530859375" />
                  <Point X="-3.43601171875" Y="-23.395466796875" />
                  <Point X="-3.4435078125" Y="-23.37619140625" />
                  <Point X="-3.447782226562" Y="-23.3667578125" />
                  <Point X="-3.477068603516" Y="-23.3105" />
                  <Point X="-3.482797607422" Y="-23.30071875" />
                  <Point X="-3.494286865234" Y="-23.2835234375" />
                  <Point X="-3.500500488281" Y="-23.27523828125" />
                  <Point X="-3.514415039062" Y="-23.258654296875" />
                  <Point X="-3.521497558594" Y="-23.251091796875" />
                  <Point X="-3.536438232422" Y="-23.2367890625" />
                  <Point X="-3.557273925781" Y="-23.22008984375" />
                  <Point X="-4.227615234375" Y="-22.70571875" />
                  <Point X="-4.002294677734" Y="-22.319689453125" />
                  <Point X="-3.726337158203" Y="-21.964986328125" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244925292969" Y="-22.242279296875" />
                  <Point X="-3.225998046875" Y="-22.250609375" />
                  <Point X="-3.216302490234" Y="-22.254259765625" />
                  <Point X="-3.195660644531" Y="-22.26076953125" />
                  <Point X="-3.185624755859" Y="-22.263341796875" />
                  <Point X="-3.165332519531" Y="-22.26737890625" />
                  <Point X="-3.155076171875" Y="-22.26884375" />
                  <Point X="-3.070831298828" Y="-22.27621484375" />
                  <Point X="-3.058941894531" Y="-22.276677734375" />
                  <Point X="-3.038334228516" Y="-22.2762109375" />
                  <Point X="-3.028037597656" Y="-22.275416015625" />
                  <Point X="-3.006595703125" Y="-22.27258203125" />
                  <Point X="-2.996427978516" Y="-22.270669921875" />
                  <Point X="-2.97635546875" Y="-22.26575" />
                  <Point X="-2.956928710938" Y="-22.258671875" />
                  <Point X="-2.938395263672" Y="-22.249525390625" />
                  <Point X="-2.929383789062" Y="-22.24444921875" />
                  <Point X="-2.91114453125" Y="-22.232826171875" />
                  <Point X="-2.902732666016" Y="-22.226796875" />
                  <Point X="-2.886611816406" Y="-22.213857421875" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.819105224609" Y="-22.147150390625" />
                  <Point X="-2.811246582031" Y="-22.1384921875" />
                  <Point X="-2.798322753906" Y="-22.12238671875" />
                  <Point X="-2.792300048828" Y="-22.113982421875" />
                  <Point X="-2.780666992188" Y="-22.0957265625" />
                  <Point X="-2.775585693359" Y="-22.086701171875" />
                  <Point X="-2.766433837891" Y="-22.0681484375" />
                  <Point X="-2.759355224609" Y="-22.048705078125" />
                  <Point X="-2.754437011719" Y="-22.028609375" />
                  <Point X="-2.752526855469" Y="-22.0184296875" />
                  <Point X="-2.749700683594" Y="-21.99696875" />
                  <Point X="-2.74891015625" Y="-21.986638671875" />
                  <Point X="-2.748458496094" Y="-21.965955078125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.75616796875" Y="-21.87135546875" />
                  <Point X="-2.757745117188" Y="-21.859802734375" />
                  <Point X="-2.761779052734" Y="-21.83951953125" />
                  <Point X="-2.764349609375" Y="-21.82948828125" />
                  <Point X="-2.770858398438" Y="-21.808841796875" />
                  <Point X="-2.774508544922" Y="-21.799142578125" />
                  <Point X="-2.782843994141" Y="-21.78019921875" />
                  <Point X="-2.793280029297" Y="-21.76099609375" />
                  <Point X="-3.05938671875" Y="-21.3000859375" />
                  <Point X="-2.648375" Y="-20.984966796875" />
                  <Point X="-2.1925234375" Y="-20.731703125" />
                  <Point X="-2.118563720703" Y="-20.82808984375" />
                  <Point X="-2.111822509766" Y="-20.83594921875" />
                  <Point X="-2.097524169922" Y="-20.85088671875" />
                  <Point X="-2.089966552734" Y="-20.85796484375" />
                  <Point X="-2.073389160156" Y="-20.871876953125" />
                  <Point X="-2.065103759766" Y="-20.87809375" />
                  <Point X="-2.047901000977" Y="-20.88958984375" />
                  <Point X="-2.038984130859" Y="-20.894869140625" />
                  <Point X="-1.945219848633" Y="-20.943681640625" />
                  <Point X="-1.934335205078" Y="-20.9487109375" />
                  <Point X="-1.915060913086" Y="-20.95620703125" />
                  <Point X="-1.905219360352" Y="-20.9594296875" />
                  <Point X="-1.884311523438" Y="-20.965033203125" />
                  <Point X="-1.874175048828" Y="-20.967166015625" />
                  <Point X="-1.853726318359" Y="-20.970314453125" />
                  <Point X="-1.833054931641" Y="-20.971216796875" />
                  <Point X="-1.812409545898" Y="-20.96986328125" />
                  <Point X="-1.802123291016" Y="-20.968623046875" />
                  <Point X="-1.780806640625" Y="-20.96486328125" />
                  <Point X="-1.770715209961" Y="-20.9625078125" />
                  <Point X="-1.750861083984" Y="-20.95671875" />
                  <Point X="-1.741098632812" Y="-20.95328515625" />
                  <Point X="-1.64349230957" Y="-20.91285546875" />
                  <Point X="-1.632540405273" Y="-20.90770703125" />
                  <Point X="-1.614400146484" Y="-20.89775" />
                  <Point X="-1.605603637695" Y="-20.89227734375" />
                  <Point X="-1.587883422852" Y="-20.87986328125" />
                  <Point X="-1.579739257812" Y="-20.87346875" />
                  <Point X="-1.564195922852" Y="-20.85983203125" />
                  <Point X="-1.55023059082" Y="-20.844587890625" />
                  <Point X="-1.538005981445" Y="-20.827912109375" />
                  <Point X="-1.532347290039" Y="-20.81923828125" />
                  <Point X="-1.521530761719" Y="-20.8005" />
                  <Point X="-1.516850097656" Y="-20.79126171875" />
                  <Point X="-1.508523071289" Y="-20.772333984375" />
                  <Point X="-1.504876831055" Y="-20.76264453125" />
                  <Point X="-1.473105957031" Y="-20.66187890625" />
                  <Point X="-1.470038208008" Y="-20.650279296875" />
                  <Point X="-1.465996704102" Y="-20.629978515625" />
                  <Point X="-1.464530639648" Y="-20.619716796875" />
                  <Point X="-1.462641967773" Y="-20.5981484375" />
                  <Point X="-1.462302368164" Y="-20.587791015625" />
                  <Point X="-1.462753295898" Y="-20.5671015625" />
                  <Point X="-1.464197631836" Y="-20.5518046875" />
                  <Point X="-1.479266601562" Y="-20.43734375" />
                  <Point X="-0.931176147461" Y="-20.2836796875" />
                  <Point X="-0.365222595215" Y="-20.217443359375" />
                  <Point X="-0.225666229248" Y="-20.7382734375" />
                  <Point X="-0.221880355835" Y="-20.749533203125" />
                  <Point X="-0.209742477417" Y="-20.779322265625" />
                  <Point X="-0.202780822754" Y="-20.793087890625" />
                  <Point X="-0.190678756714" Y="-20.812849609375" />
                  <Point X="-0.176254455566" Y="-20.830990234375" />
                  <Point X="-0.159728637695" Y="-20.847232421875" />
                  <Point X="-0.150969543457" Y="-20.85484765625" />
                  <Point X="-0.125777816772" Y="-20.874177734375" />
                  <Point X="-0.112728874207" Y="-20.882591796875" />
                  <Point X="-0.085433387756" Y="-20.897181640625" />
                  <Point X="-0.056123767853" Y="-20.90712890625" />
                  <Point X="-0.0255872612" Y="-20.912169921875" />
                  <Point X="-0.010113833427" Y="-20.913439453125" />
                  <Point X="0.022425662994" Y="-20.913439453125" />
                  <Point X="0.03789894104" Y="-20.912169921875" />
                  <Point X="0.068435592651" Y="-20.90712890625" />
                  <Point X="0.09774521637" Y="-20.897181640625" />
                  <Point X="0.12504070282" Y="-20.882591796875" />
                  <Point X="0.138089950562" Y="-20.874177734375" />
                  <Point X="0.163281524658" Y="-20.85484765625" />
                  <Point X="0.173177139282" Y="-20.846095703125" />
                  <Point X="0.191656463623" Y="-20.82730859375" />
                  <Point X="0.207349258423" Y="-20.80614453125" />
                  <Point X="0.219956939697" Y="-20.783005859375" />
                  <Point X="0.225455535889" Y="-20.77099609375" />
                  <Point X="0.235236999512" Y="-20.74498828125" />
                  <Point X="0.240924804688" Y="-20.72727734375" />
                  <Point X="0.378190673828" Y="-20.2149921875" />
                  <Point X="0.827850219727" Y="-20.262083984375" />
                  <Point X="1.453624633789" Y="-20.413166015625" />
                  <Point X="1.858241455078" Y="-20.559923828125" />
                  <Point X="2.250441162109" Y="-20.743341796875" />
                  <Point X="2.629447021484" Y="-20.96415234375" />
                  <Point X="2.817780029297" Y="-21.09808203125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.062371826172" Y="-22.4068984375" />
                  <Point X="2.053179199219" Y="-22.42656640625" />
                  <Point X="2.044180664062" Y="-22.45044140625" />
                  <Point X="2.04130090332" Y="-22.45940625" />
                  <Point X="2.020158569336" Y="-22.53846875" />
                  <Point X="2.018334350586" Y="-22.5467578125" />
                  <Point X="2.01435546875" Y="-22.571890625" />
                  <Point X="2.012691162109" Y="-22.592015625" />
                  <Point X="2.012384643555" Y="-22.601626953125" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.021649658203" Y="-22.6987421875" />
                  <Point X="2.023794311523" Y="-22.710927734375" />
                  <Point X="2.029136108398" Y="-22.732859375" />
                  <Point X="2.032460693359" Y="-22.7436640625" />
                  <Point X="2.040727783203" Y="-22.76576171875" />
                  <Point X="2.045311523438" Y="-22.776095703125" />
                  <Point X="2.055677246094" Y="-22.796150390625" />
                  <Point X="2.061459228516" Y="-22.80587109375" />
                  <Point X="2.103761962891" Y="-22.86821484375" />
                  <Point X="2.108824707031" Y="-22.87500390625" />
                  <Point X="2.128485839844" Y="-22.89762890625" />
                  <Point X="2.157009521484" Y="-22.92407421875" />
                  <Point X="2.168257080078" Y="-22.93301953125" />
                  <Point X="2.230559570312" Y="-22.975294921875" />
                  <Point X="2.241196289062" Y="-22.981703125" />
                  <Point X="2.261222900391" Y="-22.992064453125" />
                  <Point X="2.271541992188" Y="-22.9966484375" />
                  <Point X="2.293667724609" Y="-23.004939453125" />
                  <Point X="2.304490966797" Y="-23.008275390625" />
                  <Point X="2.326455322266" Y="-23.0136328125" />
                  <Point X="2.337596435547" Y="-23.015654296875" />
                  <Point X="2.405962646484" Y="-23.023896484375" />
                  <Point X="2.414505126953" Y="-23.024537109375" />
                  <Point X="2.444826416016" Y="-23.024521484375" />
                  <Point X="2.483585205078" Y="-23.02026171875" />
                  <Point X="2.497749023438" Y="-23.01760546875" />
                  <Point X="2.576811035156" Y="-22.996462890625" />
                  <Point X="2.582253417969" Y="-22.99483203125" />
                  <Point X="2.60140234375" Y="-22.988015625" />
                  <Point X="2.626883300781" Y="-22.976771484375" />
                  <Point X="2.636026611328" Y="-22.9721328125" />
                  <Point X="2.658782470703" Y="-22.95899609375" />
                  <Point X="3.940405517578" Y="-22.219048828125" />
                  <Point X="4.043950683594" Y="-22.362953125" />
                  <Point X="4.136884765625" Y="-22.516529296875" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168137939453" Y="-23.260130859375" />
                  <Point X="3.1521171875" Y="-23.27478515625" />
                  <Point X="3.134667236328" Y="-23.2933984375" />
                  <Point X="3.128575683594" Y="-23.300578125" />
                  <Point X="3.071687988281" Y="-23.37479296875" />
                  <Point X="3.066875488281" Y="-23.38168359375" />
                  <Point X="3.053694824219" Y="-23.40314453125" />
                  <Point X="3.044331054688" Y="-23.42126171875" />
                  <Point X="3.040322265625" Y="-23.43009765625" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.008944335938" Y="-23.533119140625" />
                  <Point X="3.006222900391" Y="-23.54534765625" />
                  <Point X="3.00276953125" Y="-23.567650390625" />
                  <Point X="3.001707763672" Y="-23.578904296875" />
                  <Point X="3.000892822266" Y="-23.602466796875" />
                  <Point X="3.001174560547" Y="-23.613763671875" />
                  <Point X="3.003077636719" Y="-23.636244140625" />
                  <Point X="3.004698974609" Y="-23.647427734375" />
                  <Point X="3.022098388672" Y="-23.73175390625" />
                  <Point X="3.024155029297" Y="-23.739888671875" />
                  <Point X="3.031747802734" Y="-23.763875" />
                  <Point X="3.039215820312" Y="-23.782560546875" />
                  <Point X="3.043214111328" Y="-23.791263671875" />
                  <Point X="3.056918457031" Y="-23.816474609375" />
                  <Point X="3.104243164062" Y="-23.88840625" />
                  <Point X="3.111728271484" Y="-23.898560546875" />
                  <Point X="3.126309082031" Y="-23.91583984375" />
                  <Point X="3.134115478516" Y="-23.924044921875" />
                  <Point X="3.151350830078" Y="-23.94012109375" />
                  <Point X="3.160051513672" Y="-23.947318359375" />
                  <Point X="3.178248291016" Y="-23.9606328125" />
                  <Point X="3.187744384766" Y="-23.96675" />
                  <Point X="3.256324462891" Y="-24.00535546875" />
                  <Point X="3.263922119141" Y="-24.009193359375" />
                  <Point X="3.287330322266" Y="-24.01931640625" />
                  <Point X="3.306368652344" Y="-24.025912109375" />
                  <Point X="3.315580810547" Y="-24.028591796875" />
                  <Point X="3.343673339844" Y="-24.034744140625" />
                  <Point X="3.436398193359" Y="-24.046998046875" />
                  <Point X="3.441975341797" Y="-24.047568359375" />
                  <Point X="3.462085693359" Y="-24.04860546875" />
                  <Point X="3.490263427734" Y="-24.047994140625" />
                  <Point X="3.500602050781" Y="-24.047203125" />
                  <Point X="3.52221875" Y="-24.044357421875" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.085765625" />
                  <Point X="4.783870605469" Y="-24.286078125" />
                  <Point X="3.6919921875" Y="-24.578646484375" />
                  <Point X="3.686020996094" Y="-24.580458984375" />
                  <Point X="3.665622802734" Y="-24.587865234375" />
                  <Point X="3.642380615234" Y="-24.5983828125" />
                  <Point X="3.634006103516" Y="-24.602685546875" />
                  <Point X="3.542906738281" Y="-24.655341796875" />
                  <Point X="3.535883789062" Y="-24.659822265625" />
                  <Point X="3.515637207031" Y="-24.674474609375" />
                  <Point X="3.499916015625" Y="-24.68770703125" />
                  <Point X="3.492776611328" Y="-24.694373046875" />
                  <Point X="3.472797851562" Y="-24.71576953125" />
                  <Point X="3.418138183594" Y="-24.785419921875" />
                  <Point X="3.41085546875" Y="-24.7957890625" />
                  <Point X="3.399132080078" Y="-24.815068359375" />
                  <Point X="3.393845214844" Y="-24.825056640625" />
                  <Point X="3.384069824219" Y="-24.8465234375" />
                  <Point X="3.380005859375" Y="-24.8570703125" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.370376464844" Y="-24.88952734375" />
                  <Point X="3.352156494141" Y="-24.9846640625" />
                  <Point X="3.350950439453" Y="-24.99290234375" />
                  <Point X="3.348783935547" Y="-25.0177578125" />
                  <Point X="3.348501708984" Y="-25.038041015625" />
                  <Point X="3.348856933594" Y="-25.047677734375" />
                  <Point X="3.351874267578" Y="-25.076423828125" />
                  <Point X="3.370094238281" Y="-25.171560546875" />
                  <Point X="3.373160888672" Y="-25.18399609375" />
                  <Point X="3.380010009766" Y="-25.20550390625" />
                  <Point X="3.384075683594" Y="-25.2160546875" />
                  <Point X="3.393850097656" Y="-25.237515625" />
                  <Point X="3.399136474609" Y="-25.2475" />
                  <Point X="3.410857421875" Y="-25.2667734375" />
                  <Point X="3.417291992188" Y="-25.2760625" />
                  <Point X="3.471951660156" Y="-25.345712890625" />
                  <Point X="3.477459960938" Y="-25.35212109375" />
                  <Point X="3.495068847656" Y="-25.370310546875" />
                  <Point X="3.510225585938" Y="-25.383806640625" />
                  <Point X="3.517757568359" Y="-25.38985546875" />
                  <Point X="3.541495605469" Y="-25.406404296875" />
                  <Point X="3.632571044922" Y="-25.459046875" />
                  <Point X="3.655419677734" Y="-25.470251953125" />
                  <Point X="3.682023193359" Y="-25.480642578125" />
                  <Point X="3.692002197266" Y="-25.483916015625" />
                  <Point X="3.711825683594" Y="-25.4892265625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761612304688" Y="-25.93105859375" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.436782226562" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400096191406" Y="-25.90804296875" />
                  <Point X="3.366722167969" Y="-25.910841796875" />
                  <Point X="3.354483886719" Y="-25.912677734375" />
                  <Point X="3.175687988281" Y="-25.9515390625" />
                  <Point X="3.157878173828" Y="-25.9567421875" />
                  <Point X="3.128760009766" Y="-25.968365234375" />
                  <Point X="3.114683349609" Y="-25.97538671875" />
                  <Point X="3.086854492188" Y="-25.992279296875" />
                  <Point X="3.074127197266" Y="-26.001529296875" />
                  <Point X="3.050374511719" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.931299072266" Y="-26.16317578125" />
                  <Point X="2.921356201172" Y="-26.17680859375" />
                  <Point X="2.906621582031" Y="-26.201205078125" />
                  <Point X="2.900174560547" Y="-26.213958984375" />
                  <Point X="2.888824707031" Y="-26.24135546875" />
                  <Point X="2.884365478516" Y="-26.254923828125" />
                  <Point X="2.877531494141" Y="-26.282578125" />
                  <Point X="2.875156738281" Y="-26.2966640625" />
                  <Point X="2.859667480469" Y="-26.46498828125" />
                  <Point X="2.859288330078" Y="-26.483328125" />
                  <Point X="2.861607910156" Y="-26.514599609375" />
                  <Point X="2.864066894531" Y="-26.530138671875" />
                  <Point X="2.871799316406" Y="-26.5617578125" />
                  <Point X="2.876788085938" Y="-26.57667578125" />
                  <Point X="2.889158935547" Y="-26.605484375" />
                  <Point X="2.896541015625" Y="-26.619375" />
                  <Point X="2.995489257812" Y="-26.77328125" />
                  <Point X="2.9995078125" Y="-26.77905078125" />
                  <Point X="3.015462158203" Y="-26.799072265625" />
                  <Point X="3.042230224609" Y="-26.82683984375" />
                  <Point X="3.052789306641" Y="-26.836271484375" />
                  <Point X="3.071185546875" Y="-26.850388671875" />
                  <Point X="4.087171875" Y="-27.62998046875" />
                  <Point X="4.045494384766" Y="-27.697421875" />
                  <Point X="4.001272460938" Y="-27.76025390625" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841192138672" Y="-27.09088671875" />
                  <Point X="2.815027099609" Y="-27.079515625" />
                  <Point X="2.783122802734" Y="-27.069328125" />
                  <Point X="2.771109619141" Y="-27.066337890625" />
                  <Point X="2.558314208984" Y="-27.02790625" />
                  <Point X="2.539292480469" Y="-27.02580078125" />
                  <Point X="2.507946777344" Y="-27.025404296875" />
                  <Point X="2.492259277344" Y="-27.0265078125" />
                  <Point X="2.460105712891" Y="-27.03146875" />
                  <Point X="2.4448203125" Y="-27.035142578125" />
                  <Point X="2.4150625" Y="-27.044962890625" />
                  <Point X="2.400590087891" Y="-27.051109375" />
                  <Point X="2.223809326172" Y="-27.144146484375" />
                  <Point X="2.208907470703" Y="-27.15320703125" />
                  <Point X="2.186007324219" Y="-27.1700859375" />
                  <Point X="2.175194824219" Y="-27.17938671875" />
                  <Point X="2.154247070312" Y="-27.2003359375" />
                  <Point X="2.144938720703" Y="-27.211162109375" />
                  <Point X="2.128048095703" Y="-27.23408984375" />
                  <Point X="2.120465820312" Y="-27.24619140625" />
                  <Point X="2.027427368164" Y="-27.42297265625" />
                  <Point X="2.019839233398" Y="-27.440185546875" />
                  <Point X="2.010016357422" Y="-27.469955078125" />
                  <Point X="2.006341308594" Y="-27.485248046875" />
                  <Point X="2.001381103516" Y="-27.517423828125" />
                  <Point X="2.000279785156" Y="-27.53312109375" />
                  <Point X="2.000683227539" Y="-27.564478515625" />
                  <Point X="2.002187744141" Y="-27.580138671875" />
                  <Point X="2.040618286133" Y="-27.792935546875" />
                  <Point X="2.042078979492" Y="-27.7996328125" />
                  <Point X="2.04742175293" Y="-27.819486328125" />
                  <Point X="2.053919677734" Y="-27.838896484375" />
                  <Point X="2.057391357422" Y="-27.847763671875" />
                  <Point X="2.069548583984" Y="-27.873583984375" />
                  <Point X="2.081370117187" Y="-27.89405859375" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.723752929688" Y="-29.03608203125" />
                  <Point X="1.833914428711" Y="-27.876423828125" />
                  <Point X="1.828655151367" Y="-27.8701484375" />
                  <Point X="1.808838256836" Y="-27.8496328125" />
                  <Point X="1.783255371094" Y="-27.828005859375" />
                  <Point X="1.773299072266" Y="-27.820646484375" />
                  <Point X="1.563425292969" Y="-27.685716796875" />
                  <Point X="1.546225097656" Y="-27.676212890625" />
                  <Point X="1.517421020508" Y="-27.663853515625" />
                  <Point X="1.502508056641" Y="-27.65887109375" />
                  <Point X="1.470899536133" Y="-27.6511484375" />
                  <Point X="1.455368530273" Y="-27.648693359375" />
                  <Point X="1.424116699219" Y="-27.64637890625" />
                  <Point X="1.408395629883" Y="-27.64651953125" />
                  <Point X="1.17889440918" Y="-27.66763671875" />
                  <Point X="1.161278808594" Y="-27.67033203125" />
                  <Point X="1.133613037109" Y="-27.677162109375" />
                  <Point X="1.120035400391" Y="-27.68162109375" />
                  <Point X="1.092637817383" Y="-27.692966796875" />
                  <Point X="1.07989074707" Y="-27.699408203125" />
                  <Point X="1.055500976563" Y="-27.714130859375" />
                  <Point X="1.043858398438" Y="-27.722412109375" />
                  <Point X="0.866619140625" Y="-27.86978125" />
                  <Point X="0.85263848877" Y="-27.8831015625" />
                  <Point X="0.832163330078" Y="-27.906859375" />
                  <Point X="0.822913635254" Y="-27.919587890625" />
                  <Point X="0.806033203125" Y="-27.947400390625" />
                  <Point X="0.79901373291" Y="-27.961474609375" />
                  <Point X="0.787393615723" Y="-27.990587890625" />
                  <Point X="0.782792724609" Y="-28.005626953125" />
                  <Point X="0.729799133301" Y="-28.249439453125" />
                  <Point X="0.728609863281" Y="-28.25601953125" />
                  <Point X="0.725973022461" Y="-28.275896484375" />
                  <Point X="0.724438476562" Y="-28.2968203125" />
                  <Point X="0.724223571777" Y="-28.306509765625" />
                  <Point X="0.725554931641" Y="-28.335515625" />
                  <Point X="0.728904907227" Y="-28.360962890625" />
                  <Point X="0.83309185791" Y="-29.15233984375" />
                  <Point X="0.655065002441" Y="-28.487935546875" />
                  <Point X="0.652606506348" Y="-28.480125" />
                  <Point X="0.642148376465" Y="-28.4535859375" />
                  <Point X="0.626791137695" Y="-28.4238203125" />
                  <Point X="0.620409851074" Y="-28.4132109375" />
                  <Point X="0.459178710938" Y="-28.180908203125" />
                  <Point X="0.446673583984" Y="-28.16517578125" />
                  <Point X="0.424794219971" Y="-28.142724609375" />
                  <Point X="0.412922943115" Y="-28.132408203125" />
                  <Point X="0.386667541504" Y="-28.113162109375" />
                  <Point X="0.373251037598" Y="-28.104943359375" />
                  <Point X="0.345245574951" Y="-28.09083203125" />
                  <Point X="0.330656616211" Y="-28.084939453125" />
                  <Point X="0.081160690308" Y="-28.007505859375" />
                  <Point X="0.063379592896" Y="-28.003111328125" />
                  <Point X="0.035220607758" Y="-27.998841796875" />
                  <Point X="0.020979265213" Y="-27.997767578125" />
                  <Point X="-0.008665058136" Y="-27.997767578125" />
                  <Point X="-0.022905954361" Y="-27.998841796875" />
                  <Point X="-0.051065830231" Y="-28.003111328125" />
                  <Point X="-0.064984962463" Y="-28.006306640625" />
                  <Point X="-0.314380828857" Y="-28.0837109375" />
                  <Point X="-0.332778289795" Y="-28.090771484375" />
                  <Point X="-0.360845733643" Y="-28.104892578125" />
                  <Point X="-0.374294158936" Y="-28.113123046875" />
                  <Point X="-0.400569061279" Y="-28.132373046875" />
                  <Point X="-0.412455474854" Y="-28.14269921875" />
                  <Point X="-0.434355072021" Y="-28.16516796875" />
                  <Point X="-0.444368225098" Y="-28.177310546875" />
                  <Point X="-0.60559954834" Y="-28.409615234375" />
                  <Point X="-0.618972473145" Y="-28.432025390625" />
                  <Point X="-0.628689147949" Y="-28.451189453125" />
                  <Point X="-0.632682800293" Y="-28.4601953125" />
                  <Point X="-0.642753479004" Y="-28.487939453125" />
                  <Point X="-0.648861755371" Y="-28.510736328125" />
                  <Point X="-0.985424499512" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.66311396324" Y="-28.957055921688" />
                  <Point X="2.689539623397" Y="-28.94743776797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.602474996792" Y="-28.878029812127" />
                  <Point X="2.64130680976" Y="-28.863896188064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.541836030344" Y="-28.799003702565" />
                  <Point X="2.593073996122" Y="-28.780354608158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.481197063896" Y="-28.719977593004" />
                  <Point X="2.544841182485" Y="-28.696813028251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420558097448" Y="-28.640951483442" />
                  <Point X="2.496608368848" Y="-28.613271448345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.824621482506" Y="-29.120728018672" />
                  <Point X="0.828733057141" Y="-29.119231527889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.359919131001" Y="-28.561925373881" />
                  <Point X="2.44837555521" Y="-28.529729868439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.135097043416" Y="-29.732910341262" />
                  <Point X="-0.959186241151" Y="-29.668884045352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.799939718856" Y="-29.028614557584" />
                  <Point X="0.816031980122" Y="-29.022757453481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299280164553" Y="-28.482899264319" />
                  <Point X="2.400142741573" Y="-28.446188288532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.127794851808" Y="-29.629155672487" />
                  <Point X="-0.929170096224" Y="-29.556862173666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.775257955206" Y="-28.936501096497" />
                  <Point X="0.803330903104" Y="-28.926283379074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.238641198105" Y="-28.403873154758" />
                  <Point X="2.351909927936" Y="-28.362646708626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120586902951" Y="-29.525435305267" />
                  <Point X="-0.899153951297" Y="-29.44484030198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.750576191556" Y="-28.84438763541" />
                  <Point X="0.790629826085" Y="-28.829809304667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.178002231657" Y="-28.324847045196" />
                  <Point X="2.303677114299" Y="-28.27910512872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.896076707295" Y="-27.699519075811" />
                  <Point X="4.087159194054" Y="-27.629970738342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.136754793525" Y="-29.430223047802" />
                  <Point X="-0.86913780637" Y="-29.332818430294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725894427905" Y="-28.752274174322" />
                  <Point X="0.777928749066" Y="-28.73333523026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.117363265209" Y="-28.245820935635" />
                  <Point X="2.255444300661" Y="-28.195563548813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.788677735796" Y="-27.637512216243" />
                  <Point X="3.997795359032" Y="-27.561399625924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.155506597979" Y="-29.335951258077" />
                  <Point X="-0.839121661443" Y="-29.220796558608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.701212664255" Y="-28.660160713235" />
                  <Point X="0.765227672047" Y="-28.636861155852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056724298761" Y="-28.166794826073" />
                  <Point X="2.207211487024" Y="-28.112021968907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.681278764297" Y="-27.575505356674" />
                  <Point X="3.90843152401" Y="-27.492828513507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.174258402433" Y="-29.241679468352" />
                  <Point X="-0.809105516516" Y="-29.108774686922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.676530900605" Y="-28.568047252148" />
                  <Point X="0.752526595029" Y="-28.540387081445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.996085332314" Y="-28.087768716512" />
                  <Point X="2.158978673387" Y="-28.028480389001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573879792797" Y="-27.513498497105" />
                  <Point X="3.819067688988" Y="-27.42425740109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.19899639551" Y="-29.149586473102" />
                  <Point X="-0.779089371589" Y="-28.996752815236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.651067066282" Y="-28.476218441506" />
                  <Point X="0.73982551801" Y="-28.443913007038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.935446365866" Y="-28.00874260695" />
                  <Point X="2.110745859749" Y="-27.944938809094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.466480821298" Y="-27.451491637537" />
                  <Point X="3.729703853966" Y="-27.355686288672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.252934619023" Y="-29.068121492565" />
                  <Point X="-0.749073226663" Y="-28.88473094355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.605487737411" Y="-28.391711072128" />
                  <Point X="0.727124556046" Y="-28.347438890754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.874807399418" Y="-27.929716497389" />
                  <Point X="2.063620747947" Y="-27.860994058692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.359081849798" Y="-27.389484777968" />
                  <Point X="3.640340018944" Y="-27.287115176255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.333386106393" Y="-28.996306550884" />
                  <Point X="-0.719057081736" Y="-28.772709071863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.549471425131" Y="-28.311002454046" />
                  <Point X="0.73075997549" Y="-28.245018817902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.810980579193" Y="-27.851850671713" />
                  <Point X="2.036438395922" Y="-27.769790737341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251682878299" Y="-27.3274779184" />
                  <Point X="3.550976183922" Y="-27.218544063838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.414853563387" Y="-28.924861391906" />
                  <Point X="-0.689040936809" Y="-28.660687200177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.493455112851" Y="-28.230293835964" />
                  <Point X="0.754621466461" Y="-28.135237057058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.717505123392" Y="-27.784776066874" />
                  <Point X="2.019306673521" Y="-27.674929285971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.1442839068" Y="-27.265471058831" />
                  <Point X="3.4616123489" Y="-27.14997295142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.496840640567" Y="-28.853605359209" />
                  <Point X="-0.659024791882" Y="-28.548665328491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.433040259694" Y="-28.151186155835" />
                  <Point X="0.778482957432" Y="-28.025455296214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.617098926885" Y="-27.720224045353" />
                  <Point X="2.002180736304" Y="-27.580065728967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0368849353" Y="-27.203464199262" />
                  <Point X="3.372248513878" Y="-27.081401839003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.547635398757" Y="-29.134966485128" />
                  <Point X="-2.456691585978" Y="-29.101865644286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.622593399736" Y="-28.798278732039" />
                  <Point X="-0.619572602558" Y="-28.433209017516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.333800887035" Y="-28.086209445165" />
                  <Point X="0.834723782176" Y="-27.903888421671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.505321138888" Y="-27.659810944651" />
                  <Point X="2.008393681685" Y="-27.476707513396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.929485963801" Y="-27.141457339694" />
                  <Point X="3.282884678857" Y="-27.012830726585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.650466004261" Y="-29.071296876318" />
                  <Point X="-2.271213152938" Y="-28.933260127176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.855228463955" Y="-28.781854082476" />
                  <Point X="-0.52890437483" Y="-28.299111593044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.184315182973" Y="-28.039520903507" />
                  <Point X="1.034744708588" Y="-27.729989869842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.215249262353" Y="-27.664291585122" />
                  <Point X="2.062769531659" Y="-27.355819434158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.81816329684" Y="-27.080878588481" />
                  <Point X="3.193520843835" Y="-26.944259614168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752536547799" Y="-29.007350627576" />
                  <Point X="-0.431988982703" Y="-28.162740386682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.021220160271" Y="-27.997785748742" />
                  <Point X="2.131195452263" Y="-27.22981754742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.643615358505" Y="-27.043311954103" />
                  <Point X="3.104157008813" Y="-26.875688501751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.841692021181" Y="-28.938703677724" />
                  <Point X="3.021029544171" Y="-26.804847536145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.930847494562" Y="-28.870056727871" />
                  <Point X="2.964107081531" Y="-26.724468729822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948420415163" Y="-28.775355859514" />
                  <Point X="2.911435773032" Y="-26.64254262993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.530515837058" Y="-26.053245679731" />
                  <Point X="4.752136617023" Y="-25.972582312529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874523557588" Y="-28.647362714566" />
                  <Point X="2.870477644897" Y="-26.556353281037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.326535942361" Y="-26.026391401404" />
                  <Point X="4.771665757768" Y="-25.864377398212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.800626700013" Y="-28.519369569617" />
                  <Point X="2.860219437685" Y="-26.458990074735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.122556047665" Y="-25.999537123077" />
                  <Point X="4.755949425486" Y="-25.769000786969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.726729842439" Y="-28.391376424669" />
                  <Point X="2.869844790141" Y="-26.354389844561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.918576152969" Y="-25.97268284475" />
                  <Point X="4.595966110119" Y="-25.726133063356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.652832984864" Y="-28.263383279721" />
                  <Point X="2.886947230917" Y="-26.2470681768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.714596258272" Y="-25.945828566423" />
                  <Point X="4.435982794753" Y="-25.683265339744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.578936127289" Y="-28.135390134773" />
                  <Point X="2.971058567049" Y="-26.115357265698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.510616363576" Y="-25.918974288096" />
                  <Point X="4.275999479386" Y="-25.640397616131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.505039269715" Y="-28.007396989825" />
                  <Point X="4.116016164019" Y="-25.597529892519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.43114241214" Y="-27.879403844876" />
                  <Point X="3.956032848653" Y="-25.554662168906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.357245554566" Y="-27.751410699928" />
                  <Point X="3.796049533286" Y="-25.511794445294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313377561941" Y="-27.634347167991" />
                  <Point X="3.645623844671" Y="-25.465448030034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.321194786268" Y="-27.536095516575" />
                  <Point X="3.537581252227" Y="-25.403675429331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.821096085966" Y="-27.980918055617" />
                  <Point X="-3.572882813898" Y="-27.890575812835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.365971128027" Y="-27.451295883789" />
                  <Point X="3.460202894685" Y="-25.330741959867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.881352480373" Y="-27.90175270122" />
                  <Point X="-3.099099807256" Y="-27.617036012531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.448674342678" Y="-27.380300503815" />
                  <Point X="3.40131312575" Y="-25.251079194477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.94160887478" Y="-27.822587346823" />
                  <Point X="3.368265317893" Y="-25.16201072446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.001865269187" Y="-27.743421992426" />
                  <Point X="3.350909331635" Y="-25.067230898459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.051783782221" Y="-27.660493956925" />
                  <Point X="3.356064599387" Y="-24.964257646062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.101321202947" Y="-27.577427215166" />
                  <Point X="3.381181641279" Y="-24.854018902055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150858623673" Y="-27.494360473407" />
                  <Point X="3.468615746869" Y="-24.721098601776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.32883162619" Y="-24.40800562666" />
                  <Point X="4.777432661268" Y="-24.24472820283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084464709927" Y="-27.369098176682" />
                  <Point X="4.762536590785" Y="-24.149053040709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.833825774877" Y="-27.176776176391" />
                  <Point X="4.745029883411" Y="-24.054328072708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.583186839828" Y="-26.984454176099" />
                  <Point X="4.722421849748" Y="-23.961459835631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.332547904779" Y="-26.792132175808" />
                  <Point X="4.610574164968" Y="-23.901072175277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.08190896973" Y="-26.599810175517" />
                  <Point X="4.175406773421" Y="-23.958363264338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.966447879771" Y="-26.456688887171" />
                  <Point X="3.740239381874" Y="-24.0156543534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948848843168" Y="-26.34918647331" />
                  <Point X="3.392353864424" Y="-24.041177438299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.968829897948" Y="-26.255362094114" />
                  <Point X="3.239316867966" Y="-23.995781461366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.028129167363" Y="-26.17584837471" />
                  <Point X="3.141001980403" Y="-23.930468265638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.134318607237" Y="-26.113401281632" />
                  <Point X="3.080008843856" Y="-23.851571063451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.45030603534" Y="-26.127314411479" />
                  <Point X="3.033200939669" Y="-23.767510858918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.885470244853" Y="-26.184604342374" />
                  <Point X="3.010333808004" Y="-23.674736925802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.320634454366" Y="-26.24189427327" />
                  <Point X="3.001915343995" Y="-23.576704107734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.666071690993" Y="-26.266526256824" />
                  <Point X="3.027647408353" Y="-23.466241513856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.689698317386" Y="-26.174028757182" />
                  <Point X="3.098986941821" Y="-23.339179158763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.713324943779" Y="-26.08153125754" />
                  <Point X="3.284561409495" Y="-23.170538687904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.736951570171" Y="-25.989033757897" />
                  <Point X="3.53520126216" Y="-22.978216353628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752289264224" Y="-25.89351933361" />
                  <Point X="3.785841114824" Y="-22.785894019352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766032987354" Y="-25.797424751352" />
                  <Point X="4.036480967489" Y="-22.593571685076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.779776710485" Y="-25.701330169094" />
                  <Point X="-4.275070867808" Y="-25.517632265299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333512395007" Y="-25.174933007379" />
                  <Point X="4.10683341554" Y="-22.466868599692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296737661114" Y="-25.060451210484" />
                  <Point X="2.326718481126" Y="-23.013680561007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.968916884541" Y="-22.779939457671" />
                  <Point X="4.056698539586" Y="-22.384019313853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.303778898925" Y="-24.961917123074" />
                  <Point X="2.203977589704" Y="-22.957257703626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.442701617277" Y="-22.50639902912" />
                  <Point X="4.000962149546" Y="-22.303208812408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.34151755475" Y="-24.87455598209" />
                  <Point X="2.119248910386" Y="-22.886999532502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.916486350013" Y="-22.232858600569" />
                  <Point X="3.943315810679" Y="-22.223093475485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.412849127797" Y="-24.799421663057" />
                  <Point X="2.062038899507" Y="-22.806725385178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.535709480801" Y="-24.743042286137" />
                  <Point X="2.025723025887" Y="-22.718846393822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.695693595041" Y="-24.700174853291" />
                  <Point X="2.013122957266" Y="-22.622335555365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.85567770928" Y="-24.657307420444" />
                  <Point X="2.026021539591" Y="-22.516543966949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.01566182352" Y="-24.614439987598" />
                  <Point X="2.065494681552" Y="-22.401080029837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.17564593776" Y="-24.571572554751" />
                  <Point X="2.139391451713" Y="-22.273086916705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.335630052" Y="-24.528705121905" />
                  <Point X="2.213288221874" Y="-22.145093803573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.495614166239" Y="-24.485837689058" />
                  <Point X="2.287184992035" Y="-22.017100690441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.655598280479" Y="-24.442970256212" />
                  <Point X="2.361081762195" Y="-21.889107577308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782467916617" Y="-24.388050139013" />
                  <Point X="2.434978532356" Y="-21.761114464176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76665657447" Y="-24.281198392722" />
                  <Point X="2.508875302517" Y="-21.633121351044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750845232323" Y="-24.174346646432" />
                  <Point X="-3.694562452415" Y="-23.789891155577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.561989472443" Y="-23.741638536999" />
                  <Point X="2.582772072678" Y="-21.505128237912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735033890176" Y="-24.067494900141" />
                  <Point X="-3.92980958849" Y="-23.77441722242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.45177920169" Y="-23.60042839055" />
                  <Point X="2.656668842838" Y="-21.37713512478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708056546822" Y="-23.956579061776" />
                  <Point X="-4.133789361587" Y="-23.747562899834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421724001717" Y="-23.488392303989" />
                  <Point X="2.730565612999" Y="-21.249142011648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.677663414328" Y="-23.844419977836" />
                  <Point X="-4.337769134685" Y="-23.720708577249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.437023886236" Y="-23.392864118157" />
                  <Point X="2.80446238316" Y="-21.121148898516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.647270281833" Y="-23.732260893897" />
                  <Point X="-4.541748907782" Y="-23.693854254663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.4790758947" Y="-23.307072909144" />
                  <Point X="2.740692268601" Y="-21.043262433666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.544962773954" Y="-23.229956883636" />
                  <Point X="2.646657621746" Y="-20.976391357726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.633954872796" Y="-23.161250470314" />
                  <Point X="2.542191658713" Y="-20.913316970378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723318543868" Y="-23.092679298224" />
                  <Point X="2.435388807415" Y="-20.8510931408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.81268221494" Y="-23.024108126133" />
                  <Point X="2.328585956117" Y="-20.788869311222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.902045886012" Y="-22.955536954043" />
                  <Point X="2.217822289246" Y="-20.728087100616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.991409557084" Y="-22.886965781953" />
                  <Point X="2.096258269999" Y="-20.671235896794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.080773228156" Y="-22.818394609863" />
                  <Point X="1.974694250753" Y="-20.614384692973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.170136899228" Y="-22.749823437772" />
                  <Point X="1.852392000358" Y="-20.557802183315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.200881020696" Y="-22.65991649448" />
                  <Point X="-3.131971658898" Y="-22.270865303657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.834577823054" Y="-22.162622799556" />
                  <Point X="1.713270082452" Y="-20.507341531981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.125954180244" Y="-22.531548466423" />
                  <Point X="-3.278972522811" Y="-22.223272354148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755443905812" Y="-22.032723520774" />
                  <Point X="1.574148164546" Y="-20.456880880648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.051027339792" Y="-22.403180438365" />
                  <Point X="-3.386371547919" Y="-22.161265514091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751035290882" Y="-21.93002202778" />
                  <Point X="0.051791385901" Y="-20.909876545623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.12138079491" Y="-20.884548072124" />
                  <Point X="1.43130056125" Y="-20.407776267899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963935918196" Y="-22.270384864859" />
                  <Point X="-3.493770573027" Y="-22.099258674034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.763345732831" Y="-21.833405773835" />
                  <Point X="-0.126456841334" Y="-20.873656706262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.236319728148" Y="-20.741616833282" />
                  <Point X="1.264309318281" Y="-20.367459221338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.854213039603" Y="-22.129352114648" />
                  <Point X="-3.601169598135" Y="-22.037251833977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.801768503333" Y="-21.74629363023" />
                  <Point X="-0.199149385931" Y="-20.799017740364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.267181045623" Y="-20.629287343945" />
                  <Point X="1.097318075312" Y="-20.327142174776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.74449016101" Y="-21.988319364437" />
                  <Point X="-3.708568623242" Y="-21.975244993921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.850001280379" Y="-21.662752037005" />
                  <Point X="-0.233161629571" Y="-20.710300296264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.297197091239" Y="-20.517265508406" />
                  <Point X="0.930326832343" Y="-20.286825128215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.898234057425" Y="-21.579210443781" />
                  <Point X="-0.257843419455" Y="-20.618186844725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.327213136854" Y="-20.405243672866" />
                  <Point X="0.744518701864" Y="-20.253356868609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.94646683447" Y="-21.495668850556" />
                  <Point X="-0.282525209338" Y="-20.526073393185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.357229182469" Y="-20.293221837326" />
                  <Point X="0.528821368247" Y="-20.230767389271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.994699611516" Y="-21.412127257332" />
                  <Point X="-0.307206999222" Y="-20.433959941646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.042932388562" Y="-21.328585664107" />
                  <Point X="-1.961759943878" Y="-20.935071076133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.507551750316" Y="-20.769752813517" />
                  <Point X="-0.331888789106" Y="-20.341846490107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.893991192438" Y="-21.173278613677" />
                  <Point X="-2.070941935542" Y="-20.873713182832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.471420289207" Y="-20.655505148768" />
                  <Point X="-0.35657057899" Y="-20.249733038568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.636987202579" Y="-20.978639922895" />
                  <Point X="-2.141440742475" Y="-20.798275761722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.464200805219" Y="-20.551780583104" />
                  <Point X="-0.631114627464" Y="-20.248562011823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.47690177989" Y="-20.455306471445" />
                  <Point X="-1.254126333147" Y="-20.374222839905" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998373046875" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464321105957" Y="-28.521546875" />
                  <Point X="0.303089996338" Y="-28.289244140625" />
                  <Point X="0.300593475342" Y="-28.285646484375" />
                  <Point X="0.274338195801" Y="-28.266400390625" />
                  <Point X="0.024842357635" Y="-28.188966796875" />
                  <Point X="0.020979211807" Y="-28.187767578125" />
                  <Point X="-0.008665060997" Y="-28.187767578125" />
                  <Point X="-0.258140625" Y="-28.265197265625" />
                  <Point X="-0.262004272461" Y="-28.266392578125" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.449510406494" Y="-28.517947265625" />
                  <Point X="-0.449509796143" Y="-28.517947265625" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.465335235596" Y="-28.559908203125" />
                  <Point X="-0.847744140625" Y="-29.987078125" />
                  <Point X="-1.0971484375" Y="-29.93866796875" />
                  <Point X="-1.100237426758" Y="-29.938068359375" />
                  <Point X="-1.351589599609" Y="-29.8733984375" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309682861328" Y="-29.53476171875" />
                  <Point X="-1.369287353516" Y="-29.235109375" />
                  <Point X="-1.370210327148" Y="-29.23046875" />
                  <Point X="-1.386283081055" Y="-29.20262890625" />
                  <Point X="-1.615987182617" Y="-29.00118359375" />
                  <Point X="-1.619543945312" Y="-28.998064453125" />
                  <Point X="-1.649240722656" Y="-28.985763671875" />
                  <Point X="-1.954109375" Y="-28.96578125" />
                  <Point X="-1.958838256836" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.243911132812" Y="-29.143529296875" />
                  <Point X="-2.247849121094" Y="-29.146162109375" />
                  <Point X="-2.263163330078" Y="-29.161763671875" />
                  <Point X="-2.457095214844" Y="-29.414501953125" />
                  <Point X="-2.851297119141" Y="-29.170421875" />
                  <Point X="-2.855825683594" Y="-29.1676171875" />
                  <Point X="-3.228580322266" Y="-28.880607421875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.59759375" />
                  <Point X="-2.513979736328" Y="-27.568763671875" />
                  <Point X="-2.531063720703" Y="-27.5516796875" />
                  <Point X="-2.531393554688" Y="-27.5513515625" />
                  <Point X="-2.560203125" Y="-27.537193359375" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.611344238281" Y="-27.554822265625" />
                  <Point X="-3.842959472656" Y="-28.265896484375" />
                  <Point X="-4.158120605469" Y="-27.851837890625" />
                  <Point X="-4.161700195312" Y="-27.847134765625" />
                  <Point X="-4.431019042969" Y="-27.39552734375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145821777344" Y="-26.396013671875" />
                  <Point X="-3.138236572266" Y="-26.3667265625" />
                  <Point X="-3.138119140625" Y="-26.3662734375" />
                  <Point X="-3.140324707031" Y="-26.334599609375" />
                  <Point X="-3.161158203125" Y="-26.310640625" />
                  <Point X="-3.187237060547" Y="-26.295291015625" />
                  <Point X="-3.187747802734" Y="-26.2949921875" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.244349121094" Y="-26.29183984375" />
                  <Point X="-4.803283691406" Y="-26.497076171875" />
                  <Point X="-4.925991699219" Y="-26.016677734375" />
                  <Point X="-4.927392089844" Y="-26.011197265625" />
                  <Point X="-4.998395996094" Y="-25.5147421875" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541895507813" Y="-25.12142578125" />
                  <Point X="-3.514565673828" Y="-25.10245703125" />
                  <Point X="-3.514187744141" Y="-25.1021953125" />
                  <Point X="-3.494898925781" Y="-25.075908203125" />
                  <Point X="-3.485796142578" Y="-25.046578125" />
                  <Point X="-3.485655029297" Y="-25.046125" />
                  <Point X="-3.485647705078" Y="-25.0164609375" />
                  <Point X="-3.494757568359" Y="-24.987107421875" />
                  <Point X="-3.494895263672" Y="-24.9866640625" />
                  <Point X="-3.51414453125" Y="-24.960396484375" />
                  <Point X="-3.541389648438" Y="-24.94148828125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.580088378906" Y="-24.9278515625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.918547363281" Y="-24.0096796875" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.773516113281" Y="-23.471701171875" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731704589844" Y="-23.6041328125" />
                  <Point X="-3.671231933594" Y="-23.58506640625" />
                  <Point X="-3.670295410156" Y="-23.584771484375" />
                  <Point X="-3.651541015625" Y="-23.57394921875" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.614848388672" Y="-23.497619140625" />
                  <Point X="-3.614464355469" Y="-23.49669140625" />
                  <Point X="-3.610714355469" Y="-23.47538671875" />
                  <Point X="-3.616313964844" Y="-23.4544921875" />
                  <Point X="-3.645600341797" Y="-23.398234375" />
                  <Point X="-3.646053710938" Y="-23.39736328125" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.672945800781" Y="-23.3708203125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.163522460938" Y="-22.21900390625" />
                  <Point X="-4.160014160156" Y="-22.212994140625" />
                  <Point X="-3.774670166016" Y="-21.717689453125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138515136719" Y="-22.07956640625" />
                  <Point X="-3.054270263672" Y="-22.0869375" />
                  <Point X="-3.05293359375" Y="-22.087052734375" />
                  <Point X="-3.031491699219" Y="-22.08421875" />
                  <Point X="-3.013252441406" Y="-22.072595703125" />
                  <Point X="-2.953454833984" Y="-22.012798828125" />
                  <Point X="-2.952533447266" Y="-22.011876953125" />
                  <Point X="-2.940900390625" Y="-21.99362109375" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.945444824219" Y="-21.8879140625" />
                  <Point X="-2.94555859375" Y="-21.88661328125" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-2.957818115234" Y="-21.8560078125" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.758983642578" Y="-20.8303515625" />
                  <Point X="-2.752871582031" Y="-20.825666015625" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951249267578" Y="-20.726337890625" />
                  <Point X="-1.857484985352" Y="-20.775150390625" />
                  <Point X="-1.856033203125" Y="-20.77590625" />
                  <Point X="-1.835125488281" Y="-20.781509765625" />
                  <Point X="-1.813808837891" Y="-20.77775" />
                  <Point X="-1.716147094727" Y="-20.737296875" />
                  <Point X="-1.714619995117" Y="-20.7366640625" />
                  <Point X="-1.696899780273" Y="-20.72425" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.654298828125" Y="-20.604703125" />
                  <Point X="-1.653806396484" Y="-20.603142578125" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.652571411133" Y="-20.576609375" />
                  <Point X="-1.689137817383" Y="-20.298859375" />
                  <Point X="-0.975997680664" Y="-20.098919921875" />
                  <Point X="-0.968094665527" Y="-20.096703125" />
                  <Point X="-0.224200042725" Y="-20.009642578125" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.035305446625" Y="-20.704109375" />
                  <Point X="-0.010113825798" Y="-20.723439453125" />
                  <Point X="0.022425748825" Y="-20.723439453125" />
                  <Point X="0.047617370605" Y="-20.704109375" />
                  <Point X="0.057398895264" Y="-20.6781015625" />
                  <Point X="0.2366484375" Y="-20.009130859375" />
                  <Point X="0.853303161621" Y="-20.073712890625" />
                  <Point X="0.860210327148" Y="-20.074435546875" />
                  <Point X="1.501184448242" Y="-20.2291875" />
                  <Point X="1.508452514648" Y="-20.23094140625" />
                  <Point X="1.926622680664" Y="-20.382615234375" />
                  <Point X="1.931039306641" Y="-20.384216796875" />
                  <Point X="2.334462158203" Y="-20.572884765625" />
                  <Point X="2.338694824219" Y="-20.57486328125" />
                  <Point X="2.728419677734" Y="-20.80191796875" />
                  <Point X="2.732532958984" Y="-20.804314453125" />
                  <Point X="3.068740966797" Y="-21.04340625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224851318359" Y="-22.50848828125" />
                  <Point X="2.203708984375" Y="-22.58755078125" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.210287353516" Y="-22.67603125" />
                  <Point X="2.210414794922" Y="-22.67708984375" />
                  <Point X="2.218681884766" Y="-22.6991875" />
                  <Point X="2.260984619141" Y="-22.76153125" />
                  <Point X="2.274939941406" Y="-22.775796875" />
                  <Point X="2.337283203125" Y="-22.818099609375" />
                  <Point X="2.338212402344" Y="-22.81873046875" />
                  <Point X="2.360338134766" Y="-22.827021484375" />
                  <Point X="2.428704345703" Y="-22.835263671875" />
                  <Point X="2.448664550781" Y="-22.8340546875" />
                  <Point X="2.5277265625" Y="-22.812912109375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.563789794922" Y="-22.7944453125" />
                  <Point X="3.994248291016" Y="-21.968568359375" />
                  <Point X="4.200155273438" Y="-22.254732421875" />
                  <Point X="4.202586425781" Y="-22.258111328125" />
                  <Point X="4.387513183594" Y="-22.563705078125" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.27937109375" Y="-23.41616796875" />
                  <Point X="3.222483398438" Y="-23.4903828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.191923828125" Y="-23.584291015625" />
                  <Point X="3.191593994141" Y="-23.585470703125" />
                  <Point X="3.190779052734" Y="-23.609033203125" />
                  <Point X="3.208178466797" Y="-23.693359375" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.262971191406" Y="-23.7839765625" />
                  <Point X="3.263712158203" Y="-23.785103515625" />
                  <Point X="3.280947509766" Y="-23.8011796875" />
                  <Point X="3.349527587891" Y="-23.83978515625" />
                  <Point X="3.349532958984" Y="-23.8397890625" />
                  <Point X="3.368565917969" Y="-23.846380859375" />
                  <Point X="3.461290771484" Y="-23.858634765625" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.497420410156" Y="-23.855982421875" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.938143554688" Y="-24.04432421875" />
                  <Point X="4.939187988281" Y="-24.04861328125" />
                  <Point X="4.997858398438" Y="-24.4254453125" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.637987792969" Y="-24.819837890625" />
                  <Point X="3.637988037109" Y="-24.819837890625" />
                  <Point X="3.622266601562" Y="-24.8330703125" />
                  <Point X="3.567606933594" Y="-24.902720703125" />
                  <Point X="3.566760742188" Y="-24.903798828125" />
                  <Point X="3.556985351562" Y="-24.925265625" />
                  <Point X="3.538765380859" Y="-25.02040234375" />
                  <Point X="3.538764160156" Y="-25.020408203125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.556703125" Y="-25.135822265625" />
                  <Point X="3.556986328125" Y="-25.13730078125" />
                  <Point X="3.566760742188" Y="-25.15876171875" />
                  <Point X="3.621420410156" Y="-25.228412109375" />
                  <Point X="3.636577148438" Y="-25.241908203125" />
                  <Point X="3.727652587891" Y="-25.29455078125" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="3.760991210938" Y="-25.30569921875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.949015625" Y="-25.962525390625" />
                  <Point X="4.948430664062" Y="-25.96640625" />
                  <Point X="4.874545898438" Y="-26.2901796875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394838378906" Y="-26.098341796875" />
                  <Point X="3.216042480469" Y="-26.137203125" />
                  <Point X="3.213274169922" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.077380859375" Y="-26.284666015625" />
                  <Point X="3.075707275391" Y="-26.286677734375" />
                  <Point X="3.064357421875" Y="-26.31407421875" />
                  <Point X="3.048868164062" Y="-26.4823984375" />
                  <Point X="3.048628417969" Y="-26.485005859375" />
                  <Point X="3.056360839844" Y="-26.516625" />
                  <Point X="3.155309082031" Y="-26.67053125" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.186856933594" Y="-26.699658203125" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.205774414062" Y="-27.799484375" />
                  <Point X="4.204128417969" Y="-27.8021484375" />
                  <Point X="4.0566875" Y="-28.011640625" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737341064453" Y="-27.253314453125" />
                  <Point X="2.524545654297" Y="-27.2148828125" />
                  <Point X="2.521231445312" Y="-27.21428515625" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.312297119141" Y="-27.312283203125" />
                  <Point X="2.309549804688" Y="-27.31373046875" />
                  <Point X="2.288602050781" Y="-27.3346796875" />
                  <Point X="2.195563476562" Y="-27.5114609375" />
                  <Point X="2.194123046875" Y="-27.514197265625" />
                  <Point X="2.189162841797" Y="-27.546373046875" />
                  <Point X="2.227593505859" Y="-27.759169921875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.245913085938" Y="-27.7990546875" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.837253173828" Y="-29.18881640625" />
                  <Point X="2.835305419922" Y="-29.19020703125" />
                  <Point X="2.679775390625" Y="-29.29087890625" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670549316406" Y="-27.980466796875" />
                  <Point X="1.460675537109" Y="-27.845537109375" />
                  <Point X="1.457413696289" Y="-27.84344140625" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.196284545898" Y="-27.856837890625" />
                  <Point X="1.19273034668" Y="-27.8571640625" />
                  <Point X="1.165332763672" Y="-27.868509765625" />
                  <Point X="0.988093444824" Y="-28.01587890625" />
                  <Point X="0.985338134766" Y="-28.018169921875" />
                  <Point X="0.968457763672" Y="-28.045982421875" />
                  <Point X="0.915464111328" Y="-28.289794921875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.917279602051" Y="-28.336166015625" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="0.996203735352" Y="-29.96283984375" />
                  <Point X="0.994347412109" Y="-29.96324609375" />
                  <Point X="0.860200256348" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#216" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.190297965249" Y="5.0653261847" Z="2.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.5" />
                  <Point X="-0.205301466071" Y="5.074911947802" Z="2.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.5" />
                  <Point X="-0.995652588278" Y="4.980503814645" Z="2.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.5" />
                  <Point X="-1.7082996835" Y="4.448146597217" Z="2.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.5" />
                  <Point X="-1.708138087686" Y="4.441619523077" Z="2.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.5" />
                  <Point X="-1.741437351441" Y="4.340177718811" Z="2.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.5" />
                  <Point X="-1.840550907092" Y="4.300480373425" Z="2.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.5" />
                  <Point X="-2.131240526435" Y="4.605929519272" Z="2.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.5" />
                  <Point X="-2.144235126693" Y="4.604377896997" Z="2.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.5" />
                  <Point X="-2.795559906076" Y="4.240609982665" Z="2.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.5" />
                  <Point X="-3.007275393598" Y="3.150273425835" Z="2.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.5" />
                  <Point X="-3.001410560372" Y="3.139008452092" Z="2.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.5" />
                  <Point X="-2.994965925961" Y="3.053837641926" Z="2.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.5" />
                  <Point X="-3.056068013515" Y="2.994154138801" Z="2.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.5" />
                  <Point X="-3.783585922462" Y="3.372918555147" Z="2.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.5" />
                  <Point X="-3.799861084417" Y="3.370552673632" Z="2.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.5" />
                  <Point X="-4.212859457823" Y="2.837483301517" Z="2.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.5" />
                  <Point X="-3.709539753124" Y="1.62079207813" Z="2.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.5" />
                  <Point X="-3.696108816359" Y="1.609963006463" Z="2.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.5" />
                  <Point X="-3.667198258572" Y="1.552797058877" Z="2.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.5" />
                  <Point X="-3.692406534916" Y="1.493904793318" Z="2.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.5" />
                  <Point X="-4.800278107478" Y="1.612723037554" Z="2.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.5" />
                  <Point X="-4.818879692203" Y="1.606061209653" Z="2.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.5" />
                  <Point X="-4.97416491715" Y="1.028918518768" Z="2.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.5" />
                  <Point X="-3.59918522931" Y="0.055131246957" Z="2.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.5" />
                  <Point X="-3.576137559381" Y="0.04877532354" Z="2.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.5" />
                  <Point X="-3.548666798223" Y="0.029352454173" Z="2.5" />
                  <Point X="-3.539556741714" Y="0" Z="2.5" />
                  <Point X="-3.539697802912" Y="-0.000454496888" Z="2.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.5" />
                  <Point X="-3.549231035742" Y="-0.030100659594" Z="2.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.5" />
                  <Point X="-5.037702559463" Y="-0.440580809799" Z="2.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.5" />
                  <Point X="-5.059142810836" Y="-0.454923121477" Z="2.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.5" />
                  <Point X="-4.980591677739" Y="-0.997774967371" Z="2.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.5" />
                  <Point X="-3.243978762234" Y="-1.310130905777" Z="2.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.5" />
                  <Point X="-3.218755081009" Y="-1.307100971095" Z="2.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.5" />
                  <Point X="-3.192793784061" Y="-1.3229033917" Z="2.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.5" />
                  <Point X="-4.483039756779" Y="-2.336415960968" Z="2.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.5" />
                  <Point X="-4.498424610343" Y="-2.359161273806" Z="2.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.5" />
                  <Point X="-4.204062047001" Y="-2.850840780243" Z="2.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.5" />
                  <Point X="-2.5925002819" Y="-2.566842129031" Z="2.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.5" />
                  <Point X="-2.572574967876" Y="-2.555755501255" Z="2.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.5" />
                  <Point X="-3.288574884137" Y="-3.842578100592" Z="2.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.5" />
                  <Point X="-3.293682734016" Y="-3.867046023603" Z="2.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.5" />
                  <Point X="-2.88377617712" Y="-4.181650216827" Z="2.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.5" />
                  <Point X="-2.229651486684" Y="-4.160921210869" Z="2.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.5" />
                  <Point X="-2.222288802539" Y="-4.153823911689" Z="2.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.5" />
                  <Point X="-1.963535376207" Y="-3.984393773903" Z="2.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.5" />
                  <Point X="-1.655110083492" Y="-4.007495138604" Z="2.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.5" />
                  <Point X="-1.42448305568" Y="-4.213580351293" Z="2.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.5" />
                  <Point X="-1.412363783299" Y="-4.873918343503" Z="2.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.5" />
                  <Point X="-1.408590254488" Y="-4.880663324148" Z="2.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.5" />
                  <Point X="-1.112831492951" Y="-4.956470692524" Z="2.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.5" />
                  <Point X="-0.423194850433" Y="-3.541568709474" Z="2.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.5" />
                  <Point X="-0.414590257354" Y="-3.515176065645" Z="2.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.5" />
                  <Point X="-0.249495855884" Y="-3.281673674848" Z="2.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.5" />
                  <Point X="0.003863223476" Y="-3.20543837044" Z="2.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.5" />
                  <Point X="0.255855602078" Y="-3.286469690123" Z="2.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.5" />
                  <Point X="0.811560009968" Y="-4.990967320342" Z="2.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.5" />
                  <Point X="0.820417939353" Y="-5.013263401872" Z="2.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.5" />
                  <Point X="1.000740206392" Y="-4.980402952187" Z="2.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.5" />
                  <Point X="0.960695882631" Y="-3.298360218998" Z="2.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.5" />
                  <Point X="0.958166344025" Y="-3.269138473286" Z="2.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.5" />
                  <Point X="1.013904411061" Y="-3.023044139116" Z="2.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.5" />
                  <Point X="1.194697861121" Y="-2.875348387402" Z="2.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.5" />
                  <Point X="1.427480166139" Y="-2.856315852334" Z="2.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.5" />
                  <Point X="2.646422138372" Y="-4.306288336414" Z="2.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.5" />
                  <Point X="2.665023494066" Y="-4.324723779245" Z="2.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.5" />
                  <Point X="2.860159098462" Y="-4.198222944594" Z="2.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.5" />
                  <Point X="2.283058313457" Y="-2.742773908323" Z="2.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.5" />
                  <Point X="2.270641830715" Y="-2.719003671479" Z="2.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.5" />
                  <Point X="2.233651731628" Y="-2.503470888155" Z="2.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.5" />
                  <Point X="2.329427490217" Y="-2.325249578339" Z="2.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.5" />
                  <Point X="2.509503132034" Y="-2.232806178776" Z="2.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.5" />
                  <Point X="4.044639688783" Y="-3.034691186818" Z="2.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.5" />
                  <Point X="4.067777389652" Y="-3.04272967923" Z="2.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.5" />
                  <Point X="4.242154101566" Y="-2.794484614797" Z="2.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.5" />
                  <Point X="3.211139034035" Y="-1.628708938924" Z="2.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.5" />
                  <Point X="3.191210698985" Y="-1.612209908391" Z="2.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.5" />
                  <Point X="3.092502341785" Y="-1.455696150305" Z="2.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.5" />
                  <Point X="3.109664836429" Y="-1.285359675513" Z="2.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.5" />
                  <Point X="3.220504024901" Y="-1.154782438881" Z="2.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.5" />
                  <Point X="4.884017200855" Y="-1.311387155841" Z="2.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.5" />
                  <Point X="4.908294155754" Y="-1.308772156914" Z="2.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.5" />
                  <Point X="4.992301071731" Y="-0.938700788063" Z="2.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.5" />
                  <Point X="3.76777607314" Y="-0.226122293549" Z="2.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.5" />
                  <Point X="3.746542105864" Y="-0.219995288379" Z="2.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.5" />
                  <Point X="3.654596358413" Y="-0.166259750576" Z="2.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.5" />
                  <Point X="3.59965460495" Y="-0.095137412217" Z="2.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.5" />
                  <Point X="3.581716845474" Y="0.001473118956" Z="2.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.5" />
                  <Point X="3.600783079987" Y="0.097688987999" Z="2.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.5" />
                  <Point X="3.656853308488" Y="0.168153555863" Z="2.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.5" />
                  <Point X="5.028191744166" Y="0.56384963922" Z="2.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.5" />
                  <Point X="5.047010239293" Y="0.57561546551" Z="2.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.5" />
                  <Point X="4.980567842339" Y="0.998787150061" Z="2.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.5" />
                  <Point X="3.48473797933" Y="1.224869958804" Z="2.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.5" />
                  <Point X="3.461685648805" Y="1.222213837023" Z="2.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.5" />
                  <Point X="3.367898715572" Y="1.235066338931" Z="2.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.5" />
                  <Point X="3.298585769598" Y="1.274784828066" Z="2.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.5" />
                  <Point X="3.25099157923" Y="1.348022134343" Z="2.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.5" />
                  <Point X="3.233920230973" Y="1.433522760962" Z="2.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.5" />
                  <Point X="3.255997049412" Y="1.510463119403" Z="2.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.5" />
                  <Point X="4.430014318854" Y="2.441888449425" Z="2.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.5" />
                  <Point X="4.444123092444" Y="2.460430835126" Z="2.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.5" />
                  <Point X="4.234586620218" Y="2.805746901938" Z="2.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.5" />
                  <Point X="2.532633804763" Y="2.28013688321" Z="2.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.5" />
                  <Point X="2.508653713849" Y="2.266671399139" Z="2.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.5" />
                  <Point X="2.428533064189" Y="2.245656821337" Z="2.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.5" />
                  <Point X="2.359201455744" Y="2.254555545308" Z="2.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.5" />
                  <Point X="2.296203054927" Y="2.297823404638" Z="2.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.5" />
                  <Point X="2.253772881421" Y="2.361225379643" Z="2.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.5" />
                  <Point X="2.245856543518" Y="2.430815862033" Z="2.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.5" />
                  <Point X="3.115488469649" Y="3.979505277192" Z="2.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.5" />
                  <Point X="3.122906615617" Y="4.006328888027" Z="2.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.5" />
                  <Point X="2.747433167234" Y="4.272565293294" Z="2.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.5" />
                  <Point X="2.349484631596" Y="4.503689454734" Z="2.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.5" />
                  <Point X="1.937515857713" Y="4.695669622845" Z="2.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.5" />
                  <Point X="1.506762649233" Y="4.850697268768" Z="2.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.5" />
                  <Point X="0.852352950287" Y="5.007296780804" Z="2.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.5" />
                  <Point X="0.002946596304" Y="4.366121321251" Z="2.5" />
                  <Point X="0" Y="4.355124473572" Z="2.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>