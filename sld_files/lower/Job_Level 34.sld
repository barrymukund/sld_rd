<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#183" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2481" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.823722473145" Y="-29.48442578125" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557720153809" Y="-28.497138671875" />
                  <Point X="0.54236315918" Y="-28.467376953125" />
                  <Point X="0.435922729492" Y="-28.314017578125" />
                  <Point X="0.378635375977" Y="-28.2314765625" />
                  <Point X="0.356752502441" Y="-28.2090234375" />
                  <Point X="0.330497314453" Y="-28.189779296875" />
                  <Point X="0.302495025635" Y="-28.175669921875" />
                  <Point X="0.137784515381" Y="-28.12455078125" />
                  <Point X="0.049135883331" Y="-28.097037109375" />
                  <Point X="0.020983165741" Y="-28.092767578125" />
                  <Point X="-0.008657706261" Y="-28.092765625" />
                  <Point X="-0.036823932648" Y="-28.09703515625" />
                  <Point X="-0.201534286499" Y="-28.14815625" />
                  <Point X="-0.290183074951" Y="-28.17566796875" />
                  <Point X="-0.318183410645" Y="-28.189775390625" />
                  <Point X="-0.344439331055" Y="-28.20901953125" />
                  <Point X="-0.366323577881" Y="-28.2314765625" />
                  <Point X="-0.47276385498" Y="-28.384837890625" />
                  <Point X="-0.530051208496" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.691150817871" Y="-29.035611328125" />
                  <Point X="-0.916584838867" Y="-29.87694140625" />
                  <Point X="-0.978742004395" Y="-29.864876953125" />
                  <Point X="-1.079353271484" Y="-29.84534765625" />
                  <Point X="-1.24641796875" Y="-29.80236328125" />
                  <Point X="-1.246311523438" Y="-29.8015546875" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.255858154297" Y="-29.318404296875" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131203125" />
                  <Point X="-1.47528918457" Y="-28.99821484375" />
                  <Point X="-1.556905639648" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886474609" Y="-28.897994140625" />
                  <Point X="-1.643027709961" Y="-28.890966796875" />
                  <Point X="-1.844293579102" Y="-28.877775390625" />
                  <Point X="-1.952616821289" Y="-28.87067578125" />
                  <Point X="-1.983414794922" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.210363037109" Y="-29.006857421875" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.413792236328" Y="-29.202013671875" />
                  <Point X="-2.480147460938" Y="-29.28848828125" />
                  <Point X="-2.654258544922" Y="-29.18068359375" />
                  <Point X="-2.801712890625" Y="-29.089384765625" />
                  <Point X="-3.062044677734" Y="-28.8889375" />
                  <Point X="-3.104721435547" Y="-28.856078125" />
                  <Point X="-2.907723144531" Y="-28.5148671875" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412859130859" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616126953125" />
                  <Point X="-2.405575195312" Y="-27.5851953125" />
                  <Point X="-2.414559326172" Y="-27.555578125" />
                  <Point X="-2.428776367188" Y="-27.526748046875" />
                  <Point X="-2.446806396484" Y="-27.5015859375" />
                  <Point X="-2.464155029297" Y="-27.48423828125" />
                  <Point X="-2.489311035156" Y="-27.466212890625" />
                  <Point X="-2.518139892578" Y="-27.45199609375" />
                  <Point X="-2.547758056641" Y="-27.44301171875" />
                  <Point X="-2.578692138672" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.090336181641" Y="-27.721671875" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.966365966797" Y="-27.94691015625" />
                  <Point X="-4.082862060547" Y="-27.793857421875" />
                  <Point X="-4.269498535156" Y="-27.480896484375" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.952730957031" Y="-27.14826953125" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.35965625" />
                  <Point X="-3.045556884766" Y="-26.327984375" />
                  <Point X="-3.05255859375" Y="-26.29823828125" />
                  <Point X="-3.068641845703" Y="-26.272255859375" />
                  <Point X="-3.089475830078" Y="-26.248298828125" />
                  <Point X="-3.112975585938" Y="-26.228765625" />
                  <Point X="-3.139458251953" Y="-26.2131796875" />
                  <Point X="-3.168722412109" Y="-26.201955078125" />
                  <Point X="-3.200608398438" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-3.801466796875" Y="-26.269365234375" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.788515136719" Y="-26.171029296875" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.883457519531" Y="-25.647396484375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.497481933594" Y="-25.478875" />
                  <Point X="-3.532875732422" Y="-25.22041015625" />
                  <Point X="-3.517485351562" Y="-25.21482421875" />
                  <Point X="-3.487729736328" Y="-25.199470703125" />
                  <Point X="-3.4696875" Y="-25.18694921875" />
                  <Point X="-3.459976806641" Y="-25.180208984375" />
                  <Point X="-3.437520263672" Y="-25.15832421875" />
                  <Point X="-3.418278076172" Y="-25.132072265625" />
                  <Point X="-3.404168457031" Y="-25.104068359375" />
                  <Point X="-3.398154296875" Y="-25.084689453125" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.3906484375" Y="-25.04610546875" />
                  <Point X="-3.390647705078" Y="-25.016462890625" />
                  <Point X="-3.394916748047" Y="-24.988302734375" />
                  <Point X="-3.400930908203" Y="-24.968923828125" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.418276367188" Y="-24.9304921875" />
                  <Point X="-3.437519287109" Y="-24.90423828125" />
                  <Point X="-3.459976806641" Y="-24.8823515625" />
                  <Point X="-3.478019042969" Y="-24.869830078125" />
                  <Point X="-3.486252929688" Y="-24.86471875" />
                  <Point X="-3.511532958984" Y="-24.850755859375" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-4.052036621094" Y="-24.70304296875" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.853851074219" Y="-24.221458984375" />
                  <Point X="-4.82448828125" Y="-24.023029296875" />
                  <Point X="-4.725087890625" Y="-23.6562109375" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.457119140625" Y="-23.60917578125" />
                  <Point X="-3.765666503906" Y="-23.70020703125" />
                  <Point X="-3.744985839844" Y="-23.700658203125" />
                  <Point X="-3.723422607422" Y="-23.698771484375" />
                  <Point X="-3.703138427734" Y="-23.694736328125" />
                  <Point X="-3.663205078125" Y="-23.682146484375" />
                  <Point X="-3.641712402344" Y="-23.675369140625" />
                  <Point X="-3.622774658203" Y="-23.667037109375" />
                  <Point X="-3.60403125" Y="-23.65621484375" />
                  <Point X="-3.587351806641" Y="-23.643984375" />
                  <Point X="-3.573714599609" Y="-23.62843359375" />
                  <Point X="-3.56130078125" Y="-23.610705078125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.535328125" Y="-23.55388671875" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.515804443359" Y="-23.47125" />
                  <Point X="-3.518951660156" Y="-23.4508046875" />
                  <Point X="-3.524553955078" Y="-23.4298984375" />
                  <Point X="-3.532050292969" Y="-23.410623046875" />
                  <Point X="-3.551384277344" Y="-23.373482421875" />
                  <Point X="-3.561790039062" Y="-23.353494140625" />
                  <Point X="-3.573281494141" Y="-23.336294921875" />
                  <Point X="-3.587193603516" Y="-23.31971484375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-3.899927734375" Y="-23.07690625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.195247558594" Y="-22.461810546875" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.817847167969" Y="-21.9278984375" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.631328369141" Y="-21.91014453125" />
                  <Point X="-3.206657226562" Y="-22.155328125" />
                  <Point X="-3.187729980469" Y="-22.163658203125" />
                  <Point X="-3.167088378906" Y="-22.17016796875" />
                  <Point X="-3.146794189453" Y="-22.174205078125" />
                  <Point X="-3.091177978516" Y="-22.1790703125" />
                  <Point X="-3.061244873047" Y="-22.181689453125" />
                  <Point X="-3.040561523438" Y="-22.18123828125" />
                  <Point X="-3.019102783203" Y="-22.178412109375" />
                  <Point X="-2.999013916016" Y="-22.17349609375" />
                  <Point X="-2.980465087891" Y="-22.16434765625" />
                  <Point X="-2.962210693359" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.906601074219" Y="-22.100294921875" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.848301757812" Y="-21.908263671875" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-3.001755859375" Y="-21.58990625" />
                  <Point X="-3.183333007812" Y="-21.27540625" />
                  <Point X="-2.899336425781" Y="-21.05766796875" />
                  <Point X="-2.700621337891" Y="-20.905314453125" />
                  <Point X="-2.285928466797" Y="-20.674919921875" />
                  <Point X="-2.167037353516" Y="-20.6088671875" />
                  <Point X="-2.043195922852" Y="-20.7702578125" />
                  <Point X="-2.028892700195" Y="-20.78519921875" />
                  <Point X="-2.012313476562" Y="-20.799111328125" />
                  <Point X="-1.995114868164" Y="-20.810603515625" />
                  <Point X="-1.933214355469" Y="-20.842828125" />
                  <Point X="-1.899898803711" Y="-20.860171875" />
                  <Point X="-1.880625732422" Y="-20.86766796875" />
                  <Point X="-1.859719116211" Y="-20.873271484375" />
                  <Point X="-1.839269287109" Y="-20.876419921875" />
                  <Point X="-1.818622802734" Y="-20.87506640625" />
                  <Point X="-1.797307373047" Y="-20.871306640625" />
                  <Point X="-1.777452880859" Y="-20.865517578125" />
                  <Point X="-1.712979370117" Y="-20.838810546875" />
                  <Point X="-1.678279052734" Y="-20.8244375" />
                  <Point X="-1.660147094727" Y="-20.814490234375" />
                  <Point X="-1.642417236328" Y="-20.802076171875" />
                  <Point X="-1.626864868164" Y="-20.7884375" />
                  <Point X="-1.614633300781" Y="-20.771755859375" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734078125" />
                  <Point X="-1.574495239258" Y="-20.667521484375" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.572732666016" Y="-20.45521875" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.206878540039" Y="-20.262314453125" />
                  <Point X="-0.949623840332" Y="-20.19019140625" />
                  <Point X="-0.446900909424" Y="-20.131353515625" />
                  <Point X="-0.294711456299" Y="-20.113541015625" />
                  <Point X="-0.259532806396" Y="-20.244830078125" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113983154" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907631" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425933838" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.213830307007" Y="-20.461341796875" />
                  <Point X="0.307419525146" Y="-20.1120625" />
                  <Point X="0.619423217773" Y="-20.14473828125" />
                  <Point X="0.844045227051" Y="-20.16826171875" />
                  <Point X="1.259970581055" Y="-20.2686796875" />
                  <Point X="1.481023681641" Y="-20.322048828125" />
                  <Point X="1.751239135742" Y="-20.420056640625" />
                  <Point X="1.894645751953" Y="-20.472072265625" />
                  <Point X="2.156430175781" Y="-20.5945" />
                  <Point X="2.294559082031" Y="-20.659099609375" />
                  <Point X="2.547496337891" Y="-20.806458984375" />
                  <Point X="2.680988525391" Y="-20.884232421875" />
                  <Point X="2.919493408203" Y="-21.05384375" />
                  <Point X="2.943260742188" Y="-21.07074609375" />
                  <Point X="2.707730712891" Y="-21.4786953125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.119119140625" Y="-22.536140625" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.113170410156" Y="-22.664181640625" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442138672" Y="-22.710396484375" />
                  <Point X="2.129708740234" Y="-22.732486328125" />
                  <Point X="2.140070800781" Y="-22.752529296875" />
                  <Point X="2.167997802734" Y="-22.7936875" />
                  <Point X="2.183028564453" Y="-22.815837890625" />
                  <Point X="2.194470214844" Y="-22.829677734375" />
                  <Point X="2.221597167969" Y="-22.85440625" />
                  <Point X="2.262754394531" Y="-22.882333984375" />
                  <Point X="2.284905761719" Y="-22.897365234375" />
                  <Point X="2.3049453125" Y="-22.907724609375" />
                  <Point X="2.327033203125" Y="-22.9159921875" />
                  <Point X="2.348964599609" Y="-22.921337890625" />
                  <Point X="2.394098144531" Y="-22.926779296875" />
                  <Point X="2.418389404297" Y="-22.929708984375" />
                  <Point X="2.436467285156" Y="-22.93015625" />
                  <Point X="2.473208251953" Y="-22.925830078125" />
                  <Point X="2.525402832031" Y="-22.91187109375" />
                  <Point X="2.553494628906" Y="-22.904359375" />
                  <Point X="2.565284667969" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.110709472656" Y="-22.588375" />
                  <Point X="3.967325439453" Y="-22.09380859375" />
                  <Point X="4.044094238281" Y="-22.2005" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.256241210938" Y="-22.530271484375" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.9682734375" Y="-22.76565234375" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221421630859" Y="-23.33976171875" />
                  <Point X="3.203974121094" Y="-23.358373046875" />
                  <Point X="3.166409423828" Y="-23.40737890625" />
                  <Point X="3.146191894531" Y="-23.43375390625" />
                  <Point X="3.136607421875" Y="-23.4490859375" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.107636962891" Y="-23.53294921875" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.109226074219" Y="-23.68390234375" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120680419922" Y="-23.7310234375" />
                  <Point X="3.136282958984" Y="-23.764259765625" />
                  <Point X="3.167525390625" Y="-23.81174609375" />
                  <Point X="3.184340576172" Y="-23.8373046875" />
                  <Point X="3.198893066406" Y="-23.854548828125" />
                  <Point X="3.21613671875" Y="-23.870638671875" />
                  <Point X="3.234346191406" Y="-23.88396484375" />
                  <Point X="3.279620849609" Y="-23.909451171875" />
                  <Point X="3.30398828125" Y="-23.92316796875" />
                  <Point X="3.320522216797" Y="-23.9305" />
                  <Point X="3.356120117188" Y="-23.9405625" />
                  <Point X="3.417334472656" Y="-23.94865234375" />
                  <Point X="3.450280761719" Y="-23.953005859375" />
                  <Point X="3.462698974609" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="3.984235351562" Y="-23.887712890625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.8119296875" Y="-23.92750390625" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.887838378906" Y="-24.336322265625" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.561758789062" Y="-24.4439453125" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704787597656" Y="-24.674416015625" />
                  <Point X="3.681547607422" Y="-24.684931640625" />
                  <Point X="3.62140625" Y="-24.719693359375" />
                  <Point X="3.589037597656" Y="-24.73840234375" />
                  <Point X="3.574310302734" Y="-24.748904296875" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.511445556641" Y="-24.820404296875" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480300537109" Y="-24.86443359375" />
                  <Point X="3.470527099609" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.45165234375" Y="-24.970203125" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.45720703125" Y="-25.121361328125" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470526855469" Y="-25.1766640625" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.528109130859" Y="-25.263388671875" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.559997802734" Y="-25.301234375" />
                  <Point X="3.589035644531" Y="-25.32415625" />
                  <Point X="3.649177001953" Y="-25.35891796875" />
                  <Point X="3.681545654297" Y="-25.37762890625" />
                  <Point X="3.692698486328" Y="-25.383134765625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.171463867187" Y="-25.514037109375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.874010253906" Y="-25.82278515625" />
                  <Point X="4.855021484375" Y="-25.948732421875" />
                  <Point X="4.801338378906" Y="-26.183978515625" />
                  <Point X="4.801174316406" Y="-26.18469921875" />
                  <Point X="4.404551757812" Y="-26.132482421875" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035400391" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.256623046875" Y="-26.031166015625" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163975830078" Y="-26.056595703125" />
                  <Point X="3.136148193359" Y="-26.07348828125" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.041051757813" Y="-26.179767578125" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.959531982422" Y="-26.41648828125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.507564453125" />
                  <Point X="2.964078613281" Y="-26.539185546875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.041773681641" Y="-26.6696015625" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086931396484" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.532762939453" Y="-27.084826171875" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.178334472656" Y="-27.66317578125" />
                  <Point X="4.124813964844" Y="-27.74978125" />
                  <Point X="4.028979980469" Y="-27.8859453125" />
                  <Point X="3.673927978516" Y="-27.68095703125" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786128417969" Y="-27.17001171875" />
                  <Point X="2.754224365234" Y="-27.159826171875" />
                  <Point X="2.613742675781" Y="-27.134455078125" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474610839844" Y="-27.125353515625" />
                  <Point X="2.444833496094" Y="-27.135177734375" />
                  <Point X="2.328127685547" Y="-27.19659765625" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242384277344" Y="-27.246548828125" />
                  <Point X="2.221425048828" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.143110595703" Y="-27.40714453125" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.121046386719" Y="-27.703740234375" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.423083251953" Y="-28.295921875" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.845363769531" Y="-29.06627734375" />
                  <Point X="2.781833251953" Y="-29.11165625" />
                  <Point X="2.701764404297" Y="-29.163482421875" />
                  <Point X="2.424705566406" Y="-28.802412109375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506469727" Y="-27.922181640625" />
                  <Point X="1.721923706055" Y="-27.900556640625" />
                  <Point X="1.583370849609" Y="-27.81148046875" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.75116796875" />
                  <Point X="1.448366333008" Y="-27.7434375" />
                  <Point X="1.417100463867" Y="-27.741119140625" />
                  <Point X="1.265569335938" Y="-27.7550625" />
                  <Point X="1.184013549805" Y="-27.76256640625" />
                  <Point X="1.15636328125" Y="-27.7693984375" />
                  <Point X="1.128977050781" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.7954609375" />
                  <Point X="0.987586608887" Y="-27.89275" />
                  <Point X="0.92461151123" Y="-27.94511328125" />
                  <Point X="0.904140441895" Y="-27.968865234375" />
                  <Point X="0.887248596191" Y="-27.99669140625" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.840639404297" Y="-28.186767578125" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.896616210938" Y="-28.907037109375" />
                  <Point X="1.022065551758" Y="-29.859916015625" />
                  <Point X="0.975708374023" Y="-29.870078125" />
                  <Point X="0.929315734863" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058445068359" Y="-29.7526328125" />
                  <Point X="-1.141246337891" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.54103515625" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.16268359375" Y="-29.29987109375" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026245117" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230573242188" Y="-29.094861328125" />
                  <Point X="-1.250208251953" Y="-29.070935546875" />
                  <Point X="-1.261006958008" Y="-29.05977734375" />
                  <Point X="-1.412651245117" Y="-28.9267890625" />
                  <Point X="-1.494267822266" Y="-28.855212890625" />
                  <Point X="-1.506740112305" Y="-28.84596484375" />
                  <Point X="-1.533023681641" Y="-28.82962109375" />
                  <Point X="-1.546834838867" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591316040039" Y="-28.805474609375" />
                  <Point X="-1.621457275391" Y="-28.798447265625" />
                  <Point X="-1.636814575195" Y="-28.796169921875" />
                  <Point X="-1.838080444336" Y="-28.782978515625" />
                  <Point X="-1.946403686523" Y="-28.77587890625" />
                  <Point X="-1.961928100586" Y="-28.7761328125" />
                  <Point X="-1.992726074219" Y="-28.779166015625" />
                  <Point X="-2.007999511719" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053667480469" Y="-28.795494140625" />
                  <Point X="-2.081861816406" Y="-28.808267578125" />
                  <Point X="-2.095436523437" Y="-28.815810546875" />
                  <Point X="-2.263142089844" Y="-28.9278671875" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359683349609" Y="-28.9927578125" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.489160888672" Y="-29.144181640625" />
                  <Point X="-2.503200195312" Y="-29.162478515625" />
                  <Point X="-2.604247558594" Y="-29.099912109375" />
                  <Point X="-2.747599365234" Y="-29.011154296875" />
                  <Point X="-2.980862548828" Y="-28.831548828125" />
                  <Point X="-2.825450683594" Y="-28.5623671875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684570312" Y="-27.666185546875" />
                  <Point X="-2.313413574219" Y="-27.63466015625" />
                  <Point X="-2.311638916016" Y="-27.619236328125" />
                  <Point X="-2.310625976562" Y="-27.5883046875" />
                  <Point X="-2.314665771484" Y="-27.557619140625" />
                  <Point X="-2.323649902344" Y="-27.528001953125" />
                  <Point X="-2.329355957031" Y="-27.5135625" />
                  <Point X="-2.343572998047" Y="-27.484732421875" />
                  <Point X="-2.3515546875" Y="-27.4714140625" />
                  <Point X="-2.369584716797" Y="-27.446251953125" />
                  <Point X="-2.379633056641" Y="-27.434408203125" />
                  <Point X="-2.396981689453" Y="-27.417060546875" />
                  <Point X="-2.408822021484" Y="-27.407015625" />
                  <Point X="-2.433978027344" Y="-27.388990234375" />
                  <Point X="-2.447293701172" Y="-27.381009765625" />
                  <Point X="-2.476122558594" Y="-27.36679296875" />
                  <Point X="-2.490563476562" Y="-27.3610859375" />
                  <Point X="-2.520181640625" Y="-27.3521015625" />
                  <Point X="-2.550869384766" Y="-27.3480625" />
                  <Point X="-2.581803466797" Y="-27.349076171875" />
                  <Point X="-2.597227050781" Y="-27.3508515625" />
                  <Point X="-2.628753417969" Y="-27.357123046875" />
                  <Point X="-2.643684082031" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.137836181641" Y="-27.639400390625" />
                  <Point X="-3.793088134766" Y="-28.0177109375" />
                  <Point X="-3.890772460938" Y="-27.889373046875" />
                  <Point X="-4.004015380859" Y="-27.74059375" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.894898681641" Y="-27.223638671875" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.398802734375" />
                  <Point X="-2.948748535156" Y="-26.368373046875" />
                  <Point X="-2.948577880859" Y="-26.353044921875" />
                  <Point X="-2.950787109375" Y="-26.321373046875" />
                  <Point X="-2.953083984375" Y="-26.30621875" />
                  <Point X="-2.960085693359" Y="-26.27647265625" />
                  <Point X="-2.971781982422" Y="-26.248236328125" />
                  <Point X="-2.987865234375" Y="-26.22225390625" />
                  <Point X="-2.99695703125" Y="-26.209916015625" />
                  <Point X="-3.017791015625" Y="-26.185958984375" />
                  <Point X="-3.02875" Y="-26.1752421875" />
                  <Point X="-3.052249755859" Y="-26.155708984375" />
                  <Point X="-3.064790527344" Y="-26.146892578125" />
                  <Point X="-3.091273193359" Y="-26.131306640625" />
                  <Point X="-3.105436767578" Y="-26.12448046875" />
                  <Point X="-3.134700927734" Y="-26.113255859375" />
                  <Point X="-3.149801513672" Y="-26.108857421875" />
                  <Point X="-3.1816875" Y="-26.102376953125" />
                  <Point X="-3.1973046875" Y="-26.10053125" />
                  <Point X="-3.228625488281" Y="-26.09944140625" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-3.813866699219" Y="-26.175177734375" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.696470214844" Y="-26.147517578125" />
                  <Point X="-4.740762695312" Y="-25.974115234375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.472893554688" Y="-25.570638671875" />
                  <Point X="-3.508287597656" Y="-25.312173828125" />
                  <Point X="-3.500464355469" Y="-25.3097109375" />
                  <Point X="-3.473923828125" Y="-25.299248046875" />
                  <Point X="-3.444168212891" Y="-25.28389453125" />
                  <Point X="-3.433564941406" Y="-25.277517578125" />
                  <Point X="-3.415522705078" Y="-25.26499609375" />
                  <Point X="-3.393673339844" Y="-25.248244140625" />
                  <Point X="-3.371216796875" Y="-25.226359375" />
                  <Point X="-3.360898925781" Y="-25.214486328125" />
                  <Point X="-3.341656738281" Y="-25.188234375" />
                  <Point X="-3.333438476563" Y="-25.174818359375" />
                  <Point X="-3.319328857422" Y="-25.146814453125" />
                  <Point X="-3.3134375" Y="-25.1322265625" />
                  <Point X="-3.307423339844" Y="-25.11284765625" />
                  <Point X="-3.300990966797" Y="-25.088501953125" />
                  <Point X="-3.296721923828" Y="-25.060345703125" />
                  <Point X="-3.2956484375" Y="-25.046107421875" />
                  <Point X="-3.295647705078" Y="-25.01646484375" />
                  <Point X="-3.296720947266" Y="-25.002224609375" />
                  <Point X="-3.300989990234" Y="-24.974064453125" />
                  <Point X="-3.304185791016" Y="-24.96014453125" />
                  <Point X="-3.310199951172" Y="-24.940765625" />
                  <Point X="-3.319328125" Y="-24.915748046875" />
                  <Point X="-3.333436767578" Y="-24.88774609375" />
                  <Point X="-3.341654052734" Y="-24.87433203125" />
                  <Point X="-3.360896972656" Y="-24.848078125" />
                  <Point X="-3.371214355469" Y="-24.836203125" />
                  <Point X="-3.393671875" Y="-24.81431640625" />
                  <Point X="-3.405812011719" Y="-24.8043046875" />
                  <Point X="-3.423854248047" Y="-24.791783203125" />
                  <Point X="-3.440322021484" Y="-24.781560546875" />
                  <Point X="-3.465602050781" Y="-24.76759765625" />
                  <Point X="-3.476008300781" Y="-24.7626484375" />
                  <Point X="-3.4973515625" Y="-24.75404296875" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-4.027448974609" Y="-24.611279296875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.759874511719" Y="-24.235365234375" />
                  <Point X="-4.731331542969" Y="-24.042474609375" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.469519042969" Y="-23.70336328125" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747057861328" Y="-23.795634765625" />
                  <Point X="-3.736705322266" Y="-23.795296875" />
                  <Point X="-3.715142089844" Y="-23.79341015625" />
                  <Point X="-3.704887451172" Y="-23.7919453125" />
                  <Point X="-3.684603271484" Y="-23.78791015625" />
                  <Point X="-3.674573730469" Y="-23.78533984375" />
                  <Point X="-3.634640380859" Y="-23.77275" />
                  <Point X="-3.613147705078" Y="-23.76597265625" />
                  <Point X="-3.603454345703" Y="-23.76232421875" />
                  <Point X="-3.584516601562" Y="-23.7539921875" />
                  <Point X="-3.575272216797" Y="-23.74930859375" />
                  <Point X="-3.556528808594" Y="-23.738486328125" />
                  <Point X="-3.547854980469" Y="-23.732826171875" />
                  <Point X="-3.531175537109" Y="-23.720595703125" />
                  <Point X="-3.515926025391" Y="-23.70662109375" />
                  <Point X="-3.502288818359" Y="-23.6910703125" />
                  <Point X="-3.495895507812" Y="-23.682923828125" />
                  <Point X="-3.483481689453" Y="-23.6651953125" />
                  <Point X="-3.478011962891" Y="-23.656400390625" />
                  <Point X="-3.468062744141" Y="-23.638265625" />
                  <Point X="-3.463583251953" Y="-23.62892578125" />
                  <Point X="-3.447559814453" Y="-23.5902421875" />
                  <Point X="-3.438935791016" Y="-23.569421875" />
                  <Point X="-3.435498779297" Y="-23.5596484375" />
                  <Point X="-3.429710205078" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.4210078125" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057617188" Y="-23.4363515625" />
                  <Point X="-3.427189208984" Y="-23.42621484375" />
                  <Point X="-3.432791503906" Y="-23.40530859375" />
                  <Point X="-3.436014160156" Y="-23.39546484375" />
                  <Point X="-3.443510498047" Y="-23.376189453125" />
                  <Point X="-3.447784179688" Y="-23.3667578125" />
                  <Point X="-3.467118164062" Y="-23.3296171875" />
                  <Point X="-3.477523925781" Y="-23.30962890625" />
                  <Point X="-3.482799072266" Y="-23.300716796875" />
                  <Point X="-3.494290527344" Y="-23.283517578125" />
                  <Point X="-3.500506835938" Y="-23.27523046875" />
                  <Point X="-3.514418945312" Y="-23.258650390625" />
                  <Point X="-3.521498779297" Y="-23.251091796875" />
                  <Point X="-3.536441162109" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-3.842095458984" Y="-23.001537109375" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.113201171875" Y="-22.50969921875" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.742866455078" Y="-21.986232421875" />
                  <Point X="-3.726337402344" Y="-21.964986328125" />
                  <Point X="-3.678828613281" Y="-21.992416015625" />
                  <Point X="-3.254157470703" Y="-22.237599609375" />
                  <Point X="-3.244925537109" Y="-22.242279296875" />
                  <Point X="-3.225998291016" Y="-22.250609375" />
                  <Point X="-3.216302978516" Y="-22.254259765625" />
                  <Point X="-3.195661376953" Y="-22.26076953125" />
                  <Point X="-3.185623535156" Y="-22.263341796875" />
                  <Point X="-3.165329345703" Y="-22.26737890625" />
                  <Point X="-3.155072998047" Y="-22.26884375" />
                  <Point X="-3.099456787109" Y="-22.273708984375" />
                  <Point X="-3.069523681641" Y="-22.276328125" />
                  <Point X="-3.059173095703" Y="-22.276666015625" />
                  <Point X="-3.038489746094" Y="-22.27621484375" />
                  <Point X="-3.028156982422" Y="-22.27542578125" />
                  <Point X="-3.006698242188" Y="-22.272599609375" />
                  <Point X="-2.996521240234" Y="-22.270689453125" />
                  <Point X="-2.976432373047" Y="-22.2657734375" />
                  <Point X="-2.9569921875" Y="-22.258697265625" />
                  <Point X="-2.938443359375" Y="-22.249548828125" />
                  <Point X="-2.929422851563" Y="-22.244470703125" />
                  <Point X="-2.911168457031" Y="-22.232841796875" />
                  <Point X="-2.902749755859" Y="-22.22680859375" />
                  <Point X="-2.886616943359" Y="-22.213861328125" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.839426025391" Y="-22.167470703125" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.753663330078" Y="-21.899984375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.919483642578" Y="-21.54240625" />
                  <Point X="-3.059387207031" Y="-21.300087890625" />
                  <Point X="-2.841534179688" Y="-21.133060546875" />
                  <Point X="-2.648368652344" Y="-20.9849609375" />
                  <Point X="-2.239791015625" Y="-20.75796484375" />
                  <Point X="-2.192524169922" Y="-20.731705078125" />
                  <Point X="-2.118563964844" Y="-20.82808984375" />
                  <Point X="-2.111820556641" Y="-20.835951171875" />
                  <Point X="-2.097517333984" Y="-20.850892578125" />
                  <Point X="-2.089958740234" Y="-20.85797265625" />
                  <Point X="-2.073379638672" Y="-20.871884765625" />
                  <Point X="-2.065093994141" Y="-20.878099609375" />
                  <Point X="-2.047895507812" Y="-20.889591796875" />
                  <Point X="-2.038982299805" Y="-20.894869140625" />
                  <Point X="-1.977081787109" Y="-20.92709375" />
                  <Point X="-1.943766235352" Y="-20.9444375" />
                  <Point X="-1.934335205078" Y="-20.9487109375" />
                  <Point X="-1.915062133789" Y="-20.95620703125" />
                  <Point X="-1.905220092773" Y="-20.9594296875" />
                  <Point X="-1.884313476562" Y="-20.965033203125" />
                  <Point X="-1.874174926758" Y="-20.967166015625" />
                  <Point X="-1.853725097656" Y="-20.970314453125" />
                  <Point X="-1.8330546875" Y="-20.971216796875" />
                  <Point X="-1.812408325195" Y="-20.96986328125" />
                  <Point X="-1.802120727539" Y="-20.968623046875" />
                  <Point X="-1.780805297852" Y="-20.96486328125" />
                  <Point X="-1.770715087891" Y="-20.962509765625" />
                  <Point X="-1.750860595703" Y="-20.956720703125" />
                  <Point X="-1.741096679688" Y="-20.95328515625" />
                  <Point X="-1.676623168945" Y="-20.926578125" />
                  <Point X="-1.641922729492" Y="-20.912205078125" />
                  <Point X="-1.63258605957" Y="-20.9077265625" />
                  <Point X="-1.614454101562" Y="-20.897779296875" />
                  <Point X="-1.605658813477" Y="-20.892310546875" />
                  <Point X="-1.587928955078" Y="-20.879896484375" />
                  <Point X="-1.579780395508" Y="-20.873501953125" />
                  <Point X="-1.564228027344" Y="-20.85986328125" />
                  <Point X="-1.550252807617" Y="-20.844611328125" />
                  <Point X="-1.538021240234" Y="-20.8279296875" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.52153918457" Y="-20.80051171875" />
                  <Point X="-1.516856445312" Y="-20.791271484375" />
                  <Point X="-1.508525512695" Y="-20.772337890625" />
                  <Point X="-1.504877319336" Y="-20.76264453125" />
                  <Point X="-1.483892089844" Y="-20.696087890625" />
                  <Point X="-1.47259777832" Y="-20.660267578125" />
                  <Point X="-1.470026611328" Y="-20.650236328125" />
                  <Point X="-1.465991333008" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.478545288086" Y="-20.442818359375" />
                  <Point X="-1.479265869141" Y="-20.437345703125" />
                  <Point X="-1.181232543945" Y="-20.353787109375" />
                  <Point X="-0.93116394043" Y="-20.2836796875" />
                  <Point X="-0.43585760498" Y="-20.225708984375" />
                  <Point X="-0.365222686768" Y="-20.21744140625" />
                  <Point X="-0.351295776367" Y="-20.26941796875" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.305593353271" Y="-20.4859296875" />
                  <Point X="0.378190673828" Y="-20.2149921875" />
                  <Point X="0.609528076172" Y="-20.239220703125" />
                  <Point X="0.82786529541" Y="-20.2620859375" />
                  <Point X="1.237675048828" Y="-20.36102734375" />
                  <Point X="1.453608520508" Y="-20.41316015625" />
                  <Point X="1.718847167969" Y="-20.50936328125" />
                  <Point X="1.858247436523" Y="-20.55992578125" />
                  <Point X="2.116185546875" Y="-20.6805546875" />
                  <Point X="2.250433837891" Y="-20.74333984375" />
                  <Point X="2.499674072266" Y="-20.888544921875" />
                  <Point X="2.629443847656" Y="-20.964150390625" />
                  <Point X="2.817780517578" Y="-21.098083984375" />
                  <Point X="2.625458251953" Y="-21.4311953125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.02734387207" Y="-22.511599609375" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017912231445" Y="-22.5485390625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.018853637695" Y="-22.6755546875" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.023800537109" Y="-22.71096484375" />
                  <Point X="2.029143310547" Y="-22.732888671875" />
                  <Point X="2.032468261719" Y="-22.743693359375" />
                  <Point X="2.040734863281" Y="-22.765783203125" />
                  <Point X="2.045319335938" Y="-22.776115234375" />
                  <Point X="2.055681640625" Y="-22.796158203125" />
                  <Point X="2.061458984375" Y="-22.805869140625" />
                  <Point X="2.089385986328" Y="-22.84702734375" />
                  <Point X="2.104416748047" Y="-22.869177734375" />
                  <Point X="2.109809814453" Y="-22.876369140625" />
                  <Point X="2.130470458984" Y="-22.899884765625" />
                  <Point X="2.157597412109" Y="-22.92461328125" />
                  <Point X="2.168255126953" Y="-22.933017578125" />
                  <Point X="2.209412353516" Y="-22.9609453125" />
                  <Point X="2.231563720703" Y="-22.9759765625" />
                  <Point X="2.241280273438" Y="-22.981755859375" />
                  <Point X="2.261319824219" Y="-22.992115234375" />
                  <Point X="2.271642822266" Y="-22.9966953125" />
                  <Point X="2.293730712891" Y="-23.004962890625" />
                  <Point X="2.304535888672" Y="-23.0082890625" />
                  <Point X="2.326467285156" Y="-23.013634765625" />
                  <Point X="2.337593505859" Y="-23.015654296875" />
                  <Point X="2.382727050781" Y="-23.021095703125" />
                  <Point X="2.407018310547" Y="-23.024025390625" />
                  <Point X="2.416039794922" Y="-23.0246796875" />
                  <Point X="2.447576660156" Y="-23.02450390625" />
                  <Point X="2.484317626953" Y="-23.020177734375" />
                  <Point X="2.497752685547" Y="-23.01760546875" />
                  <Point X="2.549947265625" Y="-23.003646484375" />
                  <Point X="2.5780390625" Y="-22.996134765625" />
                  <Point X="2.584003173828" Y="-22.994328125" />
                  <Point X="2.604410644531" Y="-22.9869296875" />
                  <Point X="2.627659912109" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.158209472656" Y="-22.670646484375" />
                  <Point X="3.940403076172" Y="-22.219048828125" />
                  <Point X="3.966981689453" Y="-22.255986328125" />
                  <Point X="4.043959228516" Y="-22.362966796875" />
                  <Point X="4.136884765625" Y="-22.51652734375" />
                  <Point X="3.91044140625" Y="-22.690283203125" />
                  <Point X="3.172951904297" Y="-23.2561796875" />
                  <Point X="3.168135986328" Y="-23.2601328125" />
                  <Point X="3.152114501953" Y="-23.2747890625" />
                  <Point X="3.134666992188" Y="-23.293400390625" />
                  <Point X="3.128576660156" Y="-23.300578125" />
                  <Point X="3.091011962891" Y="-23.349583984375" />
                  <Point X="3.070794433594" Y="-23.375958984375" />
                  <Point X="3.06563671875" Y="-23.383396484375" />
                  <Point X="3.049740966797" Y="-23.410625" />
                  <Point X="3.034763427734" Y="-23.444453125" />
                  <Point X="3.030140380859" Y="-23.457328125" />
                  <Point X="3.016147460938" Y="-23.50736328125" />
                  <Point X="3.008616455078" Y="-23.53429296875" />
                  <Point X="3.006225341797" Y="-23.545337890625" />
                  <Point X="3.002771728516" Y="-23.56763671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.016186035156" Y="-23.703099609375" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024598388672" Y="-23.741767578125" />
                  <Point X="3.034684814453" Y="-23.771392578125" />
                  <Point X="3.050287353516" Y="-23.80462890625" />
                  <Point X="3.056919433594" Y="-23.816474609375" />
                  <Point X="3.088161865234" Y="-23.8639609375" />
                  <Point X="3.104977050781" Y="-23.88951953125" />
                  <Point X="3.111738525391" Y="-23.89857421875" />
                  <Point X="3.126291015625" Y="-23.915818359375" />
                  <Point X="3.13408203125" Y="-23.9240078125" />
                  <Point X="3.151325683594" Y="-23.94009765625" />
                  <Point X="3.160032226562" Y="-23.947302734375" />
                  <Point X="3.178241699219" Y="-23.96062890625" />
                  <Point X="3.187744628906" Y="-23.96675" />
                  <Point X="3.233019287109" Y="-23.992236328125" />
                  <Point X="3.25738671875" Y="-24.005953125" />
                  <Point X="3.265477050781" Y="-24.01001171875" />
                  <Point X="3.294680908203" Y="-24.02191796875" />
                  <Point X="3.330278808594" Y="-24.03198046875" />
                  <Point X="3.343673583984" Y="-24.034744140625" />
                  <Point X="3.404887939453" Y="-24.042833984375" />
                  <Point X="3.437834228516" Y="-24.0471875" />
                  <Point X="3.444033935547" Y="-24.04780078125" />
                  <Point X="3.465709228516" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="3.996635253906" Y="-23.981900390625" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.719625488281" Y="-23.949974609375" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="4.537170410156" Y="-24.352181640625" />
                  <Point X="3.691991210938" Y="-24.578646484375" />
                  <Point X="3.686022216797" Y="-24.580458984375" />
                  <Point X="3.665624511719" Y="-24.58786328125" />
                  <Point X="3.642384521484" Y="-24.59837890625" />
                  <Point X="3.634007568359" Y="-24.602681640625" />
                  <Point X="3.573866210938" Y="-24.637443359375" />
                  <Point X="3.541497558594" Y="-24.65615234375" />
                  <Point X="3.533881103516" Y="-24.6610546875" />
                  <Point X="3.508773193359" Y="-24.680130859375" />
                  <Point X="3.481993164062" Y="-24.705650390625" />
                  <Point X="3.472796142578" Y="-24.7157734375" />
                  <Point X="3.436711425781" Y="-24.76175390625" />
                  <Point X="3.417290283203" Y="-24.786501953125" />
                  <Point X="3.410852050781" Y="-24.795794921875" />
                  <Point X="3.399128173828" Y="-24.815076171875" />
                  <Point X="3.393842529297" Y="-24.825064453125" />
                  <Point X="3.384069091797" Y="-24.84652734375" />
                  <Point X="3.380005859375" Y="-24.8570703125" />
                  <Point X="3.373159423828" Y="-24.8785703125" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.358347900391" Y="-24.952333984375" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.99503515625" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280029297" Y="-25.062943359375" />
                  <Point X="3.351874267578" Y="-25.076423828125" />
                  <Point X="3.363902587891" Y="-25.13923046875" />
                  <Point X="3.370376464844" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.18398828125" />
                  <Point X="3.380005126953" Y="-25.20548828125" />
                  <Point X="3.384068359375" Y="-25.216033203125" />
                  <Point X="3.393841552734" Y="-25.23749609375" />
                  <Point X="3.399128662109" Y="-25.247484375" />
                  <Point X="3.410853027344" Y="-25.266765625" />
                  <Point X="3.417290283203" Y="-25.27605859375" />
                  <Point X="3.453375" Y="-25.3220390625" />
                  <Point X="3.472796142578" Y="-25.346787109375" />
                  <Point X="3.478720214844" Y="-25.35363671875" />
                  <Point X="3.501135986328" Y="-25.37580078125" />
                  <Point X="3.530173828125" Y="-25.39872265625" />
                  <Point X="3.541495605469" Y="-25.40640625" />
                  <Point X="3.601636962891" Y="-25.44116796875" />
                  <Point X="3.634005615234" Y="-25.45987890625" />
                  <Point X="3.639491943359" Y="-25.462814453125" />
                  <Point X="3.659139648438" Y="-25.472009765625" />
                  <Point X="3.683021240234" Y="-25.48102734375" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.146876464844" Y="-25.60580078125" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.780071777344" Y="-25.808623046875" />
                  <Point X="4.761612304688" Y="-25.931060546875" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="4.416951660156" Y="-26.038294921875" />
                  <Point X="3.436782226562" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400096923828" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354481201172" Y="-25.912677734375" />
                  <Point X="3.236445068359" Y="-25.938333984375" />
                  <Point X="3.172916992188" Y="-25.952140625" />
                  <Point X="3.157877197266" Y="-25.9567421875" />
                  <Point X="3.128758056641" Y="-25.968365234375" />
                  <Point X="3.114678710938" Y="-25.97538671875" />
                  <Point X="3.086851074219" Y="-25.992279296875" />
                  <Point X="3.074123046875" Y="-26.00153125" />
                  <Point X="3.050372070312" Y="-26.02200390625" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.968003662109" Y="-26.11903125" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606201172" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.864931640625" Y="-26.407783203125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607177734" Y="-26.51458984375" />
                  <Point X="2.864065429688" Y="-26.530126953125" />
                  <Point X="2.871796875" Y="-26.561748046875" />
                  <Point X="2.876786132812" Y="-26.57666796875" />
                  <Point X="2.889158203125" Y="-26.60548046875" />
                  <Point X="2.896541015625" Y="-26.619373046875" />
                  <Point X="2.961864013672" Y="-26.7209765625" />
                  <Point X="2.997021240234" Y="-26.775662109375" />
                  <Point X="3.001742675781" Y="-26.782353515625" />
                  <Point X="3.019788818359" Y="-26.804443359375" />
                  <Point X="3.043485595703" Y="-26.8281171875" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.474930419922" Y="-27.1601953125" />
                  <Point X="4.087170410156" Y="-27.629984375" />
                  <Point X="4.045499023438" Y="-27.697416015625" />
                  <Point X="4.001273925781" Y="-27.760251953125" />
                  <Point X="3.721427734375" Y="-27.59868359375" />
                  <Point X="2.848454345703" Y="-27.094671875" />
                  <Point X="2.841198730469" Y="-27.090890625" />
                  <Point X="2.815020996094" Y="-27.07951171875" />
                  <Point X="2.783116943359" Y="-27.069326171875" />
                  <Point X="2.771108398438" Y="-27.066337890625" />
                  <Point X="2.630626708984" Y="-27.040966796875" />
                  <Point X="2.555018066406" Y="-27.0273125" />
                  <Point X="2.539359130859" Y="-27.02580859375" />
                  <Point X="2.508008789062" Y="-27.025404296875" />
                  <Point X="2.492317382813" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444846191406" Y="-27.03513671875" />
                  <Point X="2.415068847656" Y="-27.0449609375" />
                  <Point X="2.40058984375" Y="-27.051109375" />
                  <Point X="2.283884033203" Y="-27.112529296875" />
                  <Point X="2.221071777344" Y="-27.145587890625" />
                  <Point X="2.208970214844" Y="-27.153169921875" />
                  <Point X="2.1860390625" Y="-27.1700625" />
                  <Point X="2.175209472656" Y="-27.179373046875" />
                  <Point X="2.154250244141" Y="-27.20033203125" />
                  <Point X="2.144938476562" Y="-27.211162109375" />
                  <Point X="2.128045410156" Y="-27.23409375" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.059042724609" Y="-27.362900390625" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.02755871582" Y="-27.720623046875" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.340810791016" Y="-28.343421875" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723753662109" Y="-29.036083984375" />
                  <Point X="2.50007421875" Y="-28.744580078125" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657714844" Y="-27.87015234375" />
                  <Point X="1.808834594727" Y="-27.84962890625" />
                  <Point X="1.783251831055" Y="-27.82800390625" />
                  <Point X="1.773298339844" Y="-27.820646484375" />
                  <Point X="1.634745483398" Y="-27.7315703125" />
                  <Point X="1.560174804688" Y="-27.68362890625" />
                  <Point X="1.546279663086" Y="-27.676244140625" />
                  <Point X="1.517465942383" Y="-27.663873046875" />
                  <Point X="1.502547485352" Y="-27.65888671875" />
                  <Point X="1.470927368164" Y="-27.65115625" />
                  <Point X="1.455391235352" Y="-27.648697265625" />
                  <Point X="1.424125244141" Y="-27.64637890625" />
                  <Point X="1.408395629883" Y="-27.64651953125" />
                  <Point X="1.256864501953" Y="-27.660462890625" />
                  <Point X="1.17530871582" Y="-27.667966796875" />
                  <Point X="1.161225463867" Y="-27.67033984375" />
                  <Point X="1.133575317383" Y="-27.677171875" />
                  <Point X="1.120008422852" Y="-27.681630859375" />
                  <Point X="1.092622070312" Y="-27.692974609375" />
                  <Point X="1.079880126953" Y="-27.699412109375" />
                  <Point X="1.055498413086" Y="-27.714130859375" />
                  <Point X="1.043858032227" Y="-27.722412109375" />
                  <Point X="0.92684942627" Y="-27.819701171875" />
                  <Point X="0.863874328613" Y="-27.872064453125" />
                  <Point X="0.852650512695" Y="-27.883091796875" />
                  <Point X="0.832179321289" Y="-27.90684375" />
                  <Point X="0.822932312012" Y="-27.919568359375" />
                  <Point X="0.806040405273" Y="-27.94739453125" />
                  <Point X="0.799018859863" Y="-27.961470703125" />
                  <Point X="0.78739465332" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.747807006836" Y="-28.16658984375" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.802429016113" Y="-28.9194375" />
                  <Point X="0.833091064453" Y="-29.152337890625" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642143676758" Y="-28.453576171875" />
                  <Point X="0.626786621094" Y="-28.423814453125" />
                  <Point X="0.620407409668" Y="-28.413208984375" />
                  <Point X="0.51396697998" Y="-28.259849609375" />
                  <Point X="0.4566796875" Y="-28.17730859375" />
                  <Point X="0.446668884277" Y="-28.165171875" />
                  <Point X="0.424786102295" Y="-28.14271875" />
                  <Point X="0.412913665771" Y="-28.13240234375" />
                  <Point X="0.386658538818" Y="-28.113158203125" />
                  <Point X="0.373244720459" Y="-28.104939453125" />
                  <Point X="0.345242401123" Y="-28.090830078125" />
                  <Point X="0.330654022217" Y="-28.084939453125" />
                  <Point X="0.16594342041" Y="-28.0338203125" />
                  <Point X="0.077294914246" Y="-28.006306640625" />
                  <Point X="0.063380393982" Y="-28.003111328125" />
                  <Point X="0.035227645874" Y="-27.998841796875" />
                  <Point X="0.020989425659" Y="-27.997767578125" />
                  <Point X="-0.008651480675" Y="-27.997765625" />
                  <Point X="-0.022895498276" Y="-27.998837890625" />
                  <Point X="-0.051061767578" Y="-28.003107421875" />
                  <Point X="-0.06498387146" Y="-28.0063046875" />
                  <Point X="-0.22969430542" Y="-28.05742578125" />
                  <Point X="-0.318343109131" Y="-28.0849375" />
                  <Point X="-0.332928222656" Y="-28.090828125" />
                  <Point X="-0.360928466797" Y="-28.104935546875" />
                  <Point X="-0.37434362793" Y="-28.11315234375" />
                  <Point X="-0.400599487305" Y="-28.132396484375" />
                  <Point X="-0.4124765625" Y="-28.142716796875" />
                  <Point X="-0.434360839844" Y="-28.165173828125" />
                  <Point X="-0.44436819458" Y="-28.177310546875" />
                  <Point X="-0.550808532715" Y="-28.330671875" />
                  <Point X="-0.60809576416" Y="-28.4132109375" />
                  <Point X="-0.612469482422" Y="-28.420130859375" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.782913757324" Y="-29.0110234375" />
                  <Point X="-0.985425476074" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.932770503312" Y="-28.748250839947" />
                  <Point X="-2.471791034333" Y="-29.121544653558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.968425836883" Y="-27.787351528169" />
                  <Point X="-3.729378605077" Y="-27.980928159665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884678457797" Y="-28.66495285177" />
                  <Point X="-2.413939113941" Y="-29.046150056221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.12431230426" Y="-27.538874997136" />
                  <Point X="-3.641252984933" Y="-27.930048721013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.836586412281" Y="-28.581654863592" />
                  <Point X="-2.343085690764" Y="-28.981283868247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.135065452132" Y="-27.407925110923" />
                  <Point X="-3.553127364788" Y="-27.879169282362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.788494372655" Y="-28.498356870646" />
                  <Point X="-2.260375824493" Y="-28.926018838581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.057555271229" Y="-27.348449459069" />
                  <Point X="-3.465001744644" Y="-27.82828984371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.740402334803" Y="-28.415058876262" />
                  <Point X="-2.177665702669" Y="-28.870754015858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.138712470203" Y="-29.712081754746" />
                  <Point X="-1.102696411159" Y="-29.741246984298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.980045090325" Y="-27.288973807215" />
                  <Point X="-3.3768761245" Y="-27.777410405058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.692310296951" Y="-28.331760881878" />
                  <Point X="-2.094915951703" Y="-28.815521284181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.124169239134" Y="-29.601616472296" />
                  <Point X="-0.973791910945" Y="-29.723389631618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.902534909421" Y="-27.229498155361" />
                  <Point X="-3.288750504356" Y="-27.726530966406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.644218259099" Y="-28.248462887495" />
                  <Point X="-1.98927428659" Y="-28.77882605907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.127517304883" Y="-29.476663103351" />
                  <Point X="-0.946877178887" Y="-29.622942593137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.825024705839" Y="-27.170022521872" />
                  <Point X="-3.200624884212" Y="-27.675651527755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.596126221247" Y="-28.165164893111" />
                  <Point X="-1.832759249368" Y="-28.783327278408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.156501599557" Y="-29.330949925551" />
                  <Point X="-0.919962446829" Y="-29.522495554655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.747514499778" Y="-27.11054689039" />
                  <Point X="-3.112499229126" Y="-27.624772117398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.548034183396" Y="-28.081866898727" />
                  <Point X="-1.668508663062" Y="-28.794092621881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.18640505293" Y="-29.184492427712" />
                  <Point X="-0.893047714771" Y="-29.422048516174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.67317382019" Y="-26.238720593782" />
                  <Point X="-4.620503172447" Y="-26.281372443342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.670004293717" Y="-27.051071258908" />
                  <Point X="-3.024373487451" Y="-27.573892777161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.499942145544" Y="-27.998568904344" />
                  <Point X="-0.866132982712" Y="-29.321601477692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.712541617934" Y="-26.084599020987" />
                  <Point X="-4.490656586603" Y="-26.264277976564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.592494087655" Y="-26.991595627426" />
                  <Point X="-2.936247745775" Y="-27.523013436923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.451850107692" Y="-27.91527090996" />
                  <Point X="-0.839218250654" Y="-29.221154439211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.746361301765" Y="-25.934970222253" />
                  <Point X="-4.360810000759" Y="-26.247183509785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514983881594" Y="-26.932119995944" />
                  <Point X="-2.848122004099" Y="-27.472134096686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.40375806984" Y="-27.831972915576" />
                  <Point X="-0.812303518596" Y="-29.12070740073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76613475003" Y="-25.796715840807" />
                  <Point X="-4.230963414915" Y="-26.230089043006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.437473675533" Y="-26.872644364462" />
                  <Point X="-2.759996262423" Y="-27.421254756448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.355666031989" Y="-27.748674921193" />
                  <Point X="-0.785388786538" Y="-29.020260362248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.785908198296" Y="-25.65846145936" />
                  <Point X="-4.101116829072" Y="-26.212994576228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.359963469472" Y="-26.81316873298" />
                  <Point X="-2.670504315138" Y="-27.3714817477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.317865888284" Y="-27.657042715257" />
                  <Point X="-0.758474106548" Y="-28.919813281603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676147703" Y="-25.625101597166" />
                  <Point X="-3.971270243228" Y="-26.195900109449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.282453263411" Y="-26.753693101498" />
                  <Point X="-2.548002159313" Y="-27.348439878759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.322799501051" Y="-27.530805395652" />
                  <Point X="-0.73155943183" Y="-28.819366196688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.562723045149" Y="-25.594708915305" />
                  <Point X="-3.841423657384" Y="-26.17880564267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.20494305735" Y="-26.694217470016" />
                  <Point X="-0.704644757113" Y="-28.718919111772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.449298252127" Y="-25.564316342902" />
                  <Point X="-3.711577095533" Y="-26.161711156463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.127432851289" Y="-26.634741838534" />
                  <Point X="-0.677730082396" Y="-28.618472026857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.335872944495" Y="-25.533924187224" />
                  <Point X="-3.581730540144" Y="-26.144616665022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.049922645228" Y="-26.575266207052" />
                  <Point X="-0.650815407679" Y="-28.518024941942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.222447636862" Y="-25.503532031545" />
                  <Point X="-3.451883984756" Y="-26.127522173581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.98858327776" Y="-26.502695688674" />
                  <Point X="-0.61497978477" Y="-28.424801898434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.109022329229" Y="-25.473139875866" />
                  <Point X="-3.322037429368" Y="-26.110427682139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953331061303" Y="-26.409000211936" />
                  <Point X="-0.561407021833" Y="-28.345942107714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.995597021596" Y="-25.442747720188" />
                  <Point X="-3.180800018193" Y="-26.102557323839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.95868524704" Y="-26.282422319056" />
                  <Point X="-0.507091622186" Y="-28.267683692345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.882171713964" Y="-25.412355564509" />
                  <Point X="-0.452776346242" Y="-28.189425176803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.768746406331" Y="-25.381963408831" />
                  <Point X="-0.385715825935" Y="-28.121487556645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818075326938" Y="-29.096298411543" />
                  <Point X="0.826624754893" Y="-29.103221601795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.655321098698" Y="-25.351571253152" />
                  <Point X="-0.290547427523" Y="-28.076311247385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.776244031769" Y="-28.940181937868" />
                  <Point X="0.808610714386" Y="-28.966391960659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.773024366655" Y="-24.324230834151" />
                  <Point X="-4.611957763108" Y="-24.454659997984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.541895791065" Y="-25.321179097473" />
                  <Point X="-0.181416274999" Y="-28.042441753463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.7344127366" Y="-28.784065464193" />
                  <Point X="0.790596770708" Y="-28.829562397934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.756871214255" Y="-24.21506924029" />
                  <Point X="-4.386349520535" Y="-24.515111791817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.440044510138" Y="-25.281414479769" />
                  <Point X="-0.072286449499" Y="-28.008571184938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692581441432" Y="-28.627948990517" />
                  <Point X="0.772582877618" Y="-28.692732876173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.740718041947" Y="-24.105907662551" />
                  <Point X="-4.160741277962" Y="-24.57556358565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.365375730981" Y="-25.219637906149" />
                  <Point X="0.075310953445" Y="-28.005851046424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.648676865827" Y="-28.470153607449" />
                  <Point X="0.754568984527" Y="-28.555903354413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.719952193199" Y="-24.000481356543" />
                  <Point X="-3.935133612167" Y="-24.636014912417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.315637158531" Y="-25.137673249192" />
                  <Point X="0.319757178993" Y="-28.081557538087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.480650529911" Y="-28.211846404707" />
                  <Point X="0.736555091436" Y="-28.419073832653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692787796657" Y="-23.900236492374" />
                  <Point X="-3.709526779164" Y="-24.696465564803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.295648079487" Y="-25.031617927481" />
                  <Point X="0.724950506945" Y="-28.28743446666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665623400115" Y="-23.799991628206" />
                  <Point X="-3.477667228073" Y="-24.76197956846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.345641316241" Y="-24.86889204383" />
                  <Point X="0.744644411922" Y="-28.181140117702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.638459003573" Y="-23.699746764037" />
                  <Point X="0.767237766748" Y="-28.077193696937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.485660728978" Y="-23.701238208343" />
                  <Point X="0.793221678011" Y="-27.975992894637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.305397637586" Y="-23.724970222768" />
                  <Point X="0.843115985803" Y="-27.894154349675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.125134523608" Y="-23.748702255481" />
                  <Point X="0.914653413262" Y="-27.829842057447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.94487140963" Y="-23.772434288194" />
                  <Point X="0.989134227251" Y="-27.767913272635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.765768796825" Y="-23.795226565587" />
                  <Point X="1.065911899657" Y="-27.707844447095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640346859032" Y="-23.774549089464" />
                  <Point X="1.168947293458" Y="-27.66903870509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.504905542629" Y="-28.750876364283" />
                  <Point X="2.638526922276" Y="-28.859080824014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.544212328202" Y="-23.73015513881" />
                  <Point X="1.303961828355" Y="-27.656129160938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.257171634412" Y="-28.428023242168" />
                  <Point X="2.505981337708" Y="-28.629505367201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.480129911581" Y="-23.659805897838" />
                  <Point X="1.444767953354" Y="-27.647909553979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.009437763081" Y="-28.105170149922" />
                  <Point X="2.37343575314" Y="-28.399929910388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.43940111424" Y="-23.570545268856" />
                  <Point X="2.240889753146" Y="-28.170354117169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421653686765" Y="-23.462674693496" />
                  <Point X="2.108343617512" Y="-27.940778214111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.220503717359" Y="-22.693536535044" />
                  <Point X="-3.80507825423" Y="-23.029941442068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.497930986132" Y="-23.278664395614" />
                  <Point X="2.034432372706" Y="-27.758683909434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.172053477822" Y="-22.610528606666" />
                  <Point X="2.008574082869" Y="-27.615502120438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.123603238284" Y="-22.527520678288" />
                  <Point X="2.00548973479" Y="-27.490762305851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.075152529138" Y="-22.444513130192" />
                  <Point X="2.040965791784" Y="-27.397248091605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026701691604" Y="-22.361505686062" />
                  <Point X="2.086075908682" Y="-27.311535385246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.97334034614" Y="-22.282474692848" />
                  <Point X="2.133035294759" Y="-27.227320187539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.914994920657" Y="-22.207479728055" />
                  <Point X="2.200305853997" Y="-27.159552653554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.856649495174" Y="-22.132484763261" />
                  <Point X="2.289526373266" Y="-27.109559846732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.798304069691" Y="-22.057489798467" />
                  <Point X="2.381020616961" Y="-27.061408265646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739958668171" Y="-21.982494814268" />
                  <Point X="2.489424278583" Y="-27.026949661207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.323196637283" Y="-22.197739893763" />
                  <Point X="2.665458879917" Y="-27.047257511898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.075878408631" Y="-22.275772087684" />
                  <Point X="2.940823658416" Y="-27.148001354071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950165460835" Y="-22.255330266815" />
                  <Point X="3.466746984125" Y="-27.451643507154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.869978079962" Y="-22.19802256875" />
                  <Point X="3.99266609803" Y="-27.755282249587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803877453632" Y="-22.129307641776" />
                  <Point X="4.05439235961" Y="-27.683025031884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.758213880096" Y="-22.044043115765" />
                  <Point X="2.901637855733" Y="-26.62730068169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751262065732" Y="-21.927430425279" />
                  <Point X="2.859399258812" Y="-26.470854381559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.78334694747" Y="-21.77920644158" />
                  <Point X="2.8696153113" Y="-26.356885018986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.914803018014" Y="-21.550513255827" />
                  <Point X="2.88652818867" Y="-26.248338638276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.047350140071" Y="-21.32093655398" />
                  <Point X="2.93170861841" Y="-26.162682870133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988887853469" Y="-21.246036221455" />
                  <Point X="2.992450851241" Y="-26.08962880166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.911346717693" Y="-21.186585636362" />
                  <Point X="3.054986134085" Y="-26.018026716458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.833805584634" Y="-21.127135049069" />
                  <Point X="3.139380058818" Y="-25.964125410446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.756264476109" Y="-21.067684441909" />
                  <Point X="3.253822426884" Y="-25.934556854067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.678723367584" Y="-21.008233834749" />
                  <Point X="3.374670070055" Y="-25.910175187197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.593885970844" Y="-20.954691645287" />
                  <Point X="3.541516135222" Y="-25.923042308011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.504354916977" Y="-20.904950304424" />
                  <Point X="3.72177913222" Y="-25.946774245995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.41482386311" Y="-20.855208963561" />
                  <Point X="3.902042129217" Y="-25.970506183979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.325292809243" Y="-20.805467622697" />
                  <Point X="4.082305126214" Y="-25.994238121963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.235761719258" Y="-20.755726311082" />
                  <Point X="4.262568123211" Y="-26.017970059948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.819771957205" Y="-20.970346019605" />
                  <Point X="3.370444304748" Y="-25.173300277363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.784697777828" Y="-25.50875612556" />
                  <Point X="4.442831096424" Y="-26.041701978672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.707215349912" Y="-20.939250404261" />
                  <Point X="3.34892473801" Y="-25.033631917058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.010304608385" Y="-25.569206775964" />
                  <Point X="4.623093927758" Y="-26.065433782505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.610504249271" Y="-20.895323350633" />
                  <Point X="3.363871292699" Y="-24.923493239637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.235911709628" Y="-25.629657645567" />
                  <Point X="4.73767077379" Y="-26.035974124236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.539791153231" Y="-20.830343527985" />
                  <Point X="3.393690538988" Y="-24.825398230404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.461519226082" Y="-25.6901088514" />
                  <Point X="4.761215743812" Y="-25.932798306262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.498278977608" Y="-20.741717266228" />
                  <Point X="3.448112563758" Y="-24.747226158356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.687126742536" Y="-25.750560057233" />
                  <Point X="4.777760419243" Y="-25.823953761501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468676044239" Y="-20.643447090246" />
                  <Point X="3.512611774445" Y="-24.677214430565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468171947302" Y="-20.521613141137" />
                  <Point X="3.597550167932" Y="-24.623754026656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.436190720113" Y="-20.425268869516" />
                  <Point X="3.692601536325" Y="-24.578482948354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.324057448202" Y="-20.39383044394" />
                  <Point X="3.806026814567" Y="-24.548090768875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.211924176292" Y="-20.362392018363" />
                  <Point X="3.060804271706" Y="-23.82237929373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313935055983" Y="-24.027360561148" />
                  <Point X="3.919452092809" Y="-24.517698589396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.099789869112" Y="-20.33095443113" />
                  <Point X="3.006501578779" Y="-23.656163681278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.490373447274" Y="-24.047995394498" />
                  <Point X="4.032877371051" Y="-24.487306409918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.987655171788" Y="-20.299517159831" />
                  <Point X="2.033258392045" Y="-22.745804729485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371550434466" Y="-23.019748223994" />
                  <Point X="3.008369418425" Y="-23.53543406924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.620809182142" Y="-24.031378011192" />
                  <Point X="4.146302649293" Y="-24.456914230439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.865715720995" Y="-20.27601962134" />
                  <Point X="-0.15810179744" Y="-20.849034078301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.124452031612" Y="-20.876283121389" />
                  <Point X="2.012987307274" Y="-22.607147369942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.514372083284" Y="-23.013160756042" />
                  <Point X="3.037985905182" Y="-23.437174868575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.75065589854" Y="-24.014283650134" />
                  <Point X="4.259727927535" Y="-24.42652205096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.733822134761" Y="-20.260582782793" />
                  <Point X="-0.248317998287" Y="-20.65373628056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.064222597973" Y="-20.906826665137" />
                  <Point X="2.030656130419" Y="-22.499213142051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.622718036873" Y="-22.97865542056" />
                  <Point X="3.087091892972" Y="-23.354697954662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.880502614938" Y="-23.997189289076" />
                  <Point X="4.373153205777" Y="-24.396129871481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.601928548527" Y="-20.245145944246" />
                  <Point X="-0.290149276311" Y="-20.497619820768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.158021033895" Y="-20.860540982125" />
                  <Point X="2.063954647812" Y="-22.403935591004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.711678152742" Y="-22.928451743222" />
                  <Point X="3.146614331489" Y="-23.28065611623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.010349263838" Y="-23.980094873359" />
                  <Point X="4.486578484019" Y="-24.365737692002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.470034962294" Y="-20.229709105699" />
                  <Point X="-0.331980554335" Y="-20.341503360976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.217144108429" Y="-20.786175745116" />
                  <Point X="2.111972743141" Y="-22.320577719146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.799803796433" Y="-22.877572323639" />
                  <Point X="3.221418958824" Y="-23.218989550295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.140195341156" Y="-23.962999994785" />
                  <Point X="4.600003221105" Y="-24.335345074304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.250631422768" Y="-20.691051078822" />
                  <Point X="2.160064858118" Y="-22.237279787218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887929440125" Y="-22.826692904055" />
                  <Point X="3.298929079533" Y="-23.159513849697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.270041418475" Y="-23.945905116211" />
                  <Point X="4.713427522466" Y="-24.304952103763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.277546058128" Y="-20.590603962037" />
                  <Point X="2.208156973096" Y="-22.153981855289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.976055083817" Y="-22.775813484472" />
                  <Point X="3.376439200242" Y="-23.100038149098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.399887495794" Y="-23.928810237637" />
                  <Point X="4.775618474508" Y="-24.233071184976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.304460693489" Y="-20.490156845251" />
                  <Point X="2.256249088073" Y="-22.070683923361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.064180727509" Y="-22.724934064889" />
                  <Point X="3.453949320951" Y="-23.040562448499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.529733573113" Y="-23.911715359062" />
                  <Point X="4.753840166643" Y="-24.093193300237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331375350691" Y="-20.389709746153" />
                  <Point X="2.30434120305" Y="-21.987385991432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.1523063712" Y="-22.674054645306" />
                  <Point X="3.53145944166" Y="-22.981086747901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.659579650431" Y="-23.894620480488" />
                  <Point X="4.717584829192" Y="-23.941592148091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.358290008853" Y="-20.289262647832" />
                  <Point X="2.352433318028" Y="-21.904088059504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.240432206139" Y="-22.623175380591" />
                  <Point X="3.608969562369" Y="-22.921611047302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.423373827887" Y="-20.219724326545" />
                  <Point X="2.400525433005" Y="-21.820790127575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.328558054809" Y="-22.572296126996" />
                  <Point X="3.686479683078" Y="-22.862135346704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.596754256651" Y="-20.237882870666" />
                  <Point X="2.448617547983" Y="-21.737492195647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.416683903478" Y="-22.5214168734" />
                  <Point X="3.763989803788" Y="-22.802659646105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.770132868823" Y="-20.256039943741" />
                  <Point X="2.49670966296" Y="-21.654194263718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.504809752148" Y="-22.470537619804" />
                  <Point X="3.841499924497" Y="-22.743183945506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.971328196529" Y="-20.296722548911" />
                  <Point X="2.544801777938" Y="-21.570896331789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592935600817" Y="-22.419658366209" />
                  <Point X="3.919010057529" Y="-22.683708254887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.186410163271" Y="-20.348650332647" />
                  <Point X="2.592893892915" Y="-21.487598399861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.681061449486" Y="-22.368779112613" />
                  <Point X="3.996520289713" Y="-22.624232644559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.401491392892" Y="-20.400577519473" />
                  <Point X="2.640986020242" Y="-21.404300477933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.769187298156" Y="-22.317899859018" />
                  <Point X="4.074030521898" Y="-22.564757034231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.660777653602" Y="-20.488301234664" />
                  <Point X="2.689078173468" Y="-21.321002576978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857313146825" Y="-22.267020605422" />
                  <Point X="4.109457040899" Y="-22.471202704911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.957508022765" Y="-20.606346591016" />
                  <Point X="2.737170326694" Y="-21.237704676022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347394329643" Y="-20.799828138328" />
                  <Point X="2.78526247992" Y="-21.154406775067" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.731959533691" Y="-29.509013671875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318969727" Y="-28.521544921875" />
                  <Point X="0.357878479004" Y="-28.368185546875" />
                  <Point X="0.300591186523" Y="-28.28564453125" />
                  <Point X="0.27433605957" Y="-28.266400390625" />
                  <Point X="0.109625595093" Y="-28.21528125" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="-0.008663995743" Y="-28.187765625" />
                  <Point X="-0.173374313354" Y="-28.23888671875" />
                  <Point X="-0.262023132324" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.394719329834" Y="-28.43900390625" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.599387878418" Y="-29.06019921875" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-0.996843261719" Y="-29.95813671875" />
                  <Point X="-1.100258056641" Y="-29.9380625" />
                  <Point X="-1.291021240234" Y="-29.888982421875" />
                  <Point X="-1.35158972168" Y="-29.8733984375" />
                  <Point X="-1.340498779297" Y="-29.789154296875" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.349032714844" Y="-29.3369375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.537927001953" Y="-29.069640625" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.850506713867" Y="-28.972572265625" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.157583984375" Y="-29.08584765625" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.338423583984" Y="-29.259845703125" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.704269775391" Y="-29.261455078125" />
                  <Point X="-2.855836914062" Y="-29.167609375" />
                  <Point X="-3.120001953125" Y="-28.9642109375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.989995605469" Y="-28.4673671875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.59759375" />
                  <Point X="-2.513979736328" Y="-27.568763671875" />
                  <Point X="-2.531328369141" Y="-27.551416015625" />
                  <Point X="-2.560157226562" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.042836181641" Y="-27.803943359375" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.041959228516" Y="-28.00444921875" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.351091308594" Y="-27.5295546875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-4.010563232422" Y="-27.072900390625" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140326660156" Y="-26.334595703125" />
                  <Point X="-3.161160644531" Y="-26.310638671875" />
                  <Point X="-3.187643310547" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.789066894531" Y="-26.363552734375" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.880560058594" Y="-26.194541015625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.977500488281" Y="-25.66084765625" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.522069824219" Y="-25.387111328125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.54189453125" Y="-25.121423828125" />
                  <Point X="-3.523852294922" Y="-25.10890234375" />
                  <Point X="-3.514141601562" Y="-25.102162109375" />
                  <Point X="-3.494899414062" Y="-25.07591015625" />
                  <Point X="-3.488885253906" Y="-25.05653125" />
                  <Point X="-3.4856484375" Y="-25.046103515625" />
                  <Point X="-3.485647705078" Y="-25.0164609375" />
                  <Point X="-3.491661865234" Y="-24.99708203125" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.514141601562" Y="-24.9603984375" />
                  <Point X="-3.532183837891" Y="-24.947876953125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.076624267578" Y="-24.794806640625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.947827636719" Y="-24.207552734375" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.816780761719" Y="-23.63136328125" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.444719238281" Y="-23.51498828125" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731703125" Y="-23.6041328125" />
                  <Point X="-3.691769775391" Y="-23.59154296875" />
                  <Point X="-3.670277099609" Y="-23.584765625" />
                  <Point X="-3.651533691406" Y="-23.573943359375" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.623096435547" Y="-23.51753125" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.61631640625" Y="-23.45448828125" />
                  <Point X="-3.635650390625" Y="-23.41734765625" />
                  <Point X="-3.646056152344" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.957760009766" Y="-23.152275390625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.277293945313" Y="-22.413921875" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.892827880859" Y="-21.869564453125" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.583828125" Y="-21.827873046875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138515380859" Y="-22.07956640625" />
                  <Point X="-3.082899169922" Y="-22.084431640625" />
                  <Point X="-3.052966064453" Y="-22.08705078125" />
                  <Point X="-3.031507324219" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.973776123047" Y="-22.033119140625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.942940185547" Y="-21.91654296875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.084028076172" Y="-21.63740625" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-2.957138427734" Y="-20.982275390625" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.332065917969" Y="-20.591875" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.097831542969" Y="-20.543001953125" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247436523" Y="-20.726337890625" />
                  <Point X="-1.889346923828" Y="-20.7585625" />
                  <Point X="-1.85603137207" Y="-20.77590625" />
                  <Point X="-1.835124755859" Y="-20.781509765625" />
                  <Point X="-1.813809204102" Y="-20.77775" />
                  <Point X="-1.749335693359" Y="-20.75104296875" />
                  <Point X="-1.714635375977" Y="-20.736669921875" />
                  <Point X="-1.696905517578" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.665098388672" Y="-20.638955078125" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.666919921875" Y="-20.467619140625" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.232524536133" Y="-20.170841796875" />
                  <Point X="-0.968083435059" Y="-20.096703125" />
                  <Point X="-0.457944061279" Y="-20.036998046875" />
                  <Point X="-0.224200134277" Y="-20.009640625" />
                  <Point X="-0.167769882202" Y="-20.2202421875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282114029" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594032288" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.122067337036" Y="-20.43675390625" />
                  <Point X="0.236648376465" Y="-20.0091328125" />
                  <Point X="0.62931817627" Y="-20.050255859375" />
                  <Point X="0.860210205078" Y="-20.074435546875" />
                  <Point X="1.282266113281" Y="-20.176333984375" />
                  <Point X="1.508455078125" Y="-20.230943359375" />
                  <Point X="1.783630981445" Y="-20.33075" />
                  <Point X="1.931045410156" Y="-20.38421875" />
                  <Point X="2.196675048828" Y="-20.5084453125" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.595318847656" Y="-20.724373046875" />
                  <Point X="2.732533691406" Y="-20.804314453125" />
                  <Point X="2.974550048828" Y="-20.976423828125" />
                  <Point X="3.068740722656" Y="-21.043408203125" />
                  <Point X="2.790003173828" Y="-21.5261953125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.21089453125" Y="-22.560681640625" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.207487060547" Y="-22.65280859375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682617188" Y="-22.699189453125" />
                  <Point X="2.246609619141" Y="-22.74034765625" />
                  <Point X="2.261640380859" Y="-22.762498046875" />
                  <Point X="2.274939208984" Y="-22.775794921875" />
                  <Point X="2.316096435547" Y="-22.80372265625" />
                  <Point X="2.338247802734" Y="-22.81875390625" />
                  <Point X="2.360335693359" Y="-22.827021484375" />
                  <Point X="2.405469238281" Y="-22.832462890625" />
                  <Point X="2.429760498047" Y="-22.835392578125" />
                  <Point X="2.448663818359" Y="-22.8340546875" />
                  <Point X="2.500858398438" Y="-22.820095703125" />
                  <Point X="2.528950195312" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.063209472656" Y="-22.506103515625" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.121206542969" Y="-22.145013671875" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.337518066406" Y="-22.481087890625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.026105712891" Y="-22.841021484375" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371582031" Y="-23.41616796875" />
                  <Point X="3.241806884766" Y="-23.465173828125" />
                  <Point X="3.221589355469" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.199126464844" Y="-23.55853515625" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.202266113281" Y="-23.664705078125" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.246888916016" Y="-23.75953125" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280947753906" Y="-23.8011796875" />
                  <Point X="3.326222412109" Y="-23.826666015625" />
                  <Point X="3.35058984375" Y="-23.8403828125" />
                  <Point X="3.368566650391" Y="-23.846380859375" />
                  <Point X="3.429781005859" Y="-23.854470703125" />
                  <Point X="3.462727294922" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.971835449219" Y="-23.793525390625" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.904233886719" Y="-23.905033203125" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.981707519531" Y="-24.32170703125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.586346679688" Y="-24.535708984375" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087646484" Y="-24.767181640625" />
                  <Point X="3.668946289062" Y="-24.801943359375" />
                  <Point X="3.636577636719" Y="-24.82065234375" />
                  <Point X="3.622264404297" Y="-24.83307421875" />
                  <Point X="3.5861796875" Y="-24.8790546875" />
                  <Point X="3.566758544922" Y="-24.903802734375" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.544956787109" Y="-24.988072265625" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.550511474609" Y="-25.1034921875" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758544922" Y="-25.1587578125" />
                  <Point X="3.602843261719" Y="-25.20473828125" />
                  <Point X="3.622264404297" Y="-25.229486328125" />
                  <Point X="3.636575683594" Y="-25.24190625" />
                  <Point X="3.696717041016" Y="-25.27666796875" />
                  <Point X="3.729085693359" Y="-25.29537890625" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.196051269531" Y="-25.4222734375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.967948730469" Y="-25.836947265625" />
                  <Point X="4.948430664062" Y="-25.966404296875" />
                  <Point X="4.893957519531" Y="-26.20511328125" />
                  <Point X="4.874545898438" Y="-26.290177734375" />
                  <Point X="4.392151855469" Y="-26.226669921875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394837158203" Y="-26.098341796875" />
                  <Point X="3.276801025391" Y="-26.123998046875" />
                  <Point X="3.213272949219" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.114099853516" Y="-26.24050390625" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.054132324219" Y="-26.425193359375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.121683349609" Y="-26.6182265625" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.590595458984" Y="-27.00945703125" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.259147949219" Y="-27.7131171875" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.091476074219" Y="-27.962208984375" />
                  <Point X="4.056687744141" Y="-28.011638671875" />
                  <Point X="3.626427978516" Y="-27.763228515625" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340332031" Y="-27.253314453125" />
                  <Point X="2.596858642578" Y="-27.227943359375" />
                  <Point X="2.52125" Y="-27.2142890625" />
                  <Point X="2.489077148438" Y="-27.21924609375" />
                  <Point X="2.372371337891" Y="-27.280666015625" />
                  <Point X="2.309559082031" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.227178466797" Y="-27.451388671875" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.214533935547" Y="-27.686857421875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.505355712891" Y="-28.248421875" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.900581298828" Y="-29.14358203125" />
                  <Point X="2.835294433594" Y="-29.19021484375" />
                  <Point X="2.709342041016" Y="-29.2717421875" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="2.349336914062" Y="-28.860244140625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.53199621582" Y="-27.891390625" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425805297852" Y="-27.83571875" />
                  <Point X="1.274274169922" Y="-27.849662109375" />
                  <Point X="1.192718383789" Y="-27.857166015625" />
                  <Point X="1.165332275391" Y="-27.868509765625" />
                  <Point X="1.048323730469" Y="-27.965798828125" />
                  <Point X="0.985348632813" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.933471801758" Y="-28.2069453125" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.990803527832" Y="-28.89463671875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.056107299805" Y="-29.9497109375" />
                  <Point X="0.994372619629" Y="-29.9632421875" />
                  <Point X="0.877987854004" Y="-29.984384765625" />
                  <Point X="0.860200378418" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#182" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.125629508639" Y="4.823980206672" Z="1.65" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.65" />
                  <Point X="-0.469912334385" Y="5.043943110425" Z="1.65" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.65" />
                  <Point X="-1.252178203445" Y="4.908583028187" Z="1.65" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.65" />
                  <Point X="-1.722648312199" Y="4.557135362634" Z="1.65" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.65" />
                  <Point X="-1.718940200075" Y="4.407359683241" Z="1.65" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.65" />
                  <Point X="-1.77462546924" Y="4.326430634745" Z="1.65" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.65" />
                  <Point X="-1.872414581505" Y="4.317067670483" Z="1.65" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.65" />
                  <Point X="-2.064319912689" Y="4.518716837131" Z="1.65" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.65" />
                  <Point X="-2.362504832927" Y="4.483112020102" Z="1.65" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.65" />
                  <Point X="-2.993716423558" Y="4.088685555161" Z="1.65" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.65" />
                  <Point X="-3.133485194254" Y="3.368875200345" Z="1.65" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.65" />
                  <Point X="-2.998905846443" Y="3.11037973065" Z="1.65" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.65" />
                  <Point X="-3.015286797011" Y="3.033516770875" Z="1.65" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.65" />
                  <Point X="-3.084696734957" Y="2.99665885273" Z="1.65" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.65" />
                  <Point X="-3.564984147952" Y="3.246708754492" Z="1.65" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.65" />
                  <Point X="-3.938447569163" Y="3.192419266796" Z="1.65" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.65" />
                  <Point X="-4.326631857437" Y="2.642563931115" Z="1.65" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.65" />
                  <Point X="-3.994353963097" Y="1.839337683475" Z="1.65" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.65" />
                  <Point X="-3.686156501953" Y="1.590844785858" Z="1.65" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.65" />
                  <Point X="-3.675446429225" Y="1.532884212133" Z="1.65" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.65" />
                  <Point X="-3.712962534561" Y="1.487423517907" Z="1.65" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.65" />
                  <Point X="-4.444349026869" Y="1.565864070969" Z="1.65" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.65" />
                  <Point X="-4.871196482541" Y="1.412996219563" Z="1.65" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.65" />
                  <Point X="-5.003445168995" Y="0.831045370337" Z="1.65" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.65" />
                  <Point X="-4.095721209114" Y="0.188177663805" Z="1.65" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.65" />
                  <Point X="-3.566850179384" Y="0.042329351451" Z="1.65" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.65" />
                  <Point X="-3.5455709517" Y="0.019377686936" Z="1.65" />
                  <Point X="-3.539556741714" Y="0" Z="1.65" />
                  <Point X="-3.542793649435" Y="-0.010429264125" Z="1.65" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.65" />
                  <Point X="-3.55851841574" Y="-0.036546631683" Z="1.65" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.65" />
                  <Point X="-4.541166579658" Y="-0.307534392951" Z="1.65" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.65" />
                  <Point X="-5.033152444448" Y="-0.636645021899" Z="1.65" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.65" />
                  <Point X="-4.935160716439" Y="-1.175635487009" Z="1.65" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.65" />
                  <Point X="-3.788696396723" Y="-1.381844329081" Z="1.65" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.65" />
                  <Point X="-3.209892799467" Y="-1.312316924358" Z="1.65" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.65" />
                  <Point X="-3.195372060448" Y="-1.332858219357" Z="1.65" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.65" />
                  <Point X="-4.047157140572" Y="-2.00195148008" Z="1.65" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.65" />
                  <Point X="-4.40019080394" Y="-2.523884372212" Z="1.65" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.65" />
                  <Point X="-4.08790118129" Y="-3.003452242808" Z="1.65" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.65" />
                  <Point X="-3.023992208709" Y="-2.815964105668" Z="1.65" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.65" />
                  <Point X="-2.566769358056" Y="-2.561561111075" Z="1.65" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.65" />
                  <Point X="-3.039452907501" Y="-3.411086173783" Z="1.65" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.65" />
                  <Point X="-3.156661884968" Y="-3.972547513625" Z="1.65" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.65" />
                  <Point X="-2.736747004072" Y="-4.272687029783" Z="1.65" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.65" />
                  <Point X="-2.304911785335" Y="-4.259002307355" Z="1.65" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.65" />
                  <Point X="-2.135961502823" Y="-4.096141769278" Z="1.65" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.65" />
                  <Point X="-1.859932707158" Y="-3.991184258974" Z="1.65" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.65" />
                  <Point X="-1.577050342366" Y="-4.075951787535" Z="1.65" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.65" />
                  <Point X="-1.404227808407" Y="-4.315410260848" Z="1.65" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.65" />
                  <Point X="-1.396226996457" Y="-4.751347331373" Z="1.65" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.65" />
                  <Point X="-1.309636461511" Y="-4.906123274894" Z="1.65" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.65" />
                  <Point X="-1.012526887399" Y="-4.975940169876" Z="1.65" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.65" />
                  <Point X="-0.557247649777" Y="-4.04186060328" Z="1.65" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.65" />
                  <Point X="-0.359799490546" Y="-3.436233016523" Z="1.65" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.65" />
                  <Point X="-0.164710335927" Y="-3.255359378332" Z="1.65" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.65" />
                  <Point X="0.088648743434" Y="-3.231752666957" Z="1.65" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.65" />
                  <Point X="0.310646368887" Y="-3.365412739245" Z="1.65" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.65" />
                  <Point X="0.677507210624" Y="-4.490675426536" Z="1.65" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.65" />
                  <Point X="0.880768635723" Y="-5.002299882919" Z="1.65" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.65" />
                  <Point X="1.060656110338" Y="-4.967269420509" Z="1.65" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.65" />
                  <Point X="1.034219943712" Y="-3.856830841699" Z="1.65" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.65" />
                  <Point X="0.976175044097" Y="-3.186284326887" Z="1.65" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.65" />
                  <Point X="1.074135189454" Y="-2.972964221958" Z="1.65" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.65" />
                  <Point X="1.27269931876" Y="-2.868170622319" Z="1.65" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.65" />
                  <Point X="1.498801003679" Y="-2.902168561089" Z="1.65" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.65" />
                  <Point X="2.303513134109" Y="-3.859400712666" Z="1.65" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.65" />
                  <Point X="2.730355334248" Y="-4.282435738058" Z="1.65" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.65" />
                  <Point X="2.923487119629" Y="-4.152989213659" Z="1.65" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.65" />
                  <Point X="2.54250097529" Y="-3.192141592205" Z="1.65" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.65" />
                  <Point X="2.257582021067" Y="-2.646689940221" Z="1.65" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.65" />
                  <Point X="2.265268748407" Y="-2.443395971216" Z="1.65" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.65" />
                  <Point X="2.389502407156" Y="-2.29363256156" Z="1.65" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.65" />
                  <Point X="2.581816863292" Y="-2.245865988424" Z="1.65" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.65" />
                  <Point X="3.595272004902" Y="-2.775248524985" Z="1.65" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.65" />
                  <Point X="4.126208959577" Y="-2.959706466834" Z="1.65" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.65" />
                  <Point X="4.295525499896" Y="-2.708121650852" Z="1.65" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.65" />
                  <Point X="3.614877568823" Y="-1.938508454239" Z="1.65" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.65" />
                  <Point X="3.157585395189" Y="-1.559907958202" Z="1.65" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.65" />
                  <Point X="3.097765967032" Y="-1.398495076051" Z="1.65" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.65" />
                  <Point X="3.146390215393" Y="-1.241190446003" Z="1.65" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.65" />
                  <Point X="3.281263742898" Y="-1.141576062817" Z="1.65" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.65" />
                  <Point X="4.379469615134" Y="-1.244962211043" Z="1.65" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.65" />
                  <Point X="4.936548841618" Y="-1.184956267733" Z="1.65" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.65" />
                  <Point X="5.011234229298" Y="-0.813121155177" Z="1.65" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.65" />
                  <Point X="4.202836337661" Y="-0.342696334733" Z="1.65" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.65" />
                  <Point X="3.715584039563" Y="-0.202100971147" Z="1.65" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.65" />
                  <Point X="3.636021518632" Y="-0.142591039697" Z="1.65" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.65" />
                  <Point X="3.593462991689" Y="-0.062807115867" Z="1.65" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.65" />
                  <Point X="3.587908458735" Y="0.033803415333" Z="1.65" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.65" />
                  <Point X="3.619357919768" Y="0.121357698878" Z="1.65" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.65" />
                  <Point X="3.68781137479" Y="0.186047873095" Z="1.65" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.65" />
                  <Point X="4.593131479646" Y="0.447275598036" Z="1.65" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.65" />
                  <Point X="5.02495634205" Y="0.717264047407" Z="1.65" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.65" />
                  <Point X="4.946659059787" Y="1.138074233444" Z="1.65" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.65" />
                  <Point X="3.959153121651" Y="1.287327916409" Z="1.65" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.65" />
                  <Point X="3.430175145997" Y="1.22637833383" Z="1.65" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.65" />
                  <Point X="3.344593299115" Y="1.248185269817" Z="1.65" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.65" />
                  <Point X="3.282503516313" Y="1.299229146157" Z="1.65" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.65" />
                  <Point X="3.245078737367" Y="1.376678780796" Z="1.65" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.65" />
                  <Point X="3.241123128566" Y="1.459278597149" Z="1.65" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.65" />
                  <Point X="3.275333603712" Y="1.535689178235" Z="1.65" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.65" />
                  <Point X="4.050387674845" Y="2.150590690147" Z="1.65" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.65" />
                  <Point X="4.374139344331" Y="2.576079715703" Z="1.65" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.65" />
                  <Point X="4.155636071235" Y="2.915470254719" Z="1.65" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.65" />
                  <Point X="3.032053401006" Y="2.568476905092" Z="1.65" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.65" />
                  <Point X="2.481786262754" Y="2.259486692696" Z="1.65" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.65" />
                  <Point X="2.405300332547" Y="2.248458258457" Z="1.65" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.65" />
                  <Point X="2.338015467848" Y="2.268931101696" Z="1.65" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.65" />
                  <Point X="2.281827498539" Y="2.319009392534" Z="1.65" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.65" />
                  <Point X="2.250971444302" Y="2.384458111286" Z="1.65" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.65" />
                  <Point X="2.253041249961" Y="2.457683313128" Z="1.65" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.65" />
                  <Point X="2.827148447767" Y="3.480085680949" Z="1.65" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.65" />
                  <Point X="2.997371403445" Y="4.095602595563" Z="1.65" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.65" />
                  <Point X="2.614332830181" Y="4.350109857857" Z="1.65" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.65" />
                  <Point X="2.211700472314" Y="4.568126497772" Z="1.65" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.65" />
                  <Point X="1.794523883314" Y="4.74753386843" Z="1.65" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.65" />
                  <Point X="1.287844504293" Y="4.903550857606" Z="1.65" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.65" />
                  <Point X="0.628369881515" Y="5.030753790179" Z="1.65" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.65" />
                  <Point X="0.067615052914" Y="4.607467299279" Z="1.65" />
                  <Point X="0" Y="4.355124473572" Z="1.65" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>