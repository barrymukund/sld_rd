<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#133" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="806" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.62658605957" Y="-28.748703125" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542363037109" Y="-28.467375" />
                  <Point X="0.516497192383" Y="-28.430107421875" />
                  <Point X="0.378635375977" Y="-28.231474609375" />
                  <Point X="0.356752349854" Y="-28.209021484375" />
                  <Point X="0.330497161865" Y="-28.18977734375" />
                  <Point X="0.30249395752" Y="-28.17566796875" />
                  <Point X="0.262468200684" Y="-28.16324609375" />
                  <Point X="0.049134822845" Y="-28.09703515625" />
                  <Point X="0.020976791382" Y="-28.092765625" />
                  <Point X="-0.008664384842" Y="-28.092765625" />
                  <Point X="-0.036826667786" Y="-28.09703515625" />
                  <Point X="-0.076852432251" Y="-28.109458984375" />
                  <Point X="-0.290185516357" Y="-28.17566796875" />
                  <Point X="-0.318183990479" Y="-28.189775390625" />
                  <Point X="-0.34443963623" Y="-28.20901953125" />
                  <Point X="-0.366323577881" Y="-28.231474609375" />
                  <Point X="-0.39218927002" Y="-28.2687421875" />
                  <Point X="-0.530051208496" Y="-28.467375" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.888287231445" Y="-29.7713359375" />
                  <Point X="-0.916584411621" Y="-29.87694140625" />
                  <Point X="-1.079368896484" Y="-29.84534375" />
                  <Point X="-1.121832763672" Y="-29.834419921875" />
                  <Point X="-1.24641809082" Y="-29.802365234375" />
                  <Point X="-1.222580810547" Y="-29.6213046875" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.226070800781" Y="-29.468154296875" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323645507812" Y="-29.131203125" />
                  <Point X="-1.36049609375" Y="-29.09888671875" />
                  <Point X="-1.55690637207" Y="-28.926638671875" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.643028564453" Y="-28.890966796875" />
                  <Point X="-1.69193762207" Y="-28.88776171875" />
                  <Point X="-1.952617797852" Y="-28.87067578125" />
                  <Point X="-1.983414672852" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.042657226562" Y="-28.89480078125" />
                  <Point X="-2.083410888672" Y="-28.92203125" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.4801484375" Y="-29.288490234375" />
                  <Point X="-2.801714111328" Y="-29.089384765625" />
                  <Point X="-2.860542236328" Y="-29.044087890625" />
                  <Point X="-3.104721923828" Y="-28.856078125" />
                  <Point X="-2.541367431641" Y="-27.8803203125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.411279052734" Y="-27.646970703125" />
                  <Point X="-2.405864013672" Y="-27.624439453125" />
                  <Point X="-2.406064208984" Y="-27.60126953125" />
                  <Point X="-2.410208984375" Y="-27.5697890625" />
                  <Point X="-2.416571289062" Y="-27.545970703125" />
                  <Point X="-2.42884765625" Y="-27.524591796875" />
                  <Point X="-2.441172363281" Y="-27.50842578125" />
                  <Point X="-2.449546875" Y="-27.49884765625" />
                  <Point X="-2.464154785156" Y="-27.484240234375" />
                  <Point X="-2.489311767578" Y="-27.466212890625" />
                  <Point X="-2.518140869141" Y="-27.45199609375" />
                  <Point X="-2.547759033203" Y="-27.44301171875" />
                  <Point X="-2.578693359375" Y="-27.444025390625" />
                  <Point X="-2.610219238281" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-3.724883544922" Y="-28.08802734375" />
                  <Point X="-3.818024169922" Y="-28.141802734375" />
                  <Point X="-4.082861816406" Y="-27.793857421875" />
                  <Point X="-4.125037597656" Y="-27.72313671875" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.311727050781" Y="-26.65641015625" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083064941406" Y="-26.4758828125" />
                  <Point X="-3.064365966797" Y="-26.446455078125" />
                  <Point X="-3.056435058594" Y="-26.426685546875" />
                  <Point X="-3.052639404297" Y="-26.415134765625" />
                  <Point X="-3.046152099609" Y="-26.390087890625" />
                  <Point X="-3.042037353516" Y="-26.35860546875" />
                  <Point X="-3.042736328125" Y="-26.33573828125" />
                  <Point X="-3.048882080078" Y="-26.313701171875" />
                  <Point X="-3.060885253906" Y="-26.284720703125" />
                  <Point X="-3.0731875" Y="-26.263369140625" />
                  <Point X="-3.090567138672" Y="-26.245900390625" />
                  <Point X="-3.107256591797" Y="-26.23300390625" />
                  <Point X="-3.117160644531" Y="-26.226302734375" />
                  <Point X="-3.139459472656" Y="-26.2131796875" />
                  <Point X="-3.168722167969" Y="-26.201955078125" />
                  <Point X="-3.200608154297" Y="-26.195474609375" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.602521484375" Y="-26.374826171875" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.834077636719" Y="-25.99265625" />
                  <Point X="-4.845236328125" Y="-25.914634765625" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.767281738281" Y="-25.28321875" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.514007568359" Y="-25.213125" />
                  <Point X="-3.494055175781" Y="-25.202861328125" />
                  <Point X="-3.483344970703" Y="-25.196427734375" />
                  <Point X="-3.4599765625" Y="-25.180208984375" />
                  <Point X="-3.436021240234" Y="-25.158681640625" />
                  <Point X="-3.415918945313" Y="-25.1301328125" />
                  <Point X="-3.407031982422" Y="-25.110685546875" />
                  <Point X="-3.40270703125" Y="-25.099359375" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.390733886719" Y="-25.042064453125" />
                  <Point X="-3.392195068359" Y="-25.00771484375" />
                  <Point X="-3.396378173828" Y="-24.98359375" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.417483886719" Y="-24.929169921875" />
                  <Point X="-3.438826660156" Y="-24.901251953125" />
                  <Point X="-3.455030029297" Y="-24.88669140625" />
                  <Point X="-3.464361083984" Y="-24.87930859375" />
                  <Point X="-3.487729492188" Y="-24.86308984375" />
                  <Point X="-3.501922851562" Y="-24.854953125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.782235839844" Y="-24.50738671875" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.824488769531" Y="-24.02303125" />
                  <Point X="-4.802023925781" Y="-23.94012890625" />
                  <Point X="-4.703550292969" Y="-23.576732421875" />
                  <Point X="-3.933694091797" Y="-23.6780859375" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744991699219" Y="-23.700658203125" />
                  <Point X="-3.723435058594" Y="-23.6987734375" />
                  <Point X="-3.703150390625" Y="-23.694740234375" />
                  <Point X="-3.693440673828" Y="-23.6916796875" />
                  <Point X="-3.64171875" Y="-23.67537109375" />
                  <Point X="-3.622782958984" Y="-23.667041015625" />
                  <Point X="-3.6040390625" Y="-23.656220703125" />
                  <Point X="-3.587354003906" Y="-23.64398828125" />
                  <Point X="-3.573712890625" Y="-23.628431640625" />
                  <Point X="-3.561298828125" Y="-23.610701171875" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.547457763672" Y="-23.583169921875" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532049804688" Y="-23.410623046875" />
                  <Point X="-3.536748046875" Y="-23.40159765625" />
                  <Point X="-3.561789550781" Y="-23.353494140625" />
                  <Point X="-3.57328125" Y="-23.336294921875" />
                  <Point X="-3.587193359375" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.318771972656" Y="-22.755515625" />
                  <Point X="-4.351860351563" Y="-22.730126953125" />
                  <Point X="-4.081156738281" Y="-22.26634765625" />
                  <Point X="-4.021650878906" Y="-22.189859375" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.309855224609" Y="-22.09574609375" />
                  <Point X="-3.206657226562" Y="-22.155328125" />
                  <Point X="-3.187729736328" Y="-22.163658203125" />
                  <Point X="-3.167087646484" Y="-22.17016796875" />
                  <Point X="-3.146789550781" Y="-22.174205078125" />
                  <Point X="-3.133274414062" Y="-22.17538671875" />
                  <Point X="-3.061240234375" Y="-22.181689453125" />
                  <Point X="-3.040561035156" Y="-22.18123828125" />
                  <Point X="-3.019102539062" Y="-22.178412109375" />
                  <Point X="-2.999013916016" Y="-22.17349609375" />
                  <Point X="-2.980465087891" Y="-22.16434765625" />
                  <Point X="-2.962210693359" Y="-22.15271875" />
                  <Point X="-2.946076171875" Y="-22.13976953125" />
                  <Point X="-2.936482910156" Y="-22.13017578125" />
                  <Point X="-2.885352539062" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435546875" Y="-21.963880859375" />
                  <Point X="-2.844617919922" Y="-21.950365234375" />
                  <Point X="-2.850920166016" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.183333007812" Y="-21.275404296875" />
                  <Point X="-2.700628417969" Y="-20.905318359375" />
                  <Point X="-2.606913330078" Y="-20.853251953125" />
                  <Point X="-2.167037353516" Y="-20.608865234375" />
                  <Point X="-2.074787109375" Y="-20.72908984375" />
                  <Point X="-2.043195068359" Y="-20.770259765625" />
                  <Point X="-2.02888684082" Y="-20.785205078125" />
                  <Point X="-2.012307373047" Y="-20.799115234375" />
                  <Point X="-1.995119873047" Y="-20.810599609375" />
                  <Point X="-1.980077514648" Y="-20.818431640625" />
                  <Point X="-1.899903686523" Y="-20.86016796875" />
                  <Point X="-1.88062512207" Y="-20.86766796875" />
                  <Point X="-1.859718261719" Y="-20.873271484375" />
                  <Point X="-1.839268310547" Y="-20.876419921875" />
                  <Point X="-1.818621826172" Y="-20.87506640625" />
                  <Point X="-1.797306518555" Y="-20.871306640625" />
                  <Point X="-1.777451049805" Y="-20.865517578125" />
                  <Point X="-1.761783447266" Y="-20.85902734375" />
                  <Point X="-1.67827722168" Y="-20.8244375" />
                  <Point X="-1.660145751953" Y="-20.814490234375" />
                  <Point X="-1.642416259766" Y="-20.802076171875" />
                  <Point X="-1.626864013672" Y="-20.7884375" />
                  <Point X="-1.614632568359" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734078125" />
                  <Point X="-1.590380859375" Y="-20.717904296875" />
                  <Point X="-1.563201171875" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584201904297" Y="-20.3681015625" />
                  <Point X="-1.584122802734" Y="-20.368080078125" />
                  <Point X="-0.94962487793" Y="-20.19019140625" />
                  <Point X="-0.836033203125" Y="-20.17689453125" />
                  <Point X="-0.294711303711" Y="-20.113541015625" />
                  <Point X="-0.164432159424" Y="-20.59975" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113998413" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907631" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425949097" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.307419525146" Y="-20.1120625" />
                  <Point X="0.844044250488" Y="-20.16826171875" />
                  <Point X="0.938031921387" Y="-20.190953125" />
                  <Point X="1.4810234375" Y="-20.322048828125" />
                  <Point X="1.540956665039" Y="-20.343787109375" />
                  <Point X="1.894643432617" Y="-20.472072265625" />
                  <Point X="1.953805786133" Y="-20.499740234375" />
                  <Point X="2.294564453125" Y="-20.6591015625" />
                  <Point X="2.351760986328" Y="-20.692423828125" />
                  <Point X="2.680971679688" Y="-20.884220703125" />
                  <Point X="2.734883056641" Y="-20.922560546875" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.283701171875" Y="-22.213134765625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.139879638672" Y="-22.465912109375" />
                  <Point X="2.131486328125" Y="-22.4906796875" />
                  <Point X="2.129684814453" Y="-22.49662890625" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108382324219" Y="-22.588724609375" />
                  <Point X="2.1083671875" Y="-22.61859375" />
                  <Point X="2.109050292969" Y="-22.630015625" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.1400703125" Y="-22.75252734375" />
                  <Point X="2.146856933594" Y="-22.762529296875" />
                  <Point X="2.183028076172" Y="-22.8158359375" />
                  <Point X="2.199609130859" Y="-22.83444921875" />
                  <Point X="2.22291015625" Y="-22.854537109375" />
                  <Point X="2.231599121094" Y="-22.8611953125" />
                  <Point X="2.28490625" Y="-22.8973671875" />
                  <Point X="2.304954101562" Y="-22.90773046875" />
                  <Point X="2.327043212891" Y="-22.91599609375" />
                  <Point X="2.348966308594" Y="-22.921337890625" />
                  <Point X="2.359934082031" Y="-22.92266015625" />
                  <Point X="2.418391113281" Y="-22.929708984375" />
                  <Point X="2.443832519531" Y="-22.929345703125" />
                  <Point X="2.475418701172" Y="-22.924615234375" />
                  <Point X="2.485890380859" Y="-22.9224375" />
                  <Point X="2.553493164062" Y="-22.904359375" />
                  <Point X="2.565287841797" Y="-22.900359375" />
                  <Point X="2.588533691406" Y="-22.889853515625" />
                  <Point X="3.845149658203" Y="-22.164345703125" />
                  <Point X="3.967324951172" Y="-22.09380859375" />
                  <Point X="4.123274414062" Y="-22.31054296875" />
                  <Point X="4.153323242187" Y="-22.36019921875" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.409998779297" Y="-23.19403125" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.216885742188" Y="-23.344630859375" />
                  <Point X="3.198512695312" Y="-23.3657890625" />
                  <Point X="3.194845458984" Y="-23.37028125" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.133621582031" Y="-23.455677734375" />
                  <Point X="3.121751464844" Y="-23.484787109375" />
                  <Point X="3.118229248047" Y="-23.495072265625" />
                  <Point X="3.100105712891" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.100530761719" Y="-23.641759765625" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.123869873047" Y="-23.737927734375" />
                  <Point X="3.138659912109" Y="-23.76684375" />
                  <Point X="3.143874511719" Y="-23.775798828125" />
                  <Point X="3.184340087891" Y="-23.8373046875" />
                  <Point X="3.198892578125" Y="-23.854548828125" />
                  <Point X="3.216135986328" Y="-23.870638671875" />
                  <Point X="3.234345458984" Y="-23.88396484375" />
                  <Point X="3.245347412109" Y="-23.890158203125" />
                  <Point X="3.303987548828" Y="-23.92316796875" />
                  <Point X="3.328210449219" Y="-23.932708984375" />
                  <Point X="3.361061767578" Y="-23.940671875" />
                  <Point X="3.370993652344" Y="-23.94252734375" />
                  <Point X="3.450278808594" Y="-23.953005859375" />
                  <Point X="3.462697753906" Y="-23.95382421875" />
                  <Point X="3.488203857422" Y="-23.953015625" />
                  <Point X="4.681904785156" Y="-23.79586328125" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.85540625" Y="-24.128015625" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.921963867188" Y="-24.615376953125" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.698408447266" Y="-24.67733984375" />
                  <Point X="3.671713867188" Y="-24.690794921875" />
                  <Point X="3.666932861328" Y="-24.69337890625" />
                  <Point X="3.589037597656" Y="-24.73840234375" />
                  <Point X="3.568669433594" Y="-24.75421875" />
                  <Point X="3.545587890625" Y="-24.7778125" />
                  <Point X="3.53876171875" Y="-24.785595703125" />
                  <Point X="3.492024658203" Y="-24.845150390625" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463681152344" Y="-24.90739453125" />
                  <Point X="3.460758056641" Y="-24.92265625" />
                  <Point X="3.445178955078" Y="-25.004005859375" />
                  <Point X="3.443830566406" Y="-25.029998046875" />
                  <Point X="3.446753662109" Y="-25.064068359375" />
                  <Point X="3.4481015625" Y="-25.07381640625" />
                  <Point X="3.463680664062" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.1766640625" />
                  <Point X="3.480300292969" Y="-25.19812890625" />
                  <Point X="3.492024169922" Y="-25.217408203125" />
                  <Point X="3.50079296875" Y="-25.22858203125" />
                  <Point X="3.547530029297" Y="-25.28813671875" />
                  <Point X="3.566685546875" Y="-25.306533203125" />
                  <Point X="3.595612060547" Y="-25.327400390625" />
                  <Point X="3.603649902344" Y="-25.332603515625" />
                  <Point X="3.681545166016" Y="-25.37762890625" />
                  <Point X="3.692709472656" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.811258300781" Y="-25.685470703125" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.842889648438" Y="-26.00189453125" />
                  <Point X="4.801173828125" Y="-26.184697265625" />
                  <Point X="3.662569824219" Y="-26.034798828125" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035400391" Y="-26.0027109375" />
                  <Point X="3.374658935547" Y="-26.005509765625" />
                  <Point X="3.345975341797" Y="-26.011744140625" />
                  <Point X="3.193094726563" Y="-26.04497265625" />
                  <Point X="3.163973632812" Y="-26.05659765625" />
                  <Point X="3.136146972656" Y="-26.073490234375" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.095059814453" Y="-26.1148125" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.967272705078" Y="-26.332369140625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.507564453125" />
                  <Point X="2.964078613281" Y="-26.539185546875" />
                  <Point X="2.976451660156" Y="-26.568" />
                  <Point X="2.992325683594" Y="-26.592689453125" />
                  <Point X="3.076931884766" Y="-26.7242890625" />
                  <Point X="3.086931396484" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="4.12649609375" Y="-27.5404140625" />
                  <Point X="4.213122558594" Y="-27.606884765625" />
                  <Point X="4.124810546875" Y="-27.749787109375" />
                  <Point X="4.099716796875" Y="-27.78544140625" />
                  <Point X="4.028981445312" Y="-27.885947265625" />
                  <Point X="3.013093261719" Y="-27.299423828125" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754226318359" Y="-27.159828125" />
                  <Point X="2.720088378906" Y="-27.153662109375" />
                  <Point X="2.538135986328" Y="-27.120802734375" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.416473388672" Y="-27.150103515625" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204532470703" Y="-27.2904375" />
                  <Point X="2.189606689453" Y="-27.318796875" />
                  <Point X="2.110053466797" Y="-27.469955078125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.101840576172" Y="-27.597396484375" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.804616455078" Y="-28.9567578125" />
                  <Point X="2.861283691406" Y="-29.054908203125" />
                  <Point X="2.781862060547" Y="-29.11163671875" />
                  <Point X="2.753798095703" Y="-29.12980078125" />
                  <Point X="2.701764160156" Y="-29.16348046875" />
                  <Point X="1.920427368164" Y="-28.145224609375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506469727" Y="-27.922181640625" />
                  <Point X="1.721924316406" Y="-27.900556640625" />
                  <Point X="1.688254882813" Y="-27.87891015625" />
                  <Point X="1.50880078125" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.75116796875" />
                  <Point X="1.448366088867" Y="-27.7434375" />
                  <Point X="1.417099487305" Y="-27.741119140625" />
                  <Point X="1.380276245117" Y="-27.7445078125" />
                  <Point X="1.184012695312" Y="-27.76256640625" />
                  <Point X="1.156363037109" Y="-27.7693984375" />
                  <Point X="1.128977050781" Y="-27.7807421875" />
                  <Point X="1.104594604492" Y="-27.795462890625" />
                  <Point X="1.076160522461" Y="-27.81910546875" />
                  <Point X="0.924610778809" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.86712286377" Y="-28.064923828125" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="1.004739868164" Y="-29.728318359375" />
                  <Point X="1.022065307617" Y="-29.859916015625" />
                  <Point X="0.975717712402" Y="-29.87007421875" />
                  <Point X="0.949757873535" Y="-29.874791015625" />
                  <Point X="0.929315551758" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058480224609" Y="-29.752625" />
                  <Point X="-1.098164672852" Y="-29.742416015625" />
                  <Point X="-1.141246459961" Y="-29.73133203125" />
                  <Point X="-1.128393554688" Y="-29.633705078125" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.54103515625" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.132896240234" Y="-29.44962109375" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026245117" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.23057434082" Y="-29.094861328125" />
                  <Point X="-1.250210083008" Y="-29.070935546875" />
                  <Point X="-1.261008422852" Y="-29.05977734375" />
                  <Point X="-1.297859008789" Y="-29.0274609375" />
                  <Point X="-1.49426940918" Y="-28.855212890625" />
                  <Point X="-1.506739746094" Y="-28.84596484375" />
                  <Point X="-1.533022460938" Y="-28.82962109375" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576531982422" Y="-28.810224609375" />
                  <Point X="-1.59131652832" Y="-28.805474609375" />
                  <Point X="-1.621458740234" Y="-28.798447265625" />
                  <Point X="-1.63681640625" Y="-28.796169921875" />
                  <Point X="-1.685725463867" Y="-28.79296484375" />
                  <Point X="-1.946405639648" Y="-28.77587890625" />
                  <Point X="-1.961929321289" Y="-28.7761328125" />
                  <Point X="-1.992726196289" Y="-28.779166015625" />
                  <Point X="-2.007999511719" Y="-28.7819453125" />
                  <Point X="-2.039048095703" Y="-28.790263671875" />
                  <Point X="-2.053667724609" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436035156" Y="-28.815810546875" />
                  <Point X="-2.136189697266" Y="-28.843041015625" />
                  <Point X="-2.353402587891" Y="-28.988177734375" />
                  <Point X="-2.359682373047" Y="-28.992755859375" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.503201660156" Y="-29.162478515625" />
                  <Point X="-2.747597412109" Y="-29.01115625" />
                  <Point X="-2.802583984375" Y="-28.96881640625" />
                  <Point X="-2.980862792969" Y="-28.831548828125" />
                  <Point X="-2.459094970703" Y="-27.9278203125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.336204345703" Y="-27.71348046875" />
                  <Point X="-2.323722167969" Y="-27.68383203125" />
                  <Point X="-2.318909179688" Y="-27.669169921875" />
                  <Point X="-2.313494140625" Y="-27.646638671875" />
                  <Point X="-2.310867675781" Y="-27.623619140625" />
                  <Point X="-2.311067871094" Y="-27.60044921875" />
                  <Point X="-2.311876953125" Y="-27.588869140625" />
                  <Point X="-2.316021728516" Y="-27.557388671875" />
                  <Point X="-2.318427001953" Y="-27.5452734375" />
                  <Point X="-2.324789306641" Y="-27.521455078125" />
                  <Point X="-2.334187744141" Y="-27.4986640625" />
                  <Point X="-2.346464111328" Y="-27.47728515625" />
                  <Point X="-2.353299072266" Y="-27.466994140625" />
                  <Point X="-2.365623779297" Y="-27.450828125" />
                  <Point X="-2.382372802734" Y="-27.431671875" />
                  <Point X="-2.396980712891" Y="-27.417064453125" />
                  <Point X="-2.408819091797" Y="-27.40701953125" />
                  <Point X="-2.433976074219" Y="-27.3889921875" />
                  <Point X="-2.447294677734" Y="-27.381009765625" />
                  <Point X="-2.476123779297" Y="-27.36679296875" />
                  <Point X="-2.490564453125" Y="-27.3610859375" />
                  <Point X="-2.520182617188" Y="-27.3521015625" />
                  <Point X="-2.550870361328" Y="-27.3480625" />
                  <Point X="-2.5818046875" Y="-27.349076171875" />
                  <Point X="-2.597228759766" Y="-27.3508515625" />
                  <Point X="-2.628754638672" Y="-27.357123046875" />
                  <Point X="-2.643685302734" Y="-27.36138671875" />
                  <Point X="-2.672649902344" Y="-27.3722890625" />
                  <Point X="-2.686683837891" Y="-27.378927734375" />
                  <Point X="-3.772383544922" Y="-28.005755859375" />
                  <Point X="-3.793089111328" Y="-28.0177109375" />
                  <Point X="-4.004012939453" Y="-27.74059765625" />
                  <Point X="-4.0434453125" Y="-27.6744765625" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.253894775391" Y="-26.731779296875" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039159423828" Y="-26.566068359375" />
                  <Point X="-3.016269775391" Y="-26.543435546875" />
                  <Point X="-3.0028828125" Y="-26.52683203125" />
                  <Point X="-2.984183837891" Y="-26.497404296875" />
                  <Point X="-2.976196289062" Y="-26.481826171875" />
                  <Point X="-2.968265380859" Y="-26.462056640625" />
                  <Point X="-2.960674072266" Y="-26.438955078125" />
                  <Point X="-2.954186767578" Y="-26.413908203125" />
                  <Point X="-2.951953369141" Y="-26.402400390625" />
                  <Point X="-2.947838623047" Y="-26.37091796875" />
                  <Point X="-2.947081787109" Y="-26.355703125" />
                  <Point X="-2.947780761719" Y="-26.3328359375" />
                  <Point X="-2.951228271484" Y="-26.31021875" />
                  <Point X="-2.957374023438" Y="-26.288181640625" />
                  <Point X="-2.961112548828" Y="-26.277349609375" />
                  <Point X="-2.973115722656" Y="-26.248369140625" />
                  <Point X="-2.978571044922" Y="-26.23729296875" />
                  <Point X="-2.990873291016" Y="-26.21594140625" />
                  <Point X="-3.005840820312" Y="-26.196365234375" />
                  <Point X="-3.023220458984" Y="-26.178896484375" />
                  <Point X="-3.032479492188" Y="-26.170728515625" />
                  <Point X="-3.049168945312" Y="-26.15783203125" />
                  <Point X="-3.068977050781" Y="-26.1444296875" />
                  <Point X="-3.091275878906" Y="-26.131306640625" />
                  <Point X="-3.105436523438" Y="-26.12448046875" />
                  <Point X="-3.13469921875" Y="-26.113255859375" />
                  <Point X="-3.149801269531" Y="-26.108857421875" />
                  <Point X="-3.181687255859" Y="-26.102376953125" />
                  <Point X="-3.197304443359" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.614921386719" Y="-26.280638671875" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.740762695312" Y="-25.9741171875" />
                  <Point X="-4.751193359375" Y="-25.901185546875" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.742693847656" Y="-25.374982421875" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.498658203125" Y="-25.309033203125" />
                  <Point X="-3.479789794922" Y="-25.301748046875" />
                  <Point X="-3.470551269531" Y="-25.297603515625" />
                  <Point X="-3.450598876953" Y="-25.28733984375" />
                  <Point X="-3.429178466797" Y="-25.27447265625" />
                  <Point X="-3.405810058594" Y="-25.25825390625" />
                  <Point X="-3.396477783203" Y="-25.250869140625" />
                  <Point X="-3.372522460938" Y="-25.229341796875" />
                  <Point X="-3.358345458984" Y="-25.213376953125" />
                  <Point X="-3.338243164062" Y="-25.184828125" />
                  <Point X="-3.329513427734" Y="-25.169619140625" />
                  <Point X="-3.320626464844" Y="-25.150171875" />
                  <Point X="-3.3119765625" Y="-25.12751953125" />
                  <Point X="-3.304187011719" Y="-25.102421875" />
                  <Point X="-3.300709472656" Y="-25.086501953125" />
                  <Point X="-3.296525878906" Y="-25.0543046875" />
                  <Point X="-3.295819824219" Y="-25.03802734375" />
                  <Point X="-3.297281005859" Y="-25.003677734375" />
                  <Point X="-3.298592285156" Y="-24.991482421875" />
                  <Point X="-3.302775390625" Y="-24.967361328125" />
                  <Point X="-3.305647216797" Y="-24.955435546875" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.317668457031" Y="-24.91921484375" />
                  <Point X="-3.330984619141" Y="-24.889890625" />
                  <Point X="-3.34201171875" Y="-24.87147265625" />
                  <Point X="-3.363354492188" Y="-24.8435546875" />
                  <Point X="-3.375329101562" Y="-24.83058984375" />
                  <Point X="-3.391532470703" Y="-24.816029296875" />
                  <Point X="-3.410194580078" Y="-24.801263671875" />
                  <Point X="-3.433562988281" Y="-24.785044921875" />
                  <Point X="-3.440481445312" Y="-24.780671875" />
                  <Point X="-3.465612548828" Y="-24.767166015625" />
                  <Point X="-3.496565673828" Y="-24.75436328125" />
                  <Point X="-3.508288330078" Y="-24.75038671875" />
                  <Point X="-4.757647949219" Y="-24.415623046875" />
                  <Point X="-4.785446289062" Y="-24.408173828125" />
                  <Point X="-4.731332519531" Y="-24.04248046875" />
                  <Point X="-4.710330566406" Y="-23.9649765625" />
                  <Point X="-4.633584960938" Y="-23.681763671875" />
                  <Point X="-3.946093994141" Y="-23.7722734375" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.767739013672" Y="-23.79518359375" />
                  <Point X="-3.747064453125" Y="-23.795634765625" />
                  <Point X="-3.736717041016" Y="-23.795296875" />
                  <Point X="-3.715160400391" Y="-23.793412109375" />
                  <Point X="-3.704908935547" Y="-23.79194921875" />
                  <Point X="-3.684624267578" Y="-23.787916015625" />
                  <Point X="-3.664881347656" Y="-23.78228515625" />
                  <Point X="-3.613159423828" Y="-23.7659765625" />
                  <Point X="-3.603465087891" Y="-23.762328125" />
                  <Point X="-3.584529296875" Y="-23.753998046875" />
                  <Point X="-3.575287841797" Y="-23.74931640625" />
                  <Point X="-3.556543945313" Y="-23.73849609375" />
                  <Point X="-3.547869384766" Y="-23.7328359375" />
                  <Point X="-3.531184326172" Y="-23.720603515625" />
                  <Point X="-3.515925292969" Y="-23.70662109375" />
                  <Point X="-3.502284179688" Y="-23.691064453125" />
                  <Point X="-3.495891601562" Y="-23.68291796875" />
                  <Point X="-3.483477539062" Y="-23.6651875" />
                  <Point X="-3.478010498047" Y="-23.656396484375" />
                  <Point X="-3.468063232422" Y="-23.638265625" />
                  <Point X="-3.459689208984" Y="-23.619525390625" />
                  <Point X="-3.438935546875" Y="-23.569421875" />
                  <Point X="-3.435499267578" Y="-23.559650390625" />
                  <Point X="-3.4297109375" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436012451172" Y="-23.39546875" />
                  <Point X="-3.443508789062" Y="-23.37619140625" />
                  <Point X="-3.452481689453" Y="-23.357732421875" />
                  <Point X="-3.477523193359" Y="-23.30962890625" />
                  <Point X="-3.482799072266" Y="-23.300716796875" />
                  <Point X="-3.494290771484" Y="-23.283517578125" />
                  <Point X="-3.500506591797" Y="-23.27523046875" />
                  <Point X="-3.514418701172" Y="-23.258650390625" />
                  <Point X="-3.521498535156" Y="-23.251091796875" />
                  <Point X="-3.536440917969" Y="-23.236787109375" />
                  <Point X="-3.544303466797" Y="-23.230041015625" />
                  <Point X="-4.227614257812" Y="-22.705716796875" />
                  <Point X="-4.002299072266" Y="-22.31969921875" />
                  <Point X="-3.946669433594" Y="-22.248193359375" />
                  <Point X="-3.726336425781" Y="-21.964986328125" />
                  <Point X="-3.357355224609" Y="-22.178017578125" />
                  <Point X="-3.254157226562" Y="-22.237599609375" />
                  <Point X="-3.244925048828" Y="-22.242279296875" />
                  <Point X="-3.225997558594" Y="-22.250609375" />
                  <Point X="-3.216302246094" Y="-22.254259765625" />
                  <Point X="-3.19566015625" Y="-22.26076953125" />
                  <Point X="-3.185619384766" Y="-22.26334375" />
                  <Point X="-3.165321289062" Y="-22.267380859375" />
                  <Point X="-3.141548828125" Y="-22.270025390625" />
                  <Point X="-3.069514648438" Y="-22.276328125" />
                  <Point X="-3.05916796875" Y="-22.276666015625" />
                  <Point X="-3.038488769531" Y="-22.27621484375" />
                  <Point X="-3.02815625" Y="-22.27542578125" />
                  <Point X="-3.006697753906" Y="-22.272599609375" />
                  <Point X="-2.996520751953" Y="-22.270689453125" />
                  <Point X="-2.976432128906" Y="-22.2657734375" />
                  <Point X="-2.9569921875" Y="-22.258697265625" />
                  <Point X="-2.938443359375" Y="-22.249548828125" />
                  <Point X="-2.929422851563" Y="-22.244470703125" />
                  <Point X="-2.911168457031" Y="-22.232841796875" />
                  <Point X="-2.902748291016" Y="-22.22680859375" />
                  <Point X="-2.886613769531" Y="-22.213859375" />
                  <Point X="-2.869306152344" Y="-22.197349609375" />
                  <Point X="-2.81817578125" Y="-22.146220703125" />
                  <Point X="-2.811264404297" Y="-22.138509765625" />
                  <Point X="-2.798320800781" Y="-22.1223828125" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.986634765625" />
                  <Point X="-2.748458251953" Y="-21.965955078125" />
                  <Point X="-2.749979248047" Y="-21.9420859375" />
                  <Point X="-2.756281494141" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761781005859" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.059386962891" Y="-21.300083984375" />
                  <Point X="-2.648377197266" Y="-20.984966796875" />
                  <Point X="-2.560775634766" Y="-20.936296875" />
                  <Point X="-2.192525634766" Y="-20.731703125" />
                  <Point X="-2.150156005859" Y="-20.786921875" />
                  <Point X="-2.118563964844" Y="-20.828091796875" />
                  <Point X="-2.111816894531" Y="-20.83595703125" />
                  <Point X="-2.097508544922" Y="-20.85090234375" />
                  <Point X="-2.089947265625" Y="-20.857982421875" />
                  <Point X="-2.073367919922" Y="-20.871892578125" />
                  <Point X="-2.065086669922" Y="-20.87810546875" />
                  <Point X="-2.047899169922" Y="-20.88958984375" />
                  <Point X="-2.023950073242" Y="-20.902693359375" />
                  <Point X="-1.943776245117" Y="-20.9444296875" />
                  <Point X="-1.934347167969" Y="-20.948703125" />
                  <Point X="-1.915068603516" Y="-20.956203125" />
                  <Point X="-1.905219238281" Y="-20.9594296875" />
                  <Point X="-1.88431237793" Y="-20.965033203125" />
                  <Point X="-1.874174072266" Y="-20.967166015625" />
                  <Point X="-1.853723999023" Y="-20.970314453125" />
                  <Point X="-1.833053710938" Y="-20.971216796875" />
                  <Point X="-1.812407226562" Y="-20.96986328125" />
                  <Point X="-1.802119750977" Y="-20.968623046875" />
                  <Point X="-1.780804443359" Y="-20.96486328125" />
                  <Point X="-1.770715332031" Y="-20.962509765625" />
                  <Point X="-1.750859985352" Y="-20.956720703125" />
                  <Point X="-1.725426147461" Y="-20.946794921875" />
                  <Point X="-1.641919921875" Y="-20.912205078125" />
                  <Point X="-1.632583374023" Y="-20.9077265625" />
                  <Point X="-1.614451782227" Y="-20.897779296875" />
                  <Point X="-1.605656738281" Y="-20.892310546875" />
                  <Point X="-1.587927246094" Y="-20.879896484375" />
                  <Point X="-1.579779296875" Y="-20.873501953125" />
                  <Point X="-1.564226928711" Y="-20.85986328125" />
                  <Point X="-1.550251708984" Y="-20.844611328125" />
                  <Point X="-1.538020263672" Y="-20.8279296875" />
                  <Point X="-1.532359863281" Y="-20.819255859375" />
                  <Point X="-1.521538085938" Y="-20.80051171875" />
                  <Point X="-1.516855102539" Y="-20.79126953125" />
                  <Point X="-1.508524780273" Y="-20.7723359375" />
                  <Point X="-1.499777709961" Y="-20.746470703125" />
                  <Point X="-1.472598144531" Y="-20.660267578125" />
                  <Point X="-1.470026977539" Y="-20.650236328125" />
                  <Point X="-1.465991577148" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-0.931166809082" Y="-20.2836796875" />
                  <Point X="-0.824988037109" Y="-20.27125" />
                  <Point X="-0.36522253418" Y="-20.21744140625" />
                  <Point X="-0.256195098877" Y="-20.624337890625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.378190765381" Y="-20.2149921875" />
                  <Point X="0.827864624023" Y="-20.2620859375" />
                  <Point X="0.915736755371" Y="-20.28330078125" />
                  <Point X="1.453606323242" Y="-20.41316015625" />
                  <Point X="1.508564331055" Y="-20.43309375" />
                  <Point X="1.858248535156" Y="-20.559927734375" />
                  <Point X="1.913561523438" Y="-20.585794921875" />
                  <Point X="2.250438232422" Y="-20.743341796875" />
                  <Point X="2.303938720703" Y="-20.774509765625" />
                  <Point X="2.629436279297" Y="-20.964142578125" />
                  <Point X="2.679825683594" Y="-20.999978515625" />
                  <Point X="2.817779541016" Y="-21.098083984375" />
                  <Point X="2.201428710938" Y="-22.165634765625" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.061038330078" Y="-22.40971875" />
                  <Point X="2.049905761719" Y="-22.435421875" />
                  <Point X="2.041512207031" Y="-22.460189453125" />
                  <Point X="2.037909545898" Y="-22.472087890625" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017419799805" Y="-22.55183203125" />
                  <Point X="2.01419519043" Y="-22.57632421875" />
                  <Point X="2.013382202148" Y="-22.58867578125" />
                  <Point X="2.01336730957" Y="-22.618544921875" />
                  <Point X="2.014733520508" Y="-22.641388671875" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144042969" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045318847656" Y="-22.776111328125" />
                  <Point X="2.055680664062" Y="-22.796154296875" />
                  <Point X="2.068245117188" Y="-22.815869140625" />
                  <Point X="2.104416259766" Y="-22.86917578125" />
                  <Point X="2.112092285156" Y="-22.87902734375" />
                  <Point X="2.128673339844" Y="-22.897640625" />
                  <Point X="2.137578369141" Y="-22.90640234375" />
                  <Point X="2.160879394531" Y="-22.926490234375" />
                  <Point X="2.178257324219" Y="-22.939806640625" />
                  <Point X="2.231564453125" Y="-22.975978515625" />
                  <Point X="2.241281982422" Y="-22.9817578125" />
                  <Point X="2.261329833984" Y="-22.99212109375" />
                  <Point X="2.27166015625" Y="-22.996705078125" />
                  <Point X="2.293749267578" Y="-23.004970703125" />
                  <Point X="2.304553466797" Y="-23.008294921875" />
                  <Point X="2.3264765625" Y="-23.01363671875" />
                  <Point X="2.348563232422" Y="-23.0169765625" />
                  <Point X="2.407020263672" Y="-23.024025390625" />
                  <Point X="2.419747558594" Y="-23.02469921875" />
                  <Point X="2.445188964844" Y="-23.0243359375" />
                  <Point X="2.457903076172" Y="-23.023298828125" />
                  <Point X="2.489489257812" Y="-23.018568359375" />
                  <Point X="2.510432617188" Y="-23.014212890625" />
                  <Point X="2.578035400391" Y="-22.996134765625" />
                  <Point X="2.584004150391" Y="-22.994326171875" />
                  <Point X="2.604412597656" Y="-22.9869296875" />
                  <Point X="2.627658447266" Y="-22.976423828125" />
                  <Point X="2.636033691406" Y="-22.972125" />
                  <Point X="3.892649658203" Y="-22.2466171875" />
                  <Point X="3.94040234375" Y="-22.219046875" />
                  <Point X="4.043956787109" Y="-22.36296484375" />
                  <Point X="4.072046142578" Y="-22.4093828125" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.352166503906" Y="-23.118662109375" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.165669921875" Y="-23.262373046875" />
                  <Point X="3.145155517578" Y="-23.28234375" />
                  <Point X="3.126782470703" Y="-23.303501953125" />
                  <Point X="3.119447998047" Y="-23.312486328125" />
                  <Point X="3.070794189453" Y="-23.375958984375" />
                  <Point X="3.063776855469" Y="-23.386501953125" />
                  <Point X="3.051206787109" Y="-23.40842578125" />
                  <Point X="3.045654052734" Y="-23.419806640625" />
                  <Point X="3.033783935547" Y="-23.448916015625" />
                  <Point X="3.026739501953" Y="-23.469486328125" />
                  <Point X="3.008615966797" Y="-23.53429296875" />
                  <Point X="3.006224853516" Y="-23.54533984375" />
                  <Point X="3.002771484375" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.007490722656" Y="-23.66095703125" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.025787841797" Y="-23.74537890625" />
                  <Point X="3.034249267578" Y="-23.76944140625" />
                  <Point X="3.039291259766" Y="-23.7811875" />
                  <Point X="3.054081298828" Y="-23.810103515625" />
                  <Point X="3.064510498047" Y="-23.828013671875" />
                  <Point X="3.104976074219" Y="-23.88951953125" />
                  <Point X="3.111738037109" Y="-23.89857421875" />
                  <Point X="3.126290527344" Y="-23.915818359375" />
                  <Point X="3.134081054688" Y="-23.9240078125" />
                  <Point X="3.151324462891" Y="-23.94009765625" />
                  <Point X="3.160031494141" Y="-23.947302734375" />
                  <Point X="3.178240966797" Y="-23.96062890625" />
                  <Point X="3.198745361328" Y="-23.972943359375" />
                  <Point X="3.257385498047" Y="-24.005953125" />
                  <Point X="3.269171875" Y="-24.01155859375" />
                  <Point X="3.293394775391" Y="-24.021099609375" />
                  <Point X="3.305831298828" Y="-24.02503515625" />
                  <Point X="3.338682617188" Y="-24.032998046875" />
                  <Point X="3.358546386719" Y="-24.036708984375" />
                  <Point X="3.437831542969" Y="-24.0471875" />
                  <Point X="3.444032226562" Y="-24.04780078125" />
                  <Point X="3.465708007812" Y="-24.04877734375" />
                  <Point X="3.491214111328" Y="-24.04796875" />
                  <Point X="3.500603759766" Y="-24.047203125" />
                  <Point X="4.6943046875" Y="-23.89005078125" />
                  <Point X="4.704704589844" Y="-23.888681640625" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.761537109375" Y="-24.142630859375" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="3.897375976562" Y="-24.52361328125" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.682728759766" Y="-24.581646484375" />
                  <Point X="3.655649414062" Y="-24.592505859375" />
                  <Point X="3.628954833984" Y="-24.6059609375" />
                  <Point X="3.619392822266" Y="-24.61112890625" />
                  <Point X="3.541497558594" Y="-24.65615234375" />
                  <Point X="3.530771728516" Y="-24.663369140625" />
                  <Point X="3.510403564453" Y="-24.679185546875" />
                  <Point X="3.500761230469" Y="-24.68778515625" />
                  <Point X="3.4776796875" Y="-24.71137890625" />
                  <Point X="3.46402734375" Y="-24.7269453125" />
                  <Point X="3.417290283203" Y="-24.7865" />
                  <Point X="3.410853271484" Y="-24.79579296875" />
                  <Point X="3.39912890625" Y="-24.81507421875" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.380004394531" Y="-24.85707421875" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.367454101562" Y="-24.90478515625" />
                  <Point X="3.351875" Y="-24.986134765625" />
                  <Point X="3.350306640625" Y="-24.999083984375" />
                  <Point X="3.348958251953" Y="-25.025076171875" />
                  <Point X="3.349178222656" Y="-25.038119140625" />
                  <Point X="3.352101318359" Y="-25.072189453125" />
                  <Point X="3.354797119141" Y="-25.091685546875" />
                  <Point X="3.370376220703" Y="-25.173033203125" />
                  <Point X="3.373158447266" Y="-25.183986328125" />
                  <Point X="3.380004150391" Y="-25.205486328125" />
                  <Point X="3.384067626953" Y="-25.216033203125" />
                  <Point X="3.393841552734" Y="-25.237498046875" />
                  <Point X="3.399130126953" Y="-25.24748828125" />
                  <Point X="3.410854003906" Y="-25.266767578125" />
                  <Point X="3.426058105469" Y="-25.28723046875" />
                  <Point X="3.472795166016" Y="-25.34678515625" />
                  <Point X="3.481726074219" Y="-25.35665625" />
                  <Point X="3.500881591797" Y="-25.375052734375" />
                  <Point X="3.511106201172" Y="-25.383578125" />
                  <Point X="3.540032714844" Y="-25.4044453125" />
                  <Point X="3.556108398438" Y="-25.4148515625" />
                  <Point X="3.634003662109" Y="-25.459876953125" />
                  <Point X="3.639490234375" Y="-25.4628125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683026855469" Y="-25.481029296875" />
                  <Point X="3.6919921875" Y="-25.483916015625" />
                  <Point X="4.784876464844" Y="-25.77675390625" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.750270507812" Y="-25.980759765625" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.674969726562" Y="-25.940611328125" />
                  <Point X="3.436781738281" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400096923828" Y="-25.90804296875" />
                  <Point X="3.366720458984" Y="-25.910841796875" />
                  <Point X="3.354481689453" Y="-25.912677734375" />
                  <Point X="3.325798095703" Y="-25.918912109375" />
                  <Point X="3.172917480469" Y="-25.952140625" />
                  <Point X="3.157873779297" Y="-25.9567421875" />
                  <Point X="3.128752685547" Y="-25.9683671875" />
                  <Point X="3.114675292969" Y="-25.975390625" />
                  <Point X="3.086848632812" Y="-25.992283203125" />
                  <Point X="3.074123291016" Y="-26.00153125" />
                  <Point X="3.050373535156" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="3.02201171875" Y="-26.054076171875" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606201172" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.872672363281" Y="-26.3236640625" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607177734" Y="-26.51458984375" />
                  <Point X="2.864065429688" Y="-26.530126953125" />
                  <Point X="2.871796875" Y="-26.561748046875" />
                  <Point X="2.876786132812" Y="-26.576669921875" />
                  <Point X="2.889159179688" Y="-26.605484375" />
                  <Point X="2.89654296875" Y="-26.619376953125" />
                  <Point X="2.912416992188" Y="-26.64406640625" />
                  <Point X="2.997023193359" Y="-26.775666015625" />
                  <Point X="3.001745117188" Y="-26.782357421875" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486083984" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="4.068663818359" Y="-27.615783203125" />
                  <Point X="4.087170654297" Y="-27.629984375" />
                  <Point X="4.045493164062" Y="-27.697423828125" />
                  <Point X="4.022029052734" Y="-27.730763671875" />
                  <Point X="4.001273681641" Y="-27.76025390625" />
                  <Point X="3.060593261719" Y="-27.21715234375" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841199951172" Y="-27.090890625" />
                  <Point X="2.815017578125" Y="-27.079509765625" />
                  <Point X="2.783114990234" Y="-27.069326171875" />
                  <Point X="2.771112060547" Y="-27.066341796875" />
                  <Point X="2.736974121094" Y="-27.06017578125" />
                  <Point X="2.555021728516" Y="-27.02731640625" />
                  <Point X="2.539366943359" Y="-27.025810546875" />
                  <Point X="2.508014648438" Y="-27.025404296875" />
                  <Point X="2.492317138672" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444846923828" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400589599609" Y="-27.051109375" />
                  <Point X="2.372229248047" Y="-27.06603515625" />
                  <Point X="2.221071533203" Y="-27.145587890625" />
                  <Point X="2.208967773438" Y="-27.153171875" />
                  <Point X="2.186038330078" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144943115234" Y="-27.211158203125" />
                  <Point X="2.128049316406" Y="-27.234087890625" />
                  <Point X="2.120465087891" Y="-27.24619140625" />
                  <Point X="2.105539306641" Y="-27.27455078125" />
                  <Point X="2.025985961914" Y="-27.425708984375" />
                  <Point X="2.019836914062" Y="-27.440189453125" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.002187744141" Y="-27.580140625" />
                  <Point X="2.008352905273" Y="-27.614279296875" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.722343994141" Y="-29.0042578125" />
                  <Point X="2.735893554688" Y="-29.0277265625" />
                  <Point X="2.723754394531" Y="-29.036083984375" />
                  <Point X="1.995795898438" Y="-28.087392578125" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657714844" Y="-27.87015234375" />
                  <Point X="1.808835571289" Y="-27.849630859375" />
                  <Point X="1.783253417969" Y="-27.828005859375" />
                  <Point X="1.773299560547" Y="-27.820646484375" />
                  <Point X="1.739630004883" Y="-27.799" />
                  <Point X="1.56017590332" Y="-27.68362890625" />
                  <Point X="1.546279663086" Y="-27.676244140625" />
                  <Point X="1.517465332031" Y="-27.663873046875" />
                  <Point X="1.502547363281" Y="-27.65888671875" />
                  <Point X="1.470927001953" Y="-27.65115625" />
                  <Point X="1.45539074707" Y="-27.648697265625" />
                  <Point X="1.424124267578" Y="-27.64637890625" />
                  <Point X="1.408393920898" Y="-27.64651953125" />
                  <Point X="1.371570556641" Y="-27.649908203125" />
                  <Point X="1.175307128906" Y="-27.667966796875" />
                  <Point X="1.161224121094" Y="-27.67033984375" />
                  <Point X="1.133574462891" Y="-27.677171875" />
                  <Point X="1.1200078125" Y="-27.681630859375" />
                  <Point X="1.092621826172" Y="-27.692974609375" />
                  <Point X="1.079876342773" Y="-27.6994140625" />
                  <Point X="1.055493896484" Y="-27.714134765625" />
                  <Point X="1.043856689453" Y="-27.722416015625" />
                  <Point X="1.015422607422" Y="-27.74605859375" />
                  <Point X="0.86387286377" Y="-27.872068359375" />
                  <Point X="0.852652404785" Y="-27.883091796875" />
                  <Point X="0.832182861328" Y="-27.90683984375" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.774290405273" Y="-28.04474609375" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833090942383" Y="-29.152337890625" />
                  <Point X="0.718349060059" Y="-28.724115234375" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146789551" Y="-28.453583984375" />
                  <Point X="0.626788085938" Y="-28.42381640625" />
                  <Point X="0.620407287598" Y="-28.41320703125" />
                  <Point X="0.594541564941" Y="-28.375939453125" />
                  <Point X="0.4566796875" Y="-28.177306640625" />
                  <Point X="0.446668609619" Y="-28.16516796875" />
                  <Point X="0.424785675049" Y="-28.14271484375" />
                  <Point X="0.412913513184" Y="-28.132400390625" />
                  <Point X="0.38665838623" Y="-28.11315625" />
                  <Point X="0.373243530273" Y="-28.1049375" />
                  <Point X="0.34524029541" Y="-28.090828125" />
                  <Point X="0.330652069092" Y="-28.0849375" />
                  <Point X="0.290626281738" Y="-28.072515625" />
                  <Point X="0.077292984009" Y="-28.0063046875" />
                  <Point X="0.063376678467" Y="-28.003109375" />
                  <Point X="0.035218582153" Y="-27.99883984375" />
                  <Point X="0.020976791382" Y="-27.997765625" />
                  <Point X="-0.008664410591" Y="-27.997765625" />
                  <Point X="-0.022904119492" Y="-27.99883984375" />
                  <Point X="-0.051066375732" Y="-28.003109375" />
                  <Point X="-0.064988777161" Y="-28.0063046875" />
                  <Point X="-0.105014556885" Y="-28.018728515625" />
                  <Point X="-0.318347717285" Y="-28.0849375" />
                  <Point X="-0.332932830811" Y="-28.090828125" />
                  <Point X="-0.360931304932" Y="-28.104935546875" />
                  <Point X="-0.374344512939" Y="-28.11315234375" />
                  <Point X="-0.400600250244" Y="-28.132396484375" />
                  <Point X="-0.412474456787" Y="-28.14271484375" />
                  <Point X="-0.434358306885" Y="-28.165169921875" />
                  <Point X="-0.444368041992" Y="-28.177306640625" />
                  <Point X="-0.470233642578" Y="-28.21457421875" />
                  <Point X="-0.60809564209" Y="-28.41320703125" />
                  <Point X="-0.612471252441" Y="-28.420130859375" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.980050170898" Y="-29.746748046875" />
                  <Point X="-0.985424621582" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.669035092634" Y="-26.071481941456" />
                  <Point X="4.691396014669" Y="-25.751705858244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.574671820505" Y="-26.059058835571" />
                  <Point X="4.597915564493" Y="-25.726657810238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.697023597045" Y="-24.309346913362" />
                  <Point X="4.721589434538" Y="-23.958039070052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.480308548376" Y="-26.046635729686" />
                  <Point X="4.504435114318" Y="-25.701609762232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.599973187259" Y="-24.335351666299" />
                  <Point X="4.630524500689" Y="-23.89844752922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.385945276247" Y="-26.034212623801" />
                  <Point X="4.410954664143" Y="-25.676561714226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.502922777473" Y="-24.361356419235" />
                  <Point X="4.534407672747" Y="-23.911101439789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.291582004119" Y="-26.021789517916" />
                  <Point X="4.317474213968" Y="-25.65151366622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.405872367687" Y="-24.387361172171" />
                  <Point X="4.438290844805" Y="-23.923755350357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.083476036914" Y="-27.635962733447" />
                  <Point X="4.084060945688" Y="-27.627598148279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.19721873199" Y="-26.009366412031" />
                  <Point X="4.223993763792" Y="-25.626465618214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.308821957901" Y="-24.413365925108" />
                  <Point X="4.342174016862" Y="-23.936409260925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.980395663173" Y="-27.748199988456" />
                  <Point X="3.993678613992" Y="-27.558244941884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102855459861" Y="-25.996943306146" />
                  <Point X="4.130513313617" Y="-25.601417570207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.211771548115" Y="-24.439370678044" />
                  <Point X="4.24605718892" Y="-23.949063171494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.888859212947" Y="-27.695351445965" />
                  <Point X="3.903296262698" Y="-27.488892015741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008492187733" Y="-25.98452020026" />
                  <Point X="4.037032863442" Y="-25.576369522201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.114721138329" Y="-24.465375430981" />
                  <Point X="4.149940360978" Y="-23.961717082062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.797322762722" Y="-27.642502903473" />
                  <Point X="3.812913911405" Y="-27.419539089598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.914128915604" Y="-25.972097094375" />
                  <Point X="3.943552413267" Y="-25.551321474195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.017670728543" Y="-24.491380183917" />
                  <Point X="4.053823533035" Y="-23.974370992631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.705786312496" Y="-27.589654360982" />
                  <Point X="3.722531560111" Y="-27.350186163455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.819765643475" Y="-25.95967398849" />
                  <Point X="3.850071963091" Y="-25.526273426189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.920620318757" Y="-24.517384936854" />
                  <Point X="3.957706705093" Y="-23.987024903199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.056204794207" Y="-22.578436603854" />
                  <Point X="4.068442594279" Y="-22.403427909314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.614249862271" Y="-27.53680581849" />
                  <Point X="3.632149208818" Y="-27.280833237312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.725402371346" Y="-25.947250882605" />
                  <Point X="3.756591512916" Y="-25.501225378183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.823569901708" Y="-24.543389793658" />
                  <Point X="3.86158987715" Y="-23.999678813767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.955573276454" Y="-22.655653586649" />
                  <Point X="3.982055787917" Y="-22.276936028584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.522713412045" Y="-27.483957275999" />
                  <Point X="3.541766857524" Y="-27.21148031117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.631039093033" Y="-25.934827865161" />
                  <Point X="3.663292741587" Y="-25.473579201639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726519482371" Y="-24.569394683174" />
                  <Point X="3.765473049208" Y="-24.012332724336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854941758702" Y="-22.732870569444" />
                  <Point X="3.888788003712" Y="-22.248846715515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.43117696182" Y="-27.431108733507" />
                  <Point X="3.45138450623" Y="-27.142127385027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.536675807621" Y="-25.922404949246" />
                  <Point X="3.571543519095" Y="-25.423773444321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.628721730733" Y="-24.606086922529" />
                  <Point X="3.669356221266" Y="-24.024986634904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.75431024095" Y="-22.810087552239" />
                  <Point X="3.789549540636" Y="-22.306142088298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.339640511595" Y="-27.378260191016" />
                  <Point X="3.361002154937" Y="-27.072774458884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.442312522208" Y="-25.909982033332" />
                  <Point X="3.481056574642" Y="-25.355916270041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.529410259801" Y="-24.664426356299" />
                  <Point X="3.573239393323" Y="-24.037640545473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.653678723197" Y="-22.887304535034" />
                  <Point X="3.690311077561" Y="-22.36343746108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.248104061369" Y="-27.325411648525" />
                  <Point X="3.270619803643" Y="-27.003421532741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.346774907955" Y="-25.914352802232" />
                  <Point X="3.394074440263" Y="-25.237937976599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.426459033085" Y="-24.774816722799" />
                  <Point X="3.477254248117" Y="-24.048411305176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.553047205445" Y="-22.964521517829" />
                  <Point X="3.591072614485" Y="-22.420732833862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.156567611144" Y="-27.272563106033" />
                  <Point X="3.18023745235" Y="-26.934068606598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.250073200984" Y="-25.935370872597" />
                  <Point X="3.382618110271" Y="-24.039890360856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.452415687692" Y="-23.041738500624" />
                  <Point X="3.49183415141" Y="-22.478028206644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.065031160918" Y="-27.219714563542" />
                  <Point X="3.089855101056" Y="-26.864715680455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.15321679563" Y="-25.958601232891" />
                  <Point X="3.288825946819" Y="-24.019300020387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351784169698" Y="-23.118955486874" />
                  <Point X="3.392595688334" Y="-22.535323579427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.973494719416" Y="-27.166865896305" />
                  <Point X="3.000505052677" Y="-26.780600134862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.053755227231" Y="-26.019087160445" />
                  <Point X="3.196912513021" Y="-23.971842594145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251152588349" Y="-23.196173379145" />
                  <Point X="3.293357225259" Y="-22.592618952209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.881958278358" Y="-27.114017222712" />
                  <Point X="2.914584669516" Y="-26.647438091607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.950022238425" Y="-26.140657245687" />
                  <Point X="3.107226408943" Y="-23.892532868949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.150241188186" Y="-23.277392866882" />
                  <Point X="3.194118762183" Y="-22.649914324991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658288237629" Y="-28.950767059321" />
                  <Point X="2.661871973824" Y="-28.899517244032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.789704318578" Y="-27.071429544904" />
                  <Point X="3.022989300117" Y="-23.735298881211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.044925780178" Y="-23.421592600999" />
                  <Point X="3.094880299108" Y="-22.707209697773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.571009941374" Y="-28.83702407802" />
                  <Point X="2.57692811667" Y="-28.75239022827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69577949659" Y="-27.052736309884" />
                  <Point X="2.995641836032" Y="-22.764505070556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.48373164512" Y="-28.72328109672" />
                  <Point X="2.491984259515" Y="-28.605263212509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601735137651" Y="-27.035752532901" />
                  <Point X="2.896403372957" Y="-22.821800443338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.396453348865" Y="-28.609538115419" />
                  <Point X="2.40704040236" Y="-28.458136196747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.507222898204" Y="-27.025459758919" />
                  <Point X="2.797164909881" Y="-22.87909581612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.309175052611" Y="-28.495795134119" />
                  <Point X="2.322096545205" Y="-28.311009180985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.410491329858" Y="-27.046904866838" />
                  <Point X="2.697926446806" Y="-22.936391188902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.221896756356" Y="-28.382052152818" />
                  <Point X="2.237152688051" Y="-28.163882165223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.311693819413" Y="-27.097894323209" />
                  <Point X="2.599023904407" Y="-22.988882672208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.719315820755" Y="-21.268628123135" />
                  <Point X="2.735340954565" Y="-21.039458032786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.134618460102" Y="-28.268309171518" />
                  <Point X="2.152208830896" Y="-28.016755149461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212762731465" Y="-27.150794026882" />
                  <Point X="2.50189652854" Y="-23.015988091379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.610960151603" Y="-21.456305617202" />
                  <Point X="2.644620427804" Y="-20.974941241147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.047340163848" Y="-28.154566190217" />
                  <Point X="2.067306094583" Y="-27.86904007826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.109388212111" Y="-27.267237760127" />
                  <Point X="2.406110199056" Y="-23.023915653799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.502604482452" Y="-21.64398311127" />
                  <Point X="2.553247415367" Y="-20.919755429384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.96006188801" Y="-28.040822916941" />
                  <Point X="2.311846268925" Y="-23.010071891156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.3942488133" Y="-21.831660605337" />
                  <Point X="2.461743216991" Y="-20.866445663963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.872783641623" Y="-27.927079222506" />
                  <Point X="2.219567572779" Y="-22.967837959847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.285893144149" Y="-22.019338099404" />
                  <Point X="2.370239018615" Y="-20.813135898542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.784411114041" Y="-27.828984478229" />
                  <Point X="2.12920751618" Y="-22.898166204722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.177537465821" Y="-22.207015724707" />
                  <Point X="2.278734786171" Y="-20.759826620309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.693356139104" Y="-27.76925051832" />
                  <Point X="2.042892760066" Y="-22.770643957449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.069181755049" Y="-22.394693813975" />
                  <Point X="2.186738679227" Y="-20.713551475143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.602221217376" Y="-27.710659850806" />
                  <Point X="2.09452241236" Y="-20.670424763555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.510425432825" Y="-27.661519961948" />
                  <Point X="2.002306145493" Y="-20.627298051967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.416247294156" Y="-27.64644932424" />
                  <Point X="1.910089874394" Y="-20.584171400899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.320444494915" Y="-27.65461241516" />
                  <Point X="1.81758454368" Y="-20.545178494921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.22459581393" Y="-27.663431645576" />
                  <Point X="1.724708200841" Y="-20.511491309517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.128281371973" Y="-27.67891156822" />
                  <Point X="1.631831858002" Y="-20.477804124113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.029152303462" Y="-27.734642525846" />
                  <Point X="1.538955515163" Y="-20.444116938709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.928041488619" Y="-27.818713776365" />
                  <Point X="1.446016382107" Y="-20.411327695239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758911952843" Y="-28.875498054159" />
                  <Point X="0.772001147549" Y="-28.688313849111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826057913306" Y="-27.915266083101" />
                  <Point X="1.352365475258" Y="-20.388717291234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.683389188219" Y="-28.593643138344" />
                  <Point X="1.258714568409" Y="-20.366106887229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.602571506233" Y="-28.387509068574" />
                  <Point X="1.16506366156" Y="-20.343496483224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.516056065986" Y="-28.262856738112" />
                  <Point X="1.071412754712" Y="-20.320886079219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.428928017037" Y="-28.146965120245" />
                  <Point X="0.977761847863" Y="-20.298275675214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.337830733937" Y="-28.087836195268" />
                  <Point X="0.884110931755" Y="-20.275665403614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.244667504231" Y="-28.058251683189" />
                  <Point X="0.790105046539" Y="-20.258131426763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.151458417657" Y="-28.029322954898" />
                  <Point X="0.695565412368" Y="-20.248230415594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.058115255238" Y="-28.002311600511" />
                  <Point X="0.601025778196" Y="-20.238329404425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.037023640565" Y="-28.000980429928" />
                  <Point X="0.506486144024" Y="-20.228428393256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.134128522902" Y="-28.027764176642" />
                  <Point X="0.411946509853" Y="-20.218527382088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.231473087254" Y="-28.057975535852" />
                  <Point X="0.295340122312" Y="-20.524195646219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.328888113531" Y="-28.089194547539" />
                  <Point X="0.177956364909" Y="-20.840980817305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.429052076426" Y="-28.159725184365" />
                  <Point X="0.078438717804" Y="-20.902268707714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.534604961265" Y="-28.307320995385" />
                  <Point X="-0.017580219125" Y="-20.913522711576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.64239238843" Y="-28.486872250454" />
                  <Point X="-0.110721355701" Y="-20.88362225304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.771226591475" Y="-28.96740642316" />
                  <Point X="-0.199768892193" Y="-20.795180585895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900087523876" Y="-29.44832284346" />
                  <Point X="-0.277495583558" Y="-20.544843290864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.017159209482" Y="-29.760645179937" />
                  <Point X="-0.353018333525" Y="-20.26298816544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.110887589407" Y="-29.73914269254" />
                  <Point X="-0.445724189666" Y="-20.226862906664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.172094273175" Y="-29.252558282297" />
                  <Point X="-0.541741967478" Y="-20.238100334366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.254328378234" Y="-29.066680006178" />
                  <Point X="-0.637759745289" Y="-20.249337762068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.343988998564" Y="-28.987005846398" />
                  <Point X="-0.7337775231" Y="-20.26057518977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.433718375558" Y="-28.90831495272" />
                  <Point X="-0.829795310709" Y="-20.271812757578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.523846546728" Y="-28.835327081459" />
                  <Point X="-0.925813284204" Y="-20.283052983687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.616579189009" Y="-28.79958488233" />
                  <Point X="-1.022887248752" Y="-20.309394585414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.71123135101" Y="-28.791293094096" />
                  <Point X="-1.120023567116" Y="-20.336627888259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.806028849702" Y="-28.785079717368" />
                  <Point X="-1.21715988548" Y="-20.363861191103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.900826348393" Y="-28.778866340639" />
                  <Point X="-1.314296203845" Y="-20.391094493947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.996122500764" Y="-28.779784043744" />
                  <Point X="-1.411432522209" Y="-20.418327796791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.093810538524" Y="-28.814907301539" />
                  <Point X="-1.534980987008" Y="-20.823272390915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.19369675101" Y="-28.881465922459" />
                  <Point X="-1.636241272239" Y="-20.909481167583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.293596392958" Y="-28.948216593632" />
                  <Point X="-1.734325339468" Y="-20.950267910637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.394128801286" Y="-29.024016245623" />
                  <Point X="-1.831012852971" Y="-20.971083004938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.498626030259" Y="-29.156515474435" />
                  <Point X="-2.406314675301" Y="-27.836401595475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.378336062356" Y="-27.436288789422" />
                  <Point X="-1.924935902252" Y="-20.952364419027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.590495469569" Y="-29.108428897701" />
                  <Point X="-2.514670392876" Y="-28.024079782022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.468955606894" Y="-27.370327884703" />
                  <Point X="-2.016949382911" Y="-20.906337729559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.681775349469" Y="-29.051911228625" />
                  <Point X="-2.623026066704" Y="-28.21175734298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.562657649207" Y="-27.348448751898" />
                  <Point X="-2.107570053514" Y="-20.840392928323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.772800435541" Y="-28.991749838032" />
                  <Point X="-2.731381740533" Y="-28.399434903937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.659202769606" Y="-27.367227529956" />
                  <Point X="-2.19530987053" Y="-20.733250001404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.86316691698" Y="-28.922169962402" />
                  <Point X="-2.839737414362" Y="-28.587112464894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.758137656203" Y="-27.420181556833" />
                  <Point X="-2.294391180226" Y="-20.788297976165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953533483756" Y="-28.852591307133" />
                  <Point X="-2.948093088191" Y="-28.774790025852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.857376111915" Y="-27.477476824318" />
                  <Point X="-2.393472489923" Y="-20.843345950926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956614567628" Y="-27.534772091802" />
                  <Point X="-2.49255379962" Y="-20.898393925687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.05585302334" Y="-27.592067359286" />
                  <Point X="-2.978536558694" Y="-26.486390402233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963513625515" Y="-26.27155244865" />
                  <Point X="-2.591635106988" Y="-20.953441867159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.155091479052" Y="-27.649362626771" />
                  <Point X="-3.081687749766" Y="-26.599640392257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.050720109196" Y="-26.156782499703" />
                  <Point X="-2.764430155053" Y="-22.062645412857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.753325366851" Y="-21.903839542918" />
                  <Point X="-2.691376830333" Y="-21.01793419709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.254329934765" Y="-27.706657894255" />
                  <Point X="-3.182319281074" Y="-26.676857568901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.142744558743" Y="-26.110912672636" />
                  <Point X="-2.869064685894" Y="-22.197108149843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.833676013259" Y="-21.691026553231" />
                  <Point X="-2.79200363569" Y="-21.095083789492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.353568390477" Y="-27.763953161739" />
                  <Point X="-3.282950814268" Y="-26.754074772516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.237203261654" Y="-26.099854290523" />
                  <Point X="-2.968906661543" Y="-22.263034154526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.918619872824" Y="-21.543899571927" />
                  <Point X="-2.892630441047" Y="-21.172233381894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.452806846189" Y="-27.821248429224" />
                  <Point X="-3.383582352107" Y="-26.831292042568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333278092922" Y="-26.111907620669" />
                  <Point X="-3.065078377475" Y="-22.276472999918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.003563732388" Y="-21.396772590623" />
                  <Point X="-2.993257246404" Y="-21.249382974297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.552045301902" Y="-27.878543696708" />
                  <Point X="-3.484213889947" Y="-26.908509312621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.42939492603" Y="-26.124561605102" />
                  <Point X="-3.366305503386" Y="-25.222340827541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.341795646865" Y="-24.871833549446" />
                  <Point X="-3.159718159585" Y="-22.268004170989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.651283757614" Y="-27.935838964193" />
                  <Point X="-3.584845427786" Y="-26.985726582673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.525511759137" Y="-26.137215589536" />
                  <Point X="-3.466660405596" Y="-25.295602023795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.431079313882" Y="-24.786768706132" />
                  <Point X="-3.252869682975" Y="-22.2382522508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.750522213326" Y="-27.993134231677" />
                  <Point X="-3.685476965626" Y="-27.062943852725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.621628592245" Y="-26.149869573969" />
                  <Point X="-3.564096875305" Y="-25.327127690834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.523482527714" Y="-24.746315460712" />
                  <Point X="-3.441643212281" Y="-23.575958724029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.430347944385" Y="-23.414428867565" />
                  <Point X="-3.344412189882" Y="-22.18549032289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842896904385" Y="-27.95227309158" />
                  <Point X="-3.786108503465" Y="-27.140161122778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.717745425352" Y="-26.162523558402" />
                  <Point X="-3.661147265586" Y="-25.353132164836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.616962986733" Y="-24.721267539177" />
                  <Point X="-3.547843804615" Y="-23.732817183763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514668205572" Y="-23.258384013984" />
                  <Point X="-3.435948633331" Y="-22.132641683503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.930116031938" Y="-27.837683958436" />
                  <Point X="-3.886740041305" Y="-27.21737839283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.81386225846" Y="-26.175177542836" />
                  <Point X="-3.758197658682" Y="-25.379136679099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710443445752" Y="-24.696219617641" />
                  <Point X="-3.646121285089" Y="-23.776369865083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604678703882" Y="-23.18371334243" />
                  <Point X="-3.527485082514" Y="-22.079793126102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.017032473097" Y="-27.718766208175" />
                  <Point X="-3.987371579144" Y="-27.294595662882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.909979091567" Y="-26.187831527269" />
                  <Point X="-3.855248066586" Y="-25.405141405127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803923904771" Y="-24.671171696106" />
                  <Point X="-3.742690410654" Y="-23.795491933009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.695061050037" Y="-23.1143603428" />
                  <Point X="-3.619021531697" Y="-22.026944568701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.102269865899" Y="-27.575836947737" />
                  <Point X="-4.088003116983" Y="-27.371812932934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.006095924675" Y="-26.200485511702" />
                  <Point X="-3.95229847449" Y="-25.431146131155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.89740436379" Y="-24.64612377457" />
                  <Point X="-3.83730034562" Y="-23.786596270029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.785443396192" Y="-23.04500734317" />
                  <Point X="-3.71055798088" Y="-21.9740960113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.102212757782" Y="-26.213139496136" />
                  <Point X="-4.049348882395" Y="-25.457150857183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990884822809" Y="-24.621075853035" />
                  <Point X="-3.931663621361" Y="-23.7741732158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.875825742347" Y="-22.97565434354" />
                  <Point X="-3.812936672249" Y="-22.076298740873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.19832959089" Y="-26.225793480569" />
                  <Point X="-4.146399290299" Y="-25.483155583211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084365281828" Y="-24.596027931499" />
                  <Point X="-4.026026891944" Y="-23.761750087819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.966208088502" Y="-22.906301343911" />
                  <Point X="-3.91757352396" Y="-22.210794667864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.294446423997" Y="-26.238447465003" />
                  <Point X="-4.243449698203" Y="-25.509160309239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.177845740847" Y="-24.570980009964" />
                  <Point X="-4.120390161597" Y="-23.749326946524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056590434657" Y="-22.836948344281" />
                  <Point X="-4.022887347779" Y="-22.354971747017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.390563257105" Y="-26.251101449436" />
                  <Point X="-4.340500106108" Y="-25.535165035267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.271326199866" Y="-24.545932088429" />
                  <Point X="-4.214753431249" Y="-23.736903805228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146972780811" Y="-22.767595344651" />
                  <Point X="-4.131081037455" Y="-22.540332826681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.486680090212" Y="-26.263755433869" />
                  <Point X="-4.437550514012" Y="-25.561169761295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.364806658885" Y="-24.520884166893" />
                  <Point X="-4.309116700902" Y="-23.724480663933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.58279692332" Y="-26.276409418303" />
                  <Point X="-4.534600921916" Y="-25.587174487323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.458287117904" Y="-24.495836245358" />
                  <Point X="-4.403479970555" Y="-23.712057522637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.674916489703" Y="-26.231899825378" />
                  <Point X="-4.631651329821" Y="-25.613179213351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.551767576923" Y="-24.470788323822" />
                  <Point X="-4.497843240207" Y="-23.699634381342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.748392232964" Y="-25.920771140228" />
                  <Point X="-4.728701737725" Y="-25.639183939379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.645248035941" Y="-24.445740402287" />
                  <Point X="-4.59220650986" Y="-23.687211240047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.73872849496" Y="-24.420692480752" />
                  <Point X="-4.70565527697" Y="-23.947723428229" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.534823059082" Y="-28.773291015625" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.438452972412" Y="-28.484275390625" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.274335906982" Y="-28.2663984375" />
                  <Point X="0.234310150146" Y="-28.2539765625" />
                  <Point X="0.02097677803" Y="-28.187765625" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.04869020462" Y="-28.200189453125" />
                  <Point X="-0.2620234375" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.314144866943" Y="-28.32291015625" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.796524353027" Y="-29.795923828125" />
                  <Point X="-0.84774395752" Y="-29.987076171875" />
                  <Point X="-0.849336730957" Y="-29.986767578125" />
                  <Point X="-1.100259155273" Y="-29.9380625" />
                  <Point X="-1.145501220703" Y="-29.926423828125" />
                  <Point X="-1.35158972168" Y="-29.8733984375" />
                  <Point X="-1.316768188477" Y="-29.608904296875" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.319245361328" Y="-29.4866875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.423133178711" Y="-29.1703125" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240722656" Y="-28.985763671875" />
                  <Point X="-1.698149780273" Y="-28.98255859375" />
                  <Point X="-1.958829833984" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.030632080078" Y="-29.001021484375" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.449100341797" Y="-29.40408203125" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.488050048828" Y="-29.395333984375" />
                  <Point X="-2.855837646484" Y="-29.167609375" />
                  <Point X="-2.918500732422" Y="-29.119359375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.623639892578" Y="-27.8328203125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.500251464844" Y="-27.613669921875" />
                  <Point X="-2.504396240234" Y="-27.582189453125" />
                  <Point X="-2.516720947266" Y="-27.5660234375" />
                  <Point X="-2.531328857422" Y="-27.551416015625" />
                  <Point X="-2.560157958984" Y="-27.53719921875" />
                  <Point X="-2.591683837891" Y="-27.543470703125" />
                  <Point X="-3.677383544922" Y="-28.170298828125" />
                  <Point X="-3.842959472656" Y="-28.26589453125" />
                  <Point X="-3.871134521484" Y="-28.22887890625" />
                  <Point X="-4.161703613281" Y="-27.84712890625" />
                  <Point X="-4.206629882812" Y="-27.771794921875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.369559326172" Y="-26.581041015625" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535644531" Y="-26.411083984375" />
                  <Point X="-3.144604736328" Y="-26.391314453125" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.136651611328" Y="-26.350052734375" />
                  <Point X="-3.148654785156" Y="-26.321072265625" />
                  <Point X="-3.165344238281" Y="-26.30817578125" />
                  <Point X="-3.187643066406" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.590121582031" Y="-26.469013671875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.813749511719" Y="-26.4561015625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.939279296875" Y="-25.9280859375" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.791869628906" Y="-25.191455078125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.537511474609" Y="-25.1183828125" />
                  <Point X="-3.514143066406" Y="-25.1021640625" />
                  <Point X="-3.502324462891" Y="-25.090646484375" />
                  <Point X="-3.4934375" Y="-25.07119921875" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.487109130859" Y="-25.011751953125" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.50232421875" Y="-24.9719140625" />
                  <Point X="-3.518527587891" Y="-24.957353515625" />
                  <Point X="-3.541895996094" Y="-24.941134765625" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.806823730469" Y="-24.599150390625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.990887207031" Y="-24.49854296875" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.893717285156" Y="-23.91528125" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.921294189453" Y="-23.5838984375" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731709716797" Y="-23.604134765625" />
                  <Point X="-3.722" Y="-23.60107421875" />
                  <Point X="-3.670278076172" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.635226318359" Y="-23.546814453125" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.621014404297" Y="-23.445462890625" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968017578" Y="-23.380779296875" />
                  <Point X="-4.376604492188" Y="-22.830884765625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.444605957031" Y="-22.700568359375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.096631835938" Y="-22.131525390625" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.262355224609" Y="-22.013474609375" />
                  <Point X="-3.159157226562" Y="-22.073056640625" />
                  <Point X="-3.138515136719" Y="-22.07956640625" />
                  <Point X="-3.125" Y="-22.080748046875" />
                  <Point X="-3.052965820312" Y="-22.08705078125" />
                  <Point X="-3.031507324219" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-3.003659667969" Y="-22.063001953125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.939256591797" Y="-21.95864453125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.269630615234" Y="-21.315931640625" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.248544921875" Y="-21.205693359375" />
                  <Point X="-2.752873046875" Y="-20.825666015625" />
                  <Point X="-2.65305078125" Y="-20.77020703125" />
                  <Point X="-2.141549316406" Y="-20.48602734375" />
                  <Point X="-1.999418334961" Y="-20.6712578125" />
                  <Point X="-1.967826293945" Y="-20.712427734375" />
                  <Point X="-1.951246826172" Y="-20.726337890625" />
                  <Point X="-1.936204589844" Y="-20.734169921875" />
                  <Point X="-1.856030761719" Y="-20.77590625" />
                  <Point X="-1.835124023438" Y="-20.781509765625" />
                  <Point X="-1.81380859375" Y="-20.77775" />
                  <Point X="-1.798141113281" Y="-20.771259765625" />
                  <Point X="-1.714634765625" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.680983886719" Y="-20.689337890625" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.688020996094" Y="-20.307341796875" />
                  <Point X="-1.689137573242" Y="-20.298859375" />
                  <Point X="-1.609768066406" Y="-20.276607421875" />
                  <Point X="-0.968082763672" Y="-20.096703125" />
                  <Point X="-0.847077941895" Y="-20.0825390625" />
                  <Point X="-0.224200027466" Y="-20.009640625" />
                  <Point X="-0.072669212341" Y="-20.575162109375" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282129288" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594047546" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.217168014526" Y="-20.081833984375" />
                  <Point X="0.236648391724" Y="-20.0091328125" />
                  <Point X="0.299931152344" Y="-20.015759765625" />
                  <Point X="0.860210205078" Y="-20.074435546875" />
                  <Point X="0.960327636719" Y="-20.098607421875" />
                  <Point X="1.508456176758" Y="-20.230943359375" />
                  <Point X="1.573348999023" Y="-20.25448046875" />
                  <Point X="1.931044433594" Y="-20.38421875" />
                  <Point X="1.994051269531" Y="-20.413685546875" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.399583007812" Y="-20.610337890625" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.789939697266" Y="-20.845140625" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.365973632812" Y="-22.260634765625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.221460205078" Y="-22.521169921875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.2033671875" Y="-22.618642578125" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.22546875" Y="-22.709189453125" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.284940917969" Y="-22.782583984375" />
                  <Point X="2.338248046875" Y="-22.818755859375" />
                  <Point X="2.360337158203" Y="-22.827021484375" />
                  <Point X="2.371304931641" Y="-22.82834375" />
                  <Point X="2.429761962891" Y="-22.835392578125" />
                  <Point X="2.461348144531" Y="-22.830662109375" />
                  <Point X="2.528950927734" Y="-22.812583984375" />
                  <Point X="2.541033691406" Y="-22.80758203125" />
                  <Point X="3.797649658203" Y="-22.08207421875" />
                  <Point X="3.994247314453" Y="-21.9685703125" />
                  <Point X="4.005102783203" Y="-21.98365625" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.234600585938" Y="-22.311015625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.467831054688" Y="-23.269400390625" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.270242919922" Y="-23.428076171875" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.209718994141" Y="-23.520658203125" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.193570800781" Y="-23.6225625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.223238525391" Y="-23.723583984375" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280947509766" Y="-23.8011796875" />
                  <Point X="3.291949462891" Y="-23.807373046875" />
                  <Point X="3.350589599609" Y="-23.8403828125" />
                  <Point X="3.383440917969" Y="-23.848345703125" />
                  <Point X="3.462726074219" Y="-23.85882421875" />
                  <Point X="3.475803955078" Y="-23.858828125" />
                  <Point X="4.669504882812" Y="-23.70167578125" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.854368164063" Y="-23.70019921875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.949275390625" Y="-24.113400390625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="3.946551757812" Y="-24.707140625" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.714472900391" Y="-24.77562890625" />
                  <Point X="3.636577636719" Y="-24.82065234375" />
                  <Point X="3.61349609375" Y="-24.84424609375" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.554062011719" Y="-24.94052734375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.541406005859" Y="-25.055947265625" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.575527832031" Y="-25.16993359375" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.65119140625" Y="-25.25035546875" />
                  <Point X="3.729086669922" Y="-25.295380859375" />
                  <Point X="3.74116796875" Y="-25.300388671875" />
                  <Point X="4.835846191406" Y="-25.59370703125" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.995791503906" Y="-25.6522734375" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.935508789062" Y="-26.023029296875" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.650169921875" Y="-26.128986328125" />
                  <Point X="3.411981933594" Y="-26.09762890625" />
                  <Point X="3.394836181641" Y="-26.098341796875" />
                  <Point X="3.366152587891" Y="-26.104576171875" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.168107910156" Y="-26.175548828125" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.061873046875" Y="-26.34107421875" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.072234375" Y="-26.5413125" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.184328613281" Y="-27.465044921875" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.337635742188" Y="-27.58611328125" />
                  <Point X="4.204134277344" Y="-27.802138671875" />
                  <Point X="4.177405273438" Y="-27.8401171875" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="2.965593261719" Y="-27.3816953125" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.703202636719" Y="-27.2471484375" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.460717529297" Y="-27.234171875" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.273674072266" Y="-27.36304296875" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.195328369141" Y="-27.580513671875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.886888916016" Y="-28.9092578125" />
                  <Point X="2.986673828125" Y="-29.08208984375" />
                  <Point X="2.835297851563" Y="-29.190212890625" />
                  <Point X="2.805418212891" Y="-29.209552734375" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="1.845058837891" Y="-28.203056640625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.636879760742" Y="-27.9588203125" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425805175781" Y="-27.83571875" />
                  <Point X="1.388982055664" Y="-27.839107421875" />
                  <Point X="1.192718383789" Y="-27.857166015625" />
                  <Point X="1.165332641602" Y="-27.868509765625" />
                  <Point X="1.136898803711" Y="-27.89215234375" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.95995526123" Y="-28.0851015625" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.098927124023" Y="-29.71591796875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="0.994368530273" Y="-29.9632421875" />
                  <Point X="0.966738586426" Y="-29.96826171875" />
                  <Point X="0.860200500488" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#132" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.030528837156" Y="4.469059650747" Z="0.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.4" />
                  <Point X="-0.859045964258" Y="4.998400702517" Z="0.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.4" />
                  <Point X="-1.629421755161" Y="4.802817165748" Z="0.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.4" />
                  <Point X="-1.743749236756" Y="4.717412958834" Z="0.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.4" />
                  <Point X="-1.73482565947" Y="4.356977565837" Z="0.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.4" />
                  <Point X="-1.823431524828" Y="4.306214334649" Z="0.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.4" />
                  <Point X="-1.919272926231" Y="4.341460754392" Z="0.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.4" />
                  <Point X="-1.965907245416" Y="4.390462892805" Z="0.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.4" />
                  <Point X="-2.683489695037" Y="4.304779848199" Z="0.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.4" />
                  <Point X="-3.285123066913" Y="3.865267279419" Z="0.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.4" />
                  <Point X="-3.319087842277" Y="3.690348398154" Z="0.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.4" />
                  <Point X="-2.995222443605" Y="3.068278669706" Z="0.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.4" />
                  <Point X="-3.045170430908" Y="3.003633136978" Z="0.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.4" />
                  <Point X="-3.126797795901" Y="3.000342255568" Z="0.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.4" />
                  <Point X="-3.243510950143" Y="3.061106106468" Z="0.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.4" />
                  <Point X="-4.1422512232" Y="2.93045837439" Z="0.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.4" />
                  <Point X="-4.49394420981" Y="2.355917798171" Z="0.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.4" />
                  <Point X="-4.413198389528" Y="2.16072827957" Z="0.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.4" />
                  <Point X="-3.671520745474" Y="1.562729755556" Z="0.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.4" />
                  <Point X="-3.687576091951" Y="1.50360061398" Z="0.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.4" />
                  <Point X="-3.743191945802" Y="1.477892230536" Z="0.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.4" />
                  <Point X="-3.920923908327" Y="1.496953825991" Z="0.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.4" />
                  <Point X="-4.94813293892" Y="1.12907711649" Z="0.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.4" />
                  <Point X="-5.046504362885" Y="0.540055446174" Z="0.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.4" />
                  <Point X="-4.825921179415" Y="0.38383415917" Z="0.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.4" />
                  <Point X="-3.553192267622" Y="0.032849980732" Z="0.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.4" />
                  <Point X="-3.541018236225" Y="0.004708911587" Z="0.4" />
                  <Point X="-3.539556741714" Y="0" Z="0.4" />
                  <Point X="-3.54734636491" Y="-0.025098039474" Z="0.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.4" />
                  <Point X="-3.572176327501" Y="-0.046026002402" Z="0.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.4" />
                  <Point X="-3.810966609357" Y="-0.111877897586" Z="0.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.4" />
                  <Point X="-4.994931317408" Y="-0.903883110755" Z="0.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.4" />
                  <Point X="-4.868350479233" Y="-1.437195074712" Z="0.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.4" />
                  <Point X="-4.589751741561" Y="-1.487305245705" Z="0.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.4" />
                  <Point X="-3.196860032493" Y="-1.319987443863" Z="0.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.4" />
                  <Point X="-3.199163643371" Y="-1.347497671794" Z="0.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.4" />
                  <Point X="-3.40615329321" Y="-1.510091949363" Z="0.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.4" />
                  <Point X="-4.255729323935" Y="-2.766124222808" Z="0.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.4" />
                  <Point X="-3.917076378774" Y="-3.227880864227" Z="0.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.4" />
                  <Point X="-3.658539159899" Y="-3.182319953662" Z="0.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.4" />
                  <Point X="-2.558231696556" Y="-2.570098772576" Z="0.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.4" />
                  <Point X="-2.673097059506" Y="-2.776539222593" Z="0.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.4" />
                  <Point X="-2.955160636368" Y="-4.127696763658" Z="0.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.4" />
                  <Point X="-2.520527631943" Y="-4.406564695894" Z="0.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.4" />
                  <Point X="-2.415588695114" Y="-4.403239213951" Z="0.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.4" />
                  <Point X="-2.009009591475" Y="-4.011315089263" Z="0.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.4" />
                  <Point X="-1.70757584091" Y="-4.001170266431" Z="0.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.4" />
                  <Point X="-1.462256605416" Y="-4.176623330081" Z="0.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.4" />
                  <Point X="-1.374440680063" Y="-4.46516012784" Z="0.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.4" />
                  <Point X="-1.372496427572" Y="-4.571095842946" Z="0.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.4" />
                  <Point X="-1.164116177721" Y="-4.943564378934" Z="0.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.4" />
                  <Point X="-0.865020114528" Y="-5.004571754218" Z="0.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.4" />
                  <Point X="-0.7543841194" Y="-4.777583976523" Z="0.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.4" />
                  <Point X="-0.279224833474" Y="-3.320140297226" Z="0.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.4" />
                  <Point X="-0.040025747755" Y="-3.216661883454" Z="0.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.4" />
                  <Point X="0.213333331606" Y="-3.270450161834" Z="0.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.4" />
                  <Point X="0.391221025958" Y="-3.481505458542" Z="0.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.4" />
                  <Point X="0.480370741001" Y="-3.754952053292" Z="0.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.4" />
                  <Point X="0.969519659796" Y="-4.986177060929" Z="0.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.4" />
                  <Point X="1.148767733789" Y="-4.947955403336" Z="0.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.4" />
                  <Point X="1.14234356295" Y="-4.678111169199" Z="0.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.4" />
                  <Point X="1.002658426557" Y="-3.064439993946" Z="0.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.4" />
                  <Point X="1.162709863562" Y="-2.899317284961" Z="0.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.4" />
                  <Point X="1.387407344699" Y="-2.857615085432" Z="0.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.4" />
                  <Point X="1.603684588297" Y="-2.969599015139" Z="0.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.4" />
                  <Point X="1.799235186664" Y="-3.202213030684" Z="0.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.4" />
                  <Point X="2.82643156981" Y="-4.220247442195" Z="0.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.4" />
                  <Point X="3.016616562522" Y="-4.086469021107" Z="0.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.4" />
                  <Point X="2.924034301514" Y="-3.852976421443" Z="0.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.4" />
                  <Point X="2.238376418643" Y="-2.540346217782" Z="0.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.4" />
                  <Point X="2.311764361318" Y="-2.355050505131" Z="0.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.4" />
                  <Point X="2.477847873242" Y="-2.247136948649" Z="0.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.4" />
                  <Point X="2.688160585731" Y="-2.265071590848" Z="0.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.4" />
                  <Point X="2.934437175664" Y="-2.39371519876" Z="0.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.4" />
                  <Point X="4.212137738877" Y="-2.837613507429" Z="0.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.4" />
                  <Point X="4.374012850382" Y="-2.58111729211" Z="0.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.4" />
                  <Point X="4.208610708218" Y="-2.394095976762" Z="0.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.4" />
                  <Point X="3.108136419019" Y="-1.482993325571" Z="0.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.4" />
                  <Point X="3.105506592394" Y="-1.314375849207" Z="0.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.4" />
                  <Point X="3.200398125633" Y="-1.176235696724" Z="0.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.4" />
                  <Point X="3.370616269366" Y="-1.122154921547" Z="0.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.4" />
                  <Point X="3.637487871427" Y="-1.147278468694" Z="0.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.4" />
                  <Point X="4.978099850241" Y="-1.002874077761" Z="0.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.4" />
                  <Point X="5.039077108073" Y="-0.628445224462" Z="0.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.4" />
                  <Point X="4.842630844309" Y="-0.514128748239" Z="0.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.4" />
                  <Point X="3.670057471473" Y="-0.175785798747" Z="0.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.4" />
                  <Point X="3.608705577778" Y="-0.107784111933" Z="0.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.4" />
                  <Point X="3.584357678071" Y="-0.015262562411" Z="0.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.4" />
                  <Point X="3.597013772353" Y="0.081347968827" Z="0.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.4" />
                  <Point X="3.646673860622" Y="0.156164626642" Z="0.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.4" />
                  <Point X="3.73333794288" Y="0.212363045495" Z="0.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.4" />
                  <Point X="3.953336972998" Y="0.275843184529" Z="0.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.4" />
                  <Point X="4.992524140223" Y="0.925570785492" Z="0.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.4" />
                  <Point X="4.896793203092" Y="1.342908179594" Z="0.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.4" />
                  <Point X="4.656822448593" Y="1.379177854064" Z="0.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.4" />
                  <Point X="3.38383617128" Y="1.232502593841" Z="0.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.4" />
                  <Point X="3.310320627854" Y="1.267477815238" Z="0.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.4" />
                  <Point X="3.258853143836" Y="1.335176672761" Z="0.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.4" />
                  <Point X="3.236383381685" Y="1.418820907933" Z="0.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.4" />
                  <Point X="3.251715625027" Y="1.497154826835" Z="0.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.4" />
                  <Point X="3.303769712976" Y="1.572786323576" Z="0.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.4" />
                  <Point X="3.492113198361" Y="1.722211632384" Z="0.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.4" />
                  <Point X="4.271222067695" Y="2.746151598906" Z="0.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.4" />
                  <Point X="4.039532322732" Y="3.076828126456" Z="0.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.4" />
                  <Point X="3.766493983716" Y="2.992506349036" Z="0.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.4" />
                  <Point X="2.442275305261" Y="2.248920947927" Z="0.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.4" />
                  <Point X="2.37113455072" Y="2.252578018927" Z="0.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.4" />
                  <Point X="2.306859603296" Y="2.290071625797" Z="0.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.4" />
                  <Point X="2.260686974438" Y="2.350165257086" Z="0.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.4" />
                  <Point X="2.246851683832" Y="2.418623893113" Z="0.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.4" />
                  <Point X="2.26360699473" Y="2.497194270621" Z="0.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.4" />
                  <Point X="2.403119003823" Y="2.745645098239" Z="0.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.4" />
                  <Point X="2.81276079731" Y="4.226887459588" Z="0.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.4" />
                  <Point X="2.418597040398" Y="4.464145982215" Z="0.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.4" />
                  <Point X="2.009076708665" Y="4.662886855183" Z="0.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.4" />
                  <Point X="1.584241568022" Y="4.823804817821" Z="0.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.4" />
                  <Point X="0.965906055851" Y="4.981276723544" Z="0.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.4" />
                  <Point X="0.298983015673" Y="5.065249392201" Z="0.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.4" />
                  <Point X="0.162715724397" Y="4.962387855204" Z="0.4" />
                  <Point X="0" Y="4.355124473572" Z="0.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>