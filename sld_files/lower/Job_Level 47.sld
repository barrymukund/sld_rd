<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#209" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3352" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475585938" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.926233398438" Y="-29.867001953125" />
                  <Point X="0.563301940918" Y="-28.5125234375" />
                  <Point X="0.557721679688" Y="-28.497142578125" />
                  <Point X="0.542363220215" Y="-28.467375" />
                  <Point X="0.394023925781" Y="-28.253646484375" />
                  <Point X="0.378635406494" Y="-28.231474609375" />
                  <Point X="0.356752380371" Y="-28.209021484375" />
                  <Point X="0.330497192383" Y="-28.18977734375" />
                  <Point X="0.30249520874" Y="-28.17566796875" />
                  <Point X="0.072948799133" Y="-28.10442578125" />
                  <Point X="0.049136070251" Y="-28.09703515625" />
                  <Point X="0.020976823807" Y="-28.092765625" />
                  <Point X="-0.00866480732" Y="-28.092765625" />
                  <Point X="-0.036824054718" Y="-28.09703515625" />
                  <Point X="-0.266370452881" Y="-28.16827734375" />
                  <Point X="-0.290183197021" Y="-28.17566796875" />
                  <Point X="-0.318185028076" Y="-28.18977734375" />
                  <Point X="-0.344440216064" Y="-28.209021484375" />
                  <Point X="-0.366323547363" Y="-28.231474609375" />
                  <Point X="-0.514662841797" Y="-28.445203125" />
                  <Point X="-0.530051208496" Y="-28.467375" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.588640014648" Y="-28.65303515625" />
                  <Point X="-0.916584716797" Y="-29.87694140625" />
                  <Point X="-1.055444458008" Y="-29.84998828125" />
                  <Point X="-1.079326660156" Y="-29.8453515625" />
                  <Point X="-1.246417602539" Y="-29.802361328125" />
                  <Point X="-1.214962890625" Y="-29.563439453125" />
                  <Point X="-1.214201049805" Y="-29.5479296875" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.271347290039" Y="-29.240533203125" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.32364465332" Y="-29.131205078125" />
                  <Point X="-1.534981933594" Y="-28.9458671875" />
                  <Point X="-1.556905639648" Y="-28.926640625" />
                  <Point X="-1.583190673828" Y="-28.910294921875" />
                  <Point X="-1.612889404297" Y="-28.897994140625" />
                  <Point X="-1.643028930664" Y="-28.890966796875" />
                  <Point X="-1.923520385742" Y="-28.87258203125" />
                  <Point X="-1.952618041992" Y="-28.87067578125" />
                  <Point X="-1.98342199707" Y="-28.8737109375" />
                  <Point X="-2.014469970703" Y="-28.88203125" />
                  <Point X="-2.042658325195" Y="-28.894802734375" />
                  <Point X="-2.276378662109" Y="-29.05096875" />
                  <Point X="-2.300624511719" Y="-29.067169921875" />
                  <Point X="-2.312787109375" Y="-29.076822265625" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.356240234375" Y="-29.127009765625" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.766692626953" Y="-29.111068359375" />
                  <Point X="-2.801725341797" Y="-29.089376953125" />
                  <Point X="-3.104721923828" Y="-28.856080078125" />
                  <Point X="-3.098228271484" Y="-28.84483203125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405575927734" Y="-27.585189453125" />
                  <Point X="-2.414560791016" Y="-27.555572265625" />
                  <Point X="-2.428778808594" Y="-27.5267421875" />
                  <Point X="-2.446808349609" Y="-27.501583984375" />
                  <Point X="-2.462522460938" Y="-27.48587109375" />
                  <Point X="-2.464145507812" Y="-27.484248046875" />
                  <Point X="-2.489304931641" Y="-27.466216796875" />
                  <Point X="-2.518134765625" Y="-27.451998046875" />
                  <Point X="-2.54775390625" Y="-27.443013671875" />
                  <Point X="-2.578689453125" Y="-27.444025390625" />
                  <Point X="-2.610217529297" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-2.760372070312" Y="-27.53116796875" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.0551953125" Y="-27.83020703125" />
                  <Point X="-4.082860107422" Y="-27.793861328125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-4.286052734375" Y="-27.40403515625" />
                  <Point X="-3.105954345703" Y="-26.498515625" />
                  <Point X="-3.084579833984" Y="-26.47559765625" />
                  <Point X="-3.066615966797" Y="-26.448470703125" />
                  <Point X="-3.053857177734" Y="-26.419837890625" />
                  <Point X="-3.046876708984" Y="-26.392884765625" />
                  <Point X="-3.046152587891" Y="-26.39008984375" />
                  <Point X="-3.043348144531" Y="-26.35966796875" />
                  <Point X="-3.045555419922" Y="-26.327994140625" />
                  <Point X="-3.052555664062" Y="-26.29824609375" />
                  <Point X="-3.068638671875" Y="-26.272259765625" />
                  <Point X="-3.089472900391" Y="-26.24830078125" />
                  <Point X="-3.112974853516" Y="-26.228765625" />
                  <Point X="-3.136968505859" Y="-26.21464453125" />
                  <Point X="-3.139457519531" Y="-26.2131796875" />
                  <Point X="-3.168722167969" Y="-26.201955078125" />
                  <Point X="-3.200608886719" Y="-26.195474609375" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-3.384917724609" Y="-26.214525390625" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.823256835938" Y="-26.035017578125" />
                  <Point X="-4.834076660156" Y="-25.99266015625" />
                  <Point X="-4.892423828125" Y="-25.584701171875" />
                  <Point X="-4.877185058594" Y="-25.5806171875" />
                  <Point X="-3.532875488281" Y="-25.22041015625" />
                  <Point X="-3.517486572266" Y="-25.214826171875" />
                  <Point X="-3.487728027344" Y="-25.19946875" />
                  <Point X="-3.462583496094" Y="-25.182017578125" />
                  <Point X="-3.459975097656" Y="-25.18020703125" />
                  <Point X="-3.437519042969" Y="-25.158322265625" />
                  <Point X="-3.41827734375" Y="-25.1320703125" />
                  <Point X="-3.404168701172" Y="-25.1040703125" />
                  <Point X="-3.395787109375" Y="-25.077064453125" />
                  <Point X="-3.394917724609" Y="-25.074263671875" />
                  <Point X="-3.3906484375" Y="-25.04610546875" />
                  <Point X="-3.390647949219" Y="-25.016462890625" />
                  <Point X="-3.394917236328" Y="-24.98830078125" />
                  <Point X="-3.403296386719" Y="-24.961302734375" />
                  <Point X="-3.404161376953" Y="-24.958515625" />
                  <Point X="-3.418268554688" Y="-24.9305078125" />
                  <Point X="-3.437513183594" Y="-24.90424609375" />
                  <Point X="-3.459975097656" Y="-24.882353515625" />
                  <Point X="-3.485119628906" Y="-24.86490234375" />
                  <Point X="-3.497009765625" Y="-24.857873046875" />
                  <Point X="-3.515187988281" Y="-24.84883984375" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-3.672332763672" Y="-24.804783203125" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.831460449219" Y="-24.07014453125" />
                  <Point X="-4.824487792969" Y="-24.0230234375" />
                  <Point X="-4.703551757812" Y="-23.576734375" />
                  <Point X="-3.765668457031" Y="-23.70020703125" />
                  <Point X="-3.744984863281" Y="-23.700658203125" />
                  <Point X="-3.723423095703" Y="-23.698771484375" />
                  <Point X="-3.703137939453" Y="-23.694736328125" />
                  <Point X="-3.647485107422" Y="-23.677189453125" />
                  <Point X="-3.641711914063" Y="-23.675369140625" />
                  <Point X="-3.622780761719" Y="-23.6670390625" />
                  <Point X="-3.604038085938" Y="-23.65621875" />
                  <Point X="-3.587356445312" Y="-23.64398828125" />
                  <Point X="-3.573717529297" Y="-23.6284375" />
                  <Point X="-3.561303222656" Y="-23.610708984375" />
                  <Point X="-3.551352539062" Y="-23.5925703125" />
                  <Point X="-3.529021728516" Y="-23.538658203125" />
                  <Point X="-3.526705078125" Y="-23.53306640625" />
                  <Point X="-3.52091796875" Y="-23.51321484375" />
                  <Point X="-3.517158203125" Y="-23.4918984375" />
                  <Point X="-3.515804443359" Y="-23.47125390625" />
                  <Point X="-3.518951416016" Y="-23.45080859375" />
                  <Point X="-3.524553955078" Y="-23.429900390625" />
                  <Point X="-3.53205078125" Y="-23.41062109375" />
                  <Point X="-3.558995361328" Y="-23.358861328125" />
                  <Point X="-3.561790527344" Y="-23.3534921875" />
                  <Point X="-3.57328125" Y="-23.336294921875" />
                  <Point X="-3.587193115234" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-3.682128417969" Y="-23.244029296875" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.108245117188" Y="-22.31275390625" />
                  <Point X="-4.08115625" Y="-22.266345703125" />
                  <Point X="-3.750504638672" Y="-21.841337890625" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.1877265625" Y="-22.163658203125" />
                  <Point X="-3.167082275391" Y="-22.17016796875" />
                  <Point X="-3.146792724609" Y="-22.174205078125" />
                  <Point X="-3.069283935547" Y="-22.180986328125" />
                  <Point X="-3.061243408203" Y="-22.181689453125" />
                  <Point X="-3.040557861328" Y="-22.18123828125" />
                  <Point X="-3.019100585938" Y="-22.178412109375" />
                  <Point X="-2.999010742188" Y="-22.173494140625" />
                  <Point X="-2.9804609375" Y="-22.164345703125" />
                  <Point X="-2.962208007812" Y="-22.152716796875" />
                  <Point X="-2.946078369141" Y="-22.139771484375" />
                  <Point X="-2.891062011719" Y="-22.084755859375" />
                  <Point X="-2.885354736328" Y="-22.079048828125" />
                  <Point X="-2.872409179688" Y="-22.062919921875" />
                  <Point X="-2.860779296875" Y="-22.044666015625" />
                  <Point X="-2.851629638672" Y="-22.02611328125" />
                  <Point X="-2.846712646484" Y="-22.00601953125" />
                  <Point X="-2.843887207031" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.850217041016" Y="-21.88637109375" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795654297" Y="-21.81846484375" />
                  <Point X="-2.905242919922" Y="-21.7570703125" />
                  <Point X="-3.183332275391" Y="-21.275404296875" />
                  <Point X="-2.7478046875" Y="-20.941490234375" />
                  <Point X="-2.700620605469" Y="-20.905314453125" />
                  <Point X="-2.167035644531" Y="-20.608865234375" />
                  <Point X="-2.04319519043" Y="-20.7702578125" />
                  <Point X="-2.028891479492" Y="-20.78519921875" />
                  <Point X="-2.012311767578" Y="-20.799111328125" />
                  <Point X="-1.995113891602" Y="-20.810603515625" />
                  <Point X="-1.908864135742" Y="-20.85550390625" />
                  <Point X="-1.899915527344" Y="-20.8601640625" />
                  <Point X="-1.880635009766" Y="-20.8676640625" />
                  <Point X="-1.859725219727" Y="-20.87326953125" />
                  <Point X="-1.839274536133" Y="-20.876419921875" />
                  <Point X="-1.818627197266" Y="-20.87506640625" />
                  <Point X="-1.797307983398" Y="-20.871306640625" />
                  <Point X="-1.777452026367" Y="-20.865517578125" />
                  <Point X="-1.687599243164" Y="-20.828298828125" />
                  <Point X="-1.678278076172" Y="-20.8244375" />
                  <Point X="-1.660137573242" Y="-20.814484375" />
                  <Point X="-1.642408691406" Y="-20.802068359375" />
                  <Point X="-1.626861450195" Y="-20.78843359375" />
                  <Point X="-1.614633178711" Y="-20.771755859375" />
                  <Point X="-1.603811645508" Y="-20.753013671875" />
                  <Point X="-1.595479980469" Y="-20.734078125" />
                  <Point X="-1.566234619141" Y="-20.64132421875" />
                  <Point X="-1.563200683594" Y="-20.631701171875" />
                  <Point X="-1.559165161133" Y="-20.6114140625" />
                  <Point X="-1.557278930664" Y="-20.589853515625" />
                  <Point X="-1.55773046875" Y="-20.569173828125" />
                  <Point X="-1.561760498047" Y="-20.538564453125" />
                  <Point X="-1.584202148438" Y="-20.368103515625" />
                  <Point X="-1.010712097168" Y="-20.20731640625" />
                  <Point X="-0.949625427246" Y="-20.19019140625" />
                  <Point X="-0.294711120605" Y="-20.11354296875" />
                  <Point X="-0.133903274536" Y="-20.713685546875" />
                  <Point X="-0.121129814148" Y="-20.741876953125" />
                  <Point X="-0.103271522522" Y="-20.768603515625" />
                  <Point X="-0.082113975525" Y="-20.791193359375" />
                  <Point X="-0.05481804657" Y="-20.805783203125" />
                  <Point X="-0.024379972458" Y="-20.816115234375" />
                  <Point X="0.006155915737" Y="-20.82115625" />
                  <Point X="0.036691837311" Y="-20.816115234375" />
                  <Point X="0.06712991333" Y="-20.805783203125" />
                  <Point X="0.094425926208" Y="-20.791193359375" />
                  <Point X="0.115583534241" Y="-20.768603515625" />
                  <Point X="0.133441680908" Y="-20.741876953125" />
                  <Point X="0.146215301514" Y="-20.713685546875" />
                  <Point X="0.164377929688" Y="-20.645900390625" />
                  <Point X="0.307419555664" Y="-20.112060546875" />
                  <Point X="0.790704162598" Y="-20.16267578125" />
                  <Point X="0.844030212402" Y="-20.168259765625" />
                  <Point X="1.427377807617" Y="-20.30909765625" />
                  <Point X="1.481040039062" Y="-20.3220546875" />
                  <Point X="1.8605859375" Y="-20.45971875" />
                  <Point X="1.894647460938" Y="-20.472072265625" />
                  <Point X="2.261794433594" Y="-20.643775390625" />
                  <Point X="2.294557128906" Y="-20.65909765625" />
                  <Point X="2.649278564453" Y="-20.8657578125" />
                  <Point X="2.680990234375" Y="-20.884232421875" />
                  <Point X="2.943260498047" Y="-21.070744140625" />
                  <Point X="2.928226074219" Y="-21.09678515625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142075683594" Y="-22.4600703125" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.113625" Y="-22.556685546875" />
                  <Point X="2.111155761719" Y="-22.569267578125" />
                  <Point X="2.107800048828" Y="-22.59571484375" />
                  <Point X="2.107727783203" Y="-22.619046875" />
                  <Point X="2.1153125" Y="-22.6819453125" />
                  <Point X="2.116099121094" Y="-22.688470703125" />
                  <Point X="2.121442871094" Y="-22.710396484375" />
                  <Point X="2.129711669922" Y="-22.732490234375" />
                  <Point X="2.140072998047" Y="-22.75253125" />
                  <Point X="2.178993164063" Y="-22.809890625" />
                  <Point X="2.187016845703" Y="-22.820130859375" />
                  <Point X="2.204352050781" Y="-22.839376953125" />
                  <Point X="2.221599121094" Y="-22.854408203125" />
                  <Point X="2.278957519531" Y="-22.893328125" />
                  <Point X="2.284891845703" Y="-22.89735546875" />
                  <Point X="2.304932617188" Y="-22.90771875" />
                  <Point X="2.327026367188" Y="-22.915990234375" />
                  <Point X="2.348962646484" Y="-22.921337890625" />
                  <Point X="2.411862304688" Y="-22.928921875" />
                  <Point X="2.425291503906" Y="-22.929583984375" />
                  <Point X="2.450719726563" Y="-22.929033203125" />
                  <Point X="2.473206054688" Y="-22.925830078125" />
                  <Point X="2.545946533203" Y="-22.906376953125" />
                  <Point X="2.553389404297" Y="-22.9040546875" />
                  <Point X="2.573020263672" Y="-22.89703515625" />
                  <Point X="2.588533691406" Y="-22.88985546875" />
                  <Point X="2.728800048828" Y="-22.808873046875" />
                  <Point X="3.967326416016" Y="-22.093810546875" />
                  <Point X="4.104468261719" Y="-22.28440625" />
                  <Point X="4.123275390625" Y="-22.31054296875" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="4.258575683594" Y="-22.542896484375" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221428710938" Y="-23.339755859375" />
                  <Point X="3.203975830078" Y="-23.358369140625" />
                  <Point X="3.151624511719" Y="-23.426666015625" />
                  <Point X="3.144783935547" Y="-23.43690234375" />
                  <Point X="3.130880859375" Y="-23.460943359375" />
                  <Point X="3.121629638672" Y="-23.482916015625" />
                  <Point X="3.102128662109" Y="-23.552646484375" />
                  <Point X="3.100105712891" Y="-23.559880859375" />
                  <Point X="3.096652099609" Y="-23.582181640625" />
                  <Point X="3.095836425781" Y="-23.60575" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.113747802734" Y="-23.70581640625" />
                  <Point X="3.117076416016" Y="-23.717875" />
                  <Point X="3.125935302734" Y="-23.743302734375" />
                  <Point X="3.136282958984" Y="-23.76426171875" />
                  <Point X="3.179823730469" Y="-23.83044140625" />
                  <Point X="3.184340576172" Y="-23.837306640625" />
                  <Point X="3.198897460938" Y="-23.8545546875" />
                  <Point X="3.216141113281" Y="-23.870642578125" />
                  <Point X="3.234346923828" Y="-23.88396484375" />
                  <Point X="3.297438964844" Y="-23.91948046875" />
                  <Point X="3.309143554688" Y="-23.9250546875" />
                  <Point X="3.333667480469" Y="-23.934740234375" />
                  <Point X="3.356118408203" Y="-23.9405625" />
                  <Point X="3.441429199219" Y="-23.9518359375" />
                  <Point X="3.448796875" Y="-23.952517578125" />
                  <Point X="3.470725830078" Y="-23.95369140625" />
                  <Point X="3.488203125" Y="-23.953015625" />
                  <Point X="3.621446777344" Y="-23.935474609375" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.837859863281" Y="-24.03401953125" />
                  <Point X="4.845937011719" Y="-24.0671953125" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.716580322266" Y="-24.67041015625" />
                  <Point X="3.704785644531" Y="-24.674416015625" />
                  <Point X="3.681548095703" Y="-24.684931640625" />
                  <Point X="3.597732910156" Y="-24.733376953125" />
                  <Point X="3.58787109375" Y="-24.7399296875" />
                  <Point X="3.564860595703" Y="-24.75737890625" />
                  <Point X="3.547528808594" Y="-24.77442578125" />
                  <Point X="3.497239746094" Y="-24.838505859375" />
                  <Point X="3.492022949219" Y="-24.845154296875" />
                  <Point X="3.480297119141" Y="-24.8644375" />
                  <Point X="3.470525634766" Y="-24.8858984375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.446917724609" Y="-24.994927734375" />
                  <Point X="3.44540625" Y="-25.006884765625" />
                  <Point X="3.443667236328" Y="-25.0347734375" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.461942138672" Y="-25.146083984375" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470525634766" Y="-25.176662109375" />
                  <Point X="3.480297119141" Y="-25.198123046875" />
                  <Point X="3.492022949219" Y="-25.21740625" />
                  <Point X="3.542312011719" Y="-25.281486328125" />
                  <Point X="3.550674560547" Y="-25.2908046875" />
                  <Point X="3.570206054688" Y="-25.309876953125" />
                  <Point X="3.589037109375" Y="-25.324158203125" />
                  <Point X="3.672852294922" Y="-25.372603515625" />
                  <Point X="3.679072021484" Y="-25.375896484375" />
                  <Point X="3.699847167969" Y="-25.385931640625" />
                  <Point X="3.716580322266" Y="-25.39215234375" />
                  <Point X="3.838770751953" Y="-25.424892578125" />
                  <Point X="4.89147265625" Y="-25.706962890625" />
                  <Point X="4.859532714844" Y="-25.91881640625" />
                  <Point X="4.855022949219" Y="-25.948728515625" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.790380371094" Y="-26.183279296875" />
                  <Point X="3.424380371094" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659423828" Y="-26.005509765625" />
                  <Point X="3.210159912109" Y="-26.041263671875" />
                  <Point X="3.193095214844" Y="-26.04497265625" />
                  <Point X="3.1639765625" Y="-26.056595703125" />
                  <Point X="3.136151123047" Y="-26.073486328125" />
                  <Point X="3.1123984375" Y="-26.093958984375" />
                  <Point X="3.012968994141" Y="-26.21354296875" />
                  <Point X="3.002654296875" Y="-26.225947265625" />
                  <Point X="2.987935302734" Y="-26.250326171875" />
                  <Point X="2.976590332031" Y="-26.277712890625" />
                  <Point X="2.969757324219" Y="-26.305365234375" />
                  <Point X="2.955506591797" Y="-26.46023046875" />
                  <Point X="2.954028320312" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.50756640625" />
                  <Point X="2.964078857422" Y="-26.5391875" />
                  <Point X="2.976450439453" Y="-26.567998046875" />
                  <Point X="3.067486816406" Y="-26.70959765625" />
                  <Point X="3.072163818359" Y="-26.71623046875" />
                  <Point X="3.093227783203" Y="-26.743548828125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.224021972656" Y="-26.847919921875" />
                  <Point X="4.213122558594" Y="-27.606884765625" />
                  <Point X="4.137520996094" Y="-27.72921875" />
                  <Point X="4.124816894531" Y="-27.74977734375" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="4.017562255859" Y="-27.879353515625" />
                  <Point X="2.800954833984" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224365234" Y="-27.159826171875" />
                  <Point X="2.558443847656" Y="-27.12446875" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.506784912109" Y="-27.120396484375" />
                  <Point X="2.474610107422" Y="-27.125353515625" />
                  <Point X="2.444831787109" Y="-27.135177734375" />
                  <Point X="2.282186279297" Y="-27.220775390625" />
                  <Point X="2.265313720703" Y="-27.22965625" />
                  <Point X="2.242377685547" Y="-27.2465546875" />
                  <Point X="2.221419677734" Y="-27.267515625" />
                  <Point X="2.204531738281" Y="-27.29044140625" />
                  <Point X="2.118932617188" Y="-27.4530859375" />
                  <Point X="2.110052734375" Y="-27.469958984375" />
                  <Point X="2.100228759766" Y="-27.499734375" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.131033203125" Y="-27.7590390625" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.224686035156" Y="-27.952287109375" />
                  <Point X="2.861283935547" Y="-29.05490625" />
                  <Point X="2.796935546875" Y="-29.100869140625" />
                  <Point X="2.781853759766" Y="-29.111642578125" />
                  <Point X="2.701765625" Y="-29.163482421875" />
                  <Point X="2.686930419922" Y="-29.1441484375" />
                  <Point X="1.758546630859" Y="-27.934255859375" />
                  <Point X="1.747503051758" Y="-27.9221796875" />
                  <Point X="1.721922485352" Y="-27.900556640625" />
                  <Point X="1.528829956055" Y="-27.776416015625" />
                  <Point X="1.508798950195" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.75116796875" />
                  <Point X="1.448367675781" Y="-27.7434375" />
                  <Point X="1.417100952148" Y="-27.741119140625" />
                  <Point X="1.205921630859" Y="-27.76055078125" />
                  <Point X="1.184014160156" Y="-27.76256640625" />
                  <Point X="1.156365112305" Y="-27.769396484375" />
                  <Point X="1.128977783203" Y="-27.780740234375" />
                  <Point X="1.104594238281" Y="-27.7954609375" />
                  <Point X="0.941526733398" Y="-27.931046875" />
                  <Point X="0.924610473633" Y="-27.94511328125" />
                  <Point X="0.904138183594" Y="-27.9688671875" />
                  <Point X="0.887247436523" Y="-27.996693359375" />
                  <Point X="0.875624389648" Y="-28.025810546875" />
                  <Point X="0.826868103027" Y="-28.250126953125" />
                  <Point X="0.821810302734" Y="-28.273396484375" />
                  <Point X="0.819724853516" Y="-28.289626953125" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="0.840392150879" Y="-28.479970703125" />
                  <Point X="1.022065490723" Y="-29.8599140625" />
                  <Point X="0.989949401855" Y="-29.866955078125" />
                  <Point X="0.975661376953" Y="-29.8700859375" />
                  <Point X="0.929315917969" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.037342529297" Y="-29.756728515625" />
                  <Point X="-1.058420654297" Y="-29.75263671875" />
                  <Point X="-1.141245849609" Y="-29.731326171875" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077270508" Y="-29.568099609375" />
                  <Point X="-1.119451660156" Y="-29.541033203125" />
                  <Point X="-1.121759033203" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.178172729492" Y="-29.222" />
                  <Point X="-1.183861572266" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230575317383" Y="-29.094859375" />
                  <Point X="-1.250209960938" Y="-29.070935546875" />
                  <Point X="-1.261006835938" Y="-29.05978125" />
                  <Point X="-1.472344116211" Y="-28.874443359375" />
                  <Point X="-1.494267822266" Y="-28.855216796875" />
                  <Point X="-1.506737792969" Y="-28.845966796875" />
                  <Point X="-1.533022827148" Y="-28.82962109375" />
                  <Point X="-1.546837768555" Y="-28.822525390625" />
                  <Point X="-1.576536499023" Y="-28.810224609375" />
                  <Point X="-1.591317749023" Y="-28.8054765625" />
                  <Point X="-1.621457275391" Y="-28.79844921875" />
                  <Point X="-1.636815673828" Y="-28.796169921875" />
                  <Point X="-1.917307006836" Y="-28.77778515625" />
                  <Point X="-1.946404663086" Y="-28.77587890625" />
                  <Point X="-1.96193347168" Y="-28.7761328125" />
                  <Point X="-1.992737426758" Y="-28.77916796875" />
                  <Point X="-2.008012817383" Y="-28.78194921875" />
                  <Point X="-2.039060668945" Y="-28.79026953125" />
                  <Point X="-2.05367578125" Y="-28.795498046875" />
                  <Point X="-2.081864257813" Y="-28.80826953125" />
                  <Point X="-2.095437255859" Y="-28.8158125" />
                  <Point X="-2.329157714844" Y="-28.971978515625" />
                  <Point X="-2.353403564453" Y="-28.9881796875" />
                  <Point X="-2.359680175781" Y="-28.992755859375" />
                  <Point X="-2.380446044922" Y="-29.010134765625" />
                  <Point X="-2.40276171875" Y="-29.032775390625" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.431608886719" Y="-29.069177734375" />
                  <Point X="-2.503200927734" Y="-29.162478515625" />
                  <Point X="-2.716681884766" Y="-29.030296875" />
                  <Point X="-2.747599121094" Y="-29.011154296875" />
                  <Point X="-2.980862548828" Y="-28.831548828125" />
                  <Point X="-2.341487548828" Y="-27.7241171875" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.619232421875" />
                  <Point X="-2.310626708984" Y="-27.588296875" />
                  <Point X="-2.314666992188" Y="-27.557611328125" />
                  <Point X="-2.323651855469" Y="-27.527994140625" />
                  <Point X="-2.329358642578" Y="-27.513552734375" />
                  <Point X="-2.343576660156" Y="-27.48472265625" />
                  <Point X="-2.351560546875" Y="-27.471404296875" />
                  <Point X="-2.369590087891" Y="-27.44624609375" />
                  <Point X="-2.379635742188" Y="-27.43440625" />
                  <Point X="-2.395349853516" Y="-27.418693359375" />
                  <Point X="-2.408805419922" Y="-27.40703125" />
                  <Point X="-2.43396484375" Y="-27.389" />
                  <Point X="-2.447284179688" Y="-27.381015625" />
                  <Point X="-2.476114013672" Y="-27.366796875" />
                  <Point X="-2.490559082031" Y="-27.361087890625" />
                  <Point X="-2.520178222656" Y="-27.352103515625" />
                  <Point X="-2.550859130859" Y="-27.348064453125" />
                  <Point X="-2.581794677734" Y="-27.349076171875" />
                  <Point X="-2.597223388672" Y="-27.3508515625" />
                  <Point X="-2.628751464844" Y="-27.357123046875" />
                  <Point X="-2.643681884766" Y="-27.36138671875" />
                  <Point X="-2.672648193359" Y="-27.3722890625" />
                  <Point X="-2.686684082031" Y="-27.378927734375" />
                  <Point X="-2.807872314453" Y="-27.448896484375" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-3.979601806641" Y="-27.77266796875" />
                  <Point X="-4.004020263672" Y="-27.740587890625" />
                  <Point X="-4.181265625" Y="-27.443373046875" />
                  <Point X="-3.048120849609" Y="-26.5738828125" />
                  <Point X="-3.03648046875" Y="-26.563310546875" />
                  <Point X="-3.015105957031" Y="-26.540392578125" />
                  <Point X="-3.005372802734" Y="-26.528048828125" />
                  <Point X="-2.987408935547" Y="-26.500921875" />
                  <Point X="-2.979841064453" Y="-26.48713671875" />
                  <Point X="-2.967082275391" Y="-26.45850390625" />
                  <Point X="-2.961891357422" Y="-26.44365625" />
                  <Point X="-2.954910888672" Y="-26.416703125" />
                  <Point X="-2.951553710938" Y="-26.398810546875" />
                  <Point X="-2.948749267578" Y="-26.368388671875" />
                  <Point X="-2.948577880859" Y="-26.353064453125" />
                  <Point X="-2.95078515625" Y="-26.321390625" />
                  <Point X="-2.953081298828" Y="-26.306232421875" />
                  <Point X="-2.960081542969" Y="-26.276484375" />
                  <Point X="-2.971775146484" Y="-26.24825" />
                  <Point X="-2.987858154297" Y="-26.222263671875" />
                  <Point X="-2.996951660156" Y="-26.209921875" />
                  <Point X="-3.017785888672" Y="-26.185962890625" />
                  <Point X="-3.028746826172" Y="-26.175244140625" />
                  <Point X="-3.052248779297" Y="-26.155708984375" />
                  <Point X="-3.064789794922" Y="-26.146892578125" />
                  <Point X="-3.088783447266" Y="-26.132771484375" />
                  <Point X="-3.105436523438" Y="-26.12448046875" />
                  <Point X="-3.134701171875" Y="-26.113255859375" />
                  <Point X="-3.149801757812" Y="-26.108857421875" />
                  <Point X="-3.181688476562" Y="-26.102376953125" />
                  <Point X="-3.197305175781" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244328613281" Y="-26.100197265625" />
                  <Point X="-3.397317382812" Y="-26.120337890625" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.731211914062" Y="-26.011505859375" />
                  <Point X="-4.740761230469" Y="-25.974123046875" />
                  <Point X="-4.786451660156" Y="-25.65465625" />
                  <Point X="-3.508283447266" Y="-25.312171875" />
                  <Point X="-3.500471435547" Y="-25.309712890625" />
                  <Point X="-3.473919677734" Y="-25.299248046875" />
                  <Point X="-3.444161132812" Y="-25.283890625" />
                  <Point X="-3.433562011719" Y="-25.277513671875" />
                  <Point X="-3.408417480469" Y="-25.2600625" />
                  <Point X="-3.393670898438" Y="-25.2482421875" />
                  <Point X="-3.37121484375" Y="-25.226357421875" />
                  <Point X="-3.360896972656" Y="-25.214482421875" />
                  <Point X="-3.341655273438" Y="-25.18823046875" />
                  <Point X="-3.333438720703" Y="-25.174818359375" />
                  <Point X="-3.319330078125" Y="-25.146818359375" />
                  <Point X="-3.313437988281" Y="-25.13223046875" />
                  <Point X="-3.305056396484" Y="-25.105224609375" />
                  <Point X="-3.300991210938" Y="-25.08850390625" />
                  <Point X="-3.296721923828" Y="-25.060345703125" />
                  <Point X="-3.2956484375" Y="-25.046107421875" />
                  <Point X="-3.295647949219" Y="-25.01646484375" />
                  <Point X="-3.296721191406" Y="-25.002224609375" />
                  <Point X="-3.300990478516" Y="-24.9740625" />
                  <Point X="-3.304186523438" Y="-24.960140625" />
                  <Point X="-3.312565673828" Y="-24.933142578125" />
                  <Point X="-3.31931640625" Y="-24.915779296875" />
                  <Point X="-3.333423583984" Y="-24.887771484375" />
                  <Point X="-3.341640625" Y="-24.87435546875" />
                  <Point X="-3.360885253906" Y="-24.84809375" />
                  <Point X="-3.371205810547" Y="-24.83621484375" />
                  <Point X="-3.393667724609" Y="-24.814322265625" />
                  <Point X="-3.405809082031" Y="-24.80430859375" />
                  <Point X="-3.430953613281" Y="-24.786857421875" />
                  <Point X="-3.4367734375" Y="-24.783125" />
                  <Point X="-3.454733886719" Y="-24.772798828125" />
                  <Point X="-3.472912109375" Y="-24.763765625" />
                  <Point X="-3.481583251953" Y="-24.759982421875" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.647745117188" Y="-24.71301953125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.737483886719" Y="-24.08405078125" />
                  <Point X="-4.731331054688" Y="-24.04246875" />
                  <Point X="-4.633586914062" Y="-23.681765625" />
                  <Point X="-3.778068359375" Y="-23.79439453125" />
                  <Point X="-3.767740234375" Y="-23.79518359375" />
                  <Point X="-3.747056640625" Y="-23.795634765625" />
                  <Point X="-3.736703613281" Y="-23.795296875" />
                  <Point X="-3.715141845703" Y="-23.79341015625" />
                  <Point X="-3.704888671875" Y="-23.7919453125" />
                  <Point X="-3.684603515625" Y="-23.78791015625" />
                  <Point X="-3.674571533203" Y="-23.78533984375" />
                  <Point X="-3.618918701172" Y="-23.76779296875" />
                  <Point X="-3.603450439453" Y="-23.76232421875" />
                  <Point X="-3.584519287109" Y="-23.753994140625" />
                  <Point X="-3.575283203125" Y="-23.7493125" />
                  <Point X="-3.556540527344" Y="-23.7384921875" />
                  <Point X="-3.547866699219" Y="-23.732833984375" />
                  <Point X="-3.531185058594" Y="-23.720603515625" />
                  <Point X="-3.515934570312" Y="-23.70662890625" />
                  <Point X="-3.502295654297" Y="-23.691078125" />
                  <Point X="-3.495899414062" Y="-23.6829296875" />
                  <Point X="-3.483485107422" Y="-23.665201171875" />
                  <Point X="-3.478013183594" Y="-23.656400390625" />
                  <Point X="-3.4680625" Y="-23.63826171875" />
                  <Point X="-3.463583740234" Y="-23.628923828125" />
                  <Point X="-3.441252929688" Y="-23.57501171875" />
                  <Point X="-3.435501464844" Y="-23.559654296875" />
                  <Point X="-3.429714355469" Y="-23.539802734375" />
                  <Point X="-3.427362060547" Y="-23.529716796875" />
                  <Point X="-3.423602294922" Y="-23.508400390625" />
                  <Point X="-3.422361816406" Y="-23.498115234375" />
                  <Point X="-3.421008056641" Y="-23.477470703125" />
                  <Point X="-3.42191015625" Y="-23.45680078125" />
                  <Point X="-3.425057128906" Y="-23.43635546875" />
                  <Point X="-3.427188720703" Y="-23.426220703125" />
                  <Point X="-3.432791259766" Y="-23.4053125" />
                  <Point X="-3.436012451172" Y="-23.395470703125" />
                  <Point X="-3.443509277344" Y="-23.37619140625" />
                  <Point X="-3.447784912109" Y="-23.36675390625" />
                  <Point X="-3.474729492188" Y="-23.314994140625" />
                  <Point X="-3.482800537109" Y="-23.300712890625" />
                  <Point X="-3.494291259766" Y="-23.283515625" />
                  <Point X="-3.500506103516" Y="-23.27523046875" />
                  <Point X="-3.51441796875" Y="-23.258650390625" />
                  <Point X="-3.521498779297" Y="-23.25108984375" />
                  <Point X="-3.53644140625" Y="-23.23678515625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.624295898438" Y="-23.16866015625" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.026198730469" Y="-22.360642578125" />
                  <Point X="-4.002296630859" Y="-22.319693359375" />
                  <Point X="-3.726337890625" Y="-21.964986328125" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244919921875" Y="-22.24228125" />
                  <Point X="-3.225989501953" Y="-22.250611328125" />
                  <Point X="-3.216296142578" Y="-22.254259765625" />
                  <Point X="-3.195651855469" Y="-22.26076953125" />
                  <Point X="-3.185621337891" Y="-22.263341796875" />
                  <Point X="-3.165331787109" Y="-22.26737890625" />
                  <Point X="-3.155072753906" Y="-22.26884375" />
                  <Point X="-3.077563964844" Y="-22.275625" />
                  <Point X="-3.059171875" Y="-22.276666015625" />
                  <Point X="-3.038486328125" Y="-22.27621484375" />
                  <Point X="-3.02815234375" Y="-22.27542578125" />
                  <Point X="-3.006695068359" Y="-22.272599609375" />
                  <Point X="-2.99651171875" Y="-22.2706875" />
                  <Point X="-2.976421875" Y="-22.26576953125" />
                  <Point X="-2.956990722656" Y="-22.2586953125" />
                  <Point X="-2.938440917969" Y="-22.249546875" />
                  <Point X="-2.929415771484" Y="-22.244466796875" />
                  <Point X="-2.911162841797" Y="-22.232837890625" />
                  <Point X="-2.902745605469" Y="-22.226806640625" />
                  <Point X="-2.886615966797" Y="-22.213861328125" />
                  <Point X="-2.878903564453" Y="-22.206947265625" />
                  <Point X="-2.823887207031" Y="-22.151931640625" />
                  <Point X="-2.811267333984" Y="-22.138513671875" />
                  <Point X="-2.798321777344" Y="-22.122384765625" />
                  <Point X="-2.792288818359" Y="-22.113966796875" />
                  <Point X="-2.780658935547" Y="-22.095712890625" />
                  <Point X="-2.775577148438" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759352294922" Y="-22.048693359375" />
                  <Point X="-2.754435302734" Y="-22.028599609375" />
                  <Point X="-2.752525634766" Y="-22.018419921875" />
                  <Point X="-2.749700195312" Y="-21.9969609375" />
                  <Point X="-2.748909912109" Y="-21.986634765625" />
                  <Point X="-2.748458496094" Y="-21.965955078125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.755578613281" Y="-21.878091796875" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509033203" Y="-21.799140625" />
                  <Point X="-2.782840576172" Y="-21.780205078125" />
                  <Point X="-2.787523925781" Y="-21.770962890625" />
                  <Point X="-2.822971191406" Y="-21.709568359375" />
                  <Point X="-3.059385986328" Y="-21.300083984375" />
                  <Point X="-2.690002685547" Y="-21.0168828125" />
                  <Point X="-2.648369628906" Y="-20.984962890625" />
                  <Point X="-2.192523193359" Y="-20.731703125" />
                  <Point X="-2.118563964844" Y="-20.82808984375" />
                  <Point X="-2.111818847656" Y="-20.835953125" />
                  <Point X="-2.097515136719" Y="-20.85089453125" />
                  <Point X="-2.089956542969" Y="-20.85797265625" />
                  <Point X="-2.073376708984" Y="-20.871884765625" />
                  <Point X="-2.065093994141" Y="-20.878099609375" />
                  <Point X="-2.047895996094" Y="-20.889591796875" />
                  <Point X="-2.038981201172" Y="-20.894869140625" />
                  <Point X="-1.952743408203" Y="-20.939763671875" />
                  <Point X="-1.952743652344" Y="-20.939763671875" />
                  <Point X="-1.934355957031" Y="-20.948701171875" />
                  <Point X="-1.915075439453" Y="-20.956201171875" />
                  <Point X="-1.905233886719" Y="-20.959423828125" />
                  <Point X="-1.884323974609" Y="-20.965029296875" />
                  <Point X="-1.874189331055" Y="-20.967162109375" />
                  <Point X="-1.853738525391" Y="-20.9703125" />
                  <Point X="-1.833060302734" Y="-20.971216796875" />
                  <Point X="-1.812412963867" Y="-20.96986328125" />
                  <Point X="-1.802127929688" Y="-20.968623046875" />
                  <Point X="-1.78080871582" Y="-20.96486328125" />
                  <Point X="-1.770717529297" Y="-20.962509765625" />
                  <Point X="-1.750861572266" Y="-20.956720703125" />
                  <Point X="-1.741096679688" Y="-20.95328515625" />
                  <Point X="-1.651243896484" Y="-20.91606640625" />
                  <Point X="-1.632580932617" Y="-20.907724609375" />
                  <Point X="-1.614440429688" Y="-20.897771484375" />
                  <Point X="-1.605641723633" Y="-20.892298828125" />
                  <Point X="-1.587912719727" Y="-20.8798828125" />
                  <Point X="-1.579770263672" Y="-20.8734921875" />
                  <Point X="-1.564223022461" Y="-20.859857421875" />
                  <Point X="-1.550248413086" Y="-20.844607421875" />
                  <Point X="-1.538020141602" Y="-20.8279296875" />
                  <Point X="-1.532362304688" Y="-20.8192578125" />
                  <Point X="-1.521540771484" Y="-20.800515625" />
                  <Point X="-1.516856811523" Y="-20.7912734375" />
                  <Point X="-1.508525024414" Y="-20.772337890625" />
                  <Point X="-1.504876953125" Y="-20.76264453125" />
                  <Point X="-1.475631591797" Y="-20.669890625" />
                  <Point X="-1.470026245117" Y="-20.650236328125" />
                  <Point X="-1.465990722656" Y="-20.62994921875" />
                  <Point X="-1.464526611328" Y="-20.619693359375" />
                  <Point X="-1.462640380859" Y="-20.5981328125" />
                  <Point X="-1.462301513672" Y="-20.587779296875" />
                  <Point X="-1.462753051758" Y="-20.567099609375" />
                  <Point X="-1.463543334961" Y="-20.5567734375" />
                  <Point X="-1.467573364258" Y="-20.5261640625" />
                  <Point X="-1.479266479492" Y="-20.437345703125" />
                  <Point X="-0.98506628418" Y="-20.2987890625" />
                  <Point X="-0.931167358398" Y="-20.2836796875" />
                  <Point X="-0.365222351074" Y="-20.217443359375" />
                  <Point X="-0.225666290283" Y="-20.7382734375" />
                  <Point X="-0.220435211182" Y="-20.752892578125" />
                  <Point X="-0.207661819458" Y="-20.781083984375" />
                  <Point X="-0.20011920166" Y="-20.79465625" />
                  <Point X="-0.182260818481" Y="-20.8213828125" />
                  <Point X="-0.172608810425" Y="-20.833544921875" />
                  <Point X="-0.151451293945" Y="-20.856134765625" />
                  <Point X="-0.12689641571" Y="-20.8749765625" />
                  <Point X="-0.099600479126" Y="-20.88956640625" />
                  <Point X="-0.085353935242" Y="-20.8957421875" />
                  <Point X="-0.054915962219" Y="-20.90607421875" />
                  <Point X="-0.039853626251" Y="-20.909845703125" />
                  <Point X="-0.009317714691" Y="-20.91488671875" />
                  <Point X="0.021629587173" Y="-20.91488671875" />
                  <Point X="0.052165500641" Y="-20.909845703125" />
                  <Point X="0.067227836609" Y="-20.90607421875" />
                  <Point X="0.097665802002" Y="-20.8957421875" />
                  <Point X="0.111912200928" Y="-20.88956640625" />
                  <Point X="0.139208129883" Y="-20.8749765625" />
                  <Point X="0.163763168335" Y="-20.856134765625" />
                  <Point X="0.184920822144" Y="-20.833544921875" />
                  <Point X="0.194573135376" Y="-20.8213828125" />
                  <Point X="0.212431228638" Y="-20.79465625" />
                  <Point X="0.219973388672" Y="-20.781083984375" />
                  <Point X="0.232747085571" Y="-20.752892578125" />
                  <Point X="0.237978302002" Y="-20.7382734375" />
                  <Point X="0.256140930176" Y="-20.67048828125" />
                  <Point X="0.378190704346" Y="-20.2149921875" />
                  <Point X="0.78080871582" Y="-20.257158203125" />
                  <Point X="0.827852416992" Y="-20.262083984375" />
                  <Point X="1.405082641602" Y="-20.4014453125" />
                  <Point X="1.453621704102" Y="-20.413166015625" />
                  <Point X="1.828193603516" Y="-20.549025390625" />
                  <Point X="1.85824987793" Y="-20.55992578125" />
                  <Point X="2.221549560547" Y="-20.729830078125" />
                  <Point X="2.250430175781" Y="-20.7433359375" />
                  <Point X="2.601455810547" Y="-20.94784375" />
                  <Point X="2.629448486328" Y="-20.96415234375" />
                  <Point X="2.817780029297" Y="-21.09808203125" />
                  <Point X="2.065308105469" Y="-22.40140234375" />
                  <Point X="2.062371826172" Y="-22.4068984375" />
                  <Point X="2.053180664062" Y="-22.426564453125" />
                  <Point X="2.044181518555" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.021849853516" Y="-22.53214453125" />
                  <Point X="2.016911376953" Y="-22.55730859375" />
                  <Point X="2.013555541992" Y="-22.583755859375" />
                  <Point X="2.012800537109" Y="-22.595419921875" />
                  <Point X="2.012728271484" Y="-22.618751953125" />
                  <Point X="2.013411010742" Y="-22.630419921875" />
                  <Point X="2.020995605469" Y="-22.693318359375" />
                  <Point X="2.02380078125" Y="-22.71096484375" />
                  <Point X="2.02914453125" Y="-22.732890625" />
                  <Point X="2.032469970703" Y="-22.7436953125" />
                  <Point X="2.040738769531" Y="-22.7657890625" />
                  <Point X="2.045322875977" Y="-22.776119140625" />
                  <Point X="2.055684326172" Y="-22.79616015625" />
                  <Point X="2.061461425781" Y="-22.80587109375" />
                  <Point X="2.100381591797" Y="-22.86323046875" />
                  <Point X="2.116428955078" Y="-22.8837109375" />
                  <Point X="2.133764160156" Y="-22.90295703125" />
                  <Point X="2.141935302734" Y="-22.910994140625" />
                  <Point X="2.159182373047" Y="-22.926025390625" />
                  <Point X="2.168258300781" Y="-22.93301953125" />
                  <Point X="2.225616699219" Y="-22.971939453125" />
                  <Point X="2.241255371094" Y="-22.981740234375" />
                  <Point X="2.261296142578" Y="-22.992103515625" />
                  <Point X="2.271624267578" Y="-22.9966875" />
                  <Point X="2.293718017578" Y="-23.004958984375" />
                  <Point X="2.304526123047" Y="-23.008287109375" />
                  <Point X="2.326462402344" Y="-23.013634765625" />
                  <Point X="2.337590576172" Y="-23.015654296875" />
                  <Point X="2.400490234375" Y="-23.02323828125" />
                  <Point X="2.407184082031" Y="-23.023806640625" />
                  <Point X="2.427348632812" Y="-23.0245625" />
                  <Point X="2.452776855469" Y="-23.02401171875" />
                  <Point X="2.464116943359" Y="-23.023083984375" />
                  <Point X="2.486603271484" Y="-23.019880859375" />
                  <Point X="2.497749511719" Y="-23.01760546875" />
                  <Point X="2.570489990234" Y="-22.99815234375" />
                  <Point X="2.585375732422" Y="-22.9935078125" />
                  <Point X="2.605006591797" Y="-22.98648828125" />
                  <Point X="2.612920654297" Y="-22.98325" />
                  <Point X="2.636033447266" Y="-22.97212890625" />
                  <Point X="2.776299804688" Y="-22.891146484375" />
                  <Point X="3.940405029297" Y="-22.21905078125" />
                  <Point X="4.027355712891" Y="-22.339892578125" />
                  <Point X="4.043958740234" Y="-22.362966796875" />
                  <Point X="4.136884277344" Y="-22.51652734375" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168133300781" Y="-23.260134765625" />
                  <Point X="3.152128173828" Y="-23.274775390625" />
                  <Point X="3.134675292969" Y="-23.293388671875" />
                  <Point X="3.128578125" Y="-23.30057421875" />
                  <Point X="3.076226806641" Y="-23.36887109375" />
                  <Point X="3.062545654297" Y="-23.38934375" />
                  <Point X="3.048642578125" Y="-23.413384765625" />
                  <Point X="3.043324951172" Y="-23.424080078125" />
                  <Point X="3.034073730469" Y="-23.446052734375" />
                  <Point X="3.030140136719" Y="-23.457330078125" />
                  <Point X="3.010639160156" Y="-23.527060546875" />
                  <Point X="3.006224853516" Y="-23.545341796875" />
                  <Point X="3.002771240234" Y="-23.567642578125" />
                  <Point X="3.001708984375" Y="-23.578896484375" />
                  <Point X="3.000893310547" Y="-23.60246484375" />
                  <Point X="3.001175048828" Y="-23.613763671875" />
                  <Point X="3.003078125" Y="-23.63624609375" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.020707763672" Y="-23.725013671875" />
                  <Point X="3.027364990234" Y="-23.749130859375" />
                  <Point X="3.036223876953" Y="-23.77455859375" />
                  <Point X="3.040751464844" Y="-23.785359375" />
                  <Point X="3.051099121094" Y="-23.806318359375" />
                  <Point X="3.056919189453" Y="-23.8164765625" />
                  <Point X="3.100459960938" Y="-23.88265625" />
                  <Point X="3.111740966797" Y="-23.898578125" />
                  <Point X="3.126297851562" Y="-23.915826171875" />
                  <Point X="3.134090576172" Y="-23.924017578125" />
                  <Point X="3.151334228516" Y="-23.94010546875" />
                  <Point X="3.160040039062" Y="-23.94730859375" />
                  <Point X="3.178245849609" Y="-23.960630859375" />
                  <Point X="3.187745849609" Y="-23.96675" />
                  <Point X="3.250837890625" Y="-24.002265625" />
                  <Point X="3.274247070312" Y="-24.0134140625" />
                  <Point X="3.298770996094" Y="-24.023099609375" />
                  <Point X="3.309819824219" Y="-24.02669921875" />
                  <Point X="3.332270751953" Y="-24.032521484375" />
                  <Point X="3.343672851563" Y="-24.034744140625" />
                  <Point X="3.428983642578" Y="-24.046017578125" />
                  <Point X="3.443718994141" Y="-24.047380859375" />
                  <Point X="3.465647949219" Y="-24.0485546875" />
                  <Point X="3.474396484375" Y="-24.04862109375" />
                  <Point X="3.500602539062" Y="-24.047203125" />
                  <Point X="3.633846191406" Y="-24.029662109375" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.745555664063" Y="-24.056490234375" />
                  <Point X="4.752684082031" Y="-24.08576953125" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="3.691992431641" Y="-24.578646484375" />
                  <Point X="3.686029052734" Y="-24.58045703125" />
                  <Point X="3.665619140625" Y="-24.587865234375" />
                  <Point X="3.642381591797" Y="-24.598380859375" />
                  <Point X="3.6340078125" Y="-24.602681640625" />
                  <Point X="3.550192626953" Y="-24.651126953125" />
                  <Point X="3.530468994141" Y="-24.664232421875" />
                  <Point X="3.507458496094" Y="-24.681681640625" />
                  <Point X="3.498244384766" Y="-24.6896484375" />
                  <Point X="3.480912597656" Y="-24.7066953125" />
                  <Point X="3.472794921875" Y="-24.715775390625" />
                  <Point X="3.422505859375" Y="-24.77985546875" />
                  <Point X="3.410852050781" Y="-24.795794921875" />
                  <Point X="3.399126220703" Y="-24.815078125" />
                  <Point X="3.393837402344" Y="-24.8250703125" />
                  <Point X="3.384065917969" Y="-24.84653125" />
                  <Point X="3.380003173828" Y="-24.857076171875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.370376464844" Y="-24.88952734375" />
                  <Point X="3.35361328125" Y="-24.97705859375" />
                  <Point X="3.350590332031" Y="-25.00097265625" />
                  <Point X="3.348851318359" Y="-25.028861328125" />
                  <Point X="3.348858642578" Y="-25.04080078125" />
                  <Point X="3.350370361328" Y="-25.06458203125" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.368637939453" Y="-25.163953125" />
                  <Point X="3.373158203125" Y="-25.183984375" />
                  <Point X="3.380002685547" Y="-25.205482421875" />
                  <Point X="3.384065917969" Y="-25.216029296875" />
                  <Point X="3.393837402344" Y="-25.237490234375" />
                  <Point X="3.399126220703" Y="-25.247482421875" />
                  <Point X="3.410852050781" Y="-25.266765625" />
                  <Point X="3.4172890625" Y="-25.276056640625" />
                  <Point X="3.467578125" Y="-25.34013671875" />
                  <Point X="3.484303222656" Y="-25.3587734375" />
                  <Point X="3.503834716797" Y="-25.377845703125" />
                  <Point X="3.512800537109" Y="-25.3855703125" />
                  <Point X="3.531631591797" Y="-25.3998515625" />
                  <Point X="3.541496826172" Y="-25.406408203125" />
                  <Point X="3.625312011719" Y="-25.454853515625" />
                  <Point X="3.637751464844" Y="-25.461439453125" />
                  <Point X="3.658526611328" Y="-25.471474609375" />
                  <Point X="3.666743652344" Y="-25.4749765625" />
                  <Point X="3.691992919922" Y="-25.483916015625" />
                  <Point X="3.814183349609" Y="-25.51665625" />
                  <Point X="4.784877441406" Y="-25.776751953125" />
                  <Point X="4.765594238281" Y="-25.904654296875" />
                  <Point X="4.761614746094" Y="-25.93105078125" />
                  <Point X="4.727801269531" Y="-26.07922265625" />
                  <Point X="3.436771240234" Y="-25.90925390625" />
                  <Point X="3.428621826172" Y="-25.908537109375" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720947266" Y="-25.910841796875" />
                  <Point X="3.354482421875" Y="-25.912677734375" />
                  <Point X="3.189982910156" Y="-25.948431640625" />
                  <Point X="3.157876953125" Y="-25.9567421875" />
                  <Point X="3.128758300781" Y="-25.968365234375" />
                  <Point X="3.114680908203" Y="-25.97538671875" />
                  <Point X="3.08685546875" Y="-25.99227734375" />
                  <Point X="3.074128417969" Y="-26.00152734375" />
                  <Point X="3.050375732422" Y="-26.022" />
                  <Point X="3.039350097656" Y="-26.03322265625" />
                  <Point X="2.939920654297" Y="-26.152806640625" />
                  <Point X="2.921327636719" Y="-26.176845703125" />
                  <Point X="2.906608642578" Y="-26.201224609375" />
                  <Point X="2.90016796875" Y="-26.21396875" />
                  <Point X="2.888822998047" Y="-26.24135546875" />
                  <Point X="2.884364257812" Y="-26.254923828125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875156982422" Y="-26.29666015625" />
                  <Point X="2.86090625" Y="-26.451525390625" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607421875" Y="-26.514591796875" />
                  <Point X="2.864065673828" Y="-26.530130859375" />
                  <Point X="2.871797363281" Y="-26.561751953125" />
                  <Point X="2.876786621094" Y="-26.576671875" />
                  <Point X="2.889158203125" Y="-26.605482421875" />
                  <Point X="2.896540527344" Y="-26.619373046875" />
                  <Point X="2.987576904297" Y="-26.76097265625" />
                  <Point X="2.996930908203" Y="-26.77423828125" />
                  <Point X="3.017994873047" Y="-26.801556640625" />
                  <Point X="3.026128662109" Y="-26.81080078125" />
                  <Point X="3.043529296875" Y="-26.828162109375" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.166189697266" Y="-26.9232890625" />
                  <Point X="4.087170898438" Y="-27.629984375" />
                  <Point X="4.056707763672" Y="-27.67927734375" />
                  <Point X="4.0455" Y="-27.6974140625" />
                  <Point X="4.0012734375" Y="-27.76025390625" />
                  <Point X="2.848452148438" Y="-27.094671875" />
                  <Point X="2.84119140625" Y="-27.09088671875" />
                  <Point X="2.815026123047" Y="-27.079515625" />
                  <Point X="2.78312109375" Y="-27.069328125" />
                  <Point X="2.771107910156" Y="-27.066337890625" />
                  <Point X="2.575327392578" Y="-27.03098046875" />
                  <Point X="2.555017578125" Y="-27.0273125" />
                  <Point X="2.539359130859" Y="-27.02580859375" />
                  <Point X="2.508010009766" Y="-27.025404296875" />
                  <Point X="2.492319335938" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444846435547" Y="-27.03513671875" />
                  <Point X="2.415068115234" Y="-27.0449609375" />
                  <Point X="2.400587890625" Y="-27.051109375" />
                  <Point X="2.237942382812" Y="-27.13670703125" />
                  <Point X="2.208963623047" Y="-27.153173828125" />
                  <Point X="2.186027587891" Y="-27.170072265625" />
                  <Point X="2.175197753906" Y="-27.179384765625" />
                  <Point X="2.154239746094" Y="-27.200345703125" />
                  <Point X="2.144931884766" Y="-27.211171875" />
                  <Point X="2.128043945313" Y="-27.23409765625" />
                  <Point X="2.120463867188" Y="-27.246197265625" />
                  <Point X="2.034864746094" Y="-27.408841796875" />
                  <Point X="2.019836303711" Y="-27.440193359375" />
                  <Point X="2.010012329102" Y="-27.46996875" />
                  <Point X="2.006337036133" Y="-27.485265625" />
                  <Point X="2.001379394531" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.037545532227" Y="-27.775921875" />
                  <Point X="2.041213378906" Y="-27.796232421875" />
                  <Point X="2.043014770508" Y="-27.804220703125" />
                  <Point X="2.051236816406" Y="-27.831546875" />
                  <Point X="2.0640703125" Y="-27.862482421875" />
                  <Point X="2.069547119141" Y="-27.873580078125" />
                  <Point X="2.142413818359" Y="-27.999787109375" />
                  <Point X="2.735894287109" Y="-29.027724609375" />
                  <Point X="2.723755371094" Y="-29.036083984375" />
                  <Point X="1.833915893555" Y="-27.876423828125" />
                  <Point X="1.828652099609" Y="-27.87014453125" />
                  <Point X="1.808831176758" Y="-27.849626953125" />
                  <Point X="1.783250488281" Y="-27.82800390625" />
                  <Point X="1.773297119141" Y="-27.820646484375" />
                  <Point X="1.580204711914" Y="-27.696505859375" />
                  <Point X="1.560173706055" Y="-27.68362890625" />
                  <Point X="1.546279907227" Y="-27.67624609375" />
                  <Point X="1.517467163086" Y="-27.663875" />
                  <Point X="1.502548461914" Y="-27.65888671875" />
                  <Point X="1.4709296875" Y="-27.65115625" />
                  <Point X="1.455392456055" Y="-27.648697265625" />
                  <Point X="1.424125732422" Y="-27.64637890625" />
                  <Point X="1.408396240234" Y="-27.64651953125" />
                  <Point X="1.197216918945" Y="-27.665951171875" />
                  <Point X="1.175309448242" Y="-27.667966796875" />
                  <Point X="1.161231323242" Y="-27.670337890625" />
                  <Point X="1.133582275391" Y="-27.67716796875" />
                  <Point X="1.120011474609" Y="-27.681626953125" />
                  <Point X="1.092624145508" Y="-27.692970703125" />
                  <Point X="1.07987878418" Y="-27.699412109375" />
                  <Point X="1.055495239258" Y="-27.7141328125" />
                  <Point X="1.043857055664" Y="-27.722412109375" />
                  <Point X="0.880789611816" Y="-27.857998046875" />
                  <Point X="0.863873352051" Y="-27.872064453125" />
                  <Point X="0.852648742676" Y="-27.88309375" />
                  <Point X="0.832176391602" Y="-27.90684765625" />
                  <Point X="0.822928649902" Y="-27.919572265625" />
                  <Point X="0.806037780762" Y="-27.9473984375" />
                  <Point X="0.799017272949" Y="-27.96147265625" />
                  <Point X="0.787394226074" Y="-27.99058984375" />
                  <Point X="0.782791992188" Y="-28.0056328125" />
                  <Point X="0.734035644531" Y="-28.22994921875" />
                  <Point X="0.728977844238" Y="-28.25321875" />
                  <Point X="0.727584960938" Y="-28.2612890625" />
                  <Point X="0.724724853516" Y="-28.28967578125" />
                  <Point X="0.724742248535" Y="-28.32316796875" />
                  <Point X="0.725555053711" Y="-28.33551953125" />
                  <Point X="0.74620489502" Y="-28.49237109375" />
                  <Point X="0.833091796875" Y="-29.152341796875" />
                  <Point X="0.655064697266" Y="-28.487935546875" />
                  <Point X="0.652606201172" Y="-28.480123046875" />
                  <Point X="0.642146972656" Y="-28.453583984375" />
                  <Point X="0.626788452148" Y="-28.42381640625" />
                  <Point X="0.620407653809" Y="-28.41320703125" />
                  <Point X="0.47206829834" Y="-28.199478515625" />
                  <Point X="0.456679870605" Y="-28.177306640625" />
                  <Point X="0.446668640137" Y="-28.16516796875" />
                  <Point X="0.424785705566" Y="-28.14271484375" />
                  <Point X="0.412913543701" Y="-28.132400390625" />
                  <Point X="0.386658416748" Y="-28.11315625" />
                  <Point X="0.373244903564" Y="-28.104939453125" />
                  <Point X="0.345243011475" Y="-28.090830078125" />
                  <Point X="0.330654510498" Y="-28.0849375" />
                  <Point X="0.101108062744" Y="-28.0136953125" />
                  <Point X="0.077295249939" Y="-28.0063046875" />
                  <Point X="0.063377311707" Y="-28.003109375" />
                  <Point X="0.035218029022" Y="-27.99883984375" />
                  <Point X="0.020976835251" Y="-27.997765625" />
                  <Point X="-0.008664813995" Y="-27.997765625" />
                  <Point X="-0.022906009674" Y="-27.99883984375" />
                  <Point X="-0.051065292358" Y="-28.003109375" />
                  <Point X="-0.064983230591" Y="-28.0063046875" />
                  <Point X="-0.294529663086" Y="-28.077546875" />
                  <Point X="-0.318342468262" Y="-28.0849375" />
                  <Point X="-0.332931152344" Y="-28.090830078125" />
                  <Point X="-0.360933044434" Y="-28.104939453125" />
                  <Point X="-0.374346252441" Y="-28.11315625" />
                  <Point X="-0.400601379395" Y="-28.132400390625" />
                  <Point X="-0.412473083496" Y="-28.14271484375" />
                  <Point X="-0.434356323242" Y="-28.16516796875" />
                  <Point X="-0.444368011475" Y="-28.177306640625" />
                  <Point X="-0.592707214355" Y="-28.39103515625" />
                  <Point X="-0.608095581055" Y="-28.41320703125" />
                  <Point X="-0.612471191406" Y="-28.420130859375" />
                  <Point X="-0.625977172852" Y="-28.445263671875" />
                  <Point X="-0.638778198242" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.680403015137" Y="-28.628447265625" />
                  <Point X="-0.985425231934" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.764859587828" Y="-24.291170329698" />
                  <Point X="4.607417608799" Y="-23.901487757226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068925800719" Y="-22.568673762375" />
                  <Point X="3.930077898916" Y="-22.225013145998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.672407581772" Y="-24.315942965378" />
                  <Point X="4.510131604316" Y="-23.914295826953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.990712672925" Y="-22.628688858452" />
                  <Point X="3.846996929639" Y="-22.272979911615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.579955575715" Y="-24.340715601058" />
                  <Point X="4.412845599834" Y="-23.927103896679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.912499545132" Y="-22.688703954529" />
                  <Point X="3.763915960363" Y="-22.320946677232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.487503569658" Y="-24.365488236738" />
                  <Point X="4.315559595351" Y="-23.939911966406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.834286417339" Y="-22.748719050606" />
                  <Point X="3.680834991086" Y="-22.368913442849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.395051563602" Y="-24.390260872418" />
                  <Point X="4.218273590869" Y="-23.952720036132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.756073289546" Y="-22.808734146683" />
                  <Point X="3.597754021809" Y="-22.416880208466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.302599557545" Y="-24.415033508098" />
                  <Point X="4.120987586386" Y="-23.965528105859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.677860161752" Y="-22.86874924276" />
                  <Point X="3.514673052533" Y="-22.464846974083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.775483359226" Y="-25.839061369275" />
                  <Point X="4.746112054346" Y="-25.766364838699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.210147551489" Y="-24.439806143778" />
                  <Point X="4.023701581903" Y="-23.978336175585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.599647033959" Y="-22.928764338837" />
                  <Point X="3.431592083256" Y="-22.5128137397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.74305245003" Y="-26.012391432721" />
                  <Point X="4.631212441529" Y="-25.735577697995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.117695545432" Y="-24.464578779458" />
                  <Point X="3.926415577421" Y="-23.991144245312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.521433906166" Y="-22.988779434914" />
                  <Point X="3.348511113979" Y="-22.560780505317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.782341942339" Y="-21.159462631782" />
                  <Point X="2.733257776721" Y="-21.037975058749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.664210740425" Y="-26.07085073422" />
                  <Point X="4.516312828712" Y="-25.704790557291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.025243539376" Y="-24.489351415138" />
                  <Point X="3.829129572938" Y="-24.003952315038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.443220778372" Y="-23.048794530991" />
                  <Point X="3.265430144703" Y="-22.608747270934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.722063590626" Y="-21.263867856354" />
                  <Point X="2.59220376066" Y="-20.942453498417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.555993713128" Y="-26.056603573084" />
                  <Point X="4.401413215895" Y="-25.674003416588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932791533319" Y="-24.514124050818" />
                  <Point X="3.731843568456" Y="-24.016760384765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365007650579" Y="-23.108809627068" />
                  <Point X="3.182349175426" Y="-22.656714036551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.661785238913" Y="-21.368273080926" />
                  <Point X="2.458200459136" Y="-20.864383068943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.447776685831" Y="-26.042356411948" />
                  <Point X="4.286513603078" Y="-25.643216275884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840339527262" Y="-24.538896686498" />
                  <Point X="3.634557563973" Y="-24.029568454491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.286794522786" Y="-23.168824723144" />
                  <Point X="3.099268206149" Y="-22.704680802168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.6015068872" Y="-21.472678305498" />
                  <Point X="2.324197157612" Y="-20.786312639469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.339559658534" Y="-26.028109250813" />
                  <Point X="4.17161399026" Y="-25.61242913518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.747887521206" Y="-24.563669322179" />
                  <Point X="3.537271277501" Y="-24.042375826269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.208581394993" Y="-23.228839819221" />
                  <Point X="3.016187236873" Y="-22.752647567785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.541228535487" Y="-21.57708353007" />
                  <Point X="2.193642940846" Y="-20.716778994336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.231342631237" Y="-26.013862089677" />
                  <Point X="4.056714377443" Y="-25.581641994476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.656812624549" Y="-24.59185042323" />
                  <Point X="3.436565237088" Y="-24.046719010025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.132998448525" Y="-23.295364842519" />
                  <Point X="2.933106267596" Y="-22.800614333402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.480950183775" Y="-21.681488754643" />
                  <Point X="2.067311752001" Y="-20.657697710093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.12312560394" Y="-25.999614928541" />
                  <Point X="3.941814764626" Y="-25.550854853772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.572979684899" Y="-24.637955996861" />
                  <Point X="3.327911514631" Y="-24.031390990441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.066261205553" Y="-23.383783750248" />
                  <Point X="2.850025298319" Y="-22.848581099019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420671832062" Y="-21.785893979215" />
                  <Point X="1.940980563157" Y="-20.59861642585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.014908576644" Y="-25.985367767406" />
                  <Point X="3.826915151809" Y="-25.520067713069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.493349240702" Y="-24.694463111741" />
                  <Point X="3.20274547633" Y="-23.97519355499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015158674483" Y="-23.510899927865" />
                  <Point X="2.766944322366" Y="-22.89654784811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.360393480349" Y="-21.890299203787" />
                  <Point X="1.816816445113" Y="-20.544898830058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.906691549347" Y="-25.97112060627" />
                  <Point X="3.712015716038" Y="-25.489281010571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.424409269865" Y="-24.77743007669" />
                  <Point X="2.683863293796" Y="-22.944514466971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.300115128636" Y="-21.994704428359" />
                  <Point X="1.696762645218" Y="-20.501354628675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.79847452205" Y="-25.956873445134" />
                  <Point X="3.586606488557" Y="-25.432481660778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369367434411" Y="-24.894796133811" />
                  <Point X="2.599199907945" Y="-22.988564614129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239836776923" Y="-22.099109652931" />
                  <Point X="1.576708845322" Y="-20.457810427293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.690257494753" Y="-25.942626283999" />
                  <Point X="3.424826156983" Y="-25.2856606694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362315857766" Y="-25.130942249605" />
                  <Point X="2.507426753137" Y="-23.015017465608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.17955842521" Y="-22.203514877504" />
                  <Point X="1.456655045426" Y="-20.414266225911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.582040467456" Y="-25.928379122863" />
                  <Point X="2.408537507072" Y="-23.023857373171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.119280073497" Y="-22.307920102076" />
                  <Point X="1.342953947446" Y="-20.386445513524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.473823440159" Y="-25.914131961727" />
                  <Point X="2.299112381285" Y="-23.006620063346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059388309802" Y="-22.41328216557" />
                  <Point X="1.229418376862" Y="-20.35903449582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.073529952075" Y="-27.65205704572" />
                  <Point X="4.054475872045" Y="-27.604896542736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369924779572" Y="-25.910573133264" />
                  <Point X="2.16630771504" Y="-22.931516360294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.016482753366" Y="-22.560686567339" />
                  <Point X="1.115882806279" Y="-20.331623478116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009856360509" Y="-27.748058756768" />
                  <Point X="3.90597745575" Y="-27.490949445254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.275270301192" Y="-25.929894458651" />
                  <Point X="1.002347235695" Y="-20.304212460412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.885261137333" Y="-27.693274138329" />
                  <Point X="3.757479039455" Y="-27.377002347771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.181215875176" Y="-25.950700965756" />
                  <Point X="0.888811665111" Y="-20.276801442708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751628565202" Y="-27.616121296303" />
                  <Point X="3.60898062316" Y="-27.263055250289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.09384003914" Y="-25.988037563119" />
                  <Point X="0.778308696024" Y="-20.256896377099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.617995993071" Y="-27.538968454276" />
                  <Point X="3.460482206865" Y="-27.149108152806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019352497565" Y="-26.057273808666" />
                  <Point X="0.671320866219" Y="-20.245691586517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48436342094" Y="-27.461815612249" />
                  <Point X="3.31198379057" Y="-27.035161055324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.950398189768" Y="-26.140205288393" />
                  <Point X="0.564333036415" Y="-20.234486795935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.350730848809" Y="-27.384662770223" />
                  <Point X="3.163485379356" Y="-26.921213970418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.888814797415" Y="-26.241380424034" />
                  <Point X="0.45734520661" Y="-20.223282005353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.217098276678" Y="-27.307509928196" />
                  <Point X="3.006829124962" Y="-26.787075515104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.862826588622" Y="-26.430656730549" />
                  <Point X="0.367561824795" Y="-20.25465971781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.083465704548" Y="-27.230357086169" />
                  <Point X="0.326705741213" Y="-20.407136742895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.949833132417" Y="-27.153204244143" />
                  <Point X="0.28584965763" Y="-20.55961376798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.818148425291" Y="-27.080872537182" />
                  <Point X="0.244993701099" Y="-20.71209110753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.704990955091" Y="-27.054397350768" />
                  <Point X="0.18934908263" Y="-20.827965224337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.594465555723" Y="-27.034436768266" />
                  <Point X="0.111796959081" Y="-20.889616363326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.489005919608" Y="-27.027014389793" />
                  <Point X="0.019546044853" Y="-20.91488671875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.397035482011" Y="-27.052978949238" />
                  <Point X="-0.091774091683" Y="-20.89295909273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.312540952569" Y="-27.097447030672" />
                  <Point X="-0.462558345309" Y="-20.22883524157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.228182842178" Y="-27.142252761106" />
                  <Point X="-0.56039294948" Y="-20.24028547942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.150806502904" Y="-27.204338981447" />
                  <Point X="-0.658227553651" Y="-20.25173571727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.089232904334" Y="-27.305538357553" />
                  <Point X="-0.756062157822" Y="-20.26318595512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.64392353037" Y="-28.932045214211" />
                  <Point X="2.558304021395" Y="-28.720129493149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.031416112949" Y="-27.41603615773" />
                  <Point X="-0.853896761993" Y="-20.27463619297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.427516484107" Y="-28.65001835946" />
                  <Point X="2.21700307467" Y="-28.128979387296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.007789441367" Y="-27.611157473952" />
                  <Point X="-0.95051270294" Y="-20.289102728144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.211109437843" Y="-28.367991504709" />
                  <Point X="-1.042548655947" Y="-20.314905131257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.99470239158" Y="-28.085964649958" />
                  <Point X="-1.13458413911" Y="-20.340708697274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.79049168937" Y="-27.834124806034" />
                  <Point X="-1.226619622274" Y="-20.366512263291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.650754699786" Y="-27.741863000621" />
                  <Point X="-1.318655105437" Y="-20.392315829308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.516678106484" Y="-27.663611167632" />
                  <Point X="-1.410690588601" Y="-20.418119395325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.407350705822" Y="-27.646615735977" />
                  <Point X="-1.466642762088" Y="-20.53323228675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.30856249778" Y="-27.655705721421" />
                  <Point X="-1.492401301421" Y="-20.723077045125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.209774289737" Y="-27.664795706866" />
                  <Point X="-1.54735501486" Y="-20.840661211888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.114959229419" Y="-27.683719578009" />
                  <Point X="-1.624509656438" Y="-20.903296153282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.032085664078" Y="-27.732199686381" />
                  <Point X="-1.711694245741" Y="-20.94110610292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.955389791568" Y="-27.795970121063" />
                  <Point X="-1.802995224643" Y="-20.968727630778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.878693957192" Y="-27.859740650129" />
                  <Point X="-1.909822014932" Y="-20.957921426983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809406796552" Y="-27.941848290161" />
                  <Point X="-2.037431678578" Y="-20.895675806567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.76527594731" Y="-28.086219985814" />
                  <Point X="-2.203645193769" Y="-20.737882300699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.7294357907" Y="-28.251111865807" />
                  <Point X="-2.287322906308" Y="-20.784372074915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.756720173782" Y="-28.57224246412" />
                  <Point X="-2.371000618846" Y="-20.83086184913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.806244941906" Y="-28.948419947065" />
                  <Point X="0.72372230454" Y="-28.744169252211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.547652149009" Y="-28.308380324978" />
                  <Point X="-2.454678331384" Y="-20.877351623345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.360376101111" Y="-28.098455221308" />
                  <Point X="-2.538356043922" Y="-20.923841397561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.241242009122" Y="-28.057187376876" />
                  <Point X="-2.62203375646" Y="-20.970331171776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.124091220512" Y="-28.020828380561" />
                  <Point X="-2.701977223324" Y="-21.026063528369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.012312461871" Y="-27.997765625" />
                  <Point X="-2.780205821267" Y="-21.08604033448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.084278789027" Y="-28.012293270189" />
                  <Point X="-2.858434419211" Y="-21.146017140591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.175323182129" Y="-28.040549870188" />
                  <Point X="-2.936663017154" Y="-21.205993946703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.266367575231" Y="-28.068806470187" />
                  <Point X="-2.75226187319" Y="-21.916002174326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.865260469237" Y="-21.636320834797" />
                  <Point X="-3.014891615098" Y="-21.265970752814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.355363531123" Y="-28.102133130195" />
                  <Point X="-2.781549856395" Y="-22.097111252577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.43294264226" Y="-28.163717472563" />
                  <Point X="-2.850934223829" Y="-22.178978297351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.498437736097" Y="-28.255210807287" />
                  <Point X="-2.927443598855" Y="-22.243210329502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.563199253007" Y="-28.348519808617" />
                  <Point X="-3.017457630516" Y="-22.274017163557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.626278568134" Y="-28.445992405466" />
                  <Point X="-3.120797038017" Y="-22.271842535056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.670282696676" Y="-28.59067774586" />
                  <Point X="-3.233099942087" Y="-22.247482474034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.711138775601" Y="-28.743154782473" />
                  <Point X="-3.365533938959" Y="-22.173296209873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.751994845678" Y="-28.895631840985" />
                  <Point X="-3.499166416178" Y="-22.096143602762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.792850915755" Y="-29.048108899497" />
                  <Point X="-3.632798893397" Y="-22.01899099565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.833706985833" Y="-29.200585958009" />
                  <Point X="-3.746571351415" Y="-21.99099366097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.87456305591" Y="-29.353063016522" />
                  <Point X="-3.814009934665" Y="-22.077676690599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.915419125987" Y="-29.505540075034" />
                  <Point X="-3.881448517914" Y="-22.164359720228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.956275196064" Y="-29.658017133546" />
                  <Point X="-3.429158024703" Y="-23.537417354344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557409295693" Y="-23.219984319581" />
                  <Point X="-3.948887101164" Y="-22.251042749857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.017280567935" Y="-29.760622920084" />
                  <Point X="-1.123244986736" Y="-29.498351780179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.322077716367" Y="-29.00622350504" />
                  <Point X="-3.481367116925" Y="-23.661794696998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.705907636129" Y="-23.106037409857" />
                  <Point X="-4.014892585554" Y="-22.341272823631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.130456417208" Y="-29.734102243867" />
                  <Point X="-1.138869806168" Y="-29.713278375458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.480764767835" Y="-28.867058650589" />
                  <Point X="-3.553611967221" Y="-23.736581798246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.854405751312" Y="-22.992091057653" />
                  <Point X="-4.075441590639" Y="-22.445008157601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.609852361501" Y="-28.801155025009" />
                  <Point X="-3.640689357208" Y="-23.774657075502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.002903866495" Y="-22.87814470545" />
                  <Point X="-4.13599046309" Y="-22.548743819852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.716435761118" Y="-28.790951234266" />
                  <Point X="-3.734875758525" Y="-23.795136932276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.151401981678" Y="-22.764198353246" />
                  <Point X="-4.19653933554" Y="-22.652479482103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.821683721648" Y="-28.784052771251" />
                  <Point X="-2.31344330861" Y="-27.566905082522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.352632429282" Y="-27.469908605149" />
                  <Point X="-3.306346862788" Y="-25.109382548865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438584059435" Y="-24.782084001912" />
                  <Point X="-3.840982934831" Y="-23.786111835589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.926931551514" Y="-28.777154631642" />
                  <Point X="-2.347899349619" Y="-27.735222768841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.500246930065" Y="-27.358149275329" />
                  <Point X="-3.364615460822" Y="-25.218762088346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.559382761914" Y="-24.736696101937" />
                  <Point X="-3.949199789905" Y="-23.771865100719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.025555793538" Y="-28.786650447223" />
                  <Point X="-2.408177677737" Y="-27.839628051814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.605028886775" Y="-27.352404212244" />
                  <Point X="-2.966667446891" Y="-26.457317366413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.10006098623" Y="-26.127156770865" />
                  <Point X="-3.44142719563" Y="-25.282245753778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.674282402037" Y="-24.705908893649" />
                  <Point X="-4.05741664498" Y="-23.757618365849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.111813444682" Y="-28.826754649313" />
                  <Point X="-2.468456005854" Y="-27.944033334788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.694865122408" Y="-27.383651106912" />
                  <Point X="-3.029380106845" Y="-26.555697466661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.213506974832" Y="-26.099967476344" />
                  <Point X="-3.529500166573" Y="-25.317856881698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.789182055568" Y="-24.675121652177" />
                  <Point X="-4.165633500054" Y="-23.74337163098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.192493784011" Y="-28.880663182553" />
                  <Point X="-2.528734333972" Y="-28.048438617761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777945918596" Y="-27.431618300938" />
                  <Point X="-3.106419758337" Y="-26.618617018503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.312261635622" Y="-26.109140494153" />
                  <Point X="-3.62195223765" Y="-25.342629356448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.904081709098" Y="-24.644334410704" />
                  <Point X="-4.273850355128" Y="-23.72912489611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.27317412334" Y="-28.934571715794" />
                  <Point X="-2.589012662089" Y="-28.152843900734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.861026807098" Y="-27.479585266479" />
                  <Point X="-3.184632951928" Y="-26.678631951726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.409547846978" Y="-26.121948051851" />
                  <Point X="-3.714404308726" Y="-25.367401831198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.018981362629" Y="-24.613547169232" />
                  <Point X="-4.382067210203" Y="-23.71487816124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.353845667636" Y="-28.988502017504" />
                  <Point X="-2.649290990206" Y="-28.257249183708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.944107747574" Y="-27.527552103379" />
                  <Point X="-3.262846145519" Y="-26.738646884948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506833929424" Y="-26.134755928611" />
                  <Point X="-3.806856379803" Y="-25.392174305948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.133881016159" Y="-24.582759927759" />
                  <Point X="-4.490284065277" Y="-23.70063142637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.426435398274" Y="-29.062435509953" />
                  <Point X="-2.709569318324" Y="-28.361654466681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027188688051" Y="-27.57551894028" />
                  <Point X="-3.34105933911" Y="-26.79866181817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.60412001187" Y="-26.147563805371" />
                  <Point X="-3.899308450879" Y="-25.416946780698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.248780669689" Y="-24.551972686286" />
                  <Point X="-4.598500920351" Y="-23.6863846915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.493555100024" Y="-29.149907798987" />
                  <Point X="-2.769847646441" Y="-28.466059749655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.110269628527" Y="-27.62348577718" />
                  <Point X="-3.4192725327" Y="-26.858676751393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.701406094316" Y="-26.160371682132" />
                  <Point X="-3.991760521956" Y="-25.441719255448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.36368032322" Y="-24.521185444814" />
                  <Point X="-4.661383735243" Y="-23.784343643497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.62020765187" Y="-29.090031113405" />
                  <Point X="-2.830125974558" Y="-28.570465032628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.193350569003" Y="-27.671452614081" />
                  <Point X="-3.497485726291" Y="-26.918691684615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.798692176761" Y="-26.173179558892" />
                  <Point X="-4.084212593032" Y="-25.466491730198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.47857997675" Y="-24.490398203341" />
                  <Point X="-4.702516637457" Y="-23.936135518427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.757669533649" Y="-29.00340039741" />
                  <Point X="-2.890404302676" Y="-28.674870315602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.276431509479" Y="-27.719419450981" />
                  <Point X="-3.575698919882" Y="-26.978706617838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.895978259207" Y="-26.185987435652" />
                  <Point X="-4.176664664109" Y="-25.491264204948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.593479630281" Y="-24.459610961869" />
                  <Point X="-4.739556573046" Y="-24.098057841243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.906397840627" Y="-28.88888430052" />
                  <Point X="-2.950682630793" Y="-28.779275598575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.359512449956" Y="-27.767386287881" />
                  <Point X="-3.653912113473" Y="-27.03872155106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.993264341653" Y="-26.198795312412" />
                  <Point X="-4.269116735185" Y="-25.516036679698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708379283811" Y="-24.428823720396" />
                  <Point X="-4.767023164445" Y="-24.283675022406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442593390432" Y="-27.815353124782" />
                  <Point X="-3.732125307063" Y="-27.098736484283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.090550424099" Y="-26.211603189173" />
                  <Point X="-4.361568806262" Y="-25.540809154448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.525674330908" Y="-27.863319961682" />
                  <Point X="-3.810338500654" Y="-27.158751417505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.187836506545" Y="-26.224411065933" />
                  <Point X="-4.454020877338" Y="-25.565581629198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.608755271384" Y="-27.911286798583" />
                  <Point X="-3.888551694245" Y="-27.218766350728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.285122588991" Y="-26.237218942693" />
                  <Point X="-4.546472948415" Y="-25.590354103948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.69183621186" Y="-27.959253635483" />
                  <Point X="-3.966764887836" Y="-27.27878128395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.382408671437" Y="-26.250026819454" />
                  <Point X="-4.638925019491" Y="-25.615126578698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.774917152337" Y="-28.007220472384" />
                  <Point X="-4.044978081426" Y="-27.338796217173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.479694753883" Y="-26.262834696214" />
                  <Point X="-4.731377090568" Y="-25.639899053448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963705290765" Y="-27.793552813322" />
                  <Point X="-4.123191275017" Y="-27.398811150395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.576980836329" Y="-26.275642572974" />
                  <Point X="-4.763752932294" Y="-25.813365413666" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000163818359" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.834470397949" Y="-29.89158984375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.315979553223" Y="-28.307814453125" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.274335906982" Y="-28.2663984375" />
                  <Point X="0.044789520264" Y="-28.19515625" />
                  <Point X="0.02097677803" Y="-28.187765625" />
                  <Point X="-0.008664756775" Y="-28.187765625" />
                  <Point X="-0.238211135864" Y="-28.2590078125" />
                  <Point X="-0.262023895264" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.436618408203" Y="-28.49937109375" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.49687701416" Y="-28.677623046875" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-1.073546142578" Y="-29.943248046875" />
                  <Point X="-1.10023425293" Y="-29.93806640625" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.364521850586" Y="-29.25906640625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.597619873047" Y="-29.017291015625" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.6492421875" Y="-28.985763671875" />
                  <Point X="-1.929733764648" Y="-28.96737890625" />
                  <Point X="-1.958831420898" Y="-28.96547265625" />
                  <Point X="-1.989879272461" Y="-28.97379296875" />
                  <Point X="-2.223599853516" Y="-29.129958984375" />
                  <Point X="-2.247845703125" Y="-29.14616015625" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.280871582031" Y="-29.184841796875" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.816703613281" Y="-29.19183984375" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-3.224782714844" Y="-28.883533203125" />
                  <Point X="-3.228580810547" Y="-28.880609375" />
                  <Point X="-3.180500732422" Y="-28.79733203125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513980957031" Y="-27.56876171875" />
                  <Point X="-2.529695068359" Y="-27.553048828125" />
                  <Point X="-2.531325683594" Y="-27.55141796875" />
                  <Point X="-2.560155517578" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.712871826172" Y="-27.613439453125" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.130788574219" Y="-27.88774609375" />
                  <Point X="-4.161701660156" Y="-27.8471328125" />
                  <Point X="-4.426211425781" Y="-27.40358984375" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-4.343885253906" Y="-27.328666015625" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822998047" Y="-26.39601953125" />
                  <Point X="-3.138842529297" Y="-26.36906640625" />
                  <Point X="-3.138118408203" Y="-26.366271484375" />
                  <Point X="-3.140325683594" Y="-26.33459765625" />
                  <Point X="-3.161159912109" Y="-26.310638671875" />
                  <Point X="-3.185153564453" Y="-26.296517578125" />
                  <Point X="-3.187642578125" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.372518066406" Y="-26.308712890625" />
                  <Point X="-4.803283691406" Y="-26.497076171875" />
                  <Point X="-4.915301757812" Y="-26.058529296875" />
                  <Point X="-4.927392089844" Y="-26.011197265625" />
                  <Point X="-4.997375" Y="-25.5218828125" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-4.9017734375" Y="-25.488853515625" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541894042969" Y="-25.121423828125" />
                  <Point X="-3.516749511719" Y="-25.10397265625" />
                  <Point X="-3.514141113281" Y="-25.102162109375" />
                  <Point X="-3.494899414062" Y="-25.07591015625" />
                  <Point X="-3.486517822266" Y="-25.048904296875" />
                  <Point X="-3.4856484375" Y="-25.046103515625" />
                  <Point X="-3.485647949219" Y="-25.0164609375" />
                  <Point X="-3.494027099609" Y="-24.989462890625" />
                  <Point X="-3.494896484375" Y="-24.98666015625" />
                  <Point X="-3.514141113281" Y="-24.9603984375" />
                  <Point X="-3.539285644531" Y="-24.942947265625" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.696920410156" Y="-24.896546875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.925437011719" Y="-24.05623828125" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.776774414062" Y="-23.483724609375" />
                  <Point X="-4.773516601562" Y="-23.471701171875" />
                  <Point X="-4.716900390625" Y="-23.47915625" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731704345703" Y="-23.6041328125" />
                  <Point X="-3.676051513672" Y="-23.5865859375" />
                  <Point X="-3.670278320312" Y="-23.584765625" />
                  <Point X="-3.651535644531" Y="-23.5739453125" />
                  <Point X="-3.639121337891" Y="-23.556216796875" />
                  <Point X="-3.616790527344" Y="-23.5023046875" />
                  <Point X="-3.614473876953" Y="-23.496712890625" />
                  <Point X="-3.610714111328" Y="-23.475396484375" />
                  <Point X="-3.616316650391" Y="-23.45448828125" />
                  <Point X="-3.643261230469" Y="-23.402728515625" />
                  <Point X="-3.646056396484" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.7399609375" Y="-23.3193984375" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.190291503906" Y="-22.264865234375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.786850585938" Y="-21.73334375" />
                  <Point X="-3.774671142578" Y="-21.717689453125" />
                  <Point X="-3.750994140625" Y="-21.731359375" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138512695312" Y="-22.07956640625" />
                  <Point X="-3.06100390625" Y="-22.08634765625" />
                  <Point X="-3.052963378906" Y="-22.08705078125" />
                  <Point X="-3.031506103516" Y="-22.084224609375" />
                  <Point X="-3.013253173828" Y="-22.072595703125" />
                  <Point X="-2.958236816406" Y="-22.017580078125" />
                  <Point X="-2.952529541016" Y="-22.011873046875" />
                  <Point X="-2.940899658203" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.94485546875" Y="-21.894650390625" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-2.987514648438" Y="-21.804572265625" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.805606689453" Y="-20.86609765625" />
                  <Point X="-2.752872070313" Y="-20.825666015625" />
                  <Point X="-2.165153808594" Y="-20.499142578125" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246826172" Y="-20.726337890625" />
                  <Point X="-1.864985229492" Y="-20.771244140625" />
                  <Point X="-1.856036499023" Y="-20.775904296875" />
                  <Point X="-1.835126464844" Y="-20.781509765625" />
                  <Point X="-1.813807373047" Y="-20.77775" />
                  <Point X="-1.723954589844" Y="-20.74053125" />
                  <Point X="-1.714633422852" Y="-20.736669921875" />
                  <Point X="-1.696904418945" Y="-20.72425390625" />
                  <Point X="-1.686083007812" Y="-20.70551171875" />
                  <Point X="-1.656837768555" Y="-20.6127578125" />
                  <Point X="-1.653803710938" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.655947631836" Y="-20.55096484375" />
                  <Point X="-1.689137695313" Y="-20.298861328125" />
                  <Point X="-1.036358032227" Y="-20.11584375" />
                  <Point X="-0.968084289551" Y="-20.096703125" />
                  <Point X="-0.255593933105" Y="-20.01331640625" />
                  <Point X="-0.224200088501" Y="-20.009642578125" />
                  <Point X="-0.217222290039" Y="-20.03568359375" />
                  <Point X="-0.168925888062" Y="-20.0227421875" />
                  <Point X="-0.217222137451" Y="-20.03568359375" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282114029" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594032288" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.072614936829" Y="-20.6213125" />
                  <Point X="0.236648391724" Y="-20.009130859375" />
                  <Point X="0.800599304199" Y="-20.068193359375" />
                  <Point X="0.860208557129" Y="-20.074435546875" />
                  <Point X="1.449673095703" Y="-20.21675" />
                  <Point X="1.508457641602" Y="-20.230943359375" />
                  <Point X="1.892978271484" Y="-20.370412109375" />
                  <Point X="1.931044433594" Y="-20.38421875" />
                  <Point X="2.302039306641" Y="-20.557720703125" />
                  <Point X="2.338683349609" Y="-20.574857421875" />
                  <Point X="2.697101318359" Y="-20.783671875" />
                  <Point X="2.732532470703" Y="-20.804314453125" />
                  <Point X="3.068740722656" Y="-21.04340625" />
                  <Point X="3.010498535156" Y="-21.14428515625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224851806641" Y="-22.508486328125" />
                  <Point X="2.205400146484" Y="-22.5812265625" />
                  <Point X="2.202044433594" Y="-22.607673828125" />
                  <Point X="2.209629150391" Y="-22.670572265625" />
                  <Point X="2.210415771484" Y="-22.67709765625" />
                  <Point X="2.218684570312" Y="-22.69919140625" />
                  <Point X="2.257604736328" Y="-22.75655078125" />
                  <Point X="2.274939941406" Y="-22.775796875" />
                  <Point X="2.332298339844" Y="-22.814716796875" />
                  <Point X="2.338240966797" Y="-22.81875" />
                  <Point X="2.360334716797" Y="-22.827021484375" />
                  <Point X="2.423234375" Y="-22.83460546875" />
                  <Point X="2.448662597656" Y="-22.8340546875" />
                  <Point X="2.521403076172" Y="-22.8146015625" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.681300292969" Y="-22.726599609375" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.181580566406" Y="-22.228919921875" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.316408203125" Y="-22.618265625" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279373535156" Y="-23.4161640625" />
                  <Point X="3.227022216797" Y="-23.4844609375" />
                  <Point X="3.213119140625" Y="-23.508501953125" />
                  <Point X="3.193618164062" Y="-23.578232421875" />
                  <Point X="3.191595214844" Y="-23.585466796875" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.206787841797" Y="-23.686619140625" />
                  <Point X="3.215646728516" Y="-23.712046875" />
                  <Point X="3.2591875" Y="-23.7782265625" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280947998047" Y="-23.8011796875" />
                  <Point X="3.344040039062" Y="-23.8366953125" />
                  <Point X="3.344039794922" Y="-23.8366953125" />
                  <Point X="3.368563964844" Y="-23.846380859375" />
                  <Point X="3.453874755859" Y="-23.857654296875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.609047363281" Y="-23.841287109375" />
                  <Point X="4.848975097656" Y="-23.678046875" />
                  <Point X="4.9301640625" Y="-24.011548828125" />
                  <Point X="4.939189453125" Y="-24.048619140625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.919039550781" Y="-24.4465625" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729088378906" Y="-24.767181640625" />
                  <Point X="3.645273193359" Y="-24.815626953125" />
                  <Point X="3.622262695312" Y="-24.833076171875" />
                  <Point X="3.571973632812" Y="-24.89715625" />
                  <Point X="3.566756835938" Y="-24.9038046875" />
                  <Point X="3.556985351562" Y="-24.925265625" />
                  <Point X="3.540222167969" Y="-25.012796875" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.555246337891" Y="-25.12821484375" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566756835938" Y="-25.158755859375" />
                  <Point X="3.617045898438" Y="-25.2228359375" />
                  <Point X="3.636577392578" Y="-25.241908203125" />
                  <Point X="3.720392578125" Y="-25.290353515625" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="3.863358154297" Y="-25.33312890625" />
                  <Point X="4.998068359375" Y="-25.637173828125" />
                  <Point X="4.953471191406" Y="-25.932978515625" />
                  <Point X="4.948432128906" Y="-25.966400390625" />
                  <Point X="4.874545898438" Y="-26.290177734375" />
                  <Point X="4.777982421875" Y="-26.277466796875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836425781" Y="-26.098341796875" />
                  <Point X="3.230336914062" Y="-26.134095703125" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.185446777344" Y="-26.1546953125" />
                  <Point X="3.086017333984" Y="-26.274279296875" />
                  <Point X="3.075702636719" Y="-26.28668359375" />
                  <Point X="3.064357666016" Y="-26.3140703125" />
                  <Point X="3.050106933594" Y="-26.468935546875" />
                  <Point X="3.048628662109" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.147396728516" Y="-26.65822265625" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.281854248047" Y="-26.77255078125" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.218334472656" Y="-27.77916015625" />
                  <Point X="4.204133789063" Y="-27.802140625" />
                  <Point X="4.0566875" Y="-28.011638671875" />
                  <Point X="3.970062011719" Y="-27.961625" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.541560302734" Y="-27.21795703125" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489075683594" Y="-27.21924609375" />
                  <Point X="2.326430175781" Y="-27.30484375" />
                  <Point X="2.309557617188" Y="-27.313724609375" />
                  <Point X="2.288599609375" Y="-27.334685546875" />
                  <Point X="2.203000488281" Y="-27.497330078125" />
                  <Point X="2.194120605469" Y="-27.514203125" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.224520751953" Y="-27.74215625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.306958251953" Y="-27.904787109375" />
                  <Point X="2.986673828125" Y="-29.082087890625" />
                  <Point X="2.852153076172" Y="-29.178173828125" />
                  <Point X="2.835309082031" Y="-29.190205078125" />
                  <Point X="2.679775634766" Y="-29.290880859375" />
                  <Point X="2.611561279297" Y="-29.20198046875" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670547729492" Y="-27.980466796875" />
                  <Point X="1.477455200195" Y="-27.856326171875" />
                  <Point X="1.457424194336" Y="-27.84344921875" />
                  <Point X="1.425805541992" Y="-27.83571875" />
                  <Point X="1.214626220703" Y="-27.855150390625" />
                  <Point X="1.19271875" Y="-27.857166015625" />
                  <Point X="1.165331420898" Y="-27.868509765625" />
                  <Point X="1.002263977051" Y="-28.004095703125" />
                  <Point X="0.985347717285" Y="-28.018162109375" />
                  <Point X="0.96845690918" Y="-28.04598828125" />
                  <Point X="0.919700561523" Y="-28.2703046875" />
                  <Point X="0.914642700195" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.934579345703" Y="-28.4675703125" />
                  <Point X="1.127642333984" Y="-29.93402734375" />
                  <Point X="1.010291442871" Y="-29.959751953125" />
                  <Point X="0.994323181152" Y="-29.963251953125" />
                  <Point X="0.860200744629" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#208" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.175081857811" Y="5.008538895752" Z="2.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.3" />
                  <Point X="-0.267562846851" Y="5.067625162536" Z="2.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.3" />
                  <Point X="-1.056011556553" Y="4.963581276655" Z="2.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.3" />
                  <Point X="-1.711675831429" Y="4.47379101261" Z="2.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.3" />
                  <Point X="-1.710679761189" Y="4.433558384292" Z="2.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.3" />
                  <Point X="-1.749246320335" Y="4.336943110796" Z="2.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.3" />
                  <Point X="-1.848048242248" Y="4.304383266851" Z="2.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.3" />
                  <Point X="-2.115494499671" Y="4.58540888818" Z="2.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.3" />
                  <Point X="-2.19559270463" Y="4.575844749492" Z="2.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.3" />
                  <Point X="-2.842184969013" Y="4.204863058547" Z="2.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.3" />
                  <Point X="-3.036971817282" Y="3.201709137485" Z="2.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.3" />
                  <Point X="-3.000821215918" Y="3.132272282341" Z="2.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.3" />
                  <Point X="-2.999747307384" Y="3.049056260502" Z="2.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.3" />
                  <Point X="-3.062804183266" Y="2.994743483255" Z="2.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.3" />
                  <Point X="-3.732150210812" Y="3.343222131464" Z="2.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.3" />
                  <Point X="-3.832469669063" Y="3.328638930847" Z="2.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.3" />
                  <Point X="-4.239629434203" Y="2.791619920246" Z="2.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.3" />
                  <Point X="-3.776554861353" Y="1.672214573506" Z="2.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.3" />
                  <Point X="-3.693767095323" Y="1.605464601615" Z="2.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.3" />
                  <Point X="-3.669139004608" Y="1.548111683173" Z="2.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.3" />
                  <Point X="-3.697243240715" Y="1.492379787339" Z="2.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.3" />
                  <Point X="-4.716530088512" Y="1.601697398357" Z="2.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.3" />
                  <Point X="-4.831189525224" Y="1.560634153161" Z="2.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.3" />
                  <Point X="-4.981054388172" Y="0.982360130902" Z="2.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.3" />
                  <Point X="-3.716017224558" Y="0.086436286216" Z="2.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.3" />
                  <Point X="-3.573952293499" Y="0.047258624225" Z="2.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.3" />
                  <Point X="-3.547938363747" Y="0.027005450117" Z="2.3" />
                  <Point X="-3.539556741714" Y="0" Z="2.3" />
                  <Point X="-3.540426237388" Y="-0.002801500944" Z="2.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.3" />
                  <Point X="-3.551416301624" Y="-0.031617358909" Z="2.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.3" />
                  <Point X="-4.920870564215" Y="-0.40927577054" Z="2.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.3" />
                  <Point X="-5.053027430509" Y="-0.497681215694" Z="2.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.3" />
                  <Point X="-4.969902039786" Y="-1.039624501403" Z="2.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.3" />
                  <Point X="-3.372147617408" Y="-1.327004652437" Z="2.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.3" />
                  <Point X="-3.216669838293" Y="-1.308328254216" Z="2.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.3" />
                  <Point X="-3.193400437328" Y="-1.32524570409" Z="2.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.3" />
                  <Point X="-4.380479141201" Y="-2.257718436053" Z="2.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.3" />
                  <Point X="-4.475310773542" Y="-2.397919649902" Z="2.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.3" />
                  <Point X="-4.176730078599" Y="-2.88674935967" Z="2.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.3" />
                  <Point X="-2.694027794091" Y="-2.62545906471" Z="2.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.3" />
                  <Point X="-2.571208942036" Y="-2.557121527095" Z="2.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.3" />
                  <Point X="-3.229957948458" Y="-3.741050588401" Z="2.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.3" />
                  <Point X="-3.26144253424" Y="-3.891869903608" Z="2.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.3" />
                  <Point X="-2.849181077579" Y="-4.203070643405" Z="2.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.3" />
                  <Point X="-2.247359792249" Y="-4.183999115925" Z="2.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.3" />
                  <Point X="-2.201976496723" Y="-4.140251642886" Z="2.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.3" />
                  <Point X="-1.939158277608" Y="-3.985991535096" Z="2.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.3" />
                  <Point X="-1.63674308558" Y="-4.023602585412" Z="2.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.3" />
                  <Point X="-1.419717115145" Y="-4.237540330011" Z="2.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.3" />
                  <Point X="-1.408566892278" Y="-4.845078105354" Z="2.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.3" />
                  <Point X="-1.385307009081" Y="-4.886653900794" Z="2.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.3" />
                  <Point X="-1.089230409292" Y="-4.961051746018" Z="2.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.3" />
                  <Point X="-0.454736685573" Y="-3.659284449193" Z="2.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.3" />
                  <Point X="-0.401698312223" Y="-3.496601230557" Z="2.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.3" />
                  <Point X="-0.229546321777" Y="-3.275482075668" Z="2.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.3" />
                  <Point X="0.023812757584" Y="-3.21162996962" Z="2.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.3" />
                  <Point X="0.268747547209" Y="-3.305044525211" Z="2.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.3" />
                  <Point X="0.780018174828" Y="-4.873251580623" Z="2.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.3" />
                  <Point X="0.834618103204" Y="-5.010683750354" Z="2.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.3" />
                  <Point X="1.014838066144" Y="-4.977312709439" Z="2.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.3" />
                  <Point X="0.977995661709" Y="-3.429765071398" Z="2.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.3" />
                  <Point X="0.962403685218" Y="-3.249643380016" Z="2.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.3" />
                  <Point X="1.028076358918" Y="-3.011260629196" Z="2.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.3" />
                  <Point X="1.213051145272" Y="-2.8736595015" Z="2.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.3" />
                  <Point X="1.444261539678" Y="-2.867104724982" Z="2.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.3" />
                  <Point X="2.565737666781" Y="-4.201138307297" Z="2.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.3" />
                  <Point X="2.680395691756" Y="-4.314773651907" Z="2.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.3" />
                  <Point X="2.875059809325" Y="-4.187579713786" Z="2.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.3" />
                  <Point X="2.344103645653" Y="-2.848507481002" Z="2.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.3" />
                  <Point X="2.267568934327" Y="-2.701988675889" Z="2.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.3" />
                  <Point X="2.241091029694" Y="-2.489335613581" Z="2.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.3" />
                  <Point X="2.343562764791" Y="-2.317810280273" Z="2.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.3" />
                  <Point X="2.526518127624" Y="-2.235879075164" Z="2.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.3" />
                  <Point X="3.938906116105" Y="-2.973645854622" Z="2.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.3" />
                  <Point X="4.081525994341" Y="-3.023194805725" Z="2.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.3" />
                  <Point X="4.254712077643" Y="-2.774163917398" Z="2.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.3" />
                  <Point X="3.306136336338" Y="-1.701602942527" Z="2.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.3" />
                  <Point X="3.183298862798" Y="-1.59990356717" Z="2.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.3" />
                  <Point X="3.093740841843" Y="-1.44223707401" Z="2.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.3" />
                  <Point X="3.118306102067" Y="-1.274966915628" Z="2.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.3" />
                  <Point X="3.234800429135" Y="-1.151675056278" Z="2.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.3" />
                  <Point X="4.765300121862" Y="-1.295757757065" Z="2.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.3" />
                  <Point X="4.914942317134" Y="-1.279639006519" Z="2.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.3" />
                  <Point X="4.996755932335" Y="-0.909152639149" Z="2.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.3" />
                  <Point X="3.870143194204" Y="-0.25355147971" Z="2.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.3" />
                  <Point X="3.73925785497" Y="-0.215784860795" Z="2.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.3" />
                  <Point X="3.650225807876" Y="-0.160690642134" Z="2.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.3" />
                  <Point X="3.598197754771" Y="-0.087530283664" Z="2.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.3" />
                  <Point X="3.583173695653" Y="0.009080247515" Z="2.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.3" />
                  <Point X="3.605153630524" Y="0.103258096441" Z="2.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.3" />
                  <Point X="3.664137559383" Y="0.172363983447" Z="2.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.3" />
                  <Point X="4.925824623103" Y="0.536420453059" Z="2.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.3" />
                  <Point X="5.041821087001" Y="0.608944543603" Z="2.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.3" />
                  <Point X="4.972589305268" Y="1.031560581445" Z="2.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.3" />
                  <Point X="3.596365071641" Y="1.239565948829" Z="2.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.3" />
                  <Point X="3.454271412851" Y="1.223193718625" Z="2.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.3" />
                  <Point X="3.36241508817" Y="1.238153146198" Z="2.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.3" />
                  <Point X="3.294801710001" Y="1.280536432323" Z="2.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.3" />
                  <Point X="3.249600322321" Y="1.354764874685" Z="2.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.3" />
                  <Point X="3.235615030407" Y="1.439582957712" Z="2.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.3" />
                  <Point X="3.260546826895" Y="1.516398662658" Z="2.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.3" />
                  <Point X="4.340690402616" Y="2.373347800183" Z="2.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.3" />
                  <Point X="4.427656328182" Y="2.487642336438" Z="2.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.3" />
                  <Point X="4.216010020457" Y="2.831564161416" Z="2.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.3" />
                  <Point X="2.650144297997" Y="2.347981594241" Z="2.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.3" />
                  <Point X="2.50233196065" Y="2.264980879976" Z="2.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.3" />
                  <Point X="2.423066539097" Y="2.246315983013" Z="2.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.3" />
                  <Point X="2.354216517416" Y="2.257938029164" Z="2.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.3" />
                  <Point X="2.292820571071" Y="2.302808342966" Z="2.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.3" />
                  <Point X="2.253113719746" Y="2.366691904736" Z="2.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.3" />
                  <Point X="2.247547062681" Y="2.437137615232" Z="2.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.3" />
                  <Point X="3.047643758618" Y="3.861994783958" Z="2.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.3" />
                  <Point X="3.093368918636" Y="4.027334466271" Z="2.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.3" />
                  <Point X="2.716115440868" Y="4.290811073191" Z="2.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.3" />
                  <Point X="2.317064829412" Y="4.518851111919" Z="2.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.3" />
                  <Point X="1.903870687266" Y="4.707872974747" Z="2.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.3" />
                  <Point X="1.455252497482" Y="4.863133407318" Z="2.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.3" />
                  <Point X="0.799651051753" Y="5.012816077128" Z="2.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.3" />
                  <Point X="0.018162703742" Y="4.422908610199" Z="2.3" />
                  <Point X="0" Y="4.355124473572" Z="2.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>