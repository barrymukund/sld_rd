<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#179" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2347" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.807951477051" Y="-29.425568359375" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542363037109" Y="-28.467375" />
                  <Point X="0.442368682861" Y="-28.323302734375" />
                  <Point X="0.378635528564" Y="-28.231474609375" />
                  <Point X="0.356748565674" Y="-28.209017578125" />
                  <Point X="0.330493225098" Y="-28.189775390625" />
                  <Point X="0.302494873047" Y="-28.175669921875" />
                  <Point X="0.147759277344" Y="-28.127646484375" />
                  <Point X="0.049135734558" Y="-28.097037109375" />
                  <Point X="0.02098301506" Y="-28.092767578125" />
                  <Point X="-0.008657706261" Y="-28.092765625" />
                  <Point X="-0.036824085236" Y="-28.09703515625" />
                  <Point X="-0.191559692383" Y="-28.145060546875" />
                  <Point X="-0.290183227539" Y="-28.17566796875" />
                  <Point X="-0.318183410645" Y="-28.189775390625" />
                  <Point X="-0.344439331055" Y="-28.20901953125" />
                  <Point X="-0.366323577881" Y="-28.2314765625" />
                  <Point X="-0.466318084717" Y="-28.37555078125" />
                  <Point X="-0.530051208496" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.706921813965" Y="-29.09446875" />
                  <Point X="-0.916584899902" Y="-29.87694140625" />
                  <Point X="-0.966941650391" Y="-29.86716796875" />
                  <Point X="-1.079321533203" Y="-29.845353515625" />
                  <Point X="-1.246417602539" Y="-29.802361328125" />
                  <Point X="-1.244413085938" Y="-29.787134765625" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.253475097656" Y="-29.3303828125" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287938354492" Y="-29.182966796875" />
                  <Point X="-1.304010620117" Y="-29.15512890625" />
                  <Point X="-1.323644897461" Y="-29.131205078125" />
                  <Point X="-1.466105712891" Y="-29.00626953125" />
                  <Point X="-1.556905639648" Y="-28.926640625" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.643027587891" Y="-28.890966796875" />
                  <Point X="-1.832104858398" Y="-28.87857421875" />
                  <Point X="-1.952616699219" Y="-28.87067578125" />
                  <Point X="-1.983414916992" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.20020703125" Y="-29.000072265625" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312789306641" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.422646484375" Y="-29.213552734375" />
                  <Point X="-2.480147460938" Y="-29.28848828125" />
                  <Point X="-2.6369609375" Y="-29.19139453125" />
                  <Point X="-2.801724853516" Y="-29.089376953125" />
                  <Point X="-3.045924316406" Y="-28.9013515625" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.878414794922" Y="-28.464103515625" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405576171875" Y="-27.5851875" />
                  <Point X="-2.4145625" Y="-27.5555703125" />
                  <Point X="-2.428781982422" Y="-27.526740234375" />
                  <Point X="-2.446807861328" Y="-27.5015859375" />
                  <Point X="-2.464151855469" Y="-27.4842421875" />
                  <Point X="-2.489304931641" Y="-27.466216796875" />
                  <Point X="-2.518134277344" Y="-27.451998046875" />
                  <Point X="-2.54775390625" Y="-27.443013671875" />
                  <Point X="-2.578689941406" Y="-27.444025390625" />
                  <Point X="-2.610217773438" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.141100097656" Y="-27.75098046875" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.952699951172" Y="-27.964865234375" />
                  <Point X="-4.082860351562" Y="-27.793861328125" />
                  <Point X="-4.25794140625" Y="-27.50027734375" />
                  <Point X="-4.306142089844" Y="-27.419451171875" />
                  <Point X="-3.901450683594" Y="-27.108919921875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.35965625" />
                  <Point X="-3.045557128906" Y="-26.327982421875" />
                  <Point X="-3.052560058594" Y="-26.298234375" />
                  <Point X="-3.068645996094" Y="-26.27225" />
                  <Point X="-3.089480957031" Y="-26.248294921875" />
                  <Point X="-3.112976806641" Y="-26.228765625" />
                  <Point X="-3.139459472656" Y="-26.2131796875" />
                  <Point X="-3.168722900391" Y="-26.20195703125" />
                  <Point X="-3.200607421875" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-3.86555078125" Y="-26.277802734375" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.783170410156" Y="-26.191953125" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.880399902344" Y="-25.668775390625" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.439065917969" Y="-25.46322265625" />
                  <Point X="-3.532875732422" Y="-25.22041015625" />
                  <Point X="-3.517491699219" Y="-25.214828125" />
                  <Point X="-3.487729736328" Y="-25.199470703125" />
                  <Point X="-3.470780029297" Y="-25.18770703125" />
                  <Point X="-3.459976806641" Y="-25.180208984375" />
                  <Point X="-3.437521240234" Y="-25.158326171875" />
                  <Point X="-3.418277832031" Y="-25.132072265625" />
                  <Point X="-3.404168945312" Y="-25.1040703125" />
                  <Point X="-3.39491796875" Y="-25.074263671875" />
                  <Point X="-3.3906484375" Y="-25.04610546875" />
                  <Point X="-3.390647705078" Y="-25.016462890625" />
                  <Point X="-3.394916748047" Y="-24.988302734375" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.418276123047" Y="-24.9304921875" />
                  <Point X="-3.437520263672" Y="-24.904236328125" />
                  <Point X="-3.459976806641" Y="-24.8823515625" />
                  <Point X="-3.476926513672" Y="-24.870587890625" />
                  <Point X="-3.48479296875" Y="-24.8656796875" />
                  <Point X="-3.511164306641" Y="-24.8509609375" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-4.110453125" Y="-24.687390625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.857295898438" Y="-24.24473828125" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.731242675781" Y="-23.678923828125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.415245605469" Y="-23.614689453125" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744984863281" Y="-23.700658203125" />
                  <Point X="-3.723423583984" Y="-23.698771484375" />
                  <Point X="-3.703138671875" Y="-23.694736328125" />
                  <Point X="-3.665623535156" Y="-23.682908203125" />
                  <Point X="-3.641712646484" Y="-23.675369140625" />
                  <Point X="-3.622784179688" Y="-23.667041015625" />
                  <Point X="-3.604040039063" Y="-23.656220703125" />
                  <Point X="-3.587355224609" Y="-23.64398828125" />
                  <Point X="-3.573714355469" Y="-23.62843359375" />
                  <Point X="-3.561299804688" Y="-23.610703125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.536298339844" Y="-23.556228515625" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532050292969" Y="-23.41062109375" />
                  <Point X="-3.550213378906" Y="-23.37573046875" />
                  <Point X="-3.561790039062" Y="-23.3534921875" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-3.933435302734" Y="-23.0511953125" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.208632324219" Y="-22.4847421875" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.834151855469" Y="-21.94885546875" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.605610351562" Y="-21.9249921875" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187730224609" Y="-22.163658203125" />
                  <Point X="-3.167088867188" Y="-22.17016796875" />
                  <Point X="-3.146793945312" Y="-22.174205078125" />
                  <Point X="-3.094545898438" Y="-22.178775390625" />
                  <Point X="-3.061244628906" Y="-22.181689453125" />
                  <Point X="-3.040561767578" Y="-22.18123828125" />
                  <Point X="-3.019102783203" Y="-22.178412109375" />
                  <Point X="-2.999013916016" Y="-22.17349609375" />
                  <Point X="-2.980465087891" Y="-22.16434765625" />
                  <Point X="-2.962210693359" Y="-22.15271875" />
                  <Point X="-2.946078125" Y="-22.139771484375" />
                  <Point X="-2.908991943359" Y="-22.102685546875" />
                  <Point X="-2.885354492188" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.848007080078" Y="-21.9116328125" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-3.016604003906" Y="-21.5641875" />
                  <Point X="-3.183332519531" Y="-21.27540625" />
                  <Point X="-2.922648681641" Y="-21.075541015625" />
                  <Point X="-2.700620605469" Y="-20.9053125" />
                  <Point X="-2.311607421875" Y="-20.6891875" />
                  <Point X="-2.167036621094" Y="-20.6088671875" />
                  <Point X="-2.165327636719" Y="-20.61109375" />
                  <Point X="-2.043195678711" Y="-20.7702578125" />
                  <Point X="-2.028891967773" Y="-20.78519921875" />
                  <Point X="-2.01231237793" Y="-20.799111328125" />
                  <Point X="-1.995114868164" Y="-20.810603515625" />
                  <Point X="-1.936963012695" Y="-20.840876953125" />
                  <Point X="-1.899898803711" Y="-20.860171875" />
                  <Point X="-1.880625732422" Y="-20.86766796875" />
                  <Point X="-1.859719238281" Y="-20.873271484375" />
                  <Point X="-1.839269287109" Y="-20.876419921875" />
                  <Point X="-1.818622436523" Y="-20.87506640625" />
                  <Point X="-1.797307128906" Y="-20.871306640625" />
                  <Point X="-1.777452636719" Y="-20.865517578125" />
                  <Point X="-1.716883544922" Y="-20.840427734375" />
                  <Point X="-1.678278686523" Y="-20.8244375" />
                  <Point X="-1.660147094727" Y="-20.814490234375" />
                  <Point X="-1.642417236328" Y="-20.802076171875" />
                  <Point X="-1.626864868164" Y="-20.7884375" />
                  <Point X="-1.614633300781" Y="-20.771755859375" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734078125" />
                  <Point X="-1.575766235352" Y="-20.671552734375" />
                  <Point X="-1.563201171875" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.574420898438" Y="-20.442396484375" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.237057861328" Y="-20.270775390625" />
                  <Point X="-0.949623718262" Y="-20.19019140625" />
                  <Point X="-0.478031585693" Y="-20.13499609375" />
                  <Point X="-0.294711303711" Y="-20.113541015625" />
                  <Point X="-0.251924758911" Y="-20.27322265625" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113990784" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907154" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425949097" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.221438339233" Y="-20.43294921875" />
                  <Point X="0.307419616699" Y="-20.1120625" />
                  <Point X="0.59307208252" Y="-20.141978515625" />
                  <Point X="0.844030639648" Y="-20.168259765625" />
                  <Point X="1.234215576172" Y="-20.262462890625" />
                  <Point X="1.481038696289" Y="-20.3220546875" />
                  <Point X="1.734416503906" Y="-20.413955078125" />
                  <Point X="1.894645996094" Y="-20.472072265625" />
                  <Point X="2.140220458984" Y="-20.58691796875" />
                  <Point X="2.294555908203" Y="-20.659095703125" />
                  <Point X="2.531836914062" Y="-20.7973359375" />
                  <Point X="2.680977294922" Y="-20.8842265625" />
                  <Point X="2.904724853516" Y="-21.04334375" />
                  <Point X="2.943259033203" Y="-21.07074609375" />
                  <Point X="2.673808349609" Y="-21.53744921875" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142075683594" Y="-22.4600703125" />
                  <Point X="2.133076660156" Y="-22.483943359375" />
                  <Point X="2.119964355469" Y="-22.5329765625" />
                  <Point X="2.111606933594" Y="-22.56423046875" />
                  <Point X="2.108618896484" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.112840820312" Y="-22.66144921875" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140071044922" Y="-22.752529296875" />
                  <Point X="2.166306884766" Y="-22.7911953125" />
                  <Point X="2.183028808594" Y="-22.815837890625" />
                  <Point X="2.194469238281" Y="-22.829677734375" />
                  <Point X="2.221596923828" Y="-22.85440625" />
                  <Point X="2.26026171875" Y="-22.880642578125" />
                  <Point X="2.284905517578" Y="-22.897365234375" />
                  <Point X="2.304944580078" Y="-22.907724609375" />
                  <Point X="2.327031982422" Y="-22.9159921875" />
                  <Point X="2.348965087891" Y="-22.921337890625" />
                  <Point X="2.391365478516" Y="-22.92644921875" />
                  <Point X="2.418389892578" Y="-22.929708984375" />
                  <Point X="2.436466308594" Y="-22.93015625" />
                  <Point X="2.473208007812" Y="-22.925830078125" />
                  <Point X="2.522241699219" Y="-22.912716796875" />
                  <Point X="2.553494384766" Y="-22.904359375" />
                  <Point X="2.565286376953" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.169464599609" Y="-22.554453125" />
                  <Point X="3.967325439453" Y="-22.09380859375" />
                  <Point X="4.034806152344" Y="-22.187591796875" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.2480078125" Y="-22.516666015625" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.923611328125" Y="-22.799923828125" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221423339844" Y="-23.339759765625" />
                  <Point X="3.203974365234" Y="-23.358373046875" />
                  <Point X="3.168684570312" Y="-23.40441015625" />
                  <Point X="3.146192138672" Y="-23.43375390625" />
                  <Point X="3.13660546875" Y="-23.44908984375" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.108484375" Y="-23.529919921875" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.108530517578" Y="-23.68053125" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679443359" Y="-23.731021484375" />
                  <Point X="3.136282714844" Y="-23.76426171875" />
                  <Point X="3.165633056641" Y="-23.808873046875" />
                  <Point X="3.184340332031" Y="-23.837306640625" />
                  <Point X="3.198893798828" Y="-23.85455078125" />
                  <Point X="3.216137695313" Y="-23.870640625" />
                  <Point X="3.234345947266" Y="-23.883966796875" />
                  <Point X="3.27687890625" Y="-23.90791015625" />
                  <Point X="3.303988037109" Y="-23.923169921875" />
                  <Point X="3.320530029297" Y="-23.93050390625" />
                  <Point X="3.356120117188" Y="-23.9405625" />
                  <Point X="3.413627441406" Y="-23.948162109375" />
                  <Point X="3.450280761719" Y="-23.953005859375" />
                  <Point X="3.462698486328" Y="-23.95382421875" />
                  <Point X="3.488203369141" Y="-23.953015625" />
                  <Point X="4.040048583984" Y="-23.880365234375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.807940429688" Y="-23.9111171875" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.885243652344" Y="-24.319658203125" />
                  <Point X="4.890864257812" Y="-24.355759765625" />
                  <Point X="4.510575195312" Y="-24.457658203125" />
                  <Point X="3.716580078125" Y="-24.67041015625" />
                  <Point X="3.704788085938" Y="-24.674416015625" />
                  <Point X="3.681547607422" Y="-24.684931640625" />
                  <Point X="3.625048339844" Y="-24.717587890625" />
                  <Point X="3.589037597656" Y="-24.73840234375" />
                  <Point X="3.574313232422" Y="-24.74890234375" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.513630859375" Y="-24.81762109375" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480300537109" Y="-24.864431640625" />
                  <Point X="3.470526611328" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.452381103516" Y="-24.966400390625" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443483154297" Y="-25.021875" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.456478759766" Y="-25.117556640625" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470526855469" Y="-25.1766640625" />
                  <Point X="3.480301025391" Y="-25.19812890625" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.525923828125" Y="-25.26060546875" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.560001708984" Y="-25.301240234375" />
                  <Point X="3.589035644531" Y="-25.32415625" />
                  <Point X="3.645534912109" Y="-25.3568125" />
                  <Point X="3.681545654297" Y="-25.37762890625" />
                  <Point X="3.692698730469" Y="-25.383134765625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.222646972656" Y="-25.527751953125" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.876237792969" Y="-25.80801171875" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.804662597656" Y="-26.16941015625" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.345193359375" Y="-26.12466796875" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.263771240234" Y="-26.029611328125" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163973876953" Y="-26.05659765625" />
                  <Point X="3.136147216797" Y="-26.073490234375" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.045372314453" Y="-26.174572265625" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.960151367188" Y="-26.409759765625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.037817626953" Y="-26.66344921875" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.58026171875" Y="-27.1212734375" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.18461328125" Y="-27.653015625" />
                  <Point X="4.124813964844" Y="-27.749779296875" />
                  <Point X="4.028979980469" Y="-27.8859453125" />
                  <Point X="3.621061279297" Y="-27.65043359375" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224365234" Y="-27.159826171875" />
                  <Point X="2.62225" Y="-27.1359921875" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.506784179688" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444833496094" Y="-27.135177734375" />
                  <Point X="2.3351953125" Y="-27.19287890625" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.146829833984" Y="-27.400078125" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.119510009766" Y="-27.695232421875" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.453605957031" Y="-28.3487890625" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.852814453125" Y="-29.060955078125" />
                  <Point X="2.781861572266" Y="-29.11163671875" />
                  <Point X="2.701763916016" Y="-29.16348046875" />
                  <Point X="2.38436328125" Y="-28.7498359375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721923583984" Y="-27.900556640625" />
                  <Point X="1.591761352539" Y="-27.816875" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986816406" Y="-27.75116796875" />
                  <Point X="1.448366943359" Y="-27.7434375" />
                  <Point X="1.417100463867" Y="-27.741119140625" />
                  <Point X="1.274745849609" Y="-27.75421875" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.156363891602" Y="-27.7693984375" />
                  <Point X="1.128977661133" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.795462890625" />
                  <Point X="0.994672485352" Y="-27.886861328125" />
                  <Point X="0.924611328125" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.842758056641" Y="-28.17701953125" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.905266296387" Y="-28.972740234375" />
                  <Point X="1.022065307617" Y="-29.859916015625" />
                  <Point X="0.975713867188" Y="-29.870076171875" />
                  <Point X="0.929315307617" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.05841003418" Y="-29.752638671875" />
                  <Point X="-1.14124609375" Y="-29.731326171875" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.54103515625" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.160300415039" Y="-29.311849609375" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188125" Y="-29.178470703125" />
                  <Point X="-1.19902734375" Y="-29.14950390625" />
                  <Point X="-1.205666015625" Y="-29.135466796875" />
                  <Point X="-1.22173840332" Y="-29.10762890625" />
                  <Point X="-1.230575317383" Y="-29.094861328125" />
                  <Point X="-1.250209594727" Y="-29.0709375" />
                  <Point X="-1.261006958008" Y="-29.05978125" />
                  <Point X="-1.403467651367" Y="-28.934845703125" />
                  <Point X="-1.494267578125" Y="-28.855216796875" />
                  <Point X="-1.506735839844" Y="-28.84596875" />
                  <Point X="-1.53301940918" Y="-28.829623046875" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576531860352" Y="-28.810224609375" />
                  <Point X="-1.591315917969" Y="-28.805474609375" />
                  <Point X="-1.62145715332" Y="-28.798447265625" />
                  <Point X="-1.636814331055" Y="-28.796169921875" />
                  <Point X="-1.825891601562" Y="-28.78377734375" />
                  <Point X="-1.946403442383" Y="-28.77587890625" />
                  <Point X="-1.961927856445" Y="-28.7761328125" />
                  <Point X="-1.992726074219" Y="-28.779166015625" />
                  <Point X="-2.00799987793" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053667236328" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436767578" Y="-28.815810546875" />
                  <Point X="-2.252986328125" Y="-28.92108203125" />
                  <Point X="-2.353403320312" Y="-28.988177734375" />
                  <Point X="-2.359686279297" Y="-28.992759765625" />
                  <Point X="-2.380448242188" Y="-29.01013671875" />
                  <Point X="-2.402761962891" Y="-29.032775390625" />
                  <Point X="-2.410471923828" Y="-29.041630859375" />
                  <Point X="-2.498015380859" Y="-29.155720703125" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.586950195313" Y="-29.110623046875" />
                  <Point X="-2.747600341797" Y="-29.01115234375" />
                  <Point X="-2.980862304688" Y="-28.831548828125" />
                  <Point X="-2.796142333984" Y="-28.511603515625" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.61923046875" />
                  <Point X="-2.310626953125" Y="-27.58829296875" />
                  <Point X="-2.314668701172" Y="-27.55760546875" />
                  <Point X="-2.323655029297" Y="-27.52798828125" />
                  <Point X="-2.329362060547" Y="-27.513548828125" />
                  <Point X="-2.343581542969" Y="-27.48471875" />
                  <Point X="-2.351562255859" Y="-27.471404296875" />
                  <Point X="-2.369588134766" Y="-27.44625" />
                  <Point X="-2.379633300781" Y="-27.43441015625" />
                  <Point X="-2.396977294922" Y="-27.41706640625" />
                  <Point X="-2.408814453125" Y="-27.4070234375" />
                  <Point X="-2.433967529297" Y="-27.388998046875" />
                  <Point X="-2.447283447266" Y="-27.381015625" />
                  <Point X="-2.476112792969" Y="-27.366796875" />
                  <Point X="-2.490559082031" Y="-27.361087890625" />
                  <Point X="-2.520178710938" Y="-27.352103515625" />
                  <Point X="-2.550859130859" Y="-27.348064453125" />
                  <Point X="-2.581795166016" Y="-27.349076171875" />
                  <Point X="-2.597224121094" Y="-27.3508515625" />
                  <Point X="-2.628751953125" Y="-27.357123046875" />
                  <Point X="-2.643682617188" Y="-27.36138671875" />
                  <Point X="-2.6726484375" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.188600097656" Y="-27.668708984375" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-3.877106689453" Y="-27.907326171875" />
                  <Point X="-4.004019775391" Y="-27.740587890625" />
                  <Point X="-4.176348632812" Y="-27.451619140625" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.843618164062" Y="-27.1842890625" />
                  <Point X="-3.048122070312" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.398802734375" />
                  <Point X="-2.948748535156" Y="-26.368373046875" />
                  <Point X="-2.948577880859" Y="-26.353044921875" />
                  <Point X="-2.950787353516" Y="-26.32137109375" />
                  <Point X="-2.953084960938" Y="-26.306212890625" />
                  <Point X="-2.960087890625" Y="-26.27646484375" />
                  <Point X="-2.971785400391" Y="-26.24823046875" />
                  <Point X="-2.987871337891" Y="-26.22224609375" />
                  <Point X="-2.996965087891" Y="-26.20990625" />
                  <Point X="-3.017800048828" Y="-26.185951171875" />
                  <Point X="-3.028756347656" Y="-26.175236328125" />
                  <Point X="-3.052252197266" Y="-26.15570703125" />
                  <Point X="-3.064791748047" Y="-26.146892578125" />
                  <Point X="-3.091274414062" Y="-26.131306640625" />
                  <Point X="-3.105442382812" Y="-26.124478515625" />
                  <Point X="-3.134705810547" Y="-26.113255859375" />
                  <Point X="-3.149801269531" Y="-26.108861328125" />
                  <Point X="-3.181685791016" Y="-26.102380859375" />
                  <Point X="-3.197297851562" Y="-26.10053515625" />
                  <Point X="-3.228619384766" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-3.877950683594" Y="-26.183615234375" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.691125488281" Y="-26.16844140625" />
                  <Point X="-4.740762695312" Y="-25.974115234375" />
                  <Point X="-4.786356933594" Y="-25.65532421875" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.414478027344" Y="-25.554986328125" />
                  <Point X="-3.508287597656" Y="-25.312173828125" />
                  <Point X="-3.500472412109" Y="-25.309712890625" />
                  <Point X="-3.473928710938" Y="-25.299251953125" />
                  <Point X="-3.444166748047" Y="-25.28389453125" />
                  <Point X="-3.433563720703" Y="-25.277515625" />
                  <Point X="-3.416614013672" Y="-25.265751953125" />
                  <Point X="-3.393675048828" Y="-25.24824609375" />
                  <Point X="-3.371219482422" Y="-25.22636328125" />
                  <Point X="-3.360899658203" Y="-25.21448828125" />
                  <Point X="-3.34165625" Y="-25.188234375" />
                  <Point X="-3.333438476563" Y="-25.174818359375" />
                  <Point X="-3.319329589844" Y="-25.14681640625" />
                  <Point X="-3.313438476562" Y="-25.13223046875" />
                  <Point X="-3.3041875" Y="-25.102423828125" />
                  <Point X="-3.300991455078" Y="-25.088505859375" />
                  <Point X="-3.296721923828" Y="-25.06034765625" />
                  <Point X="-3.2956484375" Y="-25.046107421875" />
                  <Point X="-3.295647705078" Y="-25.01646484375" />
                  <Point X="-3.296720947266" Y="-25.002224609375" />
                  <Point X="-3.300989990234" Y="-24.974064453125" />
                  <Point X="-3.304185791016" Y="-24.96014453125" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.319327636719" Y="-24.915748046875" />
                  <Point X="-3.333436035156" Y="-24.88774609375" />
                  <Point X="-3.341653564453" Y="-24.87433203125" />
                  <Point X="-3.360897705078" Y="-24.848076171875" />
                  <Point X="-3.371216796875" Y="-24.836201171875" />
                  <Point X="-3.393673339844" Y="-24.81431640625" />
                  <Point X="-3.405810791016" Y="-24.804306640625" />
                  <Point X="-3.422760498047" Y="-24.79254296875" />
                  <Point X="-3.438493408203" Y="-24.7827265625" />
                  <Point X="-3.464864746094" Y="-24.7680078125" />
                  <Point X="-3.475443115234" Y="-24.762931640625" />
                  <Point X="-3.497155029297" Y="-24.75412109375" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-4.085865478516" Y="-24.595626953125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.763319335938" Y="-24.25864453125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.639549804688" Y="-23.703771484375" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.427645996094" Y="-23.708876953125" />
                  <Point X="-3.778066650391" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747056884766" Y="-23.795634765625" />
                  <Point X="-3.736703613281" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704888916016" Y="-23.7919453125" />
                  <Point X="-3.684604003906" Y="-23.78791015625" />
                  <Point X="-3.674572509766" Y="-23.78533984375" />
                  <Point X="-3.637057373047" Y="-23.77351171875" />
                  <Point X="-3.613146484375" Y="-23.76597265625" />
                  <Point X="-3.603454101562" Y="-23.76232421875" />
                  <Point X="-3.584525634766" Y="-23.75399609375" />
                  <Point X="-3.575289550781" Y="-23.74931640625" />
                  <Point X="-3.556545410156" Y="-23.73849609375" />
                  <Point X="-3.547869873047" Y="-23.7328359375" />
                  <Point X="-3.531185058594" Y="-23.720603515625" />
                  <Point X="-3.515929931641" Y="-23.706625" />
                  <Point X="-3.5022890625" Y="-23.6910703125" />
                  <Point X="-3.495894042969" Y="-23.682921875" />
                  <Point X="-3.483479492188" Y="-23.66519140625" />
                  <Point X="-3.478011230469" Y="-23.6563984375" />
                  <Point X="-3.468062988281" Y="-23.638265625" />
                  <Point X="-3.463583007813" Y="-23.62892578125" />
                  <Point X="-3.448529785156" Y="-23.592583984375" />
                  <Point X="-3.438935546875" Y="-23.569421875" />
                  <Point X="-3.435499267578" Y="-23.559650390625" />
                  <Point X="-3.4297109375" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436011962891" Y="-23.395470703125" />
                  <Point X="-3.443508789062" Y="-23.37619140625" />
                  <Point X="-3.447784423828" Y="-23.36675390625" />
                  <Point X="-3.465947509766" Y="-23.33186328125" />
                  <Point X="-3.477524169922" Y="-23.309625" />
                  <Point X="-3.482802246094" Y="-23.3007109375" />
                  <Point X="-3.494293945312" Y="-23.283513671875" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521498046875" Y="-23.251091796875" />
                  <Point X="-3.536439941406" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-3.875603027344" Y="-22.975826171875" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.1265859375" Y="-22.532630859375" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.759171142578" Y="-22.007189453125" />
                  <Point X="-3.726337158203" Y="-21.964986328125" />
                  <Point X="-3.653110351562" Y="-22.007263671875" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244926025391" Y="-22.242279296875" />
                  <Point X="-3.225999267578" Y="-22.250609375" />
                  <Point X="-3.216303466797" Y="-22.254259765625" />
                  <Point X="-3.195662109375" Y="-22.26076953125" />
                  <Point X="-3.185623291016" Y="-22.263341796875" />
                  <Point X="-3.165328369141" Y="-22.26737890625" />
                  <Point X="-3.155072265625" Y="-22.26884375" />
                  <Point X="-3.10282421875" Y="-22.2734140625" />
                  <Point X="-3.069522949219" Y="-22.276328125" />
                  <Point X="-3.059172851562" Y="-22.276666015625" />
                  <Point X="-3.038489990234" Y="-22.27621484375" />
                  <Point X="-3.028157226562" Y="-22.27542578125" />
                  <Point X="-3.006698242188" Y="-22.272599609375" />
                  <Point X="-2.996521240234" Y="-22.270689453125" />
                  <Point X="-2.976432373047" Y="-22.2657734375" />
                  <Point X="-2.9569921875" Y="-22.258697265625" />
                  <Point X="-2.938443359375" Y="-22.249548828125" />
                  <Point X="-2.929422851563" Y="-22.244470703125" />
                  <Point X="-2.911168457031" Y="-22.232841796875" />
                  <Point X="-2.902749267578" Y="-22.22680859375" />
                  <Point X="-2.886616699219" Y="-22.213861328125" />
                  <Point X="-2.878903320312" Y="-22.206947265625" />
                  <Point X="-2.841817138672" Y="-22.169861328125" />
                  <Point X="-2.8181796875" Y="-22.146224609375" />
                  <Point X="-2.811267089844" Y="-22.138513671875" />
                  <Point X="-2.798321533203" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.753368652344" Y="-21.903353515625" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.934331787109" Y="-21.5166875" />
                  <Point X="-3.059387451172" Y="-21.3000859375" />
                  <Point X="-2.864846191406" Y="-21.150931640625" />
                  <Point X="-2.648375488281" Y="-20.98496484375" />
                  <Point X="-2.265470214844" Y="-20.772232421875" />
                  <Point X="-2.1925234375" Y="-20.731705078125" />
                  <Point X="-2.118563964844" Y="-20.82808984375" />
                  <Point X="-2.111819335938" Y="-20.835953125" />
                  <Point X="-2.097515625" Y="-20.85089453125" />
                  <Point X="-2.089957275391" Y="-20.85797265625" />
                  <Point X="-2.073377685547" Y="-20.871884765625" />
                  <Point X="-2.065095214844" Y="-20.87809765625" />
                  <Point X="-2.047897827148" Y="-20.88958984375" />
                  <Point X="-2.038982666016" Y="-20.894869140625" />
                  <Point X="-1.980830810547" Y="-20.925142578125" />
                  <Point X="-1.943766479492" Y="-20.9444375" />
                  <Point X="-1.934335205078" Y="-20.9487109375" />
                  <Point X="-1.915062133789" Y="-20.95620703125" />
                  <Point X="-1.905220092773" Y="-20.9594296875" />
                  <Point X="-1.884313720703" Y="-20.965033203125" />
                  <Point X="-1.874174926758" Y="-20.967166015625" />
                  <Point X="-1.853724975586" Y="-20.970314453125" />
                  <Point X="-1.83305480957" Y="-20.971216796875" />
                  <Point X="-1.812407958984" Y="-20.96986328125" />
                  <Point X="-1.802120239258" Y="-20.968623046875" />
                  <Point X="-1.780805053711" Y="-20.96486328125" />
                  <Point X="-1.77071484375" Y="-20.962509765625" />
                  <Point X="-1.750860351562" Y="-20.956720703125" />
                  <Point X="-1.741096069336" Y="-20.95328515625" />
                  <Point X="-1.680526977539" Y="-20.9281953125" />
                  <Point X="-1.64192199707" Y="-20.912205078125" />
                  <Point X="-1.632584960938" Y="-20.9077265625" />
                  <Point X="-1.614453369141" Y="-20.897779296875" />
                  <Point X="-1.605658813477" Y="-20.892310546875" />
                  <Point X="-1.587928955078" Y="-20.879896484375" />
                  <Point X="-1.579780395508" Y="-20.873501953125" />
                  <Point X="-1.564228027344" Y="-20.85986328125" />
                  <Point X="-1.550252807617" Y="-20.844611328125" />
                  <Point X="-1.538021240234" Y="-20.8279296875" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.52153918457" Y="-20.80051171875" />
                  <Point X="-1.516856445312" Y="-20.791271484375" />
                  <Point X="-1.508525512695" Y="-20.772337890625" />
                  <Point X="-1.504877441406" Y="-20.76264453125" />
                  <Point X="-1.485163208008" Y="-20.700119140625" />
                  <Point X="-1.472598144531" Y="-20.660267578125" />
                  <Point X="-1.470026977539" Y="-20.650236328125" />
                  <Point X="-1.465991577148" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-1.211411743164" Y="-20.362248046875" />
                  <Point X="-0.931163818359" Y="-20.2836796875" />
                  <Point X="-0.46698815918" Y="-20.2293515625" />
                  <Point X="-0.365222595215" Y="-20.21744140625" />
                  <Point X="-0.343687774658" Y="-20.297810546875" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.31320123291" Y="-20.457537109375" />
                  <Point X="0.378190704346" Y="-20.2149921875" />
                  <Point X="0.583176940918" Y="-20.2364609375" />
                  <Point X="0.827851867676" Y="-20.262083984375" />
                  <Point X="1.211920166016" Y="-20.354810546875" />
                  <Point X="1.453622314453" Y="-20.413166015625" />
                  <Point X="1.702024658203" Y="-20.50326171875" />
                  <Point X="1.858249511719" Y="-20.55992578125" />
                  <Point X="2.099976074219" Y="-20.67297265625" />
                  <Point X="2.250426269531" Y="-20.743333984375" />
                  <Point X="2.484013916016" Y="-20.879421875" />
                  <Point X="2.629434326172" Y="-20.96414453125" />
                  <Point X="2.817778320312" Y="-21.0980859375" />
                  <Point X="2.591535888672" Y="-21.48994921875" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.062371826172" Y="-22.4068984375" />
                  <Point X="2.053181640625" Y="-22.426560546875" />
                  <Point X="2.044182617188" Y="-22.45043359375" />
                  <Point X="2.041301513672" Y="-22.459400390625" />
                  <Point X="2.028189208984" Y="-22.50843359375" />
                  <Point X="2.019831787109" Y="-22.5396875" />
                  <Point X="2.017912231445" Y="-22.54853515625" />
                  <Point X="2.013646484375" Y="-22.57978125" />
                  <Point X="2.012755615234" Y="-22.61676171875" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.018524047852" Y="-22.672822265625" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029143920898" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040735717773" Y="-22.765783203125" />
                  <Point X="2.045319580078" Y="-22.77611328125" />
                  <Point X="2.055681396484" Y="-22.79615625" />
                  <Point X="2.061459228516" Y="-22.805869140625" />
                  <Point X="2.087695068359" Y="-22.84453515625" />
                  <Point X="2.104416992188" Y="-22.869177734375" />
                  <Point X="2.109806884766" Y="-22.876365234375" />
                  <Point X="2.130470458984" Y="-22.899884765625" />
                  <Point X="2.157598144531" Y="-22.92461328125" />
                  <Point X="2.168254882812" Y="-22.933017578125" />
                  <Point X="2.206919677734" Y="-22.95925390625" />
                  <Point X="2.231563476562" Y="-22.9759765625" />
                  <Point X="2.241279052734" Y="-22.981755859375" />
                  <Point X="2.261318115234" Y="-22.992115234375" />
                  <Point X="2.271641601562" Y="-22.9966953125" />
                  <Point X="2.293729003906" Y="-23.004962890625" />
                  <Point X="2.304536376953" Y="-23.008291015625" />
                  <Point X="2.326469482422" Y="-23.01363671875" />
                  <Point X="2.337595214844" Y="-23.015654296875" />
                  <Point X="2.379995605469" Y="-23.020765625" />
                  <Point X="2.407020019531" Y="-23.024025390625" />
                  <Point X="2.416040039062" Y="-23.0246796875" />
                  <Point X="2.447575439453" Y="-23.02450390625" />
                  <Point X="2.484317138672" Y="-23.020177734375" />
                  <Point X="2.497751708984" Y="-23.01760546875" />
                  <Point X="2.546785400391" Y="-23.0044921875" />
                  <Point X="2.578038085938" Y="-22.996134765625" />
                  <Point X="2.583998291016" Y="-22.994328125" />
                  <Point X="2.604414794922" Y="-22.9869296875" />
                  <Point X="2.627662353516" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.216964599609" Y="-22.636724609375" />
                  <Point X="3.940403076172" Y="-22.219048828125" />
                  <Point X="3.957693603516" Y="-22.243078125" />
                  <Point X="4.043957275391" Y="-22.36296484375" />
                  <Point X="4.136884765625" Y="-22.516529296875" />
                  <Point X="3.865779052734" Y="-22.7245546875" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168137939453" Y="-23.260130859375" />
                  <Point X="3.152115478516" Y="-23.274787109375" />
                  <Point X="3.134666503906" Y="-23.293400390625" />
                  <Point X="3.128577636719" Y="-23.300578125" />
                  <Point X="3.093287841797" Y="-23.346615234375" />
                  <Point X="3.070795410156" Y="-23.375958984375" />
                  <Point X="3.065636230469" Y="-23.3833984375" />
                  <Point X="3.049738769531" Y="-23.41062890625" />
                  <Point X="3.034763183594" Y="-23.444453125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.016994628906" Y="-23.504333984375" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.015490478516" Y="-23.699728515625" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034682617188" Y="-23.771388671875" />
                  <Point X="3.050285888672" Y="-23.80462890625" />
                  <Point X="3.056918701172" Y="-23.8164765625" />
                  <Point X="3.086269042969" Y="-23.861087890625" />
                  <Point X="3.104976318359" Y="-23.889521484375" />
                  <Point X="3.111740478516" Y="-23.898578125" />
                  <Point X="3.126293945313" Y="-23.915822265625" />
                  <Point X="3.134083251953" Y="-23.924009765625" />
                  <Point X="3.151327148438" Y="-23.940099609375" />
                  <Point X="3.160030761719" Y="-23.947302734375" />
                  <Point X="3.178239013672" Y="-23.96062890625" />
                  <Point X="3.187743652344" Y="-23.966751953125" />
                  <Point X="3.230276611328" Y="-23.9906953125" />
                  <Point X="3.257385742188" Y="-24.005955078125" />
                  <Point X="3.265483886719" Y="-24.010017578125" />
                  <Point X="3.294692871094" Y="-24.021923828125" />
                  <Point X="3.330282958984" Y="-24.031982421875" />
                  <Point X="3.343674072266" Y="-24.034744140625" />
                  <Point X="3.401181396484" Y="-24.04234375" />
                  <Point X="3.437834716797" Y="-24.0471875" />
                  <Point X="3.444033691406" Y="-24.04780078125" />
                  <Point X="3.465708740234" Y="-24.04877734375" />
                  <Point X="3.491213623047" Y="-24.04796875" />
                  <Point X="3.500603027344" Y="-24.047203125" />
                  <Point X="4.052448242188" Y="-23.974552734375" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.715636230469" Y="-23.933587890625" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.485987304688" Y="-24.36589453125" />
                  <Point X="3.6919921875" Y="-24.578646484375" />
                  <Point X="3.686022705078" Y="-24.580458984375" />
                  <Point X="3.665625732422" Y="-24.58786328125" />
                  <Point X="3.642385253906" Y="-24.59837890625" />
                  <Point X="3.6340078125" Y="-24.602681640625" />
                  <Point X="3.577508544922" Y="-24.635337890625" />
                  <Point X="3.541497802734" Y="-24.65615234375" />
                  <Point X="3.533880615234" Y="-24.6610546875" />
                  <Point X="3.50877734375" Y="-24.680126953125" />
                  <Point X="3.481994384766" Y="-24.7056484375" />
                  <Point X="3.472795410156" Y="-24.715775390625" />
                  <Point X="3.438895996094" Y="-24.75897265625" />
                  <Point X="3.417289550781" Y="-24.78650390625" />
                  <Point X="3.410854248047" Y="-24.79579296875" />
                  <Point X="3.399130371094" Y="-24.815072265625" />
                  <Point X="3.393841796875" Y="-24.8250625" />
                  <Point X="3.384067871094" Y="-24.84652734375" />
                  <Point X="3.380004394531" Y="-24.85707421875" />
                  <Point X="3.373158691406" Y="-24.87857421875" />
                  <Point X="3.370376464844" Y="-24.88952734375" />
                  <Point X="3.359076660156" Y="-24.94853125" />
                  <Point X="3.351874267578" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584472656" Y="-25.02626171875" />
                  <Point X="3.350280029297" Y="-25.06294140625" />
                  <Point X="3.351874511719" Y="-25.076423828125" />
                  <Point X="3.363174560547" Y="-25.13542578125" />
                  <Point X="3.370376708984" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.18398828125" />
                  <Point X="3.380005126953" Y="-25.20548828125" />
                  <Point X="3.384068603516" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.237498046875" />
                  <Point X="3.399130126953" Y="-25.24748828125" />
                  <Point X="3.410853515625" Y="-25.266767578125" />
                  <Point X="3.417289550781" Y="-25.276056640625" />
                  <Point X="3.451188964844" Y="-25.31925390625" />
                  <Point X="3.472795410156" Y="-25.34678515625" />
                  <Point X="3.478715820312" Y="-25.353630859375" />
                  <Point X="3.501144287109" Y="-25.375810546875" />
                  <Point X="3.530178222656" Y="-25.3987265625" />
                  <Point X="3.541495849609" Y="-25.40640625" />
                  <Point X="3.597995117188" Y="-25.4390625" />
                  <Point X="3.634005859375" Y="-25.45987890625" />
                  <Point X="3.639492675781" Y="-25.462814453125" />
                  <Point X="3.659139648438" Y="-25.472009765625" />
                  <Point X="3.683020996094" Y="-25.48102734375" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.198059082031" Y="-25.619515625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.782299316406" Y="-25.793849609375" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.727801757812" Y="-26.07921875" />
                  <Point X="4.357593261719" Y="-26.03048046875" />
                  <Point X="3.436782226562" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354481933594" Y="-25.912677734375" />
                  <Point X="3.243593994141" Y="-25.936779296875" />
                  <Point X="3.172917724609" Y="-25.952140625" />
                  <Point X="3.157874023438" Y="-25.9567421875" />
                  <Point X="3.128752929688" Y="-25.9683671875" />
                  <Point X="3.114675537109" Y="-25.975390625" />
                  <Point X="3.086848876953" Y="-25.992283203125" />
                  <Point X="3.074124023438" Y="-26.00153125" />
                  <Point X="3.050374023438" Y="-26.022001953125" />
                  <Point X="3.039348876953" Y="-26.033224609375" />
                  <Point X="2.972323974609" Y="-26.1138359375" />
                  <Point X="2.929604736328" Y="-26.165212890625" />
                  <Point X="2.921325683594" Y="-26.17684765625" />
                  <Point X="2.906605957031" Y="-26.201228515625" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.865551025391" Y="-26.4010546875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.957907714844" Y="-26.71482421875" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486328125" Y="-26.828119140625" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.522429199219" Y="-27.196642578125" />
                  <Point X="4.087170166016" Y="-27.629984375" />
                  <Point X="4.045500976562" Y="-27.69741015625" />
                  <Point X="4.001272460938" Y="-27.760251953125" />
                  <Point X="3.668561279297" Y="-27.568162109375" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841191650391" Y="-27.09088671875" />
                  <Point X="2.815026123047" Y="-27.079515625" />
                  <Point X="2.78312109375" Y="-27.069328125" />
                  <Point X="2.771107910156" Y="-27.066337890625" />
                  <Point X="2.639133544922" Y="-27.04250390625" />
                  <Point X="2.555017578125" Y="-27.0273125" />
                  <Point X="2.539359130859" Y="-27.02580859375" />
                  <Point X="2.508009277344" Y="-27.025404296875" />
                  <Point X="2.492317871094" Y="-27.02650390625" />
                  <Point X="2.460145019531" Y="-27.0314609375" />
                  <Point X="2.444847167969" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400589355469" Y="-27.051109375" />
                  <Point X="2.290951171875" Y="-27.108810546875" />
                  <Point X="2.221071289062" Y="-27.145587890625" />
                  <Point X="2.208968261719" Y="-27.153171875" />
                  <Point X="2.186038330078" Y="-27.170064453125" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144940673828" Y="-27.21116015625" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.06276171875" Y="-27.355833984375" />
                  <Point X="2.025984619141" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.44019140625" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.026022338867" Y="-27.712115234375" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.371333496094" Y="-28.3962890625" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.72375390625" Y="-29.036083984375" />
                  <Point X="2.459731933594" Y="-28.69200390625" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828658813477" Y="-27.87015234375" />
                  <Point X="1.808834350586" Y="-27.84962890625" />
                  <Point X="1.783251098633" Y="-27.82800390625" />
                  <Point X="1.773298217773" Y="-27.820646484375" />
                  <Point X="1.643135864258" Y="-27.73696484375" />
                  <Point X="1.560174682617" Y="-27.68362890625" />
                  <Point X="1.546280151367" Y="-27.676244140625" />
                  <Point X="1.517466674805" Y="-27.663873046875" />
                  <Point X="1.502548095703" Y="-27.65888671875" />
                  <Point X="1.470928344727" Y="-27.65115625" />
                  <Point X="1.455391723633" Y="-27.648697265625" />
                  <Point X="1.424125244141" Y="-27.64637890625" />
                  <Point X="1.408395263672" Y="-27.64651953125" />
                  <Point X="1.266040649414" Y="-27.659619140625" />
                  <Point X="1.17530847168" Y="-27.667966796875" />
                  <Point X="1.161225341797" Y="-27.67033984375" />
                  <Point X="1.133575561523" Y="-27.677171875" />
                  <Point X="1.120008911133" Y="-27.681630859375" />
                  <Point X="1.092622680664" Y="-27.692974609375" />
                  <Point X="1.079876953125" Y="-27.6994140625" />
                  <Point X="1.055494384766" Y="-27.714134765625" />
                  <Point X="1.043857666016" Y="-27.722416015625" />
                  <Point X="0.933934875488" Y="-27.813814453125" />
                  <Point X="0.863873718262" Y="-27.872068359375" />
                  <Point X="0.852653930664" Y="-27.88308984375" />
                  <Point X="0.832183776855" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.749925598145" Y="-28.156841796875" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.811079040527" Y="-28.985140625" />
                  <Point X="0.833091003418" Y="-29.152337890625" />
                  <Point X="0.655065124512" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146789551" Y="-28.453583984375" />
                  <Point X="0.626788085938" Y="-28.42381640625" />
                  <Point X="0.620407287598" Y="-28.41320703125" />
                  <Point X="0.520413024902" Y="-28.269134765625" />
                  <Point X="0.456679840088" Y="-28.177306640625" />
                  <Point X="0.446668609619" Y="-28.16516796875" />
                  <Point X="0.424781646729" Y="-28.1427109375" />
                  <Point X="0.412905792236" Y="-28.132392578125" />
                  <Point X="0.386650512695" Y="-28.113150390625" />
                  <Point X="0.373235961914" Y="-28.10493359375" />
                  <Point X="0.345237640381" Y="-28.090828125" />
                  <Point X="0.330653869629" Y="-28.084939453125" />
                  <Point X="0.175918243408" Y="-28.036916015625" />
                  <Point X="0.077294761658" Y="-28.006306640625" />
                  <Point X="0.063380245209" Y="-28.003111328125" />
                  <Point X="0.035227500916" Y="-27.998841796875" />
                  <Point X="0.020989276886" Y="-27.997767578125" />
                  <Point X="-0.008651480675" Y="-27.997765625" />
                  <Point X="-0.02289535141" Y="-27.998837890625" />
                  <Point X="-0.051061767578" Y="-28.003107421875" />
                  <Point X="-0.064984169006" Y="-28.0063046875" />
                  <Point X="-0.219719772339" Y="-28.054330078125" />
                  <Point X="-0.318343261719" Y="-28.0849375" />
                  <Point X="-0.332928527832" Y="-28.090828125" />
                  <Point X="-0.360928619385" Y="-28.104935546875" />
                  <Point X="-0.37434362793" Y="-28.11315234375" />
                  <Point X="-0.400599487305" Y="-28.132396484375" />
                  <Point X="-0.4124765625" Y="-28.142716796875" />
                  <Point X="-0.434360839844" Y="-28.165173828125" />
                  <Point X="-0.44436819458" Y="-28.177310546875" />
                  <Point X="-0.544362731934" Y="-28.321384765625" />
                  <Point X="-0.60809576416" Y="-28.4132109375" />
                  <Point X="-0.612469482422" Y="-28.420130859375" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.798684753418" Y="-29.069880859375" />
                  <Point X="-0.985425598145" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.394156148918" Y="-20.216664290681" />
                  <Point X="0.377184295036" Y="-20.218748172307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.341236420272" Y="-20.30695914438" />
                  <Point X="-1.478055268247" Y="-20.446542947455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814852734489" Y="-20.260722678567" />
                  <Point X="0.350665591641" Y="-20.317717693078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.31640702083" Y="-20.39962391089" />
                  <Point X="-1.465654758957" Y="-20.540733789785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.082524285055" Y="-20.323570178183" />
                  <Point X="0.324146888247" Y="-20.416687213848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.291577621387" Y="-20.492288677401" />
                  <Point X="-1.467324649127" Y="-20.636652259935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.345315492646" Y="-20.387016908568" />
                  <Point X="0.297628148026" Y="-20.515656739141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.266748221945" Y="-20.584953443912" />
                  <Point X="-1.496458417209" Y="-20.735942870275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.129617310515" Y="-20.813685006971" />
                  <Point X="-2.399792234429" Y="-20.846858316371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.569518564502" Y="-20.455201666256" />
                  <Point X="0.271109381922" Y="-20.614626267611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.241918822502" Y="-20.677618210422" />
                  <Point X="-1.545136184713" Y="-20.837633182002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.033696864987" Y="-20.897620890604" />
                  <Point X="-2.620946691844" Y="-20.969726102735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.766667573014" Y="-20.526708245232" />
                  <Point X="0.244590615818" Y="-20.713595796081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.212794671411" Y="-20.769755647737" />
                  <Point X="-1.76203201521" Y="-20.95997807473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.848184797595" Y="-20.970556306286" />
                  <Point X="-2.77858404784" Y="-21.084794969691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.945049666257" Y="-20.600519111659" />
                  <Point X="0.198891000961" Y="-20.814920436644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.149426551427" Y="-20.857688454368" />
                  <Point X="-2.927227950911" Y="-21.19875957948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.107152416864" Y="-20.676328830023" />
                  <Point X="-3.053664961314" Y="-21.309997525702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266185730341" Y="-20.752515427877" />
                  <Point X="-3.002062576112" Y="-21.399374982912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.401875674313" Y="-20.831568231106" />
                  <Point X="-2.950460190909" Y="-21.488752440122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.537565151168" Y="-20.91062109169" />
                  <Point X="-2.898857850138" Y="-21.578129902788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.666499302099" Y="-20.990503402001" />
                  <Point X="-2.847255529568" Y="-21.667507367934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.78127219631" Y="-21.072124496" />
                  <Point X="-2.795653208998" Y="-21.756884833079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.777219697708" Y="-21.16833551368" />
                  <Point X="-2.760047006632" Y="-21.848226374575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723644012985" Y="-21.966541209887" />
                  <Point X="-3.727959054202" Y="-21.967071030329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717743236764" Y="-21.27135173824" />
                  <Point X="-2.749926396576" Y="-21.942697153332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.586936655008" Y="-22.045469090385" />
                  <Point X="-3.810288009655" Y="-22.072893188392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658266775819" Y="-21.3743679628" />
                  <Point X="-2.757049607635" Y="-22.039285207092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.450229560596" Y="-22.124397003243" />
                  <Point X="-3.892616583022" Y="-22.178715299541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.598790314875" Y="-21.47738418736" />
                  <Point X="-2.814432214617" Y="-22.142044338713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.313522466184" Y="-22.203324916102" />
                  <Point X="-3.974945156389" Y="-22.28453741069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.539313913038" Y="-21.580400404662" />
                  <Point X="-2.947718135377" Y="-22.254123225386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103995184228" Y="-22.273311634209" />
                  <Point X="-4.042483376957" Y="-22.388543494866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479837519413" Y="-21.683416620956" />
                  <Point X="-4.102663494779" Y="-22.491646117626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420361125788" Y="-21.78643283725" />
                  <Point X="-4.162842994476" Y="-22.59474866449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.360884732163" Y="-21.889449053544" />
                  <Point X="-4.223022086341" Y="-22.697851161279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.301408338537" Y="-21.992465269837" />
                  <Point X="-4.128289879987" Y="-22.781932942337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.241931944912" Y="-22.095481486131" />
                  <Point X="-4.02076007234" Y="-22.86444337554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.182455551287" Y="-22.198497702425" />
                  <Point X="-3.913230264693" Y="-22.946953808743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.122979157662" Y="-22.301513918719" />
                  <Point X="-3.805700508042" Y="-23.029464248207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.063646744729" Y="-22.404512456407" />
                  <Point X="-3.69817077884" Y="-23.111974691041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.973949574123" Y="-22.265670195727" />
                  <Point X="3.828778709572" Y="-22.283494936587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029254840822" Y="-22.504448684646" />
                  <Point X="-3.590641049639" Y="-23.194485133876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.037228873388" Y="-22.353613908171" />
                  <Point X="3.618217502284" Y="-22.405062035385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013107742247" Y="-22.602144732473" />
                  <Point X="-3.497827977281" Y="-23.278802554959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.092138527718" Y="-22.442585283793" />
                  <Point X="3.407656294996" Y="-22.526629134184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021419864996" Y="-22.696837565549" />
                  <Point X="-3.447090232702" Y="-23.368286176688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.111633677039" Y="-22.535905013863" />
                  <Point X="3.197095179096" Y="-22.648196221762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.051883789442" Y="-22.788810499381" />
                  <Point X="-3.42173183975" Y="-23.460885990963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.963134271726" Y="-22.649851881557" />
                  <Point X="2.986534940266" Y="-22.769763201649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.110636892164" Y="-22.877309958881" />
                  <Point X="-3.4350879987" Y="-23.558239354494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.540704904324" Y="-23.693992040778" />
                  <Point X="-4.640210542157" Y="-23.706209796826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.814635240555" Y="-22.763798703313" />
                  <Point X="2.775974701435" Y="-22.891330181536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.209349857065" Y="-22.960902964248" />
                  <Point X="-3.479913081974" Y="-23.659456616079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.164527466868" Y="-23.743516692716" />
                  <Point X="-4.667039779652" Y="-23.80521744639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.666136921583" Y="-22.877745437621" />
                  <Point X="-3.642009059058" Y="-23.775072932868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.788347563196" Y="-23.793041041842" />
                  <Point X="-4.693869017147" Y="-23.904225095954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.51763860261" Y="-22.99169217193" />
                  <Point X="-4.720698254642" Y="-24.003232745518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369140283638" Y="-23.105638906238" />
                  <Point X="-4.740039560147" Y="-24.10132099264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220641964666" Y="-23.219585640546" />
                  <Point X="-4.754464738848" Y="-24.198805615291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.106555375473" Y="-23.329307145724" />
                  <Point X="-4.768889926091" Y="-24.296290238991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.039734331048" Y="-23.433225171741" />
                  <Point X="-4.783315126912" Y="-24.393774864359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.009060335421" Y="-23.532704898244" />
                  <Point X="-4.57667002489" Y="-24.464115469663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002483850822" Y="-23.629225822436" />
                  <Point X="-4.331712857717" Y="-24.52975194487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020242401869" Y="-23.722758779963" />
                  <Point X="-4.086755690543" Y="-24.595388420077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.05560511938" Y="-23.814130217639" />
                  <Point X="-3.841795881524" Y="-24.661024570906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.115084580763" Y="-23.902540491509" />
                  <Point X="-3.596836062869" Y="-24.726660720553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220673304871" Y="-23.985289259802" />
                  <Point X="-3.412406431056" Y="-24.799729042611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.707093055791" Y="-23.898493296786" />
                  <Point X="3.565064985542" Y="-24.038716711931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48919090913" Y="-24.048032877087" />
                  <Point X="-3.334575513798" Y="-24.88588604103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729717654202" Y="-23.991428778823" />
                  <Point X="-3.300482301182" Y="-24.977413354308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752342874324" Y="-24.084364184524" />
                  <Point X="-3.298625001379" Y="-25.072898739985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767085572224" Y="-24.178267442254" />
                  <Point X="-3.332401036672" Y="-25.172759349067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.781707925459" Y="-24.272185476452" />
                  <Point X="-3.440619236816" Y="-25.281760306672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.222054058424" Y="-24.436615764192" />
                  <Point X="-4.015358896559" Y="-25.448042896845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.629942527303" Y="-24.605031351964" />
                  <Point X="-4.67469618766" Y="-25.624712770038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.469091840781" Y="-24.720494766299" />
                  <Point X="-4.775279557063" Y="-25.732776288303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.393655989581" Y="-24.825470557583" />
                  <Point X="-4.761826643224" Y="-25.826837911603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.363607342283" Y="-24.924873500966" />
                  <Point X="-4.748373729385" Y="-25.920899534903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.348834091846" Y="-25.022400861453" />
                  <Point X="-4.730468946264" Y="-26.014414537388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.359605762952" Y="-25.116791699964" />
                  <Point X="-4.706764090194" Y="-26.107217380463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.38166531866" Y="-25.209796560522" />
                  <Point X="-4.683059186944" Y="-26.200020217745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435240968731" Y="-25.298931731271" />
                  <Point X="-3.15405874807" Y="-26.107996003656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.512922688821" Y="-25.385107048798" />
                  <Point X="-3.016992371375" Y="-26.186879802198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.643665222886" Y="-25.46476731758" />
                  <Point X="-2.960426389452" Y="-26.275647806364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.872757704354" Y="-25.532351731256" />
                  <Point X="-2.948893411479" Y="-26.369945168146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.117716565543" Y="-25.597987998464" />
                  <Point X="-2.971508224521" Y="-26.468435351454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.362675727776" Y="-25.663624228709" />
                  <Point X="-3.047703226542" Y="-26.573504354739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.607635036936" Y="-25.729260440913" />
                  <Point X="-3.196110305857" Y="-26.687439886226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.780812648051" Y="-25.803710337392" />
                  <Point X="3.670563996175" Y="-25.940031730606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.058248676092" Y="-26.015214598317" />
                  <Point X="-3.344608956502" Y="-26.801386661258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766110088939" Y="-25.901229018076" />
                  <Point X="4.046742066975" Y="-25.98955630478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.965246971158" Y="-26.122347205239" />
                  <Point X="-3.493107607147" Y="-26.915333436291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.746014200349" Y="-25.999409916351" />
                  <Point X="4.422920406041" Y="-26.039080846015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.89489457018" Y="-26.22669882732" />
                  <Point X="-3.641606257792" Y="-27.029280211324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.872534934658" Y="-26.325157678768" />
                  <Point X="-3.790104908437" Y="-27.143226986357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.863626850593" Y="-26.421964887377" />
                  <Point X="-3.938603181445" Y="-27.257173715021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86212511868" Y="-26.517862710289" />
                  <Point X="-4.087101241697" Y="-27.371120417563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.891540441434" Y="-26.609964396219" />
                  <Point X="-4.161805131509" Y="-27.47600633529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.947601879974" Y="-26.698794350523" />
                  <Point X="-2.480078455175" Y="-27.365229697196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.712356237321" Y="-27.393749822684" />
                  <Point X="-4.108620050724" Y="-27.565189461918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.005831143507" Y="-26.787358129387" />
                  <Point X="-2.368846074412" Y="-27.447285511584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.922916758096" Y="-27.51531683719" />
                  <Point X="-4.055434969939" Y="-27.654372588545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098886137938" Y="-26.871645846175" />
                  <Point X="-2.320887250295" Y="-27.537110341842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.133477278871" Y="-27.636883851695" />
                  <Point X="-4.001802612803" Y="-27.743500796543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.206415641591" Y="-26.954156316703" />
                  <Point X="2.577515180255" Y="-27.0313755837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.395657619586" Y="-27.053704884434" />
                  <Point X="-2.313092263266" Y="-27.631866671201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.344036950526" Y="-27.758450761942" />
                  <Point X="-3.935176877832" Y="-27.831033618348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313945145244" Y="-27.036666787232" />
                  <Point X="2.847132067144" Y="-27.09398422605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179129222813" Y="-27.176004661973" />
                  <Point X="-2.345806019212" Y="-27.731596848779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.554596321058" Y="-27.880017635215" />
                  <Point X="-3.868551165045" Y="-27.918566442877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.421474648896" Y="-27.11917725776" />
                  <Point X="2.983944970498" Y="-27.172899147205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.102050323575" Y="-27.28118219419" />
                  <Point X="-2.405282357173" Y="-27.834613058239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.765155691589" Y="-28.001584508488" />
                  <Point X="-3.801925602837" Y="-28.006099285895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.529004154321" Y="-27.201687728071" />
                  <Point X="3.120652242818" Y="-27.251827038219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.048196956618" Y="-27.383507989623" />
                  <Point X="-2.464758695135" Y="-27.937629267698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.636533686952" Y="-27.284198195042" />
                  <Point X="3.257359515139" Y="-27.330754929234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006560465239" Y="-27.484333741353" />
                  <Point X="-2.524235033097" Y="-28.040645477157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.744063219584" Y="-27.366708662012" />
                  <Point X="3.394066787459" Y="-27.409682820248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.002266213207" Y="-27.580574442622" />
                  <Point X="1.450440399408" Y="-27.648330132864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.095058899631" Y="-27.691965494267" />
                  <Point X="-2.583711371059" Y="-28.143661686617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.851592752216" Y="-27.449219128982" />
                  <Point X="3.53077405978" Y="-27.488610711263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.019176964596" Y="-27.674211496857" />
                  <Point X="1.621480399645" Y="-27.723042494957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.942897817728" Y="-27.806361959314" />
                  <Point X="-2.643187709021" Y="-28.246677896076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.959122284847" Y="-27.531729595953" />
                  <Point X="3.6674813321" Y="-27.567538602277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.036087514023" Y="-27.767848575889" />
                  <Point X="1.746483799812" Y="-27.803407440775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825135593461" Y="-27.91653477573" />
                  <Point X="0.082030859159" Y="-28.007776564236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.169059326947" Y="-28.038606562484" />
                  <Point X="-2.702664046982" Y="-28.349694105535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.066651817479" Y="-27.614240062923" />
                  <Point X="3.804189086985" Y="-27.64646643404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.063139752523" Y="-27.860240412082" />
                  <Point X="1.84230293084" Y="-27.887355764264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.780153543521" Y="-28.017771310399" />
                  <Point X="0.303003566758" Y="-28.076357960781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.435939835159" Y="-28.167088801917" />
                  <Point X="-2.762140384944" Y="-28.452710314995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.033837147675" Y="-27.713982631165" />
                  <Point X="3.940896845711" Y="-27.725394265331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.113532893908" Y="-27.949766345763" />
                  <Point X="1.909422759319" Y="-27.974827919015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758779115071" Y="-28.116109193629" />
                  <Point X="0.43732308566" Y="-28.15557903105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.510016279287" Y="-28.271897679001" />
                  <Point X="-2.821616687833" Y="-28.555726520148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.16513512239" Y="-28.039143822216" />
                  <Point X="1.976542587799" Y="-28.062300073765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.737404852138" Y="-28.214447056537" />
                  <Point X="0.502477261789" Y="-28.243292537562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.582635265149" Y="-28.376527602712" />
                  <Point X="-2.881092943907" Y="-28.658742719552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.216737350872" Y="-28.128521298669" />
                  <Point X="2.043662416279" Y="-28.149772228516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724736134255" Y="-28.311716012918" />
                  <Point X="0.563690978667" Y="-28.331489871632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.639810932107" Y="-28.479261325292" />
                  <Point X="-2.940569199981" Y="-28.761758918957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.268339579353" Y="-28.217898775122" />
                  <Point X="2.110782244758" Y="-28.237244383267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.734858465343" Y="-28.406186580359" />
                  <Point X="0.62434592338" Y="-28.419755814298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.666968561762" Y="-28.578309296343" />
                  <Point X="-2.946282730011" Y="-28.858173885652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.319941807835" Y="-28.307276251575" />
                  <Point X="2.177902073238" Y="-28.324716538018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.747258898432" Y="-28.500377432045" />
                  <Point X="0.661229287197" Y="-28.510940540085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.69348736589" Y="-28.677278829483" />
                  <Point X="-1.653089776533" Y="-28.795103190115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.157054475499" Y="-28.856982274388" />
                  <Point X="-2.839070750135" Y="-28.940723343198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371544036045" Y="-28.396653728061" />
                  <Point X="2.245021901717" Y="-28.412188692769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.759659331522" Y="-28.594568283732" />
                  <Point X="0.686058708961" Y="-28.603605303855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.720006170019" Y="-28.776248362622" />
                  <Point X="-1.478164729847" Y="-28.869338528485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.332549439473" Y="-28.974243779899" />
                  <Point X="-2.728659604115" Y="-29.022879992533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.423146197787" Y="-28.486031212708" />
                  <Point X="2.312141730197" Y="-28.49966084752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.772059764612" Y="-28.688759135419" />
                  <Point X="0.710888130725" Y="-28.696270067625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.746524974147" Y="-28.875217895761" />
                  <Point X="-1.382428141398" Y="-28.953296986929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.442573210747" Y="-29.083466433762" />
                  <Point X="-2.599658888878" Y="-29.102754129775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.474748359529" Y="-28.575408697356" />
                  <Point X="2.379261558677" Y="-28.587133002271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784460197702" Y="-28.782949987105" />
                  <Point X="0.735717552488" Y="-28.788934831395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.773043778275" Y="-28.9741874289" />
                  <Point X="-1.286692415356" Y="-29.037255551262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.52635052127" Y="-28.664786182003" />
                  <Point X="2.446381387156" Y="-28.674605157021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.796860630792" Y="-28.877140838792" />
                  <Point X="0.760546974252" Y="-28.881599595165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.799562583186" Y="-29.073156962135" />
                  <Point X="-1.212376438114" Y="-29.123844130047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.577952683012" Y="-28.754163666651" />
                  <Point X="2.513501162491" Y="-28.762077318298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809261063882" Y="-28.971331690478" />
                  <Point X="0.785376396016" Y="-28.974264358935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.826081410958" Y="-29.172126498178" />
                  <Point X="-1.179462334424" Y="-29.215516219697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.629554844754" Y="-28.843541151298" />
                  <Point X="2.580620924629" Y="-28.849549481194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.821661532267" Y="-29.065522537831" />
                  <Point X="0.81020581778" Y="-29.066929122705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.85260023873" Y="-29.27109603422" />
                  <Point X="-1.160877633496" Y="-29.308947738772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.681157006496" Y="-28.932918635946" />
                  <Point X="2.647740686768" Y="-28.937021644091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.879119066501" Y="-29.370065570262" />
                  <Point X="-1.142293096176" Y="-29.402379277937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.732759168238" Y="-29.022296120593" />
                  <Point X="2.714860448907" Y="-29.024493806987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.905637894273" Y="-29.469035106304" />
                  <Point X="-1.1237085641" Y="-29.495810817745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.932156722045" Y="-29.568004642346" />
                  <Point X="-1.122826293978" Y="-29.591415922014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.958675549817" Y="-29.666974178389" />
                  <Point X="-1.135634433683" Y="-29.688701997242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.985194377589" Y="-29.765943714431" />
                  <Point X="-0.98805897009" Y="-29.766295442163" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998375" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.716188598633" Y="-29.45015625" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.364324401855" Y="-28.377470703125" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.274335906982" Y="-28.266400390625" />
                  <Point X="0.119600318909" Y="-28.218376953125" />
                  <Point X="0.02097677803" Y="-28.187767578125" />
                  <Point X="-0.008663995743" Y="-28.187765625" />
                  <Point X="-0.163399581909" Y="-28.235791015625" />
                  <Point X="-0.262023132324" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.388273406982" Y="-28.429716796875" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.615158813477" Y="-29.119056640625" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-0.985042541504" Y="-29.960427734375" />
                  <Point X="-1.100231201172" Y="-29.938068359375" />
                  <Point X="-1.279380126953" Y="-29.891974609375" />
                  <Point X="-1.351589477539" Y="-29.873396484375" />
                  <Point X="-1.338600341797" Y="-29.774734375" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.346649658203" Y="-29.348916015625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282958984" Y="-29.20262890625" />
                  <Point X="-1.528743652344" Y="-29.077693359375" />
                  <Point X="-1.619543701172" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.838318115234" Y="-28.97337109375" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878295898" Y="-28.973791015625" />
                  <Point X="-2.147427734375" Y="-29.0790625" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.347277587891" Y="-29.271384765625" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.686971923828" Y="-29.272166015625" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-3.103881835938" Y="-28.976623046875" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.960687255859" Y="-28.416603515625" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513982421875" Y="-27.56876171875" />
                  <Point X="-2.531326416016" Y="-27.55141796875" />
                  <Point X="-2.560155761719" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.093600097656" Y="-27.833251953125" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.028293212891" Y="-28.022404296875" />
                  <Point X="-4.161701660156" Y="-27.8471328125" />
                  <Point X="-4.339534179688" Y="-27.548935546875" />
                  <Point X="-4.43101953125" Y="-27.39552734375" />
                  <Point X="-3.959282958984" Y="-27.03355078125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140326904297" Y="-26.33459375" />
                  <Point X="-3.161161865234" Y="-26.310638671875" />
                  <Point X="-3.18764453125" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-3.853150878906" Y="-26.371990234375" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.875215332031" Y="-26.21546484375" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.974442871094" Y="-25.6822265625" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.463653808594" Y="-25.371458984375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541895751953" Y="-25.12142578125" />
                  <Point X="-3.524946044922" Y="-25.109662109375" />
                  <Point X="-3.514142822266" Y="-25.1021640625" />
                  <Point X="-3.494899414062" Y="-25.07591015625" />
                  <Point X="-3.4856484375" Y="-25.046103515625" />
                  <Point X="-3.485647705078" Y="-25.0164609375" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.514142822266" Y="-24.960396484375" />
                  <Point X="-3.531092529297" Y="-24.9486328125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.135040527344" Y="-24.779154296875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.951272460938" Y="-24.23083203125" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.822935546875" Y="-23.654076171875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.402845214844" Y="-23.520501953125" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731704833984" Y="-23.6041328125" />
                  <Point X="-3.694189697266" Y="-23.5923046875" />
                  <Point X="-3.670278808594" Y="-23.584765625" />
                  <Point X="-3.651534667969" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.624066894531" Y="-23.519873046875" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.634479248047" Y="-23.41959765625" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.991267578125" Y="-23.126564453125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.290678710937" Y="-22.436853515625" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.909132568359" Y="-21.890521484375" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.558110351562" Y="-21.842720703125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138515625" Y="-22.07956640625" />
                  <Point X="-3.086267578125" Y="-22.08413671875" />
                  <Point X="-3.052966308594" Y="-22.08705078125" />
                  <Point X="-3.031507324219" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.976166748047" Y="-22.035509765625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.942645507812" Y="-21.919912109375" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.098876220703" Y="-21.6116875" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.980450927734" Y="-21.0001484375" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.357744628906" Y="-20.606142578125" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.089958496094" Y="-20.55326171875" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247070312" Y="-20.726337890625" />
                  <Point X="-1.893095214844" Y="-20.756611328125" />
                  <Point X="-1.856031005859" Y="-20.77590625" />
                  <Point X="-1.835124633789" Y="-20.781509765625" />
                  <Point X="-1.813809204102" Y="-20.77775" />
                  <Point X="-1.753240234375" Y="-20.75266015625" />
                  <Point X="-1.714635375977" Y="-20.736669921875" />
                  <Point X="-1.696905517578" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.666369262695" Y="-20.642986328125" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.668608154297" Y="-20.454796875" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.262703979492" Y="-20.179302734375" />
                  <Point X="-0.968083312988" Y="-20.096703125" />
                  <Point X="-0.489074951172" Y="-20.040640625" />
                  <Point X="-0.224199996948" Y="-20.009640625" />
                  <Point X="-0.16016178894" Y="-20.248634765625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282123566" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594043732" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.129675430298" Y="-20.408361328125" />
                  <Point X="0.236648406982" Y="-20.0091328125" />
                  <Point X="0.602967041016" Y="-20.04749609375" />
                  <Point X="0.860209960938" Y="-20.074435546875" />
                  <Point X="1.256510864258" Y="-20.170115234375" />
                  <Point X="1.508455322266" Y="-20.230943359375" />
                  <Point X="1.766808349609" Y="-20.3246484375" />
                  <Point X="1.931043457031" Y="-20.38421875" />
                  <Point X="2.18046484375" Y="-20.50086328125" />
                  <Point X="2.338683837891" Y="-20.574857421875" />
                  <Point X="2.579659667969" Y="-20.71525" />
                  <Point X="2.732521240234" Y="-20.80430859375" />
                  <Point X="2.959781494141" Y="-20.965923828125" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.756080810547" Y="-21.58494921875" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224851806641" Y="-22.508486328125" />
                  <Point X="2.211739501953" Y="-22.55751953125" />
                  <Point X="2.203382080078" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.207157470703" Y="-22.650076171875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682861328" Y="-22.699189453125" />
                  <Point X="2.244918701172" Y="-22.73785546875" />
                  <Point X="2.261640625" Y="-22.762498046875" />
                  <Point X="2.274938964844" Y="-22.775794921875" />
                  <Point X="2.313603759766" Y="-22.80203125" />
                  <Point X="2.338247558594" Y="-22.81875390625" />
                  <Point X="2.360334960938" Y="-22.827021484375" />
                  <Point X="2.402735351562" Y="-22.8321328125" />
                  <Point X="2.429759765625" Y="-22.835392578125" />
                  <Point X="2.448664306641" Y="-22.8340546875" />
                  <Point X="2.497697998047" Y="-22.82094140625" />
                  <Point X="2.528950683594" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.121964599609" Y="-22.472181640625" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.111918457031" Y="-22.13210546875" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.329284667969" Y="-22.467482421875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.981443603516" Y="-22.87529296875" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.27937109375" Y="-23.41616796875" />
                  <Point X="3.244081298828" Y="-23.462205078125" />
                  <Point X="3.221588867188" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.199974121094" Y="-23.555505859375" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.201570556641" Y="-23.661333984375" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646728516" Y="-23.712046875" />
                  <Point X="3.244997070312" Y="-23.756658203125" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280948242188" Y="-23.801181640625" />
                  <Point X="3.323481201172" Y="-23.825125" />
                  <Point X="3.350590332031" Y="-23.840384765625" />
                  <Point X="3.368566162109" Y="-23.846380859375" />
                  <Point X="3.426073486328" Y="-23.85398046875" />
                  <Point X="3.462726806641" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.027648925781" Y="-23.786177734375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.900244628906" Y="-23.888646484375" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.979112792969" Y="-24.30504296875" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.535163085938" Y="-24.549421875" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087402344" Y="-24.767181640625" />
                  <Point X="3.672588134766" Y="-24.799837890625" />
                  <Point X="3.636577392578" Y="-24.82065234375" />
                  <Point X="3.622265136719" Y="-24.833072265625" />
                  <Point X="3.588365722656" Y="-24.87626953125" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985351562" Y="-24.925265625" />
                  <Point X="3.545685546875" Y="-24.98426953125" />
                  <Point X="3.538483154297" Y="-25.021876953125" />
                  <Point X="3.538482910156" Y="-25.040685546875" />
                  <Point X="3.549782958984" Y="-25.0996875" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566759277344" Y="-25.158759765625" />
                  <Point X="3.600658691406" Y="-25.20195703125" />
                  <Point X="3.622265136719" Y="-25.22948828125" />
                  <Point X="3.636575439453" Y="-25.24190625" />
                  <Point X="3.693074707031" Y="-25.2745625" />
                  <Point X="3.729085449219" Y="-25.29537890625" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.247234863281" Y="-25.43598828125" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.970176269531" Y="-25.822173828125" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.897281738281" Y="-26.190544921875" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.332793457031" Y="-26.21885546875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836425781" Y="-26.098341796875" />
                  <Point X="3.283948486328" Y="-26.122443359375" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.118420654297" Y="-26.23530859375" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.054751708984" Y="-26.41846484375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.117727539062" Y="-26.61207421875" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.638094238281" Y="-27.045904296875" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.265426757813" Y="-27.70295703125" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.098350585938" Y="-27.95244140625" />
                  <Point X="4.056687744141" Y="-28.011638671875" />
                  <Point X="3.573561279297" Y="-27.732705078125" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.605366455078" Y="-27.22948046875" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.379439453125" Y="-27.276947265625" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.230897949219" Y="-27.444322265625" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.212997558594" Y="-27.678349609375" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.535878417969" Y="-28.3012890625" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.908031494141" Y="-29.138259765625" />
                  <Point X="2.835295654297" Y="-29.19021484375" />
                  <Point X="2.717027832031" Y="-29.266765625" />
                  <Point X="2.679774902344" Y="-29.29087890625" />
                  <Point X="2.308994628906" Y="-28.80766796875" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.54038684082" Y="-27.89678515625" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425805664062" Y="-27.83571875" />
                  <Point X="1.283451049805" Y="-27.848818359375" />
                  <Point X="1.19271887207" Y="-27.857166015625" />
                  <Point X="1.165332641602" Y="-27.868509765625" />
                  <Point X="1.05541003418" Y="-27.959908203125" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.935590454102" Y="-28.197197265625" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.999453491211" Y="-28.96033984375" />
                  <Point X="1.127642333984" Y="-29.934029296875" />
                  <Point X="1.063155029297" Y="-29.9481640625" />
                  <Point X="0.994361572266" Y="-29.963244140625" />
                  <Point X="0.885088317871" Y="-29.983095703125" />
                  <Point X="0.860200500488" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#178" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.118021454921" Y="4.795586562198" Z="1.55" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.55" />
                  <Point X="-0.501043024775" Y="5.040299717792" Z="1.55" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.55" />
                  <Point X="-1.282357687583" Y="4.900121759192" Z="1.55" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.55" />
                  <Point X="-1.724336386163" Y="4.56995757033" Z="1.55" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.55" />
                  <Point X="-1.720211036827" Y="4.403329113849" Z="1.55" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.55" />
                  <Point X="-1.778529953687" Y="4.324813330738" Z="1.55" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.55" />
                  <Point X="-1.876163249083" Y="4.319019117196" Z="1.55" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.55" />
                  <Point X="-2.056446899308" Y="4.508456521585" Z="1.55" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.55" />
                  <Point X="-2.388183621896" Y="4.46884544635" Z="1.55" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.55" />
                  <Point X="-3.017028955026" Y="4.070812093102" Z="1.55" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.55" />
                  <Point X="-3.148333406096" Y="3.39459305617" Z="1.55" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.55" />
                  <Point X="-2.998611174216" Y="3.107011645774" Z="1.55" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.55" />
                  <Point X="-3.017677487723" Y="3.031126080164" Z="1.55" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.55" />
                  <Point X="-3.088064819832" Y="2.996953524957" Z="1.55" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.55" />
                  <Point X="-3.539266292127" Y="3.23186054265" Z="1.55" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.55" />
                  <Point X="-3.954751861485" Y="3.171462395404" Z="1.55" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.55" />
                  <Point X="-4.340016845627" Y="2.619632240479" Z="1.55" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.55" />
                  <Point X="-4.027861517212" Y="1.865048931163" Z="1.55" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.55" />
                  <Point X="-3.684985641435" Y="1.588595583434" Z="1.55" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.55" />
                  <Point X="-3.676416802243" Y="1.530541524281" Z="1.55" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.55" />
                  <Point X="-3.71538088746" Y="1.486661014917" Z="1.55" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.55" />
                  <Point X="-4.402475017386" Y="1.56035125137" Z="1.55" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.55" />
                  <Point X="-4.877351399051" Y="1.390282691317" Z="1.55" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.55" />
                  <Point X="-5.006889904506" Y="0.807766176404" Z="1.55" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.55" />
                  <Point X="-4.154137206738" Y="0.203830183435" Z="1.55" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.55" />
                  <Point X="-3.565757546443" Y="0.041571001793" Z="1.55" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.55" />
                  <Point X="-3.545206734462" Y="0.018204184908" Z="1.55" />
                  <Point X="-3.539556741714" Y="0" Z="1.55" />
                  <Point X="-3.543157866673" Y="-0.011602766153" Z="1.55" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.55" />
                  <Point X="-3.559611048681" Y="-0.037304981341" Z="1.55" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.55" />
                  <Point X="-4.482750582034" Y="-0.291881873321" Z="1.55" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.55" />
                  <Point X="-5.030094754285" Y="-0.658024069008" Z="1.55" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.55" />
                  <Point X="-4.929815897462" Y="-1.196560254025" Z="1.55" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.55" />
                  <Point X="-3.85278082431" Y="-1.390281202411" Z="1.55" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.55" />
                  <Point X="-3.208850178109" Y="-1.312930565919" Z="1.55" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.55" />
                  <Point X="-3.195675387082" Y="-1.334029375552" Z="1.55" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.55" />
                  <Point X="-3.995876832783" Y="-1.962602717623" Z="1.55" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.55" />
                  <Point X="-4.38863388554" Y="-2.543263560259" Z="1.55" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.55" />
                  <Point X="-4.074235197089" Y="-3.021406532522" Z="1.55" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.55" />
                  <Point X="-3.074755964804" Y="-2.845272573507" Z="1.55" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.55" />
                  <Point X="-2.566086345136" Y="-2.562244123995" Z="1.55" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.55" />
                  <Point X="-3.010144439661" Y="-3.360322417688" Z="1.55" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.55" />
                  <Point X="-3.14054178508" Y="-3.984959453628" Z="1.55" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.55" />
                  <Point X="-2.719449454302" Y="-4.283397243072" Z="1.55" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.55" />
                  <Point X="-2.313765938117" Y="-4.270541259882" Z="1.55" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.55" />
                  <Point X="-2.125805349915" Y="-4.089355634877" Z="1.55" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.55" />
                  <Point X="-1.847744157858" Y="-3.99198313957" Z="1.55" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.55" />
                  <Point X="-1.56786684341" Y="-4.084005510939" Z="1.55" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.55" />
                  <Point X="-1.401844838139" Y="-4.327390250207" Z="1.55" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.55" />
                  <Point X="-1.394328550946" Y="-4.736927212299" Z="1.55" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.55" />
                  <Point X="-1.297994838808" Y="-4.909118563218" Z="1.55" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.55" />
                  <Point X="-1.000726345569" Y="-4.978230696624" Z="1.55" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.55" />
                  <Point X="-0.573018567347" Y="-4.100718473139" Z="1.55" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.55" />
                  <Point X="-0.35335351798" Y="-3.426945598979" Z="1.55" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.55" />
                  <Point X="-0.154735568874" Y="-3.252263578741" Z="1.55" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.55" />
                  <Point X="0.098623510487" Y="-3.234848466547" Z="1.55" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.55" />
                  <Point X="0.317092341452" Y="-3.374700156789" Z="1.55" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.55" />
                  <Point X="0.661736293054" Y="-4.431817556677" Z="1.55" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.55" />
                  <Point X="0.887868717649" Y="-5.00101005716" Z="1.55" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.55" />
                  <Point X="1.067705040214" Y="-4.965724299135" Z="1.55" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.55" />
                  <Point X="1.042869833251" Y="-3.922533267899" Z="1.55" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.55" />
                  <Point X="0.978293714694" Y="-3.176536780252" Z="1.55" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.55" />
                  <Point X="1.081221163383" Y="-2.967072466998" Z="1.55" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.55" />
                  <Point X="1.281875960835" Y="-2.867326179368" Z="1.55" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.55" />
                  <Point X="1.507191690448" Y="-2.907562997413" Z="1.55" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.55" />
                  <Point X="2.263170898313" Y="-3.806825698108" Z="1.55" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.55" />
                  <Point X="2.738041433093" Y="-4.277460674389" Z="1.55" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.55" />
                  <Point X="2.93093747506" Y="-4.147667598255" Z="1.55" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.55" />
                  <Point X="2.573023641388" Y="-3.245008378544" Z="1.55" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.55" />
                  <Point X="2.256045572873" Y="-2.638182442426" Z="1.55" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.55" />
                  <Point X="2.26898839744" Y="-2.43632833393" Z="1.55" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.55" />
                  <Point X="2.396570044443" Y="-2.289912912527" Z="1.55" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.55" />
                  <Point X="2.590324361087" Y="-2.247402436618" Z="1.55" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.55" />
                  <Point X="3.542405218563" Y="-2.744725858887" Z="1.55" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.55" />
                  <Point X="4.133083261921" Y="-2.949939030082" Z="1.55" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.55" />
                  <Point X="4.301804487935" Y="-2.697961302153" Z="1.55" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.55" />
                  <Point X="3.662376219975" Y="-1.974955456041" Z="1.55" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.55" />
                  <Point X="3.153629477095" Y="-1.553754787591" Z="1.55" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.55" />
                  <Point X="3.098385217061" Y="-1.391765537903" Z="1.55" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.55" />
                  <Point X="3.150710848212" Y="-1.235994066061" Z="1.55" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.55" />
                  <Point X="3.288411945016" Y="-1.140022371516" Z="1.55" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.55" />
                  <Point X="4.320111075638" Y="-1.237147511655" Z="1.55" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.55" />
                  <Point X="4.939872922308" Y="-1.170389692535" Z="1.55" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.55" />
                  <Point X="5.0134616596" Y="-0.79834708072" Z="1.55" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.55" />
                  <Point X="4.254019898193" Y="-0.356410927814" Z="1.55" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.55" />
                  <Point X="3.711941914116" Y="-0.199995757355" Z="1.55" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.55" />
                  <Point X="3.633836243364" Y="-0.139806485476" Z="1.55" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.55" />
                  <Point X="3.5927345666" Y="-0.05900355159" Z="1.55" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.55" />
                  <Point X="3.588636883824" Y="0.037606979612" Z="1.55" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.55" />
                  <Point X="3.621543195037" Y="0.124142253099" Z="1.55" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.55" />
                  <Point X="3.691453500237" Y="0.188153086887" Z="1.55" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.55" />
                  <Point X="4.541947919114" Y="0.433561004955" Z="1.55" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.55" />
                  <Point X="5.022361765904" Y="0.733928586454" Z="1.55" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.55" />
                  <Point X="4.942669791251" Y="1.154460949136" Z="1.55" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.55" />
                  <Point X="4.014966667806" Y="1.294675911422" Z="1.55" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.55" />
                  <Point X="3.42646802802" Y="1.226868274631" Z="1.55" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.55" />
                  <Point X="3.341851485414" Y="1.249728673451" Z="1.55" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.55" />
                  <Point X="3.280611486515" Y="1.302104948285" Z="1.55" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.55" />
                  <Point X="3.244383108912" Y="1.380050150967" Z="1.55" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.55" />
                  <Point X="3.241970528283" Y="1.462308695524" Z="1.55" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.55" />
                  <Point X="3.277608492453" Y="1.538656949862" Z="1.55" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.55" />
                  <Point X="4.005725716726" Y="2.116320365526" Z="1.55" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.55" />
                  <Point X="4.365905962201" Y="2.58968546636" Z="1.55" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.55" />
                  <Point X="4.146347771355" Y="2.928378884458" Z="1.55" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.55" />
                  <Point X="3.090808647623" Y="2.602399260608" Z="1.55" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.55" />
                  <Point X="2.478625386154" Y="2.258641433115" Z="1.55" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.55" />
                  <Point X="2.402567070001" Y="2.248787839295" Z="1.55" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.55" />
                  <Point X="2.335522998684" Y="2.270622343624" Z="1.55" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.55" />
                  <Point X="2.280136256611" Y="2.321501861698" Z="1.55" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.55" />
                  <Point X="2.250641863464" Y="2.387191373832" Z="1.55" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.55" />
                  <Point X="2.253886509543" Y="2.460844189727" Z="1.55" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.55" />
                  <Point X="2.793226092252" Y="3.421330434332" Z="1.55" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.55" />
                  <Point X="2.982602554955" Y="4.106105384685" Z="1.55" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.55" />
                  <Point X="2.598673966999" Y="4.359232747806" Z="1.55" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.55" />
                  <Point X="2.195490571222" Y="4.575707326365" Z="1.55" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.55" />
                  <Point X="1.777701298091" Y="4.753635544382" Z="1.55" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.55" />
                  <Point X="1.262089428417" Y="4.909768926881" Z="1.55" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.55" />
                  <Point X="0.602018932247" Y="5.033513438341" Z="1.55" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.55" />
                  <Point X="0.075223106632" Y="4.635860943753" Z="1.55" />
                  <Point X="0" Y="4.355124473572" Z="1.55" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>