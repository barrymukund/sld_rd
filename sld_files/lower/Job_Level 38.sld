<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#191" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2749" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.855264221191" Y="-29.602142578125" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557720458984" Y="-28.497138671875" />
                  <Point X="0.542363342285" Y="-28.467376953125" />
                  <Point X="0.423031036377" Y="-28.295443359375" />
                  <Point X="0.378635528564" Y="-28.2314765625" />
                  <Point X="0.356752655029" Y="-28.2090234375" />
                  <Point X="0.330497467041" Y="-28.189779296875" />
                  <Point X="0.302495178223" Y="-28.175669921875" />
                  <Point X="0.11783531189" Y="-28.118359375" />
                  <Point X="0.049136035919" Y="-28.097037109375" />
                  <Point X="0.020983318329" Y="-28.092767578125" />
                  <Point X="-0.008657554626" Y="-28.092765625" />
                  <Point X="-0.036823631287" Y="-28.09703515625" />
                  <Point X="-0.221483642578" Y="-28.15434765625" />
                  <Point X="-0.290182769775" Y="-28.17566796875" />
                  <Point X="-0.318183105469" Y="-28.189775390625" />
                  <Point X="-0.344439025879" Y="-28.20901953125" />
                  <Point X="-0.366323425293" Y="-28.2314765625" />
                  <Point X="-0.485655883789" Y="-28.403412109375" />
                  <Point X="-0.530051208496" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.659609069824" Y="-28.917896484375" />
                  <Point X="-0.916584655762" Y="-29.87694140625" />
                  <Point X="-1.002342834473" Y="-29.860294921875" />
                  <Point X="-1.079354125977" Y="-29.845345703125" />
                  <Point X="-1.24641809082" Y="-29.80236328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.260623901367" Y="-29.294443359375" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131203125" />
                  <Point X="-1.49365625" Y="-28.982107421875" />
                  <Point X="-1.556905639648" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886474609" Y="-28.897994140625" />
                  <Point X="-1.643027709961" Y="-28.890966796875" />
                  <Point X="-1.868670654297" Y="-28.876177734375" />
                  <Point X="-1.952616821289" Y="-28.87067578125" />
                  <Point X="-1.983414794922" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.230675537109" Y="-29.0204296875" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.396083984375" Y="-29.178935546875" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.688853515625" Y="-29.159263671875" />
                  <Point X="-2.801728027344" Y="-29.089375" />
                  <Point X="-3.09428515625" Y="-28.864115234375" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.966340087891" Y="-28.61639453125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405576171875" Y="-27.5851875" />
                  <Point X="-2.4145625" Y="-27.5555703125" />
                  <Point X="-2.428781982422" Y="-27.526740234375" />
                  <Point X="-2.446808105469" Y="-27.5015859375" />
                  <Point X="-2.464150390625" Y="-27.484244140625" />
                  <Point X="-2.489299560547" Y="-27.466220703125" />
                  <Point X="-2.518129638672" Y="-27.452" />
                  <Point X="-2.547750488281" Y="-27.44301171875" />
                  <Point X="-2.578688476562" Y="-27.444025390625" />
                  <Point X="-2.610217285156" Y="-27.450296875" />
                  <Point X="-2.639183349609" Y="-27.461197265625" />
                  <Point X="-2.98880859375" Y="-27.663052734375" />
                  <Point X="-3.818024169922" Y="-28.14180078125" />
                  <Point X="-3.993697998047" Y="-27.911001953125" />
                  <Point X="-4.082862060547" Y="-27.793857421875" />
                  <Point X="-4.292612304688" Y="-27.442138671875" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-4.055291748047" Y="-27.226966796875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084576171875" Y="-26.47559375" />
                  <Point X="-3.066611083984" Y="-26.4484609375" />
                  <Point X="-3.053856201172" Y="-26.41983203125" />
                  <Point X="-3.048240722656" Y="-26.398150390625" />
                  <Point X="-3.046151611328" Y="-26.390083984375" />
                  <Point X="-3.043347412109" Y="-26.35965625" />
                  <Point X="-3.045556396484" Y="-26.327986328125" />
                  <Point X="-3.052558105469" Y="-26.298240234375" />
                  <Point X="-3.068640869141" Y="-26.272255859375" />
                  <Point X="-3.089472900391" Y="-26.24830078125" />
                  <Point X="-3.112974365234" Y="-26.228767578125" />
                  <Point X="-3.132276123047" Y="-26.217408203125" />
                  <Point X="-3.13945703125" Y="-26.213181640625" />
                  <Point X="-3.168715820312" Y="-26.201958984375" />
                  <Point X="-3.200604003906" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-3.673297607422" Y="-26.2524921875" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.799204589844" Y="-26.1291796875" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.889572753906" Y="-25.604638671875" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.614313476562" Y="-25.5101796875" />
                  <Point X="-3.532875732422" Y="-25.22041015625" />
                  <Point X="-3.517488769531" Y="-25.214826171875" />
                  <Point X="-3.487729980469" Y="-25.199470703125" />
                  <Point X="-3.467502441406" Y="-25.185431640625" />
                  <Point X="-3.459977050781" Y="-25.180208984375" />
                  <Point X="-3.437523681641" Y="-25.158326171875" />
                  <Point X="-3.418278076172" Y="-25.1320703125" />
                  <Point X="-3.404168212891" Y="-25.104068359375" />
                  <Point X="-3.397425537109" Y="-25.08234375" />
                  <Point X="-3.394917236328" Y="-25.07426171875" />
                  <Point X="-3.390647705078" Y="-25.046103515625" />
                  <Point X="-3.390647216797" Y="-25.01646484375" />
                  <Point X="-3.394916503906" Y="-24.9883046875" />
                  <Point X="-3.401658935547" Y="-24.966580078125" />
                  <Point X="-3.404167480469" Y="-24.95849609375" />
                  <Point X="-3.418276367188" Y="-24.9304921875" />
                  <Point X="-3.437522460938" Y="-24.904234375" />
                  <Point X="-3.459977050781" Y="-24.8823515625" />
                  <Point X="-3.480204589844" Y="-24.8683125" />
                  <Point X="-3.489306640625" Y="-24.8627265625" />
                  <Point X="-3.512398681641" Y="-24.850283203125" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-3.935204589844" Y="-24.73434765625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.846961914062" Y="-24.17490234375" />
                  <Point X="-4.82448828125" Y="-24.023029296875" />
                  <Point X="-4.712777832031" Y="-23.610783203125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.540867675781" Y="-23.598150390625" />
                  <Point X="-3.765666503906" Y="-23.70020703125" />
                  <Point X="-3.744985595703" Y="-23.700658203125" />
                  <Point X="-3.723423095703" Y="-23.698771484375" />
                  <Point X="-3.703138183594" Y="-23.694736328125" />
                  <Point X="-3.658368164062" Y="-23.68062109375" />
                  <Point X="-3.641712158203" Y="-23.675369140625" />
                  <Point X="-3.622776367188" Y="-23.667037109375" />
                  <Point X="-3.604032470703" Y="-23.65621484375" />
                  <Point X="-3.587352783203" Y="-23.643984375" />
                  <Point X="-3.573715332031" Y="-23.62843359375" />
                  <Point X="-3.561301269531" Y="-23.610705078125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.533387207031" Y="-23.549201171875" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532050292969" Y="-23.41062109375" />
                  <Point X="-3.553726074219" Y="-23.368982421875" />
                  <Point X="-3.561790039062" Y="-23.3534921875" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-3.832912353516" Y="-23.128328125" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.168477539062" Y="-22.415947265625" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.785239013672" Y="-21.885984375" />
                  <Point X="-3.750505126953" Y="-21.84133984375" />
                  <Point X="-3.682763916016" Y="-21.88044921875" />
                  <Point X="-3.206656982422" Y="-22.155330078125" />
                  <Point X="-3.187728759766" Y="-22.163658203125" />
                  <Point X="-3.167086181641" Y="-22.17016796875" />
                  <Point X="-3.146794189453" Y="-22.174205078125" />
                  <Point X="-3.084441894531" Y="-22.17966015625" />
                  <Point X="-3.061244873047" Y="-22.181689453125" />
                  <Point X="-3.040560302734" Y="-22.18123828125" />
                  <Point X="-3.019102050781" Y="-22.178412109375" />
                  <Point X="-2.999012695312" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946078125" Y="-22.139771484375" />
                  <Point X="-2.901819824219" Y="-22.095513671875" />
                  <Point X="-2.885354492188" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.848890869141" Y="-21.90152734375" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-2.972059326172" Y="-21.641341796875" />
                  <Point X="-3.183333007812" Y="-21.27540625" />
                  <Point X="-2.8527109375" Y="-21.021919921875" />
                  <Point X="-2.70062109375" Y="-20.905314453125" />
                  <Point X="-2.234570800781" Y="-20.64638671875" />
                  <Point X="-2.167036621094" Y="-20.608865234375" />
                  <Point X="-2.043195800781" Y="-20.7702578125" />
                  <Point X="-2.028894897461" Y="-20.785197265625" />
                  <Point X="-2.012316650391" Y="-20.799109375" />
                  <Point X="-1.995115112305" Y="-20.810603515625" />
                  <Point X="-1.925717163086" Y="-20.84673046875" />
                  <Point X="-1.899899169922" Y="-20.860171875" />
                  <Point X="-1.880626342773" Y="-20.86766796875" />
                  <Point X="-1.859719116211" Y="-20.873271484375" />
                  <Point X="-1.83926965332" Y="-20.876419921875" />
                  <Point X="-1.818623901367" Y="-20.87506640625" />
                  <Point X="-1.797307617188" Y="-20.871306640625" />
                  <Point X="-1.777453125" Y="-20.865517578125" />
                  <Point X="-1.705170532227" Y="-20.835576171875" />
                  <Point X="-1.678279052734" Y="-20.8244375" />
                  <Point X="-1.660146362305" Y="-20.814490234375" />
                  <Point X="-1.642416625977" Y="-20.802076171875" />
                  <Point X="-1.626864379883" Y="-20.7884375" />
                  <Point X="-1.6146328125" Y="-20.771755859375" />
                  <Point X="-1.603810913086" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734078125" />
                  <Point X="-1.571953857422" Y="-20.6594609375" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.569356689453" Y="-20.48086328125" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.14651965332" Y="-20.245392578125" />
                  <Point X="-0.949623840332" Y="-20.19019140625" />
                  <Point X="-0.384639526367" Y="-20.12406640625" />
                  <Point X="-0.294711456299" Y="-20.113541015625" />
                  <Point X="-0.27474887085" Y="-20.18804296875" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113975525" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907631" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425926208" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.198614074707" Y="-20.51812890625" />
                  <Point X="0.307419494629" Y="-20.1120625" />
                  <Point X="0.672125183105" Y="-20.1502578125" />
                  <Point X="0.844031188965" Y="-20.168259765625" />
                  <Point X="1.311480834961" Y="-20.2811171875" />
                  <Point X="1.481039794922" Y="-20.3220546875" />
                  <Point X="1.784884155273" Y="-20.432259765625" />
                  <Point X="1.894648193359" Y="-20.47207421875" />
                  <Point X="2.188850341797" Y="-20.609662109375" />
                  <Point X="2.294556152344" Y="-20.659095703125" />
                  <Point X="2.578813964844" Y="-20.824705078125" />
                  <Point X="2.680975097656" Y="-20.884224609375" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.775575439453" Y="-21.36118359375" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.117428466797" Y="-22.542462890625" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.113829589844" Y="-22.6696484375" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140071044922" Y="-22.752529296875" />
                  <Point X="2.171380615234" Y="-22.798671875" />
                  <Point X="2.183028808594" Y="-22.815837890625" />
                  <Point X="2.194465576172" Y="-22.829671875" />
                  <Point X="2.221596923828" Y="-22.85440625" />
                  <Point X="2.267739257812" Y="-22.885716796875" />
                  <Point X="2.284905517578" Y="-22.897365234375" />
                  <Point X="2.304949707031" Y="-22.9077265625" />
                  <Point X="2.327040771484" Y="-22.915994140625" />
                  <Point X="2.348968017578" Y="-22.921337890625" />
                  <Point X="2.3995625" Y="-22.9274375" />
                  <Point X="2.408583496094" Y="-22.928091796875" />
                  <Point X="2.446315917969" Y="-22.929025390625" />
                  <Point X="2.473208251953" Y="-22.925830078125" />
                  <Point X="2.531724609375" Y="-22.910181640625" />
                  <Point X="2.553494628906" Y="-22.904359375" />
                  <Point X="2.565291748047" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.88985546875" />
                  <Point X="2.993198974609" Y="-22.65622265625" />
                  <Point X="3.967326904297" Y="-22.093810546875" />
                  <Point X="4.062670898438" Y="-22.22631640625" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.26219921875" Y="-22.540115234375" />
                  <Point X="4.05759765625" Y="-22.697111328125" />
                  <Point X="3.230784179688" Y="-23.331548828125" />
                  <Point X="3.221421630859" Y="-23.33976171875" />
                  <Point X="3.203974121094" Y="-23.358373046875" />
                  <Point X="3.161859619141" Y="-23.413314453125" />
                  <Point X="3.146191894531" Y="-23.43375390625" />
                  <Point X="3.136607421875" Y="-23.4490859375" />
                  <Point X="3.121629638672" Y="-23.4829140625" />
                  <Point X="3.105941894531" Y="-23.539009765625" />
                  <Point X="3.100105712891" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.110617431641" Y="-23.69064453125" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120680419922" Y="-23.7310234375" />
                  <Point X="3.136282958984" Y="-23.764259765625" />
                  <Point X="3.171309570313" Y="-23.817498046875" />
                  <Point X="3.184340576172" Y="-23.8373046875" />
                  <Point X="3.198890136719" Y="-23.854544921875" />
                  <Point X="3.216132324219" Y="-23.870634765625" />
                  <Point X="3.234345703125" Y="-23.88396484375" />
                  <Point X="3.285104003906" Y="-23.912537109375" />
                  <Point X="3.303987792969" Y="-23.92316796875" />
                  <Point X="3.320525146484" Y="-23.9305" />
                  <Point X="3.356120605469" Y="-23.9405625" />
                  <Point X="3.424749267578" Y="-23.9496328125" />
                  <Point X="3.45028125" Y="-23.953005859375" />
                  <Point X="3.462700439453" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="3.872608154297" Y="-23.902408203125" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.819908203125" Y="-23.96027734375" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.664125976563" Y="-24.416515625" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704784912109" Y="-24.674416015625" />
                  <Point X="3.681547851562" Y="-24.68493359375" />
                  <Point X="3.614122314453" Y="-24.72390625" />
                  <Point X="3.589037841797" Y="-24.738404296875" />
                  <Point X="3.574317626953" Y="-24.7488984375" />
                  <Point X="3.547530517578" Y="-24.774423828125" />
                  <Point X="3.507075195312" Y="-24.825974609375" />
                  <Point X="3.492024658203" Y="-24.84515234375" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.470526855469" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.450195556641" Y="-24.977810546875" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.458664306641" Y="-25.12896875" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526611328" Y="-25.1766640625" />
                  <Point X="3.480300537109" Y="-25.19812890625" />
                  <Point X="3.492024658203" Y="-25.217408203125" />
                  <Point X="3.532479980469" Y="-25.268958984375" />
                  <Point X="3.547530517578" Y="-25.28813671875" />
                  <Point X="3.559997314453" Y="-25.301236328125" />
                  <Point X="3.58903515625" Y="-25.32415625" />
                  <Point X="3.656460693359" Y="-25.36312890625" />
                  <Point X="3.681545166016" Y="-25.37762890625" />
                  <Point X="3.692710205078" Y="-25.383140625" />
                  <Point X="3.716580322266" Y="-25.39215234375" />
                  <Point X="4.069096679688" Y="-25.486607421875" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.869555664062" Y="-25.85233203125" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.523268554688" Y="-26.14811328125" />
                  <Point X="3.424381591797" Y="-26.00344140625" />
                  <Point X="3.408035400391" Y="-26.0027109375" />
                  <Point X="3.374659423828" Y="-26.005509765625" />
                  <Point X="3.242326904297" Y="-26.0342734375" />
                  <Point X="3.193095214844" Y="-26.04497265625" />
                  <Point X="3.1639765625" Y="-26.056595703125" />
                  <Point X="3.136148681641" Y="-26.07348828125" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.032410644531" Y="-26.19016015625" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.958293457031" Y="-26.429947265625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.507564453125" />
                  <Point X="2.964078613281" Y="-26.539185546875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.049685546875" Y="-26.681908203125" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086931396484" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.437765869141" Y="-27.011931640625" />
                  <Point X="4.213122558594" Y="-27.6068828125" />
                  <Point X="4.165776367188" Y="-27.68349609375" />
                  <Point X="4.124813964844" Y="-27.74978125" />
                  <Point X="4.028980224609" Y="-27.8859453125" />
                  <Point X="3.779661621094" Y="-27.742001953125" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224365234" Y="-27.159826171875" />
                  <Point X="2.596727539062" Y="-27.1313828125" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.506784179688" Y="-27.120396484375" />
                  <Point X="2.474610839844" Y="-27.125353515625" />
                  <Point X="2.444833496094" Y="-27.135177734375" />
                  <Point X="2.313992431641" Y="-27.204037109375" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242384033203" Y="-27.246548828125" />
                  <Point X="2.221425048828" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.135671142578" Y="-27.42128125" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229492188" Y="-27.499732421875" />
                  <Point X="2.095271484375" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.124118896484" Y="-27.720755859375" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.362037841797" Y="-28.190189453125" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.830463378906" Y="-29.076919921875" />
                  <Point X="2.781862548828" Y="-29.11163671875" />
                  <Point X="2.701763916016" Y="-29.163482421875" />
                  <Point X="2.505389648438" Y="-28.9075625" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506225586" Y="-27.922181640625" />
                  <Point X="1.721923583984" Y="-27.900556640625" />
                  <Point X="1.566589111328" Y="-27.80069140625" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986938477" Y="-27.75116796875" />
                  <Point X="1.44836730957" Y="-27.7434375" />
                  <Point X="1.417100708008" Y="-27.741119140625" />
                  <Point X="1.247216186523" Y="-27.756751953125" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.156364746094" Y="-27.769396484375" />
                  <Point X="1.128977905273" Y="-27.780740234375" />
                  <Point X="1.104594970703" Y="-27.7954609375" />
                  <Point X="0.973414550781" Y="-27.904533203125" />
                  <Point X="0.924611206055" Y="-27.94511328125" />
                  <Point X="0.904139526367" Y="-27.968865234375" />
                  <Point X="0.887248168945" Y="-27.99669140625" />
                  <Point X="0.875624511719" Y="-28.025810546875" />
                  <Point X="0.836402160645" Y="-28.206263671875" />
                  <Point X="0.821810241699" Y="-28.273396484375" />
                  <Point X="0.81972479248" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.879316467285" Y="-28.7756328125" />
                  <Point X="1.022065551758" Y="-29.859916015625" />
                  <Point X="1.021667663574" Y="-29.86000390625" />
                  <Point X="0.975712585449" Y="-29.870076171875" />
                  <Point X="0.929315185547" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058452636719" Y="-29.75262890625" />
                  <Point X="-1.14124621582" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.16744934082" Y="-29.27591015625" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026245117" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230573242188" Y="-29.094861328125" />
                  <Point X="-1.250208251953" Y="-29.070935546875" />
                  <Point X="-1.261006958008" Y="-29.05977734375" />
                  <Point X="-1.431018432617" Y="-28.910681640625" />
                  <Point X="-1.494267822266" Y="-28.855212890625" />
                  <Point X="-1.506740112305" Y="-28.84596484375" />
                  <Point X="-1.533023681641" Y="-28.82962109375" />
                  <Point X="-1.546834838867" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591316040039" Y="-28.805474609375" />
                  <Point X="-1.621457275391" Y="-28.798447265625" />
                  <Point X="-1.636814575195" Y="-28.796169921875" />
                  <Point X="-1.862457519531" Y="-28.781380859375" />
                  <Point X="-1.946403686523" Y="-28.77587890625" />
                  <Point X="-1.961928100586" Y="-28.7761328125" />
                  <Point X="-1.992726074219" Y="-28.779166015625" />
                  <Point X="-2.007999511719" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053667480469" Y="-28.795494140625" />
                  <Point X="-2.081861816406" Y="-28.808267578125" />
                  <Point X="-2.095436523437" Y="-28.815810546875" />
                  <Point X="-2.283454589844" Y="-28.941439453125" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359683349609" Y="-28.9927578125" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.471452636719" Y="-29.121103515625" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.638842529297" Y="-29.0784921875" />
                  <Point X="-2.747615234375" Y="-29.011142578125" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.884067626953" Y="-28.66389453125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.61923046875" />
                  <Point X="-2.310626953125" Y="-27.58829296875" />
                  <Point X="-2.314668701172" Y="-27.55760546875" />
                  <Point X="-2.323655029297" Y="-27.52798828125" />
                  <Point X="-2.329362060547" Y="-27.513548828125" />
                  <Point X="-2.343581542969" Y="-27.48471875" />
                  <Point X="-2.351562744141" Y="-27.47140234375" />
                  <Point X="-2.369588867188" Y="-27.446248046875" />
                  <Point X="-2.379633789062" Y="-27.43441015625" />
                  <Point X="-2.396976074219" Y="-27.417068359375" />
                  <Point X="-2.408811279297" Y="-27.407025390625" />
                  <Point X="-2.433960449219" Y="-27.389001953125" />
                  <Point X="-2.447274414062" Y="-27.381021484375" />
                  <Point X="-2.476104492188" Y="-27.36680078125" />
                  <Point X="-2.490544433594" Y="-27.36109375" />
                  <Point X="-2.520165283203" Y="-27.35210546875" />
                  <Point X="-2.550861572266" Y="-27.3480625" />
                  <Point X="-2.581799560547" Y="-27.349076171875" />
                  <Point X="-2.597222167969" Y="-27.3508515625" />
                  <Point X="-2.628750976562" Y="-27.357123046875" />
                  <Point X="-2.643676513672" Y="-27.361384765625" />
                  <Point X="-2.672642578125" Y="-27.37228515625" />
                  <Point X="-2.686683105469" Y="-27.378923828125" />
                  <Point X="-3.036308349609" Y="-27.580779296875" />
                  <Point X="-3.793089355469" Y="-28.01770703125" />
                  <Point X="-3.918104736328" Y="-27.853462890625" />
                  <Point X="-4.004021484375" Y="-27.740583984375" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.997459472656" Y="-27.3023359375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481201172" Y="-26.563310546875" />
                  <Point X="-3.015102783203" Y="-26.540388671875" />
                  <Point X="-3.005365478516" Y="-26.528041015625" />
                  <Point X="-2.987400390625" Y="-26.500908203125" />
                  <Point X="-2.979833740234" Y="-26.487123046875" />
                  <Point X="-2.967078857422" Y="-26.458494140625" />
                  <Point X="-2.961890625" Y="-26.443650390625" />
                  <Point X="-2.956275146484" Y="-26.42196875" />
                  <Point X="-2.951552490234" Y="-26.398802734375" />
                  <Point X="-2.948748291016" Y="-26.368375" />
                  <Point X="-2.948577636719" Y="-26.353046875" />
                  <Point X="-2.950786621094" Y="-26.321376953125" />
                  <Point X="-2.953083496094" Y="-26.306220703125" />
                  <Point X="-2.960085205078" Y="-26.276474609375" />
                  <Point X="-2.971779052734" Y="-26.2482421875" />
                  <Point X="-2.987861816406" Y="-26.2222578125" />
                  <Point X="-2.996955566406" Y="-26.209916015625" />
                  <Point X="-3.017787597656" Y="-26.1859609375" />
                  <Point X="-3.028749755859" Y="-26.1752421875" />
                  <Point X="-3.052251220703" Y="-26.155708984375" />
                  <Point X="-3.064790527344" Y="-26.14689453125" />
                  <Point X="-3.084092285156" Y="-26.13553515625" />
                  <Point X="-3.105435058594" Y="-26.124482421875" />
                  <Point X="-3.134693847656" Y="-26.113259765625" />
                  <Point X="-3.149790771484" Y="-26.10886328125" />
                  <Point X="-3.181678955078" Y="-26.102380859375" />
                  <Point X="-3.197294921875" Y="-26.10053515625" />
                  <Point X="-3.228619873047" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-3.685697509766" Y="-26.1583046875" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.707159667969" Y="-26.10566796875" />
                  <Point X="-4.740762695312" Y="-25.974115234375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.589725097656" Y="-25.601943359375" />
                  <Point X="-3.508287353516" Y="-25.312173828125" />
                  <Point X="-3.500468017578" Y="-25.3097109375" />
                  <Point X="-3.473926513672" Y="-25.29925" />
                  <Point X="-3.444167724609" Y="-25.28389453125" />
                  <Point X="-3.433562744141" Y="-25.277515625" />
                  <Point X="-3.413335205078" Y="-25.2634765625" />
                  <Point X="-3.393671875" Y="-25.2482421875" />
                  <Point X="-3.371218505859" Y="-25.226359375" />
                  <Point X="-3.360903076172" Y="-25.21448828125" />
                  <Point X="-3.341657470703" Y="-25.188232421875" />
                  <Point X="-3.333439941406" Y="-25.174818359375" />
                  <Point X="-3.319330078125" Y="-25.14681640625" />
                  <Point X="-3.313437744141" Y="-25.132228515625" />
                  <Point X="-3.306695068359" Y="-25.11050390625" />
                  <Point X="-3.300990722656" Y="-25.08850390625" />
                  <Point X="-3.296721191406" Y="-25.060345703125" />
                  <Point X="-3.295647705078" Y="-25.04610546875" />
                  <Point X="-3.295647216797" Y="-25.016466796875" />
                  <Point X="-3.296720458984" Y="-25.002224609375" />
                  <Point X="-3.300989746094" Y="-24.974064453125" />
                  <Point X="-3.304185791016" Y="-24.960146484375" />
                  <Point X="-3.310928222656" Y="-24.938421875" />
                  <Point X="-3.319326904297" Y="-24.915751953125" />
                  <Point X="-3.333435791016" Y="-24.887748046875" />
                  <Point X="-3.341654541016" Y="-24.874330078125" />
                  <Point X="-3.360900634766" Y="-24.848072265625" />
                  <Point X="-3.371219238281" Y="-24.83619921875" />
                  <Point X="-3.393673828125" Y="-24.81431640625" />
                  <Point X="-3.405809814453" Y="-24.804306640625" />
                  <Point X="-3.426037353516" Y="-24.790267578125" />
                  <Point X="-3.444241455078" Y="-24.779095703125" />
                  <Point X="-3.467333496094" Y="-24.76665234375" />
                  <Point X="-3.477333007812" Y="-24.7619921875" />
                  <Point X="-3.497810546875" Y="-24.753859375" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.910616943359" Y="-24.642583984375" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.752985351562" Y="-24.18880859375" />
                  <Point X="-4.731331542969" Y="-24.042474609375" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.553268066406" Y="-23.692337890625" />
                  <Point X="-3.778066650391" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747057617188" Y="-23.795634765625" />
                  <Point X="-3.736704833984" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704888427734" Y="-23.7919453125" />
                  <Point X="-3.684603515625" Y="-23.78791015625" />
                  <Point X="-3.674572509766" Y="-23.78533984375" />
                  <Point X="-3.629802490234" Y="-23.771224609375" />
                  <Point X="-3.603450927734" Y="-23.76232421875" />
                  <Point X="-3.584515136719" Y="-23.7539921875" />
                  <Point X="-3.575274902344" Y="-23.74930859375" />
                  <Point X="-3.556531005859" Y="-23.738486328125" />
                  <Point X="-3.547856933594" Y="-23.732826171875" />
                  <Point X="-3.531177246094" Y="-23.720595703125" />
                  <Point X="-3.515927490234" Y="-23.70662109375" />
                  <Point X="-3.502290039063" Y="-23.6910703125" />
                  <Point X="-3.495896728516" Y="-23.682923828125" />
                  <Point X="-3.483482666016" Y="-23.6651953125" />
                  <Point X="-3.478013427734" Y="-23.656400390625" />
                  <Point X="-3.468063720703" Y="-23.638265625" />
                  <Point X="-3.463583251953" Y="-23.62892578125" />
                  <Point X="-3.445618896484" Y="-23.585556640625" />
                  <Point X="-3.435498779297" Y="-23.5596484375" />
                  <Point X="-3.429710205078" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436011962891" Y="-23.395470703125" />
                  <Point X="-3.443508789062" Y="-23.37619140625" />
                  <Point X="-3.447784423828" Y="-23.36675390625" />
                  <Point X="-3.469460205078" Y="-23.325115234375" />
                  <Point X="-3.482802246094" Y="-23.3007109375" />
                  <Point X="-3.494293945312" Y="-23.283513671875" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521497558594" Y="-23.25109375" />
                  <Point X="-3.536439208984" Y="-23.2367890625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.775079833984" Y="-23.052958984375" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.086431152344" Y="-22.4638359375" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.726339111328" Y="-21.96498828125" />
                  <Point X="-3.254156005859" Y="-22.237603515625" />
                  <Point X="-3.244916015625" Y="-22.24228515625" />
                  <Point X="-3.225987792969" Y="-22.25061328125" />
                  <Point X="-3.216300537109" Y="-22.254259765625" />
                  <Point X="-3.195657958984" Y="-22.26076953125" />
                  <Point X="-3.185623291016" Y="-22.263341796875" />
                  <Point X="-3.165331298828" Y="-22.26737890625" />
                  <Point X="-3.155073974609" Y="-22.26884375" />
                  <Point X="-3.092721679688" Y="-22.274298828125" />
                  <Point X="-3.069524658203" Y="-22.276328125" />
                  <Point X="-3.059173339844" Y="-22.276666015625" />
                  <Point X="-3.038488769531" Y="-22.27621484375" />
                  <Point X="-3.028155517578" Y="-22.27542578125" />
                  <Point X="-3.006697265625" Y="-22.272599609375" />
                  <Point X="-2.996512695312" Y="-22.2706875" />
                  <Point X="-2.976423339844" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.938449462891" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.90274609375" Y="-22.226806640625" />
                  <Point X="-2.886614501953" Y="-22.213859375" />
                  <Point X="-2.878903320312" Y="-22.206947265625" />
                  <Point X="-2.834645019531" Y="-22.162689453125" />
                  <Point X="-2.8181796875" Y="-22.146224609375" />
                  <Point X="-2.811267089844" Y="-22.138513671875" />
                  <Point X="-2.798321533203" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.754252441406" Y="-21.893248046875" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.889787109375" Y="-21.593841796875" />
                  <Point X="-3.059387939453" Y="-21.3000859375" />
                  <Point X="-2.794908691406" Y="-21.097310546875" />
                  <Point X="-2.648377441406" Y="-20.984966796875" />
                  <Point X="-2.192523681641" Y="-20.731705078125" />
                  <Point X="-2.118564453125" Y="-20.82808984375" />
                  <Point X="-2.111821533203" Y="-20.835951171875" />
                  <Point X="-2.097520751953" Y="-20.850890625" />
                  <Point X="-2.089963134766" Y="-20.85796875" />
                  <Point X="-2.073384765625" Y="-20.871880859375" />
                  <Point X="-2.065097167969" Y="-20.87809765625" />
                  <Point X="-2.047895629883" Y="-20.889591796875" />
                  <Point X="-2.038981811523" Y="-20.894869140625" />
                  <Point X="-1.969583862305" Y="-20.93099609375" />
                  <Point X="-1.943765991211" Y="-20.9444375" />
                  <Point X="-1.9343359375" Y="-20.9487109375" />
                  <Point X="-1.915063110352" Y="-20.95620703125" />
                  <Point X="-1.905220092773" Y="-20.9594296875" />
                  <Point X="-1.884312866211" Y="-20.965033203125" />
                  <Point X="-1.874175170898" Y="-20.967166015625" />
                  <Point X="-1.853725708008" Y="-20.970314453125" />
                  <Point X="-1.833054931641" Y="-20.971216796875" />
                  <Point X="-1.812409179688" Y="-20.96986328125" />
                  <Point X="-1.802122558594" Y="-20.968623046875" />
                  <Point X="-1.780806274414" Y="-20.96486328125" />
                  <Point X="-1.770715332031" Y="-20.962509765625" />
                  <Point X="-1.750860839844" Y="-20.956720703125" />
                  <Point X="-1.741097167969" Y="-20.95328515625" />
                  <Point X="-1.668814575195" Y="-20.92334375" />
                  <Point X="-1.641923095703" Y="-20.912205078125" />
                  <Point X="-1.632587524414" Y="-20.907728515625" />
                  <Point X="-1.614454956055" Y="-20.89778125" />
                  <Point X="-1.605657958984" Y="-20.892310546875" />
                  <Point X="-1.587928100586" Y="-20.879896484375" />
                  <Point X="-1.579779541016" Y="-20.873501953125" />
                  <Point X="-1.564227294922" Y="-20.85986328125" />
                  <Point X="-1.550252319336" Y="-20.844611328125" />
                  <Point X="-1.538020874023" Y="-20.8279296875" />
                  <Point X="-1.532360351562" Y="-20.819255859375" />
                  <Point X="-1.521538330078" Y="-20.80051171875" />
                  <Point X="-1.51685546875" Y="-20.791271484375" />
                  <Point X="-1.508525024414" Y="-20.772337890625" />
                  <Point X="-1.504877319336" Y="-20.76264453125" />
                  <Point X="-1.481350708008" Y="-20.68802734375" />
                  <Point X="-1.47259777832" Y="-20.660267578125" />
                  <Point X="-1.470026611328" Y="-20.650236328125" />
                  <Point X="-1.465991333008" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.475169433594" Y="-20.468462890625" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-1.120873779297" Y="-20.336865234375" />
                  <Point X="-0.93116418457" Y="-20.2836796875" />
                  <Point X="-0.373596191406" Y="-20.218421875" />
                  <Point X="-0.365222839355" Y="-20.21744140625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451187134" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.290377044678" Y="-20.542716796875" />
                  <Point X="0.378190643311" Y="-20.2149921875" />
                  <Point X="0.662230041504" Y="-20.244740234375" />
                  <Point X="0.827852600098" Y="-20.262083984375" />
                  <Point X="1.289185424805" Y="-20.37346484375" />
                  <Point X="1.453622802734" Y="-20.413166015625" />
                  <Point X="1.7524921875" Y="-20.52156640625" />
                  <Point X="1.858250244141" Y="-20.559927734375" />
                  <Point X="2.148605712891" Y="-20.695716796875" />
                  <Point X="2.250428222656" Y="-20.743333984375" />
                  <Point X="2.530990966797" Y="-20.906791015625" />
                  <Point X="2.629434082031" Y="-20.96414453125" />
                  <Point X="2.817779052734" Y="-21.098083984375" />
                  <Point X="2.693302978516" Y="-21.31368359375" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.025653198242" Y="-22.517921875" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017912231445" Y="-22.5485390625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.019512817383" Y="-22.681021484375" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144287109" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.045319580078" Y="-22.77611328125" />
                  <Point X="2.055681396484" Y="-22.79615625" />
                  <Point X="2.061459716797" Y="-22.80587109375" />
                  <Point X="2.092769287109" Y="-22.852013671875" />
                  <Point X="2.104417480469" Y="-22.8691796875" />
                  <Point X="2.109810058594" Y="-22.876369140625" />
                  <Point X="2.130463134766" Y="-22.899876953125" />
                  <Point X="2.157594482422" Y="-22.924611328125" />
                  <Point X="2.168254638672" Y="-22.933017578125" />
                  <Point X="2.214396972656" Y="-22.964328125" />
                  <Point X="2.231563232422" Y="-22.9759765625" />
                  <Point X="2.241281494141" Y="-22.981755859375" />
                  <Point X="2.261325683594" Y="-22.9921171875" />
                  <Point X="2.271651611328" Y="-22.99669921875" />
                  <Point X="2.293742675781" Y="-23.004966796875" />
                  <Point X="2.304547363281" Y="-23.00829296875" />
                  <Point X="2.326474609375" Y="-23.01363671875" />
                  <Point X="2.337597167969" Y="-23.015654296875" />
                  <Point X="2.388191650391" Y="-23.02175390625" />
                  <Point X="2.406233642578" Y="-23.0230625" />
                  <Point X="2.443966064453" Y="-23.02399609375" />
                  <Point X="2.457524902344" Y="-23.023361328125" />
                  <Point X="2.484417236328" Y="-23.020166015625" />
                  <Point X="2.497750732422" Y="-23.01760546875" />
                  <Point X="2.556267089844" Y="-23.00195703125" />
                  <Point X="2.578037109375" Y="-22.996134765625" />
                  <Point X="2.584" Y="-22.994328125" />
                  <Point X="2.604415527344" Y="-22.9869296875" />
                  <Point X="2.627657714844" Y="-22.97642578125" />
                  <Point X="2.636033935547" Y="-22.97212890625" />
                  <Point X="3.040698974609" Y="-22.73849609375" />
                  <Point X="3.940405761719" Y="-22.21905078125" />
                  <Point X="3.98555859375" Y="-22.281802734375" />
                  <Point X="4.043957763672" Y="-22.36296484375" />
                  <Point X="4.136884765625" Y="-22.51652734375" />
                  <Point X="3.999765380859" Y="-22.6217421875" />
                  <Point X="3.172951904297" Y="-23.2561796875" />
                  <Point X="3.168136962891" Y="-23.2601328125" />
                  <Point X="3.152114501953" Y="-23.2747890625" />
                  <Point X="3.134666992188" Y="-23.293400390625" />
                  <Point X="3.128576660156" Y="-23.300578125" />
                  <Point X="3.086462158203" Y="-23.35551953125" />
                  <Point X="3.070794433594" Y="-23.375958984375" />
                  <Point X="3.06563671875" Y="-23.383396484375" />
                  <Point X="3.049741210938" Y="-23.410625" />
                  <Point X="3.034763427734" Y="-23.444453125" />
                  <Point X="3.030139892578" Y="-23.457328125" />
                  <Point X="3.014452148438" Y="-23.513423828125" />
                  <Point X="3.008615966797" Y="-23.53429296875" />
                  <Point X="3.006224853516" Y="-23.54533984375" />
                  <Point X="3.002771484375" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.017577392578" Y="-23.709841796875" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024598388672" Y="-23.741767578125" />
                  <Point X="3.034684814453" Y="-23.771392578125" />
                  <Point X="3.050287353516" Y="-23.80462890625" />
                  <Point X="3.056919433594" Y="-23.816474609375" />
                  <Point X="3.091946044922" Y="-23.869712890625" />
                  <Point X="3.104977050781" Y="-23.88951953125" />
                  <Point X="3.111739257813" Y="-23.89857421875" />
                  <Point X="3.126288818359" Y="-23.915814453125" />
                  <Point X="3.134076171875" Y="-23.924" />
                  <Point X="3.151318359375" Y="-23.94008984375" />
                  <Point X="3.160025146484" Y="-23.947296875" />
                  <Point X="3.178238525391" Y="-23.960626953125" />
                  <Point X="3.187745117188" Y="-23.96675" />
                  <Point X="3.238503417969" Y="-23.995322265625" />
                  <Point X="3.257387207031" Y="-24.005953125" />
                  <Point X="3.265483154297" Y="-24.010015625" />
                  <Point X="3.294682373047" Y="-24.02191796875" />
                  <Point X="3.330277832031" Y="-24.03198046875" />
                  <Point X="3.343673095703" Y="-24.034744140625" />
                  <Point X="3.412301757812" Y="-24.043814453125" />
                  <Point X="3.437833740234" Y="-24.0471875" />
                  <Point X="3.444034912109" Y="-24.04780078125" />
                  <Point X="3.4657109375" Y="-24.04877734375" />
                  <Point X="3.491214111328" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="3.885008056641" Y="-23.996595703125" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.727604003906" Y="-23.982748046875" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="4.639537597656" Y="-24.324751953125" />
                  <Point X="3.691991210938" Y="-24.578646484375" />
                  <Point X="3.686028320312" Y="-24.58045703125" />
                  <Point X="3.665611816406" Y="-24.587869140625" />
                  <Point X="3.642374755859" Y="-24.59838671875" />
                  <Point X="3.634007080078" Y="-24.602685546875" />
                  <Point X="3.566581542969" Y="-24.641658203125" />
                  <Point X="3.541497070312" Y="-24.65615625" />
                  <Point X="3.533890869141" Y="-24.661048828125" />
                  <Point X="3.508781738281" Y="-24.680123046875" />
                  <Point X="3.481994628906" Y="-24.7056484375" />
                  <Point X="3.472795898438" Y="-24.715775390625" />
                  <Point X="3.432340576172" Y="-24.767326171875" />
                  <Point X="3.417290039063" Y="-24.78650390625" />
                  <Point X="3.410854248047" Y="-24.79579296875" />
                  <Point X="3.399130615234" Y="-24.815072265625" />
                  <Point X="3.393842773438" Y="-24.8250625" />
                  <Point X="3.384068603516" Y="-24.84652734375" />
                  <Point X="3.380005371094" Y="-24.857072265625" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.356891113281" Y="-24.95994140625" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.99503515625" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.365360107422" Y="-25.146837890625" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373158691406" Y="-25.183986328125" />
                  <Point X="3.380004150391" Y="-25.205486328125" />
                  <Point X="3.384067871094" Y="-25.216033203125" />
                  <Point X="3.393841796875" Y="-25.237498046875" />
                  <Point X="3.399130859375" Y="-25.247490234375" />
                  <Point X="3.410854980469" Y="-25.26676953125" />
                  <Point X="3.417290039063" Y="-25.276056640625" />
                  <Point X="3.457745361328" Y="-25.327607421875" />
                  <Point X="3.472795898438" Y="-25.34678515625" />
                  <Point X="3.478713867188" Y="-25.35362890625" />
                  <Point X="3.501138427734" Y="-25.375806640625" />
                  <Point X="3.530176269531" Y="-25.3987265625" />
                  <Point X="3.541494384766" Y="-25.406404296875" />
                  <Point X="3.608919921875" Y="-25.445376953125" />
                  <Point X="3.634004394531" Y="-25.459876953125" />
                  <Point X="3.639492675781" Y="-25.462814453125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683026367188" Y="-25.481029296875" />
                  <Point X="3.691992919922" Y="-25.483916015625" />
                  <Point X="4.044509277344" Y="-25.57837109375" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.7756171875" Y="-25.838169921875" />
                  <Point X="4.76161328125" Y="-25.9310546875" />
                  <Point X="4.727801757812" Y="-26.079220703125" />
                  <Point X="4.535668457031" Y="-26.05392578125" />
                  <Point X="3.43678125" Y="-25.90925390625" />
                  <Point X="3.428622558594" Y="-25.90853515625" />
                  <Point X="3.400096679688" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354481445312" Y="-25.912677734375" />
                  <Point X="3.222148925781" Y="-25.94144140625" />
                  <Point X="3.172917236328" Y="-25.952140625" />
                  <Point X="3.157876953125" Y="-25.9567421875" />
                  <Point X="3.128758300781" Y="-25.968365234375" />
                  <Point X="3.114679931641" Y="-25.97538671875" />
                  <Point X="3.086852050781" Y="-25.992279296875" />
                  <Point X="3.074124267578" Y="-26.001529296875" />
                  <Point X="3.050372802734" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.959362548828" Y="-26.129423828125" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606201172" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.863693115234" Y="-26.4212421875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607177734" Y="-26.51458984375" />
                  <Point X="2.864065429688" Y="-26.530126953125" />
                  <Point X="2.871796875" Y="-26.561748046875" />
                  <Point X="2.876786132812" Y="-26.57666796875" />
                  <Point X="2.889158203125" Y="-26.60548046875" />
                  <Point X="2.896541015625" Y="-26.619373046875" />
                  <Point X="2.969775878906" Y="-26.733283203125" />
                  <Point X="2.997021240234" Y="-26.775662109375" />
                  <Point X="3.001742675781" Y="-26.782353515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486083984" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.37993359375" Y="-27.08730078125" />
                  <Point X="4.087170654297" Y="-27.629982421875" />
                  <Point X="4.084962890625" Y="-27.6335546875" />
                  <Point X="4.045496337891" Y="-27.697419921875" />
                  <Point X="4.001273925781" Y="-27.760251953125" />
                  <Point X="3.827161376953" Y="-27.659728515625" />
                  <Point X="2.848454345703" Y="-27.094671875" />
                  <Point X="2.841191650391" Y="-27.09088671875" />
                  <Point X="2.815026123047" Y="-27.079515625" />
                  <Point X="2.78312109375" Y="-27.069328125" />
                  <Point X="2.771107910156" Y="-27.066337890625" />
                  <Point X="2.613611083984" Y="-27.03789453125" />
                  <Point X="2.555017578125" Y="-27.0273125" />
                  <Point X="2.539359130859" Y="-27.02580859375" />
                  <Point X="2.508009277344" Y="-27.025404296875" />
                  <Point X="2.492317871094" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444846191406" Y="-27.03513671875" />
                  <Point X="2.415068847656" Y="-27.0449609375" />
                  <Point X="2.40058984375" Y="-27.051109375" />
                  <Point X="2.269748779297" Y="-27.11996875" />
                  <Point X="2.221071777344" Y="-27.145587890625" />
                  <Point X="2.208970703125" Y="-27.153169921875" />
                  <Point X="2.186039306641" Y="-27.1700625" />
                  <Point X="2.175208984375" Y="-27.179373046875" />
                  <Point X="2.15425" Y="-27.20033203125" />
                  <Point X="2.144938476562" Y="-27.211162109375" />
                  <Point X="2.128045410156" Y="-27.23409375" />
                  <Point X="2.120463867188" Y="-27.2461953125" />
                  <Point X="2.051603027344" Y="-27.377037109375" />
                  <Point X="2.025984863281" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.440193359375" />
                  <Point X="2.010012573242" Y="-27.46996875" />
                  <Point X="2.006337890625" Y="-27.485263671875" />
                  <Point X="2.001379760742" Y="-27.5174375" />
                  <Point X="2.000279418945" Y="-27.53312890625" />
                  <Point X="2.000683227539" Y="-27.56448046875" />
                  <Point X="2.002187744141" Y="-27.580140625" />
                  <Point X="2.030631347656" Y="-27.737638671875" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.279765380859" Y="-28.237689453125" />
                  <Point X="2.735893798828" Y="-29.0277265625" />
                  <Point X="2.723752929688" Y="-29.036083984375" />
                  <Point X="2.580758300781" Y="-28.84973046875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657104492" Y="-27.870150390625" />
                  <Point X="1.808834594727" Y="-27.84962890625" />
                  <Point X="1.783251831055" Y="-27.82800390625" />
                  <Point X="1.773298217773" Y="-27.820646484375" />
                  <Point X="1.617963745117" Y="-27.72078125" />
                  <Point X="1.560174804688" Y="-27.68362890625" />
                  <Point X="1.546280273438" Y="-27.676244140625" />
                  <Point X="1.517467041016" Y="-27.663873046875" />
                  <Point X="1.502548461914" Y="-27.65888671875" />
                  <Point X="1.470928710938" Y="-27.65115625" />
                  <Point X="1.455392089844" Y="-27.648697265625" />
                  <Point X="1.424125488281" Y="-27.64637890625" />
                  <Point X="1.408395507812" Y="-27.64651953125" />
                  <Point X="1.238511108398" Y="-27.66215234375" />
                  <Point X="1.17530847168" Y="-27.667966796875" />
                  <Point X="1.161230834961" Y="-27.670337890625" />
                  <Point X="1.13358190918" Y="-27.67716796875" />
                  <Point X="1.120010498047" Y="-27.681626953125" />
                  <Point X="1.092623657227" Y="-27.692970703125" />
                  <Point X="1.079877929688" Y="-27.699412109375" />
                  <Point X="1.055494995117" Y="-27.7141328125" />
                  <Point X="1.043857910156" Y="-27.722412109375" />
                  <Point X="0.912677490234" Y="-27.831484375" />
                  <Point X="0.863874206543" Y="-27.872064453125" />
                  <Point X="0.852651062012" Y="-27.883091796875" />
                  <Point X="0.832179321289" Y="-27.90684375" />
                  <Point X="0.822930664062" Y="-27.919568359375" />
                  <Point X="0.806039367676" Y="-27.94739453125" />
                  <Point X="0.799017822266" Y="-27.96147265625" />
                  <Point X="0.787394165039" Y="-27.990591796875" />
                  <Point X="0.782792053223" Y="-28.0056328125" />
                  <Point X="0.743569763184" Y="-28.1860859375" />
                  <Point X="0.728977783203" Y="-28.25321875" />
                  <Point X="0.727584899902" Y="-28.2612890625" />
                  <Point X="0.72472479248" Y="-28.28967578125" />
                  <Point X="0.7247421875" Y="-28.32316796875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.785129150391" Y="-28.788033203125" />
                  <Point X="0.833091064453" Y="-29.152337890625" />
                  <Point X="0.655065124512" Y="-28.487935546875" />
                  <Point X="0.652606445312" Y="-28.480123046875" />
                  <Point X="0.642143859863" Y="-28.453576171875" />
                  <Point X="0.626786621094" Y="-28.423814453125" />
                  <Point X="0.620407592773" Y="-28.413208984375" />
                  <Point X="0.501075286865" Y="-28.241275390625" />
                  <Point X="0.4566796875" Y="-28.17730859375" />
                  <Point X="0.446669036865" Y="-28.165171875" />
                  <Point X="0.424786254883" Y="-28.14271875" />
                  <Point X="0.412913818359" Y="-28.13240234375" />
                  <Point X="0.386658691406" Y="-28.113158203125" />
                  <Point X="0.373244873047" Y="-28.104939453125" />
                  <Point X="0.345242523193" Y="-28.090830078125" />
                  <Point X="0.330654144287" Y="-28.084939453125" />
                  <Point X="0.145994216919" Y="-28.02762890625" />
                  <Point X="0.077295059204" Y="-28.006306640625" />
                  <Point X="0.06338053894" Y="-28.003111328125" />
                  <Point X="0.035227794647" Y="-27.998841796875" />
                  <Point X="0.020989574432" Y="-27.997767578125" />
                  <Point X="-0.008651331902" Y="-27.997765625" />
                  <Point X="-0.02289535141" Y="-27.998837890625" />
                  <Point X="-0.051061470032" Y="-28.003107421875" />
                  <Point X="-0.064983421326" Y="-28.0063046875" />
                  <Point X="-0.249643508911" Y="-28.0636171875" />
                  <Point X="-0.318342529297" Y="-28.0849375" />
                  <Point X="-0.33292791748" Y="-28.090828125" />
                  <Point X="-0.360928192139" Y="-28.104935546875" />
                  <Point X="-0.374343322754" Y="-28.11315234375" />
                  <Point X="-0.400599212646" Y="-28.132396484375" />
                  <Point X="-0.412475952148" Y="-28.142716796875" />
                  <Point X="-0.43436038208" Y="-28.165173828125" />
                  <Point X="-0.444367889404" Y="-28.17730859375" />
                  <Point X="-0.563700378418" Y="-28.349244140625" />
                  <Point X="-0.60809564209" Y="-28.413208984375" />
                  <Point X="-0.612468444824" Y="-28.42012890625" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.751372131348" Y="-28.89330859375" />
                  <Point X="-0.985425048828" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948702582889" Y="-21.21522382987" />
                  <Point X="-3.04465484626" Y="-21.325604282253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.642526849961" Y="-22.013377346872" />
                  <Point X="-4.193370597969" Y="-22.647050591896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.598651595772" Y="-20.957340276611" />
                  <Point X="-2.994417720515" Y="-21.412617123163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.558714588593" Y="-22.061766412494" />
                  <Point X="-4.162156027379" Y="-22.755946379282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.355198237583" Y="-20.822083267955" />
                  <Point X="-2.944180594769" Y="-21.499629964073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.474902327226" Y="-22.110155478116" />
                  <Point X="-4.086646476913" Y="-22.813886621219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.172941479603" Y="-20.757224894809" />
                  <Point X="-2.893943469024" Y="-21.586642804983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.391090065858" Y="-22.158544543739" />
                  <Point X="-4.011136926447" Y="-22.871826863156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.113648935782" Y="-20.833820668851" />
                  <Point X="-2.843706205733" Y="-21.673655487665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.307277804491" Y="-22.206933609361" />
                  <Point X="-3.93562737598" Y="-22.929767105093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.593872990944" Y="-23.686992064739" />
                  <Point X="-4.653630904256" Y="-23.755735680295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.04020949638" Y="-20.894142301156" />
                  <Point X="-2.793468930036" Y="-21.760668156075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.221002880611" Y="-22.252489705832" />
                  <Point X="-3.860117825514" Y="-22.98770734703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.480923187256" Y="-23.701862222212" />
                  <Point X="-4.71064182982" Y="-23.966123291167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.953602490675" Y="-20.939316381187" />
                  <Point X="-2.757184028188" Y="-21.863731194567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.112575129185" Y="-22.272561889361" />
                  <Point X="-3.784608275048" Y="-23.045647588968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.367973293405" Y="-23.716732275964" />
                  <Point X="-4.747783298117" Y="-24.153653706131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.363044670715" Y="-20.404761365704" />
                  <Point X="-1.467705160299" Y="-20.525159486406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.854560714385" Y="-20.970185893984" />
                  <Point X="-2.750159672652" Y="-22.000454641115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981976031153" Y="-22.267128856211" />
                  <Point X="-3.709098984975" Y="-23.103588130453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255023399555" Y="-23.731602329717" />
                  <Point X="-4.773606358492" Y="-24.328163782201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.196603980953" Y="-20.358097297762" />
                  <Point X="-1.480179865241" Y="-20.684314036097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.698741305023" Y="-20.935740211459" />
                  <Point X="-3.633589732505" Y="-23.161528715196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.142073505705" Y="-23.74647238347" />
                  <Point X="-4.730159391042" Y="-24.422987806694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.030164553592" Y="-20.311434682047" />
                  <Point X="-3.558080480036" Y="-23.21946929994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.029123611855" Y="-23.761342437222" />
                  <Point X="-4.628063789622" Y="-24.450344295541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.87438441726" Y="-20.277034177976" />
                  <Point X="-3.491505113031" Y="-23.287687144275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.916173718004" Y="-23.776212490975" />
                  <Point X="-4.525968188202" Y="-24.477700784389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.734250790871" Y="-20.260632924626" />
                  <Point X="-3.443216925046" Y="-23.376941981612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803223824154" Y="-23.791082544728" />
                  <Point X="-4.423872586782" Y="-24.505057273236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.594117164482" Y="-20.244231671275" />
                  <Point X="-3.42233593031" Y="-23.497725188194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.671518526138" Y="-23.784376974063" />
                  <Point X="-4.321776985362" Y="-24.532413762083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.453983538093" Y="-20.227830417925" />
                  <Point X="-4.219681383942" Y="-24.55977025093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.354350043829" Y="-20.258019237061" />
                  <Point X="-4.117585782522" Y="-24.587126739778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.324691790021" Y="-20.368705362103" />
                  <Point X="-4.015490181102" Y="-24.614483228625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.295033536213" Y="-20.479391487146" />
                  <Point X="-3.913394579682" Y="-24.641839717472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.265375282404" Y="-20.590077612188" />
                  <Point X="-3.811298796631" Y="-24.669195997376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.631962125633" Y="-25.613261164026" />
                  <Point X="-4.769709509944" Y="-25.771721403114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.235717028596" Y="-20.700763737231" />
                  <Point X="-3.7092030085" Y="-24.696552271437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.467862825226" Y="-25.569290556428" />
                  <Point X="-4.751925316575" Y="-25.896067072151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.196356063286" Y="-20.800288169497" />
                  <Point X="-3.607107220369" Y="-24.723908545498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.303764045978" Y="-25.525320548354" />
                  <Point X="-4.730118703132" Y="-26.015785476216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.433293516966" Y="-20.220763227993" />
                  <Point X="0.351402023919" Y="-20.314968614414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.132000721366" Y="-20.871059860554" />
                  <Point X="-3.505151815369" Y="-24.751426311878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.139665266729" Y="-25.481350540279" />
                  <Point X="-4.701531134941" Y="-26.127703284167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.548665983705" Y="-20.23284643043" />
                  <Point X="0.295312950978" Y="-20.524295755153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.039842157427" Y="-20.909847603381" />
                  <Point X="-3.41793076352" Y="-24.795894012624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.97556648748" Y="-25.437380532205" />
                  <Point X="-4.672943672104" Y="-26.239621213313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.664038470025" Y="-20.244929610342" />
                  <Point X="0.239224587993" Y="-20.73362207918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.098676578217" Y="-20.895304069325" />
                  <Point X="-3.350155708317" Y="-24.862731773558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.811467708232" Y="-25.393410524131" />
                  <Point X="-4.578561334516" Y="-26.275850797189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77941218602" Y="-20.257011375675" />
                  <Point X="-3.305442070326" Y="-24.956098660277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.647368928983" Y="-25.349440516057" />
                  <Point X="-4.436417896559" Y="-26.257137520106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.888211158659" Y="-20.27665651805" />
                  <Point X="-3.303680002868" Y="-25.098875676779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.479914236679" Y="-25.301609971627" />
                  <Point X="-4.294274458601" Y="-26.238424243023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.992251912572" Y="-20.301775364922" />
                  <Point X="-4.152131020644" Y="-26.219710965941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.096292666485" Y="-20.326894211794" />
                  <Point X="-4.009987582687" Y="-26.200997688858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.200333420398" Y="-20.352013058666" />
                  <Point X="-3.86784414473" Y="-26.182284411775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.30437413442" Y="-20.377131951427" />
                  <Point X="-3.725700706772" Y="-26.163571134693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.408414615085" Y="-20.402251112635" />
                  <Point X="-3.583557053339" Y="-26.144857609733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.507739976076" Y="-20.432794398553" />
                  <Point X="-3.441413315514" Y="-26.126143987692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.603442122601" Y="-20.467505715924" />
                  <Point X="-3.299269577689" Y="-26.107430365651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.699144269127" Y="-20.502217033295" />
                  <Point X="-3.170907363747" Y="-26.104570573288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.794845696651" Y="-20.536929177782" />
                  <Point X="-3.076058315703" Y="-26.1402632682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.888451660879" Y="-20.574051877043" />
                  <Point X="-3.003844284372" Y="-26.201994571236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.88603012501" Y="-27.216833291604" />
                  <Point X="-4.141265250426" Y="-27.510447716296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.977945518767" Y="-20.615905013526" />
                  <Point X="-2.956383448745" Y="-26.292201168587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.507993449701" Y="-26.926755886794" />
                  <Point X="-4.090047278002" Y="-27.596332222174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.067439376655" Y="-20.657758150009" />
                  <Point X="-2.961776300514" Y="-26.443208978125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.129956774392" Y="-26.636678481985" />
                  <Point X="-4.038829305578" Y="-27.682216728053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.15693331677" Y="-20.699611191899" />
                  <Point X="-3.985193775996" Y="-27.765320152553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.24642805834" Y="-20.741463311822" />
                  <Point X="-3.926430329175" Y="-27.842524583068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330251589731" Y="-20.789839412765" />
                  <Point X="-3.86766635447" Y="-27.919728406322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.41380977362" Y="-20.838520761092" />
                  <Point X="-3.80890229263" Y="-27.996932129338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.497367957508" Y="-20.887202109419" />
                  <Point X="-3.608385879093" Y="-27.911068425314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580926099283" Y="-20.935883506192" />
                  <Point X="-3.355681751112" Y="-27.765169623347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662064071718" Y="-20.987348989314" />
                  <Point X="-3.102977623132" Y="-27.619270821381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.739852665568" Y="-21.042667491744" />
                  <Point X="-2.850274208201" Y="-27.473372839684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.817641259418" Y="-21.097985994174" />
                  <Point X="-2.622213089752" Y="-27.355822577342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.569281413722" Y="-21.528495357521" />
                  <Point X="-2.498753600235" Y="-27.358602724267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.320341755744" Y="-21.959671718601" />
                  <Point X="-2.412606305947" Y="-27.404305641788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.071402097765" Y="-22.390848079681" />
                  <Point X="-2.348907482862" Y="-27.475832571371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013093826607" Y="-22.602728115939" />
                  <Point X="-2.311964373628" Y="-27.578138428881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02841634005" Y="-22.729905623791" />
                  <Point X="-2.397854811704" Y="-27.821748118563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.073207082852" Y="-22.823183811573" />
                  <Point X="-2.646793877252" Y="-28.25292379813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.131552041221" Y="-22.900869657982" />
                  <Point X="-2.895733044965" Y="-28.684099595224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.206765956762" Y="-22.959149988997" />
                  <Point X="-2.931243826061" Y="-28.869754119149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293042150032" Y="-23.004704625201" />
                  <Point X="-2.855838333828" Y="-28.92781406639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.403154280339" Y="-23.022839152482" />
                  <Point X="-2.780432841594" Y="-28.985874013631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544431774365" Y="-23.00512202994" />
                  <Point X="-2.70139821293" Y="-29.039759116976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.762418642683" Y="-22.899160866674" />
                  <Point X="-2.619567020451" Y="-29.09042714166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015122045092" Y="-22.753262899383" />
                  <Point X="-2.537735815457" Y="-29.141095151948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.267825807394" Y="-22.607364518082" />
                  <Point X="-2.175733050332" Y="-28.869462650857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.520529610223" Y="-22.461466090159" />
                  <Point X="-1.969362948844" Y="-28.776865049148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.773233413053" Y="-22.315567662236" />
                  <Point X="-1.848223241914" Y="-28.782313800673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.959700449599" Y="-22.245865917643" />
                  <Point X="-1.729132183217" Y="-28.790119252403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.016706685324" Y="-22.325091788287" />
                  <Point X="3.276181197094" Y="-23.176968914689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.029035358949" Y="-23.461277678867" />
                  <Point X="-1.612342656083" Y="-28.80057231333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.070923719469" Y="-22.40752626831" />
                  <Point X="3.65422019524" Y="-22.886888837761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003054606431" Y="-23.635969158997" />
                  <Point X="-1.519193793506" Y="-28.83822084789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.122586399394" Y="-22.492899196729" />
                  <Point X="4.032258722797" Y="-22.596809302185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.028077873144" Y="-23.751987226762" />
                  <Point X="-1.445367634134" Y="-28.898097609759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.074572178868" Y="-23.843305689578" />
                  <Point X="-1.373942171766" Y="-28.960736057616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.132103981365" Y="-23.921926964812" />
                  <Point X="-1.302516635836" Y="-29.023374420851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.208741844932" Y="-23.978569231005" />
                  <Point X="-0.406836687354" Y="-28.137816548374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.457047384539" Y="-28.195577348119" />
                  <Point X="-1.234560025586" Y="-29.090003326595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.296491117223" Y="-24.022429283642" />
                  <Point X="-0.204199105224" Y="-28.049512719012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.668347160721" Y="-28.583453978329" />
                  <Point X="-1.18749716395" Y="-29.180667740652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.404655931398" Y="-24.042803941878" />
                  <Point X="-0.035998517619" Y="-28.000824120192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.724435867831" Y="-28.792780698227" />
                  <Point X="-1.163159860967" Y="-29.297474919421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.530081567178" Y="-24.043322296259" />
                  <Point X="0.083450960603" Y="-28.008217257424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.780524801109" Y="-29.002107678302" />
                  <Point X="-1.13972017355" Y="-29.415314686778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.672225118117" Y="-24.024608889205" />
                  <Point X="0.182581928013" Y="-28.038984167576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.836613943362" Y="-29.211434898775" />
                  <Point X="-1.119735913472" Y="-29.53712946858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.814368669056" Y="-24.005895482152" />
                  <Point X="0.281713508757" Y="-28.069750372166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.892703085615" Y="-29.420762119248" />
                  <Point X="-1.137421763755" Y="-29.702278755238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956512376573" Y="-23.987181894976" />
                  <Point X="0.375695233797" Y="-28.106440808062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.948792227868" Y="-29.630089339721" />
                  <Point X="-1.055767365799" Y="-29.753150158755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.098656238775" Y="-23.968468129857" />
                  <Point X="0.448542682975" Y="-28.167443447218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.240800100976" Y="-23.949754364737" />
                  <Point X="3.694756664964" Y="-24.577905482495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.355261649783" Y="-24.968449822369" />
                  <Point X="0.505143052325" Y="-28.247136213717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.382943963177" Y="-23.931040599617" />
                  <Point X="3.858855636002" Y="-24.533935253793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358266298933" Y="-25.109797412149" />
                  <Point X="1.12364785355" Y="-27.680431873911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.763400509636" Y="-28.094849037135" />
                  <Point X="0.561026809896" Y="-28.327653347768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.525087825379" Y="-23.912326834497" />
                  <Point X="4.02295460704" Y="-24.48996502509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386723461798" Y="-25.221865234267" />
                  <Point X="1.267753390313" Y="-27.65946146035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725355171551" Y="-28.283419235347" />
                  <Point X="0.616910567467" Y="-28.408170481819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.66723168758" Y="-23.893613069377" />
                  <Point X="4.187053578078" Y="-24.445994796388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.440159402821" Y="-25.305198259141" />
                  <Point X="1.404574197687" Y="-27.646871169334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.736128060171" Y="-28.41583048786" />
                  <Point X="0.659396413631" Y="-28.504100149874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.724983067778" Y="-23.971981749362" />
                  <Point X="4.351152549116" Y="-24.402024567685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.503225017681" Y="-25.377453611461" />
                  <Point X="1.516075293502" Y="-27.663407874574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.752684334573" Y="-28.541588716084" />
                  <Point X="0.689054653758" Y="-28.614786290656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752523123082" Y="-24.085104583044" />
                  <Point X="4.515251520154" Y="-24.358054338982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583053266423" Y="-25.430425759342" />
                  <Point X="3.103344156982" Y="-25.982267963499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.89856245963" Y="-26.21784235851" />
                  <Point X="1.601357956724" Y="-27.710105436357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769240608975" Y="-28.667346944308" />
                  <Point X="0.718712893884" Y="-28.725472431438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.771692681594" Y="-24.207856571788" />
                  <Point X="4.679351022813" Y="-24.31408349872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.669409380723" Y="-25.475888456917" />
                  <Point X="3.274625728835" Y="-25.930035097737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.865595311378" Y="-26.400570767572" />
                  <Point X="2.214241795833" Y="-27.149867273787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.105056645387" Y="-27.275470421397" />
                  <Point X="1.68210649416" Y="-27.762018913199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.785796890776" Y="-28.79310516402" />
                  <Point X="0.74837113401" Y="-28.836158572221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.770111814951" Y="-25.504847601288" />
                  <Point X="3.41933093972" Y="-25.908374838011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86731100351" Y="-26.543401132784" />
                  <Point X="2.437843397403" Y="-27.037447098773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000376867436" Y="-27.540694774065" />
                  <Point X="1.762854963144" Y="-27.813932468784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.80235334865" Y="-28.918863181183" />
                  <Point X="0.778029374136" Y="-28.946844713003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.872207739969" Y="-25.532203717879" />
                  <Point X="3.533387033525" Y="-25.921972354284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.909552746822" Y="-26.639611609048" />
                  <Point X="2.570153290488" Y="-27.030046021043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017630569175" Y="-27.665650703913" />
                  <Point X="1.834146039371" Y="-27.876725510213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818909806524" Y="-29.044621198347" />
                  <Point X="0.807687614262" Y="-29.057530853785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974303664986" Y="-25.55955983447" />
                  <Point X="3.646336813031" Y="-25.936842539575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.963069362632" Y="-26.722851828197" />
                  <Point X="2.678949446093" Y="-27.049694404045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.039283328755" Y="-27.785546096601" />
                  <Point X="1.893163131778" Y="-27.953638154859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.076399502882" Y="-25.586916051282" />
                  <Point X="3.759286592537" Y="-25.951712724866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.018930792886" Y="-26.803394646887" />
                  <Point X="2.78675084068" Y="-27.070487128695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.077163885667" Y="-27.886773543918" />
                  <Point X="1.952180224185" Y="-28.030550799506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.178495148982" Y="-25.614272488731" />
                  <Point X="3.872236372043" Y="-25.966582910156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.090842724251" Y="-26.86547347618" />
                  <Point X="2.877184450062" Y="-27.111259204748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.127400883005" Y="-27.973786532544" />
                  <Point X="2.011197316592" Y="-28.107463444152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.280590795082" Y="-25.64162892618" />
                  <Point X="3.985186151549" Y="-25.981453095447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.166352121095" Y="-26.923413894839" />
                  <Point X="2.960996737397" Y="-27.159648240498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.177637880343" Y="-28.060799521169" />
                  <Point X="2.070214408999" Y="-28.184376088798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.382686441182" Y="-25.668985363629" />
                  <Point X="4.098135931056" Y="-25.996323280737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.241861517939" Y="-26.981354313499" />
                  <Point X="3.044809024731" Y="-27.208037276249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.227874877681" Y="-28.147812509795" />
                  <Point X="2.129231501406" Y="-28.261288733444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.484782087282" Y="-25.696341801078" />
                  <Point X="4.211085710562" Y="-26.011193466028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.317370914783" Y="-27.039294732158" />
                  <Point X="3.128621312066" Y="-27.256426311999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.278111875019" Y="-28.234825498421" />
                  <Point X="2.188248593812" Y="-28.33820137809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.586877733382" Y="-25.723698238527" />
                  <Point X="4.324035490068" Y="-26.026063651318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392880317697" Y="-27.097235143834" />
                  <Point X="3.2124335994" Y="-27.30481534775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.328348875803" Y="-28.321838483083" />
                  <Point X="2.247265686219" Y="-28.415114022736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.688973379481" Y="-25.751054675976" />
                  <Point X="4.436985269574" Y="-26.040933836609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.468389749946" Y="-27.155175521765" />
                  <Point X="3.296245886735" Y="-27.3532043835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.378585876703" Y="-28.40885146761" />
                  <Point X="2.306282778626" Y="-28.492026667383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783275043445" Y="-25.787377064241" />
                  <Point X="4.549935050072" Y="-26.055804020758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.543899182194" Y="-27.213115899697" />
                  <Point X="3.380058174069" Y="-27.40159341925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.428822877604" Y="-28.495864452138" />
                  <Point X="2.365299871033" Y="-28.568939312029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.753554041897" Y="-25.966371208691" />
                  <Point X="4.662884837433" Y="-26.070674197013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.619408614442" Y="-27.271056277628" />
                  <Point X="3.463870461404" Y="-27.449982455001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479059878505" Y="-28.582877436665" />
                  <Point X="2.42431696344" Y="-28.645851956675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.69491804669" Y="-27.32899665556" />
                  <Point X="3.547682748739" Y="-27.498371490751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.529296879405" Y="-28.669890421193" />
                  <Point X="2.483334055847" Y="-28.722764601321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.770427478939" Y="-27.386937033491" />
                  <Point X="3.631495036073" Y="-27.546760526502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.579533880306" Y="-28.75690340572" />
                  <Point X="2.542351148254" Y="-28.799677245967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.845936911187" Y="-27.444877411422" />
                  <Point X="3.715307323408" Y="-27.595149562252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.629770881207" Y="-28.843916390247" />
                  <Point X="2.601368292844" Y="-28.876589830583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.921446343435" Y="-27.502817789354" />
                  <Point X="3.799119610742" Y="-27.643538598003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.680007882107" Y="-28.930929374775" />
                  <Point X="2.660385534681" Y="-28.95350230333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.996955775684" Y="-27.560758167285" />
                  <Point X="3.882931984617" Y="-27.6919275342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730244883008" Y="-29.017942359302" />
                  <Point X="2.719402776517" Y="-29.030414776077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.072465207932" Y="-27.618698545217" />
                  <Point X="3.966744402005" Y="-27.740316420341" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998375" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.763501281738" Y="-29.62673046875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464319122314" Y="-28.521544921875" />
                  <Point X="0.344986785889" Y="-28.349611328125" />
                  <Point X="0.300591339111" Y="-28.28564453125" />
                  <Point X="0.274336212158" Y="-28.266400390625" />
                  <Point X="0.089676300049" Y="-28.20908984375" />
                  <Point X="0.020977081299" Y="-28.187767578125" />
                  <Point X="-0.008663844109" Y="-28.187765625" />
                  <Point X="-0.193323745728" Y="-28.245078125" />
                  <Point X="-0.262022979736" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.407611358643" Y="-28.457578125" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.567846069336" Y="-28.942484375" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-1.020444702148" Y="-29.9535546875" />
                  <Point X="-1.100255615234" Y="-29.9380625" />
                  <Point X="-1.314304199219" Y="-29.8829921875" />
                  <Point X="-1.35158984375" Y="-29.8733984375" />
                  <Point X="-1.344295654297" Y="-29.817994140625" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.353798461914" Y="-29.3129765625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.556294067383" Y="-29.053533203125" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.874883789062" Y="-28.970974609375" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.177896484375" Y="-29.099419921875" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.320715332031" Y="-29.236767578125" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.738864501953" Y="-29.24003515625" />
                  <Point X="-2.855839599609" Y="-29.167607421875" />
                  <Point X="-3.152242431641" Y="-28.93938671875" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-3.048612548828" Y="-28.56889453125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513982421875" Y="-27.56876171875" />
                  <Point X="-2.531324707031" Y="-27.551419921875" />
                  <Point X="-2.560154785156" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.941308837891" Y="-27.745326171875" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.069291259766" Y="-27.968541015625" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.374205078125" Y="-27.490796875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-4.113124023438" Y="-27.15159765625" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145821777344" Y="-26.396013671875" />
                  <Point X="-3.140206298828" Y="-26.37433203125" />
                  <Point X="-3.1381171875" Y="-26.366265625" />
                  <Point X="-3.140326171875" Y="-26.334595703125" />
                  <Point X="-3.161158203125" Y="-26.310640625" />
                  <Point X="-3.180459960938" Y="-26.29928125" />
                  <Point X="-3.187640869141" Y="-26.2950546875" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-3.660897705078" Y="-26.3466796875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.891249511719" Y="-26.15269140625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.983615722656" Y="-25.61808984375" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.638901855469" Y="-25.418416015625" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541897216797" Y="-25.12142578125" />
                  <Point X="-3.521669677734" Y="-25.10738671875" />
                  <Point X="-3.514144287109" Y="-25.1021640625" />
                  <Point X="-3.494898681641" Y="-25.075908203125" />
                  <Point X="-3.488156005859" Y="-25.05418359375" />
                  <Point X="-3.485647705078" Y="-25.0461015625" />
                  <Point X="-3.485647216797" Y="-25.016462890625" />
                  <Point X="-3.492389648438" Y="-24.99473828125" />
                  <Point X="-3.494898193359" Y="-24.986654296875" />
                  <Point X="-3.514144287109" Y="-24.960396484375" />
                  <Point X="-3.534371826172" Y="-24.946357421875" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.959792236328" Y="-24.826111328125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.940938476562" Y="-24.16099609375" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.804470703125" Y="-23.585935546875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.528467285156" Y="-23.503962890625" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731703857422" Y="-23.6041328125" />
                  <Point X="-3.686933837891" Y="-23.590017578125" />
                  <Point X="-3.670277832031" Y="-23.584765625" />
                  <Point X="-3.651533935547" Y="-23.573943359375" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.621155517578" Y="-23.512845703125" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.637991943359" Y="-23.412849609375" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.890744873047" Y="-23.203697265625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.250523925781" Y="-22.36805859375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.860219726562" Y="-21.827650390625" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.635263916016" Y="-21.79817578125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514404297" Y="-22.07956640625" />
                  <Point X="-3.076162109375" Y="-22.085021484375" />
                  <Point X="-3.052965087891" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.968994628906" Y="-22.028337890625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.943529296875" Y="-21.909806640625" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.054331542969" Y="-21.688841796875" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-2.910513183594" Y="-20.94652734375" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.280708251953" Y="-20.563341796875" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.113577636719" Y="-20.52248046875" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951248413086" Y="-20.726337890625" />
                  <Point X="-1.881850463867" Y="-20.76246484375" />
                  <Point X="-1.856032348633" Y="-20.77590625" />
                  <Point X="-1.835125244141" Y="-20.781509765625" />
                  <Point X="-1.813809082031" Y="-20.77775" />
                  <Point X="-1.741526489258" Y="-20.74780859375" />
                  <Point X="-1.714635009766" Y="-20.736669921875" />
                  <Point X="-1.696905395508" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.662556884766" Y="-20.63089453125" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.663543945312" Y="-20.493263671875" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.172165405273" Y="-20.153919921875" />
                  <Point X="-0.968083068848" Y="-20.096703125" />
                  <Point X="-0.39568270874" Y="-20.0297109375" />
                  <Point X="-0.224200149536" Y="-20.009640625" />
                  <Point X="-0.182985931396" Y="-20.163455078125" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282110214" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594028473" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.106851142883" Y="-20.493541015625" />
                  <Point X="0.236648376465" Y="-20.0091328125" />
                  <Point X="0.682020080566" Y="-20.055775390625" />
                  <Point X="0.860209960938" Y="-20.074435546875" />
                  <Point X="1.333776245117" Y="-20.18876953125" />
                  <Point X="1.508454956055" Y="-20.230943359375" />
                  <Point X="1.817276000977" Y="-20.342953125" />
                  <Point X="1.93104699707" Y="-20.384220703125" />
                  <Point X="2.229094970703" Y="-20.523607421875" />
                  <Point X="2.33868359375" Y="-20.574857421875" />
                  <Point X="2.62663671875" Y="-20.742619140625" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="3.004088134766" Y="-20.997431640625" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.857847900391" Y="-21.40868359375" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.209203857422" Y="-22.56700390625" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.208146240234" Y="-22.658275390625" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.249991943359" Y="-22.745330078125" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.274939208984" Y="-22.775794921875" />
                  <Point X="2.321081542969" Y="-22.80710546875" />
                  <Point X="2.338247802734" Y="-22.81875390625" />
                  <Point X="2.360338867188" Y="-22.827021484375" />
                  <Point X="2.410933349609" Y="-22.83312109375" />
                  <Point X="2.410933105469" Y="-22.83312109375" />
                  <Point X="2.448665771484" Y="-22.8340546875" />
                  <Point X="2.507182128906" Y="-22.81840625" />
                  <Point X="2.528952148438" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.945698974609" Y="-22.57394921875" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.139783203125" Y="-22.170830078125" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.353984863281" Y="-22.508298828125" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.1154296875" Y="-22.77248046875" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371582031" Y="-23.41616796875" />
                  <Point X="3.237257080078" Y="-23.471109375" />
                  <Point X="3.221589355469" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.197431640625" Y="-23.564595703125" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.203657470703" Y="-23.671447265625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.250673095703" Y="-23.765283203125" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280946289062" Y="-23.8011796875" />
                  <Point X="3.331704589844" Y="-23.829751953125" />
                  <Point X="3.350588378906" Y="-23.8403828125" />
                  <Point X="3.368568115234" Y="-23.846380859375" />
                  <Point X="3.437196777344" Y="-23.855451171875" />
                  <Point X="3.462728759766" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.860208251953" Y="-23.808220703125" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.912212402344" Y="-23.937806640625" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.986896484375" Y="-24.355037109375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.688713867188" Y="-24.508279296875" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729088623047" Y="-24.767181640625" />
                  <Point X="3.661663085938" Y="-24.806154296875" />
                  <Point X="3.636578613281" Y="-24.82065234375" />
                  <Point X="3.622265136719" Y="-24.833072265625" />
                  <Point X="3.581809814453" Y="-24.884623046875" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.5435" Y="-24.9956796875" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.551968505859" Y="-25.111099609375" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759277344" Y="-25.158759765625" />
                  <Point X="3.607214599609" Y="-25.210310546875" />
                  <Point X="3.622265136719" Y="-25.22948828125" />
                  <Point X="3.636575927734" Y="-25.241908203125" />
                  <Point X="3.704001464844" Y="-25.280880859375" />
                  <Point X="3.7290859375" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.093684082031" Y="-25.39484375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.963494140625" Y="-25.866494140625" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.887309570312" Y="-26.234244140625" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.510869140625" Y="-26.24230078125" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394837402344" Y="-26.098341796875" />
                  <Point X="3.262504882813" Y="-26.12710546875" />
                  <Point X="3.213273193359" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.105458740234" Y="-26.250896484375" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.052893798828" Y="-26.43865234375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.129595214844" Y="-26.630533203125" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.495598144531" Y="-26.9365625" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.24658984375" Y="-27.7334375" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.077727539062" Y="-27.981744140625" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="3.732161621094" Y="-27.8242734375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.579843994141" Y="-27.22487109375" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077148438" Y="-27.21924609375" />
                  <Point X="2.358236083984" Y="-27.28810546875" />
                  <Point X="2.309559082031" Y="-27.313724609375" />
                  <Point X="2.288600097656" Y="-27.33468359375" />
                  <Point X="2.219739257812" Y="-27.465525390625" />
                  <Point X="2.19412109375" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.217606689453" Y="-27.703873046875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.444310302734" Y="-28.142689453125" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.885680419922" Y="-29.154224609375" />
                  <Point X="2.835295654297" Y="-29.19021484375" />
                  <Point X="2.693969482422" Y="-29.28169140625" />
                  <Point X="2.679774902344" Y="-29.29087890625" />
                  <Point X="2.430021240234" Y="-28.96539453125" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670548950195" Y="-27.980466796875" />
                  <Point X="1.515214599609" Y="-27.8806015625" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425805908203" Y="-27.83571875" />
                  <Point X="1.255921386719" Y="-27.8513515625" />
                  <Point X="1.192718994141" Y="-27.857166015625" />
                  <Point X="1.16533203125" Y="-27.868509765625" />
                  <Point X="1.034151611328" Y="-27.97758203125" />
                  <Point X="0.985348327637" Y="-28.018162109375" />
                  <Point X="0.96845690918" Y="-28.04598828125" />
                  <Point X="0.929234619141" Y="-28.22644140625" />
                  <Point X="0.914642700195" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.97350378418" Y="-28.763232421875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.042009521484" Y="-29.95280078125" />
                  <Point X="0.994369445801" Y="-29.9632421875" />
                  <Point X="0.863788085938" Y="-29.98696484375" />
                  <Point X="0.860200500488" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#190" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.140845616077" Y="4.88076749562" Z="1.85" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.85" />
                  <Point X="-0.407650953605" Y="5.05122989569" Z="1.85" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.85" />
                  <Point X="-1.191819235171" Y="4.925505566177" Z="1.85" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.85" />
                  <Point X="-1.71927216427" Y="4.531490947242" Z="1.85" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.85" />
                  <Point X="-1.716398526572" Y="4.415420822026" Z="1.85" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.85" />
                  <Point X="-1.766816500346" Y="4.329665242761" Z="1.85" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.85" />
                  <Point X="-1.864917246349" Y="4.313164777058" Z="1.85" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.85" />
                  <Point X="-2.080065939453" Y="4.539237468223" Z="1.85" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.85" />
                  <Point X="-2.31114725499" Y="4.511645167607" Z="1.85" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.85" />
                  <Point X="-2.947091360621" Y="4.12443247928" Z="1.85" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.85" />
                  <Point X="-3.10378877057" Y="3.317439488696" Z="1.85" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.85" />
                  <Point X="-2.999495190897" Y="3.117115900401" Z="1.85" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.85" />
                  <Point X="-3.010505415587" Y="3.038298152299" Z="1.85" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.85" />
                  <Point X="-3.077960565206" Y="2.996069508276" Z="1.85" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.85" />
                  <Point X="-3.616419859601" Y="3.276405178175" Z="1.85" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.85" />
                  <Point X="-3.905838984517" Y="3.234333009581" Z="1.85" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.85" />
                  <Point X="-4.299861881057" Y="2.688427312386" Z="1.85" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.85" />
                  <Point X="-3.927338854868" Y="1.7879151881" Z="1.85" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.85" />
                  <Point X="-3.68849822299" Y="1.595343190706" Z="1.85" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.85" />
                  <Point X="-3.673505683189" Y="1.537569587837" Z="1.85" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.85" />
                  <Point X="-3.708125828762" Y="1.488948523886" Z="1.85" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.85" />
                  <Point X="-4.528097045836" Y="1.576889710165" Z="1.85" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.85" />
                  <Point X="-4.858886649521" Y="1.458423276055" Z="1.85" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.85" />
                  <Point X="-4.996555697973" Y="0.877603758203" Z="1.85" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.85" />
                  <Point X="-3.978889213866" Y="0.156872624547" Z="1.85" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.85" />
                  <Point X="-3.569035445265" Y="0.043846050766" Z="1.85" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.85" />
                  <Point X="-3.546299386176" Y="0.021724690992" Z="1.85" />
                  <Point X="-3.539556741714" Y="0" Z="1.85" />
                  <Point X="-3.542065214959" Y="-0.008082260069" Z="1.85" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.85" />
                  <Point X="-3.556333149858" Y="-0.035029932368" Z="1.85" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.85" />
                  <Point X="-4.657998574907" Y="-0.338839432209" Z="1.85" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.85" />
                  <Point X="-5.039267824775" Y="-0.593886927682" Z="1.85" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.85" />
                  <Point X="-4.945850354392" Y="-1.133785952976" Z="1.85" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.85" />
                  <Point X="-3.660527541549" Y="-1.364970582421" Z="1.85" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.85" />
                  <Point X="-3.211978042183" Y="-1.311089641238" Z="1.85" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.85" />
                  <Point X="-3.19476540718" Y="-1.330515906967" Z="1.85" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.85" />
                  <Point X="-4.14971775615" Y="-2.080649004995" Z="1.85" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.85" />
                  <Point X="-4.423304640741" Y="-2.485125996116" Z="1.85" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.85" />
                  <Point X="-4.115233149693" Y="-2.967543663381" Z="1.85" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.85" />
                  <Point X="-2.922464696519" Y="-2.757347169988" Z="1.85" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.85" />
                  <Point X="-2.568135383896" Y="-2.560195085235" Z="1.85" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.85" />
                  <Point X="-3.09806984318" Y="-3.512613685973" Z="1.85" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.85" />
                  <Point X="-3.188902084744" Y="-3.94772363362" Z="1.85" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.85" />
                  <Point X="-2.771342103613" Y="-4.251266603205" Z="1.85" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.85" />
                  <Point X="-2.28720347977" Y="-4.235924402299" Z="1.85" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.85" />
                  <Point X="-2.156273808638" Y="-4.109714038081" Z="1.85" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.85" />
                  <Point X="-1.884309805758" Y="-3.98958649778" Z="1.85" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.85" />
                  <Point X="-1.595417340278" Y="-4.059844340728" Z="1.85" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.85" />
                  <Point X="-1.408993748942" Y="-4.291450282129" Z="1.85" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.85" />
                  <Point X="-1.400023887479" Y="-4.780187569521" Z="1.85" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.85" />
                  <Point X="-1.332919706917" Y="-4.900132698248" Z="1.85" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.85" />
                  <Point X="-1.036127971058" Y="-4.971359116382" Z="1.85" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.85" />
                  <Point X="-0.525705814637" Y="-3.924144863561" Z="1.85" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.85" />
                  <Point X="-0.372691435677" Y="-3.45480785161" Z="1.85" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.85" />
                  <Point X="-0.184659870035" Y="-3.261550977512" Z="1.85" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.85" />
                  <Point X="0.068699209326" Y="-3.225561067776" Z="1.85" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.85" />
                  <Point X="0.297754423755" Y="-3.346837904158" Z="1.85" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.85" />
                  <Point X="0.709049045764" Y="-4.608391166255" Z="1.85" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.85" />
                  <Point X="0.866568471871" Y="-5.004879534438" Z="1.85" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.85" />
                  <Point X="1.046558250586" Y="-4.970359663257" Z="1.85" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.85" />
                  <Point X="1.016920164634" Y="-3.725425989298" Z="1.85" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.85" />
                  <Point X="0.971937702904" Y="-3.205779420157" Z="1.85" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.85" />
                  <Point X="1.059963241597" Y="-2.984747731877" Z="1.85" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.85" />
                  <Point X="1.25434603461" Y="-2.869859508221" Z="1.85" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.85" />
                  <Point X="1.48201963014" Y="-2.891379688441" Z="1.85" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.85" />
                  <Point X="2.3841976057" Y="-3.964550741783" Z="1.85" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.85" />
                  <Point X="2.714983136558" Y="-4.292385865396" Z="1.85" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.85" />
                  <Point X="2.908586408766" Y="-4.163632444467" Z="1.85" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.85" />
                  <Point X="2.481455643094" Y="-3.086408019527" Z="1.85" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.85" />
                  <Point X="2.260654917455" Y="-2.663704935811" Z="1.85" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.85" />
                  <Point X="2.257829450342" Y="-2.45753124579" Z="1.85" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.85" />
                  <Point X="2.375367132582" Y="-2.301071859625" Z="1.85" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.85" />
                  <Point X="2.564801867702" Y="-2.242793092036" Z="1.85" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.85" />
                  <Point X="3.70100557758" Y="-2.836293857181" Z="1.85" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.85" />
                  <Point X="4.112460354889" Y="-2.979241340339" Z="1.85" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.85" />
                  <Point X="4.282967523818" Y="-2.728442348251" Z="1.85" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.85" />
                  <Point X="3.51988026652" Y="-1.865614450636" Z="1.85" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.85" />
                  <Point X="3.165497231376" Y="-1.572214299423" Z="1.85" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.85" />
                  <Point X="3.096527466974" Y="-1.411954152346" Z="1.85" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.85" />
                  <Point X="3.137748949754" Y="-1.251583205888" Z="1.85" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.85" />
                  <Point X="3.266967338664" Y="-1.144683445421" Z="1.85" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.85" />
                  <Point X="4.498186694127" Y="-1.260591609819" Z="1.85" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.85" />
                  <Point X="4.929900680238" Y="-1.214089418129" Z="1.85" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.85" />
                  <Point X="5.006779368694" Y="-0.842669304092" Z="1.85" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.85" />
                  <Point X="4.100469216597" Y="-0.315267148572" Z="1.85" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.85" />
                  <Point X="3.722868290457" Y="-0.206311398731" Z="1.85" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.85" />
                  <Point X="3.640392069169" Y="-0.148160148139" Z="1.85" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.85" />
                  <Point X="3.594919841868" Y="-0.07041424442" Z="1.85" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.85" />
                  <Point X="3.586451608556" Y="0.026196286773" Z="1.85" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.85" />
                  <Point X="3.614987369232" Y="0.115788590436" Z="1.85" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.85" />
                  <Point X="3.680527123895" Y="0.181837445511" Z="1.85" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.85" />
                  <Point X="4.695498600709" Y="0.474704784197" Z="1.85" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.85" />
                  <Point X="5.030145494343" Y="0.683934969314" Z="1.85" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.85" />
                  <Point X="4.954637596858" Y="1.10530080206" Z="1.85" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.85" />
                  <Point X="3.84752602934" Y="1.272631926384" Z="1.85" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.85" />
                  <Point X="3.437589381952" Y="1.225398452228" Z="1.85" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.85" />
                  <Point X="3.350076926516" Y="1.24509846255" Z="1.85" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.85" />
                  <Point X="3.28628757591" Y="1.2934775419" Z="1.85" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.85" />
                  <Point X="3.246469994276" Y="1.369936040454" Z="1.85" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.85" />
                  <Point X="3.239428329133" Y="1.453218400399" Z="1.85" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.85" />
                  <Point X="3.27078382623" Y="1.52975363498" Z="1.85" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.85" />
                  <Point X="4.139711591082" Y="2.219131339389" Z="1.85" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.85" />
                  <Point X="4.390606108593" Y="2.548868214391" Z="1.85" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.85" />
                  <Point X="4.174212670996" Y="2.889652995241" Z="1.85" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.85" />
                  <Point X="2.914542907773" Y="2.500632194061" Z="1.85" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.85" />
                  <Point X="2.488108015953" Y="2.261177211859" Z="1.85" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.85" />
                  <Point X="2.410766857639" Y="2.247799096782" Z="1.85" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.85" />
                  <Point X="2.343000406177" Y="2.26554861784" Z="1.85" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.85" />
                  <Point X="2.285209982395" Y="2.314024454205" Z="1.85" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.85" />
                  <Point X="2.251630605977" Y="2.378991586193" Z="1.85" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.85" />
                  <Point X="2.251350730798" Y="2.451361559929" Z="1.85" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.85" />
                  <Point X="2.894993158799" Y="3.597596174182" Z="1.85" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.85" />
                  <Point X="3.026909100427" Y="4.074597017319" Z="1.85" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.85" />
                  <Point X="2.645650556547" Y="4.33186407796" Z="1.85" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.85" />
                  <Point X="2.244120274498" Y="4.552964840587" Z="1.85" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.85" />
                  <Point X="1.828169053761" Y="4.735330516528" Z="1.85" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.85" />
                  <Point X="1.339354656043" Y="4.891114719056" Z="1.85" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.85" />
                  <Point X="0.681071780049" Y="5.025234493856" Z="1.85" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.85" />
                  <Point X="0.052398945476" Y="4.550680010331" Z="1.85" />
                  <Point X="0" Y="4.355124473572" Z="1.85" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>