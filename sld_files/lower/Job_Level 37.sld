<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#189" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2682" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.84737890625" Y="-29.572712890625" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.54236315918" Y="-28.467375" />
                  <Point X="0.426253845215" Y="-28.300083984375" />
                  <Point X="0.378635528564" Y="-28.231474609375" />
                  <Point X="0.35675189209" Y="-28.20901953125" />
                  <Point X="0.330496246338" Y="-28.189775390625" />
                  <Point X="0.302495025635" Y="-28.17566796875" />
                  <Point X="0.122822387695" Y="-28.119904296875" />
                  <Point X="0.049135883331" Y="-28.09703515625" />
                  <Point X="0.020976488113" Y="-28.092765625" />
                  <Point X="-0.008664384842" Y="-28.092765625" />
                  <Point X="-0.036823783875" Y="-28.09703515625" />
                  <Point X="-0.216496414185" Y="-28.152798828125" />
                  <Point X="-0.290182922363" Y="-28.17566796875" />
                  <Point X="-0.318184753418" Y="-28.18977734375" />
                  <Point X="-0.344439941406" Y="-28.209021484375" />
                  <Point X="-0.366323425293" Y="-28.2314765625" />
                  <Point X="-0.482432769775" Y="-28.39876953125" />
                  <Point X="-0.530051086426" Y="-28.467376953125" />
                  <Point X="-0.538188293457" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.667494384766" Y="-28.94732421875" />
                  <Point X="-0.916584838867" Y="-29.87694140625" />
                  <Point X="-0.996442626953" Y="-29.86144140625" />
                  <Point X="-1.079320678711" Y="-29.845353515625" />
                  <Point X="-1.24641784668" Y="-29.802361328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.259432373047" Y="-29.30043359375" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.2879375" Y="-29.18296875" />
                  <Point X="-1.304009277344" Y="-29.155130859375" />
                  <Point X="-1.32364453125" Y="-29.131203125" />
                  <Point X="-1.489064208984" Y="-28.986134765625" />
                  <Point X="-1.556905517578" Y="-28.926638671875" />
                  <Point X="-1.583189697266" Y="-28.910294921875" />
                  <Point X="-1.612887939453" Y="-28.897994140625" />
                  <Point X="-1.643027709961" Y="-28.890966796875" />
                  <Point X="-1.862576416016" Y="-28.876576171875" />
                  <Point X="-1.952616821289" Y="-28.87067578125" />
                  <Point X="-1.983415771484" Y="-28.873708984375" />
                  <Point X="-2.014463745117" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.225597167969" Y="-29.017037109375" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312789306641" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.400510986328" Y="-29.184705078125" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.680204833984" Y="-29.164619140625" />
                  <Point X="-2.8017265625" Y="-29.089376953125" />
                  <Point X="-3.086224853516" Y="-28.870322265625" />
                  <Point X="-3.104722412109" Y="-28.856080078125" />
                  <Point X="-2.951685791016" Y="-28.59101171875" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405576171875" Y="-27.5851875" />
                  <Point X="-2.414562255859" Y="-27.5555703125" />
                  <Point X="-2.428781494141" Y="-27.526740234375" />
                  <Point X="-2.446808837891" Y="-27.501583984375" />
                  <Point X="-2.464157470703" Y="-27.484236328125" />
                  <Point X="-2.489317626953" Y="-27.466208984375" />
                  <Point X="-2.518145996094" Y="-27.451994140625" />
                  <Point X="-2.547762695312" Y="-27.44301171875" />
                  <Point X="-2.5786953125" Y="-27.444025390625" />
                  <Point X="-2.610219726562" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.461197265625" />
                  <Point X="-3.014190673828" Y="-27.67770703125" />
                  <Point X="-3.818024169922" Y="-28.14180078125" />
                  <Point X="-3.986864990234" Y="-27.919978515625" />
                  <Point X="-4.082862060547" Y="-27.793857421875" />
                  <Point X="-4.286833984375" Y="-27.451828125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-4.029651611328" Y="-27.20729296875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084576416016" Y="-26.47559375" />
                  <Point X="-3.066611083984" Y="-26.4484609375" />
                  <Point X="-3.053856201172" Y="-26.419833984375" />
                  <Point X="-3.048392333984" Y="-26.39873828125" />
                  <Point X="-3.046151611328" Y="-26.3900859375" />
                  <Point X="-3.043347412109" Y="-26.359654296875" />
                  <Point X="-3.045556884766" Y="-26.327984375" />
                  <Point X="-3.05255859375" Y="-26.29823828125" />
                  <Point X="-3.068641601562" Y="-26.272255859375" />
                  <Point X="-3.089473876953" Y="-26.24830078125" />
                  <Point X="-3.112974365234" Y="-26.228767578125" />
                  <Point X="-3.131754882812" Y="-26.21771484375" />
                  <Point X="-3.13945703125" Y="-26.213181640625" />
                  <Point X="-3.168716064453" Y="-26.201958984375" />
                  <Point X="-3.200604003906" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-3.70533984375" Y="-26.256708984375" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.796532226562" Y="-26.139642578125" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.888043945312" Y="-25.615328125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.58510546875" Y="-25.502353515625" />
                  <Point X="-3.532875732422" Y="-25.22041015625" />
                  <Point X="-3.517496582031" Y="-25.214830078125" />
                  <Point X="-3.487730224609" Y="-25.19947265625" />
                  <Point X="-3.468048828125" Y="-25.1858125" />
                  <Point X="-3.459977294922" Y="-25.1802109375" />
                  <Point X="-3.437524169922" Y="-25.158328125" />
                  <Point X="-3.418278564453" Y="-25.132072265625" />
                  <Point X="-3.404168457031" Y="-25.104068359375" />
                  <Point X="-3.397607910156" Y="-25.0829296875" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.390648193359" Y="-25.046107421875" />
                  <Point X="-3.390647216797" Y="-25.016466796875" />
                  <Point X="-3.394916503906" Y="-24.9883046875" />
                  <Point X="-3.401477050781" Y="-24.967166015625" />
                  <Point X="-3.404167480469" Y="-24.95849609375" />
                  <Point X="-3.418275634766" Y="-24.930494140625" />
                  <Point X="-3.437522705078" Y="-24.904234375" />
                  <Point X="-3.459982910156" Y="-24.88234765625" />
                  <Point X="-3.479664306641" Y="-24.868689453125" />
                  <Point X="-3.488528564453" Y="-24.86323046875" />
                  <Point X="-3.512165771484" Y="-24.850408203125" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-3.964412597656" Y="-24.726521484375" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.848684082031" Y="-24.186541015625" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.71585546875" Y="-23.622140625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.519930175781" Y="-23.60090625" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985107422" Y="-23.700658203125" />
                  <Point X="-3.723423339844" Y="-23.698771484375" />
                  <Point X="-3.703138183594" Y="-23.694736328125" />
                  <Point X="-3.659577148438" Y="-23.681001953125" />
                  <Point X="-3.641712158203" Y="-23.675369140625" />
                  <Point X="-3.622777832031" Y="-23.667037109375" />
                  <Point X="-3.604033447266" Y="-23.65621484375" />
                  <Point X="-3.587351806641" Y="-23.643984375" />
                  <Point X="-3.573713378906" Y="-23.628431640625" />
                  <Point X="-3.561299072266" Y="-23.610701171875" />
                  <Point X="-3.5513515625" Y="-23.592568359375" />
                  <Point X="-3.533872558594" Y="-23.55037109375" />
                  <Point X="-3.526704101563" Y="-23.533064453125" />
                  <Point X="-3.520915771484" Y="-23.513205078125" />
                  <Point X="-3.517157470703" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.471251953125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.532050048828" Y="-23.41062109375" />
                  <Point X="-3.553140380859" Y="-23.370107421875" />
                  <Point X="-3.561789794922" Y="-23.3534921875" />
                  <Point X="-3.573281982422" Y="-23.33629296875" />
                  <Point X="-3.587194580078" Y="-23.319712890625" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-3.849666259766" Y="-23.11547265625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.175169921875" Y="-22.427412109375" />
                  <Point X="-4.08115625" Y="-22.266345703125" />
                  <Point X="-3.793391113281" Y="-21.896462890625" />
                  <Point X="-3.750504882813" Y="-21.841337890625" />
                  <Point X="-3.669904785156" Y="-21.887873046875" />
                  <Point X="-3.206656738281" Y="-22.155330078125" />
                  <Point X="-3.1877265625" Y="-22.163658203125" />
                  <Point X="-3.167082275391" Y="-22.17016796875" />
                  <Point X="-3.146793701172" Y="-22.174205078125" />
                  <Point X="-3.086125244141" Y="-22.179513671875" />
                  <Point X="-3.061244384766" Y="-22.181689453125" />
                  <Point X="-3.040557861328" Y="-22.18123828125" />
                  <Point X="-3.019100585938" Y="-22.178412109375" />
                  <Point X="-2.999010986328" Y="-22.173494140625" />
                  <Point X="-2.980461425781" Y="-22.164345703125" />
                  <Point X="-2.962208251953" Y="-22.152716796875" />
                  <Point X="-2.946078125" Y="-22.139771484375" />
                  <Point X="-2.903015136719" Y="-22.096708984375" />
                  <Point X="-2.885354492188" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.848743652344" Y="-21.903212890625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-2.979483398438" Y="-21.628482421875" />
                  <Point X="-3.183332763672" Y="-21.27540625" />
                  <Point X="-2.8643671875" Y="-21.030857421875" />
                  <Point X="-2.700620849609" Y="-20.905314453125" />
                  <Point X="-2.247409912109" Y="-20.653521484375" />
                  <Point X="-2.167036621094" Y="-20.6088671875" />
                  <Point X="-2.043196044922" Y="-20.7702578125" />
                  <Point X="-2.028891479492" Y="-20.78519921875" />
                  <Point X="-2.012311645508" Y="-20.799111328125" />
                  <Point X="-1.995114868164" Y="-20.810603515625" />
                  <Point X="-1.927591186523" Y="-20.845755859375" />
                  <Point X="-1.899898681641" Y="-20.860171875" />
                  <Point X="-1.880625366211" Y="-20.86766796875" />
                  <Point X="-1.859718994141" Y="-20.873271484375" />
                  <Point X="-1.839268920898" Y="-20.876419921875" />
                  <Point X="-1.818622192383" Y="-20.87506640625" />
                  <Point X="-1.797307006836" Y="-20.871306640625" />
                  <Point X="-1.777453125" Y="-20.865517578125" />
                  <Point X="-1.707122924805" Y="-20.836384765625" />
                  <Point X="-1.678279174805" Y="-20.8244375" />
                  <Point X="-1.660147216797" Y="-20.814490234375" />
                  <Point X="-1.642417236328" Y="-20.802076171875" />
                  <Point X="-1.626864746094" Y="-20.7884375" />
                  <Point X="-1.614633056641" Y="-20.771755859375" />
                  <Point X="-1.603810913086" Y="-20.75301171875" />
                  <Point X="-1.595480102539" Y="-20.734078125" />
                  <Point X="-1.572588867188" Y="-20.6614765625" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.570200805664" Y="-20.474453125" />
                  <Point X="-1.584202026367" Y="-20.368103515625" />
                  <Point X="-1.161609375" Y="-20.249623046875" />
                  <Point X="-0.949623962402" Y="-20.19019140625" />
                  <Point X="-0.400204864502" Y="-20.125888671875" />
                  <Point X="-0.29471105957" Y="-20.113541015625" />
                  <Point X="-0.270944854736" Y="-20.20223828125" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113998413" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907631" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425949097" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.202418243408" Y="-20.50393359375" />
                  <Point X="0.307419616699" Y="-20.112060546875" />
                  <Point X="0.658949157715" Y="-20.148876953125" />
                  <Point X="0.844031066895" Y="-20.168259765625" />
                  <Point X="1.298603027344" Y="-20.2780078125" />
                  <Point X="1.481039794922" Y="-20.3220546875" />
                  <Point X="1.776472900391" Y="-20.429208984375" />
                  <Point X="1.894646118164" Y="-20.472072265625" />
                  <Point X="2.180745117188" Y="-20.60587109375" />
                  <Point X="2.294558105469" Y="-20.65909765625" />
                  <Point X="2.570984375" Y="-20.82014453125" />
                  <Point X="2.6809921875" Y="-20.884234375" />
                  <Point X="2.941646972656" Y="-21.06959765625" />
                  <Point X="2.943260986328" Y="-21.07074609375" />
                  <Point X="2.758614257812" Y="-21.3905625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.4839453125" />
                  <Point X="2.1178515625" Y="-22.540880859375" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.113664794922" Y="-22.66828125" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121443115234" Y="-22.7103984375" />
                  <Point X="2.129709716797" Y="-22.732486328125" />
                  <Point X="2.140071044922" Y="-22.75252734375" />
                  <Point X="2.170534912109" Y="-22.797423828125" />
                  <Point X="2.183028808594" Y="-22.8158359375" />
                  <Point X="2.194465332031" Y="-22.829671875" />
                  <Point X="2.221596923828" Y="-22.85440625" />
                  <Point X="2.266492919922" Y="-22.88487109375" />
                  <Point X="2.284905517578" Y="-22.897365234375" />
                  <Point X="2.304944335938" Y="-22.907724609375" />
                  <Point X="2.327031494141" Y="-22.9159921875" />
                  <Point X="2.348964111328" Y="-22.921337890625" />
                  <Point X="2.398197509766" Y="-22.9272734375" />
                  <Point X="2.418388916016" Y="-22.929708984375" />
                  <Point X="2.436466308594" Y="-22.93015625" />
                  <Point X="2.473208007812" Y="-22.925830078125" />
                  <Point X="2.530144042969" Y="-22.910603515625" />
                  <Point X="2.553494384766" Y="-22.904359375" />
                  <Point X="2.565286376953" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.88985546875" />
                  <Point X="3.022576416016" Y="-22.63926171875" />
                  <Point X="3.967326416016" Y="-22.093810546875" />
                  <Point X="4.058026611328" Y="-22.21986328125" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="4.035266357422" Y="-22.714248046875" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221423339844" Y="-23.339759765625" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.162996826172" Y="-23.411830078125" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.136605957031" Y="-23.449087890625" />
                  <Point X="3.121629638672" Y="-23.4829140625" />
                  <Point X="3.106365722656" Y="-23.537494140625" />
                  <Point X="3.100105712891" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.11026953125" Y="-23.688958984375" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679443359" Y="-23.731021484375" />
                  <Point X="3.136282958984" Y="-23.76426171875" />
                  <Point X="3.170363525391" Y="-23.8160625" />
                  <Point X="3.184340576172" Y="-23.837306640625" />
                  <Point X="3.198895019531" Y="-23.854552734375" />
                  <Point X="3.216137451172" Y="-23.870640625" />
                  <Point X="3.234345947266" Y="-23.88396484375" />
                  <Point X="3.283733398438" Y="-23.911765625" />
                  <Point X="3.303988037109" Y="-23.92316796875" />
                  <Point X="3.320521484375" Y="-23.9305" />
                  <Point X="3.356120117188" Y="-23.9405625" />
                  <Point X="3.422895263672" Y="-23.94938671875" />
                  <Point X="3.450280761719" Y="-23.953005859375" />
                  <Point X="3.462698242188" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="3.900515136719" Y="-23.898734375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.817913574219" Y="-23.952083984375" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.638534179688" Y="-24.423373046875" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704785644531" Y="-24.674416015625" />
                  <Point X="3.681547851562" Y="-24.68493359375" />
                  <Point X="3.615943359375" Y="-24.722853515625" />
                  <Point X="3.589037841797" Y="-24.738404296875" />
                  <Point X="3.574317382812" Y="-24.7488984375" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.508167724609" Y="-24.82458203125" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.450559814453" Y="-24.975908203125" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.458300048828" Y="-25.12706640625" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.176662109375" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.531386962891" Y="-25.26756640625" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.559997802734" Y="-25.301236328125" />
                  <Point X="3.589035400391" Y="-25.32415625" />
                  <Point X="3.654639892578" Y="-25.362076171875" />
                  <Point X="3.681545410156" Y="-25.37762890625" />
                  <Point X="3.692710205078" Y="-25.383140625" />
                  <Point X="3.716580322266" Y="-25.39215234375" />
                  <Point X="4.094688476562" Y="-25.49346484375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.870668945313" Y="-25.844947265625" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.49358984375" Y="-26.144205078125" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.245900878906" Y="-26.03349609375" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163975097656" Y="-26.056595703125" />
                  <Point X="3.136147949219" Y="-26.07348828125" />
                  <Point X="3.112397460938" Y="-26.0939609375" />
                  <Point X="3.034571044922" Y="-26.1875625" />
                  <Point X="3.002653320312" Y="-26.22594921875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.958603271484" Y="-26.42658203125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.047707519531" Y="-26.67883203125" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.461515136719" Y="-27.03015625" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.168916015625" Y="-27.678416015625" />
                  <Point X="4.124810546875" Y="-27.749787109375" />
                  <Point X="4.028981201172" Y="-27.8859453125" />
                  <Point X="3.753228271484" Y="-27.726740234375" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786128417969" Y="-27.17001171875" />
                  <Point X="2.754224365234" Y="-27.159826171875" />
                  <Point X="2.600981445312" Y="-27.132150390625" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.506783447266" Y="-27.120396484375" />
                  <Point X="2.474610839844" Y="-27.125353515625" />
                  <Point X="2.444833496094" Y="-27.135177734375" />
                  <Point X="2.317526123047" Y="-27.202177734375" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242385009766" Y="-27.246548828125" />
                  <Point X="2.221425537109" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.137531005859" Y="-27.41774609375" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.123351074219" Y="-27.716501953125" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.377299560547" Y="-28.21662109375" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.834188232422" Y="-29.074259765625" />
                  <Point X="2.781831054688" Y="-29.11165625" />
                  <Point X="2.701764892578" Y="-29.163482421875" />
                  <Point X="2.48521875" Y="-28.8812734375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506347656" Y="-27.922181640625" />
                  <Point X="1.721923706055" Y="-27.900556640625" />
                  <Point X="1.570784912109" Y="-27.803388671875" />
                  <Point X="1.508800292969" Y="-27.7635390625" />
                  <Point X="1.479987060547" Y="-27.75116796875" />
                  <Point X="1.448367797852" Y="-27.7434375" />
                  <Point X="1.417100830078" Y="-27.741119140625" />
                  <Point X="1.2518046875" Y="-27.756330078125" />
                  <Point X="1.184014038086" Y="-27.76256640625" />
                  <Point X="1.156365356445" Y="-27.769396484375" />
                  <Point X="1.128978271484" Y="-27.780740234375" />
                  <Point X="1.104594970703" Y="-27.795462890625" />
                  <Point X="0.976957641602" Y="-27.90158984375" />
                  <Point X="0.924611328125" Y="-27.945115234375" />
                  <Point X="0.904140441895" Y="-27.968865234375" />
                  <Point X="0.887248596191" Y="-27.99669140625" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.837461364746" Y="-28.201388671875" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.883641601562" Y="-28.808484375" />
                  <Point X="1.022065490723" Y="-29.859916015625" />
                  <Point X="0.975710266113" Y="-29.870078125" />
                  <Point X="0.929315551758" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058412475586" Y="-29.752638671875" />
                  <Point X="-1.14124597168" Y="-29.731326171875" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.1662578125" Y="-29.281900390625" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.18812487793" Y="-29.178470703125" />
                  <Point X="-1.199026245117" Y="-29.149505859375" />
                  <Point X="-1.205664428711" Y="-29.13546875" />
                  <Point X="-1.221736328125" Y="-29.107630859375" />
                  <Point X="-1.230570678711" Y="-29.0948671875" />
                  <Point X="-1.250205932617" Y="-29.070939453125" />
                  <Point X="-1.261006958008" Y="-29.05977734375" />
                  <Point X="-1.426426513672" Y="-28.914708984375" />
                  <Point X="-1.494267822266" Y="-28.855212890625" />
                  <Point X="-1.506740844727" Y="-28.845962890625" />
                  <Point X="-1.533025024414" Y="-28.829619140625" />
                  <Point X="-1.546836425781" Y="-28.822525390625" />
                  <Point X="-1.576534667969" Y="-28.810224609375" />
                  <Point X="-1.59131640625" Y="-28.8054765625" />
                  <Point X="-1.621456176758" Y="-28.79844921875" />
                  <Point X="-1.636813964844" Y="-28.796169921875" />
                  <Point X="-1.856362792969" Y="-28.781779296875" />
                  <Point X="-1.946403198242" Y="-28.77587890625" />
                  <Point X="-1.961927612305" Y="-28.7761328125" />
                  <Point X="-1.99272668457" Y="-28.779166015625" />
                  <Point X="-2.008001098633" Y="-28.7819453125" />
                  <Point X="-2.039049072266" Y="-28.790263671875" />
                  <Point X="-2.053668212891" Y="-28.795494140625" />
                  <Point X="-2.081862060547" Y="-28.808267578125" />
                  <Point X="-2.095436767578" Y="-28.815810546875" />
                  <Point X="-2.278376464844" Y="-28.938046875" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359685302734" Y="-28.9927578125" />
                  <Point X="-2.380448730469" Y="-29.01013671875" />
                  <Point X="-2.402762207031" Y="-29.032775390625" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.475879638672" Y="-29.126873046875" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.630194091797" Y="-29.08384765625" />
                  <Point X="-2.747611816406" Y="-29.011146484375" />
                  <Point X="-2.980863525391" Y="-28.83155078125" />
                  <Point X="-2.869413330078" Y="-28.63851171875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.61923046875" />
                  <Point X="-2.310626953125" Y="-27.58829296875" />
                  <Point X="-2.314668457031" Y="-27.55760546875" />
                  <Point X="-2.323654541016" Y="-27.52798828125" />
                  <Point X="-2.329361572266" Y="-27.513548828125" />
                  <Point X="-2.343580810547" Y="-27.48471875" />
                  <Point X="-2.351562011719" Y="-27.471404296875" />
                  <Point X="-2.369589355469" Y="-27.446248046875" />
                  <Point X="-2.379635498047" Y="-27.43440625" />
                  <Point X="-2.396984130859" Y="-27.41705859375" />
                  <Point X="-2.408826416016" Y="-27.407013671875" />
                  <Point X="-2.433986572266" Y="-27.388986328125" />
                  <Point X="-2.447304443359" Y="-27.38100390625" />
                  <Point X="-2.4761328125" Y="-27.3667890625" />
                  <Point X="-2.490573730469" Y="-27.361083984375" />
                  <Point X="-2.520190429688" Y="-27.3521015625" />
                  <Point X="-2.550874267578" Y="-27.3480625" />
                  <Point X="-2.581806884766" Y="-27.349076171875" />
                  <Point X="-2.597231445312" Y="-27.3508515625" />
                  <Point X="-2.628755859375" Y="-27.357123046875" />
                  <Point X="-2.643681152344" Y="-27.361384765625" />
                  <Point X="-2.672645019531" Y="-27.37228515625" />
                  <Point X="-2.68668359375" Y="-27.378923828125" />
                  <Point X="-3.061690673828" Y="-27.59543359375" />
                  <Point X="-3.793089355469" Y="-28.01770703125" />
                  <Point X="-3.911271728516" Y="-27.862439453125" />
                  <Point X="-4.004021484375" Y="-27.740583984375" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.971819335938" Y="-27.282662109375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036480712891" Y="-26.563310546875" />
                  <Point X="-3.015102539062" Y="-26.540388671875" />
                  <Point X="-3.005365966797" Y="-26.528041015625" />
                  <Point X="-2.987400634766" Y="-26.500908203125" />
                  <Point X="-2.979834716797" Y="-26.487125" />
                  <Point X="-2.967079833984" Y="-26.458498046875" />
                  <Point X="-2.961890869141" Y="-26.443654296875" />
                  <Point X="-2.956427001953" Y="-26.42255859375" />
                  <Point X="-2.951552490234" Y="-26.398802734375" />
                  <Point X="-2.948748291016" Y="-26.36837109375" />
                  <Point X="-2.948577880859" Y="-26.35304296875" />
                  <Point X="-2.950787353516" Y="-26.321373046875" />
                  <Point X="-2.953083984375" Y="-26.30621875" />
                  <Point X="-2.960085693359" Y="-26.27647265625" />
                  <Point X="-2.971781494141" Y="-26.24823828125" />
                  <Point X="-2.987864501953" Y="-26.222255859375" />
                  <Point X="-2.996956787109" Y="-26.209916015625" />
                  <Point X="-3.0177890625" Y="-26.1859609375" />
                  <Point X="-3.028749023438" Y="-26.1752421875" />
                  <Point X="-3.052249511719" Y="-26.155708984375" />
                  <Point X="-3.064790039063" Y="-26.14689453125" />
                  <Point X="-3.083570556641" Y="-26.135841796875" />
                  <Point X="-3.105435302734" Y="-26.124482421875" />
                  <Point X="-3.134694335938" Y="-26.113259765625" />
                  <Point X="-3.149790771484" Y="-26.10886328125" />
                  <Point X="-3.181678710938" Y="-26.102380859375" />
                  <Point X="-3.197294921875" Y="-26.10053515625" />
                  <Point X="-3.228619873047" Y="-26.099443359375" />
                  <Point X="-3.244328613281" Y="-26.100197265625" />
                  <Point X="-3.717739501953" Y="-26.162521484375" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.704487304688" Y="-26.116130859375" />
                  <Point X="-4.740762207031" Y="-25.974115234375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.560517089844" Y="-25.5941171875" />
                  <Point X="-3.508287353516" Y="-25.312173828125" />
                  <Point X="-3.500473388672" Y="-25.309712890625" />
                  <Point X="-3.473938720703" Y="-25.299255859375" />
                  <Point X="-3.444172363281" Y="-25.2838984375" />
                  <Point X="-3.433562744141" Y="-25.277517578125" />
                  <Point X="-3.413881347656" Y="-25.263857421875" />
                  <Point X="-3.393671630859" Y="-25.248244140625" />
                  <Point X="-3.371218505859" Y="-25.226361328125" />
                  <Point X="-3.360903564453" Y="-25.214490234375" />
                  <Point X="-3.341657958984" Y="-25.188234375" />
                  <Point X="-3.333439453125" Y="-25.1748203125" />
                  <Point X="-3.319329345703" Y="-25.14681640625" />
                  <Point X="-3.313437744141" Y="-25.1322265625" />
                  <Point X="-3.306877197266" Y="-25.111087890625" />
                  <Point X="-3.300991210938" Y="-25.08850390625" />
                  <Point X="-3.296721923828" Y="-25.060349609375" />
                  <Point X="-3.295648193359" Y="-25.046111328125" />
                  <Point X="-3.295647216797" Y="-25.016470703125" />
                  <Point X="-3.296720458984" Y="-25.002228515625" />
                  <Point X="-3.300989746094" Y="-24.97406640625" />
                  <Point X="-3.304185791016" Y="-24.960146484375" />
                  <Point X="-3.310746337891" Y="-24.9390078125" />
                  <Point X="-3.319327148438" Y="-24.915751953125" />
                  <Point X="-3.333435302734" Y="-24.88775" />
                  <Point X="-3.341653076172" Y="-24.874333984375" />
                  <Point X="-3.360900146484" Y="-24.84807421875" />
                  <Point X="-3.371221679688" Y="-24.8361953125" />
                  <Point X="-3.393681884766" Y="-24.81430859375" />
                  <Point X="-3.405820556641" Y="-24.80430078125" />
                  <Point X="-3.425501953125" Y="-24.790642578125" />
                  <Point X="-3.44323046875" Y="-24.779724609375" />
                  <Point X="-3.466867675781" Y="-24.76690234375" />
                  <Point X="-3.47698046875" Y="-24.7621640625" />
                  <Point X="-3.497690917969" Y="-24.75390625" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.939824951172" Y="-24.6347578125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.754707519531" Y="-24.200447265625" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.532330078125" Y="-23.69509375" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057128906" Y="-23.795634765625" />
                  <Point X="-3.736703857422" Y="-23.795296875" />
                  <Point X="-3.715142089844" Y="-23.79341015625" />
                  <Point X="-3.704888916016" Y="-23.7919453125" />
                  <Point X="-3.684603759766" Y="-23.78791015625" />
                  <Point X="-3.674571777344" Y="-23.78533984375" />
                  <Point X="-3.631010742188" Y="-23.77160546875" />
                  <Point X="-3.603448486328" Y="-23.762322265625" />
                  <Point X="-3.584514160156" Y="-23.753990234375" />
                  <Point X="-3.575277099609" Y="-23.74930859375" />
                  <Point X="-3.556532714844" Y="-23.738486328125" />
                  <Point X="-3.547862060547" Y="-23.732830078125" />
                  <Point X="-3.531180419922" Y="-23.720599609375" />
                  <Point X="-3.515924804688" Y="-23.706619140625" />
                  <Point X="-3.502286376953" Y="-23.69106640625" />
                  <Point X="-3.495892578125" Y="-23.682919921875" />
                  <Point X="-3.483478271484" Y="-23.665189453125" />
                  <Point X="-3.478009033203" Y="-23.656392578125" />
                  <Point X="-3.468061523438" Y="-23.638259765625" />
                  <Point X="-3.463583251953" Y="-23.628923828125" />
                  <Point X="-3.446104248047" Y="-23.5867265625" />
                  <Point X="-3.435499267578" Y="-23.5596484375" />
                  <Point X="-3.4297109375" Y="-23.5397890625" />
                  <Point X="-3.427359130859" Y="-23.529701171875" />
                  <Point X="-3.423600830078" Y="-23.50838671875" />
                  <Point X="-3.422360839844" Y="-23.498103515625" />
                  <Point X="-3.421008056641" Y="-23.47746484375" />
                  <Point X="-3.42191015625" Y="-23.456802734375" />
                  <Point X="-3.425056640625" Y="-23.436357421875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436011962891" Y="-23.395470703125" />
                  <Point X="-3.443509033203" Y="-23.37619140625" />
                  <Point X="-3.447784179688" Y="-23.36675390625" />
                  <Point X="-3.468874511719" Y="-23.326240234375" />
                  <Point X="-3.482800292969" Y="-23.300712890625" />
                  <Point X="-3.494292480469" Y="-23.283513671875" />
                  <Point X="-3.500508300781" Y="-23.2752265625" />
                  <Point X="-3.514420898438" Y="-23.258646484375" />
                  <Point X="-3.521501708984" Y="-23.251087890625" />
                  <Point X="-3.536442871094" Y="-23.23678515625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.791833740234" Y="-23.040103515625" />
                  <Point X="-4.227614746094" Y="-22.70571875" />
                  <Point X="-4.093123535156" Y="-22.47530078125" />
                  <Point X="-4.002296386719" Y="-22.319693359375" />
                  <Point X="-3.726338134766" Y="-21.964986328125" />
                  <Point X="-3.717405273438" Y="-21.97014453125" />
                  <Point X="-3.254157226562" Y="-22.2376015625" />
                  <Point X="-3.244912353516" Y="-22.242287109375" />
                  <Point X="-3.225982177734" Y="-22.250615234375" />
                  <Point X="-3.216296142578" Y="-22.254259765625" />
                  <Point X="-3.195651855469" Y="-22.26076953125" />
                  <Point X="-3.185622314453" Y="-22.263341796875" />
                  <Point X="-3.165333740234" Y="-22.26737890625" />
                  <Point X="-3.155074707031" Y="-22.26884375" />
                  <Point X="-3.09440625" Y="-22.27415234375" />
                  <Point X="-3.069525390625" Y="-22.276328125" />
                  <Point X="-3.059172851562" Y="-22.276666015625" />
                  <Point X="-3.038486328125" Y="-22.27621484375" />
                  <Point X="-3.02815234375" Y="-22.27542578125" />
                  <Point X="-3.006695068359" Y="-22.272599609375" />
                  <Point X="-2.996511474609" Y="-22.2706875" />
                  <Point X="-2.976421875" Y="-22.26576953125" />
                  <Point X="-2.956990478516" Y="-22.2586953125" />
                  <Point X="-2.938440917969" Y="-22.249546875" />
                  <Point X="-2.929416748047" Y="-22.244466796875" />
                  <Point X="-2.911163574219" Y="-22.232837890625" />
                  <Point X="-2.902746826172" Y="-22.226806640625" />
                  <Point X="-2.886616699219" Y="-22.213861328125" />
                  <Point X="-2.878903320312" Y="-22.206947265625" />
                  <Point X="-2.835840332031" Y="-22.163884765625" />
                  <Point X="-2.8181796875" Y="-22.146224609375" />
                  <Point X="-2.811267089844" Y="-22.138513671875" />
                  <Point X="-2.798321533203" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.754105224609" Y="-21.89493359375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.897211181641" Y="-21.580982421875" />
                  <Point X="-3.059386962891" Y="-21.3000859375" />
                  <Point X="-2.806564941406" Y="-21.10625" />
                  <Point X="-2.648368652344" Y="-20.984962890625" />
                  <Point X="-2.201272460938" Y="-20.73656640625" />
                  <Point X="-2.1925234375" Y="-20.731705078125" />
                  <Point X="-2.118564453125" Y="-20.82808984375" />
                  <Point X="-2.111817626953" Y="-20.835955078125" />
                  <Point X="-2.097513183594" Y="-20.850896484375" />
                  <Point X="-2.089956298828" Y="-20.85797265625" />
                  <Point X="-2.073376464844" Y="-20.871884765625" />
                  <Point X="-2.065096191406" Y="-20.87809765625" />
                  <Point X="-2.047899291992" Y="-20.88958984375" />
                  <Point X="-2.038982666016" Y="-20.894869140625" />
                  <Point X="-1.971458984375" Y="-20.930021484375" />
                  <Point X="-1.943766479492" Y="-20.9444375" />
                  <Point X="-1.934334838867" Y="-20.9487109375" />
                  <Point X="-1.915061401367" Y="-20.95620703125" />
                  <Point X="-1.905219970703" Y="-20.9594296875" />
                  <Point X="-1.884313720703" Y="-20.965033203125" />
                  <Point X="-1.874174560547" Y="-20.967166015625" />
                  <Point X="-1.853724487305" Y="-20.970314453125" />
                  <Point X="-1.83305456543" Y="-20.971216796875" />
                  <Point X="-1.812407714844" Y="-20.96986328125" />
                  <Point X="-1.802119995117" Y="-20.968623046875" />
                  <Point X="-1.78080480957" Y="-20.96486328125" />
                  <Point X="-1.770713989258" Y="-20.9625078125" />
                  <Point X="-1.750860107422" Y="-20.95671875" />
                  <Point X="-1.741097045898" Y="-20.95328515625" />
                  <Point X="-1.670766723633" Y="-20.92415234375" />
                  <Point X="-1.641923095703" Y="-20.912205078125" />
                  <Point X="-1.632586181641" Y="-20.9077265625" />
                  <Point X="-1.614454101562" Y="-20.897779296875" />
                  <Point X="-1.605659301758" Y="-20.892310546875" />
                  <Point X="-1.587929077148" Y="-20.879896484375" />
                  <Point X="-1.579780639648" Y="-20.873501953125" />
                  <Point X="-1.564228027344" Y="-20.85986328125" />
                  <Point X="-1.550252929688" Y="-20.84461328125" />
                  <Point X="-1.538021240234" Y="-20.827931640625" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.52153894043" Y="-20.80051171875" />
                  <Point X="-1.516856079102" Y="-20.791271484375" />
                  <Point X="-1.508525268555" Y="-20.772337890625" />
                  <Point X="-1.504877075195" Y="-20.76264453125" />
                  <Point X="-1.481985717773" Y="-20.69004296875" />
                  <Point X="-1.47259765625" Y="-20.660267578125" />
                  <Point X="-1.470026123047" Y="-20.650234375" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.476013549805" Y="-20.462052734375" />
                  <Point X="-1.479266235352" Y="-20.437345703125" />
                  <Point X="-1.135963500977" Y="-20.341095703125" />
                  <Point X="-0.931164916992" Y="-20.2836796875" />
                  <Point X="-0.389161621094" Y="-20.220244140625" />
                  <Point X="-0.365222229004" Y="-20.21744140625" />
                  <Point X="-0.362707794189" Y="-20.226826171875" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.294181213379" Y="-20.528521484375" />
                  <Point X="0.378190765381" Y="-20.2149921875" />
                  <Point X="0.649053771973" Y="-20.243359375" />
                  <Point X="0.827852722168" Y="-20.262083984375" />
                  <Point X="1.276307617188" Y="-20.37035546875" />
                  <Point X="1.453624023438" Y="-20.413166015625" />
                  <Point X="1.744080932617" Y="-20.518515625" />
                  <Point X="1.858246459961" Y="-20.55992578125" />
                  <Point X="2.140500488281" Y="-20.69192578125" />
                  <Point X="2.250428466797" Y="-20.7433359375" />
                  <Point X="2.523161376953" Y="-20.90223046875" />
                  <Point X="2.629451416016" Y="-20.964154296875" />
                  <Point X="2.817780761719" Y="-21.098083984375" />
                  <Point X="2.676341796875" Y="-21.3430625" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.0531796875" Y="-22.42656640625" />
                  <Point X="2.044181640625" Y="-22.450439453125" />
                  <Point X="2.041301635742" Y="-22.459404296875" />
                  <Point X="2.026076293945" Y="-22.51633984375" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017912719727" Y="-22.548537109375" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.019348022461" Y="-22.679654296875" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.023801025391" Y="-22.710966796875" />
                  <Point X="2.029144775391" Y="-22.732892578125" />
                  <Point X="2.032470214844" Y="-22.743697265625" />
                  <Point X="2.040736694336" Y="-22.76578515625" />
                  <Point X="2.045320922852" Y="-22.776115234375" />
                  <Point X="2.055682373047" Y="-22.79615625" />
                  <Point X="2.061459472656" Y="-22.8058671875" />
                  <Point X="2.091923339844" Y="-22.850763671875" />
                  <Point X="2.104417236328" Y="-22.86917578125" />
                  <Point X="2.109805175781" Y="-22.876361328125" />
                  <Point X="2.130463378906" Y="-22.899876953125" />
                  <Point X="2.157594970703" Y="-22.924611328125" />
                  <Point X="2.168254638672" Y="-22.933017578125" />
                  <Point X="2.213150634766" Y="-22.963482421875" />
                  <Point X="2.231563232422" Y="-22.9759765625" />
                  <Point X="2.241278808594" Y="-22.981755859375" />
                  <Point X="2.261317626953" Y="-22.992115234375" />
                  <Point X="2.271640869141" Y="-22.9966953125" />
                  <Point X="2.293728027344" Y="-23.004962890625" />
                  <Point X="2.304535400391" Y="-23.008291015625" />
                  <Point X="2.326468017578" Y="-23.01363671875" />
                  <Point X="2.337593261719" Y="-23.015654296875" />
                  <Point X="2.386826660156" Y="-23.02158984375" />
                  <Point X="2.407018066406" Y="-23.024025390625" />
                  <Point X="2.4160390625" Y="-23.0246796875" />
                  <Point X="2.447575439453" Y="-23.02450390625" />
                  <Point X="2.484317138672" Y="-23.020177734375" />
                  <Point X="2.497751708984" Y="-23.01760546875" />
                  <Point X="2.554687744141" Y="-23.00237890625" />
                  <Point X="2.578038085938" Y="-22.996134765625" />
                  <Point X="2.583998291016" Y="-22.994328125" />
                  <Point X="2.604408691406" Y="-22.986931640625" />
                  <Point X="2.62765625" Y="-22.97642578125" />
                  <Point X="2.636033935547" Y="-22.97212890625" />
                  <Point X="3.070076416016" Y="-22.72153515625" />
                  <Point X="3.940405029297" Y="-22.21905078125" />
                  <Point X="3.9809140625" Y="-22.275349609375" />
                  <Point X="4.043958007812" Y="-22.36296484375" />
                  <Point X="4.136884765625" Y="-22.516529296875" />
                  <Point X="3.977434082031" Y="-22.63887890625" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168137939453" Y="-23.260130859375" />
                  <Point X="3.152116455078" Y="-23.274787109375" />
                  <Point X="3.134666992188" Y="-23.293400390625" />
                  <Point X="3.128576660156" Y="-23.300578125" />
                  <Point X="3.087599609375" Y="-23.35403515625" />
                  <Point X="3.070794433594" Y="-23.375958984375" />
                  <Point X="3.065636474609" Y="-23.383396484375" />
                  <Point X="3.049739013672" Y="-23.41062890625" />
                  <Point X="3.034762695312" Y="-23.444455078125" />
                  <Point X="3.030139892578" Y="-23.457328125" />
                  <Point X="3.014875976562" Y="-23.511908203125" />
                  <Point X="3.008615966797" Y="-23.53429296875" />
                  <Point X="3.006224853516" Y="-23.54533984375" />
                  <Point X="3.002771484375" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.017229492188" Y="-23.70815625" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034682861328" Y="-23.771390625" />
                  <Point X="3.050286376953" Y="-23.804630859375" />
                  <Point X="3.056919189453" Y="-23.8164765625" />
                  <Point X="3.090999755859" Y="-23.86827734375" />
                  <Point X="3.104976806641" Y="-23.889521484375" />
                  <Point X="3.111739257813" Y="-23.898576171875" />
                  <Point X="3.126293701172" Y="-23.915822265625" />
                  <Point X="3.134085693359" Y="-23.924013671875" />
                  <Point X="3.151328125" Y="-23.9401015625" />
                  <Point X="3.160036376953" Y="-23.947306640625" />
                  <Point X="3.178244873047" Y="-23.960630859375" />
                  <Point X="3.187745117188" Y="-23.96675" />
                  <Point X="3.237132568359" Y="-23.99455078125" />
                  <Point X="3.257387207031" Y="-24.005953125" />
                  <Point X="3.265475830078" Y="-24.01001171875" />
                  <Point X="3.294680664062" Y="-24.02191796875" />
                  <Point X="3.330279296875" Y="-24.03198046875" />
                  <Point X="3.343674316406" Y="-24.034744140625" />
                  <Point X="3.410449462891" Y="-24.043568359375" />
                  <Point X="3.437834960938" Y="-24.0471875" />
                  <Point X="3.444033447266" Y="-24.04780078125" />
                  <Point X="3.465708496094" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="3.912915039063" Y="-23.992921875" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.725609375" Y="-23.9745546875" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="4.613945800781" Y="-24.331609375" />
                  <Point X="3.691991210938" Y="-24.578646484375" />
                  <Point X="3.686026855469" Y="-24.58045703125" />
                  <Point X="3.665613525391" Y="-24.5878671875" />
                  <Point X="3.642375732422" Y="-24.598384765625" />
                  <Point X="3.634007324219" Y="-24.602685546875" />
                  <Point X="3.568402832031" Y="-24.64060546875" />
                  <Point X="3.541497314453" Y="-24.65615625" />
                  <Point X="3.533891357422" Y="-24.661048828125" />
                  <Point X="3.508781494141" Y="-24.680123046875" />
                  <Point X="3.481994384766" Y="-24.7056484375" />
                  <Point X="3.472795654297" Y="-24.715775390625" />
                  <Point X="3.433433105469" Y="-24.76593359375" />
                  <Point X="3.417289794922" Y="-24.78650390625" />
                  <Point X="3.410854736328" Y="-24.795791015625" />
                  <Point X="3.399130615234" Y="-24.8150703125" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.38000390625" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.370376708984" Y="-24.88952734375" />
                  <Point X="3.357255615234" Y="-24.9580390625" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.364995849609" Y="-25.144935546875" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.183986328125" />
                  <Point X="3.380004394531" Y="-25.205484375" />
                  <Point X="3.384067382812" Y="-25.216029296875" />
                  <Point X="3.393841064453" Y="-25.237494140625" />
                  <Point X="3.399128662109" Y="-25.247484375" />
                  <Point X="3.410853027344" Y="-25.266765625" />
                  <Point X="3.417289794922" Y="-25.276056640625" />
                  <Point X="3.45665234375" Y="-25.32621484375" />
                  <Point X="3.472795654297" Y="-25.34678515625" />
                  <Point X="3.478715332031" Y="-25.353630859375" />
                  <Point X="3.501138671875" Y="-25.375806640625" />
                  <Point X="3.530176269531" Y="-25.3987265625" />
                  <Point X="3.541494873047" Y="-25.406404296875" />
                  <Point X="3.607099365234" Y="-25.44432421875" />
                  <Point X="3.634004882812" Y="-25.459876953125" />
                  <Point X="3.6394921875" Y="-25.462814453125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683026367188" Y="-25.481029296875" />
                  <Point X="3.691992919922" Y="-25.483916015625" />
                  <Point X="4.070101074219" Y="-25.585228515625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.77673046875" Y="-25.83078515625" />
                  <Point X="4.761614257812" Y="-25.931046875" />
                  <Point X="4.727801757812" Y="-26.07921875" />
                  <Point X="4.505989746094" Y="-26.050017578125" />
                  <Point X="3.436781738281" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354481445312" Y="-25.912677734375" />
                  <Point X="3.225723144531" Y="-25.9406640625" />
                  <Point X="3.172917236328" Y="-25.952140625" />
                  <Point X="3.157877929688" Y="-25.9567421875" />
                  <Point X="3.128758056641" Y="-25.968365234375" />
                  <Point X="3.114677490234" Y="-25.97538671875" />
                  <Point X="3.086850341797" Y="-25.992279296875" />
                  <Point X="3.074122070312" Y="-26.00153125" />
                  <Point X="3.050371582031" Y="-26.02200390625" />
                  <Point X="3.039349365234" Y="-26.033224609375" />
                  <Point X="2.961522949219" Y="-26.126826171875" />
                  <Point X="2.929605224609" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606445312" Y="-26.201228515625" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.864002929688" Y="-26.417876953125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.967797607422" Y="-26.73020703125" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486328125" Y="-26.828119140625" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.403682617188" Y="-27.105525390625" />
                  <Point X="4.087170166016" Y="-27.629984375" />
                  <Point X="4.045495361328" Y="-27.697421875" />
                  <Point X="4.001274902344" Y="-27.760251953125" />
                  <Point X="3.800728027344" Y="-27.644466796875" />
                  <Point X="2.848454345703" Y="-27.094671875" />
                  <Point X="2.841198730469" Y="-27.090890625" />
                  <Point X="2.815020996094" Y="-27.07951171875" />
                  <Point X="2.783116943359" Y="-27.069326171875" />
                  <Point X="2.771108398438" Y="-27.066337890625" />
                  <Point X="2.617865478516" Y="-27.038662109375" />
                  <Point X="2.555018066406" Y="-27.0273125" />
                  <Point X="2.539359130859" Y="-27.02580859375" />
                  <Point X="2.508008544922" Y="-27.025404296875" />
                  <Point X="2.492316894531" Y="-27.02650390625" />
                  <Point X="2.460144287109" Y="-27.0314609375" />
                  <Point X="2.444846191406" Y="-27.03513671875" />
                  <Point X="2.415068847656" Y="-27.0449609375" />
                  <Point X="2.400589599609" Y="-27.051109375" />
                  <Point X="2.273282226562" Y="-27.118109375" />
                  <Point X="2.221071533203" Y="-27.145587890625" />
                  <Point X="2.208969238281" Y="-27.153169921875" />
                  <Point X="2.186038818359" Y="-27.1700625" />
                  <Point X="2.175210693359" Y="-27.179373046875" />
                  <Point X="2.154251220703" Y="-27.20033203125" />
                  <Point X="2.144939697266" Y="-27.211162109375" />
                  <Point X="2.128046142578" Y="-27.23409375" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.053463134766" Y="-27.373501953125" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.02986340332" Y="-27.733384765625" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236816406" Y="-27.831546875" />
                  <Point X="2.0640703125" Y="-27.862482421875" />
                  <Point X="2.069547119141" Y="-27.873580078125" />
                  <Point X="2.29502734375" Y="-28.26412109375" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.72375390625" Y="-29.036083984375" />
                  <Point X="2.560587402344" Y="-28.82344140625" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828657348633" Y="-27.870150390625" />
                  <Point X="1.808834594727" Y="-27.84962890625" />
                  <Point X="1.783252075195" Y="-27.82800390625" />
                  <Point X="1.773298461914" Y="-27.820646484375" />
                  <Point X="1.622159667969" Y="-27.723478515625" />
                  <Point X="1.560175048828" Y="-27.68362890625" />
                  <Point X="1.546280395508" Y="-27.676244140625" />
                  <Point X="1.517467163086" Y="-27.663873046875" />
                  <Point X="1.502548706055" Y="-27.65888671875" />
                  <Point X="1.470929443359" Y="-27.65115625" />
                  <Point X="1.455392456055" Y="-27.648697265625" />
                  <Point X="1.424125488281" Y="-27.64637890625" />
                  <Point X="1.408395507812" Y="-27.64651953125" />
                  <Point X="1.243099365234" Y="-27.66173046875" />
                  <Point X="1.17530871582" Y="-27.667966796875" />
                  <Point X="1.161230957031" Y="-27.670337890625" />
                  <Point X="1.133582275391" Y="-27.67716796875" />
                  <Point X="1.120011352539" Y="-27.681626953125" />
                  <Point X="1.092624267578" Y="-27.692970703125" />
                  <Point X="1.079874145508" Y="-27.699416015625" />
                  <Point X="1.055490844727" Y="-27.714138671875" />
                  <Point X="1.043857666016" Y="-27.722416015625" />
                  <Point X="0.916220336914" Y="-27.82854296875" />
                  <Point X="0.863874023438" Y="-27.872068359375" />
                  <Point X="0.852652587891" Y="-27.883091796875" />
                  <Point X="0.83218170166" Y="-27.906841796875" />
                  <Point X="0.822932312012" Y="-27.919568359375" />
                  <Point X="0.806040405273" Y="-27.94739453125" />
                  <Point X="0.799018859863" Y="-27.961470703125" />
                  <Point X="0.78739465332" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.744628845215" Y="-28.1812109375" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.789454406738" Y="-28.820884765625" />
                  <Point X="0.833091064453" Y="-29.152337890625" />
                  <Point X="0.655065124512" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146972656" Y="-28.453583984375" />
                  <Point X="0.626788391113" Y="-28.42381640625" />
                  <Point X="0.620407592773" Y="-28.41320703125" />
                  <Point X="0.504298156738" Y="-28.245916015625" />
                  <Point X="0.456679840088" Y="-28.177306640625" />
                  <Point X="0.446670837402" Y="-28.165169921875" />
                  <Point X="0.424787139893" Y="-28.14271484375" />
                  <Point X="0.412912475586" Y="-28.132396484375" />
                  <Point X="0.386656890869" Y="-28.11315234375" />
                  <Point X="0.373240264893" Y="-28.104935546875" />
                  <Point X="0.345238983154" Y="-28.090828125" />
                  <Point X="0.330654449463" Y="-28.0849375" />
                  <Point X="0.1509818573" Y="-28.029173828125" />
                  <Point X="0.077295211792" Y="-28.0063046875" />
                  <Point X="0.063376972198" Y="-28.003109375" />
                  <Point X="0.035217689514" Y="-27.99883984375" />
                  <Point X="0.020976495743" Y="-27.997765625" />
                  <Point X="-0.008664410591" Y="-27.997765625" />
                  <Point X="-0.022905605316" Y="-27.99883984375" />
                  <Point X="-0.051064888" Y="-28.003109375" />
                  <Point X="-0.064983123779" Y="-28.0063046875" />
                  <Point X="-0.244655883789" Y="-28.062068359375" />
                  <Point X="-0.318342376709" Y="-28.0849375" />
                  <Point X="-0.332930908203" Y="-28.090830078125" />
                  <Point X="-0.360932800293" Y="-28.104939453125" />
                  <Point X="-0.374346008301" Y="-28.11315625" />
                  <Point X="-0.400601135254" Y="-28.132400390625" />
                  <Point X="-0.412475372314" Y="-28.14271875" />
                  <Point X="-0.434358886719" Y="-28.165173828125" />
                  <Point X="-0.444368041992" Y="-28.177310546875" />
                  <Point X="-0.560477478027" Y="-28.344603515625" />
                  <Point X="-0.60809564209" Y="-28.4132109375" />
                  <Point X="-0.612470092773" Y="-28.420130859375" />
                  <Point X="-0.625975402832" Y="-28.44526171875" />
                  <Point X="-0.63877722168" Y="-28.476212890625" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.759257324219" Y="-28.922736328125" />
                  <Point X="-0.985425537109" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.361482005455" Y="-20.808035866667" />
                  <Point X="0.611473497277" Y="-20.239423633785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.709298409829" Y="-21.020937178424" />
                  <Point X="0.366230607765" Y="-20.259628299924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.789588646688" Y="-21.146913969097" />
                  <Point X="0.341609155226" Y="-20.351517216347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.741027308586" Y="-21.231024345175" />
                  <Point X="0.316987702687" Y="-20.44340613277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.363959906693" Y="-20.22215284238" />
                  <Point X="-0.374953727075" Y="-20.218580733601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.692465970484" Y="-21.315134721253" />
                  <Point X="0.292366240555" Y="-20.535295046076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.334642512611" Y="-20.331567552462" />
                  <Point X="-0.600966641405" Y="-20.245033597435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.643904720851" Y="-21.399245126076" />
                  <Point X="0.267744657885" Y="-20.627183920217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.305325010648" Y="-20.440982297597" />
                  <Point X="-0.826980752023" Y="-20.271486072571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.595343515195" Y="-21.483355545188" />
                  <Point X="0.243123075215" Y="-20.719072794357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.276007508684" Y="-20.550397042732" />
                  <Point X="-1.020122767112" Y="-20.308619339001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.546782309539" Y="-21.567465964301" />
                  <Point X="0.204562432297" Y="-20.806432593277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.246690006721" Y="-20.659811787867" />
                  <Point X="-1.185152950992" Y="-20.354886693088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.498221103883" Y="-21.651576383413" />
                  <Point X="0.127444575661" Y="-20.881264394027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.212285504151" Y="-20.770879399693" />
                  <Point X="-1.3501809884" Y="-20.401154744606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.941352075702" Y="-22.220366971703" />
                  <Point X="3.939287318889" Y="-22.219696091547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.449659898227" Y="-21.735686802525" />
                  <Point X="-1.47627411433" Y="-20.460073515735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.035158216157" Y="-22.350735345667" />
                  <Point X="3.828578854864" Y="-22.283613642348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.401098692571" Y="-21.819797221637" />
                  <Point X="-1.462968357144" Y="-20.56428572962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.112139531632" Y="-22.47563700261" />
                  <Point X="3.717870390838" Y="-22.347531193149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.352537486915" Y="-21.903907640749" />
                  <Point X="-1.472820251526" Y="-20.660973566393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.075509166935" Y="-22.563623986942" />
                  <Point X="3.607161926813" Y="-22.41144874395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.303976281259" Y="-21.988018059861" />
                  <Point X="-1.501388449468" Y="-20.751580107498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.984055832301" Y="-22.633797908536" />
                  <Point X="3.496453462787" Y="-22.475366294751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.255415075603" Y="-22.072128478974" />
                  <Point X="-1.544910854729" Y="-20.837327732104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.892603055601" Y="-22.703972011414" />
                  <Point X="3.385744998762" Y="-22.539283845552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.206853869947" Y="-22.156238898086" />
                  <Point X="-1.63383235295" Y="-20.908324297216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.801150322453" Y="-22.774146128442" />
                  <Point X="3.275036534736" Y="-22.603201396353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.15829266429" Y="-22.240349317198" />
                  <Point X="-1.77291666475" Y="-20.963021976178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.085994837604" Y="-20.861296711357" />
                  <Point X="-2.300391864467" Y="-20.791634894516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.709697589305" Y="-22.84432024547" />
                  <Point X="3.164328070711" Y="-22.667118947154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.109731458634" Y="-22.32445973631" />
                  <Point X="-2.413837923217" Y="-20.854662946871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.618244856156" Y="-22.914494362499" />
                  <Point X="3.053619575128" Y="-22.731036487702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.061534537466" Y="-22.408688518627" />
                  <Point X="-2.527283981966" Y="-20.917690999225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.526792123008" Y="-22.984668479527" />
                  <Point X="2.942910898811" Y="-22.794953969525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030820729358" Y="-22.498597908729" />
                  <Point X="-2.640730040716" Y="-20.98071905158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435339389859" Y="-23.054842596556" />
                  <Point X="2.832202222494" Y="-22.858871451348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013333014899" Y="-22.592804717162" />
                  <Point X="-2.733713583674" Y="-21.050395778351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.343886656711" Y="-23.125016713584" />
                  <Point X="2.721493546176" Y="-22.922788933172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021229599308" Y="-22.695259384272" />
                  <Point X="-2.825219834125" Y="-21.120552506553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.252433923563" Y="-23.195190830612" />
                  <Point X="2.60736553586" Y="-22.985595406031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.063507261689" Y="-22.808885140793" />
                  <Point X="-2.916725961386" Y="-21.190709274783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.162040162174" Y="-23.265709028423" />
                  <Point X="2.420157477212" Y="-23.024656731785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.195940471373" Y="-22.951804210357" />
                  <Point X="-3.008232088647" Y="-21.260866043012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.09534712319" Y="-23.343928057758" />
                  <Point X="-3.028083674586" Y="-21.354304783042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.725336197272" Y="-23.973432523875" />
                  <Point X="4.533758940136" Y="-23.911185299681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.042629032362" Y="-23.426687823003" />
                  <Point X="-2.957095973606" Y="-21.477258996583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.751743356474" Y="-24.081901641324" />
                  <Point X="4.314979551799" Y="-23.939988478583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.01342727847" Y="-23.517088509302" />
                  <Point X="-2.886108195258" Y="-21.600213235263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.76848106303" Y="-24.187228963156" />
                  <Point X="4.096200163463" Y="-23.968791657486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001155630076" Y="-23.612990120336" />
                  <Point X="-2.815119999613" Y="-21.723167609531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.773679775963" Y="-24.288807038686" />
                  <Point X="3.877420547691" Y="-23.99759476249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.01942900743" Y="-23.718816411857" />
                  <Point X="-2.761594117916" Y="-21.840448134055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.605198741794" Y="-24.333953143545" />
                  <Point X="3.658639757491" Y="-24.026397485899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.06897750342" Y="-23.834804605426" />
                  <Point X="-2.749797515122" Y="-21.944169993954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.436714797036" Y="-24.379098302697" />
                  <Point X="3.39967959662" Y="-24.042145140395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.215197509022" Y="-23.982203276532" />
                  <Point X="-2.757597571604" Y="-22.041524513274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.268230852278" Y="-24.424243461849" />
                  <Point X="-2.802011728278" Y="-22.126982390281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.09974690752" Y="-24.469388621" />
                  <Point X="-2.875084522779" Y="-22.203128511392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.931262962762" Y="-24.514533780152" />
                  <Point X="-2.98400696754" Y="-22.26762637503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.482940957134" Y="-22.105512894691" />
                  <Point X="-3.764494142971" Y="-22.014030719075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.762779018003" Y="-24.559678939304" />
                  <Point X="-3.826525982534" Y="-22.09376426391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.617348355815" Y="-24.612314564025" />
                  <Point X="-3.888557822098" Y="-22.173497808745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.511727492571" Y="-24.677885176527" />
                  <Point X="-3.950589661662" Y="-22.25323135358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.441936763179" Y="-24.755097705235" />
                  <Point X="-4.010453984612" Y="-22.333669167254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.388170495654" Y="-24.837516897226" />
                  <Point X="-4.059463688414" Y="-22.417633860485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362786775815" Y="-24.92915813799" />
                  <Point X="-4.108473160124" Y="-22.501598629127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.34870043336" Y="-25.02447011918" />
                  <Point X="-4.157482122889" Y="-22.585563563135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361874930328" Y="-25.128639684036" />
                  <Point X="-4.206491085655" Y="-22.669528497143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.394749294181" Y="-25.239210123656" />
                  <Point X="-4.099145606173" Y="-22.804296069031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.782860899488" Y="-25.790123836092" />
                  <Point X="4.538664003148" Y="-25.710779454712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.497916575145" Y="-25.37262011655" />
                  <Point X="-3.873358586867" Y="-22.97754763006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.768504169546" Y="-25.885347963063" />
                  <Point X="-3.647573746184" Y="-23.150798483212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.75057741431" Y="-25.9794121185" />
                  <Point X="-3.480471231101" Y="-23.304982292955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729356432031" Y="-26.072405914687" />
                  <Point X="-3.428372467781" Y="-23.421799118609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.248827866175" Y="-26.01616163034" />
                  <Point X="-3.426077675446" Y="-23.52243365314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.731984866055" Y="-25.948118071044" />
                  <Point X="-3.456724194885" Y="-23.612364906656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.331115171749" Y="-25.917756523044" />
                  <Point X="-3.506642784786" Y="-23.696034284892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.151504112727" Y="-25.959286263609" />
                  <Point X="-3.606692796178" Y="-23.763414976885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.047161194733" Y="-26.025272105693" />
                  <Point X="-3.846503950965" Y="-23.785384520621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980827998872" Y="-26.103608055146" />
                  <Point X="-4.363347720774" Y="-23.717340711237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.917633725668" Y="-26.182963902396" />
                  <Point X="-4.645455880401" Y="-23.725567125009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880448359597" Y="-26.27077055585" />
                  <Point X="-4.670333553868" Y="-23.817372790206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868699143225" Y="-26.366841915338" />
                  <Point X="-4.695211227336" Y="-23.909178455403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859774290299" Y="-26.463830966139" />
                  <Point X="-4.720088900803" Y="-24.000984120599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.874001531735" Y="-26.568342588407" />
                  <Point X="-4.739061053052" Y="-24.094708605956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.942198706555" Y="-26.690390105036" />
                  <Point X="-4.753163861007" Y="-24.190015237182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.036173103188" Y="-26.820813148747" />
                  <Point X="-4.767266808926" Y="-24.285321822931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.255828351249" Y="-26.992072376525" />
                  <Point X="-3.368052081124" Y="-24.839843158555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.278689840431" Y="-24.543959014423" />
                  <Point X="-4.781369774048" Y="-24.38062840309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.481612161298" Y="-27.165322894803" />
                  <Point X="-3.30410217703" Y="-24.960510653271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707396713341" Y="-27.33857365417" />
                  <Point X="-3.297075696963" Y="-25.062682606342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.933181265384" Y="-27.511824413536" />
                  <Point X="3.104776482595" Y="-27.242659383155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.457057311576" Y="-27.032202666863" />
                  <Point X="-3.323020244909" Y="-25.154141623007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.07082364907" Y="-27.656436046335" />
                  <Point X="3.500485831193" Y="-27.4711220558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.3270059963" Y="-27.089835344311" />
                  <Point X="-3.380459337837" Y="-25.235367441684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.016468742612" Y="-27.738663977943" />
                  <Point X="3.896196162934" Y="-27.699585047888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.21085919563" Y="-27.151985872422" />
                  <Point X="-3.481765048623" Y="-25.302340132211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.133471064209" Y="-27.226729855571" />
                  <Point X="-3.64564941693" Y="-25.348979784346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086218024381" Y="-27.311265323527" />
                  <Point X="-3.814133601376" Y="-25.394124865619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.041324049913" Y="-27.396567298283" />
                  <Point X="-3.982617785821" Y="-25.439269946891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006376136395" Y="-27.485100944141" />
                  <Point X="-4.151101970266" Y="-25.484415028164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.002857131707" Y="-27.58384646151" />
                  <Point X="-4.319586154711" Y="-25.529560109437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02202139179" Y="-27.689962218377" />
                  <Point X="-4.488070339156" Y="-25.574705190709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.041185605668" Y="-27.796077960231" />
                  <Point X="-3.116399447705" Y="-26.120276991394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.154429442238" Y="-26.107920297122" />
                  <Point X="-4.656553046745" Y="-25.619850751842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.092005741211" Y="-27.912479334537" />
                  <Point X="1.736437003312" Y="-27.796948048229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.303245702057" Y="-27.656195662214" />
                  <Point X="-2.96272665226" Y="-26.270097220712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.416048396469" Y="-26.122804057288" />
                  <Point X="-4.783021079527" Y="-25.67864770835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162993867207" Y="-28.035433686175" />
                  <Point X="1.88715544472" Y="-27.945808349731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.099823611668" Y="-27.689988729701" />
                  <Point X="-2.949299139627" Y="-26.37434899534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.634830024342" Y="-26.151606508521" />
                  <Point X="-4.768038326205" Y="-25.78340481131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.233981993204" Y="-28.158388037812" />
                  <Point X="1.98925951937" Y="-28.078872885953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.001244805548" Y="-27.757847445264" />
                  <Point X="-2.970957356176" Y="-26.467200725501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853610762542" Y="-26.180409248826" />
                  <Point X="-4.753055572883" Y="-25.888161914271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.304970030363" Y="-28.281342360585" />
                  <Point X="2.091363594021" Y="-28.211937422175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.914865052121" Y="-27.829669873322" />
                  <Point X="-3.023947338856" Y="-26.549872147728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.072390957862" Y="-26.209212165523" />
                  <Point X="-4.735767880604" Y="-25.993667937298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375957522093" Y="-28.404296506137" />
                  <Point X="2.193467668671" Y="-28.345001958397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.834975082788" Y="-27.903600960057" />
                  <Point X="-3.110398894947" Y="-26.621671245687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.291171153182" Y="-26.238015082221" />
                  <Point X="-4.707944161739" Y="-26.102597322882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.446945013823" Y="-28.527250651688" />
                  <Point X="2.295571743321" Y="-28.478066494619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.788299184966" Y="-27.988323952818" />
                  <Point X="-3.201851625721" Y="-26.691845363486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.509951348502" Y="-26.266817998918" />
                  <Point X="-4.680119965861" Y="-26.211526863457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.517932505553" Y="-28.65020479724" />
                  <Point X="2.397675817971" Y="-28.611131030841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.76638991091" Y="-28.08109410945" />
                  <Point X="-3.293304356495" Y="-26.762019481286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.588919997283" Y="-28.773158942792" />
                  <Point X="2.499779892621" Y="-28.744195567063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746110581292" Y="-28.174393867133" />
                  <Point X="-3.38475708727" Y="-26.832193599086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.659907489013" Y="-28.896113088344" />
                  <Point X="2.601883971069" Y="-28.877260104519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.726904357975" Y="-28.26804229819" />
                  <Point X="0.459309458468" Y="-28.181095444729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.071968635543" Y="-28.008472727807" />
                  <Point X="-3.476209818044" Y="-26.902367716886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730894980743" Y="-29.019067233896" />
                  <Point X="2.703988055107" Y="-29.010324643792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.729952448651" Y="-28.368921594189" />
                  <Point X="0.54882430104" Y="-28.310069491488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.229204152471" Y="-28.057272722713" />
                  <Point X="-3.567662548818" Y="-26.972541834685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.743690720105" Y="-28.473274340479" />
                  <Point X="0.63390077054" Y="-28.437601423417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.370981708434" Y="-28.111095313599" />
                  <Point X="-2.355147650659" Y="-27.466400718376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.659746047596" Y="-27.36743069977" />
                  <Point X="-3.659115279593" Y="-27.042715952485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.757428991558" Y="-28.577627086769" />
                  <Point X="0.671627873581" Y="-28.54974861358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.449934550259" Y="-28.185330891519" />
                  <Point X="-2.311664035084" Y="-27.580418312842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.774953629438" Y="-27.429886398577" />
                  <Point X="-3.750568010367" Y="-27.112890070285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.771167263011" Y="-28.681979833059" />
                  <Point X="0.7009454511" Y="-28.659163383264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.506505078827" Y="-28.266838923864" />
                  <Point X="-2.322693552004" Y="-27.676723516858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.885662301802" Y="-27.493803881685" />
                  <Point X="-3.842020741142" Y="-27.183064188085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784905534464" Y="-28.786332579349" />
                  <Point X="0.730263028619" Y="-28.768578152948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.563075664606" Y="-28.34834693762" />
                  <Point X="-2.364039570955" Y="-27.763178292242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.996370974166" Y="-27.557721364792" />
                  <Point X="-3.933473471916" Y="-27.253238305884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.798643831355" Y="-28.890685333904" />
                  <Point X="0.759580606138" Y="-28.877992922633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.617985756152" Y="-28.430394478657" />
                  <Point X="-2.412600730577" Y="-27.847288726312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.107079541684" Y="-27.621638881967" />
                  <Point X="-4.024926221255" Y="-27.323412417652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812382140839" Y="-28.995038092551" />
                  <Point X="0.788898183656" Y="-28.987407692317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.651207681407" Y="-28.519488932097" />
                  <Point X="-2.461161890199" Y="-27.931399160381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.217787958318" Y="-27.685556448166" />
                  <Point X="-4.116378984" Y="-27.393586525064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826120450323" Y="-29.099390851198" />
                  <Point X="0.818215761175" Y="-29.096822462001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.675829197765" Y="-28.611377827784" />
                  <Point X="-2.509723049821" Y="-28.015509594451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.328496374951" Y="-27.749474014366" />
                  <Point X="-4.159800558849" Y="-27.479366911457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.700450714122" Y="-28.703266723471" />
                  <Point X="-2.558284209443" Y="-28.09962002852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.439204791585" Y="-27.813391580566" />
                  <Point X="-4.085914044408" Y="-27.603263006587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.725072230479" Y="-28.795155619158" />
                  <Point X="-2.606845369065" Y="-28.18373046259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.549913208218" Y="-27.877309146765" />
                  <Point X="-4.012027529967" Y="-27.727159101718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.749693746836" Y="-28.887044514846" />
                  <Point X="-2.655406528688" Y="-28.267840896659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.660621624852" Y="-27.941226712965" />
                  <Point X="-3.913955442007" Y="-27.858913566049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.77431530073" Y="-28.978933398336" />
                  <Point X="-2.70396768831" Y="-28.351951330729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.771330041486" Y="-28.005144279164" />
                  <Point X="-3.812942974617" Y="-27.991623417572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.798936878463" Y="-29.070822274081" />
                  <Point X="-1.523986529539" Y="-28.8352393617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.646103985897" Y="-28.795560994875" />
                  <Point X="-2.752528847932" Y="-28.436061764798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.823558456197" Y="-29.162711149826" />
                  <Point X="-1.332018002552" Y="-28.997502628477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.999946080814" Y="-28.780479640183" />
                  <Point X="-2.801090007554" Y="-28.520172198868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.84818003393" Y="-29.25460002557" />
                  <Point X="-1.204000694125" Y="-29.138986884747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.129201009617" Y="-28.838371079282" />
                  <Point X="-2.849651167176" Y="-28.604282632937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.872801611664" Y="-29.346488901315" />
                  <Point X="-1.172799335955" Y="-29.249013731868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.229784150423" Y="-28.905578547028" />
                  <Point X="-2.898212129102" Y="-28.688393131242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.897423189397" Y="-29.43837777706" />
                  <Point X="-1.151557422451" Y="-29.355804559254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.330367494312" Y="-28.972785948788" />
                  <Point X="-2.946772955367" Y="-28.772503673626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.922044767131" Y="-29.530266652805" />
                  <Point X="-1.130315496526" Y="-29.462595390675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413552626708" Y="-29.045646372142" />
                  <Point X="-2.913982488467" Y="-28.883046853473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.946666344864" Y="-29.622155528549" />
                  <Point X="-1.120024996056" Y="-29.565827888265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.474903663659" Y="-29.125601123155" />
                  <Point X="-2.659772795979" Y="-29.065533500838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.971287922597" Y="-29.714044404294" />
                  <Point X="-1.132092069342" Y="-29.661795969781" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.755615905762" Y="-29.59730078125" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.348209503174" Y="-28.354251953125" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.274335601807" Y="-28.2663984375" />
                  <Point X="0.094662979126" Y="-28.210634765625" />
                  <Point X="0.020976472855" Y="-28.187765625" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.188337081909" Y="-28.243529296875" />
                  <Point X="-0.262023590088" Y="-28.2663984375" />
                  <Point X="-0.288278839111" Y="-28.285642578125" />
                  <Point X="-0.404388153076" Y="-28.452935546875" />
                  <Point X="-0.452006500244" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.575731506348" Y="-28.971912109375" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-1.014543823242" Y="-29.954701171875" />
                  <Point X="-1.100231689453" Y="-29.938068359375" />
                  <Point X="-1.308484130859" Y="-29.884486328125" />
                  <Point X="-1.351589599609" Y="-29.873396484375" />
                  <Point X="-1.343346435547" Y="-29.810783203125" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.352606933594" Y="-29.318966796875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282348633" Y="-29.20262890625" />
                  <Point X="-1.551702026367" Y="-29.057560546875" />
                  <Point X="-1.619543212891" Y="-28.998064453125" />
                  <Point X="-1.649241333008" Y="-28.985763671875" />
                  <Point X="-1.868790039062" Y="-28.971373046875" />
                  <Point X="-1.958830444336" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.172818115234" Y="-29.09602734375" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.325142333984" Y="-29.242537109375" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.730215820312" Y="-29.245390625" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-3.144182128906" Y="-28.94559375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-3.033958251953" Y="-28.54351171875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513982177734" Y="-27.56876171875" />
                  <Point X="-2.531330810547" Y="-27.5514140625" />
                  <Point X="-2.560159179688" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.966690673828" Y="-27.75998046875" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.062458251953" Y="-27.977517578125" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.368426757812" Y="-27.500486328125" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-4.087483886719" Y="-27.131923828125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145821533203" Y="-26.396013671875" />
                  <Point X="-3.140357666016" Y="-26.37491796875" />
                  <Point X="-3.138116943359" Y="-26.366265625" />
                  <Point X="-3.140326416016" Y="-26.334595703125" />
                  <Point X="-3.161158691406" Y="-26.310640625" />
                  <Point X="-3.179939208984" Y="-26.299587890625" />
                  <Point X="-3.187641357422" Y="-26.2950546875" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.692940185547" Y="-26.350896484375" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.888577148438" Y="-26.163154296875" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.982086914062" Y="-25.628779296875" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.609693847656" Y="-25.41058984375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541897705078" Y="-25.121427734375" />
                  <Point X="-3.522216308594" Y="-25.107767578125" />
                  <Point X="-3.514144775391" Y="-25.102166015625" />
                  <Point X="-3.494899169922" Y="-25.07591015625" />
                  <Point X="-3.488338623047" Y="-25.054771484375" />
                  <Point X="-3.485648193359" Y="-25.046103515625" />
                  <Point X="-3.485647216797" Y="-25.016462890625" />
                  <Point X="-3.492207763672" Y="-24.99532421875" />
                  <Point X="-3.494898193359" Y="-24.986654296875" />
                  <Point X="-3.514145263672" Y="-24.96039453125" />
                  <Point X="-3.533826660156" Y="-24.946736328125" />
                  <Point X="-3.533826904297" Y="-24.946736328125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.989000244141" Y="-24.81828515625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.942660644531" Y="-24.172634765625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.807548339844" Y="-23.59729296875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.507530273438" Y="-23.50671875" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731704589844" Y="-23.6041328125" />
                  <Point X="-3.688143554688" Y="-23.5903984375" />
                  <Point X="-3.670278564453" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.573943359375" />
                  <Point X="-3.639119873047" Y="-23.556212890625" />
                  <Point X="-3.621640869141" Y="-23.514015625" />
                  <Point X="-3.614472412109" Y="-23.496708984375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.63740625" Y="-23.413974609375" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.907498779297" Y="-23.190841796875" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.257216308594" Y="-22.3795234375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.868371826172" Y="-21.83812890625" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.622405029297" Y="-21.805599609375" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138512695312" Y="-22.07956640625" />
                  <Point X="-3.077844238281" Y="-22.084875" />
                  <Point X="-3.052963378906" Y="-22.08705078125" />
                  <Point X="-3.031506103516" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.970189941406" Y="-22.029533203125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.943382080078" Y="-21.9114921875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.061755615234" Y="-21.675982421875" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.922169433594" Y="-20.95546484375" />
                  <Point X="-2.752873779297" Y="-20.82566796875" />
                  <Point X="-2.293547363281" Y="-20.5704765625" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.109640869141" Y="-20.527611328125" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246826172" Y="-20.726337890625" />
                  <Point X="-1.883723266602" Y="-20.761490234375" />
                  <Point X="-1.856030761719" Y="-20.77590625" />
                  <Point X="-1.835124389648" Y="-20.781509765625" />
                  <Point X="-1.813809204102" Y="-20.77775" />
                  <Point X="-1.743478881836" Y="-20.7486171875" />
                  <Point X="-1.714635253906" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.663192016602" Y="-20.63291015625" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.664388061523" Y="-20.486853515625" />
                  <Point X="-1.689137695313" Y="-20.298861328125" />
                  <Point X="-1.187255249023" Y="-20.158150390625" />
                  <Point X="-0.968083251953" Y="-20.096703125" />
                  <Point X="-0.41124798584" Y="-20.031533203125" />
                  <Point X="-0.224199935913" Y="-20.009640625" />
                  <Point X="-0.17918196106" Y="-20.177650390625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282125473" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594043732" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.110655265808" Y="-20.479345703125" />
                  <Point X="0.236648422241" Y="-20.009130859375" />
                  <Point X="0.668844360352" Y="-20.05439453125" />
                  <Point X="0.860210021973" Y="-20.074435546875" />
                  <Point X="1.3208984375" Y="-20.18566015625" />
                  <Point X="1.508455322266" Y="-20.230943359375" />
                  <Point X="1.808864746094" Y="-20.33990234375" />
                  <Point X="1.931045166016" Y="-20.38421875" />
                  <Point X="2.220989990234" Y="-20.51981640625" />
                  <Point X="2.338686279297" Y="-20.574859375" />
                  <Point X="2.618807373047" Y="-20.73805859375" />
                  <Point X="2.732533203125" Y="-20.804314453125" />
                  <Point X="2.996703369141" Y="-20.992177734375" />
                  <Point X="3.068740722656" Y="-21.043408203125" />
                  <Point X="2.84088671875" Y="-21.4380625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.209626708984" Y="-22.565421875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.207981445312" Y="-22.656908203125" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682617188" Y="-22.6991875" />
                  <Point X="2.249146484375" Y="-22.744083984375" />
                  <Point X="2.261640380859" Y="-22.76249609375" />
                  <Point X="2.274939208984" Y="-22.775794921875" />
                  <Point X="2.319835205078" Y="-22.806259765625" />
                  <Point X="2.338247802734" Y="-22.81875390625" />
                  <Point X="2.360334960938" Y="-22.827021484375" />
                  <Point X="2.409568359375" Y="-22.83295703125" />
                  <Point X="2.429759765625" Y="-22.835392578125" />
                  <Point X="2.448664306641" Y="-22.8340546875" />
                  <Point X="2.505600341797" Y="-22.818828125" />
                  <Point X="2.528950683594" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.975076416016" Y="-22.55698828125" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.135139160156" Y="-22.164376953125" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.349868164063" Y="-22.50149609375" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.093098632813" Y="-22.7896171875" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.27937109375" Y="-23.41616796875" />
                  <Point X="3.238394042969" Y="-23.469625" />
                  <Point X="3.221588867188" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.19785546875" Y="-23.563080078125" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.203309570312" Y="-23.66976171875" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646728516" Y="-23.712046875" />
                  <Point X="3.249727294922" Y="-23.76384765625" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280946777344" Y="-23.8011796875" />
                  <Point X="3.330334228516" Y="-23.82898046875" />
                  <Point X="3.350588867188" Y="-23.8403828125" />
                  <Point X="3.368565917969" Y="-23.846380859375" />
                  <Point X="3.435341064453" Y="-23.855205078125" />
                  <Point X="3.4627265625" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.888115234375" Y="-23.804546875" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.910217773438" Y="-23.92961328125" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.985599609375" Y="-24.346705078125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.663122070312" Y="-24.51513671875" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729088378906" Y="-24.767181640625" />
                  <Point X="3.663483886719" Y="-24.8051015625" />
                  <Point X="3.636578369141" Y="-24.82065234375" />
                  <Point X="3.622264892578" Y="-24.833072265625" />
                  <Point X="3.58290234375" Y="-24.88323046875" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.543864013672" Y="-24.99377734375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.551604248047" Y="-25.109197265625" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.606121582031" Y="-25.20891796875" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.636575927734" Y="-25.241908203125" />
                  <Point X="3.702180419922" Y="-25.279828125" />
                  <Point X="3.7290859375" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.119275878906" Y="-25.401701171875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.964607421875" Y="-25.859109375" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.888971679688" Y="-26.2269609375" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.481189941406" Y="-26.238392578125" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836914062" Y="-26.098341796875" />
                  <Point X="3.266078613281" Y="-26.126328125" />
                  <Point X="3.213272705078" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.107619140625" Y="-26.248298828125" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.053203613281" Y="-26.435287109375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.127617431641" Y="-26.62745703125" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.51934765625" Y="-26.954787109375" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.249729492188" Y="-27.728357421875" />
                  <Point X="4.204126953125" Y="-27.802150390625" />
                  <Point X="4.081165283203" Y="-27.976861328125" />
                  <Point X="4.056688476562" Y="-28.011638671875" />
                  <Point X="3.705728271484" Y="-27.80901171875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340332031" Y="-27.253314453125" />
                  <Point X="2.584097412109" Y="-27.225638671875" />
                  <Point X="2.52125" Y="-27.2142890625" />
                  <Point X="2.489077392578" Y="-27.21924609375" />
                  <Point X="2.361770019531" Y="-27.28624609375" />
                  <Point X="2.309559326172" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.221598876953" Y="-27.461990234375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.216838623047" Y="-27.699619140625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.459571777344" Y="-28.16912109375" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.889405761719" Y="-29.151564453125" />
                  <Point X="2.835291503906" Y="-29.190216796875" />
                  <Point X="2.697813232422" Y="-29.279205078125" />
                  <Point X="2.679775634766" Y="-29.290880859375" />
                  <Point X="2.409850097656" Y="-28.93910546875" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670548950195" Y="-27.980466796875" />
                  <Point X="1.51941015625" Y="-27.883298828125" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425806152344" Y="-27.83571875" />
                  <Point X="1.260510009766" Y="-27.8509296875" />
                  <Point X="1.192719360352" Y="-27.857166015625" />
                  <Point X="1.165332275391" Y="-27.868509765625" />
                  <Point X="1.037694946289" Y="-27.97463671875" />
                  <Point X="0.985348632813" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.930293762207" Y="-28.22156640625" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.977828918457" Y="-28.796083984375" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.045533203125" Y="-29.95202734375" />
                  <Point X="0.994362976074" Y="-29.963244140625" />
                  <Point X="0.867338500977" Y="-29.9863203125" />
                  <Point X="0.860200622559" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#188" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.137041589218" Y="4.866570673383" Z="1.8" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.8" />
                  <Point X="-0.4232162988" Y="5.049408199373" Z="1.8" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.8" />
                  <Point X="-1.206908977239" Y="4.921274931679" Z="1.8" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.8" />
                  <Point X="-1.720116201252" Y="4.53790205109" Z="1.8" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.8" />
                  <Point X="-1.717033944948" Y="4.41340553733" Z="1.8" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.8" />
                  <Point X="-1.76876874257" Y="4.328856590757" Z="1.8" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.8" />
                  <Point X="-1.866791580138" Y="4.314140500414" Z="1.8" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.8" />
                  <Point X="-2.076129432762" Y="4.53410731045" Z="1.8" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.8" />
                  <Point X="-2.323986649474" Y="4.504511880731" Z="1.8" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.8" />
                  <Point X="-2.958747626355" Y="4.11549574825" Z="1.8" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.8" />
                  <Point X="-3.111212876491" Y="3.330298416608" Z="1.8" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.8" />
                  <Point X="-2.999347854783" Y="3.115431857963" Z="1.8" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.8" />
                  <Point X="-3.011700760943" Y="3.037102806943" Z="1.8" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.8" />
                  <Point X="-3.079644607643" Y="2.99621684439" Z="1.8" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.8" />
                  <Point X="-3.603560931689" Y="3.268981072254" Z="1.8" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.8" />
                  <Point X="-3.913991130678" Y="3.223854573885" Z="1.8" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.8" />
                  <Point X="-4.306554375152" Y="2.676961467068" Z="1.8" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.8" />
                  <Point X="-3.944092631926" Y="1.800770811944" Z="1.8" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.8" />
                  <Point X="-3.687912792731" Y="1.594218589494" Z="1.8" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.8" />
                  <Point X="-3.673990869698" Y="1.536398243911" Z="1.8" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.8" />
                  <Point X="-3.709335005212" Y="1.488567272391" Z="1.8" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.8" />
                  <Point X="-4.507160041094" Y="1.574133300366" Z="1.8" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.8" />
                  <Point X="-4.861964107776" Y="1.447066511932" Z="1.8" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.8" />
                  <Point X="-4.998278065728" Y="0.865964161236" Z="1.8" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.8" />
                  <Point X="-4.008097212678" Y="0.164698884362" Z="1.8" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.8" />
                  <Point X="-3.568489128795" Y="0.043466875937" Z="1.8" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.8" />
                  <Point X="-3.546117277557" Y="0.021137939978" Z="1.8" />
                  <Point X="-3.539556741714" Y="0" Z="1.8" />
                  <Point X="-3.542247323578" Y="-0.008669011083" Z="1.8" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.8" />
                  <Point X="-3.556879466328" Y="-0.035409107197" Z="1.8" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.8" />
                  <Point X="-4.628790576095" Y="-0.331013172394" Z="1.8" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.8" />
                  <Point X="-5.037738979693" Y="-0.604576451236" Z="1.8" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.8" />
                  <Point X="-4.943177944904" Y="-1.144248336485" Z="1.8" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.8" />
                  <Point X="-3.692569755343" Y="-1.369189019086" Z="1.8" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.8" />
                  <Point X="-3.211456731504" Y="-1.311396462018" Z="1.8" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.8" />
                  <Point X="-3.194917070497" Y="-1.331101485065" Z="1.8" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.8" />
                  <Point X="-4.124077602256" Y="-2.060974623766" Z="1.8" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.8" />
                  <Point X="-4.417526181541" Y="-2.49481559014" Z="1.8" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.8" />
                  <Point X="-4.108400157592" Y="-2.976520808238" Z="1.8" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.8" />
                  <Point X="-2.947846574567" Y="-2.772001403908" Z="1.8" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.8" />
                  <Point X="-2.567793877436" Y="-2.560536591695" Z="1.8" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.8" />
                  <Point X="-3.08341560926" Y="-3.487231807926" Z="1.8" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.8" />
                  <Point X="-3.1808420348" Y="-3.953929603621" Z="1.8" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.8" />
                  <Point X="-2.762693328728" Y="-4.256621709849" Z="1.8" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.8" />
                  <Point X="-2.291630556161" Y="-4.241693878563" Z="1.8" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.8" />
                  <Point X="-2.151195732184" Y="-4.10632097088" Z="1.8" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.8" />
                  <Point X="-1.878215531108" Y="-3.989985938079" Z="1.8" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.8" />
                  <Point X="-1.5908255908" Y="-4.06387120243" Z="1.8" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.8" />
                  <Point X="-1.407802263808" Y="-4.297440276808" Z="1.8" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.8" />
                  <Point X="-1.399074664723" Y="-4.772977509984" Z="1.8" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.8" />
                  <Point X="-1.327098895566" Y="-4.90163034241" Z="1.8" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.8" />
                  <Point X="-1.030227700143" Y="-4.972504379755" Z="1.8" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.8" />
                  <Point X="-0.533591273422" Y="-3.95357379849" Z="1.8" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.8" />
                  <Point X="-0.369468449394" Y="-3.450164142838" Z="1.8" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.8" />
                  <Point X="-0.179672486508" Y="-3.260003077717" Z="1.8" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.8" />
                  <Point X="0.073686592853" Y="-3.227108967571" Z="1.8" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.8" />
                  <Point X="0.300977410038" Y="-3.351481612929" Z="1.8" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.8" />
                  <Point X="0.701163586979" Y="-4.578962231325" Z="1.8" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.8" />
                  <Point X="0.870118512834" Y="-5.004234621558" Z="1.8" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.8" />
                  <Point X="1.050082715524" Y="-4.96958710257" Z="1.8" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.8" />
                  <Point X="1.021245109404" Y="-3.758277202398" Z="1.8" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.8" />
                  <Point X="0.972997038202" Y="-3.20090564684" Z="1.8" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.8" />
                  <Point X="1.063506228561" Y="-2.981801854397" Z="1.8" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.8" />
                  <Point X="1.258934355647" Y="-2.869437286745" Z="1.8" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.8" />
                  <Point X="1.486214973525" Y="-2.894076906603" Z="1.8" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.8" />
                  <Point X="2.364026487802" Y="-3.938263234504" Z="1.8" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.8" />
                  <Point X="2.718826185981" Y="-4.289898333562" Z="1.8" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.8" />
                  <Point X="2.912311586482" Y="-4.160971636765" Z="1.8" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.8" />
                  <Point X="2.496716976143" Y="-3.112841412697" Z="1.8" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.8" />
                  <Point X="2.259886693358" Y="-2.659451186913" Z="1.8" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.8" />
                  <Point X="2.259689274858" Y="-2.453997427147" Z="1.8" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.8" />
                  <Point X="2.378900951225" Y="-2.299212035109" Z="1.8" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.8" />
                  <Point X="2.569055616599" Y="-2.243561316133" Z="1.8" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.8" />
                  <Point X="3.67457218441" Y="-2.821032524132" Z="1.8" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.8" />
                  <Point X="4.115897506061" Y="-2.974357621963" Z="1.8" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.8" />
                  <Point X="4.286107017838" Y="-2.723362173901" Z="1.8" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.8" />
                  <Point X="3.543629592096" Y="-1.883837951537" Z="1.8" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.8" />
                  <Point X="3.16351927233" Y="-1.569137714118" Z="1.8" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.8" />
                  <Point X="3.096837091988" Y="-1.408589383272" Z="1.8" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.8" />
                  <Point X="3.139909266164" Y="-1.248985015917" Z="1.8" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.8" />
                  <Point X="3.270541439722" Y="-1.14390659977" Z="1.8" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.8" />
                  <Point X="4.468507424379" Y="-1.256684260125" Z="1.8" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.8" />
                  <Point X="4.931562720583" Y="-1.20680613053" Z="1.8" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.8" />
                  <Point X="5.007893083845" Y="-0.835282266863" Z="1.8" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.8" />
                  <Point X="4.126060996863" Y="-0.322124445112" Z="1.8" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.8" />
                  <Point X="3.721047227734" Y="-0.205258791835" Z="1.8" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.8" />
                  <Point X="3.639299431535" Y="-0.146767871028" Z="1.8" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.8" />
                  <Point X="3.594555629324" Y="-0.068512462281" Z="1.8" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.8" />
                  <Point X="3.586815821101" Y="0.028098068913" Z="1.8" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.8" />
                  <Point X="3.616080006866" Y="0.117180867547" Z="1.8" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.8" />
                  <Point X="3.682348186619" Y="0.182890052407" Z="1.8" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.8" />
                  <Point X="4.669906820444" Y="0.467847487656" Z="1.8" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.8" />
                  <Point X="5.02884820627" Y="0.692267238837" Z="1.8" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.8" />
                  <Point X="4.95264296259" Y="1.113494159906" Z="1.8" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.8" />
                  <Point X="3.875432802418" Y="1.276305923891" Z="1.8" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.8" />
                  <Point X="3.435735822964" Y="1.225643422629" Z="1.8" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.8" />
                  <Point X="3.348706019666" Y="1.245870164367" Z="1.8" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.8" />
                  <Point X="3.28534156101" Y="1.294915442964" Z="1.8" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.8" />
                  <Point X="3.246122180048" Y="1.37162172554" Z="1.8" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.8" />
                  <Point X="3.239852028991" Y="1.454733449587" Z="1.8" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.8" />
                  <Point X="3.2719212706" Y="1.531237520794" Z="1.8" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.8" />
                  <Point X="4.117380612023" Y="2.201996177078" Z="1.8" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.8" />
                  <Point X="4.386489417528" Y="2.555671089719" Z="1.8" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.8" />
                  <Point X="4.169568521056" Y="2.89610731011" Z="1.8" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.8" />
                  <Point X="2.943920531081" Y="2.517593371819" Z="1.8" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.8" />
                  <Point X="2.486527577653" Y="2.260754582069" Z="1.8" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.8" />
                  <Point X="2.409400226366" Y="2.247963887201" Z="1.8" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.8" />
                  <Point X="2.341754171595" Y="2.266394238804" Z="1.8" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.8" />
                  <Point X="2.284364361431" Y="2.315270688787" Z="1.8" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.8" />
                  <Point X="2.251465815558" Y="2.380358217466" Z="1.8" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.8" />
                  <Point X="2.251773360589" Y="2.452941998229" Z="1.8" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.8" />
                  <Point X="2.878031981041" Y="3.568218550874" Z="1.8" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.8" />
                  <Point X="3.019524676182" Y="4.07984841188" Z="1.8" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.8" />
                  <Point X="2.637821124955" Y="4.336425522934" Z="1.8" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.8" />
                  <Point X="2.236015323952" Y="4.556755254883" Z="1.8" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.8" />
                  <Point X="1.819757761149" Y="4.738381354503" Z="1.8" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.8" />
                  <Point X="1.326477118106" Y="4.894223753694" Z="1.8" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.8" />
                  <Point X="0.667896305416" Y="5.026614317936" Z="1.8" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.8" />
                  <Point X="0.056202972335" Y="4.564876832568" Z="1.8" />
                  <Point X="0" Y="4.355124473572" Z="1.8" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>