<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#169" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2012" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.995282226562" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.768524108887" Y="-29.278423828125" />
                  <Point X="0.563302001953" Y="-28.5125234375" />
                  <Point X="0.557721679688" Y="-28.497142578125" />
                  <Point X="0.542363098145" Y="-28.467375" />
                  <Point X="0.458483581543" Y="-28.346521484375" />
                  <Point X="0.378635437012" Y="-28.231474609375" />
                  <Point X="0.356748931885" Y="-28.209017578125" />
                  <Point X="0.330494049072" Y="-28.189775390625" />
                  <Point X="0.302494659424" Y="-28.175669921875" />
                  <Point X="0.17269593811" Y="-28.13538671875" />
                  <Point X="0.049135520935" Y="-28.097037109375" />
                  <Point X="0.020983409882" Y="-28.092767578125" />
                  <Point X="-0.008658220291" Y="-28.092765625" />
                  <Point X="-0.036824142456" Y="-28.09703515625" />
                  <Point X="-0.166622848511" Y="-28.1373203125" />
                  <Point X="-0.290183258057" Y="-28.17566796875" />
                  <Point X="-0.318184509277" Y="-28.189775390625" />
                  <Point X="-0.344439849854" Y="-28.20901953125" />
                  <Point X="-0.366323608398" Y="-28.2314765625" />
                  <Point X="-0.450203155518" Y="-28.35233203125" />
                  <Point X="-0.530051269531" Y="-28.467376953125" />
                  <Point X="-0.538188781738" Y="-28.481572265625" />
                  <Point X="-0.55099017334" Y="-28.5125234375" />
                  <Point X="-0.746349121094" Y="-29.241615234375" />
                  <Point X="-0.916584655762" Y="-29.876943359375" />
                  <Point X="-0.937439086914" Y="-29.87289453125" />
                  <Point X="-1.079325317383" Y="-29.845353515625" />
                  <Point X="-1.226604248047" Y="-29.807458984375" />
                  <Point X="-1.24641784668" Y="-29.802361328125" />
                  <Point X="-1.239666992188" Y="-29.751083984375" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.247517578125" Y="-29.360333984375" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131205078125" />
                  <Point X="-1.443147094727" Y="-29.026404296875" />
                  <Point X="-1.556905395508" Y="-28.926640625" />
                  <Point X="-1.583188720703" Y="-28.910294921875" />
                  <Point X="-1.612885620117" Y="-28.897994140625" />
                  <Point X="-1.643027709961" Y="-28.890966796875" />
                  <Point X="-1.801633789062" Y="-28.880572265625" />
                  <Point X="-1.952616943359" Y="-28.87067578125" />
                  <Point X="-1.9834140625" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042657348633" Y="-28.89480078125" />
                  <Point X="-2.17481640625" Y="-28.98310546875" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.444781982422" Y="-29.242400390625" />
                  <Point X="-2.480147460938" Y="-29.28848828125" />
                  <Point X="-2.593717041016" Y="-29.218169921875" />
                  <Point X="-2.801724609375" Y="-29.08937890625" />
                  <Point X="-3.005624023438" Y="-28.932380859375" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.805143554688" Y="-28.337193359375" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412859130859" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616126953125" />
                  <Point X="-2.405575195312" Y="-27.5851953125" />
                  <Point X="-2.41455859375" Y="-27.555578125" />
                  <Point X="-2.428774902344" Y="-27.526748046875" />
                  <Point X="-2.446805664062" Y="-27.5015859375" />
                  <Point X="-2.464154296875" Y="-27.48423828125" />
                  <Point X="-2.489310791016" Y="-27.466212890625" />
                  <Point X="-2.518140136719" Y="-27.45199609375" />
                  <Point X="-2.5477578125" Y="-27.44301171875" />
                  <Point X="-2.578691894531" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.268009277344" Y="-27.824251953125" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.91853515625" Y="-28.00975" />
                  <Point X="-4.082862792969" Y="-27.793857421875" />
                  <Point X="-4.229049316406" Y="-27.548724609375" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.773249755859" Y="-27.010548828125" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.359658203125" />
                  <Point X="-3.045556396484" Y="-26.327986328125" />
                  <Point X="-3.052557861328" Y="-26.298240234375" />
                  <Point X="-3.068641357422" Y="-26.272255859375" />
                  <Point X="-3.089473632812" Y="-26.24830078125" />
                  <Point X="-3.112973388672" Y="-26.228767578125" />
                  <Point X="-3.139456054688" Y="-26.213181640625" />
                  <Point X="-3.168715820312" Y="-26.201958984375" />
                  <Point X="-3.200603759766" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.025761962891" Y="-26.29889453125" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.769808105469" Y="-26.244265625" />
                  <Point X="-4.834078125" Y="-25.992650390625" />
                  <Point X="-4.872755371094" Y="-25.72222265625" />
                  <Point X="-4.892423828125" Y="-25.584701171875" />
                  <Point X="-4.293025878906" Y="-25.424091796875" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.510187011719" Y="-25.211046875" />
                  <Point X="-3.480403808594" Y="-25.193958984375" />
                  <Point X="-3.467237548828" Y="-25.184849609375" />
                  <Point X="-3.441882568359" Y="-25.163939453125" />
                  <Point X="-3.425740722656" Y="-25.146859375" />
                  <Point X="-3.414285400391" Y="-25.12633984375" />
                  <Point X="-3.402119384766" Y="-25.096330078125" />
                  <Point X="-3.397581542969" Y="-25.081953125" />
                  <Point X="-3.390822753906" Y="-25.052595703125" />
                  <Point X="-3.388400878906" Y="-25.031193359375" />
                  <Point X="-3.390862060547" Y="-25.009796875" />
                  <Point X="-3.397848388672" Y="-24.979705078125" />
                  <Point X="-3.402419189453" Y="-24.9653203125" />
                  <Point X="-3.414355712891" Y="-24.936046875" />
                  <Point X="-3.4258515625" Y="-24.91555078125" />
                  <Point X="-3.442026611328" Y="-24.89850390625" />
                  <Point X="-3.4680625" Y="-24.877119140625" />
                  <Point X="-3.481255859375" Y="-24.86803125" />
                  <Point X="-3.510360107422" Y="-24.8514140625" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.2564921875" Y="-24.6482578125" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.865907714844" Y="-24.3029375" />
                  <Point X="-4.82448828125" Y="-24.023029296875" />
                  <Point X="-4.746629882812" Y="-23.73570703125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.310560058594" Y="-23.628470703125" />
                  <Point X="-3.765666503906" Y="-23.70020703125" />
                  <Point X="-3.744985107422" Y="-23.700658203125" />
                  <Point X="-3.723423583984" Y="-23.698771484375" />
                  <Point X="-3.703138671875" Y="-23.694736328125" />
                  <Point X="-3.671669433594" Y="-23.684814453125" />
                  <Point X="-3.641712646484" Y="-23.675369140625" />
                  <Point X="-3.622784667969" Y="-23.667041015625" />
                  <Point X="-3.604040283203" Y="-23.656220703125" />
                  <Point X="-3.587355224609" Y="-23.64398828125" />
                  <Point X="-3.573714111328" Y="-23.62843359375" />
                  <Point X="-3.561299560547" Y="-23.610703125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.538724365234" Y="-23.5620859375" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.532050292969" Y="-23.41062109375" />
                  <Point X="-3.547286376953" Y="-23.381353515625" />
                  <Point X="-3.561790039062" Y="-23.3534921875" />
                  <Point X="-3.573281982422" Y="-23.33629296875" />
                  <Point X="-3.587194580078" Y="-23.319712890625" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.017204101562" Y="-22.986916015625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.242094726562" Y="-22.5420703125" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.874912353516" Y="-22.001248046875" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.541315917969" Y="-21.96211328125" />
                  <Point X="-3.206657226562" Y="-22.155328125" />
                  <Point X="-3.187729736328" Y="-22.163658203125" />
                  <Point X="-3.167087890625" Y="-22.17016796875" />
                  <Point X="-3.146793945312" Y="-22.174205078125" />
                  <Point X="-3.102966064453" Y="-22.1780390625" />
                  <Point X="-3.061244628906" Y="-22.181689453125" />
                  <Point X="-3.040561279297" Y="-22.18123828125" />
                  <Point X="-3.019102539062" Y="-22.178412109375" />
                  <Point X="-2.999013427734" Y="-22.173494140625" />
                  <Point X="-2.980464355469" Y="-22.16434765625" />
                  <Point X="-2.962210205078" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.914968505859" Y="-22.108662109375" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.847270263672" Y="-21.920052734375" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.053724121094" Y="-21.499892578125" />
                  <Point X="-3.183332519531" Y="-21.275404296875" />
                  <Point X="-2.980929931641" Y="-21.120224609375" />
                  <Point X="-2.700620117188" Y="-20.9053125" />
                  <Point X="-2.375804443359" Y="-20.724853515625" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.14564453125" Y="-20.636744140625" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028892211914" Y="-20.78519921875" />
                  <Point X="-2.012312744141" Y="-20.799111328125" />
                  <Point X="-1.995114868164" Y="-20.810603515625" />
                  <Point X="-1.946334838867" Y="-20.835998046875" />
                  <Point X="-1.899899047852" Y="-20.860171875" />
                  <Point X="-1.880625854492" Y="-20.86766796875" />
                  <Point X="-1.859719238281" Y="-20.873271484375" />
                  <Point X="-1.839269287109" Y="-20.876419921875" />
                  <Point X="-1.818622558594" Y="-20.87506640625" />
                  <Point X="-1.797307495117" Y="-20.871306640625" />
                  <Point X="-1.777452148438" Y="-20.865517578125" />
                  <Point X="-1.72664440918" Y="-20.844470703125" />
                  <Point X="-1.678278320312" Y="-20.8244375" />
                  <Point X="-1.660147460938" Y="-20.814490234375" />
                  <Point X="-1.642417480469" Y="-20.802076171875" />
                  <Point X="-1.626864990234" Y="-20.7884375" />
                  <Point X="-1.614633300781" Y="-20.771755859375" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.59548034668" Y="-20.734078125" />
                  <Point X="-1.578943237305" Y="-20.68162890625" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.557730224609" Y="-20.569173828125" />
                  <Point X="-1.578641113281" Y="-20.410341796875" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.312506713867" Y="-20.2919296875" />
                  <Point X="-0.949623779297" Y="-20.19019140625" />
                  <Point X="-0.555858520508" Y="-20.14410546875" />
                  <Point X="-0.294711181641" Y="-20.113541015625" />
                  <Point X="-0.232904556274" Y="-20.34420703125" />
                  <Point X="-0.133903366089" Y="-20.713685546875" />
                  <Point X="-0.121129745483" Y="-20.741876953125" />
                  <Point X="-0.103271606445" Y="-20.768603515625" />
                  <Point X="-0.082114006042" Y="-20.791193359375" />
                  <Point X="-0.054818138123" Y="-20.805783203125" />
                  <Point X="-0.024380064011" Y="-20.816115234375" />
                  <Point X="0.006155911922" Y="-20.82115625" />
                  <Point X="0.036691894531" Y="-20.816115234375" />
                  <Point X="0.067129966736" Y="-20.805783203125" />
                  <Point X="0.094425949097" Y="-20.791193359375" />
                  <Point X="0.115583587646" Y="-20.768603515625" />
                  <Point X="0.133441726685" Y="-20.741876953125" />
                  <Point X="0.146215194702" Y="-20.713685546875" />
                  <Point X="0.240458358765" Y="-20.36196484375" />
                  <Point X="0.307419586182" Y="-20.112060546875" />
                  <Point X="0.527194519043" Y="-20.135078125" />
                  <Point X="0.844031005859" Y="-20.168259765625" />
                  <Point X="1.169827880859" Y="-20.24691796875" />
                  <Point X="1.48103918457" Y="-20.3220546875" />
                  <Point X="1.692360229492" Y="-20.398701171875" />
                  <Point X="1.894645507812" Y="-20.472072265625" />
                  <Point X="2.099695800781" Y="-20.567966796875" />
                  <Point X="2.294559082031" Y="-20.659099609375" />
                  <Point X="2.492690429688" Y="-20.774529296875" />
                  <Point X="2.680975585938" Y="-20.884224609375" />
                  <Point X="2.867802734375" Y="-21.0170859375" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.589002441406" Y="-21.684337890625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142075683594" Y="-22.4600703125" />
                  <Point X="2.133076660156" Y="-22.483943359375" />
                  <Point X="2.122077392578" Y="-22.52507421875" />
                  <Point X="2.111606933594" Y="-22.56423046875" />
                  <Point X="2.108618896484" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.112016845703" Y="-22.654615234375" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.140071044922" Y="-22.752529296875" />
                  <Point X="2.162078857422" Y="-22.784962890625" />
                  <Point X="2.183028808594" Y="-22.815837890625" />
                  <Point X="2.19446484375" Y="-22.829671875" />
                  <Point X="2.221597167969" Y="-22.85440625" />
                  <Point X="2.254030761719" Y="-22.8764140625" />
                  <Point X="2.284905761719" Y="-22.897365234375" />
                  <Point X="2.304944580078" Y="-22.907724609375" />
                  <Point X="2.327032226562" Y="-22.9159921875" />
                  <Point X="2.348966308594" Y="-22.921337890625" />
                  <Point X="2.384533447266" Y="-22.925625" />
                  <Point X="2.418391113281" Y="-22.929708984375" />
                  <Point X="2.436466064453" Y="-22.93015625" />
                  <Point X="2.473208251953" Y="-22.925830078125" />
                  <Point X="2.51433984375" Y="-22.914830078125" />
                  <Point X="2.553494628906" Y="-22.904359375" />
                  <Point X="2.565287353516" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.88985546875" />
                  <Point X="3.316352783203" Y="-22.469650390625" />
                  <Point X="3.967326660156" Y="-22.093810546875" />
                  <Point X="4.011585205078" Y="-22.1553203125" />
                  <Point X="4.1232734375" Y="-22.310541015625" />
                  <Point X="4.227424316406" Y="-22.482650390625" />
                  <Point X="4.262198730469" Y="-22.540115234375" />
                  <Point X="3.811956542969" Y="-22.885599609375" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221421630859" Y="-23.33976171875" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.174371337891" Y="-23.3969921875" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.136606933594" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.110603027344" Y="-23.52234375" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.106791503906" Y="-23.672103515625" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679443359" Y="-23.731021484375" />
                  <Point X="3.136282714844" Y="-23.76426171875" />
                  <Point X="3.160903076172" Y="-23.80168359375" />
                  <Point X="3.184340332031" Y="-23.837306640625" />
                  <Point X="3.198896484375" Y="-23.854552734375" />
                  <Point X="3.216139648438" Y="-23.870640625" />
                  <Point X="3.234346191406" Y="-23.88396484375" />
                  <Point X="3.270024414062" Y="-23.904048828125" />
                  <Point X="3.30398828125" Y="-23.92316796875" />
                  <Point X="3.320521484375" Y="-23.930498046875" />
                  <Point X="3.356119873047" Y="-23.9405625" />
                  <Point X="3.404359375" Y="-23.9469375" />
                  <Point X="3.450280517578" Y="-23.953005859375" />
                  <Point X="3.462698486328" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.179583007812" Y="-23.861994140625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.797967285156" Y="-23.870150390625" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.878757324219" Y="-24.277998046875" />
                  <Point X="4.890864257812" Y="-24.355759765625" />
                  <Point X="4.382616210938" Y="-24.4919453125" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704785644531" Y="-24.674416015625" />
                  <Point X="3.681547119141" Y="-24.68493359375" />
                  <Point X="3.634153320312" Y="-24.712328125" />
                  <Point X="3.589037109375" Y="-24.738404296875" />
                  <Point X="3.574316162109" Y="-24.748900390625" />
                  <Point X="3.547530029297" Y="-24.774423828125" />
                  <Point X="3.51909375" Y="-24.81066015625" />
                  <Point X="3.492024169922" Y="-24.84515234375" />
                  <Point X="3.480301513672" Y="-24.8644296875" />
                  <Point X="3.470527099609" Y="-24.88589453125" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.454201904297" Y="-24.956890625" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.454657958984" Y="-25.108048828125" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526855469" Y="-25.1766640625" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.492024658203" Y="-25.217408203125" />
                  <Point X="3.5204609375" Y="-25.253642578125" />
                  <Point X="3.547530517578" Y="-25.28813671875" />
                  <Point X="3.559994628906" Y="-25.301232421875" />
                  <Point X="3.589035644531" Y="-25.32415625" />
                  <Point X="3.6364296875" Y="-25.35155078125" />
                  <Point X="3.681545654297" Y="-25.37762890625" />
                  <Point X="3.692708984375" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.350605957031" Y="-25.5620390625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.881806152344" Y="-25.771078125" />
                  <Point X="4.855023925781" Y="-25.948720703125" />
                  <Point X="4.81297265625" Y="-26.132994140625" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.196796875" Y="-26.105130859375" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.281641845703" Y="-26.0257265625" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163973144531" Y="-26.05659765625" />
                  <Point X="3.136146728516" Y="-26.073490234375" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.056174072266" Y="-26.161580078125" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.961699462891" Y="-26.392935546875" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450927734" Y="-26.567998046875" />
                  <Point X="3.027928222656" Y="-26.64806640625" />
                  <Point X="3.076931152344" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.699008544922" Y="-27.212390625" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.200310546875" Y="-27.627615234375" />
                  <Point X="4.124815429687" Y="-27.749779296875" />
                  <Point X="4.037848144531" Y="-27.873345703125" />
                  <Point X="4.028980224609" Y="-27.8859453125" />
                  <Point X="3.488894287109" Y="-27.574126953125" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786137939453" Y="-27.170015625" />
                  <Point X="2.754224853516" Y="-27.15982421875" />
                  <Point X="2.643519287109" Y="-27.139830078125" />
                  <Point X="2.538134521484" Y="-27.120798828125" />
                  <Point X="2.506777587891" Y="-27.120396484375" />
                  <Point X="2.47460546875" Y="-27.12535546875" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.352864501953" Y="-27.183580078125" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.156129394531" Y="-27.382408203125" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.115668701172" Y="-27.673962890625" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.529912597656" Y="-28.480955078125" />
                  <Point X="2.861283691406" Y="-29.05490625" />
                  <Point X="2.781852539062" Y="-29.111642578125" />
                  <Point X="2.701763916016" Y="-29.16348046875" />
                  <Point X="2.283507568359" Y="-28.6183984375" />
                  <Point X="1.758546020508" Y="-27.934255859375" />
                  <Point X="1.747507080078" Y="-27.92218359375" />
                  <Point X="1.721923583984" Y="-27.900556640625" />
                  <Point X="1.612738037109" Y="-27.830361328125" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986816406" Y="-27.75116796875" />
                  <Point X="1.448366577148" Y="-27.7434375" />
                  <Point X="1.417100585938" Y="-27.741119140625" />
                  <Point X="1.29768737793" Y="-27.752107421875" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.156363891602" Y="-27.769396484375" />
                  <Point X="1.128977661133" Y="-27.780740234375" />
                  <Point X="1.104595092773" Y="-27.795462890625" />
                  <Point X="1.012387573242" Y="-27.872130859375" />
                  <Point X="0.924611572266" Y="-27.945115234375" />
                  <Point X="0.904140808105" Y="-27.968865234375" />
                  <Point X="0.887248779297" Y="-27.99669140625" />
                  <Point X="0.875624267578" Y="-28.025810546875" />
                  <Point X="0.8480546875" Y="-28.15265234375" />
                  <Point X="0.821809997559" Y="-28.273396484375" />
                  <Point X="0.81972454834" Y="-28.289626953125" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="0.926890991211" Y="-29.13699609375" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.975713684082" Y="-29.870076171875" />
                  <Point X="0.929315185547" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058395751953" Y="-29.75264453125" />
                  <Point X="-1.14124609375" Y="-29.731326171875" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.154343017578" Y="-29.34180078125" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124633789" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230575561523" Y="-29.094859375" />
                  <Point X="-1.250210327148" Y="-29.070935546875" />
                  <Point X="-1.261006835938" Y="-29.05978125" />
                  <Point X="-1.380509155273" Y="-28.95498046875" />
                  <Point X="-1.494267578125" Y="-28.855216796875" />
                  <Point X="-1.506735229492" Y="-28.84596875" />
                  <Point X="-1.533018554688" Y="-28.829623046875" />
                  <Point X="-1.546833984375" Y="-28.822525390625" />
                  <Point X="-1.576530883789" Y="-28.810224609375" />
                  <Point X="-1.591315673828" Y="-28.805474609375" />
                  <Point X="-1.621457885742" Y="-28.798447265625" />
                  <Point X="-1.636815063477" Y="-28.796169921875" />
                  <Point X="-1.795421020508" Y="-28.785775390625" />
                  <Point X="-1.946404296875" Y="-28.77587890625" />
                  <Point X="-1.961928466797" Y="-28.7761328125" />
                  <Point X="-1.992725585938" Y="-28.779166015625" />
                  <Point X="-2.007998535156" Y="-28.7819453125" />
                  <Point X="-2.039047607422" Y="-28.790263671875" />
                  <Point X="-2.053667236328" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436035156" Y="-28.815810546875" />
                  <Point X="-2.227595214844" Y="-28.904115234375" />
                  <Point X="-2.353402587891" Y="-28.988177734375" />
                  <Point X="-2.359682373047" Y="-28.992755859375" />
                  <Point X="-2.380450683594" Y="-29.010138671875" />
                  <Point X="-2.402763427734" Y="-29.03277734375" />
                  <Point X="-2.410471923828" Y="-29.041630859375" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.543706542969" Y="-29.1373984375" />
                  <Point X="-2.747608642578" Y="-29.0111484375" />
                  <Point X="-2.947666259766" Y="-28.857109375" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.72287109375" Y="-28.384693359375" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684570312" Y="-27.666185546875" />
                  <Point X="-2.313413574219" Y="-27.63466015625" />
                  <Point X="-2.311638916016" Y="-27.619236328125" />
                  <Point X="-2.310625976562" Y="-27.5883046875" />
                  <Point X="-2.314665039062" Y="-27.55762109375" />
                  <Point X="-2.3236484375" Y="-27.52800390625" />
                  <Point X="-2.329354492188" Y="-27.5135625" />
                  <Point X="-2.343570800781" Y="-27.484732421875" />
                  <Point X="-2.351554199219" Y="-27.471412109375" />
                  <Point X="-2.369584960938" Y="-27.44625" />
                  <Point X="-2.379632324219" Y="-27.434408203125" />
                  <Point X="-2.396980957031" Y="-27.417060546875" />
                  <Point X="-2.408822021484" Y="-27.407015625" />
                  <Point X="-2.433978515625" Y="-27.388990234375" />
                  <Point X="-2.447293945312" Y="-27.381009765625" />
                  <Point X="-2.476123291016" Y="-27.36679296875" />
                  <Point X="-2.490563232422" Y="-27.3610859375" />
                  <Point X="-2.520180908203" Y="-27.3521015625" />
                  <Point X="-2.550869140625" Y="-27.3480625" />
                  <Point X="-2.581803222656" Y="-27.349076171875" />
                  <Point X="-2.597226806641" Y="-27.3508515625" />
                  <Point X="-2.628753417969" Y="-27.357123046875" />
                  <Point X="-2.643684082031" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.315509277344" Y="-27.74198046875" />
                  <Point X="-3.793087890625" Y="-28.0177109375" />
                  <Point X="-3.842941650391" Y="-27.952212890625" />
                  <Point X="-4.004016357422" Y="-27.74059375" />
                  <Point X="-4.147456542969" Y="-27.50006640625" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.715417480469" Y="-27.08591796875" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.3988046875" />
                  <Point X="-2.948748535156" Y="-26.368376953125" />
                  <Point X="-2.948577880859" Y="-26.353048828125" />
                  <Point X="-2.950786621094" Y="-26.321376953125" />
                  <Point X="-2.953083496094" Y="-26.306220703125" />
                  <Point X="-2.960084960938" Y="-26.276474609375" />
                  <Point X="-2.971779785156" Y="-26.248240234375" />
                  <Point X="-2.98786328125" Y="-26.222255859375" />
                  <Point X="-2.996956542969" Y="-26.209916015625" />
                  <Point X="-3.017788818359" Y="-26.1859609375" />
                  <Point X="-3.028747802734" Y="-26.175244140625" />
                  <Point X="-3.052247558594" Y="-26.1557109375" />
                  <Point X="-3.064788330078" Y="-26.14689453125" />
                  <Point X="-3.091270996094" Y="-26.13130859375" />
                  <Point X="-3.105435302734" Y="-26.124482421875" />
                  <Point X="-3.134695068359" Y="-26.113259765625" />
                  <Point X="-3.149790527344" Y="-26.10886328125" />
                  <Point X="-3.181678466797" Y="-26.102380859375" />
                  <Point X="-3.197294677734" Y="-26.10053515625" />
                  <Point X="-3.228619873047" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.038161865234" Y="-26.20470703125" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.677763183594" Y="-26.22075390625" />
                  <Point X="-4.740762695312" Y="-25.974111328125" />
                  <Point X="-4.778712402344" Y="-25.708771484375" />
                  <Point X="-4.786451660156" Y="-25.654658203125" />
                  <Point X="-4.268437988281" Y="-25.51585546875" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.496635986328" Y="-25.3082265625" />
                  <Point X="-3.473947021484" Y="-25.29886328125" />
                  <Point X="-3.46291015625" Y="-25.293447265625" />
                  <Point X="-3.433126953125" Y="-25.276359375" />
                  <Point X="-3.426351806641" Y="-25.272083984375" />
                  <Point X="-3.406794433594" Y="-25.258140625" />
                  <Point X="-3.381439453125" Y="-25.23723046875" />
                  <Point X="-3.372837890625" Y="-25.22919140625" />
                  <Point X="-3.356696044922" Y="-25.212111328125" />
                  <Point X="-3.342791259766" Y="-25.19316796875" />
                  <Point X="-3.3313359375" Y="-25.1726484375" />
                  <Point X="-3.326245117188" Y="-25.16203125" />
                  <Point X="-3.314079101562" Y="-25.132021484375" />
                  <Point X="-3.311524902344" Y="-25.124923828125" />
                  <Point X="-3.305003417969" Y="-25.103267578125" />
                  <Point X="-3.298244628906" Y="-25.07391015625" />
                  <Point X="-3.296425292969" Y="-25.06327734375" />
                  <Point X="-3.294003417969" Y="-25.041875" />
                  <Point X="-3.294023193359" Y="-25.020337890625" />
                  <Point X="-3.296484375" Y="-24.99894140625" />
                  <Point X="-3.298323242188" Y="-24.9883125" />
                  <Point X="-3.305309570312" Y="-24.958220703125" />
                  <Point X="-3.307309326172" Y="-24.950935546875" />
                  <Point X="-3.314451171875" Y="-24.929451171875" />
                  <Point X="-3.326387695312" Y="-24.900177734375" />
                  <Point X="-3.331498779297" Y="-24.88957421875" />
                  <Point X="-3.342994628906" Y="-24.869078125" />
                  <Point X="-3.356937255859" Y="-24.85016015625" />
                  <Point X="-3.373112304688" Y="-24.83311328125" />
                  <Point X="-3.381729492188" Y="-24.825091796875" />
                  <Point X="-3.407765380859" Y="-24.80370703125" />
                  <Point X="-3.414171875" Y="-24.7988828125" />
                  <Point X="-3.434152099609" Y="-24.78553125" />
                  <Point X="-3.463256347656" Y="-24.7689140625" />
                  <Point X="-3.474214111328" Y="-24.76355859375" />
                  <Point X="-3.496729980469" Y="-24.754294921875" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.231904296875" Y="-24.556494140625" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.771931152344" Y="-24.31684375" />
                  <Point X="-4.73133203125" Y="-24.042478515625" />
                  <Point X="-4.654936523438" Y="-23.7605546875" />
                  <Point X="-4.633585449219" Y="-23.681763671875" />
                  <Point X="-4.322959960938" Y="-23.722658203125" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747057128906" Y="-23.795634765625" />
                  <Point X="-3.736703857422" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704888916016" Y="-23.7919453125" />
                  <Point X="-3.684604003906" Y="-23.78791015625" />
                  <Point X="-3.674572509766" Y="-23.78533984375" />
                  <Point X="-3.643103271484" Y="-23.77541796875" />
                  <Point X="-3.613146484375" Y="-23.76597265625" />
                  <Point X="-3.603453125" Y="-23.76232421875" />
                  <Point X="-3.584525146484" Y="-23.75399609375" />
                  <Point X="-3.575290527344" Y="-23.74931640625" />
                  <Point X="-3.556546142578" Y="-23.73849609375" />
                  <Point X="-3.547870605469" Y="-23.7328359375" />
                  <Point X="-3.531185546875" Y="-23.720603515625" />
                  <Point X="-3.515930419922" Y="-23.706626953125" />
                  <Point X="-3.502289306641" Y="-23.691072265625" />
                  <Point X="-3.495893798828" Y="-23.682921875" />
                  <Point X="-3.483479248047" Y="-23.66519140625" />
                  <Point X="-3.478010498047" Y="-23.656396484375" />
                  <Point X="-3.4680625" Y="-23.638263671875" />
                  <Point X="-3.463583251953" Y="-23.62892578125" />
                  <Point X="-3.450956054688" Y="-23.59844140625" />
                  <Point X="-3.438935791016" Y="-23.569421875" />
                  <Point X="-3.435498779297" Y="-23.5596484375" />
                  <Point X="-3.429710205078" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436012207031" Y="-23.39546875" />
                  <Point X="-3.443509521484" Y="-23.376189453125" />
                  <Point X="-3.447784667969" Y="-23.36675390625" />
                  <Point X="-3.463020751953" Y="-23.337486328125" />
                  <Point X="-3.477524414062" Y="-23.309625" />
                  <Point X="-3.482800048828" Y="-23.300712890625" />
                  <Point X="-3.494291992188" Y="-23.283513671875" />
                  <Point X="-3.500508300781" Y="-23.2752265625" />
                  <Point X="-3.514420898438" Y="-23.258646484375" />
                  <Point X="-3.521501708984" Y="-23.251087890625" />
                  <Point X="-3.536442871094" Y="-23.23678515625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.959371582031" Y="-22.911546875" />
                  <Point X="-4.227614746094" Y="-22.70571875" />
                  <Point X="-4.160048339844" Y="-22.589958984375" />
                  <Point X="-4.002294921875" Y="-22.31969140625" />
                  <Point X="-3.799931640625" Y="-22.05958203125" />
                  <Point X="-3.726337158203" Y="-21.964986328125" />
                  <Point X="-3.588816162109" Y="-22.044384765625" />
                  <Point X="-3.254157470703" Y="-22.237599609375" />
                  <Point X="-3.244925048828" Y="-22.242279296875" />
                  <Point X="-3.225997558594" Y="-22.250609375" />
                  <Point X="-3.216302490234" Y="-22.254259765625" />
                  <Point X="-3.195660644531" Y="-22.26076953125" />
                  <Point X="-3.185623291016" Y="-22.263341796875" />
                  <Point X="-3.165329345703" Y="-22.26737890625" />
                  <Point X="-3.155072753906" Y="-22.26884375" />
                  <Point X="-3.111244873047" Y="-22.272677734375" />
                  <Point X="-3.0695234375" Y="-22.276328125" />
                  <Point X="-3.059172851562" Y="-22.276666015625" />
                  <Point X="-3.038489501953" Y="-22.27621484375" />
                  <Point X="-3.028156738281" Y="-22.27542578125" />
                  <Point X="-3.006697998047" Y="-22.272599609375" />
                  <Point X="-2.996512939453" Y="-22.2706875" />
                  <Point X="-2.976423828125" Y="-22.26576953125" />
                  <Point X="-2.956999267578" Y="-22.25869921875" />
                  <Point X="-2.938450195312" Y="-22.249552734375" />
                  <Point X="-2.929421630859" Y="-22.244470703125" />
                  <Point X="-2.911167480469" Y="-22.232841796875" />
                  <Point X="-2.902748291016" Y="-22.22680859375" />
                  <Point X="-2.886615966797" Y="-22.213861328125" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.847793457031" Y="-22.175837890625" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.752631835938" Y="-21.9117734375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-2.971451660156" Y="-21.452392578125" />
                  <Point X="-3.059386474609" Y="-21.3000859375" />
                  <Point X="-2.923127929688" Y="-21.1956171875" />
                  <Point X="-2.648368408203" Y="-20.9849609375" />
                  <Point X="-2.329667236328" Y="-20.8078984375" />
                  <Point X="-2.192524658203" Y="-20.731705078125" />
                  <Point X="-2.118564941406" Y="-20.82808984375" />
                  <Point X="-2.111820068359" Y="-20.835951171875" />
                  <Point X="-2.097516845703" Y="-20.850892578125" />
                  <Point X="-2.089957763672" Y="-20.85797265625" />
                  <Point X="-2.073378417969" Y="-20.871884765625" />
                  <Point X="-2.065094970703" Y="-20.878099609375" />
                  <Point X="-2.047897094727" Y="-20.889591796875" />
                  <Point X="-2.038982666016" Y="-20.894869140625" />
                  <Point X="-1.990202636719" Y="-20.920263671875" />
                  <Point X="-1.943766845703" Y="-20.9444375" />
                  <Point X="-1.934335327148" Y="-20.9487109375" />
                  <Point X="-1.915062011719" Y="-20.95620703125" />
                  <Point X="-1.905220214844" Y="-20.9594296875" />
                  <Point X="-1.884313598633" Y="-20.965033203125" />
                  <Point X="-1.874174926758" Y="-20.967166015625" />
                  <Point X="-1.853724975586" Y="-20.970314453125" />
                  <Point X="-1.83305480957" Y="-20.971216796875" />
                  <Point X="-1.812408081055" Y="-20.96986328125" />
                  <Point X="-1.802120239258" Y="-20.968623046875" />
                  <Point X="-1.780805175781" Y="-20.96486328125" />
                  <Point X="-1.770716186523" Y="-20.962509765625" />
                  <Point X="-1.750860961914" Y="-20.956720703125" />
                  <Point X="-1.741094848633" Y="-20.95328515625" />
                  <Point X="-1.690287231445" Y="-20.93223828125" />
                  <Point X="-1.641921020508" Y="-20.912205078125" />
                  <Point X="-1.632583251953" Y="-20.9077265625" />
                  <Point X="-1.614452392578" Y="-20.897779296875" />
                  <Point X="-1.605659423828" Y="-20.892310546875" />
                  <Point X="-1.587929443359" Y="-20.879896484375" />
                  <Point X="-1.579780883789" Y="-20.873501953125" />
                  <Point X="-1.564228393555" Y="-20.85986328125" />
                  <Point X="-1.550253173828" Y="-20.84461328125" />
                  <Point X="-1.538021484375" Y="-20.827931640625" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.5215390625" Y="-20.80051171875" />
                  <Point X="-1.516856567383" Y="-20.7912734375" />
                  <Point X="-1.508525634766" Y="-20.77233984375" />
                  <Point X="-1.504877197266" Y="-20.76264453125" />
                  <Point X="-1.48833996582" Y="-20.7101953125" />
                  <Point X="-1.47259777832" Y="-20.660267578125" />
                  <Point X="-1.470026611328" Y="-20.650236328125" />
                  <Point X="-1.465991333008" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752807617" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-1.286860839844" Y="-20.38340234375" />
                  <Point X="-0.931164978027" Y="-20.2836796875" />
                  <Point X="-0.544815185547" Y="-20.2384609375" />
                  <Point X="-0.365222412109" Y="-20.21744140625" />
                  <Point X="-0.324667510986" Y="-20.368794921875" />
                  <Point X="-0.225666366577" Y="-20.7382734375" />
                  <Point X="-0.220435150146" Y="-20.752892578125" />
                  <Point X="-0.207661453247" Y="-20.781083984375" />
                  <Point X="-0.200119293213" Y="-20.79465625" />
                  <Point X="-0.182261199951" Y="-20.8213828125" />
                  <Point X="-0.172608886719" Y="-20.833544921875" />
                  <Point X="-0.15145123291" Y="-20.856134765625" />
                  <Point X="-0.126896499634" Y="-20.8749765625" />
                  <Point X="-0.099600570679" Y="-20.88956640625" />
                  <Point X="-0.085354026794" Y="-20.8957421875" />
                  <Point X="-0.054916057587" Y="-20.90607421875" />
                  <Point X="-0.039853721619" Y="-20.909845703125" />
                  <Point X="-0.009317661285" Y="-20.91488671875" />
                  <Point X="0.021629489899" Y="-20.91488671875" />
                  <Point X="0.052165550232" Y="-20.909845703125" />
                  <Point X="0.067227882385" Y="-20.90607421875" />
                  <Point X="0.097665855408" Y="-20.8957421875" />
                  <Point X="0.111912246704" Y="-20.88956640625" />
                  <Point X="0.139208175659" Y="-20.8749765625" />
                  <Point X="0.163763061523" Y="-20.856134765625" />
                  <Point X="0.184920715332" Y="-20.833544921875" />
                  <Point X="0.194573181152" Y="-20.8213828125" />
                  <Point X="0.212431259155" Y="-20.79465625" />
                  <Point X="0.219973724365" Y="-20.781083984375" />
                  <Point X="0.232747116089" Y="-20.752892578125" />
                  <Point X="0.23797819519" Y="-20.7382734375" />
                  <Point X="0.332221313477" Y="-20.386552734375" />
                  <Point X="0.378190795898" Y="-20.2149921875" />
                  <Point X="0.51729901123" Y="-20.229560546875" />
                  <Point X="0.827866333008" Y="-20.2620859375" />
                  <Point X="1.147532348633" Y="-20.339263671875" />
                  <Point X="1.453607177734" Y="-20.41316015625" />
                  <Point X="1.659968261719" Y="-20.4880078125" />
                  <Point X="1.858247680664" Y="-20.55992578125" />
                  <Point X="2.059451171875" Y="-20.654021484375" />
                  <Point X="2.250433837891" Y="-20.74333984375" />
                  <Point X="2.444868164062" Y="-20.856615234375" />
                  <Point X="2.629429443359" Y="-20.964140625" />
                  <Point X="2.81274609375" Y="-21.094505859375" />
                  <Point X="2.817778808594" Y="-21.098083984375" />
                  <Point X="2.506729980469" Y="-21.636837890625" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.062371826172" Y="-22.4068984375" />
                  <Point X="2.053181640625" Y="-22.426560546875" />
                  <Point X="2.044182617188" Y="-22.45043359375" />
                  <Point X="2.041301635742" Y="-22.459400390625" />
                  <Point X="2.030302368164" Y="-22.50053125" />
                  <Point X="2.01983190918" Y="-22.5396875" />
                  <Point X="2.017912231445" Y="-22.54853515625" />
                  <Point X="2.013646484375" Y="-22.57978125" />
                  <Point X="2.012755615234" Y="-22.61676171875" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.017700073242" Y="-22.66598828125" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029143920898" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045318359375" Y="-22.776111328125" />
                  <Point X="2.055680908203" Y="-22.79615625" />
                  <Point X="2.061459960938" Y="-22.80587109375" />
                  <Point X="2.083467773438" Y="-22.8383046875" />
                  <Point X="2.104417724609" Y="-22.8691796875" />
                  <Point X="2.109808105469" Y="-22.8763671875" />
                  <Point X="2.130463867188" Y="-22.899876953125" />
                  <Point X="2.157596191406" Y="-22.924611328125" />
                  <Point X="2.168255615234" Y="-22.933017578125" />
                  <Point X="2.200689208984" Y="-22.955025390625" />
                  <Point X="2.231564208984" Y="-22.9759765625" />
                  <Point X="2.241279052734" Y="-22.981755859375" />
                  <Point X="2.261317871094" Y="-22.992115234375" />
                  <Point X="2.271641845703" Y="-22.9966953125" />
                  <Point X="2.293729492188" Y="-23.004962890625" />
                  <Point X="2.304537597656" Y="-23.008291015625" />
                  <Point X="2.326471679688" Y="-23.01363671875" />
                  <Point X="2.33759765625" Y="-23.015654296875" />
                  <Point X="2.373164794922" Y="-23.01994140625" />
                  <Point X="2.407022460938" Y="-23.024025390625" />
                  <Point X="2.416041015625" Y="-23.0246796875" />
                  <Point X="2.447574951172" Y="-23.02450390625" />
                  <Point X="2.484317138672" Y="-23.020177734375" />
                  <Point X="2.497751953125" Y="-23.01760546875" />
                  <Point X="2.538883544922" Y="-23.00660546875" />
                  <Point X="2.578038330078" Y="-22.996134765625" />
                  <Point X="2.584010253906" Y="-22.99432421875" />
                  <Point X="2.604405029297" Y="-22.986931640625" />
                  <Point X="2.627651611328" Y="-22.976427734375" />
                  <Point X="2.636033935547" Y="-22.97212890625" />
                  <Point X="3.363852783203" Y="-22.551923828125" />
                  <Point X="3.940405029297" Y="-22.21905078125" />
                  <Point X="4.043959960938" Y="-22.36296875" />
                  <Point X="4.136884277344" Y="-22.51652734375" />
                  <Point X="3.754124023438" Y="-22.81023046875" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168135986328" Y="-23.2601328125" />
                  <Point X="3.152114990234" Y="-23.274787109375" />
                  <Point X="3.134667236328" Y="-23.2933984375" />
                  <Point X="3.128576171875" Y="-23.300578125" />
                  <Point X="3.098973632812" Y="-23.339197265625" />
                  <Point X="3.070793945312" Y="-23.375958984375" />
                  <Point X="3.065634033203" Y="-23.383400390625" />
                  <Point X="3.049740722656" Y="-23.410626953125" />
                  <Point X="3.034763671875" Y="-23.444453125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.01911328125" Y="-23.4967578125" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.013751464844" Y="-23.69130078125" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034682617188" Y="-23.771388671875" />
                  <Point X="3.050285888672" Y="-23.80462890625" />
                  <Point X="3.056918701172" Y="-23.8164765625" />
                  <Point X="3.0815390625" Y="-23.8538984375" />
                  <Point X="3.104976318359" Y="-23.889521484375" />
                  <Point X="3.111742431641" Y="-23.89858203125" />
                  <Point X="3.126298583984" Y="-23.915828125" />
                  <Point X="3.134088623047" Y="-23.924013671875" />
                  <Point X="3.151331787109" Y="-23.9401015625" />
                  <Point X="3.160034667969" Y="-23.9473046875" />
                  <Point X="3.178241210938" Y="-23.96062890625" />
                  <Point X="3.187744873047" Y="-23.96675" />
                  <Point X="3.223423095703" Y="-23.986833984375" />
                  <Point X="3.257386962891" Y="-24.005953125" />
                  <Point X="3.265484130859" Y="-24.010015625" />
                  <Point X="3.294676025391" Y="-24.0219140625" />
                  <Point X="3.330274414062" Y="-24.031978515625" />
                  <Point X="3.343673583984" Y="-24.034744140625" />
                  <Point X="3.391913085938" Y="-24.041119140625" />
                  <Point X="3.437834228516" Y="-24.0471875" />
                  <Point X="3.444033447266" Y="-24.04780078125" />
                  <Point X="3.465708740234" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.191982910156" Y="-23.956181640625" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.705663085938" Y="-23.89262109375" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.358028320312" Y="-24.400181640625" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686027587891" Y="-24.58045703125" />
                  <Point X="3.665614501953" Y="-24.5878671875" />
                  <Point X="3.642375976562" Y="-24.598384765625" />
                  <Point X="3.634005859375" Y="-24.602685546875" />
                  <Point X="3.586612060547" Y="-24.630080078125" />
                  <Point X="3.541495849609" Y="-24.65615625" />
                  <Point X="3.533885253906" Y="-24.661052734375" />
                  <Point X="3.508781738281" Y="-24.680123046875" />
                  <Point X="3.481995605469" Y="-24.705646484375" />
                  <Point X="3.472794677734" Y="-24.715775390625" />
                  <Point X="3.444358398438" Y="-24.75201171875" />
                  <Point X="3.417288818359" Y="-24.78650390625" />
                  <Point X="3.410854003906" Y="-24.79579296875" />
                  <Point X="3.399131347656" Y="-24.8150703125" />
                  <Point X="3.393843505859" Y="-24.82505859375" />
                  <Point X="3.384069091797" Y="-24.8465234375" />
                  <Point X="3.380005126953" Y="-24.857072265625" />
                  <Point X="3.373158691406" Y="-24.87857421875" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.360897460938" Y="-24.939021484375" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.99503515625" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.361353759766" Y="-25.12591796875" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373158935547" Y="-25.183986328125" />
                  <Point X="3.380004638672" Y="-25.205486328125" />
                  <Point X="3.384068359375" Y="-25.216033203125" />
                  <Point X="3.393841552734" Y="-25.23749609375" />
                  <Point X="3.399129150391" Y="-25.247486328125" />
                  <Point X="3.410853759766" Y="-25.266767578125" />
                  <Point X="3.417290771484" Y="-25.27605859375" />
                  <Point X="3.445727050781" Y="-25.31229296875" />
                  <Point X="3.472796630859" Y="-25.346787109375" />
                  <Point X="3.478716552734" Y="-25.3536328125" />
                  <Point X="3.501133544922" Y="-25.37580078125" />
                  <Point X="3.530174560547" Y="-25.398724609375" />
                  <Point X="3.541494628906" Y="-25.406404296875" />
                  <Point X="3.588888671875" Y="-25.433798828125" />
                  <Point X="3.634004638672" Y="-25.459876953125" />
                  <Point X="3.639487792969" Y="-25.4628125" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026855469" Y="-25.481029296875" />
                  <Point X="3.691991943359" Y="-25.483916015625" />
                  <Point X="4.326018066406" Y="-25.653802734375" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761614746094" Y="-25.931048828125" />
                  <Point X="4.727801757812" Y="-26.07921875" />
                  <Point X="4.209196777344" Y="-26.010943359375" />
                  <Point X="3.436782226562" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354482421875" Y="-25.912677734375" />
                  <Point X="3.261465087891" Y="-25.93289453125" />
                  <Point X="3.172918212891" Y="-25.952140625" />
                  <Point X="3.157874755859" Y="-25.9567421875" />
                  <Point X="3.128752929688" Y="-25.9683671875" />
                  <Point X="3.114674560547" Y="-25.975390625" />
                  <Point X="3.086848144531" Y="-25.992283203125" />
                  <Point X="3.074122802734" Y="-26.00153125" />
                  <Point X="3.050373291016" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.983125976562" Y="-26.10084375" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606201172" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.867099121094" Y="-26.38423046875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876787109375" Y="-26.576671875" />
                  <Point X="2.889158935547" Y="-26.605482421875" />
                  <Point X="2.896541259766" Y="-26.619373046875" />
                  <Point X="2.948018554688" Y="-26.69944140625" />
                  <Point X="2.997021484375" Y="-26.775662109375" />
                  <Point X="3.001740966797" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.641176269531" Y="-27.287759765625" />
                  <Point X="4.087169921875" Y="-27.629984375" />
                  <Point X="4.045491943359" Y="-27.69742578125" />
                  <Point X="4.0012734375" Y="-27.76025390625" />
                  <Point X="3.536394287109" Y="-27.49185546875" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841201416016" Y="-27.090892578125" />
                  <Point X="2.815038085938" Y="-27.079517578125" />
                  <Point X="2.783125" Y="-27.069326171875" />
                  <Point X="2.771109375" Y="-27.0663359375" />
                  <Point X="2.660403808594" Y="-27.046341796875" />
                  <Point X="2.555019042969" Y="-27.027310546875" />
                  <Point X="2.539353271484" Y="-27.025806640625" />
                  <Point X="2.507996337891" Y="-27.025404296875" />
                  <Point X="2.492305175781" Y="-27.026505859375" />
                  <Point X="2.460133056641" Y="-27.03146484375" />
                  <Point X="2.444841064453" Y="-27.035138671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400589599609" Y="-27.051109375" />
                  <Point X="2.308620361328" Y="-27.09951171875" />
                  <Point X="2.221071533203" Y="-27.145587890625" />
                  <Point X="2.208967773438" Y="-27.153171875" />
                  <Point X="2.186038330078" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144941650391" Y="-27.21116015625" />
                  <Point X="2.128047363281" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.072061523438" Y="-27.3381640625" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.022181030273" Y="-27.690845703125" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.447640136719" Y="-28.528455078125" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.72375390625" Y="-29.036083984375" />
                  <Point X="2.358876220703" Y="-28.56056640625" />
                  <Point X="1.833914672852" Y="-27.876423828125" />
                  <Point X="1.828654541016" Y="-27.8701484375" />
                  <Point X="1.808837524414" Y="-27.8496328125" />
                  <Point X="1.78325402832" Y="-27.828005859375" />
                  <Point X="1.773297973633" Y="-27.820646484375" />
                  <Point X="1.664112304688" Y="-27.750451171875" />
                  <Point X="1.560174560547" Y="-27.68362890625" />
                  <Point X="1.546280151367" Y="-27.676244140625" />
                  <Point X="1.517466796875" Y="-27.663873046875" />
                  <Point X="1.502547851562" Y="-27.65888671875" />
                  <Point X="1.470927612305" Y="-27.65115625" />
                  <Point X="1.455391479492" Y="-27.648697265625" />
                  <Point X="1.424125488281" Y="-27.64637890625" />
                  <Point X="1.408395507812" Y="-27.64651953125" />
                  <Point X="1.288982177734" Y="-27.6575078125" />
                  <Point X="1.17530859375" Y="-27.667966796875" />
                  <Point X="1.161231445312" Y="-27.670337890625" />
                  <Point X="1.133581665039" Y="-27.67716796875" />
                  <Point X="1.120008911133" Y="-27.68162890625" />
                  <Point X="1.092622680664" Y="-27.69297265625" />
                  <Point X="1.079872558594" Y="-27.699416015625" />
                  <Point X="1.055489868164" Y="-27.714138671875" />
                  <Point X="1.043857910156" Y="-27.722416015625" />
                  <Point X="0.95165032959" Y="-27.799083984375" />
                  <Point X="0.863874389648" Y="-27.872068359375" />
                  <Point X="0.852652587891" Y="-27.883091796875" />
                  <Point X="0.832181884766" Y="-27.906841796875" />
                  <Point X="0.822932800293" Y="-27.919568359375" />
                  <Point X="0.806040771484" Y="-27.94739453125" />
                  <Point X="0.799019348145" Y="-27.961470703125" />
                  <Point X="0.787394836426" Y="-27.99058984375" />
                  <Point X="0.782791809082" Y="-28.0056328125" />
                  <Point X="0.755222290039" Y="-28.132474609375" />
                  <Point X="0.728977539063" Y="-28.25321875" />
                  <Point X="0.727584655762" Y="-28.2612890625" />
                  <Point X="0.72472454834" Y="-28.289677734375" />
                  <Point X="0.724742248535" Y="-28.323169921875" />
                  <Point X="0.725555053711" Y="-28.33551953125" />
                  <Point X="0.832703735352" Y="-29.149396484375" />
                  <Point X="0.833091125488" Y="-29.15233984375" />
                  <Point X="0.655064880371" Y="-28.487935546875" />
                  <Point X="0.652606079102" Y="-28.480123046875" />
                  <Point X="0.642146728516" Y="-28.453583984375" />
                  <Point X="0.626788146973" Y="-28.42381640625" />
                  <Point X="0.620407348633" Y="-28.41320703125" />
                  <Point X="0.536527832031" Y="-28.292353515625" />
                  <Point X="0.456679626465" Y="-28.177306640625" />
                  <Point X="0.446669281006" Y="-28.165169921875" />
                  <Point X="0.424782775879" Y="-28.142712890625" />
                  <Point X="0.412906890869" Y="-28.132392578125" />
                  <Point X="0.386651916504" Y="-28.113150390625" />
                  <Point X="0.373235595703" Y="-28.10493359375" />
                  <Point X="0.345236236572" Y="-28.090828125" />
                  <Point X="0.330653045654" Y="-28.084939453125" />
                  <Point X="0.200854431152" Y="-28.04465625" />
                  <Point X="0.077293968201" Y="-28.006306640625" />
                  <Point X="0.063380336761" Y="-28.003111328125" />
                  <Point X="0.035228187561" Y="-27.998841796875" />
                  <Point X="0.020989667892" Y="-27.997767578125" />
                  <Point X="-0.008651978493" Y="-27.997765625" />
                  <Point X="-0.022896144867" Y="-27.998837890625" />
                  <Point X="-0.051062114716" Y="-28.003107421875" />
                  <Point X="-0.064983917236" Y="-28.0063046875" />
                  <Point X="-0.194782531738" Y="-28.04658984375" />
                  <Point X="-0.318342987061" Y="-28.0849375" />
                  <Point X="-0.332927215576" Y="-28.090828125" />
                  <Point X="-0.360928527832" Y="-28.104935546875" />
                  <Point X="-0.374345458984" Y="-28.11315234375" />
                  <Point X="-0.400600860596" Y="-28.132396484375" />
                  <Point X="-0.412477783203" Y="-28.14271875" />
                  <Point X="-0.434361602783" Y="-28.16517578125" />
                  <Point X="-0.444368225098" Y="-28.177310546875" />
                  <Point X="-0.52824786377" Y="-28.298166015625" />
                  <Point X="-0.608095825195" Y="-28.4132109375" />
                  <Point X="-0.612469482422" Y="-28.420130859375" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777832031" Y="-28.47621484375" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.838112182617" Y="-29.21702734375" />
                  <Point X="-0.985425292969" Y="-29.76680859375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.15080688597" Y="-20.786071939887" />
                  <Point X="-3.03658986825" Y="-21.339570577956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.359898322031" Y="-20.403879366438" />
                  <Point X="-1.474263683724" Y="-20.47534277588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.088524367918" Y="-20.859175451515" />
                  <Point X="-2.989060702594" Y="-21.421893007335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.034734389896" Y="-20.31271633922" />
                  <Point X="-1.462471585953" Y="-20.579996203707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.999373178603" Y="-20.915489553946" />
                  <Point X="-2.941531636124" Y="-21.504215498693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.701691102768" Y="-21.979215851962" />
                  <Point X="-3.771197605855" Y="-22.0226483355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.780836146975" Y="-20.266085057034" />
                  <Point X="-1.487549614424" Y="-20.707688643425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.894862322576" Y="-20.962205871392" />
                  <Point X="-2.894002628028" Y="-21.586538026526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.608512199642" Y="-22.033013159474" />
                  <Point X="-3.940801876898" Y="-22.240650794747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.560245983532" Y="-20.240266972885" />
                  <Point X="-2.846473619932" Y="-21.66886055436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.515332925417" Y="-22.086810235097" />
                  <Point X="-4.06790422619" Y="-22.432095105695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.362242504205" Y="-20.228562615402" />
                  <Point X="-2.798944611836" Y="-21.751183082193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422153551724" Y="-22.140607248565" />
                  <Point X="-4.17083118837" Y="-22.608432958165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.336531316815" Y="-20.32451843072" />
                  <Point X="-2.761697131929" Y="-21.839930221883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.32897417803" Y="-22.194404262034" />
                  <Point X="-4.191543150756" Y="-22.733397176998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.310820124209" Y="-20.420474242779" />
                  <Point X="-2.749768757998" Y="-21.944498494915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234007235242" Y="-22.24708427836" />
                  <Point X="-4.111078511335" Y="-22.79513923823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.285108927134" Y="-20.516430052046" />
                  <Point X="-2.765860779702" Y="-22.066575854407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.097602888858" Y="-22.273871331157" />
                  <Point X="-4.030613871914" Y="-22.856881299463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.25939773006" Y="-20.612385861312" />
                  <Point X="-3.950149299954" Y="-22.91862340285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.530338067551" Y="-20.230926113599" />
                  <Point X="0.342465166494" Y="-20.348322131524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.233686532985" Y="-20.708341670579" />
                  <Point X="-3.869685249126" Y="-22.980365831876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.683877278754" Y="-20.247006114521" />
                  <Point X="0.306412633551" Y="-20.482872202738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.197888663754" Y="-20.797994627552" />
                  <Point X="-3.789221198297" Y="-23.042108260903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.835909463806" Y="-20.264027809898" />
                  <Point X="0.270360114129" Y="-20.617422265503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.133591486252" Y="-20.869839240236" />
                  <Point X="-3.708757147469" Y="-23.103850689929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.965220004456" Y="-20.295247564487" />
                  <Point X="0.232712451464" Y="-20.752969084393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.022837996823" Y="-20.912654727394" />
                  <Point X="-3.628293096641" Y="-23.165593118955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.485531733477" Y="-23.701255270387" />
                  <Point X="-4.670123997629" Y="-23.816601318855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.094530545105" Y="-20.326467319075" />
                  <Point X="-3.547829045812" Y="-23.227335547982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.337456709149" Y="-23.720749674221" />
                  <Point X="-4.706667390884" Y="-23.951458113634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.223841056163" Y="-20.357687092155" />
                  <Point X="-3.483738073601" Y="-23.299309012033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.189381648781" Y="-23.740244055534" />
                  <Point X="-4.737269334159" Y="-24.082602278415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.353151546666" Y="-20.388906878078" />
                  <Point X="-3.440379739986" Y="-23.384237666526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.041306584501" Y="-23.759738434403" />
                  <Point X="-4.755534651834" Y="-24.206037663952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.478918782213" Y="-20.42234073543" />
                  <Point X="-3.421465334156" Y="-23.484440582333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.893231520221" Y="-23.779232813272" />
                  <Point X="-4.773799985494" Y="-24.329473059479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.592350490238" Y="-20.46348268587" />
                  <Point X="-3.46014894524" Y="-23.620634733639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73983007953" Y="-23.795398902764" />
                  <Point X="-4.739973683998" Y="-24.420357988705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.705781835235" Y="-20.504624863155" />
                  <Point X="-4.614503503495" Y="-24.453977466649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.819212644432" Y="-20.545767375245" />
                  <Point X="-4.489033322991" Y="-24.487596944593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.925496629898" Y="-20.591375718448" />
                  <Point X="-4.363563142487" Y="-24.521216422537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.028030717861" Y="-20.639327257673" />
                  <Point X="-4.238092961984" Y="-24.554835900481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.130563929253" Y="-20.687279344641" />
                  <Point X="-4.112622983859" Y="-24.588455504886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.233096753346" Y="-20.73523167362" />
                  <Point X="-3.987153016234" Y="-24.622075115851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.327521655611" Y="-20.788250394457" />
                  <Point X="-3.861683048609" Y="-24.655694726817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420296619429" Y="-20.842300111262" />
                  <Point X="-3.736213080984" Y="-24.689314337782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.513070991618" Y="-20.896350197757" />
                  <Point X="-3.610743113359" Y="-24.722933948748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.605845150662" Y="-20.950400417441" />
                  <Point X="-3.487642185809" Y="-24.758033900249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69196205258" Y="-21.008610553071" />
                  <Point X="-3.396400460532" Y="-24.813041690828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.710849086346" Y="-25.634400351758" />
                  <Point X="-4.782909036556" Y="-25.679428406145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.775809739236" Y="-21.06823865177" />
                  <Point X="-3.333602645612" Y="-24.885823209237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.396989565876" Y="-25.550301104931" />
                  <Point X="-4.768201815017" Y="-25.782260262473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.767244019276" Y="-21.18561305597" />
                  <Point X="-3.300860160515" Y="-24.977385382114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.083131377661" Y="-25.46620269059" />
                  <Point X="-4.753494500222" Y="-25.885092060528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.66606688221" Y="-21.360857496356" />
                  <Point X="-3.301972470447" Y="-25.09010237882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.769274113657" Y="-25.382104853758" />
                  <Point X="-4.73744828432" Y="-25.987087220316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.564889745144" Y="-21.536101936741" />
                  <Point X="-4.712773092263" Y="-26.083690397367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.463712432855" Y="-21.711346486619" />
                  <Point X="-4.688097900206" Y="-26.180293574417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.362534883662" Y="-21.88659118453" />
                  <Point X="-4.663422374304" Y="-26.276896542858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.261357334469" Y="-22.061835882441" />
                  <Point X="-4.456829184253" Y="-26.259824738402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.160179785277" Y="-22.237080580353" />
                  <Point X="-4.229705021836" Y="-26.229923758548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.060178976707" Y="-22.411589969114" />
                  <Point X="-4.002580736099" Y="-26.200022701637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017690149042" Y="-22.550161883639" />
                  <Point X="-3.775455786496" Y="-26.170121229895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017272759651" Y="-22.662444645797" />
                  <Point X="-3.548330836893" Y="-26.140219758154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038922569472" Y="-22.760938291484" />
                  <Point X="-3.32120588729" Y="-26.110318286412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.08671782357" Y="-22.843094450351" />
                  <Point X="-3.142842982886" Y="-26.110886722252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148624669826" Y="-22.916432707772" />
                  <Point X="-3.044976362866" Y="-26.161754819146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.232098507547" Y="-22.976294413213" />
                  <Point X="-2.980796571289" Y="-26.233672782697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.346638217635" Y="-23.016744007122" />
                  <Point X="-2.950414701669" Y="-26.326710031837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544569003621" Y="-23.00508507348" />
                  <Point X="-2.962897566207" Y="-26.44653213963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953231659708" Y="-22.236876900831" />
                  <Point X="-3.314333455162" Y="-26.778155604119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008835515838" Y="-22.314153703607" />
                  <Point X="-4.100690396701" Y="-27.381547904867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.062076691242" Y="-22.392906873157" />
                  <Point X="-4.137582966836" Y="-27.516622889577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.111265609023" Y="-22.474192174301" />
                  <Point X="-4.088914105159" Y="-27.598233157842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.760087211868" Y="-22.805654740055" />
                  <Point X="-4.040245243483" Y="-27.679843426107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.111347802349" Y="-23.323054062759" />
                  <Point X="-3.989246753689" Y="-27.759997981161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020484812533" Y="-23.491853508637" />
                  <Point X="-3.931463698216" Y="-27.835913069056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001350205785" Y="-23.615832086274" />
                  <Point X="-3.873680642743" Y="-27.91182815695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019016009491" Y="-23.716815215281" />
                  <Point X="-3.815897706983" Y="-27.98774331965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052222220889" Y="-23.808087619804" />
                  <Point X="-2.626799778041" Y="-27.356734415755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.10386836755" Y="-23.887837473932" />
                  <Point X="-2.469137390261" Y="-27.370237970001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.172902697318" Y="-23.956721985349" />
                  <Point X="-2.384603516048" Y="-27.429437291127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.266333563815" Y="-24.010361848672" />
                  <Point X="-2.331852461774" Y="-27.50849672235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.395603706753" Y="-24.041606846552" />
                  <Point X="-2.311259515868" Y="-27.607650769907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583355080856" Y="-24.036308715416" />
                  <Point X="-2.354533505091" Y="-27.746713307827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.810479997096" Y="-24.006407264522" />
                  <Point X="-2.455710935123" Y="-27.921957931278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.037604913336" Y="-23.976505813628" />
                  <Point X="-2.556888365156" Y="-28.09720255473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.264730187414" Y="-23.946604139133" />
                  <Point X="-2.658065795188" Y="-28.272447178182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.491856220868" Y="-23.916701990126" />
                  <Point X="-2.759243241636" Y="-28.447691811891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.706191662858" Y="-23.894792289718" />
                  <Point X="-2.860420717332" Y="-28.622936463876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729862465461" Y="-23.992023078955" />
                  <Point X="3.865448994745" Y="-24.532168564183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392696714157" Y="-24.827576975368" />
                  <Point X="-2.961598193028" Y="-28.798181115861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.753253979038" Y="-24.089428387346" />
                  <Point X="4.179307390446" Y="-24.44807002019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356449165103" Y="-24.962248906173" />
                  <Point X="-2.915843283289" Y="-28.881612223386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.769148627477" Y="-24.191518256996" />
                  <Point X="4.493166475128" Y="-24.363971045673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351995418882" Y="-25.077053864007" />
                  <Point X="-2.835531763859" Y="-28.943449964607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.371375751257" Y="-25.176965636596" />
                  <Point X="-2.755220244428" Y="-29.005287705829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.409774790912" Y="-25.264993201892" />
                  <Point X="-2.666095959744" Y="-29.061618620139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.467977425687" Y="-25.34064610754" />
                  <Point X="-2.080099099858" Y="-28.807469090401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.124122639691" Y="-28.834978051204" />
                  <Point X="-2.576048988178" Y="-29.117372975695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.541764905506" Y="-25.406560521166" />
                  <Point X="-1.859398308554" Y="-28.781581878292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.634924904776" Y="-25.460369641118" />
                  <Point X="-1.697144105426" Y="-28.792216147858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751148573965" Y="-25.499766980594" />
                  <Point X="-1.558586186991" Y="-28.817657499483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.876618595466" Y="-25.533386557894" />
                  <Point X="3.224368493518" Y="-25.940957656381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.998945768935" Y="-26.081817408197" />
                  <Point X="-0.09656240847" Y="-28.016105596842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.513156991872" Y="-28.276422784181" />
                  <Point X="-1.471447738359" Y="-28.87522930188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.002088616966" Y="-25.567006135195" />
                  <Point X="3.451315690174" Y="-25.911167257009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.881292040987" Y="-26.267357565249" />
                  <Point X="0.091390216561" Y="-28.010681710169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.633605501044" Y="-28.463709314365" />
                  <Point X="-1.396858317906" Y="-28.940642607381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.127558638467" Y="-25.600625712495" />
                  <Point X="3.599390873542" Y="-25.930661561463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866706851168" Y="-26.388493351678" />
                  <Point X="0.211169187256" Y="-28.047857450698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.672848455243" Y="-28.600252982042" />
                  <Point X="-1.322268844436" Y="-29.006055879754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.253028659967" Y="-25.634245289795" />
                  <Point X="3.74746605691" Y="-25.950155865918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860835850547" Y="-26.504183908351" />
                  <Point X="0.330923438772" Y="-28.085048637484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.708900926441" Y="-28.734803014674" />
                  <Point X="-1.249062869392" Y="-29.072333657892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.378498886371" Y="-25.667864739058" />
                  <Point X="3.895541240277" Y="-25.969650170372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.886797474007" Y="-26.599983233844" />
                  <Point X="1.178073641225" Y="-27.667712387827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.902702474505" Y="-27.839783390309" />
                  <Point X="0.421909641614" Y="-28.140216096201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.744953397638" Y="-28.869353047305" />
                  <Point X="-1.197933989118" Y="-29.152406735931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.503969397751" Y="-25.701484010248" />
                  <Point X="4.043616423645" Y="-25.989144474827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.9362350246" Y="-26.681113171964" />
                  <Point X="1.388303059304" Y="-27.648368415919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.777484615193" Y="-28.030050141225" />
                  <Point X="0.482436021676" Y="-28.214416964637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.781005868836" Y="-29.003903079937" />
                  <Point X="-1.172850826516" Y="-29.248754984691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.629439909131" Y="-25.735103281437" />
                  <Point X="4.191691607013" Y="-26.008638779281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.987614257716" Y="-26.761029812185" />
                  <Point X="2.560028315707" Y="-27.028215162654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.076008643713" Y="-27.330664221404" />
                  <Point X="1.532460654385" Y="-27.670310701227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749309426506" Y="-28.159677901439" />
                  <Point X="0.536665842019" Y="-28.292552360265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.817058340034" Y="-29.138453112568" />
                  <Point X="-1.153031817192" Y="-29.348392641499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.754910420511" Y="-25.768722552627" />
                  <Point X="4.339766683386" Y="-26.028133150593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.050160183666" Y="-26.833968728291" />
                  <Point X="2.699106081696" Y="-27.053331677475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006173660753" Y="-27.486323910266" />
                  <Point X="1.624370580487" Y="-27.724900953589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725005763818" Y="-28.286886463711" />
                  <Point X="0.590896209347" Y="-28.370687414098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.853110821342" Y="-29.273003151518" />
                  <Point X="-1.133212700684" Y="-29.44803023133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770684818314" Y="-25.870887563214" />
                  <Point X="4.487841745416" Y="-26.047627530868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.130417508435" Y="-26.895840334097" />
                  <Point X="2.827677958432" Y="-27.085013000504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.005546480499" Y="-27.598737764304" />
                  <Point X="1.712731884503" Y="-27.781708631134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733224242021" Y="-28.393772936882" />
                  <Point X="0.641015455212" Y="-28.451391381735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.889163316843" Y="-29.407553199335" />
                  <Point X="-1.119696211772" Y="-29.551606139983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.74630315283" Y="-25.998144867043" />
                  <Point X="4.635916807445" Y="-26.067121911144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.210881645748" Y="-26.957582709081" />
                  <Point X="2.922799157233" Y="-27.137596626976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.023725897544" Y="-27.699399952077" />
                  <Point X="1.797984908844" Y="-27.840458577385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746851153094" Y="-28.497279846111" />
                  <Point X="0.67037354184" Y="-28.545068361491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.925215812344" Y="-29.542103247153" />
                  <Point X="-1.133466152202" Y="-29.672232502055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.291345783061" Y="-27.019325084066" />
                  <Point X="3.015978488176" Y="-27.191393667158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.042055899709" Y="-27.799968043823" />
                  <Point X="1.861716897477" Y="-27.912656359271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.760478064167" Y="-28.60078675534" />
                  <Point X="0.69608476806" Y="-28.641024152545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.961268307844" Y="-29.67665329497" />
                  <Point X="-1.075738307435" Y="-29.748182089428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.371809920375" Y="-27.08106745905" />
                  <Point X="3.109157819119" Y="-27.245190707339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.078554933533" Y="-27.889182864531" />
                  <Point X="1.919816749469" Y="-27.988373490731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77410497524" Y="-28.704293664568" />
                  <Point X="0.72179599428" Y="-28.7369799436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.452274057688" Y="-27.142809834035" />
                  <Point X="3.202337150063" Y="-27.298987747521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.126084002207" Y="-27.971505354512" />
                  <Point X="1.97791660146" Y="-28.064090622191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.787731886313" Y="-28.807800573797" />
                  <Point X="0.7475072205" Y="-28.832935734654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.532738195001" Y="-27.204552209019" />
                  <Point X="3.295516481006" Y="-27.352784787702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.173613070881" Y="-28.053827844492" />
                  <Point X="2.036016453451" Y="-28.13980775365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.801358797386" Y="-28.911307483026" />
                  <Point X="0.773218446721" Y="-28.928891525708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613202332314" Y="-27.266294584004" />
                  <Point X="3.388695811949" Y="-27.406581827884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.221142139555" Y="-28.136150334472" />
                  <Point X="2.094116305442" Y="-28.21552488511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814985708458" Y="-29.014814392254" />
                  <Point X="0.798929672941" Y="-29.024847316763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.693666363897" Y="-27.328037025056" />
                  <Point X="3.481875142892" Y="-27.460378868066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.268671208228" Y="-28.218472824453" />
                  <Point X="2.152216157433" Y="-28.29124201657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.828612619531" Y="-29.118321301483" />
                  <Point X="0.824640899161" Y="-29.120803107817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.774130339131" Y="-27.389779501319" />
                  <Point X="3.575054425953" Y="-27.514175938167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.316200276902" Y="-28.300795314433" />
                  <Point X="2.210316009425" Y="-28.366959148029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854594314365" Y="-27.451521977582" />
                  <Point X="3.668233641489" Y="-27.567973050463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.363729345576" Y="-28.383117804413" />
                  <Point X="2.268415861416" Y="-28.442676279489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.935058289599" Y="-27.513264453845" />
                  <Point X="3.761412857026" Y="-27.621770162759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.41125841425" Y="-28.465440294394" />
                  <Point X="2.326515713407" Y="-28.518393410948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.015522264833" Y="-27.575006930108" />
                  <Point X="3.854592072562" Y="-27.675567275054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.458787456437" Y="-28.547762800925" />
                  <Point X="2.384615559589" Y="-28.594110546038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.074812867013" Y="-27.649979998268" />
                  <Point X="3.947771288099" Y="-27.72936438735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.506316412178" Y="-28.630085361473" />
                  <Point X="2.442715398467" Y="-28.669827685691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.55384536792" Y="-28.712407922021" />
                  <Point X="2.500815237346" Y="-28.745544825345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601374323662" Y="-28.794730482569" />
                  <Point X="2.558915076224" Y="-28.821261964998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.648903279403" Y="-28.877053043118" />
                  <Point X="2.617014915103" Y="-28.896979104652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.696432235145" Y="-28.959375603666" />
                  <Point X="2.675114753981" Y="-28.972696244305" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998374023438" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.676761230469" Y="-29.30301171875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.380439300537" Y="-28.400689453125" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.274336212158" Y="-28.266400390625" />
                  <Point X="0.144537506104" Y="-28.2261171875" />
                  <Point X="0.020977081299" Y="-28.187767578125" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.138463165283" Y="-28.22805078125" />
                  <Point X="-0.262023590088" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.372158508301" Y="-28.406498046875" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.654586181641" Y="-29.266203125" />
                  <Point X="-0.84774407959" Y="-29.987078125" />
                  <Point X="-0.955541137695" Y="-29.966154296875" />
                  <Point X="-1.100231323242" Y="-29.938068359375" />
                  <Point X="-1.250276123047" Y="-29.899462890625" />
                  <Point X="-1.351589599609" Y="-29.873396484375" />
                  <Point X="-1.333854248047" Y="-29.73868359375" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.340692138672" Y="-29.3788671875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.505784912109" Y="-29.097828125" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240356445" Y="-28.985763671875" />
                  <Point X="-1.807846435547" Y="-28.975369140625" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989878662109" Y="-28.973791015625" />
                  <Point X="-2.122037597656" Y="-29.062095703125" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.369413085938" Y="-29.300232421875" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.643728027344" Y="-29.29894140625" />
                  <Point X="-2.855837890625" Y="-29.167609375" />
                  <Point X="-3.063581542969" Y="-29.00765234375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.887416015625" Y="-28.289693359375" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.59759375" />
                  <Point X="-2.513979003906" Y="-27.568763671875" />
                  <Point X="-2.531327636719" Y="-27.551416015625" />
                  <Point X="-2.560156982422" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.220509277344" Y="-27.9065234375" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.994128417969" Y="-28.0672890625" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.310642089844" Y="-27.5973828125" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.83108203125" Y="-26.9351796875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140326171875" Y="-26.334595703125" />
                  <Point X="-3.161158447266" Y="-26.310640625" />
                  <Point X="-3.187641113281" Y="-26.2950546875" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.013362060547" Y="-26.39308203125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.861853027344" Y="-26.26777734375" />
                  <Point X="-4.927393066406" Y="-26.01119140625" />
                  <Point X="-4.966798339844" Y="-25.735671875" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-4.317613769531" Y="-25.332328125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.527680664063" Y="-25.11155859375" />
                  <Point X="-3.502325683594" Y="-25.0906484375" />
                  <Point X="-3.490159667969" Y="-25.060638671875" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.490387207031" Y="-25.001189453125" />
                  <Point X="-3.502323730469" Y="-24.971916015625" />
                  <Point X="-3.528359619141" Y="-24.95053125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.281080078125" Y="-24.740021484375" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.959884277344" Y="-24.28903125" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.838322753906" Y="-23.710859375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.29816015625" Y="-23.534283203125" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731704833984" Y="-23.6041328125" />
                  <Point X="-3.700235595703" Y="-23.5942109375" />
                  <Point X="-3.670278808594" Y="-23.584765625" />
                  <Point X="-3.651534423828" Y="-23.5739453125" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.626492675781" Y="-23.52573046875" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.631552001953" Y="-23.425220703125" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.075036621094" Y="-23.06228515625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.324141113281" Y="-22.494181640625" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.949893066406" Y="-21.9429140625" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.493815673828" Y="-21.879841796875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138515136719" Y="-22.07956640625" />
                  <Point X="-3.094687255859" Y="-22.083400390625" />
                  <Point X="-3.052965820312" Y="-22.08705078125" />
                  <Point X="-3.031507080078" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.982143554688" Y="-22.041486328125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.941908691406" Y="-21.92833203125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.135996582031" Y="-21.547392578125" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.038732177734" Y="-21.04483203125" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.421941650391" Y="-20.64180859375" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.070275878906" Y="-20.578912109375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247192383" Y="-20.726337890625" />
                  <Point X="-1.902467041016" Y="-20.751732421875" />
                  <Point X="-1.85603125" Y="-20.77590625" />
                  <Point X="-1.835124755859" Y="-20.781509765625" />
                  <Point X="-1.813809448242" Y="-20.77775" />
                  <Point X="-1.763001708984" Y="-20.756703125" />
                  <Point X="-1.714635498047" Y="-20.736669921875" />
                  <Point X="-1.696905517578" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.669546386719" Y="-20.6530625" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.672828369141" Y="-20.4227421875" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.338152587891" Y="-20.20045703125" />
                  <Point X="-0.968083068848" Y="-20.096703125" />
                  <Point X="-0.566901611328" Y="-20.04975" />
                  <Point X="-0.224199981689" Y="-20.009640625" />
                  <Point X="-0.14114163208" Y="-20.319619140625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282121658" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594039917" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.148695449829" Y="-20.337376953125" />
                  <Point X="0.236648422241" Y="-20.009130859375" />
                  <Point X="0.537089599609" Y="-20.040595703125" />
                  <Point X="0.860210266113" Y="-20.074435546875" />
                  <Point X="1.192123413086" Y="-20.1545703125" />
                  <Point X="1.508455566406" Y="-20.230943359375" />
                  <Point X="1.724752075195" Y="-20.30939453125" />
                  <Point X="1.931044311523" Y="-20.38421875" />
                  <Point X="2.139940429688" Y="-20.481912109375" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.540512939453" Y="-20.692443359375" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.922859375" Y="-20.939666015625" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.671274902344" Y="-21.731837890625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224851806641" Y="-22.508486328125" />
                  <Point X="2.213852539062" Y="-22.5496171875" />
                  <Point X="2.203382080078" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.206333496094" Y="-22.6432421875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.240689941406" Y="-22.73162109375" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.274938720703" Y="-22.775794921875" />
                  <Point X="2.307372314453" Y="-22.797802734375" />
                  <Point X="2.338247314453" Y="-22.81875390625" />
                  <Point X="2.360334960938" Y="-22.827021484375" />
                  <Point X="2.395902099609" Y="-22.83130859375" />
                  <Point X="2.429759765625" Y="-22.835392578125" />
                  <Point X="2.448664550781" Y="-22.8340546875" />
                  <Point X="2.489796142578" Y="-22.8230546875" />
                  <Point X="2.528950927734" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.268852783203" Y="-22.387376953125" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.088697753906" Y="-22.099833984375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.308701171875" Y="-22.433466796875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.869788818359" Y="-22.96096875" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371582031" Y="-23.41616796875" />
                  <Point X="3.249769042969" Y="-23.454787109375" />
                  <Point X="3.221589355469" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.202092773438" Y="-23.5479296875" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.199831542969" Y="-23.65290625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646728516" Y="-23.712046875" />
                  <Point X="3.240267089844" Y="-23.74946875" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280947509766" Y="-23.8011796875" />
                  <Point X="3.316625732422" Y="-23.821263671875" />
                  <Point X="3.350589599609" Y="-23.8403828125" />
                  <Point X="3.368566162109" Y="-23.846380859375" />
                  <Point X="3.416805664062" Y="-23.852755859375" />
                  <Point X="3.462726806641" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.167183105469" Y="-23.767806640625" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.890271484375" Y="-23.8476796875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.972626464844" Y="-24.2633828125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.407204101562" Y="-24.583708984375" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729088378906" Y="-24.767181640625" />
                  <Point X="3.681694580078" Y="-24.794576171875" />
                  <Point X="3.636578369141" Y="-24.82065234375" />
                  <Point X="3.622265380859" Y="-24.833072265625" />
                  <Point X="3.593829101562" Y="-24.86930859375" />
                  <Point X="3.566759521484" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.547506347656" Y="-24.974759765625" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.547962158203" Y="-25.0901796875" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758544922" Y="-25.1587578125" />
                  <Point X="3.595194824219" Y="-25.1949921875" />
                  <Point X="3.622264404297" Y="-25.229486328125" />
                  <Point X="3.636576660156" Y="-25.241908203125" />
                  <Point X="3.683970703125" Y="-25.269302734375" />
                  <Point X="3.729086669922" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.375193847656" Y="-25.470275390625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.975744628906" Y="-25.785240234375" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.905591796875" Y="-26.15412890625" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.184396972656" Y="-26.199318359375" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.3948359375" Y="-26.098341796875" />
                  <Point X="3.301818603516" Y="-26.11855859375" />
                  <Point X="3.213271728516" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.129222167969" Y="-26.22231640625" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.056299804688" Y="-26.401640625" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.107837890625" Y="-26.59669140625" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.756840820312" Y="-27.137021484375" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.281124023438" Y="-27.677556640625" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.115536132812" Y="-27.9280234375" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="3.441394287109" Y="-27.6563984375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340332031" Y="-27.2533125" />
                  <Point X="2.626634765625" Y="-27.233318359375" />
                  <Point X="2.52125" Y="-27.214287109375" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.397108642578" Y="-27.2676484375" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.240197265625" Y="-27.42665234375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.20915625" Y="-27.657080078125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.612185058594" Y="-28.433455078125" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.926657714844" Y="-29.12495703125" />
                  <Point X="2.835297851563" Y="-29.190212890625" />
                  <Point X="2.736243164062" Y="-29.254328125" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="2.208138916016" Y="-28.67623046875" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670549194336" Y="-27.980466796875" />
                  <Point X="1.561363647461" Y="-27.910271484375" />
                  <Point X="1.45742578125" Y="-27.84344921875" />
                  <Point X="1.425805541992" Y="-27.83571875" />
                  <Point X="1.306392456055" Y="-27.84670703125" />
                  <Point X="1.19271862793" Y="-27.857166015625" />
                  <Point X="1.165332275391" Y="-27.868509765625" />
                  <Point X="1.073124755859" Y="-27.945177734375" />
                  <Point X="0.985348754883" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.940887145996" Y="-28.172830078125" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.021078308105" Y="-29.124595703125" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.080778076172" Y="-29.944302734375" />
                  <Point X="0.99436730957" Y="-29.9632421875" />
                  <Point X="0.902838500977" Y="-29.97987109375" />
                  <Point X="0.860200500488" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#168" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.099001320624" Y="4.724602451013" Z="1.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.3" />
                  <Point X="-0.578869750749" Y="5.03119123621" Z="1.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.3" />
                  <Point X="-1.357806397926" Y="4.878968586704" Z="1.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.3" />
                  <Point X="-1.728556571075" Y="4.60201308957" Z="1.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.3" />
                  <Point X="-1.723388128706" Y="4.393252690368" Z="1.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.3" />
                  <Point X="-1.788291164805" Y="4.320770070718" Z="1.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.3" />
                  <Point X="-1.885534918028" Y="4.323897733978" Z="1.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.3" />
                  <Point X="-2.036764365853" Y="4.48280573272" Z="1.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.3" />
                  <Point X="-2.452380594318" Y="4.43317901197" Z="1.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.3" />
                  <Point X="-3.075310283697" Y="4.026128437953" Z="1.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.3" />
                  <Point X="-3.185453935701" Y="3.458887695732" Z="1.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.3" />
                  <Point X="-2.997874493648" Y="3.098591433586" Z="1.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.3" />
                  <Point X="-3.023654214502" Y="3.025149353384" Z="1.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.3" />
                  <Point X="-3.096485032021" Y="2.997690205525" Z="1.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.3" />
                  <Point X="-3.474971652565" Y="3.194740013045" Z="1.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.3" />
                  <Point X="-3.995512592293" Y="3.119070216922" Z="1.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.3" />
                  <Point X="-4.373479316101" Y="2.562303013891" Z="1.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.3" />
                  <Point X="-4.111630402498" Y="1.929327050382" Z="1.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.3" />
                  <Point X="-3.682058490139" Y="1.582972577373" Z="1.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.3" />
                  <Point X="-3.678842734788" Y="1.52468480465" Z="1.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.3" />
                  <Point X="-3.721426769708" Y="1.484754757443" Z="1.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.3" />
                  <Point X="-4.297789993677" Y="1.546569202375" Z="1.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.3" />
                  <Point X="-4.892738690327" Y="1.333498870703" Z="1.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.3" />
                  <Point X="-5.015501743284" Y="0.749568191571" Z="1.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.3" />
                  <Point X="-4.300177200798" Y="0.242961482507" Z="1.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.3" />
                  <Point X="-3.56302596409" Y="0.039675127649" Z="1.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.3" />
                  <Point X="-3.544296191367" Y="0.015270429838" Z="1.3" />
                  <Point X="-3.539556741714" Y="0" Z="1.3" />
                  <Point X="-3.544068409768" Y="-0.014536521223" Z="1.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.3" />
                  <Point X="-3.562342631033" Y="-0.039200855485" Z="1.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.3" />
                  <Point X="-4.336710587974" Y="-0.252750574249" Z="1.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.3" />
                  <Point X="-5.022450528877" Y="-0.711471686779" Z="1.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.3" />
                  <Point X="-4.916453850021" Y="-1.248872171566" Z="1.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.3" />
                  <Point X="-4.012991893278" Y="-1.411373385736" Z="1.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.3" />
                  <Point X="-3.206243624714" Y="-1.31446466982" Z="1.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.3" />
                  <Point X="-3.196433703667" Y="-1.33695726604" Z="1.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.3" />
                  <Point X="-3.867676063311" Y="-1.864230811479" Z="1.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.3" />
                  <Point X="-4.359741589539" Y="-2.591711530379" Z="1.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.3" />
                  <Point X="-4.040070236586" Y="-3.066292256805" Z="1.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.3" />
                  <Point X="-3.201665355042" Y="-2.918543743106" Z="1.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.3" />
                  <Point X="-2.564378812836" Y="-2.563951656295" Z="1.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.3" />
                  <Point X="-2.936873270062" Y="-3.23341302745" Z="1.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.3" />
                  <Point X="-3.10024153536" Y="-4.015989303634" Z="1.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.3" />
                  <Point X="-2.676205579876" Y="-4.310172776294" Z="1.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.3" />
                  <Point X="-2.335901320073" Y="-4.299388641202" Z="1.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.3" />
                  <Point X="-2.100414967645" Y="-4.072390298874" Z="1.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.3" />
                  <Point X="-1.817272784609" Y="-3.993980341062" Z="1.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.3" />
                  <Point X="-1.54490809602" Y="-4.104139819448" Z="1.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.3" />
                  <Point X="-1.39588741247" Y="-4.357340223605" Z="1.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.3" />
                  <Point X="-1.389582437169" Y="-4.700876914613" Z="1.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.3" />
                  <Point X="-1.26889078205" Y="-4.916606784025" Z="1.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.3" />
                  <Point X="-0.971224990995" Y="-4.983957013492" Z="1.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.3" />
                  <Point X="-0.612445861271" Y="-4.247863147788" Z="1.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.3" />
                  <Point X="-0.337238586566" Y="-3.40372705512" Z="1.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.3" />
                  <Point X="-0.129798651239" Y="-3.244524079766" Z="1.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.3" />
                  <Point X="0.123560428122" Y="-3.242587965522" Z="1.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.3" />
                  <Point X="0.333207272867" Y="-3.397918700648" Z="1.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.3" />
                  <Point X="0.62230899913" Y="-4.284672882028" Z="1.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.3" />
                  <Point X="0.905618922463" Y="-4.997785492762" Z="1.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.3" />
                  <Point X="1.085327364905" Y="-4.961861495701" Z="1.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.3" />
                  <Point X="1.064494557099" Y="-4.086789333399" Z="1.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.3" />
                  <Point X="0.983590391186" Y="-3.152167913664" Z="1.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.3" />
                  <Point X="1.098936098204" Y="-2.952343079598" Z="1.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.3" />
                  <Point X="1.304817566023" Y="-2.86521507199" Z="1.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.3" />
                  <Point X="1.528168407372" Y="-2.921049088223" Z="1.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.3" />
                  <Point X="2.162315308824" Y="-3.675388161711" Z="1.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.3" />
                  <Point X="2.757256680205" Y="-4.265023015216" Z="1.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.3" />
                  <Point X="2.949563363639" Y="-4.134363559744" Z="1.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.3" />
                  <Point X="2.649330306633" Y="-3.377175344392" Z="1.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.3" />
                  <Point X="2.252204452388" Y="-2.616913697938" Z="1.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.3" />
                  <Point X="2.278287520022" Y="-2.418659240712" Z="1.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.3" />
                  <Point X="2.41423913766" Y="-2.280613789945" Z="1.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.3" />
                  <Point X="2.611593105575" Y="-2.251243557102" Z="1.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.3" />
                  <Point X="3.410238252715" Y="-2.668419193642" Z="1.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.3" />
                  <Point X="4.150269017781" Y="-2.9255204382" Z="1.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.3" />
                  <Point X="4.317501958032" Y="-2.672560430404" Z="1.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.3" />
                  <Point X="3.781122847854" Y="-2.066072960546" Z="1.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.3" />
                  <Point X="3.143739681861" Y="-1.538371861065" Z="1.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.3" />
                  <Point X="3.099933342133" Y="-1.374941692535" Z="1.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.3" />
                  <Point X="3.16151243026" Y="-1.223003116205" Z="1.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.3" />
                  <Point X="3.306282450309" Y="-1.136138143262" Z="1.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.3" />
                  <Point X="4.171714726896" Y="-1.217610763186" Z="1.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.3" />
                  <Point X="4.948183124032" Y="-1.133973254541" Z="1.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.3" />
                  <Point X="5.019030235355" Y="-0.761411894577" Z="1.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.3" />
                  <Point X="4.381978799522" Y="-0.390697410515" Z="1.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.3" />
                  <Point X="3.702836600498" Y="-0.194732722875" Z="1.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.3" />
                  <Point X="3.628373055193" Y="-0.132845099923" Z="1.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.3" />
                  <Point X="3.590913503876" Y="-0.049494640899" Z="1.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.3" />
                  <Point X="3.590457946548" Y="0.047115890311" Z="1.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.3" />
                  <Point X="3.627006383207" Y="0.131103638652" Z="1.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.3" />
                  <Point X="3.700558813855" Y="0.193416121367" Z="1.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.3" />
                  <Point X="4.413989017784" Y="0.399274522254" Z="1.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.3" />
                  <Point X="5.015875325539" Y="0.775589934071" Z="1.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.3" />
                  <Point X="4.932696619912" Y="1.195427738366" Z="1.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.3" />
                  <Point X="4.154500533195" Y="1.313045898952" Z="1.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.3" />
                  <Point X="3.417200233077" Y="1.228093126633" Z="1.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.3" />
                  <Point X="3.334996951162" Y="1.253587182535" Z="1.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.3" />
                  <Point X="3.275881412019" Y="1.309294453606" Z="1.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.3" />
                  <Point X="3.242644037776" Y="1.388478576394" Z="1.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.3" />
                  <Point X="3.244089027575" Y="1.469883941461" Z="1.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.3" />
                  <Point X="3.283295714306" Y="1.54607637893" Z="1.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.3" />
                  <Point X="3.89407082143" Y="2.030644553973" Z="1.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.3" />
                  <Point X="4.345322506873" Y="2.623699843" Z="1.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.3" />
                  <Point X="4.123127021654" Y="2.960650458805" Z="1.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.3" />
                  <Point X="3.237696764165" Y="2.687205149396" Z="1.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.3" />
                  <Point X="2.470723194656" Y="2.256528284161" Z="1.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.3" />
                  <Point X="2.395733913635" Y="2.249611791389" Z="1.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.3" />
                  <Point X="2.329291825774" Y="2.274850448444" Z="1.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.3" />
                  <Point X="2.275908151791" Y="2.327733034608" Z="1.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.3" />
                  <Point X="2.24981791137" Y="2.394024530197" Z="1.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.3" />
                  <Point X="2.255999658497" Y="2.468746381226" Z="1.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.3" />
                  <Point X="2.708420203463" Y="3.27444231779" Z="1.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.3" />
                  <Point X="2.945680433728" Y="4.13236235749" Z="1.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.3" />
                  <Point X="2.559526809042" Y="4.382039972677" Z="1.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.3" />
                  <Point X="2.154965818492" Y="4.594659397847" Z="1.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.3" />
                  <Point X="1.735644835032" Y="4.76888973426" Z="1.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.3" />
                  <Point X="1.197701738729" Y="4.925314100069" Z="1.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.3" />
                  <Point X="0.536141559079" Y="5.040412558745" Z="1.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.3" />
                  <Point X="0.094243240929" Y="4.706845054938" Z="1.3" />
                  <Point X="0" Y="4.355124473572" Z="1.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>