<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#177" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2280" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.800066101074" Y="-29.396140625" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557720153809" Y="-28.497138671875" />
                  <Point X="0.542363037109" Y="-28.467376953125" />
                  <Point X="0.445591644287" Y="-28.32794921875" />
                  <Point X="0.378635375977" Y="-28.2314765625" />
                  <Point X="0.356752960205" Y="-28.2090234375" />
                  <Point X="0.330498077393" Y="-28.189779296875" />
                  <Point X="0.302494873047" Y="-28.175669921875" />
                  <Point X="0.152746505737" Y="-28.1291953125" />
                  <Point X="0.049135734558" Y="-28.097037109375" />
                  <Point X="0.020983469009" Y="-28.092767578125" />
                  <Point X="-0.008658161163" Y="-28.092765625" />
                  <Point X="-0.036824085236" Y="-28.09703515625" />
                  <Point X="-0.186572158813" Y="-28.14351171875" />
                  <Point X="-0.290183227539" Y="-28.17566796875" />
                  <Point X="-0.318184448242" Y="-28.189775390625" />
                  <Point X="-0.344439941406" Y="-28.20901953125" />
                  <Point X="-0.366323425293" Y="-28.2314765625" />
                  <Point X="-0.463094970703" Y="-28.37090625" />
                  <Point X="-0.530051208496" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.714807189941" Y="-29.1238984375" />
                  <Point X="-0.916584777832" Y="-29.87694140625" />
                  <Point X="-0.961041259766" Y="-29.8683125" />
                  <Point X="-1.079320922852" Y="-29.845353515625" />
                  <Point X="-1.24641784668" Y="-29.802361328125" />
                  <Point X="-1.243463867188" Y="-29.779923828125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.252283569336" Y="-29.336373046875" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131205078125" />
                  <Point X="-1.461513793945" Y="-29.010296875" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583188720703" Y="-28.910294921875" />
                  <Point X="-1.612885620117" Y="-28.897994140625" />
                  <Point X="-1.643027709961" Y="-28.890966796875" />
                  <Point X="-1.826010864258" Y="-28.878974609375" />
                  <Point X="-1.952616943359" Y="-28.87067578125" />
                  <Point X="-1.9834140625" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042657348633" Y="-28.89480078125" />
                  <Point X="-2.19512890625" Y="-28.996677734375" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312791259766" Y="-29.076826171875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.427073486328" Y="-29.219322265625" />
                  <Point X="-2.480147460938" Y="-29.288490234375" />
                  <Point X="-2.628312255859" Y="-29.19675" />
                  <Point X="-2.801726074219" Y="-29.089376953125" />
                  <Point X="-3.037864501953" Y="-28.907556640625" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.863760498047" Y="-28.438720703125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405575927734" Y="-27.585189453125" />
                  <Point X="-2.414561767578" Y="-27.5555703125" />
                  <Point X="-2.428780761719" Y="-27.526740234375" />
                  <Point X="-2.44680859375" Y="-27.501583984375" />
                  <Point X="-2.464157226562" Y="-27.484236328125" />
                  <Point X="-2.489317626953" Y="-27.466208984375" />
                  <Point X="-2.518146240234" Y="-27.451994140625" />
                  <Point X="-2.547762939453" Y="-27.44301171875" />
                  <Point X="-2.5786953125" Y="-27.444025390625" />
                  <Point X="-2.610219726562" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.166481933594" Y="-27.765634765625" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.9458671875" Y="-27.973841796875" />
                  <Point X="-4.082862792969" Y="-27.793857421875" />
                  <Point X="-4.252163085938" Y="-27.509966796875" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.875810546875" Y="-27.08924609375" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577148438" Y="-26.475595703125" />
                  <Point X="-3.066612548828" Y="-26.44846484375" />
                  <Point X="-3.053856689453" Y="-26.419833984375" />
                  <Point X="-3.046152099609" Y="-26.3900859375" />
                  <Point X="-3.043347900391" Y="-26.359658203125" />
                  <Point X="-3.045556884766" Y="-26.327986328125" />
                  <Point X="-3.052559082031" Y="-26.29823828125" />
                  <Point X="-3.068644042969" Y="-26.27225390625" />
                  <Point X="-3.089479492188" Y="-26.248296875" />
                  <Point X="-3.112977050781" Y="-26.228765625" />
                  <Point X="-3.139459716797" Y="-26.2131796875" />
                  <Point X="-3.168723144531" Y="-26.20195703125" />
                  <Point X="-3.200607666016" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-3.897593017578" Y="-26.28201953125" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.780498046875" Y="-26.202416015625" />
                  <Point X="-4.834077636719" Y="-25.99265625" />
                  <Point X="-4.87887109375" Y="-25.67946484375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.409857910156" Y="-25.455396484375" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.509680664062" Y="-25.21075390625" />
                  <Point X="-3.477709960938" Y="-25.1921484375" />
                  <Point X="-3.464513427734" Y="-25.18288671875" />
                  <Point X="-3.441344970703" Y="-25.1634921875" />
                  <Point X="-3.425367919922" Y="-25.14634765625" />
                  <Point X="-3.414073730469" Y="-25.125814453125" />
                  <Point X="-3.401180175781" Y="-25.093458984375" />
                  <Point X="-3.396713867188" Y="-25.0789921875" />
                  <Point X="-3.39068359375" Y="-25.051982421875" />
                  <Point X="-3.388403076172" Y="-25.030625" />
                  <Point X="-3.390978515625" Y="-25.00930078125" />
                  <Point X="-3.398693359375" Y="-24.976861328125" />
                  <Point X="-3.403410888672" Y="-24.962333984375" />
                  <Point X="-3.414619873047" Y="-24.93540625" />
                  <Point X="-3.426225830078" Y="-24.915046875" />
                  <Point X="-3.442462646484" Y="-24.898146484375" />
                  <Point X="-3.470684814453" Y="-24.875244140625" />
                  <Point X="-3.484072753906" Y="-24.86615625" />
                  <Point X="-3.510989501953" Y="-24.85105859375" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.139661132812" Y="-24.679564453125" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.859018554688" Y="-24.25637890625" />
                  <Point X="-4.82448828125" Y="-24.023029296875" />
                  <Point X="-4.7343203125" Y="-23.69028125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.394308105469" Y="-23.6174453125" />
                  <Point X="-3.765666503906" Y="-23.70020703125" />
                  <Point X="-3.744984863281" Y="-23.700658203125" />
                  <Point X="-3.723423828125" Y="-23.698771484375" />
                  <Point X="-3.703138183594" Y="-23.694736328125" />
                  <Point X="-3.666832275391" Y="-23.6832890625" />
                  <Point X="-3.641712158203" Y="-23.675369140625" />
                  <Point X="-3.62278515625" Y="-23.667041015625" />
                  <Point X="-3.604040771484" Y="-23.656220703125" />
                  <Point X="-3.587355957031" Y="-23.64398828125" />
                  <Point X="-3.573715087891" Y="-23.62843359375" />
                  <Point X="-3.561300292969" Y="-23.610703125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.536783691406" Y="-23.557400390625" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553955078" Y="-23.4298984375" />
                  <Point X="-3.532050048828" Y="-23.41062109375" />
                  <Point X="-3.549627685547" Y="-23.376853515625" />
                  <Point X="-3.561789794922" Y="-23.3534921875" />
                  <Point X="-3.573284667969" Y="-23.3362890625" />
                  <Point X="-3.587196533203" Y="-23.3197109375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-3.950188720703" Y="-23.038337890625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.215324707031" Y="-22.49620703125" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.842303710938" Y="-21.959333984375" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.592751464844" Y="-21.932416015625" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187728759766" Y="-22.163658203125" />
                  <Point X="-3.167086181641" Y="-22.17016796875" />
                  <Point X="-3.146794189453" Y="-22.174205078125" />
                  <Point X="-3.096229980469" Y="-22.17862890625" />
                  <Point X="-3.061244873047" Y="-22.181689453125" />
                  <Point X="-3.040560302734" Y="-22.18123828125" />
                  <Point X="-3.019102050781" Y="-22.178412109375" />
                  <Point X="-2.999012695312" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946078125" Y="-22.139771484375" />
                  <Point X="-2.910187255859" Y="-22.103880859375" />
                  <Point X="-2.885354492188" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.847859619141" Y="-21.91331640625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.024027832031" Y="-21.551328125" />
                  <Point X="-3.183333496094" Y="-21.275404296875" />
                  <Point X="-2.934304931641" Y="-21.084474609375" />
                  <Point X="-2.700630371094" Y="-20.905318359375" />
                  <Point X="-2.324446533203" Y="-20.6963203125" />
                  <Point X="-2.167037109375" Y="-20.6088671875" />
                  <Point X="-2.161391113281" Y="-20.616224609375" />
                  <Point X="-2.043195800781" Y="-20.7702578125" />
                  <Point X="-2.028893432617" Y="-20.78519921875" />
                  <Point X="-2.012314697266" Y="-20.799111328125" />
                  <Point X="-1.995114624023" Y="-20.810603515625" />
                  <Point X="-1.938836791992" Y="-20.839900390625" />
                  <Point X="-1.89989855957" Y="-20.860171875" />
                  <Point X="-1.880625976562" Y="-20.86766796875" />
                  <Point X="-1.859719238281" Y="-20.873271484375" />
                  <Point X="-1.83926953125" Y="-20.876419921875" />
                  <Point X="-1.818623291016" Y="-20.87506640625" />
                  <Point X="-1.797307495117" Y="-20.871306640625" />
                  <Point X="-1.777452636719" Y="-20.865517578125" />
                  <Point X="-1.718835571289" Y="-20.841236328125" />
                  <Point X="-1.678278686523" Y="-20.8244375" />
                  <Point X="-1.660147338867" Y="-20.814490234375" />
                  <Point X="-1.642417358398" Y="-20.802076171875" />
                  <Point X="-1.626864990234" Y="-20.7884375" />
                  <Point X="-1.614633422852" Y="-20.771755859375" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.576401367188" Y="-20.673568359375" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.575265014648" Y="-20.435986328125" />
                  <Point X="-1.584201904297" Y="-20.368103515625" />
                  <Point X="-1.252147705078" Y="-20.275005859375" />
                  <Point X="-0.949623535156" Y="-20.19019140625" />
                  <Point X="-0.493597106934" Y="-20.136818359375" />
                  <Point X="-0.294711303711" Y="-20.113541015625" />
                  <Point X="-0.248120742798" Y="-20.287419921875" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113990784" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155908585" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425941467" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.225242355347" Y="-20.418751953125" />
                  <Point X="0.307419616699" Y="-20.112060546875" />
                  <Point X="0.57989642334" Y="-20.14059765625" />
                  <Point X="0.84403125" Y="-20.168259765625" />
                  <Point X="1.221337890625" Y="-20.259353515625" />
                  <Point X="1.481039794922" Y="-20.3220546875" />
                  <Point X="1.726005249023" Y="-20.410904296875" />
                  <Point X="1.894645141602" Y="-20.472072265625" />
                  <Point X="2.132115234375" Y="-20.58312890625" />
                  <Point X="2.294558105469" Y="-20.659099609375" />
                  <Point X="2.5240078125" Y="-20.792775390625" />
                  <Point X="2.680975097656" Y="-20.884224609375" />
                  <Point X="2.897340332031" Y="-21.038091796875" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.656847167969" Y="-21.566828125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.483943359375" />
                  <Point X="2.120387207031" Y="-22.531396484375" />
                  <Point X="2.111607177734" Y="-22.56423046875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.112676025391" Y="-22.66008203125" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140070800781" Y="-22.752529296875" />
                  <Point X="2.1654609375" Y="-22.78994921875" />
                  <Point X="2.183028564453" Y="-22.815837890625" />
                  <Point X="2.194469238281" Y="-22.829677734375" />
                  <Point X="2.221596923828" Y="-22.85440625" />
                  <Point X="2.259015380859" Y="-22.879796875" />
                  <Point X="2.284905517578" Y="-22.897365234375" />
                  <Point X="2.304946533203" Y="-22.9077265625" />
                  <Point X="2.327035400391" Y="-22.915994140625" />
                  <Point X="2.348965087891" Y="-22.921337890625" />
                  <Point X="2.389998779297" Y="-22.92628515625" />
                  <Point X="2.418389892578" Y="-22.929708984375" />
                  <Point X="2.436467529297" Y="-22.93015625" />
                  <Point X="2.473207519531" Y="-22.925830078125" />
                  <Point X="2.520660888672" Y="-22.913140625" />
                  <Point X="2.553493896484" Y="-22.904359375" />
                  <Point X="2.565292480469" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.198842285156" Y="-22.5374921875" />
                  <Point X="3.967325683594" Y="-22.09380859375" />
                  <Point X="4.030161621094" Y="-22.18113671875" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.243891113281" Y="-22.50986328125" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.901280517578" Y="-22.81705859375" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221421630859" Y="-23.33976171875" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.169821533203" Y="-23.402927734375" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.136606933594" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.108908203125" Y="-23.528404296875" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.108182617188" Y="-23.678845703125" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679443359" Y="-23.731021484375" />
                  <Point X="3.136282714844" Y="-23.76426171875" />
                  <Point X="3.164687255859" Y="-23.807435546875" />
                  <Point X="3.184340332031" Y="-23.837306640625" />
                  <Point X="3.198894042969" Y="-23.85455078125" />
                  <Point X="3.216135986328" Y="-23.870638671875" />
                  <Point X="3.234346435547" Y="-23.88396484375" />
                  <Point X="3.275508300781" Y="-23.907134765625" />
                  <Point X="3.303988525391" Y="-23.92316796875" />
                  <Point X="3.320523681641" Y="-23.9305" />
                  <Point X="3.356119628906" Y="-23.9405625" />
                  <Point X="3.4117734375" Y="-23.94791796875" />
                  <Point X="3.450280273438" Y="-23.953005859375" />
                  <Point X="3.462699462891" Y="-23.95382421875" />
                  <Point X="3.488203857422" Y="-23.953015625" />
                  <Point X="4.067955810547" Y="-23.876689453125" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.805945800781" Y="-23.902923828125" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.883946289062" Y="-24.311326171875" />
                  <Point X="4.890864257812" Y="-24.355759765625" />
                  <Point X="4.484983398438" Y="-24.464515625" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704788818359" Y="-24.674416015625" />
                  <Point X="3.681547607422" Y="-24.684931640625" />
                  <Point X="3.626869384766" Y="-24.71653515625" />
                  <Point X="3.589037597656" Y="-24.73840234375" />
                  <Point X="3.574312988281" Y="-24.74890234375" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.514723388672" Y="-24.816228515625" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.452745117188" Y="-24.964498046875" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.456114746094" Y="-25.11565625" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.176662109375" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.524831298828" Y="-25.259212890625" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.560000732422" Y="-25.30123828125" />
                  <Point X="3.589035400391" Y="-25.32415625" />
                  <Point X="3.643713623047" Y="-25.35576171875" />
                  <Point X="3.681545410156" Y="-25.37762890625" />
                  <Point X="3.692708251953" Y="-25.383138671875" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.248238769531" Y="-25.534609375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.8773515625" Y="-25.800623046875" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.806324707031" Y="-26.162126953125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.315514160156" Y="-26.12076171875" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.267345458984" Y="-26.028833984375" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163973144531" Y="-26.05659765625" />
                  <Point X="3.136146728516" Y="-26.073490234375" />
                  <Point X="3.112397460938" Y="-26.0939609375" />
                  <Point X="3.047532958984" Y="-26.17197265625" />
                  <Point X="3.002653320312" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.9604609375" Y="-26.40639453125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.035839599609" Y="-26.660373046875" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.604011230469" Y="-27.13949609375" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.187752929688" Y="-27.647935546875" />
                  <Point X="4.124814453125" Y="-27.74978125" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="3.594627685547" Y="-27.635171875" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754224609375" Y="-27.159826171875" />
                  <Point X="2.626504150391" Y="-27.136759765625" />
                  <Point X="2.538134277344" Y="-27.12080078125" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.338729248047" Y="-27.19101953125" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.148689941406" Y="-27.39654296875" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.118741699219" Y="-27.690978515625" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.4688671875" Y="-28.37522265625" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.856539306641" Y="-29.058294921875" />
                  <Point X="2.781833251953" Y="-29.11165625" />
                  <Point X="2.701764404297" Y="-29.163482421875" />
                  <Point X="2.364192138672" Y="-28.723548828125" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721923706055" Y="-27.900556640625" />
                  <Point X="1.595956787109" Y="-27.819572265625" />
                  <Point X="1.508800292969" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365478516" Y="-27.743435546875" />
                  <Point X="1.417100830078" Y="-27.741119140625" />
                  <Point X="1.279334472656" Y="-27.753794921875" />
                  <Point X="1.184013916016" Y="-27.76256640625" />
                  <Point X="1.156362548828" Y="-27.7693984375" />
                  <Point X="1.128976806641" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.7954609375" />
                  <Point X="0.998215576172" Y="-27.883912109375" />
                  <Point X="0.924611633301" Y="-27.94511328125" />
                  <Point X="0.904140441895" Y="-27.968865234375" />
                  <Point X="0.887248596191" Y="-27.99669140625" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.843817443848" Y="-28.172146484375" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.909591247559" Y="-29.00558984375" />
                  <Point X="1.022065551758" Y="-29.859916015625" />
                  <Point X="0.975707824707" Y="-29.870078125" />
                  <Point X="0.929315673828" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.05841394043" Y="-29.752638671875" />
                  <Point X="-1.14124609375" Y="-29.731326171875" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.159109008789" Y="-29.31783984375" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124633789" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230575561523" Y="-29.094859375" />
                  <Point X="-1.250210327148" Y="-29.070935546875" />
                  <Point X="-1.261006958008" Y="-29.05978125" />
                  <Point X="-1.398875854492" Y="-28.938873046875" />
                  <Point X="-1.494267578125" Y="-28.855216796875" />
                  <Point X="-1.506735229492" Y="-28.84596875" />
                  <Point X="-1.533018432617" Y="-28.829623046875" />
                  <Point X="-1.546833984375" Y="-28.822525390625" />
                  <Point X="-1.576530883789" Y="-28.810224609375" />
                  <Point X="-1.591315673828" Y="-28.805474609375" />
                  <Point X="-1.621457763672" Y="-28.798447265625" />
                  <Point X="-1.636815063477" Y="-28.796169921875" />
                  <Point X="-1.819798217773" Y="-28.784177734375" />
                  <Point X="-1.946404296875" Y="-28.77587890625" />
                  <Point X="-1.961928344727" Y="-28.7761328125" />
                  <Point X="-1.992725585938" Y="-28.779166015625" />
                  <Point X="-2.007998535156" Y="-28.7819453125" />
                  <Point X="-2.039047607422" Y="-28.790263671875" />
                  <Point X="-2.053667236328" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436035156" Y="-28.815810546875" />
                  <Point X="-2.247907714844" Y="-28.9176875" />
                  <Point X="-2.353402832031" Y="-28.988177734375" />
                  <Point X="-2.359687744141" Y="-28.992759765625" />
                  <Point X="-2.380450683594" Y="-29.010138671875" />
                  <Point X="-2.402762207031" Y="-29.032775390625" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.502442138672" Y="-29.161490234375" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.578301025391" Y="-29.115978515625" />
                  <Point X="-2.747613525391" Y="-29.01114453125" />
                  <Point X="-2.979906982422" Y="-28.83228515625" />
                  <Point X="-2.980862792969" Y="-28.831548828125" />
                  <Point X="-2.781488037109" Y="-28.486220703125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.619232421875" />
                  <Point X="-2.310626708984" Y="-27.588296875" />
                  <Point X="-2.314667480469" Y="-27.557609375" />
                  <Point X="-2.323653320313" Y="-27.527990234375" />
                  <Point X="-2.329360595703" Y="-27.513548828125" />
                  <Point X="-2.343579589844" Y="-27.48471875" />
                  <Point X="-2.351562011719" Y="-27.47140234375" />
                  <Point X="-2.36958984375" Y="-27.44624609375" />
                  <Point X="-2.379635253906" Y="-27.43440625" />
                  <Point X="-2.396983886719" Y="-27.41705859375" />
                  <Point X="-2.408826660156" Y="-27.40701171875" />
                  <Point X="-2.433987060547" Y="-27.388984375" />
                  <Point X="-2.4473046875" Y="-27.38100390625" />
                  <Point X="-2.476133300781" Y="-27.3667890625" />
                  <Point X="-2.490573974609" Y="-27.361083984375" />
                  <Point X="-2.520190673828" Y="-27.3521015625" />
                  <Point X="-2.550874511719" Y="-27.3480625" />
                  <Point X="-2.581806884766" Y="-27.349076171875" />
                  <Point X="-2.597231445312" Y="-27.3508515625" />
                  <Point X="-2.628755859375" Y="-27.357123046875" />
                  <Point X="-2.643686523438" Y="-27.36138671875" />
                  <Point X="-2.672650390625" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.213981933594" Y="-27.68336328125" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-3.870273681641" Y="-27.916302734375" />
                  <Point X="-4.004021484375" Y="-27.7405859375" />
                  <Point X="-4.1705703125" Y="-27.46130859375" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.817978271484" Y="-27.164615234375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036482421875" Y="-26.5633125" />
                  <Point X="-3.015104980469" Y="-26.540392578125" />
                  <Point X="-3.005367431641" Y="-26.528044921875" />
                  <Point X="-2.987402832031" Y="-26.5009140625" />
                  <Point X="-2.979835449219" Y="-26.487126953125" />
                  <Point X="-2.967079589844" Y="-26.45849609375" />
                  <Point X="-2.961891113281" Y="-26.44365234375" />
                  <Point X="-2.954186523438" Y="-26.413904296875" />
                  <Point X="-2.951552978516" Y="-26.3988046875" />
                  <Point X="-2.948748779297" Y="-26.368376953125" />
                  <Point X="-2.948578125" Y="-26.353048828125" />
                  <Point X="-2.950787109375" Y="-26.321376953125" />
                  <Point X="-2.953083984375" Y="-26.306220703125" />
                  <Point X="-2.960086181641" Y="-26.27647265625" />
                  <Point X="-2.971782958984" Y="-26.248236328125" />
                  <Point X="-2.987867919922" Y="-26.222251953125" />
                  <Point X="-2.996961425781" Y="-26.209912109375" />
                  <Point X="-3.017796875" Y="-26.185955078125" />
                  <Point X="-3.02875390625" Y="-26.175240234375" />
                  <Point X="-3.052251464844" Y="-26.155708984375" />
                  <Point X="-3.064791992188" Y="-26.146892578125" />
                  <Point X="-3.091274658203" Y="-26.131306640625" />
                  <Point X="-3.105442626953" Y="-26.124478515625" />
                  <Point X="-3.134706054688" Y="-26.113255859375" />
                  <Point X="-3.149801513672" Y="-26.108861328125" />
                  <Point X="-3.181686035156" Y="-26.102380859375" />
                  <Point X="-3.197298095703" Y="-26.10053515625" />
                  <Point X="-3.228619384766" Y="-26.099443359375" />
                  <Point X="-3.244328613281" Y="-26.100197265625" />
                  <Point X="-3.909992675781" Y="-26.18783203125" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.688453125" Y="-26.178904296875" />
                  <Point X="-4.740762207031" Y="-25.974119140625" />
                  <Point X="-4.784828125" Y="-25.666013671875" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.385270019531" Y="-25.54716015625" />
                  <Point X="-3.508287841797" Y="-25.312173828125" />
                  <Point X="-3.496364746094" Y="-25.30811328125" />
                  <Point X="-3.473169433594" Y="-25.29845703125" />
                  <Point X="-3.461897460938" Y="-25.292861328125" />
                  <Point X="-3.429926757812" Y="-25.274255859375" />
                  <Point X="-3.423135498047" Y="-25.269908203125" />
                  <Point X="-3.403533691406" Y="-25.255732421875" />
                  <Point X="-3.380365234375" Y="-25.236337890625" />
                  <Point X="-3.371845214844" Y="-25.228259765625" />
                  <Point X="-3.355868164062" Y="-25.211115234375" />
                  <Point X="-3.34212890625" Y="-25.1921328125" />
                  <Point X="-3.330834716797" Y="-25.171599609375" />
                  <Point X="-3.325822753906" Y="-25.160982421875" />
                  <Point X="-3.312929199219" Y="-25.128626953125" />
                  <Point X="-3.310407714844" Y="-25.121482421875" />
                  <Point X="-3.303996582031" Y="-25.099693359375" />
                  <Point X="-3.297966308594" Y="-25.07268359375" />
                  <Point X="-3.296220703125" Y="-25.062068359375" />
                  <Point X="-3.293940185547" Y="-25.0407109375" />
                  <Point X="-3.294088378906" Y="-25.019234375" />
                  <Point X="-3.296663818359" Y="-24.99791015625" />
                  <Point X="-3.298556152344" Y="-24.9873203125" />
                  <Point X="-3.306270996094" Y="-24.954880859375" />
                  <Point X="-3.308338134766" Y="-24.94751953125" />
                  <Point X="-3.315706054688" Y="-24.925826171875" />
                  <Point X="-3.326915039062" Y="-24.8988984375" />
                  <Point X="-3.332087890625" Y="-24.888359375" />
                  <Point X="-3.343693847656" Y="-24.868" />
                  <Point X="-3.357719238281" Y="-24.84923046875" />
                  <Point X="-3.373956054688" Y="-24.832330078125" />
                  <Point X="-3.382600585938" Y="-24.82437890625" />
                  <Point X="-3.410822753906" Y="-24.8014765625" />
                  <Point X="-3.417329345703" Y="-24.796642578125" />
                  <Point X="-3.437598632812" Y="-24.78330078125" />
                  <Point X="-3.464515380859" Y="-24.768203125" />
                  <Point X="-3.475175537109" Y="-24.763068359375" />
                  <Point X="-3.497062011719" Y="-24.75416015625" />
                  <Point X="-3.508288330078" Y="-24.75038671875" />
                  <Point X="-4.115073730469" Y="-24.58780078125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.765041992188" Y="-24.27028515625" />
                  <Point X="-4.73133203125" Y="-24.042478515625" />
                  <Point X="-4.642626953125" Y="-23.71512890625" />
                  <Point X="-4.633585449219" Y="-23.681763671875" />
                  <Point X="-4.406708007812" Y="-23.7116328125" />
                  <Point X="-3.778066650391" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747056884766" Y="-23.795634765625" />
                  <Point X="-3.736703369141" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704889892578" Y="-23.7919453125" />
                  <Point X="-3.684604248047" Y="-23.78791015625" />
                  <Point X="-3.674571044922" Y="-23.78533984375" />
                  <Point X="-3.638265136719" Y="-23.773892578125" />
                  <Point X="-3.613145019531" Y="-23.76597265625" />
                  <Point X="-3.603451171875" Y="-23.76232421875" />
                  <Point X="-3.584524169922" Y="-23.75399609375" />
                  <Point X="-3.575291015625" Y="-23.74931640625" />
                  <Point X="-3.556546630859" Y="-23.73849609375" />
                  <Point X="-3.547870605469" Y="-23.7328359375" />
                  <Point X="-3.531185791016" Y="-23.720603515625" />
                  <Point X="-3.515930664062" Y="-23.706625" />
                  <Point X="-3.502289794922" Y="-23.6910703125" />
                  <Point X="-3.495895263672" Y="-23.682921875" />
                  <Point X="-3.48348046875" Y="-23.66519140625" />
                  <Point X="-3.478012695312" Y="-23.656400390625" />
                  <Point X="-3.468063964844" Y="-23.638267578125" />
                  <Point X="-3.463583007813" Y="-23.62892578125" />
                  <Point X="-3.449015136719" Y="-23.593755859375" />
                  <Point X="-3.438935546875" Y="-23.569421875" />
                  <Point X="-3.435499267578" Y="-23.559650390625" />
                  <Point X="-3.4297109375" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432791259766" Y="-23.405310546875" />
                  <Point X="-3.436012451172" Y="-23.39546875" />
                  <Point X="-3.443508544922" Y="-23.37619140625" />
                  <Point X="-3.447783447266" Y="-23.366755859375" />
                  <Point X="-3.465361083984" Y="-23.33298828125" />
                  <Point X="-3.477523193359" Y="-23.309626953125" />
                  <Point X="-3.482800537109" Y="-23.300712890625" />
                  <Point X="-3.494295410156" Y="-23.283509765625" />
                  <Point X="-3.500512939453" Y="-23.275220703125" />
                  <Point X="-3.514424804688" Y="-23.258642578125" />
                  <Point X="-3.521503662109" Y="-23.2510859375" />
                  <Point X="-3.536442871094" Y="-23.23678515625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.892356201172" Y="-22.96296875" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.133278320312" Y="-22.544095703125" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.767322998047" Y="-22.01766796875" />
                  <Point X="-3.726336914062" Y="-21.964986328125" />
                  <Point X="-3.640251464844" Y="-22.0146875" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244923583984" Y="-22.242279296875" />
                  <Point X="-3.225995361328" Y="-22.250609375" />
                  <Point X="-3.216300537109" Y="-22.254259765625" />
                  <Point X="-3.195657958984" Y="-22.26076953125" />
                  <Point X="-3.185623291016" Y="-22.263341796875" />
                  <Point X="-3.165331298828" Y="-22.26737890625" />
                  <Point X="-3.155073974609" Y="-22.26884375" />
                  <Point X="-3.104509765625" Y="-22.273267578125" />
                  <Point X="-3.069524658203" Y="-22.276328125" />
                  <Point X="-3.059173339844" Y="-22.276666015625" />
                  <Point X="-3.038488769531" Y="-22.27621484375" />
                  <Point X="-3.028155517578" Y="-22.27542578125" />
                  <Point X="-3.006697265625" Y="-22.272599609375" />
                  <Point X="-2.996512695312" Y="-22.2706875" />
                  <Point X="-2.976423339844" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.938449462891" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.90274609375" Y="-22.226806640625" />
                  <Point X="-2.886614501953" Y="-22.213859375" />
                  <Point X="-2.878903320312" Y="-22.206947265625" />
                  <Point X="-2.843012451172" Y="-22.171056640625" />
                  <Point X="-2.8181796875" Y="-22.146224609375" />
                  <Point X="-2.811267089844" Y="-22.138513671875" />
                  <Point X="-2.798321533203" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.753221191406" Y="-21.905037109375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-2.941755371094" Y="-21.503828125" />
                  <Point X="-3.059388183594" Y="-21.300083984375" />
                  <Point X="-2.876502441406" Y="-21.159865234375" />
                  <Point X="-2.648385498047" Y="-20.984970703125" />
                  <Point X="-2.278309326172" Y="-20.779365234375" />
                  <Point X="-2.192524658203" Y="-20.731705078125" />
                  <Point X="-2.118562255859" Y="-20.82809375" />
                  <Point X="-2.111822509766" Y="-20.83594921875" />
                  <Point X="-2.097520019531" Y="-20.850890625" />
                  <Point X="-2.089960449219" Y="-20.857970703125" />
                  <Point X="-2.073381835938" Y="-20.8718828125" />
                  <Point X="-2.065092285156" Y="-20.8781015625" />
                  <Point X="-2.047892089844" Y="-20.88959375" />
                  <Point X="-2.038981323242" Y="-20.894869140625" />
                  <Point X="-1.982703491211" Y="-20.924166015625" />
                  <Point X="-1.943765258789" Y="-20.9444375" />
                  <Point X="-1.934335693359" Y="-20.9487109375" />
                  <Point X="-1.915063110352" Y="-20.95620703125" />
                  <Point X="-1.905220092773" Y="-20.9594296875" />
                  <Point X="-1.884313476562" Y="-20.965033203125" />
                  <Point X="-1.874175170898" Y="-20.967166015625" />
                  <Point X="-1.853725463867" Y="-20.970314453125" />
                  <Point X="-1.833054931641" Y="-20.971216796875" />
                  <Point X="-1.812408691406" Y="-20.96986328125" />
                  <Point X="-1.802121582031" Y="-20.968623046875" />
                  <Point X="-1.780805786133" Y="-20.96486328125" />
                  <Point X="-1.770715698242" Y="-20.962509765625" />
                  <Point X="-1.750860839844" Y="-20.956720703125" />
                  <Point X="-1.741096069336" Y="-20.95328515625" />
                  <Point X="-1.682479003906" Y="-20.92900390625" />
                  <Point X="-1.64192199707" Y="-20.912205078125" />
                  <Point X="-1.632584472656" Y="-20.9077265625" />
                  <Point X="-1.614453125" Y="-20.897779296875" />
                  <Point X="-1.605659301758" Y="-20.892310546875" />
                  <Point X="-1.587929321289" Y="-20.879896484375" />
                  <Point X="-1.579780639648" Y="-20.873501953125" />
                  <Point X="-1.564228027344" Y="-20.85986328125" />
                  <Point X="-1.550252929688" Y="-20.844611328125" />
                  <Point X="-1.538021362305" Y="-20.8279296875" />
                  <Point X="-1.532361450195" Y="-20.819255859375" />
                  <Point X="-1.521539306641" Y="-20.80051171875" />
                  <Point X="-1.516856811523" Y="-20.7912734375" />
                  <Point X="-1.508525756836" Y="-20.77233984375" />
                  <Point X="-1.504877197266" Y="-20.76264453125" />
                  <Point X="-1.485798339844" Y="-20.702134765625" />
                  <Point X="-1.472597900391" Y="-20.660267578125" />
                  <Point X="-1.470026489258" Y="-20.650234375" />
                  <Point X="-1.465990966797" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266235352" Y="-20.437345703125" />
                  <Point X="-1.226501586914" Y="-20.366478515625" />
                  <Point X="-0.931164306641" Y="-20.2836796875" />
                  <Point X="-0.482553741455" Y="-20.231173828125" />
                  <Point X="-0.365222564697" Y="-20.21744140625" />
                  <Point X="-0.339883758545" Y="-20.3120078125" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.317005371094" Y="-20.44333984375" />
                  <Point X="0.378190765381" Y="-20.2149921875" />
                  <Point X="0.570000915527" Y="-20.235080078125" />
                  <Point X="0.827852783203" Y="-20.262083984375" />
                  <Point X="1.199042480469" Y="-20.351701171875" />
                  <Point X="1.453624023438" Y="-20.413166015625" />
                  <Point X="1.69361328125" Y="-20.5002109375" />
                  <Point X="1.858245483398" Y="-20.55992578125" />
                  <Point X="2.091870605469" Y="-20.66918359375" />
                  <Point X="2.250431884766" Y="-20.74333984375" />
                  <Point X="2.476185302734" Y="-20.874861328125" />
                  <Point X="2.629430419922" Y="-20.964142578125" />
                  <Point X="2.817778808594" Y="-21.0980859375" />
                  <Point X="2.574574707031" Y="-21.519328125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053180664062" Y="-22.426564453125" />
                  <Point X="2.044182373047" Y="-22.450435546875" />
                  <Point X="2.041301757812" Y="-22.459400390625" />
                  <Point X="2.028612060547" Y="-22.506853515625" />
                  <Point X="2.01983203125" Y="-22.5396875" />
                  <Point X="2.017912231445" Y="-22.548537109375" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.01835925293" Y="-22.671455078125" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029143920898" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040735717773" Y="-22.765783203125" />
                  <Point X="2.045319091797" Y="-22.77611328125" />
                  <Point X="2.055680664062" Y="-22.79615625" />
                  <Point X="2.061458740234" Y="-22.805869140625" />
                  <Point X="2.086848876953" Y="-22.8432890625" />
                  <Point X="2.104416503906" Y="-22.869177734375" />
                  <Point X="2.109807373047" Y="-22.876365234375" />
                  <Point X="2.130470458984" Y="-22.899884765625" />
                  <Point X="2.157598144531" Y="-22.92461328125" />
                  <Point X="2.168254882812" Y="-22.933017578125" />
                  <Point X="2.205673339844" Y="-22.958408203125" />
                  <Point X="2.231563476562" Y="-22.9759765625" />
                  <Point X="2.241276123047" Y="-22.98175390625" />
                  <Point X="2.261317138672" Y="-22.992115234375" />
                  <Point X="2.271645507812" Y="-22.99669921875" />
                  <Point X="2.293734375" Y="-23.004966796875" />
                  <Point X="2.304544189453" Y="-23.00829296875" />
                  <Point X="2.326473876953" Y="-23.01363671875" />
                  <Point X="2.33759375" Y="-23.015654296875" />
                  <Point X="2.378627441406" Y="-23.0206015625" />
                  <Point X="2.407018554688" Y="-23.024025390625" />
                  <Point X="2.416040283203" Y="-23.0246796875" />
                  <Point X="2.447577148438" Y="-23.02450390625" />
                  <Point X="2.484317138672" Y="-23.020177734375" />
                  <Point X="2.497749023438" Y="-23.01760546875" />
                  <Point X="2.545202392578" Y="-23.004916015625" />
                  <Point X="2.578035400391" Y="-22.996134765625" />
                  <Point X="2.583995849609" Y="-22.994330078125" />
                  <Point X="2.604423339844" Y="-22.98692578125" />
                  <Point X="2.627664794922" Y="-22.976419921875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.246342285156" Y="-22.619763671875" />
                  <Point X="3.940403076172" Y="-22.219048828125" />
                  <Point X="3.953049072266" Y="-22.236623046875" />
                  <Point X="4.043958007812" Y="-22.36296484375" />
                  <Point X="4.136884765625" Y="-22.516529296875" />
                  <Point X="3.843448242188" Y="-22.741689453125" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168135986328" Y="-23.2601328125" />
                  <Point X="3.152114990234" Y="-23.274787109375" />
                  <Point X="3.134667236328" Y="-23.2933984375" />
                  <Point X="3.128576171875" Y="-23.300578125" />
                  <Point X="3.094423828125" Y="-23.3451328125" />
                  <Point X="3.070793945312" Y="-23.375958984375" />
                  <Point X="3.065634033203" Y="-23.383400390625" />
                  <Point X="3.049740722656" Y="-23.410626953125" />
                  <Point X="3.034763671875" Y="-23.444453125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.017418457031" Y="-23.502818359375" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.015142578125" Y="-23.69804296875" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034682617188" Y="-23.771388671875" />
                  <Point X="3.050285888672" Y="-23.80462890625" />
                  <Point X="3.056918701172" Y="-23.8164765625" />
                  <Point X="3.085323242188" Y="-23.859650390625" />
                  <Point X="3.104976318359" Y="-23.889521484375" />
                  <Point X="3.111740966797" Y="-23.898580078125" />
                  <Point X="3.126294677734" Y="-23.91582421875" />
                  <Point X="3.134083740234" Y="-23.924009765625" />
                  <Point X="3.151325683594" Y="-23.94009765625" />
                  <Point X="3.160033447266" Y="-23.947302734375" />
                  <Point X="3.178243896484" Y="-23.96062890625" />
                  <Point X="3.187746582031" Y="-23.96675" />
                  <Point X="3.228908447266" Y="-23.989919921875" />
                  <Point X="3.257388671875" Y="-24.005953125" />
                  <Point X="3.265479736328" Y="-24.010013671875" />
                  <Point X="3.294681152344" Y="-24.02191796875" />
                  <Point X="3.330277099609" Y="-24.03198046875" />
                  <Point X="3.343672119141" Y="-24.034744140625" />
                  <Point X="3.399325927734" Y="-24.042099609375" />
                  <Point X="3.437832763672" Y="-24.0471875" />
                  <Point X="3.444033935547" Y="-24.04780078125" />
                  <Point X="3.465709960938" Y="-24.04877734375" />
                  <Point X="3.491214355469" Y="-24.04796875" />
                  <Point X="3.500604003906" Y="-24.047203125" />
                  <Point X="4.080355957031" Y="-23.970876953125" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.713641601562" Y="-23.92539453125" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.460395507812" Y="-24.372751953125" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686020263672" Y="-24.580458984375" />
                  <Point X="3.665627441406" Y="-24.58786328125" />
                  <Point X="3.642386230469" Y="-24.59837890625" />
                  <Point X="3.634008056641" Y="-24.602681640625" />
                  <Point X="3.579329833984" Y="-24.63428515625" />
                  <Point X="3.541498046875" Y="-24.65615234375" />
                  <Point X="3.533881103516" Y="-24.6610546875" />
                  <Point X="3.508776611328" Y="-24.680126953125" />
                  <Point X="3.481993896484" Y="-24.7056484375" />
                  <Point X="3.472795654297" Y="-24.715775390625" />
                  <Point X="3.439988769531" Y="-24.757580078125" />
                  <Point X="3.417289794922" Y="-24.78650390625" />
                  <Point X="3.410854736328" Y="-24.795791015625" />
                  <Point X="3.399130615234" Y="-24.8150703125" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.38000390625" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.370376708984" Y="-24.88952734375" />
                  <Point X="3.359440917969" Y="-24.94662890625" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.362810546875" Y="-25.133525390625" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.183986328125" />
                  <Point X="3.380004394531" Y="-25.205484375" />
                  <Point X="3.384067382812" Y="-25.216029296875" />
                  <Point X="3.393841064453" Y="-25.237494140625" />
                  <Point X="3.399128662109" Y="-25.247484375" />
                  <Point X="3.410853027344" Y="-25.266765625" />
                  <Point X="3.417289794922" Y="-25.276056640625" />
                  <Point X="3.450096679688" Y="-25.317861328125" />
                  <Point X="3.472795654297" Y="-25.34678515625" />
                  <Point X="3.478718261719" Y="-25.353634765625" />
                  <Point X="3.501141113281" Y="-25.375806640625" />
                  <Point X="3.53017578125" Y="-25.398724609375" />
                  <Point X="3.541493652344" Y="-25.406404296875" />
                  <Point X="3.596171875" Y="-25.438009765625" />
                  <Point X="3.634003662109" Y="-25.459876953125" />
                  <Point X="3.639498046875" Y="-25.46281640625" />
                  <Point X="3.659150146484" Y="-25.472013671875" />
                  <Point X="3.683021972656" Y="-25.48102734375" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.223650878906" Y="-25.626373046875" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.783413085938" Y="-25.7864609375" />
                  <Point X="4.76161328125" Y="-25.9310546875" />
                  <Point X="4.727801757812" Y="-26.079220703125" />
                  <Point X="4.3279140625" Y="-26.02657421875" />
                  <Point X="3.436781738281" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354482421875" Y="-25.912677734375" />
                  <Point X="3.247168701172" Y="-25.936001953125" />
                  <Point X="3.172918212891" Y="-25.952140625" />
                  <Point X="3.157874755859" Y="-25.9567421875" />
                  <Point X="3.128752929688" Y="-25.9683671875" />
                  <Point X="3.114674560547" Y="-25.975390625" />
                  <Point X="3.086848144531" Y="-25.992283203125" />
                  <Point X="3.074122314453" Y="-26.00153125" />
                  <Point X="3.050373046875" Y="-26.022001953125" />
                  <Point X="3.039349609375" Y="-26.033224609375" />
                  <Point X="2.974485107422" Y="-26.111236328125" />
                  <Point X="2.92960546875" Y="-26.165212890625" />
                  <Point X="2.921327148438" Y="-26.17684765625" />
                  <Point X="2.906606689453" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.865860595703" Y="-26.397689453125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.9559296875" Y="-26.711748046875" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.546178955078" Y="-27.214865234375" />
                  <Point X="4.087170410156" Y="-27.629984375" />
                  <Point X="4.045498291016" Y="-27.69741796875" />
                  <Point X="4.001273681641" Y="-27.76025390625" />
                  <Point X="3.642127685547" Y="-27.552900390625" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841199951172" Y="-27.090890625" />
                  <Point X="2.815021240234" Y="-27.07951171875" />
                  <Point X="2.783116943359" Y="-27.069326171875" />
                  <Point X="2.771108642578" Y="-27.066337890625" />
                  <Point X="2.643388183594" Y="-27.043271484375" />
                  <Point X="2.555018310547" Y="-27.0273125" />
                  <Point X="2.539359375" Y="-27.02580859375" />
                  <Point X="2.508008789062" Y="-27.025404296875" />
                  <Point X="2.492317138672" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444846923828" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400589599609" Y="-27.051109375" />
                  <Point X="2.294485107422" Y="-27.106951171875" />
                  <Point X="2.221071533203" Y="-27.145587890625" />
                  <Point X="2.208967773438" Y="-27.153171875" />
                  <Point X="2.186038330078" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144941650391" Y="-27.21116015625" />
                  <Point X="2.128047363281" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.064622070313" Y="-27.352298828125" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.02525390625" Y="-27.707861328125" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.386594726562" Y="-28.42272265625" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723753662109" Y="-29.036083984375" />
                  <Point X="2.439560791016" Y="-28.665716796875" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828658813477" Y="-27.87015234375" />
                  <Point X="1.808834594727" Y="-27.84962890625" />
                  <Point X="1.783251464844" Y="-27.82800390625" />
                  <Point X="1.773298217773" Y="-27.820646484375" />
                  <Point X="1.647331298828" Y="-27.739662109375" />
                  <Point X="1.560174804688" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517470581055" Y="-27.663873046875" />
                  <Point X="1.502546875" Y="-27.6588828125" />
                  <Point X="1.470926025391" Y="-27.65115234375" />
                  <Point X="1.455384765625" Y="-27.6486953125" />
                  <Point X="1.424120117188" Y="-27.64637890625" />
                  <Point X="1.408396728516" Y="-27.64651953125" />
                  <Point X="1.270630371094" Y="-27.6591953125" />
                  <Point X="1.175309814453" Y="-27.667966796875" />
                  <Point X="1.161226806641" Y="-27.67033984375" />
                  <Point X="1.133575439453" Y="-27.677171875" />
                  <Point X="1.120007080078" Y="-27.681630859375" />
                  <Point X="1.092621337891" Y="-27.692974609375" />
                  <Point X="1.079879638672" Y="-27.699412109375" />
                  <Point X="1.055498046875" Y="-27.714130859375" />
                  <Point X="1.043858154297" Y="-27.722412109375" />
                  <Point X="0.937478637695" Y="-27.81086328125" />
                  <Point X="0.863874633789" Y="-27.872064453125" />
                  <Point X="0.852650756836" Y="-27.883091796875" />
                  <Point X="0.832179626465" Y="-27.90684375" />
                  <Point X="0.822932312012" Y="-27.919568359375" />
                  <Point X="0.806040405273" Y="-27.94739453125" />
                  <Point X="0.799018859863" Y="-27.961470703125" />
                  <Point X="0.78739465332" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.750984985352" Y="-28.15196875" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.815403930664" Y="-29.017990234375" />
                  <Point X="0.83309161377" Y="-29.152341796875" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642143554688" Y="-28.453576171875" />
                  <Point X="0.626786315918" Y="-28.423814453125" />
                  <Point X="0.620407165527" Y="-28.413208984375" />
                  <Point X="0.523635803223" Y="-28.27378125" />
                  <Point X="0.456679534912" Y="-28.17730859375" />
                  <Point X="0.446669647217" Y="-28.165171875" />
                  <Point X="0.424787139893" Y="-28.14271875" />
                  <Point X="0.412914550781" Y="-28.13240234375" />
                  <Point X="0.386659729004" Y="-28.113158203125" />
                  <Point X="0.373244415283" Y="-28.104939453125" />
                  <Point X="0.345241210938" Y="-28.090830078125" />
                  <Point X="0.330653411865" Y="-28.084939453125" />
                  <Point X="0.180904998779" Y="-28.03846484375" />
                  <Point X="0.077294174194" Y="-28.006306640625" />
                  <Point X="0.063380393982" Y="-28.003111328125" />
                  <Point X="0.035228092194" Y="-27.998841796875" />
                  <Point X="0.020989723206" Y="-27.997767578125" />
                  <Point X="-0.008651926041" Y="-27.997765625" />
                  <Point X="-0.022896093369" Y="-27.998837890625" />
                  <Point X="-0.051062065125" Y="-28.003107421875" />
                  <Point X="-0.064983718872" Y="-28.0063046875" />
                  <Point X="-0.214731704712" Y="-28.05278125" />
                  <Point X="-0.318342803955" Y="-28.0849375" />
                  <Point X="-0.332927185059" Y="-28.090828125" />
                  <Point X="-0.360928466797" Y="-28.104935546875" />
                  <Point X="-0.374345275879" Y="-28.11315234375" />
                  <Point X="-0.40060067749" Y="-28.132396484375" />
                  <Point X="-0.41247833252" Y="-28.14271875" />
                  <Point X="-0.434361724854" Y="-28.16517578125" />
                  <Point X="-0.444367889404" Y="-28.177310546875" />
                  <Point X="-0.541139404297" Y="-28.316740234375" />
                  <Point X="-0.60809564209" Y="-28.4132109375" />
                  <Point X="-0.612468444824" Y="-28.42012890625" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.806570068359" Y="-29.099310546875" />
                  <Point X="-0.985425292969" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.060865679287" Y="-22.574860376992" />
                  <Point X="3.876679055064" Y="-22.255839785764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.984846592948" Y="-22.633191457109" />
                  <Point X="3.794406572885" Y="-22.303339666565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767043292241" Y="-24.177995881796" />
                  <Point X="4.607402369065" Y="-23.901489691888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.90882750661" Y="-22.691522537226" />
                  <Point X="3.712134090705" Y="-22.350839547365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.728337946176" Y="-24.300956255888" />
                  <Point X="4.505454841046" Y="-23.914911393654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.8328084443" Y="-22.749853658962" />
                  <Point X="3.629861608526" Y="-22.398339428165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.633338076875" Y="-24.326411655546" />
                  <Point X="4.403507313028" Y="-23.92833309542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.75678952964" Y="-22.808185036434" />
                  <Point X="3.547589126346" Y="-22.445839308966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.793621090938" Y="-21.139928364355" />
                  <Point X="2.735807513548" Y="-21.039792310949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.538338207574" Y="-24.351867055204" />
                  <Point X="4.301559785009" Y="-23.941754797186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.68077061498" Y="-22.866516413907" />
                  <Point X="3.465316644167" Y="-22.493339189766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.738772836652" Y="-21.234928401226" />
                  <Point X="2.558614427363" Y="-20.922884882926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.44333833053" Y="-24.377322441451" />
                  <Point X="4.199612256991" Y="-23.955176498952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.60475170032" Y="-22.924847791379" />
                  <Point X="3.383044161988" Y="-22.540839070566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.683924582367" Y="-21.329928438098" />
                  <Point X="2.393318580267" Y="-20.826584077476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.348338418105" Y="-24.402777766416" />
                  <Point X="4.097664728972" Y="-23.968598200717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.52873278566" Y="-22.983179168852" />
                  <Point X="3.300771679808" Y="-22.588338951367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.629076328082" Y="-21.424928474969" />
                  <Point X="2.230060283981" Y="-20.733812413552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.25333850568" Y="-24.428233091381" />
                  <Point X="3.995717206127" Y="-23.982019911444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.452713871" Y="-23.041510546324" />
                  <Point X="3.218499213772" Y="-22.635838860128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574228073551" Y="-21.519928511415" />
                  <Point X="2.079787860938" Y="-20.663532941865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.158338593255" Y="-24.453688416347" />
                  <Point X="3.89376968434" Y="-23.995441624002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.37669495634" Y="-23.099841923796" />
                  <Point X="3.136226779292" Y="-22.683338823547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.519379780442" Y="-21.614928481042" />
                  <Point X="1.929517516251" Y="-20.593257069995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.06333868083" Y="-24.479143741312" />
                  <Point X="3.791822162552" Y="-24.008863336561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.300676041679" Y="-23.158173301269" />
                  <Point X="3.053954344813" Y="-22.730838786967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464531487333" Y="-21.709928450669" />
                  <Point X="1.785301631512" Y="-20.533467830369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767885223244" Y="-25.88945414907" />
                  <Point X="4.687798627374" Y="-25.750740096019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.968338768405" Y="-24.504599066277" />
                  <Point X="3.689874640765" Y="-24.02228504912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.224657127019" Y="-23.216504678741" />
                  <Point X="2.971681910334" Y="-22.778338750387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.409683194224" Y="-21.804928420296" />
                  <Point X="1.646548517546" Y="-20.483140387272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.739118615145" Y="-26.029628922281" />
                  <Point X="4.558026398339" Y="-25.715968001919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.87333885598" Y="-24.530054391242" />
                  <Point X="3.587927118978" Y="-24.035706761678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.149945834449" Y="-23.27710092411" />
                  <Point X="2.889409475855" Y="-22.825838713806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.354834901116" Y="-21.899928389923" />
                  <Point X="1.507796291365" Y="-20.432814481863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.652316237996" Y="-26.069282794842" />
                  <Point X="4.428254169305" Y="-25.681195907819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.778338943554" Y="-24.555509716207" />
                  <Point X="3.48541616235" Y="-24.048152576466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0859270759" Y="-23.356217181667" />
                  <Point X="2.807137041376" Y="-22.873338677226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299986608007" Y="-21.99492835955" />
                  <Point X="1.375925041816" Y="-20.394406777586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.533595742522" Y="-26.053652864781" />
                  <Point X="4.29848194027" Y="-25.64642381372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.683562145441" Y="-24.581351486496" />
                  <Point X="3.369986147614" Y="-24.038221926225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.031853052793" Y="-23.452558226274" />
                  <Point X="2.724864606896" Y="-22.920838640646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.245138314898" Y="-22.089928329177" />
                  <Point X="1.248460960553" Y="-20.363632512699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.414875247048" Y="-26.03802293472" />
                  <Point X="4.168709662867" Y="-25.611651635844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.598147385491" Y="-24.623408782546" />
                  <Point X="3.234084893821" Y="-23.992834049844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001335188848" Y="-23.589699735383" />
                  <Point X="2.642592172417" Y="-22.968338604065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.190290021789" Y="-22.184928298804" />
                  <Point X="1.120997009426" Y="-20.332858473213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.296154753501" Y="-26.022393007997" />
                  <Point X="4.038937319586" Y="-25.576879343864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.517410203576" Y="-24.673567881409" />
                  <Point X="2.552835019629" Y="-23.002874655093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.13544172868" Y="-22.279928268431" />
                  <Point X="0.9935331407" Y="-20.30208457645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.17743426523" Y="-26.006763090413" />
                  <Point X="3.909164976305" Y="-25.542107051883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449339100457" Y="-24.74566527228" />
                  <Point X="2.455113756758" Y="-23.02361646282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.080593435571" Y="-22.374928238058" />
                  <Point X="0.866069271974" Y="-20.271310679687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.05871377696" Y="-25.991133172829" />
                  <Point X="3.779392633023" Y="-25.507334759903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.390155829362" Y="-24.833156839785" />
                  <Point X="2.341061642738" Y="-23.016072406627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.03445312103" Y="-22.485010868995" />
                  <Point X="0.746102766351" Y="-20.253522596742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.939993288689" Y="-25.975503255244" />
                  <Point X="3.645657770227" Y="-25.465699182797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356086177357" Y="-24.964146471516" />
                  <Point X="2.193177008963" Y="-22.949928707271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.014652470008" Y="-22.640715135404" />
                  <Point X="0.629346695511" Y="-20.241295149955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.821272800418" Y="-25.95987333766" />
                  <Point X="3.452027347284" Y="-25.320321452368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.373037375944" Y="-25.183506808718" />
                  <Point X="0.512590569886" Y="-20.229067608279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.702552312148" Y="-25.944243420076" />
                  <Point X="0.395834387631" Y="-20.216839968515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583831823877" Y="-25.928613502492" />
                  <Point X="0.348673046698" Y="-20.325154129866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.465111335606" Y="-25.912983584908" />
                  <Point X="0.313900698668" Y="-20.45492665638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.355177959812" Y="-25.912573392584" />
                  <Point X="0.279128320969" Y="-20.584699131504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.257687895137" Y="-25.933715647334" />
                  <Point X="0.244355943269" Y="-20.714471606628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.160773752809" Y="-25.95585542885" />
                  <Point X="0.19554483786" Y="-20.819928292086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.051221659694" Y="-27.688156445068" />
                  <Point X="3.962328932682" Y="-27.534189725459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.07646516296" Y="-25.999828667718" />
                  <Point X="0.122719001232" Y="-20.883790242942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974089023593" Y="-27.74455880042" />
                  <Point X="3.765380697035" Y="-27.383065374859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007892618164" Y="-26.071057536126" />
                  <Point X="0.03016269864" Y="-20.913478024292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.359618014791" Y="-20.23835802482" />
                  <Point X="-0.37128460033" Y="-20.218150905916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.809544007175" Y="-27.649558471851" />
                  <Point X="3.568432461389" Y="-27.231941024258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.943150989809" Y="-26.148921746451" />
                  <Point X="-0.091249045632" Y="-20.893186714578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.264618417331" Y="-20.592902154318" />
                  <Point X="-0.474037810668" Y="-20.230177124969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.644998990757" Y="-27.554558143283" />
                  <Point X="3.371484830847" Y="-27.080817721729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.88809666012" Y="-26.243564850253" />
                  <Point X="-0.576790958335" Y="-20.242203452573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.480454304142" Y="-27.459558385951" />
                  <Point X="3.174537277386" Y="-26.929694552708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866070018854" Y="-26.39541358846" />
                  <Point X="-0.679544100337" Y="-20.254229789987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.315909623385" Y="-27.364558638764" />
                  <Point X="-0.78229724234" Y="-20.266256127401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.151364942627" Y="-27.269558891576" />
                  <Point X="-0.885050384343" Y="-20.278282464814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.986820261869" Y="-27.174559144389" />
                  <Point X="-0.983207090833" Y="-20.298270062069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.824669245616" Y="-27.08370534574" />
                  <Point X="-1.077621519955" Y="-20.324739473863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.697243701683" Y="-27.052997829464" />
                  <Point X="-1.172035949077" Y="-20.351208885656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574777968096" Y="-27.030880956706" />
                  <Point X="-1.266450084163" Y="-20.377678806735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464985642084" Y="-27.030715069773" />
                  <Point X="-1.360863818362" Y="-20.404149422169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.37487671262" Y="-27.064641825726" />
                  <Point X="-1.455277552562" Y="-20.430620037603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.290744168595" Y="-27.108919984903" />
                  <Point X="-1.463380295917" Y="-20.606585674432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.207304095822" Y="-27.154397539473" />
                  <Point X="-1.497141349166" Y="-20.738109814886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136777487231" Y="-27.222241870108" />
                  <Point X="-1.547509445479" Y="-20.840869712992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.08252728233" Y="-27.318277758899" />
                  <Point X="-1.621968695837" Y="-20.90190250828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030216442812" Y="-27.417672727067" />
                  <Point X="-1.709545142672" Y="-20.940215652814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000578447307" Y="-27.556338213018" />
                  <Point X="-1.802793880845" Y="-20.968704100558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.054970816972" Y="-27.840548560821" />
                  <Point X="-1.921050052394" Y="-20.953878403125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.36736099704" Y="-28.571624224485" />
                  <Point X="-2.082508598125" Y="-20.864223998603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.924290707424" Y="-27.994203971546" />
                  <Point X="-2.250211228333" Y="-20.763754522519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.679618808642" Y="-27.760419811671" />
                  <Point X="-2.333266695222" Y="-20.809898234021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.513393935347" Y="-27.662509885642" />
                  <Point X="-2.416322197437" Y="-20.856041884337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.395168075647" Y="-27.647736689874" />
                  <Point X="-2.499377699652" Y="-20.902185534652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.291004839531" Y="-27.65732067264" />
                  <Point X="-2.582433201867" Y="-20.948329184968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.186842138346" Y="-27.666905581935" />
                  <Point X="-2.664043698618" Y="-20.996975658166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.092292474949" Y="-27.693140761091" />
                  <Point X="-2.740082057128" Y="-21.055273357902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.013884531694" Y="-27.747334219657" />
                  <Point X="-2.816120415638" Y="-21.113571057638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.939767660037" Y="-27.80896003225" />
                  <Point X="-2.89215869057" Y="-21.171868902136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.865651470714" Y="-27.870587026678" />
                  <Point X="-2.968196643163" Y="-21.23016730494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.803379518405" Y="-27.952728841393" />
                  <Point X="-3.044234595757" Y="-21.288465707745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766774181886" Y="-28.079326538713" />
                  <Point X="-2.749135988763" Y="-21.989591488301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.736771649419" Y="-28.217360628125" />
                  <Point X="-2.789526966599" Y="-22.109632262521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.734493584658" Y="-28.403414904217" />
                  <Point X="-2.856165989358" Y="-22.184210089337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766896299344" Y="-28.649538052355" />
                  <Point X="-2.930666217169" Y="-22.245171909592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.799299014029" Y="-28.895661200494" />
                  <Point X="0.734017028334" Y="-28.78258948445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331403458473" Y="-28.085242325633" />
                  <Point X="-3.023267382444" Y="-22.274781986496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.831701694292" Y="-29.141784289011" />
                  <Point X="0.829016584454" Y="-29.137133542346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.197711290154" Y="-28.043680697532" />
                  <Point X="-3.135398530366" Y="-22.270565141185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.064777204388" Y="-28.003432106927" />
                  <Point X="-3.269113130996" Y="-22.228964659178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.045586050766" Y="-28.002277341712" />
                  <Point X="-3.433658089114" Y="-22.133964431589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.139588944974" Y="-28.029459552885" />
                  <Point X="-3.598203047232" Y="-22.038964204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.232616113555" Y="-28.058331770418" />
                  <Point X="-3.740270577724" Y="-21.982896023081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.325323905838" Y="-28.087757163926" />
                  <Point X="-3.803238505566" Y="-22.063832372812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.406358813221" Y="-28.137400587152" />
                  <Point X="-3.866206370785" Y="-22.144768831009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.470923199003" Y="-28.215571790619" />
                  <Point X="-3.929174236004" Y="-22.225705289205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.530806002822" Y="-28.301851731905" />
                  <Point X="-3.992142101223" Y="-22.306641747401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.590688978312" Y="-28.388131375848" />
                  <Point X="-3.421503210015" Y="-23.485017299749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.588129913385" Y="-23.196411383614" />
                  <Point X="-4.048551054907" Y="-22.398938573619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.642756342455" Y="-28.487948055736" />
                  <Point X="-3.457121084448" Y="-23.613325331573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.785078539629" Y="-23.045286356479" />
                  <Point X="-4.103698985358" Y="-22.493419556146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.677528703638" Y="-28.617720559467" />
                  <Point X="-3.514133930984" Y="-23.704576184689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.982025971" Y="-22.894163398924" />
                  <Point X="-4.15884659558" Y="-22.587901093325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.712301064822" Y="-28.747493063197" />
                  <Point X="-3.593115363948" Y="-23.75777632994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.178971972888" Y="-22.743042917306" />
                  <Point X="-4.213993835343" Y="-22.682383272158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.747073426006" Y="-28.877265566928" />
                  <Point X="-3.685330711852" Y="-23.788054662133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.78184578719" Y="-29.007038070659" />
                  <Point X="-3.79246104357" Y="-23.792499484566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.816618170562" Y="-29.136810535957" />
                  <Point X="-3.911181431504" Y="-23.77686974077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.851390608533" Y="-29.266582906689" />
                  <Point X="-3.293973344154" Y="-25.035905506902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.440876122598" Y="-24.781462430865" />
                  <Point X="-4.029901819438" Y="-23.761239996974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.886163046503" Y="-29.396355277421" />
                  <Point X="-3.328357098027" Y="-25.16635109824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.579536213838" Y="-24.731296107854" />
                  <Point X="-4.148622207372" Y="-23.745610253178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.920935484474" Y="-29.526127648154" />
                  <Point X="-3.392015918489" Y="-25.24609078685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.709308375874" Y="-24.6965241298" />
                  <Point X="-4.267342595306" Y="-23.729980509382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.955707922444" Y="-29.655900018886" />
                  <Point X="-1.163430556944" Y="-29.29611386205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.339743469589" Y="-28.990730939318" />
                  <Point X="-3.471855417026" Y="-25.297804718932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.83908053791" Y="-24.661752151746" />
                  <Point X="-4.38606298324" Y="-23.714350765586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.003385293475" Y="-29.763320389889" />
                  <Point X="-1.119924226325" Y="-29.561469037132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.546435279663" Y="-28.822730222762" />
                  <Point X="-2.950773367479" Y="-26.390345303661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103820187286" Y="-26.125260435817" />
                  <Point X="-3.564551892676" Y="-25.327249713425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.968852699946" Y="-24.626980173692" />
                  <Point X="-4.504783461453" Y="-23.698720865422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.129862704048" Y="-29.734255088767" />
                  <Point X="-1.139446325214" Y="-29.717655769987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.672829122952" Y="-28.793809664421" />
                  <Point X="-2.32106714311" Y="-27.67102847811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.502017484883" Y="-27.357613292511" />
                  <Point X="-2.992333650325" Y="-26.508360782195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.228418185273" Y="-26.099450372783" />
                  <Point X="-3.659551910992" Y="-25.35270485498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.098624861982" Y="-24.592208195637" />
                  <Point X="-4.62350395867" Y="-23.683090932342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.786839587144" Y="-28.786337747846" />
                  <Point X="-2.370800268597" Y="-27.774888177947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.613723654239" Y="-27.354132531668" />
                  <Point X="-3.059263978273" Y="-26.58243405362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.331089012191" Y="-26.111619284106" />
                  <Point X="-3.754551929309" Y="-25.378159996536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.228397393383" Y="-24.557435577824" />
                  <Point X="-4.665650122524" Y="-23.800091635203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.900850601181" Y="-28.778864878912" />
                  <Point X="-2.42564852705" Y="-27.8698882076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703499418785" Y="-27.388636346187" />
                  <Point X="-3.135282928969" Y="-26.640765368677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.433036698842" Y="-26.125040711112" />
                  <Point X="-3.849551947625" Y="-25.403615138092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.358169978397" Y="-24.522662867149" />
                  <Point X="-4.700690183411" Y="-23.929400469446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.00866549862" Y="-28.782123998734" />
                  <Point X="-2.480496785503" Y="-27.964888237253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.785771851844" Y="-27.436136312067" />
                  <Point X="-3.211301879665" Y="-26.699096683734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.534984385493" Y="-26.138462138119" />
                  <Point X="-3.944551965942" Y="-25.429070279647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.487942563412" Y="-24.487890156475" />
                  <Point X="-4.734141102323" Y="-24.061461778332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.097945168802" Y="-28.817487073895" />
                  <Point X="-2.535345043955" Y="-28.059888266906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868044284902" Y="-27.483636277946" />
                  <Point X="-3.28732083036" Y="-26.757427998792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.636932072144" Y="-26.151883565125" />
                  <Point X="-4.039551984259" Y="-25.454525421203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.617715148426" Y="-24.4531174458" />
                  <Point X="-4.756520650639" Y="-24.212699263598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.177104563175" Y="-28.870378980946" />
                  <Point X="-2.590193302408" Y="-28.154888296559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950316717961" Y="-27.531136243826" />
                  <Point X="-3.363339781056" Y="-26.815759313849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.738879758795" Y="-26.165304992132" />
                  <Point X="-4.134552002575" Y="-25.479980562759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.747487733441" Y="-24.418344735126" />
                  <Point X="-4.77889996484" Y="-24.363937154363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.256263904881" Y="-28.923270979216" />
                  <Point X="-2.645041560861" Y="-28.249888326212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.03258915102" Y="-27.578636209706" />
                  <Point X="-3.439358731751" Y="-26.874090628906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.840827445446" Y="-26.178726419138" />
                  <Point X="-4.229552020892" Y="-25.505435704314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.335422800347" Y="-28.976163750399" />
                  <Point X="-2.699889819314" Y="-28.344888355866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.114861584079" Y="-27.626136175585" />
                  <Point X="-3.515377682447" Y="-26.932421943963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.942775094891" Y="-26.192147910587" />
                  <Point X="-4.324552039209" Y="-25.53089084587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.408577766665" Y="-29.03945563191" />
                  <Point X="-2.754738077767" Y="-28.439888385519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.197134017138" Y="-27.673636141465" />
                  <Point X="-3.591396633142" Y="-26.99075325902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.044722665838" Y="-26.205569537997" />
                  <Point X="-4.419551981724" Y="-25.556346118717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.471271273196" Y="-29.120867293294" />
                  <Point X="-2.809586307638" Y="-28.534888464676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.279406380772" Y="-27.721136227592" />
                  <Point X="-3.667415583838" Y="-27.049084574078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146670236786" Y="-26.218991165408" />
                  <Point X="-4.514551789985" Y="-25.5818016241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586844480737" Y="-29.11068862584" />
                  <Point X="-2.8644345103" Y="-28.629888590962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361678726528" Y="-27.768636344684" />
                  <Point X="-3.743434534533" Y="-27.107415889135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.248617807734" Y="-26.232412792818" />
                  <Point X="-4.609551598246" Y="-25.607257129482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75913434549" Y="-29.002273826458" />
                  <Point X="-2.919282712962" Y="-28.724888717247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443951072284" Y="-27.816136461777" />
                  <Point X="-3.81945348471" Y="-27.165747205091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.350565378681" Y="-26.245834420229" />
                  <Point X="-4.704551406508" Y="-25.632712634864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956623291508" Y="-28.850212938021" />
                  <Point X="-2.974130915624" Y="-28.819888843533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.526223418041" Y="-27.863636578869" />
                  <Point X="-3.895472408658" Y="-27.224078566477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.452512949629" Y="-26.259256047639" />
                  <Point X="-4.781471463462" Y="-25.689483188098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.608495763797" Y="-27.911136695962" />
                  <Point X="-3.971491332605" Y="-27.282409927863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.554460520576" Y="-26.27267767505" />
                  <Point X="-4.745348907046" Y="-25.94204929111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.690768109553" Y="-27.958636813054" />
                  <Point X="-4.047510256553" Y="-27.340741289249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.656408091524" Y="-26.28609930246" />
                  <Point X="-4.664772920428" Y="-26.271610993801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.773040455309" Y="-28.006136930146" />
                  <Point X="-4.123529180501" Y="-27.399072650635" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.708303161621" Y="-29.420728515625" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318969727" Y="-28.521544921875" />
                  <Point X="0.367547454834" Y="-28.3821171875" />
                  <Point X="0.300591186523" Y="-28.28564453125" />
                  <Point X="0.274336364746" Y="-28.266400390625" />
                  <Point X="0.124588058472" Y="-28.21992578125" />
                  <Point X="0.020977233887" Y="-28.187767578125" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.158412597656" Y="-28.2342421875" />
                  <Point X="-0.262023590088" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.385050537109" Y="-28.425072265625" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.623044250488" Y="-29.148486328125" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-0.979142578125" Y="-29.961572265625" />
                  <Point X="-1.10023059082" Y="-29.938068359375" />
                  <Point X="-1.273559204102" Y="-29.89347265625" />
                  <Point X="-1.351589599609" Y="-29.873396484375" />
                  <Point X="-1.337651123047" Y="-29.7675234375" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.345458129883" Y="-29.35490625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.524151855469" Y="-29.081720703125" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240356445" Y="-28.985763671875" />
                  <Point X="-1.832223510742" Y="-28.973771484375" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989878662109" Y="-28.973791015625" />
                  <Point X="-2.142350097656" Y="-29.07566796875" />
                  <Point X="-2.247845214844" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.351704833984" Y="-29.277154296875" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.678323242188" Y="-29.277521484375" />
                  <Point X="-2.855839599609" Y="-29.167607421875" />
                  <Point X="-3.095822021484" Y="-28.982828125" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.946032958984" Y="-28.391220703125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513981933594" Y="-27.56876171875" />
                  <Point X="-2.531330566406" Y="-27.5514140625" />
                  <Point X="-2.560159179688" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.118981933594" Y="-27.84790625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.021460449219" Y="-28.031380859375" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.333755859375" Y="-27.558625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.933642822266" Y="-27.013876953125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822265625" Y="-26.396015625" />
                  <Point X="-3.138117675781" Y="-26.366267578125" />
                  <Point X="-3.140326660156" Y="-26.334595703125" />
                  <Point X="-3.161162109375" Y="-26.310638671875" />
                  <Point X="-3.187644775391" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.885193359375" Y="-26.37620703125" />
                  <Point X="-4.803283691406" Y="-26.497076171875" />
                  <Point X="-4.87254296875" Y="-26.225927734375" />
                  <Point X="-4.927393066406" Y="-26.011193359375" />
                  <Point X="-4.9729140625" Y="-25.692916015625" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.434445800781" Y="-25.3636328125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.525493164062" Y="-25.110041015625" />
                  <Point X="-3.502324707031" Y="-25.090646484375" />
                  <Point X="-3.489431152344" Y="-25.058291015625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.491115722656" Y="-24.998841796875" />
                  <Point X="-3.502324707031" Y="-24.9719140625" />
                  <Point X="-3.530546875" Y="-24.94901171875" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.164248535156" Y="-24.771328125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.952995117188" Y="-24.24247265625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.826013183594" Y="-23.66543359375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.381908203125" Y="-23.5232578125" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731705322266" Y="-23.6041328125" />
                  <Point X="-3.695399414062" Y="-23.592685546875" />
                  <Point X="-3.670279296875" Y="-23.584765625" />
                  <Point X="-3.651534912109" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.624552246094" Y="-23.521044921875" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316650391" Y="-23.454486328125" />
                  <Point X="-3.633894287109" Y="-23.42071875" />
                  <Point X="-3.646056396484" Y="-23.397357421875" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.008021240234" Y="-23.11370703125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.29737109375" Y="-22.448318359375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.917284423828" Y="-21.901" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.545251464844" Y="-21.85014453125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514404297" Y="-22.07956640625" />
                  <Point X="-3.087950195312" Y="-22.083990234375" />
                  <Point X="-3.052965087891" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.977362060547" Y="-22.036705078125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.942498046875" Y="-21.921595703125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.106300292969" Y="-21.598828125" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-2.992107177734" Y="-21.009083984375" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.370583984375" Y="-20.613275390625" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.086021972656" Y="-20.558392578125" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247802734" Y="-20.726337890625" />
                  <Point X="-1.894970092773" Y="-20.755634765625" />
                  <Point X="-1.856031738281" Y="-20.77590625" />
                  <Point X="-1.835125" Y="-20.781509765625" />
                  <Point X="-1.813809204102" Y="-20.77775" />
                  <Point X="-1.755192260742" Y="-20.75346875" />
                  <Point X="-1.714635375977" Y="-20.736669921875" />
                  <Point X="-1.696905395508" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.667004516602" Y="-20.645001953125" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.669452148438" Y="-20.44838671875" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.277793823242" Y="-20.183533203125" />
                  <Point X="-0.968083312988" Y="-20.096703125" />
                  <Point X="-0.504640258789" Y="-20.042462890625" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.156357818604" Y="-20.26283203125" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282119751" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594036102" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.133479400635" Y="-20.3941640625" />
                  <Point X="0.236648422241" Y="-20.009130859375" />
                  <Point X="0.589791503906" Y="-20.046115234375" />
                  <Point X="0.860210021973" Y="-20.074435546875" />
                  <Point X="1.243633300781" Y="-20.167005859375" />
                  <Point X="1.508455322266" Y="-20.230943359375" />
                  <Point X="1.758397094727" Y="-20.32159765625" />
                  <Point X="1.931045410156" Y="-20.38421875" />
                  <Point X="2.172360107422" Y="-20.49707421875" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.571830566406" Y="-20.710689453125" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.952396972656" Y="-20.960671875" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.739119628906" Y="-21.614328125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.212162353516" Y="-22.555939453125" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.206992675781" Y="-22.648708984375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682861328" Y="-22.699189453125" />
                  <Point X="2.244072998047" Y="-22.736609375" />
                  <Point X="2.261640625" Y="-22.762498046875" />
                  <Point X="2.274938964844" Y="-22.775794921875" />
                  <Point X="2.312357421875" Y="-22.801185546875" />
                  <Point X="2.338247558594" Y="-22.81875390625" />
                  <Point X="2.360336425781" Y="-22.827021484375" />
                  <Point X="2.401370117188" Y="-22.83196875" />
                  <Point X="2.429761230469" Y="-22.835392578125" />
                  <Point X="2.448666015625" Y="-22.8340546875" />
                  <Point X="2.496119384766" Y="-22.821365234375" />
                  <Point X="2.528952392578" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.151342285156" Y="-22.455220703125" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.107273925781" Y="-22.125650390625" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.32516796875" Y="-22.4606796875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.959112792969" Y="-22.892427734375" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371582031" Y="-23.41616796875" />
                  <Point X="3.245219238281" Y="-23.46072265625" />
                  <Point X="3.221589355469" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.200397949219" Y="-23.553990234375" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.20122265625" Y="-23.6596484375" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646728516" Y="-23.712046875" />
                  <Point X="3.244051269531" Y="-23.755220703125" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280946289062" Y="-23.8011796875" />
                  <Point X="3.322108154297" Y="-23.824349609375" />
                  <Point X="3.350588378906" Y="-23.8403828125" />
                  <Point X="3.368567138672" Y="-23.846380859375" />
                  <Point X="3.424220947266" Y="-23.853736328125" />
                  <Point X="3.462727783203" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.055555664062" Y="-23.782501953125" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.89825" Y="-23.880453125" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.977815429688" Y="-24.2967109375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.509571289062" Y="-24.556279296875" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.674408935547" Y="-24.79878515625" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.622264892578" Y="-24.833072265625" />
                  <Point X="3.589458007812" Y="-24.874876953125" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.546049316406" Y="-24.9823671875" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.549418945312" Y="-25.097787109375" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.599565917969" Y="-25.200564453125" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.636577148438" Y="-25.241908203125" />
                  <Point X="3.691255371094" Y="-25.273513671875" />
                  <Point X="3.729087158203" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.272826660156" Y="-25.442845703125" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.971290039062" Y="-25.81478515625" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.898943847656" Y="-26.18326171875" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.303114257812" Y="-26.21494921875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.3948359375" Y="-26.098341796875" />
                  <Point X="3.287522216797" Y="-26.121666015625" />
                  <Point X="3.213271728516" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.120580810547" Y="-26.232708984375" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.055061279297" Y="-26.415099609375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.115749511719" Y="-26.608998046875" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.661843505859" Y="-27.064126953125" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.26856640625" Y="-27.697876953125" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.101787597656" Y="-27.94755859375" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.547127685547" Y="-27.717443359375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.609620117188" Y="-27.230248046875" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.382973388672" Y="-27.275087890625" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.2327578125" Y="-27.440787109375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.212229248047" Y="-27.674095703125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.551139648438" Y="-28.32772265625" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.911756835938" Y="-29.135599609375" />
                  <Point X="2.835294433594" Y="-29.19021484375" />
                  <Point X="2.720871337891" Y="-29.264279296875" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="2.288823486328" Y="-28.781380859375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549194336" Y="-27.980466796875" />
                  <Point X="1.544582275391" Y="-27.899482421875" />
                  <Point X="1.45742578125" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.288038574219" Y="-27.84839453125" />
                  <Point X="1.192718017578" Y="-27.857166015625" />
                  <Point X="1.165332275391" Y="-27.868509765625" />
                  <Point X="1.058952636719" Y="-27.9569609375" />
                  <Point X="0.985348632813" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.936649841309" Y="-28.19232421875" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.003778442383" Y="-28.993189453125" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.066680175781" Y="-29.947392578125" />
                  <Point X="0.994364868164" Y="-29.963244140625" />
                  <Point X="0.88863873291" Y="-29.982451171875" />
                  <Point X="0.860200683594" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#176" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.114217428061" Y="4.781389739961" Z="1.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.5" />
                  <Point X="-0.51660836997" Y="5.038478021476" Z="1.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.5" />
                  <Point X="-1.297447429651" Y="4.895891124694" Z="1.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.5" />
                  <Point X="-1.725180423146" Y="4.576368674178" Z="1.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.5" />
                  <Point X="-1.720846455202" Y="4.401313829153" Z="1.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.5" />
                  <Point X="-1.780482195911" Y="4.324004678734" Z="1.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.5" />
                  <Point X="-1.878037582872" Y="4.319994840552" Z="1.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.5" />
                  <Point X="-2.052510392617" Y="4.503326363812" Z="1.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.5" />
                  <Point X="-2.401023016381" Y="4.461712159474" Z="1.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.5" />
                  <Point X="-3.028685220761" Y="4.061875362072" Z="1.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.5" />
                  <Point X="-3.155757512017" Y="3.407451984082" Z="1.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.5" />
                  <Point X="-2.998463838102" Y="3.105327603337" Z="1.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.5" />
                  <Point X="-3.018872833079" Y="3.029930734808" Z="1.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.5" />
                  <Point X="-3.08974886227" Y="2.997100861071" Z="1.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.5" />
                  <Point X="-3.526407364215" Y="3.224436436729" Z="1.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.5" />
                  <Point X="-3.962904007647" Y="3.160983959707" Z="1.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.5" />
                  <Point X="-4.346709339722" Y="2.608166395162" Z="1.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.5" />
                  <Point X="-4.044615294269" Y="1.877904555007" Z="1.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.5" />
                  <Point X="-3.684400211176" Y="1.587470982221" Z="1.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.5" />
                  <Point X="-3.676901988752" Y="1.529370180355" Z="1.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.5" />
                  <Point X="-3.716590063909" Y="1.486279763422" Z="1.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.5" />
                  <Point X="-4.381538012644" Y="1.557594841571" Z="1.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.5" />
                  <Point X="-4.880428857307" Y="1.378925927194" Z="1.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.5" />
                  <Point X="-5.008612272262" Y="0.796126579437" Z="1.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.5" />
                  <Point X="-4.18334520555" Y="0.211656443249" Z="1.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.5" />
                  <Point X="-3.565211229972" Y="0.041191826964" Z="1.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.5" />
                  <Point X="-3.545024625843" Y="0.017617433894" Z="1.5" />
                  <Point X="-3.539556741714" Y="0" Z="1.5" />
                  <Point X="-3.543339975292" Y="-0.012189517167" Z="1.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.5" />
                  <Point X="-3.560157365151" Y="-0.037684156169" Z="1.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.5" />
                  <Point X="-4.453542583222" Y="-0.284055613507" Z="1.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.5" />
                  <Point X="-5.028565909203" Y="-0.668713592562" Z="1.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.5" />
                  <Point X="-4.927143487974" Y="-1.207022637533" Z="1.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.5" />
                  <Point X="-3.884823038104" Y="-1.394499639076" Z="1.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.5" />
                  <Point X="-3.20832886743" Y="-1.313237386699" Z="1.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.5" />
                  <Point X="-3.195827050399" Y="-1.33461495365" Z="1.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.5" />
                  <Point X="-3.970236678889" Y="-1.942928336394" Z="1.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.5" />
                  <Point X="-4.382855426339" Y="-2.552953154283" Z="1.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.5" />
                  <Point X="-4.067402204988" Y="-3.030383677378" Z="1.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.5" />
                  <Point X="-3.100137842852" Y="-2.859926807427" Z="1.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.5" />
                  <Point X="-2.565744838676" Y="-2.562585630455" Z="1.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.5" />
                  <Point X="-2.995490205741" Y="-3.33494053964" Z="1.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.5" />
                  <Point X="-3.132481735136" Y="-3.991165423629" Z="1.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.5" />
                  <Point X="-2.710800679417" Y="-4.288752349716" Z="1.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.5" />
                  <Point X="-2.318193014508" Y="-4.276310736146" Z="1.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.5" />
                  <Point X="-2.120727273461" Y="-4.085962567676" Z="1.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.5" />
                  <Point X="-1.841649883209" Y="-3.992382579868" Z="1.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.5" />
                  <Point X="-1.563275093932" Y="-4.088032372641" Z="1.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.5" />
                  <Point X="-1.400653353005" Y="-4.333380244887" Z="1.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.5" />
                  <Point X="-1.393379328191" Y="-4.729717152762" Z="1.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.5" />
                  <Point X="-1.292174027456" Y="-4.910616207379" Z="1.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.5" />
                  <Point X="-0.994826074654" Y="-4.979375959997" Z="1.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.5" />
                  <Point X="-0.580904026132" Y="-4.130147408069" Z="1.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.5" />
                  <Point X="-0.350130531697" Y="-3.422301890207" Z="1.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.5" />
                  <Point X="-0.149748185347" Y="-3.250715678946" Z="1.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.5" />
                  <Point X="0.103610894014" Y="-3.236396366342" Z="1.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.5" />
                  <Point X="0.320315327735" Y="-3.379343865561" Z="1.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.5" />
                  <Point X="0.653850834269" Y="-4.402388621747" Z="1.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.5" />
                  <Point X="0.891418758611" Y="-5.000365144281" Z="1.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.5" />
                  <Point X="1.071229505152" Y="-4.964951738448" Z="1.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.5" />
                  <Point X="1.047194778021" Y="-3.955384480999" Z="1.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.5" />
                  <Point X="0.979353049992" Y="-3.171663006934" Z="1.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.5" />
                  <Point X="1.084764150347" Y="-2.964126589518" Z="1.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.5" />
                  <Point X="1.286464281873" Y="-2.866903957892" Z="1.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.5" />
                  <Point X="1.511387033833" Y="-2.910260215575" Z="1.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.5" />
                  <Point X="2.242999780416" Y="-3.780538190829" Z="1.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.5" />
                  <Point X="2.741884482515" Y="-4.274973142554" Z="1.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.5" />
                  <Point X="2.934662652776" Y="-4.145006790553" Z="1.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.5" />
                  <Point X="2.588284974437" Y="-3.271441771714" Z="1.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.5" />
                  <Point X="2.255277348776" Y="-2.633928693528" Z="1.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.5" />
                  <Point X="2.270848221957" Y="-2.432794515286" Z="1.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.5" />
                  <Point X="2.400103863086" Y="-2.28805308801" Z="1.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.5" />
                  <Point X="2.594578109985" Y="-2.248170660715" Z="1.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.5" />
                  <Point X="3.515971825393" Y="-2.729464525838" Z="1.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.5" />
                  <Point X="4.136520413093" Y="-2.945055311705" Z="1.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.5" />
                  <Point X="4.304943981954" Y="-2.692881127803" Z="1.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.5" />
                  <Point X="3.686125545551" Y="-1.993178956942" Z="1.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.5" />
                  <Point X="3.151651518049" Y="-1.550678202286" Z="1.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.5" />
                  <Point X="3.098694842075" Y="-1.38840076883" Z="1.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.5" />
                  <Point X="3.152871164621" Y="-1.23339587609" Z="1.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.5" />
                  <Point X="3.291986046074" Y="-1.139245525865" Z="1.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.5" />
                  <Point X="4.290431805889" Y="-1.233240161961" Z="1.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.5" />
                  <Point X="4.941534962652" Y="-1.163106404936" Z="1.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.5" />
                  <Point X="5.014575374751" Y="-0.790960043491" Z="1.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.5" />
                  <Point X="4.279611678459" Y="-0.363268224354" Z="1.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.5" />
                  <Point X="3.710120851392" Y="-0.198943150459" Z="1.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.5" />
                  <Point X="3.63274360573" Y="-0.138414208365" Z="1.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.5" />
                  <Point X="3.592370354055" Y="-0.057101769452" Z="1.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.5" />
                  <Point X="3.589001096369" Y="0.039508761752" Z="1.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.5" />
                  <Point X="3.622635832671" Y="0.12553453021" Z="1.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.5" />
                  <Point X="3.69327456296" Y="0.189205693783" Z="1.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.5" />
                  <Point X="4.516356138848" Y="0.426703708415" Z="1.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.5" />
                  <Point X="5.021064477831" Y="0.742260855977" Z="1.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.5" />
                  <Point X="4.940675156983" Y="1.162654306982" Z="1.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.5" />
                  <Point X="4.042873440884" Y="1.298349908928" Z="1.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.5" />
                  <Point X="3.424614469031" Y="1.227113245031" Z="1.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.5" />
                  <Point X="3.340480578563" Y="1.250500375268" Z="1.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.5" />
                  <Point X="3.279665471616" Y="1.30354284935" Z="1.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.5" />
                  <Point X="3.244035294685" Y="1.381735836052" Z="1.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.5" />
                  <Point X="3.242394228142" Y="1.463823744711" Z="1.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.5" />
                  <Point X="3.278745936824" Y="1.540140835676" Z="1.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.5" />
                  <Point X="3.983394737667" Y="2.099185203215" Z="1.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.5" />
                  <Point X="4.361789271135" Y="2.596488341688" Z="1.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.5" />
                  <Point X="4.141703621415" Y="2.934833199327" Z="1.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.5" />
                  <Point X="3.120186270931" Y="2.619360438365" Z="1.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.5" />
                  <Point X="2.477044947855" Y="2.258218803324" Z="1.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.5" />
                  <Point X="2.401200438728" Y="2.248952629713" Z="1.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.5" />
                  <Point X="2.334276764102" Y="2.271467964588" Z="1.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.5" />
                  <Point X="2.279290635647" Y="2.32274809628" Z="1.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.5" />
                  <Point X="2.250477073045" Y="2.388558005105" Z="1.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.5" />
                  <Point X="2.254309139334" Y="2.462424628027" Z="1.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.5" />
                  <Point X="2.776264914494" Y="3.391952811024" Z="1.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.5" />
                  <Point X="2.975218130709" Y="4.111356779246" Z="1.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.5" />
                  <Point X="2.590844535407" Y="4.36379419278" Z="1.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.5" />
                  <Point X="2.187385620676" Y="4.579497740662" Z="1.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.5" />
                  <Point X="1.769290005479" Y="4.756686382357" Z="1.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.5" />
                  <Point X="1.24921189048" Y="4.912877961519" Z="1.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.5" />
                  <Point X="0.588843457614" Y="5.034893262422" Z="1.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.5" />
                  <Point X="0.079027133492" Y="4.65005776599" Z="1.5" />
                  <Point X="0" Y="4.355124473572" Z="1.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>