<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#185" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2548" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995282226562" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.831607849121" Y="-29.51385546875" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.54236315918" Y="-28.467375" />
                  <Point X="0.432699615479" Y="-28.30937109375" />
                  <Point X="0.378635375977" Y="-28.231474609375" />
                  <Point X="0.356751739502" Y="-28.20901953125" />
                  <Point X="0.33049609375" Y="-28.189775390625" />
                  <Point X="0.302494873047" Y="-28.17566796875" />
                  <Point X="0.132797286987" Y="-28.123" />
                  <Point X="0.049135883331" Y="-28.09703515625" />
                  <Point X="0.020976488113" Y="-28.092765625" />
                  <Point X="-0.008664384842" Y="-28.092765625" />
                  <Point X="-0.036823932648" Y="-28.09703515625" />
                  <Point X="-0.20652166748" Y="-28.149703125" />
                  <Point X="-0.290183074951" Y="-28.17566796875" />
                  <Point X="-0.318184753418" Y="-28.18977734375" />
                  <Point X="-0.344439941406" Y="-28.209021484375" />
                  <Point X="-0.366323577881" Y="-28.2314765625" />
                  <Point X="-0.475986968994" Y="-28.389482421875" />
                  <Point X="-0.530051208496" Y="-28.467376953125" />
                  <Point X="-0.538188293457" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.683265441895" Y="-29.00618359375" />
                  <Point X="-0.916584899902" Y="-29.87694140625" />
                  <Point X="-0.984642272949" Y="-29.863732421875" />
                  <Point X="-1.079352539062" Y="-29.84534765625" />
                  <Point X="-1.246418212891" Y="-29.80236328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.257049438477" Y="-29.3124140625" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131205078125" />
                  <Point X="-1.479880737305" Y="-28.994189453125" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583189697266" Y="-28.910294921875" />
                  <Point X="-1.612887573242" Y="-28.897994140625" />
                  <Point X="-1.643027709961" Y="-28.890966796875" />
                  <Point X="-1.850387817383" Y="-28.877375" />
                  <Point X="-1.952616821289" Y="-28.87067578125" />
                  <Point X="-1.983415527344" Y="-28.873708984375" />
                  <Point X="-2.014463745117" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.215441162109" Y="-29.01025" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312791259766" Y="-29.076826171875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.409365234375" Y="-29.196244140625" />
                  <Point X="-2.480147705078" Y="-29.288490234375" />
                  <Point X="-2.662906982422" Y="-29.175330078125" />
                  <Point X="-2.8017265625" Y="-29.089376953125" />
                  <Point X="-3.070104492188" Y="-28.882732421875" />
                  <Point X="-3.104721923828" Y="-28.856078125" />
                  <Point X="-2.922377441406" Y="-28.540248046875" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406587890625" Y="-27.616125" />
                  <Point X="-2.405575683594" Y="-27.585189453125" />
                  <Point X="-2.414561279297" Y="-27.5555703125" />
                  <Point X="-2.428779052734" Y="-27.5267421875" />
                  <Point X="-2.446807128906" Y="-27.5015859375" />
                  <Point X="-2.464155761719" Y="-27.48423828125" />
                  <Point X="-2.489311523438" Y="-27.466212890625" />
                  <Point X="-2.518140136719" Y="-27.45199609375" />
                  <Point X="-2.547758544922" Y="-27.44301171875" />
                  <Point X="-2.578693115234" Y="-27.444025390625" />
                  <Point X="-2.610218994141" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.064954345703" Y="-27.707017578125" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.973198974609" Y="-27.93793359375" />
                  <Point X="-4.082862060547" Y="-27.793857421875" />
                  <Point X="-4.27527734375" Y="-27.47120703125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.97837109375" Y="-27.167943359375" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577148438" Y="-26.475595703125" />
                  <Point X="-3.066612548828" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.048696044922" Y="-26.399908203125" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.043347900391" Y="-26.359658203125" />
                  <Point X="-3.045556640625" Y="-26.327986328125" />
                  <Point X="-3.052558105469" Y="-26.298240234375" />
                  <Point X="-3.068641601562" Y="-26.272255859375" />
                  <Point X="-3.089475585938" Y="-26.248298828125" />
                  <Point X="-3.112974853516" Y="-26.228765625" />
                  <Point X="-3.130712646484" Y="-26.218326171875" />
                  <Point X="-3.139457519531" Y="-26.2131796875" />
                  <Point X="-3.168722412109" Y="-26.201955078125" />
                  <Point X="-3.200608642578" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-3.769424560547" Y="-26.265146484375" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.7911875" Y="-26.16056640625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.884986328125" Y="-25.63670703125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.526689941406" Y="-25.486701171875" />
                  <Point X="-3.532875732422" Y="-25.22041015625" />
                  <Point X="-3.517485351562" Y="-25.21482421875" />
                  <Point X="-3.4877265625" Y="-25.199466796875" />
                  <Point X="-3.469151367188" Y="-25.18657421875" />
                  <Point X="-3.459986816406" Y="-25.18021484375" />
                  <Point X="-3.437527832031" Y="-25.15833203125" />
                  <Point X="-3.418280761719" Y="-25.132076171875" />
                  <Point X="-3.404168457031" Y="-25.1040703125" />
                  <Point X="-3.397972167969" Y="-25.08410546875" />
                  <Point X="-3.394917480469" Y="-25.074263671875" />
                  <Point X="-3.390648193359" Y="-25.046107421875" />
                  <Point X="-3.390647216797" Y="-25.016466796875" />
                  <Point X="-3.394916503906" Y="-24.988302734375" />
                  <Point X="-3.401112792969" Y="-24.968337890625" />
                  <Point X="-3.404167480469" Y="-24.958494140625" />
                  <Point X="-3.418272949219" Y="-24.930498046875" />
                  <Point X="-3.437515136719" Y="-24.9042421875" />
                  <Point X="-3.4599765625" Y="-24.8823515625" />
                  <Point X="-3.478565185547" Y="-24.869451171875" />
                  <Point X="-3.486999023438" Y="-24.864228515625" />
                  <Point X="-3.511733886719" Y="-24.85064453125" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-4.022828613281" Y="-24.710869140625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.85212890625" Y="-24.2098203125" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.722010253906" Y="-23.644853515625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.478056640625" Y="-23.606419921875" />
                  <Point X="-3.765666503906" Y="-23.70020703125" />
                  <Point X="-3.744986083984" Y="-23.700658203125" />
                  <Point X="-3.723421875" Y="-23.698771484375" />
                  <Point X="-3.703138183594" Y="-23.694736328125" />
                  <Point X="-3.661995605469" Y="-23.681765625" />
                  <Point X="-3.641712158203" Y="-23.675369140625" />
                  <Point X="-3.622772216797" Y="-23.66703515625" />
                  <Point X="-3.604029541016" Y="-23.656212890625" />
                  <Point X="-3.587350341797" Y="-23.643982421875" />
                  <Point X="-3.573713378906" Y="-23.628431640625" />
                  <Point X="-3.561300048828" Y="-23.610703125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.534842773438" Y="-23.55271484375" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.471248046875" />
                  <Point X="-3.518951904297" Y="-23.4508046875" />
                  <Point X="-3.524554443359" Y="-23.4298984375" />
                  <Point X="-3.532050292969" Y="-23.410623046875" />
                  <Point X="-3.551969726562" Y="-23.372357421875" />
                  <Point X="-3.561790039062" Y="-23.353494140625" />
                  <Point X="-3.57328125" Y="-23.336294921875" />
                  <Point X="-3.587193115234" Y="-23.31971484375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-3.883174072266" Y="-23.08976171875" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.1885546875" Y="-22.45034375" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.8096953125" Y="-21.917419921875" />
                  <Point X="-3.750504150391" Y="-21.841337890625" />
                  <Point X="-3.644187255859" Y="-21.902720703125" />
                  <Point X="-3.206657226562" Y="-22.155328125" />
                  <Point X="-3.187727539062" Y="-22.163658203125" />
                  <Point X="-3.167083984375" Y="-22.17016796875" />
                  <Point X="-3.146794189453" Y="-22.174205078125" />
                  <Point X="-3.089493896484" Y="-22.17921875" />
                  <Point X="-3.061244873047" Y="-22.181689453125" />
                  <Point X="-3.040558837891" Y="-22.18123828125" />
                  <Point X="-3.019101074219" Y="-22.178412109375" />
                  <Point X="-2.999011474609" Y="-22.173494140625" />
                  <Point X="-2.980462158203" Y="-22.164345703125" />
                  <Point X="-2.962208740234" Y="-22.152716796875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.905405761719" Y="-22.099099609375" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.848448974609" Y="-21.906580078125" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-2.994331298828" Y="-21.602763671875" />
                  <Point X="-3.183332519531" Y="-21.275404296875" />
                  <Point X="-2.887679931641" Y="-21.04873046875" />
                  <Point X="-2.70062109375" Y="-20.905314453125" />
                  <Point X="-2.273089111328" Y="-20.667787109375" />
                  <Point X="-2.167036376953" Y="-20.608865234375" />
                  <Point X="-2.043195678711" Y="-20.7702578125" />
                  <Point X="-2.028890869141" Y="-20.785201171875" />
                  <Point X="-2.012310791016" Y="-20.79911328125" />
                  <Point X="-1.995114868164" Y="-20.810603515625" />
                  <Point X="-1.93133996582" Y="-20.8438046875" />
                  <Point X="-1.899898803711" Y="-20.860171875" />
                  <Point X="-1.880625244141" Y="-20.86766796875" />
                  <Point X="-1.859718994141" Y="-20.873271484375" />
                  <Point X="-1.839268798828" Y="-20.876419921875" />
                  <Point X="-1.818621826172" Y="-20.87506640625" />
                  <Point X="-1.797306884766" Y="-20.871306640625" />
                  <Point X="-1.77745300293" Y="-20.865517578125" />
                  <Point X="-1.711027099609" Y="-20.838001953125" />
                  <Point X="-1.678279052734" Y="-20.8244375" />
                  <Point X="-1.660147216797" Y="-20.814490234375" />
                  <Point X="-1.642417236328" Y="-20.802076171875" />
                  <Point X="-1.626864746094" Y="-20.7884375" />
                  <Point X="-1.614633056641" Y="-20.771755859375" />
                  <Point X="-1.603810913086" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.573859863281" Y="-20.6655078125" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.571888793945" Y="-20.461630859375" />
                  <Point X="-1.584201904297" Y="-20.368103515625" />
                  <Point X="-1.191788818359" Y="-20.258083984375" />
                  <Point X="-0.949623474121" Y="-20.19019140625" />
                  <Point X="-0.431335693359" Y="-20.12953125" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.26333682251" Y="-20.2306328125" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113983154" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907154" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425941467" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.210026275635" Y="-20.4755390625" />
                  <Point X="0.307419616699" Y="-20.112060546875" />
                  <Point X="0.632598205566" Y="-20.1461171875" />
                  <Point X="0.844031311035" Y="-20.168259765625" />
                  <Point X="1.272848022461" Y="-20.2717890625" />
                  <Point X="1.481038818359" Y="-20.3220546875" />
                  <Point X="1.759650268555" Y="-20.423107421875" />
                  <Point X="1.894647827148" Y="-20.47207421875" />
                  <Point X="2.164535400391" Y="-20.598291015625" />
                  <Point X="2.294557617188" Y="-20.65909765625" />
                  <Point X="2.555325439453" Y="-20.811021484375" />
                  <Point X="2.6809765625" Y="-20.884224609375" />
                  <Point X="2.926878173828" Y="-21.05909765625" />
                  <Point X="2.943259277344" Y="-21.07074609375" />
                  <Point X="2.724691894531" Y="-21.44931640625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.118696533203" Y="-22.537720703125" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.113335205078" Y="-22.665548828125" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121443115234" Y="-22.7103984375" />
                  <Point X="2.129709716797" Y="-22.732486328125" />
                  <Point X="2.140071044922" Y="-22.75252734375" />
                  <Point X="2.16884375" Y="-22.794931640625" />
                  <Point X="2.183028808594" Y="-22.8158359375" />
                  <Point X="2.194465332031" Y="-22.829671875" />
                  <Point X="2.221597167969" Y="-22.85440625" />
                  <Point X="2.264000732422" Y="-22.8831796875" />
                  <Point X="2.284905761719" Y="-22.897365234375" />
                  <Point X="2.304943359375" Y="-22.907724609375" />
                  <Point X="2.327029785156" Y="-22.9159921875" />
                  <Point X="2.348964599609" Y="-22.921337890625" />
                  <Point X="2.39546484375" Y="-22.926943359375" />
                  <Point X="2.418389404297" Y="-22.929708984375" />
                  <Point X="2.436465087891" Y="-22.93015625" />
                  <Point X="2.473208007812" Y="-22.925830078125" />
                  <Point X="2.526983154297" Y="-22.91144921875" />
                  <Point X="2.553494384766" Y="-22.904359375" />
                  <Point X="2.5652890625" Y="-22.900359375" />
                  <Point X="2.588533691406" Y="-22.88985546875" />
                  <Point X="3.081331542969" Y="-22.60533984375" />
                  <Point X="3.967326904297" Y="-22.093810546875" />
                  <Point X="4.04873828125" Y="-22.206953125" />
                  <Point X="4.123274414062" Y="-22.310541015625" />
                  <Point X="4.260357910156" Y="-22.53707421875" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.990604248047" Y="-22.748517578125" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221420654297" Y="-23.339763671875" />
                  <Point X="3.203974121094" Y="-23.358373046875" />
                  <Point X="3.165271972656" Y="-23.40886328125" />
                  <Point X="3.146191894531" Y="-23.43375390625" />
                  <Point X="3.136607666016" Y="-23.4490859375" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.107213378906" Y="-23.53446484375" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.109573974609" Y="-23.685587890625" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120680175781" Y="-23.7310234375" />
                  <Point X="3.136282958984" Y="-23.76426171875" />
                  <Point X="3.168471435547" Y="-23.8131875" />
                  <Point X="3.184340576172" Y="-23.837306640625" />
                  <Point X="3.198895996094" Y="-23.854552734375" />
                  <Point X="3.216138671875" Y="-23.870640625" />
                  <Point X="3.234346191406" Y="-23.88396484375" />
                  <Point X="3.280991699219" Y="-23.91022265625" />
                  <Point X="3.30398828125" Y="-23.92316796875" />
                  <Point X="3.320520507812" Y="-23.930498046875" />
                  <Point X="3.356120117188" Y="-23.9405625" />
                  <Point X="3.419187988281" Y="-23.948896484375" />
                  <Point X="3.450280761719" Y="-23.953005859375" />
                  <Point X="3.462697753906" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="3.956328613281" Y="-23.89138671875" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.813924316406" Y="-23.935697265625" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.889135742188" Y="-24.34465625" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.587350585938" Y="-24.437087890625" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704786865234" Y="-24.674416015625" />
                  <Point X="3.681547607422" Y="-24.684931640625" />
                  <Point X="3.619585205078" Y="-24.72074609375" />
                  <Point X="3.589037597656" Y="-24.73840234375" />
                  <Point X="3.574311279297" Y="-24.748904296875" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.510352783203" Y="-24.821796875" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480300537109" Y="-24.86443359375" />
                  <Point X="3.470527099609" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.451288085938" Y="-24.97210546875" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.457571289062" Y="-25.123263671875" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.47052734375" Y="-25.1766640625" />
                  <Point X="3.48030078125" Y="-25.198126953125" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.529201660156" Y="-25.26478125" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.559997558594" Y="-25.301234375" />
                  <Point X="3.589035400391" Y="-25.32415625" />
                  <Point X="3.650997802734" Y="-25.359970703125" />
                  <Point X="3.681545410156" Y="-25.37762890625" />
                  <Point X="3.692698486328" Y="-25.383134765625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.145872070313" Y="-25.5071796875" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.872896484375" Y="-25.830173828125" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.434230957031" Y="-26.136390625" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.253049072266" Y="-26.03194140625" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163973632812" Y="-26.05659765625" />
                  <Point X="3.136146972656" Y="-26.073490234375" />
                  <Point X="3.112397460938" Y="-26.0939609375" />
                  <Point X="3.038891845703" Y="-26.182365234375" />
                  <Point X="3.002653320312" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.959222412109" Y="-26.419853515625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.95634765625" Y="-26.50756640625" />
                  <Point X="2.964079589844" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.043751464844" Y="-26.6726796875" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930419922" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.509013671875" Y="-27.066603515625" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.175194824219" Y="-27.668255859375" />
                  <Point X="4.124813964844" Y="-27.74978125" />
                  <Point X="4.028981445312" Y="-27.8859453125" />
                  <Point X="3.700361083984" Y="-27.696216796875" />
                  <Point X="2.800954345703" Y="-27.176943359375" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224365234" Y="-27.159826171875" />
                  <Point X="2.609488769531" Y="-27.1336875" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.506783935547" Y="-27.120396484375" />
                  <Point X="2.474611572266" Y="-27.125353515625" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.324593994141" Y="-27.198458984375" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242386474609" Y="-27.246548828125" />
                  <Point X="2.221426513672" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.141250732422" Y="-27.4106796875" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229492188" Y="-27.499732421875" />
                  <Point X="2.095271484375" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.121814208984" Y="-27.707994140625" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.407821777344" Y="-28.26948828125" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.841638916016" Y="-29.0689375" />
                  <Point X="2.781861328125" Y="-29.11163671875" />
                  <Point X="2.701764160156" Y="-29.16348046875" />
                  <Point X="2.444876708984" Y="-28.82869921875" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506469727" Y="-27.922181640625" />
                  <Point X="1.721923706055" Y="-27.900556640625" />
                  <Point X="1.579175415039" Y="-27.808783203125" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.75116796875" />
                  <Point X="1.448366088867" Y="-27.7434375" />
                  <Point X="1.417100708008" Y="-27.741119140625" />
                  <Point X="1.260981079102" Y="-27.755484375" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.156368164062" Y="-27.769396484375" />
                  <Point X="1.128982543945" Y="-27.78073828125" />
                  <Point X="1.104595214844" Y="-27.7954609375" />
                  <Point X="0.984043701172" Y="-27.895697265625" />
                  <Point X="0.92461151123" Y="-27.94511328125" />
                  <Point X="0.904139709473" Y="-27.968865234375" />
                  <Point X="0.887247558594" Y="-27.996693359375" />
                  <Point X="0.875624511719" Y="-28.025810546875" />
                  <Point X="0.83958001709" Y="-28.191642578125" />
                  <Point X="0.821810241699" Y="-28.273396484375" />
                  <Point X="0.81972479248" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.892291381836" Y="-28.874185546875" />
                  <Point X="1.022065490723" Y="-29.859916015625" />
                  <Point X="0.975713623047" Y="-29.870076171875" />
                  <Point X="0.929315429688" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058441894531" Y="-29.7526328125" />
                  <Point X="-1.141246459961" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.16387487793" Y="-29.293880859375" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124633789" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230575561523" Y="-29.094859375" />
                  <Point X="-1.250210327148" Y="-29.070935546875" />
                  <Point X="-1.261006958008" Y="-29.05978125" />
                  <Point X="-1.417242919922" Y="-28.922765625" />
                  <Point X="-1.494267578125" Y="-28.855216796875" />
                  <Point X="-1.506736572266" Y="-28.84596875" />
                  <Point X="-1.533020751953" Y="-28.829623046875" />
                  <Point X="-1.5468359375" Y="-28.822525390625" />
                  <Point X="-1.576533813477" Y="-28.810224609375" />
                  <Point X="-1.59131640625" Y="-28.8054765625" />
                  <Point X="-1.621456542969" Y="-28.79844921875" />
                  <Point X="-1.636813964844" Y="-28.796169921875" />
                  <Point X="-1.844174194336" Y="-28.782578125" />
                  <Point X="-1.946403198242" Y="-28.77587890625" />
                  <Point X="-1.961927856445" Y="-28.7761328125" />
                  <Point X="-1.99272644043" Y="-28.779166015625" />
                  <Point X="-2.008000610352" Y="-28.7819453125" />
                  <Point X="-2.039048828125" Y="-28.790263671875" />
                  <Point X="-2.053668212891" Y="-28.795494140625" />
                  <Point X="-2.081862060547" Y="-28.808267578125" />
                  <Point X="-2.095436523437" Y="-28.815810546875" />
                  <Point X="-2.268219970703" Y="-28.931259765625" />
                  <Point X="-2.353402832031" Y="-28.988177734375" />
                  <Point X="-2.359687744141" Y="-28.992759765625" />
                  <Point X="-2.380450683594" Y="-29.010138671875" />
                  <Point X="-2.402762207031" Y="-29.032775390625" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.484733886719" Y="-29.138412109375" />
                  <Point X="-2.503200927734" Y="-29.162478515625" />
                  <Point X="-2.612895751953" Y="-29.09455859375" />
                  <Point X="-2.747613037109" Y="-29.01114453125" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.840104980469" Y="-28.587748046875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413085938" Y="-27.634658203125" />
                  <Point X="-2.311638671875" Y="-27.619232421875" />
                  <Point X="-2.310626464844" Y="-27.588296875" />
                  <Point X="-2.314666992188" Y="-27.557611328125" />
                  <Point X="-2.323652587891" Y="-27.5279921875" />
                  <Point X="-2.329359863281" Y="-27.513548828125" />
                  <Point X="-2.343577636719" Y="-27.484720703125" />
                  <Point X="-2.351560546875" Y="-27.471404296875" />
                  <Point X="-2.369588623047" Y="-27.446248046875" />
                  <Point X="-2.379633789062" Y="-27.434408203125" />
                  <Point X="-2.396982421875" Y="-27.417060546875" />
                  <Point X="-2.408822265625" Y="-27.407015625" />
                  <Point X="-2.433978027344" Y="-27.388990234375" />
                  <Point X="-2.447293945312" Y="-27.381009765625" />
                  <Point X="-2.476122558594" Y="-27.36679296875" />
                  <Point X="-2.490563964844" Y="-27.3610859375" />
                  <Point X="-2.520182373047" Y="-27.3521015625" />
                  <Point X="-2.550869873047" Y="-27.3480625" />
                  <Point X="-2.581804443359" Y="-27.349076171875" />
                  <Point X="-2.597228515625" Y="-27.3508515625" />
                  <Point X="-2.628754394531" Y="-27.357123046875" />
                  <Point X="-2.643685058594" Y="-27.36138671875" />
                  <Point X="-2.672649658203" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.112454345703" Y="-27.62474609375" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-3.897605712891" Y="-27.88039453125" />
                  <Point X="-4.004021484375" Y="-27.740583984375" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.920538818359" Y="-27.2433125" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036482421875" Y="-26.5633125" />
                  <Point X="-3.015104980469" Y="-26.540392578125" />
                  <Point X="-3.005367431641" Y="-26.528044921875" />
                  <Point X="-2.987402832031" Y="-26.5009140625" />
                  <Point X="-2.979835693359" Y="-26.487126953125" />
                  <Point X="-2.967079589844" Y="-26.45849609375" />
                  <Point X="-2.961890625" Y="-26.44365234375" />
                  <Point X="-2.956730224609" Y="-26.4237265625" />
                  <Point X="-2.951552734375" Y="-26.398802734375" />
                  <Point X="-2.948748779297" Y="-26.368375" />
                  <Point X="-2.948578125" Y="-26.353048828125" />
                  <Point X="-2.950786865234" Y="-26.321376953125" />
                  <Point X="-2.953083740234" Y="-26.306220703125" />
                  <Point X="-2.960085205078" Y="-26.276474609375" />
                  <Point X="-2.971780029297" Y="-26.248240234375" />
                  <Point X="-2.987863525391" Y="-26.222255859375" />
                  <Point X="-2.996956787109" Y="-26.209916015625" />
                  <Point X="-3.017790771484" Y="-26.185958984375" />
                  <Point X="-3.028749023438" Y="-26.1752421875" />
                  <Point X="-3.052248291016" Y="-26.155708984375" />
                  <Point X="-3.064789306641" Y="-26.146892578125" />
                  <Point X="-3.082527099609" Y="-26.136453125" />
                  <Point X="-3.105436767578" Y="-26.12448046875" />
                  <Point X="-3.134701660156" Y="-26.113255859375" />
                  <Point X="-3.149801757812" Y="-26.108857421875" />
                  <Point X="-3.181687988281" Y="-26.102376953125" />
                  <Point X="-3.197304931641" Y="-26.10053125" />
                  <Point X="-3.228625488281" Y="-26.09944140625" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-3.781824462891" Y="-26.170958984375" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.699142578125" Y="-26.1370546875" />
                  <Point X="-4.740762207031" Y="-25.974115234375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.5021015625" Y="-25.57846484375" />
                  <Point X="-3.508287597656" Y="-25.312173828125" />
                  <Point X="-3.500464355469" Y="-25.3097109375" />
                  <Point X="-3.473918701172" Y="-25.29924609375" />
                  <Point X="-3.444159912109" Y="-25.283888671875" />
                  <Point X="-3.433558349609" Y="-25.277509765625" />
                  <Point X="-3.414983154297" Y="-25.2646171875" />
                  <Point X="-3.393690185547" Y="-25.2482578125" />
                  <Point X="-3.371231201172" Y="-25.226375" />
                  <Point X="-3.360909423828" Y="-25.214498046875" />
                  <Point X="-3.341662353516" Y="-25.1882421875" />
                  <Point X="-3.333443115234" Y="-25.174826171875" />
                  <Point X="-3.319330810547" Y="-25.1468203125" />
                  <Point X="-3.313437744141" Y="-25.13223046875" />
                  <Point X="-3.307241455078" Y="-25.112265625" />
                  <Point X="-3.300990966797" Y="-25.088505859375" />
                  <Point X="-3.296721679688" Y="-25.060349609375" />
                  <Point X="-3.295648193359" Y="-25.046111328125" />
                  <Point X="-3.295647216797" Y="-25.016470703125" />
                  <Point X="-3.296720214844" Y="-25.002228515625" />
                  <Point X="-3.300989501953" Y="-24.974064453125" />
                  <Point X="-3.304185791016" Y="-24.960142578125" />
                  <Point X="-3.310382080078" Y="-24.940177734375" />
                  <Point X="-3.319327392578" Y="-24.915748046875" />
                  <Point X="-3.333432861328" Y="-24.887751953125" />
                  <Point X="-3.341647705078" Y="-24.874341796875" />
                  <Point X="-3.360889892578" Y="-24.8480859375" />
                  <Point X="-3.371209960938" Y="-24.836208984375" />
                  <Point X="-3.393671386719" Y="-24.814318359375" />
                  <Point X="-3.405812744141" Y="-24.8043046875" />
                  <Point X="-3.424401367188" Y="-24.791404296875" />
                  <Point X="-3.441269042969" Y="-24.780958984375" />
                  <Point X="-3.46600390625" Y="-24.767375" />
                  <Point X="-3.476318115234" Y="-24.7624921875" />
                  <Point X="-3.497460449219" Y="-24.753998046875" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.998240966797" Y="-24.61910546875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.75815234375" Y="-24.2237265625" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.49045703125" Y="-23.700607421875" />
                  <Point X="-3.778066650391" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747058105469" Y="-23.795634765625" />
                  <Point X="-3.736705810547" Y="-23.795296875" />
                  <Point X="-3.715141601563" Y="-23.79341015625" />
                  <Point X="-3.704886230469" Y="-23.7919453125" />
                  <Point X="-3.684602539062" Y="-23.78791015625" />
                  <Point X="-3.67457421875" Y="-23.78533984375" />
                  <Point X="-3.633431640625" Y="-23.772369140625" />
                  <Point X="-3.613148193359" Y="-23.76597265625" />
                  <Point X="-3.603450439453" Y="-23.76232421875" />
                  <Point X="-3.584510498047" Y="-23.753990234375" />
                  <Point X="-3.575268310547" Y="-23.7493046875" />
                  <Point X="-3.556525634766" Y="-23.738482421875" />
                  <Point X="-3.547852783203" Y="-23.73282421875" />
                  <Point X="-3.531173583984" Y="-23.72059375" />
                  <Point X="-3.515924072266" Y="-23.706619140625" />
                  <Point X="-3.502287109375" Y="-23.691068359375" />
                  <Point X="-3.495893310547" Y="-23.682919921875" />
                  <Point X="-3.483479980469" Y="-23.66519140625" />
                  <Point X="-3.478011962891" Y="-23.6563984375" />
                  <Point X="-3.468063476562" Y="-23.638265625" />
                  <Point X="-3.463583007813" Y="-23.62892578125" />
                  <Point X="-3.44707421875" Y="-23.5890703125" />
                  <Point X="-3.438935546875" Y="-23.569421875" />
                  <Point X="-3.435499267578" Y="-23.559650390625" />
                  <Point X="-3.4297109375" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477458984375" />
                  <Point X="-3.421910888672" Y="-23.45679296875" />
                  <Point X="-3.425058105469" Y="-23.436349609375" />
                  <Point X="-3.427189697266" Y="-23.42621484375" />
                  <Point X="-3.432792236328" Y="-23.40530859375" />
                  <Point X="-3.436013671875" Y="-23.395466796875" />
                  <Point X="-3.443509521484" Y="-23.37619140625" />
                  <Point X="-3.447783935547" Y="-23.3667578125" />
                  <Point X="-3.467703369141" Y="-23.3284921875" />
                  <Point X="-3.477523681641" Y="-23.30962890625" />
                  <Point X="-3.482798339844" Y="-23.30071875" />
                  <Point X="-3.494289550781" Y="-23.28351953125" />
                  <Point X="-3.500506103516" Y="-23.27523046875" />
                  <Point X="-3.51441796875" Y="-23.258650390625" />
                  <Point X="-3.521499267578" Y="-23.25108984375" />
                  <Point X="-3.536442138672" Y="-23.23678515625" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-3.825341796875" Y="-23.014392578125" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.106508300781" Y="-22.498232421875" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.734714599609" Y="-21.97575390625" />
                  <Point X="-3.726337890625" Y="-21.964986328125" />
                  <Point X="-3.691687744141" Y="-21.9849921875" />
                  <Point X="-3.254157714844" Y="-22.237599609375" />
                  <Point X="-3.244921386719" Y="-22.24228125" />
                  <Point X="-3.225991699219" Y="-22.250611328125" />
                  <Point X="-3.216298095703" Y="-22.254259765625" />
                  <Point X="-3.195654541016" Y="-22.26076953125" />
                  <Point X="-3.185623046875" Y="-22.263341796875" />
                  <Point X="-3.165333251953" Y="-22.26737890625" />
                  <Point X="-3.155074951172" Y="-22.26884375" />
                  <Point X="-3.097774658203" Y="-22.273857421875" />
                  <Point X="-3.069525634766" Y="-22.276328125" />
                  <Point X="-3.059173339844" Y="-22.276666015625" />
                  <Point X="-3.038487304688" Y="-22.27621484375" />
                  <Point X="-3.028153564453" Y="-22.27542578125" />
                  <Point X="-3.006695800781" Y="-22.272599609375" />
                  <Point X="-2.996511962891" Y="-22.2706875" />
                  <Point X="-2.976422363281" Y="-22.26576953125" />
                  <Point X="-2.956990722656" Y="-22.2586953125" />
                  <Point X="-2.93844140625" Y="-22.249546875" />
                  <Point X="-2.92941796875" Y="-22.244466796875" />
                  <Point X="-2.911164550781" Y="-22.232837890625" />
                  <Point X="-2.902749023438" Y="-22.22680859375" />
                  <Point X="-2.886618164062" Y="-22.21386328125" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.838230712891" Y="-22.166275390625" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.753810546875" Y="-21.89830078125" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-2.912058837891" Y="-21.555263671875" />
                  <Point X="-3.059386474609" Y="-21.300083984375" />
                  <Point X="-2.829877929688" Y="-21.124123046875" />
                  <Point X="-2.648369384766" Y="-20.984962890625" />
                  <Point X="-2.226951660156" Y="-20.75083203125" />
                  <Point X="-2.1925234375" Y="-20.731703125" />
                  <Point X="-2.118564208984" Y="-20.82808984375" />
                  <Point X="-2.111821044922" Y="-20.835951171875" />
                  <Point X="-2.097516357422" Y="-20.85089453125" />
                  <Point X="-2.089955078125" Y="-20.8579765625" />
                  <Point X="-2.073375" Y="-20.871888671875" />
                  <Point X="-2.065090820313" Y="-20.8781015625" />
                  <Point X="-2.047894897461" Y="-20.889591796875" />
                  <Point X="-2.038983154297" Y="-20.894869140625" />
                  <Point X="-1.975208251953" Y="-20.9280703125" />
                  <Point X="-1.943767089844" Y="-20.9444375" />
                  <Point X="-1.934334472656" Y="-20.9487109375" />
                  <Point X="-1.915060913086" Y="-20.95620703125" />
                  <Point X="-1.905219970703" Y="-20.9594296875" />
                  <Point X="-1.884313720703" Y="-20.965033203125" />
                  <Point X="-1.874174560547" Y="-20.967166015625" />
                  <Point X="-1.853724365234" Y="-20.970314453125" />
                  <Point X="-1.83305456543" Y="-20.971216796875" />
                  <Point X="-1.812407470703" Y="-20.96986328125" />
                  <Point X="-1.802119384766" Y="-20.968623046875" />
                  <Point X="-1.780804443359" Y="-20.96486328125" />
                  <Point X="-1.770713867188" Y="-20.9625078125" />
                  <Point X="-1.750859985352" Y="-20.95671875" />
                  <Point X="-1.741096801758" Y="-20.95328515625" />
                  <Point X="-1.674670898438" Y="-20.92576953125" />
                  <Point X="-1.641922729492" Y="-20.912205078125" />
                  <Point X="-1.63258581543" Y="-20.9077265625" />
                  <Point X="-1.614453979492" Y="-20.897779296875" />
                  <Point X="-1.605659301758" Y="-20.892310546875" />
                  <Point X="-1.587929077148" Y="-20.879896484375" />
                  <Point X="-1.579780639648" Y="-20.873501953125" />
                  <Point X="-1.564228027344" Y="-20.85986328125" />
                  <Point X="-1.550252929688" Y="-20.84461328125" />
                  <Point X="-1.538021240234" Y="-20.827931640625" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.52153894043" Y="-20.80051171875" />
                  <Point X="-1.516855834961" Y="-20.791271484375" />
                  <Point X="-1.508525146484" Y="-20.772337890625" />
                  <Point X="-1.504877197266" Y="-20.76264453125" />
                  <Point X="-1.483256835938" Y="-20.69407421875" />
                  <Point X="-1.472597900391" Y="-20.660267578125" />
                  <Point X="-1.470026489258" Y="-20.650234375" />
                  <Point X="-1.465990966797" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.477701416016" Y="-20.44923046875" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-1.166142822266" Y="-20.349556640625" />
                  <Point X="-0.931164611816" Y="-20.2836796875" />
                  <Point X="-0.420292327881" Y="-20.22388671875" />
                  <Point X="-0.365222686768" Y="-20.21744140625" />
                  <Point X="-0.35509979248" Y="-20.255220703125" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.301789215088" Y="-20.500126953125" />
                  <Point X="0.378190704346" Y="-20.2149921875" />
                  <Point X="0.622702758789" Y="-20.240599609375" />
                  <Point X="0.827853088379" Y="-20.262083984375" />
                  <Point X="1.250552734375" Y="-20.36413671875" />
                  <Point X="1.453623168945" Y="-20.413166015625" />
                  <Point X="1.727258300781" Y="-20.5124140625" />
                  <Point X="1.858249389648" Y="-20.559927734375" />
                  <Point X="2.124290771484" Y="-20.684345703125" />
                  <Point X="2.250429931641" Y="-20.7433359375" />
                  <Point X="2.507502441406" Y="-20.893107421875" />
                  <Point X="2.629432373047" Y="-20.964142578125" />
                  <Point X="2.817778564453" Y="-21.0980859375" />
                  <Point X="2.642419433594" Y="-21.40181640625" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.026921264648" Y="-22.5131796875" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017912231445" Y="-22.5485390625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.019018432617" Y="-22.676921875" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.023801025391" Y="-22.710966796875" />
                  <Point X="2.029144775391" Y="-22.732892578125" />
                  <Point X="2.032470214844" Y="-22.743697265625" />
                  <Point X="2.040736694336" Y="-22.76578515625" />
                  <Point X="2.045320922852" Y="-22.776115234375" />
                  <Point X="2.055682373047" Y="-22.79615625" />
                  <Point X="2.061459472656" Y="-22.8058671875" />
                  <Point X="2.090232177734" Y="-22.848271484375" />
                  <Point X="2.104417236328" Y="-22.86917578125" />
                  <Point X="2.109805175781" Y="-22.876361328125" />
                  <Point X="2.130463623047" Y="-22.899876953125" />
                  <Point X="2.157595458984" Y="-22.924611328125" />
                  <Point X="2.168255126953" Y="-22.933017578125" />
                  <Point X="2.210658691406" Y="-22.961791015625" />
                  <Point X="2.231563720703" Y="-22.9759765625" />
                  <Point X="2.241276855469" Y="-22.98175390625" />
                  <Point X="2.261314453125" Y="-22.99211328125" />
                  <Point X="2.271638916016" Y="-22.9966953125" />
                  <Point X="2.293725341797" Y="-23.004962890625" />
                  <Point X="2.304535888672" Y="-23.008291015625" />
                  <Point X="2.326470703125" Y="-23.01363671875" />
                  <Point X="2.337594970703" Y="-23.015654296875" />
                  <Point X="2.384095214844" Y="-23.021259765625" />
                  <Point X="2.407019775391" Y="-23.024025390625" />
                  <Point X="2.416039550781" Y="-23.0246796875" />
                  <Point X="2.447573730469" Y="-23.02450390625" />
                  <Point X="2.484316650391" Y="-23.020177734375" />
                  <Point X="2.497750976562" Y="-23.01760546875" />
                  <Point X="2.551526123047" Y="-23.003224609375" />
                  <Point X="2.578037353516" Y="-22.996134765625" />
                  <Point X="2.584005371094" Y="-22.994326171875" />
                  <Point X="2.604409423828" Y="-22.986931640625" />
                  <Point X="2.627654052734" Y="-22.976427734375" />
                  <Point X="2.636033447266" Y="-22.97212890625" />
                  <Point X="3.128831298828" Y="-22.68761328125" />
                  <Point X="3.940405761719" Y="-22.21905078125" />
                  <Point X="3.971625976562" Y="-22.262439453125" />
                  <Point X="4.043956542969" Y="-22.362962890625" />
                  <Point X="4.136885253906" Y="-22.51652734375" />
                  <Point X="3.932772216797" Y="-22.6731484375" />
                  <Point X="3.172951904297" Y="-23.2561796875" />
                  <Point X="3.168131103516" Y="-23.26013671875" />
                  <Point X="3.152114990234" Y="-23.2747890625" />
                  <Point X="3.134668457031" Y="-23.2933984375" />
                  <Point X="3.128576416016" Y="-23.300578125" />
                  <Point X="3.089874267578" Y="-23.351068359375" />
                  <Point X="3.070794189453" Y="-23.375958984375" />
                  <Point X="3.065635986328" Y="-23.3833984375" />
                  <Point X="3.049741455078" Y="-23.410625" />
                  <Point X="3.034763671875" Y="-23.444453125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.015723632812" Y="-23.50887890625" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.016533935547" Y="-23.70478515625" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024597900391" Y="-23.741765625" />
                  <Point X="3.034683837891" Y="-23.771392578125" />
                  <Point X="3.050286621094" Y="-23.804630859375" />
                  <Point X="3.056918701172" Y="-23.8164765625" />
                  <Point X="3.089107177734" Y="-23.86540234375" />
                  <Point X="3.104976318359" Y="-23.889521484375" />
                  <Point X="3.111741210938" Y="-23.898580078125" />
                  <Point X="3.126296630859" Y="-23.915826171875" />
                  <Point X="3.134087158203" Y="-23.924013671875" />
                  <Point X="3.151329833984" Y="-23.9401015625" />
                  <Point X="3.160035644531" Y="-23.9473046875" />
                  <Point X="3.178243164062" Y="-23.96062890625" />
                  <Point X="3.187744873047" Y="-23.96675" />
                  <Point X="3.234390380859" Y="-23.9930078125" />
                  <Point X="3.257386962891" Y="-24.005953125" />
                  <Point X="3.265482177734" Y="-24.010013671875" />
                  <Point X="3.29467578125" Y="-24.0219140625" />
                  <Point X="3.330275390625" Y="-24.031978515625" />
                  <Point X="3.343674804688" Y="-24.034744140625" />
                  <Point X="3.406742675781" Y="-24.043078125" />
                  <Point X="3.437835449219" Y="-24.0471875" />
                  <Point X="3.444033203125" Y="-24.04780078125" />
                  <Point X="3.465708007812" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="3.968728515625" Y="-23.98557421875" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.721620117188" Y="-23.95816796875" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.562762207031" Y="-24.34532421875" />
                  <Point X="3.691991210938" Y="-24.578646484375" />
                  <Point X="3.686023925781" Y="-24.580458984375" />
                  <Point X="3.665622802734" Y="-24.587865234375" />
                  <Point X="3.642383544922" Y="-24.598380859375" />
                  <Point X="3.634007324219" Y="-24.602681640625" />
                  <Point X="3.572044921875" Y="-24.63849609375" />
                  <Point X="3.541497314453" Y="-24.65615234375" />
                  <Point X="3.533878662109" Y="-24.661056640625" />
                  <Point X="3.508775390625" Y="-24.68012890625" />
                  <Point X="3.481994384766" Y="-24.7056484375" />
                  <Point X="3.472796142578" Y="-24.7157734375" />
                  <Point X="3.435618652344" Y="-24.763146484375" />
                  <Point X="3.417290283203" Y="-24.786501953125" />
                  <Point X="3.410852050781" Y="-24.795794921875" />
                  <Point X="3.399128173828" Y="-24.815076171875" />
                  <Point X="3.393842529297" Y="-24.825064453125" />
                  <Point X="3.384069091797" Y="-24.84652734375" />
                  <Point X="3.380005859375" Y="-24.8570703125" />
                  <Point X="3.373159423828" Y="-24.8785703125" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.357983642578" Y="-24.954236328125" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.99503515625" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280029297" Y="-25.062943359375" />
                  <Point X="3.351874267578" Y="-25.076423828125" />
                  <Point X="3.364266845703" Y="-25.1411328125" />
                  <Point X="3.370376464844" Y="-25.173033203125" />
                  <Point X="3.373159667969" Y="-25.183990234375" />
                  <Point X="3.380006103516" Y="-25.205490234375" />
                  <Point X="3.384069335938" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.23749609375" />
                  <Point X="3.399127929688" Y="-25.247482421875" />
                  <Point X="3.4108515625" Y="-25.266763671875" />
                  <Point X="3.417290039063" Y="-25.27605859375" />
                  <Point X="3.454467285156" Y="-25.323431640625" />
                  <Point X="3.472795898438" Y="-25.346787109375" />
                  <Point X="3.478719726563" Y="-25.353634765625" />
                  <Point X="3.501135742188" Y="-25.37580078125" />
                  <Point X="3.530173583984" Y="-25.39872265625" />
                  <Point X="3.541495117188" Y="-25.40640625" />
                  <Point X="3.603457519531" Y="-25.442220703125" />
                  <Point X="3.634005126953" Y="-25.45987890625" />
                  <Point X="3.639492431641" Y="-25.462814453125" />
                  <Point X="3.659139648438" Y="-25.472009765625" />
                  <Point X="3.683021240234" Y="-25.48102734375" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.121284667969" Y="-25.598943359375" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.778958007812" Y="-25.81601171875" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.727801757812" Y="-26.079220703125" />
                  <Point X="4.446630859375" Y="-26.042203125" />
                  <Point X="3.436781738281" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354482177734" Y="-25.912677734375" />
                  <Point X="3.232872070312" Y="-25.939109375" />
                  <Point X="3.17291796875" Y="-25.952140625" />
                  <Point X="3.157874267578" Y="-25.9567421875" />
                  <Point X="3.128752929688" Y="-25.9683671875" />
                  <Point X="3.114675292969" Y="-25.975390625" />
                  <Point X="3.086848632812" Y="-25.992283203125" />
                  <Point X="3.074123046875" Y="-26.00153125" />
                  <Point X="3.050373535156" Y="-26.022001953125" />
                  <Point X="3.039349609375" Y="-26.033224609375" />
                  <Point X="2.965843994141" Y="-26.12162890625" />
                  <Point X="2.92960546875" Y="-26.165212890625" />
                  <Point X="2.921327148438" Y="-26.17684765625" />
                  <Point X="2.906606689453" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.864622070312" Y="-26.4111484375" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607910156" Y="-26.514591796875" />
                  <Point X="2.86406640625" Y="-26.530130859375" />
                  <Point X="2.871798339844" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576669921875" />
                  <Point X="2.889157958984" Y="-26.60548046875" />
                  <Point X="2.896540527344" Y="-26.619373046875" />
                  <Point X="2.963841308594" Y="-26.7240546875" />
                  <Point X="2.997020751953" Y="-26.775662109375" />
                  <Point X="3.001739990234" Y="-26.782349609375" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043487060547" Y="-26.828119140625" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.451181152344" Y="-27.14197265625" />
                  <Point X="4.087170166016" Y="-27.629984375" />
                  <Point X="4.045500976562" Y="-27.697412109375" />
                  <Point X="4.001274658203" Y="-27.760251953125" />
                  <Point X="3.747861083984" Y="-27.613943359375" />
                  <Point X="2.848454345703" Y="-27.094669921875" />
                  <Point X="2.841182617188" Y="-27.090880859375" />
                  <Point X="2.815026123047" Y="-27.079515625" />
                  <Point X="2.78312109375" Y="-27.069328125" />
                  <Point X="2.771107910156" Y="-27.066337890625" />
                  <Point X="2.626372314453" Y="-27.04019921875" />
                  <Point X="2.555017578125" Y="-27.0273125" />
                  <Point X="2.539359130859" Y="-27.02580859375" />
                  <Point X="2.508009033203" Y="-27.025404296875" />
                  <Point X="2.492317382813" Y="-27.02650390625" />
                  <Point X="2.460145019531" Y="-27.0314609375" />
                  <Point X="2.444847412109" Y="-27.03513671875" />
                  <Point X="2.415069580078" Y="-27.0449609375" />
                  <Point X="2.400589355469" Y="-27.051109375" />
                  <Point X="2.280349609375" Y="-27.114390625" />
                  <Point X="2.221071289062" Y="-27.145587890625" />
                  <Point X="2.208967529297" Y="-27.153171875" />
                  <Point X="2.186038330078" Y="-27.170064453125" />
                  <Point X="2.175212890625" Y="-27.179373046875" />
                  <Point X="2.154252929688" Y="-27.20033203125" />
                  <Point X="2.144942382812" Y="-27.21116015625" />
                  <Point X="2.128047851562" Y="-27.234091796875" />
                  <Point X="2.120463867188" Y="-27.2461953125" />
                  <Point X="2.057182617187" Y="-27.366435546875" />
                  <Point X="2.025984863281" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.440193359375" />
                  <Point X="2.010012573242" Y="-27.46996875" />
                  <Point X="2.006337890625" Y="-27.485263671875" />
                  <Point X="2.001379760742" Y="-27.5174375" />
                  <Point X="2.000279418945" Y="-27.53312890625" />
                  <Point X="2.000683227539" Y="-27.56448046875" />
                  <Point X="2.002187744141" Y="-27.580140625" />
                  <Point X="2.028326538086" Y="-27.724876953125" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.325549316406" Y="-28.31698828125" />
                  <Point X="2.735893798828" Y="-29.027724609375" />
                  <Point X="2.723753662109" Y="-29.036083984375" />
                  <Point X="2.520245117188" Y="-28.7708671875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657714844" Y="-27.87015234375" />
                  <Point X="1.808834594727" Y="-27.84962890625" />
                  <Point X="1.783251831055" Y="-27.82800390625" />
                  <Point X="1.773298339844" Y="-27.820646484375" />
                  <Point X="1.630550048828" Y="-27.728873046875" />
                  <Point X="1.560174804688" Y="-27.68362890625" />
                  <Point X="1.546279663086" Y="-27.676244140625" />
                  <Point X="1.517465942383" Y="-27.663873046875" />
                  <Point X="1.502547363281" Y="-27.65888671875" />
                  <Point X="1.470927001953" Y="-27.65115625" />
                  <Point X="1.455391113281" Y="-27.648697265625" />
                  <Point X="1.424125732422" Y="-27.64637890625" />
                  <Point X="1.408396118164" Y="-27.64651953125" />
                  <Point X="1.252276489258" Y="-27.660884765625" />
                  <Point X="1.175308959961" Y="-27.667966796875" />
                  <Point X="1.161228149414" Y="-27.67033984375" />
                  <Point X="1.133582641602" Y="-27.677169921875" />
                  <Point X="1.120017822266" Y="-27.681626953125" />
                  <Point X="1.092632324219" Y="-27.69296875" />
                  <Point X="1.079884399414" Y="-27.69941015625" />
                  <Point X="1.055496948242" Y="-27.7141328125" />
                  <Point X="1.043857543945" Y="-27.7224140625" />
                  <Point X="0.923306091309" Y="-27.822650390625" />
                  <Point X="0.863873901367" Y="-27.87206640625" />
                  <Point X="0.852651550293" Y="-27.883091796875" />
                  <Point X="0.832179748535" Y="-27.90684375" />
                  <Point X="0.822930358887" Y="-27.9195703125" />
                  <Point X="0.806038208008" Y="-27.9473984375" />
                  <Point X="0.79901739502" Y="-27.96147265625" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782792053223" Y="-28.0056328125" />
                  <Point X="0.746747619629" Y="-28.17146484375" />
                  <Point X="0.728977783203" Y="-28.25321875" />
                  <Point X="0.727584899902" Y="-28.2612890625" />
                  <Point X="0.72472479248" Y="-28.28967578125" />
                  <Point X="0.7247421875" Y="-28.32316796875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.798104064941" Y="-28.8865859375" />
                  <Point X="0.833091308594" Y="-29.15233984375" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146972656" Y="-28.453583984375" />
                  <Point X="0.626788391113" Y="-28.42381640625" />
                  <Point X="0.620407592773" Y="-28.41320703125" />
                  <Point X="0.51074395752" Y="-28.255203125" />
                  <Point X="0.4566796875" Y="-28.177306640625" />
                  <Point X="0.446670684814" Y="-28.165169921875" />
                  <Point X="0.424786987305" Y="-28.14271484375" />
                  <Point X="0.412912322998" Y="-28.132396484375" />
                  <Point X="0.386656616211" Y="-28.11315234375" />
                  <Point X="0.373240112305" Y="-28.104935546875" />
                  <Point X="0.345238830566" Y="-28.090828125" />
                  <Point X="0.330654296875" Y="-28.0849375" />
                  <Point X="0.160956832886" Y="-28.03226953125" />
                  <Point X="0.07729535675" Y="-28.0063046875" />
                  <Point X="0.063376972198" Y="-28.003109375" />
                  <Point X="0.035217689514" Y="-27.99883984375" />
                  <Point X="0.020976495743" Y="-27.997765625" />
                  <Point X="-0.008664410591" Y="-27.997765625" />
                  <Point X="-0.022905456543" Y="-27.99883984375" />
                  <Point X="-0.051065036774" Y="-28.003109375" />
                  <Point X="-0.064983421326" Y="-28.0063046875" />
                  <Point X="-0.23468119812" Y="-28.05897265625" />
                  <Point X="-0.318342529297" Y="-28.0849375" />
                  <Point X="-0.332931182861" Y="-28.090830078125" />
                  <Point X="-0.360932922363" Y="-28.104939453125" />
                  <Point X="-0.374346008301" Y="-28.11315625" />
                  <Point X="-0.400601135254" Y="-28.132400390625" />
                  <Point X="-0.412475219727" Y="-28.142716796875" />
                  <Point X="-0.434358886719" Y="-28.165171875" />
                  <Point X="-0.444368347168" Y="-28.177310546875" />
                  <Point X="-0.554031677246" Y="-28.33531640625" />
                  <Point X="-0.608095947266" Y="-28.4132109375" />
                  <Point X="-0.61247052002" Y="-28.4201328125" />
                  <Point X="-0.625975402832" Y="-28.44526171875" />
                  <Point X="-0.63877722168" Y="-28.476212890625" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.775028442383" Y="-28.981595703125" />
                  <Point X="-0.98542578125" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.665117051724" Y="-28.959667468342" />
                  <Point X="2.689206019964" Y="-28.94685913673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.606480441339" Y="-28.883250952309" />
                  <Point X="2.641677007051" Y="-28.864536606358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.547843830955" Y="-28.806834436276" />
                  <Point X="2.594147994138" Y="-28.782214075986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.48920727775" Y="-28.730417889839" />
                  <Point X="2.546618981225" Y="-28.699891545614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.43057077539" Y="-28.654001316368" />
                  <Point X="2.499089968312" Y="-28.617569015242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371934273029" Y="-28.577584742897" />
                  <Point X="2.451560955399" Y="-28.53524648487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.957090539195" Y="-27.73474220552" />
                  <Point X="4.054409342386" Y="-27.682996879985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.313297770669" Y="-28.501168169427" />
                  <Point X="2.404031942486" Y="-28.452923954498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.860076754674" Y="-27.678731194936" />
                  <Point X="4.031743796458" Y="-27.587454209713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.254661268309" Y="-28.424751595956" />
                  <Point X="2.356502929573" Y="-28.370601424126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763062970152" Y="-27.622720184351" />
                  <Point X="3.948917688459" Y="-27.523899477709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814487973967" Y="-29.082911164969" />
                  <Point X="0.823331755393" Y="-29.078208842973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.196024765949" Y="-28.348335022485" />
                  <Point X="2.308973895812" Y="-28.288278904839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.666049186507" Y="-27.566709173302" />
                  <Point X="3.86609158046" Y="-27.460344745704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789253432536" Y="-28.988734453836" />
                  <Point X="0.810093384179" Y="-28.977653654992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.137388263588" Y="-28.271918449014" />
                  <Point X="2.261444823119" Y="-28.205956406252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.569035403024" Y="-27.510698162165" />
                  <Point X="3.783265472462" Y="-27.396790013699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764018891105" Y="-28.894557742703" />
                  <Point X="0.796855018734" Y="-28.877098463943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.078751761228" Y="-28.195501875543" />
                  <Point X="2.213915750426" Y="-28.123633907666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.472021619541" Y="-27.454687151029" />
                  <Point X="3.700439364463" Y="-27.333235281695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.053825978482" Y="-29.753528850344" />
                  <Point X="-0.969912744454" Y="-29.708911392371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.738784349673" Y="-28.80038103157" />
                  <Point X="0.783616708659" Y="-28.776543243454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.020115258868" Y="-28.119085302072" />
                  <Point X="2.166386677733" Y="-28.041311409079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.375007836058" Y="-27.398676139892" />
                  <Point X="3.617613256464" Y="-27.26968054969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.135738006416" Y="-29.689488093348" />
                  <Point X="-0.936293026749" Y="-29.583441316562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.713549808242" Y="-28.706204320438" />
                  <Point X="0.770378398584" Y="-28.675988022965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.961478756508" Y="-28.042668728601" />
                  <Point X="2.11885760504" Y="-27.958988910493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.277994052575" Y="-27.342665128756" />
                  <Point X="3.534787148465" Y="-27.206125817685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120595488729" Y="-29.573842519059" />
                  <Point X="-0.902673309044" Y="-29.457971240752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.688315266811" Y="-28.612027609305" />
                  <Point X="0.757140088509" Y="-28.575432802476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.902842254148" Y="-27.96625215513" />
                  <Point X="2.071328532346" Y="-27.876666411906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.180980269092" Y="-27.286654117619" />
                  <Point X="3.451961040467" Y="-27.142571085681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128728699948" Y="-29.470572869359" />
                  <Point X="-0.869053591339" Y="-29.332501164943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.663080725379" Y="-28.517850898172" />
                  <Point X="0.743901778433" Y="-28.474877581986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.844205751787" Y="-27.88983558166" />
                  <Point X="2.039376400818" Y="-27.786061506786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.083966485609" Y="-27.230643106483" />
                  <Point X="3.369135138493" Y="-27.079016244131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.148083453494" Y="-29.373269819551" />
                  <Point X="-0.835433873634" Y="-29.207031089134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.62911130129" Y="-28.428318606533" />
                  <Point X="0.730663468358" Y="-28.374322361497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.772699914994" Y="-27.820261754686" />
                  <Point X="2.021647556878" Y="-27.687893945506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.986952702126" Y="-27.174632095346" />
                  <Point X="3.286309238477" Y="-27.015461401539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.167438173448" Y="-29.275966751882" />
                  <Point X="-0.801814155929" Y="-29.081561013325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.575868127126" Y="-28.349034349592" />
                  <Point X="0.726831595421" Y="-28.268765649663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.681100280428" Y="-27.761371989506" />
                  <Point X="2.003918877577" Y="-27.589726296686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.889938918643" Y="-27.11862108421" />
                  <Point X="3.203483338461" Y="-26.951906558948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.187901246593" Y="-29.179253006059" />
                  <Point X="-0.768194499265" Y="-28.956090969973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.521321424228" Y="-28.270443191174" />
                  <Point X="0.751879738749" Y="-28.147853160795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.589500456908" Y="-27.702482324795" />
                  <Point X="2.007554720608" Y="-27.480198929839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.781192951422" Y="-27.068848185821" />
                  <Point X="3.120657438446" Y="-26.888351716357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.659358574049" Y="-26.070209810048" />
                  <Point X="4.739593299406" Y="-26.027548249829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.230867072266" Y="-29.094504185993" />
                  <Point X="-0.734575081853" Y="-28.830621053832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.466774893518" Y="-28.191851941201" />
                  <Point X="0.778321773514" Y="-28.026199526702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.479660135084" Y="-27.65329130507" />
                  <Point X="2.07195800548" Y="-27.338360941027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.63110961523" Y="-27.041054756394" />
                  <Point X="3.039352114379" Y="-26.823988369192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.497163955029" Y="-26.04885606393" />
                  <Point X="4.765351883939" Y="-25.906258012671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.301360498548" Y="-29.024392050802" />
                  <Point X="-0.70095566444" Y="-28.705151137691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.397493587487" Y="-28.12109531024" />
                  <Point X="0.861544193033" Y="-27.874355226503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.265272797328" Y="-27.659688919768" />
                  <Point X="2.185901973111" Y="-27.170181703941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.4317708586" Y="-27.039450898575" />
                  <Point X="2.979407948149" Y="-26.748267092934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.334968910687" Y="-26.027502543961" />
                  <Point X="4.782987118131" Y="-25.789287037506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.37773946806" Y="-28.957409314456" />
                  <Point X="-0.667336247027" Y="-28.57968122155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.288024680786" Y="-28.071706805591" />
                  <Point X="2.927856732566" Y="-26.668083205657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.172773673861" Y="-26.006149126336" />
                  <Point X="4.664745703069" Y="-25.744562958292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.454118493141" Y="-28.890426607658" />
                  <Point X="-0.628538169675" Y="-28.451457763077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.160251772162" Y="-28.032050711402" />
                  <Point X="2.880622975108" Y="-26.585603685175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.010578437036" Y="-25.984795708711" />
                  <Point X="4.530195487047" Y="-25.708510422368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.537647888729" Y="-28.827245820297" />
                  <Point X="-0.519857206981" Y="-28.286076915354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.022203411905" Y="-27.99785817176" />
                  <Point X="2.859719124203" Y="-26.489124305044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.848383200211" Y="-25.963442291086" />
                  <Point X="4.395645271024" Y="-25.672457886444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.676647318878" Y="-28.793558973487" />
                  <Point X="2.867739971967" Y="-26.377265389823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.686187963386" Y="-25.942088873461" />
                  <Point X="4.261095055002" Y="-25.636405350519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.535373972324" Y="-29.142557879528" />
                  <Point X="-2.455207825994" Y="-29.099932783425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.856795041872" Y="-28.78175106208" />
                  <Point X="2.88268363487" Y="-26.261725548498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.52399272656" Y="-25.920735455836" />
                  <Point X="4.12654483898" Y="-25.600352814595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.628862385413" Y="-29.084672395703" />
                  <Point X="2.98330307004" Y="-26.100631090994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.32456102053" Y="-25.919181020089" />
                  <Point X="3.991994287558" Y="-25.564300457006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.722350327015" Y="-29.026786661184" />
                  <Point X="3.857443722491" Y="-25.528248106672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.807934764457" Y="-28.96469855896" />
                  <Point X="3.722893157424" Y="-25.492195756338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890592660942" Y="-28.901054387307" />
                  <Point X="3.608761305043" Y="-25.445286583886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.973250557427" Y="-28.837410215654" />
                  <Point X="3.51560651341" Y="-25.387223710387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.899482230439" Y="-28.690592745621" />
                  <Point X="3.448213661478" Y="-25.31546297057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.809846186841" Y="-28.535338261008" />
                  <Point X="3.393579639597" Y="-25.236918240478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.720209935133" Y="-28.38008366574" />
                  <Point X="3.364923395428" Y="-25.144560880963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.630573683425" Y="-28.224829070473" />
                  <Point X="3.349459120497" Y="-25.045189226983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770308299286" Y="-24.289710317652" />
                  <Point X="4.78335626227" Y="-24.282772592669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.540937431717" Y="-28.069574475206" />
                  <Point X="3.362498430597" Y="-24.930661948004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.362376380134" Y="-24.399017411726" />
                  <Point X="4.767885368152" Y="-24.183404458173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.451301180009" Y="-27.914319879938" />
                  <Point X="3.409447801296" Y="-24.798104369978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.954452505349" Y="-24.508320228533" />
                  <Point X="4.752280408366" Y="-24.084107607657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.361664928302" Y="-27.759065284671" />
                  <Point X="4.729088467856" Y="-23.988844826349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.312329612128" Y="-27.625239076932" />
                  <Point X="4.705897403189" Y="-23.893581579347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.325065443677" Y="-27.524416683871" />
                  <Point X="4.449594673422" Y="-23.922266003309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.372951709706" Y="-27.44228410835" />
                  <Point X="4.180646190272" Y="-23.957674293615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.453920563362" Y="-27.377741856695" />
                  <Point X="3.911698278976" Y="-23.993082279862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.812331567916" Y="-27.992427645073" />
                  <Point X="-3.014956507978" Y="-27.568455805133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.610768079889" Y="-27.35354500575" />
                  <Point X="3.642752492602" Y="-24.028489136267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.87063210679" Y="-27.915832436648" />
                  <Point X="3.411721783405" Y="-24.043736188535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.928932393877" Y="-27.839237094346" />
                  <Point X="3.26961839379" Y="-24.011699746249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.987232464167" Y="-27.76264163677" />
                  <Point X="3.171960457856" Y="-23.956031237046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.038709190202" Y="-27.682418142698" />
                  <Point X="3.102310915858" Y="-23.885470400622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.087426235035" Y="-27.600727300103" />
                  <Point X="3.050678019139" Y="-23.805329943976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.136143279868" Y="-27.519036457508" />
                  <Point X="3.018598779226" Y="-23.714792623583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.147561001099" Y="-27.417513212759" />
                  <Point X="3.001393749567" Y="-23.616346545309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.690914420916" Y="-27.067115764325" />
                  <Point X="3.01826817153" Y="-23.499780101182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234264769258" Y="-26.716716682757" />
                  <Point X="3.085935537473" Y="-23.356206569679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.972078238653" Y="-26.469715476765" />
                  <Point X="3.401434495023" Y="-23.080858643455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948808286193" Y="-26.349748468752" />
                  <Point X="3.858080362001" Y="-22.730461574238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.969699774538" Y="-26.253262515331" />
                  <Point X="4.117700662797" Y="-22.484824856839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027541336239" Y="-26.176423264414" />
                  <Point X="4.068440604957" Y="-22.40342273938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.120988196321" Y="-26.118515686464" />
                  <Point X="4.015792998658" Y="-22.323821813389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.303555955248" Y="-26.107994530987" />
                  <Point X="2.519619337087" Y="-23.011757305834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.124004659949" Y="-22.69039992931" />
                  <Point X="3.959797660733" Y="-22.246000907877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.572501950976" Y="-26.143401498708" />
                  <Point X="2.317733974053" Y="-23.011507502658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.841448067851" Y="-26.178808530843" />
                  <Point X="2.209878097681" Y="-22.96126133457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.110394610041" Y="-26.214215789123" />
                  <Point X="2.128077807194" Y="-22.897161165719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.379341152231" Y="-26.249623047403" />
                  <Point X="2.0709940694" Y="-22.819918972683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.648287694421" Y="-26.285030305682" />
                  <Point X="2.02959015721" Y="-22.734339668487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.683980200952" Y="-26.196414193229" />
                  <Point X="2.013969476773" Y="-22.635051176788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708176892905" Y="-26.10168564774" />
                  <Point X="2.024617281823" Y="-22.521795483602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.732373429002" Y="-26.00695701938" />
                  <Point X="2.072670633665" Y="-22.388650908389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750105127227" Y="-25.908790975751" />
                  <Point X="-3.749676487503" Y="-25.376853632305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.320166763828" Y="-25.148479261237" />
                  <Point X="2.162306846638" Y="-22.233396333717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.764406099244" Y="-25.808800782639" />
                  <Point X="-4.157599588525" Y="-25.486156037696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.295647591643" Y="-25.027848031314" />
                  <Point X="2.251943059611" Y="-22.078141759046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.778707071261" Y="-25.708810589527" />
                  <Point X="-4.565524406868" Y="-25.595459356202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.314087397845" Y="-24.930058495374" />
                  <Point X="2.341579272583" Y="-21.922887184374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361327478739" Y="-24.847582337123" />
                  <Point X="2.431215485556" Y="-21.767632609703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.439935476176" Y="-24.781784795949" />
                  <Point X="2.520851698528" Y="-21.612378035031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.558125093429" Y="-24.737033175351" />
                  <Point X="2.610487911501" Y="-21.45712346036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.692675579205" Y="-24.700980782857" />
                  <Point X="2.700124302758" Y="-21.301868790893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.82722606498" Y="-24.664928390363" />
                  <Point X="2.789760792671" Y="-21.14661406897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.961776550755" Y="-24.628875997869" />
                  <Point X="2.758268188657" Y="-21.055764828736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.096326646192" Y="-24.592823397828" />
                  <Point X="2.671698695464" Y="-20.994200489946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.230876596518" Y="-24.556770720631" />
                  <Point X="2.580017610531" Y="-20.935354032694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.365426546843" Y="-24.520718043433" />
                  <Point X="2.483460243977" Y="-20.879100340372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.499976497169" Y="-24.484665366235" />
                  <Point X="2.386903792901" Y="-20.822846161281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.634526447494" Y="-24.448612689037" />
                  <Point X="2.290347341825" Y="-20.76659198219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76907639782" Y="-24.412560011839" />
                  <Point X="2.187276459783" Y="-20.713801587486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770267377899" Y="-24.305599112365" />
                  <Point X="-3.802725788897" Y="-23.791148123968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453966438157" Y="-23.605709487799" />
                  <Point X="2.079614591528" Y="-20.663452263453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752986589321" Y="-24.188816599276" />
                  <Point X="-3.964921336571" Y="-23.769794871624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421220682884" Y="-23.480704106058" />
                  <Point X="1.971952976097" Y="-20.61310280499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735705860114" Y="-24.072034117755" />
                  <Point X="-4.127116884245" Y="-23.74844161928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.440683479202" Y="-23.383458503611" />
                  <Point X="1.864291360667" Y="-20.562753346527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.70589013807" Y="-23.948586662317" />
                  <Point X="-4.289312431918" Y="-23.727088366936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.484011851477" Y="-23.298902452993" />
                  <Point X="1.74470721429" Y="-20.518743210217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.67182598247" Y="-23.822880274688" />
                  <Point X="-4.451507979592" Y="-23.705735114592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.549442105053" Y="-23.22609818112" />
                  <Point X="1.624411626485" Y="-20.475111354025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.63776182687" Y="-23.697173887058" />
                  <Point X="-4.613702689357" Y="-23.684381416725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.632268156792" Y="-23.162543419202" />
                  <Point X="1.504115603185" Y="-20.431479729389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.715094208531" Y="-23.098988657283" />
                  <Point X="1.3728714456" Y="-20.393669331012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.79792026027" Y="-23.035433895365" />
                  <Point X="1.233707757527" Y="-20.360069821689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.880746434926" Y="-22.971879198802" />
                  <Point X="-0.011445590193" Y="-20.914535445722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.211558744907" Y="-20.795961937448" />
                  <Point X="1.094542644136" Y="-20.326471070222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963572670417" Y="-22.908324534586" />
                  <Point X="-0.132014803812" Y="-20.871049078955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257961425794" Y="-20.66369503955" />
                  <Point X="0.955377530746" Y="-20.292872318755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.046398905908" Y="-22.84476987037" />
                  <Point X="-0.197648968637" Y="-20.798353228617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.29158088081" Y="-20.538225103415" />
                  <Point X="0.813712006832" Y="-20.260603059146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.129225141399" Y="-22.781215206154" />
                  <Point X="-3.163367528283" Y="-22.267659603618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761223615201" Y="-22.053835892148" />
                  <Point X="-0.233316211728" Y="-20.709723683354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.325200377268" Y="-20.412755145246" />
                  <Point X="0.644654294185" Y="-20.24289848464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.21205137689" Y="-22.717660541938" />
                  <Point X="-3.280540606359" Y="-22.222367479553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750132202244" Y="-21.940344328453" />
                  <Point X="-0.25855075859" Y="-20.615546975109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.358819891796" Y="-20.287285177468" />
                  <Point X="0.475597421869" Y="-20.225193463322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153662935402" Y="-22.579020702084" />
                  <Point X="-3.377554591001" Y="-22.166356575375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761908417512" Y="-21.839011698364" />
                  <Point X="-0.283785305452" Y="-20.521370266864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.062599984327" Y="-22.423007517307" />
                  <Point X="-3.474568575642" Y="-22.110345671196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.798976873586" Y="-21.751127191261" />
                  <Point X="-0.309019852314" Y="-20.427193558619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.954072246441" Y="-22.257708140661" />
                  <Point X="-3.571582560284" Y="-22.054334767018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.846505861885" Y="-21.668804647801" />
                  <Point X="-0.334254399177" Y="-20.333016850374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.81130886165" Y="-22.074205347656" />
                  <Point X="-3.668596544926" Y="-21.998323862839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.894034850183" Y="-21.586482104341" />
                  <Point X="-0.35948893566" Y="-20.23884013661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.941563751735" Y="-21.504159514757" />
                  <Point X="-1.912557091894" Y="-20.957026968477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.496397629259" Y="-20.735751057119" />
                  <Point X="-0.56573621867" Y="-20.240909607426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.989092600296" Y="-21.421836896998" />
                  <Point X="-2.01828048273" Y="-20.905646937716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.463748765694" Y="-20.610797193613" />
                  <Point X="-0.825206506767" Y="-20.271278252027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.036621448857" Y="-21.339514279238" />
                  <Point X="-2.10431131905" Y="-20.843796189986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.470147838466" Y="-20.506605486144" />
                  <Point X="-1.184418554444" Y="-20.354680530928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.820811507658" Y="-21.117171943241" />
                  <Point X="-2.164510627375" Y="-20.768210575187" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998373046875" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.739844970703" Y="-29.538443359375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.354655303955" Y="-28.3635390625" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.274335449219" Y="-28.2663984375" />
                  <Point X="0.10463785553" Y="-28.21373046875" />
                  <Point X="0.020976472855" Y="-28.187765625" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.178362197876" Y="-28.24043359375" />
                  <Point X="-0.262023590088" Y="-28.2663984375" />
                  <Point X="-0.288278839111" Y="-28.285642578125" />
                  <Point X="-0.397942230225" Y="-28.4436484375" />
                  <Point X="-0.452006500244" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.591502441406" Y="-29.030771484375" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-1.002743103027" Y="-29.9569921875" />
                  <Point X="-1.100259887695" Y="-29.9380625" />
                  <Point X="-1.296842163086" Y="-29.887484375" />
                  <Point X="-1.35158984375" Y="-29.8733984375" />
                  <Point X="-1.341447998047" Y="-29.79636328125" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.350223999023" Y="-29.330947265625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.542518798828" Y="-29.06561328125" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649241333008" Y="-28.985763671875" />
                  <Point X="-1.85660144043" Y="-28.972171875" />
                  <Point X="-1.958830444336" Y="-28.96547265625" />
                  <Point X="-1.989878662109" Y="-28.973791015625" />
                  <Point X="-2.162662353516" Y="-29.089240234375" />
                  <Point X="-2.247845214844" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.333996582031" Y="-29.254076171875" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.71291796875" Y="-29.2561015625" />
                  <Point X="-2.855840820312" Y="-29.167607421875" />
                  <Point X="-3.128062255859" Y="-28.95800390625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-3.004649902344" Y="-28.492748046875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.597591796875" />
                  <Point X="-2.51398046875" Y="-27.568763671875" />
                  <Point X="-2.531329101562" Y="-27.551416015625" />
                  <Point X="-2.560157714844" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.017454345703" Y="-27.7892890625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.048792236328" Y="-27.99547265625" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.356870117188" Y="-27.519865234375" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-4.036203369141" Y="-27.09257421875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822265625" Y="-26.396015625" />
                  <Point X="-3.140661865234" Y="-26.37608984375" />
                  <Point X="-3.138117675781" Y="-26.366267578125" />
                  <Point X="-3.140326416016" Y="-26.334595703125" />
                  <Point X="-3.161160400391" Y="-26.310638671875" />
                  <Point X="-3.178898193359" Y="-26.30019921875" />
                  <Point X="-3.187643066406" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.757024658203" Y="-26.359333984375" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.883232421875" Y="-26.184078125" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.979029296875" Y="-25.650158203125" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.551277832031" Y="-25.3949375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.54189453125" Y="-25.121423828125" />
                  <Point X="-3.523310791016" Y="-25.108525390625" />
                  <Point X="-3.514146240234" Y="-25.102166015625" />
                  <Point X="-3.494899169922" Y="-25.07591015625" />
                  <Point X="-3.488702880859" Y="-25.0559453125" />
                  <Point X="-3.485648193359" Y="-25.046103515625" />
                  <Point X="-3.485647216797" Y="-25.016462890625" />
                  <Point X="-3.491843505859" Y="-24.996498046875" />
                  <Point X="-3.494898193359" Y="-24.986654296875" />
                  <Point X="-3.514140380859" Y="-24.9603984375" />
                  <Point X="-3.532729003906" Y="-24.947498046875" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.047416259766" Y="-24.8026328125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.94610546875" Y="-24.1959140625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.813703125" Y="-23.620005859375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.46565625" Y="-23.512232421875" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731702148438" Y="-23.6041328125" />
                  <Point X="-3.690559570313" Y="-23.591162109375" />
                  <Point X="-3.670276123047" Y="-23.584765625" />
                  <Point X="-3.651533447266" Y="-23.573943359375" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.622611328125" Y="-23.516359375" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316650391" Y="-23.45448828125" />
                  <Point X="-3.636236083984" Y="-23.41622265625" />
                  <Point X="-3.646056396484" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.941006347656" Y="-23.165130859375" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.270601074219" Y="-22.402455078125" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.884676025391" Y="-21.8590859375" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.596687011719" Y="-21.82044921875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138513427734" Y="-22.07956640625" />
                  <Point X="-3.081213134766" Y="-22.084580078125" />
                  <Point X="-3.052964111328" Y="-22.08705078125" />
                  <Point X="-3.031506347656" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.972580810547" Y="-22.031923828125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.943087402344" Y="-21.914859375" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.076603759766" Y="-21.650263671875" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.945482177734" Y="-20.973337890625" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.3192265625" Y="-20.5847421875" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.101767822266" Y="-20.53787109375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246582031" Y="-20.726337890625" />
                  <Point X="-1.887471679688" Y="-20.7595390625" />
                  <Point X="-1.856030517578" Y="-20.77590625" />
                  <Point X="-1.835124267578" Y="-20.781509765625" />
                  <Point X="-1.813809204102" Y="-20.77775" />
                  <Point X="-1.747383300781" Y="-20.750234375" />
                  <Point X="-1.714635253906" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.664462890625" Y="-20.63694140625" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.666076049805" Y="-20.47403125" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.217434814453" Y="-20.166611328125" />
                  <Point X="-0.968083007812" Y="-20.096703125" />
                  <Point X="-0.44237890625" Y="-20.03517578125" />
                  <Point X="-0.224200027466" Y="-20.009640625" />
                  <Point X="-0.171573852539" Y="-20.206044921875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282117844" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594036102" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.118263366699" Y="-20.450951171875" />
                  <Point X="0.236648422241" Y="-20.009130859375" />
                  <Point X="0.642493408203" Y="-20.051634765625" />
                  <Point X="0.860209777832" Y="-20.074435546875" />
                  <Point X="1.295143310547" Y="-20.17944140625" />
                  <Point X="1.508455566406" Y="-20.230943359375" />
                  <Point X="1.792042236328" Y="-20.33380078125" />
                  <Point X="1.931046508789" Y="-20.384220703125" />
                  <Point X="2.204780029297" Y="-20.512236328125" />
                  <Point X="2.338686279297" Y="-20.574859375" />
                  <Point X="2.6031484375" Y="-20.728935546875" />
                  <Point X="2.73251953125" Y="-20.804306640625" />
                  <Point X="2.981934814453" Y="-20.981677734375" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.806964355469" Y="-21.49681640625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.210471923828" Y="-22.56226171875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.207651855469" Y="-22.65417578125" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682617188" Y="-22.6991875" />
                  <Point X="2.247455322266" Y="-22.741591796875" />
                  <Point X="2.261640380859" Y="-22.76249609375" />
                  <Point X="2.274939208984" Y="-22.775794921875" />
                  <Point X="2.317342773438" Y="-22.804568359375" />
                  <Point X="2.338247802734" Y="-22.81875390625" />
                  <Point X="2.360334228516" Y="-22.827021484375" />
                  <Point X="2.406834472656" Y="-22.832626953125" />
                  <Point X="2.429759033203" Y="-22.835392578125" />
                  <Point X="2.448665039062" Y="-22.8340546875" />
                  <Point X="2.502440185547" Y="-22.819673828125" />
                  <Point X="2.528951416016" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.033831787109" Y="-22.52306640625" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.125850585938" Y="-22.151466796875" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.341634765625" Y="-22.487890625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.048436523438" Y="-22.82388671875" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371826172" Y="-23.41616796875" />
                  <Point X="3.240669677734" Y="-23.466658203125" />
                  <Point X="3.221589599609" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.198703125" Y="-23.56005078125" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.202614013672" Y="-23.666390625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215647216797" Y="-23.712046875" />
                  <Point X="3.247835693359" Y="-23.76097265625" />
                  <Point X="3.263704833984" Y="-23.785091796875" />
                  <Point X="3.280947509766" Y="-23.8011796875" />
                  <Point X="3.327593017578" Y="-23.8274375" />
                  <Point X="3.350589599609" Y="-23.8403828125" />
                  <Point X="3.368565429688" Y="-23.846380859375" />
                  <Point X="3.431633300781" Y="-23.85471484375" />
                  <Point X="3.462726074219" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.943928710938" Y="-23.79719921875" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.906228515625" Y="-23.9132265625" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.983004882812" Y="-24.330041015625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.611938476562" Y="-24.5288515625" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087890625" Y="-24.767181640625" />
                  <Point X="3.667125488281" Y="-24.80299609375" />
                  <Point X="3.636577880859" Y="-24.82065234375" />
                  <Point X="3.622264404297" Y="-24.83307421875" />
                  <Point X="3.585086914062" Y="-24.880447265625" />
                  <Point X="3.566758544922" Y="-24.903802734375" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.544592529297" Y="-24.989974609375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.550875732422" Y="-25.10539453125" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.603936035156" Y="-25.206130859375" />
                  <Point X="3.622264648438" Y="-25.229486328125" />
                  <Point X="3.636575683594" Y="-25.24190625" />
                  <Point X="3.698538085938" Y="-25.277720703125" />
                  <Point X="3.729085693359" Y="-25.29537890625" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.170459472656" Y="-25.415416015625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.966834960938" Y="-25.8443359375" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.892295898438" Y="-26.21239453125" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.421831054688" Y="-26.230578125" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836181641" Y="-26.098341796875" />
                  <Point X="3.273226074219" Y="-26.1247734375" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.111939697266" Y="-26.2431015625" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.053822753906" Y="-26.42855859375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360839844" Y="-26.516623046875" />
                  <Point X="3.123661621094" Y="-26.6213046875" />
                  <Point X="3.156841064453" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.566846191406" Y="-26.991234375" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.256008300781" Y="-27.718197265625" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.088039306641" Y="-27.96709375" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.652861328125" Y="-27.778490234375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.592605224609" Y="-27.22717578125" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489078125" Y="-27.21924609375" />
                  <Point X="2.368838378906" Y="-27.28252734375" />
                  <Point X="2.309560058594" Y="-27.313724609375" />
                  <Point X="2.288600097656" Y="-27.33468359375" />
                  <Point X="2.225318847656" Y="-27.454923828125" />
                  <Point X="2.19412109375" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.215302001953" Y="-27.691111328125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.490094238281" Y="-28.22198828125" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.896855957031" Y="-29.1462421875" />
                  <Point X="2.835295654297" Y="-29.19021484375" />
                  <Point X="2.705498535156" Y="-29.274228515625" />
                  <Point X="2.679774902344" Y="-29.29087890625" />
                  <Point X="2.369508056641" Y="-28.88653125" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.52780078125" Y="-27.888693359375" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425805175781" Y="-27.83571875" />
                  <Point X="1.269685668945" Y="-27.850083984375" />
                  <Point X="1.192718383789" Y="-27.857166015625" />
                  <Point X="1.165332763672" Y="-27.8685078125" />
                  <Point X="1.04478125" Y="-27.968744140625" />
                  <Point X="0.985349060059" Y="-28.01816015625" />
                  <Point X="0.96845690918" Y="-28.04598828125" />
                  <Point X="0.932412475586" Y="-28.2118203125" />
                  <Point X="0.914642700195" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.98647869873" Y="-28.86178515625" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.05258215332" Y="-29.950482421875" />
                  <Point X="0.99436932373" Y="-29.9632421875" />
                  <Point X="0.874437438965" Y="-29.985029296875" />
                  <Point X="0.860200256348" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#184" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.129433535499" Y="4.838177028909" Z="1.7" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.7" />
                  <Point X="-0.45434698919" Y="5.045764806741" Z="1.7" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.7" />
                  <Point X="-1.237088461377" Y="4.912813662684" Z="1.7" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.7" />
                  <Point X="-1.721804275216" Y="4.550724258786" Z="1.7" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.7" />
                  <Point X="-1.718304781699" Y="4.409374967938" Z="1.7" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.7" />
                  <Point X="-1.772673227017" Y="4.327239286749" Z="1.7" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.7" />
                  <Point X="-1.870540247716" Y="4.316091947127" Z="1.7" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.7" />
                  <Point X="-2.06825641938" Y="4.523846994904" Z="1.7" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.7" />
                  <Point X="-2.349665438443" Y="4.490245306979" Z="1.7" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.7" />
                  <Point X="-2.982060157824" Y="4.097622286191" Z="1.7" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.7" />
                  <Point X="-3.126061088333" Y="3.356016272433" Z="1.7" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.7" />
                  <Point X="-2.999053182556" Y="3.112063773088" Z="1.7" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.7" />
                  <Point X="-3.014091451655" Y="3.034712116231" Z="1.7" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.7" />
                  <Point X="-3.083012692519" Y="2.996511516617" Z="1.7" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.7" />
                  <Point X="-3.577843075864" Y="3.254132860412" Z="1.7" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.7" />
                  <Point X="-3.930295423001" Y="3.202897702492" Z="1.7" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.7" />
                  <Point X="-4.319939363342" Y="2.654029776433" Z="1.7" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.7" />
                  <Point X="-3.97760018604" Y="1.826482059631" Z="1.7" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.7" />
                  <Point X="-3.686741932212" Y="1.59196938707" Z="1.7" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.7" />
                  <Point X="-3.674961242716" Y="1.534055556059" Z="1.7" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.7" />
                  <Point X="-3.711753358111" Y="1.487804769401" Z="1.7" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.7" />
                  <Point X="-4.465286031611" Y="1.568620480768" Z="1.7" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.7" />
                  <Point X="-4.868119024286" Y="1.424352983686" Z="1.7" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.7" />
                  <Point X="-5.00172280124" Y="0.842684967303" Z="1.7" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.7" />
                  <Point X="-4.066513210302" Y="0.180351403991" Z="1.7" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.7" />
                  <Point X="-3.567396495854" Y="0.04270852628" Z="1.7" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.7" />
                  <Point X="-3.545753060319" Y="0.01996443795" Z="1.7" />
                  <Point X="-3.539556741714" Y="0" Z="1.7" />
                  <Point X="-3.542611540816" Y="-0.009842513111" Z="1.7" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.7" />
                  <Point X="-3.557972099269" Y="-0.036167456854" Z="1.7" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.7" />
                  <Point X="-4.57037457847" Y="-0.315360652765" Z="1.7" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.7" />
                  <Point X="-5.03468128953" Y="-0.625955498345" Z="1.7" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.7" />
                  <Point X="-4.937833125927" Y="-1.165173103501" Z="1.7" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.7" />
                  <Point X="-3.75665418293" Y="-1.377625892416" Z="1.7" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.7" />
                  <Point X="-3.210414110146" Y="-1.312010103578" Z="1.7" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.7" />
                  <Point X="-3.195220397131" Y="-1.33227264126" Z="1.7" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.7" />
                  <Point X="-4.072797294467" Y="-2.021625861309" Z="1.7" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.7" />
                  <Point X="-4.40596926314" Y="-2.514194778188" Z="1.7" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.7" />
                  <Point X="-4.094734173391" Y="-2.994475097951" Z="1.7" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.7" />
                  <Point X="-2.998610330662" Y="-2.801309871748" Z="1.7" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.7" />
                  <Point X="-2.567110864516" Y="-2.561219604615" Z="1.7" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.7" />
                  <Point X="-3.054107141421" Y="-3.43646805183" Z="1.7" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.7" />
                  <Point X="-3.164721934912" Y="-3.966341543624" Z="1.7" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.7" />
                  <Point X="-2.745395778957" Y="-4.267331923138" Z="1.7" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.7" />
                  <Point X="-2.300484708943" Y="-4.253232831091" Z="1.7" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.7" />
                  <Point X="-2.141039579276" Y="-4.099534836479" Z="1.7" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.7" />
                  <Point X="-1.866026981808" Y="-3.990784818675" Z="1.7" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.7" />
                  <Point X="-1.581642091844" Y="-4.071924925834" Z="1.7" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.7" />
                  <Point X="-1.40541929354" Y="-4.309420266168" Z="1.7" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.7" />
                  <Point X="-1.397176219213" Y="-4.75855739091" Z="1.7" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.7" />
                  <Point X="-1.315457272862" Y="-4.904625630733" Z="1.7" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.7" />
                  <Point X="-1.018427158314" Y="-4.974794906503" Z="1.7" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.7" />
                  <Point X="-0.549362190992" Y="-4.01243166835" Z="1.7" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.7" />
                  <Point X="-0.363022476829" Y="-3.440876725295" Z="1.7" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.7" />
                  <Point X="-0.169697719454" Y="-3.256907278127" Z="1.7" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.7" />
                  <Point X="0.083661359907" Y="-3.230204767161" Z="1.7" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.7" />
                  <Point X="0.307423382604" Y="-3.360769030473" Z="1.7" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.7" />
                  <Point X="0.685392669409" Y="-4.520104361466" Z="1.7" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.7" />
                  <Point X="0.87721859476" Y="-5.002944795799" Z="1.7" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.7" />
                  <Point X="1.0571316454" Y="-4.968041981196" Z="1.7" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.7" />
                  <Point X="1.029894998943" Y="-3.823979628599" Z="1.7" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.7" />
                  <Point X="0.975115708799" Y="-3.191158100205" Z="1.7" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.7" />
                  <Point X="1.07059220249" Y="-2.975910099437" Z="1.7" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.7" />
                  <Point X="1.268110997722" Y="-2.868592843794" Z="1.7" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.7" />
                  <Point X="1.494605660294" Y="-2.899471342927" Z="1.7" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.7" />
                  <Point X="2.323684252007" Y="-3.885688219946" Z="1.7" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.7" />
                  <Point X="2.726512284826" Y="-4.284923269893" Z="1.7" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.7" />
                  <Point X="2.919761941913" Y="-4.155650021361" Z="1.7" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.7" />
                  <Point X="2.527239642241" Y="-3.165708199036" Z="1.7" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.7" />
                  <Point X="2.258350245164" Y="-2.650943689118" Z="1.7" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.7" />
                  <Point X="2.263408923891" Y="-2.44692978986" Z="1.7" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.7" />
                  <Point X="2.385968588512" Y="-2.295492386076" Z="1.7" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.7" />
                  <Point X="2.577563114394" Y="-2.245097764327" Z="1.7" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.7" />
                  <Point X="3.621705398071" Y="-2.790509858034" Z="1.7" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.7" />
                  <Point X="4.122771808405" Y="-2.96459018521" Z="1.7" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.7" />
                  <Point X="4.292386005877" Y="-2.713201825202" Z="1.7" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.7" />
                  <Point X="3.591128243248" Y="-1.920284953338" Z="1.7" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.7" />
                  <Point X="3.159563354236" Y="-1.562984543507" Z="1.7" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.7" />
                  <Point X="3.097456342017" Y="-1.401859845125" Z="1.7" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.7" />
                  <Point X="3.144229898983" Y="-1.243788635974" Z="1.7" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.7" />
                  <Point X="3.27768964184" Y="-1.142352908468" Z="1.7" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.7" />
                  <Point X="4.409148884883" Y="-1.248869560737" Z="1.7" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.7" />
                  <Point X="4.934886801273" Y="-1.192239555332" Z="1.7" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.7" />
                  <Point X="5.010120514147" Y="-0.820508192406" Z="1.7" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.7" />
                  <Point X="4.177244557395" Y="-0.335839038193" Z="1.7" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.7" />
                  <Point X="3.717405102286" Y="-0.203153578043" Z="1.7" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.7" />
                  <Point X="3.637114156266" Y="-0.143983316807" Z="1.7" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.7" />
                  <Point X="3.593827204234" Y="-0.064708898005" Z="1.7" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.7" />
                  <Point X="3.58754424619" Y="0.031901633193" Z="1.7" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.7" />
                  <Point X="3.618265282134" Y="0.119965421768" Z="1.7" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.7" />
                  <Point X="3.685990312066" Y="0.184995266199" Z="1.7" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.7" />
                  <Point X="4.618723259912" Y="0.454132894576" Z="1.7" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.7" />
                  <Point X="5.026253630123" Y="0.708931777884" Z="1.7" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.7" />
                  <Point X="4.948653694054" Y="1.129880875598" Z="1.7" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.7" />
                  <Point X="3.931246348573" Y="1.283653918903" Z="1.7" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.7" />
                  <Point X="3.432028704986" Y="1.22613336343" Z="1.7" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.7" />
                  <Point X="3.345964205965" Y="1.247413568" Z="1.7" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.7" />
                  <Point X="3.283449531212" Y="1.297791245093" Z="1.7" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.7" />
                  <Point X="3.245426551594" Y="1.374993095711" Z="1.7" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.7" />
                  <Point X="3.240699428708" Y="1.457763547961" Z="1.7" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.7" />
                  <Point X="3.274196159341" Y="1.534205292421" Z="1.7" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.7" />
                  <Point X="4.072718653904" Y="2.167725852457" Z="1.7" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.7" />
                  <Point X="4.378256035397" Y="2.569276840375" Z="1.7" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.7" />
                  <Point X="4.160280221175" Y="2.909015939849" Z="1.7" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.7" />
                  <Point X="3.002675777698" Y="2.551515727334" Z="1.7" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.7" />
                  <Point X="2.483366701054" Y="2.259909322487" Z="1.7" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.7" />
                  <Point X="2.40666696382" Y="2.248293468038" Z="1.7" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.7" />
                  <Point X="2.33926170243" Y="2.268085480732" Z="1.7" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.7" />
                  <Point X="2.282673119503" Y="2.317763157952" Z="1.7" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.7" />
                  <Point X="2.251136234721" Y="2.383091480013" Z="1.7" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.7" />
                  <Point X="2.25261862017" Y="2.456102874828" Z="1.7" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.7" />
                  <Point X="2.844109625525" Y="3.509463304257" Z="1.7" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.7" />
                  <Point X="3.004755827691" Y="4.090351201002" Z="1.7" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.7" />
                  <Point X="2.622162261773" Y="4.345548412883" Z="1.7" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.7" />
                  <Point X="2.21980542286" Y="4.564336083476" Z="1.7" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.7" />
                  <Point X="1.802935175926" Y="4.744483030455" Z="1.7" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.7" />
                  <Point X="1.30072204223" Y="4.900441822969" Z="1.7" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.7" />
                  <Point X="0.641545356148" Y="5.029373966098" Z="1.7" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.7" />
                  <Point X="0.063811026054" Y="4.593270477042" Z="1.7" />
                  <Point X="0" Y="4.355124473572" Z="1.7" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>