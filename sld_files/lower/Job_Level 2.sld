<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#119" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="337" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475585938" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.571387817383" Y="-28.542701171875" />
                  <Point X="0.563302124023" Y="-28.5125234375" />
                  <Point X="0.555903381348" Y="-28.49343359375" />
                  <Point X="0.545378234863" Y="-28.473103515625" />
                  <Point X="0.539058288574" Y="-28.46261328125" />
                  <Point X="0.378635406494" Y="-28.231474609375" />
                  <Point X="0.357096191406" Y="-28.207509765625" />
                  <Point X="0.328327056885" Y="-28.1873125" />
                  <Point X="0.308468933105" Y="-28.17830078125" />
                  <Point X="0.297370300293" Y="-28.174080078125" />
                  <Point X="0.0491365242" Y="-28.097037109375" />
                  <Point X="0.0203969841" Y="-28.09159375" />
                  <Point X="-0.011795484543" Y="-28.09223046875" />
                  <Point X="-0.031733154297" Y="-28.09606640625" />
                  <Point X="-0.041943489075" Y="-28.098625" />
                  <Point X="-0.29018258667" Y="-28.17566796875" />
                  <Point X="-0.319508483887" Y="-28.188984375" />
                  <Point X="-0.347585174561" Y="-28.2105078125" />
                  <Point X="-0.362407806396" Y="-28.227087890625" />
                  <Point X="-0.369628326416" Y="-28.236236328125" />
                  <Point X="-0.530051208496" Y="-28.467375" />
                  <Point X="-0.538189453125" Y="-28.48157421875" />
                  <Point X="-0.550989929199" Y="-28.5125234375" />
                  <Point X="-0.916584350586" Y="-29.87694140625" />
                  <Point X="-1.07933190918" Y="-29.8453515625" />
                  <Point X="-1.081078491211" Y="-29.84490234375" />
                  <Point X="-1.246417724609" Y="-29.802361328125" />
                  <Point X="-1.215936523438" Y="-29.570833984375" />
                  <Point X="-1.214440063477" Y="-29.543630859375" />
                  <Point X="-1.216194580078" Y="-29.52120703125" />
                  <Point X="-1.21773046875" Y="-29.510083984375" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287037353516" Y="-29.1817578125" />
                  <Point X="-1.305322631836" Y="-29.15168359375" />
                  <Point X="-1.320364990234" Y="-29.134962890625" />
                  <Point X="-1.328353149414" Y="-29.127076171875" />
                  <Point X="-1.556905639648" Y="-28.926640625" />
                  <Point X="-1.583204223633" Y="-28.908791015625" />
                  <Point X="-1.616027587891" Y="-28.89605859375" />
                  <Point X="-1.638140869141" Y="-28.891951171875" />
                  <Point X="-1.649276977539" Y="-28.890556640625" />
                  <Point X="-1.952616943359" Y="-28.87067578125" />
                  <Point X="-1.984345947266" Y="-28.872525390625" />
                  <Point X="-2.018122558594" Y="-28.882400390625" />
                  <Point X="-2.038169799805" Y="-28.8926015625" />
                  <Point X="-2.047864990234" Y="-28.898279296875" />
                  <Point X="-2.300624267578" Y="-29.06716796875" />
                  <Point X="-2.312788085938" Y="-29.076822265625" />
                  <Point X="-2.335103515625" Y="-29.099462890625" />
                  <Point X="-2.480148193359" Y="-29.28848828125" />
                  <Point X="-2.801720947266" Y="-29.089380859375" />
                  <Point X="-2.804124511719" Y="-29.087529296875" />
                  <Point X="-3.104721923828" Y="-28.856080078125" />
                  <Point X="-2.438787597656" Y="-27.702646484375" />
                  <Point X="-2.423760986328" Y="-27.676619140625" />
                  <Point X="-2.412853759766" Y="-27.647626953125" />
                  <Point X="-2.406586425781" Y="-27.61607421875" />
                  <Point X="-2.405616210938" Y="-27.58488671875" />
                  <Point X="-2.414802978516" Y="-27.55506640625" />
                  <Point X="-2.429404052734" Y="-27.525876953125" />
                  <Point X="-2.447193115234" Y="-27.501201171875" />
                  <Point X="-2.464154541016" Y="-27.484240234375" />
                  <Point X="-2.489311767578" Y="-27.466212890625" />
                  <Point X="-2.518141113281" Y="-27.45199609375" />
                  <Point X="-2.547758789062" Y="-27.44301171875" />
                  <Point X="-2.578692871094" Y="-27.444025390625" />
                  <Point X="-2.610219482422" Y="-27.450296875" />
                  <Point X="-2.639184570312" Y="-27.46119921875" />
                  <Point X="-3.8180234375" Y="-28.141802734375" />
                  <Point X="-4.082861816406" Y="-27.793859375" />
                  <Point X="-4.084587646484" Y="-27.79096484375" />
                  <Point X="-4.306142089844" Y="-27.419451171875" />
                  <Point X="-3.132246337891" Y="-26.518689453125" />
                  <Point X="-3.105954833984" Y="-26.498515625" />
                  <Point X="-3.084248046875" Y="-26.47509375" />
                  <Point X="-3.066121826172" Y="-26.44733984375" />
                  <Point X="-3.053695556641" Y="-26.4192109375" />
                  <Point X="-3.046152099609" Y="-26.3900859375" />
                  <Point X="-3.043348144531" Y="-26.359650390625" />
                  <Point X="-3.045560058594" Y="-26.327970703125" />
                  <Point X="-3.052677734375" Y="-26.297953125" />
                  <Point X="-3.069039306641" Y="-26.271796875" />
                  <Point X="-3.090417236328" Y="-26.247525390625" />
                  <Point X="-3.113521728516" Y="-26.228443359375" />
                  <Point X="-3.139456542969" Y="-26.2131796875" />
                  <Point X="-3.168720703125" Y="-26.20195703125" />
                  <Point X="-3.200605712891" Y="-26.1954765625" />
                  <Point X="-3.231927001953" Y="-26.194384765625" />
                  <Point X="-4.7321015625" Y="-26.39188671875" />
                  <Point X="-4.834077148438" Y="-25.99265625" />
                  <Point X="-4.834533691406" Y="-25.989462890625" />
                  <Point X="-4.892423828125" Y="-25.584701171875" />
                  <Point X="-3.562825439453" Y="-25.228435546875" />
                  <Point X="-3.532875488281" Y="-25.22041015625" />
                  <Point X="-3.516926025391" Y="-25.2145625" />
                  <Point X="-3.487168945312" Y="-25.19908203125" />
                  <Point X="-3.459976318359" Y="-25.180208984375" />
                  <Point X="-3.437174560547" Y="-25.157849609375" />
                  <Point X="-3.417743652344" Y="-25.1309921875" />
                  <Point X="-3.403981445312" Y="-25.103466796875" />
                  <Point X="-3.394917236328" Y="-25.07426171875" />
                  <Point X="-3.390649658203" Y="-25.045515625" />
                  <Point X="-3.3908359375" Y="-25.0152734375" />
                  <Point X="-3.395103271484" Y="-24.987701171875" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.418660400391" Y="-24.929970703125" />
                  <Point X="-3.438475585938" Y="-24.903318359375" />
                  <Point X="-3.460548095703" Y="-24.881955078125" />
                  <Point X="-3.487730224609" Y="-24.86308984375" />
                  <Point X="-3.501921875" Y="-24.854955078125" />
                  <Point X="-3.532875244141" Y="-24.842150390625" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.824488769531" Y="-24.023033203125" />
                  <Point X="-4.823565429688" Y="-24.019625" />
                  <Point X="-4.703550292969" Y="-23.576732421875" />
                  <Point X="-3.787134521484" Y="-23.697380859375" />
                  <Point X="-3.765665527344" Y="-23.70020703125" />
                  <Point X="-3.744997802734" Y="-23.700658203125" />
                  <Point X="-3.723446044922" Y="-23.698775390625" />
                  <Point X="-3.703094482422" Y="-23.69472265625" />
                  <Point X="-3.701898193359" Y="-23.694345703125" />
                  <Point X="-3.641711914063" Y="-23.675369140625" />
                  <Point X="-3.622784667969" Y="-23.667041015625" />
                  <Point X="-3.604040283203" Y="-23.656220703125" />
                  <Point X="-3.587354980469" Y="-23.64398828125" />
                  <Point X="-3.573713623047" Y="-23.62843359375" />
                  <Point X="-3.561299316406" Y="-23.610703125" />
                  <Point X="-3.551352050781" Y="-23.5925703125" />
                  <Point X="-3.550854492188" Y="-23.591369140625" />
                  <Point X="-3.526704589844" Y="-23.53306640625" />
                  <Point X="-3.520914550781" Y="-23.51319921875" />
                  <Point X="-3.517156494141" Y="-23.491875" />
                  <Point X="-3.515805664062" Y="-23.4712265625" />
                  <Point X="-3.518957763672" Y="-23.45077734375" />
                  <Point X="-3.524566650391" Y="-23.429865234375" />
                  <Point X="-3.532058105469" Y="-23.410607421875" />
                  <Point X="-3.532650878906" Y="-23.40946875" />
                  <Point X="-3.561790283203" Y="-23.3534921875" />
                  <Point X="-3.573282958984" Y="-23.33629296875" />
                  <Point X="-3.587196044922" Y="-23.319712890625" />
                  <Point X="-3.602136230469" Y="-23.30541015625" />
                  <Point X="-4.351859863281" Y="-22.730125" />
                  <Point X="-4.081155517578" Y="-22.266345703125" />
                  <Point X="-4.078716796875" Y="-22.2632109375" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.219843994141" Y="-22.14771484375" />
                  <Point X="-3.206658447266" Y="-22.155328125" />
                  <Point X="-3.187704101563" Y="-22.163666015625" />
                  <Point X="-3.167040771484" Y="-22.17017578125" />
                  <Point X="-3.146833496094" Y="-22.17419921875" />
                  <Point X="-3.145068603516" Y="-22.17435546875" />
                  <Point X="-3.06124609375" Y="-22.181689453125" />
                  <Point X="-3.040580078125" Y="-22.181240234375" />
                  <Point X="-3.0191328125" Y="-22.178419921875" />
                  <Point X="-2.999048095703" Y="-22.173509765625" />
                  <Point X="-2.980501464844" Y="-22.164369140625" />
                  <Point X="-2.962252929688" Y="-22.152751953125" />
                  <Point X="-2.946134521484" Y="-22.139828125" />
                  <Point X="-2.944851074219" Y="-22.138546875" />
                  <Point X="-2.885353271484" Y="-22.079048828125" />
                  <Point X="-2.872406738281" Y="-22.062916015625" />
                  <Point X="-2.860777587891" Y="-22.044662109375" />
                  <Point X="-2.851628662109" Y="-22.026111328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843436035156" Y="-21.963876953125" />
                  <Point X="-2.843587158203" Y="-21.962150390625" />
                  <Point X="-2.850920654297" Y="-21.878326171875" />
                  <Point X="-2.854955322266" Y="-21.85804296875" />
                  <Point X="-2.861463623047" Y="-21.837400390625" />
                  <Point X="-2.869794677734" Y="-21.818466796875" />
                  <Point X="-3.183332763672" Y="-21.275404296875" />
                  <Point X="-2.700613525391" Y="-20.90530859375" />
                  <Point X="-2.696784423828" Y="-20.903181640625" />
                  <Point X="-2.167037353516" Y="-20.6088671875" />
                  <Point X="-2.047229736328" Y="-20.765001953125" />
                  <Point X="-2.043193115234" Y="-20.77026171875" />
                  <Point X="-2.028906005859" Y="-20.785189453125" />
                  <Point X="-2.012354858398" Y="-20.799083984375" />
                  <Point X="-1.995182006836" Y="-20.810568359375" />
                  <Point X="-1.99319140625" Y="-20.81160546875" />
                  <Point X="-1.899897216797" Y="-20.860171875" />
                  <Point X="-1.880614746094" Y="-20.867669921875" />
                  <Point X="-1.859698608398" Y="-20.8732734375" />
                  <Point X="-1.839244140625" Y="-20.876419921875" />
                  <Point X="-1.81859362793" Y="-20.8750625" />
                  <Point X="-1.797270263672" Y="-20.871296875" />
                  <Point X="-1.77732043457" Y="-20.86546484375" />
                  <Point X="-1.775435668945" Y="-20.864681640625" />
                  <Point X="-1.678280639648" Y="-20.824439453125" />
                  <Point X="-1.660146484375" Y="-20.814490234375" />
                  <Point X="-1.642416748047" Y="-20.802076171875" />
                  <Point X="-1.626864257812" Y="-20.7884375" />
                  <Point X="-1.614632568359" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595481323242" Y="-20.73408203125" />
                  <Point X="-1.594829711914" Y="-20.732015625" />
                  <Point X="-1.563201904297" Y="-20.631705078125" />
                  <Point X="-1.559165527344" Y="-20.611416015625" />
                  <Point X="-1.557278930664" Y="-20.58985546875" />
                  <Point X="-1.557730224609" Y="-20.569173828125" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-0.949621643066" Y="-20.190189453125" />
                  <Point X="-0.944988830566" Y="-20.189646484375" />
                  <Point X="-0.294711181641" Y="-20.113541015625" />
                  <Point X="-0.137803833008" Y="-20.699126953125" />
                  <Point X="-0.133903427124" Y="-20.71368359375" />
                  <Point X="-0.121132087708" Y="-20.74187109375" />
                  <Point X="-0.103273948669" Y="-20.768599609375" />
                  <Point X="-0.082115707397" Y="-20.79119140625" />
                  <Point X="-0.05481804657" Y="-20.805783203125" />
                  <Point X="-0.024379669189" Y="-20.816115234375" />
                  <Point X="0.006155908585" Y="-20.82115625" />
                  <Point X="0.036691532135" Y="-20.816115234375" />
                  <Point X="0.067130065918" Y="-20.805783203125" />
                  <Point X="0.09442755127" Y="-20.79119140625" />
                  <Point X="0.11558581543" Y="-20.7686015625" />
                  <Point X="0.133443954468" Y="-20.741873046875" />
                  <Point X="0.146215454102" Y="-20.71368359375" />
                  <Point X="0.307419219971" Y="-20.1120625" />
                  <Point X="0.844041809082" Y="-20.16826171875" />
                  <Point X="0.847887695312" Y="-20.169189453125" />
                  <Point X="1.481030761719" Y="-20.32205078125" />
                  <Point X="1.482095947266" Y="-20.322435546875" />
                  <Point X="1.894659179688" Y="-20.472078125" />
                  <Point X="1.897074462891" Y="-20.47320703125" />
                  <Point X="2.294562011719" Y="-20.659099609375" />
                  <Point X="2.296956787109" Y="-20.660494140625" />
                  <Point X="2.680973388672" Y="-20.88422265625" />
                  <Point X="2.683198242188" Y="-20.8858046875" />
                  <Point X="2.943259765625" Y="-21.07074609375" />
                  <Point X="2.164973144531" Y="-22.418779296875" />
                  <Point X="2.147581298828" Y="-22.44890234375" />
                  <Point X="2.141544433594" Y="-22.461380859375" />
                  <Point X="2.132643554688" Y="-22.48556640625" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108539306641" Y="-22.583314453125" />
                  <Point X="2.107370605469" Y="-22.6036171875" />
                  <Point X="2.107896728516" Y="-22.62044921875" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121434570313" Y="-22.710376953125" />
                  <Point X="2.1296875" Y="-22.7324453125" />
                  <Point X="2.140038085938" Y="-22.75248046875" />
                  <Point X="2.140938964844" Y="-22.75380859375" />
                  <Point X="2.183029541016" Y="-22.815837890625" />
                  <Point X="2.195432128906" Y="-22.830625" />
                  <Point X="2.210009033203" Y="-22.844791015625" />
                  <Point X="2.222874755859" Y="-22.8552734375" />
                  <Point X="2.284905517578" Y="-22.897365234375" />
                  <Point X="2.304979980469" Y="-22.90773828125" />
                  <Point X="2.327104492188" Y="-22.916009765625" />
                  <Point X="2.349039550781" Y="-22.92134765625" />
                  <Point X="2.350405029297" Y="-22.92151171875" />
                  <Point X="2.418387451172" Y="-22.929708984375" />
                  <Point X="2.437930419922" Y="-22.930041015625" />
                  <Point X="2.458454589844" Y="-22.92826953125" />
                  <Point X="2.474828125" Y="-22.925396484375" />
                  <Point X="2.553493896484" Y="-22.904359375" />
                  <Point X="2.565286865234" Y="-22.900361328125" />
                  <Point X="2.588534423828" Y="-22.889853515625" />
                  <Point X="3.967326171875" Y="-22.09380859375" />
                  <Point X="4.12326953125" Y="-22.31053515625" />
                  <Point X="4.124506835938" Y="-22.312580078125" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.253682128906" Y="-23.313978515625" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.220309082031" Y="-23.34089453125" />
                  <Point X="3.202807373047" Y="-23.35989453125" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.135984863281" Y="-23.45035546875" />
                  <Point X="3.127080566406" Y="-23.468859375" />
                  <Point X="3.121195068359" Y="-23.484466796875" />
                  <Point X="3.100105712891" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739746094" Y="-23.628234375" />
                  <Point X="3.098096435547" Y="-23.629962890625" />
                  <Point X="3.115408691406" Y="-23.7138671875" />
                  <Point X="3.121277832031" Y="-23.73243359375" />
                  <Point X="3.129446044922" Y="-23.751287109375" />
                  <Point X="3.137252929688" Y="-23.765736328125" />
                  <Point X="3.184340576172" Y="-23.837306640625" />
                  <Point X="3.198896972656" Y="-23.8545546875" />
                  <Point X="3.21614453125" Y="-23.870646484375" />
                  <Point X="3.234458496094" Y="-23.884029296875" />
                  <Point X="3.235755615234" Y="-23.8847578125" />
                  <Point X="3.303987304688" Y="-23.92316796875" />
                  <Point X="3.322101318359" Y="-23.931009765625" />
                  <Point X="3.341977783203" Y="-23.9372578125" />
                  <Point X="3.358018798828" Y="-23.9408125" />
                  <Point X="3.450278808594" Y="-23.953005859375" />
                  <Point X="3.462697753906" Y="-23.95382421875" />
                  <Point X="3.488203857422" Y="-23.953015625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845936035156" Y="-24.06719140625" />
                  <Point X="4.846325683594" Y="-24.069693359375" />
                  <Point X="4.890864746094" Y="-24.35576171875" />
                  <Point X="3.742819091797" Y="-24.66337890625" />
                  <Point X="3.716577148438" Y="-24.670412109375" />
                  <Point X="3.703175537109" Y="-24.6751015625" />
                  <Point X="3.679679199219" Y="-24.68601171875" />
                  <Point X="3.589036376953" Y="-24.738404296875" />
                  <Point X="3.573131591797" Y="-24.749943359375" />
                  <Point X="3.557698730469" Y="-24.763791015625" />
                  <Point X="3.546409912109" Y="-24.775849609375" />
                  <Point X="3.492024414062" Y="-24.845150390625" />
                  <Point X="3.480302001953" Y="-24.864427734375" />
                  <Point X="3.470529541016" Y="-24.88588671875" />
                  <Point X="3.463684814453" Y="-24.907375" />
                  <Point X="3.463307128906" Y="-24.909345703125" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443498291016" Y="-25.0235859375" />
                  <Point X="3.443871826172" Y="-25.04434375" />
                  <Point X="3.445552001953" Y="-25.06050390625" />
                  <Point X="3.463680664062" Y="-25.1551640625" />
                  <Point X="3.470526123047" Y="-25.176662109375" />
                  <Point X="3.480299804688" Y="-25.198126953125" />
                  <Point X="3.492022216797" Y="-25.21740625" />
                  <Point X="3.493142578125" Y="-25.218833984375" />
                  <Point X="3.547528076172" Y="-25.288134765625" />
                  <Point X="3.56140625" Y="-25.302435546875" />
                  <Point X="3.577585449219" Y="-25.31593359375" />
                  <Point X="3.590901611328" Y="-25.325234375" />
                  <Point X="3.681544189453" Y="-25.37762890625" />
                  <Point X="3.692709960938" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.89147265625" Y="-25.706962890625" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.854522949219" Y="-25.950916015625" />
                  <Point X="4.801173828125" Y="-26.184697265625" />
                  <Point X="3.454813964844" Y="-26.007447265625" />
                  <Point X="3.424380859375" Y="-26.00344140625" />
                  <Point X="3.405106445312" Y="-26.00287890625" />
                  <Point X="3.384297119141" Y="-26.004388671875" />
                  <Point X="3.370994628906" Y="-26.006306640625" />
                  <Point X="3.193095214844" Y="-26.04497265625" />
                  <Point X="3.161443847656" Y="-26.0581875" />
                  <Point X="3.131401611328" Y="-26.077744140625" />
                  <Point X="3.110181884766" Y="-26.096625" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.986842529297" Y="-26.25308203125" />
                  <Point X="2.975181640625" Y="-26.28391796875" />
                  <Point X="2.969439941406" Y="-26.30881640625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.955109130859" Y="-26.508484375" />
                  <Point X="2.963745849609" Y="-26.541599609375" />
                  <Point X="2.972285644531" Y="-26.55991796875" />
                  <Point X="2.978479003906" Y="-26.57115234375" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086934326172" Y="-26.737240234375" />
                  <Point X="3.110629394531" Y="-26.76091015625" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.124817871094" Y="-27.749775390625" />
                  <Point X="4.12377734375" Y="-27.75125390625" />
                  <Point X="4.028980957031" Y="-27.885947265625" />
                  <Point X="2.828058837891" Y="-27.19259375" />
                  <Point X="2.800954101562" Y="-27.1769453125" />
                  <Point X="2.782955810547" Y="-27.1689140625" />
                  <Point X="2.762473876953" Y="-27.16222265625" />
                  <Point X="2.749855712891" Y="-27.159037109375" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.505991455078" Y="-27.11908203125" />
                  <Point X="2.472045166016" Y="-27.1248828125" />
                  <Point X="2.452744628906" Y="-27.1319609375" />
                  <Point X="2.441209228516" Y="-27.137083984375" />
                  <Point X="2.265314941406" Y="-27.22965625" />
                  <Point X="2.240001464844" Y="-27.24901953125" />
                  <Point X="2.217134765625" Y="-27.2736015625" />
                  <Point X="2.202624755859" Y="-27.2940625" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.098733398438" Y="-27.500109375" />
                  <Point X="2.094059326172" Y="-27.534421875" />
                  <Point X="2.095059570312" Y="-27.55528515625" />
                  <Point X="2.096463134766" Y="-27.567619140625" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138986816406" Y="-27.795146484375" />
                  <Point X="2.151819580078" Y="-27.826080078125" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.78184375" Y="-29.1116484375" />
                  <Point X="2.780694091797" Y="-29.112392578125" />
                  <Point X="2.701764160156" Y="-29.163482421875" />
                  <Point X="1.779229614258" Y="-27.9612109375" />
                  <Point X="1.758546020508" Y="-27.934255859375" />
                  <Point X="1.744694091797" Y="-27.9196953125" />
                  <Point X="1.727763793945" Y="-27.90530859375" />
                  <Point X="1.717621704102" Y="-27.897791015625" />
                  <Point X="1.508800048828" Y="-27.7635390625" />
                  <Point X="1.47974987793" Y="-27.7496484375" />
                  <Point X="1.445816040039" Y="-27.7420078125" />
                  <Point X="1.424647460938" Y="-27.741216796875" />
                  <Point X="1.412394897461" Y="-27.74155078125" />
                  <Point X="1.184013061523" Y="-27.76256640625" />
                  <Point X="1.152791259766" Y="-27.770962890625" />
                  <Point X="1.121768066406" Y="-27.78533203125" />
                  <Point X="1.100957519531" Y="-27.79848828125" />
                  <Point X="0.924611694336" Y="-27.945115234375" />
                  <Point X="0.902615600586" Y="-27.968638671875" />
                  <Point X="0.885034912109" Y="-27.998919921875" />
                  <Point X="0.877830505371" Y="-28.0192421875" />
                  <Point X="0.874538146973" Y="-28.030806640625" />
                  <Point X="0.821810119629" Y="-28.273396484375" />
                  <Point X="0.81972454834" Y="-28.289626953125" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="1.022065307617" Y="-29.859916015625" />
                  <Point X="0.975671081543" Y="-29.870083984375" />
                  <Point X="0.974598999023" Y="-29.870279296875" />
                  <Point X="0.929315856934" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058428833008" Y="-29.752634765625" />
                  <Point X="-1.14124609375" Y="-29.731326171875" />
                  <Point X="-1.121749267578" Y="-29.583234375" />
                  <Point X="-1.121079833984" Y="-29.576052734375" />
                  <Point X="-1.119583496094" Y="-29.548849609375" />
                  <Point X="-1.119729492188" Y="-29.536220703125" />
                  <Point X="-1.121484008789" Y="-29.513796875" />
                  <Point X="-1.124555908203" Y="-29.49155078125" />
                  <Point X="-1.183861572266" Y="-29.193400390625" />
                  <Point X="-1.186859985352" Y="-29.182046875" />
                  <Point X="-1.196861083984" Y="-29.15187109375" />
                  <Point X="-1.205863647461" Y="-29.132404296875" />
                  <Point X="-1.224148925781" Y="-29.102330078125" />
                  <Point X="-1.234696533203" Y="-29.088146484375" />
                  <Point X="-1.249738891602" Y="-29.07142578125" />
                  <Point X="-1.265715209961" Y="-29.05565234375" />
                  <Point X="-1.494267700195" Y="-28.855216796875" />
                  <Point X="-1.50355456543" Y="-28.848037109375" />
                  <Point X="-1.529853149414" Y="-28.8301875" />
                  <Point X="-1.548847412109" Y="-28.820220703125" />
                  <Point X="-1.581670776367" Y="-28.80748828125" />
                  <Point X="-1.598678588867" Y="-28.80265625" />
                  <Point X="-1.620791870117" Y="-28.798548828125" />
                  <Point X="-1.643063964844" Y="-28.795759765625" />
                  <Point X="-1.946403930664" Y="-28.77587890625" />
                  <Point X="-1.958145507812" Y="-28.7758359375" />
                  <Point X="-1.989874511719" Y="-28.777685546875" />
                  <Point X="-2.011004394531" Y="-28.781341796875" />
                  <Point X="-2.044781005859" Y="-28.791216796875" />
                  <Point X="-2.061206787109" Y="-28.797732421875" />
                  <Point X="-2.08125390625" Y="-28.80793359375" />
                  <Point X="-2.100644287109" Y="-28.8192890625" />
                  <Point X="-2.353403564453" Y="-28.988177734375" />
                  <Point X="-2.35968359375" Y="-28.9927578125" />
                  <Point X="-2.380447265625" Y="-29.010134765625" />
                  <Point X="-2.402762695312" Y="-29.032775390625" />
                  <Point X="-2.410471923828" Y="-29.041630859375" />
                  <Point X="-2.503201416016" Y="-29.162478515625" />
                  <Point X="-2.747589599609" Y="-29.01116015625" />
                  <Point X="-2.980862548828" Y="-28.831548828125" />
                  <Point X="-2.356515136719" Y="-27.750146484375" />
                  <Point X="-2.341488525391" Y="-27.724119140625" />
                  <Point X="-2.334845214844" Y="-27.7100703125" />
                  <Point X="-2.323937988281" Y="-27.681078125" />
                  <Point X="-2.319674072266" Y="-27.666134765625" />
                  <Point X="-2.313406738281" Y="-27.63458203125" />
                  <Point X="-2.311632324219" Y="-27.61902734375" />
                  <Point X="-2.310662109375" Y="-27.58783984375" />
                  <Point X="-2.314826904297" Y="-27.55691796875" />
                  <Point X="-2.324013671875" Y="-27.52709765625" />
                  <Point X="-2.32983984375" Y="-27.51256640625" />
                  <Point X="-2.344440917969" Y="-27.483376953125" />
                  <Point X="-2.352341552734" Y="-27.470322265625" />
                  <Point X="-2.370130615234" Y="-27.445646484375" />
                  <Point X="-2.380019042969" Y="-27.434025390625" />
                  <Point X="-2.39698046875" Y="-27.417064453125" />
                  <Point X="-2.408819335938" Y="-27.40701953125" />
                  <Point X="-2.4339765625" Y="-27.3889921875" />
                  <Point X="-2.447294921875" Y="-27.381009765625" />
                  <Point X="-2.476124267578" Y="-27.36679296875" />
                  <Point X="-2.490564208984" Y="-27.3610859375" />
                  <Point X="-2.520181884766" Y="-27.3521015625" />
                  <Point X="-2.550870117188" Y="-27.3480625" />
                  <Point X="-2.581804199219" Y="-27.349076171875" />
                  <Point X="-2.597227783203" Y="-27.3508515625" />
                  <Point X="-2.628754394531" Y="-27.357123046875" />
                  <Point X="-2.643685058594" Y="-27.36138671875" />
                  <Point X="-2.672650146484" Y="-27.3722890625" />
                  <Point X="-2.686684570312" Y="-27.378927734375" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-4.004020751953" Y="-27.740587890625" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.0744140625" Y="-26.59405859375" />
                  <Point X="-3.048122558594" Y="-26.573884765625" />
                  <Point X="-3.036277099609" Y="-26.563091796875" />
                  <Point X="-3.0145703125" Y="-26.539669921875" />
                  <Point X="-3.004708984375" Y="-26.527041015625" />
                  <Point X="-2.986582763672" Y="-26.499287109375" />
                  <Point X="-2.979223388672" Y="-26.485728515625" />
                  <Point X="-2.966797119141" Y="-26.457599609375" />
                  <Point X="-2.961730224609" Y="-26.443029296875" />
                  <Point X="-2.954186767578" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.39880078125" />
                  <Point X="-2.948748779297" Y="-26.368365234375" />
                  <Point X="-2.948578857422" Y="-26.353033203125" />
                  <Point X="-2.950790771484" Y="-26.321353515625" />
                  <Point X="-2.953123046875" Y="-26.306052734375" />
                  <Point X="-2.960240722656" Y="-26.27603515625" />
                  <Point X="-2.972137207031" Y="-26.247572265625" />
                  <Point X="-2.988498779297" Y="-26.221416015625" />
                  <Point X="-2.997749267578" Y="-26.209005859375" />
                  <Point X="-3.019127197266" Y="-26.184734375" />
                  <Point X="-3.029921630859" Y="-26.17427734375" />
                  <Point X="-3.053026123047" Y="-26.1551953125" />
                  <Point X="-3.065336181641" Y="-26.1465703125" />
                  <Point X="-3.091270996094" Y="-26.131306640625" />
                  <Point X="-3.105440185547" Y="-26.124478515625" />
                  <Point X="-3.134704345703" Y="-26.113255859375" />
                  <Point X="-3.149799316406" Y="-26.108861328125" />
                  <Point X="-3.181684326172" Y="-26.102380859375" />
                  <Point X="-3.197296142578" Y="-26.10053515625" />
                  <Point X="-3.228617431641" Y="-26.099443359375" />
                  <Point X="-3.244326904297" Y="-26.100197265625" />
                  <Point X="-4.660919921875" Y="-26.2866953125" />
                  <Point X="-4.740760742188" Y="-25.974123046875" />
                  <Point X="-4.786451660156" Y="-25.654658203125" />
                  <Point X="-3.538237548828" Y="-25.32019921875" />
                  <Point X="-3.508287597656" Y="-25.312173828125" />
                  <Point X="-3.500173583984" Y="-25.309603515625" />
                  <Point X="-3.473082275391" Y="-25.29883984375" />
                  <Point X="-3.443325195312" Y="-25.283359375" />
                  <Point X="-3.433002197266" Y="-25.277126953125" />
                  <Point X="-3.405809570312" Y="-25.25825390625" />
                  <Point X="-3.393462402344" Y="-25.2480390625" />
                  <Point X="-3.370660644531" Y="-25.2256796875" />
                  <Point X="-3.360206054688" Y="-25.21353515625" />
                  <Point X="-3.340775146484" Y="-25.186677734375" />
                  <Point X="-3.332772460938" Y="-25.1734765625" />
                  <Point X="-3.319010253906" Y="-25.145951171875" />
                  <Point X="-3.313250732422" Y="-25.131626953125" />
                  <Point X="-3.304186523438" Y="-25.102421875" />
                  <Point X="-3.300947021484" Y="-25.088212890625" />
                  <Point X="-3.296679443359" Y="-25.059466796875" />
                  <Point X="-3.295651367188" Y="-25.0449296875" />
                  <Point X="-3.295837646484" Y="-25.0146875" />
                  <Point X="-3.296953613281" Y="-25.000744140625" />
                  <Point X="-3.301220947266" Y="-24.973171875" />
                  <Point X="-3.304372314453" Y="-24.95954296875" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.319473144531" Y="-24.9154609375" />
                  <Point X="-3.333965820312" Y="-24.8869375" />
                  <Point X="-3.342422119141" Y="-24.8732890625" />
                  <Point X="-3.362237304688" Y="-24.84663671875" />
                  <Point X="-3.372406005859" Y="-24.8350546875" />
                  <Point X="-3.394478515625" Y="-24.81369140625" />
                  <Point X="-3.406382324219" Y="-24.80391015625" />
                  <Point X="-3.433564453125" Y="-24.785044921875" />
                  <Point X="-3.440486572266" Y="-24.780669921875" />
                  <Point X="-3.465607177734" Y="-24.767169921875" />
                  <Point X="-3.496560546875" Y="-24.754365234375" />
                  <Point X="-3.508287353516" Y="-24.75038671875" />
                  <Point X="-4.7854453125" Y="-24.408173828125" />
                  <Point X="-4.731331054688" Y="-24.042474609375" />
                  <Point X="-4.633585449219" Y="-23.681763671875" />
                  <Point X="-3.799534423828" Y="-23.791568359375" />
                  <Point X="-3.778065429688" Y="-23.79439453125" />
                  <Point X="-3.767738769531" Y="-23.79518359375" />
                  <Point X="-3.747071044922" Y="-23.795634765625" />
                  <Point X="-3.736729980469" Y="-23.795296875" />
                  <Point X="-3.715178222656" Y="-23.7934140625" />
                  <Point X="-3.704892333984" Y="-23.7919453125" />
                  <Point X="-3.684540771484" Y="-23.787892578125" />
                  <Point X="-3.673331298828" Y="-23.78494921875" />
                  <Point X="-3.613145019531" Y="-23.76597265625" />
                  <Point X="-3.603451171875" Y="-23.76232421875" />
                  <Point X="-3.584523925781" Y="-23.75399609375" />
                  <Point X="-3.575290527344" Y="-23.74931640625" />
                  <Point X="-3.556546142578" Y="-23.73849609375" />
                  <Point X="-3.54787109375" Y="-23.7328359375" />
                  <Point X="-3.531185791016" Y="-23.720603515625" />
                  <Point X="-3.515930908203" Y="-23.706626953125" />
                  <Point X="-3.502289550781" Y="-23.691072265625" />
                  <Point X="-3.495892822266" Y="-23.682921875" />
                  <Point X="-3.483478515625" Y="-23.66519140625" />
                  <Point X="-3.478008789062" Y="-23.65639453125" />
                  <Point X="-3.468061523438" Y="-23.63826171875" />
                  <Point X="-3.463086425781" Y="-23.627724609375" />
                  <Point X="-3.438936523438" Y="-23.569421875" />
                  <Point X="-3.435499023438" Y="-23.559646484375" />
                  <Point X="-3.429708984375" Y="-23.539779296875" />
                  <Point X="-3.427356445313" Y="-23.5296875" />
                  <Point X="-3.423598388672" Y="-23.50836328125" />
                  <Point X="-3.422359130859" Y="-23.498076171875" />
                  <Point X="-3.421008300781" Y="-23.477427734375" />
                  <Point X="-3.421914550781" Y="-23.45675390625" />
                  <Point X="-3.425066650391" Y="-23.4363046875" />
                  <Point X="-3.427200927734" Y="-23.42616796875" />
                  <Point X="-3.432809814453" Y="-23.405255859375" />
                  <Point X="-3.436029785156" Y="-23.395423828125" />
                  <Point X="-3.443521240234" Y="-23.376166015625" />
                  <Point X="-3.448385498047" Y="-23.3656015625" />
                  <Point X="-3.477524902344" Y="-23.309625" />
                  <Point X="-3.482801757812" Y="-23.3007109375" />
                  <Point X="-3.494294433594" Y="-23.28351171875" />
                  <Point X="-3.500510253906" Y="-23.2752265625" />
                  <Point X="-3.514423339844" Y="-23.258646484375" />
                  <Point X="-3.521500732422" Y="-23.25108984375" />
                  <Point X="-3.536440917969" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-4.22761328125" Y="-22.705716796875" />
                  <Point X="-4.002290527344" Y="-22.3196875" />
                  <Point X="-3.726337158203" Y="-21.964986328125" />
                  <Point X="-3.267343994141" Y="-22.229986328125" />
                  <Point X="-3.244910888672" Y="-22.242287109375" />
                  <Point X="-3.225956542969" Y="-22.250625" />
                  <Point X="-3.216249755859" Y="-22.254275390625" />
                  <Point X="-3.195586425781" Y="-22.26078515625" />
                  <Point X="-3.185591796875" Y="-22.26334765625" />
                  <Point X="-3.165384521484" Y="-22.26737109375" />
                  <Point X="-3.153348876953" Y="-22.268994140625" />
                  <Point X="-3.069526367188" Y="-22.276328125" />
                  <Point X="-3.059181640625" Y="-22.27666796875" />
                  <Point X="-3.038515625" Y="-22.27621875" />
                  <Point X="-3.028194335938" Y="-22.2754296875" />
                  <Point X="-3.006747070312" Y="-22.272609375" />
                  <Point X="-2.996572265625" Y="-22.270703125" />
                  <Point X="-2.976487548828" Y="-22.26579296875" />
                  <Point X="-2.957051269531" Y="-22.25872265625" />
                  <Point X="-2.938504638672" Y="-22.24958203125" />
                  <Point X="-2.929484375" Y="-22.2445078125" />
                  <Point X="-2.911235839844" Y="-22.232890625" />
                  <Point X="-2.902825439453" Y="-22.226869140625" />
                  <Point X="-2.88670703125" Y="-22.2139453125" />
                  <Point X="-2.87767578125" Y="-22.20572265625" />
                  <Point X="-2.818177978516" Y="-22.146224609375" />
                  <Point X="-2.811260986328" Y="-22.1385078125" />
                  <Point X="-2.798314453125" Y="-22.122375" />
                  <Point X="-2.792284912109" Y="-22.113958984375" />
                  <Point X="-2.780655761719" Y="-22.095705078125" />
                  <Point X="-2.775575927734" Y="-22.086681640625" />
                  <Point X="-2.766427001953" Y="-22.068130859375" />
                  <Point X="-2.759351318359" Y="-22.04869140625" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909423828" Y="-21.986630859375" />
                  <Point X="-2.748458496094" Y="-21.965947265625" />
                  <Point X="-2.748948974609" Y="-21.9538671875" />
                  <Point X="-2.756282470703" Y="-21.87004296875" />
                  <Point X="-2.75774609375" Y="-21.85979296875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764351806641" Y="-21.8294765625" />
                  <Point X="-2.770860107422" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799138671875" />
                  <Point X="-2.782840332031" Y="-21.780205078125" />
                  <Point X="-2.787522216797" Y="-21.770966796875" />
                  <Point X="-3.05938671875" Y="-21.3000859375" />
                  <Point X="-2.648363769531" Y="-20.984958984375" />
                  <Point X="-2.192524414062" Y="-20.731705078125" />
                  <Point X="-2.122598144531" Y="-20.822833984375" />
                  <Point X="-2.111824707031" Y="-20.835947265625" />
                  <Point X="-2.097537597656" Y="-20.850875" />
                  <Point X="-2.089987548828" Y="-20.85794921875" />
                  <Point X="-2.073436279297" Y="-20.87184375" />
                  <Point X="-2.065165283203" Y="-20.878052734375" />
                  <Point X="-2.047992431641" Y="-20.889537109375" />
                  <Point X="-2.037057861328" Y="-20.89587109375" />
                  <Point X="-1.943763671875" Y="-20.9444375" />
                  <Point X="-1.934326904297" Y="-20.948712890625" />
                  <Point X="-1.915044433594" Y="-20.9562109375" />
                  <Point X="-1.905198730469" Y="-20.95943359375" />
                  <Point X="-1.884282592773" Y="-20.965037109375" />
                  <Point X="-1.874142456055" Y="-20.967169921875" />
                  <Point X="-1.853687988281" Y="-20.97031640625" />
                  <Point X="-1.833013061523" Y="-20.97121484375" />
                  <Point X="-1.812362426758" Y="-20.969857421875" />
                  <Point X="-1.802072631836" Y="-20.968615234375" />
                  <Point X="-1.780749267578" Y="-20.964849609375" />
                  <Point X="-1.770614135742" Y="-20.96248046875" />
                  <Point X="-1.750664306641" Y="-20.9566484375" />
                  <Point X="-1.739081298828" Y="-20.952451171875" />
                  <Point X="-1.641926269531" Y="-20.912208984375" />
                  <Point X="-1.632584960938" Y="-20.907728515625" />
                  <Point X="-1.614450805664" Y="-20.897779296875" />
                  <Point X="-1.605657958984" Y="-20.892310546875" />
                  <Point X="-1.587928222656" Y="-20.879896484375" />
                  <Point X="-1.579780151367" Y="-20.873501953125" />
                  <Point X="-1.564227539062" Y="-20.85986328125" />
                  <Point X="-1.550252441406" Y="-20.84461328125" />
                  <Point X="-1.538020751953" Y="-20.827931640625" />
                  <Point X="-1.532359863281" Y="-20.819255859375" />
                  <Point X="-1.521538085938" Y="-20.80051171875" />
                  <Point X="-1.516856567383" Y="-20.7912734375" />
                  <Point X="-1.508527099609" Y="-20.77234375" />
                  <Point X="-1.504227539062" Y="-20.7605859375" />
                  <Point X="-1.472599853516" Y="-20.660275390625" />
                  <Point X="-1.470027832031" Y="-20.6502421875" />
                  <Point X="-1.465991455078" Y="-20.629953125" />
                  <Point X="-1.464527099609" Y="-20.619697265625" />
                  <Point X="-1.462640625" Y="-20.59813671875" />
                  <Point X="-1.462301513672" Y="-20.587783203125" />
                  <Point X="-1.462752807617" Y="-20.5671015625" />
                  <Point X="-1.46354296875" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-0.931157287598" Y="-20.28367578125" />
                  <Point X="-0.365222473145" Y="-20.21744140625" />
                  <Point X="-0.229566741943" Y="-20.72371484375" />
                  <Point X="-0.220435806274" Y="-20.752890625" />
                  <Point X="-0.207664489746" Y="-20.781078125" />
                  <Point X="-0.200123504639" Y="-20.7946484375" />
                  <Point X="-0.182265274048" Y="-20.821376953125" />
                  <Point X="-0.172612960815" Y="-20.8335390625" />
                  <Point X="-0.151454711914" Y="-20.856130859375" />
                  <Point X="-0.12690057373" Y="-20.87497265625" />
                  <Point X="-0.099602859497" Y="-20.889564453125" />
                  <Point X="-0.085353782654" Y="-20.8957421875" />
                  <Point X="-0.054915370941" Y="-20.90607421875" />
                  <Point X="-0.039853477478" Y="-20.909845703125" />
                  <Point X="-0.009317863464" Y="-20.91488671875" />
                  <Point X="0.021629735947" Y="-20.91488671875" />
                  <Point X="0.052165351868" Y="-20.909845703125" />
                  <Point X="0.067227088928" Y="-20.90607421875" />
                  <Point X="0.097665657043" Y="-20.8957421875" />
                  <Point X="0.111915176392" Y="-20.889564453125" />
                  <Point X="0.139212600708" Y="-20.87497265625" />
                  <Point X="0.163763763428" Y="-20.8561328125" />
                  <Point X="0.184922012329" Y="-20.83354296875" />
                  <Point X="0.194577148438" Y="-20.82137890625" />
                  <Point X="0.212435379028" Y="-20.794650390625" />
                  <Point X="0.219977096558" Y="-20.781078125" />
                  <Point X="0.232748718262" Y="-20.752888671875" />
                  <Point X="0.23797845459" Y="-20.738271484375" />
                  <Point X="0.378190368652" Y="-20.2149921875" />
                  <Point X="0.827867675781" Y="-20.2620859375" />
                  <Point X="1.453625244141" Y="-20.4131640625" />
                  <Point X="1.858269165039" Y="-20.559935546875" />
                  <Point X="2.250439208984" Y="-20.74333984375" />
                  <Point X="2.629437988281" Y="-20.964146484375" />
                  <Point X="2.817779785156" Y="-21.098083984375" />
                  <Point X="2.082700683594" Y="-22.371279296875" />
                  <Point X="2.065308837891" Y="-22.40140234375" />
                  <Point X="2.062063232422" Y="-22.40753125" />
                  <Point X="2.052390380859" Y="-22.4285703125" />
                  <Point X="2.043489379883" Y="-22.452755859375" />
                  <Point X="2.040868286133" Y="-22.461025390625" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017811767578" Y="-22.54915234375" />
                  <Point X="2.013696289062" Y="-22.57785546875" />
                  <Point X="2.012527587891" Y="-22.598158203125" />
                  <Point X="2.012416992188" Y="-22.6065859375" />
                  <Point X="2.013579956055" Y="-22.631822265625" />
                  <Point X="2.021782348633" Y="-22.699845703125" />
                  <Point X="2.023797851563" Y="-22.710955078125" />
                  <Point X="2.029133300781" Y="-22.732859375" />
                  <Point X="2.03245324707" Y="-22.743654296875" />
                  <Point X="2.040706176758" Y="-22.76572265625" />
                  <Point X="2.045285522461" Y="-22.776048828125" />
                  <Point X="2.055635986328" Y="-22.796083984375" />
                  <Point X="2.062328369141" Y="-22.807150390625" />
                  <Point X="2.104418945313" Y="-22.8691796875" />
                  <Point X="2.110242431641" Y="-22.87688671875" />
                  <Point X="2.129224121094" Y="-22.89875390625" />
                  <Point X="2.143801025391" Y="-22.912919921875" />
                  <Point X="2.150002685547" Y="-22.91844140625" />
                  <Point X="2.169532470703" Y="-22.933884765625" />
                  <Point X="2.231563232422" Y="-22.9759765625" />
                  <Point X="2.241294433594" Y="-22.981763671875" />
                  <Point X="2.261368896484" Y="-22.99213671875" />
                  <Point X="2.271712158203" Y="-22.99672265625" />
                  <Point X="2.293836669922" Y="-23.004994140625" />
                  <Point X="2.304641845703" Y="-23.00831640625" />
                  <Point X="2.326576904297" Y="-23.013654296875" />
                  <Point X="2.339072509766" Y="-23.01583203125" />
                  <Point X="2.407014892578" Y="-23.024025390625" />
                  <Point X="2.416773681641" Y="-23.0246953125" />
                  <Point X="2.446099609375" Y="-23.024689453125" />
                  <Point X="2.466623779297" Y="-23.02291796875" />
                  <Point X="2.474873291016" Y="-23.02183984375" />
                  <Point X="2.499370849609" Y="-23.017171875" />
                  <Point X="2.578036621094" Y="-22.996134765625" />
                  <Point X="2.583995605469" Y="-22.994330078125" />
                  <Point X="2.604415283203" Y="-22.9869296875" />
                  <Point X="2.627662841797" Y="-22.976421875" />
                  <Point X="2.636034423828" Y="-22.972125" />
                  <Point X="3.940403564453" Y="-22.219048828125" />
                  <Point X="4.043949462891" Y="-22.362953125" />
                  <Point X="4.136883789062" Y="-22.516529296875" />
                  <Point X="3.195849853516" Y="-23.238609375" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.167538330078" Y="-23.260662109375" />
                  <Point X="3.150435546875" Y="-23.27653125" />
                  <Point X="3.132933837891" Y="-23.29553125" />
                  <Point X="3.127409912109" Y="-23.302099609375" />
                  <Point X="3.070794189453" Y="-23.375958984375" />
                  <Point X="3.065263427734" Y="-23.383998046875" />
                  <Point X="3.050380615234" Y="-23.409162109375" />
                  <Point X="3.041476318359" Y="-23.427666015625" />
                  <Point X="3.038190673828" Y="-23.43533984375" />
                  <Point X="3.029705322266" Y="-23.458880859375" />
                  <Point X="3.008615966797" Y="-23.53429296875" />
                  <Point X="3.006224853516" Y="-23.54533984375" />
                  <Point X="3.002771484375" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003078125" Y="-23.63624609375" />
                  <Point X="3.005056640625" Y="-23.649162109375" />
                  <Point X="3.022368896484" Y="-23.73306640625" />
                  <Point X="3.024826904297" Y="-23.742501953125" />
                  <Point X="3.034107177734" Y="-23.77019921875" />
                  <Point X="3.042275390625" Y="-23.789052734375" />
                  <Point X="3.045865478516" Y="-23.7964453125" />
                  <Point X="3.057889160156" Y="-23.817951171875" />
                  <Point X="3.104976806641" Y="-23.889521484375" />
                  <Point X="3.111739746094" Y="-23.898578125" />
                  <Point X="3.126296142578" Y="-23.915826171875" />
                  <Point X="3.134089599609" Y="-23.924017578125" />
                  <Point X="3.151337158203" Y="-23.940109375" />
                  <Point X="3.160094238281" Y="-23.947349609375" />
                  <Point X="3.178408203125" Y="-23.960732421875" />
                  <Point X="3.189153320312" Y="-23.96754296875" />
                  <Point X="3.257385009766" Y="-24.005953125" />
                  <Point X="3.266245361328" Y="-24.010349609375" />
                  <Point X="3.293613037109" Y="-24.02163671875" />
                  <Point X="3.313489501953" Y="-24.027884765625" />
                  <Point X="3.321424316406" Y="-24.0300078125" />
                  <Point X="3.345571533203" Y="-24.034994140625" />
                  <Point X="3.437831542969" Y="-24.0471875" />
                  <Point X="3.444032226562" Y="-24.04780078125" />
                  <Point X="3.465708007812" Y="-24.04877734375" />
                  <Point X="3.491214111328" Y="-24.04796875" />
                  <Point X="3.500603759766" Y="-24.047203125" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.085765625" />
                  <Point X="4.783870605469" Y="-24.286078125" />
                  <Point X="3.718231445312" Y="-24.571615234375" />
                  <Point X="3.691989501953" Y="-24.5786484375" />
                  <Point X="3.685200439453" Y="-24.580744140625" />
                  <Point X="3.663166503906" Y="-24.5889375" />
                  <Point X="3.639670166016" Y="-24.59984765625" />
                  <Point X="3.632138427734" Y="-24.603763671875" />
                  <Point X="3.541495605469" Y="-24.65615625" />
                  <Point X="3.533248779297" Y="-24.661509765625" />
                  <Point X="3.509686035156" Y="-24.679234375" />
                  <Point X="3.494253173828" Y="-24.69308203125" />
                  <Point X="3.488346435547" Y="-24.698865234375" />
                  <Point X="3.471675537109" Y="-24.71719921875" />
                  <Point X="3.417290039063" Y="-24.7865" />
                  <Point X="3.410853759766" Y="-24.795791015625" />
                  <Point X="3.399131347656" Y="-24.815068359375" />
                  <Point X="3.393845214844" Y="-24.8250546875" />
                  <Point X="3.384072753906" Y="-24.846513671875" />
                  <Point X="3.380010742188" Y="-24.857052734375" />
                  <Point X="3.373166015625" Y="-24.878541015625" />
                  <Point X="3.370002685547" Y="-24.8914765625" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350526367188" Y="-24.995884765625" />
                  <Point X="3.348513671875" Y="-25.025294921875" />
                  <Point X="3.348887207031" Y="-25.046052734375" />
                  <Point X="3.349381103516" Y="-25.05416796875" />
                  <Point X="3.352247558594" Y="-25.078373046875" />
                  <Point X="3.370376220703" Y="-25.173033203125" />
                  <Point X="3.373158935547" Y="-25.18398828125" />
                  <Point X="3.380004394531" Y="-25.205486328125" />
                  <Point X="3.384067138672" Y="-25.216029296875" />
                  <Point X="3.393840820312" Y="-25.237494140625" />
                  <Point X="3.399126953125" Y="-25.247482421875" />
                  <Point X="3.410849365234" Y="-25.26676171875" />
                  <Point X="3.418406005859" Y="-25.27748046875" />
                  <Point X="3.472791503906" Y="-25.34678125" />
                  <Point X="3.479353271484" Y="-25.354294921875" />
                  <Point X="3.500547851562" Y="-25.3753828125" />
                  <Point X="3.516727050781" Y="-25.388880859375" />
                  <Point X="3.523187255859" Y="-25.39381640625" />
                  <Point X="3.543359375" Y="-25.407482421875" />
                  <Point X="3.634001953125" Y="-25.459876953125" />
                  <Point X="3.639493896484" Y="-25.46281640625" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026123047" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.76161328125" Y="-25.931052734375" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.467213867188" Y="-25.913259765625" />
                  <Point X="3.436780761719" Y="-25.90925390625" />
                  <Point X="3.427152099609" Y="-25.908482421875" />
                  <Point X="3.398231933594" Y="-25.90812890625" />
                  <Point X="3.377422607422" Y="-25.909638671875" />
                  <Point X="3.370739990234" Y="-25.910361328125" />
                  <Point X="3.350817626953" Y="-25.913474609375" />
                  <Point X="3.172918212891" Y="-25.952140625" />
                  <Point X="3.156493652344" Y="-25.957306640625" />
                  <Point X="3.124842285156" Y="-25.970521484375" />
                  <Point X="3.109615478516" Y="-25.9785703125" />
                  <Point X="3.079573242188" Y="-25.998126953125" />
                  <Point X="3.068251708984" Y="-26.006771484375" />
                  <Point X="3.047031982422" Y="-26.02565234375" />
                  <Point X="3.037133789062" Y="-26.035888671875" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.920571777344" Y="-26.178119140625" />
                  <Point X="2.904761230469" Y="-26.205251953125" />
                  <Point X="2.897983886719" Y="-26.219478515625" />
                  <Point X="2.886322998047" Y="-26.250314453125" />
                  <Point X="2.882611083984" Y="-26.2625703125" />
                  <Point X="2.876869384766" Y="-26.28746875" />
                  <Point X="2.874839599609" Y="-26.300111328125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.85908203125" Y="-26.479484375" />
                  <Point X="2.860162597656" Y="-26.511671875" />
                  <Point X="2.863184082031" Y="-26.532458984375" />
                  <Point X="2.871820800781" Y="-26.56557421875" />
                  <Point X="2.877642578125" Y="-26.581740234375" />
                  <Point X="2.886182373047" Y="-26.60005859375" />
                  <Point X="2.898569091797" Y="-26.62252734375" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001742431641" Y="-26.782353515625" />
                  <Point X="3.019794921875" Y="-26.804451171875" />
                  <Point X="3.043489990234" Y="-26.82812109375" />
                  <Point X="3.052796875" Y="-26.836279296875" />
                  <Point X="4.087170410156" Y="-27.629984375" />
                  <Point X="4.045500732422" Y="-27.697412109375" />
                  <Point X="4.001272949219" Y="-27.76025390625" />
                  <Point X="2.875558837891" Y="-27.110322265625" />
                  <Point X="2.848454101562" Y="-27.094673828125" />
                  <Point X="2.839666015625" Y="-27.09019140625" />
                  <Point X="2.812457519531" Y="-27.078611328125" />
                  <Point X="2.791975585938" Y="-27.071919921875" />
                  <Point X="2.766739257812" Y="-27.065548828125" />
                  <Point X="2.555017578125" Y="-27.0273125" />
                  <Point X="2.543206787109" Y="-27.025935546875" />
                  <Point X="2.511064208984" Y="-27.024216796875" />
                  <Point X="2.489989746094" Y="-27.025439453125" />
                  <Point X="2.456043457031" Y="-27.031240234375" />
                  <Point X="2.4393359375" Y="-27.03569140625" />
                  <Point X="2.420035400391" Y="-27.04276953125" />
                  <Point X="2.396964599609" Y="-27.053015625" />
                  <Point X="2.2210703125" Y="-27.145587890625" />
                  <Point X="2.207595947266" Y="-27.154201171875" />
                  <Point X="2.182282470703" Y="-27.173564453125" />
                  <Point X="2.170443359375" Y="-27.184314453125" />
                  <Point X="2.147576660156" Y="-27.208896484375" />
                  <Point X="2.139642578125" Y="-27.218646484375" />
                  <Point X="2.125132568359" Y="-27.239107421875" />
                  <Point X="2.118556640625" Y="-27.249818359375" />
                  <Point X="2.025984741211" Y="-27.425712890625" />
                  <Point X="2.02111328125" Y="-27.436568359375" />
                  <Point X="2.009793945312" Y="-27.466720703125" />
                  <Point X="2.004602905273" Y="-27.487287109375" />
                  <Point X="1.999928710938" Y="-27.521599609375" />
                  <Point X="1.999168334961" Y="-27.538970703125" />
                  <Point X="2.000168579102" Y="-27.559833984375" />
                  <Point X="2.002975463867" Y="-27.584501953125" />
                  <Point X="2.041213378906" Y="-27.796232421875" />
                  <Point X="2.043015380859" Y="-27.804224609375" />
                  <Point X="2.051238037109" Y="-27.831548828125" />
                  <Point X="2.064070800781" Y="-27.862482421875" />
                  <Point X="2.069547119141" Y="-27.873580078125" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.723752929688" Y="-29.036083984375" />
                  <Point X="1.854598144531" Y="-27.90337890625" />
                  <Point X="1.833914672852" Y="-27.876423828125" />
                  <Point X="1.82737512207" Y="-27.86877734375" />
                  <Point X="1.813523193359" Y="-27.854216796875" />
                  <Point X="1.806210693359" Y="-27.847302734375" />
                  <Point X="1.789280395508" Y="-27.832916015625" />
                  <Point X="1.76899621582" Y="-27.817880859375" />
                  <Point X="1.560174560547" Y="-27.68362890625" />
                  <Point X="1.54978125" Y="-27.67783203125" />
                  <Point X="1.520731079102" Y="-27.66394140625" />
                  <Point X="1.500618041992" Y="-27.65696875" />
                  <Point X="1.466684082031" Y="-27.649328125" />
                  <Point X="1.449363647461" Y="-27.64707421875" />
                  <Point X="1.428194946289" Y="-27.646283203125" />
                  <Point X="1.403689819336" Y="-27.646951171875" />
                  <Point X="1.175307983398" Y="-27.667966796875" />
                  <Point X="1.159341308594" Y="-27.670826171875" />
                  <Point X="1.128119506836" Y="-27.67922265625" />
                  <Point X="1.112864501953" Y="-27.684759765625" />
                  <Point X="1.081841308594" Y="-27.69912890625" />
                  <Point X="1.071003540039" Y="-27.705033203125" />
                  <Point X="1.050192993164" Y="-27.718189453125" />
                  <Point X="1.040220214844" Y="-27.72544140625" />
                  <Point X="0.863874389648" Y="-27.872068359375" />
                  <Point X="0.855221557617" Y="-27.88023046875" />
                  <Point X="0.833225524902" Y="-27.90375390625" />
                  <Point X="0.820458374023" Y="-27.920939453125" />
                  <Point X="0.802877624512" Y="-27.951220703125" />
                  <Point X="0.795494934082" Y="-27.967177734375" />
                  <Point X="0.788290588379" Y="-27.9875" />
                  <Point X="0.781705688477" Y="-28.01062890625" />
                  <Point X="0.728977722168" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.72472454834" Y="-28.289677734375" />
                  <Point X="0.724742248535" Y="-28.323169921875" />
                  <Point X="0.725555053711" Y="-28.33551953125" />
                  <Point X="0.833091491699" Y="-29.15233984375" />
                  <Point X="0.663150817871" Y="-28.51811328125" />
                  <Point X="0.655065002441" Y="-28.487935546875" />
                  <Point X="0.651881774902" Y="-28.47819140625" />
                  <Point X="0.644483032227" Y="-28.4591015625" />
                  <Point X="0.640267822266" Y="-28.4497578125" />
                  <Point X="0.629742614746" Y="-28.429427734375" />
                  <Point X="0.617102722168" Y="-28.4084453125" />
                  <Point X="0.456679870605" Y="-28.177306640625" />
                  <Point X="0.449291107178" Y="-28.167970703125" />
                  <Point X="0.42775177002" Y="-28.144005859375" />
                  <Point X="0.411681915283" Y="-28.1297578125" />
                  <Point X="0.382912689209" Y="-28.109560546875" />
                  <Point X="0.367585205078" Y="-28.100802734375" />
                  <Point X="0.347727111816" Y="-28.091791015625" />
                  <Point X="0.325529815674" Y="-28.083349609375" />
                  <Point X="0.077296142578" Y="-28.006306640625" />
                  <Point X="0.066815559387" Y="-28.003697265625" />
                  <Point X="0.038075901031" Y="-27.99825390625" />
                  <Point X="0.018518463135" Y="-27.99661328125" />
                  <Point X="-0.01367414856" Y="-27.99725" />
                  <Point X="-0.029743999481" Y="-27.99894140625" />
                  <Point X="-0.049681613922" Y="-28.00277734375" />
                  <Point X="-0.070102546692" Y="-28.00789453125" />
                  <Point X="-0.318341583252" Y="-28.0849375" />
                  <Point X="-0.32946081543" Y="-28.08916796875" />
                  <Point X="-0.358786621094" Y="-28.102484375" />
                  <Point X="-0.377306091309" Y="-28.11358984375" />
                  <Point X="-0.405382720947" Y="-28.13511328125" />
                  <Point X="-0.418408935547" Y="-28.14719140625" />
                  <Point X="-0.433231536865" Y="-28.163771484375" />
                  <Point X="-0.447672790527" Y="-28.182068359375" />
                  <Point X="-0.608095581055" Y="-28.41320703125" />
                  <Point X="-0.612473144531" Y="-28.420134765625" />
                  <Point X="-0.625977172852" Y="-28.445265625" />
                  <Point X="-0.638777587891" Y="-28.47621484375" />
                  <Point X="-0.642752807617" Y="-28.487935546875" />
                  <Point X="-0.985424865723" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.908586862305" Y="-28.887198366964" />
                  <Point X="-2.443466463825" Y="-29.084630262624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.129533592871" Y="-29.642361676534" />
                  <Point X="-0.970201966857" Y="-29.709993939203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.944584819338" Y="-28.768713904915" />
                  <Point X="-2.378894445234" Y="-29.008835222494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119647027474" Y="-29.54335403871" />
                  <Point X="-0.945372546332" Y="-29.617329167063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.896728134775" Y="-28.685823626446" />
                  <Point X="-2.287759304743" Y="-28.944315558651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.13617574595" Y="-29.433133778119" />
                  <Point X="-0.920543125808" Y="-29.524664394923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.848871450211" Y="-28.602933347977" />
                  <Point X="-2.193306562151" Y="-28.88120413335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.158597429984" Y="-29.320412102057" />
                  <Point X="-0.895713705284" Y="-29.431999622783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.801014765647" Y="-28.520043069508" />
                  <Point X="-2.098707483334" Y="-28.81815482409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.181019114017" Y="-29.207690425994" />
                  <Point X="-0.87088428476" Y="-29.339334850643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.817805124915" Y="-27.985236932781" />
                  <Point X="-3.771145518579" Y="-28.005042760604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.753158081084" Y="-28.437152791039" />
                  <Point X="-1.955245588058" Y="-28.775846549867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.244076549434" Y="-29.077719896831" />
                  <Point X="-0.846054864235" Y="-29.246670078503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.93385366514" Y="-27.832773014143" />
                  <Point X="-3.668129410465" Y="-27.945566268309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.70530139652" Y="-28.35426251257" />
                  <Point X="-1.669240812224" Y="-28.794044138671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465319801859" Y="-28.880603472067" />
                  <Point X="-0.821225443711" Y="-29.154005306363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036601368176" Y="-27.685954965928" />
                  <Point X="-3.565113302351" Y="-27.886089776014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.657444711957" Y="-28.271372234101" />
                  <Point X="-0.796396023187" Y="-29.061340534224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.119007932725" Y="-27.547771218733" />
                  <Point X="-3.462097194236" Y="-27.826613283719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.609588027393" Y="-28.188481955632" />
                  <Point X="-0.771566602662" Y="-28.968675762084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.160091617857" Y="-27.427127993184" />
                  <Point X="-3.359081086122" Y="-27.767136791424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.56173134283" Y="-28.105591677163" />
                  <Point X="-0.746737182138" Y="-28.876010989944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.073496465421" Y="-27.360681218745" />
                  <Point X="-3.256064978008" Y="-27.707660299129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.513874658266" Y="-28.022701398694" />
                  <Point X="-0.721907761614" Y="-28.783346217804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.986901312985" Y="-27.294234444307" />
                  <Point X="-3.153048869894" Y="-27.648183806834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466017973703" Y="-27.939811120225" />
                  <Point X="-0.69707834109" Y="-28.690681445664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.900306160549" Y="-27.227787669868" />
                  <Point X="-3.05003276178" Y="-27.588707314539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.418161289139" Y="-27.856920841756" />
                  <Point X="-0.672248920565" Y="-28.598016673524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.813711008113" Y="-27.16134089543" />
                  <Point X="-2.947016653666" Y="-27.529230822243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.370304604575" Y="-27.774030563287" />
                  <Point X="-0.647419500041" Y="-28.505351901384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.827483658866" Y="-29.131411148688" />
                  <Point X="0.830505019862" Y="-29.132693640342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.727115855677" Y="-27.094894120991" />
                  <Point X="-2.844000545552" Y="-27.469754329948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.326997038351" Y="-27.689209298647" />
                  <Point X="-0.61090204058" Y="-28.417648407424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.796281244016" Y="-29.014962273526" />
                  <Point X="0.816113690865" Y="-29.023380647757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640520703241" Y="-27.028447346553" />
                  <Point X="-2.740984437438" Y="-27.410277837653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.31081866683" Y="-27.592872374072" />
                  <Point X="-0.555786348469" Y="-28.33783939485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765078829167" Y="-28.898513398364" />
                  <Point X="0.801722361868" Y="-28.914067655173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.553925550804" Y="-26.962000572114" />
                  <Point X="-2.624887515444" Y="-27.356353821425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.350940400409" Y="-27.472637472731" />
                  <Point X="-0.500457389784" Y="-28.258120908565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733876414318" Y="-28.782064523202" />
                  <Point X="0.787331032872" Y="-28.804754662589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.467330398368" Y="-26.895553797675" />
                  <Point X="-0.444866960299" Y="-28.17851341005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.702673999469" Y="-28.66561564804" />
                  <Point X="0.772939703875" Y="-28.695441670004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.66120482471" Y="-26.285579934123" />
                  <Point X="-4.659131762535" Y="-26.286459896809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.380735245932" Y="-26.829107023237" />
                  <Point X="-0.367929650035" Y="-28.107967124831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.671471584619" Y="-28.549166772879" />
                  <Point X="0.758548374878" Y="-28.58612867742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.690772271985" Y="-26.169825061522" />
                  <Point X="-4.473555135909" Y="-26.262028265435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.294140093496" Y="-26.762660248798" />
                  <Point X="-0.237880116529" Y="-28.05996564081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.628789110049" Y="-28.427844901477" />
                  <Point X="0.744157045882" Y="-28.476815684836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.720339719261" Y="-26.05407018892" />
                  <Point X="-4.287978509282" Y="-26.237596634062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.20754494106" Y="-26.69621347436" />
                  <Point X="-0.097434242585" Y="-28.016377141486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.529764094182" Y="-28.282607040213" />
                  <Point X="0.729765716885" Y="-28.367502692251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.745621926126" Y="-25.940134292954" />
                  <Point X="-4.102401882655" Y="-26.213165002689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.120949788624" Y="-26.629766699921" />
                  <Point X="0.243496011658" Y="-28.057889212643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.409960113403" Y="-28.128549031637" />
                  <Point X="0.727383455804" Y="-28.263287246563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.761336569827" Y="-25.830259586604" />
                  <Point X="-3.916825256029" Y="-26.188733371315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.035886069338" Y="-26.562669870678" />
                  <Point X="0.747376587898" Y="-28.168569591781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69121538092" Y="-28.99368020619" />
                  <Point X="2.724360701067" Y="-29.007749559867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.777051213529" Y="-25.720384880253" />
                  <Point X="-3.731248629402" Y="-26.164301739942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.978400886744" Y="-26.483866647141" />
                  <Point X="0.767913697359" Y="-28.074082841691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.573771168485" Y="-28.840623859848" />
                  <Point X="2.645432710326" Y="-28.87104237965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726563842774" Y="-25.638611261822" />
                  <Point X="-3.545672002775" Y="-26.139870108569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950954984844" Y="-26.392312505452" />
                  <Point X="0.790747031433" Y="-27.980570781122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.456326956051" Y="-28.687567513507" />
                  <Point X="2.566504719586" Y="-28.734335199434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.57751627565" Y="-25.59867396463" />
                  <Point X="-3.360095376149" Y="-26.115438477195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.957833155201" Y="-26.2861886595" />
                  <Point X="0.838819090287" Y="-27.897771923615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338882743616" Y="-28.534511167165" />
                  <Point X="2.487576728846" Y="-28.597628019217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.428468708527" Y="-25.558736667437" />
                  <Point X="0.917113289075" Y="-27.827801603403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.221438531181" Y="-28.381454820823" />
                  <Point X="2.408648738106" Y="-28.460920839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.279421141403" Y="-25.518799370245" />
                  <Point X="0.999285621068" Y="-27.759477453069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103994318746" Y="-28.228398474481" />
                  <Point X="2.329720747366" Y="-28.324213658784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.13037357428" Y="-25.478862073053" />
                  <Point X="1.090643075068" Y="-27.695052155712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.986550106311" Y="-28.07534212814" />
                  <Point X="2.250792756626" Y="-28.187506478567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.981326007156" Y="-25.438924775861" />
                  <Point X="1.253103110712" Y="-27.66080811363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.869105893876" Y="-27.922285781798" />
                  <Point X="2.171864765886" Y="-28.05079929835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.832278440033" Y="-25.398987478668" />
                  <Point X="1.472024940713" Y="-27.65053068133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.579948014785" Y="-27.696341308362" />
                  <Point X="2.092936775145" Y="-27.914092118133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683230872909" Y="-25.359050181476" />
                  <Point X="2.039785600637" Y="-27.788326547249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.534183364608" Y="-25.319112859315" />
                  <Point X="2.01959978902" Y="-27.67655394272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.41666865732" Y="-25.26579065724" />
                  <Point X="2.00079838819" Y="-27.565368985704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.345271565123" Y="-25.192892688974" />
                  <Point X="2.010032773798" Y="-27.466084513985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.305477116643" Y="-25.106580194326" />
                  <Point X="2.050101733023" Y="-27.379888542234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.780849749331" Y="-24.377117431371" />
                  <Point X="-4.574572450492" Y="-24.464676949884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296435812283" Y="-25.007213764479" />
                  <Point X="2.094499046164" Y="-27.295529847716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766480670561" Y="-24.280012507588" />
                  <Point X="-3.915230242971" Y="-24.641346876387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333226350103" Y="-24.888392871846" />
                  <Point X="2.143972319667" Y="-27.21332577054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.75211159179" Y="-24.182907583805" />
                  <Point X="2.223952397998" Y="-27.144071063736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.73774251302" Y="-24.085802660021" />
                  <Point X="2.332500487484" Y="-27.086942758217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.717440954179" Y="-23.991215924625" />
                  <Point X="2.448894486413" Y="-27.033144843667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.555397685684" Y="-27.502827585813" />
                  <Point X="4.038211092566" Y="-27.707769717963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692359593502" Y="-23.898658094735" />
                  <Point X="2.770756983235" Y="-27.066563131997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880311812314" Y="-27.113066397935" />
                  <Point X="4.073646024942" Y="-27.619606718517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.667278232826" Y="-23.806100264845" />
                  <Point X="3.772631299705" Y="-27.388629312492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.642196872149" Y="-23.713542434955" />
                  <Point X="3.471616574467" Y="-27.157651906467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.402147894213" Y="-23.712232944892" />
                  <Point X="3.17060184923" Y="-26.926674500442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.049701583817" Y="-23.758633291868" />
                  <Point X="2.974144218196" Y="-26.740078947762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723016136587" Y="-23.794098801186" />
                  <Point X="2.885879295719" Y="-26.599408475163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.579890522553" Y="-23.751647784044" />
                  <Point X="2.859264065586" Y="-26.48490674439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.496585489464" Y="-23.683804436801" />
                  <Point X="2.867028392339" Y="-26.384998269708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.451494791518" Y="-23.599740066671" />
                  <Point X="2.877183918431" Y="-26.286104798926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423600662918" Y="-23.508376185928" />
                  <Point X="2.909734385701" Y="-26.196717416684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.434328640766" Y="-23.400618193649" />
                  <Point X="2.968485491488" Y="-26.118451545662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.50820696914" Y="-23.266054467937" />
                  <Point X="3.031911151433" Y="-26.042169905157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.784967537811" Y="-23.045372340563" />
                  <Point X="3.116562615669" Y="-25.974898084026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.085981244681" Y="-22.814395366809" />
                  <Point X="3.260988123168" Y="-25.932998838925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.202050844322" Y="-22.661922508981" />
                  <Point X="3.453307357571" Y="-25.911429274748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153772746334" Y="-22.579211109898" />
                  <Point X="3.805752256159" Y="-25.957829022446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.105494648346" Y="-22.496499710815" />
                  <Point X="4.158198278161" Y="-26.004229247006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.057216550358" Y="-22.413788311732" />
                  <Point X="4.510644300163" Y="-26.050629471566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.008938452369" Y="-22.331076912649" />
                  <Point X="4.736044036498" Y="-26.043101747367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.950243033826" Y="-22.252787403794" />
                  <Point X="4.757515154474" Y="-25.94901146037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.889884072686" Y="-22.175204026877" />
                  <Point X="3.418314098561" Y="-25.27735010244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.269504994833" Y="-25.638659201695" />
                  <Point X="4.773446068383" Y="-25.85256949627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.829525111546" Y="-22.09762064996" />
                  <Point X="3.366366347971" Y="-25.152095354702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.769166150406" Y="-22.020037273043" />
                  <Point X="-3.226535630281" Y="-22.250370263343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.22577210054" Y="-22.250694362489" />
                  <Point X="3.34880413651" Y="-25.041436402367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960644949947" Y="-22.260029925156" />
                  <Point X="3.360128029985" Y="-24.943038874115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868073221236" Y="-22.196120056833" />
                  <Point X="3.382918438387" Y="-24.849508592679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.798405236421" Y="-22.122488126029" />
                  <Point X="3.432363550275" Y="-24.767292561607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.756507183656" Y="-22.037068558423" />
                  <Point X="3.496313397918" Y="-24.691233425578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75047494493" Y="-21.936424855994" />
                  <Point X="3.590322565605" Y="-24.627933713901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.765133066446" Y="-21.826998616704" />
                  <Point X="3.707533302937" Y="-24.574482484234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.830866855213" Y="-21.695892042945" />
                  <Point X="3.856580055939" Y="-24.534544841468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.9097952661" Y="-21.559184684386" />
                  <Point X="4.005627807868" Y="-24.494607622721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988723676988" Y="-21.422477325827" />
                  <Point X="4.154675559797" Y="-24.454670403974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.050313638215" Y="-21.293129702502" />
                  <Point X="3.125137377528" Y="-23.91445313742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.437842490479" Y="-24.047188582768" />
                  <Point X="4.303723311726" Y="-24.414733185226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963672156437" Y="-21.226702593702" />
                  <Point X="3.035386404053" Y="-23.773151873596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.638302676938" Y="-24.029074647719" />
                  <Point X="4.452771063655" Y="-24.374795966479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.877030674659" Y="-21.160275484902" />
                  <Point X="3.006845699008" Y="-23.657832827215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.823879140662" Y="-24.004642947198" />
                  <Point X="4.601818815584" Y="-24.334858747732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.790389192881" Y="-21.093848376103" />
                  <Point X="3.004913353625" Y="-23.55380835941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009455604385" Y="-23.980211246676" />
                  <Point X="4.750866567512" Y="-24.294921528985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703747711103" Y="-21.027421267303" />
                  <Point X="3.0291436585" Y="-23.460889277765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.195032068109" Y="-23.955779546155" />
                  <Point X="4.770475060146" Y="-24.200040604438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.610373063154" Y="-20.963852217976" />
                  <Point X="3.07113792824" Y="-23.375510551841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.380608531833" Y="-23.931347845633" />
                  <Point X="4.753269682552" Y="-24.089533119093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.505068196789" Y="-20.905347245919" />
                  <Point X="2.097536565431" Y="-22.859037056448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479154186945" Y="-23.021024126202" />
                  <Point X="3.131070942645" Y="-23.297746371262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.566184995557" Y="-23.906916145112" />
                  <Point X="4.725618707595" Y="-23.974591740727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.399763330423" Y="-20.846842273862" />
                  <Point X="2.027486439475" Y="-22.726098306254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.622601409882" Y="-22.978709623941" />
                  <Point X="3.20975336747" Y="-23.227940843225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.294458464058" Y="-20.788337301805" />
                  <Point X="-2.019094074003" Y="-20.905222550664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.872594087865" Y="-20.967408105355" />
                  <Point X="2.012882857404" Y="-22.616695217585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.726786547697" Y="-22.919729355313" />
                  <Point X="3.296348551308" Y="-23.161494082116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.701433497073" Y="-20.93685722982" />
                  <Point X="2.025415660186" Y="-22.518810840889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.829802904369" Y="-22.860252968524" />
                  <Point X="3.382943735146" Y="-23.095047321007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.589650204058" Y="-20.881102186724" />
                  <Point X="2.052972645059" Y="-22.427303851125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.932819261041" Y="-22.800776581736" />
                  <Point X="3.469538918984" Y="-23.028600559897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.524458541577" Y="-20.805570169821" />
                  <Point X="2.098726858557" Y="-22.343521126636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.035835617713" Y="-22.741300194947" />
                  <Point X="3.556134102822" Y="-22.962153798788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.490425719937" Y="-20.716812009678" />
                  <Point X="2.146583546254" Y="-22.260630849497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.138851974385" Y="-22.681823808159" />
                  <Point X="3.64272928666" Y="-22.895707037679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465187267366" Y="-20.624320861341" />
                  <Point X="2.194440233951" Y="-22.177740572358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.241868331056" Y="-22.62234742137" />
                  <Point X="3.729324470498" Y="-22.82926027657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468417830746" Y="-20.519745332691" />
                  <Point X="2.242296921648" Y="-22.094850295219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.344884687728" Y="-22.562871034581" />
                  <Point X="3.815919654336" Y="-22.76281351546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.443216076848" Y="-20.427238606691" />
                  <Point X="2.290153609345" Y="-22.01196001808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.4479010444" Y="-22.503394647793" />
                  <Point X="3.902514838174" Y="-22.696366754351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.296793603208" Y="-20.386187023425" />
                  <Point X="2.338010297042" Y="-21.929069740941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.550917401072" Y="-22.443918261004" />
                  <Point X="3.989110022012" Y="-22.629919993242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.150371129567" Y="-20.345135440159" />
                  <Point X="-0.225135205062" Y="-20.737874789164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.120903764269" Y="-20.884759617072" />
                  <Point X="2.385866984739" Y="-21.846179463803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.653933757744" Y="-22.384441874215" />
                  <Point X="4.07570520585" Y="-22.563473232133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.003948655926" Y="-20.304083856893" />
                  <Point X="-0.257056812291" Y="-20.621120634949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.19902807261" Y="-20.814717182643" />
                  <Point X="2.433723672436" Y="-21.763289186664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.756950114416" Y="-22.324965487427" />
                  <Point X="4.112217398034" Y="-22.475767502346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.835317797505" Y="-20.272459173675" />
                  <Point X="-0.288259234904" Y="-20.504671756491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.240438455874" Y="-20.729090611615" />
                  <Point X="2.481580360132" Y="-21.680398909525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.859966471088" Y="-22.265489100638" />
                  <Point X="4.023885122925" Y="-22.33506844025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.64473183316" Y="-20.250153880009" />
                  <Point X="-0.319461657517" Y="-20.388222878034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.265267847836" Y="-20.636425827351" />
                  <Point X="2.529437047829" Y="-21.597508632386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.454145868815" Y="-20.227848586343" />
                  <Point X="-0.35066408013" Y="-20.271773999576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.290097239799" Y="-20.543761043088" />
                  <Point X="2.577293735526" Y="-21.514618355247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.314926631761" Y="-20.451096258824" />
                  <Point X="2.625150423223" Y="-21.431728078108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.339756023723" Y="-20.35843147456" />
                  <Point X="2.67300711092" Y="-21.348837800969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.364585415685" Y="-20.265766690296" />
                  <Point X="2.720863798617" Y="-21.26594752383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.524101924072" Y="-20.230273195023" />
                  <Point X="2.768720486314" Y="-21.183057246691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.86106252439" Y="-20.270100248059" />
                  <Point X="2.816577174011" Y="-21.100166969552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.424889281438" Y="-20.406226271278" />
                  <Point X="2.334620282809" Y="-20.792384170885" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000163818359" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.479624908447" Y="-28.5672890625" />
                  <Point X="0.47153918457" Y="-28.537111328125" />
                  <Point X="0.461013946533" Y="-28.51678125" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.289068786621" Y="-28.273822265625" />
                  <Point X="0.269210662842" Y="-28.264810546875" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="0.006153041363" Y="-28.18551953125" />
                  <Point X="-0.01378453064" Y="-28.18935546875" />
                  <Point X="-0.262023590088" Y="-28.2663984375" />
                  <Point X="-0.276761322021" Y="-28.27382421875" />
                  <Point X="-0.291583892822" Y="-28.290404296875" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459226989746" Y="-28.537111328125" />
                  <Point X="-0.847744018555" Y="-29.987078125" />
                  <Point X="-1.100232421875" Y="-29.938068359375" />
                  <Point X="-1.104755615234" Y="-29.936904296875" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.310123779297" Y="-29.55843359375" />
                  <Point X="-1.309150390625" Y="-29.551041015625" />
                  <Point X="-1.310905029297" Y="-29.5286171875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.375948730469" Y="-29.215220703125" />
                  <Point X="-1.390991088867" Y="-29.1985" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.633376708984" Y="-28.9894609375" />
                  <Point X="-1.655489990234" Y="-28.985353515625" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.975038452148" Y="-28.967068359375" />
                  <Point X="-1.995085693359" Y="-28.97726953125" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734863281" Y="-29.157294921875" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.855837402344" Y="-29.167609375" />
                  <Point X="-2.862080322266" Y="-29.162802734375" />
                  <Point X="-3.228580810547" Y="-28.880609375" />
                  <Point X="-2.521060058594" Y="-27.655146484375" />
                  <Point X="-2.506033447266" Y="-27.629119140625" />
                  <Point X="-2.499766113281" Y="-27.59756640625" />
                  <Point X="-2.5143671875" Y="-27.568376953125" />
                  <Point X="-2.531328613281" Y="-27.551416015625" />
                  <Point X="-2.560157958984" Y="-27.53719921875" />
                  <Point X="-2.591684570312" Y="-27.543470703125" />
                  <Point X="-3.842959716797" Y="-28.26589453125" />
                  <Point X="-4.161704589844" Y="-27.84712890625" />
                  <Point X="-4.166180664062" Y="-27.839623046875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.190078613281" Y="-26.4433203125" />
                  <Point X="-3.163787109375" Y="-26.423146484375" />
                  <Point X="-3.145660888672" Y="-26.395392578125" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140329345703" Y="-26.334587890625" />
                  <Point X="-3.161707275391" Y="-26.31031640625" />
                  <Point X="-3.187642089844" Y="-26.295052734375" />
                  <Point X="-3.219527099609" Y="-26.288572265625" />
                  <Point X="-4.803283203125" Y="-26.497078125" />
                  <Point X="-4.927393066406" Y="-26.01119140625" />
                  <Point X="-4.928577148438" Y="-26.00291015625" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-3.587413330078" Y="-25.136671875" />
                  <Point X="-3.557463378906" Y="-25.128646484375" />
                  <Point X="-3.541335693359" Y="-25.121037109375" />
                  <Point X="-3.514143066406" Y="-25.1021640625" />
                  <Point X="-3.494712158203" Y="-25.075306640625" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.485834228516" Y="-25.015859375" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.514713867188" Y="-24.96" />
                  <Point X="-3.541895996094" Y="-24.941134765625" />
                  <Point X="-3.557463134766" Y="-24.9339140625" />
                  <Point X="-4.998186035156" Y="-24.547873046875" />
                  <Point X="-4.917645507812" Y="-24.0035859375" />
                  <Point X="-4.915259277344" Y="-23.994779296875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.774734619141" Y="-23.603193359375" />
                  <Point X="-3.753265625" Y="-23.60601953125" />
                  <Point X="-3.731713867188" Y="-23.60413671875" />
                  <Point X="-3.730465087891" Y="-23.6037421875" />
                  <Point X="-3.670278808594" Y="-23.584765625" />
                  <Point X="-3.651534423828" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.638622558594" Y="-23.555013671875" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714599609" Y="-23.47538671875" />
                  <Point X="-3.616323486328" Y="-23.454474609375" />
                  <Point X="-3.616916259766" Y="-23.4533359375" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.65996875" Y="-23.380779296875" />
                  <Point X="-4.476105957031" Y="-22.754533203125" />
                  <Point X="-4.160016601562" Y="-22.212998046875" />
                  <Point X="-4.153696777344" Y="-22.204875" />
                  <Point X="-3.774670898438" Y="-21.717689453125" />
                  <Point X="-3.172343994141" Y="-22.065443359375" />
                  <Point X="-3.159158447266" Y="-22.073056640625" />
                  <Point X="-3.138495117188" Y="-22.07956640625" />
                  <Point X="-3.136788330078" Y="-22.079716796875" />
                  <Point X="-3.052965820312" Y="-22.08705078125" />
                  <Point X="-3.031518554688" Y="-22.08423046875" />
                  <Point X="-3.013270019531" Y="-22.07261328125" />
                  <Point X="-3.012026367188" Y="-22.07137109375" />
                  <Point X="-2.952528564453" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.938225341797" Y="-21.97043359375" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067138672" Y="-21.865966796875" />
                  <Point X="-3.307278808594" Y="-21.25072265625" />
                  <Point X="-2.752874023438" Y="-20.82566796875" />
                  <Point X="-2.742926513672" Y="-20.820140625" />
                  <Point X="-2.141549316406" Y="-20.486029296875" />
                  <Point X="-1.971861328125" Y="-20.707169921875" />
                  <Point X="-1.967824829102" Y="-20.7124296875" />
                  <Point X="-1.95127355957" Y="-20.72632421875" />
                  <Point X="-1.949324951172" Y="-20.72733984375" />
                  <Point X="-1.856030761719" Y="-20.77590625" />
                  <Point X="-1.835114624023" Y="-20.781509765625" />
                  <Point X="-1.813791259766" Y="-20.777744140625" />
                  <Point X="-1.811790039063" Y="-20.776912109375" />
                  <Point X="-1.714635009766" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.685431884766" Y="-20.7034453125" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917480469" Y="-20.58157421875" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-0.968080444336" Y="-20.096701171875" />
                  <Point X="-0.956035339355" Y="-20.095291015625" />
                  <Point X="-0.224199996948" Y="-20.009640625" />
                  <Point X="-0.04604094696" Y="-20.6745390625" />
                  <Point X="-0.042140533447" Y="-20.689095703125" />
                  <Point X="-0.024282501221" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.03659444046" Y="-20.71582421875" />
                  <Point X="0.05445255661" Y="-20.689095703125" />
                  <Point X="0.236648086548" Y="-20.0091328125" />
                  <Point X="0.860210998535" Y="-20.074435546875" />
                  <Point X="0.870184875488" Y="-20.07684375" />
                  <Point X="1.508461669922" Y="-20.2309453125" />
                  <Point X="1.514469604492" Y="-20.233123046875" />
                  <Point X="1.931043334961" Y="-20.38421875" />
                  <Point X="1.937316650391" Y="-20.38715234375" />
                  <Point X="2.338685302734" Y="-20.574859375" />
                  <Point X="2.344777099609" Y="-20.578408203125" />
                  <Point X="2.732521484375" Y="-20.80430859375" />
                  <Point X="2.738248779297" Y="-20.808380859375" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.247245605469" Y="-22.466279296875" />
                  <Point X="2.229853759766" Y="-22.49640234375" />
                  <Point X="2.224418701172" Y="-22.510107421875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202213623047" Y="-22.609076171875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218668945312" Y="-22.69916796875" />
                  <Point X="2.219549560547" Y="-22.700466796875" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.276217041016" Y="-22.776662109375" />
                  <Point X="2.338247802734" Y="-22.81875390625" />
                  <Point X="2.360372314453" Y="-22.827025390625" />
                  <Point X="2.361737792969" Y="-22.827189453125" />
                  <Point X="2.429761230469" Y="-22.835392578125" />
                  <Point X="2.450285400391" Y="-22.83362109375" />
                  <Point X="2.528951171875" Y="-22.812583984375" />
                  <Point X="2.541034423828" Y="-22.80758203125" />
                  <Point X="3.994248535156" Y="-21.9685703125" />
                  <Point X="4.20258984375" Y="-22.2581171875" />
                  <Point X="4.205783691406" Y="-22.263396484375" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.311514404297" Y="-23.38934765625" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.278204833984" Y="-23.417689453125" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.212684814453" Y="-23.510052734375" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.191136230469" Y="-23.610763671875" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.216616699219" Y="-23.713521484375" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280951904297" Y="-23.80118359375" />
                  <Point X="3.282357910156" Y="-23.80197265625" />
                  <Point X="3.350589599609" Y="-23.8403828125" />
                  <Point X="3.370466064453" Y="-23.846630859375" />
                  <Point X="3.462726074219" Y="-23.85882421875" />
                  <Point X="3.475803955078" Y="-23.858828125" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.939187988281" Y="-24.04861328125" />
                  <Point X="4.940194335938" Y="-24.055076171875" />
                  <Point X="4.997858398438" Y="-24.4254453125" />
                  <Point X="3.767406738281" Y="-24.755142578125" />
                  <Point X="3.741164794922" Y="-24.76217578125" />
                  <Point X="3.727219970703" Y="-24.768259765625" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.621144287109" Y="-24.8345" />
                  <Point X="3.566758789062" Y="-24.90380078125" />
                  <Point X="3.556986328125" Y="-24.925259765625" />
                  <Point X="3.556611572266" Y="-24.92721484375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538856445312" Y="-25.042634765625" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.158759765625" />
                  <Point X="3.567879150391" Y="-25.1601875" />
                  <Point X="3.622264648438" Y="-25.22948828125" />
                  <Point X="3.638443847656" Y="-25.242986328125" />
                  <Point X="3.729086425781" Y="-25.295380859375" />
                  <Point X="3.74116796875" Y="-25.300388671875" />
                  <Point X="4.998068359375" Y="-25.637173828125" />
                  <Point X="4.948432128906" Y="-25.966396484375" />
                  <Point X="4.947143066406" Y="-25.972046875" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.4424140625" Y="-26.101634765625" />
                  <Point X="3.411980957031" Y="-26.09762890625" />
                  <Point X="3.391171630859" Y="-26.099138671875" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.183229980469" Y="-26.157361328125" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064040283203" Y="-26.317521484375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.049849121094" Y="-26.501458984375" />
                  <Point X="3.058388916016" Y="-26.51977734375" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168461914062" Y="-26.685541015625" />
                  <Point X="4.339073730469" Y="-27.58378515625" />
                  <Point X="4.204133300781" Y="-27.802140625" />
                  <Point X="4.201465332031" Y="-27.8059296875" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="2.780558837891" Y="-27.274865234375" />
                  <Point X="2.753454101563" Y="-27.259216796875" />
                  <Point X="2.732972167969" Y="-27.252525390625" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.504754394531" Y="-27.21407421875" />
                  <Point X="2.485453857422" Y="-27.22115234375" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.286692871094" Y="-27.338306640625" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.188950439453" Y="-27.529873046875" />
                  <Point X="2.189950683594" Y="-27.550736328125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234092041016" Y="-27.778580078125" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.835294433594" Y="-29.19021484375" />
                  <Point X="2.832319580078" Y="-29.192140625" />
                  <Point X="2.679775146484" Y="-29.290880859375" />
                  <Point X="1.703860961914" Y="-28.01904296875" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.666247192383" Y="-27.977701171875" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.442268798828" Y="-27.83694140625" />
                  <Point X="1.421099975586" Y="-27.836150390625" />
                  <Point X="1.192718017578" Y="-27.857166015625" />
                  <Point X="1.161694824219" Y="-27.87153515625" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.974574951172" Y="-28.030662109375" />
                  <Point X="0.967370605469" Y="-28.050984375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="0.99436517334" Y="-29.9632421875" />
                  <Point X="0.99157824707" Y="-29.96375" />
                  <Point X="0.860200561523" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#118" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.00390064914" Y="4.369681895088" Z="0.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.05" />
                  <Point X="-0.968003380622" Y="4.985648828303" Z="0.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.05" />
                  <Point X="-1.735049949642" Y="4.773202724265" Z="0.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.05" />
                  <Point X="-1.749657495632" Y="4.76229068577" Z="0.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.05" />
                  <Point X="-1.739273588101" Y="4.342870572963" Z="0.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.05" />
                  <Point X="-1.837097220393" Y="4.300553770622" Z="0.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.05" />
                  <Point X="-1.932393262754" Y="4.348290817886" Z="0.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.05" />
                  <Point X="-1.93835169858" Y="4.354551788394" Z="0.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.05" />
                  <Point X="-2.773365456428" Y="4.254846840066" Z="0.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.05" />
                  <Point X="-3.366716927053" Y="3.802710162212" Z="0.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.05" />
                  <Point X="-3.371056583724" Y="3.780360893541" Z="0.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.05" />
                  <Point X="-2.99419109081" Y="3.056490372642" Z="0.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.05" />
                  <Point X="-3.053537848399" Y="2.995265719487" Z="0.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.05" />
                  <Point X="-3.138586092965" Y="3.001373608363" Z="0.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.05" />
                  <Point X="-3.153498454756" Y="3.009137365022" Z="0.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.05" />
                  <Point X="-4.19931624633" Y="2.857109324516" Z="0.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.05" />
                  <Point X="-4.540791668474" Y="2.275656880946" Z="0.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.05" />
                  <Point X="-4.530474828928" Y="2.250717646477" Z="0.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.05" />
                  <Point X="-3.667422733659" Y="1.554857547071" Z="0.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.05" />
                  <Point X="-3.690972397514" Y="1.495401206497" Z="0.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.05" />
                  <Point X="-3.751656180949" Y="1.475223470073" Z="0.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.05" />
                  <Point X="-3.774364875135" Y="1.477658957397" Z="0.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.05" />
                  <Point X="-4.969675146706" Y="1.04957976763" Z="0.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.05" />
                  <Point X="-5.058560937174" Y="0.458578267408" Z="0.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.05" />
                  <Point X="-5.030377171099" Y="0.438617977872" Z="0.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.05" />
                  <Point X="-3.549368052329" Y="0.03019575693" Z="0.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.05" />
                  <Point X="-3.539743475892" Y="0.00060165449" Z="0.05" />
                  <Point X="-3.539556741714" Y="0" Z="0.05" />
                  <Point X="-3.548621125243" Y="-0.029205296571" Z="0.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.05" />
                  <Point X="-3.576000542794" Y="-0.048680226204" Z="0.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.05" />
                  <Point X="-3.606510617673" Y="-0.057094078884" Z="0.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.05" />
                  <Point X="-4.984229401837" Y="-0.978709775635" Z="0.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.05" />
                  <Point X="-4.849643612815" Y="-1.510431759269" Z="0.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.05" />
                  <Point X="-4.814047238116" Y="-1.516834302359" Z="0.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.05" />
                  <Point X="-3.193210857741" Y="-1.322135189324" Z="0.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.05" />
                  <Point X="-3.200225286589" Y="-1.351596718477" Z="0.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.05" />
                  <Point X="-3.226672215948" Y="-1.372371280762" Z="0.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.05" />
                  <Point X="-4.215280109534" Y="-2.833951380975" Z="0.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.05" />
                  <Point X="-3.86924543407" Y="-3.290720878224" Z="0.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.05" />
                  <Point X="-3.836212306232" Y="-3.284899591101" Z="0.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.05" />
                  <Point X="-2.555841151336" Y="-2.572489317796" Z="0.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.05" />
                  <Point X="-2.570517422067" Y="-2.59886607626" Z="0.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.05" />
                  <Point X="-2.89874028676" Y="-4.171138553667" Z="0.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.05" />
                  <Point X="-2.459986207747" Y="-4.444050442405" Z="0.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.05" />
                  <Point X="-2.446578229853" Y="-4.443625547797" Z="0.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.05" />
                  <Point X="-1.973463056298" Y="-3.987563618858" Z="0.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.05" />
                  <Point X="-1.66491591836" Y="-4.003966348519" Z="0.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.05" />
                  <Point X="-1.430114359069" Y="-4.204811361994" Z="0.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.05" />
                  <Point X="-1.366100284126" Y="-4.507090090598" Z="0.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.05" />
                  <Point X="-1.365851868284" Y="-4.520625426187" Z="0.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.05" />
                  <Point X="-1.12337049826" Y="-4.954047888065" Z="0.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.05" />
                  <Point X="-0.823718218124" Y="-5.012588597834" Z="0.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.05" />
                  <Point X="-0.809582330895" Y="-4.983586521031" Z="0.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.05" />
                  <Point X="-0.256663929494" Y="-3.287634335822" Z="0.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.05" />
                  <Point X="-0.005114063067" Y="-3.205826584888" Z="0.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.05" />
                  <Point X="0.248245016294" Y="-3.2812854604" Z="0.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.05" />
                  <Point X="0.413781929938" Y="-3.514011419946" Z="0.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.05" />
                  <Point X="0.425172529506" Y="-3.548949508784" Z="0.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.05" />
                  <Point X="0.994369946537" Y="-4.981662670772" Z="0.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.05" />
                  <Point X="1.173438988355" Y="-4.942547478528" Z="0.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.05" />
                  <Point X="1.172618176336" Y="-4.9080696609" Z="0.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.05" />
                  <Point X="1.010073773645" Y="-3.030323580723" Z="0.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.05" />
                  <Point X="1.187510772313" Y="-2.878696142601" Z="0.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.05" />
                  <Point X="1.419525591962" Y="-2.854659535104" Z="0.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.05" />
                  <Point X="1.63305199199" Y="-2.988479542273" Z="0.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.05" />
                  <Point X="1.658037361379" Y="-3.01820047973" Z="0.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.05" />
                  <Point X="2.853332915767" Y="-4.202834719354" Z="0.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.05" />
                  <Point X="3.042692806531" Y="-4.067843367192" Z="0.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.05" />
                  <Point X="3.030863632857" Y="-4.03801017363" Z="0.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.05" />
                  <Point X="2.232998849965" Y="-2.510569975499" Z="0.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.05" />
                  <Point X="2.324783132933" Y="-2.330313774627" Z="0.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.05" />
                  <Point X="2.502584603746" Y="-2.234118177034" Z="0.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.05" />
                  <Point X="2.717936828013" Y="-2.270449159526" Z="0.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.05" />
                  <Point X="2.749403423477" Y="-2.286885867417" Z="0.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.05" />
                  <Point X="4.236197797082" Y="-2.803427478795" Z="0.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.05" />
                  <Point X="4.395989308518" Y="-2.545556071662" Z="0.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.05" />
                  <Point X="4.374855987248" Y="-2.521660483069" Z="0.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.05" />
                  <Point X="3.094290705691" Y="-1.461457228435" Z="0.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.05" />
                  <Point X="3.107673967496" Y="-1.29082246569" Z="0.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.05" />
                  <Point X="3.2155203405" Y="-1.158048366925" Z="0.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.05" />
                  <Point X="3.395634976776" Y="-1.116717001991" Z="0.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.05" />
                  <Point X="3.429732983189" Y="-1.119927020836" Z="0.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.05" />
                  <Point X="4.989734132655" Y="-0.951891064569" Z="0.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.05" />
                  <Point X="5.04687311413" Y="-0.576735963862" Z="0.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.05" />
                  <Point X="5.02177330617" Y="-0.562129824021" Z="0.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.05" />
                  <Point X="3.657310032407" Y="-0.168417550475" Z="0.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.05" />
                  <Point X="3.601057114339" Y="-0.09803817216" Z="0.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.05" />
                  <Point X="3.581808190258" Y="-0.001950087443" Z="0.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.05" />
                  <Point X="3.599563260166" Y="0.094660443805" Z="0.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.05" />
                  <Point X="3.654322324062" Y="0.165910566415" Z="0.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.05" />
                  <Point X="3.746085381945" Y="0.219731293767" Z="0.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.05" />
                  <Point X="3.774194511136" Y="0.227842108748" Z="0.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.05" />
                  <Point X="4.983443123711" Y="0.983896672156" Z="0.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.05" />
                  <Point X="4.882830763218" Y="1.400261684516" Z="0.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.05" />
                  <Point X="4.852169860137" Y="1.404895836607" Z="0.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.05" />
                  <Point X="3.370861258359" Y="1.234217386644" Z="0.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.05" />
                  <Point X="3.300724279901" Y="1.272879727956" Z="0.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.05" />
                  <Point X="3.252231039542" Y="1.345241980211" Z="0.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.05" />
                  <Point X="3.233948682095" Y="1.430620703531" Z="0.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.05" />
                  <Point X="3.254681524036" Y="1.507760171147" Z="0.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.05" />
                  <Point X="3.31173182357" Y="1.583173524272" Z="0.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.05" />
                  <Point X="3.335796344946" Y="1.602265496211" Z="0.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.05" />
                  <Point X="4.242405230237" Y="2.793771726203" Z="0.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.05" />
                  <Point X="4.007023273151" Y="3.122008330542" Z="0.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.05" />
                  <Point X="3.972137346875" Y="3.11123459334" Z="0.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.05" />
                  <Point X="2.431212237163" Y="2.245962539392" Z="0.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.05" />
                  <Point X="2.361568131808" Y="2.253731551859" Z="0.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.05" />
                  <Point X="2.298135961221" Y="2.295990972545" Z="0.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.05" />
                  <Point X="2.25476762769" Y="2.358888899161" Z="0.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.05" />
                  <Point X="2.2456981509" Y="2.428190312024" Z="0.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.05" />
                  <Point X="2.266565403266" Y="2.508257338719" Z="0.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.05" />
                  <Point X="2.284390759519" Y="2.54000173508" Z="0.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.05" />
                  <Point X="2.761069827592" Y="4.263647221515" Z="0.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.05" />
                  <Point X="2.363791019259" Y="4.496076097035" Z="0.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.05" />
                  <Point X="1.952342054843" Y="4.689419755257" Z="0.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.05" />
                  <Point X="1.52536251974" Y="4.84516068365" Z="0.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.05" />
                  <Point X="0.875763290288" Y="5.003039966007" Z="0.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.05" />
                  <Point X="0.206754693237" Y="5.074908160767" Z="0.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.05" />
                  <Point X="0.189343912413" Y="5.061765610863" Z="0.05" />
                  <Point X="0" Y="4.355124473572" Z="0.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>