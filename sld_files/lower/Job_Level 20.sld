<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#155" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1543" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.71332611084" Y="-29.072421875" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542363037109" Y="-28.467375" />
                  <Point X="0.481044555664" Y="-28.37902734375" />
                  <Point X="0.378635375977" Y="-28.231474609375" />
                  <Point X="0.356752044678" Y="-28.20901953125" />
                  <Point X="0.330496551514" Y="-28.189775390625" />
                  <Point X="0.302495025635" Y="-28.17566796875" />
                  <Point X="0.207608108521" Y="-28.14621875" />
                  <Point X="0.049135883331" Y="-28.09703515625" />
                  <Point X="0.020976638794" Y="-28.092765625" />
                  <Point X="-0.00866468811" Y="-28.092765625" />
                  <Point X="-0.036824085236" Y="-28.09703515625" />
                  <Point X="-0.131710998535" Y="-28.126484375" />
                  <Point X="-0.290183074951" Y="-28.17566796875" />
                  <Point X="-0.318184906006" Y="-28.18977734375" />
                  <Point X="-0.344440093994" Y="-28.209021484375" />
                  <Point X="-0.366323730469" Y="-28.2314765625" />
                  <Point X="-0.427642333984" Y="-28.319826171875" />
                  <Point X="-0.530051391602" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.801547180176" Y="-29.4476171875" />
                  <Point X="-0.916584655762" Y="-29.87694140625" />
                  <Point X="-1.079324707031" Y="-29.845353515625" />
                  <Point X="-1.185859130859" Y="-29.817943359375" />
                  <Point X="-1.24641784668" Y="-29.802361328125" />
                  <Point X="-1.233022338867" Y="-29.70061328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.239177001953" Y="-29.402263671875" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287938354492" Y="-29.182966796875" />
                  <Point X="-1.304010620117" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131205078125" />
                  <Point X="-1.411004516602" Y="-29.054591796875" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.64302734375" Y="-28.890966796875" />
                  <Point X="-1.758973510742" Y="-28.8833671875" />
                  <Point X="-1.952616577148" Y="-28.87067578125" />
                  <Point X="-1.983414916992" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.139270019531" Y="-28.95935546875" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312789306641" Y="-29.07682421875" />
                  <Point X="-2.335102539062" Y="-29.099462890625" />
                  <Point X="-2.475770996094" Y="-29.28278515625" />
                  <Point X="-2.480147949219" Y="-29.288490234375" />
                  <Point X="-2.533176025391" Y="-29.25565625" />
                  <Point X="-2.801724609375" Y="-29.089376953125" />
                  <Point X="-2.949204101562" Y="-28.975822265625" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.702563964844" Y="-28.15951953125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858398438" Y="-27.647650390625" />
                  <Point X="-2.406587890625" Y="-27.61612109375" />
                  <Point X="-2.405576416016" Y="-27.585185546875" />
                  <Point X="-2.414562988281" Y="-27.55556640625" />
                  <Point X="-2.428782714844" Y="-27.526736328125" />
                  <Point X="-2.446810058594" Y="-27.50158203125" />
                  <Point X="-2.464155029297" Y="-27.48423828125" />
                  <Point X="-2.489310791016" Y="-27.466212890625" />
                  <Point X="-2.518139892578" Y="-27.45199609375" />
                  <Point X="-2.5477578125" Y="-27.44301171875" />
                  <Point X="-2.578691894531" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.445682617188" Y="-27.92683203125" />
                  <Point X="-3.818022705078" Y="-28.141802734375" />
                  <Point X="-3.870703857422" Y="-28.07258984375" />
                  <Point X="-4.08286328125" Y="-27.79385546875" />
                  <Point X="-4.188600097656" Y="-27.61655078125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.593768798828" Y="-26.872828125" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.35965625" />
                  <Point X="-3.045556884766" Y="-26.327984375" />
                  <Point X="-3.052558837891" Y="-26.29823828125" />
                  <Point X="-3.068642578125" Y="-26.27225390625" />
                  <Point X="-3.089477050781" Y="-26.248296875" />
                  <Point X="-3.112976074219" Y="-26.228765625" />
                  <Point X="-3.139458740234" Y="-26.2131796875" />
                  <Point X="-3.168722412109" Y="-26.20195703125" />
                  <Point X="-3.200607666016" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.250057617188" Y="-26.328423828125" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.7511015625" Y="-26.317501953125" />
                  <Point X="-4.834077636719" Y="-25.99265625" />
                  <Point X="-4.862053710938" Y="-25.797048828125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.088569824219" Y="-25.36930859375" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.511299560547" Y="-25.21167578125" />
                  <Point X="-3.48533984375" Y="-25.1972421875" />
                  <Point X="-3.477341064453" Y="-25.192259765625" />
                  <Point X="-3.459981933594" Y="-25.180212890625" />
                  <Point X="-3.436019287109" Y="-25.1586796875" />
                  <Point X="-3.414752441406" Y="-25.127470703125" />
                  <Point X="-3.403861572266" Y="-25.1015703125" />
                  <Point X="-3.399059082031" Y="-25.086921875" />
                  <Point X="-3.391025390625" Y="-25.05345703125" />
                  <Point X="-3.388405273438" Y="-25.0321953125" />
                  <Point X="-3.390615722656" Y="-25.01088671875" />
                  <Point X="-3.396327148438" Y="-24.98490234375" />
                  <Point X="-3.400779052734" Y="-24.9703359375" />
                  <Point X="-3.4139921875" Y="-24.936951171875" />
                  <Point X="-3.425205078125" Y="-24.9164375" />
                  <Point X="-3.441088623047" Y="-24.899283203125" />
                  <Point X="-3.463303710938" Y="-24.880552734375" />
                  <Point X="-3.470372558594" Y="-24.875138671875" />
                  <Point X="-3.4877265625" Y="-24.86309375" />
                  <Point X="-3.501916015625" Y="-24.854958984375" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.460948242188" Y="-24.593474609375" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.877964355469" Y="-24.3844140625" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.768172363281" Y="-23.815205078125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.164001464844" Y="-23.647765625" />
                  <Point X="-3.765666503906" Y="-23.70020703125" />
                  <Point X="-3.744985595703" Y="-23.700658203125" />
                  <Point X="-3.723422607422" Y="-23.698771484375" />
                  <Point X="-3.703141601563" Y="-23.69473828125" />
                  <Point X="-3.68013671875" Y="-23.687486328125" />
                  <Point X="-3.641715576172" Y="-23.67537109375" />
                  <Point X="-3.622774902344" Y="-23.667037109375" />
                  <Point X="-3.604031494141" Y="-23.65621484375" />
                  <Point X="-3.587352294922" Y="-23.643984375" />
                  <Point X="-3.573715332031" Y="-23.62843359375" />
                  <Point X="-3.561301269531" Y="-23.610705078125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.542120605469" Y="-23.57028515625" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.515804443359" Y="-23.47125" />
                  <Point X="-3.518951660156" Y="-23.4508046875" />
                  <Point X="-3.524553955078" Y="-23.4298984375" />
                  <Point X="-3.532049804688" Y="-23.410623046875" />
                  <Point X="-3.543187744141" Y="-23.3892265625" />
                  <Point X="-3.561789550781" Y="-23.353494140625" />
                  <Point X="-3.573281005859" Y="-23.336294921875" />
                  <Point X="-3.587192871094" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.13448046875" Y="-22.896927734375" />
                  <Point X="-4.351860351563" Y="-22.730126953125" />
                  <Point X="-4.288942382813" Y="-22.62233203125" />
                  <Point X="-4.081156982422" Y="-22.26634765625" />
                  <Point X="-3.931977294922" Y="-22.074595703125" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.451303222656" Y="-22.01408203125" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187729492188" Y="-22.163658203125" />
                  <Point X="-3.167087402344" Y="-22.17016796875" />
                  <Point X="-3.146793701172" Y="-22.174205078125" />
                  <Point X="-3.114754150391" Y="-22.1770078125" />
                  <Point X="-3.061244384766" Y="-22.181689453125" />
                  <Point X="-3.040560791016" Y="-22.18123828125" />
                  <Point X="-3.019102294922" Y="-22.178412109375" />
                  <Point X="-2.999013427734" Y="-22.173494140625" />
                  <Point X="-2.980464355469" Y="-22.16434765625" />
                  <Point X="-2.962210205078" Y="-22.15271875" />
                  <Point X="-2.946077392578" Y="-22.13976953125" />
                  <Point X="-2.923335449219" Y="-22.11702734375" />
                  <Point X="-2.885353759766" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.846239013672" Y="-21.93183984375" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.105692871094" Y="-21.409880859375" />
                  <Point X="-3.183333496094" Y="-21.275404296875" />
                  <Point X="-3.062523681641" Y="-21.182779296875" />
                  <Point X="-2.700629150391" Y="-20.905318359375" />
                  <Point X="-2.465680175781" Y="-20.774787109375" />
                  <Point X="-2.167036376953" Y="-20.608865234375" />
                  <Point X="-2.118089111328" Y="-20.67265625" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028892333984" Y="-20.78519921875" />
                  <Point X="-2.012312988281" Y="-20.799111328125" />
                  <Point X="-1.995115356445" Y="-20.810603515625" />
                  <Point X="-1.959455444336" Y="-20.82916796875" />
                  <Point X="-1.899899291992" Y="-20.860171875" />
                  <Point X="-1.88062512207" Y="-20.86766796875" />
                  <Point X="-1.859717895508" Y="-20.873271484375" />
                  <Point X="-1.839268310547" Y="-20.876419921875" />
                  <Point X="-1.818622192383" Y="-20.87506640625" />
                  <Point X="-1.797306518555" Y="-20.871306640625" />
                  <Point X="-1.777453491211" Y="-20.86551953125" />
                  <Point X="-1.740311279297" Y="-20.850134765625" />
                  <Point X="-1.678279541016" Y="-20.824439453125" />
                  <Point X="-1.660145141602" Y="-20.814490234375" />
                  <Point X="-1.642415771484" Y="-20.802076171875" />
                  <Point X="-1.626863647461" Y="-20.7884375" />
                  <Point X="-1.614632324219" Y="-20.771755859375" />
                  <Point X="-1.603810546875" Y="-20.75301171875" />
                  <Point X="-1.595480102539" Y="-20.734078125" />
                  <Point X="-1.583391113281" Y="-20.695736328125" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584202026367" Y="-20.368103515625" />
                  <Point X="-1.418134521484" Y="-20.32154296875" />
                  <Point X="-0.94962310791" Y="-20.19019140625" />
                  <Point X="-0.664815917969" Y="-20.156857421875" />
                  <Point X="-0.294711364746" Y="-20.113541015625" />
                  <Point X="-0.206276519775" Y="-20.4435859375" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129692078" Y="-20.741876953125" />
                  <Point X="-0.103271400452" Y="-20.768603515625" />
                  <Point X="-0.082113853455" Y="-20.791193359375" />
                  <Point X="-0.054817928314" Y="-20.805783203125" />
                  <Point X="-0.024379852295" Y="-20.816115234375" />
                  <Point X="0.006156154156" Y="-20.82115625" />
                  <Point X="0.036692108154" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094426071167" Y="-20.791193359375" />
                  <Point X="0.115583656311" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.267086730957" Y="-20.262587890625" />
                  <Point X="0.307419708252" Y="-20.1120625" />
                  <Point X="0.43496585083" Y="-20.125419921875" />
                  <Point X="0.84403125" Y="-20.168259765625" />
                  <Point X="1.079684936523" Y="-20.225154296875" />
                  <Point X="1.481039550781" Y="-20.3220546875" />
                  <Point X="1.633481201172" Y="-20.377345703125" />
                  <Point X="1.894647583008" Y="-20.472072265625" />
                  <Point X="2.042961181641" Y="-20.54143359375" />
                  <Point X="2.294556884766" Y="-20.65909765625" />
                  <Point X="2.437884033203" Y="-20.742599609375" />
                  <Point X="2.680976318359" Y="-20.884224609375" />
                  <Point X="2.816111816406" Y="-20.980326171875" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.470274169922" Y="-21.889982421875" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.483943359375" />
                  <Point X="2.125036132813" Y="-22.51401171875" />
                  <Point X="2.111607177734" Y="-22.56423046875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.11086328125" Y="-22.645048828125" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.1400703125" Y="-22.75252734375" />
                  <Point X="2.156158691406" Y="-22.77623828125" />
                  <Point X="2.183028076172" Y="-22.8158359375" />
                  <Point X="2.194464355469" Y="-22.829669921875" />
                  <Point X="2.221598388672" Y="-22.85440625" />
                  <Point X="2.245308349609" Y="-22.870494140625" />
                  <Point X="2.284906982422" Y="-22.897365234375" />
                  <Point X="2.304947265625" Y="-22.9077265625" />
                  <Point X="2.327036865234" Y="-22.915994140625" />
                  <Point X="2.348965576172" Y="-22.921337890625" />
                  <Point X="2.374966308594" Y="-22.92447265625" />
                  <Point X="2.418390380859" Y="-22.929708984375" />
                  <Point X="2.436468505859" Y="-22.93015625" />
                  <Point X="2.473207763672" Y="-22.925830078125" />
                  <Point X="2.503276367188" Y="-22.9177890625" />
                  <Point X="2.553494140625" Y="-22.904359375" />
                  <Point X="2.5652890625" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.52199609375" Y="-22.350919921875" />
                  <Point X="3.967326171875" Y="-22.09380859375" />
                  <Point X="3.979076171875" Y="-22.110138671875" />
                  <Point X="4.123271484375" Y="-22.310537109375" />
                  <Point X="4.198606933594" Y="-22.43503125" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.655639648438" Y="-23.005544921875" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221423339844" Y="-23.339759765625" />
                  <Point X="3.203974609375" Y="-23.35837109375" />
                  <Point X="3.182334228516" Y="-23.3866015625" />
                  <Point X="3.146192382812" Y="-23.433751953125" />
                  <Point X="3.136605957031" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.113568847656" Y="-23.51173828125" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.104356689453" Y="-23.660302734375" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679931641" Y="-23.7310234375" />
                  <Point X="3.136283203125" Y="-23.76426171875" />
                  <Point X="3.154281494141" Y="-23.7916171875" />
                  <Point X="3.184340820312" Y="-23.837306640625" />
                  <Point X="3.198892333984" Y="-23.854548828125" />
                  <Point X="3.216135742188" Y="-23.870638671875" />
                  <Point X="3.234347167969" Y="-23.88396484375" />
                  <Point X="3.260429199219" Y="-23.898646484375" />
                  <Point X="3.303989257812" Y="-23.92316796875" />
                  <Point X="3.320521728516" Y="-23.9305" />
                  <Point X="3.356120361328" Y="-23.9405625" />
                  <Point X="3.391385009766" Y="-23.94522265625" />
                  <Point X="3.450281005859" Y="-23.953005859375" />
                  <Point X="3.462698486328" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.374930175781" Y="-23.83627734375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.784004882812" Y="-23.812796875" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.869676269531" Y="-24.219671875" />
                  <Point X="4.890864257812" Y="-24.355759765625" />
                  <Point X="4.203473632812" Y="-24.539947265625" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704788085938" Y="-24.674416015625" />
                  <Point X="3.681547851562" Y="-24.684931640625" />
                  <Point X="3.646901367188" Y="-24.70495703125" />
                  <Point X="3.589037841797" Y="-24.73840234375" />
                  <Point X="3.574313232422" Y="-24.74890234375" />
                  <Point X="3.547529296875" Y="-24.774423828125" />
                  <Point X="3.526741455078" Y="-24.8009140625" />
                  <Point X="3.4920234375" Y="-24.84515234375" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.470526855469" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.456751464844" Y="-24.943578125" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.452108398438" Y="-25.094736328125" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526855469" Y="-25.1766640625" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.492024902344" Y="-25.217408203125" />
                  <Point X="3.512812744141" Y="-25.243896484375" />
                  <Point X="3.547530761719" Y="-25.28813671875" />
                  <Point X="3.559994140625" Y="-25.301232421875" />
                  <Point X="3.589036865234" Y="-25.324158203125" />
                  <Point X="3.623683349609" Y="-25.34418359375" />
                  <Point X="3.681546875" Y="-25.377630859375" />
                  <Point X="3.692709228516" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.529748535156" Y="-25.6100390625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.889602539062" Y="-25.719365234375" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.824606933594" Y="-26.08201171875" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.989041748047" Y="-26.077779296875" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035400391" Y="-26.0027109375" />
                  <Point X="3.374659423828" Y="-26.005509765625" />
                  <Point X="3.306660644531" Y="-26.0202890625" />
                  <Point X="3.193095214844" Y="-26.04497265625" />
                  <Point X="3.163973632812" Y="-26.05659765625" />
                  <Point X="3.136146972656" Y="-26.073490234375" />
                  <Point X="3.112397460938" Y="-26.0939609375" />
                  <Point X="3.071296386719" Y="-26.143392578125" />
                  <Point X="3.002653320312" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.963866699219" Y="-26.369380859375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.014082275391" Y="-26.62653125" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.86525390625" Y="-27.339955078125" />
                  <Point X="4.213122558594" Y="-27.6068828125" />
                  <Point X="4.124813964844" Y="-27.74978125" />
                  <Point X="4.061908203125" Y="-27.83916015625" />
                  <Point X="4.028981201172" Y="-27.8859453125" />
                  <Point X="3.303860351562" Y="-27.467296875" />
                  <Point X="2.800954589844" Y="-27.176943359375" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224121094" Y="-27.159826171875" />
                  <Point X="2.673294921875" Y="-27.1452109375" />
                  <Point X="2.538133789062" Y="-27.12080078125" />
                  <Point X="2.506784179688" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444834228516" Y="-27.135177734375" />
                  <Point X="2.377601806641" Y="-27.170560546875" />
                  <Point X="2.265316162109" Y="-27.22965625" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.169147949219" Y="-27.357671875" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.110291259766" Y="-27.6441875" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.636741943359" Y="-28.665990234375" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.781864257812" Y="-29.1116328125" />
                  <Point X="2.7115234375" Y="-29.1571640625" />
                  <Point X="2.701763427734" Y="-29.163482421875" />
                  <Point X="2.142309814453" Y="-28.43438671875" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747507202148" Y="-27.92218359375" />
                  <Point X="1.721923217773" Y="-27.900556640625" />
                  <Point X="1.642104980469" Y="-27.8492421875" />
                  <Point X="1.508799804688" Y="-27.7635390625" />
                  <Point X="1.479986816406" Y="-27.75116796875" />
                  <Point X="1.448366455078" Y="-27.7434375" />
                  <Point X="1.417100097656" Y="-27.741119140625" />
                  <Point X="1.329805419922" Y="-27.74915234375" />
                  <Point X="1.184013061523" Y="-27.76256640625" />
                  <Point X="1.156363647461" Y="-27.7693984375" />
                  <Point X="1.128977416992" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.795462890625" />
                  <Point X="1.037188476563" Y="-27.851509765625" />
                  <Point X="0.92461151123" Y="-27.945115234375" />
                  <Point X="0.90414074707" Y="-27.968865234375" />
                  <Point X="0.887248779297" Y="-27.99669140625" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.855470092773" Y="-28.11853515625" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.957165710449" Y="-29.366955078125" />
                  <Point X="1.022065429688" Y="-29.859916015625" />
                  <Point X="0.975700683594" Y="-29.870078125" />
                  <Point X="0.929315551758" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058418457031" Y="-29.752638671875" />
                  <Point X="-1.14124609375" Y="-29.731328125" />
                  <Point X="-1.138834960938" Y="-29.713013671875" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759155273" Y="-29.509330078125" />
                  <Point X="-1.123333740234" Y="-29.497693359375" />
                  <Point X="-1.146002319336" Y="-29.38373046875" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188125" Y="-29.178470703125" />
                  <Point X="-1.19902734375" Y="-29.14950390625" />
                  <Point X="-1.205666015625" Y="-29.135466796875" />
                  <Point X="-1.22173840332" Y="-29.10762890625" />
                  <Point X="-1.230575195312" Y="-29.094861328125" />
                  <Point X="-1.250209350586" Y="-29.0709375" />
                  <Point X="-1.261006591797" Y="-29.05978125" />
                  <Point X="-1.348366455078" Y="-28.98316796875" />
                  <Point X="-1.494267333984" Y="-28.855216796875" />
                  <Point X="-1.506735839844" Y="-28.84596875" />
                  <Point X="-1.53301953125" Y="-28.829623046875" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576531860352" Y="-28.810224609375" />
                  <Point X="-1.591315673828" Y="-28.805474609375" />
                  <Point X="-1.621456665039" Y="-28.798447265625" />
                  <Point X="-1.636813964844" Y="-28.796169921875" />
                  <Point X="-1.752760131836" Y="-28.7885703125" />
                  <Point X="-1.946403198242" Y="-28.77587890625" />
                  <Point X="-1.961927612305" Y="-28.7761328125" />
                  <Point X="-1.992726074219" Y="-28.779166015625" />
                  <Point X="-2.00799987793" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053667236328" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095437011719" Y="-28.815810546875" />
                  <Point X="-2.192049316406" Y="-28.880365234375" />
                  <Point X="-2.353403320312" Y="-28.988177734375" />
                  <Point X="-2.359686279297" Y="-28.992759765625" />
                  <Point X="-2.380448974609" Y="-29.01013671875" />
                  <Point X="-2.402762207031" Y="-29.032775390625" />
                  <Point X="-2.410470947266" Y="-29.041630859375" />
                  <Point X="-2.503202148438" Y="-29.16248046875" />
                  <Point X="-2.747599853516" Y="-29.011154296875" />
                  <Point X="-2.891246582031" Y="-28.90055078125" />
                  <Point X="-2.980863037109" Y="-28.83155078125" />
                  <Point X="-2.620291503906" Y="-28.20701953125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947021484" Y="-27.68111328125" />
                  <Point X="-2.319683105469" Y="-27.666181640625" />
                  <Point X="-2.313412597656" Y="-27.63465234375" />
                  <Point X="-2.311638671875" Y="-27.619224609375" />
                  <Point X="-2.310627197266" Y="-27.5882890625" />
                  <Point X="-2.314668457031" Y="-27.557603515625" />
                  <Point X="-2.323655029297" Y="-27.527984375" />
                  <Point X="-2.329362792969" Y="-27.51354296875" />
                  <Point X="-2.343582519531" Y="-27.484712890625" />
                  <Point X="-2.351565185547" Y="-27.471396484375" />
                  <Point X="-2.369592529297" Y="-27.4462421875" />
                  <Point X="-2.379637207031" Y="-27.434404296875" />
                  <Point X="-2.396982177734" Y="-27.417060546875" />
                  <Point X="-2.408821533203" Y="-27.407015625" />
                  <Point X="-2.433977294922" Y="-27.388990234375" />
                  <Point X="-2.447293701172" Y="-27.381009765625" />
                  <Point X="-2.476122802734" Y="-27.36679296875" />
                  <Point X="-2.490563232422" Y="-27.3610859375" />
                  <Point X="-2.520181152344" Y="-27.3521015625" />
                  <Point X="-2.550869140625" Y="-27.3480625" />
                  <Point X="-2.581803222656" Y="-27.349076171875" />
                  <Point X="-2.597226806641" Y="-27.3508515625" />
                  <Point X="-2.628753417969" Y="-27.357123046875" />
                  <Point X="-2.643684082031" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.493182617188" Y="-27.844560546875" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-3.795110107422" Y="-28.015052734375" />
                  <Point X="-4.004017822266" Y="-27.74058984375" />
                  <Point X="-4.107007324219" Y="-27.567892578125" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.535936523438" Y="-26.948197265625" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.398802734375" />
                  <Point X="-2.948748535156" Y="-26.368373046875" />
                  <Point X="-2.948577880859" Y="-26.353044921875" />
                  <Point X="-2.950787109375" Y="-26.321373046875" />
                  <Point X="-2.953084228516" Y="-26.306216796875" />
                  <Point X="-2.960086181641" Y="-26.276470703125" />
                  <Point X="-2.971781005859" Y="-26.24823828125" />
                  <Point X="-2.987864746094" Y="-26.22225390625" />
                  <Point X="-2.996958496094" Y="-26.209912109375" />
                  <Point X="-3.01779296875" Y="-26.185955078125" />
                  <Point X="-3.028753662109" Y="-26.17523828125" />
                  <Point X="-3.052252685547" Y="-26.15570703125" />
                  <Point X="-3.064791015625" Y="-26.146892578125" />
                  <Point X="-3.091273681641" Y="-26.131306640625" />
                  <Point X="-3.105441894531" Y="-26.124478515625" />
                  <Point X="-3.134705566406" Y="-26.113255859375" />
                  <Point X="-3.149801025391" Y="-26.108861328125" />
                  <Point X="-3.181686279297" Y="-26.102380859375" />
                  <Point X="-3.197298095703" Y="-26.10053515625" />
                  <Point X="-3.228619384766" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.262457519531" Y="-26.234236328125" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.740762695312" Y="-25.9741171875" />
                  <Point X="-4.768010742188" Y="-25.783599609375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.063981933594" Y="-25.461072265625" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.497229003906" Y="-25.30846875" />
                  <Point X="-3.475652587891" Y="-25.299734375" />
                  <Point X="-3.465135253906" Y="-25.294705078125" />
                  <Point X="-3.439175537109" Y="-25.280271484375" />
                  <Point X="-3.423177978516" Y="-25.270306640625" />
                  <Point X="-3.405818847656" Y="-25.258259765625" />
                  <Point X="-3.396484375" Y="-25.250875" />
                  <Point X="-3.372521728516" Y="-25.229341796875" />
                  <Point X="-3.357513671875" Y="-25.21217578125" />
                  <Point X="-3.336246826172" Y="-25.180966796875" />
                  <Point X="-3.327179443359" Y="-25.164294921875" />
                  <Point X="-3.316288574219" Y="-25.13839453125" />
                  <Point X="-3.313589355469" Y="-25.131166015625" />
                  <Point X="-3.30668359375" Y="-25.10909765625" />
                  <Point X="-3.298649902344" Y="-25.0756328125" />
                  <Point X="-3.296738525391" Y="-25.065076171875" />
                  <Point X="-3.294118408203" Y="-25.043814453125" />
                  <Point X="-3.293912353516" Y="-25.022392578125" />
                  <Point X="-3.296122802734" Y="-25.001083984375" />
                  <Point X="-3.297830566406" Y="-24.9904921875" />
                  <Point X="-3.303541992188" Y="-24.9645078125" />
                  <Point X="-3.305475585938" Y="-24.957134765625" />
                  <Point X="-3.312445800781" Y="-24.935375" />
                  <Point X="-3.325658935547" Y="-24.901990234375" />
                  <Point X="-3.330632568359" Y="-24.89138671875" />
                  <Point X="-3.341845458984" Y="-24.870873046875" />
                  <Point X="-3.355497802734" Y="-24.85189453125" />
                  <Point X="-3.371381347656" Y="-24.834740234375" />
                  <Point X="-3.379851806641" Y="-24.826654296875" />
                  <Point X="-3.402066894531" Y="-24.807923828125" />
                  <Point X="-3.416204589844" Y="-24.797095703125" />
                  <Point X="-3.43355859375" Y="-24.78505078125" />
                  <Point X="-3.440477294922" Y="-24.780677734375" />
                  <Point X="-3.465598388672" Y="-24.76717578125" />
                  <Point X="-3.496558349609" Y="-24.7543671875" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.436360351563" Y="-24.5017109375" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.783987792969" Y="-24.3983203125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.676479492188" Y="-23.840052734375" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.176401367188" Y="-23.741953125" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747057617188" Y="-23.795634765625" />
                  <Point X="-3.736704833984" Y="-23.795296875" />
                  <Point X="-3.715141845703" Y="-23.79341015625" />
                  <Point X="-3.704893066406" Y="-23.791947265625" />
                  <Point X="-3.684612060547" Y="-23.7879140625" />
                  <Point X="-3.674579833984" Y="-23.78534375" />
                  <Point X="-3.651574951172" Y="-23.778091796875" />
                  <Point X="-3.613153808594" Y="-23.7659765625" />
                  <Point X="-3.603455078125" Y="-23.762326171875" />
                  <Point X="-3.584514404297" Y="-23.7539921875" />
                  <Point X="-3.575272460938" Y="-23.74930859375" />
                  <Point X="-3.556529052734" Y="-23.738486328125" />
                  <Point X="-3.547854736328" Y="-23.732826171875" />
                  <Point X="-3.531175537109" Y="-23.720595703125" />
                  <Point X="-3.515926025391" Y="-23.70662109375" />
                  <Point X="-3.5022890625" Y="-23.6910703125" />
                  <Point X="-3.495896728516" Y="-23.682923828125" />
                  <Point X="-3.483482666016" Y="-23.6651953125" />
                  <Point X="-3.478013427734" Y="-23.656400390625" />
                  <Point X="-3.468063720703" Y="-23.638265625" />
                  <Point X="-3.463583251953" Y="-23.62892578125" />
                  <Point X="-3.454352294922" Y="-23.606640625" />
                  <Point X="-3.438935791016" Y="-23.569421875" />
                  <Point X="-3.435498779297" Y="-23.5596484375" />
                  <Point X="-3.429710205078" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.4210078125" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057617188" Y="-23.4363515625" />
                  <Point X="-3.427189208984" Y="-23.42621484375" />
                  <Point X="-3.432791503906" Y="-23.40530859375" />
                  <Point X="-3.436013183594" Y="-23.395466796875" />
                  <Point X="-3.443509033203" Y="-23.37619140625" />
                  <Point X="-3.447783203125" Y="-23.3667578125" />
                  <Point X="-3.458921142578" Y="-23.345361328125" />
                  <Point X="-3.477522949219" Y="-23.30962890625" />
                  <Point X="-3.482798583984" Y="-23.300716796875" />
                  <Point X="-3.494290039062" Y="-23.283517578125" />
                  <Point X="-3.500505859375" Y="-23.27523046875" />
                  <Point X="-3.514417724609" Y="-23.258650390625" />
                  <Point X="-3.521499023438" Y="-23.25108984375" />
                  <Point X="-3.536441894531" Y="-23.23678515625" />
                  <Point X="-3.544303466797" Y="-23.230041015625" />
                  <Point X="-4.076648193359" Y="-22.82155859375" />
                  <Point X="-4.227615234375" Y="-22.70571875" />
                  <Point X="-4.206895996094" Y="-22.670220703125" />
                  <Point X="-4.002296386719" Y="-22.3196953125" />
                  <Point X="-3.856996337891" Y="-22.1329296875" />
                  <Point X="-3.726337158203" Y="-21.964986328125" />
                  <Point X="-3.498803222656" Y="-22.096353515625" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244924804688" Y="-22.242279296875" />
                  <Point X="-3.225997314453" Y="-22.250609375" />
                  <Point X="-3.216302001953" Y="-22.254259765625" />
                  <Point X="-3.195659912109" Y="-22.26076953125" />
                  <Point X="-3.185623046875" Y="-22.263341796875" />
                  <Point X="-3.165329345703" Y="-22.26737890625" />
                  <Point X="-3.155072509766" Y="-22.26884375" />
                  <Point X="-3.123032958984" Y="-22.271646484375" />
                  <Point X="-3.069523193359" Y="-22.276328125" />
                  <Point X="-3.059172607422" Y="-22.276666015625" />
                  <Point X="-3.038489013672" Y="-22.27621484375" />
                  <Point X="-3.028156005859" Y="-22.27542578125" />
                  <Point X="-3.006697509766" Y="-22.272599609375" />
                  <Point X="-2.996512451172" Y="-22.2706875" />
                  <Point X="-2.976423583984" Y="-22.26576953125" />
                  <Point X="-2.956999267578" Y="-22.25869921875" />
                  <Point X="-2.938450195312" Y="-22.249552734375" />
                  <Point X="-2.929421630859" Y="-22.244470703125" />
                  <Point X="-2.911167480469" Y="-22.232841796875" />
                  <Point X="-2.902743896484" Y="-22.2268046875" />
                  <Point X="-2.886611083984" Y="-22.21385546875" />
                  <Point X="-2.878901855469" Y="-22.206943359375" />
                  <Point X="-2.856159912109" Y="-22.184201171875" />
                  <Point X="-2.818178222656" Y="-22.146220703125" />
                  <Point X="-2.811268310547" Y="-22.138513671875" />
                  <Point X="-2.798323486328" Y="-22.12238671875" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.751600585938" Y="-21.923560546875" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.023420410156" Y="-21.362380859375" />
                  <Point X="-3.059388183594" Y="-21.300083984375" />
                  <Point X="-3.004720947266" Y="-21.258169921875" />
                  <Point X="-2.648384033203" Y="-20.984970703125" />
                  <Point X="-2.41954296875" Y="-20.85783203125" />
                  <Point X="-2.192525390625" Y="-20.731705078125" />
                  <Point X="-2.118564697266" Y="-20.82808984375" />
                  <Point X="-2.1118203125" Y="-20.835951171875" />
                  <Point X="-2.097517333984" Y="-20.850892578125" />
                  <Point X="-2.089958251953" Y="-20.85797265625" />
                  <Point X="-2.07337890625" Y="-20.871884765625" />
                  <Point X="-2.065095703125" Y="-20.87809765625" />
                  <Point X="-2.047898193359" Y="-20.88958984375" />
                  <Point X="-2.038983642578" Y="-20.894869140625" />
                  <Point X="-2.003323608398" Y="-20.91343359375" />
                  <Point X="-1.943767456055" Y="-20.9444375" />
                  <Point X="-1.934333984375" Y="-20.9487109375" />
                  <Point X="-1.915059936523" Y="-20.95620703125" />
                  <Point X="-1.90521875" Y="-20.9594296875" />
                  <Point X="-1.884311523438" Y="-20.965033203125" />
                  <Point X="-1.874173828125" Y="-20.967166015625" />
                  <Point X="-1.853724243164" Y="-20.970314453125" />
                  <Point X="-1.833053710938" Y="-20.971216796875" />
                  <Point X="-1.812407470703" Y="-20.96986328125" />
                  <Point X="-1.802120239258" Y="-20.968623046875" />
                  <Point X="-1.7808046875" Y="-20.96486328125" />
                  <Point X="-1.770720703125" Y="-20.96251171875" />
                  <Point X="-1.750867675781" Y="-20.956724609375" />
                  <Point X="-1.741098754883" Y="-20.9532890625" />
                  <Point X="-1.703956298828" Y="-20.937904296875" />
                  <Point X="-1.641924682617" Y="-20.912208984375" />
                  <Point X="-1.632584350586" Y="-20.907728515625" />
                  <Point X="-1.614449951172" Y="-20.897779296875" />
                  <Point X="-1.605655883789" Y="-20.892310546875" />
                  <Point X="-1.587926513672" Y="-20.879896484375" />
                  <Point X="-1.579778320312" Y="-20.873501953125" />
                  <Point X="-1.564226196289" Y="-20.85986328125" />
                  <Point X="-1.550251098633" Y="-20.844611328125" />
                  <Point X="-1.538019775391" Y="-20.8279296875" />
                  <Point X="-1.532359619141" Y="-20.819255859375" />
                  <Point X="-1.521537841797" Y="-20.80051171875" />
                  <Point X="-1.516855102539" Y="-20.791271484375" />
                  <Point X="-1.508524658203" Y="-20.772337890625" />
                  <Point X="-1.504877075195" Y="-20.76264453125" />
                  <Point X="-1.492788085938" Y="-20.724302734375" />
                  <Point X="-1.47259765625" Y="-20.660267578125" />
                  <Point X="-1.470026123047" Y="-20.650234375" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-1.39248828125" Y="-20.413015625" />
                  <Point X="-0.931163513184" Y="-20.2836796875" />
                  <Point X="-0.653772521973" Y="-20.251212890625" />
                  <Point X="-0.365222686768" Y="-20.21744140625" />
                  <Point X="-0.298039428711" Y="-20.468173828125" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.22043510437" Y="-20.752892578125" />
                  <Point X="-0.207661407471" Y="-20.781083984375" />
                  <Point X="-0.200119094849" Y="-20.79465625" />
                  <Point X="-0.18226071167" Y="-20.8213828125" />
                  <Point X="-0.172608703613" Y="-20.833544921875" />
                  <Point X="-0.151451187134" Y="-20.856134765625" />
                  <Point X="-0.126896156311" Y="-20.8749765625" />
                  <Point X="-0.099600372314" Y="-20.88956640625" />
                  <Point X="-0.08535382843" Y="-20.8957421875" />
                  <Point X="-0.054915859222" Y="-20.90607421875" />
                  <Point X="-0.039853370667" Y="-20.909845703125" />
                  <Point X="-0.009317459106" Y="-20.91488671875" />
                  <Point X="0.021629692078" Y="-20.91488671875" />
                  <Point X="0.052165752411" Y="-20.909845703125" />
                  <Point X="0.067228240967" Y="-20.90607421875" />
                  <Point X="0.097666061401" Y="-20.8957421875" />
                  <Point X="0.111912307739" Y="-20.88956640625" />
                  <Point X="0.139208236694" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920928955" Y="-20.833544921875" />
                  <Point X="0.194573242188" Y="-20.8213828125" />
                  <Point X="0.21243132019" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.358849700928" Y="-20.28717578125" />
                  <Point X="0.378191009521" Y="-20.2149921875" />
                  <Point X="0.425071014404" Y="-20.21990234375" />
                  <Point X="0.827851013184" Y="-20.262083984375" />
                  <Point X="1.057389404297" Y="-20.317501953125" />
                  <Point X="1.453623046875" Y="-20.413166015625" />
                  <Point X="1.601089233398" Y="-20.46665234375" />
                  <Point X="1.858249633789" Y="-20.55992578125" />
                  <Point X="2.002716430664" Y="-20.62748828125" />
                  <Point X="2.250429931641" Y="-20.7433359375" />
                  <Point X="2.390061279297" Y="-20.824685546875" />
                  <Point X="2.629432128906" Y="-20.964142578125" />
                  <Point X="2.761055175781" Y="-21.05774609375" />
                  <Point X="2.817779052734" Y="-21.098083984375" />
                  <Point X="2.388001708984" Y="-21.842482421875" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053180664062" Y="-22.426564453125" />
                  <Point X="2.044182373047" Y="-22.450435546875" />
                  <Point X="2.041301757812" Y="-22.459400390625" />
                  <Point X="2.033260986328" Y="-22.48946875" />
                  <Point X="2.01983203125" Y="-22.5396875" />
                  <Point X="2.017912231445" Y="-22.548537109375" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.016546386719" Y="-22.656421875" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144287109" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.04532019043" Y="-22.776115234375" />
                  <Point X="2.055681152344" Y="-22.79615625" />
                  <Point X="2.061458251953" Y="-22.8058671875" />
                  <Point X="2.077546630859" Y="-22.829578125" />
                  <Point X="2.104416015625" Y="-22.86917578125" />
                  <Point X="2.109808105469" Y="-22.876365234375" />
                  <Point X="2.130462646484" Y="-22.899875" />
                  <Point X="2.157596679688" Y="-22.924611328125" />
                  <Point X="2.168258056641" Y="-22.933017578125" />
                  <Point X="2.191968017578" Y="-22.94910546875" />
                  <Point X="2.231566650391" Y="-22.9759765625" />
                  <Point X="2.241276123047" Y="-22.98175390625" />
                  <Point X="2.26131640625" Y="-22.992115234375" />
                  <Point X="2.271647216797" Y="-22.99669921875" />
                  <Point X="2.293736816406" Y="-23.004966796875" />
                  <Point X="2.304544677734" Y="-23.00829296875" />
                  <Point X="2.326473388672" Y="-23.01363671875" />
                  <Point X="2.337594238281" Y="-23.015654296875" />
                  <Point X="2.363594970703" Y="-23.0187890625" />
                  <Point X="2.407019042969" Y="-23.024025390625" />
                  <Point X="2.416040771484" Y="-23.0246796875" />
                  <Point X="2.447578369141" Y="-23.02450390625" />
                  <Point X="2.484317626953" Y="-23.020177734375" />
                  <Point X="2.497750488281" Y="-23.01760546875" />
                  <Point X="2.527819091797" Y="-23.009564453125" />
                  <Point X="2.578036865234" Y="-22.996134765625" />
                  <Point X="2.584004638672" Y="-22.994326171875" />
                  <Point X="2.604415039063" Y="-22.986927734375" />
                  <Point X="2.627659912109" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.56949609375" Y="-22.43319140625" />
                  <Point X="3.940404541016" Y="-22.219046875" />
                  <Point X="4.043948486328" Y="-22.36294921875" />
                  <Point X="4.117329589844" Y="-22.48421484375" />
                  <Point X="4.136883789062" Y="-22.516529296875" />
                  <Point X="3.597807373047" Y="-22.93017578125" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168137939453" Y="-23.260130859375" />
                  <Point X="3.152118408203" Y="-23.274783203125" />
                  <Point X="3.134669677734" Y="-23.29339453125" />
                  <Point X="3.128578125" Y="-23.30057421875" />
                  <Point X="3.106937744141" Y="-23.3288046875" />
                  <Point X="3.070795898438" Y="-23.375955078125" />
                  <Point X="3.065635986328" Y="-23.383396484375" />
                  <Point X="3.049738769531" Y="-23.41062890625" />
                  <Point X="3.034762695312" Y="-23.444455078125" />
                  <Point X="3.030140380859" Y="-23.457328125" />
                  <Point X="3.022079345703" Y="-23.48615234375" />
                  <Point X="3.008616455078" Y="-23.53429296875" />
                  <Point X="3.006225341797" Y="-23.545337890625" />
                  <Point X="3.002771728516" Y="-23.56763671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.011316650391" Y="-23.6795" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034684082031" Y="-23.771392578125" />
                  <Point X="3.050287353516" Y="-23.804630859375" />
                  <Point X="3.056920166016" Y="-23.816478515625" />
                  <Point X="3.074918457031" Y="-23.843833984375" />
                  <Point X="3.104977783203" Y="-23.8895234375" />
                  <Point X="3.111740234375" Y="-23.898578125" />
                  <Point X="3.126291748047" Y="-23.9158203125" />
                  <Point X="3.134080810547" Y="-23.9240078125" />
                  <Point X="3.15132421875" Y="-23.94009765625" />
                  <Point X="3.16003515625" Y="-23.9473046875" />
                  <Point X="3.178246582031" Y="-23.960630859375" />
                  <Point X="3.187747070312" Y="-23.96675" />
                  <Point X="3.213829101562" Y="-23.981431640625" />
                  <Point X="3.257389160156" Y="-24.005953125" />
                  <Point X="3.265475097656" Y="-24.010009765625" />
                  <Point X="3.294680908203" Y="-24.02191796875" />
                  <Point X="3.330279541016" Y="-24.03198046875" />
                  <Point X="3.343674560547" Y="-24.034744140625" />
                  <Point X="3.378939208984" Y="-24.039404296875" />
                  <Point X="3.437835205078" Y="-24.0471875" />
                  <Point X="3.444033691406" Y="-24.04780078125" />
                  <Point X="3.465708740234" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.387330078125" Y="-23.93046484375" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.75268359375" Y="-24.085767578125" />
                  <Point X="4.775807128906" Y="-24.234287109375" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.178885742188" Y="-24.44818359375" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.686021972656" Y="-24.580458984375" />
                  <Point X="3.665625488281" Y="-24.58786328125" />
                  <Point X="3.642385253906" Y="-24.59837890625" />
                  <Point X="3.634008300781" Y="-24.602681640625" />
                  <Point X="3.599361816406" Y="-24.62270703125" />
                  <Point X="3.541498291016" Y="-24.65615234375" />
                  <Point X="3.533881347656" Y="-24.6610546875" />
                  <Point X="3.508778564453" Y="-24.680125" />
                  <Point X="3.481994628906" Y="-24.705646484375" />
                  <Point X="3.472793457031" Y="-24.715775390625" />
                  <Point X="3.452005615234" Y="-24.742265625" />
                  <Point X="3.417287597656" Y="-24.78650390625" />
                  <Point X="3.410850585938" Y="-24.795796875" />
                  <Point X="3.399128173828" Y="-24.815076171875" />
                  <Point X="3.393842773438" Y="-24.8250625" />
                  <Point X="3.384068603516" Y="-24.84652734375" />
                  <Point X="3.380005126953" Y="-24.857072265625" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.370376708984" Y="-24.88952734375" />
                  <Point X="3.363447265625" Y="-24.925708984375" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.358804199219" Y="-25.11260546875" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373158935547" Y="-25.183986328125" />
                  <Point X="3.380004638672" Y="-25.205486328125" />
                  <Point X="3.384068359375" Y="-25.216033203125" />
                  <Point X="3.393841552734" Y="-25.23749609375" />
                  <Point X="3.399129638672" Y="-25.247486328125" />
                  <Point X="3.410854492188" Y="-25.266767578125" />
                  <Point X="3.417291259766" Y="-25.27605859375" />
                  <Point X="3.438079101562" Y="-25.302546875" />
                  <Point X="3.472797119141" Y="-25.346787109375" />
                  <Point X="3.47871484375" Y="-25.353630859375" />
                  <Point X="3.501132080078" Y="-25.375798828125" />
                  <Point X="3.530174804688" Y="-25.398724609375" />
                  <Point X="3.541497314453" Y="-25.406408203125" />
                  <Point X="3.576143798828" Y="-25.42643359375" />
                  <Point X="3.634007324219" Y="-25.459880859375" />
                  <Point X="3.639498046875" Y="-25.462818359375" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027099609" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.505160644531" Y="-25.701802734375" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761614257812" Y="-25.93105078125" />
                  <Point X="4.731987792969" Y="-26.060876953125" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="4.001441894531" Y="-25.983591796875" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400096679688" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354482666016" Y="-25.912677734375" />
                  <Point X="3.286483886719" Y="-25.92745703125" />
                  <Point X="3.172918457031" Y="-25.952140625" />
                  <Point X="3.157874755859" Y="-25.9567421875" />
                  <Point X="3.128753173828" Y="-25.9683671875" />
                  <Point X="3.114675292969" Y="-25.975390625" />
                  <Point X="3.086848632812" Y="-25.992283203125" />
                  <Point X="3.074123046875" Y="-26.00153125" />
                  <Point X="3.050373535156" Y="-26.022001953125" />
                  <Point X="3.039349609375" Y="-26.033224609375" />
                  <Point X="2.998248535156" Y="-26.08265625" />
                  <Point X="2.92960546875" Y="-26.165212890625" />
                  <Point X="2.921327148438" Y="-26.17684765625" />
                  <Point X="2.906606689453" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.869266357422" Y="-26.36067578125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.934172363281" Y="-26.67790625" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.807421630859" Y="-27.41532421875" />
                  <Point X="4.087170898438" Y="-27.629982421875" />
                  <Point X="4.045495361328" Y="-27.697419921875" />
                  <Point X="4.001273681641" Y="-27.760251953125" />
                  <Point X="3.351360351562" Y="-27.385025390625" />
                  <Point X="2.848454589844" Y="-27.094671875" />
                  <Point X="2.841182373047" Y="-27.090880859375" />
                  <Point X="2.815025878906" Y="-27.079515625" />
                  <Point X="2.783120605469" Y="-27.069328125" />
                  <Point X="2.771107421875" Y="-27.066337890625" />
                  <Point X="2.690178222656" Y="-27.05172265625" />
                  <Point X="2.555017089844" Y="-27.0273125" />
                  <Point X="2.539358886719" Y="-27.02580859375" />
                  <Point X="2.508009277344" Y="-27.025404296875" />
                  <Point X="2.492317871094" Y="-27.02650390625" />
                  <Point X="2.460145019531" Y="-27.0314609375" />
                  <Point X="2.444846435547" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400590820312" Y="-27.051109375" />
                  <Point X="2.333358398438" Y="-27.0864921875" />
                  <Point X="2.221072753906" Y="-27.145587890625" />
                  <Point X="2.208970214844" Y="-27.153169921875" />
                  <Point X="2.186039550781" Y="-27.1700625" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144940429687" Y="-27.21116015625" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.085080078125" Y="-27.313427734375" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.016803588867" Y="-27.6610703125" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.554469482422" Y="-28.713490234375" />
                  <Point X="2.735893066406" Y="-29.027724609375" />
                  <Point X="2.723751953125" Y="-29.036083984375" />
                  <Point X="2.217678466797" Y="-28.3765546875" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828654663086" Y="-27.8701484375" />
                  <Point X="1.808836914062" Y="-27.8496328125" />
                  <Point X="1.783252929688" Y="-27.828005859375" />
                  <Point X="1.773297119141" Y="-27.820646484375" />
                  <Point X="1.693478881836" Y="-27.76933203125" />
                  <Point X="1.560173706055" Y="-27.68362890625" />
                  <Point X="1.546280151367" Y="-27.67624609375" />
                  <Point X="1.517467163086" Y="-27.663875" />
                  <Point X="1.502547607422" Y="-27.65888671875" />
                  <Point X="1.470927368164" Y="-27.65115625" />
                  <Point X="1.455391235352" Y="-27.648697265625" />
                  <Point X="1.42412487793" Y="-27.64637890625" />
                  <Point X="1.40839465332" Y="-27.64651953125" />
                  <Point X="1.321099853516" Y="-27.654552734375" />
                  <Point X="1.175307739258" Y="-27.667966796875" />
                  <Point X="1.161224365234" Y="-27.67033984375" />
                  <Point X="1.133574951172" Y="-27.677171875" />
                  <Point X="1.120008666992" Y="-27.681630859375" />
                  <Point X="1.092622314453" Y="-27.692974609375" />
                  <Point X="1.079876342773" Y="-27.699416015625" />
                  <Point X="1.055494140625" Y="-27.71413671875" />
                  <Point X="1.043857910156" Y="-27.722416015625" />
                  <Point X="0.97645111084" Y="-27.778462890625" />
                  <Point X="0.863874206543" Y="-27.872068359375" />
                  <Point X="0.852652587891" Y="-27.883091796875" />
                  <Point X="0.83218170166" Y="-27.906841796875" />
                  <Point X="0.822932739258" Y="-27.919568359375" />
                  <Point X="0.806040710449" Y="-27.94739453125" />
                  <Point X="0.799019287109" Y="-27.961470703125" />
                  <Point X="0.787394775391" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.762637634277" Y="-28.098357421875" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091247559" Y="-29.15233984375" />
                  <Point X="0.805089050293" Y="-29.047833984375" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146789551" Y="-28.453583984375" />
                  <Point X="0.626788085938" Y="-28.42381640625" />
                  <Point X="0.620407287598" Y="-28.41320703125" />
                  <Point X="0.559088745117" Y="-28.324859375" />
                  <Point X="0.4566796875" Y="-28.177306640625" />
                  <Point X="0.446671112061" Y="-28.165171875" />
                  <Point X="0.424787750244" Y="-28.142716796875" />
                  <Point X="0.412912780762" Y="-28.132396484375" />
                  <Point X="0.386657348633" Y="-28.11315234375" />
                  <Point X="0.373240112305" Y="-28.10493359375" />
                  <Point X="0.345238677979" Y="-28.090826171875" />
                  <Point X="0.330654296875" Y="-28.0849375" />
                  <Point X="0.235767349243" Y="-28.05548828125" />
                  <Point X="0.077295211792" Y="-28.0063046875" />
                  <Point X="0.063377120972" Y="-28.003109375" />
                  <Point X="0.035217838287" Y="-27.99883984375" />
                  <Point X="0.020976644516" Y="-27.997765625" />
                  <Point X="-0.008664708138" Y="-27.997765625" />
                  <Point X="-0.022905902863" Y="-27.99883984375" />
                  <Point X="-0.051065185547" Y="-28.003109375" />
                  <Point X="-0.064983421326" Y="-28.0063046875" />
                  <Point X="-0.159870223999" Y="-28.03575390625" />
                  <Point X="-0.318342376709" Y="-28.0849375" />
                  <Point X="-0.332931030273" Y="-28.090830078125" />
                  <Point X="-0.360932922363" Y="-28.104939453125" />
                  <Point X="-0.374346160889" Y="-28.11315625" />
                  <Point X="-0.400601287842" Y="-28.132400390625" />
                  <Point X="-0.412475372314" Y="-28.142716796875" />
                  <Point X="-0.434359039307" Y="-28.165171875" />
                  <Point X="-0.444368499756" Y="-28.177310546875" />
                  <Point X="-0.505687042236" Y="-28.26566015625" />
                  <Point X="-0.608096069336" Y="-28.4132109375" />
                  <Point X="-0.612470092773" Y="-28.420130859375" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.893310119629" Y="-29.423029296875" />
                  <Point X="-0.985425109863" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.70623242323" Y="-24.306879544531" />
                  <Point X="4.612486406381" Y="-23.900820934241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.614413609281" Y="-24.331482657951" />
                  <Point X="4.51786366235" Y="-23.913278892155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.522594795332" Y="-24.356085771371" />
                  <Point X="4.423240918318" Y="-23.92573685007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.103766477636" Y="-22.541941017805" />
                  <Point X="4.073851612235" Y="-22.412365500038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.772316556315" Y="-25.860063645197" />
                  <Point X="4.750986027052" Y="-25.76767097231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.430775981383" Y="-24.380688884792" />
                  <Point X="4.328618063646" Y="-23.938194328744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020940309007" Y="-22.605495557476" />
                  <Point X="3.932742844262" Y="-22.223470366759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.725338731881" Y="-26.078894422884" />
                  <Point X="4.647058103555" Y="-25.739823769866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.338957167434" Y="-24.405291998212" />
                  <Point X="4.2339951413" Y="-23.950651514295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.938114140379" Y="-22.669050097147" />
                  <Point X="3.846711280587" Y="-22.273140815118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.624783535841" Y="-26.065656108048" />
                  <Point X="4.543130180059" Y="-25.711976567422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.247138353485" Y="-24.429895111632" />
                  <Point X="4.139372218954" Y="-23.963108699845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.85528797175" Y="-22.732604636818" />
                  <Point X="3.760679716912" Y="-22.322811263478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.524228339801" Y="-26.052417793212" />
                  <Point X="4.439202246115" Y="-25.684129319727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.155319521029" Y="-24.454498144889" />
                  <Point X="4.044749296608" Y="-23.975565885396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.772461803121" Y="-22.796159176489" />
                  <Point X="3.674648153236" Y="-22.372481711837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.423673143761" Y="-26.039179478377" />
                  <Point X="4.335274306158" Y="-25.656282045984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.063500634972" Y="-24.479100945974" />
                  <Point X="3.950126374262" Y="-23.988023070946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.689635634493" Y="-22.859713716161" />
                  <Point X="3.588616589561" Y="-22.422152160196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.323117947721" Y="-26.025941163541" />
                  <Point X="4.231346366201" Y="-25.628434772241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.971681748915" Y="-24.50370374706" />
                  <Point X="3.855503451916" Y="-24.000480256496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.606809465864" Y="-22.923268255832" />
                  <Point X="3.502584994632" Y="-22.471822473179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.222562751681" Y="-26.012702848705" />
                  <Point X="4.127418426244" Y="-25.600587498497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879862862858" Y="-24.528306548146" />
                  <Point X="3.76088052957" Y="-24.012937442047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.523983357" Y="-22.986823054374" />
                  <Point X="3.416553390771" Y="-22.521492747477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.122007555641" Y="-25.999464533869" />
                  <Point X="4.023490486286" Y="-25.572740224754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.7880439768" Y="-24.552909349232" />
                  <Point X="3.666257607225" Y="-24.025394627597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.441157255424" Y="-23.050377884482" />
                  <Point X="3.330521786911" Y="-22.571163021775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021452359601" Y="-25.986226219034" />
                  <Point X="3.919562546329" Y="-25.544892951011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696225090743" Y="-24.577512150317" />
                  <Point X="3.571634684879" Y="-24.037851813147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358331153848" Y="-23.11393271459" />
                  <Point X="3.24449018305" Y="-22.620833296073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.920897192111" Y="-25.972988027862" />
                  <Point X="3.815634606372" Y="-25.517045677267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.608006689356" Y="-24.617710363887" />
                  <Point X="3.476578592882" Y="-24.048432734804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.275505052272" Y="-23.177487544698" />
                  <Point X="3.15845857919" Y="-22.670503570371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.801818975686" Y="-21.12572773198" />
                  <Point X="2.791048357863" Y="-21.079075060726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.820342031714" Y="-25.959749867413" />
                  <Point X="3.711706666414" Y="-25.489198403524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.522509169088" Y="-24.669694008381" />
                  <Point X="3.376934151629" Y="-24.039139332357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.192678950696" Y="-23.241042374806" />
                  <Point X="3.072426975329" Y="-22.720173844669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.732170774294" Y="-21.246362318808" />
                  <Point X="2.674397735777" Y="-20.996119796289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.719786871317" Y="-25.946511706965" />
                  <Point X="3.603347756716" Y="-25.442158491246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.444092582138" Y="-24.752348544708" />
                  <Point X="3.273462000719" Y="-24.013266297878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.113473260178" Y="-23.320278928066" />
                  <Point X="2.986395371469" Y="-22.769844118966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662522572902" Y="-21.366996905635" />
                  <Point X="2.56020504504" Y="-20.923811002188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.031204715267" Y="-27.717724675702" />
                  <Point X="3.994537813027" Y="-27.558902873262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.61923171092" Y="-25.933273546516" />
                  <Point X="3.487391554518" Y="-25.362211089799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.374652209866" Y="-24.873883338354" />
                  <Point X="3.160876869591" Y="-23.947920609438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.041256466833" Y="-23.429787720821" />
                  <Point X="2.900363767608" Y="-22.819514393264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.592874371509" Y="-21.487631492463" />
                  <Point X="2.447554238598" Y="-20.85818084271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.934642678946" Y="-27.721782635848" />
                  <Point X="3.876048463169" Y="-27.467983203838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518676550523" Y="-25.920035386068" />
                  <Point X="2.814332163748" Y="-22.869184667562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.523226170117" Y="-21.60826607929" />
                  <Point X="2.33490336529" Y="-20.792550393604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.822149398367" Y="-27.656834795848" />
                  <Point X="3.757559057548" Y="-27.377063292879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.418482229252" Y="-25.908360191587" />
                  <Point X="2.728300559887" Y="-22.91885494186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.453577968725" Y="-21.728900666118" />
                  <Point X="2.223090605213" Y="-20.730550211742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.709656117788" Y="-27.591886955848" />
                  <Point X="3.63906957518" Y="-27.286143049492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.323533120779" Y="-25.919404509796" />
                  <Point X="2.642268956027" Y="-22.968525216158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.383929761854" Y="-21.849535229216" />
                  <Point X="2.11379063607" Y="-20.679434123182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.597162837209" Y="-27.526939115848" />
                  <Point X="3.520580092812" Y="-27.195222806104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.230692867146" Y="-25.939583281869" />
                  <Point X="2.552708054645" Y="-23.00290842339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.314281466757" Y="-21.970169410165" />
                  <Point X="2.004490666926" Y="-20.628318034621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.484669556631" Y="-27.461991275847" />
                  <Point X="3.402090610444" Y="-27.104302562717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.13890378555" Y="-25.964315180259" />
                  <Point X="2.45986096581" Y="-23.023057588947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.24463317166" Y="-22.090803591114" />
                  <Point X="1.895190694994" Y="-20.577201933983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.372176276052" Y="-27.397043435847" />
                  <Point X="3.283601128076" Y="-27.01338231933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.054001074086" Y="-26.018875224736" />
                  <Point X="2.361313093658" Y="-23.018513949107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.174984876564" Y="-22.211437772063" />
                  <Point X="1.787804410959" Y="-20.534374926302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.259682919263" Y="-27.332095265743" />
                  <Point X="3.165111645708" Y="-26.922462075943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.97709951377" Y="-26.108092062378" />
                  <Point X="2.257232052508" Y="-22.990003521238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.105336581467" Y="-22.332071953012" />
                  <Point X="1.681395113209" Y="-20.495779711144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147189545169" Y="-27.267147020686" />
                  <Point X="3.046426968818" Y="-26.830696352191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.902828627355" Y="-26.208703600555" />
                  <Point X="2.141181276557" Y="-22.909646475864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.03911857843" Y="-22.467564361258" />
                  <Point X="1.574985834984" Y="-20.457184580558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034696171076" Y="-27.202198775629" />
                  <Point X="2.902157046109" Y="-26.628108753437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86104424583" Y="-26.450029650902" />
                  <Point X="1.468576616827" Y="-20.418589710154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.922202796982" Y="-27.137250530573" />
                  <Point X="1.364879038416" Y="-20.391740241891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.811084182418" Y="-27.07825702325" />
                  <Point X="1.261624830047" Y="-20.36681122027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.708211202483" Y="-27.054979283391" />
                  <Point X="1.158370621679" Y="-20.341882198649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.606470266571" Y="-27.036604964907" />
                  <Point X="1.055116413991" Y="-20.316953179976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.506411350565" Y="-27.025516275065" />
                  <Point X="0.951862236535" Y="-20.292024292256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.41355051964" Y="-27.045605917091" />
                  <Point X="0.84860805908" Y="-20.267095404536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326343336917" Y="-27.090184199909" />
                  <Point X="0.748022090766" Y="-20.253723800336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658440205704" Y="-28.950967865831" />
                  <Point X="2.636360216474" Y="-28.855328925179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239407611375" Y="-27.135938292955" />
                  <Point X="0.648107468698" Y="-20.243260116206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.518981960437" Y="-28.769221931833" />
                  <Point X="2.473895934643" Y="-28.573932898839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.156301883477" Y="-27.198281928399" />
                  <Point X="0.54819284663" Y="-20.232796432076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.37952371517" Y="-28.587475997835" />
                  <Point X="2.311431623596" Y="-28.292536745952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085293066213" Y="-27.313023040402" />
                  <Point X="0.448278224562" Y="-20.222332747945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.240065469903" Y="-28.405730063837" />
                  <Point X="2.148967312549" Y="-28.011140593066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01825792565" Y="-27.444976037172" />
                  <Point X="0.362556053476" Y="-20.273343322843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.100607307655" Y="-28.22398448943" />
                  <Point X="0.310182667648" Y="-20.468803356519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.961149161281" Y="-28.042238983787" />
                  <Point X="0.257809259173" Y="-20.664263292102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.822433094586" Y="-27.863707778366" />
                  <Point X="0.19606785174" Y="-20.819145966205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.70483072236" Y="-27.776630031157" />
                  <Point X="0.11450665714" Y="-20.888179710364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.590338041009" Y="-27.703021834949" />
                  <Point X="0.023116884763" Y="-20.914641207002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.481459494528" Y="-27.653731128484" />
                  <Point X="-0.07818325002" Y="-20.898176207973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.382838623411" Y="-27.648871295385" />
                  <Point X="-0.199297856861" Y="-20.795885301263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.287367922129" Y="-27.657656346928" />
                  <Point X="-0.428627804672" Y="-20.224862255917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.191897001771" Y="-27.666440449546" />
                  <Point X="-0.523561537751" Y="-20.235973172273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.099834331862" Y="-27.689987306762" />
                  <Point X="-0.618495270831" Y="-20.247084088628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.015303514157" Y="-27.746158200085" />
                  <Point X="-0.71342893968" Y="-20.258195283195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.933506497564" Y="-27.814170486966" />
                  <Point X="-0.808362570548" Y="-20.269306642279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.85207425242" Y="-27.883762772585" />
                  <Point X="-0.903296201416" Y="-20.280418001363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.782752820143" Y="-28.00581275195" />
                  <Point X="-0.995854880117" Y="-20.30181640846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.735472981787" Y="-28.223335363617" />
                  <Point X="-1.087426721708" Y="-20.327489276688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.807401372291" Y="-28.957205542608" />
                  <Point X="-1.1789985633" Y="-20.353162144916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.566318847895" Y="-28.33527649532" />
                  <Point X="-1.270570404891" Y="-20.378835013144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.424257696765" Y="-28.142256137371" />
                  <Point X="-1.362142246483" Y="-20.404507881372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.312203726756" Y="-28.079211160508" />
                  <Point X="-1.453713897263" Y="-20.430181576096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.207179593689" Y="-28.046615752754" />
                  <Point X="-1.48731617104" Y="-20.706948228756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.102155460061" Y="-28.014020342574" />
                  <Point X="-1.552473156766" Y="-20.847036407886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.000903872577" Y="-27.997765625" />
                  <Point X="-1.635623588603" Y="-20.909186409296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.092641822859" Y="-28.014888792919" />
                  <Point X="-1.724525448627" Y="-20.946424238268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.183621764651" Y="-28.043125460851" />
                  <Point X="-1.816550307011" Y="-20.970134875187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.274601713316" Y="-28.071362099011" />
                  <Point X="-1.917482186296" Y="-20.955264965965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.363925437459" Y="-28.106772633728" />
                  <Point X="-2.027550402474" Y="-20.90082123391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.444946950387" Y="-28.178143996028" />
                  <Point X="-2.151857203791" Y="-20.78470341384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.518109127966" Y="-28.283557879782" />
                  <Point X="-2.253739958662" Y="-20.765714809958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.591271638496" Y="-28.388970321366" />
                  <Point X="-2.340154745208" Y="-20.813725337698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.655198976704" Y="-28.534384689053" />
                  <Point X="-2.426569540412" Y="-20.861735827936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.707572302692" Y="-28.729844981927" />
                  <Point X="-2.512984433437" Y="-20.909745894466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.75994562868" Y="-28.925305274801" />
                  <Point X="-2.599399326461" Y="-20.957755960997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.812318954667" Y="-29.120765567675" />
                  <Point X="-2.684264259791" Y="-21.012479640553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.864692280655" Y="-29.31622586055" />
                  <Point X="-2.76710076824" Y="-21.075989393541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.91706564333" Y="-29.511685994514" />
                  <Point X="-2.84993727669" Y="-21.139499146528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.969439050201" Y="-29.707145937044" />
                  <Point X="-2.749643911649" Y="-21.996231528398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.81106404349" Y="-21.730191709134" />
                  <Point X="-2.932773785139" Y="-21.203008899515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.056342069218" Y="-29.753041697614" />
                  <Point X="-1.173378359002" Y="-29.246101832001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.190368244541" Y="-29.172510552678" />
                  <Point X="-2.813674726579" Y="-22.141197689163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.973528122077" Y="-21.448796563142" />
                  <Point X="-3.015610240801" Y="-21.266518881151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.327318849814" Y="-29.001626400817" />
                  <Point X="-2.893181657307" Y="-22.219129427724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.449569253062" Y="-28.894415819372" />
                  <Point X="-2.979726172248" Y="-22.266578040049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.565445009506" Y="-28.814816866767" />
                  <Point X="-3.075086450042" Y="-22.275841388267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.667716506285" Y="-28.794144436689" />
                  <Point X="-3.174982399381" Y="-22.265458584611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.766713413674" Y="-28.787655811549" />
                  <Point X="-3.282720281882" Y="-22.221108636657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.865710239593" Y="-28.781167539294" />
                  <Point X="-3.395213553301" Y="-22.156160836331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.964317156607" Y="-28.776368148059" />
                  <Point X="-3.507706833621" Y="-22.09121299745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.057046961883" Y="-28.797025324521" />
                  <Point X="-2.318961024494" Y="-27.662550881187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.368570625243" Y="-27.447668092408" />
                  <Point X="-3.620200217505" Y="-22.026264709989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.142888909082" Y="-28.847517092076" />
                  <Point X="-2.384890485777" Y="-27.799293101081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.485606301241" Y="-27.363044976238" />
                  <Point X="-3.730585617438" Y="-21.970447104151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.227357557118" Y="-28.903957271818" />
                  <Point X="-2.454538751652" Y="-27.919927408605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586212955574" Y="-27.349583771048" />
                  <Point X="-3.451775619084" Y="-23.600419976374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.535524603345" Y="-23.237663271552" />
                  <Point X="-3.805772878882" Y="-22.067089385997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311826295479" Y="-28.960397060321" />
                  <Point X="-2.524187017526" Y="-28.040561716129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.677896834254" Y="-27.374771353333" />
                  <Point X="-3.52321412781" Y="-23.713299890178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.654262650022" Y="-23.145666377857" />
                  <Point X="-3.880960035819" Y="-22.163732120514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.394541023784" Y="-29.024434301067" />
                  <Point X="-2.593835283401" Y="-28.161196023654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.764115147656" Y="-27.423632899755" />
                  <Point X="-2.980179974722" Y="-26.487753314039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.057727457444" Y="-26.151858263515" />
                  <Point X="-3.302495441445" Y="-25.091651646017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358628282378" Y="-24.848513599762" />
                  <Point X="-3.608919578166" Y="-23.764382890513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.77275211058" Y="-23.054746228938" />
                  <Point X="-3.956146969369" Y="-22.260375822629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.470122287666" Y="-29.11936997086" />
                  <Point X="-2.663483486497" Y="-28.2818306031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.850146723743" Y="-27.473303294353" />
                  <Point X="-3.056338368996" Y="-26.58018915746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.165908788" Y="-26.105587531011" />
                  <Point X="-3.369107603118" Y="-25.225436765643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.475888315447" Y="-24.762918686351" />
                  <Point X="-3.700267116844" Y="-23.791027321399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.891241571139" Y="-22.963826080019" />
                  <Point X="-4.029278612448" Y="-22.365921965829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.566752841138" Y="-29.123131150624" />
                  <Point X="-2.73313165114" Y="-28.402465349106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.93617829983" Y="-27.522973688951" />
                  <Point X="-3.139164512397" Y="-26.643743806404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.264052627934" Y="-26.102793946975" />
                  <Point X="-3.452266509331" Y="-25.287550060498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.580763849847" Y="-24.730966930141" />
                  <Point X="-3.797581778594" Y="-23.791825302662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.009731031697" Y="-22.872905931099" />
                  <Point X="-4.099143775811" Y="-22.485616787113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.680513733464" Y="-29.05269268092" />
                  <Point X="-2.802779815784" Y="-28.523100095111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.022209875917" Y="-27.57264408355" />
                  <Point X="-3.221990655799" Y="-26.707298455348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358675517237" Y="-26.115251275646" />
                  <Point X="-3.54199537974" Y="-25.321205713939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.684691810813" Y="-24.7031195654" />
                  <Point X="-3.898136958054" Y="-23.778587059641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.128220374758" Y="-22.781986291114" />
                  <Point X="-4.169008939175" Y="-22.605311608397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.796250766274" Y="-28.97369460639" />
                  <Point X="-2.872427980427" Y="-28.643734841117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.108241452004" Y="-27.622314478148" />
                  <Point X="-3.3048167992" Y="-26.770853104292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453298406541" Y="-26.127708604317" />
                  <Point X="-3.633814240965" Y="-25.345808622586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.788619771778" Y="-24.675272200659" />
                  <Point X="-3.998692137514" Y="-23.76534881662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.914827986449" Y="-28.882394328806" />
                  <Point X="-2.94207614507" Y="-28.764369587122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.194273028091" Y="-27.671984872746" />
                  <Point X="-3.387642942601" Y="-26.834407753236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.547921295845" Y="-26.140165932988" />
                  <Point X="-3.725633102189" Y="-25.370411531232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.892547732744" Y="-24.647424835918" />
                  <Point X="-4.099247316975" Y="-23.752110573599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.280304604178" Y="-27.721655267344" />
                  <Point X="-3.470469086002" Y="-26.897962402179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.642544185149" Y="-26.152623261659" />
                  <Point X="-3.817451963414" Y="-25.395014439879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.99647569371" Y="-24.619577471177" />
                  <Point X="-4.199802501382" Y="-23.738872309152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.366336180265" Y="-27.771325661942" />
                  <Point X="-3.553295225607" Y="-26.961517067566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737167074453" Y="-26.16508059033" />
                  <Point X="-3.909270824639" Y="-25.419617348525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.100403654676" Y="-24.591730106436" />
                  <Point X="-4.300357702098" Y="-23.725633974062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.452367756352" Y="-27.820996056541" />
                  <Point X="-3.636121350895" Y="-27.025071794965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.831789963756" Y="-26.177537919001" />
                  <Point X="-4.001089685863" Y="-25.444220257172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.204331615641" Y="-24.563882741695" />
                  <Point X="-4.400912902814" Y="-23.712395638971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.538399323831" Y="-27.870666488424" />
                  <Point X="-3.718947476183" Y="-27.088626522364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.92641285306" Y="-26.189995247672" />
                  <Point X="-4.092908549777" Y="-25.46882315417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.308259576607" Y="-24.536035376954" />
                  <Point X="-4.50146810353" Y="-23.699157303881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.62443088354" Y="-27.920336953964" />
                  <Point X="-3.801773601472" Y="-27.152181249763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.021035742364" Y="-26.202452576343" />
                  <Point X="-4.184727419538" Y="-25.493426025843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.412187537573" Y="-24.508188012213" />
                  <Point X="-4.602023304247" Y="-23.68591896879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710462443249" Y="-27.970007419503" />
                  <Point X="-3.88459972676" Y="-27.215735977162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115658631668" Y="-26.214909905014" />
                  <Point X="-4.276546289299" Y="-25.518028897515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.51611550461" Y="-24.480340621176" />
                  <Point X="-4.669707298753" Y="-23.815061470357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.798629193016" Y="-28.010429360819" />
                  <Point X="-3.967425852048" Y="-27.279290704561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.210281520972" Y="-26.227367233685" />
                  <Point X="-4.36836515906" Y="-25.542631769187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.620043473486" Y="-24.452493222169" />
                  <Point X="-4.722353341426" Y="-24.009340497488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.938575981473" Y="-27.826567313779" />
                  <Point X="-4.050251977336" Y="-27.34284543196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.304904447439" Y="-26.239824401384" />
                  <Point X="-4.460184028821" Y="-25.56723464086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.723971442363" Y="-24.424645823162" />
                  <Point X="-4.762919603674" Y="-24.255942802098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.088711958902" Y="-27.598571040531" />
                  <Point X="-4.133078102625" Y="-27.406400159359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.399527419588" Y="-26.252281371214" />
                  <Point X="-4.552002898582" Y="-25.591837512532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.494150391737" Y="-26.264738341044" />
                  <Point X="-4.643821768343" Y="-25.616440384204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.588773363885" Y="-26.277195310874" />
                  <Point X="-4.735640638104" Y="-25.641043255876" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998373046875" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.621563110352" Y="-29.097009765625" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.403000274658" Y="-28.4331953125" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.274335754395" Y="-28.2663984375" />
                  <Point X="0.179448806763" Y="-28.23694921875" />
                  <Point X="0.020976625443" Y="-28.187765625" />
                  <Point X="-0.008664756775" Y="-28.187765625" />
                  <Point X="-0.103551712036" Y="-28.21721484375" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.349597686768" Y="-28.3739921875" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.709784301758" Y="-29.472205078125" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-0.914239685059" Y="-29.974169921875" />
                  <Point X="-1.100230102539" Y="-29.938068359375" />
                  <Point X="-1.209530151367" Y="-29.909947265625" />
                  <Point X="-1.351589599609" Y="-29.873396484375" />
                  <Point X="-1.327209594727" Y="-29.688212890625" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.33235168457" Y="-29.420796875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282958984" Y="-29.20262890625" />
                  <Point X="-1.473642700195" Y="-29.126015625" />
                  <Point X="-1.619543701172" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.765187011719" Y="-28.9781640625" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878295898" Y="-28.973791015625" />
                  <Point X="-2.086490722656" Y="-29.038345703125" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.400402587891" Y="-29.3406171875" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.583186767578" Y="-29.336427734375" />
                  <Point X="-2.855839355469" Y="-29.167607421875" />
                  <Point X="-3.007161621094" Y="-29.05109375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.784836425781" Y="-28.11201953125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499763183594" Y="-27.59758984375" />
                  <Point X="-2.513982910156" Y="-27.568759765625" />
                  <Point X="-2.531327880859" Y="-27.551416015625" />
                  <Point X="-2.560156982422" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.398182617188" Y="-28.009103515625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.946297363281" Y="-28.13012890625" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.270192871094" Y="-27.665208984375" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.651601074219" Y="-26.797458984375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140326660156" Y="-26.334595703125" />
                  <Point X="-3.161161132812" Y="-26.310638671875" />
                  <Point X="-3.187643798828" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.237657714844" Y="-26.422611328125" />
                  <Point X="-4.803283691406" Y="-26.497076171875" />
                  <Point X="-4.843146484375" Y="-26.341013671875" />
                  <Point X="-4.927393066406" Y="-26.011193359375" />
                  <Point X="-4.956096679688" Y="-25.8105" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.113157714844" Y="-25.277544921875" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.531504150391" Y="-25.114212890625" />
                  <Point X="-3.514145019531" Y="-25.102166015625" />
                  <Point X="-3.502325439453" Y="-25.090646484375" />
                  <Point X="-3.491434570313" Y="-25.06474609375" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.489112304688" Y="-25.005296875" />
                  <Point X="-3.502325439453" Y="-24.971912109375" />
                  <Point X="-3.524540527344" Y="-24.953181640625" />
                  <Point X="-3.54189453125" Y="-24.94113671875" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.485536132812" Y="-24.68523828125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.971940917969" Y="-24.3705078125" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.859865234375" Y="-23.790357421875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.1516015625" Y="-23.553578125" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731703369141" Y="-23.6041328125" />
                  <Point X="-3.708698486328" Y="-23.596880859375" />
                  <Point X="-3.67027734375" Y="-23.584765625" />
                  <Point X="-3.651533935547" Y="-23.573943359375" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.629888916016" Y="-23.5339296875" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.61631640625" Y="-23.45448828125" />
                  <Point X="-3.627454345703" Y="-23.433091796875" />
                  <Point X="-3.646056152344" Y="-23.397359375" />
                  <Point X="-3.659968017578" Y="-23.380779296875" />
                  <Point X="-4.192312988281" Y="-22.972296875" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.370988769531" Y="-22.574443359375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.006958251953" Y="-22.01626171875" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.403803222656" Y="-21.931810546875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514892578" Y="-22.07956640625" />
                  <Point X="-3.106475341797" Y="-22.082369140625" />
                  <Point X="-3.052965576172" Y="-22.08705078125" />
                  <Point X="-3.031507080078" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.990510986328" Y="-22.049853515625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.940877441406" Y="-21.940119140625" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.187965332031" Y="-21.457380859375" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-3.120326171875" Y="-21.107388671875" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.511817382812" Y="-20.6917421875" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.042720214844" Y="-20.61482421875" />
                  <Point X="-1.967826538086" Y="-20.71242578125" />
                  <Point X="-1.951247192383" Y="-20.726337890625" />
                  <Point X="-1.915587280273" Y="-20.74490234375" />
                  <Point X="-1.85603112793" Y="-20.77590625" />
                  <Point X="-1.835124023438" Y="-20.781509765625" />
                  <Point X="-1.813808349609" Y="-20.77775" />
                  <Point X="-1.776666137695" Y="-20.762365234375" />
                  <Point X="-1.714634399414" Y="-20.736669921875" />
                  <Point X="-1.696905029297" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.673994140625" Y="-20.667169921875" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.678736572266" Y="-20.377865234375" />
                  <Point X="-1.689137695313" Y="-20.298861328125" />
                  <Point X="-1.443780883789" Y="-20.2300703125" />
                  <Point X="-0.968083312988" Y="-20.096703125" />
                  <Point X="-0.675859008789" Y="-20.062501953125" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.114513511658" Y="-20.418998046875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282073975" Y="-20.71582421875" />
                  <Point X="0.006156086445" Y="-20.72615625" />
                  <Point X="0.036594089508" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.17532371521" Y="-20.238" />
                  <Point X="0.236648452759" Y="-20.009130859375" />
                  <Point X="0.444861328125" Y="-20.0309375" />
                  <Point X="0.860209960938" Y="-20.074435546875" />
                  <Point X="1.10198046875" Y="-20.132806640625" />
                  <Point X="1.508456298828" Y="-20.230943359375" />
                  <Point X="1.665873168945" Y="-20.2880390625" />
                  <Point X="1.931043823242" Y="-20.38421875" />
                  <Point X="2.083205566406" Y="-20.45537890625" />
                  <Point X="2.338685302734" Y="-20.574859375" />
                  <Point X="2.485706787109" Y="-20.660513671875" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.871168457031" Y="-20.90290625" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.552546630859" Y="-21.937482421875" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.216811279297" Y="-22.5385546875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.205179931641" Y="-22.63367578125" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.234770751953" Y="-22.7228984375" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.274938720703" Y="-22.775794921875" />
                  <Point X="2.298648681641" Y="-22.7918828125" />
                  <Point X="2.338247314453" Y="-22.81875390625" />
                  <Point X="2.360336914062" Y="-22.827021484375" />
                  <Point X="2.386337646484" Y="-22.83015625" />
                  <Point X="2.42976171875" Y="-22.835392578125" />
                  <Point X="2.448665039062" Y="-22.8340546875" />
                  <Point X="2.478733642578" Y="-22.826013671875" />
                  <Point X="2.528951416016" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.47449609375" Y="-22.2686484375" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.056188232422" Y="-22.05465234375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.279884277344" Y="-22.38584765625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.713471923828" Y="-23.0809140625" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.27937109375" Y="-23.41616796875" />
                  <Point X="3.257730712891" Y="-23.4443984375" />
                  <Point X="3.221588867188" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.205058349609" Y="-23.53732421875" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.197396728516" Y="-23.64110546875" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646240234" Y="-23.712044921875" />
                  <Point X="3.23364453125" Y="-23.739400390625" />
                  <Point X="3.263703857422" Y="-23.78508984375" />
                  <Point X="3.280947265625" Y="-23.8011796875" />
                  <Point X="3.307029296875" Y="-23.815861328125" />
                  <Point X="3.350589355469" Y="-23.8403828125" />
                  <Point X="3.368566162109" Y="-23.846380859375" />
                  <Point X="3.403830810547" Y="-23.851041015625" />
                  <Point X="3.462726806641" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.362530273438" Y="-23.74208984375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.876309082031" Y="-23.790326171875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.963545410156" Y="-24.205056640625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.228061523438" Y="-24.6317109375" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087402344" Y="-24.767181640625" />
                  <Point X="3.694440917969" Y="-24.78720703125" />
                  <Point X="3.636577392578" Y="-24.82065234375" />
                  <Point X="3.622265136719" Y="-24.833072265625" />
                  <Point X="3.601477294922" Y="-24.8595625" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.550055664062" Y="-24.961447265625" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.545412597656" Y="-25.0768671875" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758544922" Y="-25.1587578125" />
                  <Point X="3.587546386719" Y="-25.18524609375" />
                  <Point X="3.622264404297" Y="-25.229486328125" />
                  <Point X="3.636576416016" Y="-25.241908203125" />
                  <Point X="3.671222900391" Y="-25.26193359375" />
                  <Point X="3.729086425781" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.554336425781" Y="-25.518275390625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.983541015625" Y="-25.73352734375" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.917226074219" Y="-26.103146484375" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.976641845703" Y="-26.171966796875" />
                  <Point X="3.411981933594" Y="-26.09762890625" />
                  <Point X="3.394836181641" Y="-26.098341796875" />
                  <Point X="3.326837402344" Y="-26.11312109375" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.144344238281" Y="-26.20412890625" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.058467041016" Y="-26.3780859375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.0939921875" Y="-26.57515625" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.923086181641" Y="-27.2645859375" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.303100585938" Y="-27.641994140625" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.139596191406" Y="-27.893837890625" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.256360351562" Y="-27.5495703125" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.656411621094" Y="-27.23869921875" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.421845214844" Y="-27.25462890625" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.253215820312" Y="-27.401916015625" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.203778808594" Y="-27.6273046875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.719014404297" Y="-28.618490234375" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.952733886719" Y="-29.106330078125" />
                  <Point X="2.835294433594" Y="-29.19021484375" />
                  <Point X="2.763144775391" Y="-29.236916015625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="2.066941162109" Y="-28.49221875" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549316406" Y="-27.980466796875" />
                  <Point X="1.590731201172" Y="-27.92915234375" />
                  <Point X="1.45742590332" Y="-27.84344921875" />
                  <Point X="1.425805541992" Y="-27.83571875" />
                  <Point X="1.338510864258" Y="-27.843751953125" />
                  <Point X="1.19271862793" Y="-27.857166015625" />
                  <Point X="1.165332519531" Y="-27.868509765625" />
                  <Point X="1.09792578125" Y="-27.924556640625" />
                  <Point X="0.985348754883" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.94830255127" Y="-28.138712890625" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.051352905273" Y="-29.3545546875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.10544921875" Y="-29.93889453125" />
                  <Point X="0.994369445801" Y="-29.9632421875" />
                  <Point X="0.927688049316" Y="-29.97535546875" />
                  <Point X="0.860200439453" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#154" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.072373132609" Y="4.625224695354" Z="0.95" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.95" />
                  <Point X="-0.687827167114" Y="5.018439361996" Z="0.95" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.95" />
                  <Point X="-1.463434592406" Y="4.849354145221" Z="0.95" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.95" />
                  <Point X="-1.734464829951" Y="4.646890816506" Z="0.95" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.95" />
                  <Point X="-1.727836057336" Y="4.379145697495" Z="0.95" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.95" />
                  <Point X="-1.80195686037" Y="4.315109506691" Z="0.95" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.95" />
                  <Point X="-1.898655254551" Y="4.330727797472" Z="0.95" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.95" />
                  <Point X="-2.009208819016" Y="4.446894628308" Z="0.95" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.95" />
                  <Point X="-2.542256355709" Y="4.383246003837" Z="0.95" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.95" />
                  <Point X="-3.156904143837" Y="3.963571320746" Z="0.95" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.95" />
                  <Point X="-3.237422677147" Y="3.548900191118" Z="0.95" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.95" />
                  <Point X="-2.996843140853" Y="3.086803136521" Z="0.95" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.95" />
                  <Point X="-3.032021631993" Y="3.016781935893" Z="0.95" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.95" />
                  <Point X="-3.108273329085" Y="2.998721558319" Z="0.95" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.95" />
                  <Point X="-3.384959157179" Y="3.142771271599" Z="0.95" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.95" />
                  <Point X="-4.052577615423" Y="3.045721167049" Z="0.95" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.95" />
                  <Point X="-4.420326774766" Y="2.482042096666" Z="0.95" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.95" />
                  <Point X="-4.228906841898" Y="2.019316417288" Z="0.95" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.95" />
                  <Point X="-3.677960478325" Y="1.575100368889" Z="0.95" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.95" />
                  <Point X="-3.682239040352" Y="1.516485397167" Z="0.95" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.95" />
                  <Point X="-3.729891004856" Y="1.482085996979" Z="0.95" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.95" />
                  <Point X="-4.151230960485" Y="1.527274333781" Z="0.95" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.95" />
                  <Point X="-4.914280898113" Y="1.254001521842" Z="0.95" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.95" />
                  <Point X="-5.027558317573" Y="0.668091012806" Z="0.95" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.95" />
                  <Point X="-4.504633192483" Y="0.29774530121" Z="0.95" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.95" />
                  <Point X="-3.559201748797" Y="0.037020903848" Z="0.95" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.95" />
                  <Point X="-3.543021431034" Y="0.011163172741" Z="0.95" />
                  <Point X="-3.539556741714" Y="0" Z="0.95" />
                  <Point X="-3.545343170101" Y="-0.01864377832" Z="0.95" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.95" />
                  <Point X="-3.566166846326" Y="-0.041855079286" Z="0.95" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.95" />
                  <Point X="-4.13225459629" Y="-0.197966755546" Z="0.95" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.95" />
                  <Point X="-5.011748613306" Y="-0.786298351659" Z="0.95" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.95" />
                  <Point X="-4.897746983604" Y="-1.322108856122" Z="0.95" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.95" />
                  <Point X="-4.237287389832" Y="-1.44090244239" Z="0.95" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.95" />
                  <Point X="-3.202594449962" Y="-1.316612415281" Z="0.95" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.95" />
                  <Point X="-3.197495346885" Y="-1.341056312722" Z="0.95" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.95" />
                  <Point X="-3.688194986049" Y="-1.726510142878" Z="0.95" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.95" />
                  <Point X="-4.319292375137" Y="-2.659538688545" Z="0.95" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.95" />
                  <Point X="-3.992239291881" Y="-3.129132270803" Z="0.95" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.95" />
                  <Point X="-3.379338501375" Y="-3.021123380545" Z="0.95" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.95" />
                  <Point X="-2.561988267616" Y="-2.566342201516" Z="0.95" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.95" />
                  <Point X="-2.834293632624" Y="-3.055739881117" Z="0.95" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.95" />
                  <Point X="-3.043821185752" Y="-4.059431093643" Z="0.95" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.95" />
                  <Point X="-2.61566415568" Y="-4.347658522805" Z="0.95" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.95" />
                  <Point X="-2.366890854811" Y="-4.339774975048" Z="0.95" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.95" />
                  <Point X="-2.064868432468" Y="-4.04863882847" Z="0.95" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.95" />
                  <Point X="-1.774612862059" Y="-3.99677642315" Z="0.95" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.95" />
                  <Point X="-1.512765849674" Y="-4.132327851361" Z="0.95" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.95" />
                  <Point X="-1.387547016534" Y="-4.399270186363" Z="0.95" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.95" />
                  <Point X="-1.382937877881" Y="-4.650406497854" Z="0.95" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.95" />
                  <Point X="-1.228145102589" Y="-4.927090293157" Z="0.95" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.95" />
                  <Point X="-0.929923094591" Y="-4.991973857108" Z="0.95" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.95" />
                  <Point X="-0.667644072766" Y="-4.453865692296" Z="0.95" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.95" />
                  <Point X="-0.314677682586" Y="-3.371221093716" Z="0.95" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.95" />
                  <Point X="-0.094886966551" Y="-3.2336887812" Z="0.95" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.95" />
                  <Point X="0.15847211281" Y="-3.253423264088" Z="0.95" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.95" />
                  <Point X="0.355768176847" Y="-3.430424662052" Z="0.95" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.95" />
                  <Point X="0.567110787635" Y="-4.07867033752" Z="0.95" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.95" />
                  <Point X="0.930469209204" Y="-4.993271102605" Z="0.95" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.95" />
                  <Point X="1.109998619471" Y="-4.956453570892" Z="0.95" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.95" />
                  <Point X="1.094769170485" Y="-4.316747825099" Z="0.95" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.95" />
                  <Point X="0.991005738275" Y="-3.11805150044" Z="0.95" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.95" />
                  <Point X="1.123737006955" Y="-2.931721937239" Z="0.95" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.95" />
                  <Point X="1.336935813286" Y="-2.862259521662" Z="0.95" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.95" />
                  <Point X="1.557535811065" Y="-2.939929615357" Z="0.95" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.95" />
                  <Point X="2.02111748354" Y="-3.491375610757" Z="0.95" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.95" />
                  <Point X="2.784158026163" Y="-4.247610292375" Z="0.95" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.95" />
                  <Point X="2.975639607649" Y="-4.11573790583" Z="0.95" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.95" />
                  <Point X="2.756159637975" Y="-3.562209096578" Z="0.95" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.95" />
                  <Point X="2.24682688371" Y="-2.587137455655" Z="0.95" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.95" />
                  <Point X="2.291306291637" Y="-2.393922510208" Z="0.95" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.95" />
                  <Point X="2.438975868164" Y="-2.26759501833" Z="0.95" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.95" />
                  <Point X="2.641369347858" Y="-2.256621125781" Z="0.95" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.95" />
                  <Point X="3.225204500529" Y="-2.561589862299" Z="0.95" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.95" />
                  <Point X="4.174329075985" Y="-2.891334409567" Z="0.95" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.95" />
                  <Point X="4.339478416168" Y="-2.636999209956" Z="0.95" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.95" />
                  <Point X="3.947368126884" Y="-2.193637466852" Z="0.95" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.95" />
                  <Point X="3.129893968534" Y="-1.516835763929" Z="0.95" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.95" />
                  <Point X="3.102100717235" Y="-1.351388309018" Z="0.95" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.95" />
                  <Point X="3.176634645127" Y="-1.204815786407" Z="0.95" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.95" />
                  <Point X="3.33130115772" Y="-1.130700223706" Z="0.95" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.95" />
                  <Point X="3.963959838658" Y="-1.190259315328" Z="0.95" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.95" />
                  <Point X="4.959817406447" Y="-1.082990241349" Z="0.95" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.95" />
                  <Point X="5.026826241412" Y="-0.709702633977" Z="0.95" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.95" />
                  <Point X="4.561121261384" Y="-0.438698486297" Z="0.95" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.95" />
                  <Point X="3.690089161432" Y="-0.187364474603" Z="0.95" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.95" />
                  <Point X="3.620724591754" Y="-0.123099160149" Z="0.95" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.95" />
                  <Point X="3.588364016063" Y="-0.036182165931" Z="0.95" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.95" />
                  <Point X="3.593007434361" Y="0.060428365289" Z="0.95" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.95" />
                  <Point X="3.634654846647" Y="0.140849578426" Z="0.95" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.95" />
                  <Point X="3.71330625292" Y="0.200784369639" Z="0.95" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.95" />
                  <Point X="4.234846555923" Y="0.351273446472" Z="0.95" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.95" />
                  <Point X="5.006794309027" Y="0.833915820735" Z="0.95" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.95" />
                  <Point X="4.918734180038" Y="1.252781243288" Z="0.95" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.95" />
                  <Point X="4.349847944739" Y="1.338763881496" Z="0.95" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.95" />
                  <Point X="3.404225320156" Y="1.229807919436" Z="0.95" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.95" />
                  <Point X="3.325400603209" Y="1.258989095253" Z="0.95" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.95" />
                  <Point X="3.269259307726" Y="1.319359761055" Z="0.95" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.95" />
                  <Point X="3.240209338185" Y="1.400278371993" Z="0.95" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.95" />
                  <Point X="3.247054926584" Y="1.480489285773" Z="0.95" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.95" />
                  <Point X="3.2912578249" Y="1.556463579626" Z="0.95" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.95" />
                  <Point X="3.737753968014" Y="1.9106984178" Z="0.95" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.95" />
                  <Point X="4.316505669415" Y="2.671319970297" Z="0.95" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.95" />
                  <Point X="4.090617972073" Y="3.005830662892" Z="0.95" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.95" />
                  <Point X="3.443340127324" Y="2.805933393701" Z="0.95" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.95" />
                  <Point X="2.459660126558" Y="2.253569875626" Z="0.95" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.95" />
                  <Point X="2.386167494724" Y="2.25076532432" Z="0.95" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.95" />
                  <Point X="2.320568183699" Y="2.280769795192" Z="0.95" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.95" />
                  <Point X="2.269988805043" Y="2.336456676683" Z="0.95" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.95" />
                  <Point X="2.248664378438" Y="2.403590949109" Z="0.95" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.95" />
                  <Point X="2.258958067032" Y="2.479809449324" Z="0.95" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.95" />
                  <Point X="2.589691959159" Y="3.068798954631" Z="0.95" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.95" />
                  <Point X="2.89398946401" Y="4.169122119417" Z="0.95" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.95" />
                  <Point X="2.504720787903" Y="4.413970087498" Z="0.95" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.95" />
                  <Point X="2.09823116467" Y="4.621192297922" Z="0.95" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.95" />
                  <Point X="1.676765786751" Y="4.790245600089" Z="0.95" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.95" />
                  <Point X="1.107558973165" Y="4.947077342532" Z="0.95" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.95" />
                  <Point X="0.443913236643" Y="5.050071327311" Z="0.95" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.95" />
                  <Point X="0.120871428944" Y="4.806222810597" Z="0.95" />
                  <Point X="0" Y="4.355124473572" Z="0.95" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>