<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#195" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2883" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.87103527832" Y="-29.661" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721313477" Y="-28.497140625" />
                  <Point X="0.542363037109" Y="-28.467375" />
                  <Point X="0.41658480835" Y="-28.28615234375" />
                  <Point X="0.378635528564" Y="-28.231474609375" />
                  <Point X="0.356749328613" Y="-28.209017578125" />
                  <Point X="0.330494598389" Y="-28.189775390625" />
                  <Point X="0.302495330811" Y="-28.175669921875" />
                  <Point X="0.107860710144" Y="-28.115263671875" />
                  <Point X="0.049136188507" Y="-28.097037109375" />
                  <Point X="0.020983469009" Y="-28.092767578125" />
                  <Point X="-0.008657554626" Y="-28.092765625" />
                  <Point X="-0.036823631287" Y="-28.09703515625" />
                  <Point X="-0.231458251953" Y="-28.157443359375" />
                  <Point X="-0.290182769775" Y="-28.17566796875" />
                  <Point X="-0.318182800293" Y="-28.189775390625" />
                  <Point X="-0.344439025879" Y="-28.20901953125" />
                  <Point X="-0.366323577881" Y="-28.231474609375" />
                  <Point X="-0.492101806641" Y="-28.412697265625" />
                  <Point X="-0.530051208496" Y="-28.467375" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.643838195801" Y="-28.8590390625" />
                  <Point X="-0.916584838867" Y="-29.87694140625" />
                  <Point X="-1.014143188477" Y="-29.858005859375" />
                  <Point X="-1.079352416992" Y="-29.84534765625" />
                  <Point X="-1.24641784668" Y="-29.80236328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.263006958008" Y="-29.282462890625" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287938354492" Y="-29.182966796875" />
                  <Point X="-1.304010620117" Y="-29.15512890625" />
                  <Point X="-1.323644897461" Y="-29.131205078125" />
                  <Point X="-1.50283972168" Y="-28.9740546875" />
                  <Point X="-1.556905761719" Y="-28.926640625" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886230469" Y="-28.897994140625" />
                  <Point X="-1.643027709961" Y="-28.890966796875" />
                  <Point X="-1.88085925293" Y="-28.87537890625" />
                  <Point X="-1.952616821289" Y="-28.87067578125" />
                  <Point X="-1.983415161133" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.042657348633" Y="-28.89480078125" />
                  <Point X="-2.240831298828" Y="-29.027216796875" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312788330078" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.387229736328" Y="-29.167396484375" />
                  <Point X="-2.480147460938" Y="-29.28848828125" />
                  <Point X="-2.706151123047" Y="-29.148552734375" />
                  <Point X="-2.801726806641" Y="-29.089376953125" />
                  <Point X="-3.104721923828" Y="-28.856078125" />
                  <Point X="-2.995648681641" Y="-28.667158203125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412859375" Y="-27.647654296875" />
                  <Point X="-2.406588134766" Y="-27.61612890625" />
                  <Point X="-2.405574951172" Y="-27.5851953125" />
                  <Point X="-2.414559082031" Y="-27.555578125" />
                  <Point X="-2.428775146484" Y="-27.52675" />
                  <Point X="-2.446805175781" Y="-27.501587890625" />
                  <Point X="-2.464153808594" Y="-27.484240234375" />
                  <Point X="-2.489305664062" Y="-27.466216796875" />
                  <Point X="-2.518135009766" Y="-27.451998046875" />
                  <Point X="-2.547754882812" Y="-27.443013671875" />
                  <Point X="-2.578691162109" Y="-27.444025390625" />
                  <Point X="-2.610218261719" Y="-27.450296875" />
                  <Point X="-2.639183349609" Y="-27.461197265625" />
                  <Point X="-2.938044677734" Y="-27.633744140625" />
                  <Point X="-3.818024169922" Y="-28.14180078125" />
                  <Point X="-4.007364013672" Y="-27.893046875" />
                  <Point X="-4.082860839844" Y="-27.793859375" />
                  <Point X="-4.304169433594" Y="-27.422759765625" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-4.106571777344" Y="-27.266314453125" />
                  <Point X="-3.105954345703" Y="-26.498515625" />
                  <Point X="-3.084577148438" Y="-26.475595703125" />
                  <Point X="-3.066612548828" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.047937744141" Y="-26.39698046875" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.35966015625" />
                  <Point X="-3.045555664062" Y="-26.327990234375" />
                  <Point X="-3.052556152344" Y="-26.29824609375" />
                  <Point X="-3.068637939453" Y="-26.27226171875" />
                  <Point X="-3.089469970703" Y="-26.2483046875" />
                  <Point X="-3.112968994141" Y="-26.22876953125" />
                  <Point X="-3.133313476562" Y="-26.216794921875" />
                  <Point X="-3.139459960938" Y="-26.213177734375" />
                  <Point X="-3.168735595703" Y="-26.201951171875" />
                  <Point X="-3.200616210938" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-3.609213378906" Y="-26.2440546875" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.804549316406" Y="-26.108255859375" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.672729980469" Y="-25.525833984375" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.517493896484" Y="-25.214828125" />
                  <Point X="-3.487729248047" Y="-25.199470703125" />
                  <Point X="-3.466408935547" Y="-25.184673828125" />
                  <Point X="-3.459976318359" Y="-25.180208984375" />
                  <Point X="-3.437517578125" Y="-25.158322265625" />
                  <Point X="-3.418274658203" Y="-25.13206640625" />
                  <Point X="-3.404168457031" Y="-25.104068359375" />
                  <Point X="-3.397061523438" Y="-25.081169921875" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.390647949219" Y="-25.046103515625" />
                  <Point X="-3.390647216797" Y="-25.01646484375" />
                  <Point X="-3.394916503906" Y="-24.9883046875" />
                  <Point X="-3.402023193359" Y="-24.96540625" />
                  <Point X="-3.404167480469" Y="-24.95849609375" />
                  <Point X="-3.418272949219" Y="-24.930498046875" />
                  <Point X="-3.437516601563" Y="-24.904240234375" />
                  <Point X="-3.459976318359" Y="-24.8823515625" />
                  <Point X="-3.481296630859" Y="-24.8675546875" />
                  <Point X="-3.490900878906" Y="-24.86169921875" />
                  <Point X="-3.512902587891" Y="-24.850013671875" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-3.876788574219" Y="-24.75" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.843517089844" Y="-24.151623046875" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.706623046875" Y="-23.5880703125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.5827421875" Y="-23.592638671875" />
                  <Point X="-3.765666748047" Y="-23.70020703125" />
                  <Point X="-3.744984375" Y="-23.700658203125" />
                  <Point X="-3.723424316406" Y="-23.698771484375" />
                  <Point X="-3.703141601563" Y="-23.694736328125" />
                  <Point X="-3.655953125" Y="-23.679859375" />
                  <Point X="-3.641710693359" Y="-23.675369140625" />
                  <Point X="-3.622770751953" Y="-23.66703515625" />
                  <Point X="-3.604028320312" Y="-23.656212890625" />
                  <Point X="-3.587347167969" Y="-23.64398046875" />
                  <Point X="-3.573709228516" Y="-23.628427734375" />
                  <Point X="-3.561296142578" Y="-23.610697265625" />
                  <Point X="-3.551351318359" Y="-23.592568359375" />
                  <Point X="-3.532416748047" Y="-23.546857421875" />
                  <Point X="-3.526703857422" Y="-23.533064453125" />
                  <Point X="-3.520915283203" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.515804443359" Y="-23.47125" />
                  <Point X="-3.518951660156" Y="-23.4508046875" />
                  <Point X="-3.524554443359" Y="-23.429896484375" />
                  <Point X="-3.532050537109" Y="-23.41062109375" />
                  <Point X="-3.554897216797" Y="-23.366732421875" />
                  <Point X="-3.561790283203" Y="-23.3534921875" />
                  <Point X="-3.573284423828" Y="-23.336291015625" />
                  <Point X="-3.587196044922" Y="-23.319712890625" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-3.799404785156" Y="-23.1540390625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.155092285156" Y="-22.393015625" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.768934570312" Y="-21.86502734375" />
                  <Point X="-3.750504150391" Y="-21.841337890625" />
                  <Point X="-3.708481933594" Y="-21.865599609375" />
                  <Point X="-3.206657226562" Y="-22.155328125" />
                  <Point X="-3.187727783203" Y="-22.163658203125" />
                  <Point X="-3.167084472656" Y="-22.17016796875" />
                  <Point X="-3.146793701172" Y="-22.174205078125" />
                  <Point X="-3.081073242188" Y="-22.179955078125" />
                  <Point X="-3.061244384766" Y="-22.181689453125" />
                  <Point X="-3.040559082031" Y="-22.18123828125" />
                  <Point X="-3.019101318359" Y="-22.178412109375" />
                  <Point X="-2.999012207031" Y="-22.173494140625" />
                  <Point X="-2.980462890625" Y="-22.16434765625" />
                  <Point X="-2.962209228516" Y="-22.15271875" />
                  <Point X="-2.946078125" Y="-22.139771484375" />
                  <Point X="-2.899429199219" Y="-22.093123046875" />
                  <Point X="-2.885354492188" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.849185546875" Y="-21.89816015625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-2.957211181641" Y="-21.66705859375" />
                  <Point X="-3.183332763672" Y="-21.27540625" />
                  <Point X="-2.8293984375" Y="-21.004046875" />
                  <Point X="-2.700620849609" Y="-20.905314453125" />
                  <Point X="-2.208892089844" Y="-20.63212109375" />
                  <Point X="-2.167035888672" Y="-20.608865234375" />
                  <Point X="-2.043196289063" Y="-20.770255859375" />
                  <Point X="-2.028896118164" Y="-20.7851953125" />
                  <Point X="-2.012318481445" Y="-20.799107421875" />
                  <Point X="-1.995115600586" Y="-20.810603515625" />
                  <Point X="-1.921969116211" Y="-20.848681640625" />
                  <Point X="-1.899899780273" Y="-20.860171875" />
                  <Point X="-1.880626342773" Y="-20.86766796875" />
                  <Point X="-1.85971862793" Y="-20.873271484375" />
                  <Point X="-1.839269775391" Y="-20.876419921875" />
                  <Point X="-1.818624267578" Y="-20.87506640625" />
                  <Point X="-1.797307739258" Y="-20.871306640625" />
                  <Point X="-1.777453125" Y="-20.865517578125" />
                  <Point X="-1.701266235352" Y="-20.833958984375" />
                  <Point X="-1.678279174805" Y="-20.8244375" />
                  <Point X="-1.660146728516" Y="-20.814490234375" />
                  <Point X="-1.642416870117" Y="-20.802076171875" />
                  <Point X="-1.626864501953" Y="-20.7884375" />
                  <Point X="-1.6146328125" Y="-20.771755859375" />
                  <Point X="-1.603810913086" Y="-20.75301171875" />
                  <Point X="-1.595480102539" Y="-20.734080078125" />
                  <Point X="-1.570682617188" Y="-20.65543359375" />
                  <Point X="-1.563200927734" Y="-20.631703125" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.567668579102" Y="-20.493685546875" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.11633996582" Y="-20.236931640625" />
                  <Point X="-0.949623596191" Y="-20.19019140625" />
                  <Point X="-0.35350881958" Y="-20.120423828125" />
                  <Point X="-0.294711242676" Y="-20.113541015625" />
                  <Point X="-0.282357055664" Y="-20.1596484375" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113990784" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155908585" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425941467" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.19100617981" Y="-20.5465234375" />
                  <Point X="0.307419555664" Y="-20.1120625" />
                  <Point X="0.698476135254" Y="-20.153017578125" />
                  <Point X="0.844045532227" Y="-20.16826171875" />
                  <Point X="1.337235717773" Y="-20.287333984375" />
                  <Point X="1.4810234375" Y="-20.322048828125" />
                  <Point X="1.801706787109" Y="-20.438361328125" />
                  <Point X="1.894647216797" Y="-20.47207421875" />
                  <Point X="2.205060058594" Y="-20.6172421875" />
                  <Point X="2.294556640625" Y="-20.65909765625" />
                  <Point X="2.59447265625" Y="-20.833828125" />
                  <Point X="2.680990234375" Y="-20.884232421875" />
                  <Point X="2.943260253906" Y="-21.070744140625" />
                  <Point X="2.809497802734" Y="-21.3024296875" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.4839453125" />
                  <Point X="2.116583496094" Y="-22.545623046875" />
                  <Point X="2.114676513672" Y="-22.55439453125" />
                  <Point X="2.108362548828" Y="-22.591904296875" />
                  <Point X="2.107728027344" Y="-22.619046875" />
                  <Point X="2.114159179688" Y="-22.67237890625" />
                  <Point X="2.116099365234" Y="-22.688470703125" />
                  <Point X="2.121441894531" Y="-22.710392578125" />
                  <Point X="2.129708251953" Y="-22.732482421875" />
                  <Point X="2.140071044922" Y="-22.752529296875" />
                  <Point X="2.173071777344" Y="-22.8011640625" />
                  <Point X="2.178630371094" Y="-22.808552734375" />
                  <Point X="2.20188671875" Y="-22.83652734375" />
                  <Point X="2.221597167969" Y="-22.854408203125" />
                  <Point X="2.270231933594" Y="-22.88741015625" />
                  <Point X="2.284905761719" Y="-22.8973671875" />
                  <Point X="2.304949707031" Y="-22.907728515625" />
                  <Point X="2.327035400391" Y="-22.915994140625" />
                  <Point X="2.348963134766" Y="-22.921337890625" />
                  <Point X="2.402296386719" Y="-22.927767578125" />
                  <Point X="2.412029052734" Y="-22.9284375" />
                  <Point X="2.447027099609" Y="-22.929041015625" />
                  <Point X="2.473208007812" Y="-22.925830078125" />
                  <Point X="2.534885253906" Y="-22.9093359375" />
                  <Point X="2.539715332031" Y="-22.90790625" />
                  <Point X="2.570406982422" Y="-22.897927734375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="2.934443603516" Y="-22.690142578125" />
                  <Point X="3.967325195312" Y="-22.09380859375" />
                  <Point X="4.071958740234" Y="-22.2392265625" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.26219921875" Y="-22.540115234375" />
                  <Point X="4.102259277344" Y="-22.662841796875" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221425048828" Y="-23.3397578125" />
                  <Point X="3.203973632812" Y="-23.358373046875" />
                  <Point X="3.159584228516" Y="-23.41628125" />
                  <Point X="3.14619140625" Y="-23.43375390625" />
                  <Point X="3.136602539062" Y="-23.44909375" />
                  <Point X="3.121629882812" Y="-23.482916015625" />
                  <Point X="3.105094726562" Y="-23.542041015625" />
                  <Point X="3.100105957031" Y="-23.559880859375" />
                  <Point X="3.09665234375" Y="-23.582181640625" />
                  <Point X="3.095836669922" Y="-23.60575" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.111312988281" Y="-23.694017578125" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679931641" Y="-23.7310234375" />
                  <Point X="3.136283203125" Y="-23.76426171875" />
                  <Point X="3.173201904297" Y="-23.820376953125" />
                  <Point X="3.184340820312" Y="-23.837306640625" />
                  <Point X="3.198891601562" Y="-23.854548828125" />
                  <Point X="3.216132080078" Y="-23.87063671875" />
                  <Point X="3.234345214844" Y="-23.88396484375" />
                  <Point X="3.287845214844" Y="-23.914080078125" />
                  <Point X="3.296028564453" Y="-23.918181640625" />
                  <Point X="3.330147949219" Y="-23.933267578125" />
                  <Point X="3.356119384766" Y="-23.9405625" />
                  <Point X="3.428455078125" Y="-23.95012109375" />
                  <Point X="3.450280029297" Y="-23.953005859375" />
                  <Point X="3.462697021484" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="3.816794677734" Y="-23.909755859375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.823897460938" Y="-23.9766640625" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.715309082031" Y="-24.40280078125" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704795166016" Y="-24.674412109375" />
                  <Point X="3.681548339844" Y="-24.684931640625" />
                  <Point X="3.610480712891" Y="-24.726009765625" />
                  <Point X="3.589038330078" Y="-24.73840234375" />
                  <Point X="3.574313232422" Y="-24.748900390625" />
                  <Point X="3.547530761719" Y="-24.774423828125" />
                  <Point X="3.504890136719" Y="-24.828759765625" />
                  <Point X="3.492024902344" Y="-24.84515234375" />
                  <Point X="3.480301757812" Y="-24.8644296875" />
                  <Point X="3.47052734375" Y="-24.88589453125" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.449467285156" Y="-24.981615234375" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443483154297" Y="-25.021876953125" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.459392578125" Y="-25.132771484375" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526123047" Y="-25.176662109375" />
                  <Point X="3.480298828125" Y="-25.198125" />
                  <Point X="3.492024169922" Y="-25.217408203125" />
                  <Point X="3.534664794922" Y="-25.2717421875" />
                  <Point X="3.547530029297" Y="-25.28813671875" />
                  <Point X="3.559994140625" Y="-25.301232421875" />
                  <Point X="3.589035400391" Y="-25.32415625" />
                  <Point X="3.660103271484" Y="-25.365234375" />
                  <Point X="3.681545410156" Y="-25.37762890625" />
                  <Point X="3.692709716797" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.017912841797" Y="-25.47289453125" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.867328125" Y="-25.867107421875" />
                  <Point X="4.855021484375" Y="-25.948732421875" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.582626953125" Y="-26.155927734375" />
                  <Point X="3.424381591797" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659423828" Y="-26.005509765625" />
                  <Point X="3.235178710938" Y="-26.035826171875" />
                  <Point X="3.193095214844" Y="-26.04497265625" />
                  <Point X="3.163974121094" Y="-26.05659765625" />
                  <Point X="3.136147460938" Y="-26.073490234375" />
                  <Point X="3.112397705078" Y="-26.0939609375" />
                  <Point X="3.028090332031" Y="-26.195357421875" />
                  <Point X="3.002653564453" Y="-26.22594921875" />
                  <Point X="2.987933837891" Y="-26.250328125" />
                  <Point X="2.976590087891" Y="-26.277712890625" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.957674316406" Y="-26.436677734375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.95634765625" Y="-26.50756640625" />
                  <Point X="2.964079589844" Y="-26.5391875" />
                  <Point X="2.976450927734" Y="-26.567998046875" />
                  <Point X="3.053641601562" Y="-26.6880625" />
                  <Point X="3.076931152344" Y="-26.724287109375" />
                  <Point X="3.086930419922" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.390267333984" Y="-26.975484375" />
                  <Point X="4.213122558594" Y="-27.6068828125" />
                  <Point X="4.159497558594" Y="-27.69365625" />
                  <Point X="4.124813964844" Y="-27.74978125" />
                  <Point X="4.028981445312" Y="-27.8859453125" />
                  <Point X="3.832528076172" Y="-27.7725234375" />
                  <Point X="2.800954345703" Y="-27.176943359375" />
                  <Point X="2.786137939453" Y="-27.170015625" />
                  <Point X="2.754224365234" Y="-27.15982421875" />
                  <Point X="2.588220214844" Y="-27.12984375" />
                  <Point X="2.538134033203" Y="-27.120798828125" />
                  <Point X="2.506777587891" Y="-27.120396484375" />
                  <Point X="2.47460546875" Y="-27.12535546875" />
                  <Point X="2.444833496094" Y="-27.135177734375" />
                  <Point X="2.306924804688" Y="-27.2077578125" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.131951660156" Y="-27.42834765625" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.563259765625" />
                  <Point X="2.125655761719" Y="-27.729263671875" />
                  <Point X="2.134701171875" Y="-27.7793515625" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.331515136719" Y="-28.137322265625" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.823012695312" Y="-29.0822421875" />
                  <Point X="2.781869140625" Y="-29.11162890625" />
                  <Point X="2.701763183594" Y="-29.16348046875" />
                  <Point X="2.545732177734" Y="-28.96013671875" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506225586" Y="-27.922181640625" />
                  <Point X="1.721923706055" Y="-27.900556640625" />
                  <Point X="1.558198608398" Y="-27.795296875" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986694336" Y="-27.75116796875" />
                  <Point X="1.448366943359" Y="-27.7434375" />
                  <Point X="1.417100708008" Y="-27.741119140625" />
                  <Point X="1.238039550781" Y="-27.757595703125" />
                  <Point X="1.184013916016" Y="-27.76256640625" />
                  <Point X="1.156364135742" Y="-27.769396484375" />
                  <Point X="1.128977661133" Y="-27.780740234375" />
                  <Point X="1.104594970703" Y="-27.795462890625" />
                  <Point X="0.966328552246" Y="-27.910427734375" />
                  <Point X="0.924611328125" Y="-27.945115234375" />
                  <Point X="0.904140441895" Y="-27.968865234375" />
                  <Point X="0.887248596191" Y="-27.99669140625" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.834283325195" Y="-28.216009765625" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.870666687012" Y="-28.7099296875" />
                  <Point X="1.022065551758" Y="-29.859916015625" />
                  <Point X="1.01461895752" Y="-29.861548828125" />
                  <Point X="0.975710571289" Y="-29.870078125" />
                  <Point X="0.929315551758" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-0.996041992188" Y="-29.76474609375" />
                  <Point X="-1.058443847656" Y="-29.7526328125" />
                  <Point X="-1.14124609375" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.169832397461" Y="-29.2639296875" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188125" Y="-29.178470703125" />
                  <Point X="-1.19902734375" Y="-29.14950390625" />
                  <Point X="-1.205666015625" Y="-29.135466796875" />
                  <Point X="-1.22173840332" Y="-29.10762890625" />
                  <Point X="-1.230575317383" Y="-29.094861328125" />
                  <Point X="-1.250209594727" Y="-29.0709375" />
                  <Point X="-1.261006958008" Y="-29.05978125" />
                  <Point X="-1.440201660156" Y="-28.902630859375" />
                  <Point X="-1.494267700195" Y="-28.855216796875" />
                  <Point X="-1.506735717773" Y="-28.84596875" />
                  <Point X="-1.533019165039" Y="-28.829623046875" />
                  <Point X="-1.546834594727" Y="-28.822525390625" />
                  <Point X="-1.576531738281" Y="-28.810224609375" />
                  <Point X="-1.591315917969" Y="-28.805474609375" />
                  <Point X="-1.621457397461" Y="-28.798447265625" />
                  <Point X="-1.636814575195" Y="-28.796169921875" />
                  <Point X="-1.874646240234" Y="-28.78058203125" />
                  <Point X="-1.946403686523" Y="-28.77587890625" />
                  <Point X="-1.961927978516" Y="-28.7761328125" />
                  <Point X="-1.992726318359" Y="-28.779166015625" />
                  <Point X="-2.008000366211" Y="-28.7819453125" />
                  <Point X="-2.039048583984" Y="-28.790263671875" />
                  <Point X="-2.053667480469" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436523437" Y="-28.815810546875" />
                  <Point X="-2.293610595703" Y="-28.9482265625" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359688232422" Y="-28.99276171875" />
                  <Point X="-2.380446289062" Y="-29.010134765625" />
                  <Point X="-2.402760742188" Y="-29.0327734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.462598388672" Y="-29.109564453125" />
                  <Point X="-2.503200439453" Y="-29.162478515625" />
                  <Point X="-2.656140136719" Y="-29.06778125" />
                  <Point X="-2.747613037109" Y="-29.011146484375" />
                  <Point X="-2.98086328125" Y="-28.831548828125" />
                  <Point X="-2.913376220703" Y="-28.714658203125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850341797" Y="-27.710083984375" />
                  <Point X="-2.323948486328" Y="-27.681119140625" />
                  <Point X="-2.319685058594" Y="-27.666189453125" />
                  <Point X="-2.313413818359" Y="-27.6346640625" />
                  <Point X="-2.311639160156" Y="-27.61923828125" />
                  <Point X="-2.310625976562" Y="-27.5883046875" />
                  <Point X="-2.314665527344" Y="-27.557619140625" />
                  <Point X="-2.323649658203" Y="-27.528001953125" />
                  <Point X="-2.329355712891" Y="-27.5135625" />
                  <Point X="-2.343571777344" Y="-27.484734375" />
                  <Point X="-2.351553466797" Y="-27.471416015625" />
                  <Point X="-2.369583496094" Y="-27.44625390625" />
                  <Point X="-2.379631835938" Y="-27.43441015625" />
                  <Point X="-2.39698046875" Y="-27.4170625" />
                  <Point X="-2.408818603516" Y="-27.40701953125" />
                  <Point X="-2.433970458984" Y="-27.38899609375" />
                  <Point X="-2.447284179688" Y="-27.381015625" />
                  <Point X="-2.476113525391" Y="-27.366796875" />
                  <Point X="-2.490560058594" Y="-27.361087890625" />
                  <Point X="-2.520179931641" Y="-27.352103515625" />
                  <Point X="-2.550860107422" Y="-27.348064453125" />
                  <Point X="-2.581796386719" Y="-27.349076171875" />
                  <Point X="-2.597225830078" Y="-27.3508515625" />
                  <Point X="-2.628752929688" Y="-27.357123046875" />
                  <Point X="-2.643678466797" Y="-27.361384765625" />
                  <Point X="-2.672643554688" Y="-27.37228515625" />
                  <Point X="-2.686683105469" Y="-27.378923828125" />
                  <Point X="-2.985544433594" Y="-27.551470703125" />
                  <Point X="-3.793089111328" Y="-28.01770703125" />
                  <Point X="-3.931770507813" Y="-27.8355078125" />
                  <Point X="-4.004020751953" Y="-27.7405859375" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-4.048739257812" Y="-27.34168359375" />
                  <Point X="-3.048121826172" Y="-26.573884765625" />
                  <Point X="-3.036481933594" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005367431641" Y="-26.528044921875" />
                  <Point X="-2.987402832031" Y="-26.5009140625" />
                  <Point X="-2.979835693359" Y="-26.487126953125" />
                  <Point X="-2.967079589844" Y="-26.45849609375" />
                  <Point X="-2.961890625" Y="-26.44365234375" />
                  <Point X="-2.955971923828" Y="-26.420798828125" />
                  <Point X="-2.951552734375" Y="-26.3988046875" />
                  <Point X="-2.948748535156" Y="-26.36837890625" />
                  <Point X="-2.948577636719" Y="-26.353052734375" />
                  <Point X="-2.950785644531" Y="-26.3213828125" />
                  <Point X="-2.953082275391" Y="-26.3062265625" />
                  <Point X="-2.960082763672" Y="-26.276482421875" />
                  <Point X="-2.971775634766" Y="-26.24825" />
                  <Point X="-2.987857421875" Y="-26.222265625" />
                  <Point X="-2.996950195312" Y="-26.20992578125" />
                  <Point X="-3.017782226563" Y="-26.18596875" />
                  <Point X="-3.028739501953" Y="-26.175251953125" />
                  <Point X="-3.052238525391" Y="-26.155716796875" />
                  <Point X="-3.064780273438" Y="-26.1468984375" />
                  <Point X="-3.085124755859" Y="-26.134923828125" />
                  <Point X="-3.105444824219" Y="-26.1244765625" />
                  <Point X="-3.134720458984" Y="-26.11325" />
                  <Point X="-3.149822509766" Y="-26.108853515625" />
                  <Point X="-3.181703125" Y="-26.102376953125" />
                  <Point X="-3.197311767578" Y="-26.10053125" />
                  <Point X="-3.228624755859" Y="-26.09944140625" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-3.62161328125" Y="-26.1498671875" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.712504394531" Y="-26.084744140625" />
                  <Point X="-4.740762695312" Y="-25.974115234375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.648142089844" Y="-25.61759765625" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.500468994141" Y="-25.3097109375" />
                  <Point X="-3.473934082031" Y="-25.299251953125" />
                  <Point X="-3.444169433594" Y="-25.28389453125" />
                  <Point X="-3.433563720703" Y="-25.277515625" />
                  <Point X="-3.412243408203" Y="-25.26271875" />
                  <Point X="-3.393673095703" Y="-25.248244140625" />
                  <Point X="-3.371214355469" Y="-25.226357421875" />
                  <Point X="-3.360893310547" Y="-25.21448046875" />
                  <Point X="-3.341650390625" Y="-25.188224609375" />
                  <Point X="-3.333434326172" Y="-25.174810546875" />
                  <Point X="-3.319328125" Y="-25.1468125" />
                  <Point X="-3.313437988281" Y="-25.132228515625" />
                  <Point X="-3.306331054688" Y="-25.109330078125" />
                  <Point X="-3.300990966797" Y="-25.08850390625" />
                  <Point X="-3.296721435547" Y="-25.060345703125" />
                  <Point X="-3.295647949219" Y="-25.04610546875" />
                  <Point X="-3.295647216797" Y="-25.016466796875" />
                  <Point X="-3.296720458984" Y="-25.002224609375" />
                  <Point X="-3.300989746094" Y="-24.974064453125" />
                  <Point X="-3.304185791016" Y="-24.960146484375" />
                  <Point X="-3.311292480469" Y="-24.937248046875" />
                  <Point X="-3.319326416016" Y="-24.91575390625" />
                  <Point X="-3.333431884766" Y="-24.887755859375" />
                  <Point X="-3.341647705078" Y="-24.874341796875" />
                  <Point X="-3.360891357422" Y="-24.848083984375" />
                  <Point X="-3.371211914062" Y="-24.836205078125" />
                  <Point X="-3.393671630859" Y="-24.81431640625" />
                  <Point X="-3.405810791016" Y="-24.804306640625" />
                  <Point X="-3.427131103516" Y="-24.789509765625" />
                  <Point X="-3.446339599609" Y="-24.777798828125" />
                  <Point X="-3.468341308594" Y="-24.76611328125" />
                  <Point X="-3.478102294922" Y="-24.7616171875" />
                  <Point X="-3.498075927734" Y="-24.75375390625" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.852200927734" Y="-24.658236328125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.749540527344" Y="-24.165529296875" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.633586425781" Y="-23.681763671875" />
                  <Point X="-4.595143554688" Y="-23.686826171875" />
                  <Point X="-3.778067871094" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747056152344" Y="-23.795634765625" />
                  <Point X="-3.736702636719" Y="-23.795296875" />
                  <Point X="-3.715142578125" Y="-23.79341015625" />
                  <Point X="-3.704887695313" Y="-23.7919453125" />
                  <Point X="-3.684604980469" Y="-23.78791015625" />
                  <Point X="-3.674577148438" Y="-23.78533984375" />
                  <Point X="-3.627388671875" Y="-23.770462890625" />
                  <Point X="-3.603448974609" Y="-23.76232421875" />
                  <Point X="-3.584509033203" Y="-23.753990234375" />
                  <Point X="-3.575266357422" Y="-23.7493046875" />
                  <Point X="-3.556523925781" Y="-23.738482421875" />
                  <Point X="-3.547850097656" Y="-23.732822265625" />
                  <Point X="-3.531168945312" Y="-23.72058984375" />
                  <Point X="-3.515919189453" Y="-23.706615234375" />
                  <Point X="-3.50228125" Y="-23.6910625" />
                  <Point X="-3.495885742188" Y="-23.682912109375" />
                  <Point X="-3.48347265625" Y="-23.665181640625" />
                  <Point X="-3.478005126953" Y="-23.65638671875" />
                  <Point X="-3.468060302734" Y="-23.6382578125" />
                  <Point X="-3.463583007813" Y="-23.628923828125" />
                  <Point X="-3.4446484375" Y="-23.583212890625" />
                  <Point X="-3.435499267578" Y="-23.5596484375" />
                  <Point X="-3.429710693359" Y="-23.5397890625" />
                  <Point X="-3.427358398438" Y="-23.529701171875" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.498103515625" />
                  <Point X="-3.4210078125" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057617188" Y="-23.4363515625" />
                  <Point X="-3.427189208984" Y="-23.42621484375" />
                  <Point X="-3.432791992188" Y="-23.405306640625" />
                  <Point X="-3.436014160156" Y="-23.395462890625" />
                  <Point X="-3.443510253906" Y="-23.3761875" />
                  <Point X="-3.447784179688" Y="-23.366755859375" />
                  <Point X="-3.470630859375" Y="-23.3228671875" />
                  <Point X="-3.482802001953" Y="-23.3007109375" />
                  <Point X="-3.494296142578" Y="-23.283509765625" />
                  <Point X="-3.500512207031" Y="-23.275224609375" />
                  <Point X="-3.514423828125" Y="-23.258646484375" />
                  <Point X="-3.521499755859" Y="-23.25108984375" />
                  <Point X="-3.536439453125" Y="-23.236787109375" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.741572265625" Y="-23.078669921875" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.073045898438" Y="-22.440904296875" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.726337646484" Y="-21.964986328125" />
                  <Point X="-3.254157470703" Y="-22.237599609375" />
                  <Point X="-3.244921630859" Y="-22.24228125" />
                  <Point X="-3.2259921875" Y="-22.250611328125" />
                  <Point X="-3.216298583984" Y="-22.254259765625" />
                  <Point X="-3.195655273438" Y="-22.26076953125" />
                  <Point X="-3.185622558594" Y="-22.263341796875" />
                  <Point X="-3.165331787109" Y="-22.26737890625" />
                  <Point X="-3.155073730469" Y="-22.26884375" />
                  <Point X="-3.089353271484" Y="-22.27459375" />
                  <Point X="-3.059172851562" Y="-22.276666015625" />
                  <Point X="-3.038487548828" Y="-22.27621484375" />
                  <Point X="-3.028153808594" Y="-22.27542578125" />
                  <Point X="-3.006696044922" Y="-22.272599609375" />
                  <Point X="-2.99651171875" Y="-22.2706875" />
                  <Point X="-2.976422607422" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.93844921875" Y="-22.249552734375" />
                  <Point X="-2.929419189453" Y="-22.244470703125" />
                  <Point X="-2.911165527344" Y="-22.232841796875" />
                  <Point X="-2.902744628906" Y="-22.226806640625" />
                  <Point X="-2.886613525391" Y="-22.213859375" />
                  <Point X="-2.878903320312" Y="-22.206947265625" />
                  <Point X="-2.832254394531" Y="-22.160298828125" />
                  <Point X="-2.811267089844" Y="-22.138513671875" />
                  <Point X="-2.798321533203" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.754547119141" Y="-21.889880859375" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.874938964844" Y="-21.61955859375" />
                  <Point X="-3.059386962891" Y="-21.3000859375" />
                  <Point X="-2.771596191406" Y="-21.079439453125" />
                  <Point X="-2.648368652344" Y="-20.984962890625" />
                  <Point X="-2.192522460938" Y="-20.731705078125" />
                  <Point X="-2.118564697266" Y="-20.828087890625" />
                  <Point X="-2.111823730469" Y="-20.835947265625" />
                  <Point X="-2.0975234375" Y="-20.85088671875" />
                  <Point X="-2.089965576172" Y="-20.85796484375" />
                  <Point X="-2.073387939453" Y="-20.871876953125" />
                  <Point X="-2.065102294922" Y="-20.87809375" />
                  <Point X="-2.047899414062" Y="-20.88958984375" />
                  <Point X="-2.038982299805" Y="-20.894869140625" />
                  <Point X="-1.965835693359" Y="-20.932947265625" />
                  <Point X="-1.943766357422" Y="-20.9444375" />
                  <Point X="-1.934335693359" Y="-20.9487109375" />
                  <Point X="-1.915062255859" Y="-20.95620703125" />
                  <Point X="-1.905219604492" Y="-20.9594296875" />
                  <Point X="-1.884311767578" Y="-20.965033203125" />
                  <Point X="-1.874175048828" Y="-20.967166015625" />
                  <Point X="-1.853726196289" Y="-20.970314453125" />
                  <Point X="-1.833054931641" Y="-20.971216796875" />
                  <Point X="-1.812409423828" Y="-20.96986328125" />
                  <Point X="-1.802123168945" Y="-20.968623046875" />
                  <Point X="-1.780806640625" Y="-20.96486328125" />
                  <Point X="-1.770715698242" Y="-20.962509765625" />
                  <Point X="-1.750861206055" Y="-20.956720703125" />
                  <Point X="-1.741097167969" Y="-20.95328515625" />
                  <Point X="-1.664910400391" Y="-20.9217265625" />
                  <Point X="-1.641923339844" Y="-20.912205078125" />
                  <Point X="-1.632587158203" Y="-20.907728515625" />
                  <Point X="-1.614454711914" Y="-20.89778125" />
                  <Point X="-1.605658447266" Y="-20.892310546875" />
                  <Point X="-1.587928588867" Y="-20.879896484375" />
                  <Point X="-1.579780029297" Y="-20.873501953125" />
                  <Point X="-1.564227661133" Y="-20.85986328125" />
                  <Point X="-1.550252685547" Y="-20.84461328125" />
                  <Point X="-1.538020996094" Y="-20.827931640625" />
                  <Point X="-1.532360351562" Y="-20.819255859375" />
                  <Point X="-1.521538330078" Y="-20.80051171875" />
                  <Point X="-1.516857543945" Y="-20.791275390625" />
                  <Point X="-1.508526733398" Y="-20.77234375" />
                  <Point X="-1.504877075195" Y="-20.7626484375" />
                  <Point X="-1.480079589844" Y="-20.684001953125" />
                  <Point X="-1.472597900391" Y="-20.660271484375" />
                  <Point X="-1.470026000977" Y="-20.650234375" />
                  <Point X="-1.465990600586" Y="-20.6299453125" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.473481323242" Y="-20.48128515625" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-1.090694213867" Y="-20.328404296875" />
                  <Point X="-0.931164001465" Y="-20.2836796875" />
                  <Point X="-0.365222473145" Y="-20.217443359375" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.282769195557" Y="-20.571111328125" />
                  <Point X="0.378190704346" Y="-20.2149921875" />
                  <Point X="0.688581054688" Y="-20.2475" />
                  <Point X="0.827865783691" Y="-20.2620859375" />
                  <Point X="1.314940185547" Y="-20.379681640625" />
                  <Point X="1.453608642578" Y="-20.41316015625" />
                  <Point X="1.769314941406" Y="-20.52766796875" />
                  <Point X="1.858249755859" Y="-20.559927734375" />
                  <Point X="2.164815673828" Y="-20.703296875" />
                  <Point X="2.250428222656" Y="-20.7433359375" />
                  <Point X="2.546649902344" Y="-20.9159140625" />
                  <Point X="2.629447509766" Y="-20.964150390625" />
                  <Point X="2.817780029297" Y="-21.09808203125" />
                  <Point X="2.727225097656" Y="-21.2549296875" />
                  <Point X="2.065308349609" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.0531796875" Y="-22.42656640625" />
                  <Point X="2.044181640625" Y="-22.450439453125" />
                  <Point X="2.041301635742" Y="-22.459404296875" />
                  <Point X="2.024808227539" Y="-22.52108203125" />
                  <Point X="2.020994628906" Y="-22.538625" />
                  <Point X="2.014680541992" Y="-22.576134765625" />
                  <Point X="2.013388549805" Y="-22.58968359375" />
                  <Point X="2.01275402832" Y="-22.616826171875" />
                  <Point X="2.013411254883" Y="-22.630419921875" />
                  <Point X="2.019842407227" Y="-22.683751953125" />
                  <Point X="2.02380078125" Y="-22.71096484375" />
                  <Point X="2.029143310547" Y="-22.73288671875" />
                  <Point X="2.032467651367" Y="-22.7436875" />
                  <Point X="2.040734130859" Y="-22.76577734375" />
                  <Point X="2.045316772461" Y="-22.776107421875" />
                  <Point X="2.0556796875" Y="-22.796154296875" />
                  <Point X="2.061459716797" Y="-22.80587109375" />
                  <Point X="2.094460449219" Y="-22.854505859375" />
                  <Point X="2.105577636719" Y="-22.869283203125" />
                  <Point X="2.128833984375" Y="-22.8972578125" />
                  <Point X="2.138056640625" Y="-22.906888671875" />
                  <Point X="2.157767089844" Y="-22.92476953125" />
                  <Point X="2.168254882812" Y="-22.93301953125" />
                  <Point X="2.216889648438" Y="-22.966021484375" />
                  <Point X="2.24128125" Y="-22.9817578125" />
                  <Point X="2.261325195312" Y="-22.992119140625" />
                  <Point X="2.271651367188" Y="-22.996701171875" />
                  <Point X="2.293737060547" Y="-23.004966796875" />
                  <Point X="2.304542236328" Y="-23.00829296875" />
                  <Point X="2.326469970703" Y="-23.01363671875" />
                  <Point X="2.337592529297" Y="-23.015654296875" />
                  <Point X="2.39092578125" Y="-23.022083984375" />
                  <Point X="2.410391113281" Y="-23.023423828125" />
                  <Point X="2.445389160156" Y="-23.02402734375" />
                  <Point X="2.458591552734" Y="-23.023333984375" />
                  <Point X="2.484772460938" Y="-23.020123046875" />
                  <Point X="2.497750976562" Y="-23.01760546875" />
                  <Point X="2.559428222656" Y="-23.001111328125" />
                  <Point X="2.569088378906" Y="-22.998251953125" />
                  <Point X="2.599780029297" Y="-22.9882734375" />
                  <Point X="2.609061279297" Y="-22.984708984375" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="2.981943603516" Y="-22.7724140625" />
                  <Point X="3.94040234375" Y="-22.219048828125" />
                  <Point X="3.994845947266" Y="-22.294712890625" />
                  <Point X="4.043961181641" Y="-22.362970703125" />
                  <Point X="4.136884765625" Y="-22.51652734375" />
                  <Point X="4.044426757813" Y="-22.58747265625" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168139892578" Y="-23.26012890625" />
                  <Point X="3.152118408203" Y="-23.274783203125" />
                  <Point X="3.134666992188" Y="-23.2933984375" />
                  <Point X="3.128576660156" Y="-23.300578125" />
                  <Point X="3.084187255859" Y="-23.358486328125" />
                  <Point X="3.070794433594" Y="-23.375958984375" />
                  <Point X="3.065635009766" Y="-23.3833984375" />
                  <Point X="3.049733886719" Y="-23.410638671875" />
                  <Point X="3.034761230469" Y="-23.4444609375" />
                  <Point X="3.030140380859" Y="-23.457330078125" />
                  <Point X="3.013605224609" Y="-23.516455078125" />
                  <Point X="3.006225097656" Y="-23.545341796875" />
                  <Point X="3.002771484375" Y="-23.567642578125" />
                  <Point X="3.001709228516" Y="-23.578896484375" />
                  <Point X="3.000893554688" Y="-23.60246484375" />
                  <Point X="3.001175048828" Y="-23.61376171875" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.00469921875" Y="-23.6474296875" />
                  <Point X="3.018272705078" Y="-23.71321484375" />
                  <Point X="3.022368164062" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034684082031" Y="-23.771392578125" />
                  <Point X="3.050287353516" Y="-23.804630859375" />
                  <Point X="3.056919189453" Y="-23.8164765625" />
                  <Point X="3.093837890625" Y="-23.872591796875" />
                  <Point X="3.104976806641" Y="-23.889521484375" />
                  <Point X="3.111738769531" Y="-23.898576171875" />
                  <Point X="3.126289550781" Y="-23.915818359375" />
                  <Point X="3.134078369141" Y="-23.924005859375" />
                  <Point X="3.151318847656" Y="-23.94009375" />
                  <Point X="3.160029785156" Y="-23.94730078125" />
                  <Point X="3.178242919922" Y="-23.96062890625" />
                  <Point X="3.187745117188" Y="-23.96675" />
                  <Point X="3.241245117188" Y="-23.996865234375" />
                  <Point X="3.257611816406" Y="-24.005068359375" />
                  <Point X="3.291731201172" Y="-24.020154296875" />
                  <Point X="3.304458251953" Y="-24.024728515625" />
                  <Point X="3.3304296875" Y="-24.0320234375" />
                  <Point X="3.343674072266" Y="-24.034744140625" />
                  <Point X="3.416009765625" Y="-24.044302734375" />
                  <Point X="3.437834716797" Y="-24.0471875" />
                  <Point X="3.444032470703" Y="-24.04780078125" />
                  <Point X="3.46570703125" Y="-24.04877734375" />
                  <Point X="3.491213623047" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="3.829194580078" Y="-24.003943359375" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.731593261719" Y="-23.999134765625" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="4.690720703125" Y="-24.311037109375" />
                  <Point X="3.691991455078" Y="-24.578646484375" />
                  <Point X="3.686032226563" Y="-24.580455078125" />
                  <Point X="3.665629638672" Y="-24.587861328125" />
                  <Point X="3.6423828125" Y="-24.598380859375" />
                  <Point X="3.634007324219" Y="-24.60268359375" />
                  <Point X="3.562939697266" Y="-24.64376171875" />
                  <Point X="3.541497314453" Y="-24.656154296875" />
                  <Point X="3.533889892578" Y="-24.661048828125" />
                  <Point X="3.508773925781" Y="-24.68012890625" />
                  <Point X="3.481991455078" Y="-24.70565234375" />
                  <Point X="3.472795898438" Y="-24.715775390625" />
                  <Point X="3.430155273438" Y="-24.770111328125" />
                  <Point X="3.417290039063" Y="-24.78650390625" />
                  <Point X="3.410855712891" Y="-24.795791015625" />
                  <Point X="3.399132568359" Y="-24.815068359375" />
                  <Point X="3.39384375" Y="-24.82505859375" />
                  <Point X="3.384069335938" Y="-24.8465234375" />
                  <Point X="3.380005371094" Y="-24.857072265625" />
                  <Point X="3.373158935547" Y="-24.87857421875" />
                  <Point X="3.370376464844" Y="-24.88952734375" />
                  <Point X="3.356162841797" Y="-24.96374609375" />
                  <Point X="3.351874267578" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584472656" Y="-25.026263671875" />
                  <Point X="3.350280273438" Y="-25.06294140625" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.366088378906" Y="-25.150640625" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373158935547" Y="-25.183986328125" />
                  <Point X="3.38000390625" Y="-25.205484375" />
                  <Point X="3.384066894531" Y="-25.216029296875" />
                  <Point X="3.393839599609" Y="-25.2374921875" />
                  <Point X="3.399126953125" Y="-25.247482421875" />
                  <Point X="3.410852294922" Y="-25.266765625" />
                  <Point X="3.417290283203" Y="-25.27605859375" />
                  <Point X="3.459930908203" Y="-25.330392578125" />
                  <Point X="3.472796142578" Y="-25.346787109375" />
                  <Point X="3.478716064453" Y="-25.3536328125" />
                  <Point X="3.501133544922" Y="-25.37580078125" />
                  <Point X="3.530174804688" Y="-25.398724609375" />
                  <Point X="3.541494628906" Y="-25.406404296875" />
                  <Point X="3.6125625" Y="-25.447482421875" />
                  <Point X="3.634004638672" Y="-25.459876953125" />
                  <Point X="3.639490478516" Y="-25.4628125" />
                  <Point X="3.659155761719" Y="-25.472017578125" />
                  <Point X="3.683025878906" Y="-25.481029296875" />
                  <Point X="3.691991943359" Y="-25.483916015625" />
                  <Point X="3.993324951172" Y="-25.564658203125" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.773389648438" Y="-25.8529453125" />
                  <Point X="4.761612304688" Y="-25.931060546875" />
                  <Point X="4.727802246094" Y="-26.079220703125" />
                  <Point X="4.595026855469" Y="-26.061740234375" />
                  <Point X="3.43678125" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720947266" Y="-25.910841796875" />
                  <Point X="3.354482177734" Y="-25.912677734375" />
                  <Point X="3.215001464844" Y="-25.942994140625" />
                  <Point X="3.17291796875" Y="-25.952140625" />
                  <Point X="3.157874267578" Y="-25.9567421875" />
                  <Point X="3.128753173828" Y="-25.9683671875" />
                  <Point X="3.11467578125" Y="-25.975390625" />
                  <Point X="3.086849121094" Y="-25.992283203125" />
                  <Point X="3.074123779297" Y="-26.00153125" />
                  <Point X="3.050374023438" Y="-26.022001953125" />
                  <Point X="3.039349609375" Y="-26.033224609375" />
                  <Point X="2.955042236328" Y="-26.13462109375" />
                  <Point X="2.92960546875" Y="-26.165212890625" />
                  <Point X="2.921328125" Y="-26.176845703125" />
                  <Point X="2.906608398438" Y="-26.201224609375" />
                  <Point X="2.900166015625" Y="-26.213970703125" />
                  <Point X="2.888822265625" Y="-26.24135546875" />
                  <Point X="2.884363769531" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.863073974609" Y="-26.42797265625" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607910156" Y="-26.514591796875" />
                  <Point X="2.86406640625" Y="-26.530130859375" />
                  <Point X="2.871798339844" Y="-26.561751953125" />
                  <Point X="2.876787109375" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896541015625" Y="-26.619373046875" />
                  <Point X="2.973731689453" Y="-26.7394375" />
                  <Point X="2.997021240234" Y="-26.775662109375" />
                  <Point X="3.001739501953" Y="-26.782349609375" />
                  <Point X="3.019789550781" Y="-26.8044453125" />
                  <Point X="3.043487548828" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.332435058594" Y="-27.050853515625" />
                  <Point X="4.087170654297" Y="-27.629982421875" />
                  <Point X="4.078684082031" Y="-27.64371484375" />
                  <Point X="4.045497314453" Y="-27.69741796875" />
                  <Point X="4.001274658203" Y="-27.760251953125" />
                  <Point X="3.880027832031" Y="-27.69025" />
                  <Point X="2.848454101562" Y="-27.094669921875" />
                  <Point X="2.841192382812" Y="-27.09088671875" />
                  <Point X="2.815037841797" Y="-27.079517578125" />
                  <Point X="2.783124267578" Y="-27.069326171875" />
                  <Point X="2.771108398438" Y="-27.0663359375" />
                  <Point X="2.605104248047" Y="-27.03635546875" />
                  <Point X="2.555018066406" Y="-27.027310546875" />
                  <Point X="2.539353027344" Y="-27.025806640625" />
                  <Point X="2.507996582031" Y="-27.025404296875" />
                  <Point X="2.492305175781" Y="-27.026505859375" />
                  <Point X="2.460133056641" Y="-27.03146484375" />
                  <Point X="2.444841308594" Y="-27.035138671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400589111328" Y="-27.051109375" />
                  <Point X="2.262680419922" Y="-27.123689453125" />
                  <Point X="2.221071044922" Y="-27.145587890625" />
                  <Point X="2.208967285156" Y="-27.153171875" />
                  <Point X="2.186038085938" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144941650391" Y="-27.21116015625" />
                  <Point X="2.128047363281" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.047883789062" Y="-27.384103515625" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564484375" />
                  <Point X="2.002187866211" Y="-27.58014453125" />
                  <Point X="2.03216809082" Y="-27.7461484375" />
                  <Point X="2.041213500977" Y="-27.796236328125" />
                  <Point X="2.043015380859" Y="-27.8042265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.249242675781" Y="-28.184822265625" />
                  <Point X="2.735893066406" Y="-29.027724609375" />
                  <Point X="2.723752441406" Y="-29.03608203125" />
                  <Point X="2.621100830078" Y="-28.9023046875" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828657104492" Y="-27.870150390625" />
                  <Point X="1.808834838867" Y="-27.84962890625" />
                  <Point X="1.783252319336" Y="-27.82800390625" />
                  <Point X="1.773298461914" Y="-27.820646484375" />
                  <Point X="1.609573364258" Y="-27.71538671875" />
                  <Point X="1.560174804688" Y="-27.68362890625" />
                  <Point X="1.546280151367" Y="-27.676244140625" />
                  <Point X="1.517466552734" Y="-27.663873046875" />
                  <Point X="1.502547973633" Y="-27.65888671875" />
                  <Point X="1.470928344727" Y="-27.65115625" />
                  <Point X="1.455391845703" Y="-27.648697265625" />
                  <Point X="1.424125610352" Y="-27.64637890625" />
                  <Point X="1.408395751953" Y="-27.64651953125" />
                  <Point X="1.229334838867" Y="-27.66299609375" />
                  <Point X="1.175308959961" Y="-27.667966796875" />
                  <Point X="1.161231811523" Y="-27.670337890625" />
                  <Point X="1.13358190918" Y="-27.67716796875" />
                  <Point X="1.120009521484" Y="-27.681626953125" />
                  <Point X="1.092623168945" Y="-27.692970703125" />
                  <Point X="1.079872558594" Y="-27.699416015625" />
                  <Point X="1.055489868164" Y="-27.714138671875" />
                  <Point X="1.043857666016" Y="-27.722416015625" />
                  <Point X="0.905591308594" Y="-27.837380859375" />
                  <Point X="0.863874023438" Y="-27.872068359375" />
                  <Point X="0.852652587891" Y="-27.883091796875" />
                  <Point X="0.83218170166" Y="-27.906841796875" />
                  <Point X="0.822932312012" Y="-27.919568359375" />
                  <Point X="0.806040405273" Y="-27.94739453125" />
                  <Point X="0.799018859863" Y="-27.961470703125" />
                  <Point X="0.78739465332" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.741450805664" Y="-28.19583203125" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.776479492188" Y="-28.722330078125" />
                  <Point X="0.833091247559" Y="-29.152337890625" />
                  <Point X="0.655065124512" Y="-28.487935546875" />
                  <Point X="0.652606567383" Y="-28.480125" />
                  <Point X="0.642145629883" Y="-28.453580078125" />
                  <Point X="0.626787353516" Y="-28.423814453125" />
                  <Point X="0.620407409668" Y="-28.41320703125" />
                  <Point X="0.494629211426" Y="-28.231984375" />
                  <Point X="0.456679992676" Y="-28.177306640625" />
                  <Point X="0.446669799805" Y="-28.165169921875" />
                  <Point X="0.424783569336" Y="-28.142712890625" />
                  <Point X="0.412907409668" Y="-28.132392578125" />
                  <Point X="0.386652740479" Y="-28.113150390625" />
                  <Point X="0.37323626709" Y="-28.10493359375" />
                  <Point X="0.345237030029" Y="-28.090828125" />
                  <Point X="0.330654296875" Y="-28.084939453125" />
                  <Point X="0.136019683838" Y="-28.024533203125" />
                  <Point X="0.077295059204" Y="-28.006306640625" />
                  <Point X="0.063380687714" Y="-28.003111328125" />
                  <Point X="0.03522794342" Y="-27.998841796875" />
                  <Point X="0.020989723206" Y="-27.997767578125" />
                  <Point X="-0.008651331902" Y="-27.997765625" />
                  <Point X="-0.02289535141" Y="-27.998837890625" />
                  <Point X="-0.051061470032" Y="-28.003107421875" />
                  <Point X="-0.064983421326" Y="-28.0063046875" />
                  <Point X="-0.259618041992" Y="-28.066712890625" />
                  <Point X="-0.318342529297" Y="-28.0849375" />
                  <Point X="-0.332928222656" Y="-28.090828125" />
                  <Point X="-0.360928192139" Y="-28.104935546875" />
                  <Point X="-0.374342590332" Y="-28.11315234375" />
                  <Point X="-0.400598754883" Y="-28.132396484375" />
                  <Point X="-0.412472839355" Y="-28.14271484375" />
                  <Point X="-0.434357421875" Y="-28.165169921875" />
                  <Point X="-0.444368041992" Y="-28.177306640625" />
                  <Point X="-0.57014630127" Y="-28.358529296875" />
                  <Point X="-0.60809564209" Y="-28.41320703125" />
                  <Point X="-0.612471252441" Y="-28.420130859375" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.735601135254" Y="-28.834451171875" />
                  <Point X="-0.985425598145" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.134420774308" Y="-29.679484774062" />
                  <Point X="-0.95957238589" Y="-29.670321358314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121809587122" Y="-29.583693476877" />
                  <Point X="-0.933719173635" Y="-29.573836076002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.125115754384" Y="-29.488736372892" />
                  <Point X="-0.907865961381" Y="-29.477350793691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.143843059945" Y="-29.394587456518" />
                  <Point X="-0.882012749126" Y="-29.380865511379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.162570365507" Y="-29.300438540145" />
                  <Point X="-0.856159536872" Y="-29.284380229068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.181297640305" Y="-29.206289622159" />
                  <Point X="-0.830306324617" Y="-29.187894946757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.819451798148" Y="-29.101434787188" />
                  <Point X="0.826342167734" Y="-29.10107367822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.698341717335" Y="-29.002966339006" />
                  <Point X="2.720915817959" Y="-29.001783280523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.218572281647" Y="-29.113112730466" />
                  <Point X="-0.804453112363" Y="-29.091409664445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.794314675394" Y="-29.0076217951" />
                  <Point X="0.81390380289" Y="-29.00659517243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.628167166472" Y="-28.91151365851" />
                  <Point X="2.667605268979" Y="-28.909446795138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.618109619556" Y="-29.0913290015" />
                  <Point X="-2.44150396395" Y="-29.082073491281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.303588680816" Y="-29.022437878279" />
                  <Point X="-0.778599900108" Y="-28.994924382134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.76917755264" Y="-28.913808803011" />
                  <Point X="0.801465438046" Y="-28.912116666639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.557992999539" Y="-28.820060957892" />
                  <Point X="2.614294719999" Y="-28.817110309753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.757536439462" Y="-29.003505678634" />
                  <Point X="-2.343899656792" Y="-28.981827893424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.405946651196" Y="-28.932671859329" />
                  <Point X="-0.752746687853" Y="-28.898439099822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.744040429886" Y="-28.819995810922" />
                  <Point X="0.789027073202" Y="-28.817638160849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.487818875595" Y="-28.728608255021" />
                  <Point X="2.56098417102" Y="-28.724773824367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.873212471063" Y="-28.914437629696" />
                  <Point X="-2.189408840347" Y="-28.878600999945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.511399949216" Y="-28.843068059626" />
                  <Point X="-0.726893554695" Y="-28.801953821656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.718903307131" Y="-28.726182818834" />
                  <Point X="0.776588708358" Y="-28.723159655058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.417644751652" Y="-28.63715555215" />
                  <Point X="2.50767362204" Y="-28.632437338982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.976933961655" Y="-28.824743069812" />
                  <Point X="-0.701040577279" Y="-28.705468551652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.693766184377" Y="-28.632369826745" />
                  <Point X="0.764150394631" Y="-28.628681146589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347470627708" Y="-28.545702849279" />
                  <Point X="2.45436307306" Y="-28.540100853596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.920296509022" Y="-28.726644453826" />
                  <Point X="-0.675187599863" Y="-28.608983281648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.668629061623" Y="-28.538556834656" />
                  <Point X="0.751712081357" Y="-28.534202638096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.277296503764" Y="-28.454250146409" />
                  <Point X="2.40105252408" Y="-28.447764368211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.863659277334" Y="-28.628545849418" />
                  <Point X="-0.649334622447" Y="-28.512498011644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.637741853926" Y="-28.44504519175" />
                  <Point X="0.739273768084" Y="-28.439724129603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.207122379821" Y="-28.362797443538" />
                  <Point X="2.3477419751" Y="-28.355427882826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.807022076399" Y="-28.530447246623" />
                  <Point X="-0.609402449563" Y="-28.415274882272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.578629380439" Y="-28.353012772344" />
                  <Point X="0.72683545481" Y="-28.34524562111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136948255877" Y="-28.271344740667" />
                  <Point X="2.29443142612" Y="-28.26309139744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750384875465" Y="-28.432348643827" />
                  <Point X="-0.541017785542" Y="-28.316560621023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.514921133819" Y="-28.261221207201" />
                  <Point X="0.729684670175" Y="-28.24996592719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.066774131934" Y="-28.179892037796" />
                  <Point X="2.241120872701" Y="-28.170754912287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.69374767453" Y="-28.334250041032" />
                  <Point X="-0.472499940707" Y="-28.217839380065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.450225822442" Y="-28.169481371931" />
                  <Point X="0.750599925356" Y="-28.153739432243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.99660000799" Y="-28.088439334925" />
                  <Point X="2.187810294579" Y="-28.078418428429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.637110473596" Y="-28.236151438236" />
                  <Point X="-0.380826438637" Y="-28.117904602532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.318740312874" Y="-28.081241862625" />
                  <Point X="0.771515427343" Y="-28.057512924361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.926425884046" Y="-27.996986632055" />
                  <Point X="2.134499716458" Y="-27.986081944571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.580473272661" Y="-28.138052835441" />
                  <Point X="-0.064514650608" Y="-28.006197031291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.045655796252" Y="-28.000423242828" />
                  <Point X="0.799290101211" Y="-27.960926942514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.856251760103" Y="-27.905533929184" />
                  <Point X="2.081189138337" Y="-27.893745460713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.523836071727" Y="-28.039954232645" />
                  <Point X="0.876268724575" Y="-27.861762290941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.764819507029" Y="-27.815195317653" />
                  <Point X="2.04221060323" Y="-27.800657866308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.902404575898" Y="-27.703169231165" />
                  <Point X="4.046613618524" Y="-27.695611555488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.797747591431" Y="-28.011586733526" />
                  <Point X="-3.780965049026" Y="-28.010707197748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.467198870792" Y="-27.941855629849" />
                  <Point X="0.998376358274" Y="-27.760232528156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.628002944155" Y="-27.727235197012" />
                  <Point X="2.024994911522" Y="-27.706429729609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751346129215" Y="-27.615955496027" />
                  <Point X="4.048588970257" Y="-27.600377658821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.8673787084" Y="-27.920105572865" />
                  <Point X="-3.599744569021" Y="-27.90607946196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.410561669858" Y="-27.843757027054" />
                  <Point X="2.007975498193" Y="-27.612191306397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.600287687725" Y="-27.528741760618" />
                  <Point X="3.932538767487" Y="-27.511329219363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.937009840622" Y="-27.828624413004" />
                  <Point X="-3.418524089016" Y="-27.801451726173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.353924468923" Y="-27.745658424258" />
                  <Point X="2.001384068497" Y="-27.51740637572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449229246234" Y="-27.441528025209" />
                  <Point X="3.816488564717" Y="-27.422280779906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.006091015651" Y="-27.737114431108" />
                  <Point X="-3.237303609011" Y="-27.696823990385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316176049731" Y="-27.648549740567" />
                  <Point X="2.028542895399" Y="-27.420852669044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.298170804744" Y="-27.354314289799" />
                  <Point X="3.700438361947" Y="-27.333232340449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.061103507324" Y="-27.64486714076" />
                  <Point X="-3.056083129006" Y="-27.592196254598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.315943195796" Y="-27.55340716434" />
                  <Point X="2.080029603435" Y="-27.323023992143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147112363253" Y="-27.26710055439" />
                  <Point X="3.584388159177" Y="-27.244183900991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.116115998996" Y="-27.552619850411" />
                  <Point X="-2.874862039485" Y="-27.487568486867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.359338803286" Y="-27.460551058889" />
                  <Point X="2.134725521489" Y="-27.225027127672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.996053921763" Y="-27.179886818981" />
                  <Point X="3.468337956407" Y="-27.155135461534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.171128490668" Y="-27.460372560063" />
                  <Point X="-2.693640561513" Y="-27.382940698778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.467419221777" Y="-27.371084940736" />
                  <Point X="2.263732431401" Y="-27.123135789142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.844655449477" Y="-27.09269090383" />
                  <Point X="3.352287753637" Y="-27.066087022077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.072720030112" Y="-27.360084818313" />
                  <Point X="3.236237451468" Y="-26.977038587829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.939655410323" Y="-27.257980824219" />
                  <Point X="3.120187128785" Y="-26.887990154656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.806590542838" Y="-27.155876817143" />
                  <Point X="3.01483561199" Y="-26.798381020826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.673525675352" Y="-27.053772810068" />
                  <Point X="2.95256490753" Y="-26.706514117291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.540460807867" Y="-26.951668802993" />
                  <Point X="2.893928119313" Y="-26.614456768276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.407395940381" Y="-26.849564795917" />
                  <Point X="2.862616613333" Y="-26.5209673619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.274331072896" Y="-26.747460788842" />
                  <Point X="2.863273665328" Y="-26.425802554395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.141266205411" Y="-26.645356781767" />
                  <Point X="2.872069892121" Y="-26.330211190813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.018264294791" Y="-26.543780151914" />
                  <Point X="2.891850879527" Y="-26.234044140321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.962618135315" Y="-26.4457334874" />
                  <Point X="2.954189419411" Y="-26.135646743012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.529362959904" Y="-26.053095395769" />
                  <Point X="4.736238133386" Y="-26.042253527338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948798915932" Y="-26.349878879931" />
                  <Point X="3.036890491528" Y="-26.036182190608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.01252035054" Y="-25.985051596295" />
                  <Point X="4.758209593289" Y="-25.945971679047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.96865323593" Y="-26.255789027882" />
                  <Point X="3.283368372083" Y="-25.928134459376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.495677741177" Y="-25.917007796821" />
                  <Point X="4.773830282553" Y="-25.850022660542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.670333497669" Y="-26.249839938579" />
                  <Point X="-4.189629411103" Y="-26.22464730491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.041689574615" Y="-26.164486327329" />
                  <Point X="4.71483392862" Y="-25.757984155568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.694311932369" Y="-26.155966222223" />
                  <Point X="4.417880466684" Y="-25.678416454189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.71829037505" Y="-26.062092506285" />
                  <Point X="4.120927004747" Y="-25.598848752809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.741610948856" Y="-25.9681843129" />
                  <Point X="3.823975299295" Y="-25.519280959377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.755115482849" Y="-25.873761682667" />
                  <Point X="3.593127595239" Y="-25.436248802029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.768620016843" Y="-25.779339052434" />
                  <Point X="3.473323631258" Y="-25.347397088861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782124550836" Y="-25.684916422201" />
                  <Point X="3.404237657681" Y="-25.255887358446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.486534134564" Y="-25.574294812037" />
                  <Point X="3.368385687413" Y="-25.162635907721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.04518026536" Y="-25.456034063004" />
                  <Point X="3.350928244857" Y="-25.068420440647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.603826396157" Y="-25.337773313971" />
                  <Point X="3.354369542559" Y="-24.973109717007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.375655023015" Y="-25.230684986139" />
                  <Point X="3.373670426428" Y="-24.876967827675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.313464989933" Y="-25.132295371742" />
                  <Point X="3.422980526043" Y="-24.779253221988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.29564770521" Y="-25.036231234547" />
                  <Point X="3.509484118019" Y="-24.679589387963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.309865478799" Y="-24.941845983617" />
                  <Point X="3.709399996761" Y="-24.573981867845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.359969782558" Y="-24.84934146604" />
                  <Point X="4.150754032563" Y="-24.455721110081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.480826026366" Y="-24.760544900521" />
                  <Point X="4.592108068366" Y="-24.337460352317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.769039666596" Y="-24.680519164494" />
                  <Point X="4.77556309745" Y="-24.232715508775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.065991102795" Y="-24.600951356951" />
                  <Point X="4.760871637018" Y="-24.138355082721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.362941970162" Y="-24.521383519596" />
                  <Point X="4.742560846315" Y="-24.044184337729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.659892837528" Y="-24.441815682241" />
                  <Point X="3.303713291321" Y="-24.024460769943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.917804023279" Y="-23.992277638403" />
                  <Point X="4.719693349183" Y="-23.950252399601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.777257214438" Y="-24.352836115732" />
                  <Point X="3.148501449495" Y="-23.937464705022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763070297569" Y="-24.256962238054" />
                  <Point X="3.076416690122" Y="-23.846112134311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74888339147" Y="-24.161088360941" />
                  <Point X="3.028588621371" Y="-23.753488324312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.734696707112" Y="-24.065214495448" />
                  <Point X="3.007185492261" Y="-23.659479641908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.711384591922" Y="-23.968862386391" />
                  <Point X="3.00324955852" Y="-23.564555542585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.685234744877" Y="-23.872361558109" />
                  <Point X="3.027107490515" Y="-23.468174828481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.659084897832" Y="-23.775860729827" />
                  <Point X="-4.129619426522" Y="-23.748112620269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.526723578883" Y="-23.716516187755" />
                  <Point X="3.074950302263" Y="-23.370537120093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.458990186698" Y="-23.617836058217" />
                  <Point X="3.156085511557" Y="-23.271154631083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.425818473871" Y="-23.520967229543" />
                  <Point X="3.286305862487" Y="-23.169199698803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.427270111967" Y="-23.425912933802" />
                  <Point X="3.419370267663" Y="-23.067095715956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.465468333524" Y="-23.332784444897" />
                  <Point X="2.430776326416" Y="-23.02377535616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.473062135328" Y="-23.02155925082" />
                  <Point X="3.552434672839" Y="-22.96499173311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.531897437116" Y="-23.241135473826" />
                  <Point X="2.181092010118" Y="-22.941730383829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.739354096935" Y="-22.912473107601" />
                  <Point X="3.685499078015" Y="-22.862887750263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646025294762" Y="-23.15198628853" />
                  <Point X="2.092255116978" Y="-22.851255755247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.920574760826" Y="-22.807845362177" />
                  <Point X="3.818563483191" Y="-22.760783767416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.762074854094" Y="-23.062937815351" />
                  <Point X="2.038182639808" Y="-22.758959200826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.101795637564" Y="-22.703217605597" />
                  <Point X="3.951627888366" Y="-22.65867978457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.878125613547" Y="-22.973889405069" />
                  <Point X="2.017570200748" Y="-22.664909080113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.283016623289" Y="-22.598589843306" />
                  <Point X="4.084692453935" Y="-22.556575793317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.994176373" Y="-22.884840994786" />
                  <Point X="2.015734272464" Y="-22.569874924168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.464237609013" Y="-22.493962081015" />
                  <Point X="4.102972635845" Y="-22.460487396708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.110227132453" Y="-22.795792584503" />
                  <Point X="2.037504556959" Y="-22.473603619033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.645458594738" Y="-22.389334318724" />
                  <Point X="4.047174824976" Y="-22.368281263195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.226277891906" Y="-22.70674417422" />
                  <Point X="2.079827059449" Y="-22.376255217794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.826679580462" Y="-22.284706556433" />
                  <Point X="3.981796539533" Y="-22.276577221079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.17099549291" Y="-22.608716573586" />
                  <Point X="2.136464355683" Y="-22.278156610004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.113717147421" Y="-22.510584369828" />
                  <Point X="2.193101651917" Y="-22.180058002214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.05643863537" Y="-22.412452157341" />
                  <Point X="2.249738948151" Y="-22.081959394424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.998071285335" Y="-22.314262881273" />
                  <Point X="-3.146213792098" Y="-22.269618921787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960218640807" Y="-22.25987132895" />
                  <Point X="2.306376244386" Y="-21.983860786634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.920914939415" Y="-22.215088915656" />
                  <Point X="-3.345389310781" Y="-22.184926895539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.829957138715" Y="-22.15791424003" />
                  <Point X="2.36301354062" Y="-21.885762178844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.843758593496" Y="-22.115914950039" />
                  <Point X="-3.496447952436" Y="-22.09771317062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.763207250676" Y="-22.059285653761" />
                  <Point X="2.419650836854" Y="-21.787663571054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.766602247576" Y="-22.016740984422" />
                  <Point X="-3.647506594092" Y="-22.010499445701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748542509071" Y="-21.963386734349" />
                  <Point X="2.476288133088" Y="-21.689564963264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.756800193765" Y="-21.868689128396" />
                  <Point X="2.532925429322" Y="-21.591466355474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.785448367143" Y="-21.775060142674" />
                  <Point X="2.589562725556" Y="-21.493367747684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.838478884324" Y="-21.682708981444" />
                  <Point X="2.64620002179" Y="-21.395269139894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.89178959546" Y="-21.590372504557" />
                  <Point X="2.702837318024" Y="-21.297170532104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945100196659" Y="-21.498036021908" />
                  <Point X="2.759474185813" Y="-21.199071946768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.998410797858" Y="-21.405699539259" />
                  <Point X="2.816110729603" Y="-21.100973378412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.051721399056" Y="-21.31336305661" />
                  <Point X="2.696862956332" Y="-21.012092516523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945354377766" Y="-21.212658224366" />
                  <Point X="2.560698042162" Y="-20.924098244421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.812171319535" Y="-21.110548023176" />
                  <Point X="2.410887398852" Y="-20.83681911468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.678987382189" Y="-21.008437775914" />
                  <Point X="-1.877624912039" Y="-20.966440148452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.763722701698" Y="-20.960470786553" />
                  <Point X="-0.127248033712" Y="-20.874706783351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.159118373969" Y="-20.859698955863" />
                  <Point X="2.261077125096" Y="-20.749539965572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.502771315848" Y="-20.904072310333" />
                  <Point X="-2.060882215603" Y="-20.8809138839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.559435763582" Y="-20.85463418892" />
                  <Point X="-0.2062001891" Y="-20.783714117615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.229119062312" Y="-20.760900002369" />
                  <Point X="2.0805145669" Y="-20.663872475399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313709057091" Y="-20.799033604336" />
                  <Point X="-2.147541334996" Y="-20.790325123032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.502952212783" Y="-20.756543638587" />
                  <Point X="-0.23852671269" Y="-20.690277906058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257808470344" Y="-20.664266081335" />
                  <Point X="1.897595985234" Y="-20.578328459184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.472481277617" Y="-20.659816351672" />
                  <Point X="-0.263663852819" Y="-20.59646491488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.283661603974" Y="-20.567780803144" />
                  <Point X="1.678382822237" Y="-20.494686561376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462975847769" Y="-20.564187820333" />
                  <Point X="-0.288800992948" Y="-20.502651923702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.30951466666" Y="-20.471295528672" />
                  <Point X="1.447400926365" Y="-20.411661436724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475008095089" Y="-20.469688030825" />
                  <Point X="-0.313938133077" Y="-20.408838932524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.335367729345" Y="-20.374810254199" />
                  <Point X="1.123650688346" Y="-20.333498094871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.204805434711" Y="-20.360396936568" />
                  <Point X="-0.339075273206" Y="-20.315025941346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.361220792031" Y="-20.278324979726" />
                  <Point X="0.775572228309" Y="-20.256609741108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.424367574383" Y="-20.22436554857" />
                  <Point X="-0.364212413335" Y="-20.221212950168" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.779272277832" Y="-29.685587890625" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318664551" Y="-28.52154296875" />
                  <Point X="0.338540405273" Y="-28.3403203125" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.274336364746" Y="-28.266400390625" />
                  <Point X="0.079701728821" Y="-28.205994140625" />
                  <Point X="0.020977233887" Y="-28.187767578125" />
                  <Point X="-0.008663844109" Y="-28.187765625" />
                  <Point X="-0.203298477173" Y="-28.248173828125" />
                  <Point X="-0.262022979736" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.414057434082" Y="-28.466865234375" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.552075134277" Y="-28.883626953125" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-1.032244384766" Y="-29.951265625" />
                  <Point X="-1.100259155273" Y="-29.9380625" />
                  <Point X="-1.325946166992" Y="-29.87999609375" />
                  <Point X="-1.35158984375" Y="-29.8733984375" />
                  <Point X="-1.346193969727" Y="-29.8324140625" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683349609" Y="-29.534759765625" />
                  <Point X="-1.356181518555" Y="-29.30099609375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282958984" Y="-29.20262890625" />
                  <Point X="-1.565477783203" Y="-29.045478515625" />
                  <Point X="-1.619543823242" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.887072387695" Y="-28.97017578125" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878051758" Y="-28.973791015625" />
                  <Point X="-2.188052001953" Y="-29.10620703125" />
                  <Point X="-2.247844482422" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.311861083984" Y="-29.225228515625" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.756162353516" Y="-29.22932421875" />
                  <Point X="-2.855838378906" Y="-29.167609375" />
                  <Point X="-3.168362548828" Y="-28.926974609375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-3.077921142578" Y="-28.619658203125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762451172" Y="-27.59759375" />
                  <Point X="-2.513978515625" Y="-27.568765625" />
                  <Point X="-2.531327148438" Y="-27.55141796875" />
                  <Point X="-2.560156494141" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.890544921875" Y="-27.716017578125" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.082957275391" Y="-27.9505859375" />
                  <Point X="-4.161701660156" Y="-27.8471328125" />
                  <Point X="-4.385762207031" Y="-27.47141796875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-4.164404296875" Y="-27.1909453125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822265625" Y="-26.396015625" />
                  <Point X="-3.139903564453" Y="-26.373162109375" />
                  <Point X="-3.138117675781" Y="-26.366267578125" />
                  <Point X="-3.140325683594" Y="-26.33459765625" />
                  <Point X="-3.161157714844" Y="-26.310640625" />
                  <Point X="-3.181502197266" Y="-26.298666015625" />
                  <Point X="-3.187648681641" Y="-26.295048828125" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.596813476562" Y="-26.3382421875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.896594238281" Y="-26.131767578125" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.986673339844" Y="-25.5967109375" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.697317871094" Y="-25.4340703125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541894775391" Y="-25.12142578125" />
                  <Point X="-3.520574462891" Y="-25.10662890625" />
                  <Point X="-3.514141845703" Y="-25.1021640625" />
                  <Point X="-3.494898925781" Y="-25.075908203125" />
                  <Point X="-3.487791992188" Y="-25.053009765625" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.485647216797" Y="-25.016462890625" />
                  <Point X="-3.49275390625" Y="-24.993564453125" />
                  <Point X="-3.494898193359" Y="-24.986654296875" />
                  <Point X="-3.514141845703" Y="-24.960396484375" />
                  <Point X="-3.535462158203" Y="-24.945599609375" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.901376220703" Y="-24.841763671875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.937493652344" Y="-24.137716796875" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.798315917969" Y="-23.56322265625" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.570341308594" Y="-23.498451171875" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731706054688" Y="-23.6041328125" />
                  <Point X="-3.684517578125" Y="-23.589255859375" />
                  <Point X="-3.670275146484" Y="-23.584765625" />
                  <Point X="-3.651532714844" Y="-23.573943359375" />
                  <Point X="-3.639119628906" Y="-23.556212890625" />
                  <Point X="-3.620185058594" Y="-23.510501953125" />
                  <Point X="-3.614472167969" Y="-23.496708984375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316894531" Y="-23.454486328125" />
                  <Point X="-3.639163574219" Y="-23.41059765625" />
                  <Point X="-3.646056640625" Y="-23.397357421875" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.857237304688" Y="-23.229408203125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.237138671875" Y="-22.345126953125" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.843915283203" Y="-21.806693359375" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.660981689453" Y="-21.783328125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138513671875" Y="-22.07956640625" />
                  <Point X="-3.072793212891" Y="-22.08531640625" />
                  <Point X="-3.052964355469" Y="-22.08705078125" />
                  <Point X="-3.031506591797" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.966604003906" Y="-22.025947265625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.943823974609" Y="-21.906439453125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.039483398438" Y="-21.71455859375" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.887200683594" Y="-20.928654296875" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.255029541016" Y="-20.549076171875" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.121450439453" Y="-20.512220703125" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951249023438" Y="-20.726337890625" />
                  <Point X="-1.878102539063" Y="-20.764416015625" />
                  <Point X="-1.856033081055" Y="-20.77590625" />
                  <Point X="-1.835125488281" Y="-20.781509765625" />
                  <Point X="-1.813808959961" Y="-20.77775" />
                  <Point X="-1.737621948242" Y="-20.74619140625" />
                  <Point X="-1.714635009766" Y="-20.736669921875" />
                  <Point X="-1.696905151367" Y="-20.724255859375" />
                  <Point X="-1.686083129883" Y="-20.70551171875" />
                  <Point X="-1.661285644531" Y="-20.626865234375" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.661855834961" Y="-20.5060859375" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.141985839844" Y="-20.145458984375" />
                  <Point X="-0.968082946777" Y="-20.096703125" />
                  <Point X="-0.36455178833" Y="-20.026068359375" />
                  <Point X="-0.224199951172" Y="-20.009640625" />
                  <Point X="-0.190594024658" Y="-20.135060546875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282119751" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594036102" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.099243202209" Y="-20.521935546875" />
                  <Point X="0.236648406982" Y="-20.0091328125" />
                  <Point X="0.708371154785" Y="-20.05853515625" />
                  <Point X="0.860210083008" Y="-20.074435546875" />
                  <Point X="1.35953125" Y="-20.19498828125" />
                  <Point X="1.508454223633" Y="-20.230943359375" />
                  <Point X="1.834098388672" Y="-20.3490546875" />
                  <Point X="1.931046142578" Y="-20.384220703125" />
                  <Point X="2.245304443359" Y="-20.5311875" />
                  <Point X="2.338685302734" Y="-20.574859375" />
                  <Point X="2.642295410156" Y="-20.7517421875" />
                  <Point X="2.732532958984" Y="-20.804314453125" />
                  <Point X="3.018856445312" Y="-21.007931640625" />
                  <Point X="3.068740722656" Y="-21.04340625" />
                  <Point X="2.891770263672" Y="-21.3499296875" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.208358642578" Y="-22.5701640625" />
                  <Point X="2.202044677734" Y="-22.607673828125" />
                  <Point X="2.208475830078" Y="-22.661005859375" />
                  <Point X="2.210416015625" Y="-22.67709765625" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.251683105469" Y="-22.747822265625" />
                  <Point X="2.274939453125" Y="-22.775796875" />
                  <Point X="2.32357421875" Y="-22.808798828125" />
                  <Point X="2.338248046875" Y="-22.818755859375" />
                  <Point X="2.360333740234" Y="-22.827021484375" />
                  <Point X="2.413666992188" Y="-22.833451171875" />
                  <Point X="2.448665039062" Y="-22.8340546875" />
                  <Point X="2.510342285156" Y="-22.817560546875" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.886943603516" Y="-22.60787109375" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.149071289063" Y="-22.183740234375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.362218261719" Y="-22.521904296875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.160091308594" Y="-22.7382109375" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279370605469" Y="-23.41616796875" />
                  <Point X="3.234981201172" Y="-23.474076171875" />
                  <Point X="3.221588378906" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.508501953125" />
                  <Point X="3.196584228516" Y="-23.567626953125" />
                  <Point X="3.191595458984" Y="-23.585466796875" />
                  <Point X="3.190779785156" Y="-23.60903515625" />
                  <Point X="3.204353271484" Y="-23.6748203125" />
                  <Point X="3.208448730469" Y="-23.69466796875" />
                  <Point X="3.215647216797" Y="-23.712046875" />
                  <Point X="3.252565917969" Y="-23.768162109375" />
                  <Point X="3.263704833984" Y="-23.785091796875" />
                  <Point X="3.2809453125" Y="-23.8011796875" />
                  <Point X="3.3344453125" Y="-23.831294921875" />
                  <Point X="3.368564697266" Y="-23.846380859375" />
                  <Point X="3.440900390625" Y="-23.855939453125" />
                  <Point X="3.462725341797" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.804394775391" Y="-23.815568359375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.916201660156" Y="-23.954193359375" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.989491210938" Y="-24.371701171875" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.739896972656" Y="-24.494564453125" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729089355469" Y="-24.7671796875" />
                  <Point X="3.658021728516" Y="-24.8082578125" />
                  <Point X="3.636579345703" Y="-24.820650390625" />
                  <Point X="3.622265625" Y="-24.833072265625" />
                  <Point X="3.579625" Y="-24.887408203125" />
                  <Point X="3.566759765625" Y="-24.90380078125" />
                  <Point X="3.556985351562" Y="-24.925265625" />
                  <Point X="3.542771728516" Y="-24.999484375" />
                  <Point X="3.538483154297" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.552696777344" Y="-25.11490234375" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758056641" Y="-25.1587578125" />
                  <Point X="3.609398681641" Y="-25.213091796875" />
                  <Point X="3.622263916016" Y="-25.229486328125" />
                  <Point X="3.636576171875" Y="-25.241908203125" />
                  <Point X="3.707644042969" Y="-25.282986328125" />
                  <Point X="3.729086181641" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.042500732422" Y="-25.381130859375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.961266601562" Y="-25.88126953125" />
                  <Point X="4.948430664062" Y="-25.966404296875" />
                  <Point X="4.883985351562" Y="-26.2488125" />
                  <Point X="4.874545898438" Y="-26.290177734375" />
                  <Point X="4.570227539062" Y="-26.250115234375" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836669922" Y="-26.098341796875" />
                  <Point X="3.255355957031" Y="-26.128658203125" />
                  <Point X="3.213272460938" Y="-26.1378046875" />
                  <Point X="3.185445800781" Y="-26.154697265625" />
                  <Point X="3.101138427734" Y="-26.25609375" />
                  <Point X="3.075701660156" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.052274658203" Y="-26.4453828125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360839844" Y="-26.516623046875" />
                  <Point X="3.133551513672" Y="-26.6366875" />
                  <Point X="3.156841064453" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.448099609375" Y="-26.900115234375" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.240311035156" Y="-27.74359765625" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.070853759766" Y="-27.99151171875" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.785028320312" Y="-27.854796875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340332031" Y="-27.2533125" />
                  <Point X="2.571336181641" Y="-27.22333203125" />
                  <Point X="2.52125" Y="-27.214287109375" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.351169189453" Y="-27.291826171875" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.21601953125" Y="-27.472591796875" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.219143310547" Y="-27.71237890625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.413787597656" Y="-28.089822265625" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.878230224609" Y="-29.159546875" />
                  <Point X="2.835301757812" Y="-29.190208984375" />
                  <Point X="2.686283203125" Y="-29.286666015625" />
                  <Point X="2.679774658203" Y="-29.29087890625" />
                  <Point X="2.470363525391" Y="-29.01796875" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670548950195" Y="-27.980466796875" />
                  <Point X="1.506823852539" Y="-27.87520703125" />
                  <Point X="1.457425415039" Y="-27.84344921875" />
                  <Point X="1.425805541992" Y="-27.83571875" />
                  <Point X="1.246744384766" Y="-27.8521953125" />
                  <Point X="1.19271875" Y="-27.857166015625" />
                  <Point X="1.165332275391" Y="-27.868509765625" />
                  <Point X="1.027065795898" Y="-27.983474609375" />
                  <Point X="0.985348632813" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.927115783691" Y="-28.2361875" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.964854003906" Y="-28.697529296875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.034960449219" Y="-29.954345703125" />
                  <Point X="0.99437322998" Y="-29.9632421875" />
                  <Point X="0.860200317383" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#194" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.148453669796" Y="4.909161140093" Z="1.95" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.95" />
                  <Point X="-0.376520263215" Y="5.054873288322" Z="1.95" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.95" />
                  <Point X="-1.161639751034" Y="4.933966835172" Z="1.95" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.95" />
                  <Point X="-1.717584090305" Y="4.518668739546" Z="1.95" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.95" />
                  <Point X="-1.71512768982" Y="4.419451391419" Z="1.95" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.95" />
                  <Point X="-1.762912015899" Y="4.331282546769" Z="1.95" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.95" />
                  <Point X="-1.861168578771" Y="4.311213330345" Z="1.95" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.95" />
                  <Point X="-2.087938952835" Y="4.549497783769" Z="1.95" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.95" />
                  <Point X="-2.285468466021" Y="4.525911741359" Z="1.95" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.95" />
                  <Point X="-2.923778829153" Y="4.142305941339" Z="1.95" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.95" />
                  <Point X="-3.088940558729" Y="3.291721632871" Z="1.95" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.95" />
                  <Point X="-2.999789863124" Y="3.120483985277" Z="1.95" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.95" />
                  <Point X="-3.008114724876" Y="3.040688843011" Z="1.95" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.95" />
                  <Point X="-3.07459248033" Y="2.995774836049" Z="1.95" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.95" />
                  <Point X="-3.642137715426" Y="3.291253390017" Z="1.95" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.95" />
                  <Point X="-3.889534692194" Y="3.255289880974" Z="1.95" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.95" />
                  <Point X="-4.286476892867" Y="2.711359003022" Z="1.95" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.95" />
                  <Point X="-3.893831300754" Y="1.762203940412" Z="1.95" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.95" />
                  <Point X="-3.689669083508" Y="1.59759239313" Z="1.95" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.95" />
                  <Point X="-3.672535310171" Y="1.53991227569" Z="1.95" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.95" />
                  <Point X="-3.705707475863" Y="1.489711026875" Z="1.95" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.95" />
                  <Point X="-4.56997105532" Y="1.582402529763" Z="1.95" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.95" />
                  <Point X="-4.85273173301" Y="1.481136804301" Z="1.95" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.95" />
                  <Point X="-4.993110962462" Y="0.900882952136" Z="1.95" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.95" />
                  <Point X="-3.920473216242" Y="0.141220104918" Z="1.95" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.95" />
                  <Point X="-3.570128078206" Y="0.044604400423" Z="1.95" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.95" />
                  <Point X="-3.546663603414" Y="0.02289819302" Z="1.95" />
                  <Point X="-3.539556741714" Y="0" Z="1.95" />
                  <Point X="-3.541700997721" Y="-0.006908758041" Z="1.95" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.95" />
                  <Point X="-3.555240516917" Y="-0.034271582711" Z="1.95" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.95" />
                  <Point X="-4.716414572531" Y="-0.354491951838" Z="1.95" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.95" />
                  <Point X="-5.042325514938" Y="-0.572507880574" Z="1.95" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.95" />
                  <Point X="-4.951195173368" Y="-1.11286118596" Z="1.95" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.95" />
                  <Point X="-3.596443113962" Y="-1.356533709091" Z="1.95" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.95" />
                  <Point X="-3.21302066354" Y="-1.310475999677" Z="1.95" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.95" />
                  <Point X="-3.194462080547" Y="-1.329344750772" Z="1.95" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.95" />
                  <Point X="-4.200998063939" Y="-2.119997767452" Z="1.95" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.95" />
                  <Point X="-4.434861559141" Y="-2.465746808069" Z="1.95" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.95" />
                  <Point X="-4.128899133894" Y="-2.949589373668" Z="1.95" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.95" />
                  <Point X="-2.871700940424" Y="-2.728038702149" Z="1.95" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.95" />
                  <Point X="-2.568818396816" Y="-2.559512072315" Z="1.95" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.95" />
                  <Point X="-3.127378311019" Y="-3.563377442068" Z="1.95" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.95" />
                  <Point X="-3.205022184632" Y="-3.935311693617" Z="1.95" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.95" />
                  <Point X="-2.788639653383" Y="-4.240556389916" Z="1.95" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.95" />
                  <Point X="-2.278349326987" Y="-4.224385449772" Z="1.95" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.95" />
                  <Point X="-2.166429961546" Y="-4.116500172482" Z="1.95" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.95" />
                  <Point X="-1.896498355058" Y="-3.988787617184" Z="1.95" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.95" />
                  <Point X="-1.604600839234" Y="-4.051790617324" Z="1.95" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.95" />
                  <Point X="-1.411376719209" Y="-4.279470292769" Z="1.95" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.95" />
                  <Point X="-1.40192233299" Y="-4.794607688595" Z="1.95" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.95" />
                  <Point X="-1.34456132962" Y="-4.897137409925" Z="1.95" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.95" />
                  <Point X="-1.047928512888" Y="-4.969068589634" Z="1.95" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.95" />
                  <Point X="-0.509934897067" Y="-3.865286993701" Z="1.95" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.95" />
                  <Point X="-0.379137408243" Y="-3.464095269154" Z="1.95" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.95" />
                  <Point X="-0.194634637089" Y="-3.264646777102" Z="1.95" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.95" />
                  <Point X="0.058724442272" Y="-3.222465268186" Z="1.95" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.95" />
                  <Point X="0.291308451189" Y="-3.337550486614" Z="1.95" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.95" />
                  <Point X="0.724819963334" Y="-4.667249036115" Z="1.95" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.95" />
                  <Point X="0.859468389945" Y="-5.006169360197" Z="1.95" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.95" />
                  <Point X="1.03950932071" Y="-4.971904784631" Z="1.95" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.95" />
                  <Point X="1.008270275095" Y="-3.659723563098" Z="1.95" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.95" />
                  <Point X="0.969819032307" Y="-3.215526966793" Z="1.95" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.95" />
                  <Point X="1.052877267668" Y="-2.990639486837" Z="1.95" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.95" />
                  <Point X="1.245169392534" Y="-2.870703951172" Z="1.95" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.95" />
                  <Point X="1.473628943371" Y="-2.885985252117" Z="1.95" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.95" />
                  <Point X="2.424539841496" Y="-4.017125756342" Z="1.95" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.95" />
                  <Point X="2.707297037713" Y="-4.297360929065" Z="1.95" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.95" />
                  <Point X="2.901136053335" Y="-4.168954059871" Z="1.95" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.95" />
                  <Point X="2.450932976996" Y="-3.033541233188" Z="1.95" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.95" />
                  <Point X="2.262191365649" Y="-2.672212433606" Z="1.95" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.95" />
                  <Point X="2.254109801309" Y="-2.464598883077" Z="1.95" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.95" />
                  <Point X="2.368299495295" Y="-2.304791508658" Z="1.95" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.95" />
                  <Point X="2.556294369907" Y="-2.241256643842" Z="1.95" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.95" />
                  <Point X="3.753872363919" Y="-2.866816523279" Z="1.95" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.95" />
                  <Point X="4.105586052545" Y="-2.989008777091" Z="1.95" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.95" />
                  <Point X="4.276688535779" Y="-2.73860269695" Z="1.95" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.95" />
                  <Point X="3.472381615369" Y="-1.829167448834" Z="1.95" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.95" />
                  <Point X="3.16945314947" Y="-1.578367470033" Z="1.95" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.95" />
                  <Point X="3.095908216945" Y="-1.418683690494" Z="1.95" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.95" />
                  <Point X="3.133428316935" Y="-1.25677958583" Z="1.95" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.95" />
                  <Point X="3.259819136546" Y="-1.146237136722" Z="1.95" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.95" />
                  <Point X="4.557545233624" Y="-1.268406309207" Z="1.95" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.95" />
                  <Point X="4.926576599548" Y="-1.228655993326" Z="1.95" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.95" />
                  <Point X="5.004551938392" Y="-0.857443378549" Z="1.95" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.95" />
                  <Point X="4.049285656065" Y="-0.301552555492" Z="1.95" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.95" />
                  <Point X="3.726510415904" Y="-0.208416612523" Z="1.95" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.95" />
                  <Point X="3.642577344437" Y="-0.15094470236" Z="1.95" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.95" />
                  <Point X="3.595648266958" Y="-0.074217808696" Z="1.95" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.95" />
                  <Point X="3.585723183466" Y="0.022392722494" Z="1.95" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.95" />
                  <Point X="3.612802093963" Y="0.113004036215" Z="1.95" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.95" />
                  <Point X="3.676884998448" Y="0.179732231719" Z="1.95" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.95" />
                  <Point X="4.746682161241" Y="0.488419377277" Z="1.95" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.95" />
                  <Point X="5.032740070489" Y="0.667270430267" Z="1.95" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.95" />
                  <Point X="4.958626865393" Y="1.088914086368" Z="1.95" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.95" />
                  <Point X="3.791712483185" Y="1.265283931372" Z="1.95" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.95" />
                  <Point X="3.44129649993" Y="1.224908511428" Z="1.95" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.95" />
                  <Point X="3.352818740217" Y="1.243555058916" Z="1.95" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.95" />
                  <Point X="3.288179605708" Y="1.290601739772" Z="1.95" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.95" />
                  <Point X="3.24716562273" Y="1.366564670283" Z="1.95" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.95" />
                  <Point X="3.238580929416" Y="1.450188302024" Z="1.95" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.95" />
                  <Point X="3.268508937489" Y="1.526785863353" Z="1.95" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.95" />
                  <Point X="4.184373549201" Y="2.25340166401" Z="1.95" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.95" />
                  <Point X="4.398839490724" Y="2.535262463735" Z="1.95" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.95" />
                  <Point X="4.183500970876" Y="2.876744365502" Z="1.95" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.95" />
                  <Point X="2.855787661156" Y="2.466709838545" Z="1.95" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.95" />
                  <Point X="2.491268892552" Y="2.262022471441" Z="1.95" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.95" />
                  <Point X="2.413500120185" Y="2.247469515944" Z="1.95" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.95" />
                  <Point X="2.345492875341" Y="2.263857375912" Z="1.95" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.95" />
                  <Point X="2.286901224323" Y="2.311531985041" Z="1.95" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.95" />
                  <Point X="2.251960186815" Y="2.376258323647" Z="1.95" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.95" />
                  <Point X="2.250505471217" Y="2.44820068333" Z="1.95" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.95" />
                  <Point X="2.928915514314" Y="3.656351420799" Z="1.95" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.95" />
                  <Point X="3.041677948918" Y="4.064094228197" Z="1.95" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.95" />
                  <Point X="2.661309419729" Y="4.322741188011" Z="1.95" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.95" />
                  <Point X="2.26033017559" Y="4.545384011994" Z="1.95" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.95" />
                  <Point X="1.844991638984" Y="4.729228840577" Z="1.95" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.95" />
                  <Point X="1.365109731919" Y="4.884896649781" Z="1.95" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.95" />
                  <Point X="0.707422729317" Y="5.022474845694" Z="1.95" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.95" />
                  <Point X="0.044790891757" Y="4.522286365857" Z="1.95" />
                  <Point X="0" Y="4.355124473572" Z="1.95" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>