<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#161" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1744" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.73698248291" Y="-29.160708984375" />
                  <Point X="0.563301940918" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.54236315918" Y="-28.467375" />
                  <Point X="0.471375518799" Y="-28.365095703125" />
                  <Point X="0.378635375977" Y="-28.231474609375" />
                  <Point X="0.35674887085" Y="-28.209017578125" />
                  <Point X="0.330493835449" Y="-28.189775390625" />
                  <Point X="0.302494567871" Y="-28.175669921875" />
                  <Point X="0.192645370483" Y="-28.141578125" />
                  <Point X="0.049135429382" Y="-28.097037109375" />
                  <Point X="0.020983165741" Y="-28.092767578125" />
                  <Point X="-0.008658161163" Y="-28.092765625" />
                  <Point X="-0.036824390411" Y="-28.09703515625" />
                  <Point X="-0.146673446655" Y="-28.13112890625" />
                  <Point X="-0.290183532715" Y="-28.17566796875" />
                  <Point X="-0.318184448242" Y="-28.189775390625" />
                  <Point X="-0.344439941406" Y="-28.20901953125" />
                  <Point X="-0.366323883057" Y="-28.2314765625" />
                  <Point X="-0.437311401367" Y="-28.3337578125" />
                  <Point X="-0.530051513672" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.777890808105" Y="-29.359330078125" />
                  <Point X="-0.916584838867" Y="-29.87694140625" />
                  <Point X="-1.079318481445" Y="-29.845353515625" />
                  <Point X="-1.203320800781" Y="-29.81344921875" />
                  <Point X="-1.246417602539" Y="-29.802361328125" />
                  <Point X="-1.235870117188" Y="-29.722244140625" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.242751586914" Y="-29.38429296875" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131205078125" />
                  <Point X="-1.424779907227" Y="-29.04251171875" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886108398" Y="-28.897994140625" />
                  <Point X="-1.643027954102" Y="-28.890966796875" />
                  <Point X="-1.777256835938" Y="-28.882169921875" />
                  <Point X="-1.952616943359" Y="-28.87067578125" />
                  <Point X="-1.9834140625" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042657226562" Y="-28.89480078125" />
                  <Point X="-2.15450390625" Y="-28.969533203125" />
                  <Point X="-2.300623535156" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102539062" Y="-29.099462890625" />
                  <Point X="-2.462489746094" Y="-29.2654765625" />
                  <Point X="-2.480147460938" Y="-29.28848828125" />
                  <Point X="-2.559121826172" Y="-29.23958984375" />
                  <Point X="-2.801725585938" Y="-29.089376953125" />
                  <Point X="-2.973384033203" Y="-28.957205078125" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.746526611328" Y="-28.235666015625" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406587890625" Y="-27.616125" />
                  <Point X="-2.405575683594" Y="-27.585189453125" />
                  <Point X="-2.414561279297" Y="-27.5555703125" />
                  <Point X="-2.428779052734" Y="-27.5267421875" />
                  <Point X="-2.446807128906" Y="-27.5015859375" />
                  <Point X="-2.464155761719" Y="-27.48423828125" />
                  <Point X="-2.489311767578" Y="-27.466212890625" />
                  <Point X="-2.518140625" Y="-27.45199609375" />
                  <Point X="-2.547758789062" Y="-27.44301171875" />
                  <Point X="-2.578693359375" Y="-27.444025390625" />
                  <Point X="-2.610218994141" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.369536865234" Y="-27.882869140625" />
                  <Point X="-3.818022705078" Y="-28.141802734375" />
                  <Point X="-3.891202880859" Y="-28.045658203125" />
                  <Point X="-4.08286328125" Y="-27.79385546875" />
                  <Point X="-4.205935546875" Y="-27.587482421875" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.670689208984" Y="-26.9318515625" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.35965625" />
                  <Point X="-3.045556884766" Y="-26.327984375" />
                  <Point X="-3.052558349609" Y="-26.298240234375" />
                  <Point X="-3.068641113281" Y="-26.272255859375" />
                  <Point X="-3.089474609375" Y="-26.248298828125" />
                  <Point X="-3.112975097656" Y="-26.228765625" />
                  <Point X="-3.139457763672" Y="-26.2131796875" />
                  <Point X="-3.168722167969" Y="-26.201955078125" />
                  <Point X="-3.200608642578" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-4.153931152344" Y="-26.315767578125" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.759118652344" Y="-26.286115234375" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.866640136719" Y="-25.76498046875" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.176193847656" Y="-25.392787109375" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.510781982422" Y="-25.21138671875" />
                  <Point X="-3.483181640625" Y="-25.195814453125" />
                  <Point X="-3.469872802734" Y="-25.186736328125" />
                  <Point X="-3.442333984375" Y="-25.16430859375" />
                  <Point X="-3.426107910156" Y="-25.14735546875" />
                  <Point X="-3.414532226562" Y="-25.126943359375" />
                  <Point X="-3.403095458984" Y="-25.09928125" />
                  <Point X="-3.398431152344" Y="-25.084818359375" />
                  <Point X="-3.390944091797" Y="-25.053115234375" />
                  <Point X="-3.388402099609" Y="-25.031759765625" />
                  <Point X="-3.390729003906" Y="-25.01037890625" />
                  <Point X="-3.396986816406" Y="-24.982634765625" />
                  <Point X="-3.401469726562" Y="-24.96821484375" />
                  <Point X="-3.41413671875" Y="-24.93658984375" />
                  <Point X="-3.425486572266" Y="-24.916048828125" />
                  <Point X="-3.441525390625" Y="-24.898916015625" />
                  <Point X="-3.465372802734" Y="-24.879052734375" />
                  <Point X="-3.478537109375" Y="-24.86985546875" />
                  <Point X="-3.509827880859" Y="-24.851720703125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.37332421875" Y="-24.616953125" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.872797363281" Y="-24.34949609375" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.758939941406" Y="-23.781134765625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.2268125" Y="-23.63949609375" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985351562" Y="-23.700658203125" />
                  <Point X="-3.723423339844" Y="-23.698771484375" />
                  <Point X="-3.703139648438" Y="-23.694736328125" />
                  <Point X="-3.676507080078" Y="-23.68633984375" />
                  <Point X="-3.641713623047" Y="-23.675369140625" />
                  <Point X="-3.622783691406" Y="-23.667041015625" />
                  <Point X="-3.604039550781" Y="-23.656220703125" />
                  <Point X="-3.587354492188" Y="-23.64398828125" />
                  <Point X="-3.573713378906" Y="-23.62843359375" />
                  <Point X="-3.561299072266" Y="-23.610703125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.540665039062" Y="-23.566771484375" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532050292969" Y="-23.41062109375" />
                  <Point X="-3.544944580078" Y="-23.3858515625" />
                  <Point X="-3.561790039062" Y="-23.3534921875" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-4.084219482422" Y="-22.935494140625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.268864746094" Y="-22.587935546875" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.907520751953" Y="-22.043162109375" />
                  <Point X="-3.750503417969" Y="-21.841337890625" />
                  <Point X="-3.489879882812" Y="-21.99180859375" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187730224609" Y="-22.163658203125" />
                  <Point X="-3.167088867188" Y="-22.17016796875" />
                  <Point X="-3.14679296875" Y="-22.174205078125" />
                  <Point X="-3.109701171875" Y="-22.17744921875" />
                  <Point X="-3.061243652344" Y="-22.181689453125" />
                  <Point X="-3.040561767578" Y="-22.18123828125" />
                  <Point X="-3.019102783203" Y="-22.178412109375" />
                  <Point X="-2.999013916016" Y="-22.17349609375" />
                  <Point X="-2.980465087891" Y="-22.16434765625" />
                  <Point X="-2.962210693359" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.919749755859" Y="-22.113443359375" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.846680908203" Y="-21.9267890625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.083420654297" Y="-21.44845703125" />
                  <Point X="-3.183333496094" Y="-21.275404296875" />
                  <Point X="-3.0275546875" Y="-21.15596875" />
                  <Point X="-2.70062890625" Y="-20.905318359375" />
                  <Point X="-2.427162109375" Y="-20.75338671875" />
                  <Point X="-2.167036621094" Y="-20.608865234375" />
                  <Point X="-2.129898681641" Y="-20.657265625" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028891479492" Y="-20.78519921875" />
                  <Point X="-2.012311645508" Y="-20.799111328125" />
                  <Point X="-1.995115722656" Y="-20.8106015625" />
                  <Point X="-1.953832885742" Y="-20.83209375" />
                  <Point X="-1.899899780273" Y="-20.860169921875" />
                  <Point X="-1.880625488281" Y="-20.86766796875" />
                  <Point X="-1.859719238281" Y="-20.873271484375" />
                  <Point X="-1.839269042969" Y="-20.876419921875" />
                  <Point X="-1.818622070312" Y="-20.87506640625" />
                  <Point X="-1.797307128906" Y="-20.871306640625" />
                  <Point X="-1.777451660156" Y="-20.865517578125" />
                  <Point X="-1.734452636719" Y="-20.847705078125" />
                  <Point X="-1.678277587891" Y="-20.8244375" />
                  <Point X="-1.660148193359" Y="-20.814490234375" />
                  <Point X="-1.64241796875" Y="-20.802076171875" />
                  <Point X="-1.626865234375" Y="-20.7884375" />
                  <Point X="-1.614633422852" Y="-20.771755859375" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734080078125" />
                  <Point X="-1.581484863281" Y="-20.689693359375" />
                  <Point X="-1.563201171875" Y="-20.631703125" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.582017089844" Y="-20.384697265625" />
                  <Point X="-1.584201660156" Y="-20.368103515625" />
                  <Point X="-1.372865356445" Y="-20.3088515625" />
                  <Point X="-0.94962322998" Y="-20.19019140625" />
                  <Point X="-0.618119873047" Y="-20.151392578125" />
                  <Point X="-0.294711151123" Y="-20.113541015625" />
                  <Point X="-0.217688583374" Y="-20.400994140625" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113998413" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907631" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425949097" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.255674667358" Y="-20.305177734375" />
                  <Point X="0.307419555664" Y="-20.1120625" />
                  <Point X="0.474492675781" Y="-20.129560546875" />
                  <Point X="0.844044921875" Y="-20.16826171875" />
                  <Point X="1.118317382812" Y="-20.23448046875" />
                  <Point X="1.481025024414" Y="-20.322048828125" />
                  <Point X="1.658715209961" Y="-20.386498046875" />
                  <Point X="1.894646972656" Y="-20.472072265625" />
                  <Point X="2.067275634766" Y="-20.5528046875" />
                  <Point X="2.294559082031" Y="-20.659099609375" />
                  <Point X="2.461372558594" Y="-20.756283203125" />
                  <Point X="2.680988525391" Y="-20.884232421875" />
                  <Point X="2.838264648438" Y="-20.996078125" />
                  <Point X="2.943260498047" Y="-21.07074609375" />
                  <Point X="2.521157714844" Y="-21.801849609375" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.123768066406" Y="-22.518755859375" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.111357666016" Y="-22.6491484375" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140070800781" Y="-22.75252734375" />
                  <Point X="2.158696044922" Y="-22.7799765625" />
                  <Point X="2.183028564453" Y="-22.8158359375" />
                  <Point X="2.194464355469" Y="-22.829669921875" />
                  <Point X="2.221597900391" Y="-22.85440625" />
                  <Point X="2.249046630859" Y="-22.87303125" />
                  <Point X="2.284906494141" Y="-22.897365234375" />
                  <Point X="2.304946533203" Y="-22.9077265625" />
                  <Point X="2.327035644531" Y="-22.915994140625" />
                  <Point X="2.348965576172" Y="-22.921337890625" />
                  <Point X="2.379066162109" Y="-22.924966796875" />
                  <Point X="2.418390380859" Y="-22.929708984375" />
                  <Point X="2.436467773438" Y="-22.93015625" />
                  <Point X="2.473207519531" Y="-22.925830078125" />
                  <Point X="2.508017333984" Y="-22.916521484375" />
                  <Point X="2.553493896484" Y="-22.904359375" />
                  <Point X="2.565290771484" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.43386328125" Y="-22.401802734375" />
                  <Point X="3.967325927734" Y="-22.09380859375" />
                  <Point X="3.993008789062" Y="-22.129501953125" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.210957519531" Y="-22.455439453125" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.722632324219" Y="-22.954138671875" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221420410156" Y="-23.339763671875" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.178921142578" Y="-23.391056640625" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.136607421875" Y="-23.4490859375" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.112297607422" Y="-23.516283203125" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.105400146484" Y="-23.665359375" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120680419922" Y="-23.7310234375" />
                  <Point X="3.136282958984" Y="-23.764259765625" />
                  <Point X="3.157119384766" Y="-23.7959296875" />
                  <Point X="3.184340576172" Y="-23.8373046875" />
                  <Point X="3.198889648438" Y="-23.854544921875" />
                  <Point X="3.216133789062" Y="-23.87063671875" />
                  <Point X="3.234345214844" Y="-23.88396484375" />
                  <Point X="3.264539794922" Y="-23.900962890625" />
                  <Point X="3.303987304688" Y="-23.92316796875" />
                  <Point X="3.320529296875" Y="-23.93050390625" />
                  <Point X="3.356120849609" Y="-23.9405625" />
                  <Point X="3.396946044922" Y="-23.94595703125" />
                  <Point X="3.450281494141" Y="-23.953005859375" />
                  <Point X="3.462697998047" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.291209960938" Y="-23.847298828125" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.789988769531" Y="-23.837376953125" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.873568359375" Y="-24.24466796875" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.280249023438" Y="-24.519375" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704787597656" Y="-24.674416015625" />
                  <Point X="3.681547119141" Y="-24.684931640625" />
                  <Point X="3.6414375" Y="-24.708115234375" />
                  <Point X="3.589037109375" Y="-24.73840234375" />
                  <Point X="3.574314697266" Y="-24.748900390625" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.523464599609" Y="-24.80508984375" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.455658935547" Y="-24.949283203125" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.453200927734" Y="-25.10044140625" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.176662109375" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.516090087891" Y="-25.24807421875" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.559999023438" Y="-25.30123828125" />
                  <Point X="3.589035888672" Y="-25.324158203125" />
                  <Point X="3.629145507812" Y="-25.347341796875" />
                  <Point X="3.681545898438" Y="-25.377630859375" />
                  <Point X="3.692709228516" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.452973632812" Y="-25.589466796875" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.886261230469" Y="-25.74152734375" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.81962109375" Y="-26.103861328125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.078079833984" Y="-26.089501953125" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659423828" Y="-26.005509765625" />
                  <Point X="3.295938476562" Y="-26.022619140625" />
                  <Point X="3.193095214844" Y="-26.04497265625" />
                  <Point X="3.163973144531" Y="-26.05659765625" />
                  <Point X="3.136146728516" Y="-26.073490234375" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.064815185547" Y="-26.1511875" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.962937988281" Y="-26.3794765625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450927734" Y="-26.567998046875" />
                  <Point X="3.020016357422" Y="-26.635759765625" />
                  <Point X="3.076931152344" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.794005615234" Y="-27.28528515625" />
                  <Point X="4.213122070312" Y="-27.6068828125" />
                  <Point X="4.212868652344" Y="-27.60729296875" />
                  <Point X="4.124813964844" Y="-27.74978125" />
                  <Point X="4.051597167969" Y="-27.853810546875" />
                  <Point X="4.028980224609" Y="-27.8859453125" />
                  <Point X="3.383160644531" Y="-27.51308203125" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224121094" Y="-27.159826171875" />
                  <Point X="2.660533691406" Y="-27.14290625" />
                  <Point X="2.538133789062" Y="-27.12080078125" />
                  <Point X="2.506784179688" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.366999755859" Y="-27.176140625" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.163568359375" Y="-27.3682734375" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.112595947266" Y="-27.65694921875" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.590957763672" Y="-28.586689453125" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.781836425781" Y="-29.111654296875" />
                  <Point X="2.701764404297" Y="-29.163482421875" />
                  <Point X="2.202823242188" Y="-28.51325" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747507202148" Y="-27.92218359375" />
                  <Point X="1.721923461914" Y="-27.900556640625" />
                  <Point X="1.629519287109" Y="-27.841150390625" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.751166015625" />
                  <Point X="1.448365478516" Y="-27.743435546875" />
                  <Point X="1.417100708008" Y="-27.741119140625" />
                  <Point X="1.316040893555" Y="-27.75041796875" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.156362792969" Y="-27.7693984375" />
                  <Point X="1.128977050781" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.795462890625" />
                  <Point X="1.026559448242" Y="-27.86034765625" />
                  <Point X="0.92461151123" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.852292053223" Y="-28.13315625" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.944190795898" Y="-29.268400390625" />
                  <Point X="1.022065429688" Y="-29.859916015625" />
                  <Point X="0.975697937012" Y="-29.870078125" />
                  <Point X="0.929315795898" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058414794922" Y="-29.75263671875" />
                  <Point X="-1.14124597168" Y="-29.731326171875" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.541037109375" />
                  <Point X="-1.121759155273" Y="-29.50933203125" />
                  <Point X="-1.123333740234" Y="-29.497693359375" />
                  <Point X="-1.149576904297" Y="-29.365759765625" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188124633789" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230575561523" Y="-29.094859375" />
                  <Point X="-1.250210327148" Y="-29.070935546875" />
                  <Point X="-1.261006958008" Y="-29.05978125" />
                  <Point X="-1.362141967773" Y="-28.971087890625" />
                  <Point X="-1.494267578125" Y="-28.855216796875" />
                  <Point X="-1.506735717773" Y="-28.84596875" />
                  <Point X="-1.53301940918" Y="-28.829623046875" />
                  <Point X="-1.546834472656" Y="-28.822525390625" />
                  <Point X="-1.576531494141" Y="-28.810224609375" />
                  <Point X="-1.591316040039" Y="-28.805474609375" />
                  <Point X="-1.621457885742" Y="-28.798447265625" />
                  <Point X="-1.636815307617" Y="-28.796169921875" />
                  <Point X="-1.771044189453" Y="-28.787373046875" />
                  <Point X="-1.946404296875" Y="-28.77587890625" />
                  <Point X="-1.961928344727" Y="-28.7761328125" />
                  <Point X="-1.992725585938" Y="-28.779166015625" />
                  <Point X="-2.007998535156" Y="-28.7819453125" />
                  <Point X="-2.039047607422" Y="-28.790263671875" />
                  <Point X="-2.053667480469" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095435791016" Y="-28.815810546875" />
                  <Point X="-2.207282470703" Y="-28.89054296875" />
                  <Point X="-2.353402099609" Y="-28.988177734375" />
                  <Point X="-2.359681396484" Y="-28.992755859375" />
                  <Point X="-2.380451416016" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410470947266" Y="-29.041630859375" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.509111083984" Y="-29.158818359375" />
                  <Point X="-2.747611328125" Y="-29.011146484375" />
                  <Point X="-2.915426513672" Y="-28.88193359375" />
                  <Point X="-2.98086328125" Y="-28.83155078125" />
                  <Point X="-2.664254150391" Y="-28.283166015625" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413085938" Y="-27.634658203125" />
                  <Point X="-2.311638671875" Y="-27.619232421875" />
                  <Point X="-2.310626464844" Y="-27.588296875" />
                  <Point X="-2.314666992188" Y="-27.557611328125" />
                  <Point X="-2.323652587891" Y="-27.5279921875" />
                  <Point X="-2.329359863281" Y="-27.513548828125" />
                  <Point X="-2.343577636719" Y="-27.484720703125" />
                  <Point X="-2.351560546875" Y="-27.471404296875" />
                  <Point X="-2.369588623047" Y="-27.446248046875" />
                  <Point X="-2.379633789062" Y="-27.434408203125" />
                  <Point X="-2.396982421875" Y="-27.417060546875" />
                  <Point X="-2.408822753906" Y="-27.407015625" />
                  <Point X="-2.433978759766" Y="-27.388990234375" />
                  <Point X="-2.447294433594" Y="-27.381009765625" />
                  <Point X="-2.476123291016" Y="-27.36679296875" />
                  <Point X="-2.490564208984" Y="-27.3610859375" />
                  <Point X="-2.520182373047" Y="-27.3521015625" />
                  <Point X="-2.550870117188" Y="-27.3480625" />
                  <Point X="-2.5818046875" Y="-27.349076171875" />
                  <Point X="-2.597228759766" Y="-27.3508515625" />
                  <Point X="-2.628754394531" Y="-27.357123046875" />
                  <Point X="-2.643685058594" Y="-27.36138671875" />
                  <Point X="-2.672649658203" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.417036865234" Y="-27.80059765625" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-3.815609375" Y="-27.98812109375" />
                  <Point X="-4.004018066406" Y="-27.74058984375" />
                  <Point X="-4.124342773438" Y="-27.53882421875" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.612856933594" Y="-27.007220703125" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.398802734375" />
                  <Point X="-2.948748535156" Y="-26.368373046875" />
                  <Point X="-2.948577880859" Y="-26.353044921875" />
                  <Point X="-2.950787109375" Y="-26.321373046875" />
                  <Point X="-2.953084228516" Y="-26.306216796875" />
                  <Point X="-2.960085693359" Y="-26.27647265625" />
                  <Point X="-2.971779296875" Y="-26.2482421875" />
                  <Point X="-2.987862060547" Y="-26.2222578125" />
                  <Point X="-2.996955566406" Y="-26.209916015625" />
                  <Point X="-3.0177890625" Y="-26.185958984375" />
                  <Point X="-3.028749755859" Y="-26.175240234375" />
                  <Point X="-3.052250244141" Y="-26.15570703125" />
                  <Point X="-3.064790039063" Y="-26.146892578125" />
                  <Point X="-3.091272705078" Y="-26.131306640625" />
                  <Point X="-3.105436523438" Y="-26.12448046875" />
                  <Point X="-3.134700927734" Y="-26.113255859375" />
                  <Point X="-3.149801513672" Y="-26.108857421875" />
                  <Point X="-3.181687988281" Y="-26.102376953125" />
                  <Point X="-3.197304931641" Y="-26.10053125" />
                  <Point X="-3.228625488281" Y="-26.09944140625" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-4.166331054688" Y="-26.221580078125" />
                  <Point X="-4.660920898438" Y="-26.286693359375" />
                  <Point X="-4.66707421875" Y="-26.262603515625" />
                  <Point X="-4.740762207031" Y="-25.974119140625" />
                  <Point X="-4.772597167969" Y="-25.75153125" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.151605957031" Y="-25.48455078125" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.49695703125" Y="-25.308357421875" />
                  <Point X="-3.474863037109" Y="-25.299333984375" />
                  <Point X="-3.464100097656" Y="-25.294126953125" />
                  <Point X="-3.436499755859" Y="-25.2785546875" />
                  <Point X="-3.429648925781" Y="-25.274294921875" />
                  <Point X="-3.409882080078" Y="-25.2603984375" />
                  <Point X="-3.382343261719" Y="-25.237970703125" />
                  <Point X="-3.373703369141" Y="-25.22999609375" />
                  <Point X="-3.357477294922" Y="-25.21304296875" />
                  <Point X="-3.343471191406" Y="-25.19421875" />
                  <Point X="-3.331895507812" Y="-25.173806640625" />
                  <Point X="-3.326739746094" Y="-25.163240234375" />
                  <Point X="-3.315302978516" Y="-25.135578125" />
                  <Point X="-3.312681152344" Y="-25.128439453125" />
                  <Point X="-3.305974365234" Y="-25.10665234375" />
                  <Point X="-3.298487304688" Y="-25.07494921875" />
                  <Point X="-3.296610107422" Y="-25.06434375" />
                  <Point X="-3.294068115234" Y="-25.04298828125" />
                  <Point X="-3.293959716797" Y="-25.021482421875" />
                  <Point X="-3.296286621094" Y="-25.0001015625" />
                  <Point X="-3.298057128906" Y="-24.9894765625" />
                  <Point X="-3.304314941406" Y="-24.961732421875" />
                  <Point X="-3.30626953125" Y="-24.954431640625" />
                  <Point X="-3.313280761719" Y="-24.932892578125" />
                  <Point X="-3.325947753906" Y="-24.901267578125" />
                  <Point X="-3.330985839844" Y="-24.89064453125" />
                  <Point X="-3.342335693359" Y="-24.870103515625" />
                  <Point X="-3.356133789062" Y="-24.851125" />
                  <Point X="-3.372172607422" Y="-24.8339921875" />
                  <Point X="-3.380725097656" Y="-24.825919921875" />
                  <Point X="-3.404572509766" Y="-24.806056640625" />
                  <Point X="-3.410964355469" Y="-24.80117578125" />
                  <Point X="-3.430901123047" Y="-24.787662109375" />
                  <Point X="-3.462191894531" Y="-24.76952734375" />
                  <Point X="-3.473396728516" Y="-24.763984375" />
                  <Point X="-3.496444824219" Y="-24.7544140625" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.348736328125" Y="-24.525189453125" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.778820800781" Y="-24.36340234375" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.667247070312" Y="-23.805982421875" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.239212402344" Y="-23.73368359375" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057373047" Y="-23.795634765625" />
                  <Point X="-3.736704345703" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704887695313" Y="-23.7919453125" />
                  <Point X="-3.684604003906" Y="-23.78791015625" />
                  <Point X="-3.674574951172" Y="-23.78533984375" />
                  <Point X="-3.647942382812" Y="-23.776943359375" />
                  <Point X="-3.613148925781" Y="-23.76597265625" />
                  <Point X="-3.603457519531" Y="-23.762326171875" />
                  <Point X="-3.584527587891" Y="-23.753998046875" />
                  <Point X="-3.5752890625" Y="-23.74931640625" />
                  <Point X="-3.556544921875" Y="-23.73849609375" />
                  <Point X="-3.547869873047" Y="-23.7328359375" />
                  <Point X="-3.531184814453" Y="-23.720603515625" />
                  <Point X="-3.5159296875" Y="-23.706626953125" />
                  <Point X="-3.502288574219" Y="-23.691072265625" />
                  <Point X="-3.495892578125" Y="-23.682921875" />
                  <Point X="-3.483478271484" Y="-23.66519140625" />
                  <Point X="-3.478009033203" Y="-23.65639453125" />
                  <Point X="-3.468061523438" Y="-23.63826171875" />
                  <Point X="-3.463583251953" Y="-23.62892578125" />
                  <Point X="-3.452896728516" Y="-23.603126953125" />
                  <Point X="-3.438935791016" Y="-23.569421875" />
                  <Point X="-3.435498779297" Y="-23.5596484375" />
                  <Point X="-3.429710205078" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436011962891" Y="-23.395470703125" />
                  <Point X="-3.443508789062" Y="-23.37619140625" />
                  <Point X="-3.447784423828" Y="-23.36675390625" />
                  <Point X="-3.460678710938" Y="-23.341984375" />
                  <Point X="-3.477524169922" Y="-23.309625" />
                  <Point X="-3.482802246094" Y="-23.3007109375" />
                  <Point X="-3.494293945312" Y="-23.283513671875" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521498046875" Y="-23.251091796875" />
                  <Point X="-3.536439941406" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-4.026387207031" Y="-22.860125" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.186818359375" Y="-22.63582421875" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.832540039062" Y="-22.10149609375" />
                  <Point X="-3.726337646484" Y="-21.96498828125" />
                  <Point X="-3.537379882812" Y="-22.07408203125" />
                  <Point X="-3.254156982422" Y="-22.2376015625" />
                  <Point X="-3.244926025391" Y="-22.242279296875" />
                  <Point X="-3.225999267578" Y="-22.250609375" />
                  <Point X="-3.216303466797" Y="-22.254259765625" />
                  <Point X="-3.195662109375" Y="-22.26076953125" />
                  <Point X="-3.185622558594" Y="-22.263341796875" />
                  <Point X="-3.165326660156" Y="-22.26737890625" />
                  <Point X="-3.1550703125" Y="-22.26884375" />
                  <Point X="-3.117978515625" Y="-22.272087890625" />
                  <Point X="-3.069520996094" Y="-22.276328125" />
                  <Point X="-3.059171630859" Y="-22.276666015625" />
                  <Point X="-3.038489746094" Y="-22.27621484375" />
                  <Point X="-3.028157226562" Y="-22.27542578125" />
                  <Point X="-3.006698242188" Y="-22.272599609375" />
                  <Point X="-2.996521240234" Y="-22.270689453125" />
                  <Point X="-2.976432373047" Y="-22.2657734375" />
                  <Point X="-2.9569921875" Y="-22.258697265625" />
                  <Point X="-2.938443359375" Y="-22.249548828125" />
                  <Point X="-2.929422851563" Y="-22.244470703125" />
                  <Point X="-2.911168457031" Y="-22.232841796875" />
                  <Point X="-2.902749755859" Y="-22.22680859375" />
                  <Point X="-2.886616943359" Y="-22.213861328125" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.852574707031" Y="-22.180619140625" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.752042480469" Y="-21.918509765625" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.001148193359" Y="-21.40095703125" />
                  <Point X="-3.059388183594" Y="-21.300083984375" />
                  <Point X="-2.969752197266" Y="-21.231359375" />
                  <Point X="-2.648384521484" Y="-20.984970703125" />
                  <Point X="-2.381024658203" Y="-20.836431640625" />
                  <Point X="-2.192525146484" Y="-20.731705078125" />
                  <Point X="-2.118564697266" Y="-20.82808984375" />
                  <Point X="-2.111818359375" Y="-20.835953125" />
                  <Point X="-2.097514404297" Y="-20.85089453125" />
                  <Point X="-2.089956298828" Y="-20.85797265625" />
                  <Point X="-2.073376464844" Y="-20.871884765625" />
                  <Point X="-2.065091796875" Y="-20.878099609375" />
                  <Point X="-2.047895751953" Y="-20.88958984375" />
                  <Point X="-2.038984619141" Y="-20.894865234375" />
                  <Point X="-1.997701782227" Y="-20.916357421875" />
                  <Point X="-1.943768676758" Y="-20.94443359375" />
                  <Point X="-1.934342163086" Y="-20.94870703125" />
                  <Point X="-1.915067871094" Y="-20.956205078125" />
                  <Point X="-1.905220092773" Y="-20.9594296875" />
                  <Point X="-1.884313964844" Y="-20.965033203125" />
                  <Point X="-1.874174804688" Y="-20.967166015625" />
                  <Point X="-1.853724487305" Y="-20.970314453125" />
                  <Point X="-1.8330546875" Y="-20.971216796875" />
                  <Point X="-1.812407714844" Y="-20.96986328125" />
                  <Point X="-1.802119750977" Y="-20.968623046875" />
                  <Point X="-1.7808046875" Y="-20.96486328125" />
                  <Point X="-1.770716064453" Y="-20.962509765625" />
                  <Point X="-1.750860595703" Y="-20.956720703125" />
                  <Point X="-1.74109375" Y="-20.95328515625" />
                  <Point X="-1.698094726562" Y="-20.93547265625" />
                  <Point X="-1.641919677734" Y="-20.912205078125" />
                  <Point X="-1.632579589844" Y="-20.907724609375" />
                  <Point X="-1.614450195312" Y="-20.89777734375" />
                  <Point X="-1.605660766602" Y="-20.892310546875" />
                  <Point X="-1.587930664062" Y="-20.879896484375" />
                  <Point X="-1.579781860352" Y="-20.873501953125" />
                  <Point X="-1.564229248047" Y="-20.85986328125" />
                  <Point X="-1.550253662109" Y="-20.84461328125" />
                  <Point X="-1.538021850586" Y="-20.827931640625" />
                  <Point X="-1.532361450195" Y="-20.819255859375" />
                  <Point X="-1.521539306641" Y="-20.80051171875" />
                  <Point X="-1.516857910156" Y="-20.791275390625" />
                  <Point X="-1.508527099609" Y="-20.77234375" />
                  <Point X="-1.504877563477" Y="-20.7626484375" />
                  <Point X="-1.490882080078" Y="-20.71826171875" />
                  <Point X="-1.472598388672" Y="-20.660271484375" />
                  <Point X="-1.470026489258" Y="-20.650236328125" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-1.347219238281" Y="-20.40032421875" />
                  <Point X="-0.931163818359" Y="-20.2836796875" />
                  <Point X="-0.607076538086" Y="-20.245748046875" />
                  <Point X="-0.365222381592" Y="-20.21744140625" />
                  <Point X="-0.309451599121" Y="-20.42558203125" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.347437683105" Y="-20.329765625" />
                  <Point X="0.378190704346" Y="-20.214994140625" />
                  <Point X="0.464597137451" Y="-20.22404296875" />
                  <Point X="0.827880065918" Y="-20.262087890625" />
                  <Point X="1.096021728516" Y="-20.326826171875" />
                  <Point X="1.453592041016" Y="-20.413154296875" />
                  <Point X="1.626322998047" Y="-20.4758046875" />
                  <Point X="1.858248657227" Y="-20.55992578125" />
                  <Point X="2.027031005859" Y="-20.638859375" />
                  <Point X="2.250436035156" Y="-20.74333984375" />
                  <Point X="2.413550537109" Y="-20.838369140625" />
                  <Point X="2.629442626953" Y="-20.9641484375" />
                  <Point X="2.783208251953" Y="-21.073498046875" />
                  <Point X="2.817780273438" Y="-21.098083984375" />
                  <Point X="2.438885253906" Y="-21.754349609375" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301269531" Y="-22.459404296875" />
                  <Point X="2.031992675781" Y="-22.49421484375" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017912231445" Y="-22.5485390625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.017040893555" Y="-22.660521484375" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144287109" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.045320922852" Y="-22.7761171875" />
                  <Point X="2.055682373047" Y="-22.796158203125" />
                  <Point X="2.061459228516" Y="-22.8058671875" />
                  <Point X="2.080084472656" Y="-22.83331640625" />
                  <Point X="2.104416992188" Y="-22.86917578125" />
                  <Point X="2.109807373047" Y="-22.87636328125" />
                  <Point X="2.130462158203" Y="-22.899875" />
                  <Point X="2.157595703125" Y="-22.924611328125" />
                  <Point X="2.168257080078" Y="-22.933017578125" />
                  <Point X="2.195705810547" Y="-22.951642578125" />
                  <Point X="2.231565673828" Y="-22.9759765625" />
                  <Point X="2.241275390625" Y="-22.98175390625" />
                  <Point X="2.261315429688" Y="-22.992115234375" />
                  <Point X="2.271645751953" Y="-22.99669921875" />
                  <Point X="2.293734863281" Y="-23.004966796875" />
                  <Point X="2.304544677734" Y="-23.00829296875" />
                  <Point X="2.326474609375" Y="-23.01363671875" />
                  <Point X="2.337594726562" Y="-23.015654296875" />
                  <Point X="2.3676953125" Y="-23.019283203125" />
                  <Point X="2.40701953125" Y="-23.024025390625" />
                  <Point X="2.416040527344" Y="-23.0246796875" />
                  <Point X="2.447577392578" Y="-23.02450390625" />
                  <Point X="2.484317138672" Y="-23.020177734375" />
                  <Point X="2.497749511719" Y="-23.01760546875" />
                  <Point X="2.532559326172" Y="-23.008296875" />
                  <Point X="2.578035888672" Y="-22.996134765625" />
                  <Point X="2.583999755859" Y="-22.994328125" />
                  <Point X="2.604419189453" Y="-22.986927734375" />
                  <Point X="2.627662353516" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.48136328125" Y="-22.48407421875" />
                  <Point X="3.940403320312" Y="-22.219046875" />
                  <Point X="4.043959716797" Y="-22.36296875" />
                  <Point X="4.129680664062" Y="-22.504623046875" />
                  <Point X="4.136884765625" Y="-22.51652734375" />
                  <Point X="3.664800292969" Y="-22.87876953125" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168130859375" Y="-23.26013671875" />
                  <Point X="3.152114746094" Y="-23.2747890625" />
                  <Point X="3.134668212891" Y="-23.2933984375" />
                  <Point X="3.128576171875" Y="-23.300578125" />
                  <Point X="3.1035234375" Y="-23.33326171875" />
                  <Point X="3.070793945312" Y="-23.375958984375" />
                  <Point X="3.065635742188" Y="-23.3833984375" />
                  <Point X="3.049740966797" Y="-23.410625" />
                  <Point X="3.034763427734" Y="-23.444453125" />
                  <Point X="3.030140380859" Y="-23.457328125" />
                  <Point X="3.020808105469" Y="-23.490697265625" />
                  <Point X="3.008616455078" Y="-23.53429296875" />
                  <Point X="3.006225341797" Y="-23.545337890625" />
                  <Point X="3.002771728516" Y="-23.56763671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.012360107422" Y="-23.684556640625" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024598388672" Y="-23.741767578125" />
                  <Point X="3.034684814453" Y="-23.771392578125" />
                  <Point X="3.050287353516" Y="-23.80462890625" />
                  <Point X="3.056919433594" Y="-23.816474609375" />
                  <Point X="3.077755859375" Y="-23.84814453125" />
                  <Point X="3.104977050781" Y="-23.88951953125" />
                  <Point X="3.11173828125" Y="-23.89857421875" />
                  <Point X="3.126287353516" Y="-23.915814453125" />
                  <Point X="3.134075195312" Y="-23.924" />
                  <Point X="3.151319335938" Y="-23.940091796875" />
                  <Point X="3.160028076172" Y="-23.947298828125" />
                  <Point X="3.178239501953" Y="-23.960626953125" />
                  <Point X="3.1877421875" Y="-23.966748046875" />
                  <Point X="3.217936767578" Y="-23.98374609375" />
                  <Point X="3.257384277344" Y="-24.005951171875" />
                  <Point X="3.265474609375" Y="-24.01001171875" />
                  <Point X="3.294693115234" Y="-24.021923828125" />
                  <Point X="3.330284667969" Y="-24.031982421875" />
                  <Point X="3.343676025391" Y="-24.034744140625" />
                  <Point X="3.384501220703" Y="-24.040138671875" />
                  <Point X="3.437836669922" Y="-24.0471875" />
                  <Point X="3.444033691406" Y="-24.04780078125" />
                  <Point X="3.465708251953" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.303609863281" Y="-23.941486328125" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.77969921875" Y="-24.259283203125" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="4.255661132813" Y="-24.427611328125" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686022949219" Y="-24.580458984375" />
                  <Point X="3.665625244141" Y="-24.58786328125" />
                  <Point X="3.642384765625" Y="-24.59837890625" />
                  <Point X="3.634006591797" Y="-24.602681640625" />
                  <Point X="3.593896972656" Y="-24.625865234375" />
                  <Point X="3.541496582031" Y="-24.65615234375" />
                  <Point X="3.533882080078" Y="-24.661052734375" />
                  <Point X="3.508778076172" Y="-24.680125" />
                  <Point X="3.481993652344" Y="-24.7056484375" />
                  <Point X="3.472795654297" Y="-24.715775390625" />
                  <Point X="3.448729980469" Y="-24.74644140625" />
                  <Point X="3.417289794922" Y="-24.78650390625" />
                  <Point X="3.410854736328" Y="-24.795791015625" />
                  <Point X="3.399130615234" Y="-24.8150703125" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.38000390625" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.370376708984" Y="-24.88952734375" />
                  <Point X="3.362354736328" Y="-24.9314140625" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.359896728516" Y="-25.118310546875" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.183986328125" />
                  <Point X="3.380004394531" Y="-25.205484375" />
                  <Point X="3.384067382812" Y="-25.216029296875" />
                  <Point X="3.393841064453" Y="-25.237494140625" />
                  <Point X="3.399128662109" Y="-25.247484375" />
                  <Point X="3.410853027344" Y="-25.266765625" />
                  <Point X="3.417289794922" Y="-25.276056640625" />
                  <Point X="3.44135546875" Y="-25.30672265625" />
                  <Point X="3.472795654297" Y="-25.34678515625" />
                  <Point X="3.478713867188" Y="-25.35362890625" />
                  <Point X="3.501138916016" Y="-25.375806640625" />
                  <Point X="3.53017578125" Y="-25.3987265625" />
                  <Point X="3.541495361328" Y="-25.406408203125" />
                  <Point X="3.581604980469" Y="-25.429591796875" />
                  <Point X="3.634005371094" Y="-25.459880859375" />
                  <Point X="3.6395" Y="-25.4628203125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027099609" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.428386230469" Y="-25.68123046875" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.76161328125" Y="-25.9310546875" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="4.090479736328" Y="-25.995314453125" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720947266" Y="-25.910841796875" />
                  <Point X="3.354482910156" Y="-25.912677734375" />
                  <Point X="3.275761962891" Y="-25.929787109375" />
                  <Point X="3.172918701172" Y="-25.952140625" />
                  <Point X="3.157875244141" Y="-25.9567421875" />
                  <Point X="3.128753173828" Y="-25.9683671875" />
                  <Point X="3.114674560547" Y="-25.975390625" />
                  <Point X="3.086848144531" Y="-25.992283203125" />
                  <Point X="3.074122802734" Y="-26.00153125" />
                  <Point X="3.050373291016" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.991767089844" Y="-26.090451171875" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606201172" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.868337646484" Y="-26.370771484375" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876787109375" Y="-26.576671875" />
                  <Point X="2.889158935547" Y="-26.605482421875" />
                  <Point X="2.896541259766" Y="-26.619373046875" />
                  <Point X="2.940106689453" Y="-26.687134765625" />
                  <Point X="2.997021484375" Y="-26.775662109375" />
                  <Point X="3.001740966797" Y="-26.7823515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486328125" Y="-26.828119140625" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.736173095703" Y="-27.360654296875" />
                  <Point X="4.087170654297" Y="-27.629982421875" />
                  <Point X="4.045498535156" Y="-27.697416015625" />
                  <Point X="4.001273193359" Y="-27.76025390625" />
                  <Point X="3.430660644531" Y="-27.430810546875" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841191650391" Y="-27.09088671875" />
                  <Point X="2.815025878906" Y="-27.079515625" />
                  <Point X="2.783120605469" Y="-27.069328125" />
                  <Point X="2.771107421875" Y="-27.066337890625" />
                  <Point X="2.677416992188" Y="-27.04941796875" />
                  <Point X="2.555017089844" Y="-27.0273125" />
                  <Point X="2.539358886719" Y="-27.02580859375" />
                  <Point X="2.508009277344" Y="-27.025404296875" />
                  <Point X="2.492317871094" Y="-27.02650390625" />
                  <Point X="2.460145019531" Y="-27.0314609375" />
                  <Point X="2.444846923828" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.40058984375" Y="-27.051109375" />
                  <Point X="2.322755859375" Y="-27.092072265625" />
                  <Point X="2.221071777344" Y="-27.145587890625" />
                  <Point X="2.208968994141" Y="-27.153169921875" />
                  <Point X="2.186038818359" Y="-27.1700625" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144940673828" Y="-27.21116015625" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.079500244141" Y="-27.324029296875" />
                  <Point X="2.025984619141" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.44019140625" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.019108276367" Y="-27.67383203125" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.508685302734" Y="-28.634189453125" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.723753662109" Y="-29.0360859375" />
                  <Point X="2.278191894531" Y="-28.45541796875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828654663086" Y="-27.8701484375" />
                  <Point X="1.808837280273" Y="-27.8496328125" />
                  <Point X="1.783253540039" Y="-27.828005859375" />
                  <Point X="1.773297607422" Y="-27.820646484375" />
                  <Point X="1.680893310547" Y="-27.761240234375" />
                  <Point X="1.560174316406" Y="-27.68362890625" />
                  <Point X="1.546284667969" Y="-27.67624609375" />
                  <Point X="1.517470947266" Y="-27.663873046875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926025391" Y="-27.65115234375" />
                  <Point X="1.455384765625" Y="-27.6486953125" />
                  <Point X="1.424119995117" Y="-27.64637890625" />
                  <Point X="1.408396240234" Y="-27.64651953125" />
                  <Point X="1.307336425781" Y="-27.655818359375" />
                  <Point X="1.175309204102" Y="-27.667966796875" />
                  <Point X="1.161226196289" Y="-27.67033984375" />
                  <Point X="1.133575317383" Y="-27.677171875" />
                  <Point X="1.120007324219" Y="-27.681630859375" />
                  <Point X="1.092621582031" Y="-27.692974609375" />
                  <Point X="1.079875488281" Y="-27.699416015625" />
                  <Point X="1.055493652344" Y="-27.71413671875" />
                  <Point X="1.043857910156" Y="-27.722416015625" />
                  <Point X="0.96582208252" Y="-27.78730078125" />
                  <Point X="0.863874206543" Y="-27.872068359375" />
                  <Point X="0.85265435791" Y="-27.88308984375" />
                  <Point X="0.832184082031" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.759459594727" Y="-28.112978515625" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091491699" Y="-29.152341796875" />
                  <Point X="0.828745361328" Y="-29.13612109375" />
                  <Point X="0.655064819336" Y="-28.487935546875" />
                  <Point X="0.652606018066" Y="-28.480123046875" />
                  <Point X="0.642146972656" Y="-28.453583984375" />
                  <Point X="0.626788391113" Y="-28.42381640625" />
                  <Point X="0.620407409668" Y="-28.41320703125" />
                  <Point X="0.549419921875" Y="-28.310927734375" />
                  <Point X="0.4566796875" Y="-28.177306640625" />
                  <Point X="0.446669189453" Y="-28.165169921875" />
                  <Point X="0.424782684326" Y="-28.142712890625" />
                  <Point X="0.412906524658" Y="-28.132392578125" />
                  <Point X="0.386651550293" Y="-28.113150390625" />
                  <Point X="0.37323550415" Y="-28.10493359375" />
                  <Point X="0.345236297607" Y="-28.090828125" />
                  <Point X="0.330652954102" Y="-28.084939453125" />
                  <Point X="0.220803848267" Y="-28.05084765625" />
                  <Point X="0.077293876648" Y="-28.006306640625" />
                  <Point X="0.063380096436" Y="-28.003111328125" />
                  <Point X="0.035227794647" Y="-27.998841796875" />
                  <Point X="0.020989425659" Y="-27.997767578125" />
                  <Point X="-0.008651926041" Y="-27.997765625" />
                  <Point X="-0.022895944595" Y="-27.998837890625" />
                  <Point X="-0.051062213898" Y="-28.003107421875" />
                  <Point X="-0.064984313965" Y="-28.0063046875" />
                  <Point X="-0.174833435059" Y="-28.0403984375" />
                  <Point X="-0.318343414307" Y="-28.0849375" />
                  <Point X="-0.33292791748" Y="-28.090828125" />
                  <Point X="-0.360928771973" Y="-28.104935546875" />
                  <Point X="-0.374345275879" Y="-28.11315234375" />
                  <Point X="-0.40060067749" Y="-28.132396484375" />
                  <Point X="-0.412477600098" Y="-28.14271875" />
                  <Point X="-0.434361572266" Y="-28.16517578125" />
                  <Point X="-0.444368804932" Y="-28.177310546875" />
                  <Point X="-0.515356323242" Y="-28.279591796875" />
                  <Point X="-0.608096374512" Y="-28.4132109375" />
                  <Point X="-0.61247052002" Y="-28.420130859375" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.869653808594" Y="-29.3347421875" />
                  <Point X="-0.985425231934" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121061018339" Y="-27.397178824828" />
                  <Point X="-3.727894709244" Y="-27.980071848826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.045551571919" Y="-27.339238476097" />
                  <Point X="-3.645421437881" Y="-27.932455795147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.673116836659" Y="-26.238947002652" />
                  <Point X="-4.642543427531" Y="-26.284273945699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.970042125499" Y="-27.281298127365" />
                  <Point X="-3.562948166519" Y="-27.884839741468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.954815168014" Y="-28.786433988715" />
                  <Point X="-2.863357939703" Y="-28.922024905697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.741732958546" Y="-25.967331711784" />
                  <Point X="-4.537298451726" Y="-26.270418332212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.894532679079" Y="-27.223357778634" />
                  <Point X="-3.480474895156" Y="-27.837223687788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.901966662289" Y="-28.6948974138" />
                  <Point X="-2.646373599538" Y="-29.073829712458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772569091658" Y="-25.751727557665" />
                  <Point X="-4.432053475922" Y="-26.256562718725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.819023232659" Y="-27.165417429903" />
                  <Point X="-3.398001617244" Y="-27.789607643818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.849118156564" Y="-28.603360838885" />
                  <Point X="-2.486589952098" Y="-29.140831004811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.733095842968" Y="-25.640361348726" />
                  <Point X="-4.326808500117" Y="-26.242707105238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.743513786239" Y="-27.107477081172" />
                  <Point X="-3.315528317507" Y="-27.741991632205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.796269650839" Y="-28.51182426397" />
                  <Point X="-2.425606177885" Y="-29.061355461424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.636045523967" Y="-25.614356656911" />
                  <Point X="-4.221563524312" Y="-26.22885149175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.66800433982" Y="-27.049536732441" />
                  <Point X="-3.23305501777" Y="-27.694375620592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.743421145114" Y="-28.420287689055" />
                  <Point X="-2.358075985348" Y="-28.991585382329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.538995204966" Y="-25.588351965096" />
                  <Point X="-4.116318557597" Y="-26.214995864787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.592494901036" Y="-26.991596372388" />
                  <Point X="-3.150581718034" Y="-27.646759608979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.690572639389" Y="-28.32875111414" />
                  <Point X="-2.279218150764" Y="-28.938609223198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.441944885965" Y="-25.562347273281" />
                  <Point X="-4.01107360092" Y="-26.201140222942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.516985482934" Y="-26.933655981674" />
                  <Point X="-3.068108418297" Y="-27.599143597366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.637724111654" Y="-28.237214571855" />
                  <Point X="-2.200227924132" Y="-28.885829343348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.344894566964" Y="-25.536342581467" />
                  <Point X="-3.905828644243" Y="-26.187284581097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441476064833" Y="-26.875715590959" />
                  <Point X="-2.98563511856" Y="-27.551527585752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.584875562085" Y="-28.145678061941" />
                  <Point X="-2.121237184618" Y="-28.833050223878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.247844247963" Y="-25.510337889652" />
                  <Point X="-3.800583687566" Y="-26.173428939252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.365966646731" Y="-26.817775200244" />
                  <Point X="-2.903161818824" Y="-27.503911574139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.532027012516" Y="-28.054141552028" />
                  <Point X="-2.036048360114" Y="-28.789460143295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150793928631" Y="-25.484333198329" />
                  <Point X="-3.695338730889" Y="-26.159573297407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.29045722863" Y="-26.75983480953" />
                  <Point X="-2.820688519087" Y="-27.456295562526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.479178462947" Y="-27.962605042114" />
                  <Point X="-1.92988811336" Y="-28.776961474793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781242373435" Y="-24.379767234655" />
                  <Point X="-4.756927709477" Y="-24.415815206402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.053743569947" Y="-25.458328565345" />
                  <Point X="-3.590093774212" Y="-26.145717655562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.214947810528" Y="-26.701894418815" />
                  <Point X="-2.73821521935" Y="-27.408679550913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.426329913378" Y="-27.8710685322" />
                  <Point X="-1.809996861926" Y="-28.784819857887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760626198714" Y="-24.240444263869" />
                  <Point X="-4.617057830201" Y="-24.45329312334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.956693211264" Y="-25.432323932362" />
                  <Point X="-3.484848817534" Y="-26.131862013717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.139438392426" Y="-26.6439540281" />
                  <Point X="-2.653126800557" Y="-27.36494061274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.373481363809" Y="-27.779532022287" />
                  <Point X="-1.69010615575" Y="-28.792677432604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.127484359426" Y="-29.626798547868" />
                  <Point X="-1.040223709498" Y="-29.756167781538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.740010018964" Y="-24.10112130054" />
                  <Point X="-4.477187950925" Y="-24.490771040278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.859642852581" Y="-25.406319299379" />
                  <Point X="-3.379603860857" Y="-26.118006371872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.063928974325" Y="-26.586013637385" />
                  <Point X="-2.549828010594" Y="-27.348199660087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.324422613358" Y="-27.682376904122" />
                  <Point X="-1.558696989804" Y="-28.817611826193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.133102717574" Y="-29.448581282624" />
                  <Point X="-0.966385863935" Y="-29.695749182622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.71231442661" Y="-23.972293998015" />
                  <Point X="-4.337318066455" Y="-24.528248964915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.762592493898" Y="-25.380314666395" />
                  <Point X="-3.27435890418" Y="-26.104150730027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.996981765438" Y="-26.515379249484" />
                  <Point X="-2.361326276358" Y="-27.457777267015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.34697286479" Y="-27.479057074771" />
                  <Point X="-1.309367924664" Y="-29.017369659737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.181028864669" Y="-29.207640140822" />
                  <Point X="-0.9338065712" Y="-29.574162263665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.679472240441" Y="-23.851096834604" />
                  <Point X="-4.197448123559" Y="-24.565726976175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.665542135215" Y="-25.354310033412" />
                  <Point X="-3.157672480388" Y="-26.10725776075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953498192049" Y="-26.409958591415" />
                  <Point X="-0.901227278466" Y="-29.452575344708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.646629987571" Y="-23.729899770079" />
                  <Point X="-4.057578180662" Y="-24.603204987434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568491776532" Y="-25.328305400428" />
                  <Point X="-0.868647990691" Y="-29.330988418397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.557775513572" Y="-23.69174423836" />
                  <Point X="-3.917708237766" Y="-24.640682998693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.47379203134" Y="-25.298815839631" />
                  <Point X="-0.836068858606" Y="-29.209401261267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.432017475704" Y="-23.708300490033" />
                  <Point X="-3.777838294869" Y="-24.678161009952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.393895811347" Y="-25.247379150177" />
                  <Point X="-0.803489726521" Y="-29.087814104138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.306259437835" Y="-23.724856741707" />
                  <Point X="-3.637968351973" Y="-24.715639021211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.330650895612" Y="-25.171255886954" />
                  <Point X="-0.770910594436" Y="-28.966226947008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.180501373613" Y="-23.74141303245" />
                  <Point X="-3.49745533003" Y="-24.754070436364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.295344578717" Y="-25.053711947577" />
                  <Point X="-0.738331462351" Y="-28.844639789878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.054743279296" Y="-23.757969367812" />
                  <Point X="-0.705752330266" Y="-28.723052632748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.928985184979" Y="-23.774525703174" />
                  <Point X="-0.673173198181" Y="-28.601465475618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803227090662" Y="-23.791082038536" />
                  <Point X="-0.640212212267" Y="-28.48044444007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.69004566342" Y="-23.788992698178" />
                  <Point X="-0.589268503277" Y="-28.386083887862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.595733757024" Y="-23.758928142719" />
                  <Point X="-0.531154516315" Y="-28.302353709909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.210880073995" Y="-22.677048516507" />
                  <Point X="-4.15284233169" Y="-22.763093007949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.516233129626" Y="-23.706904963125" />
                  <Point X="-0.473041138782" Y="-28.218622628441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.157720662633" Y="-22.585972878154" />
                  <Point X="-3.915315380343" Y="-22.945353488238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.460028488599" Y="-23.620344063413" />
                  <Point X="-0.410693888517" Y="-28.141168521431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.104560999807" Y="-22.494897612612" />
                  <Point X="-3.677787692608" Y="-23.127615060268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423199393379" Y="-23.505057735745" />
                  <Point X="-0.33067262268" Y="-28.089917220064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.051401336981" Y="-22.40382234707" />
                  <Point X="-0.236560902964" Y="-28.059555875648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.997614908269" Y="-22.313676300166" />
                  <Point X="-0.141806189932" Y="-28.030147808024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.936237481811" Y="-22.234784370233" />
                  <Point X="-0.045974630978" Y="-28.002336230134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.874860055353" Y="-22.1558924403" />
                  <Point X="0.07019443574" Y="-28.004676247452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825701633456" Y="-29.124761730215" />
                  <Point X="0.830372108567" Y="-29.131685994319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.813482614645" Y="-22.077000531494" />
                  <Point X="0.214637179471" Y="-28.048933714744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.750179043797" Y="-28.842907179799" />
                  <Point X="0.802581831832" Y="-28.92059750798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.752105142292" Y="-21.998108669604" />
                  <Point X="0.363790991035" Y="-28.100175627327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.674656454139" Y="-28.561052629383" />
                  <Point X="0.774791555097" Y="-28.709509021641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.617451592873" Y="-22.027853059496" />
                  <Point X="0.747001278362" Y="-28.498420535302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429774263893" Y="-22.136208435369" />
                  <Point X="0.724727569952" Y="-28.295510697843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.243093990725" Y="-22.243085615212" />
                  <Point X="0.748916544301" Y="-28.161484620334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.108374130983" Y="-22.272928314202" />
                  <Point X="0.776843432674" Y="-28.033000228261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.9954674901" Y="-22.270431586314" />
                  <Point X="0.819137960744" Y="-27.925816738012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.907840224927" Y="-22.23045664269" />
                  <Point X="0.88540153718" Y="-27.854168823322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.837221150221" Y="-22.165266019735" />
                  <Point X="0.958817582598" Y="-27.793124879974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.775602980638" Y="-22.086731006161" />
                  <Point X="1.032233787022" Y="-27.732081172362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.74876355288" Y="-21.956634387425" />
                  <Point X="1.11437028916" Y="-27.683965837774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.95459632119" Y="-21.481587052341" />
                  <Point X="1.215664845761" Y="-27.664253486966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.995480763471" Y="-21.251085667248" />
                  <Point X="1.323559144671" Y="-27.654325656509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.919950126136" Y="-21.193176735341" />
                  <Point X="1.433245752601" Y="-27.647055033448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.844419259931" Y="-21.135268142747" />
                  <Point X="1.581947915238" Y="-27.69762734896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.768888393726" Y="-21.077359550153" />
                  <Point X="1.786107109726" Y="-27.830418095322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.693357527522" Y="-21.019450957559" />
                  <Point X="2.642549842106" Y="-28.930258955368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.614661475773" Y="-20.966234945509" />
                  <Point X="2.218968966575" Y="-28.13238677555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.531307116546" Y="-20.919925158308" />
                  <Point X="2.017224347122" Y="-27.663400370394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.447952757319" Y="-20.873615371106" />
                  <Point X="2.007735195231" Y="-27.479444417429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.36459842027" Y="-20.827305551025" />
                  <Point X="2.052325126975" Y="-27.375664003075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.281244173581" Y="-20.780995596978" />
                  <Point X="2.10254852847" Y="-27.280235551089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.197889926892" Y="-20.734685642931" />
                  <Point X="2.159621670628" Y="-27.194962257256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.940965180311" Y="-20.94570453731" />
                  <Point X="2.235711870757" Y="-27.137882911306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.810254298151" Y="-20.969603682612" />
                  <Point X="2.32028133461" Y="-27.093374590795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.714188404511" Y="-20.942139520182" />
                  <Point X="2.405078238817" Y="-27.049203464476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.625449593316" Y="-20.903812511304" />
                  <Point X="2.503814480854" Y="-27.02569825635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.550562190925" Y="-20.844949944375" />
                  <Point X="2.628437868929" Y="-27.040572320528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.500611194528" Y="-20.749117635225" />
                  <Point X="2.758923523861" Y="-27.064137552733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.466084460661" Y="-20.630417916479" />
                  <Point X="2.923231329649" Y="-27.137846185668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.478670219606" Y="-20.441871054761" />
                  <Point X="3.110908821157" Y="-27.2462018025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.384965104228" Y="-20.410906894623" />
                  <Point X="3.298586312666" Y="-27.354557419332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.288598010721" Y="-20.383889279358" />
                  <Point X="2.868030211379" Y="-26.546344042062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.075178896029" Y="-26.853454596603" />
                  <Point X="3.486263820569" Y="-27.462913060469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.192230738864" Y="-20.356871928507" />
                  <Point X="2.867840418062" Y="-26.37617495515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.312706858934" Y="-27.035716576588" />
                  <Point X="3.673941367411" Y="-27.571268759337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.095863467008" Y="-20.329854577656" />
                  <Point X="2.889839241787" Y="-26.238901845811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.550234821839" Y="-27.217978556574" />
                  <Point X="3.861618914254" Y="-27.679624458205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.999496195152" Y="-20.302837226805" />
                  <Point X="2.943474680664" Y="-26.148531947272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.787762309675" Y="-27.400239832241" />
                  <Point X="4.016245605382" Y="-27.738980248414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900266607534" Y="-20.280063433581" />
                  <Point X="3.006741649091" Y="-26.072441378511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.02528808528" Y="-27.582498569423" />
                  <Point X="4.072896446112" Y="-27.653080866967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.794060398864" Y="-20.267632906422" />
                  <Point X="3.073730783549" Y="-26.001869147825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.687854190194" Y="-20.255202379263" />
                  <Point X="3.157881648267" Y="-25.956740228576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.58164796068" Y="-20.242771883006" />
                  <Point X="3.257037412327" Y="-25.933856987426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.475441664953" Y="-20.230341484914" />
                  <Point X="-0.286846607621" Y="-20.509945155769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.014260448886" Y="-20.914070755266" />
                  <Point X="3.357079758252" Y="-25.912288157946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.369235369225" Y="-20.217911086823" />
                  <Point X="-0.362368793949" Y="-20.228091203314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.089774314299" Y="-20.898420927785" />
                  <Point X="3.472824414354" Y="-25.913998960649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.170785460756" Y="-20.848637184789" />
                  <Point X="3.598582475981" Y="-25.930555247546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.227944789591" Y="-20.763491667959" />
                  <Point X="3.724340537608" Y="-25.947111534443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.262868770589" Y="-20.645380892304" />
                  <Point X="3.850098599235" Y="-25.96366782134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.295447952953" Y="-20.523793809716" />
                  <Point X="3.349775133994" Y="-25.052020073396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.61904967742" Y="-25.451236001293" />
                  <Point X="3.975856660862" Y="-25.980224108237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.328027135318" Y="-20.402206727129" />
                  <Point X="3.366923858702" Y="-24.90755639656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.76973370357" Y="-25.504746550293" />
                  <Point X="4.101614717082" Y="-25.996780387117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.360606340401" Y="-20.280619678224" />
                  <Point X="2.050180160257" Y="-22.785515876963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.116914061912" Y="-22.884452954833" />
                  <Point X="3.408613630777" Y="-24.799476318677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.909603407441" Y="-25.542224207183" />
                  <Point X="4.227372717638" Y="-26.013336583472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.434940726182" Y="-20.220937230453" />
                  <Point X="2.015726454493" Y="-22.564548450829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.31709940723" Y="-23.011352227522" />
                  <Point X="3.469543004893" Y="-24.719920123831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.049473111312" Y="-25.579701864072" />
                  <Point X="4.353130718193" Y="-26.029892779827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.558241112777" Y="-20.233849864275" />
                  <Point X="2.047468343405" Y="-22.44172002965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.440587311548" Y="-23.024542867801" />
                  <Point X="3.541235243052" Y="-24.656320531123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.189342815183" Y="-25.617179520962" />
                  <Point X="4.478888718749" Y="-26.046448976181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.681541527634" Y="-20.246762539996" />
                  <Point X="2.097387381772" Y="-22.34584034077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.542437964033" Y="-23.005654963045" />
                  <Point X="3.017647426142" Y="-23.710181963436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.192516195988" Y="-23.969435576221" />
                  <Point X="3.623674306331" Y="-24.608653761874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.329212519054" Y="-25.654657177852" />
                  <Point X="4.604646719305" Y="-26.063005172536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804841942492" Y="-20.259675215717" />
                  <Point X="2.150236018899" Y="-22.254303960668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.634829429" Y="-22.972743236081" />
                  <Point X="3.010076924491" Y="-23.529070526429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351890194295" Y="-24.03582953876" />
                  <Point X="3.714040001294" Y="-24.572738707371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.469082306749" Y="-25.692134959014" />
                  <Point X="4.728401717111" Y="-26.076591795195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.939186053053" Y="-20.288960843639" />
                  <Point X="2.203084656027" Y="-22.162767580565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717340229866" Y="-22.925182822179" />
                  <Point X="3.047773075082" Y="-23.415069661211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.47501528571" Y="-24.048482286789" />
                  <Point X="3.811090354209" Y="-24.546734065835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.608952298716" Y="-25.729613043024" />
                  <Point X="4.757369646581" Y="-25.949650810019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.076067767944" Y="-20.322008624691" />
                  <Point X="2.255933293155" Y="-22.071231200463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.799813560369" Y="-22.877566856179" />
                  <Point X="3.105470389564" Y="-23.330721740903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.581554726243" Y="-24.036545796182" />
                  <Point X="3.908140707123" Y="-24.520729424299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.748822290683" Y="-25.767091127033" />
                  <Point X="4.779480700689" Y="-25.812544089065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.212949167922" Y="-20.355055938867" />
                  <Point X="2.308781930283" Y="-21.97969482036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.882286890872" Y="-22.829950890178" />
                  <Point X="3.170911426481" Y="-23.257854361227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.686799689589" Y="-24.022690164225" />
                  <Point X="4.005191060038" Y="-24.494724782764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.34983051416" Y="-20.388103173369" />
                  <Point X="2.36163056741" Y="-21.888158440257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.964760221375" Y="-22.782334924178" />
                  <Point X="3.246372227087" Y="-23.199841892112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.792044652936" Y="-24.008834532268" />
                  <Point X="4.102241412952" Y="-24.468720141228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.490298585019" Y="-20.426467945799" />
                  <Point X="2.414479204538" Y="-21.796622060155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.047233551878" Y="-22.734718958177" />
                  <Point X="3.321881537358" Y="-23.141901341531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.897289616282" Y="-23.994978900311" />
                  <Point X="4.199291765867" Y="-24.442715499692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.642003582925" Y="-20.481492147677" />
                  <Point X="2.467327812791" Y="-21.705085637243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.129706882381" Y="-22.687102992177" />
                  <Point X="3.397390847628" Y="-23.08396079095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.002534579629" Y="-23.981123268354" />
                  <Point X="4.296342061582" Y="-24.416710773354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.79370883518" Y="-20.536516726641" />
                  <Point X="2.520176396267" Y="-21.613549177598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.212180212883" Y="-22.639487026176" />
                  <Point X="3.472900157899" Y="-23.026020240369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.107779542976" Y="-23.967267636397" />
                  <Point X="4.393392278038" Y="-24.390705929511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.954428503644" Y="-20.604905727231" />
                  <Point X="2.573024979742" Y="-21.522012717953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.294653543386" Y="-22.591871060176" />
                  <Point X="3.54840946817" Y="-22.968079689789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.213024506322" Y="-23.953412004441" />
                  <Point X="4.490442494495" Y="-24.364701085669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.121823444166" Y="-20.683191225628" />
                  <Point X="2.625873563218" Y="-21.430476258308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.377126873889" Y="-22.544255094175" />
                  <Point X="3.62391877844" Y="-22.910139139208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.318269422595" Y="-23.939556302693" />
                  <Point X="4.587492710952" Y="-24.338696241826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294171256823" Y="-20.768819658935" />
                  <Point X="2.678722146694" Y="-21.338939798663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.459600204392" Y="-22.496639128175" />
                  <Point X="3.699428164933" Y="-22.852198701631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.423514047985" Y="-23.925700169694" />
                  <Point X="4.684542927409" Y="-24.312691397983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.482942778474" Y="-20.878797242154" />
                  <Point X="2.731570730169" Y="-21.247403339018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.542073493775" Y="-22.449023101212" />
                  <Point X="3.774937641413" Y="-22.794258397466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.528758673374" Y="-23.911844036696" />
                  <Point X="4.781593143866" Y="-24.286686554141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.678759638319" Y="-20.999219968789" />
                  <Point X="2.784419313645" Y="-21.155866879373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.624546768418" Y="-22.401407052395" />
                  <Point X="3.850447117893" Y="-22.736318093301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.634003298764" Y="-23.897987903697" />
                  <Point X="4.748179004944" Y="-24.067260349231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.70702004306" Y="-22.353791003578" />
                  <Point X="3.925956594374" Y="-22.678377789136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789493317703" Y="-22.306174954761" />
                  <Point X="4.001466070854" Y="-22.620437484972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.871966592345" Y="-22.258558905944" />
                  <Point X="4.076975547334" Y="-22.562497180807" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.645219482422" Y="-29.185296875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.393331176758" Y="-28.419263671875" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.27433605957" Y="-28.266400390625" />
                  <Point X="0.164486938477" Y="-28.23230859375" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.118513565063" Y="-28.221859375" />
                  <Point X="-0.262023590088" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.35926651001" Y="-28.387923828125" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.686127929688" Y="-29.38391796875" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-0.931940429688" Y="-29.970734375" />
                  <Point X="-1.10023046875" Y="-29.938068359375" />
                  <Point X="-1.226992797852" Y="-29.905453125" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.330057373047" Y="-29.70984375" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.335926147461" Y="-29.402826171875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.48741784668" Y="-29.113935546875" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240600586" Y="-28.985763671875" />
                  <Point X="-1.783469482422" Y="-28.976966796875" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989878662109" Y="-28.973791015625" />
                  <Point X="-2.101725341797" Y="-29.0485234375" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.387121337891" Y="-29.32330859375" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.609133056641" Y="-29.320361328125" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-3.031341552734" Y="-29.0324765625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.828799072266" Y="-28.188166015625" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.597591796875" />
                  <Point X="-2.51398046875" Y="-27.568763671875" />
                  <Point X="-2.531329101562" Y="-27.551416015625" />
                  <Point X="-2.560157958984" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.322036865234" Y="-27.965140625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.966796386719" Y="-28.103197265625" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.287528320312" Y="-27.636140625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.728521484375" Y="-26.856482421875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140326660156" Y="-26.334595703125" />
                  <Point X="-3.16116015625" Y="-26.310638671875" />
                  <Point X="-3.187642822266" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-4.14153125" Y="-26.409955078125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.851163574219" Y="-26.309626953125" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.960683105469" Y="-25.778431640625" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.200781738281" Y="-25.3010234375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.529863525391" Y="-25.11307421875" />
                  <Point X="-3.502324707031" Y="-25.090646484375" />
                  <Point X="-3.490887939453" Y="-25.062984375" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.489658691406" Y="-25.003537109375" />
                  <Point X="-3.502325683594" Y="-24.971912109375" />
                  <Point X="-3.526173095703" Y="-24.952048828125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.397912109375" Y="-24.708716796875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.966773925781" Y="-24.33558984375" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.8506328125" Y="-23.756287109375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.214412597656" Y="-23.54530859375" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731704345703" Y="-23.6041328125" />
                  <Point X="-3.705071777344" Y="-23.595736328125" />
                  <Point X="-3.670278320312" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.5739453125" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.628433349609" Y="-23.530416015625" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.629210449219" Y="-23.42971875" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.142051757812" Y="-23.01086328125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.350911132813" Y="-22.540046875" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.982501464844" Y="-21.984828125" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.442379882813" Y="-21.909537109375" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138515625" Y="-22.07956640625" />
                  <Point X="-3.101423828125" Y="-22.082810546875" />
                  <Point X="-3.052966308594" Y="-22.08705078125" />
                  <Point X="-3.031507324219" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.986924804688" Y="-22.046267578125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.941319335938" Y="-21.935068359375" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.165693115234" Y="-21.49595703125" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-3.085357177734" Y="-21.080578125" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.473299316406" Y="-20.670341796875" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.054529785156" Y="-20.59943359375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246826172" Y="-20.726337890625" />
                  <Point X="-1.909963989258" Y="-20.747830078125" />
                  <Point X="-1.856030761719" Y="-20.77590625" />
                  <Point X="-1.835124511719" Y="-20.781509765625" />
                  <Point X="-1.813809570312" Y="-20.77775" />
                  <Point X="-1.770810668945" Y="-20.7599375" />
                  <Point X="-1.714635620117" Y="-20.736669921875" />
                  <Point X="-1.696905395508" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.672087768555" Y="-20.661125" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.676204345703" Y="-20.39709765625" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.398511474609" Y="-20.21737890625" />
                  <Point X="-0.968083129883" Y="-20.096703125" />
                  <Point X="-0.629162963867" Y="-20.057037109375" />
                  <Point X="-0.224199966431" Y="-20.009640625" />
                  <Point X="-0.125925582886" Y="-20.37640625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282125473" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594043732" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.163911636353" Y="-20.28058984375" />
                  <Point X="0.236648391724" Y="-20.0091328125" />
                  <Point X="0.484387817383" Y="-20.035078125" />
                  <Point X="0.860210205078" Y="-20.074435546875" />
                  <Point X="1.14061315918" Y="-20.142134765625" />
                  <Point X="1.508455566406" Y="-20.230943359375" />
                  <Point X="1.691106933594" Y="-20.29719140625" />
                  <Point X="1.931044433594" Y="-20.38421875" />
                  <Point X="2.107520263672" Y="-20.46675" />
                  <Point X="2.338685302734" Y="-20.574859375" />
                  <Point X="2.509195068359" Y="-20.674197265625" />
                  <Point X="2.732533691406" Y="-20.804314453125" />
                  <Point X="2.893321289062" Y="-20.918658203125" />
                  <Point X="3.068740722656" Y="-21.043408203125" />
                  <Point X="2.603430175781" Y="-21.849349609375" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.215543457031" Y="-22.543296875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.205674316406" Y="-22.637775390625" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.237307617188" Y="-22.72663671875" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.274938720703" Y="-22.775794921875" />
                  <Point X="2.302387451172" Y="-22.794419921875" />
                  <Point X="2.338247314453" Y="-22.81875390625" />
                  <Point X="2.360336425781" Y="-22.827021484375" />
                  <Point X="2.390437011719" Y="-22.830650390625" />
                  <Point X="2.429761230469" Y="-22.835392578125" />
                  <Point X="2.448665527344" Y="-22.8340546875" />
                  <Point X="2.483475341797" Y="-22.82474609375" />
                  <Point X="2.528951904297" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.38636328125" Y="-22.31953125" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.070120849609" Y="-22.074015625" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.292234375" Y="-22.406255859375" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.780464599609" Y="-23.0295078125" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.279371582031" Y="-23.41616796875" />
                  <Point X="3.254318847656" Y="-23.4488515625" />
                  <Point X="3.221589355469" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.203787109375" Y="-23.541869140625" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.198440185547" Y="-23.646162109375" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.236482910156" Y="-23.74371484375" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280948242188" Y="-23.801181640625" />
                  <Point X="3.311142822266" Y="-23.8181796875" />
                  <Point X="3.350590332031" Y="-23.840384765625" />
                  <Point X="3.368565673828" Y="-23.846380859375" />
                  <Point X="3.409390869141" Y="-23.851775390625" />
                  <Point X="3.462726318359" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.278810058594" Y="-23.753111328125" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.88229296875" Y="-23.81490625" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.9674375" Y="-24.230052734375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.304836914062" Y="-24.611138671875" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087646484" Y="-24.767181640625" />
                  <Point X="3.688978027344" Y="-24.790365234375" />
                  <Point X="3.636577636719" Y="-24.82065234375" />
                  <Point X="3.622264892578" Y="-24.833072265625" />
                  <Point X="3.59819921875" Y="-24.86373828125" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.548963134766" Y="-24.96715234375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.546505126953" Y="-25.082572265625" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.590824707031" Y="-25.18942578125" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.636576416016" Y="-25.241908203125" />
                  <Point X="3.676686035156" Y="-25.265091796875" />
                  <Point X="3.729086425781" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.477561035156" Y="-25.497703125" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.980199707031" Y="-25.755689453125" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.912240234375" Y="-26.12499609375" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.065679931641" Y="-26.183689453125" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.3948359375" Y="-26.098341796875" />
                  <Point X="3.316114990234" Y="-26.115451171875" />
                  <Point X="3.213271728516" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.13786328125" Y="-26.211923828125" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.057538330078" Y="-26.388181640625" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.099926025391" Y="-26.584384765625" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.851838134766" Y="-27.209916015625" />
                  <Point X="4.339073730469" Y="-27.583783203125" />
                  <Point X="4.293682128906" Y="-27.657234375" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.12928515625" Y="-27.90848828125" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.335660644531" Y="-27.595353515625" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.643650390625" Y="-27.23639453125" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.411243652344" Y="-27.260208984375" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.247636474609" Y="-27.412517578125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.206083496094" Y="-27.64006640625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.673230224609" Y="-28.539189453125" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.941558349609" Y="-29.1143125" />
                  <Point X="2.835294433594" Y="-29.19021484375" />
                  <Point X="2.751615722656" Y="-29.24437890625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="2.127454589844" Y="-28.57108203125" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549316406" Y="-27.980466796875" />
                  <Point X="1.578145141602" Y="-27.921060546875" />
                  <Point X="1.45742590332" Y="-27.84344921875" />
                  <Point X="1.425805053711" Y="-27.83571875" />
                  <Point X="1.324745361328" Y="-27.845017578125" />
                  <Point X="1.192718261719" Y="-27.857166015625" />
                  <Point X="1.165332519531" Y="-27.868509765625" />
                  <Point X="1.087296875" Y="-27.93339453125" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.945124511719" Y="-28.153333984375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.038378051758" Y="-29.256" />
                  <Point X="1.127642578125" Y="-29.934029296875" />
                  <Point X="1.094876220703" Y="-29.941212890625" />
                  <Point X="0.994368896484" Y="-29.9632421875" />
                  <Point X="0.917038391113" Y="-29.977291015625" />
                  <Point X="0.860200561523" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#160" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.083785213187" Y="4.667815162065" Z="1.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.1" />
                  <Point X="-0.641131131529" Y="5.023904450945" Z="1.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.1" />
                  <Point X="-1.4181653662" Y="4.862046048714" Z="1.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.1" />
                  <Point X="-1.731932719004" Y="4.627657504962" Z="1.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.1" />
                  <Point X="-1.725929802209" Y="4.385191551583" Z="1.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.1" />
                  <Point X="-1.796100133699" Y="4.317535462703" Z="1.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.1" />
                  <Point X="-1.893032253184" Y="4.327800627403" Z="1.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.1" />
                  <Point X="-2.021018339089" Y="4.462285101628" Z="1.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.1" />
                  <Point X="-2.503738172256" Y="4.404645864465" Z="1.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.1" />
                  <Point X="-3.121935346634" Y="3.990381513835" Z="1.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.1" />
                  <Point X="-3.215150359384" Y="3.510323407381" Z="1.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.1" />
                  <Point X="-2.997285149194" Y="3.091855263835" Z="1.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.1" />
                  <Point X="-3.028435595926" Y="3.020367971961" Z="1.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.1" />
                  <Point X="-3.103221201772" Y="2.998279549979" Z="1.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.1" />
                  <Point X="-3.423535940916" Y="3.165043589361" Z="1.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.1" />
                  <Point X="-4.028121176939" Y="3.077156474137" Z="1.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.1" />
                  <Point X="-4.400249292481" Y="2.516439632619" Z="1.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.1" />
                  <Point X="-4.178645510727" Y="1.980749545757" Z="1.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.1" />
                  <Point X="-3.679716769102" Y="1.578474172525" Z="1.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.1" />
                  <Point X="-3.680783480825" Y="1.519999428946" Z="1.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.1" />
                  <Point X="-3.726263475507" Y="1.483229751464" Z="1.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.1" />
                  <Point X="-4.214041974711" Y="1.535543563178" Z="1.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.1" />
                  <Point X="-4.905048523348" Y="1.288071814211" Z="1.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.1" />
                  <Point X="-5.022391214307" Y="0.703009803705" Z="1.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.1" />
                  <Point X="-4.417009196047" Y="0.274266521766" Z="1.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.1" />
                  <Point X="-3.560840698209" Y="0.038158428334" Z="1.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.1" />
                  <Point X="-3.543567756891" Y="0.012923425782" Z="1.1" />
                  <Point X="-3.539556741714" Y="0" Z="1.1" />
                  <Point X="-3.544796844244" Y="-0.016883525278" Z="1.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.1" />
                  <Point X="-3.564527896915" Y="-0.0407175548" Z="1.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.1" />
                  <Point X="-4.219878592726" Y="-0.22144553499" Z="1.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.1" />
                  <Point X="-5.016335148551" Y="-0.754229780996" Z="1.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.1" />
                  <Point X="-4.905764212068" Y="-1.290721705598" Z="1.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.1" />
                  <Point X="-4.141160748452" Y="-1.428247132395" Z="1.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.1" />
                  <Point X="-3.204158381999" Y="-1.315691952941" Z="1.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.1" />
                  <Point X="-3.197040356934" Y="-1.33929957843" Z="1.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.1" />
                  <Point X="-3.765115447733" Y="-1.785533286564" Z="1.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.1" />
                  <Point X="-4.336627752738" Y="-2.630469906474" Z="1.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.1" />
                  <Point X="-4.012738268183" Y="-3.102200836232" Z="1.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.1" />
                  <Point X="-3.303192867233" Y="-2.977160678785" Z="1.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.1" />
                  <Point X="-2.563012786996" Y="-2.565317682136" Z="1.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.1" />
                  <Point X="-2.878256334383" Y="-3.131885515259" Z="1.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.1" />
                  <Point X="-3.068001335584" Y="-4.040813183639" Z="1.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.1" />
                  <Point X="-2.641610480335" Y="-4.331593202872" Z="1.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.1" />
                  <Point X="-2.353609625638" Y="-4.322466546257" Z="1.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.1" />
                  <Point X="-2.08010266183" Y="-4.058818030071" Z="1.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.1" />
                  <Point X="-1.792895686009" Y="-3.995578102255" Z="1.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.1" />
                  <Point X="-1.526541098108" Y="-4.120247266256" Z="1.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.1" />
                  <Point X="-1.391121471935" Y="-4.381300202324" Z="1.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.1" />
                  <Point X="-1.385785546148" Y="-4.672036676465" Z="1.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.1" />
                  <Point X="-1.245607536643" Y="-4.922597360672" Z="1.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.1" />
                  <Point X="-0.947623907336" Y="-4.988538066987" Z="1.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.1" />
                  <Point X="-0.643987696411" Y="-4.365578887507" Z="1.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.1" />
                  <Point X="-0.324346641434" Y="-3.385152220032" Z="1.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.1" />
                  <Point X="-0.109849117132" Y="-3.238332480585" Z="1.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.1" />
                  <Point X="0.143509962229" Y="-3.248779564703" Z="1.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.1" />
                  <Point X="0.346099217998" Y="-3.416493535736" Z="1.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.1" />
                  <Point X="0.59076716399" Y="-4.166957142309" Z="1.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.1" />
                  <Point X="0.919819086315" Y="-4.995205841244" Z="1.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.1" />
                  <Point X="1.099425224657" Y="-4.958771252953" Z="1.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.1" />
                  <Point X="1.081794336177" Y="-4.218194185799" Z="1.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.1" />
                  <Point X="0.987827732379" Y="-3.132672820393" Z="1.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.1" />
                  <Point X="1.113108046062" Y="-2.940559569679" Z="1.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.1" />
                  <Point X="1.323170850173" Y="-2.863526186089" Z="1.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.1" />
                  <Point X="1.544949780911" Y="-2.931837960871" Z="1.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.1" />
                  <Point X="2.081630837233" Y="-3.570238132594" Z="1.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.1" />
                  <Point X="2.772628877895" Y="-4.255072887878" Z="1.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.1" />
                  <Point X="2.964464074502" Y="-4.123720328936" Z="1.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.1" />
                  <Point X="2.710375638829" Y="-3.48290891707" Z="1.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.1" />
                  <Point X="2.249131556001" Y="-2.599898702348" Z="1.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.1" />
                  <Point X="2.285726818088" Y="-2.404523966139" Z="1.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.1" />
                  <Point X="2.428374412234" Y="-2.273174491879" Z="1.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.1" />
                  <Point X="2.628608101165" Y="-2.25431645349" Z="1.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.1" />
                  <Point X="3.304504680037" Y="-2.607373861446" Z="1.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.1" />
                  <Point X="4.164017622469" Y="-2.905985564696" Z="1.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.1" />
                  <Point X="4.33005993411" Y="-2.652239733005" Z="1.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.1" />
                  <Point X="3.876120150157" Y="-2.138966964149" Z="1.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.1" />
                  <Point X="3.135827845674" Y="-1.526065519844" Z="1.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.1" />
                  <Point X="3.101171842191" Y="-1.36148261624" Z="1.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.1" />
                  <Point X="3.170153695898" Y="-1.21261035632" Z="1.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.1" />
                  <Point X="3.320578854544" Y="-1.133030760658" Z="1.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.1" />
                  <Point X="4.052997647903" Y="-1.20198136441" Z="1.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.1" />
                  <Point X="4.954831285412" Y="-1.104840104145" Z="1.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.1" />
                  <Point X="5.023485095959" Y="-0.731863745663" Z="1.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.1" />
                  <Point X="4.484345920586" Y="-0.418126596676" Z="1.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.1" />
                  <Point X="3.695552349603" Y="-0.190522295291" Z="1.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.1" />
                  <Point X="3.624002504656" Y="-0.127275991481" Z="1.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.1" />
                  <Point X="3.589456653697" Y="-0.041887512346" Z="1.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.1" />
                  <Point X="3.591914796727" Y="0.05472301887" Z="1.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.1" />
                  <Point X="3.631376933744" Y="0.136672747094" Z="1.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.1" />
                  <Point X="3.707843064749" Y="0.197626548951" Z="1.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.1" />
                  <Point X="4.311621896721" Y="0.371845336093" Z="1.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.1" />
                  <Point X="5.010686173246" Y="0.808919012165" Z="1.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.1" />
                  <Point X="4.924718082841" Y="1.22820116975" Z="1.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.1" />
                  <Point X="4.266127625506" Y="1.327741888977" Z="1.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.1" />
                  <Point X="3.409785997122" Y="1.229073008235" Z="1.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.1" />
                  <Point X="3.32951332376" Y="1.256673989802" Z="1.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.1" />
                  <Point X="3.272097352423" Y="1.315046057863" Z="1.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.1" />
                  <Point X="3.241252780867" Y="1.395221316736" Z="1.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.1" />
                  <Point X="3.245783827009" Y="1.475944138211" Z="1.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.1" />
                  <Point X="3.287845491788" Y="1.552011922185" Z="1.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.1" />
                  <Point X="3.804746905192" Y="1.962103904731" Z="1.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.1" />
                  <Point X="4.328855742611" Y="2.650911344313" Z="1.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.1" />
                  <Point X="4.104550421894" Y="2.986467718283" Z="1.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.1" />
                  <Point X="3.355207257399" Y="2.755049860427" Z="1.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.1" />
                  <Point X="2.464401441457" Y="2.254837764998" Z="1.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.1" />
                  <Point X="2.390267388543" Y="2.250270953064" Z="1.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.1" />
                  <Point X="2.324306887445" Y="2.2782329323" Z="1.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.1" />
                  <Point X="2.272525667935" Y="2.332717972937" Z="1.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.1" />
                  <Point X="2.249158749695" Y="2.39949105529" Z="1.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.1" />
                  <Point X="2.25769017766" Y="2.475068134425" Z="1.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.1" />
                  <Point X="2.640575492432" Y="3.156931824556" Z="1.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.1" />
                  <Point X="2.916142736746" Y="4.153367935734" Z="1.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.1" />
                  <Point X="2.528209082677" Y="4.400285752575" Z="1.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.1" />
                  <Point X="2.122546016308" Y="4.609821055033" Z="1.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.1" />
                  <Point X="1.701999664586" Y="4.781093086162" Z="1.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.1" />
                  <Point X="1.146191586978" Y="4.937750238619" Z="1.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.1" />
                  <Point X="0.483439660544" Y="5.045931855069" Z="1.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.1" />
                  <Point X="0.109459348366" Y="4.763632343886" Z="1.1" />
                  <Point X="0" Y="4.355124473572" Z="1.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>