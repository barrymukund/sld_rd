<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#165" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1878" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995282226562" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.752753173828" Y="-29.21956640625" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542363037109" Y="-28.467375" />
                  <Point X="0.464929443359" Y="-28.35580859375" />
                  <Point X="0.378635223389" Y="-28.231474609375" />
                  <Point X="0.35674887085" Y="-28.209017578125" />
                  <Point X="0.330493835449" Y="-28.189775390625" />
                  <Point X="0.302494567871" Y="-28.175669921875" />
                  <Point X="0.182670761108" Y="-28.138482421875" />
                  <Point X="0.04913558197" Y="-28.097037109375" />
                  <Point X="0.020983318329" Y="-28.092767578125" />
                  <Point X="-0.008658161163" Y="-28.092765625" />
                  <Point X="-0.036824237823" Y="-28.09703515625" />
                  <Point X="-0.156648040771" Y="-28.134224609375" />
                  <Point X="-0.290183380127" Y="-28.17566796875" />
                  <Point X="-0.318184448242" Y="-28.189775390625" />
                  <Point X="-0.344439941406" Y="-28.20901953125" />
                  <Point X="-0.366323730469" Y="-28.2314765625" />
                  <Point X="-0.443757324219" Y="-28.343044921875" />
                  <Point X="-0.530051391602" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.762120117188" Y="-29.30047265625" />
                  <Point X="-0.916584594727" Y="-29.87694140625" />
                  <Point X="-0.925639648438" Y="-29.87518359375" />
                  <Point X="-1.079325561523" Y="-29.845353515625" />
                  <Point X="-1.214963012695" Y="-29.810455078125" />
                  <Point X="-1.24641784668" Y="-29.802361328125" />
                  <Point X="-1.237768554688" Y="-29.7366640625" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.245134643555" Y="-29.372314453125" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323645019531" Y="-29.131203125" />
                  <Point X="-1.433963623047" Y="-29.03445703125" />
                  <Point X="-1.556905883789" Y="-28.926638671875" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886108398" Y="-28.897994140625" />
                  <Point X="-1.643027954102" Y="-28.890966796875" />
                  <Point X="-1.7894453125" Y="-28.88137109375" />
                  <Point X="-1.952616943359" Y="-28.87067578125" />
                  <Point X="-1.98341418457" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.16466015625" Y="-28.9763203125" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312789306641" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.453635986328" Y="-29.253939453125" />
                  <Point X="-2.480147460938" Y="-29.288490234375" />
                  <Point X="-2.576419677734" Y="-29.228880859375" />
                  <Point X="-2.801723876953" Y="-29.08937890625" />
                  <Point X="-2.989504150391" Y="-28.94479296875" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.775835205078" Y="-28.2864296875" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858398438" Y="-27.647650390625" />
                  <Point X="-2.406587890625" Y="-27.61612109375" />
                  <Point X="-2.405576660156" Y="-27.58518359375" />
                  <Point X="-2.414563476562" Y="-27.55556640625" />
                  <Point X="-2.428783691406" Y="-27.526736328125" />
                  <Point X="-2.446811523438" Y="-27.50158203125" />
                  <Point X="-2.464155029297" Y="-27.484240234375" />
                  <Point X="-2.489305419922" Y="-27.466216796875" />
                  <Point X="-2.518134765625" Y="-27.451998046875" />
                  <Point X="-2.547754394531" Y="-27.443013671875" />
                  <Point X="-2.578690673828" Y="-27.444025390625" />
                  <Point X="-2.610218017578" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.318773193359" Y="-27.853560546875" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.904869140625" Y="-28.027705078125" />
                  <Point X="-4.082862792969" Y="-27.793857421875" />
                  <Point X="-4.217492675781" Y="-27.568103515625" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.721969482422" Y="-26.97119921875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577148438" Y="-26.475595703125" />
                  <Point X="-3.066612548828" Y="-26.44846484375" />
                  <Point X="-3.053856689453" Y="-26.419833984375" />
                  <Point X="-3.046152099609" Y="-26.3900859375" />
                  <Point X="-3.043347900391" Y="-26.359658203125" />
                  <Point X="-3.045556640625" Y="-26.327986328125" />
                  <Point X="-3.052557617188" Y="-26.2982421875" />
                  <Point X="-3.068640136719" Y="-26.2722578125" />
                  <Point X="-3.089473144531" Y="-26.24830078125" />
                  <Point X="-3.112974365234" Y="-26.228765625" />
                  <Point X="-3.13945703125" Y="-26.2131796875" />
                  <Point X="-3.168721923828" Y="-26.201955078125" />
                  <Point X="-3.200609130859" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-4.089846679687" Y="-26.307330078125" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.764463378906" Y="-26.26519140625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.869697753906" Y="-25.7436015625" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.234609863281" Y="-25.408439453125" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.510474853516" Y="-25.2112109375" />
                  <Point X="-3.481781005859" Y="-25.194880859375" />
                  <Point X="-3.468558105469" Y="-25.185798828125" />
                  <Point X="-3.442112548828" Y="-25.16412890625" />
                  <Point X="-3.425921386719" Y="-25.14710546875" />
                  <Point X="-3.414402587891" Y="-25.12662890625" />
                  <Point X="-3.402602050781" Y="-25.097794921875" />
                  <Point X="-3.398008300781" Y="-25.083396484375" />
                  <Point X="-3.390885253906" Y="-25.052865234375" />
                  <Point X="-3.388401123047" Y="-25.0314765625" />
                  <Point X="-3.390797851562" Y="-25.010076171875" />
                  <Point X="-3.397419921875" Y="-24.981158203125" />
                  <Point X="-3.401939941406" Y="-24.96677734375" />
                  <Point X="-3.414240966797" Y="-24.936330078125" />
                  <Point X="-3.425666748047" Y="-24.915802734375" />
                  <Point X="-3.441780273438" Y="-24.89870703125" />
                  <Point X="-3.466724365234" Y="-24.878078125" />
                  <Point X="-3.479891845703" Y="-24.868943359375" />
                  <Point X="-3.510087646484" Y="-24.8515703125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.314908203125" Y="-24.63260546875" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.869352539063" Y="-24.326216796875" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.75278515625" Y="-23.758421875" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.268687011719" Y="-23.633984375" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985351562" Y="-23.700658203125" />
                  <Point X="-3.723422363281" Y="-23.698771484375" />
                  <Point X="-3.703140136719" Y="-23.694736328125" />
                  <Point X="-3.674089355469" Y="-23.685578125" />
                  <Point X="-3.641714111328" Y="-23.675369140625" />
                  <Point X="-3.622773681641" Y="-23.66703515625" />
                  <Point X="-3.604030761719" Y="-23.656212890625" />
                  <Point X="-3.587352294922" Y="-23.643984375" />
                  <Point X="-3.573715576172" Y="-23.62843359375" />
                  <Point X="-3.561301513672" Y="-23.610705078125" />
                  <Point X="-3.551351318359" Y="-23.592568359375" />
                  <Point X="-3.539694580078" Y="-23.56442578125" />
                  <Point X="-3.526703857422" Y="-23.533064453125" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.515804443359" Y="-23.47125" />
                  <Point X="-3.518951660156" Y="-23.4508046875" />
                  <Point X="-3.524553955078" Y="-23.4298984375" />
                  <Point X="-3.532050048828" Y="-23.410623046875" />
                  <Point X="-3.546115234375" Y="-23.383603515625" />
                  <Point X="-3.561789794922" Y="-23.353494140625" />
                  <Point X="-3.573281494141" Y="-23.336294921875" />
                  <Point X="-3.587193603516" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.050711425781" Y="-22.961205078125" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.255479980469" Y="-22.56500390625" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.891216552734" Y="-22.022205078125" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.515597900391" Y="-21.9769609375" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187729980469" Y="-22.163658203125" />
                  <Point X="-3.167088378906" Y="-22.17016796875" />
                  <Point X="-3.146793457031" Y="-22.174205078125" />
                  <Point X="-3.106333740234" Y="-22.177744140625" />
                  <Point X="-3.061244140625" Y="-22.181689453125" />
                  <Point X="-3.040561523438" Y="-22.18123828125" />
                  <Point X="-3.019102783203" Y="-22.178412109375" />
                  <Point X="-2.999013916016" Y="-22.17349609375" />
                  <Point X="-2.980465087891" Y="-22.16434765625" />
                  <Point X="-2.962210693359" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.917359130859" Y="-22.111052734375" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.846975585938" Y="-21.923419921875" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-3.068572753906" Y="-21.47417578125" />
                  <Point X="-3.183333007812" Y="-21.27540625" />
                  <Point X="-3.004242431641" Y="-21.13809765625" />
                  <Point X="-2.70062109375" Y="-20.905314453125" />
                  <Point X="-2.401482910156" Y="-20.739119140625" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.137771728516" Y="-20.64700390625" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028891967773" Y="-20.78519921875" />
                  <Point X="-2.01231237793" Y="-20.799111328125" />
                  <Point X="-1.995115234375" Y="-20.810603515625" />
                  <Point X="-1.950083618164" Y="-20.834046875" />
                  <Point X="-1.899899169922" Y="-20.860171875" />
                  <Point X="-1.880625732422" Y="-20.86766796875" />
                  <Point X="-1.859719238281" Y="-20.873271484375" />
                  <Point X="-1.839269042969" Y="-20.876419921875" />
                  <Point X="-1.818622314453" Y="-20.87506640625" />
                  <Point X="-1.797307373047" Y="-20.871306640625" />
                  <Point X="-1.777452026367" Y="-20.865517578125" />
                  <Point X="-1.730548583984" Y="-20.846087890625" />
                  <Point X="-1.678278076172" Y="-20.8244375" />
                  <Point X="-1.660147827148" Y="-20.814490234375" />
                  <Point X="-1.642417602539" Y="-20.802076171875" />
                  <Point X="-1.626865356445" Y="-20.7884375" />
                  <Point X="-1.614633789062" Y="-20.7717578125" />
                  <Point X="-1.603811523438" Y="-20.753013671875" />
                  <Point X="-1.595480102539" Y="-20.734078125" />
                  <Point X="-1.580213989258" Y="-20.68566015625" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.580329101562" Y="-20.39751953125" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.342686035156" Y="-20.300390625" />
                  <Point X="-0.949623474121" Y="-20.19019140625" />
                  <Point X="-0.586988891602" Y="-20.147748046875" />
                  <Point X="-0.294711364746" Y="-20.113541015625" />
                  <Point X="-0.225296463013" Y="-20.3726015625" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113975525" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907631" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425926208" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.248066482544" Y="-20.3335703125" />
                  <Point X="0.307419586182" Y="-20.112060546875" />
                  <Point X="0.500843505859" Y="-20.132318359375" />
                  <Point X="0.84403112793" Y="-20.168259765625" />
                  <Point X="1.144072631836" Y="-20.24069921875" />
                  <Point X="1.481038696289" Y="-20.3220546875" />
                  <Point X="1.675537475586" Y="-20.392599609375" />
                  <Point X="1.894645141602" Y="-20.472072265625" />
                  <Point X="2.083485351562" Y="-20.56038671875" />
                  <Point X="2.294560546875" Y="-20.659099609375" />
                  <Point X="2.477031494141" Y="-20.76540625" />
                  <Point X="2.680973876953" Y="-20.88422265625" />
                  <Point X="2.853033691406" Y="-21.006583984375" />
                  <Point X="2.943259033203" Y="-21.07074609375" />
                  <Point X="2.555080078125" Y="-21.74309375" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.122922851562" Y="-22.521916015625" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107727783203" Y="-22.619048828125" />
                  <Point X="2.111687011719" Y="-22.6518828125" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.140070800781" Y="-22.752529296875" />
                  <Point X="2.160387207031" Y="-22.782470703125" />
                  <Point X="2.183028564453" Y="-22.815837890625" />
                  <Point X="2.19446484375" Y="-22.829671875" />
                  <Point X="2.22159765625" Y="-22.85440625" />
                  <Point X="2.251538818359" Y="-22.87472265625" />
                  <Point X="2.28490625" Y="-22.897365234375" />
                  <Point X="2.304947509766" Y="-22.9077265625" />
                  <Point X="2.327037353516" Y="-22.915994140625" />
                  <Point X="2.34896484375" Y="-22.921337890625" />
                  <Point X="2.381798828125" Y="-22.925296875" />
                  <Point X="2.418389648438" Y="-22.929708984375" />
                  <Point X="2.436468994141" Y="-22.93015625" />
                  <Point X="2.473208007812" Y="-22.925830078125" />
                  <Point X="2.511178710938" Y="-22.91567578125" />
                  <Point X="2.553494384766" Y="-22.904359375" />
                  <Point X="2.5652890625" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.375107910156" Y="-22.435724609375" />
                  <Point X="3.967325927734" Y="-22.09380859375" />
                  <Point X="4.002296875" Y="-22.14241015625" />
                  <Point X="4.123274414062" Y="-22.310541015625" />
                  <Point X="4.219190917969" Y="-22.469044921875" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.767294677734" Y="-22.919869140625" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221422363281" Y="-23.33976171875" />
                  <Point X="3.203974365234" Y="-23.358373046875" />
                  <Point X="3.176646728516" Y="-23.3940234375" />
                  <Point X="3.146192138672" Y="-23.43375390625" />
                  <Point X="3.136606201172" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.111450195312" Y="-23.519314453125" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.106095947266" Y="-23.66873046875" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679931641" Y="-23.7310234375" />
                  <Point X="3.136282958984" Y="-23.764259765625" />
                  <Point X="3.159011230469" Y="-23.7988046875" />
                  <Point X="3.184340576172" Y="-23.8373046875" />
                  <Point X="3.198893310547" Y="-23.854548828125" />
                  <Point X="3.216137207031" Y="-23.870638671875" />
                  <Point X="3.234345703125" Y="-23.88396484375" />
                  <Point X="3.267282226562" Y="-23.902505859375" />
                  <Point X="3.303987792969" Y="-23.92316796875" />
                  <Point X="3.320520996094" Y="-23.930498046875" />
                  <Point X="3.356120361328" Y="-23.9405625" />
                  <Point X="3.400652832031" Y="-23.946447265625" />
                  <Point X="3.450281005859" Y="-23.953005859375" />
                  <Point X="3.462698242188" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.235396484375" Y="-23.854646484375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.793978027344" Y="-23.853763671875" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.876162597656" Y="-24.26133203125" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.331432617187" Y="-24.50566015625" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704786865234" Y="-24.674416015625" />
                  <Point X="3.681547119141" Y="-24.68493359375" />
                  <Point X="3.637795410156" Y="-24.71022265625" />
                  <Point X="3.589037109375" Y="-24.738404296875" />
                  <Point X="3.574314941406" Y="-24.748900390625" />
                  <Point X="3.547530029297" Y="-24.774423828125" />
                  <Point X="3.521279052734" Y="-24.807875" />
                  <Point X="3.492024169922" Y="-24.84515234375" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.470526855469" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.454930175781" Y="-24.953087890625" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.021876953125" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.453929199219" Y="-25.104244140625" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470526855469" Y="-25.1766640625" />
                  <Point X="3.480301025391" Y="-25.19812890625" />
                  <Point X="3.492024169922" Y="-25.217408203125" />
                  <Point X="3.518275146484" Y="-25.250859375" />
                  <Point X="3.547530029297" Y="-25.28813671875" />
                  <Point X="3.559999267578" Y="-25.30123828125" />
                  <Point X="3.589035888672" Y="-25.32415625" />
                  <Point X="3.632787597656" Y="-25.3494453125" />
                  <Point X="3.681545898438" Y="-25.37762890625" />
                  <Point X="3.692708984375" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.401789550781" Y="-25.57575390625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.884033691406" Y="-25.756302734375" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.816296875" Y="-26.118427734375" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.137438476562" Y="-26.09731640625" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374658935547" Y="-26.005509765625" />
                  <Point X="3.288789794922" Y="-26.024173828125" />
                  <Point X="3.193094726563" Y="-26.04497265625" />
                  <Point X="3.163973876953" Y="-26.05659765625" />
                  <Point X="3.136147216797" Y="-26.073490234375" />
                  <Point X="3.112396972656" Y="-26.0939609375" />
                  <Point X="3.060494384766" Y="-26.156384765625" />
                  <Point X="3.002652832031" Y="-26.22594921875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.962318603516" Y="-26.386205078125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450439453" Y="-26.567998046875" />
                  <Point X="3.023971679688" Y="-26.6419140625" />
                  <Point X="3.076930664062" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.746507324219" Y="-27.248837890625" />
                  <Point X="4.213122070312" Y="-27.6068828125" />
                  <Point X="4.20658984375" Y="-27.617453125" />
                  <Point X="4.124813964844" Y="-27.74978125" />
                  <Point X="4.04472265625" Y="-27.863578125" />
                  <Point X="4.028981201172" Y="-27.8859453125" />
                  <Point X="3.43602734375" Y="-27.543603515625" />
                  <Point X="2.800954589844" Y="-27.176943359375" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754224609375" Y="-27.159826171875" />
                  <Point X="2.652026611328" Y="-27.141369140625" />
                  <Point X="2.538134277344" Y="-27.12080078125" />
                  <Point X="2.506783935547" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.444834228516" Y="-27.135177734375" />
                  <Point X="2.359932617188" Y="-27.179859375" />
                  <Point X="2.265316162109" Y="-27.22965625" />
                  <Point X="2.242385009766" Y="-27.246548828125" />
                  <Point X="2.221425537109" Y="-27.2675078125" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.159848632812" Y="-27.375341796875" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.114132080078" Y="-27.66545703125" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.560435302734" Y="-28.533822265625" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.781837890625" Y="-29.11165234375" />
                  <Point X="2.701764648438" Y="-29.163482421875" />
                  <Point X="2.243165527344" Y="-28.56582421875" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721923461914" Y="-27.900556640625" />
                  <Point X="1.621128662109" Y="-27.835755859375" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365478516" Y="-27.743435546875" />
                  <Point X="1.417100952148" Y="-27.741119140625" />
                  <Point X="1.306864501953" Y="-27.75126171875" />
                  <Point X="1.184014038086" Y="-27.76256640625" />
                  <Point X="1.156361938477" Y="-27.7693984375" />
                  <Point X="1.12897668457" Y="-27.7807421875" />
                  <Point X="1.104594848633" Y="-27.795462890625" />
                  <Point X="1.019473388672" Y="-27.866240234375" />
                  <Point X="0.924611206055" Y="-27.945115234375" />
                  <Point X="0.904141479492" Y="-27.96886328125" />
                  <Point X="0.887249206543" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.850173339844" Y="-28.142904296875" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.935540893555" Y="-29.202697265625" />
                  <Point X="1.022065490723" Y="-29.859916015625" />
                  <Point X="0.975699707031" Y="-29.870078125" />
                  <Point X="0.929315612793" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058425415039" Y="-29.75263671875" />
                  <Point X="-1.14124609375" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.151960083008" Y="-29.35378125" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026245117" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230573486328" Y="-29.094861328125" />
                  <Point X="-1.250208862305" Y="-29.070935546875" />
                  <Point X="-1.261007446289" Y="-29.05977734375" />
                  <Point X="-1.371326171875" Y="-28.96303125" />
                  <Point X="-1.494268310547" Y="-28.855212890625" />
                  <Point X="-1.506739868164" Y="-28.84596484375" />
                  <Point X="-1.533023071289" Y="-28.82962109375" />
                  <Point X="-1.546834472656" Y="-28.822525390625" />
                  <Point X="-1.576531494141" Y="-28.810224609375" />
                  <Point X="-1.591316040039" Y="-28.805474609375" />
                  <Point X="-1.621457885742" Y="-28.798447265625" />
                  <Point X="-1.636815307617" Y="-28.796169921875" />
                  <Point X="-1.783232666016" Y="-28.78657421875" />
                  <Point X="-1.946404296875" Y="-28.77587890625" />
                  <Point X="-1.961928344727" Y="-28.7761328125" />
                  <Point X="-1.992725585938" Y="-28.779166015625" />
                  <Point X="-2.007998779297" Y="-28.7819453125" />
                  <Point X="-2.039047729492" Y="-28.790263671875" />
                  <Point X="-2.053666992188" Y="-28.795494140625" />
                  <Point X="-2.081861328125" Y="-28.808267578125" />
                  <Point X="-2.095436767578" Y="-28.815810546875" />
                  <Point X="-2.217439453125" Y="-28.897330078125" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359685302734" Y="-28.9927578125" />
                  <Point X="-2.380448730469" Y="-29.01013671875" />
                  <Point X="-2.402762207031" Y="-29.032775390625" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.526408447266" Y="-29.148109375" />
                  <Point X="-2.747610595703" Y="-29.011146484375" />
                  <Point X="-2.931546386719" Y="-28.869521484375" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.693562744141" Y="-28.3339296875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947021484" Y="-27.68111328125" />
                  <Point X="-2.319683105469" Y="-27.666181640625" />
                  <Point X="-2.313412597656" Y="-27.63465234375" />
                  <Point X="-2.311638671875" Y="-27.619224609375" />
                  <Point X="-2.310627441406" Y="-27.588287109375" />
                  <Point X="-2.314669433594" Y="-27.557599609375" />
                  <Point X="-2.32365625" Y="-27.527982421875" />
                  <Point X="-2.329363769531" Y="-27.51354296875" />
                  <Point X="-2.343583984375" Y="-27.484712890625" />
                  <Point X="-2.351566894531" Y="-27.471396484375" />
                  <Point X="-2.369594726562" Y="-27.4462421875" />
                  <Point X="-2.379639648438" Y="-27.434404296875" />
                  <Point X="-2.396983154297" Y="-27.4170625" />
                  <Point X="-2.408817871094" Y="-27.407021484375" />
                  <Point X="-2.433968261719" Y="-27.388998046875" />
                  <Point X="-2.447283935547" Y="-27.381015625" />
                  <Point X="-2.47611328125" Y="-27.366796875" />
                  <Point X="-2.490559570312" Y="-27.361087890625" />
                  <Point X="-2.520179199219" Y="-27.352103515625" />
                  <Point X="-2.550859619141" Y="-27.348064453125" />
                  <Point X="-2.581795898438" Y="-27.349076171875" />
                  <Point X="-2.597225097656" Y="-27.3508515625" />
                  <Point X="-2.628752441406" Y="-27.357123046875" />
                  <Point X="-2.643683105469" Y="-27.36138671875" />
                  <Point X="-2.672648681641" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.366273193359" Y="-27.7712890625" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-3.829275878906" Y="-27.970166015625" />
                  <Point X="-4.004021484375" Y="-27.7405859375" />
                  <Point X="-4.135899902344" Y="-27.5194453125" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.664137207031" Y="-27.046568359375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036482421875" Y="-26.5633125" />
                  <Point X="-3.015104980469" Y="-26.540392578125" />
                  <Point X="-3.005367431641" Y="-26.528044921875" />
                  <Point X="-2.987402832031" Y="-26.5009140625" />
                  <Point X="-2.979835449219" Y="-26.487126953125" />
                  <Point X="-2.967079589844" Y="-26.45849609375" />
                  <Point X="-2.961891113281" Y="-26.44365234375" />
                  <Point X="-2.954186523438" Y="-26.413904296875" />
                  <Point X="-2.951552978516" Y="-26.3988046875" />
                  <Point X="-2.948748779297" Y="-26.368376953125" />
                  <Point X="-2.948578125" Y="-26.353048828125" />
                  <Point X="-2.950786865234" Y="-26.321376953125" />
                  <Point X="-2.953083740234" Y="-26.306220703125" />
                  <Point X="-2.960084716797" Y="-26.2764765625" />
                  <Point X="-2.971778076172" Y="-26.24824609375" />
                  <Point X="-2.987860595703" Y="-26.22226171875" />
                  <Point X="-2.996953857422" Y="-26.209919921875" />
                  <Point X="-3.017786865234" Y="-26.185962890625" />
                  <Point X="-3.028745849609" Y="-26.175244140625" />
                  <Point X="-3.052247070312" Y="-26.155708984375" />
                  <Point X="-3.064789306641" Y="-26.146892578125" />
                  <Point X="-3.091271972656" Y="-26.131306640625" />
                  <Point X="-3.105436279297" Y="-26.12448046875" />
                  <Point X="-3.134701171875" Y="-26.113255859375" />
                  <Point X="-3.149801757812" Y="-26.108857421875" />
                  <Point X="-3.181688964844" Y="-26.102376953125" />
                  <Point X="-3.197305419922" Y="-26.10053125" />
                  <Point X="-3.228625488281" Y="-26.09944140625" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-4.102246582031" Y="-26.213142578125" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.672418457031" Y="-26.2416796875" />
                  <Point X="-4.740763183594" Y="-25.97411328125" />
                  <Point X="-4.775654785156" Y="-25.73015234375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.210021972656" Y="-25.500203125" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.496787841797" Y="-25.3082890625" />
                  <Point X="-3.47438671875" Y="-25.29908984375" />
                  <Point X="-3.463485839844" Y="-25.293775390625" />
                  <Point X="-3.434791992188" Y="-25.2774453125" />
                  <Point X="-3.427995849609" Y="-25.273189453125" />
                  <Point X="-3.408346191406" Y="-25.25928125" />
                  <Point X="-3.381900634766" Y="-25.237611328125" />
                  <Point X="-3.373275878906" Y="-25.229599609375" />
                  <Point X="-3.357084716797" Y="-25.212576171875" />
                  <Point X="-3.343123046875" Y="-25.193681640625" />
                  <Point X="-3.331604248047" Y="-25.173205078125" />
                  <Point X="-3.326480712891" Y="-25.162611328125" />
                  <Point X="-3.314680175781" Y="-25.13377734375" />
                  <Point X="-3.312096679688" Y="-25.126669921875" />
                  <Point X="-3.305492675781" Y="-25.10498046875" />
                  <Point X="-3.298369628906" Y="-25.07444921875" />
                  <Point X="-3.29651953125" Y="-25.06382421875" />
                  <Point X="-3.294035400391" Y="-25.042435546875" />
                  <Point X="-3.293991455078" Y="-25.02090234375" />
                  <Point X="-3.296388183594" Y="-24.999501953125" />
                  <Point X="-3.298194824219" Y="-24.98887109375" />
                  <Point X="-3.304816894531" Y="-24.959953125" />
                  <Point X="-3.306791015625" Y="-24.952671875" />
                  <Point X="-3.313856933594" Y="-24.93119140625" />
                  <Point X="-3.326157958984" Y="-24.900744140625" />
                  <Point X="-3.331233398438" Y="-24.890126953125" />
                  <Point X="-3.342659179688" Y="-24.869599609375" />
                  <Point X="-3.35653515625" Y="-24.850642578125" />
                  <Point X="-3.372648681641" Y="-24.833546875" />
                  <Point X="-3.381236572266" Y="-24.825498046875" />
                  <Point X="-3.406180664062" Y="-24.804869140625" />
                  <Point X="-3.412573974609" Y="-24.800021484375" />
                  <Point X="-3.432515625" Y="-24.786599609375" />
                  <Point X="-3.462711425781" Y="-24.7692265625" />
                  <Point X="-3.473796142578" Y="-24.763775390625" />
                  <Point X="-3.496584472656" Y="-24.75435546875" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.2903203125" Y="-24.540841796875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.775375976562" Y="-24.340123046875" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.661092285156" Y="-23.78326953125" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.281087402344" Y="-23.728171875" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057373047" Y="-23.795634765625" />
                  <Point X="-3.736704589844" Y="-23.795296875" />
                  <Point X="-3.715141601563" Y="-23.79341015625" />
                  <Point X="-3.704885498047" Y="-23.7919453125" />
                  <Point X="-3.684603271484" Y="-23.78791015625" />
                  <Point X="-3.674577148438" Y="-23.78533984375" />
                  <Point X="-3.645526367188" Y="-23.776181640625" />
                  <Point X="-3.613151123047" Y="-23.76597265625" />
                  <Point X="-3.603453125" Y="-23.76232421875" />
                  <Point X="-3.584512695312" Y="-23.753990234375" />
                  <Point X="-3.575270263672" Y="-23.7493046875" />
                  <Point X="-3.55652734375" Y="-23.738482421875" />
                  <Point X="-3.547858154297" Y="-23.732826171875" />
                  <Point X="-3.5311796875" Y="-23.72059765625" />
                  <Point X="-3.515925292969" Y="-23.706619140625" />
                  <Point X="-3.502288574219" Y="-23.691068359375" />
                  <Point X="-3.495896972656" Y="-23.682923828125" />
                  <Point X="-3.483482910156" Y="-23.6651953125" />
                  <Point X="-3.478012451172" Y="-23.6563984375" />
                  <Point X="-3.468062255859" Y="-23.63826171875" />
                  <Point X="-3.463582519531" Y="-23.628921875" />
                  <Point X="-3.45192578125" Y="-23.600779296875" />
                  <Point X="-3.438935058594" Y="-23.56941796875" />
                  <Point X="-3.435499267578" Y="-23.5596484375" />
                  <Point X="-3.429711181641" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.498103515625" />
                  <Point X="-3.4210078125" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057617188" Y="-23.4363515625" />
                  <Point X="-3.427189208984" Y="-23.42621484375" />
                  <Point X="-3.432791503906" Y="-23.40530859375" />
                  <Point X="-3.436013671875" Y="-23.39546484375" />
                  <Point X="-3.443509765625" Y="-23.376189453125" />
                  <Point X="-3.447783691406" Y="-23.3667578125" />
                  <Point X="-3.461848876953" Y="-23.33973828125" />
                  <Point X="-3.4775234375" Y="-23.30962890625" />
                  <Point X="-3.482799316406" Y="-23.300716796875" />
                  <Point X="-3.494291015625" Y="-23.283517578125" />
                  <Point X="-3.500506835938" Y="-23.27523046875" />
                  <Point X="-3.514418945312" Y="-23.258650390625" />
                  <Point X="-3.521498291016" Y="-23.251091796875" />
                  <Point X="-3.536440429688" Y="-23.236787109375" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.99287890625" Y="-22.8858359375" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.17343359375" Y="-22.612892578125" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.816235839844" Y="-22.0805390625" />
                  <Point X="-3.726336914062" Y="-21.964986328125" />
                  <Point X="-3.563097900391" Y="-22.059232421875" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244925537109" Y="-22.242279296875" />
                  <Point X="-3.225998535156" Y="-22.250609375" />
                  <Point X="-3.216302978516" Y="-22.254259765625" />
                  <Point X="-3.195661376953" Y="-22.26076953125" />
                  <Point X="-3.185622802734" Y="-22.263341796875" />
                  <Point X="-3.165327880859" Y="-22.26737890625" />
                  <Point X="-3.155071533203" Y="-22.26884375" />
                  <Point X="-3.114611816406" Y="-22.2723828125" />
                  <Point X="-3.069522216797" Y="-22.276328125" />
                  <Point X="-3.059172363281" Y="-22.276666015625" />
                  <Point X="-3.038489746094" Y="-22.27621484375" />
                  <Point X="-3.028156982422" Y="-22.27542578125" />
                  <Point X="-3.006698242188" Y="-22.272599609375" />
                  <Point X="-2.996521240234" Y="-22.270689453125" />
                  <Point X="-2.976432373047" Y="-22.2657734375" />
                  <Point X="-2.9569921875" Y="-22.258697265625" />
                  <Point X="-2.938443359375" Y="-22.249548828125" />
                  <Point X="-2.929422851563" Y="-22.244470703125" />
                  <Point X="-2.911168457031" Y="-22.232841796875" />
                  <Point X="-2.902749755859" Y="-22.22680859375" />
                  <Point X="-2.886616943359" Y="-22.213861328125" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.850184082031" Y="-22.178228515625" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.752337158203" Y="-21.915140625" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.986300537109" Y="-21.42667578125" />
                  <Point X="-3.059387939453" Y="-21.3000859375" />
                  <Point X="-2.946439941406" Y="-21.21348828125" />
                  <Point X="-2.648378417969" Y="-20.984966796875" />
                  <Point X="-2.355345214844" Y="-20.8221640625" />
                  <Point X="-2.192524169922" Y="-20.731703125" />
                  <Point X="-2.118564208984" Y="-20.82808984375" />
                  <Point X="-2.111819824219" Y="-20.835951171875" />
                  <Point X="-2.097516357422" Y="-20.850892578125" />
                  <Point X="-2.089957275391" Y="-20.85797265625" />
                  <Point X="-2.073377685547" Y="-20.871884765625" />
                  <Point X="-2.065096191406" Y="-20.87809765625" />
                  <Point X="-2.047898925781" Y="-20.88958984375" />
                  <Point X="-2.038983398437" Y="-20.894869140625" />
                  <Point X="-1.993951782227" Y="-20.9183125" />
                  <Point X="-1.943767333984" Y="-20.9444375" />
                  <Point X="-1.934335083008" Y="-20.9487109375" />
                  <Point X="-1.915061645508" Y="-20.95620703125" />
                  <Point X="-1.905220092773" Y="-20.9594296875" />
                  <Point X="-1.884313720703" Y="-20.965033203125" />
                  <Point X="-1.874174804688" Y="-20.967166015625" />
                  <Point X="-1.853724487305" Y="-20.970314453125" />
                  <Point X="-1.83305456543" Y="-20.971216796875" />
                  <Point X="-1.812407836914" Y="-20.96986328125" />
                  <Point X="-1.802119873047" Y="-20.968623046875" />
                  <Point X="-1.780804931641" Y="-20.96486328125" />
                  <Point X="-1.770716064453" Y="-20.962509765625" />
                  <Point X="-1.750860839844" Y="-20.956720703125" />
                  <Point X="-1.741094482422" Y="-20.95328515625" />
                  <Point X="-1.694191040039" Y="-20.93385546875" />
                  <Point X="-1.641920532227" Y="-20.912205078125" />
                  <Point X="-1.632581787109" Y="-20.907724609375" />
                  <Point X="-1.614451538086" Y="-20.89777734375" />
                  <Point X="-1.605660400391" Y="-20.892310546875" />
                  <Point X="-1.587930175781" Y="-20.879896484375" />
                  <Point X="-1.579780639648" Y="-20.873501953125" />
                  <Point X="-1.564228271484" Y="-20.85986328125" />
                  <Point X="-1.550256469727" Y="-20.8446171875" />
                  <Point X="-1.538024780273" Y="-20.8279375" />
                  <Point X="-1.532361938477" Y="-20.819259765625" />
                  <Point X="-1.521539672852" Y="-20.800515625" />
                  <Point X="-1.516856201172" Y="-20.7912734375" />
                  <Point X="-1.508524780273" Y="-20.772337890625" />
                  <Point X="-1.504877075195" Y="-20.76264453125" />
                  <Point X="-1.489610717773" Y="-20.7142265625" />
                  <Point X="-1.47259765625" Y="-20.660267578125" />
                  <Point X="-1.470026123047" Y="-20.650234375" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-1.317040161133" Y="-20.39186328125" />
                  <Point X="-0.931164489746" Y="-20.2836796875" />
                  <Point X="-0.575945251465" Y="-20.242103515625" />
                  <Point X="-0.365222717285" Y="-20.21744140625" />
                  <Point X="-0.317059448242" Y="-20.397189453125" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451187134" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.339829528809" Y="-20.358158203125" />
                  <Point X="0.378190734863" Y="-20.2149921875" />
                  <Point X="0.490947967529" Y="-20.22680078125" />
                  <Point X="0.8278515625" Y="-20.262083984375" />
                  <Point X="1.12177722168" Y="-20.333046875" />
                  <Point X="1.453622314453" Y="-20.413166015625" />
                  <Point X="1.643145629883" Y="-20.48190625" />
                  <Point X="1.858245727539" Y="-20.55992578125" />
                  <Point X="2.043240478516" Y="-20.64644140625" />
                  <Point X="2.250434570312" Y="-20.74333984375" />
                  <Point X="2.429208984375" Y="-20.8474921875" />
                  <Point X="2.62942578125" Y="-20.964138671875" />
                  <Point X="2.7979765625" Y="-21.08400390625" />
                  <Point X="2.817778076172" Y="-21.0980859375" />
                  <Point X="2.472807617188" Y="-21.69559375" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.031147583008" Y="-22.497375" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017912231445" Y="-22.5485390625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755371094" Y="-22.616759765625" />
                  <Point X="2.013410888672" Y="-22.630421875" />
                  <Point X="2.017370239258" Y="-22.663255859375" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144042969" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045317871094" Y="-22.776111328125" />
                  <Point X="2.055680175781" Y="-22.79615625" />
                  <Point X="2.061459472656" Y="-22.80587109375" />
                  <Point X="2.081775878906" Y="-22.8358125" />
                  <Point X="2.104417236328" Y="-22.8691796875" />
                  <Point X="2.10980859375" Y="-22.8763671875" />
                  <Point X="2.130464355469" Y="-22.89987890625" />
                  <Point X="2.157597167969" Y="-22.92461328125" />
                  <Point X="2.168256591797" Y="-22.933017578125" />
                  <Point X="2.198197753906" Y="-22.953333984375" />
                  <Point X="2.231565185547" Y="-22.9759765625" />
                  <Point X="2.241277099609" Y="-22.98175390625" />
                  <Point X="2.261318359375" Y="-22.992115234375" />
                  <Point X="2.271647705078" Y="-22.99669921875" />
                  <Point X="2.293737548828" Y="-23.004966796875" />
                  <Point X="2.304544189453" Y="-23.00829296875" />
                  <Point X="2.326471679688" Y="-23.01363671875" />
                  <Point X="2.337592529297" Y="-23.015654296875" />
                  <Point X="2.370426513672" Y="-23.01961328125" />
                  <Point X="2.407017333984" Y="-23.024025390625" />
                  <Point X="2.416040039062" Y="-23.0246796875" />
                  <Point X="2.447578857422" Y="-23.02450390625" />
                  <Point X="2.484317871094" Y="-23.020177734375" />
                  <Point X="2.497750976562" Y="-23.01760546875" />
                  <Point X="2.535721679688" Y="-23.007451171875" />
                  <Point X="2.578037353516" Y="-22.996134765625" />
                  <Point X="2.584005371094" Y="-22.994326171875" />
                  <Point X="2.604415039063" Y="-22.986927734375" />
                  <Point X="2.627659912109" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.422607910156" Y="-22.51799609375" />
                  <Point X="3.940403564453" Y="-22.219046875" />
                  <Point X="4.04395703125" Y="-22.362962890625" />
                  <Point X="4.136885253906" Y="-22.51652734375" />
                  <Point X="3.709462402344" Y="-22.8445" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168133056641" Y="-23.260134765625" />
                  <Point X="3.152116210938" Y="-23.274787109375" />
                  <Point X="3.134668212891" Y="-23.2933984375" />
                  <Point X="3.128577392578" Y="-23.300578125" />
                  <Point X="3.101249755859" Y="-23.336228515625" />
                  <Point X="3.070795166016" Y="-23.375958984375" />
                  <Point X="3.065637451172" Y="-23.383396484375" />
                  <Point X="3.049739257812" Y="-23.41062890625" />
                  <Point X="3.034762939453" Y="-23.444455078125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.019960449219" Y="-23.493728515625" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.013055908203" Y="-23.687927734375" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034684814453" Y="-23.77139453125" />
                  <Point X="3.050287841797" Y="-23.804630859375" />
                  <Point X="3.056919677734" Y="-23.816474609375" />
                  <Point X="3.079647949219" Y="-23.85101953125" />
                  <Point X="3.104977294922" Y="-23.88951953125" />
                  <Point X="3.111739013672" Y="-23.89857421875" />
                  <Point X="3.126291748047" Y="-23.915818359375" />
                  <Point X="3.134082763672" Y="-23.9240078125" />
                  <Point X="3.151326660156" Y="-23.94009765625" />
                  <Point X="3.160030761719" Y="-23.94730078125" />
                  <Point X="3.178239257813" Y="-23.960626953125" />
                  <Point X="3.187743652344" Y="-23.96675" />
                  <Point X="3.220680175781" Y="-23.985291015625" />
                  <Point X="3.257385742188" Y="-24.005953125" />
                  <Point X="3.265483642578" Y="-24.010015625" />
                  <Point X="3.294676025391" Y="-24.0219140625" />
                  <Point X="3.330275390625" Y="-24.031978515625" />
                  <Point X="3.343674804688" Y="-24.034744140625" />
                  <Point X="3.388207275391" Y="-24.04062890625" />
                  <Point X="3.437835449219" Y="-24.0471875" />
                  <Point X="3.444033691406" Y="-24.04780078125" />
                  <Point X="3.465708496094" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.247796386719" Y="-23.948833984375" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.782293457031" Y="-24.275947265625" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.306844726562" Y="-24.413896484375" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686024658203" Y="-24.580458984375" />
                  <Point X="3.665617431641" Y="-24.5878671875" />
                  <Point X="3.642377685547" Y="-24.598384765625" />
                  <Point X="3.634006347656" Y="-24.602685546875" />
                  <Point X="3.590254638672" Y="-24.627974609375" />
                  <Point X="3.541496337891" Y="-24.65615625" />
                  <Point X="3.533888183594" Y="-24.66105078125" />
                  <Point X="3.508778808594" Y="-24.680125" />
                  <Point X="3.481993896484" Y="-24.7056484375" />
                  <Point X="3.472794921875" Y="-24.715775390625" />
                  <Point X="3.446543945312" Y="-24.7492265625" />
                  <Point X="3.4172890625" Y="-24.78650390625" />
                  <Point X="3.410852783203" Y="-24.795794921875" />
                  <Point X="3.399129638672" Y="-24.81507421875" />
                  <Point X="3.393842773438" Y="-24.8250625" />
                  <Point X="3.384068603516" Y="-24.84652734375" />
                  <Point X="3.380005371094" Y="-24.857072265625" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.361625732422" Y="-24.93521875" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026263671875" />
                  <Point X="3.350280029297" Y="-25.06294140625" />
                  <Point X="3.351874511719" Y="-25.076423828125" />
                  <Point X="3.360625" Y="-25.12211328125" />
                  <Point X="3.370376708984" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.18398828125" />
                  <Point X="3.380005126953" Y="-25.20548828125" />
                  <Point X="3.384068603516" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.237498046875" />
                  <Point X="3.399129638672" Y="-25.247486328125" />
                  <Point X="3.410852783203" Y="-25.266765625" />
                  <Point X="3.4172890625" Y="-25.276056640625" />
                  <Point X="3.443540039062" Y="-25.3095078125" />
                  <Point X="3.472794921875" Y="-25.34678515625" />
                  <Point X="3.47871484375" Y="-25.353630859375" />
                  <Point X="3.501142089844" Y="-25.37580859375" />
                  <Point X="3.530178710938" Y="-25.3987265625" />
                  <Point X="3.541495117188" Y="-25.406404296875" />
                  <Point X="3.585246826172" Y="-25.431693359375" />
                  <Point X="3.634005126953" Y="-25.459876953125" />
                  <Point X="3.639487304688" Y="-25.4628125" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026855469" Y="-25.481029296875" />
                  <Point X="3.691991943359" Y="-25.483916015625" />
                  <Point X="4.377201660156" Y="-25.667517578125" />
                  <Point X="4.784876953125" Y="-25.77675390625" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.727801757812" Y="-26.07921875" />
                  <Point X="4.149838378906" Y="-26.00312890625" />
                  <Point X="3.436782226562" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720458984" Y="-25.910841796875" />
                  <Point X="3.354481445312" Y="-25.912677734375" />
                  <Point X="3.268612304688" Y="-25.931341796875" />
                  <Point X="3.172917236328" Y="-25.952140625" />
                  <Point X="3.157873535156" Y="-25.9567421875" />
                  <Point X="3.128752685547" Y="-25.9683671875" />
                  <Point X="3.114675537109" Y="-25.975390625" />
                  <Point X="3.086848876953" Y="-25.992283203125" />
                  <Point X="3.074124267578" Y="-26.00153125" />
                  <Point X="3.050374023438" Y="-26.022001953125" />
                  <Point X="3.039348388672" Y="-26.033224609375" />
                  <Point X="2.987445800781" Y="-26.0956484375" />
                  <Point X="2.929604248047" Y="-26.165212890625" />
                  <Point X="2.921325195312" Y="-26.176849609375" />
                  <Point X="2.906605712891" Y="-26.20123046875" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.867718261719" Y="-26.3775" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786621094" Y="-26.576671875" />
                  <Point X="2.889157958984" Y="-26.605482421875" />
                  <Point X="2.896540283203" Y="-26.619373046875" />
                  <Point X="2.944061523438" Y="-26.6932890625" />
                  <Point X="2.997020507812" Y="-26.775662109375" />
                  <Point X="3.001741699219" Y="-26.782353515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.688675048828" Y="-27.32420703125" />
                  <Point X="4.087170410156" Y="-27.629982421875" />
                  <Point X="4.045495117188" Y="-27.697421875" />
                  <Point X="4.001273681641" Y="-27.760251953125" />
                  <Point X="3.48352734375" Y="-27.46133203125" />
                  <Point X="2.848454589844" Y="-27.094671875" />
                  <Point X="2.841190673828" Y="-27.090884765625" />
                  <Point X="2.815021240234" Y="-27.07951171875" />
                  <Point X="2.783116943359" Y="-27.069326171875" />
                  <Point X="2.771108642578" Y="-27.066337890625" />
                  <Point X="2.668910644531" Y="-27.047880859375" />
                  <Point X="2.555018310547" Y="-27.0273125" />
                  <Point X="2.539359375" Y="-27.02580859375" />
                  <Point X="2.508009033203" Y="-27.025404296875" />
                  <Point X="2.492317626953" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444846191406" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400591064453" Y="-27.051109375" />
                  <Point X="2.315689453125" Y="-27.095791015625" />
                  <Point X="2.221072998047" Y="-27.145587890625" />
                  <Point X="2.208970947266" Y="-27.153169921875" />
                  <Point X="2.186039794922" Y="-27.1700625" />
                  <Point X="2.175210693359" Y="-27.179373046875" />
                  <Point X="2.154251220703" Y="-27.20033203125" />
                  <Point X="2.144940185547" Y="-27.21116015625" />
                  <Point X="2.128046386719" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.075780517578" Y="-27.33109765625" />
                  <Point X="2.02598449707" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.44019140625" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.002187744141" Y="-27.580140625" />
                  <Point X="2.02064440918" Y="-27.68233984375" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.478162841797" Y="-28.581322265625" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723753173828" Y="-29.036083984375" />
                  <Point X="2.318534179688" Y="-28.5079921875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828658813477" Y="-27.87015234375" />
                  <Point X="1.808834228516" Y="-27.84962890625" />
                  <Point X="1.783250854492" Y="-27.82800390625" />
                  <Point X="1.773297729492" Y="-27.820646484375" />
                  <Point X="1.672502807617" Y="-27.755845703125" />
                  <Point X="1.560174438477" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517470703125" Y="-27.663873046875" />
                  <Point X="1.502546875" Y="-27.6588828125" />
                  <Point X="1.470926025391" Y="-27.65115234375" />
                  <Point X="1.455384887695" Y="-27.6486953125" />
                  <Point X="1.424120361328" Y="-27.64637890625" />
                  <Point X="1.408396972656" Y="-27.64651953125" />
                  <Point X="1.298160522461" Y="-27.656662109375" />
                  <Point X="1.175310058594" Y="-27.667966796875" />
                  <Point X="1.161227539062" Y="-27.67033984375" />
                  <Point X="1.133575439453" Y="-27.677171875" />
                  <Point X="1.120005859375" Y="-27.681630859375" />
                  <Point X="1.092620605469" Y="-27.692974609375" />
                  <Point X="1.07987512207" Y="-27.699416015625" />
                  <Point X="1.055493286133" Y="-27.71413671875" />
                  <Point X="1.043856933594" Y="-27.722416015625" />
                  <Point X="0.958735595703" Y="-27.793193359375" />
                  <Point X="0.863873291016" Y="-27.872068359375" />
                  <Point X="0.852653137207" Y="-27.88308984375" />
                  <Point X="0.83218347168" Y="-27.906837890625" />
                  <Point X="0.822933776855" Y="-27.919564453125" />
                  <Point X="0.806041625977" Y="-27.947390625" />
                  <Point X="0.799019287109" Y="-27.96146875" />
                  <Point X="0.787394470215" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.757340820312" Y="-28.1227265625" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091796875" Y="-29.152341796875" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146789551" Y="-28.453583984375" />
                  <Point X="0.626788085938" Y="-28.42381640625" />
                  <Point X="0.620407287598" Y="-28.41320703125" />
                  <Point X="0.542973693848" Y="-28.301640625" />
                  <Point X="0.456679534912" Y="-28.177306640625" />
                  <Point X="0.446669189453" Y="-28.165169921875" />
                  <Point X="0.424782836914" Y="-28.142712890625" />
                  <Point X="0.412906524658" Y="-28.132392578125" />
                  <Point X="0.386651550293" Y="-28.113150390625" />
                  <Point X="0.37323550415" Y="-28.10493359375" />
                  <Point X="0.345236297607" Y="-28.090828125" />
                  <Point X="0.330653106689" Y="-28.084939453125" />
                  <Point X="0.210829177856" Y="-28.047751953125" />
                  <Point X="0.077294021606" Y="-28.006306640625" />
                  <Point X="0.063380245209" Y="-28.003111328125" />
                  <Point X="0.03522794342" Y="-27.998841796875" />
                  <Point X="0.020989574432" Y="-27.997767578125" />
                  <Point X="-0.008651926041" Y="-27.997765625" />
                  <Point X="-0.022895944595" Y="-27.998837890625" />
                  <Point X="-0.051062065125" Y="-28.003107421875" />
                  <Point X="-0.064984016418" Y="-28.0063046875" />
                  <Point X="-0.184807815552" Y="-28.043494140625" />
                  <Point X="-0.318343261719" Y="-28.0849375" />
                  <Point X="-0.332927490234" Y="-28.090828125" />
                  <Point X="-0.360928619385" Y="-28.104935546875" />
                  <Point X="-0.374345275879" Y="-28.11315234375" />
                  <Point X="-0.40060067749" Y="-28.132396484375" />
                  <Point X="-0.412477874756" Y="-28.14271875" />
                  <Point X="-0.434361572266" Y="-28.16517578125" />
                  <Point X="-0.444368499756" Y="-28.177310546875" />
                  <Point X="-0.521802062988" Y="-28.28887890625" />
                  <Point X="-0.608096069336" Y="-28.4132109375" />
                  <Point X="-0.612470092773" Y="-28.420130859375" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.853882995605" Y="-29.275884765625" />
                  <Point X="-0.985424987793" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.635578144832" Y="-26.067077357383" />
                  <Point X="4.702008047487" Y="-25.754549237182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.541099621117" Y="-26.054639102002" />
                  <Point X="4.610119166305" Y="-25.729927671566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.446621097401" Y="-26.042200846622" />
                  <Point X="4.518230285124" Y="-25.70530610595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.986169301606" Y="-27.751531466335" />
                  <Point X="4.022545643509" Y="-27.580394232944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.352142573686" Y="-26.029762591242" />
                  <Point X="4.426341403942" Y="-25.680684540334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.719084675949" Y="-24.303435728501" />
                  <Point X="4.758039463665" Y="-24.120167861304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.899662933151" Y="-27.701587169278" />
                  <Point X="3.93904262371" Y="-27.516320291375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.257664049971" Y="-26.017324335862" />
                  <Point X="4.334452523601" Y="-25.656062970762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.616096627081" Y="-24.331031641369" />
                  <Point X="4.707596030852" Y="-23.900560791392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.813156564697" Y="-27.65164287222" />
                  <Point X="3.85553960391" Y="-27.452246349805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.163185526255" Y="-26.004886080482" />
                  <Point X="4.242563644226" Y="-25.631441396643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.513108578214" Y="-24.358627554236" />
                  <Point X="4.610358918619" Y="-23.901100674609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726650196242" Y="-27.601698575162" />
                  <Point X="3.772036584111" Y="-27.388172408235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068707001766" Y="-25.992447828744" />
                  <Point X="4.150674764852" Y="-25.606819822523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.410120529346" Y="-24.386223467104" />
                  <Point X="4.510440421297" Y="-23.914255482854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.640143827788" Y="-27.551754278105" />
                  <Point X="3.688533564395" Y="-27.324098466273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974228477149" Y="-25.980009577606" />
                  <Point X="4.058785885477" Y="-25.582198248404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.307132480479" Y="-24.413819379972" />
                  <Point X="4.410521923975" Y="-23.927410291099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.553637459333" Y="-27.501809981047" />
                  <Point X="3.605030593849" Y="-27.260024292981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879749952531" Y="-25.967571326468" />
                  <Point X="3.966897006103" Y="-25.557576674284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204144483806" Y="-24.441415047284" />
                  <Point X="4.310603426653" Y="-23.940565099345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.46713110084" Y="-27.451865637124" />
                  <Point X="3.521527623304" Y="-27.195950119689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.785271427914" Y="-25.95513307533" />
                  <Point X="3.875008126728" Y="-25.532955100165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.101156487279" Y="-24.469010713907" />
                  <Point X="4.210684960009" Y="-23.95371976326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.380624784943" Y="-27.401921092804" />
                  <Point X="3.438024652759" Y="-27.131875946397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.690792903297" Y="-25.942694824192" />
                  <Point X="3.783119247354" Y="-25.508333526045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.998168490753" Y="-24.496606380531" />
                  <Point X="4.110766545284" Y="-23.966874182915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.294118469045" Y="-27.351976548485" />
                  <Point X="3.354521682213" Y="-27.067801773105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.59631437868" Y="-25.930256573054" />
                  <Point X="3.691238556418" Y="-25.483673428353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.895180494226" Y="-24.524202047154" />
                  <Point X="4.010848130559" Y="-23.980028602569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.207612153148" Y="-27.302032004165" />
                  <Point X="3.271018711668" Y="-27.003727599813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.501835854063" Y="-25.917818321916" />
                  <Point X="3.602985474969" Y="-25.441946769838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.792192497699" Y="-24.551797713777" />
                  <Point X="3.910929715835" Y="-23.993183022223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.12110583725" Y="-27.252087459846" />
                  <Point X="3.187515741122" Y="-26.939653426521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.406766852171" Y="-25.908158047945" />
                  <Point X="3.517223332247" Y="-25.388501165991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.689181832995" Y="-24.579500025793" />
                  <Point X="3.81101130111" Y="-24.006337441878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.12588627831" Y="-22.524967143422" />
                  <Point X="4.130073017895" Y="-22.505270082309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.661913476566" Y="-28.955492903212" />
                  <Point X="2.670598925021" Y="-28.914631080898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034599521353" Y="-27.202142915526" />
                  <Point X="3.104012770577" Y="-26.875579253229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.306465459932" Y="-25.923114235144" />
                  <Point X="3.438308524695" Y="-25.302841382935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.580565532158" Y="-24.633574782338" />
                  <Point X="3.711092886385" Y="-24.019491861532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009836121368" Y="-22.614015443229" />
                  <Point X="4.058197137154" Y="-22.386494752239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.585858927456" Y="-28.856376662166" />
                  <Point X="2.599611274111" Y="-28.791676958017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.948093205455" Y="-27.152198371207" />
                  <Point X="3.021621371643" Y="-26.806275546664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.204638830183" Y="-25.945246100656" />
                  <Point X="3.369618869772" Y="-25.169076038945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463435952798" Y="-24.727701365357" />
                  <Point X="3.611174471661" Y="-24.032646281186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.893785964426" Y="-22.703063743037" />
                  <Point X="3.983836565724" Y="-22.279408972793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.509804378347" Y="-28.75726042112" />
                  <Point X="2.528623623202" Y="-28.668722835135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.861586889558" Y="-27.102253826887" />
                  <Point X="2.947408484649" Y="-26.698494966579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.099099149526" Y="-25.984846497271" />
                  <Point X="3.511256056936" Y="-24.04580070084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.777735807484" Y="-22.792112042844" />
                  <Point X="3.893828957173" Y="-22.245936715315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433749829237" Y="-28.658144180073" />
                  <Point X="2.457635962126" Y="-28.545768760082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.772048962753" Y="-27.066571890522" />
                  <Point X="2.876416009431" Y="-26.575563540284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.975327150837" Y="-26.110223206263" />
                  <Point X="3.414494580048" Y="-24.044102895695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.661685626586" Y="-22.881160455359" />
                  <Point X="3.783120517531" Y="-22.309854211079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.357695280127" Y="-28.559027939027" />
                  <Point X="2.386648276059" Y="-28.422814802607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.678530232112" Y="-27.049618163745" />
                  <Point X="3.320534686278" Y="-24.029224678256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.545635411452" Y="-22.970209028936" />
                  <Point X="3.672412077888" Y="-22.373771706843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281640726161" Y="-28.459911720829" />
                  <Point X="2.315660589991" Y="-28.299860845133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.584998262302" Y="-27.032726722364" />
                  <Point X="3.23146080256" Y="-23.991359590815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.429585196319" Y="-23.059257602513" />
                  <Point X="3.561703638246" Y="-22.437689202607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205586167039" Y="-28.360795526886" />
                  <Point X="2.244672903924" Y="-28.176906887658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.48909300082" Y="-27.027000740436" />
                  <Point X="3.146242795367" Y="-23.935354030575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313534981186" Y="-23.148306176089" />
                  <Point X="3.450995198604" Y="-22.501606698371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.129531607917" Y="-28.261679332942" />
                  <Point X="2.173685217856" Y="-28.053952930183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.385114990911" Y="-27.059254053829" />
                  <Point X="3.070123366478" Y="-23.836543024891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.197484766053" Y="-23.237354749666" />
                  <Point X="3.340286744321" Y="-22.565524263012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053477048796" Y="-28.162563138999" />
                  <Point X="2.102697531788" Y="-27.930998972709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.275759542422" Y="-27.116806226672" />
                  <Point X="3.008885834029" Y="-23.667718201128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.070941843127" Y="-23.375767632253" />
                  <Point X="3.22957828499" Y="-22.629441851403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.977422489674" Y="-28.063446945055" />
                  <Point X="2.038001171222" Y="-27.778446655853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162673369327" Y="-27.191910078832" />
                  <Point X="3.118869825659" Y="-22.693359439795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.901367930552" Y="-27.964330751112" />
                  <Point X="3.008161366328" Y="-22.757277028186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.825056500007" Y="-27.866423042203" />
                  <Point X="2.897452906997" Y="-22.821194616578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.741948224856" Y="-27.800491973072" />
                  <Point X="2.786744447667" Y="-22.885112204969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.656502268377" Y="-27.745558829906" />
                  <Point X="2.676035988336" Y="-22.949029793361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.571056449907" Y="-27.690625037459" />
                  <Point X="2.568350526494" Y="-22.998725296743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.481761227514" Y="-27.653801266611" />
                  <Point X="2.466215214599" Y="-23.022309397573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.385743624778" Y="-27.64860380873" />
                  <Point X="2.369684941197" Y="-23.019523865546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.754979149151" Y="-21.206857133799" />
                  <Point X="2.783309484778" Y="-21.073573383795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.286683948296" Y="-27.657718182591" />
                  <Point X="2.276989179291" Y="-22.998698375279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601265702803" Y="-21.473097278969" />
                  <Point X="2.698940367093" Y="-21.013574112416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.187624035995" Y="-27.666833665896" />
                  <Point X="2.190604490869" Y="-22.948181618677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44755230091" Y="-21.739337214993" />
                  <Point X="2.614210083546" Y="-20.955273992826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.800556529709" Y="-29.030918347621" />
                  <Point X="0.810776231792" Y="-28.982838429489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.084021505904" Y="-27.697320485632" />
                  <Point X="2.108981216907" Y="-22.875264168243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293839125133" Y="-22.005576087225" />
                  <Point X="2.52778965617" Y="-20.904925374783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746397261505" Y="-28.828792908766" />
                  <Point X="0.773629013414" Y="-28.700677588804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.968192058922" Y="-27.785330426713" />
                  <Point X="2.037160425316" Y="-22.756229664097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.140125949357" Y="-22.271814959458" />
                  <Point X="2.441369228794" Y="-20.854576756741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692237993302" Y="-28.626667469912" />
                  <Point X="0.736481795036" Y="-28.418516748119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.849516904328" Y="-27.886728369515" />
                  <Point X="2.354948660845" Y="-20.804228800041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.634893705656" Y="-28.439526369428" />
                  <Point X="2.268528069877" Y="-20.753880951638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.561428157533" Y="-28.328229836387" />
                  <Point X="2.180589350145" Y="-20.710675337526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.487076345953" Y="-28.221102845091" />
                  <Point X="2.092248619124" Y="-20.669361037831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.409362096921" Y="-28.12979487827" />
                  <Point X="2.003907853191" Y="-20.62804690238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.322323533815" Y="-28.082354360195" />
                  <Point X="1.91556704376" Y="-20.586732971576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.231211576535" Y="-28.054077654996" />
                  <Point X="1.82658377193" Y="-20.548441588718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.140099885054" Y="-28.025799699311" />
                  <Point X="1.736413285657" Y="-20.515735610672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.048286647541" Y="-28.000822258215" />
                  <Point X="1.646242799384" Y="-20.483029632627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.049263484963" Y="-28.002834786025" />
                  <Point X="1.556072106952" Y="-20.450324624486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.152924916272" Y="-28.0335987142" />
                  <Point X="1.465901407186" Y="-20.417619650844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.256906834157" Y="-28.065870413176" />
                  <Point X="1.37382099757" Y="-20.393899155669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.36254288006" Y="-28.105924172623" />
                  <Point X="1.281439534922" Y="-20.371595003647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.488290223219" Y="-28.240594146688" />
                  <Point X="1.189058072275" Y="-20.349290851625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.632024950168" Y="-28.459888108121" />
                  <Point X="1.096676589638" Y="-20.326986793647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.006088564366" Y="-29.762794287586" />
                  <Point X="1.004295053419" Y="-20.304682987751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.098841590885" Y="-29.742238206139" />
                  <Point X="0.9119135172" Y="-20.282379181854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.133316857202" Y="-29.447506819339" />
                  <Point X="0.819295501147" Y="-20.26118792611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.180267617462" Y="-29.211468016969" />
                  <Point X="0.724288067169" Y="-20.251237997877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.248072257965" Y="-29.073539007491" />
                  <Point X="0.62928063319" Y="-20.241288069644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.329501764935" Y="-28.999709955029" />
                  <Point X="0.534273199212" Y="-20.231338141411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.411364351883" Y="-28.927918383676" />
                  <Point X="0.439265742983" Y="-20.22138831786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.493226821524" Y="-28.856126260434" />
                  <Point X="0.226006444853" Y="-20.76776967022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.580332882307" Y="-28.809003293961" />
                  <Point X="0.102090367659" Y="-20.893824215284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.674206552179" Y="-28.793719424979" />
                  <Point X="0.00049103783" Y="-20.91488671875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.769994558306" Y="-28.787441799977" />
                  <Point X="-0.091953894378" Y="-20.892881167536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.86578239575" Y="-28.78116338139" />
                  <Point X="-0.175649540911" Y="-20.829713463497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.961835147836" Y="-28.776131288202" />
                  <Point X="-0.241095884247" Y="-20.680689538155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.06407553483" Y="-28.800209728508" />
                  <Point X="-0.295255170501" Y="-20.478564184226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.175948708806" Y="-28.869606868484" />
                  <Point X="-0.349414444814" Y="-20.276438774113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.289148278125" Y="-28.945244207932" />
                  <Point X="-0.435751039565" Y="-20.22569575458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.405558772226" Y="-29.035987760792" />
                  <Point X="-0.535351123103" Y="-20.237352543748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.526501117267" Y="-29.148051996031" />
                  <Point X="-0.634951306405" Y="-20.249009802271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.612327839661" Y="-29.09491021565" />
                  <Point X="-0.734551558342" Y="-20.260667383694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.698154562054" Y="-29.04176843527" />
                  <Point X="-2.462707239898" Y="-27.934075874256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.361435006104" Y="-27.457627473898" />
                  <Point X="-0.834151810279" Y="-20.272324965118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.78297947578" Y="-28.983913515668" />
                  <Point X="-2.616420547381" Y="-28.20031536612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.44283935136" Y="-27.383680044878" />
                  <Point X="-0.933847578363" Y="-20.284431914682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.866442152911" Y="-28.919649776765" />
                  <Point X="-2.770133854842" Y="-28.466554857875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.532894108474" Y="-27.350429603948" />
                  <Point X="-1.037124416143" Y="-20.313386472563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949904807879" Y="-28.855385933593" />
                  <Point X="-2.923847162278" Y="-28.73279434952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.631612821462" Y="-27.357939870691" />
                  <Point X="-1.140401253923" Y="-20.342341030445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.739702780346" Y="-27.409538383038" />
                  <Point X="-1.243678091703" Y="-20.371295588326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.850411236053" Y="-27.47345595438" />
                  <Point X="-1.346954969834" Y="-20.400250336044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.96111969176" Y="-27.537373525721" />
                  <Point X="-1.533516641675" Y="-20.821029231911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468822929173" Y="-20.51666924418" />
                  <Point X="-1.450231946921" Y="-20.42920554931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.071828147466" Y="-27.601291097062" />
                  <Point X="-1.65080085224" Y="-20.91588329755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.182536603173" Y="-27.665208668403" />
                  <Point X="-1.756982872759" Y="-20.958505665616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.29324505888" Y="-27.729126239744" />
                  <Point X="-3.047587687936" Y="-26.573399175787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.976762490998" Y="-26.240192821764" />
                  <Point X="-1.856523663629" Y="-20.969883504717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.403953525712" Y="-27.793043863426" />
                  <Point X="-3.163656953911" Y="-26.662537376527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.055448861299" Y="-26.153458325934" />
                  <Point X="-1.947791968552" Y="-20.942342357346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514662014106" Y="-27.856961588549" />
                  <Point X="-3.279707080507" Y="-26.751585533569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.143482259649" Y="-26.1106981397" />
                  <Point X="-2.03523803055" Y="-20.896818970828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.6253705025" Y="-27.920879313672" />
                  <Point X="-3.395757207103" Y="-26.840633690611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.238311015743" Y="-26.099907598113" />
                  <Point X="-2.117912921871" Y="-20.828848991082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.736078990894" Y="-27.984797038796" />
                  <Point X="-3.511807333699" Y="-26.929681847653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.338119497388" Y="-26.112544823295" />
                  <Point X="-2.194635637126" Y="-20.732876224601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.829913396765" Y="-27.96932844721" />
                  <Point X="-3.627857460295" Y="-27.018730004696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438037889175" Y="-26.125699135034" />
                  <Point X="-2.304763330511" Y="-20.794061524039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.905834369152" Y="-27.869583777092" />
                  <Point X="-3.743907644886" Y="-27.107778434579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.537956280962" Y="-26.138853446774" />
                  <Point X="-3.333367719048" Y="-25.17633993834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296178191896" Y="-25.001376969143" />
                  <Point X="-2.414890919536" Y="-20.855246332498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.981755341539" Y="-27.769839106975" />
                  <Point X="-3.859957855852" Y="-27.196826988551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.637874672748" Y="-26.152007758513" />
                  <Point X="-3.45434613024" Y="-25.288573851478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.360391345138" Y="-24.846551340558" />
                  <Point X="-2.772390054235" Y="-22.080222762966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748499353704" Y="-21.96782585391" />
                  <Point X="-2.52501841991" Y="-20.91643072389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.054623775173" Y="-27.655733371129" />
                  <Point X="-3.976008066818" Y="-27.285875542523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737793064535" Y="-26.165162070253" />
                  <Point X="-3.559395617505" Y="-25.3258680695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443435175915" Y="-24.780317084488" />
                  <Point X="-2.900242486815" Y="-22.224796404102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.799403953698" Y="-21.750388405003" />
                  <Point X="-2.635145920285" Y="-20.977615115282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.12622544001" Y="-27.535667956659" />
                  <Point X="-4.092058277784" Y="-27.374924096496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.837711456322" Y="-26.178316381992" />
                  <Point X="-3.662383596744" Y="-25.353463654791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.53279959915" Y="-24.743818878005" />
                  <Point X="-3.007549560291" Y="-22.272711730182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.870391693347" Y="-21.627434699608" />
                  <Point X="-2.750468003939" Y="-21.063238099578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.937629848108" Y="-26.191470693732" />
                  <Point X="-3.765371575983" Y="-25.381059240083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.624688482737" Y="-24.719197323703" />
                  <Point X="-3.104784772158" Y="-22.273242672886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.941379432996" Y="-21.504480994213" />
                  <Point X="-2.866499487555" Y="-21.152198548295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.037548239895" Y="-26.204625005471" />
                  <Point X="-3.868359555222" Y="-25.408654825374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716577366324" Y="-24.694575769401" />
                  <Point X="-3.503590971203" Y="-23.692553562009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438877528035" Y="-23.388100748793" />
                  <Point X="-3.199030063349" Y="-22.259707144747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.012367203918" Y="-21.381527435946" />
                  <Point X="-2.98253107603" Y="-21.241159490336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.137466646318" Y="-26.217779386068" />
                  <Point X="-3.971347534461" Y="-25.436250410666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.80846624991" Y="-24.669954215099" />
                  <Point X="-3.616546625246" Y="-23.767043370304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.509683851642" Y="-23.264293548026" />
                  <Point X="-3.287376616813" Y="-22.218420237491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.237385079627" Y="-26.230933893155" />
                  <Point X="-4.0743355137" Y="-25.463845995957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.900355133497" Y="-24.645332660797" />
                  <Point X="-3.719351716" Y="-23.793778532921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.591781812221" Y="-23.193609322539" />
                  <Point X="-3.373882948093" Y="-22.168475765541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.337303512936" Y="-26.244088400241" />
                  <Point X="-4.177323492939" Y="-25.491441581249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.992244017084" Y="-24.620711106495" />
                  <Point X="-3.815555935738" Y="-23.789459039007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.675284770375" Y="-23.129535090952" />
                  <Point X="-3.460389279373" Y="-22.118531293592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.437221946245" Y="-26.257242907328" />
                  <Point X="-4.280311495742" Y="-25.519037277402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.08413290067" Y="-24.596089552192" />
                  <Point X="-3.910034494315" Y="-23.777020947643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.758787728529" Y="-23.065460859364" />
                  <Point X="-3.546895610653" Y="-22.068586821643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.537140379554" Y="-26.270397414415" />
                  <Point X="-4.383299509507" Y="-25.546633025127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.176021784257" Y="-24.57146799789" />
                  <Point X="-4.004513052893" Y="-23.764582856278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842290686684" Y="-23.001386627777" />
                  <Point X="-3.63340194432" Y="-22.01864236092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.637058812863" Y="-26.283551921501" />
                  <Point X="-4.486287523273" Y="-25.574228772852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.267910667844" Y="-24.546846443588" />
                  <Point X="-4.098991611471" Y="-23.752144764913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.925793644838" Y="-22.937312396189" />
                  <Point X="-3.719908278536" Y="-21.968697902784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.701271183791" Y="-26.128722612421" />
                  <Point X="-4.589275537038" Y="-25.601824520577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.359799566037" Y="-24.522224958002" />
                  <Point X="-4.193470170049" Y="-23.739706673549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.009296628908" Y="-22.873238286526" />
                  <Point X="-3.850038476629" Y="-22.123987588135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750725070461" Y="-25.904460093931" />
                  <Point X="-4.692263550804" Y="-25.629420268303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.451688468941" Y="-24.497603494581" />
                  <Point X="-4.287948721336" Y="-23.727268547883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.092799718874" Y="-22.809164675064" />
                  <Point X="-3.983670691476" Y="-22.29575296695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.543577371845" Y="-24.472982031159" />
                  <Point X="-4.38242717952" Y="-23.714829984198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.17630280884" Y="-22.745091063603" />
                  <Point X="-4.133753106086" Y="-22.544910450876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.635466274749" Y="-24.448360567737" />
                  <Point X="-4.476905637703" Y="-23.702391420514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.727355177653" Y="-24.423739104315" />
                  <Point X="-4.571384095886" Y="-23.689952856829" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998373046875" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.66099029541" Y="-29.244154296875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.386885223389" Y="-28.4099765625" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.27433605957" Y="-28.266400390625" />
                  <Point X="0.15451222229" Y="-28.229212890625" />
                  <Point X="0.020977081299" Y="-28.187767578125" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.12848828125" Y="-28.224955078125" />
                  <Point X="-0.262023590088" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.365712585449" Y="-28.3972109375" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.670357116699" Y="-29.325060546875" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-0.943741149902" Y="-29.968443359375" />
                  <Point X="-1.100229614258" Y="-29.938068359375" />
                  <Point X="-1.238634155273" Y="-29.902458984375" />
                  <Point X="-1.351589599609" Y="-29.873396484375" />
                  <Point X="-1.331955810547" Y="-29.724263671875" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.338309204102" Y="-29.39084765625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.496601196289" Y="-29.1058828125" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240600586" Y="-28.985763671875" />
                  <Point X="-1.795657958984" Y="-28.97616796875" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.111881103516" Y="-29.055310546875" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.378267333984" Y="-29.311771484375" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.626430419922" Y="-29.30965234375" />
                  <Point X="-2.855837890625" Y="-29.167609375" />
                  <Point X="-3.047461669922" Y="-29.020064453125" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.858107666016" Y="-28.2389296875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499763183594" Y="-27.59758984375" />
                  <Point X="-2.513983398438" Y="-27.568759765625" />
                  <Point X="-2.531326904297" Y="-27.55141796875" />
                  <Point X="-2.56015625" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.271273193359" Y="-27.93583203125" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.980462402344" Y="-28.085244140625" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.299085449219" Y="-27.61676171875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.779801757813" Y="-26.895830078125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822265625" Y="-26.396015625" />
                  <Point X="-3.138117675781" Y="-26.366267578125" />
                  <Point X="-3.140326416016" Y="-26.334595703125" />
                  <Point X="-3.161159423828" Y="-26.310638671875" />
                  <Point X="-3.187642089844" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-4.077446777344" Y="-26.401517578125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.856508300781" Y="-26.288703125" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.963740722656" Y="-25.757052734375" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.259197753906" Y="-25.31667578125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.528770019531" Y="-25.11231640625" />
                  <Point X="-3.502324462891" Y="-25.090646484375" />
                  <Point X="-3.490523925781" Y="-25.0618125" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.490022949219" Y="-25.00236328125" />
                  <Point X="-3.502323974609" Y="-24.971916015625" />
                  <Point X="-3.527268066406" Y="-24.951287109375" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.33949609375" Y="-24.724369140625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.963329101563" Y="-24.312310546875" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.844478027344" Y="-23.73357421875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.256286621094" Y="-23.539796875" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731703125" Y="-23.6041328125" />
                  <Point X="-3.70265234375" Y="-23.594974609375" />
                  <Point X="-3.670277099609" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.573943359375" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.627463378906" Y="-23.528072265625" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.61631640625" Y="-23.45448828125" />
                  <Point X="-3.630381591797" Y="-23.42746875" />
                  <Point X="-3.646056152344" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.108543945313" Y="-23.03657421875" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.337526367188" Y="-22.517115234375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.966197265625" Y="-21.96387109375" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.468097900391" Y="-21.894689453125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138515380859" Y="-22.07956640625" />
                  <Point X="-3.098055664062" Y="-22.08310546875" />
                  <Point X="-3.052966064453" Y="-22.08705078125" />
                  <Point X="-3.031507324219" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.984534179688" Y="-22.043876953125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.941614013672" Y="-21.93169921875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.150844970703" Y="-21.52167578125" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-3.062044677734" Y="-21.062705078125" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.447620361328" Y="-20.65607421875" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.062403076172" Y="-20.589171875" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247070312" Y="-20.726337890625" />
                  <Point X="-1.906215454102" Y="-20.74978125" />
                  <Point X="-1.856031005859" Y="-20.77590625" />
                  <Point X="-1.835124633789" Y="-20.781509765625" />
                  <Point X="-1.813809570312" Y="-20.77775" />
                  <Point X="-1.76690612793" Y="-20.7583203125" />
                  <Point X="-1.714635620117" Y="-20.736669921875" />
                  <Point X="-1.696905517578" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.670817138672" Y="-20.65709375" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.674516357422" Y="-20.409919921875" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.36833215332" Y="-20.20891796875" />
                  <Point X="-0.968082946777" Y="-20.096703125" />
                  <Point X="-0.598032348633" Y="-20.053392578125" />
                  <Point X="-0.224200027466" Y="-20.009640625" />
                  <Point X="-0.13353352356" Y="-20.348013671875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282110214" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594028473" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.156303543091" Y="-20.308982421875" />
                  <Point X="0.236648406982" Y="-20.009130859375" />
                  <Point X="0.510738616943" Y="-20.0378359375" />
                  <Point X="0.860210021973" Y="-20.074435546875" />
                  <Point X="1.166368041992" Y="-20.1483515625" />
                  <Point X="1.508455810547" Y="-20.230943359375" />
                  <Point X="1.707929443359" Y="-20.30329296875" />
                  <Point X="1.931044921875" Y="-20.38421875" />
                  <Point X="2.12373046875" Y="-20.47433203125" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.524854003906" Y="-20.6833203125" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.908090576172" Y="-20.9291640625" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.637352539062" Y="-21.79059375" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.214698242188" Y="-22.54645703125" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.20600390625" Y="-22.640509765625" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.238998535156" Y="-22.72912890625" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.274938720703" Y="-22.775794921875" />
                  <Point X="2.304879882812" Y="-22.796111328125" />
                  <Point X="2.338247314453" Y="-22.81875390625" />
                  <Point X="2.360337158203" Y="-22.827021484375" />
                  <Point X="2.393171142578" Y="-22.83098046875" />
                  <Point X="2.429761962891" Y="-22.835392578125" />
                  <Point X="2.448665039062" Y="-22.8340546875" />
                  <Point X="2.486635742188" Y="-22.823900390625" />
                  <Point X="2.528951416016" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.327607910156" Y="-22.353453125" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.079408935547" Y="-22.086923828125" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.300467773438" Y="-22.419861328125" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.825126953125" Y="-22.99523828125" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.252043701172" Y="-23.451818359375" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.202939941406" Y="-23.544900390625" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.199135986328" Y="-23.649533203125" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646240234" Y="-23.712044921875" />
                  <Point X="3.238374511719" Y="-23.74658984375" />
                  <Point X="3.263703857422" Y="-23.78508984375" />
                  <Point X="3.280947753906" Y="-23.8011796875" />
                  <Point X="3.313884277344" Y="-23.819720703125" />
                  <Point X="3.35058984375" Y="-23.8403828125" />
                  <Point X="3.368565917969" Y="-23.846380859375" />
                  <Point X="3.413098388672" Y="-23.852265625" />
                  <Point X="3.4627265625" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.222996582031" Y="-23.760458984375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.886282226562" Y="-23.83129296875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.970031738281" Y="-24.246716796875" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.356020507813" Y="-24.597423828125" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087890625" Y="-24.767181640625" />
                  <Point X="3.685336181641" Y="-24.792470703125" />
                  <Point X="3.636577880859" Y="-24.82065234375" />
                  <Point X="3.622265136719" Y="-24.833072265625" />
                  <Point X="3.596014160156" Y="-24.8665234375" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.548234619141" Y="-24.97095703125" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538482910156" Y="-25.040685546875" />
                  <Point X="3.547233398438" Y="-25.086375" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566759277344" Y="-25.158759765625" />
                  <Point X="3.593010253906" Y="-25.1922109375" />
                  <Point X="3.622265136719" Y="-25.22948828125" />
                  <Point X="3.636576660156" Y="-25.241908203125" />
                  <Point X="3.680328369141" Y="-25.267197265625" />
                  <Point X="3.729086669922" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.426377441406" Y="-25.483990234375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.977972167969" Y="-25.77046484375" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.908916015625" Y="-26.1395625" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.125038574219" Y="-26.19150390625" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836425781" Y="-26.098341796875" />
                  <Point X="3.308967285156" Y="-26.117005859375" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.13354296875" Y="-26.21712109375" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.056918945312" Y="-26.39491015625" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.103881835938" Y="-26.5905390625" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.804339599609" Y="-27.17346875" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.287403320313" Y="-27.66739453125" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.122410644531" Y="-27.918255859375" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="3.38852734375" Y="-27.625876953125" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.635142578125" Y="-27.234857421875" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077392578" Y="-27.21924609375" />
                  <Point X="2.40417578125" Y="-27.263927734375" />
                  <Point X="2.309559326172" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.243916748047" Y="-27.4195859375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.207619873047" Y="-27.64857421875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.642707763672" Y="-28.486322265625" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.934107910156" Y="-29.119634765625" />
                  <Point X="2.835295898438" Y="-29.190212890625" />
                  <Point X="2.743929199219" Y="-29.249353515625" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="2.167796875" Y="-28.62365625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549194336" Y="-27.980466796875" />
                  <Point X="1.569754394531" Y="-27.915666015625" />
                  <Point X="1.45742578125" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.315568481445" Y="-27.845861328125" />
                  <Point X="1.192718017578" Y="-27.857166015625" />
                  <Point X="1.165332763672" Y="-27.868509765625" />
                  <Point X="1.080211303711" Y="-27.939287109375" />
                  <Point X="0.985349060059" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.943005859375" Y="-28.16308203125" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.029728149414" Y="-29.190296875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.087827148438" Y="-29.9427578125" />
                  <Point X="0.994371337891" Y="-29.9632421875" />
                  <Point X="0.909937866211" Y="-29.978580078125" />
                  <Point X="0.860200439453" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#164" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.091393266905" Y="4.696208806539" Z="1.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.2" />
                  <Point X="-0.610000441139" Y="5.027547843578" Z="1.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.2" />
                  <Point X="-1.387985882063" Y="4.870507317709" Z="1.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.2" />
                  <Point X="-1.730244645039" Y="4.614835297266" Z="1.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.2" />
                  <Point X="-1.724658965457" Y="4.389222120976" Z="1.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.2" />
                  <Point X="-1.792195649252" Y="4.319152766711" Z="1.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.2" />
                  <Point X="-1.889283585606" Y="4.32584918069" Z="1.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.2" />
                  <Point X="-2.028891352471" Y="4.472545417174" Z="1.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.2" />
                  <Point X="-2.478059383287" Y="4.418912438217" Z="1.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.2" />
                  <Point X="-3.098622815166" Y="4.008254975894" Z="1.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.2" />
                  <Point X="-3.200302147543" Y="3.484605551556" Z="1.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.2" />
                  <Point X="-2.997579821421" Y="3.09522334871" Z="1.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.2" />
                  <Point X="-3.026044905214" Y="3.022758662673" Z="1.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.2" />
                  <Point X="-3.099853116897" Y="2.997984877752" Z="1.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.2" />
                  <Point X="-3.449253796741" Y="3.179891801203" Z="1.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.2" />
                  <Point X="-4.011816884616" Y="3.09811334553" Z="1.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.2" />
                  <Point X="-4.386864304291" Y="2.539371323255" Z="1.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.2" />
                  <Point X="-4.145137956612" Y="1.955038298069" Z="1.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.2" />
                  <Point X="-3.68088762962" Y="1.580723374949" Z="1.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.2" />
                  <Point X="-3.679813107807" Y="1.522342116798" Z="1.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.2" />
                  <Point X="-3.723845122607" Y="1.483992254453" Z="1.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.2" />
                  <Point X="-4.255915984194" Y="1.541056382777" Z="1.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.2" />
                  <Point X="-4.898893606838" Y="1.310785342457" Z="1.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.2" />
                  <Point X="-5.018946478795" Y="0.726288997638" Z="1.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.2" />
                  <Point X="-4.358593198422" Y="0.258614002137" Z="1.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.2" />
                  <Point X="-3.56193333115" Y="0.038916777992" Z="1.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.2" />
                  <Point X="-3.543931974129" Y="0.01409692781" Z="1.2" />
                  <Point X="-3.539556741714" Y="0" Z="1.2" />
                  <Point X="-3.544432627006" Y="-0.015710023251" Z="1.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.2" />
                  <Point X="-3.563435263974" Y="-0.039959205142" Z="1.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.2" />
                  <Point X="-4.27829459035" Y="-0.237098054619" Z="1.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.2" />
                  <Point X="-5.019392838714" Y="-0.732850733887" Z="1.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.2" />
                  <Point X="-4.911109031045" Y="-1.269796938582" Z="1.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.2" />
                  <Point X="-4.077076320865" Y="-1.419810259065" Z="1.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.2" />
                  <Point X="-3.205201003356" Y="-1.31507831138" Z="1.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.2" />
                  <Point X="-3.1967370303" Y="-1.338128422235" Z="1.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.2" />
                  <Point X="-3.816395755522" Y="-1.824882049022" Z="1.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.2" />
                  <Point X="-4.348184671138" Y="-2.611090718426" Z="1.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.2" />
                  <Point X="-4.026404252384" Y="-3.084246546519" Z="1.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.2" />
                  <Point X="-3.252429111138" Y="-2.947852210946" Z="1.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.2" />
                  <Point X="-2.563695799916" Y="-2.564634669216" Z="1.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.2" />
                  <Point X="-2.907564802223" Y="-3.182649271355" Z="1.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.2" />
                  <Point X="-3.084121435472" Y="-4.028401243637" Z="1.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.2" />
                  <Point X="-2.658908030106" Y="-4.320882989583" Z="1.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.2" />
                  <Point X="-2.344755472855" Y="-4.310927593729" Z="1.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.2" />
                  <Point X="-2.090258814738" Y="-4.065604164473" Z="1.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.2" />
                  <Point X="-1.805084235309" Y="-3.994779221658" Z="1.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.2" />
                  <Point X="-1.535724597064" Y="-4.112193542852" Z="1.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.2" />
                  <Point X="-1.393504442203" Y="-4.369320212965" Z="1.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.2" />
                  <Point X="-1.387683991658" Y="-4.686456795539" Z="1.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.2" />
                  <Point X="-1.257249159347" Y="-4.919602072349" Z="1.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.2" />
                  <Point X="-0.959424449165" Y="-4.986247540239" Z="1.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.2" />
                  <Point X="-0.628216778841" Y="-4.306721017647" Z="1.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.2" />
                  <Point X="-0.330792614" Y="-3.394439637576" Z="1.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.2" />
                  <Point X="-0.119823884185" Y="-3.241428280176" Z="1.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.2" />
                  <Point X="0.133535195176" Y="-3.245683765113" Z="1.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.2" />
                  <Point X="0.339653245432" Y="-3.407206118192" Z="1.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.2" />
                  <Point X="0.60653808156" Y="-4.225815012168" Z="1.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.2" />
                  <Point X="0.912719004389" Y="-4.996495667003" Z="1.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.2" />
                  <Point X="1.092376294781" Y="-4.960316374327" Z="1.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.2" />
                  <Point X="1.073144446638" Y="-4.152491759599" Z="1.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.2" />
                  <Point X="0.985709061783" Y="-3.142420367028" Z="1.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.2" />
                  <Point X="1.106022072133" Y="-2.946451324639" Z="1.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.2" />
                  <Point X="1.313994208098" Y="-2.864370629039" Z="1.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.2" />
                  <Point X="1.536559094141" Y="-2.926443524547" Z="1.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.2" />
                  <Point X="2.121973073029" Y="-3.622813147153" Z="1.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.2" />
                  <Point X="2.76494277905" Y="-4.260047951547" Z="1.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.2" />
                  <Point X="2.95701371907" Y="-4.12904194434" Z="1.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.2" />
                  <Point X="2.679852972731" Y="-3.430042130731" Z="1.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.2" />
                  <Point X="2.250668004195" Y="-2.608406200143" Z="1.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.2" />
                  <Point X="2.282007169055" Y="-2.411591603426" Z="1.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.2" />
                  <Point X="2.421306774947" Y="-2.276894140912" Z="1.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.2" />
                  <Point X="2.62010060337" Y="-2.252780005296" Z="1.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.2" />
                  <Point X="3.357371466376" Y="-2.637896527544" Z="1.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.2" />
                  <Point X="4.157143320125" Y="-2.915753001448" Z="1.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.2" />
                  <Point X="4.323780946071" Y="-2.662400081705" Z="1.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.2" />
                  <Point X="3.828621499006" Y="-2.102519962348" Z="1.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.2" />
                  <Point X="3.139783763768" Y="-1.532218690455" Z="1.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.2" />
                  <Point X="3.100552592162" Y="-1.368212154387" Z="1.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.2" />
                  <Point X="3.165833063079" Y="-1.217806736262" Z="1.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.2" />
                  <Point X="3.313430652426" Y="-1.13458445196" Z="1.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.2" />
                  <Point X="4.1123561874" Y="-1.209796063798" Z="1.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.2" />
                  <Point X="4.951507204722" Y="-1.119406679343" Z="1.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.2" />
                  <Point X="5.021257665657" Y="-0.74663782012" Z="1.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.2" />
                  <Point X="4.433162360054" Y="-0.404412003595" Z="1.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.2" />
                  <Point X="3.69919447505" Y="-0.192627509083" Z="1.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.2" />
                  <Point X="3.626187779925" Y="-0.130060545702" Z="1.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.2" />
                  <Point X="3.590185078787" Y="-0.045691076622" Z="1.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.2" />
                  <Point X="3.591186371637" Y="0.050919454591" Z="1.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.2" />
                  <Point X="3.629191658476" Y="0.133888192873" Z="1.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.2" />
                  <Point X="3.704200939302" Y="0.195521335159" Z="1.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.2" />
                  <Point X="4.362805457252" Y="0.385559929173" Z="1.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.2" />
                  <Point X="5.013280749392" Y="0.792254473118" Z="1.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.2" />
                  <Point X="4.928707351377" Y="1.211814454058" Z="1.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.2" />
                  <Point X="4.21031407935" Y="1.320393893965" Z="1.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.2" />
                  <Point X="3.413493115099" Y="1.228583067434" Z="1.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.2" />
                  <Point X="3.332255137461" Y="1.255130586169" Z="1.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.2" />
                  <Point X="3.273989382221" Y="1.312170255735" Z="1.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.2" />
                  <Point X="3.241948409321" Y="1.391849946565" Z="1.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.2" />
                  <Point X="3.244936427292" Y="1.472914039836" Z="1.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.2" />
                  <Point X="3.285570603047" Y="1.549044150558" Z="1.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.2" />
                  <Point X="3.849408863311" Y="1.996374229352" Z="1.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.2" />
                  <Point X="4.337089124742" Y="2.637305593656" Z="1.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.2" />
                  <Point X="4.113838721774" Y="2.973559088544" Z="1.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.2" />
                  <Point X="3.296452010782" Y="2.721127504912" Z="1.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.2" />
                  <Point X="2.467562318056" Y="2.255683024579" Z="1.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.2" />
                  <Point X="2.393000651089" Y="2.249941372226" Z="1.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.2" />
                  <Point X="2.326799356609" Y="2.276541690372" Z="1.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.2" />
                  <Point X="2.274216909863" Y="2.330225503773" Z="1.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.2" />
                  <Point X="2.249488330533" Y="2.396757792743" Z="1.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.2" />
                  <Point X="2.256844918078" Y="2.471907257825" Z="1.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.2" />
                  <Point X="2.674497847948" Y="3.215687071173" Z="1.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.2" />
                  <Point X="2.930911585237" Y="4.142865146612" Z="1.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.2" />
                  <Point X="2.543867945859" Y="4.391162862626" Z="1.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.2" />
                  <Point X="2.1387559174" Y="4.60224022644" Z="1.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.2" />
                  <Point X="1.718822249809" Y="4.774991410211" Z="1.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.2" />
                  <Point X="1.171946662854" Y="4.931532169344" Z="1.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.2" />
                  <Point X="0.509790609812" Y="5.043172206907" Z="1.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.2" />
                  <Point X="0.101851294648" Y="4.735238699412" Z="1.2" />
                  <Point X="0" Y="4.355124473572" Z="1.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>