<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#207" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3285" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.995283203125" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.918347900391" Y="-29.83757421875" />
                  <Point X="0.563302001953" Y="-28.5125234375" />
                  <Point X="0.557720336914" Y="-28.497138671875" />
                  <Point X="0.54236340332" Y="-28.467376953125" />
                  <Point X="0.3972472229" Y="-28.25829296875" />
                  <Point X="0.378635742188" Y="-28.2314765625" />
                  <Point X="0.356754089355" Y="-28.2090234375" />
                  <Point X="0.330499816895" Y="-28.189779296875" />
                  <Point X="0.302496154785" Y="-28.175669921875" />
                  <Point X="0.077937294006" Y="-28.1059765625" />
                  <Point X="0.049137039185" Y="-28.097037109375" />
                  <Point X="0.020984472275" Y="-28.092767578125" />
                  <Point X="-0.008657006264" Y="-28.092765625" />
                  <Point X="-0.036822929382" Y="-28.09703515625" />
                  <Point X="-0.26138180542" Y="-28.16673046875" />
                  <Point X="-0.290182067871" Y="-28.17566796875" />
                  <Point X="-0.318182220459" Y="-28.189775390625" />
                  <Point X="-0.344438323975" Y="-28.20901953125" />
                  <Point X="-0.366323150635" Y="-28.2314765625" />
                  <Point X="-0.511439331055" Y="-28.4405625" />
                  <Point X="-0.530050964355" Y="-28.467376953125" />
                  <Point X="-0.538187866211" Y="-28.4815703125" />
                  <Point X="-0.55099017334" Y="-28.5125234375" />
                  <Point X="-0.59652545166" Y="-28.68246484375" />
                  <Point X="-0.916584655762" Y="-29.87694140625" />
                  <Point X="-1.049545410156" Y="-29.8511328125" />
                  <Point X="-1.079324584961" Y="-29.8453515625" />
                  <Point X="-1.246417724609" Y="-29.802361328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.270155761719" Y="-29.2465234375" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.2879375" Y="-29.18296875" />
                  <Point X="-1.304009277344" Y="-29.155130859375" />
                  <Point X="-1.32364453125" Y="-29.131203125" />
                  <Point X="-1.530389892578" Y="-28.949892578125" />
                  <Point X="-1.556905395508" Y="-28.926638671875" />
                  <Point X="-1.583188964844" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.64302746582" Y="-28.890966796875" />
                  <Point X="-1.917424682617" Y="-28.872982421875" />
                  <Point X="-1.952616455078" Y="-28.87067578125" />
                  <Point X="-1.98341418457" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.271300048828" Y="-29.04757421875" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.360667236328" Y="-29.132779296875" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.758043457031" Y="-29.116423828125" />
                  <Point X="-2.801728515625" Y="-29.089375" />
                  <Point X="-3.104721679688" Y="-28.856078125" />
                  <Point X="-3.083573730469" Y="-28.81944921875" />
                  <Point X="-2.423760986328" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406587890625" Y="-27.616125" />
                  <Point X="-2.405575683594" Y="-27.585189453125" />
                  <Point X="-2.414561035156" Y="-27.555572265625" />
                  <Point X="-2.428779541016" Y="-27.5267421875" />
                  <Point X="-2.446808837891" Y="-27.501583984375" />
                  <Point X="-2.462174804688" Y="-27.48621875" />
                  <Point X="-2.464144775391" Y="-27.484248046875" />
                  <Point X="-2.489299072266" Y="-27.466220703125" />
                  <Point X="-2.518129394531" Y="-27.452" />
                  <Point X="-2.54775" Y="-27.44301171875" />
                  <Point X="-2.578687744141" Y="-27.444025390625" />
                  <Point X="-2.610217041016" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-2.78575390625" Y="-27.545822265625" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.048361816406" Y="-27.839185546875" />
                  <Point X="-4.082860595703" Y="-27.793861328125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-4.260412597656" Y="-27.384361328125" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084576171875" Y="-26.47559375" />
                  <Point X="-3.066611083984" Y="-26.4484609375" />
                  <Point X="-3.053855957031" Y="-26.41983203125" />
                  <Point X="-3.04703125" Y="-26.39348046875" />
                  <Point X="-3.046155273438" Y="-26.390099609375" />
                  <Point X="-3.043348388672" Y="-26.35966796875" />
                  <Point X="-3.045555664062" Y="-26.327994140625" />
                  <Point X="-3.052556640625" Y="-26.298244140625" />
                  <Point X="-3.068640625" Y="-26.2722578125" />
                  <Point X="-3.089476074219" Y="-26.248298828125" />
                  <Point X="-3.112975830078" Y="-26.228765625" />
                  <Point X="-3.136448242188" Y="-26.214951171875" />
                  <Point X="-3.139458496094" Y="-26.2131796875" />
                  <Point X="-3.16872265625" Y="-26.20195703125" />
                  <Point X="-3.200607910156" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-3.416959960938" Y="-26.218744140625" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.820583984375" Y="-26.04548046875" />
                  <Point X="-4.834076660156" Y="-25.99265625" />
                  <Point X="-4.892423828125" Y="-25.584701171875" />
                  <Point X="-4.847977539062" Y="-25.572791015625" />
                  <Point X="-3.532875488281" Y="-25.22041015625" />
                  <Point X="-3.517489257812" Y="-25.214826171875" />
                  <Point X="-3.487730224609" Y="-25.199470703125" />
                  <Point X="-3.463132080078" Y="-25.1823984375" />
                  <Point X="-3.459977294922" Y="-25.180208984375" />
                  <Point X="-3.437523193359" Y="-25.158326171875" />
                  <Point X="-3.418277832031" Y="-25.1320703125" />
                  <Point X="-3.404167724609" Y="-25.10406640625" />
                  <Point X="-3.395975585938" Y="-25.077669921875" />
                  <Point X="-3.394923583984" Y="-25.074283203125" />
                  <Point X="-3.390649414062" Y="-25.046115234375" />
                  <Point X="-3.390647216797" Y="-25.016470703125" />
                  <Point X="-3.394916259766" Y="-24.9883046875" />
                  <Point X="-3.403115722656" Y="-24.961884765625" />
                  <Point X="-3.404167236328" Y="-24.95849609375" />
                  <Point X="-3.418275878906" Y="-24.930494140625" />
                  <Point X="-3.437521728516" Y="-24.904236328125" />
                  <Point X="-3.459977294922" Y="-24.8823515625" />
                  <Point X="-3.484575439453" Y="-24.865279296875" />
                  <Point X="-3.496078125" Y="-24.858443359375" />
                  <Point X="-3.514799804688" Y="-24.849033203125" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-3.701540771484" Y="-24.79695703125" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.833182617188" Y="-24.08178515625" />
                  <Point X="-4.824487792969" Y="-24.0230234375" />
                  <Point X="-4.703551269531" Y="-23.576732421875" />
                  <Point X="-3.765667480469" Y="-23.70020703125" />
                  <Point X="-3.744992675781" Y="-23.700658203125" />
                  <Point X="-3.723433349609" Y="-23.6987734375" />
                  <Point X="-3.703139892578" Y="-23.69473828125" />
                  <Point X="-3.648696289062" Y="-23.677572265625" />
                  <Point X="-3.641713867188" Y="-23.67537109375" />
                  <Point X="-3.622781738281" Y="-23.667041015625" />
                  <Point X="-3.604036621094" Y="-23.65621875" />
                  <Point X="-3.587355957031" Y="-23.64398828125" />
                  <Point X="-3.573717529297" Y="-23.6284375" />
                  <Point X="-3.561302001953" Y="-23.61070703125" />
                  <Point X="-3.551351806641" Y="-23.5925703125" />
                  <Point X="-3.529506103516" Y="-23.539830078125" />
                  <Point X="-3.526704345703" Y="-23.53306640625" />
                  <Point X="-3.520916503906" Y="-23.513208984375" />
                  <Point X="-3.517157714844" Y="-23.49189453125" />
                  <Point X="-3.515804443359" Y="-23.471255859375" />
                  <Point X="-3.518950683594" Y="-23.450810546875" />
                  <Point X="-3.524552490234" Y="-23.42990234375" />
                  <Point X="-3.532050048828" Y="-23.41062109375" />
                  <Point X="-3.558409179688" Y="-23.359986328125" />
                  <Point X="-3.561789794922" Y="-23.3534921875" />
                  <Point X="-3.573281982422" Y="-23.33629296875" />
                  <Point X="-3.587194580078" Y="-23.319712890625" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-3.698882080078" Y="-23.231173828125" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.1149375" Y="-22.324220703125" />
                  <Point X="-4.081156005859" Y="-22.26634375" />
                  <Point X="-3.750505615234" Y="-21.841337890625" />
                  <Point X="-3.206656494141" Y="-22.155330078125" />
                  <Point X="-3.187729980469" Y="-22.163658203125" />
                  <Point X="-3.167088378906" Y="-22.17016796875" />
                  <Point X="-3.146796386719" Y="-22.174205078125" />
                  <Point X="-3.070971679688" Y="-22.18083984375" />
                  <Point X="-3.061222167969" Y="-22.18169140625" />
                  <Point X="-3.040544433594" Y="-22.181236328125" />
                  <Point X="-3.019089111328" Y="-22.178408203125" />
                  <Point X="-2.999005859375" Y="-22.1734921875" />
                  <Point X="-2.980461914062" Y="-22.164345703125" />
                  <Point X="-2.962210693359" Y="-22.15271875" />
                  <Point X="-2.946078125" Y="-22.139771484375" />
                  <Point X="-2.892257080078" Y="-22.085951171875" />
                  <Point X="-2.885354492188" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.850069580078" Y="-21.8880546875" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795410156" Y="-21.818466796875" />
                  <Point X="-2.912666748047" Y="-21.744212890625" />
                  <Point X="-3.183333251953" Y="-21.275404296875" />
                  <Point X="-2.759461425781" Y="-20.95042578125" />
                  <Point X="-2.700622314453" Y="-20.905314453125" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.04319519043" Y="-20.7702578125" />
                  <Point X="-2.028893554688" Y="-20.78519921875" />
                  <Point X="-2.012314697266" Y="-20.799111328125" />
                  <Point X="-1.995115234375" Y="-20.810603515625" />
                  <Point X="-1.91072277832" Y="-20.854537109375" />
                  <Point X="-1.899899291992" Y="-20.860171875" />
                  <Point X="-1.880625244141" Y="-20.86766796875" />
                  <Point X="-1.859717529297" Y="-20.873271484375" />
                  <Point X="-1.839268554688" Y="-20.876419921875" />
                  <Point X="-1.818622924805" Y="-20.87506640625" />
                  <Point X="-1.797306518555" Y="-20.871306640625" />
                  <Point X="-1.777452392578" Y="-20.865517578125" />
                  <Point X="-1.689551879883" Y="-20.829107421875" />
                  <Point X="-1.678278320312" Y="-20.8244375" />
                  <Point X="-1.660138916016" Y="-20.814486328125" />
                  <Point X="-1.642409423828" Y="-20.8020703125" />
                  <Point X="-1.626860473633" Y="-20.788431640625" />
                  <Point X="-1.614631225586" Y="-20.771751953125" />
                  <Point X="-1.603809692383" Y="-20.7530078125" />
                  <Point X="-1.595479736328" Y="-20.734076171875" />
                  <Point X="-1.566869873047" Y="-20.643337890625" />
                  <Point X="-1.563200439453" Y="-20.63169921875" />
                  <Point X="-1.559165039062" Y="-20.611412109375" />
                  <Point X="-1.557279052734" Y="-20.589853515625" />
                  <Point X="-1.557730224609" Y="-20.569173828125" />
                  <Point X="-1.562604370117" Y="-20.53215234375" />
                  <Point X="-1.584201904297" Y="-20.368103515625" />
                  <Point X="-1.025801757812" Y="-20.211546875" />
                  <Point X="-0.949624816895" Y="-20.19019140625" />
                  <Point X="-0.294711181641" Y="-20.113541015625" />
                  <Point X="-0.133903366089" Y="-20.713685546875" />
                  <Point X="-0.121129745483" Y="-20.741876953125" />
                  <Point X="-0.103271606445" Y="-20.768603515625" />
                  <Point X="-0.082114006042" Y="-20.791193359375" />
                  <Point X="-0.054818138123" Y="-20.805783203125" />
                  <Point X="-0.024380064011" Y="-20.816115234375" />
                  <Point X="0.006155910969" Y="-20.82115625" />
                  <Point X="0.036691894531" Y="-20.816115234375" />
                  <Point X="0.067129966736" Y="-20.805783203125" />
                  <Point X="0.094425949097" Y="-20.791193359375" />
                  <Point X="0.115583587646" Y="-20.768603515625" />
                  <Point X="0.133441726685" Y="-20.741876953125" />
                  <Point X="0.146215194702" Y="-20.713685546875" />
                  <Point X="0.168182006836" Y="-20.631705078125" />
                  <Point X="0.307419616699" Y="-20.112060546875" />
                  <Point X="0.777528015137" Y="-20.161294921875" />
                  <Point X="0.844029785156" Y="-20.168259765625" />
                  <Point X="1.414500244141" Y="-20.30598828125" />
                  <Point X="1.48103894043" Y="-20.3220546875" />
                  <Point X="1.852174682617" Y="-20.45666796875" />
                  <Point X="1.894646484375" Y="-20.472072265625" />
                  <Point X="2.253689453125" Y="-20.639984375" />
                  <Point X="2.294560546875" Y="-20.659099609375" />
                  <Point X="2.641449707031" Y="-20.861197265625" />
                  <Point X="2.6809765625" Y="-20.884224609375" />
                  <Point X="2.943259277344" Y="-21.070748046875" />
                  <Point X="2.911264648438" Y="-21.1261640625" />
                  <Point X="2.147580810547" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.114047607422" Y="-22.55510546875" />
                  <Point X="2.111674560547" Y="-22.566955078125" />
                  <Point X="2.107896240234" Y="-22.594984375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.115147949219" Y="-22.68058203125" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442138672" Y="-22.710396484375" />
                  <Point X="2.129708007812" Y="-22.732484375" />
                  <Point X="2.140070068359" Y="-22.75252734375" />
                  <Point X="2.17814453125" Y="-22.808640625" />
                  <Point X="2.185691894531" Y="-22.81834765625" />
                  <Point X="2.203875488281" Y="-22.83884375" />
                  <Point X="2.221599121094" Y="-22.854408203125" />
                  <Point X="2.277711181641" Y="-22.892482421875" />
                  <Point X="2.284901367188" Y="-22.897361328125" />
                  <Point X="2.304937011719" Y="-22.907720703125" />
                  <Point X="2.327033203125" Y="-22.9159921875" />
                  <Point X="2.348968261719" Y="-22.921337890625" />
                  <Point X="2.410501464844" Y="-22.928755859375" />
                  <Point X="2.423242431641" Y="-22.9294296875" />
                  <Point X="2.450039550781" Y="-22.92904296875" />
                  <Point X="2.4732109375" Y="-22.925828125" />
                  <Point X="2.544370849609" Y="-22.906798828125" />
                  <Point X="2.551285400391" Y="-22.9046640625" />
                  <Point X="2.572490722656" Y="-22.89722265625" />
                  <Point X="2.588533691406" Y="-22.88985546875" />
                  <Point X="2.758177734375" Y="-22.791912109375" />
                  <Point X="3.967326416016" Y="-22.093810546875" />
                  <Point X="4.099823730469" Y="-22.277951171875" />
                  <Point X="4.12327734375" Y="-22.310546875" />
                  <Point X="4.262198730469" Y="-22.540115234375" />
                  <Point X="4.236245117188" Y="-22.56003125" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221426025391" Y="-23.3397578125" />
                  <Point X="3.203974853516" Y="-23.35837109375" />
                  <Point X="3.152760986328" Y="-23.42518359375" />
                  <Point X="3.146309326172" Y="-23.43475390625" />
                  <Point X="3.131269775391" Y="-23.460279296875" />
                  <Point X="3.121629394531" Y="-23.48291796875" />
                  <Point X="3.102552001953" Y="-23.5511328125" />
                  <Point X="3.10010546875" Y="-23.5598828125" />
                  <Point X="3.096651855469" Y="-23.58218359375" />
                  <Point X="3.095836425781" Y="-23.60575" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.113399902344" Y="-23.704130859375" />
                  <Point X="3.116485351562" Y="-23.715482421875" />
                  <Point X="3.125692871094" Y="-23.742595703125" />
                  <Point X="3.136283447266" Y="-23.76426171875" />
                  <Point X="3.178878173828" Y="-23.82900390625" />
                  <Point X="3.184341064453" Y="-23.837306640625" />
                  <Point X="3.198890380859" Y="-23.854546875" />
                  <Point X="3.216130126953" Y="-23.870634765625" />
                  <Point X="3.234343994141" Y="-23.88396484375" />
                  <Point X="3.296069580078" Y="-23.9187109375" />
                  <Point X="3.307106933594" Y="-23.924017578125" />
                  <Point X="3.333004638672" Y="-23.93447265625" />
                  <Point X="3.356121582031" Y="-23.9405625" />
                  <Point X="3.439578613281" Y="-23.951591796875" />
                  <Point X="3.446370117188" Y="-23.9522421875" />
                  <Point X="3.470148681641" Y="-23.95366015625" />
                  <Point X="3.488203125" Y="-23.953015625" />
                  <Point X="3.649353515625" Y="-23.93180078125" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.835865234375" Y="-24.025826171875" />
                  <Point X="4.845937011719" Y="-24.067193359375" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.868859863281" Y="-24.36165625" />
                  <Point X="3.716580078125" Y="-24.67041015625" />
                  <Point X="3.704787597656" Y="-24.674416015625" />
                  <Point X="3.681547607422" Y="-24.684931640625" />
                  <Point X="3.599553466797" Y="-24.73232421875" />
                  <Point X="3.590337890625" Y="-24.738390625" />
                  <Point X="3.565510742188" Y="-24.75688671875" />
                  <Point X="3.547531494141" Y="-24.774421875" />
                  <Point X="3.498335205078" Y="-24.837111328125" />
                  <Point X="3.492025634766" Y="-24.845150390625" />
                  <Point X="3.480302734375" Y="-24.864427734375" />
                  <Point X="3.47052734375" Y="-24.88589453125" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.447281738281" Y="-24.993025390625" />
                  <Point X="3.445822021484" Y="-25.004205078125" />
                  <Point X="3.44371875" Y="-25.03399609375" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.461577636719" Y="-25.144181640625" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470526123047" Y="-25.176662109375" />
                  <Point X="3.480298095703" Y="-25.198123046875" />
                  <Point X="3.492023193359" Y="-25.21740625" />
                  <Point X="3.541219482422" Y="-25.28009375" />
                  <Point X="3.549039306641" Y="-25.28887890625" />
                  <Point X="3.569663574219" Y="-25.30934375" />
                  <Point X="3.589036132812" Y="-25.32415625" />
                  <Point X="3.671030273438" Y="-25.37155078125" />
                  <Point X="3.676728027344" Y="-25.374591796875" />
                  <Point X="3.699323974609" Y="-25.385677734375" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="3.864362304688" Y="-25.43175" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.860645507812" Y="-25.9114296875" />
                  <Point X="4.855021484375" Y="-25.948734375" />
                  <Point X="4.801174316406" Y="-26.18469921875" />
                  <Point X="4.760702636719" Y="-26.17937109375" />
                  <Point X="3.424381347656" Y="-26.00344140625" />
                  <Point X="3.408034912109" Y="-26.0027109375" />
                  <Point X="3.374661621094" Y="-26.005509765625" />
                  <Point X="3.213736328125" Y="-26.04048828125" />
                  <Point X="3.193097412109" Y="-26.04497265625" />
                  <Point X="3.163982177734" Y="-26.05659375" />
                  <Point X="3.136151611328" Y="-26.073486328125" />
                  <Point X="3.112396972656" Y="-26.0939609375" />
                  <Point X="3.015127685547" Y="-26.2109453125" />
                  <Point X="3.002652832031" Y="-26.22594921875" />
                  <Point X="2.987931640625" Y="-26.25033203125" />
                  <Point X="2.976588867188" Y="-26.277716796875" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.955816650391" Y="-26.456865234375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.95634765625" Y="-26.50756640625" />
                  <Point X="2.964079589844" Y="-26.5391875" />
                  <Point X="2.976450927734" Y="-26.567998046875" />
                  <Point X="3.065509277344" Y="-26.706521484375" />
                  <Point X="3.076931152344" Y="-26.724287109375" />
                  <Point X="3.086930419922" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.247771240234" Y="-26.86614453125" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.14066015625" Y="-27.724138671875" />
                  <Point X="4.124810546875" Y="-27.749787109375" />
                  <Point X="4.028980957031" Y="-27.8859453125" />
                  <Point X="3.991127929688" Y="-27.864091796875" />
                  <Point X="2.800953857422" Y="-27.176943359375" />
                  <Point X="2.78613671875" Y="-27.170015625" />
                  <Point X="2.754223388672" Y="-27.15982421875" />
                  <Point X="2.562696533203" Y="-27.125234375" />
                  <Point X="2.538133056641" Y="-27.120798828125" />
                  <Point X="2.506777099609" Y="-27.120396484375" />
                  <Point X="2.474604492188" Y="-27.12535546875" />
                  <Point X="2.444832763672" Y="-27.135177734375" />
                  <Point X="2.285720947266" Y="-27.218916015625" />
                  <Point X="2.265314697266" Y="-27.22965625" />
                  <Point X="2.242383056641" Y="-27.24655078125" />
                  <Point X="2.221424316406" Y="-27.267509765625" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.120792480469" Y="-27.44955078125" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229248047" Y="-27.499732421875" />
                  <Point X="2.095271484375" Y="-27.531904296875" />
                  <Point X="2.095675537109" Y="-27.563255859375" />
                  <Point X="2.130265136719" Y="-27.754783203125" />
                  <Point X="2.134701171875" Y="-27.77934765625" />
                  <Point X="2.138984619141" Y="-27.795138671875" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.239947509766" Y="-27.978720703125" />
                  <Point X="2.861283691406" Y="-29.05490625" />
                  <Point X="2.800661865234" Y="-29.09820703125" />
                  <Point X="2.781867675781" Y="-29.111630859375" />
                  <Point X="2.701763916016" Y="-29.16348046875" />
                  <Point X="2.666758789062" Y="-29.117861328125" />
                  <Point X="1.758546264648" Y="-27.934255859375" />
                  <Point X="1.747503417969" Y="-27.9221796875" />
                  <Point X="1.721922851562" Y="-27.900556640625" />
                  <Point X="1.533025756836" Y="-27.77911328125" />
                  <Point X="1.508799438477" Y="-27.7635390625" />
                  <Point X="1.479986816406" Y="-27.75116796875" />
                  <Point X="1.448368408203" Y="-27.7434375" />
                  <Point X="1.417101318359" Y="-27.741119140625" />
                  <Point X="1.210510131836" Y="-27.76012890625" />
                  <Point X="1.184014404297" Y="-27.76256640625" />
                  <Point X="1.156365112305" Y="-27.769396484375" />
                  <Point X="1.128978149414" Y="-27.780740234375" />
                  <Point X="1.104594848633" Y="-27.795462890625" />
                  <Point X="0.945070495605" Y="-27.928103515625" />
                  <Point X="0.92461126709" Y="-27.945115234375" />
                  <Point X="0.904141723633" Y="-27.96886328125" />
                  <Point X="0.88725" Y="-27.9966875" />
                  <Point X="0.875624389648" Y="-28.02580859375" />
                  <Point X="0.827927429199" Y="-28.245251953125" />
                  <Point X="0.821810302734" Y="-28.27339453125" />
                  <Point X="0.81972467041" Y="-28.289625" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="0.844717163086" Y="-28.512822265625" />
                  <Point X="1.022065429688" Y="-29.859916015625" />
                  <Point X="0.99347265625" Y="-29.86618359375" />
                  <Point X="0.975699035645" Y="-29.870080078125" />
                  <Point X="0.929315612793" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.031443115234" Y="-29.757873046875" />
                  <Point X="-1.058421142578" Y="-29.752634765625" />
                  <Point X="-1.14124597168" Y="-29.731326171875" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.176981201172" Y="-29.227990234375" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.18812487793" Y="-29.178470703125" />
                  <Point X="-1.199026245117" Y="-29.149505859375" />
                  <Point X="-1.205664428711" Y="-29.13546875" />
                  <Point X="-1.221736206055" Y="-29.107630859375" />
                  <Point X="-1.230570678711" Y="-29.0948671875" />
                  <Point X="-1.250205810547" Y="-29.070939453125" />
                  <Point X="-1.261006713867" Y="-29.05977734375" />
                  <Point X="-1.467752075195" Y="-28.878466796875" />
                  <Point X="-1.494267578125" Y="-28.855212890625" />
                  <Point X="-1.506739868164" Y="-28.84596484375" />
                  <Point X="-1.5330234375" Y="-28.82962109375" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591315795898" Y="-28.805474609375" />
                  <Point X="-1.62145690918" Y="-28.798447265625" />
                  <Point X="-1.636814331055" Y="-28.796169921875" />
                  <Point X="-1.911211547852" Y="-28.778185546875" />
                  <Point X="-1.946403320312" Y="-28.77587890625" />
                  <Point X="-1.961927734375" Y="-28.7761328125" />
                  <Point X="-1.992725463867" Y="-28.779166015625" />
                  <Point X="-2.007998779297" Y="-28.7819453125" />
                  <Point X="-2.039047729492" Y="-28.790263671875" />
                  <Point X="-2.053666992188" Y="-28.795494140625" />
                  <Point X="-2.081861328125" Y="-28.808267578125" />
                  <Point X="-2.095436523437" Y="-28.815810546875" />
                  <Point X="-2.324079101562" Y="-28.968583984375" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359683349609" Y="-28.9927578125" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.436035888672" Y="-29.074947265625" />
                  <Point X="-2.503200927734" Y="-29.162478515625" />
                  <Point X="-2.708032714844" Y="-29.03565234375" />
                  <Point X="-2.747612548828" Y="-29.01114453125" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849609375" Y="-27.71008203125" />
                  <Point X="-2.323947509766" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413085938" Y="-27.634658203125" />
                  <Point X="-2.311638671875" Y="-27.619232421875" />
                  <Point X="-2.310626464844" Y="-27.588296875" />
                  <Point X="-2.314667236328" Y="-27.557609375" />
                  <Point X="-2.323652587891" Y="-27.5279921875" />
                  <Point X="-2.329359375" Y="-27.513552734375" />
                  <Point X="-2.343577880859" Y="-27.48472265625" />
                  <Point X="-2.351560791016" Y="-27.471404296875" />
                  <Point X="-2.369590087891" Y="-27.44624609375" />
                  <Point X="-2.379635253906" Y="-27.434408203125" />
                  <Point X="-2.394987304688" Y="-27.419056640625" />
                  <Point X="-2.408805175781" Y="-27.40703125" />
                  <Point X="-2.433959472656" Y="-27.38900390625" />
                  <Point X="-2.447274169922" Y="-27.381021484375" />
                  <Point X="-2.476104492188" Y="-27.36680078125" />
                  <Point X="-2.490543945312" Y="-27.36109375" />
                  <Point X="-2.520164550781" Y="-27.35210546875" />
                  <Point X="-2.550861083984" Y="-27.3480625" />
                  <Point X="-2.581798828125" Y="-27.349076171875" />
                  <Point X="-2.597221191406" Y="-27.3508515625" />
                  <Point X="-2.628750488281" Y="-27.357123046875" />
                  <Point X="-2.643680908203" Y="-27.36138671875" />
                  <Point X="-2.672647705078" Y="-27.3722890625" />
                  <Point X="-2.686684082031" Y="-27.378927734375" />
                  <Point X="-2.833254150391" Y="-27.46355078125" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-3.972768554688" Y="-27.781646484375" />
                  <Point X="-4.004018798828" Y="-27.74058984375" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481201172" Y="-26.563310546875" />
                  <Point X="-3.015102783203" Y="-26.540388671875" />
                  <Point X="-3.005365478516" Y="-26.528041015625" />
                  <Point X="-2.987400390625" Y="-26.500908203125" />
                  <Point X="-2.979833984375" Y="-26.487123046875" />
                  <Point X="-2.967078857422" Y="-26.458494140625" />
                  <Point X="-2.961890136719" Y="-26.443650390625" />
                  <Point X="-2.955068115234" Y="-26.41730859375" />
                  <Point X="-2.951556884766" Y="-26.39882421875" />
                  <Point X="-2.94875" Y="-26.368392578125" />
                  <Point X="-2.948578125" Y="-26.353064453125" />
                  <Point X="-2.950785400391" Y="-26.321390625" />
                  <Point X="-2.953081787109" Y="-26.306232421875" />
                  <Point X="-2.960082763672" Y="-26.276482421875" />
                  <Point X="-2.971777587891" Y="-26.24824609375" />
                  <Point X="-2.987861572266" Y="-26.222259765625" />
                  <Point X="-2.996955322266" Y="-26.20991796875" />
                  <Point X="-3.017790771484" Y="-26.185958984375" />
                  <Point X="-3.028750244141" Y="-26.1752421875" />
                  <Point X="-3.05225" Y="-26.155708984375" />
                  <Point X="-3.064790283203" Y="-26.146892578125" />
                  <Point X="-3.088262695312" Y="-26.133078125" />
                  <Point X="-3.105442138672" Y="-26.124478515625" />
                  <Point X="-3.134706298828" Y="-26.113255859375" />
                  <Point X="-3.149801269531" Y="-26.108861328125" />
                  <Point X="-3.181686523438" Y="-26.102380859375" />
                  <Point X="-3.197298339844" Y="-26.10053515625" />
                  <Point X="-3.228619384766" Y="-26.099443359375" />
                  <Point X="-3.244328613281" Y="-26.100197265625" />
                  <Point X="-3.429359619141" Y="-26.124556640625" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.7285390625" Y="-26.02196875" />
                  <Point X="-4.740761230469" Y="-25.974119140625" />
                  <Point X="-4.786451660156" Y="-25.65465625" />
                  <Point X="-3.508286132812" Y="-25.312171875" />
                  <Point X="-3.500466308594" Y="-25.3097109375" />
                  <Point X="-3.473927246094" Y="-25.29925" />
                  <Point X="-3.444168212891" Y="-25.28389453125" />
                  <Point X="-3.433563476562" Y="-25.277515625" />
                  <Point X="-3.408965332031" Y="-25.260443359375" />
                  <Point X="-3.393673339844" Y="-25.248244140625" />
                  <Point X="-3.371219238281" Y="-25.226361328125" />
                  <Point X="-3.36090234375" Y="-25.21448828125" />
                  <Point X="-3.341656982422" Y="-25.188232421875" />
                  <Point X="-3.333438720703" Y="-25.174818359375" />
                  <Point X="-3.319328613281" Y="-25.146814453125" />
                  <Point X="-3.313436767578" Y="-25.132224609375" />
                  <Point X="-3.305251953125" Y="-25.1058515625" />
                  <Point X="-3.300998779297" Y="-25.08853515625" />
                  <Point X="-3.296724609375" Y="-25.0603671875" />
                  <Point X="-3.295649414062" Y="-25.046123046875" />
                  <Point X="-3.295647216797" Y="-25.016478515625" />
                  <Point X="-3.296719970703" Y="-25.002234375" />
                  <Point X="-3.300989013672" Y="-24.974068359375" />
                  <Point X="-3.304185302734" Y="-24.960146484375" />
                  <Point X="-3.312384765625" Y="-24.9337265625" />
                  <Point X="-3.319327636719" Y="-24.91575" />
                  <Point X="-3.333436279297" Y="-24.887748046875" />
                  <Point X="-3.341653564453" Y="-24.874333984375" />
                  <Point X="-3.360899414062" Y="-24.848076171875" />
                  <Point X="-3.371216796875" Y="-24.836201171875" />
                  <Point X="-3.393672363281" Y="-24.81431640625" />
                  <Point X="-3.405810546875" Y="-24.804306640625" />
                  <Point X="-3.430408691406" Y="-24.787234375" />
                  <Point X="-3.436041503906" Y="-24.78361328125" />
                  <Point X="-3.4534140625" Y="-24.7735625" />
                  <Point X="-3.472135742188" Y="-24.76415234375" />
                  <Point X="-3.480995117188" Y="-24.760251953125" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.676953125" Y="-24.705193359375" />
                  <Point X="-4.7854453125" Y="-24.408173828125" />
                  <Point X="-4.739206054688" Y="-24.09569140625" />
                  <Point X="-4.731331054688" Y="-24.04246875" />
                  <Point X="-4.633586425781" Y="-23.681763671875" />
                  <Point X="-3.778067382812" Y="-23.79439453125" />
                  <Point X="-3.767739990234" Y="-23.79518359375" />
                  <Point X="-3.747065185547" Y="-23.795634765625" />
                  <Point X="-3.736718994141" Y="-23.795296875" />
                  <Point X="-3.715159667969" Y="-23.793412109375" />
                  <Point X="-3.70490625" Y="-23.79194921875" />
                  <Point X="-3.684612792969" Y="-23.7879140625" />
                  <Point X="-3.674572753906" Y="-23.785341796875" />
                  <Point X="-3.620129150391" Y="-23.76817578125" />
                  <Point X="-3.603453857422" Y="-23.762326171875" />
                  <Point X="-3.584521728516" Y="-23.75399609375" />
                  <Point X="-3.575282470703" Y="-23.749314453125" />
                  <Point X="-3.556537353516" Y="-23.7384921875" />
                  <Point X="-3.547863037109" Y="-23.73283203125" />
                  <Point X="-3.531182373047" Y="-23.7206015625" />
                  <Point X="-3.515932861328" Y="-23.70662890625" />
                  <Point X="-3.502294433594" Y="-23.691078125" />
                  <Point X="-3.495899169922" Y="-23.6829296875" />
                  <Point X="-3.483483642578" Y="-23.66519921875" />
                  <Point X="-3.478012939453" Y="-23.656400390625" />
                  <Point X="-3.468062744141" Y="-23.638263671875" />
                  <Point X="-3.463583251953" Y="-23.62892578125" />
                  <Point X="-3.441737548828" Y="-23.576185546875" />
                  <Point X="-3.435499511719" Y="-23.559650390625" />
                  <Point X="-3.429711669922" Y="-23.53979296875" />
                  <Point X="-3.427360107422" Y="-23.52970703125" />
                  <Point X="-3.423601318359" Y="-23.508392578125" />
                  <Point X="-3.422361328125" Y="-23.498109375" />
                  <Point X="-3.421008056641" Y="-23.477470703125" />
                  <Point X="-3.421909667969" Y="-23.456806640625" />
                  <Point X="-3.425055908203" Y="-23.436361328125" />
                  <Point X="-3.427187255859" Y="-23.426224609375" />
                  <Point X="-3.4327890625" Y="-23.40531640625" />
                  <Point X="-3.436010986328" Y="-23.39547265625" />
                  <Point X="-3.443508544922" Y="-23.37619140625" />
                  <Point X="-3.447784179688" Y="-23.36675390625" />
                  <Point X="-3.474143310547" Y="-23.316119140625" />
                  <Point X="-3.482800292969" Y="-23.300712890625" />
                  <Point X="-3.494292480469" Y="-23.283513671875" />
                  <Point X="-3.500508300781" Y="-23.2752265625" />
                  <Point X="-3.514420898438" Y="-23.258646484375" />
                  <Point X="-3.521501708984" Y="-23.251087890625" />
                  <Point X="-3.536442871094" Y="-23.23678515625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.641049560547" Y="-23.1558046875" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.032891113281" Y="-22.372109375" />
                  <Point X="-4.002295898437" Y="-22.31969140625" />
                  <Point X="-3.726338867188" Y="-21.964986328125" />
                  <Point X="-3.254156494141" Y="-22.2376015625" />
                  <Point X="-3.244918457031" Y="-22.24228515625" />
                  <Point X="-3.225991943359" Y="-22.25061328125" />
                  <Point X="-3.216302978516" Y="-22.254259765625" />
                  <Point X="-3.195661376953" Y="-22.26076953125" />
                  <Point X="-3.185625488281" Y="-22.263341796875" />
                  <Point X="-3.165333496094" Y="-22.26737890625" />
                  <Point X="-3.155077392578" Y="-22.26884375" />
                  <Point X="-3.079252685547" Y="-22.275478515625" />
                  <Point X="-3.079237792969" Y="-22.27548046875" />
                  <Point X="-3.059131835938" Y="-22.27666796875" />
                  <Point X="-3.038454101562" Y="-22.276212890625" />
                  <Point X="-3.028129394531" Y="-22.275421875" />
                  <Point X="-3.006674072266" Y="-22.27259375" />
                  <Point X="-2.996501708984" Y="-22.27068359375" />
                  <Point X="-2.976418457031" Y="-22.265767578125" />
                  <Point X="-2.956982421875" Y="-22.25869140625" />
                  <Point X="-2.938438476562" Y="-22.249544921875" />
                  <Point X="-2.929419677734" Y="-22.24446875" />
                  <Point X="-2.911168457031" Y="-22.232841796875" />
                  <Point X="-2.902749267578" Y="-22.22680859375" />
                  <Point X="-2.886616699219" Y="-22.213861328125" />
                  <Point X="-2.878903320312" Y="-22.206947265625" />
                  <Point X="-2.825082275391" Y="-22.153126953125" />
                  <Point X="-2.811267089844" Y="-22.138513671875" />
                  <Point X="-2.798321533203" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.755431152344" Y="-21.879775390625" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774510009766" Y="-21.799138671875" />
                  <Point X="-2.782841308594" Y="-21.780205078125" />
                  <Point X="-2.7875234375" Y="-21.770966796875" />
                  <Point X="-2.830394775391" Y="-21.696712890625" />
                  <Point X="-3.059387695312" Y="-21.3000859375" />
                  <Point X="-2.701659179688" Y="-21.025818359375" />
                  <Point X="-2.648370361328" Y="-20.984962890625" />
                  <Point X="-2.192523681641" Y="-20.731703125" />
                  <Point X="-2.118563720703" Y="-20.82808984375" />
                  <Point X="-2.111823486328" Y="-20.835947265625" />
                  <Point X="-2.097521972656" Y="-20.850888671875" />
                  <Point X="-2.089960449219" Y="-20.85797265625" />
                  <Point X="-2.073381591797" Y="-20.871884765625" />
                  <Point X="-2.065093505859" Y="-20.8781015625" />
                  <Point X="-2.047894042969" Y="-20.88959375" />
                  <Point X="-2.038982666016" Y="-20.894869140625" />
                  <Point X="-1.954590209961" Y="-20.938802734375" />
                  <Point X="-1.934334350586" Y="-20.9487109375" />
                  <Point X="-1.915060180664" Y="-20.95620703125" />
                  <Point X="-1.905218383789" Y="-20.9594296875" />
                  <Point X="-1.884310668945" Y="-20.965033203125" />
                  <Point X="-1.874173950195" Y="-20.967166015625" />
                  <Point X="-1.853724975586" Y="-20.970314453125" />
                  <Point X="-1.833053710938" Y="-20.971216796875" />
                  <Point X="-1.812408081055" Y="-20.96986328125" />
                  <Point X="-1.802121582031" Y="-20.968623046875" />
                  <Point X="-1.780805175781" Y="-20.96486328125" />
                  <Point X="-1.770713867188" Y="-20.9625078125" />
                  <Point X="-1.750859741211" Y="-20.95671875" />
                  <Point X="-1.741096923828" Y="-20.95328515625" />
                  <Point X="-1.653196411133" Y="-20.916875" />
                  <Point X="-1.6325859375" Y="-20.9077265625" />
                  <Point X="-1.614446533203" Y="-20.897775390625" />
                  <Point X="-1.605644165039" Y="-20.892302734375" />
                  <Point X="-1.587914672852" Y="-20.87988671875" />
                  <Point X="-1.579764770508" Y="-20.87348828125" />
                  <Point X="-1.564215820312" Y="-20.859849609375" />
                  <Point X="-1.550246459961" Y="-20.844603515625" />
                  <Point X="-1.538017211914" Y="-20.827923828125" />
                  <Point X="-1.532357910156" Y="-20.81925" />
                  <Point X="-1.521536376953" Y="-20.800505859375" />
                  <Point X="-1.516854858398" Y="-20.791267578125" />
                  <Point X="-1.508524902344" Y="-20.7723359375" />
                  <Point X="-1.504876708984" Y="-20.762642578125" />
                  <Point X="-1.476266845703" Y="-20.671904296875" />
                  <Point X="-1.470025878906" Y="-20.650232421875" />
                  <Point X="-1.465990356445" Y="-20.6299453125" />
                  <Point X="-1.464526489258" Y="-20.61969140625" />
                  <Point X="-1.46264050293" Y="-20.5981328125" />
                  <Point X="-1.462301635742" Y="-20.58778125" />
                  <Point X="-1.462752807617" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.468417114258" Y="-20.519751953125" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-1.000155883789" Y="-20.30301953125" />
                  <Point X="-0.931166687012" Y="-20.2836796875" />
                  <Point X="-0.365222412109" Y="-20.21744140625" />
                  <Point X="-0.225666366577" Y="-20.7382734375" />
                  <Point X="-0.220435150146" Y="-20.752892578125" />
                  <Point X="-0.207661453247" Y="-20.781083984375" />
                  <Point X="-0.200119293213" Y="-20.79465625" />
                  <Point X="-0.182261199951" Y="-20.8213828125" />
                  <Point X="-0.172608886719" Y="-20.833544921875" />
                  <Point X="-0.15145123291" Y="-20.856134765625" />
                  <Point X="-0.126896499634" Y="-20.8749765625" />
                  <Point X="-0.099600570679" Y="-20.88956640625" />
                  <Point X="-0.085354026794" Y="-20.8957421875" />
                  <Point X="-0.054916057587" Y="-20.90607421875" />
                  <Point X="-0.039853721619" Y="-20.909845703125" />
                  <Point X="-0.009317661285" Y="-20.91488671875" />
                  <Point X="0.021629489899" Y="-20.91488671875" />
                  <Point X="0.052165550232" Y="-20.909845703125" />
                  <Point X="0.067227882385" Y="-20.90607421875" />
                  <Point X="0.097665855408" Y="-20.8957421875" />
                  <Point X="0.111912246704" Y="-20.88956640625" />
                  <Point X="0.139208175659" Y="-20.8749765625" />
                  <Point X="0.163763061523" Y="-20.856134765625" />
                  <Point X="0.184920715332" Y="-20.833544921875" />
                  <Point X="0.194573181152" Y="-20.8213828125" />
                  <Point X="0.212431259155" Y="-20.79465625" />
                  <Point X="0.219973724365" Y="-20.781083984375" />
                  <Point X="0.232747116089" Y="-20.752892578125" />
                  <Point X="0.237978057861" Y="-20.7382734375" />
                  <Point X="0.259944976807" Y="-20.65629296875" />
                  <Point X="0.378190734863" Y="-20.214990234375" />
                  <Point X="0.767632751465" Y="-20.25577734375" />
                  <Point X="0.827851257324" Y="-20.262083984375" />
                  <Point X="1.392204956055" Y="-20.3983359375" />
                  <Point X="1.453619995117" Y="-20.413166015625" />
                  <Point X="1.819782470703" Y="-20.545974609375" />
                  <Point X="1.858249511719" Y="-20.55992578125" />
                  <Point X="2.213444824219" Y="-20.7260390625" />
                  <Point X="2.250431396484" Y="-20.743337890625" />
                  <Point X="2.593626953125" Y="-20.943283203125" />
                  <Point X="2.629431396484" Y="-20.964142578125" />
                  <Point X="2.817779052734" Y="-21.0980859375" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.062370117188" Y="-22.406900390625" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.022272338867" Y="-22.530564453125" />
                  <Point X="2.017526123047" Y="-22.554263671875" />
                  <Point X="2.013747924805" Y="-22.58229296875" />
                  <Point X="2.0128984375" Y="-22.5943203125" />
                  <Point X="2.01273034668" Y="-22.618384765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.020831298828" Y="-22.691955078125" />
                  <Point X="2.023800537109" Y="-22.71096484375" />
                  <Point X="2.029143310547" Y="-22.732888671875" />
                  <Point X="2.032468261719" Y="-22.743693359375" />
                  <Point X="2.040734130859" Y="-22.76578125" />
                  <Point X="2.045318847656" Y="-22.77611328125" />
                  <Point X="2.055680908203" Y="-22.79615625" />
                  <Point X="2.061458251953" Y="-22.8058671875" />
                  <Point X="2.099532714844" Y="-22.86198046875" />
                  <Point X="2.114627441406" Y="-22.88139453125" />
                  <Point X="2.132811035156" Y="-22.901890625" />
                  <Point X="2.141189208984" Y="-22.9102265625" />
                  <Point X="2.158912841797" Y="-22.925791015625" />
                  <Point X="2.168258300781" Y="-22.93301953125" />
                  <Point X="2.224370361328" Y="-22.97109375" />
                  <Point X="2.241269042969" Y="-22.981748046875" />
                  <Point X="2.2613046875" Y="-22.992107421875" />
                  <Point X="2.271631835938" Y="-22.99669140625" />
                  <Point X="2.293728027344" Y="-23.004962890625" />
                  <Point X="2.304539550781" Y="-23.008291015625" />
                  <Point X="2.326474609375" Y="-23.01363671875" />
                  <Point X="2.337598144531" Y="-23.015654296875" />
                  <Point X="2.399131347656" Y="-23.023072265625" />
                  <Point X="2.42461328125" Y="-23.024419921875" />
                  <Point X="2.451410400391" Y="-23.024033203125" />
                  <Point X="2.463094970703" Y="-23.023142578125" />
                  <Point X="2.486266357422" Y="-23.019927734375" />
                  <Point X="2.497753173828" Y="-23.017603515625" />
                  <Point X="2.568913085938" Y="-22.99857421875" />
                  <Point X="2.5827421875" Y="-22.9943046875" />
                  <Point X="2.603947509766" Y="-22.98686328125" />
                  <Point X="2.612135986328" Y="-22.9835546875" />
                  <Point X="2.636033447266" Y="-22.97212890625" />
                  <Point X="2.805677490234" Y="-22.874185546875" />
                  <Point X="3.940405273438" Y="-22.21905078125" />
                  <Point X="4.022711425781" Y="-22.3334375" />
                  <Point X="4.043959960938" Y="-22.36296875" />
                  <Point X="4.136885253906" Y="-22.51652734375" />
                  <Point X="3.172949462891" Y="-23.256181640625" />
                  <Point X="3.168135986328" Y="-23.2601328125" />
                  <Point X="3.152122314453" Y="-23.27478125" />
                  <Point X="3.134671142578" Y="-23.29339453125" />
                  <Point X="3.128577392578" Y="-23.300576171875" />
                  <Point X="3.077363525391" Y="-23.367388671875" />
                  <Point X="3.064460205078" Y="-23.386529296875" />
                  <Point X="3.049420654297" Y="-23.4120546875" />
                  <Point X="3.043864746094" Y="-23.42305859375" />
                  <Point X="3.034224365234" Y="-23.445697265625" />
                  <Point X="3.030139892578" Y="-23.45733203125" />
                  <Point X="3.0110625" Y="-23.525546875" />
                  <Point X="3.006224609375" Y="-23.54534375" />
                  <Point X="3.002770996094" Y="-23.56764453125" />
                  <Point X="3.001708740234" Y="-23.5788984375" />
                  <Point X="3.000893310547" Y="-23.60246484375" />
                  <Point X="3.001175048828" Y="-23.613763671875" />
                  <Point X="3.003078125" Y="-23.63624609375" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.020359863281" Y="-23.723328125" />
                  <Point X="3.026530761719" Y="-23.74603125" />
                  <Point X="3.03573828125" Y="-23.77314453125" />
                  <Point X="3.04034375" Y="-23.784314453125" />
                  <Point X="3.050934326172" Y="-23.80598046875" />
                  <Point X="3.056919433594" Y="-23.8164765625" />
                  <Point X="3.099514160156" Y="-23.88121875" />
                  <Point X="3.111739257813" Y="-23.898576171875" />
                  <Point X="3.126288574219" Y="-23.91581640625" />
                  <Point X="3.134075683594" Y="-23.924001953125" />
                  <Point X="3.151315429688" Y="-23.94008984375" />
                  <Point X="3.160023925781" Y="-23.947296875" />
                  <Point X="3.178237792969" Y="-23.960626953125" />
                  <Point X="3.187743164063" Y="-23.96675" />
                  <Point X="3.24946875" Y="-24.00149609375" />
                  <Point X="3.271543457031" Y="-24.012109375" />
                  <Point X="3.297441162109" Y="-24.022564453125" />
                  <Point X="3.308803955078" Y="-24.026337890625" />
                  <Point X="3.331920898438" Y="-24.032427734375" />
                  <Point X="3.343675048828" Y="-24.034744140625" />
                  <Point X="3.427132080078" Y="-24.0457734375" />
                  <Point X="3.440715087891" Y="-24.04707421875" />
                  <Point X="3.464493652344" Y="-24.0484921875" />
                  <Point X="3.473537841797" Y="-24.048599609375" />
                  <Point X="3.500602539062" Y="-24.047203125" />
                  <Point X="3.661752929688" Y="-24.02598828125" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.743561035156" Y="-24.048296875" />
                  <Point X="4.752684570312" Y="-24.08576953125" />
                  <Point X="4.78387109375" Y="-24.286078125" />
                  <Point X="3.691991210938" Y="-24.5786484375" />
                  <Point X="3.686023925781" Y="-24.580458984375" />
                  <Point X="3.665624511719" Y="-24.58786328125" />
                  <Point X="3.642384521484" Y="-24.59837890625" />
                  <Point X="3.634007568359" Y="-24.602681640625" />
                  <Point X="3.552013427734" Y="-24.65007421875" />
                  <Point X="3.533582275391" Y="-24.66220703125" />
                  <Point X="3.508755126953" Y="-24.680703125" />
                  <Point X="3.499180664062" Y="-24.688876953125" />
                  <Point X="3.481201416016" Y="-24.706412109375" />
                  <Point X="3.472796630859" Y="-24.7157734375" />
                  <Point X="3.423600341797" Y="-24.778462890625" />
                  <Point X="3.410855957031" Y="-24.7957890625" />
                  <Point X="3.399133056641" Y="-24.81506640625" />
                  <Point X="3.393844970703" Y="-24.825056640625" />
                  <Point X="3.384069580078" Y="-24.8465234375" />
                  <Point X="3.380005615234" Y="-24.8570703125" />
                  <Point X="3.373158935547" Y="-24.878572265625" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.353977294922" Y="-24.97515625" />
                  <Point X="3.351057861328" Y="-24.997515625" />
                  <Point X="3.348954589844" Y="-25.027306640625" />
                  <Point X="3.348886230469" Y="-25.0396328125" />
                  <Point X="3.350346191406" Y="-25.06419140625" />
                  <Point X="3.351874511719" Y="-25.076423828125" />
                  <Point X="3.3682734375" Y="-25.16205078125" />
                  <Point X="3.373158935547" Y="-25.183986328125" />
                  <Point X="3.380004150391" Y="-25.205484375" />
                  <Point X="3.384067138672" Y="-25.216029296875" />
                  <Point X="3.393839111328" Y="-25.237490234375" />
                  <Point X="3.399125976562" Y="-25.24748046875" />
                  <Point X="3.410851074219" Y="-25.266763671875" />
                  <Point X="3.417289306641" Y="-25.276056640625" />
                  <Point X="3.466485595703" Y="-25.338744140625" />
                  <Point X="3.482125244141" Y="-25.356314453125" />
                  <Point X="3.502749511719" Y="-25.376779296875" />
                  <Point X="3.511960205078" Y="-25.384810546875" />
                  <Point X="3.531332763672" Y="-25.399623046875" />
                  <Point X="3.541494628906" Y="-25.406404296875" />
                  <Point X="3.623488769531" Y="-25.453798828125" />
                  <Point X="3.634884277344" Y="-25.459880859375" />
                  <Point X="3.657480224609" Y="-25.470966796875" />
                  <Point X="3.665951171875" Y="-25.474623046875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="3.839774658203" Y="-25.523513671875" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.76670703125" Y="-25.897267578125" />
                  <Point X="4.761611816406" Y="-25.931064453125" />
                  <Point X="4.727802734375" Y="-26.079220703125" />
                  <Point X="3.43678125" Y="-25.90925390625" />
                  <Point X="3.428622314453" Y="-25.90853515625" />
                  <Point X="3.400095703125" Y="-25.90804296875" />
                  <Point X="3.366722412109" Y="-25.910841796875" />
                  <Point X="3.354483642578" Y="-25.912677734375" />
                  <Point X="3.193558349609" Y="-25.94765625" />
                  <Point X="3.172919433594" Y="-25.952140625" />
                  <Point X="3.157880615234" Y="-25.9567421875" />
                  <Point X="3.128765380859" Y="-25.96836328125" />
                  <Point X="3.114688964844" Y="-25.9753828125" />
                  <Point X="3.086858398438" Y="-25.992275390625" />
                  <Point X="3.074128417969" Y="-26.00152734375" />
                  <Point X="3.050373779297" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.942079833984" Y="-26.150208984375" />
                  <Point X="2.921325927734" Y="-26.17684765625" />
                  <Point X="2.906604736328" Y="-26.20123046875" />
                  <Point X="2.900162597656" Y="-26.213978515625" />
                  <Point X="2.888819824219" Y="-26.24136328125" />
                  <Point X="2.884362304688" Y="-26.2549296875" />
                  <Point X="2.877531005859" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.861216308594" Y="-26.44816015625" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607910156" Y="-26.514591796875" />
                  <Point X="2.86406640625" Y="-26.530130859375" />
                  <Point X="2.871798339844" Y="-26.561751953125" />
                  <Point X="2.876787109375" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896541015625" Y="-26.619373046875" />
                  <Point X="2.985599365234" Y="-26.757896484375" />
                  <Point X="2.997021240234" Y="-26.775662109375" />
                  <Point X="3.001739501953" Y="-26.782349609375" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043487060547" Y="-26.828119140625" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.189938720703" Y="-26.941513671875" />
                  <Point X="4.087170166016" Y="-27.629984375" />
                  <Point X="4.059846679688" Y="-27.674197265625" />
                  <Point X="4.045493408203" Y="-27.697423828125" />
                  <Point X="4.001274169922" Y="-27.76025390625" />
                  <Point X="2.848452392578" Y="-27.094669921875" />
                  <Point X="2.841190185547" Y="-27.090884765625" />
                  <Point X="2.815036865234" Y="-27.079517578125" />
                  <Point X="2.783123535156" Y="-27.069326171875" />
                  <Point X="2.771107177734" Y="-27.0663359375" />
                  <Point X="2.579580322266" Y="-27.03174609375" />
                  <Point X="2.555016845703" Y="-27.027310546875" />
                  <Point X="2.539352050781" Y="-27.025806640625" />
                  <Point X="2.50799609375" Y="-27.025404296875" />
                  <Point X="2.492304931641" Y="-27.026505859375" />
                  <Point X="2.460132324219" Y="-27.03146484375" />
                  <Point X="2.444840087891" Y="-27.035138671875" />
                  <Point X="2.415068359375" Y="-27.0449609375" />
                  <Point X="2.400588867188" Y="-27.051109375" />
                  <Point X="2.241477050781" Y="-27.13484765625" />
                  <Point X="2.221070800781" Y="-27.145587890625" />
                  <Point X="2.208966064453" Y="-27.153171875" />
                  <Point X="2.186034423828" Y="-27.17006640625" />
                  <Point X="2.175207519531" Y="-27.179376953125" />
                  <Point X="2.154248779297" Y="-27.2003359375" />
                  <Point X="2.144939208984" Y="-27.211162109375" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.036724487305" Y="-27.405306640625" />
                  <Point X="2.025984741211" Y="-27.425712890625" />
                  <Point X="2.01983581543" Y="-27.440193359375" />
                  <Point X="2.010012329102" Y="-27.46996875" />
                  <Point X="2.006337524414" Y="-27.485263671875" />
                  <Point X="2.001379760742" Y="-27.517435546875" />
                  <Point X="2.000279418945" Y="-27.53312890625" />
                  <Point X="2.00068347168" Y="-27.56448046875" />
                  <Point X="2.002187866211" Y="-27.580138671875" />
                  <Point X="2.03677746582" Y="-27.771666015625" />
                  <Point X="2.041213500977" Y="-27.79623046875" />
                  <Point X="2.043014526367" Y="-27.80421875" />
                  <Point X="2.051234375" Y="-27.831537109375" />
                  <Point X="2.064069091797" Y="-27.862478515625" />
                  <Point X="2.069547119141" Y="-27.873580078125" />
                  <Point X="2.157675292969" Y="-28.026220703125" />
                  <Point X="2.735893798828" Y="-29.027724609375" />
                  <Point X="2.723752197266" Y="-29.03608203125" />
                  <Point X="1.833914672852" Y="-27.876423828125" />
                  <Point X="1.828653808594" Y="-27.870146484375" />
                  <Point X="1.808831420898" Y="-27.849626953125" />
                  <Point X="1.783250854492" Y="-27.82800390625" />
                  <Point X="1.773297729492" Y="-27.820646484375" />
                  <Point X="1.584400756836" Y="-27.699203125" />
                  <Point X="1.560174316406" Y="-27.68362890625" />
                  <Point X="1.546280151367" Y="-27.67624609375" />
                  <Point X="1.517467529297" Y="-27.663875" />
                  <Point X="1.502549072266" Y="-27.65888671875" />
                  <Point X="1.470930664062" Y="-27.65115625" />
                  <Point X="1.455393066406" Y="-27.648697265625" />
                  <Point X="1.424125976562" Y="-27.64637890625" />
                  <Point X="1.408396362305" Y="-27.64651953125" />
                  <Point X="1.201805297852" Y="-27.665529296875" />
                  <Point X="1.175309448242" Y="-27.667966796875" />
                  <Point X="1.161231811523" Y="-27.670337890625" />
                  <Point X="1.133582641602" Y="-27.67716796875" />
                  <Point X="1.120010986328" Y="-27.681626953125" />
                  <Point X="1.092624023438" Y="-27.692970703125" />
                  <Point X="1.079874023438" Y="-27.699416015625" />
                  <Point X="1.055490722656" Y="-27.714138671875" />
                  <Point X="1.043857421875" Y="-27.722416015625" />
                  <Point X="0.884333068848" Y="-27.855056640625" />
                  <Point X="0.863873779297" Y="-27.872068359375" />
                  <Point X="0.852652893066" Y="-27.883091796875" />
                  <Point X="0.832183410645" Y="-27.90683984375" />
                  <Point X="0.822934875488" Y="-27.919564453125" />
                  <Point X="0.806043151855" Y="-27.947388671875" />
                  <Point X="0.799020874023" Y="-27.96146484375" />
                  <Point X="0.787395263672" Y="-27.9905859375" />
                  <Point X="0.782791992188" Y="-28.005630859375" />
                  <Point X="0.735095031738" Y="-28.22507421875" />
                  <Point X="0.728977844238" Y="-28.253216796875" />
                  <Point X="0.727585144043" Y="-28.261287109375" />
                  <Point X="0.724724731445" Y="-28.28967578125" />
                  <Point X="0.724742248535" Y="-28.323169921875" />
                  <Point X="0.725555053711" Y="-28.33551953125" />
                  <Point X="0.750529968262" Y="-28.52522265625" />
                  <Point X="0.83309173584" Y="-29.152341796875" />
                  <Point X="0.655064880371" Y="-28.487935546875" />
                  <Point X="0.652606201172" Y="-28.480123046875" />
                  <Point X="0.642143920898" Y="-28.453576171875" />
                  <Point X="0.626786987305" Y="-28.423814453125" />
                  <Point X="0.620407653809" Y="-28.413208984375" />
                  <Point X="0.475291503906" Y="-28.204125" />
                  <Point X="0.456680053711" Y="-28.17730859375" />
                  <Point X="0.446671051025" Y="-28.165171875" />
                  <Point X="0.424789459229" Y="-28.14271875" />
                  <Point X="0.412916564941" Y="-28.13240234375" />
                  <Point X="0.386662322998" Y="-28.113158203125" />
                  <Point X="0.373245544434" Y="-28.104939453125" />
                  <Point X="0.345241882324" Y="-28.090830078125" />
                  <Point X="0.33065512085" Y="-28.084939453125" />
                  <Point X="0.106096191406" Y="-28.01524609375" />
                  <Point X="0.077295898438" Y="-28.006306640625" />
                  <Point X="0.063381523132" Y="-28.003111328125" />
                  <Point X="0.035228931427" Y="-27.998841796875" />
                  <Point X="0.020990709305" Y="-27.997767578125" />
                  <Point X="-0.008650790215" Y="-27.997765625" />
                  <Point X="-0.022894956589" Y="-27.998837890625" />
                  <Point X="-0.051060779572" Y="-28.003107421875" />
                  <Point X="-0.064982582092" Y="-28.0063046875" />
                  <Point X="-0.289541381836" Y="-28.076" />
                  <Point X="-0.318341674805" Y="-28.0849375" />
                  <Point X="-0.332927368164" Y="-28.090828125" />
                  <Point X="-0.360927459717" Y="-28.104935546875" />
                  <Point X="-0.374342163086" Y="-28.11315234375" />
                  <Point X="-0.400598205566" Y="-28.132396484375" />
                  <Point X="-0.412474639893" Y="-28.142716796875" />
                  <Point X="-0.434359527588" Y="-28.165173828125" />
                  <Point X="-0.444367645264" Y="-28.177310546875" />
                  <Point X="-0.589483947754" Y="-28.386396484375" />
                  <Point X="-0.60809552002" Y="-28.4132109375" />
                  <Point X="-0.612467834473" Y="-28.42012890625" />
                  <Point X="-0.625975280762" Y="-28.44526171875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.688288391113" Y="-28.657876953125" />
                  <Point X="-0.985425109863" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667063802452" Y="-28.962204317326" />
                  <Point X="2.686718182631" Y="-28.942549937146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.917424810658" Y="-27.71184330912" />
                  <Point X="4.037441747076" Y="-27.591826372701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.608732415638" Y="-28.886185415714" />
                  <Point X="2.637542566435" Y="-28.857375264917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.832250235462" Y="-27.662667595891" />
                  <Point X="3.961422865737" Y="-27.533494965616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550401028824" Y="-28.810166514103" />
                  <Point X="2.588366950238" Y="-28.772200592689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.747075660265" Y="-27.613491882662" />
                  <Point X="3.885403984397" Y="-27.47516355853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.49206964201" Y="-28.734147612492" />
                  <Point X="2.539191334042" Y="-28.68702592046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.661901085069" Y="-27.564316169433" />
                  <Point X="3.809385103057" Y="-27.416832151444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433738255196" Y="-28.65812871088" />
                  <Point X="2.490015717845" Y="-28.601851248231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.576726509872" Y="-27.515140456204" />
                  <Point X="3.733366221718" Y="-27.358500744358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375406868382" Y="-28.582109809269" />
                  <Point X="2.440840101648" Y="-28.516676576002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491551934676" Y="-27.465964742975" />
                  <Point X="3.657347340378" Y="-27.300169337273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.317075481568" Y="-28.506090907657" />
                  <Point X="2.391664485452" Y="-28.431501903773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.406377359479" Y="-27.416789029746" />
                  <Point X="3.581328459038" Y="-27.241837930187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.258744094754" Y="-28.430072006046" />
                  <Point X="2.342488869255" Y="-28.346327231545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.321202784283" Y="-27.367613316517" />
                  <Point X="3.505309577699" Y="-27.183506523101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.623347268681" Y="-26.065468832118" />
                  <Point X="4.762297086489" Y="-25.926519014311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.20041270794" Y="-28.354053104435" />
                  <Point X="2.293313253058" Y="-28.261152559316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.236028209086" Y="-27.318437603288" />
                  <Point X="3.429290696359" Y="-27.125175116015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.504626871663" Y="-26.049838940711" />
                  <Point X="4.779227491271" Y="-25.775238321103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.142081321126" Y="-28.278034202823" />
                  <Point X="2.244137636862" Y="-28.175977887087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.15085363389" Y="-27.269261890059" />
                  <Point X="3.353271815019" Y="-27.066843708929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.385906474644" Y="-26.034209049304" />
                  <Point X="4.673268672404" Y="-25.746846851545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.083749934311" Y="-28.202015301212" />
                  <Point X="2.194962020665" Y="-28.090803214858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.065679058693" Y="-27.22008617683" />
                  <Point X="3.27725293368" Y="-27.008512301844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.267186077626" Y="-26.018579157897" />
                  <Point X="4.567309853537" Y="-25.718455381986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.025418547497" Y="-28.1259963996" />
                  <Point X="2.145786312561" Y="-28.005628634537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980504483497" Y="-27.170910463601" />
                  <Point X="3.20123405234" Y="-26.950180894758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.148465680608" Y="-26.00294926649" />
                  <Point X="4.461351034671" Y="-25.690063912427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.967087160683" Y="-28.049977497989" />
                  <Point X="2.096610316212" Y="-27.920454342461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.8953299083" Y="-27.121734750372" />
                  <Point X="3.125215351663" Y="-26.891849307009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.029745283589" Y="-25.987319375083" />
                  <Point X="4.355392215804" Y="-25.661672442868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.811384647916" Y="-29.071329722331" />
                  <Point X="0.821141751672" Y="-29.061572618575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.908755773869" Y="-27.973958596378" />
                  <Point X="2.051221164725" Y="-27.831493205522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.806062669114" Y="-27.076651701132" />
                  <Point X="3.049406283916" Y="-26.833308086331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.911024886571" Y="-25.971689483676" />
                  <Point X="4.249433396937" Y="-25.63328097331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.782993124334" Y="-28.965370957487" />
                  <Point X="0.805511914151" Y="-28.942852167671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.850424387055" Y="-27.897939694766" />
                  <Point X="2.027586989658" Y="-27.720777092163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.6956548683" Y="-27.052709213522" />
                  <Point X="2.987504438975" Y="-26.760859642847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.792304489553" Y="-25.956059592269" />
                  <Point X="4.14347457807" Y="-25.604889503751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.754601600753" Y="-28.859412192643" />
                  <Point X="0.789882076629" Y="-28.824131716767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.78474602855" Y="-27.829267764846" />
                  <Point X="2.007035134432" Y="-27.606978658964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.58185660235" Y="-27.032157191046" />
                  <Point X="2.93492985263" Y="-26.679083940766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.673584092534" Y="-25.940429700862" />
                  <Point X="4.037515759204" Y="-25.576498034192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.726210077171" Y="-28.753453427799" />
                  <Point X="0.774252239108" Y="-28.705411265862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.703737708717" Y="-27.775925796254" />
                  <Point X="2.010168689371" Y="-27.469494815599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.444369615065" Y="-27.035293889905" />
                  <Point X="2.884659138069" Y="-26.595004366902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.554863695516" Y="-25.924799809455" />
                  <Point X="3.931556940337" Y="-25.548106564634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.69781855359" Y="-28.647494662955" />
                  <Point X="0.758622401587" Y="-28.586690814958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.621961799691" Y="-27.723351416854" />
                  <Point X="2.859475384312" Y="-26.485837832233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.436117759556" Y="-25.909195456989" />
                  <Point X="3.825598097407" Y="-25.519715119138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.669427030009" Y="-28.541535898111" />
                  <Point X="0.742992554866" Y="-28.467970373254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.538190315958" Y="-27.672772612162" />
                  <Point X="2.871190023322" Y="-26.339772904798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.282677503242" Y="-25.928285424878" />
                  <Point X="3.719639098689" Y="-25.491323829431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.635640360334" Y="-28.44097227936" />
                  <Point X="0.727362698266" Y="-28.349249941428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.429812123022" Y="-27.646800516672" />
                  <Point X="3.019422158392" Y="-26.057190481302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.077630701515" Y="-25.998981938179" />
                  <Point X="3.623061045858" Y="-25.453551593837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.582979710928" Y="-28.359282640341" />
                  <Point X="0.740068088927" Y="-28.202194262342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.284326343808" Y="-27.657936007461" />
                  <Point X="3.538114016204" Y="-25.404148335065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.609447157042" Y="-24.332815194227" />
                  <Point X="4.766669394061" Y="-24.175592957208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.527936263148" Y="-28.279975799695" />
                  <Point X="0.777379499434" Y="-28.03053256341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.12935514467" Y="-27.678556918173" />
                  <Point X="3.467748790314" Y="-25.340163272529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.425920795504" Y="-24.381991267339" />
                  <Point X="4.746704438555" Y="-24.061207624288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.074847048752" Y="-29.74840882317" />
                  <Point X="-0.945960977279" Y="-29.619522751697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.472892865388" Y="-28.20066890903" />
                  <Point X="3.409318533335" Y="-25.264243241083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.242394433967" Y="-24.431167340451" />
                  <Point X="4.72040098024" Y="-23.953160794178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.133533553997" Y="-29.672745039989" />
                  <Point X="-0.896785261981" Y="-29.435996747974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.409392357175" Y="-28.129819128817" />
                  <Point X="3.369892268045" Y="-25.169319217948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.058868072429" Y="-24.480343413564" />
                  <Point X="4.642318558634" Y="-23.896892927358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120586187702" Y="-29.525447385269" />
                  <Point X="-0.847609546683" Y="-29.25247074425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.322463933875" Y="-28.082397263692" />
                  <Point X="3.349803226345" Y="-25.055057971222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.875341710891" Y="-24.529519486676" />
                  <Point X="4.487598800437" Y="-23.91726239713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.14056360436" Y="-29.411074513501" />
                  <Point X="-0.798433831385" Y="-29.068944740527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.219934376466" Y="-28.050576532676" />
                  <Point X="3.367863594568" Y="-24.902647314574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.691806396778" Y="-24.578704512363" />
                  <Point X="4.332879042239" Y="-23.937631866903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.162853685547" Y="-29.299014306263" />
                  <Point X="-0.749258116087" Y="-28.885418736803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.117404819056" Y="-28.01875580166" />
                  <Point X="4.178159284041" Y="-23.958001336675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.185578230504" Y="-29.187388562794" />
                  <Point X="-0.700082400789" Y="-28.70189273308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.00404387082" Y="-27.997766461471" />
                  <Point X="4.023439525843" Y="-23.978370806447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.229276671067" Y="-29.096736714932" />
                  <Point X="-0.650907258362" Y="-28.518367302227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.172085787514" Y="-28.039545831379" />
                  <Point X="3.868719767646" Y="-23.99874027622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.295988995865" Y="-29.029098751305" />
                  <Point X="-0.444746156266" Y="-28.177855911705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.395687385813" Y="-28.128797141253" />
                  <Point X="3.714000009448" Y="-24.019109745992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.367567081234" Y="-28.966326548248" />
                  <Point X="3.559281162421" Y="-24.039478304594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.439145166602" Y="-28.903554345191" />
                  <Point X="3.419627510448" Y="-24.044781668141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.512391582733" Y="-28.842450472897" />
                  <Point X="3.304988172041" Y="-24.025070718122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.606277673966" Y="-28.801986275704" />
                  <Point X="3.214114085025" Y="-23.981594516713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.728783801624" Y="-28.790142114937" />
                  <Point X="3.135772714558" Y="-23.925585598754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.854870211487" Y="-28.781878236375" />
                  <Point X="3.078194404342" Y="-23.848813620545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.985829067718" Y="-28.77848680418" />
                  <Point X="3.03162513668" Y="-23.761032599782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.503800085616" Y="-29.162107533652" />
                  <Point X="-2.500001536281" Y="-29.158308984317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.282483084111" Y="-28.940790532147" />
                  <Point X="3.005756210847" Y="-23.652551237189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586774755547" Y="-29.110731915158" />
                  <Point X="3.015974665247" Y="-23.507982494364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.580500829272" Y="-22.943456330339" />
                  <Point X="4.088080098052" Y="-22.435877061558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.669749425479" Y="-29.059356296664" />
                  <Point X="4.036711727811" Y="-22.352895143375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752287997099" Y="-29.007544579859" />
                  <Point X="3.980493014362" Y="-22.274763568398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.828193433623" Y="-28.949099727958" />
                  <Point X="3.849195680084" Y="-22.27171061425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.904098870147" Y="-28.890654876056" />
                  <Point X="3.531319899224" Y="-22.455236106685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.980004306671" Y="-28.832210024155" />
                  <Point X="3.213444118364" Y="-22.63876159912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.799413764707" Y="-28.517269193765" />
                  <Point X="2.895568337504" Y="-22.822287091554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.615888238445" Y="-28.199393379078" />
                  <Point X="2.592692088666" Y="-22.990813051966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.432362712183" Y="-27.881517564391" />
                  <Point X="2.424736711599" Y="-23.024418140608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.312575938392" Y="-27.627380502174" />
                  <Point X="2.306126740631" Y="-23.008677823151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.33059435611" Y="-27.511048631466" />
                  <Point X="2.215428158375" Y="-22.965026116981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.383969803982" Y="-27.430073790913" />
                  <Point X="2.138526605305" Y="-22.907577381626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.462002809479" Y="-27.373756507985" />
                  <Point X="2.079417935732" Y="-22.832335762774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.571329743353" Y="-27.348733153433" />
                  <Point X="2.032806422966" Y="-22.744596987114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803071589824" Y="-27.446124711478" />
                  <Point X="2.013369494129" Y="-22.629683627526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.120948284986" Y="-27.629651118215" />
                  <Point X="2.038382249689" Y="-22.47032058354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438824613503" Y="-27.813177158306" />
                  <Point X="2.191472655568" Y="-22.182879889235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.75670094202" Y="-27.996703198398" />
                  <Point X="2.374998782425" Y="-21.865003473953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.84450574089" Y="-27.950157708843" />
                  <Point X="2.558524909281" Y="-21.547127058672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.902570611149" Y="-27.873872290676" />
                  <Point X="2.742051036137" Y="-21.22925064339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.960635481407" Y="-27.797586872509" />
                  <Point X="2.771661752772" Y="-21.06528963833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.016709084344" Y="-27.719310187021" />
                  <Point X="2.693147116264" Y="-21.009453986412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.066898852316" Y="-27.635149666567" />
                  <Point X="2.613430327602" Y="-20.954820486649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.117088620287" Y="-27.550989146113" />
                  <Point X="-3.442932326289" Y="-26.876832852115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950198715652" Y="-26.384099241477" />
                  <Point X="2.528538042565" Y="-20.90536248326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167278388258" Y="-27.466828625659" />
                  <Point X="-4.020351521058" Y="-27.319901758458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.965017532716" Y="-26.264567770116" />
                  <Point X="2.443645844904" Y="-20.855904392496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.01929151898" Y="-26.184491467955" />
                  <Point X="2.358753647243" Y="-20.806446301732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.097569636974" Y="-26.128419297523" />
                  <Point X="2.273861449581" Y="-20.756988210968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.203808839489" Y="-26.100308211613" />
                  <Point X="2.184157184849" Y="-20.712342187275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.352256976007" Y="-26.114406059706" />
                  <Point X="2.092617177157" Y="-20.669531906541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506976179239" Y="-26.134774974512" />
                  <Point X="2.001077169465" Y="-20.626721625808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.661695491926" Y="-26.155143998774" />
                  <Point X="1.909537161773" Y="-20.583911345074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.816414804613" Y="-26.175513023036" />
                  <Point X="1.814895964414" Y="-20.544202254008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.971134117301" Y="-26.195882047297" />
                  <Point X="1.716305009489" Y="-20.508442920507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.125853429988" Y="-26.216251071559" />
                  <Point X="1.617714054565" Y="-20.472683587007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.280572742675" Y="-26.236620095821" />
                  <Point X="1.51912309964" Y="-20.436924253506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.435292055363" Y="-26.256989120083" />
                  <Point X="-3.47991192353" Y="-25.301608988251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.315407415593" Y="-25.137104480313" />
                  <Point X="1.417301087319" Y="-20.404395977402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.59001136805" Y="-26.277358144345" />
                  <Point X="-3.667481483091" Y="-25.354828259386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.299111160605" Y="-24.9864579369" />
                  <Point X="1.309079760578" Y="-20.378267015717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.675727225431" Y="-26.228723713301" />
                  <Point X="-3.851007577235" Y="-25.404004065105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333935925856" Y="-24.886932413725" />
                  <Point X="1.200857564542" Y="-20.352138923327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.70306232966" Y="-26.121708529104" />
                  <Point X="-4.03453367138" Y="-25.453179870824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.394767281427" Y="-24.813413480871" />
                  <Point X="1.092635368507" Y="-20.326010830937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.730397415212" Y="-26.014693326231" />
                  <Point X="-4.218059765524" Y="-25.502355676542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.477496380839" Y="-24.761792291857" />
                  <Point X="0.984413172472" Y="-20.299882738546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.751198334688" Y="-25.901143957281" />
                  <Point X="-4.401585859668" Y="-25.551531482261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.580967034907" Y="-24.7309126575" />
                  <Point X="0.876190976437" Y="-20.273754646156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76800914282" Y="-25.783604476988" />
                  <Point X="-4.585111953812" Y="-25.60070728798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.686925843765" Y="-24.702521177932" />
                  <Point X="0.14527092456" Y="-20.870324409608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.195299425328" Y="-20.82029590884" />
                  <Point X="0.760558854981" Y="-20.255036479187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784819950953" Y="-25.666064996695" />
                  <Point X="-4.768638047957" Y="-25.649883093699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.792884579733" Y="-24.674129625476" />
                  <Point X="-0.0301951356" Y="-20.911440181342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.272753126045" Y="-20.608491919697" />
                  <Point X="0.63894540741" Y="-20.242299638332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.898843315702" Y="-24.645738073019" />
                  <Point X="-0.127567172186" Y="-20.874461929502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.321928203089" Y="-20.424966554228" />
                  <Point X="0.517331959839" Y="-20.229562797477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.004802051671" Y="-24.617346520562" />
                  <Point X="-0.192906501613" Y="-20.805450970504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.371103280133" Y="-20.241441188759" />
                  <Point X="0.395718512268" Y="-20.216825956623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11076078764" Y="-24.588954968105" />
                  <Point X="-0.232938646433" Y="-20.711132826898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216719523608" Y="-24.560563415649" />
                  <Point X="-0.261330126576" Y="-20.605174018616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.322678259577" Y="-24.532171863192" />
                  <Point X="-0.289721606719" Y="-20.499215210334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.428636995546" Y="-24.503780310735" />
                  <Point X="-3.718566639426" Y="-23.793709954615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422318893634" Y="-23.497462208824" />
                  <Point X="-0.318113086862" Y="-20.393256402052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.534595731515" Y="-24.475388758279" />
                  <Point X="-3.844814153461" Y="-23.785607180226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441237852395" Y="-23.382030879159" />
                  <Point X="-0.346504567005" Y="-20.287297593769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.640554467483" Y="-24.446997205822" />
                  <Point X="-3.963534642818" Y="-23.769977381156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.487394483045" Y="-23.293837221384" />
                  <Point X="-0.417066516896" Y="-20.223509255235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.746513203452" Y="-24.418605653365" />
                  <Point X="-4.082255132174" Y="-23.754347582087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.552024104548" Y="-23.224116554461" />
                  <Point X="-0.569225541466" Y="-20.241317991379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770685474615" Y="-24.308427636103" />
                  <Point X="-4.20097562153" Y="-23.738717783017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.628042912201" Y="-23.165785073688" />
                  <Point X="-0.721384566036" Y="-20.259126727524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.747352452543" Y="-24.150744325605" />
                  <Point X="-4.319696110886" Y="-23.723087983948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.704061872616" Y="-23.107453745679" />
                  <Point X="-0.873543590606" Y="-20.276935463668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.71568357337" Y="-23.984725158007" />
                  <Point X="-4.438416600242" Y="-23.707458184879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.780080864565" Y="-23.049122449202" />
                  <Point X="-3.002830405432" Y="-22.271871990068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.753251604402" Y="-22.022293189039" />
                  <Point X="-1.643552934038" Y="-20.912594518675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.512634649906" Y="-20.781676234542" />
                  <Point X="-1.047154855823" Y="-20.316196440459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665744431858" Y="-23.800435728069" />
                  <Point X="-4.557137089598" Y="-23.691828385809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.856099856514" Y="-22.990791152725" />
                  <Point X="-3.135836093765" Y="-22.270527389976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754598745938" Y="-21.889290042149" />
                  <Point X="-1.83638028862" Y="-20.971071584832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462613393631" Y="-20.597304689843" />
                  <Point X="-1.233847355368" Y="-20.368538651579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.932118848462" Y="-22.932459856248" />
                  <Point X="-3.242852999755" Y="-22.243194007541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.781931557656" Y="-21.782272565442" />
                  <Point X="-1.943759563687" Y="-20.944100571473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474349563416" Y="-20.474690571202" />
                  <Point X="-1.420539854913" Y="-20.420880862699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.008137840411" Y="-22.874128559772" />
                  <Point X="-3.328619506216" Y="-22.194610225576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.830514610678" Y="-21.696505330038" />
                  <Point X="-2.032337701627" Y="-20.898328420987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.08415683236" Y="-22.815797263295" />
                  <Point X="-3.413794105833" Y="-22.145434536768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.879690262163" Y="-21.611330693098" />
                  <Point X="-2.108147400067" Y="-20.839787831002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.160175824308" Y="-22.757465966818" />
                  <Point X="-3.498968705451" Y="-22.09625884796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.928865913647" Y="-21.526156056157" />
                  <Point X="-2.16729357415" Y="-20.76458371666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.206352927534" Y="-22.669292781618" />
                  <Point X="-3.584143305068" Y="-22.047083159153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.978041565132" Y="-21.440981419216" />
                  <Point X="-2.364072588403" Y="-20.827012442487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.017989820765" Y="-22.346579386424" />
                  <Point X="-3.669317904686" Y="-21.997907470345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027217216617" Y="-21.355806782276" />
                  <Point X="-2.682670730018" Y="-21.011260295677" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998374023438" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.826585021973" Y="-29.862162109375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464319122314" Y="-28.521544921875" />
                  <Point X="0.319202911377" Y="-28.3124609375" />
                  <Point X="0.300591491699" Y="-28.28564453125" />
                  <Point X="0.274337280273" Y="-28.266400390625" />
                  <Point X="0.049778324127" Y="-28.19670703125" />
                  <Point X="0.020978147507" Y="-28.187767578125" />
                  <Point X="-0.008663235664" Y="-28.187765625" />
                  <Point X="-0.233222183228" Y="-28.2574609375" />
                  <Point X="-0.262022369385" Y="-28.2663984375" />
                  <Point X="-0.288278564453" Y="-28.285642578125" />
                  <Point X="-0.433394775391" Y="-28.494728515625" />
                  <Point X="-0.452006347656" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.504762542725" Y="-28.707052734375" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-1.067647216797" Y="-29.944392578125" />
                  <Point X="-1.100228637695" Y="-29.938068359375" />
                  <Point X="-1.351589477539" Y="-29.873396484375" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.363330322266" Y="-29.265056640625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282348633" Y="-29.20262890625" />
                  <Point X="-1.593027709961" Y="-29.021318359375" />
                  <Point X="-1.619543212891" Y="-28.998064453125" />
                  <Point X="-1.649240600586" Y="-28.985763671875" />
                  <Point X="-1.923637817383" Y="-28.967779296875" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.218520996094" Y="-29.126564453125" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.285298583984" Y="-29.190611328125" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.808054443359" Y="-29.1971953125" />
                  <Point X="-2.855842285156" Y="-29.16760546875" />
                  <Point X="-3.216723144531" Y="-28.88973828125" />
                  <Point X="-3.228580810547" Y="-28.880609375" />
                  <Point X="-3.165846435547" Y="-28.77194921875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.597591796875" />
                  <Point X="-2.513981201172" Y="-27.56876171875" />
                  <Point X="-2.529353515625" Y="-27.553390625" />
                  <Point X="-2.531323974609" Y="-27.551419921875" />
                  <Point X="-2.560154296875" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.738253662109" Y="-27.62809375" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.123955078125" Y="-27.896724609375" />
                  <Point X="-4.161701660156" Y="-27.8471328125" />
                  <Point X="-4.420432617188" Y="-27.41328125" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-4.318245117188" Y="-27.3089921875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145821777344" Y="-26.396013671875" />
                  <Point X="-3.138994628906" Y="-26.36965234375" />
                  <Point X="-3.138118652344" Y="-26.366271484375" />
                  <Point X="-3.140325927734" Y="-26.33459765625" />
                  <Point X="-3.161161376953" Y="-26.310638671875" />
                  <Point X="-3.184633789062" Y="-26.29682421875" />
                  <Point X="-3.187644042969" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.404560302734" Y="-26.312931640625" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.91262890625" Y="-26.0689921875" />
                  <Point X="-4.927392578125" Y="-26.011193359375" />
                  <Point X="-4.995846191406" Y="-25.532572265625" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-4.872565917969" Y="-25.48102734375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541896972656" Y="-25.12142578125" />
                  <Point X="-3.517298828125" Y="-25.104353515625" />
                  <Point X="-3.514144042969" Y="-25.1021640625" />
                  <Point X="-3.494898681641" Y="-25.075908203125" />
                  <Point X="-3.486701171875" Y="-25.049494140625" />
                  <Point X="-3.485649414063" Y="-25.046107421875" />
                  <Point X="-3.485647216797" Y="-25.016462890625" />
                  <Point X="-3.493846679688" Y="-24.99004296875" />
                  <Point X="-3.494898193359" Y="-24.986654296875" />
                  <Point X="-3.514144042969" Y="-24.960396484375" />
                  <Point X="-3.5387421875" Y="-24.94332421875" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.726128417969" Y="-24.888720703125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.927159179688" Y="-24.06787890625" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.779851074219" Y="-23.495080078125" />
                  <Point X="-4.773516113281" Y="-23.471701171875" />
                  <Point X="-4.695963378906" Y="-23.481912109375" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.73170703125" Y="-23.604134765625" />
                  <Point X="-3.677263427734" Y="-23.58696875" />
                  <Point X="-3.670281005859" Y="-23.584767578125" />
                  <Point X="-3.651535888672" Y="-23.5739453125" />
                  <Point X="-3.639120361328" Y="-23.55621484375" />
                  <Point X="-3.617274658203" Y="-23.503474609375" />
                  <Point X="-3.614472900391" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.475396484375" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.642675048828" Y="-23.403853515625" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.756714599609" Y="-23.30654296875" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.196983886719" Y="-22.27633203125" />
                  <Point X="-4.160016113281" Y="-22.21299609375" />
                  <Point X="-3.795002685547" Y="-21.743822265625" />
                  <Point X="-3.774671142578" Y="-21.717689453125" />
                  <Point X="-3.738135253906" Y="-21.738783203125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138515380859" Y="-22.07956640625" />
                  <Point X="-3.062690673828" Y="-22.086201171875" />
                  <Point X="-3.052959472656" Y="-22.08705078125" />
                  <Point X="-3.031504150391" Y="-22.08422265625" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.959431884766" Y="-22.018775390625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.944708007812" Y="-21.896333984375" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-2.994938720703" Y="-21.791712890625" />
                  <Point X="-3.307278808594" Y="-21.25072265625" />
                  <Point X="-2.817263427734" Y="-20.875033203125" />
                  <Point X="-2.752873779297" Y="-20.825666015625" />
                  <Point X="-2.177993408203" Y="-20.506275390625" />
                  <Point X="-2.141548828125" Y="-20.48602734375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247802734" Y="-20.726337890625" />
                  <Point X="-1.86685534668" Y="-20.770271484375" />
                  <Point X="-1.856031860352" Y="-20.77590625" />
                  <Point X="-1.835124267578" Y="-20.781509765625" />
                  <Point X="-1.813807861328" Y="-20.77775" />
                  <Point X="-1.725907348633" Y="-20.74133984375" />
                  <Point X="-1.714633789062" Y="-20.736669921875" />
                  <Point X="-1.696904418945" Y="-20.72425390625" />
                  <Point X="-1.686082885742" Y="-20.705509765625" />
                  <Point X="-1.657472900391" Y="-20.614771484375" />
                  <Point X="-1.653803588867" Y="-20.6031328125" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.656791625977" Y="-20.544552734375" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.051447753906" Y="-20.12007421875" />
                  <Point X="-0.968083618164" Y="-20.096703125" />
                  <Point X="-0.27116015625" Y="-20.01513671875" />
                  <Point X="-0.224199996948" Y="-20.009640625" />
                  <Point X="-0.213418167114" Y="-20.04987890625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282123566" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594043732" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.076419059753" Y="-20.6071171875" />
                  <Point X="0.236648422241" Y="-20.009130859375" />
                  <Point X="0.787423095703" Y="-20.0668125" />
                  <Point X="0.860208618164" Y="-20.074435546875" />
                  <Point X="1.436795532227" Y="-20.213640625" />
                  <Point X="1.508457763672" Y="-20.230943359375" />
                  <Point X="1.884566894531" Y="-20.367361328125" />
                  <Point X="1.931042602539" Y="-20.38421875" />
                  <Point X="2.293934082031" Y="-20.5539296875" />
                  <Point X="2.338688720703" Y="-20.574861328125" />
                  <Point X="2.689272460938" Y="-20.779111328125" />
                  <Point X="2.732519775391" Y="-20.804306640625" />
                  <Point X="3.063163574219" Y="-21.039443359375" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.993537353516" Y="-21.1736640625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.205822998047" Y="-22.579646484375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.209464599609" Y="-22.669208984375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218681884766" Y="-22.6991875" />
                  <Point X="2.256756347656" Y="-22.75530078125" />
                  <Point X="2.256759033203" Y="-22.7553046875" />
                  <Point X="2.274939941406" Y="-22.775796875" />
                  <Point X="2.331052001953" Y="-22.81387109375" />
                  <Point X="2.3382421875" Y="-22.81875" />
                  <Point X="2.360338378906" Y="-22.827021484375" />
                  <Point X="2.421871582031" Y="-22.834439453125" />
                  <Point X="2.448668701172" Y="-22.834052734375" />
                  <Point X="2.519828613281" Y="-22.8150234375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.710677978516" Y="-22.709638671875" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.176936035156" Y="-22.22246484375" />
                  <Point X="4.20259375" Y="-22.258123046875" />
                  <Point X="4.386918457031" Y="-22.56272265625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.294077636719" Y="-22.635400390625" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279372314453" Y="-23.416166015625" />
                  <Point X="3.228158447266" Y="-23.482978515625" />
                  <Point X="3.213118896484" Y="-23.50850390625" />
                  <Point X="3.194041503906" Y="-23.57671875" />
                  <Point X="3.191594970703" Y="-23.58546875" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.206439941406" Y="-23.68493359375" />
                  <Point X="3.215647460938" Y="-23.712046875" />
                  <Point X="3.2582421875" Y="-23.7767890625" />
                  <Point X="3.263705078125" Y="-23.785091796875" />
                  <Point X="3.280944824219" Y="-23.8011796875" />
                  <Point X="3.342670410156" Y="-23.83592578125" />
                  <Point X="3.368568115234" Y="-23.846380859375" />
                  <Point X="3.452025146484" Y="-23.85741015625" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.636954101562" Y="-23.83761328125" />
                  <Point X="4.848975097656" Y="-23.678046875" />
                  <Point X="4.928169433594" Y="-24.00335546875" />
                  <Point X="4.939189453125" Y="-24.048619140625" />
                  <Point X="4.997274902344" Y="-24.4216953125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.893447753906" Y="-24.453419921875" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087646484" Y="-24.767181640625" />
                  <Point X="3.647093505859" Y="-24.81457421875" />
                  <Point X="3.622266357422" Y="-24.8330703125" />
                  <Point X="3.573070068359" Y="-24.895759765625" />
                  <Point X="3.566760498047" Y="-24.903798828125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.540586181641" Y="-25.01089453125" />
                  <Point X="3.538482910156" Y="-25.040685546875" />
                  <Point X="3.554881835938" Y="-25.1263125" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566757080078" Y="-25.158755859375" />
                  <Point X="3.615953369141" Y="-25.221443359375" />
                  <Point X="3.636577636719" Y="-25.241908203125" />
                  <Point X="3.718571777344" Y="-25.289302734375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="3.888949951172" Y="-25.339986328125" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.954583984375" Y="-25.925591796875" />
                  <Point X="4.948430664062" Y="-25.96640625" />
                  <Point X="4.874546386719" Y="-26.290177734375" />
                  <Point X="4.748303222656" Y="-26.27355859375" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394839599609" Y="-26.098341796875" />
                  <Point X="3.233914306641" Y="-26.1333203125" />
                  <Point X="3.213275390625" Y="-26.1378046875" />
                  <Point X="3.185444824219" Y="-26.154697265625" />
                  <Point X="3.088175537109" Y="-26.271681640625" />
                  <Point X="3.075700683594" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.050416992188" Y="-26.4655703125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360839844" Y="-26.516623046875" />
                  <Point X="3.145419189453" Y="-26.655146484375" />
                  <Point X="3.156841064453" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.305603759766" Y="-26.790775390625" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.221473632812" Y="-27.774080078125" />
                  <Point X="4.204126953125" Y="-27.802150390625" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.943628662109" Y="-27.946365234375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737339599609" Y="-27.2533125" />
                  <Point X="2.545812744141" Y="-27.21872265625" />
                  <Point X="2.521249267578" Y="-27.214287109375" />
                  <Point X="2.489076660156" Y="-27.21924609375" />
                  <Point X="2.32996484375" Y="-27.302984375" />
                  <Point X="2.30955859375" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.204860595703" Y="-27.493794921875" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546373046875" />
                  <Point X="2.223752685547" Y="-27.737900390625" />
                  <Point X="2.228188720703" Y="-27.76246484375" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.322219726562" Y="-27.931220703125" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.855879150391" Y="-29.17551171875" />
                  <Point X="2.835303222656" Y="-29.190208984375" />
                  <Point X="2.679774902344" Y="-29.29087890625" />
                  <Point X="2.591390136719" Y="-29.175693359375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670547973633" Y="-27.980466796875" />
                  <Point X="1.481650878906" Y="-27.8590234375" />
                  <Point X="1.457424560547" Y="-27.84344921875" />
                  <Point X="1.425806152344" Y="-27.83571875" />
                  <Point X="1.21921496582" Y="-27.854728515625" />
                  <Point X="1.192719238281" Y="-27.857166015625" />
                  <Point X="1.165332275391" Y="-27.868509765625" />
                  <Point X="1.005807800293" Y="-28.001150390625" />
                  <Point X="0.985348632813" Y="-28.018162109375" />
                  <Point X="0.96845690918" Y="-28.045986328125" />
                  <Point X="0.920759887695" Y="-28.2654296875" />
                  <Point X="0.914642700195" Y="-28.293572265625" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.93890435791" Y="-28.500421875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.013813537598" Y="-29.95898046875" />
                  <Point X="0.994354248047" Y="-29.96324609375" />
                  <Point X="0.860200805664" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#206" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.171277830952" Y="4.994342073515" Z="2.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.25" />
                  <Point X="-0.283128192046" Y="5.06580346622" Z="2.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.25" />
                  <Point X="-1.071101298622" Y="4.959350642157" Z="2.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.25" />
                  <Point X="-1.712519868411" Y="4.480202116458" Z="2.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.25" />
                  <Point X="-1.711315179565" Y="4.431543099596" Z="2.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.25" />
                  <Point X="-1.751198562558" Y="4.336134458792" Z="2.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.25" />
                  <Point X="-1.849922576037" Y="4.305358990207" Z="2.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.25" />
                  <Point X="-2.111557992981" Y="4.580278730407" Z="2.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.25" />
                  <Point X="-2.208432099115" Y="4.568711462616" Z="2.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.25" />
                  <Point X="-2.853841234747" Y="4.195926327517" Z="2.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.25" />
                  <Point X="-3.044395923203" Y="3.214568065397" Z="2.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.25" />
                  <Point X="-3.000673879805" Y="3.130588239903" Z="2.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.25" />
                  <Point X="-3.00094265274" Y="3.047860915146" Z="2.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.25" />
                  <Point X="-3.064488225704" Y="2.994890819368" Z="2.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.25" />
                  <Point X="-3.7192912829" Y="3.335798025543" Z="2.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.25" />
                  <Point X="-3.840621815225" Y="3.318160495151" Z="2.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.25" />
                  <Point X="-4.246321928298" Y="2.780154074928" Z="2.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.25" />
                  <Point X="-3.793308638411" Y="1.685070197349" Z="2.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.25" />
                  <Point X="-3.693181665063" Y="1.604340000402" Z="2.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.25" />
                  <Point X="-3.669624191117" Y="1.546940339246" Z="2.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.25" />
                  <Point X="-3.698452417165" Y="1.491998535844" Z="2.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.25" />
                  <Point X="-4.69559308377" Y="1.598940988558" Z="2.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.25" />
                  <Point X="-4.834266983479" Y="1.549277389038" Z="2.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.25" />
                  <Point X="-4.982776755928" Y="0.970720533935" Z="2.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.25" />
                  <Point X="-3.74522522337" Y="0.09426254603" Z="2.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.25" />
                  <Point X="-3.573405977029" Y="0.046879449396" Z="2.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.25" />
                  <Point X="-3.547756255128" Y="0.026418699103" Z="2.25" />
                  <Point X="-3.539556741714" Y="0" Z="2.25" />
                  <Point X="-3.540608346007" Y="-0.003388251958" Z="2.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.25" />
                  <Point X="-3.551962618094" Y="-0.031996533738" Z="2.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.25" />
                  <Point X="-4.891662565403" Y="-0.401449510726" Z="2.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.25" />
                  <Point X="-5.051498585428" Y="-0.508370739248" Z="2.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.25" />
                  <Point X="-4.967229630298" Y="-1.050086884911" Z="2.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.25" />
                  <Point X="-3.404189831201" Y="-1.331223089101" Z="2.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.25" />
                  <Point X="-3.216148527614" Y="-1.308635074996" Z="2.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.25" />
                  <Point X="-3.193552100645" Y="-1.325831282188" Z="2.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.25" />
                  <Point X="-4.354838987306" Y="-2.238044054824" Z="2.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.25" />
                  <Point X="-4.469532314342" Y="-2.407609243926" Z="2.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.25" />
                  <Point X="-4.169897086498" Y="-2.895726504527" Z="2.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.25" />
                  <Point X="-2.719409672138" Y="-2.64011329863" Z="2.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.25" />
                  <Point X="-2.570867435576" Y="-2.557463033555" Z="2.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.25" />
                  <Point X="-3.215303714538" Y="-3.715668710354" Z="2.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.25" />
                  <Point X="-3.253382484296" Y="-3.898075873609" Z="2.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.25" />
                  <Point X="-2.840532302694" Y="-4.208425750049" Z="2.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.25" />
                  <Point X="-2.25178686864" Y="-4.189768592189" Z="2.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.25" />
                  <Point X="-2.196898420269" Y="-4.136858575686" Z="2.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.25" />
                  <Point X="-1.933064002958" Y="-3.986390975394" Z="2.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.25" />
                  <Point X="-1.632151336102" Y="-4.027629447113" Z="2.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.25" />
                  <Point X="-1.418525630012" Y="-4.243530324691" Z="2.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.25" />
                  <Point X="-1.407617669522" Y="-4.837868045817" Z="2.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.25" />
                  <Point X="-1.37948619773" Y="-4.888151544955" Z="2.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.25" />
                  <Point X="-1.083330138377" Y="-4.962197009392" Z="2.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.25" />
                  <Point X="-0.462622144358" Y="-3.688713384123" Z="2.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.25" />
                  <Point X="-0.39847532594" Y="-3.491957521785" Z="2.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.25" />
                  <Point X="-0.22455893825" Y="-3.273934175873" Z="2.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.25" />
                  <Point X="0.028800141111" Y="-3.213177869415" Z="2.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.25" />
                  <Point X="0.271970533492" Y="-3.309688233982" Z="2.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.25" />
                  <Point X="0.772132716044" Y="-4.843822645693" Z="2.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.25" />
                  <Point X="0.838168144167" Y="-5.010038837474" Z="2.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.25" />
                  <Point X="1.018362531082" Y="-4.976540148752" Z="2.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.25" />
                  <Point X="0.982320606478" Y="-3.462616284498" Z="2.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.25" />
                  <Point X="0.963463020517" Y="-3.244769606698" Z="2.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.25" />
                  <Point X="1.031619345882" Y="-3.008314751716" Z="2.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.25" />
                  <Point X="1.217639466309" Y="-2.873237280024" Z="2.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.25" />
                  <Point X="1.448456883062" Y="-2.869801943144" Z="2.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.25" />
                  <Point X="2.545566548883" Y="-4.174850800018" Z="2.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.25" />
                  <Point X="2.684238741178" Y="-4.312286120072" Z="2.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.25" />
                  <Point X="2.87878498704" Y="-4.184918906084" Z="2.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.25" />
                  <Point X="2.359364978702" Y="-2.874940874171" Z="2.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.25" />
                  <Point X="2.26680071023" Y="-2.697734926991" Z="2.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.25" />
                  <Point X="2.24295085421" Y="-2.485801794938" Z="2.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.25" />
                  <Point X="2.347096583435" Y="-2.315950455757" Z="2.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.25" />
                  <Point X="2.530771876521" Y="-2.23664729926" Z="2.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.25" />
                  <Point X="3.912472722936" Y="-2.958384521573" Z="2.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.25" />
                  <Point X="4.084963145513" Y="-3.018311087349" Z="2.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.25" />
                  <Point X="4.257851571663" Y="-2.769083743048" Z="2.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.25" />
                  <Point X="3.329885661914" Y="-1.719826443428" Z="2.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.25" />
                  <Point X="3.181320903751" Y="-1.596826981865" Z="2.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.25" />
                  <Point X="3.094050466858" Y="-1.438872304936" Z="2.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.25" />
                  <Point X="3.120466418477" Y="-1.272368725657" Z="2.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.25" />
                  <Point X="3.238374530194" Y="-1.150898210627" Z="2.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.25" />
                  <Point X="4.735620852114" Y="-1.291850407371" Z="2.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.25" />
                  <Point X="4.916604357478" Y="-1.27235571892" Z="2.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.25" />
                  <Point X="4.997869647486" Y="-0.90176560192" Z="2.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.25" />
                  <Point X="3.89573497447" Y="-0.26040877625" Z="2.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.25" />
                  <Point X="3.737436792246" Y="-0.214732253899" Z="2.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.25" />
                  <Point X="3.649133170242" Y="-0.159298365023" Z="2.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.25" />
                  <Point X="3.597833542226" Y="-0.085628501526" Z="2.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.25" />
                  <Point X="3.583537908198" Y="0.010982029655" Z="2.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.25" />
                  <Point X="3.606246268158" Y="0.104650373552" Z="2.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.25" />
                  <Point X="3.665958622106" Y="0.173416590343" Z="2.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.25" />
                  <Point X="4.900232842837" Y="0.529563156519" Z="2.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.25" />
                  <Point X="5.040523798928" Y="0.617276813127" Z="2.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.25" />
                  <Point X="4.970594671" Y="1.039753939291" Z="2.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.25" />
                  <Point X="3.624271844719" Y="1.243239946335" Z="2.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.25" />
                  <Point X="3.452417853862" Y="1.223438689025" Z="2.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.25" />
                  <Point X="3.36104418132" Y="1.238924848015" Z="2.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.25" />
                  <Point X="3.293855695102" Y="1.281974333387" Z="2.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.25" />
                  <Point X="3.249252508093" Y="1.35645055977" Z="2.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.25" />
                  <Point X="3.236038730266" Y="1.4410980069" Z="2.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.25" />
                  <Point X="3.261684271265" Y="1.517882548471" Z="2.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.25" />
                  <Point X="4.318359423557" Y="2.356212637873" Z="2.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.25" />
                  <Point X="4.423539637117" Y="2.494445211766" Z="2.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.25" />
                  <Point X="4.211365870517" Y="2.838018476285" Z="2.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.25" />
                  <Point X="2.679521921305" Y="2.364942771999" Z="2.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.25" />
                  <Point X="2.50075152235" Y="2.264558250185" Z="2.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.25" />
                  <Point X="2.421699907824" Y="2.246480773431" Z="2.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.25" />
                  <Point X="2.352970282834" Y="2.258783650128" Z="2.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.25" />
                  <Point X="2.291974950107" Y="2.304054577548" Z="2.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.25" />
                  <Point X="2.252948929327" Y="2.368058536009" Z="2.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.25" />
                  <Point X="2.247969692472" Y="2.438718053532" Z="2.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.25" />
                  <Point X="3.030682580861" Y="3.83261716065" Z="2.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.25" />
                  <Point X="3.08598449439" Y="4.032585860832" Z="2.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.25" />
                  <Point X="2.708286009277" Y="4.295372518166" Z="2.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.25" />
                  <Point X="2.308959878866" Y="4.522641526216" Z="2.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.25" />
                  <Point X="1.895459394655" Y="4.710923812723" Z="2.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.25" />
                  <Point X="1.442374959544" Y="4.866242441956" Z="2.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.25" />
                  <Point X="0.786475577119" Y="5.014195901209" Z="2.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.25" />
                  <Point X="0.021966730601" Y="4.437105432436" Z="2.25" />
                  <Point X="0" Y="4.355124473572" Z="2.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>