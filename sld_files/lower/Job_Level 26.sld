<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#167" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1945" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.76063885498" Y="-29.24899609375" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557720153809" Y="-28.497138671875" />
                  <Point X="0.542363037109" Y="-28.467376953125" />
                  <Point X="0.461706329346" Y="-28.35116796875" />
                  <Point X="0.378635223389" Y="-28.2314765625" />
                  <Point X="0.356755706787" Y="-28.209025390625" />
                  <Point X="0.330500366211" Y="-28.189779296875" />
                  <Point X="0.302494873047" Y="-28.17566796875" />
                  <Point X="0.177683532715" Y="-28.136931640625" />
                  <Point X="0.049135883331" Y="-28.09703515625" />
                  <Point X="0.020976791382" Y="-28.092765625" />
                  <Point X="-0.00866468811" Y="-28.092765625" />
                  <Point X="-0.036823932648" Y="-28.09703515625" />
                  <Point X="-0.161635116577" Y="-28.135771484375" />
                  <Point X="-0.290183074951" Y="-28.17566796875" />
                  <Point X="-0.318185058594" Y="-28.18977734375" />
                  <Point X="-0.344440246582" Y="-28.209021484375" />
                  <Point X="-0.366323577881" Y="-28.2314765625" />
                  <Point X="-0.446980133057" Y="-28.3476875" />
                  <Point X="-0.530051208496" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.754234436035" Y="-29.27104296875" />
                  <Point X="-0.916584899902" Y="-29.87694140625" />
                  <Point X="-0.931540222168" Y="-29.8740390625" />
                  <Point X="-1.079322998047" Y="-29.845353515625" />
                  <Point X="-1.220783691406" Y="-29.80895703125" />
                  <Point X="-1.246417602539" Y="-29.802361328125" />
                  <Point X="-1.238717651367" Y="-29.743875" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.246326049805" Y="-29.36632421875" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131203125" />
                  <Point X="-1.438555053711" Y="-29.0304296875" />
                  <Point X="-1.556905639648" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886474609" Y="-28.897994140625" />
                  <Point X="-1.643027587891" Y="-28.890966796875" />
                  <Point X="-1.795539306641" Y="-28.880970703125" />
                  <Point X="-1.952616699219" Y="-28.87067578125" />
                  <Point X="-1.983414794922" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.16973828125" Y="-28.979712890625" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.449208984375" Y="-29.248169921875" />
                  <Point X="-2.480147460938" Y="-29.288490234375" />
                  <Point X="-2.585068359375" Y="-29.223525390625" />
                  <Point X="-2.801726318359" Y="-29.089376953125" />
                  <Point X="-2.997564453125" Y="-28.938587890625" />
                  <Point X="-3.104722412109" Y="-28.856080078125" />
                  <Point X="-2.790489257812" Y="-28.3118125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858642578" Y="-27.647650390625" />
                  <Point X="-2.406587890625" Y="-27.61612109375" />
                  <Point X="-2.405576416016" Y="-27.58518359375" />
                  <Point X="-2.414563964844" Y="-27.555564453125" />
                  <Point X="-2.428783691406" Y="-27.526736328125" />
                  <Point X="-2.446809326172" Y="-27.501583984375" />
                  <Point X="-2.464154052734" Y="-27.484240234375" />
                  <Point X="-2.489305908203" Y="-27.466216796875" />
                  <Point X="-2.518135253906" Y="-27.451998046875" />
                  <Point X="-2.547755126953" Y="-27.443013671875" />
                  <Point X="-2.57869140625" Y="-27.444025390625" />
                  <Point X="-2.610218261719" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.293391357422" Y="-27.83890625" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.911701904297" Y="-28.018728515625" />
                  <Point X="-4.08286328125" Y="-27.793857421875" />
                  <Point X="-4.223270996094" Y="-27.5584140625" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.747609619141" Y="-26.990875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084576171875" Y="-26.47559375" />
                  <Point X="-3.066611083984" Y="-26.4484609375" />
                  <Point X="-3.053856201172" Y="-26.41983203125" />
                  <Point X="-3.046151611328" Y="-26.390083984375" />
                  <Point X="-3.043347412109" Y="-26.359654296875" />
                  <Point X="-3.045556640625" Y="-26.327984375" />
                  <Point X="-3.05255859375" Y="-26.29823828125" />
                  <Point X="-3.068641845703" Y="-26.272255859375" />
                  <Point X="-3.089474365234" Y="-26.24830078125" />
                  <Point X="-3.112973876953" Y="-26.228767578125" />
                  <Point X="-3.139456542969" Y="-26.213181640625" />
                  <Point X="-3.168716064453" Y="-26.201958984375" />
                  <Point X="-3.200603515625" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.057804199219" Y="-26.30311328125" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.767135742188" Y="-26.254728515625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.8712265625" Y="-25.732912109375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.263817871094" Y="-25.416265625" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.510330566406" Y="-25.21112890625" />
                  <Point X="-3.48108984375" Y="-25.194419921875" />
                  <Point X="-3.46789453125" Y="-25.18532421875" />
                  <Point X="-3.441995361328" Y="-25.164033203125" />
                  <Point X="-3.425827880859" Y="-25.146978515625" />
                  <Point X="-3.414340820312" Y="-25.126478515625" />
                  <Point X="-3.402358398438" Y="-25.097056640625" />
                  <Point X="-3.397795166016" Y="-25.08267578125" />
                  <Point X="-3.390854492188" Y="-25.052732421875" />
                  <Point X="-3.388400878906" Y="-25.031333984375" />
                  <Point X="-3.390830322266" Y="-25.00993359375" />
                  <Point X="-3.397634033203" Y="-24.9804296875" />
                  <Point X="-3.402177978516" Y="-24.966052734375" />
                  <Point X="-3.414297119141" Y="-24.93619140625" />
                  <Point X="-3.425758544922" Y="-24.915677734375" />
                  <Point X="-3.441904296875" Y="-24.89860546875" />
                  <Point X="-3.46739453125" Y="-24.87759765625" />
                  <Point X="-3.480573242188" Y="-24.868486328125" />
                  <Point X="-3.510223144531" Y="-24.8514921875" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.285700195312" Y="-24.640431640625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.867630371094" Y="-24.314576171875" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.749707519531" Y="-23.747064453125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.289623046875" Y="-23.6312265625" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744984863281" Y="-23.700658203125" />
                  <Point X="-3.723423828125" Y="-23.698771484375" />
                  <Point X="-3.703138183594" Y="-23.694736328125" />
                  <Point X="-3.672878173828" Y="-23.6851953125" />
                  <Point X="-3.641712158203" Y="-23.675369140625" />
                  <Point X="-3.622779052734" Y="-23.667037109375" />
                  <Point X="-3.604034423828" Y="-23.65621484375" />
                  <Point X="-3.587353271484" Y="-23.643984375" />
                  <Point X="-3.57371484375" Y="-23.628431640625" />
                  <Point X="-3.561300048828" Y="-23.610701171875" />
                  <Point X="-3.551351806641" Y="-23.592568359375" />
                  <Point X="-3.539209716797" Y="-23.563255859375" />
                  <Point X="-3.526704345703" Y="-23.533064453125" />
                  <Point X="-3.520915771484" Y="-23.513205078125" />
                  <Point X="-3.517157470703" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951660156" Y="-23.4508046875" />
                  <Point X="-3.524553955078" Y="-23.4298984375" />
                  <Point X="-3.532049804688" Y="-23.410623046875" />
                  <Point X="-3.546700439453" Y="-23.382478515625" />
                  <Point X="-3.561789550781" Y="-23.353494140625" />
                  <Point X="-3.573281494141" Y="-23.336294921875" />
                  <Point X="-3.587193603516" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.033957519531" Y="-22.974060546875" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.248787109375" Y="-22.553537109375" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.883064453125" Y="-22.0117265625" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.528456787109" Y="-21.969537109375" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187728515625" Y="-22.163658203125" />
                  <Point X="-3.167085693359" Y="-22.17016796875" />
                  <Point X="-3.146794921875" Y="-22.174205078125" />
                  <Point X="-3.104651123047" Y="-22.177892578125" />
                  <Point X="-3.061245605469" Y="-22.181689453125" />
                  <Point X="-3.040559814453" Y="-22.18123828125" />
                  <Point X="-3.019101806641" Y="-22.178412109375" />
                  <Point X="-2.999012695312" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.916163818359" Y="-22.109857421875" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.847123046875" Y="-21.921736328125" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.0611484375" Y="-21.487033203125" />
                  <Point X="-3.183333496094" Y="-21.275404296875" />
                  <Point X="-2.9925859375" Y="-21.129158203125" />
                  <Point X="-2.700629394531" Y="-20.905318359375" />
                  <Point X="-2.388643554688" Y="-20.731986328125" />
                  <Point X="-2.167036621094" Y="-20.608865234375" />
                  <Point X="-2.141708251953" Y="-20.641875" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028893432617" Y="-20.78519921875" />
                  <Point X="-2.012314697266" Y="-20.799111328125" />
                  <Point X="-1.995114379883" Y="-20.810603515625" />
                  <Point X="-1.948208496094" Y="-20.835021484375" />
                  <Point X="-1.89989831543" Y="-20.860171875" />
                  <Point X="-1.880626098633" Y="-20.86766796875" />
                  <Point X="-1.859719482422" Y="-20.873271484375" />
                  <Point X="-1.839269775391" Y="-20.876419921875" />
                  <Point X="-1.818623291016" Y="-20.87506640625" />
                  <Point X="-1.797307617188" Y="-20.871306640625" />
                  <Point X="-1.777452270508" Y="-20.865517578125" />
                  <Point X="-1.728596557617" Y="-20.845279296875" />
                  <Point X="-1.678278076172" Y="-20.8244375" />
                  <Point X="-1.660147583008" Y="-20.814490234375" />
                  <Point X="-1.642417480469" Y="-20.802076171875" />
                  <Point X="-1.626864990234" Y="-20.7884375" />
                  <Point X="-1.614633300781" Y="-20.771755859375" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734078125" />
                  <Point X="-1.579578735352" Y="-20.68364453125" />
                  <Point X="-1.563201171875" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.579484985352" Y="-20.4039296875" />
                  <Point X="-1.584201660156" Y="-20.368103515625" />
                  <Point X="-1.327596313477" Y="-20.29616015625" />
                  <Point X="-0.949623413086" Y="-20.19019140625" />
                  <Point X="-0.571423522949" Y="-20.14592578125" />
                  <Point X="-0.294711395264" Y="-20.113541015625" />
                  <Point X="-0.229100646973" Y="-20.358404296875" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113983154" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425941467" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.244262451172" Y="-20.347767578125" />
                  <Point X="0.307419586182" Y="-20.1120625" />
                  <Point X="0.514019348145" Y="-20.13369921875" />
                  <Point X="0.844030578613" Y="-20.168259765625" />
                  <Point X="1.156950317383" Y="-20.24380859375" />
                  <Point X="1.481038696289" Y="-20.3220546875" />
                  <Point X="1.683948852539" Y="-20.395650390625" />
                  <Point X="1.894646850586" Y="-20.472072265625" />
                  <Point X="2.091590820313" Y="-20.56417578125" />
                  <Point X="2.294555175781" Y="-20.65909765625" />
                  <Point X="2.484860351562" Y="-20.76996875" />
                  <Point X="2.680991210938" Y="-20.884232421875" />
                  <Point X="2.86041796875" Y="-21.01183203125" />
                  <Point X="2.943260498047" Y="-21.07074609375" />
                  <Point X="2.572041259766" Y="-21.713716796875" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.4839453125" />
                  <Point X="2.122500488281" Y="-22.52349609375" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.111852050781" Y="-22.653248046875" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.140071044922" Y="-22.752529296875" />
                  <Point X="2.161233154297" Y="-22.783716796875" />
                  <Point X="2.183028808594" Y="-22.815837890625" />
                  <Point X="2.19446484375" Y="-22.829671875" />
                  <Point X="2.221597412109" Y="-22.85440625" />
                  <Point X="2.252784912109" Y="-22.875568359375" />
                  <Point X="2.284906005859" Y="-22.897365234375" />
                  <Point X="2.304946289062" Y="-22.9077265625" />
                  <Point X="2.32703515625" Y="-22.915994140625" />
                  <Point X="2.348965820312" Y="-22.921337890625" />
                  <Point X="2.383166259766" Y="-22.9254609375" />
                  <Point X="2.418390625" Y="-22.929708984375" />
                  <Point X="2.436467285156" Y="-22.93015625" />
                  <Point X="2.47320703125" Y="-22.925830078125" />
                  <Point X="2.512758300781" Y="-22.91525390625" />
                  <Point X="2.553493408203" Y="-22.904359375" />
                  <Point X="2.565292480469" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.34573046875" Y="-22.452685546875" />
                  <Point X="3.967325439453" Y="-22.09380859375" />
                  <Point X="4.006940917969" Y="-22.148865234375" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.223307617188" Y="-22.47584765625" />
                  <Point X="4.262198730469" Y="-22.540115234375" />
                  <Point X="3.789625488281" Y="-22.902734375" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221422363281" Y="-23.33976171875" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.175508789062" Y="-23.3955078125" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.136606689453" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.111026611328" Y="-23.520828125" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739257812" Y="-23.628232421875" />
                  <Point X="3.106443359375" Y="-23.67041796875" />
                  <Point X="3.115408203125" Y="-23.713865234375" />
                  <Point X="3.120679443359" Y="-23.731021484375" />
                  <Point X="3.136282470703" Y="-23.76426171875" />
                  <Point X="3.159956787109" Y="-23.80024609375" />
                  <Point X="3.184340087891" Y="-23.837306640625" />
                  <Point X="3.198896972656" Y="-23.8545546875" />
                  <Point X="3.216140380859" Y="-23.870642578125" />
                  <Point X="3.234346191406" Y="-23.88396484375" />
                  <Point X="3.268653564453" Y="-23.90327734375" />
                  <Point X="3.30398828125" Y="-23.92316796875" />
                  <Point X="3.320522949219" Y="-23.9305" />
                  <Point X="3.356119140625" Y="-23.9405625" />
                  <Point X="3.402505126953" Y="-23.946693359375" />
                  <Point X="3.450279785156" Y="-23.953005859375" />
                  <Point X="3.462699462891" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.207489746094" Y="-23.8583203125" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.79597265625" Y="-23.86195703125" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.877459960938" Y="-24.2696640625" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.357024414062" Y="-24.498802734375" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704786132812" Y="-24.674416015625" />
                  <Point X="3.681547119141" Y="-24.68493359375" />
                  <Point X="3.635974365234" Y="-24.711275390625" />
                  <Point X="3.589037109375" Y="-24.738404296875" />
                  <Point X="3.574315917969" Y="-24.748900390625" />
                  <Point X="3.547530029297" Y="-24.774423828125" />
                  <Point X="3.520186279297" Y="-24.809267578125" />
                  <Point X="3.492024169922" Y="-24.84515234375" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.470526855469" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.454565917969" Y="-24.954990234375" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.021876953125" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.454293457031" Y="-25.106146484375" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470527099609" Y="-25.1766640625" />
                  <Point X="3.480300537109" Y="-25.198126953125" />
                  <Point X="3.492024658203" Y="-25.217408203125" />
                  <Point X="3.519368164062" Y="-25.25225" />
                  <Point X="3.547530517578" Y="-25.28813671875" />
                  <Point X="3.559994628906" Y="-25.301232421875" />
                  <Point X="3.589035644531" Y="-25.32415625" />
                  <Point X="3.634608398438" Y="-25.350498046875" />
                  <Point X="3.681545654297" Y="-25.37762890625" />
                  <Point X="3.692708984375" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.376197753906" Y="-25.568896484375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.882919921875" Y="-25.763689453125" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.814634765625" Y="-26.1257109375" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.167117675781" Y="-26.101224609375" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374658691406" Y="-26.005509765625" />
                  <Point X="3.285215332031" Y="-26.024951171875" />
                  <Point X="3.193094482422" Y="-26.04497265625" />
                  <Point X="3.163974853516" Y="-26.056595703125" />
                  <Point X="3.136147705078" Y="-26.07348828125" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.058334228516" Y="-26.158982421875" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.962009033203" Y="-26.3895703125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.025949951172" Y="-26.644990234375" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.7227578125" Y="-27.23061328125" />
                  <Point X="4.213122558594" Y="-27.606884765625" />
                  <Point X="4.203450683594" Y="-27.62253515625" />
                  <Point X="4.1248046875" Y="-27.749796875" />
                  <Point X="4.041285888672" Y="-27.86846484375" />
                  <Point X="4.028981933594" Y="-27.885947265625" />
                  <Point X="3.4624609375" Y="-27.558865234375" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224121094" Y="-27.159826171875" />
                  <Point X="2.647772460938" Y="-27.1406015625" />
                  <Point X="2.538133789062" Y="-27.12080078125" />
                  <Point X="2.506783935547" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444833496094" Y="-27.135177734375" />
                  <Point X="2.356398193359" Y="-27.181720703125" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.157988769531" Y="-27.378875" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.114900390625" Y="-27.6697109375" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.545173828125" Y="-28.507388671875" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.781839111328" Y="-29.11165234375" />
                  <Point X="2.701764404297" Y="-29.163482421875" />
                  <Point X="2.263336425781" Y="-28.592111328125" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721923583984" Y="-27.900556640625" />
                  <Point X="1.61693347168" Y="-27.83305859375" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986816406" Y="-27.75116796875" />
                  <Point X="1.448366577148" Y="-27.7434375" />
                  <Point X="1.417100219727" Y="-27.741119140625" />
                  <Point X="1.302275634766" Y="-27.751685546875" />
                  <Point X="1.184013427734" Y="-27.76256640625" />
                  <Point X="1.156363647461" Y="-27.7693984375" />
                  <Point X="1.128977539062" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.795462890625" />
                  <Point X="1.015930419922" Y="-27.869185546875" />
                  <Point X="0.92461151123" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.849114135742" Y="-28.14777734375" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.931215759277" Y="-29.16984765625" />
                  <Point X="1.022065490723" Y="-29.859916015625" />
                  <Point X="0.975712280273" Y="-29.870076171875" />
                  <Point X="0.929315673828" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058407226562" Y="-29.752640625" />
                  <Point X="-1.141245849609" Y="-29.731326171875" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.54103515625" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.153151611328" Y="-29.347791015625" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026245117" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230573242188" Y="-29.094861328125" />
                  <Point X="-1.250208251953" Y="-29.070935546875" />
                  <Point X="-1.261006958008" Y="-29.05977734375" />
                  <Point X="-1.375917236328" Y="-28.95900390625" />
                  <Point X="-1.494267822266" Y="-28.855212890625" />
                  <Point X="-1.506740112305" Y="-28.84596484375" />
                  <Point X="-1.533023681641" Y="-28.82962109375" />
                  <Point X="-1.546834838867" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591315917969" Y="-28.805474609375" />
                  <Point X="-1.62145690918" Y="-28.798447265625" />
                  <Point X="-1.636814331055" Y="-28.796169921875" />
                  <Point X="-1.789326049805" Y="-28.786173828125" />
                  <Point X="-1.946403442383" Y="-28.77587890625" />
                  <Point X="-1.961927856445" Y="-28.7761328125" />
                  <Point X="-1.992725952148" Y="-28.779166015625" />
                  <Point X="-2.007999511719" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053667480469" Y="-28.795494140625" />
                  <Point X="-2.081861816406" Y="-28.808267578125" />
                  <Point X="-2.095436523437" Y="-28.815810546875" />
                  <Point X="-2.222517333984" Y="-28.90072265625" />
                  <Point X="-2.353402832031" Y="-28.988177734375" />
                  <Point X="-2.359682373047" Y="-28.992755859375" />
                  <Point X="-2.380450683594" Y="-29.010138671875" />
                  <Point X="-2.402763427734" Y="-29.03277734375" />
                  <Point X="-2.410471923828" Y="-29.041630859375" />
                  <Point X="-2.503201904297" Y="-29.16248046875" />
                  <Point X="-2.535056884766" Y="-29.142755859375" />
                  <Point X="-2.747604003906" Y="-29.01115234375" />
                  <Point X="-2.939607177734" Y="-28.86331640625" />
                  <Point X="-2.980863769531" Y="-28.83155078125" />
                  <Point X="-2.708216796875" Y="-28.3593125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849609375" Y="-27.71008203125" />
                  <Point X="-2.323947021484" Y="-27.68111328125" />
                  <Point X="-2.31968359375" Y="-27.666181640625" />
                  <Point X="-2.313412841797" Y="-27.63465234375" />
                  <Point X="-2.311638671875" Y="-27.619224609375" />
                  <Point X="-2.310627197266" Y="-27.588287109375" />
                  <Point X="-2.314669433594" Y="-27.557599609375" />
                  <Point X="-2.323656982422" Y="-27.52798046875" />
                  <Point X="-2.329364990234" Y="-27.5135390625" />
                  <Point X="-2.343584716797" Y="-27.4847109375" />
                  <Point X="-2.351565673828" Y="-27.471396484375" />
                  <Point X="-2.369591308594" Y="-27.446244140625" />
                  <Point X="-2.379635986328" Y="-27.43440625" />
                  <Point X="-2.396980712891" Y="-27.4170625" />
                  <Point X="-2.408818847656" Y="-27.40701953125" />
                  <Point X="-2.433970703125" Y="-27.38899609375" />
                  <Point X="-2.447284423828" Y="-27.381015625" />
                  <Point X="-2.476113769531" Y="-27.366796875" />
                  <Point X="-2.490560302734" Y="-27.361087890625" />
                  <Point X="-2.520180175781" Y="-27.352103515625" />
                  <Point X="-2.550860351562" Y="-27.348064453125" />
                  <Point X="-2.581796630859" Y="-27.349076171875" />
                  <Point X="-2.597226074219" Y="-27.3508515625" />
                  <Point X="-2.628752929688" Y="-27.357123046875" />
                  <Point X="-2.64368359375" Y="-27.36138671875" />
                  <Point X="-2.672648925781" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.340891357422" Y="-27.756634765625" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-3.836108642578" Y="-27.961189453125" />
                  <Point X="-4.004020751953" Y="-27.740587890625" />
                  <Point X="-4.141678222656" Y="-27.509755859375" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.68977734375" Y="-27.066244140625" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481201172" Y="-26.563310546875" />
                  <Point X="-3.015102783203" Y="-26.540388671875" />
                  <Point X="-3.005365478516" Y="-26.528041015625" />
                  <Point X="-2.987400390625" Y="-26.500908203125" />
                  <Point X="-2.979833740234" Y="-26.487123046875" />
                  <Point X="-2.967078857422" Y="-26.458494140625" />
                  <Point X="-2.961890625" Y="-26.443650390625" />
                  <Point X="-2.954186035156" Y="-26.41390234375" />
                  <Point X="-2.951552490234" Y="-26.39880078125" />
                  <Point X="-2.948748291016" Y="-26.36837109375" />
                  <Point X="-2.948577636719" Y="-26.35304296875" />
                  <Point X="-2.950786865234" Y="-26.321373046875" />
                  <Point X="-2.953083984375" Y="-26.306216796875" />
                  <Point X="-2.9600859375" Y="-26.276470703125" />
                  <Point X="-2.971781982422" Y="-26.248236328125" />
                  <Point X="-2.987865234375" Y="-26.22225390625" />
                  <Point X="-2.996957275391" Y="-26.209916015625" />
                  <Point X="-3.017789794922" Y="-26.1859609375" />
                  <Point X="-3.028748046875" Y="-26.175244140625" />
                  <Point X="-3.052247558594" Y="-26.1557109375" />
                  <Point X="-3.064788818359" Y="-26.14689453125" />
                  <Point X="-3.091271484375" Y="-26.13130859375" />
                  <Point X="-3.105435546875" Y="-26.124482421875" />
                  <Point X="-3.134695068359" Y="-26.113259765625" />
                  <Point X="-3.149790527344" Y="-26.10886328125" />
                  <Point X="-3.181677978516" Y="-26.102380859375" />
                  <Point X="-3.197294433594" Y="-26.10053515625" />
                  <Point X="-3.228619873047" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.070204101562" Y="-26.20892578125" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.675090820312" Y="-26.231216796875" />
                  <Point X="-4.740763183594" Y="-25.97411328125" />
                  <Point X="-4.77718359375" Y="-25.719462890625" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.239229980469" Y="-25.508029296875" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.496711914063" Y="-25.3082578125" />
                  <Point X="-3.474166503906" Y="-25.2989765625" />
                  <Point X="-3.463197265625" Y="-25.293611328125" />
                  <Point X="-3.433956542969" Y="-25.27690234375" />
                  <Point X="-3.427173339844" Y="-25.27263671875" />
                  <Point X="-3.407565917969" Y="-25.2587109375" />
                  <Point X="-3.381666748047" Y="-25.237419921875" />
                  <Point X="-3.37305078125" Y="-25.229390625" />
                  <Point X="-3.356883300781" Y="-25.2123359375" />
                  <Point X="-3.342951904297" Y="-25.19341796875" />
                  <Point X="-3.33146484375" Y="-25.17291796875" />
                  <Point X="-3.326357666016" Y="-25.162310546875" />
                  <Point X="-3.314375244141" Y="-25.132888671875" />
                  <Point X="-3.311807861328" Y="-25.1257890625" />
                  <Point X="-3.305248779297" Y="-25.104126953125" />
                  <Point X="-3.298308105469" Y="-25.07418359375" />
                  <Point X="-3.296472900391" Y="-25.0635546875" />
                  <Point X="-3.294019287109" Y="-25.04215625" />
                  <Point X="-3.294007080078" Y="-25.0206171875" />
                  <Point X="-3.296436523438" Y="-24.999216796875" />
                  <Point X="-3.298259765625" Y="-24.9885859375" />
                  <Point X="-3.305063476562" Y="-24.95908203125" />
                  <Point X="-3.307050537109" Y="-24.95180078125" />
                  <Point X="-3.314151367188" Y="-24.930328125" />
                  <Point X="-3.326270507812" Y="-24.900466796875" />
                  <Point X="-3.331363769531" Y="-24.88985546875" />
                  <Point X="-3.342825195312" Y="-24.869341796875" />
                  <Point X="-3.356736572266" Y="-24.85040234375" />
                  <Point X="-3.372882324219" Y="-24.833330078125" />
                  <Point X="-3.381484863281" Y="-24.825294921875" />
                  <Point X="-3.406975097656" Y="-24.804287109375" />
                  <Point X="-3.413369384766" Y="-24.799455078125" />
                  <Point X="-3.433332519531" Y="-24.786064453125" />
                  <Point X="-3.462982421875" Y="-24.7690703125" />
                  <Point X="-3.474004882812" Y="-24.76366796875" />
                  <Point X="-3.496657714844" Y="-24.754326171875" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.261112304688" Y="-24.54866796875" />
                  <Point X="-4.785446289062" Y="-24.408173828125" />
                  <Point X="-4.773653808594" Y="-24.328482421875" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.658014648438" Y="-23.771912109375" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.302022949219" Y="-23.7254140625" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747056884766" Y="-23.795634765625" />
                  <Point X="-3.736703369141" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704889892578" Y="-23.7919453125" />
                  <Point X="-3.684604248047" Y="-23.78791015625" />
                  <Point X="-3.674571044922" Y="-23.78533984375" />
                  <Point X="-3.644311035156" Y="-23.775798828125" />
                  <Point X="-3.613145019531" Y="-23.76597265625" />
                  <Point X="-3.603446289062" Y="-23.762322265625" />
                  <Point X="-3.584513183594" Y="-23.753990234375" />
                  <Point X="-3.575278808594" Y="-23.74930859375" />
                  <Point X="-3.556534179688" Y="-23.738486328125" />
                  <Point X="-3.547862060547" Y="-23.732828125" />
                  <Point X="-3.531180908203" Y="-23.72059765625" />
                  <Point X="-3.515926269531" Y="-23.706619140625" />
                  <Point X="-3.502287841797" Y="-23.69106640625" />
                  <Point X="-3.495895019531" Y="-23.682919921875" />
                  <Point X="-3.483480224609" Y="-23.665189453125" />
                  <Point X="-3.478011474609" Y="-23.656396484375" />
                  <Point X="-3.468063232422" Y="-23.638263671875" />
                  <Point X="-3.463583740234" Y="-23.628923828125" />
                  <Point X="-3.451441650391" Y="-23.599611328125" />
                  <Point X="-3.438936279297" Y="-23.569419921875" />
                  <Point X="-3.435499755859" Y="-23.5596484375" />
                  <Point X="-3.429711181641" Y="-23.5397890625" />
                  <Point X="-3.427359130859" Y="-23.529701171875" />
                  <Point X="-3.423600830078" Y="-23.50838671875" />
                  <Point X="-3.422360839844" Y="-23.498103515625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057373047" Y="-23.4363515625" />
                  <Point X="-3.427189208984" Y="-23.42621484375" />
                  <Point X="-3.432791503906" Y="-23.40530859375" />
                  <Point X="-3.436013183594" Y="-23.395466796875" />
                  <Point X="-3.443509033203" Y="-23.37619140625" />
                  <Point X="-3.447783203125" Y="-23.3667578125" />
                  <Point X="-3.462433837891" Y="-23.33861328125" />
                  <Point X="-3.477522949219" Y="-23.30962890625" />
                  <Point X="-3.482799560547" Y="-23.30071484375" />
                  <Point X="-3.494291503906" Y="-23.283515625" />
                  <Point X="-3.500506835938" Y="-23.27523046875" />
                  <Point X="-3.514418945312" Y="-23.258650390625" />
                  <Point X="-3.521498291016" Y="-23.251091796875" />
                  <Point X="-3.536440429688" Y="-23.236787109375" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.976125" Y="-22.89869140625" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.166740722656" Y="-22.60142578125" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.808083740234" Y="-22.070060546875" />
                  <Point X="-3.726336914062" Y="-21.964986328125" />
                  <Point X="-3.575956787109" Y="-22.05180859375" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244923095703" Y="-22.24228125" />
                  <Point X="-3.225994628906" Y="-22.250611328125" />
                  <Point X="-3.216300048828" Y="-22.254259765625" />
                  <Point X="-3.195657226562" Y="-22.26076953125" />
                  <Point X="-3.185623779297" Y="-22.263341796875" />
                  <Point X="-3.165333007812" Y="-22.26737890625" />
                  <Point X="-3.155075683594" Y="-22.26884375" />
                  <Point X="-3.112931884766" Y="-22.27253125" />
                  <Point X="-3.069526367188" Y="-22.276328125" />
                  <Point X="-3.059174072266" Y="-22.276666015625" />
                  <Point X="-3.03848828125" Y="-22.27621484375" />
                  <Point X="-3.028154785156" Y="-22.27542578125" />
                  <Point X="-3.006696777344" Y="-22.272599609375" />
                  <Point X="-2.996512207031" Y="-22.2706875" />
                  <Point X="-2.976423095703" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.938449462891" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.902746826172" Y="-22.226806640625" />
                  <Point X="-2.886614990234" Y="-22.213859375" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.848988769531" Y="-22.177033203125" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.752484619141" Y="-21.91345703125" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-2.978875976562" Y="-21.439533203125" />
                  <Point X="-3.059388183594" Y="-21.300083984375" />
                  <Point X="-2.934783447266" Y="-21.204548828125" />
                  <Point X="-2.648385498047" Y="-20.984970703125" />
                  <Point X="-2.342506103516" Y="-20.81503125" />
                  <Point X="-2.192524169922" Y="-20.731703125" />
                  <Point X="-2.118564941406" Y="-20.828087890625" />
                  <Point X="-2.111822998047" Y="-20.83594921875" />
                  <Point X="-2.097520996094" Y="-20.850890625" />
                  <Point X="-2.089960449219" Y="-20.857970703125" />
                  <Point X="-2.073381835938" Y="-20.8718828125" />
                  <Point X="-2.065091552734" Y="-20.8781015625" />
                  <Point X="-2.047891357422" Y="-20.88959375" />
                  <Point X="-2.038980957031" Y="-20.894869140625" />
                  <Point X="-1.992075073242" Y="-20.919287109375" />
                  <Point X="-1.943764892578" Y="-20.9444375" />
                  <Point X="-1.93433605957" Y="-20.9487109375" />
                  <Point X="-1.915063842773" Y="-20.95620703125" />
                  <Point X="-1.905220458984" Y="-20.9594296875" />
                  <Point X="-1.884313842773" Y="-20.965033203125" />
                  <Point X="-1.874175292969" Y="-20.967166015625" />
                  <Point X="-1.853725708008" Y="-20.970314453125" />
                  <Point X="-1.833055053711" Y="-20.971216796875" />
                  <Point X="-1.812408691406" Y="-20.96986328125" />
                  <Point X="-1.802121459961" Y="-20.968623046875" />
                  <Point X="-1.780805786133" Y="-20.96486328125" />
                  <Point X="-1.770716430664" Y="-20.962509765625" />
                  <Point X="-1.750861206055" Y="-20.956720703125" />
                  <Point X="-1.741094970703" Y="-20.95328515625" />
                  <Point X="-1.692239257812" Y="-20.933046875" />
                  <Point X="-1.641920654297" Y="-20.912205078125" />
                  <Point X="-1.632582275391" Y="-20.907724609375" />
                  <Point X="-1.614451782227" Y="-20.89777734375" />
                  <Point X="-1.605659790039" Y="-20.892310546875" />
                  <Point X="-1.5879296875" Y="-20.879896484375" />
                  <Point X="-1.579780883789" Y="-20.873501953125" />
                  <Point X="-1.564228393555" Y="-20.85986328125" />
                  <Point X="-1.550253173828" Y="-20.84461328125" />
                  <Point X="-1.538021362305" Y="-20.827931640625" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.52153918457" Y="-20.80051171875" />
                  <Point X="-1.516856445312" Y="-20.791271484375" />
                  <Point X="-1.508525512695" Y="-20.772337890625" />
                  <Point X="-1.504877441406" Y="-20.76264453125" />
                  <Point X="-1.488975708008" Y="-20.7122109375" />
                  <Point X="-1.472598144531" Y="-20.660267578125" />
                  <Point X="-1.470026977539" Y="-20.650236328125" />
                  <Point X="-1.465991577148" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265869141" Y="-20.437345703125" />
                  <Point X="-1.301950439453" Y="-20.3876328125" />
                  <Point X="-0.931163635254" Y="-20.2836796875" />
                  <Point X="-0.560379821777" Y="-20.24028125" />
                  <Point X="-0.365222747803" Y="-20.21744140625" />
                  <Point X="-0.320863616943" Y="-20.3829921875" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.336025390625" Y="-20.37235546875" />
                  <Point X="0.378190673828" Y="-20.2149921875" />
                  <Point X="0.504124267578" Y="-20.228181640625" />
                  <Point X="0.827851806641" Y="-20.262083984375" />
                  <Point X="1.134654907227" Y="-20.33615625" />
                  <Point X="1.453622314453" Y="-20.413166015625" />
                  <Point X="1.651557006836" Y="-20.48495703125" />
                  <Point X="1.858249145508" Y="-20.55992578125" />
                  <Point X="2.051346191406" Y="-20.65023046875" />
                  <Point X="2.250429443359" Y="-20.743337890625" />
                  <Point X="2.437037841797" Y="-20.8520546875" />
                  <Point X="2.629445068359" Y="-20.9641484375" />
                  <Point X="2.805361083984" Y="-21.089251953125" />
                  <Point X="2.817780273438" Y="-21.098083984375" />
                  <Point X="2.489768798828" Y="-21.666216796875" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.0531796875" Y="-22.42656640625" />
                  <Point X="2.044181640625" Y="-22.450439453125" />
                  <Point X="2.041301635742" Y="-22.459404296875" />
                  <Point X="2.030725219727" Y="-22.498955078125" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017912719727" Y="-22.548537109375" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.01753527832" Y="-22.66462109375" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029143920898" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045318359375" Y="-22.776111328125" />
                  <Point X="2.055680908203" Y="-22.79615625" />
                  <Point X="2.061459960938" Y="-22.80587109375" />
                  <Point X="2.082622070312" Y="-22.83705859375" />
                  <Point X="2.104417724609" Y="-22.8691796875" />
                  <Point X="2.109808105469" Y="-22.8763671875" />
                  <Point X="2.130464111328" Y="-22.89987890625" />
                  <Point X="2.157596679688" Y="-22.92461328125" />
                  <Point X="2.168256103516" Y="-22.933017578125" />
                  <Point X="2.199443603516" Y="-22.9541796875" />
                  <Point X="2.231564697266" Y="-22.9759765625" />
                  <Point X="2.241275146484" Y="-22.98175390625" />
                  <Point X="2.261315429688" Y="-22.992115234375" />
                  <Point X="2.271645263672" Y="-22.99669921875" />
                  <Point X="2.293734130859" Y="-23.004966796875" />
                  <Point X="2.304544921875" Y="-23.00829296875" />
                  <Point X="2.326475585938" Y="-23.01363671875" />
                  <Point X="2.337595458984" Y="-23.015654296875" />
                  <Point X="2.371795898438" Y="-23.01977734375" />
                  <Point X="2.407020263672" Y="-23.024025390625" />
                  <Point X="2.416040771484" Y="-23.0246796875" />
                  <Point X="2.447576904297" Y="-23.02450390625" />
                  <Point X="2.484316650391" Y="-23.020177734375" />
                  <Point X="2.497748046875" Y="-23.01760546875" />
                  <Point X="2.537299316406" Y="-23.007029296875" />
                  <Point X="2.578034423828" Y="-22.996134765625" />
                  <Point X="2.583994384766" Y="-22.994330078125" />
                  <Point X="2.604423339844" Y="-22.98692578125" />
                  <Point X="2.627664794922" Y="-22.976419921875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.39323046875" Y="-22.53495703125" />
                  <Point X="3.940402832031" Y="-22.219048828125" />
                  <Point X="4.043959228516" Y="-22.362966796875" />
                  <Point X="4.136884277344" Y="-22.51652734375" />
                  <Point X="3.73179296875" Y="-22.827365234375" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168133056641" Y="-23.260134765625" />
                  <Point X="3.152116943359" Y="-23.274787109375" />
                  <Point X="3.134668457031" Y="-23.2933984375" />
                  <Point X="3.128576416016" Y="-23.300578125" />
                  <Point X="3.100111328125" Y="-23.337712890625" />
                  <Point X="3.070794189453" Y="-23.375958984375" />
                  <Point X="3.065634765625" Y="-23.3833984375" />
                  <Point X="3.049740234375" Y="-23.410626953125" />
                  <Point X="3.034763427734" Y="-23.444453125" />
                  <Point X="3.030140380859" Y="-23.457328125" />
                  <Point X="3.019537109375" Y="-23.4952421875" />
                  <Point X="3.008616455078" Y="-23.53429296875" />
                  <Point X="3.006225341797" Y="-23.545337890625" />
                  <Point X="3.002771728516" Y="-23.56763671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077636719" Y="-23.636244140625" />
                  <Point X="3.004698974609" Y="-23.6474296875" />
                  <Point X="3.013403076172" Y="-23.689615234375" />
                  <Point X="3.022367919922" Y="-23.7330625" />
                  <Point X="3.024597900391" Y="-23.741767578125" />
                  <Point X="3.034682373047" Y="-23.771388671875" />
                  <Point X="3.050285400391" Y="-23.80462890625" />
                  <Point X="3.056918212891" Y="-23.8164765625" />
                  <Point X="3.080592529297" Y="-23.8524609375" />
                  <Point X="3.104975830078" Y="-23.889521484375" />
                  <Point X="3.111740478516" Y="-23.898578125" />
                  <Point X="3.126297363281" Y="-23.915826171875" />
                  <Point X="3.134089599609" Y="-23.924017578125" />
                  <Point X="3.151333007812" Y="-23.94010546875" />
                  <Point X="3.160039306641" Y="-23.94730859375" />
                  <Point X="3.178245117188" Y="-23.960630859375" />
                  <Point X="3.187744628906" Y="-23.96675" />
                  <Point X="3.222052001953" Y="-23.9860625" />
                  <Point X="3.25738671875" Y="-24.005953125" />
                  <Point X="3.265478515625" Y="-24.01001171875" />
                  <Point X="3.294680664062" Y="-24.02191796875" />
                  <Point X="3.330276855469" Y="-24.03198046875" />
                  <Point X="3.343671142578" Y="-24.034744140625" />
                  <Point X="3.390057128906" Y="-24.040875" />
                  <Point X="3.437831787109" Y="-24.0471875" />
                  <Point X="3.444033691406" Y="-24.04780078125" />
                  <Point X="3.465709960938" Y="-24.04877734375" />
                  <Point X="3.491214111328" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.219889648438" Y="-23.9525078125" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.783590820312" Y="-24.284279296875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.332436523438" Y="-24.4070390625" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686026367188" Y="-24.58045703125" />
                  <Point X="3.665615722656" Y="-24.5878671875" />
                  <Point X="3.642376708984" Y="-24.598384765625" />
                  <Point X="3.634006103516" Y="-24.602685546875" />
                  <Point X="3.588433349609" Y="-24.62902734375" />
                  <Point X="3.54149609375" Y="-24.65615625" />
                  <Point X="3.533885742188" Y="-24.661052734375" />
                  <Point X="3.508781005859" Y="-24.680125" />
                  <Point X="3.481995117188" Y="-24.7056484375" />
                  <Point X="3.472794921875" Y="-24.715775390625" />
                  <Point X="3.445451171875" Y="-24.750619140625" />
                  <Point X="3.4172890625" Y="-24.78650390625" />
                  <Point X="3.410852783203" Y="-24.795794921875" />
                  <Point X="3.399129638672" Y="-24.81507421875" />
                  <Point X="3.393842773438" Y="-24.8250625" />
                  <Point X="3.384068603516" Y="-24.84652734375" />
                  <Point X="3.380005371094" Y="-24.857072265625" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.361261474609" Y="-24.93712109375" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026263671875" />
                  <Point X="3.350280029297" Y="-25.06294140625" />
                  <Point X="3.351874511719" Y="-25.076423828125" />
                  <Point X="3.360989257812" Y="-25.124015625" />
                  <Point X="3.370376708984" Y="-25.173033203125" />
                  <Point X="3.373159423828" Y="-25.18398828125" />
                  <Point X="3.380005615234" Y="-25.20548828125" />
                  <Point X="3.384069091797" Y="-25.216033203125" />
                  <Point X="3.393842529297" Y="-25.23749609375" />
                  <Point X="3.399128662109" Y="-25.247484375" />
                  <Point X="3.410852783203" Y="-25.266765625" />
                  <Point X="3.417290771484" Y="-25.27605859375" />
                  <Point X="3.444634277344" Y="-25.310900390625" />
                  <Point X="3.472796630859" Y="-25.346787109375" />
                  <Point X="3.478716552734" Y="-25.3536328125" />
                  <Point X="3.501133544922" Y="-25.37580078125" />
                  <Point X="3.530174560547" Y="-25.398724609375" />
                  <Point X="3.541494628906" Y="-25.406404296875" />
                  <Point X="3.587067382812" Y="-25.43274609375" />
                  <Point X="3.634004638672" Y="-25.459876953125" />
                  <Point X="3.639487792969" Y="-25.4628125" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026855469" Y="-25.481029296875" />
                  <Point X="3.691991943359" Y="-25.483916015625" />
                  <Point X="4.351609863281" Y="-25.66066015625" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.727801757812" Y="-26.07921875" />
                  <Point X="4.179517578125" Y="-26.007037109375" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720214844" Y="-25.910841796875" />
                  <Point X="3.354480712891" Y="-25.912677734375" />
                  <Point X="3.265037353516" Y="-25.932119140625" />
                  <Point X="3.172916503906" Y="-25.952140625" />
                  <Point X="3.157877197266" Y="-25.9567421875" />
                  <Point X="3.128757568359" Y="-25.968365234375" />
                  <Point X="3.114677246094" Y="-25.97538671875" />
                  <Point X="3.086850097656" Y="-25.992279296875" />
                  <Point X="3.074121826172" Y="-26.00153125" />
                  <Point X="3.050371337891" Y="-26.02200390625" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.985286132812" Y="-26.09824609375" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606201172" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.867408691406" Y="-26.380865234375" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.946040039062" Y="-26.696365234375" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.664925537109" Y="-27.305982421875" />
                  <Point X="4.087171142578" Y="-27.629984375" />
                  <Point X="4.045490722656" Y="-27.6974296875" />
                  <Point X="4.001274414063" Y="-27.76025390625" />
                  <Point X="3.5099609375" Y="-27.47659375" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841191650391" Y="-27.09088671875" />
                  <Point X="2.815025878906" Y="-27.079515625" />
                  <Point X="2.783120605469" Y="-27.069328125" />
                  <Point X="2.771107421875" Y="-27.066337890625" />
                  <Point X="2.664655761719" Y="-27.04711328125" />
                  <Point X="2.555017089844" Y="-27.0273125" />
                  <Point X="2.539358886719" Y="-27.02580859375" />
                  <Point X="2.508009033203" Y="-27.025404296875" />
                  <Point X="2.492317382813" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444847167969" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400589111328" Y="-27.051109375" />
                  <Point X="2.312153808594" Y="-27.09765234375" />
                  <Point X="2.221071044922" Y="-27.145587890625" />
                  <Point X="2.208967285156" Y="-27.153171875" />
                  <Point X="2.186038085938" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144942138672" Y="-27.21116015625" />
                  <Point X="2.128047607422" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.073920654297" Y="-27.334630859375" />
                  <Point X="2.025984741211" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.44019140625" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.002187744141" Y="-27.580140625" />
                  <Point X="2.021412597656" Y="-27.68659375" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.462901367188" Y="-28.554888671875" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.723753662109" Y="-29.036083984375" />
                  <Point X="2.338705078125" Y="-28.534279296875" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828658813477" Y="-27.87015234375" />
                  <Point X="1.808834350586" Y="-27.84962890625" />
                  <Point X="1.783251098633" Y="-27.82800390625" />
                  <Point X="1.773297973633" Y="-27.820646484375" />
                  <Point X="1.668307861328" Y="-27.7531484375" />
                  <Point X="1.560174560547" Y="-27.68362890625" />
                  <Point X="1.546280151367" Y="-27.676244140625" />
                  <Point X="1.517466674805" Y="-27.663873046875" />
                  <Point X="1.502547851562" Y="-27.65888671875" />
                  <Point X="1.470927490234" Y="-27.65115625" />
                  <Point X="1.455391357422" Y="-27.648697265625" />
                  <Point X="1.424125" Y="-27.64637890625" />
                  <Point X="1.408394897461" Y="-27.64651953125" />
                  <Point X="1.2935703125" Y="-27.6570859375" />
                  <Point X="1.175308105469" Y="-27.667966796875" />
                  <Point X="1.161225097656" Y="-27.67033984375" />
                  <Point X="1.133575317383" Y="-27.677171875" />
                  <Point X="1.120008544922" Y="-27.681630859375" />
                  <Point X="1.092622314453" Y="-27.692974609375" />
                  <Point X="1.079876708984" Y="-27.6994140625" />
                  <Point X="1.055494384766" Y="-27.714134765625" />
                  <Point X="1.043857910156" Y="-27.722416015625" />
                  <Point X="0.955192993164" Y="-27.796138671875" />
                  <Point X="0.863874023438" Y="-27.872068359375" />
                  <Point X="0.85265435791" Y="-27.88308984375" />
                  <Point X="0.832184082031" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.756281616211" Y="-28.127599609375" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833090698242" Y="-29.152337890625" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642143554688" Y="-28.453576171875" />
                  <Point X="0.626786315918" Y="-28.423814453125" />
                  <Point X="0.620406982422" Y="-28.413208984375" />
                  <Point X="0.539750366211" Y="-28.297" />
                  <Point X="0.456679229736" Y="-28.17730859375" />
                  <Point X="0.44667098999" Y="-28.165173828125" />
                  <Point X="0.424791442871" Y="-28.14272265625" />
                  <Point X="0.412920349121" Y="-28.13240625" />
                  <Point X="0.38666506958" Y="-28.11316015625" />
                  <Point X="0.373248596191" Y="-28.10494140625" />
                  <Point X="0.345243133545" Y="-28.090830078125" />
                  <Point X="0.330654022217" Y="-28.0849375" />
                  <Point X="0.205842575073" Y="-28.046201171875" />
                  <Point X="0.077294914246" Y="-28.0063046875" />
                  <Point X="0.063377120972" Y="-28.003109375" />
                  <Point X="0.035218135834" Y="-27.99883984375" />
                  <Point X="0.020976791382" Y="-27.997765625" />
                  <Point X="-0.008664708138" Y="-27.997765625" />
                  <Point X="-0.022905902863" Y="-27.99883984375" />
                  <Point X="-0.051065185547" Y="-28.003109375" />
                  <Point X="-0.064983123779" Y="-28.0063046875" />
                  <Point X="-0.189794265747" Y="-28.045041015625" />
                  <Point X="-0.318342224121" Y="-28.0849375" />
                  <Point X="-0.332930908203" Y="-28.090830078125" />
                  <Point X="-0.360932800293" Y="-28.104939453125" />
                  <Point X="-0.374346313477" Y="-28.11315625" />
                  <Point X="-0.40060144043" Y="-28.132400390625" />
                  <Point X="-0.412475952148" Y="-28.14271875" />
                  <Point X="-0.434359344482" Y="-28.165173828125" />
                  <Point X="-0.444368041992" Y="-28.177310546875" />
                  <Point X="-0.525024658203" Y="-28.293521484375" />
                  <Point X="-0.60809564209" Y="-28.4132109375" />
                  <Point X="-0.612468444824" Y="-28.42012890625" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.845997375488" Y="-29.246455078125" />
                  <Point X="-0.985425537109" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.77094654355" Y="-21.179202423029" />
                  <Point X="1.943398889978" Y="-20.59974731777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.723265117583" Y="-21.261789115067" />
                  <Point X="1.573294972581" Y="-20.45657135082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956301496031" Y="-22.241144068074" />
                  <Point X="3.931821689612" Y="-22.224003123086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.675583691617" Y="-21.344375807104" />
                  <Point X="1.288871484419" Y="-20.373389466297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102269327432" Y="-22.45932542989" />
                  <Point X="3.841043877585" Y="-22.276413400735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.62790226565" Y="-21.426962499142" />
                  <Point X="1.036081103154" Y="-20.312357321681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.080320520902" Y="-22.559930296035" />
                  <Point X="3.750266065557" Y="-22.328823678383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580220839683" Y="-21.50954919118" />
                  <Point X="0.793521332754" Y="-20.258488727913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.001294435706" Y="-22.620569221398" />
                  <Point X="3.65948825353" Y="-22.381233956032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.532539413717" Y="-21.592135883218" />
                  <Point X="0.59876571057" Y="-20.238092959083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.922268350509" Y="-22.681208146761" />
                  <Point X="3.568710441503" Y="-22.433644233681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.484857980865" Y="-21.674722570435" />
                  <Point X="0.404008664736" Y="-20.217696193403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.843242265313" Y="-22.741847072123" />
                  <Point X="3.477932629475" Y="-22.486054511329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.43717648805" Y="-21.757309215665" />
                  <Point X="0.355493558823" Y="-20.299699136458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.764216180116" Y="-22.802485997486" />
                  <Point X="3.387154836208" Y="-22.538464802114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.389494995235" Y="-21.839895860895" />
                  <Point X="0.329327807718" Y="-20.397351266224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.685190077878" Y="-22.863124910916" />
                  <Point X="3.296377304481" Y="-22.590875276031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.34181350242" Y="-21.922482506125" />
                  <Point X="0.303162076158" Y="-20.495003409675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.606163963784" Y="-22.923763816044" />
                  <Point X="3.205599772753" Y="-22.643285749947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294132009605" Y="-22.005069151356" />
                  <Point X="0.276996344598" Y="-20.592655553126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.52713784969" Y="-22.984402721172" />
                  <Point X="3.114822241026" Y="-22.695696223864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.24645051679" Y="-22.087655796586" />
                  <Point X="0.250830613038" Y="-20.690307696577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.351534373453" Y="-20.268527192283" />
                  <Point X="-0.41600470788" Y="-20.223384578126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.715338349015" Y="-23.932363253636" />
                  <Point X="4.661142270494" Y="-23.894414750914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.448111735595" Y="-23.0450416263" />
                  <Point X="3.024044709299" Y="-22.748106697781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.198769023975" Y="-22.170242441816" />
                  <Point X="0.218537122062" Y="-20.783669136693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.313282594192" Y="-20.411284962404" />
                  <Point X="-0.557913438136" Y="-20.239992601396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.749373918752" Y="-24.072168802066" />
                  <Point X="4.521727887525" Y="-23.912769334957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369085621501" Y="-23.105680531428" />
                  <Point X="2.933267177572" Y="-22.800517171698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.151087531159" Y="-22.252829087046" />
                  <Point X="0.160250668087" Y="-20.858830108177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.275030774775" Y="-20.554042760642" />
                  <Point X="-0.699820122374" Y="-20.256602057302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770978259528" Y="-24.203269910268" />
                  <Point X="4.382313504557" Y="-23.931123919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.290059507407" Y="-23.166319436556" />
                  <Point X="2.842489645844" Y="-22.852927645615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103406038344" Y="-22.335415732276" />
                  <Point X="0.063446825728" Y="-20.907020913962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.236778955358" Y="-20.696800558881" />
                  <Point X="-0.841726770424" Y="-20.273211538548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.74028830451" Y="-24.297754158349" />
                  <Point X="4.242899121588" Y="-23.949478503043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.211033393313" Y="-23.226958341684" />
                  <Point X="2.751712114117" Y="-22.905338119532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056822593035" Y="-22.418771238647" />
                  <Point X="-0.974894583512" Y="-20.295940017909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.620500685295" Y="-24.329851550323" />
                  <Point X="4.10348430713" Y="-23.967832784954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.136897174733" Y="-23.291021188513" />
                  <Point X="2.66093458239" Y="-22.957748593448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.026779941471" Y="-22.513708733487" />
                  <Point X="-1.093166699435" Y="-20.329098576712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.500713066081" Y="-24.361948942297" />
                  <Point X="3.96406940738" Y="-23.986187007143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.078401049198" Y="-23.36603534639" />
                  <Point X="2.557841190848" Y="-23.001535409484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012909685777" Y="-22.619970261825" />
                  <Point X="-1.211438815358" Y="-20.362257135515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.380925446866" Y="-24.394046334271" />
                  <Point X="3.82465450763" Y="-24.004541229331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.03275479658" Y="-23.450047082148" />
                  <Point X="2.425194333357" Y="-23.024628665881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.03548423664" Y="-22.751750718443" />
                  <Point X="-1.329710762994" Y="-20.395415812154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.261137677691" Y="-24.426143621242" />
                  <Point X="3.685239607881" Y="-24.02289545152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.005930987962" Y="-23.547238435082" />
                  <Point X="-1.447982161941" Y="-20.428574872989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.141349806532" Y="-24.458240836803" />
                  <Point X="3.545824708131" Y="-24.041249673709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.008297178589" Y="-23.664868845528" />
                  <Point X="-1.466896061398" Y="-20.531304803945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021561935373" Y="-24.490338052364" />
                  <Point X="3.377243481077" Y="-24.039181413658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055454769601" Y="-23.813862532171" />
                  <Point X="-1.469127689725" Y="-20.6457157869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.901774064215" Y="-24.522435267924" />
                  <Point X="-1.498165447348" Y="-20.741356916052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.781986193056" Y="-24.554532483485" />
                  <Point X="-1.538743964028" Y="-20.828917118715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.664613457743" Y="-24.588320795371" />
                  <Point X="-1.609990494115" Y="-20.89500334721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.571280067859" Y="-24.638941638141" />
                  <Point X="-1.710489327455" Y="-20.940606892456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.490011207345" Y="-24.698010155319" />
                  <Point X="-1.832457266983" Y="-20.971177607711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.429161233342" Y="-24.771376130754" />
                  <Point X="-2.274803909247" Y="-20.777416740228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.776710834746" Y="-25.830914105701" />
                  <Point X="4.646349898867" Y="-25.739634395711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.381262225567" Y="-24.85381047037" />
                  <Point X="-2.367154836446" Y="-20.828725510775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.760577696876" Y="-25.935591146882" />
                  <Point X="4.378055319801" Y="-25.66774609492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358110702709" Y="-24.953573185476" />
                  <Point X="-2.459506361391" Y="-20.880033862776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.737758638154" Y="-26.035586655883" />
                  <Point X="4.109758580219" Y="-25.595856281321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.3504247361" Y="-25.06416499965" />
                  <Point X="-2.551857886336" Y="-20.931342214777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.612827264118" Y="-26.064082351956" />
                  <Point X="3.841461604392" Y="-25.5239663023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.378105404501" Y="-25.19952079826" />
                  <Point X="-2.644209411281" Y="-20.982650566777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.4088486187" Y="-26.037228552733" />
                  <Point X="-2.723870938074" Y="-21.042844551144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204869973281" Y="-26.010374753509" />
                  <Point X="-2.802931452449" Y="-21.103459368936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.000890471917" Y="-25.983520354946" />
                  <Point X="-2.881991966824" Y="-21.164074186728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.796910849068" Y="-25.956665871318" />
                  <Point X="-2.961052180889" Y="-21.2246892148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592931226219" Y="-25.929811387691" />
                  <Point X="-3.040111791437" Y="-21.285304665459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.39663042502" Y="-25.908333672866" />
                  <Point X="-2.97439580272" Y="-21.447293082071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.264987593082" Y="-25.932129955525" />
                  <Point X="-2.862000989851" Y="-21.64196636323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.142994073344" Y="-25.962682759324" />
                  <Point X="-2.765631835081" Y="-21.825418357783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055622085231" Y="-26.017477820551" />
                  <Point X="-2.749023094013" Y="-21.953021509412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.992666660166" Y="-26.089369543281" />
                  <Point X="-2.763142738" Y="-22.059108414187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.931720721762" Y="-26.162668323721" />
                  <Point X="-2.812848046282" Y="-22.140277968572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.886886349077" Y="-26.247248543928" />
                  <Point X="-2.880814695126" Y="-22.208660794637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.870112965327" Y="-26.351477280117" />
                  <Point X="-2.968829687331" Y="-22.263005619552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860087174807" Y="-26.46043073195" />
                  <Point X="-3.12198431602" Y="-22.271739179964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.883802687391" Y="-26.593010098567" />
                  <Point X="-3.72270820439" Y="-21.967081370877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019325973367" Y="-26.803878110943" />
                  <Point X="-3.784520628503" Y="-22.039773431491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.070961173434" Y="-27.656214591408" />
                  <Point X="-3.842928895642" Y="-22.114849108478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.018653069829" Y="-27.735561648887" />
                  <Point X="-3.901337182454" Y="-22.189924771691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.357335040675" Y="-27.388475365652" />
                  <Point X="-3.959745469266" Y="-22.265000434904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717933090897" Y="-27.056734886404" />
                  <Point X="-4.015342379212" Y="-22.342044645391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.507601692577" Y="-27.02543284171" />
                  <Point X="-4.063395532426" Y="-22.424371051208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.388060764517" Y="-27.05770296869" />
                  <Point X="-4.111448685641" Y="-22.506697457025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293504223503" Y="-27.107467351818" />
                  <Point X="-4.159501838855" Y="-22.589023862841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.201259449534" Y="-27.158850451656" />
                  <Point X="-4.207554375518" Y="-22.671350700372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.133165952198" Y="-27.227144457451" />
                  <Point X="-3.490222445444" Y="-23.289605511141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086610316443" Y="-27.310519436282" />
                  <Point X="-3.422494391385" Y="-23.453002791074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.042010032818" Y="-27.395263567414" />
                  <Point X="-3.435562258245" Y="-23.559826158122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006198648232" Y="-27.486161751905" />
                  <Point X="-3.47392098014" Y="-23.648940677828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006154255662" Y="-27.602104253825" />
                  <Point X="-3.534126895998" Y="-23.722757627631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030130571355" Y="-27.734866236744" />
                  <Point X="-3.630308586822" Y="-23.771384068611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.074255861589" Y="-27.881736683524" />
                  <Point X="-3.761760548485" Y="-23.795314000074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.186650933845" Y="-28.076410146308" />
                  <Point X="-3.963581761908" Y="-23.769970850997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299046006102" Y="-28.271083609092" />
                  <Point X="1.948291857925" Y="-28.02548291048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.407229253634" Y="-27.646626796312" />
                  <Point X="-4.167561455381" Y="-23.743116317919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.411441078358" Y="-28.465757071876" />
                  <Point X="2.140613864167" Y="-28.276121814947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.260839756" Y="-27.660097352486" />
                  <Point X="-4.371540830779" Y="-23.716262007558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.523835876476" Y="-28.660430342706" />
                  <Point X="2.332935870409" Y="-28.526760719414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.124062475953" Y="-27.680298455874" />
                  <Point X="-4.575519590961" Y="-23.689408127976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.636230443079" Y="-28.855103451427" />
                  <Point X="2.525258173323" Y="-28.777399831612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.03230448123" Y="-27.732022402211" />
                  <Point X="-4.652481291444" Y="-23.751492551077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.726434038108" Y="-29.034238274573" />
                  <Point X="2.717580485411" Y="-29.028038950234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.956588013995" Y="-27.794978747018" />
                  <Point X="-4.678896005155" Y="-23.848970355349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.880871681149" Y="-27.857935185926" />
                  <Point X="-3.319696128221" Y="-24.916665941044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.587466946463" Y="-24.729170795598" />
                  <Point X="-4.705310689377" Y="-23.946448180269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.817065272885" Y="-27.929231043806" />
                  <Point X="-3.294917496852" Y="-25.049989711447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.855763963163" Y="-24.657280787959" />
                  <Point X="-4.731563386156" Y="-24.044039430019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779854079995" Y="-28.019149071971" />
                  <Point X="-3.320538888503" Y="-25.148023005807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.124060979862" Y="-24.585390780319" />
                  <Point X="-4.74711338175" Y="-24.149124791817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.757976140722" Y="-28.119803559903" />
                  <Point X="-3.371818215202" Y="-25.22809042063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.392357531769" Y="-24.513501098131" />
                  <Point X="-4.762663377344" Y="-24.254210153616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.736098343671" Y="-28.220458147422" />
                  <Point X="-3.452556154225" Y="-25.287530693039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.660653598322" Y="-24.44161175579" />
                  <Point X="-4.778213426484" Y="-24.359295477921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725108566007" Y="-28.328736608191" />
                  <Point X="-3.56231499842" Y="-25.32665030888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.741435202331" Y="-28.45614222795" />
                  <Point X="0.563948973112" Y="-28.331865032323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.116280992989" Y="-28.018404538026" />
                  <Point X="-3.682102996609" Y="-25.358747435494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758253742315" Y="-28.583892282361" />
                  <Point X="0.662882993186" Y="-28.517112964896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.066122024421" Y="-28.006658156175" />
                  <Point X="-3.801890994798" Y="-25.390844562107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.7750722823" Y="-28.711642336773" />
                  <Point X="0.701134750726" Y="-28.659870719808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.180882923403" Y="-28.042275295549" />
                  <Point X="-3.921678992987" Y="-25.422941688721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791890822284" Y="-28.839392391184" />
                  <Point X="0.739386508266" Y="-28.80262847472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.29564349675" Y="-28.077892662934" />
                  <Point X="-4.041466991175" Y="-25.455038815334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808709362269" Y="-28.967142445595" />
                  <Point X="0.777638265807" Y="-28.945386229632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.39220993952" Y="-28.126249697701" />
                  <Point X="-2.949808374299" Y="-26.335399993955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.279163163209" Y="-26.104783288015" />
                  <Point X="-4.161254989364" Y="-25.487135941948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825527902254" Y="-29.094892500006" />
                  <Point X="0.815890023347" Y="-29.088143984544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.457628665434" Y="-28.196416598608" />
                  <Point X="-2.961729071149" Y="-26.443026618092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.418577950334" Y="-26.123137589065" />
                  <Point X="-4.281042905891" Y="-25.519233125741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.511796149483" Y="-28.274461703884" />
                  <Point X="-3.005571062071" Y="-26.528301711491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557992737459" Y="-26.141491890114" />
                  <Point X="-4.400830670132" Y="-25.551330416167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.565963663871" Y="-28.352506787915" />
                  <Point X="-3.075784912337" Y="-26.59511103018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.697407524584" Y="-26.159846191164" />
                  <Point X="-4.520618434373" Y="-25.583427706593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.618633815108" Y="-28.431600336913" />
                  <Point X="-3.154811037372" Y="-26.655749927648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836822311709" Y="-26.178200492213" />
                  <Point X="-4.640406198613" Y="-25.615524997019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.652398162317" Y="-28.523931872407" />
                  <Point X="-3.233837162407" Y="-26.716388825115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.976237098834" Y="-26.196554793263" />
                  <Point X="-4.760193962854" Y="-25.647622287445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.67856392871" Y="-28.621583991467" />
                  <Point X="-2.392733292746" Y="-27.421309681001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.457414411477" Y="-27.376019474086" />
                  <Point X="-3.312863287442" Y="-26.777027722582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115652041602" Y="-26.21490898533" />
                  <Point X="-4.7720599549" Y="-25.755287216298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.704729695104" Y="-28.719236110528" />
                  <Point X="-2.310834552354" Y="-27.594629382326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.643850095367" Y="-27.361449388767" />
                  <Point X="-3.391889412477" Y="-26.83766662005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255067306173" Y="-26.233262952068" />
                  <Point X="-4.753627345742" Y="-25.884167454111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.730895461497" Y="-28.816888229589" />
                  <Point X="-2.329996072248" Y="-27.697185927585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.740303791007" Y="-27.409885369924" />
                  <Point X="-3.470915537512" Y="-26.898305517517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.394482570745" Y="-26.251616918805" />
                  <Point X="-4.729864822063" Y="-26.016779738251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.757061227891" Y="-28.914540348649" />
                  <Point X="-2.374788249342" Y="-27.781795693463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.831081354945" Y="-27.462295821287" />
                  <Point X="-3.549941662547" Y="-26.958944414984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.533897835317" Y="-26.269970885543" />
                  <Point X="-4.693789191239" Y="-26.158013752832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.783226994284" Y="-29.01219246771" />
                  <Point X="-2.422469596289" Y="-27.864382440831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921858918883" Y="-27.51470627265" />
                  <Point X="-3.628967787582" Y="-27.019583312451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.809392760677" Y="-29.10984458677" />
                  <Point X="-2.470150943237" Y="-27.946969188199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.012636482821" Y="-27.567116724012" />
                  <Point X="-3.707993940561" Y="-27.080222190352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.835558527071" Y="-29.207496705831" />
                  <Point X="-2.517832290184" Y="-28.029555935567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103414046759" Y="-27.619527175375" />
                  <Point X="-3.787020186822" Y="-27.140861002937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.861724349887" Y="-29.305148785384" />
                  <Point X="-1.454531913425" Y="-28.890060460687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.556988156216" Y="-28.818319827149" />
                  <Point X="-2.565513637131" Y="-28.112142682935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.194191610697" Y="-27.671937626738" />
                  <Point X="-3.866046433082" Y="-27.201499815521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.887890210154" Y="-29.402800838713" />
                  <Point X="-1.183362476543" Y="-29.195908930456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.766376662524" Y="-28.787678002549" />
                  <Point X="-2.613194984078" Y="-28.194729430303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.284969174634" Y="-27.724348078101" />
                  <Point X="-3.945072679343" Y="-27.262138628105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.914056070422" Y="-29.500452892043" />
                  <Point X="-1.156561357002" Y="-29.330648862324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.948799008876" Y="-28.775918086428" />
                  <Point X="-2.660876331026" Y="-28.277316177671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.37574669865" Y="-27.776758557417" />
                  <Point X="-4.024098925603" Y="-27.32277744069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.940221930689" Y="-29.598104945372" />
                  <Point X="-1.129759845515" Y="-29.465389068635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.073583103097" Y="-28.804516908938" />
                  <Point X="-2.70855767872" Y="-28.359902924515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.466524158614" Y="-27.829169081583" />
                  <Point X="-4.103125171863" Y="-27.383416253274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.966387790956" Y="-29.695756998702" />
                  <Point X="-1.1221994399" Y="-29.586656507571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.160753109262" Y="-28.859453399448" />
                  <Point X="-2.756239130201" Y="-28.442489598688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557301618579" Y="-27.881579605749" />
                  <Point X="-4.179933367704" Y="-27.445608161483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.047850612706" Y="-29.754689702761" />
                  <Point X="-1.136179155732" Y="-29.692841391095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.245505377538" Y="-28.916082808253" />
                  <Point X="-2.803920581682" Y="-28.52507627286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.648079078543" Y="-27.933990129914" />
                  <Point X="-4.061186345662" Y="-27.644729307388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.330257378549" Y="-28.972712404199" />
                  <Point X="-2.851602033163" Y="-28.607662947033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.738856538508" Y="-27.98640065408" />
                  <Point X="-3.906001978988" Y="-27.869364156679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.405543344539" Y="-29.035970189224" />
                  <Point X="-2.899283484644" Y="-28.690249621205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.46381086897" Y="-29.111144415317" />
                  <Point X="-2.946964936125" Y="-28.772836295378" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998375" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.668875854492" Y="-29.273583984375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318969727" Y="-28.521544921875" />
                  <Point X="0.383662353516" Y="-28.4053359375" />
                  <Point X="0.300591186523" Y="-28.28564453125" />
                  <Point X="0.274335754395" Y="-28.2663984375" />
                  <Point X="0.149524475098" Y="-28.227662109375" />
                  <Point X="0.02097677803" Y="-28.187765625" />
                  <Point X="-0.008664756775" Y="-28.187765625" />
                  <Point X="-0.133476028442" Y="-28.226501953125" />
                  <Point X="-0.262023895264" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.368935638428" Y="-28.401853515625" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.662471557617" Y="-29.295630859375" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-0.949641113281" Y="-29.967298828125" />
                  <Point X="-1.10023059082" Y="-29.938068359375" />
                  <Point X="-1.244455200195" Y="-29.9009609375" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.332905029297" Y="-29.731474609375" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.339500610352" Y="-29.384857421875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.501192871094" Y="-29.10185546875" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.801752563477" Y="-28.975767578125" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.116959472656" Y="-29.058703125" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.373840087891" Y="-29.306001953125" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.635079345703" Y="-29.304296875" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-3.055521728516" Y="-29.013859375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.87276171875" Y="-28.2643125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.59758984375" />
                  <Point X="-2.513982666016" Y="-27.56876171875" />
                  <Point X="-2.531327392578" Y="-27.55141796875" />
                  <Point X="-2.560156738281" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.245891357422" Y="-27.921177734375" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.987295166016" Y="-28.076267578125" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.304863769531" Y="-27.607072265625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.805441894531" Y="-26.915505859375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145821777344" Y="-26.396013671875" />
                  <Point X="-3.1381171875" Y="-26.366265625" />
                  <Point X="-3.140326416016" Y="-26.334595703125" />
                  <Point X="-3.161158935547" Y="-26.310640625" />
                  <Point X="-3.187641601562" Y="-26.2950546875" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.045404296875" Y="-26.39730078125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.859180664062" Y="-26.278240234375" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.96526953125" Y="-25.74636328125" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.288405761719" Y="-25.324501953125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.528223144531" Y="-25.1119375" />
                  <Point X="-3.502323974609" Y="-25.090646484375" />
                  <Point X="-3.490341552734" Y="-25.061224609375" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.490204589844" Y="-25.00177734375" />
                  <Point X="-3.502323730469" Y="-24.971916015625" />
                  <Point X="-3.527813964844" Y="-24.950908203125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.310288085937" Y="-24.7321953125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.961606933594" Y="-24.300669921875" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.841400390625" Y="-23.722216796875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.277223144531" Y="-23.5370390625" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731705322266" Y="-23.6041328125" />
                  <Point X="-3.7014453125" Y="-23.594591796875" />
                  <Point X="-3.670279296875" Y="-23.584765625" />
                  <Point X="-3.651534667969" Y="-23.573943359375" />
                  <Point X="-3.639119873047" Y="-23.556212890625" />
                  <Point X="-3.626977783203" Y="-23.526900390625" />
                  <Point X="-3.614472412109" Y="-23.496708984375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.61631640625" Y="-23.45448828125" />
                  <Point X="-3.630967041016" Y="-23.42634375" />
                  <Point X="-3.646056152344" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.091790039063" Y="-23.0494296875" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.330833496094" Y="-22.5056484375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.958045166016" Y="-21.953392578125" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.480956787109" Y="-21.887265625" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514160156" Y="-22.07956640625" />
                  <Point X="-3.096370361328" Y="-22.08325390625" />
                  <Point X="-3.05296484375" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.983338867188" Y="-22.042681640625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.941761474609" Y="-21.930015625" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.143420898438" Y="-21.534533203125" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-3.050388427734" Y="-21.053767578125" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.434781005859" Y="-20.64894140625" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.066339355469" Y="-20.58404296875" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247802734" Y="-20.726337890625" />
                  <Point X="-1.904341918945" Y="-20.750755859375" />
                  <Point X="-1.856031738281" Y="-20.77590625" />
                  <Point X="-1.83512512207" Y="-20.781509765625" />
                  <Point X="-1.813809448242" Y="-20.77775" />
                  <Point X="-1.764953735352" Y="-20.75751171875" />
                  <Point X="-1.714635498047" Y="-20.736669921875" />
                  <Point X="-1.696905517578" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.670181762695" Y="-20.655078125" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.673672363281" Y="-20.416330078125" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.35324230957" Y="-20.2046875" />
                  <Point X="-0.968082946777" Y="-20.096703125" />
                  <Point X="-0.582467041016" Y="-20.0515703125" />
                  <Point X="-0.224200042725" Y="-20.009640625" />
                  <Point X="-0.137337646484" Y="-20.33381640625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282115936" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594036102" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.152499572754" Y="-20.3231796875" />
                  <Point X="0.236648391724" Y="-20.0091328125" />
                  <Point X="0.52391418457" Y="-20.039216796875" />
                  <Point X="0.860209960938" Y="-20.074435546875" />
                  <Point X="1.179245605469" Y="-20.1514609375" />
                  <Point X="1.508455810547" Y="-20.230943359375" />
                  <Point X="1.716340820313" Y="-20.30634375" />
                  <Point X="1.931043701172" Y="-20.38421875" />
                  <Point X="2.131835205078" Y="-20.47812109375" />
                  <Point X="2.338685302734" Y="-20.574859375" />
                  <Point X="2.532683349609" Y="-20.6878828125" />
                  <Point X="2.732533935547" Y="-20.804314453125" />
                  <Point X="2.915474609375" Y="-20.934412109375" />
                  <Point X="3.068740722656" Y="-21.043408203125" />
                  <Point X="2.654313720703" Y="-21.761216796875" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.214275634766" Y="-22.548037109375" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.206168701172" Y="-22.641875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.239844238281" Y="-22.730375" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.274938720703" Y="-22.775794921875" />
                  <Point X="2.306126220703" Y="-22.79695703125" />
                  <Point X="2.338247314453" Y="-22.81875390625" />
                  <Point X="2.360336181641" Y="-22.827021484375" />
                  <Point X="2.394536621094" Y="-22.83114453125" />
                  <Point X="2.429760986328" Y="-22.835392578125" />
                  <Point X="2.448666015625" Y="-22.8340546875" />
                  <Point X="2.488217285156" Y="-22.823478515625" />
                  <Point X="2.528952392578" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.29823046875" Y="-22.3704140625" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.084053222656" Y="-22.09337890625" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.304584472656" Y="-22.4266640625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.847457763672" Y="-22.978103515625" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.25090625" Y="-23.453302734375" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.202516113281" Y="-23.5464140625" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.199483642578" Y="-23.651220703125" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646728516" Y="-23.712046875" />
                  <Point X="3.239321044922" Y="-23.74803125" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280947753906" Y="-23.8011796875" />
                  <Point X="3.315255126953" Y="-23.8204921875" />
                  <Point X="3.35058984375" Y="-23.8403828125" />
                  <Point X="3.368567138672" Y="-23.846380859375" />
                  <Point X="3.414953125" Y="-23.85251171875" />
                  <Point X="3.462727783203" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.19508984375" Y="-23.7641328125" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.888276855469" Y="-23.839486328125" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.971329101563" Y="-24.255048828125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.381612304687" Y="-24.59056640625" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729088134766" Y="-24.767181640625" />
                  <Point X="3.683515380859" Y="-24.7935234375" />
                  <Point X="3.636578125" Y="-24.82065234375" />
                  <Point X="3.622265136719" Y="-24.833072265625" />
                  <Point X="3.594921386719" Y="-24.867916015625" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.547870361328" Y="-24.972859375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538482910156" Y="-25.040685546875" />
                  <Point X="3.54759765625" Y="-25.08827734375" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566758544922" Y="-25.1587578125" />
                  <Point X="3.594102050781" Y="-25.193599609375" />
                  <Point X="3.622264404297" Y="-25.229486328125" />
                  <Point X="3.636576660156" Y="-25.241908203125" />
                  <Point X="3.682149414062" Y="-25.26825" />
                  <Point X="3.729086669922" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.400785644531" Y="-25.4771328125" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.976858398438" Y="-25.7778515625" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.90725390625" Y="-26.146845703125" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.154717773438" Y="-26.195412109375" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836669922" Y="-26.098341796875" />
                  <Point X="3.305393310547" Y="-26.117783203125" />
                  <Point X="3.213272460938" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.131382324219" Y="-26.21971875" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.056609375" Y="-26.398275390625" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.105859863281" Y="-26.593615234375" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.780590087891" Y="-27.155244140625" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.284264160156" Y="-27.6724765625" />
                  <Point X="4.204130859375" Y="-27.80214453125" />
                  <Point X="4.118974121094" Y="-27.923140625" />
                  <Point X="4.056688476562" Y="-28.011638671875" />
                  <Point X="3.4149609375" Y="-27.64113671875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.630889160156" Y="-27.23408984375" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.400642578125" Y="-27.2657890625" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.242056884766" Y="-27.423119140625" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.208388183594" Y="-27.652828125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.627446289062" Y="-28.459888671875" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.9303828125" Y="-29.122294921875" />
                  <Point X="2.835294433594" Y="-29.19021484375" />
                  <Point X="2.740086425781" Y="-29.251841796875" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="2.187967773438" Y="-28.649943359375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549194336" Y="-27.980466796875" />
                  <Point X="1.565559082031" Y="-27.91296875" />
                  <Point X="1.45742578125" Y="-27.84344921875" />
                  <Point X="1.425805541992" Y="-27.83571875" />
                  <Point X="1.310980957031" Y="-27.84628515625" />
                  <Point X="1.19271875" Y="-27.857166015625" />
                  <Point X="1.165332641602" Y="-27.868509765625" />
                  <Point X="1.07666796875" Y="-27.942232421875" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.941946533203" Y="-28.167955078125" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.025403076172" Y="-29.157447265625" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.084302246094" Y="-29.943529296875" />
                  <Point X="0.994363586426" Y="-29.963244140625" />
                  <Point X="0.90638873291" Y="-29.9792265625" />
                  <Point X="0.860200683594" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#166" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.095197293765" Y="4.710405628776" Z="1.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.25" />
                  <Point X="-0.594435095944" Y="5.029369539894" Z="1.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.25" />
                  <Point X="-1.372896139994" Y="4.874737952206" Z="1.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.25" />
                  <Point X="-1.729400608057" Y="4.608424193418" Z="1.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.25" />
                  <Point X="-1.724023547081" Y="4.391237405672" Z="1.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.25" />
                  <Point X="-1.790243407028" Y="4.319961418715" Z="1.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.25" />
                  <Point X="-1.887409251817" Y="4.324873457334" Z="1.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.25" />
                  <Point X="-2.032827859162" Y="4.477675574947" Z="1.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.25" />
                  <Point X="-2.465219988803" Y="4.426045725093" Z="1.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.25" />
                  <Point X="-3.086966549432" Y="4.017191706924" Z="1.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.25" />
                  <Point X="-3.192878041622" Y="3.471746623644" Z="1.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.25" />
                  <Point X="-2.997727157535" Y="3.096907391148" Z="1.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.25" />
                  <Point X="-3.024849559858" Y="3.023954008028" Z="1.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.25" />
                  <Point X="-3.098169074459" Y="2.997837541638" Z="1.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.25" />
                  <Point X="-3.462112724653" Y="3.187315907124" Z="1.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.25" />
                  <Point X="-4.003664738454" Y="3.108591781226" Z="1.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.25" />
                  <Point X="-4.380171810196" Y="2.550837168573" Z="1.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.25" />
                  <Point X="-4.128384179555" Y="1.942182674226" Z="1.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.25" />
                  <Point X="-3.68147305988" Y="1.581847976161" Z="1.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.25" />
                  <Point X="-3.679327921298" Y="1.523513460724" Z="1.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.25" />
                  <Point X="-3.722635946158" Y="1.484373505948" Z="1.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.25" />
                  <Point X="-4.276852988936" Y="1.543812792576" Z="1.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.25" />
                  <Point X="-4.895816148582" Y="1.32214210658" Z="1.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.25" />
                  <Point X="-5.01722411104" Y="0.737928594605" Z="1.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.25" />
                  <Point X="-4.32938519961" Y="0.250787742322" Z="1.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.25" />
                  <Point X="-3.56247964762" Y="0.039295952821" Z="1.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.25" />
                  <Point X="-3.544114082748" Y="0.014683678824" Z="1.25" />
                  <Point X="-3.539556741714" Y="0" Z="1.25" />
                  <Point X="-3.544250518387" Y="-0.015123272237" Z="1.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.25" />
                  <Point X="-3.562888947503" Y="-0.039580030313" Z="1.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.25" />
                  <Point X="-4.307502589162" Y="-0.244924314434" Z="1.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.25" />
                  <Point X="-5.020921683795" Y="-0.722161210333" Z="1.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.25" />
                  <Point X="-4.913781440533" Y="-1.259334555074" Z="1.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.25" />
                  <Point X="-4.045034107071" Y="-1.4155918224" Z="1.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.25" />
                  <Point X="-3.205722314035" Y="-1.3147714906" Z="1.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.25" />
                  <Point X="-3.196585366983" Y="-1.337542844137" Z="1.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.25" />
                  <Point X="-3.842035909416" Y="-1.84455643025" Z="1.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.25" />
                  <Point X="-4.353963130338" Y="-2.601401124402" Z="1.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.25" />
                  <Point X="-4.033237244485" Y="-3.075269401662" Z="1.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.25" />
                  <Point X="-3.22704723309" Y="-2.933197977026" Z="1.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.25" />
                  <Point X="-2.564037306376" Y="-2.564293162756" Z="1.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.25" />
                  <Point X="-2.922219036142" Y="-3.208031149402" Z="1.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.25" />
                  <Point X="-3.092181485416" Y="-4.022195273636" Z="1.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.25" />
                  <Point X="-2.667556804991" Y="-4.315527882938" Z="1.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.25" />
                  <Point X="-2.340328396464" Y="-4.305158117465" Z="1.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.25" />
                  <Point X="-2.095336891191" Y="-4.068997231673" Z="1.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.25" />
                  <Point X="-1.811178509959" Y="-3.99437978136" Z="1.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.25" />
                  <Point X="-1.540316346542" Y="-4.10816668115" Z="1.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.25" />
                  <Point X="-1.394695927337" Y="-4.363330218285" Z="1.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.25" />
                  <Point X="-1.388633214414" Y="-4.693666855076" Z="1.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.25" />
                  <Point X="-1.263069970698" Y="-4.918104428187" Z="1.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.25" />
                  <Point X="-0.96532472008" Y="-4.985102276866" Z="1.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.25" />
                  <Point X="-0.620331320056" Y="-4.277292082717" Z="1.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.25" />
                  <Point X="-0.334015600283" Y="-3.399083346348" Z="1.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.25" />
                  <Point X="-0.124811267712" Y="-3.242976179971" Z="1.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.25" />
                  <Point X="0.128547811649" Y="-3.244135865317" Z="1.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.25" />
                  <Point X="0.336430259149" Y="-3.40256240942" Z="1.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.25" />
                  <Point X="0.614423540345" Y="-4.255243947098" Z="1.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.25" />
                  <Point X="0.909168963426" Y="-4.997140579883" Z="1.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.25" />
                  <Point X="1.088851829843" Y="-4.961088935014" Z="1.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.25" />
                  <Point X="1.068819501868" Y="-4.119640546499" Z="1.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.25" />
                  <Point X="0.984649726484" Y="-3.147294140346" Z="1.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.25" />
                  <Point X="1.102479085169" Y="-2.949397202119" Z="1.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.25" />
                  <Point X="1.30940588706" Y="-2.864792850515" Z="1.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.25" />
                  <Point X="1.532363750757" Y="-2.923746306385" Z="1.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.25" />
                  <Point X="2.142144190927" Y="-3.649100654432" Z="1.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.25" />
                  <Point X="2.761099729628" Y="-4.262535483382" Z="1.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.25" />
                  <Point X="2.953288541354" Y="-4.131702752042" Z="1.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.25" />
                  <Point X="2.664591639682" Y="-3.403608737561" Z="1.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.25" />
                  <Point X="2.251436228292" Y="-2.61265994904" Z="1.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.25" />
                  <Point X="2.280147344539" Y="-2.415125422069" Z="1.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.25" />
                  <Point X="2.417772956303" Y="-2.278753965428" Z="1.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.25" />
                  <Point X="2.615846854472" Y="-2.252011781199" Z="1.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.25" />
                  <Point X="3.383804859546" Y="-2.653157860593" Z="1.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.25" />
                  <Point X="4.153706168953" Y="-2.920636719824" Z="1.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.25" />
                  <Point X="4.320641452052" Y="-2.667480256054" Z="1.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.25" />
                  <Point X="3.80487217343" Y="-2.084296461447" Z="1.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.25" />
                  <Point X="3.141761722815" Y="-1.53529527576" Z="1.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.25" />
                  <Point X="3.100242967148" Y="-1.371576923461" Z="1.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.25" />
                  <Point X="3.163672746669" Y="-1.220404926234" Z="1.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.25" />
                  <Point X="3.309856551368" Y="-1.135361297611" Z="1.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.25" />
                  <Point X="4.142035457148" Y="-1.213703413492" Z="1.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.25" />
                  <Point X="4.949845164377" Y="-1.126689966942" Z="1.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.25" />
                  <Point X="5.020143950506" Y="-0.754024857348" Z="1.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.25" />
                  <Point X="4.407570579788" Y="-0.397554707055" Z="1.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.25" />
                  <Point X="3.701015537774" Y="-0.193680115979" Z="1.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.25" />
                  <Point X="3.627280417559" Y="-0.131452822812" Z="1.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.25" />
                  <Point X="3.590549291332" Y="-0.047592858761" Z="1.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.25" />
                  <Point X="3.590822159093" Y="0.049017672451" Z="1.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.25" />
                  <Point X="3.628099020842" Y="0.132495915763" Z="1.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.25" />
                  <Point X="3.702379876578" Y="0.194468728263" Z="1.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.25" />
                  <Point X="4.388397237518" Y="0.392417225714" Z="1.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.25" />
                  <Point X="5.014578037466" Y="0.783922203594" Z="1.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.25" />
                  <Point X="4.930701985644" Y="1.203621096212" Z="1.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.25" />
                  <Point X="4.182407306272" Y="1.316719896459" Z="1.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.25" />
                  <Point X="3.415346674088" Y="1.228338097034" Z="1.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.25" />
                  <Point X="3.333626044311" Y="1.254358884352" Z="1.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.25" />
                  <Point X="3.27493539712" Y="1.31073235467" Z="1.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.25" />
                  <Point X="3.242296223549" Y="1.39016426148" Z="1.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.25" />
                  <Point X="3.244512727434" Y="1.471398990648" Z="1.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.25" />
                  <Point X="3.284433158676" Y="1.547560264744" Z="1.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.25" />
                  <Point X="3.87173984237" Y="2.013509391663" Z="1.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.25" />
                  <Point X="4.341205815808" Y="2.630502718328" Z="1.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.25" />
                  <Point X="4.118482871714" Y="2.967104773675" Z="1.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.25" />
                  <Point X="3.267074387473" Y="2.704166327154" Z="1.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.25" />
                  <Point X="2.469142756356" Y="2.25610565437" Z="1.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.25" />
                  <Point X="2.394367282362" Y="2.249776581807" Z="1.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.25" />
                  <Point X="2.328045591192" Y="2.275696069408" Z="1.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.25" />
                  <Point X="2.275062530827" Y="2.328979269191" Z="1.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.25" />
                  <Point X="2.249653120951" Y="2.39539116147" Z="1.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.25" />
                  <Point X="2.256422288287" Y="2.470326819526" Z="1.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.25" />
                  <Point X="2.691459025705" Y="3.245064694482" Z="1.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.25" />
                  <Point X="2.938296009482" Y="4.137613752051" Z="1.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.25" />
                  <Point X="2.551697377451" Y="4.386601417652" Z="1.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.25" />
                  <Point X="2.146860867946" Y="4.598449812144" Z="1.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.25" />
                  <Point X="1.727233542421" Y="4.771940572235" Z="1.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.25" />
                  <Point X="1.184824200791" Y="4.928423134707" Z="1.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.25" />
                  <Point X="0.522966084445" Y="5.041792382826" Z="1.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.25" />
                  <Point X="0.098047267788" Y="4.721041877175" Z="1.25" />
                  <Point X="0" Y="4.355124473572" Z="1.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>