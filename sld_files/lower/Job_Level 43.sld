<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#201" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3084" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.894691650391" Y="-29.749287109375" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557720458984" Y="-28.497138671875" />
                  <Point X="0.542363342285" Y="-28.467376953125" />
                  <Point X="0.40691619873" Y="-28.272224609375" />
                  <Point X="0.378635681152" Y="-28.2314765625" />
                  <Point X="0.356755401611" Y="-28.209025390625" />
                  <Point X="0.330499603271" Y="-28.189779296875" />
                  <Point X="0.302494873047" Y="-28.17566796875" />
                  <Point X="0.092898124695" Y="-28.1106171875" />
                  <Point X="0.049135883331" Y="-28.09703515625" />
                  <Point X="0.020976638794" Y="-28.092765625" />
                  <Point X="-0.008664536476" Y="-28.092765625" />
                  <Point X="-0.036823932648" Y="-28.09703515625" />
                  <Point X="-0.246420684814" Y="-28.1620859375" />
                  <Point X="-0.290183074951" Y="-28.17566796875" />
                  <Point X="-0.318185058594" Y="-28.18977734375" />
                  <Point X="-0.344440246582" Y="-28.209021484375" />
                  <Point X="-0.366323272705" Y="-28.2314765625" />
                  <Point X="-0.501770721436" Y="-28.426630859375" />
                  <Point X="-0.530051086426" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.62018182373" Y="-28.770751953125" />
                  <Point X="-0.916584838867" Y="-29.87694140625" />
                  <Point X="-1.031843261719" Y="-29.8545703125" />
                  <Point X="-1.079351928711" Y="-29.84534765625" />
                  <Point X="-1.246418212891" Y="-29.80236328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.266581054688" Y="-29.264494140625" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131205078125" />
                  <Point X="-1.516614868164" Y="-28.961974609375" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583189941406" Y="-28.910294921875" />
                  <Point X="-1.612888061523" Y="-28.897994140625" />
                  <Point X="-1.643028198242" Y="-28.890966796875" />
                  <Point X="-1.899142578125" Y="-28.8741796875" />
                  <Point X="-1.95261730957" Y="-28.87067578125" />
                  <Point X="-1.983421386719" Y="-28.8737109375" />
                  <Point X="-2.014469482422" Y="-28.88203125" />
                  <Point X="-2.042657714844" Y="-28.894802734375" />
                  <Point X="-2.256066162109" Y="-29.037396484375" />
                  <Point X="-2.300624267578" Y="-29.067169921875" />
                  <Point X="-2.312786132812" Y="-29.076822265625" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.373948486328" Y="-29.150087890625" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.73209765625" Y="-29.13248828125" />
                  <Point X="-2.801712158203" Y="-29.089384765625" />
                  <Point X="-3.104721435547" Y="-28.856078125" />
                  <Point X="-3.039611083984" Y="-28.743302734375" />
                  <Point X="-2.423760986328" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405576171875" Y="-27.5851875" />
                  <Point X="-2.4145625" Y="-27.5555703125" />
                  <Point X="-2.428781982422" Y="-27.526740234375" />
                  <Point X="-2.446808349609" Y="-27.5015859375" />
                  <Point X="-2.461160400391" Y="-27.487234375" />
                  <Point X="-2.464156982422" Y="-27.48423828125" />
                  <Point X="-2.489317382812" Y="-27.466208984375" />
                  <Point X="-2.518145507812" Y="-27.451994140625" />
                  <Point X="-2.547762695312" Y="-27.44301171875" />
                  <Point X="-2.5786953125" Y="-27.444025390625" />
                  <Point X="-2.610219726562" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-2.861899658203" Y="-27.58978515625" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.02786328125" Y="-27.866115234375" />
                  <Point X="-4.082857910156" Y="-27.793865234375" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-4.1834921875" Y="-27.325337890625" />
                  <Point X="-3.105954345703" Y="-26.498515625" />
                  <Point X="-3.084576171875" Y="-26.47559375" />
                  <Point X="-3.066611083984" Y="-26.4484609375" />
                  <Point X="-3.053856201172" Y="-26.41983203125" />
                  <Point X="-3.047482421875" Y="-26.39522265625" />
                  <Point X="-3.046151611328" Y="-26.390083984375" />
                  <Point X="-3.043347412109" Y="-26.359654296875" />
                  <Point X="-3.045556640625" Y="-26.327984375" />
                  <Point X="-3.05255859375" Y="-26.29823828125" />
                  <Point X="-3.068641601562" Y="-26.272255859375" />
                  <Point X="-3.089473876953" Y="-26.24830078125" />
                  <Point X="-3.112971679688" Y="-26.22876953125" />
                  <Point X="-3.134880126953" Y="-26.215875" />
                  <Point X="-3.139460205078" Y="-26.2131796875" />
                  <Point X="-3.168729492188" Y="-26.201953125" />
                  <Point X="-3.200611572266" Y="-26.195474609375" />
                  <Point X="-3.231928710938" Y="-26.194384765625" />
                  <Point X="-3.513086181641" Y="-26.2313984375" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.812566894531" Y="-26.0768671875" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.760354003906" Y="-25.5493125" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.517493408203" Y="-25.214828125" />
                  <Point X="-3.4877265625" Y="-25.19946875" />
                  <Point X="-3.464775390625" Y="-25.1835390625" />
                  <Point X="-3.459987548828" Y="-25.180216796875" />
                  <Point X="-3.437528808594" Y="-25.15833203125" />
                  <Point X="-3.418279541016" Y="-25.132072265625" />
                  <Point X="-3.404168457031" Y="-25.104068359375" />
                  <Point X="-3.396515380859" Y="-25.07941015625" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.390647949219" Y="-25.0461015625" />
                  <Point X="-3.390647949219" Y="-25.0164609375" />
                  <Point X="-3.394917480469" Y="-24.98830078125" />
                  <Point X="-3.402565185547" Y="-24.96366015625" />
                  <Point X="-3.404162597656" Y="-24.958509765625" />
                  <Point X="-3.418274902344" Y="-24.93049609375" />
                  <Point X="-3.437521972656" Y="-24.904236328125" />
                  <Point X="-3.459978759766" Y="-24.8823515625" />
                  <Point X="-3.482931152344" Y="-24.866421875" />
                  <Point X="-3.493392578125" Y="-24.8601171875" />
                  <Point X="-3.513759521484" Y="-24.849564453125" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-3.789164794922" Y="-24.773478515625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.838350097656" Y="-24.116703125" />
                  <Point X="-4.82448828125" Y="-24.023029296875" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.645553222656" Y="-23.584369140625" />
                  <Point X="-3.765666992188" Y="-23.70020703125" />
                  <Point X="-3.744985351562" Y="-23.700658203125" />
                  <Point X="-3.723422607422" Y="-23.698771484375" />
                  <Point X="-3.703137451172" Y="-23.694736328125" />
                  <Point X="-3.652321533203" Y="-23.67871484375" />
                  <Point X="-3.641711425781" Y="-23.675369140625" />
                  <Point X="-3.622773925781" Y="-23.66703515625" />
                  <Point X="-3.604031005859" Y="-23.656212890625" />
                  <Point X="-3.587352539062" Y="-23.643984375" />
                  <Point X="-3.573715820312" Y="-23.62843359375" />
                  <Point X="-3.561301757812" Y="-23.610705078125" />
                  <Point X="-3.551351806641" Y="-23.5925703125" />
                  <Point X="-3.530961669922" Y="-23.54334375" />
                  <Point X="-3.526704345703" Y="-23.53306640625" />
                  <Point X="-3.520916259766" Y="-23.513208984375" />
                  <Point X="-3.517157470703" Y="-23.491892578125" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553955078" Y="-23.4298984375" />
                  <Point X="-3.532050537109" Y="-23.41062109375" />
                  <Point X="-3.556653320312" Y="-23.363359375" />
                  <Point X="-3.561790283203" Y="-23.3534921875" />
                  <Point X="-3.573284667969" Y="-23.3362890625" />
                  <Point X="-3.587196533203" Y="-23.3197109375" />
                  <Point X="-3.602135498047" Y="-23.30541015625" />
                  <Point X="-3.749143310547" Y="-23.19260546875" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.135015136719" Y="-22.3586171875" />
                  <Point X="-4.08115625" Y="-22.266345703125" />
                  <Point X="-3.750504882813" Y="-21.841337890625" />
                  <Point X="-3.747058105469" Y="-21.843328125" />
                  <Point X="-3.206656494141" Y="-22.155330078125" />
                  <Point X="-3.187729980469" Y="-22.163658203125" />
                  <Point X="-3.167088378906" Y="-22.17016796875" />
                  <Point X="-3.146794677734" Y="-22.174205078125" />
                  <Point X="-3.076021972656" Y="-22.180396484375" />
                  <Point X="-3.061245361328" Y="-22.181689453125" />
                  <Point X="-3.040561279297" Y="-22.18123828125" />
                  <Point X="-3.019102783203" Y="-22.178412109375" />
                  <Point X="-2.999014160156" Y="-22.17349609375" />
                  <Point X="-2.980465332031" Y="-22.16434765625" />
                  <Point X="-2.9622109375" Y="-22.15271875" />
                  <Point X="-2.946078369141" Y="-22.139771484375" />
                  <Point X="-2.895843505859" Y="-22.089537109375" />
                  <Point X="-2.885354736328" Y="-22.079048828125" />
                  <Point X="-2.872409179688" Y="-22.062919921875" />
                  <Point X="-2.860779296875" Y="-22.044666015625" />
                  <Point X="-2.851629638672" Y="-22.02611328125" />
                  <Point X="-2.846712646484" Y="-22.00601953125" />
                  <Point X="-2.843887207031" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.849627685547" Y="-21.893107421875" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-2.934938476562" Y="-21.705634765625" />
                  <Point X="-3.183332519531" Y="-21.275404296875" />
                  <Point X="-2.7944296875" Y="-20.977236328125" />
                  <Point X="-2.700620849609" Y="-20.905314453125" />
                  <Point X="-2.170374023438" Y="-20.610720703125" />
                  <Point X="-2.167036621094" Y="-20.6088671875" />
                  <Point X="-2.043197509766" Y="-20.770255859375" />
                  <Point X="-2.028895507812" Y="-20.785197265625" />
                  <Point X="-2.012317382812" Y="-20.799109375" />
                  <Point X="-1.995115600586" Y="-20.810603515625" />
                  <Point X="-1.916346069336" Y="-20.851609375" />
                  <Point X="-1.899899536133" Y="-20.860171875" />
                  <Point X="-1.880625854492" Y="-20.86766796875" />
                  <Point X="-1.859718261719" Y="-20.873271484375" />
                  <Point X="-1.839269287109" Y="-20.876419921875" />
                  <Point X="-1.818623657227" Y="-20.87506640625" />
                  <Point X="-1.797307373047" Y="-20.871306640625" />
                  <Point X="-1.77745300293" Y="-20.865517578125" />
                  <Point X="-1.695409179688" Y="-20.831533203125" />
                  <Point X="-1.678279052734" Y="-20.8244375" />
                  <Point X="-1.660145507812" Y="-20.814490234375" />
                  <Point X="-1.642416137695" Y="-20.802076171875" />
                  <Point X="-1.626864013672" Y="-20.7884375" />
                  <Point X="-1.61463269043" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595480102539" Y="-20.734078125" />
                  <Point X="-1.568776367188" Y="-20.649384765625" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.565136474609" Y="-20.512919921875" />
                  <Point X="-1.584202026367" Y="-20.3681015625" />
                  <Point X="-1.071071411133" Y="-20.22423828125" />
                  <Point X="-0.949625061035" Y="-20.19019140625" />
                  <Point X="-0.306812927246" Y="-20.11495703125" />
                  <Point X="-0.293769134521" Y="-20.11705859375" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113983154" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907631" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425933838" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.17959413147" Y="-20.58911328125" />
                  <Point X="0.307419555664" Y="-20.112060546875" />
                  <Point X="0.738002075195" Y="-20.15715625" />
                  <Point X="0.844030944824" Y="-20.168259765625" />
                  <Point X="1.375868164062" Y="-20.296662109375" />
                  <Point X="1.481040527344" Y="-20.3220546875" />
                  <Point X="1.826940795898" Y="-20.447515625" />
                  <Point X="1.894646118164" Y="-20.472072265625" />
                  <Point X="2.229374755859" Y="-20.62861328125" />
                  <Point X="2.294558837891" Y="-20.659099609375" />
                  <Point X="2.617961425781" Y="-20.847513671875" />
                  <Point X="2.680975341797" Y="-20.884224609375" />
                  <Point X="2.943259277344" Y="-21.070748046875" />
                  <Point X="2.860381347656" Y="-21.214296875" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142075683594" Y="-22.4600703125" />
                  <Point X="2.133076660156" Y="-22.483943359375" />
                  <Point X="2.115315429688" Y="-22.550361328125" />
                  <Point X="2.113197021484" Y="-22.560447265625" />
                  <Point X="2.108151123047" Y="-22.59321875" />
                  <Point X="2.107727783203" Y="-22.619048828125" />
                  <Point X="2.114653320313" Y="-22.676482421875" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442138672" Y="-22.710396484375" />
                  <Point X="2.129708007812" Y="-22.732484375" />
                  <Point X="2.140070800781" Y="-22.752529296875" />
                  <Point X="2.175608398437" Y="-22.80490234375" />
                  <Point X="2.18201171875" Y="-22.813294921875" />
                  <Point X="2.202732421875" Y="-22.83753125" />
                  <Point X="2.221597412109" Y="-22.85440625" />
                  <Point X="2.273970703125" Y="-22.8899453125" />
                  <Point X="2.284906005859" Y="-22.897365234375" />
                  <Point X="2.304955566406" Y="-22.90773046875" />
                  <Point X="2.327045166016" Y="-22.91599609375" />
                  <Point X="2.348966796875" Y="-22.921337890625" />
                  <Point X="2.406393310547" Y="-22.92826171875" />
                  <Point X="2.417428710938" Y="-22.9289453125" />
                  <Point X="2.448330078125" Y="-22.9290546875" />
                  <Point X="2.473208740234" Y="-22.925830078125" />
                  <Point X="2.539627441406" Y="-22.908068359375" />
                  <Point X="2.545317871094" Y="-22.906353515625" />
                  <Point X="2.571266845703" Y="-22.897642578125" />
                  <Point X="2.588533691406" Y="-22.88985546875" />
                  <Point X="2.846310546875" Y="-22.741029296875" />
                  <Point X="3.967326416016" Y="-22.093810546875" />
                  <Point X="4.085891113281" Y="-22.258587890625" />
                  <Point X="4.12327734375" Y="-22.310546875" />
                  <Point X="4.262198730469" Y="-22.540115234375" />
                  <Point X="4.169251953125" Y="-22.611435546875" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221425048828" Y="-23.339759765625" />
                  <Point X="3.203974853516" Y="-23.35837109375" />
                  <Point X="3.156173339844" Y="-23.420732421875" />
                  <Point X="3.150670654297" Y="-23.4287265625" />
                  <Point X="3.13221875" Y="-23.458701171875" />
                  <Point X="3.121629638672" Y="-23.482916015625" />
                  <Point X="3.103823486328" Y="-23.5465859375" />
                  <Point X="3.100105712891" Y="-23.559880859375" />
                  <Point X="3.096652099609" Y="-23.582181640625" />
                  <Point X="3.095836425781" Y="-23.60575" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.112356445312" Y="-23.69907421875" />
                  <Point X="3.114880615234" Y="-23.70871875" />
                  <Point X="3.125130615234" Y="-23.74088671875" />
                  <Point X="3.136282714844" Y="-23.764259765625" />
                  <Point X="3.176039306641" Y="-23.8246875" />
                  <Point X="3.184340332031" Y="-23.8373046875" />
                  <Point X="3.198893066406" Y="-23.854548828125" />
                  <Point X="3.21613671875" Y="-23.870638671875" />
                  <Point X="3.234346191406" Y="-23.88396484375" />
                  <Point X="3.291958984375" Y="-23.916396484375" />
                  <Point X="3.301357910156" Y="-23.9210234375" />
                  <Point X="3.331366210938" Y="-23.933794921875" />
                  <Point X="3.356121826172" Y="-23.940564453125" />
                  <Point X="3.434018310547" Y="-23.950859375" />
                  <Point X="3.439520751953" Y="-23.951423828125" />
                  <Point X="3.468859130859" Y="-23.95357421875" />
                  <Point X="3.488203125" Y="-23.953015625" />
                  <Point X="3.733073974609" Y="-23.920779296875" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.829881347656" Y="-24.00124609375" />
                  <Point X="4.845936523438" Y="-24.067193359375" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.792084472656" Y="-24.382228515625" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704791503906" Y="-24.6744140625" />
                  <Point X="3.681549560547" Y="-24.684931640625" />
                  <Point X="3.605018798828" Y="-24.72916796875" />
                  <Point X="3.597311035156" Y="-24.734134765625" />
                  <Point X="3.567015625" Y="-24.75579296875" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.501611816406" Y="-24.832935546875" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.48030078125" Y="-24.86443359375" />
                  <Point X="3.47052734375" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.448374755859" Y="-24.9873203125" />
                  <Point X="3.447062011719" Y="-24.996669921875" />
                  <Point X="3.443865722656" Y="-25.032166015625" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.460485107422" Y="-25.1384765625" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.47052734375" Y="-25.176666015625" />
                  <Point X="3.480301025391" Y="-25.19812890625" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.537942871094" Y="-25.275919921875" />
                  <Point X="3.544486816406" Y="-25.2834140625" />
                  <Point X="3.568386230469" Y="-25.308052734375" />
                  <Point X="3.589035400391" Y="-25.32415625" />
                  <Point X="3.66556640625" Y="-25.368392578125" />
                  <Point X="3.670105957031" Y="-25.37085546875" />
                  <Point X="3.698166015625" Y="-25.385099609375" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="3.941137695312" Y="-25.452322265625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.863986816406" Y="-25.88926953125" />
                  <Point X="4.855022460938" Y="-25.94873046875" />
                  <Point X="4.801174804688" Y="-26.18469921875" />
                  <Point X="4.671665039062" Y="-26.1676484375" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.224456054688" Y="-26.03815625" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163976806641" Y="-26.056595703125" />
                  <Point X="3.136150878906" Y="-26.073486328125" />
                  <Point X="3.112397705078" Y="-26.093958984375" />
                  <Point X="3.021609375" Y="-26.203150390625" />
                  <Point X="3.002653564453" Y="-26.225947265625" />
                  <Point X="2.987933349609" Y="-26.250328125" />
                  <Point X="2.976589355469" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.956745361328" Y="-26.446771484375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.507564453125" />
                  <Point X="2.964078613281" Y="-26.539185546875" />
                  <Point X="2.976450439453" Y="-26.567998046875" />
                  <Point X="3.059574951172" Y="-26.697291015625" />
                  <Point X="3.076930664062" Y="-26.724287109375" />
                  <Point X="3.086931396484" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.319019042969" Y="-26.920814453125" />
                  <Point X="4.213121582031" Y="-27.6068828125" />
                  <Point X="4.150078125" Y="-27.708896484375" />
                  <Point X="4.124807128906" Y="-27.749791015625" />
                  <Point X="4.028982421875" Y="-27.8859453125" />
                  <Point X="3.911828369141" Y="-27.818306640625" />
                  <Point X="2.800954345703" Y="-27.176943359375" />
                  <Point X="2.786137451172" Y="-27.170015625" />
                  <Point X="2.754224121094" Y="-27.15982421875" />
                  <Point X="2.575458740234" Y="-27.1275390625" />
                  <Point X="2.538133789062" Y="-27.120798828125" />
                  <Point X="2.506777099609" Y="-27.120396484375" />
                  <Point X="2.474605957031" Y="-27.12535546875" />
                  <Point X="2.444833984375" Y="-27.135177734375" />
                  <Point X="2.296323730469" Y="-27.213337890625" />
                  <Point X="2.265315917969" Y="-27.22965625" />
                  <Point X="2.242388183594" Y="-27.246546875" />
                  <Point X="2.221427490234" Y="-27.267505859375" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.126372070313" Y="-27.43894921875" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.127960449219" Y="-27.7420234375" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.285731445312" Y="-28.058021484375" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.811837158203" Y="-29.090224609375" />
                  <Point X="2.781865722656" Y="-29.1116328125" />
                  <Point X="2.701763427734" Y="-29.16348046875" />
                  <Point X="2.606245605469" Y="-29.039" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747504760742" Y="-27.922181640625" />
                  <Point X="1.721923217773" Y="-27.900556640625" />
                  <Point X="1.545612182617" Y="-27.787205078125" />
                  <Point X="1.508799804688" Y="-27.7635390625" />
                  <Point X="1.479987426758" Y="-27.75116796875" />
                  <Point X="1.448369384766" Y="-27.7434375" />
                  <Point X="1.4171015625" Y="-27.741119140625" />
                  <Point X="1.224275512695" Y="-27.75886328125" />
                  <Point X="1.184014770508" Y="-27.76256640625" />
                  <Point X="1.156366943359" Y="-27.769396484375" />
                  <Point X="1.128979125977" Y="-27.780740234375" />
                  <Point X="1.104594848633" Y="-27.795462890625" />
                  <Point X="0.955699523926" Y="-27.919265625" />
                  <Point X="0.924611206055" Y="-27.945115234375" />
                  <Point X="0.904141784668" Y="-27.96886328125" />
                  <Point X="0.88725012207" Y="-27.9966875" />
                  <Point X="0.875624511719" Y="-28.02580859375" />
                  <Point X="0.831105407715" Y="-28.230630859375" />
                  <Point X="0.821810241699" Y="-28.27339453125" />
                  <Point X="0.81972479248" Y="-28.289625" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.857691833496" Y="-28.611376953125" />
                  <Point X="1.022065429688" Y="-29.859916015625" />
                  <Point X="1.004046142578" Y="-29.863865234375" />
                  <Point X="0.975669250488" Y="-29.870083984375" />
                  <Point X="0.929315429688" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.01374206543" Y="-29.761310546875" />
                  <Point X="-1.058443237305" Y="-29.7526328125" />
                  <Point X="-1.141246459961" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123333984375" Y="-29.497693359375" />
                  <Point X="-1.17340637207" Y="-29.2459609375" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124633789" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230575561523" Y="-29.094859375" />
                  <Point X="-1.250210327148" Y="-29.070935546875" />
                  <Point X="-1.261006958008" Y="-29.05978125" />
                  <Point X="-1.453976928711" Y="-28.89055078125" />
                  <Point X="-1.494267578125" Y="-28.855216796875" />
                  <Point X="-1.506736938477" Y="-28.84596875" />
                  <Point X="-1.533021240234" Y="-28.829623046875" />
                  <Point X="-1.546836425781" Y="-28.822525390625" />
                  <Point X="-1.576534545898" Y="-28.810224609375" />
                  <Point X="-1.591316894531" Y="-28.8054765625" />
                  <Point X="-1.62145690918" Y="-28.79844921875" />
                  <Point X="-1.636814819336" Y="-28.796169921875" />
                  <Point X="-1.892929077148" Y="-28.7793828125" />
                  <Point X="-1.946403808594" Y="-28.77587890625" />
                  <Point X="-1.961932617188" Y="-28.7761328125" />
                  <Point X="-1.992736694336" Y="-28.77916796875" />
                  <Point X="-2.008011962891" Y="-28.78194921875" />
                  <Point X="-2.039060180664" Y="-28.79026953125" />
                  <Point X="-2.053675537109" Y="-28.795498046875" />
                  <Point X="-2.081863769531" Y="-28.80826953125" />
                  <Point X="-2.095436523437" Y="-28.8158125" />
                  <Point X="-2.308844970703" Y="-28.95840625" />
                  <Point X="-2.353403076172" Y="-28.9881796875" />
                  <Point X="-2.359682128906" Y="-28.9927578125" />
                  <Point X="-2.380443603516" Y="-29.0101328125" />
                  <Point X="-2.402760253906" Y="-29.0327734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.449317138672" Y="-29.092255859375" />
                  <Point X="-2.503200683594" Y="-29.162478515625" />
                  <Point X="-2.682086669922" Y="-29.051716796875" />
                  <Point X="-2.747588134766" Y="-29.01116015625" />
                  <Point X="-2.980861816406" Y="-28.831546875" />
                  <Point X="-2.957338378906" Y="-28.790802734375" />
                  <Point X="-2.34148828125" Y="-27.724119140625" />
                  <Point X="-2.334849609375" Y="-27.71008203125" />
                  <Point X="-2.323947509766" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.61923046875" />
                  <Point X="-2.310626953125" Y="-27.58829296875" />
                  <Point X="-2.314668701172" Y="-27.55760546875" />
                  <Point X="-2.323655029297" Y="-27.52798828125" />
                  <Point X="-2.329362060547" Y="-27.513548828125" />
                  <Point X="-2.343581542969" Y="-27.48471875" />
                  <Point X="-2.351562988281" Y="-27.47140234375" />
                  <Point X="-2.369589355469" Y="-27.446248046875" />
                  <Point X="-2.379634277344" Y="-27.43441015625" />
                  <Point X="-2.393986328125" Y="-27.42005859375" />
                  <Point X="-2.408822509766" Y="-27.407017578125" />
                  <Point X="-2.433982910156" Y="-27.38898828125" />
                  <Point X="-2.447303710938" Y="-27.38100390625" />
                  <Point X="-2.476131835938" Y="-27.3667890625" />
                  <Point X="-2.490573730469" Y="-27.361083984375" />
                  <Point X="-2.520190917969" Y="-27.3521015625" />
                  <Point X="-2.550874267578" Y="-27.3480625" />
                  <Point X="-2.581806884766" Y="-27.349076171875" />
                  <Point X="-2.597231445312" Y="-27.3508515625" />
                  <Point X="-2.628755859375" Y="-27.357123046875" />
                  <Point X="-2.643686279297" Y="-27.36138671875" />
                  <Point X="-2.672650390625" Y="-27.3722890625" />
                  <Point X="-2.686684082031" Y="-27.378927734375" />
                  <Point X="-2.909399902344" Y="-27.507513671875" />
                  <Point X="-3.793086914062" Y="-28.0177109375" />
                  <Point X="-3.952269775391" Y="-27.808576171875" />
                  <Point X="-4.004018798828" Y="-27.74058984375" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-4.125659667969" Y="-27.40070703125" />
                  <Point X="-3.048121826172" Y="-26.573884765625" />
                  <Point X="-3.03648046875" Y="-26.563310546875" />
                  <Point X="-3.015102294922" Y="-26.540388671875" />
                  <Point X="-3.005365478516" Y="-26.528041015625" />
                  <Point X="-2.987400390625" Y="-26.500908203125" />
                  <Point X="-2.979833740234" Y="-26.487123046875" />
                  <Point X="-2.967078857422" Y="-26.458494140625" />
                  <Point X="-2.961890625" Y="-26.443650390625" />
                  <Point X="-2.955516845703" Y="-26.419041015625" />
                  <Point X="-2.951552490234" Y="-26.39880078125" />
                  <Point X="-2.948748291016" Y="-26.36837109375" />
                  <Point X="-2.948577636719" Y="-26.35304296875" />
                  <Point X="-2.950786865234" Y="-26.321373046875" />
                  <Point X="-2.953083984375" Y="-26.306216796875" />
                  <Point X="-2.9600859375" Y="-26.276470703125" />
                  <Point X="-2.971781494141" Y="-26.24823828125" />
                  <Point X="-2.987864501953" Y="-26.222255859375" />
                  <Point X="-2.996956787109" Y="-26.209916015625" />
                  <Point X="-3.0177890625" Y="-26.1859609375" />
                  <Point X="-3.028748535156" Y="-26.1752421875" />
                  <Point X="-3.052246337891" Y="-26.1557109375" />
                  <Point X="-3.064784667969" Y="-26.1468984375" />
                  <Point X="-3.086693115234" Y="-26.13400390625" />
                  <Point X="-3.105438720703" Y="-26.12448046875" />
                  <Point X="-3.134708007813" Y="-26.11325390625" />
                  <Point X="-3.149811767578" Y="-26.10885546875" />
                  <Point X="-3.181693847656" Y="-26.102376953125" />
                  <Point X="-3.197307617188" Y="-26.10053125" />
                  <Point X="-3.228624755859" Y="-26.09944140625" />
                  <Point X="-3.244328125" Y="-26.100197265625" />
                  <Point X="-3.525485595703" Y="-26.1372109375" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.720521972656" Y="-26.05335546875" />
                  <Point X="-4.740762207031" Y="-25.974115234375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.735765625" Y="-25.641076171875" />
                  <Point X="-3.508287841797" Y="-25.312173828125" />
                  <Point X="-3.500469970703" Y="-25.309712890625" />
                  <Point X="-3.473931640625" Y="-25.299251953125" />
                  <Point X="-3.444164794922" Y="-25.283892578125" />
                  <Point X="-3.433558837891" Y="-25.27751171875" />
                  <Point X="-3.410617431641" Y="-25.261587890625" />
                  <Point X="-3.410616699219" Y="-25.26158984375" />
                  <Point X="-3.3936875" Y="-25.248255859375" />
                  <Point X="-3.371228759766" Y="-25.22637109375" />
                  <Point X="-3.360909423828" Y="-25.21449609375" />
                  <Point X="-3.34166015625" Y="-25.188236328125" />
                  <Point X="-3.333441650391" Y="-25.174822265625" />
                  <Point X="-3.319330566406" Y="-25.146818359375" />
                  <Point X="-3.313437988281" Y="-25.132228515625" />
                  <Point X="-3.305784912109" Y="-25.1075703125" />
                  <Point X="-3.300990966797" Y="-25.088501953125" />
                  <Point X="-3.296721435547" Y="-25.060341796875" />
                  <Point X="-3.295647949219" Y="-25.0461015625" />
                  <Point X="-3.295647949219" Y="-25.0164609375" />
                  <Point X="-3.296721435547" Y="-25.002220703125" />
                  <Point X="-3.300990966797" Y="-24.974060546875" />
                  <Point X="-3.304187011719" Y="-24.960140625" />
                  <Point X="-3.311829345703" Y="-24.935517578125" />
                  <Point X="-3.311829101562" Y="-24.935517578125" />
                  <Point X="-3.319320068359" Y="-24.91576953125" />
                  <Point X="-3.333432373047" Y="-24.887755859375" />
                  <Point X="-3.34165234375" Y="-24.8743359375" />
                  <Point X="-3.360899414062" Y="-24.848076171875" />
                  <Point X="-3.371218994141" Y="-24.836201171875" />
                  <Point X="-3.39367578125" Y="-24.81431640625" />
                  <Point X="-3.405812988281" Y="-24.804306640625" />
                  <Point X="-3.428765380859" Y="-24.788376953125" />
                  <Point X="-3.449688232422" Y="-24.775767578125" />
                  <Point X="-3.470055175781" Y="-24.76521484375" />
                  <Point X="-3.479408447266" Y="-24.7609921875" />
                  <Point X="-3.498525146484" Y="-24.753578125" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.764577148438" Y="-24.68171484375" />
                  <Point X="-4.785446289062" Y="-24.408173828125" />
                  <Point X="-4.744373535156" Y="-24.130609375" />
                  <Point X="-4.731331542969" Y="-24.042474609375" />
                  <Point X="-4.633586425781" Y="-23.681765625" />
                  <Point X="-3.778068847656" Y="-23.79439453125" />
                  <Point X="-3.767739013672" Y="-23.79518359375" />
                  <Point X="-3.747057373047" Y="-23.795634765625" />
                  <Point X="-3.736704589844" Y="-23.795296875" />
                  <Point X="-3.715141845703" Y="-23.79341015625" />
                  <Point X="-3.704888183594" Y="-23.7919453125" />
                  <Point X="-3.684603027344" Y="-23.78791015625" />
                  <Point X="-3.674571533203" Y="-23.78533984375" />
                  <Point X="-3.623755615234" Y="-23.769318359375" />
                  <Point X="-3.603445556641" Y="-23.762322265625" />
                  <Point X="-3.584508056641" Y="-23.75398828125" />
                  <Point X="-3.575270507812" Y="-23.7493046875" />
                  <Point X="-3.556527587891" Y="-23.738482421875" />
                  <Point X="-3.547858398438" Y="-23.732826171875" />
                  <Point X="-3.531179931641" Y="-23.72059765625" />
                  <Point X="-3.515925537109" Y="-23.706619140625" />
                  <Point X="-3.502288818359" Y="-23.691068359375" />
                  <Point X="-3.495897216797" Y="-23.682923828125" />
                  <Point X="-3.483483154297" Y="-23.6651953125" />
                  <Point X="-3.478014404297" Y="-23.65640234375" />
                  <Point X="-3.468064453125" Y="-23.638267578125" />
                  <Point X="-3.463583251953" Y="-23.62892578125" />
                  <Point X="-3.443193115234" Y="-23.57969921875" />
                  <Point X="-3.435499755859" Y="-23.559650390625" />
                  <Point X="-3.429711669922" Y="-23.53979296875" />
                  <Point X="-3.427359619141" Y="-23.52970703125" />
                  <Point X="-3.423600830078" Y="-23.508390625" />
                  <Point X="-3.422360839844" Y="-23.49810546875" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432791259766" Y="-23.405310546875" />
                  <Point X="-3.436013183594" Y="-23.395466796875" />
                  <Point X="-3.443509765625" Y="-23.376189453125" />
                  <Point X="-3.447784423828" Y="-23.366755859375" />
                  <Point X="-3.472387207031" Y="-23.319494140625" />
                  <Point X="-3.482799804688" Y="-23.30071484375" />
                  <Point X="-3.494294189453" Y="-23.28351171875" />
                  <Point X="-3.500512939453" Y="-23.275220703125" />
                  <Point X="-3.514424804688" Y="-23.258642578125" />
                  <Point X="-3.521503173828" Y="-23.2510859375" />
                  <Point X="-3.536442138672" Y="-23.23678515625" />
                  <Point X="-3.544302734375" Y="-23.230041015625" />
                  <Point X="-3.691310546875" Y="-23.117236328125" />
                  <Point X="-4.227614257812" Y="-22.705716796875" />
                  <Point X="-4.05296875" Y="-22.406505859375" />
                  <Point X="-4.002296386719" Y="-22.319693359375" />
                  <Point X="-3.726338378906" Y="-21.964986328125" />
                  <Point X="-3.254160644531" Y="-22.237599609375" />
                  <Point X="-3.244918457031" Y="-22.24228515625" />
                  <Point X="-3.225991943359" Y="-22.25061328125" />
                  <Point X="-3.216302978516" Y="-22.254259765625" />
                  <Point X="-3.195661376953" Y="-22.26076953125" />
                  <Point X="-3.185624023438" Y="-22.263341796875" />
                  <Point X="-3.165330322266" Y="-22.26737890625" />
                  <Point X="-3.155073974609" Y="-22.26884375" />
                  <Point X="-3.084301269531" Y="-22.27503515625" />
                  <Point X="-3.059173583984" Y="-22.276666015625" />
                  <Point X="-3.038489501953" Y="-22.27621484375" />
                  <Point X="-3.028156494141" Y="-22.27542578125" />
                  <Point X="-3.006697998047" Y="-22.272599609375" />
                  <Point X="-2.996520996094" Y="-22.270689453125" />
                  <Point X="-2.976432373047" Y="-22.2657734375" />
                  <Point X="-2.956992431641" Y="-22.258697265625" />
                  <Point X="-2.938443603516" Y="-22.249548828125" />
                  <Point X="-2.929423095703" Y="-22.244470703125" />
                  <Point X="-2.911168701172" Y="-22.232841796875" />
                  <Point X="-2.902749511719" Y="-22.22680859375" />
                  <Point X="-2.886616943359" Y="-22.213861328125" />
                  <Point X="-2.878903564453" Y="-22.206947265625" />
                  <Point X="-2.828668701172" Y="-22.156712890625" />
                  <Point X="-2.811267333984" Y="-22.138513671875" />
                  <Point X="-2.798321777344" Y="-22.122384765625" />
                  <Point X="-2.792288818359" Y="-22.113966796875" />
                  <Point X="-2.780658935547" Y="-22.095712890625" />
                  <Point X="-2.775577148438" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759352294922" Y="-22.048693359375" />
                  <Point X="-2.754435302734" Y="-22.028599609375" />
                  <Point X="-2.752525634766" Y="-22.018419921875" />
                  <Point X="-2.749700195312" Y="-21.9969609375" />
                  <Point X="-2.748909912109" Y="-21.986634765625" />
                  <Point X="-2.748458496094" Y="-21.965955078125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.754989257812" Y="-21.884828125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-2.852666015625" Y="-21.658134765625" />
                  <Point X="-3.059386474609" Y="-21.300083984375" />
                  <Point X="-2.736627685547" Y="-21.05262890625" />
                  <Point X="-2.648369140625" Y="-20.984962890625" />
                  <Point X="-2.192524169922" Y="-20.731705078125" />
                  <Point X="-2.118565917969" Y="-20.828087890625" />
                  <Point X="-2.111824951172" Y="-20.835947265625" />
                  <Point X="-2.097522949219" Y="-20.850888671875" />
                  <Point X="-2.089963867188" Y="-20.85796875" />
                  <Point X="-2.073385742188" Y="-20.871880859375" />
                  <Point X="-2.065097412109" Y="-20.87809765625" />
                  <Point X="-2.047895629883" Y="-20.889591796875" />
                  <Point X="-2.038982543945" Y="-20.894869140625" />
                  <Point X="-1.960213012695" Y="-20.935875" />
                  <Point X="-1.934334960938" Y="-20.9487109375" />
                  <Point X="-1.915061157227" Y="-20.95620703125" />
                  <Point X="-1.905219116211" Y="-20.9594296875" />
                  <Point X="-1.884311523438" Y="-20.965033203125" />
                  <Point X="-1.874174804688" Y="-20.967166015625" />
                  <Point X="-1.853725708008" Y="-20.970314453125" />
                  <Point X="-1.83305456543" Y="-20.971216796875" />
                  <Point X="-1.812408813477" Y="-20.96986328125" />
                  <Point X="-1.802122314453" Y="-20.968623046875" />
                  <Point X="-1.780806030273" Y="-20.96486328125" />
                  <Point X="-1.77071496582" Y="-20.962509765625" />
                  <Point X="-1.750860595703" Y="-20.956720703125" />
                  <Point X="-1.741097412109" Y="-20.95328515625" />
                  <Point X="-1.659053710938" Y="-20.91930078125" />
                  <Point X="-1.632589111328" Y="-20.907728515625" />
                  <Point X="-1.614455444336" Y="-20.89778125" />
                  <Point X="-1.60565625" Y="-20.892310546875" />
                  <Point X="-1.587926879883" Y="-20.879896484375" />
                  <Point X="-1.579778686523" Y="-20.873501953125" />
                  <Point X="-1.5642265625" Y="-20.85986328125" />
                  <Point X="-1.550251464844" Y="-20.844611328125" />
                  <Point X="-1.538020019531" Y="-20.8279296875" />
                  <Point X="-1.532360229492" Y="-20.819255859375" />
                  <Point X="-1.521538330078" Y="-20.80051171875" />
                  <Point X="-1.516855712891" Y="-20.791271484375" />
                  <Point X="-1.508525024414" Y="-20.772337890625" />
                  <Point X="-1.504877075195" Y="-20.76264453125" />
                  <Point X="-1.478173217773" Y="-20.677951171875" />
                  <Point X="-1.470026123047" Y="-20.650234375" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.47094921875" Y="-20.50051953125" />
                  <Point X="-1.479266357422" Y="-20.43734375" />
                  <Point X="-1.04542565918" Y="-20.3157109375" />
                  <Point X="-0.931166992188" Y="-20.2836796875" />
                  <Point X="-0.365222961426" Y="-20.21744140625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.271357177734" Y="-20.613701171875" />
                  <Point X="0.378190704346" Y="-20.2149921875" />
                  <Point X="0.728106750488" Y="-20.251638671875" />
                  <Point X="0.827852722168" Y="-20.262083984375" />
                  <Point X="1.353572875977" Y="-20.389009765625" />
                  <Point X="1.453623535156" Y="-20.413166015625" />
                  <Point X="1.794548461914" Y="-20.536822265625" />
                  <Point X="1.858249267578" Y="-20.55992578125" />
                  <Point X="2.189130126953" Y="-20.71466796875" />
                  <Point X="2.250430908203" Y="-20.743337890625" />
                  <Point X="2.570138671875" Y="-20.929599609375" />
                  <Point X="2.629430664063" Y="-20.964142578125" />
                  <Point X="2.817779052734" Y="-21.0980859375" />
                  <Point X="2.778108886719" Y="-21.166796875" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.062371826172" Y="-22.4068984375" />
                  <Point X="2.053181640625" Y="-22.426560546875" />
                  <Point X="2.044182617188" Y="-22.45043359375" />
                  <Point X="2.041301513672" Y="-22.459400390625" />
                  <Point X="2.023540283203" Y="-22.525818359375" />
                  <Point X="2.019303344727" Y="-22.545990234375" />
                  <Point X="2.014257446289" Y="-22.57876171875" />
                  <Point X="2.01316394043" Y="-22.591662109375" />
                  <Point X="2.012740600586" Y="-22.6174921875" />
                  <Point X="2.013410888672" Y="-22.630421875" />
                  <Point X="2.020336425781" Y="-22.68785546875" />
                  <Point X="2.023800537109" Y="-22.710966796875" />
                  <Point X="2.029143554687" Y="-22.732890625" />
                  <Point X="2.032468261719" Y="-22.743693359375" />
                  <Point X="2.040734130859" Y="-22.76578125" />
                  <Point X="2.045318237305" Y="-22.776111328125" />
                  <Point X="2.055681152344" Y="-22.79615625" />
                  <Point X="2.061459716797" Y="-22.80587109375" />
                  <Point X="2.096997314453" Y="-22.858244140625" />
                  <Point X="2.109803955078" Y="-22.875029296875" />
                  <Point X="2.130524658203" Y="-22.899265625" />
                  <Point X="2.139395751953" Y="-22.908337890625" />
                  <Point X="2.158260742188" Y="-22.925212890625" />
                  <Point X="2.168254638672" Y="-22.933015625" />
                  <Point X="2.220627929688" Y="-22.9685546875" />
                  <Point X="2.241278076172" Y="-22.981755859375" />
                  <Point X="2.261327636719" Y="-22.99212109375" />
                  <Point X="2.271662353516" Y="-22.996705078125" />
                  <Point X="2.293751953125" Y="-23.004970703125" />
                  <Point X="2.304553955078" Y="-23.008294921875" />
                  <Point X="2.326475585938" Y="-23.01363671875" />
                  <Point X="2.337595214844" Y="-23.015654296875" />
                  <Point X="2.395021728516" Y="-23.022578125" />
                  <Point X="2.417092529297" Y="-23.0239453125" />
                  <Point X="2.447993896484" Y="-23.0240546875" />
                  <Point X="2.460541259766" Y="-23.023267578125" />
                  <Point X="2.485419921875" Y="-23.02004296875" />
                  <Point X="2.497751220703" Y="-23.01760546875" />
                  <Point X="2.564169921875" Y="-22.99984375" />
                  <Point X="2.57555078125" Y="-22.9964140625" />
                  <Point X="2.601499755859" Y="-22.987703125" />
                  <Point X="2.610322509766" Y="-22.9842421875" />
                  <Point X="2.636033447266" Y="-22.97212890625" />
                  <Point X="2.893810302734" Y="-22.823302734375" />
                  <Point X="3.940405273438" Y="-22.21905078125" />
                  <Point X="4.008778808594" Y="-22.31407421875" />
                  <Point X="4.043959960938" Y="-22.36296875" />
                  <Point X="4.136884277344" Y="-22.51652734375" />
                  <Point X="4.111419921875" Y="-22.53606640625" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168130126953" Y="-23.260138671875" />
                  <Point X="3.152122802734" Y="-23.27478125" />
                  <Point X="3.134672607422" Y="-23.293392578125" />
                  <Point X="3.128577148438" Y="-23.300576171875" />
                  <Point X="3.080775634766" Y="-23.3629375" />
                  <Point X="3.069770263672" Y="-23.37892578125" />
                  <Point X="3.051318359375" Y="-23.408900390625" />
                  <Point X="3.045177246094" Y="-23.420638671875" />
                  <Point X="3.034588134766" Y="-23.444853515625" />
                  <Point X="3.030140136719" Y="-23.457330078125" />
                  <Point X="3.012333984375" Y="-23.521" />
                  <Point X="3.006224853516" Y="-23.545341796875" />
                  <Point X="3.002771240234" Y="-23.567642578125" />
                  <Point X="3.001708984375" Y="-23.578896484375" />
                  <Point X="3.000893310547" Y="-23.60246484375" />
                  <Point X="3.001175048828" Y="-23.613763671875" />
                  <Point X="3.003078125" Y="-23.63624609375" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.01931640625" Y="-23.718271484375" />
                  <Point X="3.024364746094" Y="-23.737560546875" />
                  <Point X="3.034614746094" Y="-23.769728515625" />
                  <Point X="3.039390380859" Y="-23.781796875" />
                  <Point X="3.050542480469" Y="-23.805169921875" />
                  <Point X="3.056918945312" Y="-23.816474609375" />
                  <Point X="3.096675537109" Y="-23.87690234375" />
                  <Point X="3.111738769531" Y="-23.89857421875" />
                  <Point X="3.126291503906" Y="-23.915818359375" />
                  <Point X="3.13408203125" Y="-23.9240078125" />
                  <Point X="3.151325683594" Y="-23.94009765625" />
                  <Point X="3.160032226562" Y="-23.947302734375" />
                  <Point X="3.178241699219" Y="-23.96062890625" />
                  <Point X="3.187744628906" Y="-23.96675" />
                  <Point X="3.245357421875" Y="-23.999181640625" />
                  <Point X="3.264155273438" Y="-24.008435546875" />
                  <Point X="3.294163574219" Y="-24.02120703125" />
                  <Point X="3.306308105469" Y="-24.0254296875" />
                  <Point X="3.331063720703" Y="-24.03219921875" />
                  <Point X="3.343674804688" Y="-24.03474609375" />
                  <Point X="3.421571289062" Y="-24.045041015625" />
                  <Point X="3.432576171875" Y="-24.046169921875" />
                  <Point X="3.461914550781" Y="-24.0483203125" />
                  <Point X="3.471601318359" Y="-24.04853515625" />
                  <Point X="3.500602539062" Y="-24.047203125" />
                  <Point X="3.745473388672" Y="-24.014966796875" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.737577148438" Y="-24.023716796875" />
                  <Point X="4.75268359375" Y="-24.085767578125" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.76749609375" Y="-24.29046484375" />
                  <Point X="3.691991455078" Y="-24.578646484375" />
                  <Point X="3.68602734375" Y="-24.58045703125" />
                  <Point X="3.665625244141" Y="-24.58786328125" />
                  <Point X="3.642383300781" Y="-24.598380859375" />
                  <Point X="3.634008300781" Y="-24.60268359375" />
                  <Point X="3.557477539062" Y="-24.646919921875" />
                  <Point X="3.542062011719" Y="-24.656853515625" />
                  <Point X="3.511766601562" Y="-24.67851171875" />
                  <Point X="3.501362792969" Y="-24.68712890625" />
                  <Point X="3.481877441406" Y="-24.705759765625" />
                  <Point X="3.472795898438" Y="-24.7157734375" />
                  <Point X="3.426877441406" Y="-24.77428515625" />
                  <Point X="3.4108515625" Y="-24.795796875" />
                  <Point X="3.399127929688" Y="-24.815078125" />
                  <Point X="3.393842773438" Y="-24.825064453125" />
                  <Point X="3.384069335938" Y="-24.84652734375" />
                  <Point X="3.380006103516" Y="-24.8570703125" />
                  <Point X="3.373159667969" Y="-24.8785703125" />
                  <Point X="3.370376464844" Y="-24.88952734375" />
                  <Point X="3.3550703125" Y="-24.969451171875" />
                  <Point X="3.352444824219" Y="-24.988150390625" />
                  <Point X="3.349248535156" Y="-25.023646484375" />
                  <Point X="3.348983154297" Y="-25.03688671875" />
                  <Point X="3.350296142578" Y="-25.063275390625" />
                  <Point X="3.351874511719" Y="-25.076423828125" />
                  <Point X="3.367180908203" Y="-25.156345703125" />
                  <Point X="3.373158935547" Y="-25.183986328125" />
                  <Point X="3.380005371094" Y="-25.20548828125" />
                  <Point X="3.384069580078" Y="-25.216037109375" />
                  <Point X="3.393843261719" Y="-25.2375" />
                  <Point X="3.399130126953" Y="-25.24748828125" />
                  <Point X="3.410853515625" Y="-25.266767578125" />
                  <Point X="3.417290039063" Y="-25.27605859375" />
                  <Point X="3.463208496094" Y="-25.3345703125" />
                  <Point X="3.476296386719" Y="-25.34955859375" />
                  <Point X="3.500195800781" Y="-25.374197265625" />
                  <Point X="3.509964599609" Y="-25.38296484375" />
                  <Point X="3.530613769531" Y="-25.399068359375" />
                  <Point X="3.541494140625" Y="-25.406404296875" />
                  <Point X="3.618025146484" Y="-25.450640625" />
                  <Point X="3.627104248047" Y="-25.45556640625" />
                  <Point X="3.655164306641" Y="-25.469810546875" />
                  <Point X="3.664187255859" Y="-25.473814453125" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="3.916550048828" Y="-25.5440859375" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.770048339844" Y="-25.875107421875" />
                  <Point X="4.76161328125" Y="-25.93105859375" />
                  <Point X="4.727803222656" Y="-26.07921875" />
                  <Point X="4.684065429688" Y="-26.0734609375" />
                  <Point X="3.436782226562" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354482177734" Y="-25.912677734375" />
                  <Point X="3.204279052734" Y="-25.94532421875" />
                  <Point X="3.17291796875" Y="-25.952140625" />
                  <Point X="3.157876220703" Y="-25.9567421875" />
                  <Point X="3.128758056641" Y="-25.968365234375" />
                  <Point X="3.114681640625" Y="-25.97538671875" />
                  <Point X="3.086855712891" Y="-25.99227734375" />
                  <Point X="3.07412890625" Y="-26.001525390625" />
                  <Point X="3.050375732422" Y="-26.021998046875" />
                  <Point X="3.039349365234" Y="-26.03322265625" />
                  <Point X="2.948561035156" Y="-26.1424140625" />
                  <Point X="2.929605224609" Y="-26.1652109375" />
                  <Point X="2.921326904297" Y="-26.176845703125" />
                  <Point X="2.906606689453" Y="-26.2012265625" />
                  <Point X="2.900164794922" Y="-26.21397265625" />
                  <Point X="2.888820800781" Y="-26.241359375" />
                  <Point X="2.884362792969" Y="-26.254927734375" />
                  <Point X="2.877531005859" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.862145019531" Y="-26.43806640625" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607177734" Y="-26.51458984375" />
                  <Point X="2.864065429688" Y="-26.530126953125" />
                  <Point X="2.871796875" Y="-26.561748046875" />
                  <Point X="2.876785644531" Y="-26.57666796875" />
                  <Point X="2.889157470703" Y="-26.60548046875" />
                  <Point X="2.896540527344" Y="-26.619373046875" />
                  <Point X="2.979665039062" Y="-26.748666015625" />
                  <Point X="2.997020751953" Y="-26.775662109375" />
                  <Point X="3.001743164062" Y="-26.78235546875" />
                  <Point X="3.019788818359" Y="-26.804443359375" />
                  <Point X="3.043485595703" Y="-26.8281171875" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.261186523438" Y="-26.99618359375" />
                  <Point X="4.087169677734" Y="-27.629982421875" />
                  <Point X="4.069264648437" Y="-27.658955078125" />
                  <Point X="4.045482421875" Y="-27.69744140625" />
                  <Point X="4.001276123047" Y="-27.760251953125" />
                  <Point X="3.959328125" Y="-27.736033203125" />
                  <Point X="2.848454101562" Y="-27.094669921875" />
                  <Point X="2.84119140625" Y="-27.090884765625" />
                  <Point X="2.815037597656" Y="-27.079517578125" />
                  <Point X="2.783124267578" Y="-27.069326171875" />
                  <Point X="2.771108154297" Y="-27.0663359375" />
                  <Point X="2.592342773438" Y="-27.03405078125" />
                  <Point X="2.555017822266" Y="-27.027310546875" />
                  <Point X="2.539352539062" Y="-27.025806640625" />
                  <Point X="2.507995849609" Y="-27.025404296875" />
                  <Point X="2.492304443359" Y="-27.026505859375" />
                  <Point X="2.460133300781" Y="-27.03146484375" />
                  <Point X="2.444841796875" Y="-27.035138671875" />
                  <Point X="2.415069824219" Y="-27.0449609375" />
                  <Point X="2.400589355469" Y="-27.051109375" />
                  <Point X="2.252079101562" Y="-27.12926953125" />
                  <Point X="2.221071289062" Y="-27.145587890625" />
                  <Point X="2.208969482422" Y="-27.153169921875" />
                  <Point X="2.186041748047" Y="-27.170060546875" />
                  <Point X="2.175215820312" Y="-27.179369140625" />
                  <Point X="2.154255126953" Y="-27.200328125" />
                  <Point X="2.144942626953" Y="-27.211158203125" />
                  <Point X="2.128047119141" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.042304077148" Y="-27.394705078125" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.03447277832" Y="-27.75890625" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236816406" Y="-27.831546875" />
                  <Point X="2.0640703125" Y="-27.862482421875" />
                  <Point X="2.069547119141" Y="-27.873580078125" />
                  <Point X="2.203459228516" Y="-28.105521484375" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723752441406" Y="-29.036083984375" />
                  <Point X="2.681614013672" Y="-28.98116796875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828652709961" Y="-27.870146484375" />
                  <Point X="1.808834594727" Y="-27.849630859375" />
                  <Point X="1.783253173828" Y="-27.828005859375" />
                  <Point X="1.773297973633" Y="-27.820646484375" />
                  <Point X="1.596986938477" Y="-27.707294921875" />
                  <Point X="1.560174560547" Y="-27.68362890625" />
                  <Point X="1.546280883789" Y="-27.67624609375" />
                  <Point X="1.517468505859" Y="-27.663875" />
                  <Point X="1.502549926758" Y="-27.65888671875" />
                  <Point X="1.470931884766" Y="-27.65115625" />
                  <Point X="1.455393920898" Y="-27.648697265625" />
                  <Point X="1.424126098633" Y="-27.64637890625" />
                  <Point X="1.408396240234" Y="-27.64651953125" />
                  <Point X="1.21557019043" Y="-27.664263671875" />
                  <Point X="1.175309448242" Y="-27.667966796875" />
                  <Point X="1.161230957031" Y="-27.67033984375" />
                  <Point X="1.133583251953" Y="-27.677169921875" />
                  <Point X="1.120013916016" Y="-27.681626953125" />
                  <Point X="1.092625976562" Y="-27.692970703125" />
                  <Point X="1.079876342773" Y="-27.6994140625" />
                  <Point X="1.05549206543" Y="-27.71413671875" />
                  <Point X="1.043857421875" Y="-27.722416015625" />
                  <Point X="0.894962097168" Y="-27.84621875" />
                  <Point X="0.863873901367" Y="-27.872068359375" />
                  <Point X="0.852652709961" Y="-27.883091796875" />
                  <Point X="0.832183349609" Y="-27.90683984375" />
                  <Point X="0.822934814453" Y="-27.919564453125" />
                  <Point X="0.806043212891" Y="-27.947388671875" />
                  <Point X="0.799020935059" Y="-27.96146484375" />
                  <Point X="0.787395385742" Y="-27.9905859375" />
                  <Point X="0.782792053223" Y="-28.005630859375" />
                  <Point X="0.738272949219" Y="-28.210453125" />
                  <Point X="0.728977783203" Y="-28.253216796875" />
                  <Point X="0.727584899902" Y="-28.261287109375" />
                  <Point X="0.72472479248" Y="-28.289673828125" />
                  <Point X="0.7247421875" Y="-28.32316796875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.763504516602" Y="-28.62377734375" />
                  <Point X="0.833091003418" Y="-29.152337890625" />
                  <Point X="0.655065124512" Y="-28.487935546875" />
                  <Point X="0.652606445312" Y="-28.480123046875" />
                  <Point X="0.642143859863" Y="-28.453576171875" />
                  <Point X="0.626786621094" Y="-28.423814453125" />
                  <Point X="0.620407592773" Y="-28.413208984375" />
                  <Point X="0.484960388184" Y="-28.218056640625" />
                  <Point X="0.456679992676" Y="-28.17730859375" />
                  <Point X="0.446670227051" Y="-28.165171875" />
                  <Point X="0.424789978027" Y="-28.142720703125" />
                  <Point X="0.412919464111" Y="-28.13240625" />
                  <Point X="0.386663726807" Y="-28.11316015625" />
                  <Point X="0.373248748779" Y="-28.10494140625" />
                  <Point X="0.345244018555" Y="-28.090830078125" />
                  <Point X="0.330654144287" Y="-28.0849375" />
                  <Point X="0.121057373047" Y="-28.01988671875" />
                  <Point X="0.077295211792" Y="-28.0063046875" />
                  <Point X="0.063377120972" Y="-28.003109375" />
                  <Point X="0.035217838287" Y="-27.99883984375" />
                  <Point X="0.020976644516" Y="-27.997765625" />
                  <Point X="-0.008664559364" Y="-27.997765625" />
                  <Point X="-0.022905754089" Y="-27.99883984375" />
                  <Point X="-0.051065036774" Y="-28.003109375" />
                  <Point X="-0.064983276367" Y="-28.0063046875" />
                  <Point X="-0.274580047607" Y="-28.07135546875" />
                  <Point X="-0.318342376709" Y="-28.0849375" />
                  <Point X="-0.332930908203" Y="-28.090830078125" />
                  <Point X="-0.360932800293" Y="-28.104939453125" />
                  <Point X="-0.374346313477" Y="-28.11315625" />
                  <Point X="-0.40060144043" Y="-28.132400390625" />
                  <Point X="-0.412476409912" Y="-28.14271875" />
                  <Point X="-0.43435949707" Y="-28.165173828125" />
                  <Point X="-0.444367767334" Y="-28.17730859375" />
                  <Point X="-0.579815246582" Y="-28.372462890625" />
                  <Point X="-0.60809552002" Y="-28.413208984375" />
                  <Point X="-0.612468994141" Y="-28.42012890625" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.711944885254" Y="-28.7461640625" />
                  <Point X="-0.985425598145" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.989245860902" Y="-20.299961504998" />
                  <Point X="-1.469219279526" Y="-20.513659439155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.133047846561" Y="-20.809214959326" />
                  <Point X="-2.815384660143" Y="-21.113010881775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.64348497461" Y="-20.250009286621" />
                  <Point X="-1.46415088248" Y="-20.61539328986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.060737644953" Y="-20.881010829788" />
                  <Point X="-3.047649843718" Y="-21.320412450559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.362190340573" Y="-20.228759292983" />
                  <Point X="-1.495660234165" Y="-20.733412603543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.955570410513" Y="-20.938177806719" />
                  <Point X="-2.999888247482" Y="-21.403138064317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.337296024706" Y="-20.321666075915" />
                  <Point X="-1.583763500164" Y="-20.876629151293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.782684943218" Y="-20.965194683864" />
                  <Point X="-2.952126651247" Y="-21.485863678075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.312401708839" Y="-20.414572858848" />
                  <Point X="-2.904365055012" Y="-21.568589291833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.287507392971" Y="-20.507479641781" />
                  <Point X="-2.856603458776" Y="-21.651314905591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.654403289518" Y="-22.006518275371" />
                  <Point X="-3.813894917849" Y="-22.077528523371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.560186426549" Y="-20.23405248343" />
                  <Point X="0.347738866584" Y="-20.32864023125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.262613077104" Y="-20.600386424714" />
                  <Point X="-2.80884184556" Y="-21.734040511788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.552709275992" Y="-22.065231629883" />
                  <Point X="-3.937672839463" Y="-22.236628451139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749274782047" Y="-20.253855369962" />
                  <Point X="0.316100343027" Y="-20.446717055956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.237718761237" Y="-20.693293207647" />
                  <Point X="-2.767463747672" Y="-21.819608242123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.451015262465" Y="-22.123944984395" />
                  <Point X="-4.041490747782" Y="-22.38684160843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.916363192236" Y="-20.283453263222" />
                  <Point X="0.28446181947" Y="-20.564793880663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.206406652214" Y="-20.78334260497" />
                  <Point X="-2.752192130596" Y="-21.916799326588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.349321248939" Y="-22.182658338907" />
                  <Point X="-4.123501258931" Y="-22.527345486948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.067806861736" Y="-20.32001664381" />
                  <Point X="0.252823334883" Y="-20.682870688019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.145900612501" Y="-20.860394026914" />
                  <Point X="-2.753041119372" Y="-22.021167767203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.247144337252" Y="-22.241156693306" />
                  <Point X="-4.20551145077" Y="-22.667849223301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.219250531236" Y="-20.356580024399" />
                  <Point X="0.202740349027" Y="-20.809159516426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.027853886898" Y="-20.911826684927" />
                  <Point X="-2.832484559864" Y="-22.160528712227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.088788540322" Y="-22.274642596486" />
                  <Point X="-4.16496660428" Y="-22.75378794106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.37069400193" Y="-20.393143493501" />
                  <Point X="-4.079205122589" Y="-22.819594915775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.511852489619" Y="-20.434286132065" />
                  <Point X="-3.993443640898" Y="-22.885401890489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.640563598848" Y="-20.480970700576" />
                  <Point X="-3.907682159206" Y="-22.951208865204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.769274708077" Y="-20.527655269088" />
                  <Point X="-3.821920677515" Y="-23.017015839918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.893418559083" Y="-20.576373311973" />
                  <Point X="-3.736159195823" Y="-23.082822814633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.007331247953" Y="-20.629646561726" />
                  <Point X="-3.650398136283" Y="-23.148629977301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.121243936824" Y="-20.682919811478" />
                  <Point X="-3.564637539504" Y="-23.214437346004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.618681337027" Y="-23.683727880232" />
                  <Point X="-4.63623611284" Y="-23.691543769988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.235155354536" Y="-20.736193627187" />
                  <Point X="-3.492441762849" Y="-23.286284161737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.438417365579" Y="-23.707459635674" />
                  <Point X="-4.668281753098" Y="-23.809801854729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338037993223" Y="-20.794377771681" />
                  <Point X="-3.446413423836" Y="-23.369781471329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.258153394132" Y="-23.731191391116" />
                  <Point X="-4.700327393357" Y="-23.928059939469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.439212883217" Y="-20.853322254881" />
                  <Point X="-3.421650607729" Y="-23.462746801728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.077889422684" Y="-23.754923146558" />
                  <Point X="-4.731866939712" Y="-24.046092696686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.540387773211" Y="-20.912266738081" />
                  <Point X="-3.441633189984" Y="-23.575634067012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.897625451237" Y="-23.778654902" />
                  <Point X="-4.748340504003" Y="-24.157417646516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.640214494253" Y="-20.971811464771" />
                  <Point X="-3.528835861333" Y="-23.71844964419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.68504354059" Y="-23.787997783861" />
                  <Point X="-4.764813861584" Y="-24.268742504312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730142415728" Y="-21.035763420978" />
                  <Point X="-4.781287219164" Y="-24.380067362108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.815720046194" Y="-21.101652251531" />
                  <Point X="-4.676447223021" Y="-24.437380034916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734908322405" Y="-21.24162239553" />
                  <Point X="-4.530634437318" Y="-24.476450446494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.654096420343" Y="-21.3815926189" />
                  <Point X="-4.384821651616" Y="-24.515520858073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.573284518282" Y="-21.52156284227" />
                  <Point X="-4.239008865914" Y="-24.554591269651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.49247261622" Y="-21.66153306564" />
                  <Point X="-4.093196080211" Y="-24.59366168123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.411660714159" Y="-21.801503289011" />
                  <Point X="-3.947383294509" Y="-24.632732092809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330848812097" Y="-21.941473512381" />
                  <Point X="-3.801570508806" Y="-24.671802504387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.250036910036" Y="-22.081443735751" />
                  <Point X="-3.65575746716" Y="-24.710872802012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.169225007975" Y="-22.221413959121" />
                  <Point X="-3.509944338505" Y="-24.749943060899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.088413105913" Y="-22.361384182491" />
                  <Point X="-3.40324232591" Y="-24.806426710569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.033120723959" Y="-22.489992383474" />
                  <Point X="-3.337484726869" Y="-24.881139987657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012978929757" Y="-22.602950534485" />
                  <Point X="-3.302069622641" Y="-24.96936261382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022557870204" Y="-22.702676161881" />
                  <Point X="-3.298449799901" Y="-25.071741411358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.337526163539" Y="-25.534368014676" />
                  <Point X="-4.775751094376" Y="-25.729478324502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053900510588" Y="-22.792711965767" />
                  <Point X="-3.348987971785" Y="-25.198232901642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.750933988585" Y="-25.377190798267" />
                  <Point X="-4.761768385889" Y="-25.827243268044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.107972004621" Y="-22.872628232024" />
                  <Point X="-4.747785677403" Y="-25.925008211585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.183171422317" Y="-22.943137740605" />
                  <Point X="-4.72889206374" Y="-26.020586679271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965313469419" Y="-22.253667426399" />
                  <Point X="3.59446280399" Y="-22.418780780614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.285116352416" Y="-23.001739379861" />
                  <Point X="-4.705042021494" Y="-26.113958402776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021983902562" Y="-22.332426570413" />
                  <Point X="2.807378229774" Y="-22.873203857277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.474349361908" Y="-23.021477862286" />
                  <Point X="-4.681191872824" Y="-26.207330078897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.074308745599" Y="-22.413120495796" />
                  <Point X="-4.611166472822" Y="-26.280143208574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.123881327547" Y="-22.495039806767" />
                  <Point X="-4.279539422216" Y="-26.236483779278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.898715355271" Y="-22.699280603037" />
                  <Point X="-3.947912371609" Y="-26.192824349982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.575863363432" Y="-22.947014017371" />
                  <Point X="-3.616285321003" Y="-26.149164920686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.253011371593" Y="-23.194747431705" />
                  <Point X="-3.284661893558" Y="-26.105507104525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.068586204378" Y="-23.3808492529" />
                  <Point X="-3.099960928308" Y="-26.12726338305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015884601919" Y="-23.508303964534" />
                  <Point X="-3.012330187236" Y="-26.192238109867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001589427861" Y="-23.618659032544" />
                  <Point X="-2.961311041922" Y="-26.273513369332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.018652251041" Y="-23.715052620671" />
                  <Point X="-2.949088416177" Y="-26.372061952198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.050414463733" Y="-23.80490161893" />
                  <Point X="-2.98173556218" Y="-26.490587844551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.102722901455" Y="-23.88560284843" />
                  <Point X="-3.204132664478" Y="-26.693595860481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.174283731006" Y="-23.957732360827" />
                  <Point X="-3.526987287672" Y="-26.94133044637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.279398106563" Y="-24.014922872049" />
                  <Point X="-3.849841910866" Y="-27.189065032258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.441339740301" Y="-24.046812257822" />
                  <Point X="-4.172695287903" Y="-27.436799063322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.746834735339" Y="-24.01478756927" />
                  <Point X="-4.133561799674" Y="-27.523366158264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.078464370614" Y="-23.971126989206" />
                  <Point X="-4.084557663017" Y="-27.605538557384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.410094005888" Y="-23.927466409141" />
                  <Point X="-4.03555352636" Y="-27.687710956503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.707253252794" Y="-23.899153034772" />
                  <Point X="-3.982943519636" Y="-27.768277918834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.730093214569" Y="-23.992974475077" />
                  <Point X="-3.923824926595" Y="-27.845947071835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752849500905" Y="-24.086833170087" />
                  <Point X="-2.573669292452" Y="-27.348809500342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.83956277657" Y="-27.467192906707" />
                  <Point X="-3.864706887425" Y="-27.923616471435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.76799069109" Y="-24.184082324345" />
                  <Point X="4.168651362626" Y="-24.450925385611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419186670461" Y="-24.784608565188" />
                  <Point X="-2.432589144438" Y="-27.389987017976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.626640046734" Y="-27.921612731397" />
                  <Point X="-3.805588848255" Y="-28.001285871036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783131881274" Y="-24.281331478603" />
                  <Point X="4.755244953761" Y="-24.293747538677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.366021061509" Y="-24.912269865824" />
                  <Point X="-2.358897397809" Y="-27.461167784965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349246888159" Y="-25.023728655429" />
                  <Point X="-2.3179135586" Y="-27.546911050572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.360720118634" Y="-25.122610890567" />
                  <Point X="-2.31652076262" Y="-27.650281384307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.384133330095" Y="-25.216177103667" />
                  <Point X="-2.373558719206" Y="-27.779666765189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.434357184046" Y="-25.29780644966" />
                  <Point X="-2.454370552652" Y="-27.91963695801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.499034944622" Y="-25.373000501808" />
                  <Point X="-2.535182386098" Y="-28.05960715083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592002567243" Y="-25.43559909587" />
                  <Point X="-2.615994219544" Y="-28.199577343651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707633941094" Y="-25.488107137768" />
                  <Point X="-2.69680605299" Y="-28.339547536471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.853446817977" Y="-25.527177508751" />
                  <Point X="-2.777617886435" Y="-28.479517729292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.999259835181" Y="-25.566247817258" />
                  <Point X="-2.858429719881" Y="-28.619487922112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.145072959442" Y="-25.605318078101" />
                  <Point X="3.456570596445" Y="-25.91185908001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.954336218178" Y="-26.135468231962" />
                  <Point X="-2.939241553327" Y="-28.759458114933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.290886083702" Y="-25.644388338944" />
                  <Point X="3.636834023582" Y="-25.935591077794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880008338016" Y="-26.272551582786" />
                  <Point X="-2.939360535148" Y="-28.863501535511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.436699207963" Y="-25.683458599786" />
                  <Point X="3.81709745072" Y="-25.959323075579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.867285229195" Y="-26.382206722258" />
                  <Point X="-2.853785546648" Y="-28.929391542344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.582512332224" Y="-25.722528860629" />
                  <Point X="3.997360877857" Y="-25.983055073363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859750519417" Y="-26.489551837645" />
                  <Point X="-2.768210558149" Y="-28.995281549177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.728325456485" Y="-25.761599121472" />
                  <Point X="4.177624304995" Y="-26.006787071148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880134391697" Y="-26.584466799446" />
                  <Point X="-2.673433586575" Y="-29.057074569184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.774588732632" Y="-25.844991830313" />
                  <Point X="4.357887732132" Y="-26.030519068932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.927420473953" Y="-26.667404125668" />
                  <Point X="-0.113894930952" Y="-28.021484985002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.432622866625" Y="-28.163391804773" />
                  <Point X="-1.826012590851" Y="-28.783768879813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413217183552" Y="-29.045209208628" />
                  <Point X="-2.575735265574" Y="-29.117566920626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.755593137666" Y="-25.957439660544" />
                  <Point X="4.538151159269" Y="-26.054251066717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.979399013183" Y="-26.748252235441" />
                  <Point X="0.12235597013" Y="-28.020289753369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.540093421606" Y="-28.315231225135" />
                  <Point X="-1.624428319318" Y="-28.798008226078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.72917886604" Y="-26.073190498431" />
                  <Point X="4.718415056089" Y="-26.077982855385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.040433728467" Y="-26.825068275854" />
                  <Point X="2.57719514065" Y="-27.031315383292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.131119894441" Y="-27.229920878711" />
                  <Point X="0.259984065344" Y="-28.063004223933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.632158844375" Y="-28.460211838735" />
                  <Point X="-1.50367823664" Y="-28.848237271974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.124772075176" Y="-26.891508871086" />
                  <Point X="2.743359787457" Y="-27.061324562508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056910728945" Y="-27.36695137436" />
                  <Point X="1.42856670905" Y="-27.64670815626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826385842299" Y="-27.914816351881" />
                  <Point X="0.384237273283" Y="-28.111673577975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.667391894375" Y="-28.579889049724" />
                  <Point X="-1.424065960995" Y="-28.916782049612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.210533332191" Y="-26.957315945833" />
                  <Point X="2.871781480366" Y="-27.108137987467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.00498122462" Y="-27.494062325758" />
                  <Point X="1.567962468324" Y="-27.688635612079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.774950686007" Y="-28.041707205354" />
                  <Point X="0.45991518465" Y="-28.181970047448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.699030279891" Y="-28.697965812971" />
                  <Point X="-1.345416815348" Y="-28.985755640353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.296294650916" Y="-27.023122993105" />
                  <Point X="2.973475788056" Y="-27.166851211009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.005389921529" Y="-27.597870808629" />
                  <Point X="1.663530683532" Y="-27.750076347722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749926175803" Y="-28.156839281591" />
                  <Point X="0.515051663122" Y="-28.261412152084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.730668819487" Y="-28.816042644818" />
                  <Point X="-1.266767669701" Y="-29.054729231094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382056058678" Y="-27.088930000735" />
                  <Point X="3.075170095747" Y="-27.22556443455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022772783729" Y="-27.694121906202" />
                  <Point X="1.759098237719" Y="-27.811517377672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.726583869197" Y="-28.271222392531" />
                  <Point X="0.570188916673" Y="-28.340853911632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.762307465357" Y="-28.934119523982" />
                  <Point X="-1.207480216454" Y="-29.132323202688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.467817466439" Y="-27.154737008365" />
                  <Point X="3.176864403437" Y="-27.284277658092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.040155372343" Y="-27.790373125585" />
                  <Point X="1.837216681928" Y="-27.880727251916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.730548294769" Y="-28.373447763004" />
                  <Point X="0.624808703831" Y="-28.420526062062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.793946111228" Y="-29.052196403146" />
                  <Point X="-1.177941397595" Y="-29.22316211966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.5535788742" Y="-27.220544015996" />
                  <Point X="3.278558711127" Y="-27.342990881634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.073082483374" Y="-27.879703477687" />
                  <Point X="1.896692306935" Y="-27.958237444045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.743480740547" Y="-28.47168031363" />
                  <Point X="0.66059741244" Y="-28.508582348837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.825584757098" Y="-29.17027328231" />
                  <Point X="-1.15893929544" Y="-29.318692285157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.639340281961" Y="-27.286351023626" />
                  <Point X="3.380253018818" Y="-27.401704105176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.120844318467" Y="-27.962428985099" />
                  <Point X="1.956167931941" Y="-28.035747636173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.756413186325" Y="-28.569912864257" />
                  <Point X="0.685491687805" Y="-28.601489149803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.857223402969" Y="-29.288350161474" />
                  <Point X="-1.139937272522" Y="-29.414222485933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.725101689722" Y="-27.352158031256" />
                  <Point X="3.481947326508" Y="-27.460417328717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.168606153561" Y="-28.04515449251" />
                  <Point X="2.015643556948" Y="-28.113257828302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769345694642" Y="-28.668145387039" />
                  <Point X="0.71038596317" Y="-28.694395950769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.888862048839" Y="-29.406427040637" />
                  <Point X="-1.121703618446" Y="-29.510094786559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.810863097483" Y="-27.417965038887" />
                  <Point X="3.583641634199" Y="-27.519130552259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.216367938812" Y="-28.127880022113" />
                  <Point X="2.075119181954" Y="-28.19076802043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.782278278884" Y="-28.766377876017" />
                  <Point X="0.735280238534" Y="-28.787302751734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.92050069471" Y="-29.524503919801" />
                  <Point X="-1.126066577237" Y="-29.616027747423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.896624505245" Y="-27.483772046517" />
                  <Point X="3.685335941889" Y="-27.577843775801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.264129589491" Y="-28.210605611631" />
                  <Point X="2.134594806961" Y="-28.268278212559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795210863125" Y="-28.864610364996" />
                  <Point X="0.760174513899" Y="-28.8802095527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.95213934058" Y="-29.642580798965" />
                  <Point X="-1.140609932268" Y="-29.726493312722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.982385913006" Y="-27.549579054148" />
                  <Point X="3.787030249579" Y="-27.636556999342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.311891240171" Y="-28.293331201149" />
                  <Point X="2.194070431967" Y="-28.345788404687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808143447367" Y="-28.962842853975" />
                  <Point X="0.785068789264" Y="-28.973116353666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.983777986451" Y="-29.760657678129" />
                  <Point X="-0.993896105229" Y="-29.765162554851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068147320767" Y="-27.615386061778" />
                  <Point X="3.88872455727" Y="-27.695270222884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.359652890851" Y="-28.376056790666" />
                  <Point X="2.253546056974" Y="-28.423298596816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.821076031608" Y="-29.061075342953" />
                  <Point X="0.809963064629" Y="-29.066023154631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.012656012229" Y="-27.744082830582" />
                  <Point X="3.990418816295" Y="-27.753983468093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.407414541531" Y="-28.458782380184" />
                  <Point X="2.31302168198" Y="-28.500808788944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.455176192211" Y="-28.541507969702" />
                  <Point X="2.372497306987" Y="-28.578318981073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.50293784289" Y="-28.62423355922" />
                  <Point X="2.431972931993" Y="-28.655829173201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.55069949357" Y="-28.706959148737" />
                  <Point X="2.491448557" Y="-28.73333936533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.59846114425" Y="-28.789684738255" />
                  <Point X="2.550924182006" Y="-28.810849557459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.64622279493" Y="-28.872410327773" />
                  <Point X="2.610399807012" Y="-28.888359749587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69398444561" Y="-28.955135917291" />
                  <Point X="2.669875432019" Y="-28.965869941716" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.802928649902" Y="-29.773875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464319122314" Y="-28.521544921875" />
                  <Point X="0.328871887207" Y="-28.326392578125" />
                  <Point X="0.300591491699" Y="-28.28564453125" />
                  <Point X="0.274335601807" Y="-28.2663984375" />
                  <Point X="0.064738815308" Y="-28.20134765625" />
                  <Point X="0.020976625443" Y="-28.187765625" />
                  <Point X="-0.008664604187" Y="-28.187765625" />
                  <Point X="-0.218261398315" Y="-28.25281640625" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288278839111" Y="-28.285642578125" />
                  <Point X="-0.423726257324" Y="-28.480796875" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.528418762207" Y="-28.79533984375" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-1.049944580078" Y="-29.947830078125" />
                  <Point X="-1.10025769043" Y="-29.9380625" />
                  <Point X="-1.343407836914" Y="-29.87550390625" />
                  <Point X="-1.349041748047" Y="-29.85404296875" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.359755737305" Y="-29.28302734375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.579252807617" Y="-29.0333984375" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649241577148" Y="-28.985763671875" />
                  <Point X="-1.905356079102" Y="-28.9689765625" />
                  <Point X="-1.958830810547" Y="-28.96547265625" />
                  <Point X="-1.98987890625" Y="-28.97379296875" />
                  <Point X="-2.203287353516" Y="-29.11638671875" />
                  <Point X="-2.247845458984" Y="-29.14616015625" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.298579833984" Y="-29.207919921875" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.782108642578" Y="-29.213259765625" />
                  <Point X="-2.855836914062" Y="-29.167609375" />
                  <Point X="-3.192542480469" Y="-28.908357421875" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-3.121883789062" Y="-28.695802734375" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513982421875" Y="-27.56876171875" />
                  <Point X="-2.528334472656" Y="-27.55441015625" />
                  <Point X="-2.531331054688" Y="-27.5514140625" />
                  <Point X="-2.560159179688" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.814399414062" Y="-27.672056640625" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.103456542969" Y="-27.923654296875" />
                  <Point X="-4.161699707031" Y="-27.84713671875" />
                  <Point X="-4.403097167969" Y="-27.442349609375" />
                  <Point X="-4.43101953125" Y="-27.39552734375" />
                  <Point X="-4.241324707031" Y="-27.24996875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145821777344" Y="-26.396013671875" />
                  <Point X="-3.139447998047" Y="-26.371404296875" />
                  <Point X="-3.1381171875" Y="-26.366265625" />
                  <Point X="-3.140326416016" Y="-26.334595703125" />
                  <Point X="-3.161158691406" Y="-26.310640625" />
                  <Point X="-3.183067138672" Y="-26.29774609375" />
                  <Point X="-3.187647216797" Y="-26.29505078125" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.500686767578" Y="-26.3255859375" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.904611816406" Y="-26.10037890625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.991259765625" Y="-25.564642578125" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.784941894531" Y="-25.457548828125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541895019531" Y="-25.12142578125" />
                  <Point X="-3.518935791016" Y="-25.105490234375" />
                  <Point X="-3.514148193359" Y="-25.10216796875" />
                  <Point X="-3.494898925781" Y="-25.075908203125" />
                  <Point X="-3.487245849609" Y="-25.05125" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.485647949219" Y="-25.0164609375" />
                  <Point X="-3.493299804688" Y="-24.991806640625" />
                  <Point X="-3.494897460938" Y="-24.98665625" />
                  <Point X="-3.51414453125" Y="-24.960396484375" />
                  <Point X="-3.537096923828" Y="-24.944466796875" />
                  <Point X="-3.537096191406" Y="-24.944466796875" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.813752441406" Y="-24.8652421875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.932326660156" Y="-24.102796875" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.789083496094" Y="-23.52915234375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.63315234375" Y="-23.490181640625" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731703369141" Y="-23.6041328125" />
                  <Point X="-3.680887451172" Y="-23.588111328125" />
                  <Point X="-3.67027734375" Y="-23.584765625" />
                  <Point X="-3.651534423828" Y="-23.573943359375" />
                  <Point X="-3.639120361328" Y="-23.55621484375" />
                  <Point X="-3.618730224609" Y="-23.50698828125" />
                  <Point X="-3.614472900391" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316650391" Y="-23.454486328125" />
                  <Point X="-3.640919433594" Y="-23.407224609375" />
                  <Point X="-3.646056396484" Y="-23.397357421875" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.806976074219" Y="-23.267974609375" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.217061523437" Y="-22.310728515625" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.819458984375" Y="-21.7752578125" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.69955859375" Y="-21.7610546875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138515380859" Y="-22.07956640625" />
                  <Point X="-3.067742675781" Y="-22.0857578125" />
                  <Point X="-3.052966064453" Y="-22.08705078125" />
                  <Point X="-3.031507568359" Y="-22.084224609375" />
                  <Point X="-3.013253173828" Y="-22.072595703125" />
                  <Point X="-2.963018310547" Y="-22.022361328125" />
                  <Point X="-2.952529541016" Y="-22.011873046875" />
                  <Point X="-2.940899658203" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.944266113281" Y="-21.90138671875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.0172109375" Y="-21.753134765625" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.852231933594" Y="-20.90184375" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.216511474609" Y="-20.52767578125" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.133260009766" Y="-20.496830078125" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951248657227" Y="-20.726337890625" />
                  <Point X="-1.872479125977" Y="-20.76734375" />
                  <Point X="-1.856032592773" Y="-20.77590625" />
                  <Point X="-1.835125" Y="-20.781509765625" />
                  <Point X="-1.81380859375" Y="-20.77775" />
                  <Point X="-1.731764770508" Y="-20.743765625" />
                  <Point X="-1.714634643555" Y="-20.736669921875" />
                  <Point X="-1.696905151367" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.659379516602" Y="-20.620818359375" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.659323730469" Y="-20.5253203125" />
                  <Point X="-1.689137695313" Y="-20.298859375" />
                  <Point X="-1.096717163086" Y="-20.132765625" />
                  <Point X="-0.968083557129" Y="-20.096703125" />
                  <Point X="-0.317856201172" Y="-20.0206015625" />
                  <Point X="-0.22420022583" Y="-20.009640625" />
                  <Point X="-0.202006088257" Y="-20.092470703125" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282114029" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594032288" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.087831130981" Y="-20.564525390625" />
                  <Point X="0.236648406982" Y="-20.009130859375" />
                  <Point X="0.747897216797" Y="-20.062673828125" />
                  <Point X="0.860209655762" Y="-20.074435546875" />
                  <Point X="1.398163574219" Y="-20.204314453125" />
                  <Point X="1.508457275391" Y="-20.230943359375" />
                  <Point X="1.859333129883" Y="-20.358208984375" />
                  <Point X="1.931042724609" Y="-20.38421875" />
                  <Point X="2.269619384766" Y="-20.54255859375" />
                  <Point X="2.338688964844" Y="-20.574861328125" />
                  <Point X="2.665784423828" Y="-20.765427734375" />
                  <Point X="2.732518554688" Y="-20.804306640625" />
                  <Point X="3.041010498047" Y="-21.023689453125" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.942653808594" Y="-21.261796875" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224851806641" Y="-22.508486328125" />
                  <Point X="2.207090576172" Y="-22.574904296875" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.208970214844" Y="-22.665109375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218681884766" Y="-22.6991875" />
                  <Point X="2.254219482422" Y="-22.751560546875" />
                  <Point X="2.274940185547" Y="-22.775796875" />
                  <Point X="2.327313476562" Y="-22.8113359375" />
                  <Point X="2.338248779297" Y="-22.818755859375" />
                  <Point X="2.360338378906" Y="-22.827021484375" />
                  <Point X="2.417764892578" Y="-22.8339453125" />
                  <Point X="2.448666259766" Y="-22.8340546875" />
                  <Point X="2.515084960938" Y="-22.81629296875" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.798810791016" Y="-22.658755859375" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.163003417969" Y="-22.2031015625" />
                  <Point X="4.20259375" Y="-22.258123046875" />
                  <Point X="4.374568359375" Y="-22.5423125" />
                  <Point X="4.387513183594" Y="-22.563703125" />
                  <Point X="4.227084472656" Y="-22.6868046875" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279372558594" Y="-23.416166015625" />
                  <Point X="3.231571044922" Y="-23.47852734375" />
                  <Point X="3.213119140625" Y="-23.508501953125" />
                  <Point X="3.195312988281" Y="-23.572171875" />
                  <Point X="3.191595214844" Y="-23.585466796875" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.205396484375" Y="-23.679876953125" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.255403076172" Y="-23.77247265625" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280947753906" Y="-23.8011796875" />
                  <Point X="3.338560546875" Y="-23.833611328125" />
                  <Point X="3.368568847656" Y="-23.8463828125" />
                  <Point X="3.446465332031" Y="-23.856677734375" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.720674560547" Y="-23.826591796875" />
                  <Point X="4.848975097656" Y="-23.678046875" />
                  <Point X="4.922185546875" Y="-23.978775390625" />
                  <Point X="4.939189453125" Y="-24.048619140625" />
                  <Point X="4.993383300781" Y="-24.39669921875" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.816672363281" Y="-24.4739921875" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729090820312" Y="-24.7671796875" />
                  <Point X="3.652560058594" Y="-24.811416015625" />
                  <Point X="3.622264648438" Y="-24.83307421875" />
                  <Point X="3.576346191406" Y="-24.8915859375" />
                  <Point X="3.566758789062" Y="-24.903802734375" />
                  <Point X="3.556985351562" Y="-24.925265625" />
                  <Point X="3.541679199219" Y="-25.005189453125" />
                  <Point X="3.538482910156" Y="-25.040685546875" />
                  <Point X="3.553789306641" Y="-25.120607421875" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.612677246094" Y="-25.21726953125" />
                  <Point X="3.636576660156" Y="-25.241908203125" />
                  <Point X="3.713107666016" Y="-25.28614453125" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="3.965725341797" Y="-25.36055859375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.957925292969" Y="-25.903431640625" />
                  <Point X="4.948431640625" Y="-25.96640234375" />
                  <Point X="4.879" Y="-26.270662109375" />
                  <Point X="4.874546386719" Y="-26.290177734375" />
                  <Point X="4.659265136719" Y="-26.2618359375" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836181641" Y="-26.098341796875" />
                  <Point X="3.244633056641" Y="-26.13098828125" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.185446044922" Y="-26.1546953125" />
                  <Point X="3.094657714844" Y="-26.26388671875" />
                  <Point X="3.075701904297" Y="-26.28668359375" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.051345703125" Y="-26.4554765625" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.139484863281" Y="-26.645916015625" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.3768515625" Y="-26.8454453125" />
                  <Point X="4.339073730469" Y="-27.583783203125" />
                  <Point X="4.230891601562" Y="-27.758837890625" />
                  <Point X="4.204123046875" Y="-27.80215625" />
                  <Point X="4.060542480469" Y="-28.0061640625" />
                  <Point X="4.056688964844" Y="-28.011638671875" />
                  <Point X="3.864328613281" Y="-27.900580078125" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340087891" Y="-27.2533125" />
                  <Point X="2.558574707031" Y="-27.22102734375" />
                  <Point X="2.521249755859" Y="-27.214287109375" />
                  <Point X="2.489078613281" Y="-27.21924609375" />
                  <Point X="2.340568359375" Y="-27.29740625" />
                  <Point X="2.309560546875" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.210439941406" Y="-27.483193359375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.221447998047" Y="-27.725140625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.368003662109" Y="-28.010521484375" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.8670546875" Y="-29.167529296875" />
                  <Point X="2.835303466797" Y="-29.190208984375" />
                  <Point X="2.679774414062" Y="-29.29087890625" />
                  <Point X="2.530876953125" Y="-29.09683203125" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670548461914" Y="-27.980466796875" />
                  <Point X="1.494237426758" Y="-27.867115234375" />
                  <Point X="1.457425048828" Y="-27.84344921875" />
                  <Point X="1.425806884766" Y="-27.83571875" />
                  <Point X="1.232980834961" Y="-27.853462890625" />
                  <Point X="1.192720092773" Y="-27.857166015625" />
                  <Point X="1.165332275391" Y="-27.868509765625" />
                  <Point X="1.016436950684" Y="-27.9923125" />
                  <Point X="0.985348632813" Y="-28.018162109375" />
                  <Point X="0.96845690918" Y="-28.045986328125" />
                  <Point X="0.923937927246" Y="-28.25080859375" />
                  <Point X="0.914642700195" Y="-28.293572265625" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.951879089355" Y="-28.5989765625" />
                  <Point X="1.127642333984" Y="-29.93402734375" />
                  <Point X="1.024388549805" Y="-29.956662109375" />
                  <Point X="0.994327148438" Y="-29.96325" />
                  <Point X="0.860200561523" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#200" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.159865750374" Y="4.951751606804" Z="2.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.1" />
                  <Point X="-0.329824227631" Y="5.060338377271" Z="2.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.1" />
                  <Point X="-1.116370524828" Y="4.946658738665" Z="2.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.1" />
                  <Point X="-1.715051979358" Y="4.499435428002" Z="2.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.1" />
                  <Point X="-1.713221434693" Y="4.425497245507" Z="2.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.1" />
                  <Point X="-1.757055289229" Y="4.33370850278" Z="2.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.1" />
                  <Point X="-1.855545577404" Y="4.308286160276" Z="2.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.1" />
                  <Point X="-2.099748472908" Y="4.564888257088" Z="2.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.1" />
                  <Point X="-2.246950282568" Y="4.547311601988" Z="2.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.1" />
                  <Point X="-2.88881003195" Y="4.169116134428" Z="2.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.1" />
                  <Point X="-3.066668240966" Y="3.253144849134" Z="2.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.1" />
                  <Point X="-3.000231871464" Y="3.12553611259" Z="2.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.1" />
                  <Point X="-3.004528688808" Y="3.044274879078" Z="2.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.1" />
                  <Point X="-3.069540353017" Y="2.995332827709" Z="2.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.1" />
                  <Point X="-3.680714499163" Y="3.31352570778" Z="2.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.1" />
                  <Point X="-3.865078253709" Y="3.286725188062" Z="2.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.1" />
                  <Point X="-4.266399410583" Y="2.745756538975" Z="2.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.1" />
                  <Point X="-3.843569969582" Y="1.723637068881" Z="2.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.1" />
                  <Point X="-3.691425374286" Y="1.600966196766" Z="2.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.1" />
                  <Point X="-3.671079750644" Y="1.543426307468" Z="2.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.1" />
                  <Point X="-3.702079946514" Y="1.49085478136" Z="2.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.1" />
                  <Point X="-4.632782069545" Y="1.590671759161" Z="2.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.1" />
                  <Point X="-4.843499358245" Y="1.515207096669" Z="2.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.1" />
                  <Point X="-4.987943859195" Y="0.935801743036" Z="2.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.1" />
                  <Point X="-3.832849219806" Y="0.117741325474" Z="2.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.1" />
                  <Point X="-3.571767027618" Y="0.04574192491" Z="2.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.1" />
                  <Point X="-3.547209929271" Y="0.024658446061" Z="2.1" />
                  <Point X="-3.539556741714" Y="0" Z="2.1" />
                  <Point X="-3.541154671864" Y="-0.005148505" Z="2.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.1" />
                  <Point X="-3.553601567506" Y="-0.033134058224" Z="2.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.1" />
                  <Point X="-4.804038568967" Y="-0.377970731282" Z="2.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.1" />
                  <Point X="-5.046912050183" Y="-0.540439309911" Z="2.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.1" />
                  <Point X="-4.959212401833" Y="-1.081474035436" Z="2.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.1" />
                  <Point X="-3.500316472582" Y="-1.343878399096" Z="2.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.1" />
                  <Point X="-3.214584595577" Y="-1.309555537337" Z="2.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.1" />
                  <Point X="-3.194007090596" Y="-1.32758801648" Z="2.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.1" />
                  <Point X="-4.277918525623" Y="-2.179020911138" Z="2.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.1" />
                  <Point X="-4.452196936742" Y="-2.436678025997" Z="2.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.1" />
                  <Point X="-4.149398110196" Y="-2.922657939097" Z="2.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.1" />
                  <Point X="-2.795555306281" Y="-2.684076000389" Z="2.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.1" />
                  <Point X="-2.569842916196" Y="-2.558487552935" Z="2.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.1" />
                  <Point X="-3.171341012779" Y="-3.639523076211" Z="2.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.1" />
                  <Point X="-3.229202334464" Y="-3.916693783613" Z="2.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.1" />
                  <Point X="-2.814585978039" Y="-4.224491069983" Z="2.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.1" />
                  <Point X="-2.265068097814" Y="-4.20707702098" Z="2.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.1" />
                  <Point X="-2.181664190908" Y="-4.126679374084" Z="2.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.1" />
                  <Point X="-1.914781179008" Y="-3.987589296289" Z="2.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.1" />
                  <Point X="-1.618376087668" Y="-4.039710032219" Z="2.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.1" />
                  <Point X="-1.41495117461" Y="-4.26150030873" Z="2.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.1" />
                  <Point X="-1.404770001256" Y="-4.816237867206" Z="2.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.1" />
                  <Point X="-1.362023763675" Y="-4.89264447744" Z="2.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.1" />
                  <Point X="-1.065629325632" Y="-4.965632799513" Z="2.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.1" />
                  <Point X="-0.486278520712" Y="-3.777000188912" Z="2.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.1" />
                  <Point X="-0.388806367091" Y="-3.47802639547" Z="2.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.1" />
                  <Point X="-0.209596787669" Y="-3.269290476488" Z="2.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.1" />
                  <Point X="0.043762291691" Y="-3.217821568801" Z="2.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.1" />
                  <Point X="0.281639492341" Y="-3.323619360298" Z="2.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.1" />
                  <Point X="0.748476339689" Y="-4.755535840904" Z="2.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.1" />
                  <Point X="0.848818267056" Y="-5.008104098836" Z="2.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.1" />
                  <Point X="1.028935925896" Y="-4.974222466691" Z="2.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.1" />
                  <Point X="0.995295440787" Y="-3.561169923798" Z="2.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.1" />
                  <Point X="0.966641026412" Y="-3.230148286745" Z="2.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.1" />
                  <Point X="1.042248306775" Y="-2.999477119276" Z="2.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.1" />
                  <Point X="1.231404429422" Y="-2.871970615598" Z="2.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.1" />
                  <Point X="1.461042913217" Y="-2.877893597631" Z="2.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.1" />
                  <Point X="2.485053195189" Y="-4.09598827818" Z="2.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.1" />
                  <Point X="2.695767889446" Y="-4.304823524569" Z="2.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.1" />
                  <Point X="2.889960520187" Y="-4.176936482978" Z="2.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.1" />
                  <Point X="2.405148977849" Y="-2.95424105368" Z="2.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.1" />
                  <Point X="2.26449603794" Y="-2.684973680299" Z="2.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.1" />
                  <Point X="2.24853032776" Y="-2.475200339007" Z="2.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.1" />
                  <Point X="2.357698039365" Y="-2.310370982207" Z="2.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.1" />
                  <Point X="2.543533123214" Y="-2.238951971551" Z="2.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.1" />
                  <Point X="3.833172543427" Y="-2.912600522426" Z="2.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.1" />
                  <Point X="4.095274599029" Y="-3.00365993222" Z="2.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.1" />
                  <Point X="4.267270053721" Y="-2.753843219999" Z="2.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.1" />
                  <Point X="3.401133638641" Y="-1.774496946131" Z="2.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.1" />
                  <Point X="3.175387026611" Y="-1.587597225949" Z="2.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.1" />
                  <Point X="3.094979341901" Y="-1.428777997715" Z="2.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.1" />
                  <Point X="3.126947367706" Y="-1.264574155744" Z="2.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.1" />
                  <Point X="3.24909683337" Y="-1.148567673675" Z="2.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.1" />
                  <Point X="4.646583042869" Y="-1.280128358289" Z="2.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.1" />
                  <Point X="4.921590478513" Y="-1.250505856123" Z="2.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.1" />
                  <Point X="5.001210792939" Y="-0.879604490234" Z="2.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.1" />
                  <Point X="3.972510315267" Y="-0.280980665871" Z="2.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.1" />
                  <Point X="3.731973604075" Y="-0.211574433211" Z="2.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.1" />
                  <Point X="3.64585525734" Y="-0.155121533692" Z="2.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.1" />
                  <Point X="3.596740904592" Y="-0.079923155111" Z="2.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.1" />
                  <Point X="3.584630545832" Y="0.016687376075" Z="2.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.1" />
                  <Point X="3.609524181061" Y="0.108827204883" Z="2.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.1" />
                  <Point X="3.671421810277" Y="0.176574411031" Z="2.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.1" />
                  <Point X="4.823457502039" Y="0.508991266898" Z="2.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.1" />
                  <Point X="5.036631934708" Y="0.642273621697" Z="2.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.1" />
                  <Point X="4.964610768197" Y="1.064334012829" Z="2.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.1" />
                  <Point X="3.707992163952" Y="1.254261938854" Z="2.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.1" />
                  <Point X="3.446857176896" Y="1.224173600226" Z="2.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.1" />
                  <Point X="3.356931460768" Y="1.241239953466" Z="2.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.1" />
                  <Point X="3.291017650405" Y="1.286288036579" Z="2.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.1" />
                  <Point X="3.248209065412" Y="1.361507615027" Z="2.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.1" />
                  <Point X="3.237309829841" Y="1.445643154462" Z="2.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.1" />
                  <Point X="3.265096604377" Y="1.522334205912" Z="2.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.1" />
                  <Point X="4.251366486379" Y="2.304807150941" Z="2.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.1" />
                  <Point X="4.411189563921" Y="2.514853837751" Z="2.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.1" />
                  <Point X="4.197433420696" Y="2.857381420894" Z="2.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.1" />
                  <Point X="2.767654791231" Y="2.415826305272" Z="2.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.1" />
                  <Point X="2.496010207451" Y="2.263290360813" Z="2.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.1" />
                  <Point X="2.417600014005" Y="2.246975144688" Z="2.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.1" />
                  <Point X="2.349231579087" Y="2.26132051302" Z="2.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.1" />
                  <Point X="2.289438087215" Y="2.307793281295" Z="2.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.1" />
                  <Point X="2.252454558071" Y="2.372158429828" Z="2.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.1" />
                  <Point X="2.249237581844" Y="2.443459368431" Z="2.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.1" />
                  <Point X="2.979799047587" Y="3.744484290724" Z="2.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.1" />
                  <Point X="3.063831221654" Y="4.048340044514" Z="2.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.1" />
                  <Point X="2.684797714503" Y="4.309056853089" Z="2.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.1" />
                  <Point X="2.284645027228" Y="4.534012769105" Z="2.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.1" />
                  <Point X="1.870225516819" Y="4.72007632665" Z="2.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.1" />
                  <Point X="1.403742345732" Y="4.875569545869" Z="2.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.1" />
                  <Point X="0.746949153218" Y="5.018335373451" Z="2.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.1" />
                  <Point X="0.033378811179" Y="4.479695899146" Z="2.1" />
                  <Point X="0" Y="4.355124473572" Z="2.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>