<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#129" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="672" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.99528515625" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.610815063477" Y="-28.689845703125" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542361694336" Y="-28.467373046875" />
                  <Point X="0.522941894531" Y="-28.43939453125" />
                  <Point X="0.378634155273" Y="-28.23147265625" />
                  <Point X="0.356752044678" Y="-28.20901953125" />
                  <Point X="0.330496551514" Y="-28.189775390625" />
                  <Point X="0.302493804932" Y="-28.17566796875" />
                  <Point X="0.272442962646" Y="-28.166341796875" />
                  <Point X="0.049134822845" Y="-28.09703515625" />
                  <Point X="0.020976791382" Y="-28.092765625" />
                  <Point X="-0.00866468811" Y="-28.092765625" />
                  <Point X="-0.036822719574" Y="-28.09703515625" />
                  <Point X="-0.066873733521" Y="-28.106361328125" />
                  <Point X="-0.290181854248" Y="-28.17566796875" />
                  <Point X="-0.318185058594" Y="-28.18977734375" />
                  <Point X="-0.344440246582" Y="-28.209021484375" />
                  <Point X="-0.366323730469" Y="-28.2314765625" />
                  <Point X="-0.385743499756" Y="-28.25945703125" />
                  <Point X="-0.530051391602" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.904058227539" Y="-29.830193359375" />
                  <Point X="-0.916584472656" Y="-29.87694140625" />
                  <Point X="-1.079328369141" Y="-29.8453515625" />
                  <Point X="-1.110188354492" Y="-29.837412109375" />
                  <Point X="-1.246417602539" Y="-29.802361328125" />
                  <Point X="-1.220682617188" Y="-29.606884765625" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.223687866211" Y="-29.4801328125" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.32364453125" Y="-29.131205078125" />
                  <Point X="-1.351311523438" Y="-29.10694140625" />
                  <Point X="-1.556905273438" Y="-28.926640625" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.643028808594" Y="-28.890966796875" />
                  <Point X="-1.679749267578" Y="-28.888560546875" />
                  <Point X="-1.952617919922" Y="-28.87067578125" />
                  <Point X="-1.983414550781" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.042655273438" Y="-28.894798828125" />
                  <Point X="-2.073252929688" Y="-28.9152421875" />
                  <Point X="-2.300621826172" Y="-29.067166015625" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.480148193359" Y="-29.288490234375" />
                  <Point X="-2.801707275391" Y="-29.089390625" />
                  <Point X="-2.844421875" Y="-29.0565" />
                  <Point X="-3.104721679688" Y="-28.856078125" />
                  <Point X="-2.512058837891" Y="-27.829556640625" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412857666016" Y="-27.647646484375" />
                  <Point X="-2.406587646484" Y="-27.61611328125" />
                  <Point X="-2.405751953125" Y="-27.58391796875" />
                  <Point X="-2.415720458984" Y="-27.553294921875" />
                  <Point X="-2.431993896484" Y="-27.522416015625" />
                  <Point X="-2.448861328125" Y="-27.499533203125" />
                  <Point X="-2.464152099609" Y="-27.4842421875" />
                  <Point X="-2.489311035156" Y="-27.466212890625" />
                  <Point X="-2.518140380859" Y="-27.45199609375" />
                  <Point X="-2.547758300781" Y="-27.44301171875" />
                  <Point X="-2.578692138672" Y="-27.444025390625" />
                  <Point X="-2.61021875" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-3.775647216797" Y="-28.1173359375" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.082864257812" Y="-27.79385546875" />
                  <Point X="-4.113480957031" Y="-27.742515625" />
                  <Point X="-4.306143066406" Y="-27.419451171875" />
                  <Point X="-3.260446777344" Y="-26.617060546875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083064941406" Y="-26.4758828125" />
                  <Point X="-3.064640625" Y="-26.4471328125" />
                  <Point X="-3.057013183594" Y="-26.42853515625" />
                  <Point X="-3.052942871094" Y="-26.416306640625" />
                  <Point X="-3.046152099609" Y="-26.390087890625" />
                  <Point X="-3.043347167969" Y="-26.3596640625" />
                  <Point X="-3.045553955078" Y="-26.327998046875" />
                  <Point X="-3.053162597656" Y="-26.296814453125" />
                  <Point X="-3.070721191406" Y="-26.2699453125" />
                  <Point X="-3.09469921875" Y="-26.2441328125" />
                  <Point X="-3.116119628906" Y="-26.2269140625" />
                  <Point X="-3.139461181641" Y="-26.213177734375" />
                  <Point X="-3.168722412109" Y="-26.20195703125" />
                  <Point X="-3.200607421875" Y="-26.1954765625" />
                  <Point X="-3.231928710938" Y="-26.194384765625" />
                  <Point X="-4.666605957031" Y="-26.383263671875" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.842178710938" Y="-25.936013671875" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.708865722656" Y="-25.26756640625" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.514703857422" Y="-25.213478515625" />
                  <Point X="-3.495844970703" Y="-25.20397265625" />
                  <Point X="-3.484439697266" Y="-25.197185546875" />
                  <Point X="-3.459978515625" Y="-25.180208984375" />
                  <Point X="-3.436022949219" Y="-25.15868359375" />
                  <Point X="-3.416229003906" Y="-25.130802734375" />
                  <Point X="-3.407706054688" Y="-25.112529296875" />
                  <Point X="-3.403071289062" Y="-25.100533203125" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.390699951172" Y="-25.0429609375" />
                  <Point X="-3.391797119141" Y="-25.009783203125" />
                  <Point X="-3.396014648438" Y="-24.984763671875" />
                  <Point X="-3.404168457031" Y="-24.9584921875" />
                  <Point X="-3.417491943359" Y="-24.92915625" />
                  <Point X="-3.438275390625" Y="-24.90175" />
                  <Point X="-3.453393554688" Y="-24.887947265625" />
                  <Point X="-3.463278076172" Y="-24.8800625" />
                  <Point X="-3.487725585938" Y="-24.86309375" />
                  <Point X="-3.501916015625" Y="-24.854958984375" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.84065234375" Y="-24.491732421875" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.824489257812" Y="-24.02303125" />
                  <Point X="-4.808178710938" Y="-23.962841796875" />
                  <Point X="-4.703550292969" Y="-23.576732421875" />
                  <Point X="-3.891820068359" Y="-23.683599609375" />
                  <Point X="-3.765666015625" Y="-23.70020703125" />
                  <Point X="-3.744984375" Y="-23.700658203125" />
                  <Point X="-3.723423339844" Y="-23.698771484375" />
                  <Point X="-3.703140869141" Y="-23.694736328125" />
                  <Point X="-3.695855224609" Y="-23.692439453125" />
                  <Point X="-3.64171484375" Y="-23.675369140625" />
                  <Point X="-3.62278515625" Y="-23.667041015625" />
                  <Point X="-3.604040527344" Y="-23.656220703125" />
                  <Point X="-3.587354980469" Y="-23.64398828125" />
                  <Point X="-3.573713378906" Y="-23.62843359375" />
                  <Point X="-3.561299072266" Y="-23.610703125" />
                  <Point X="-3.551354003906" Y="-23.592576171875" />
                  <Point X="-3.548430419922" Y="-23.58551953125" />
                  <Point X="-3.526706542969" Y="-23.533072265625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.532051513672" Y="-23.410619140625" />
                  <Point X="-3.535578857422" Y="-23.40384375" />
                  <Point X="-3.561791259766" Y="-23.353490234375" />
                  <Point X="-3.573281494141" Y="-23.336294921875" />
                  <Point X="-3.587193847656" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.081153808594" Y="-22.26634375" />
                  <Point X="-4.037955078125" Y="-22.21081640625" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.284136962891" Y="-22.110595703125" />
                  <Point X="-3.206656738281" Y="-22.155328125" />
                  <Point X="-3.187727294922" Y="-22.163658203125" />
                  <Point X="-3.16708984375" Y="-22.170166015625" />
                  <Point X="-3.146821289063" Y="-22.174201171875" />
                  <Point X="-3.136645263672" Y="-22.17509375" />
                  <Point X="-3.061242919922" Y="-22.181689453125" />
                  <Point X="-3.040559570312" Y="-22.18123828125" />
                  <Point X="-3.0191015625" Y="-22.178412109375" />
                  <Point X="-2.999012207031" Y="-22.173494140625" />
                  <Point X="-2.980462890625" Y="-22.16434765625" />
                  <Point X="-2.962209228516" Y="-22.15271875" />
                  <Point X="-2.946074462891" Y="-22.139767578125" />
                  <Point X="-2.938872070312" Y="-22.132564453125" />
                  <Point X="-2.885350830078" Y="-22.079044921875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435546875" Y="-21.9638828125" />
                  <Point X="-2.844323242188" Y="-21.953734375" />
                  <Point X="-2.850920166016" Y="-21.87833203125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.183333251953" Y="-21.275404296875" />
                  <Point X="-2.700623291016" Y="-20.905314453125" />
                  <Point X="-2.632593017578" Y="-20.86751953125" />
                  <Point X="-2.167036865234" Y="-20.6088671875" />
                  <Point X="-2.066913818359" Y="-20.739349609375" />
                  <Point X="-2.043194824219" Y="-20.770259765625" />
                  <Point X="-2.028893676758" Y="-20.78519921875" />
                  <Point X="-2.012319702148" Y="-20.799107421875" />
                  <Point X="-1.995123291016" Y="-20.810599609375" />
                  <Point X="-1.983823974609" Y="-20.816482421875" />
                  <Point X="-1.899901489258" Y="-20.860169921875" />
                  <Point X="-1.88062512207" Y="-20.86766796875" />
                  <Point X="-1.859718383789" Y="-20.873271484375" />
                  <Point X="-1.839268432617" Y="-20.876419921875" />
                  <Point X="-1.818621826172" Y="-20.87506640625" />
                  <Point X="-1.797306762695" Y="-20.871306640625" />
                  <Point X="-1.777449951172" Y="-20.865517578125" />
                  <Point X="-1.765686889648" Y="-20.86064453125" />
                  <Point X="-1.678276000977" Y="-20.8244375" />
                  <Point X="-1.660146118164" Y="-20.814490234375" />
                  <Point X="-1.642416381836" Y="-20.802076171875" />
                  <Point X="-1.626864501953" Y="-20.7884375" />
                  <Point X="-1.614633056641" Y="-20.771755859375" />
                  <Point X="-1.603810913086" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734080078125" />
                  <Point X="-1.591651855469" Y="-20.7219375" />
                  <Point X="-1.563201171875" Y="-20.631703125" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584202026367" Y="-20.3681015625" />
                  <Point X="-0.949624633789" Y="-20.19019140625" />
                  <Point X="-0.867164550781" Y="-20.1805390625" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.156824111938" Y="-20.62814453125" />
                  <Point X="-0.133903167725" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082114044189" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155899525" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.09442590332" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441650391" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.307418823242" Y="-20.112064453125" />
                  <Point X="0.844044921875" Y="-20.168263671875" />
                  <Point X="0.91227746582" Y="-20.184736328125" />
                  <Point X="1.481037963867" Y="-20.3220546875" />
                  <Point X="1.524133789062" Y="-20.337685546875" />
                  <Point X="1.894652099609" Y="-20.47207421875" />
                  <Point X="1.93759753418" Y="-20.492158203125" />
                  <Point X="2.294561035156" Y="-20.659099609375" />
                  <Point X="2.336103027344" Y="-20.68330078125" />
                  <Point X="2.680977783203" Y="-20.884224609375" />
                  <Point X="2.72011328125" Y="-20.912056640625" />
                  <Point X="2.943260009766" Y="-21.07074609375" />
                  <Point X="2.249778808594" Y="-22.271890625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.140168212891" Y="-22.465072265625" />
                  <Point X="2.132620117188" Y="-22.4866796875" />
                  <Point X="2.130530273438" Y="-22.49346875" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108389892578" Y="-22.58756640625" />
                  <Point X="2.108045166016" Y="-22.614703125" />
                  <Point X="2.108720703125" Y="-22.627283203125" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140068847656" Y="-22.752525390625" />
                  <Point X="2.1451640625" Y="-22.76003515625" />
                  <Point X="2.183026611328" Y="-22.815833984375" />
                  <Point X="2.198716552734" Y="-22.833669921875" />
                  <Point X="2.219524902344" Y="-22.85206640625" />
                  <Point X="2.229106445312" Y="-22.85950390625" />
                  <Point X="2.284906005859" Y="-22.8973671875" />
                  <Point X="2.304953613281" Y="-22.90773046875" />
                  <Point X="2.327042236328" Y="-22.91599609375" />
                  <Point X="2.348972412109" Y="-22.92133984375" />
                  <Point X="2.35720703125" Y="-22.92233203125" />
                  <Point X="2.418397216797" Y="-22.9297109375" />
                  <Point X="2.442624755859" Y="-22.929517578125" />
                  <Point X="2.471051025391" Y="-22.9256328125" />
                  <Point X="2.48273046875" Y="-22.923283203125" />
                  <Point X="2.553493896484" Y="-22.904359375" />
                  <Point X="2.565288085938" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.903905273438" Y="-22.130423828125" />
                  <Point X="3.967325683594" Y="-22.09380859375" />
                  <Point X="4.123271484375" Y="-22.310537109375" />
                  <Point X="4.14508984375" Y="-22.34659375" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.365336914062" Y="-23.228302734375" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.21747265625" Y="-23.3439609375" />
                  <Point X="3.201374511719" Y="-23.36215234375" />
                  <Point X="3.197120849609" Y="-23.367314453125" />
                  <Point X="3.146192138672" Y="-23.43375390625" />
                  <Point X="3.134083984375" Y="-23.454564453125" />
                  <Point X="3.123061279297" Y="-23.48064453125" />
                  <Point X="3.119076904297" Y="-23.49204296875" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739746094" Y="-23.628234375" />
                  <Point X="3.099835449219" Y="-23.638390625" />
                  <Point X="3.115408691406" Y="-23.7138671875" />
                  <Point X="3.123317626953" Y="-23.73683203125" />
                  <Point X="3.136216064453" Y="-23.762875" />
                  <Point X="3.141983154297" Y="-23.77292578125" />
                  <Point X="3.184340576172" Y="-23.837306640625" />
                  <Point X="3.198896972656" Y="-23.8545546875" />
                  <Point X="3.216140380859" Y="-23.870642578125" />
                  <Point X="3.234342529297" Y="-23.883962890625" />
                  <Point X="3.242602783203" Y="-23.88861328125" />
                  <Point X="3.303984619141" Y="-23.923166015625" />
                  <Point X="3.326999267578" Y="-23.93240625" />
                  <Point X="3.356149414062" Y="-23.93987890625" />
                  <Point X="3.367291503906" Y="-23.942037109375" />
                  <Point X="3.450277587891" Y="-23.953005859375" />
                  <Point X="3.462697753906" Y="-23.95382421875" />
                  <Point X="3.488203369141" Y="-23.953015625" />
                  <Point X="4.737718261719" Y="-23.788513671875" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.852811523438" Y="-24.1113515625" />
                  <Point X="4.890864257812" Y="-24.355759765625" />
                  <Point X="3.870780273438" Y="-24.629091796875" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.699205810547" Y="-24.676943359375" />
                  <Point X="3.676153076172" Y="-24.68829296875" />
                  <Point X="3.670574462891" Y="-24.6912734375" />
                  <Point X="3.589037109375" Y="-24.73840234375" />
                  <Point X="3.569538330078" Y="-24.753341796875" />
                  <Point X="3.548639404297" Y="-24.77415625" />
                  <Point X="3.540944824219" Y="-24.78281640625" />
                  <Point X="3.492025634766" Y="-24.845150390625" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.470526855469" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.461486328125" Y="-24.91885546875" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443731689453" Y="-25.028748046875" />
                  <Point X="3.445926513672" Y="-25.059015625" />
                  <Point X="3.447373535156" Y="-25.070013671875" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.47052734375" Y="-25.1766640625" />
                  <Point X="3.48030078125" Y="-25.198126953125" />
                  <Point X="3.492025634766" Y="-25.217408203125" />
                  <Point X="3.498609130859" Y="-25.225796875" />
                  <Point X="3.547531494141" Y="-25.28813671875" />
                  <Point X="3.565651367188" Y="-25.305775390625" />
                  <Point X="3.590936035156" Y="-25.3245390625" />
                  <Point X="3.600007568359" Y="-25.330498046875" />
                  <Point X="3.681544921875" Y="-25.37762890625" />
                  <Point X="3.692708251953" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.86244140625" Y="-25.69918359375" />
                  <Point X="4.89147265625" Y="-25.706962890625" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.846213867188" Y="-25.987328125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.603211425781" Y="-26.026984375" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035400391" Y="-26.0027109375" />
                  <Point X="3.374663085938" Y="-26.0055078125" />
                  <Point X="3.353127685547" Y="-26.0101875" />
                  <Point X="3.193098876953" Y="-26.044970703125" />
                  <Point X="3.163972900391" Y="-26.05659765625" />
                  <Point X="3.136146728516" Y="-26.073490234375" />
                  <Point X="3.112395751953" Y="-26.093962890625" />
                  <Point X="3.09937890625" Y="-26.109619140625" />
                  <Point X="3.002651611328" Y="-26.225951171875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.967891845703" Y="-26.325640625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976451171875" Y="-26.567998046875" />
                  <Point X="2.988369140625" Y="-26.58653515625" />
                  <Point X="3.076931396484" Y="-26.724287109375" />
                  <Point X="3.086931640625" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="4.173994628906" Y="-27.576861328125" />
                  <Point X="4.213122558594" Y="-27.606884765625" />
                  <Point X="4.124819335938" Y="-27.7497734375" />
                  <Point X="4.106591308594" Y="-27.775671875" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="2.960226318359" Y="-27.268900390625" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224853516" Y="-27.159826171875" />
                  <Point X="2.728594238281" Y="-27.155197265625" />
                  <Point X="2.538134521484" Y="-27.12080078125" />
                  <Point X="2.506784179688" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444836669922" Y="-27.13517578125" />
                  <Point X="2.423543945312" Y="-27.146380859375" />
                  <Point X="2.265318603516" Y="-27.229654296875" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.193325439453" Y="-27.311732421875" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.100303955078" Y="-27.588888671875" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.835139160156" Y="-29.009623046875" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.781837158203" Y="-29.11165234375" />
                  <Point X="2.761482666016" Y="-29.124828125" />
                  <Point X="2.701764404297" Y="-29.163482421875" />
                  <Point X="1.880085205078" Y="-28.0926484375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721924072266" Y="-27.900556640625" />
                  <Point X="1.696645507812" Y="-27.8843046875" />
                  <Point X="1.508800537109" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365722656" Y="-27.743435546875" />
                  <Point X="1.417103393555" Y="-27.7411171875" />
                  <Point X="1.38945690918" Y="-27.74366015625" />
                  <Point X="1.184016601562" Y="-27.762564453125" />
                  <Point X="1.156362548828" Y="-27.7693984375" />
                  <Point X="1.128976928711" Y="-27.7807421875" />
                  <Point X="1.104595825195" Y="-27.7954609375" />
                  <Point X="1.083247802734" Y="-27.8132109375" />
                  <Point X="0.924612121582" Y="-27.94511328125" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.869241577148" Y="-28.05517578125" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="1.013389953613" Y="-29.79401953125" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.975690063477" Y="-29.870080078125" />
                  <Point X="0.956852539062" Y="-29.87350390625" />
                  <Point X="0.929316040039" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058430297852" Y="-29.752634765625" />
                  <Point X="-1.086518188477" Y="-29.745408203125" />
                  <Point X="-1.14124597168" Y="-29.731326171875" />
                  <Point X="-1.126495361328" Y="-29.61928515625" />
                  <Point X="-1.120775756836" Y="-29.575841796875" />
                  <Point X="-1.120077392578" Y="-29.5681015625" />
                  <Point X="-1.119451660156" Y="-29.541037109375" />
                  <Point X="-1.121759033203" Y="-29.50933203125" />
                  <Point X="-1.123333862305" Y="-29.497693359375" />
                  <Point X="-1.130513183594" Y="-29.461599609375" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026611328" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230575195312" Y="-29.094859375" />
                  <Point X="-1.250209716797" Y="-29.070935546875" />
                  <Point X="-1.261006103516" Y="-29.05978125" />
                  <Point X="-1.288673217773" Y="-29.035517578125" />
                  <Point X="-1.494266845703" Y="-28.855216796875" />
                  <Point X="-1.506735717773" Y="-28.84596875" />
                  <Point X="-1.53301953125" Y="-28.829623046875" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576531982422" Y="-28.810224609375" />
                  <Point X="-1.591316650391" Y="-28.805474609375" />
                  <Point X="-1.621458984375" Y="-28.798447265625" />
                  <Point X="-1.636816894531" Y="-28.796169921875" />
                  <Point X="-1.673537353516" Y="-28.793763671875" />
                  <Point X="-1.946405883789" Y="-28.77587890625" />
                  <Point X="-1.96192956543" Y="-28.7761328125" />
                  <Point X="-1.992726196289" Y="-28.779166015625" />
                  <Point X="-2.007999267578" Y="-28.7819453125" />
                  <Point X="-2.039047973633" Y="-28.790263671875" />
                  <Point X="-2.053665039062" Y="-28.7954921875" />
                  <Point X="-2.081856933594" Y="-28.808263671875" />
                  <Point X="-2.095432128906" Y="-28.815806640625" />
                  <Point X="-2.126029785156" Y="-28.83625" />
                  <Point X="-2.353398681641" Y="-28.988173828125" />
                  <Point X="-2.359681884766" Y="-28.992755859375" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.503201660156" Y="-29.16248046875" />
                  <Point X="-2.747583007812" Y="-29.011166015625" />
                  <Point X="-2.786462646484" Y="-28.981228515625" />
                  <Point X="-2.980862304688" Y="-28.831546875" />
                  <Point X="-2.429786376953" Y="-27.877056640625" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849121094" Y="-27.710080078125" />
                  <Point X="-2.323945556641" Y="-27.681107421875" />
                  <Point X="-2.319681640625" Y="-27.666173828125" />
                  <Point X="-2.313411621094" Y="-27.634640625" />
                  <Point X="-2.311619628906" Y="-27.618578125" />
                  <Point X="-2.310783935547" Y="-27.5863828125" />
                  <Point X="-2.315417724609" Y="-27.55451171875" />
                  <Point X="-2.325386230469" Y="-27.523888671875" />
                  <Point X="-2.331677246094" Y="-27.50900390625" />
                  <Point X="-2.347950683594" Y="-27.478125" />
                  <Point X="-2.355523925781" Y="-27.466048828125" />
                  <Point X="-2.372391357422" Y="-27.443166015625" />
                  <Point X="-2.381685546875" Y="-27.432359375" />
                  <Point X="-2.396976318359" Y="-27.417068359375" />
                  <Point X="-2.408815429688" Y="-27.4070234375" />
                  <Point X="-2.433974365234" Y="-27.388994140625" />
                  <Point X="-2.447294189453" Y="-27.381009765625" />
                  <Point X="-2.476123535156" Y="-27.36679296875" />
                  <Point X="-2.490563720703" Y="-27.3610859375" />
                  <Point X="-2.520181640625" Y="-27.3521015625" />
                  <Point X="-2.550869628906" Y="-27.3480625" />
                  <Point X="-2.581803466797" Y="-27.349076171875" />
                  <Point X="-2.597227050781" Y="-27.3508515625" />
                  <Point X="-2.628753662109" Y="-27.357123046875" />
                  <Point X="-2.643684326172" Y="-27.36138671875" />
                  <Point X="-2.672649414062" Y="-27.3722890625" />
                  <Point X="-2.686683837891" Y="-27.378927734375" />
                  <Point X="-3.793087646484" Y="-28.017708984375" />
                  <Point X="-4.004022949219" Y="-27.740583984375" />
                  <Point X="-4.031888183594" Y="-27.693857421875" />
                  <Point X="-4.181265625" Y="-27.443375" />
                  <Point X="-3.202614501953" Y="-26.6924296875" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039159423828" Y="-26.566068359375" />
                  <Point X="-3.016269775391" Y="-26.543435546875" />
                  <Point X="-3.003079833984" Y="-26.527140625" />
                  <Point X="-2.984655517578" Y="-26.498390625" />
                  <Point X="-2.976745605469" Y="-26.483181640625" />
                  <Point X="-2.969118164062" Y="-26.464583984375" />
                  <Point X="-2.960977539062" Y="-26.440126953125" />
                  <Point X="-2.954186767578" Y="-26.413908203125" />
                  <Point X="-2.951553222656" Y="-26.39880859375" />
                  <Point X="-2.948748291016" Y="-26.368384765625" />
                  <Point X="-2.948576904297" Y="-26.353060546875" />
                  <Point X="-2.950783691406" Y="-26.32139453125" />
                  <Point X="-2.953261474609" Y="-26.305478515625" />
                  <Point X="-2.960870117188" Y="-26.274294921875" />
                  <Point X="-2.973637451172" Y="-26.244845703125" />
                  <Point X="-2.991196044922" Y="-26.2179765625" />
                  <Point X="-3.001118164062" Y="-26.2052890625" />
                  <Point X="-3.025096191406" Y="-26.1794765625" />
                  <Point X="-3.0351796875" Y="-26.17008984375" />
                  <Point X="-3.056600097656" Y="-26.15287109375" />
                  <Point X="-3.067937011719" Y="-26.1450390625" />
                  <Point X="-3.091278564453" Y="-26.131302734375" />
                  <Point X="-3.105447021484" Y="-26.1244765625" />
                  <Point X="-3.134708251953" Y="-26.113255859375" />
                  <Point X="-3.149801025391" Y="-26.108861328125" />
                  <Point X="-3.181686035156" Y="-26.102380859375" />
                  <Point X="-3.197297851562" Y="-26.10053515625" />
                  <Point X="-3.228619140625" Y="-26.099443359375" />
                  <Point X="-3.244328613281" Y="-26.100197265625" />
                  <Point X="-4.660919921875" Y="-26.2866953125" />
                  <Point X="-4.740762695312" Y="-25.97411328125" />
                  <Point X="-4.748135742188" Y="-25.9225625" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.684277832031" Y="-25.359330078125" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.499018310547" Y="-25.309171875" />
                  <Point X="-3.471943847656" Y="-25.298310546875" />
                  <Point X="-3.453084960938" Y="-25.2888046875" />
                  <Point X="-3.430274414062" Y="-25.27523046875" />
                  <Point X="-3.405813232422" Y="-25.25825390625" />
                  <Point X="-3.396483398438" Y="-25.250873046875" />
                  <Point X="-3.372527832031" Y="-25.22934765625" />
                  <Point X="-3.358559570312" Y="-25.213677734375" />
                  <Point X="-3.338765625" Y="-25.185796875" />
                  <Point X="-3.330133300781" Y="-25.170958984375" />
                  <Point X="-3.321610351562" Y="-25.152685546875" />
                  <Point X="-3.312340820312" Y="-25.128693359375" />
                  <Point X="-3.304187011719" Y="-25.102421875" />
                  <Point X="-3.300768310547" Y="-25.086947265625" />
                  <Point X="-3.29655078125" Y="-25.055646484375" />
                  <Point X="-3.295751953125" Y="-25.0398203125" />
                  <Point X="-3.296849121094" Y="-25.006642578125" />
                  <Point X="-3.298118652344" Y="-24.9939921875" />
                  <Point X="-3.302336181641" Y="-24.96897265625" />
                  <Point X="-3.305284179688" Y="-24.956603515625" />
                  <Point X="-3.313437988281" Y="-24.93033203125" />
                  <Point X="-3.317671386719" Y="-24.91920703125" />
                  <Point X="-3.330994873047" Y="-24.88987109375" />
                  <Point X="-3.341796386719" Y="-24.871751953125" />
                  <Point X="-3.362579833984" Y="-24.844345703125" />
                  <Point X="-3.374221923828" Y="-24.831591796875" />
                  <Point X="-3.389340087891" Y="-24.8177890625" />
                  <Point X="-3.409109130859" Y="-24.80201953125" />
                  <Point X="-3.433556640625" Y="-24.78505078125" />
                  <Point X="-3.440478759766" Y="-24.78067578125" />
                  <Point X="-3.465598388672" Y="-24.76717578125" />
                  <Point X="-3.496558349609" Y="-24.7543671875" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.73133203125" Y="-24.0424765625" />
                  <Point X="-4.716485839844" Y="-23.987689453125" />
                  <Point X="-4.633585449219" Y="-23.681763671875" />
                  <Point X="-3.904220214844" Y="-23.777787109375" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.767738037109" Y="-23.79518359375" />
                  <Point X="-3.747056396484" Y="-23.795634765625" />
                  <Point X="-3.736702880859" Y="-23.795296875" />
                  <Point X="-3.715141845703" Y="-23.79341015625" />
                  <Point X="-3.704886474609" Y="-23.7919453125" />
                  <Point X="-3.684604003906" Y="-23.78791015625" />
                  <Point X="-3.667291259766" Y="-23.78304296875" />
                  <Point X="-3.613150878906" Y="-23.76597265625" />
                  <Point X="-3.603458251953" Y="-23.762326171875" />
                  <Point X="-3.584528564453" Y="-23.753998046875" />
                  <Point X="-3.575291503906" Y="-23.74931640625" />
                  <Point X="-3.556546875" Y="-23.73849609375" />
                  <Point X="-3.547871826172" Y="-23.732837890625" />
                  <Point X="-3.531186279297" Y="-23.72060546875" />
                  <Point X="-3.515931396484" Y="-23.706626953125" />
                  <Point X="-3.502289794922" Y="-23.691072265625" />
                  <Point X="-3.495892578125" Y="-23.682921875" />
                  <Point X="-3.483478271484" Y="-23.66519140625" />
                  <Point X="-3.478010498047" Y="-23.6563984375" />
                  <Point X="-3.468065429688" Y="-23.638271484375" />
                  <Point X="-3.460664550781" Y="-23.621880859375" />
                  <Point X="-3.438940673828" Y="-23.56943359375" />
                  <Point X="-3.435502197266" Y="-23.55965625" />
                  <Point X="-3.429711181641" Y="-23.5397890625" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436012939453" Y="-23.395466796875" />
                  <Point X="-3.443511474609" Y="-23.376185546875" />
                  <Point X="-3.451314453125" Y="-23.359974609375" />
                  <Point X="-3.477526855469" Y="-23.30962109375" />
                  <Point X="-3.482803222656" Y="-23.300708984375" />
                  <Point X="-3.494293457031" Y="-23.283513671875" />
                  <Point X="-3.500507324219" Y="-23.27523046875" />
                  <Point X="-3.514419677734" Y="-23.258650390625" />
                  <Point X="-3.521497802734" Y="-23.251091796875" />
                  <Point X="-3.536439697266" Y="-23.236787109375" />
                  <Point X="-3.544303466797" Y="-23.230041015625" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.002296875" Y="-22.319697265625" />
                  <Point X="-3.962973632812" Y="-22.269150390625" />
                  <Point X="-3.726336425781" Y="-21.964986328125" />
                  <Point X="-3.331636962891" Y="-22.1928671875" />
                  <Point X="-3.254156738281" Y="-22.237599609375" />
                  <Point X="-3.244921142578" Y="-22.24228125" />
                  <Point X="-3.225991699219" Y="-22.250611328125" />
                  <Point X="-3.216297851562" Y="-22.254259765625" />
                  <Point X="-3.195660400391" Y="-22.260767578125" />
                  <Point X="-3.185638916016" Y="-22.263337890625" />
                  <Point X="-3.165370361328" Y="-22.267373046875" />
                  <Point X="-3.144923583984" Y="-22.269732421875" />
                  <Point X="-3.069521240234" Y="-22.276328125" />
                  <Point X="-3.059171142578" Y="-22.276666015625" />
                  <Point X="-3.038487792969" Y="-22.27621484375" />
                  <Point X="-3.028154541016" Y="-22.27542578125" />
                  <Point X="-3.006696533203" Y="-22.272599609375" />
                  <Point X="-2.996512207031" Y="-22.2706875" />
                  <Point X="-2.976422851562" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.93844921875" Y="-22.249552734375" />
                  <Point X="-2.929419189453" Y="-22.244470703125" />
                  <Point X="-2.911165527344" Y="-22.232841796875" />
                  <Point X="-2.902741943359" Y="-22.2268046875" />
                  <Point X="-2.886607177734" Y="-22.213853515625" />
                  <Point X="-2.871693603516" Y="-22.199736328125" />
                  <Point X="-2.818172363281" Y="-22.146216796875" />
                  <Point X="-2.811262207031" Y="-22.1385078125" />
                  <Point X="-2.7983203125" Y="-22.1223828125" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.986634765625" />
                  <Point X="-2.748458251953" Y="-21.96595703125" />
                  <Point X="-2.749684570312" Y="-21.94545703125" />
                  <Point X="-2.756281494141" Y="-21.8700546875" />
                  <Point X="-2.757745361328" Y="-21.85980078125" />
                  <Point X="-2.761780761719" Y="-21.83951171875" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.059387207031" Y="-21.300083984375" />
                  <Point X="-2.648369384766" Y="-20.9849609375" />
                  <Point X="-2.586456542969" Y="-20.950564453125" />
                  <Point X="-2.192524414062" Y="-20.731705078125" />
                  <Point X="-2.142282226562" Y="-20.797181640625" />
                  <Point X="-2.118563232422" Y="-20.828091796875" />
                  <Point X="-2.111820068359" Y="-20.835953125" />
                  <Point X="-2.097518798828" Y="-20.850892578125" />
                  <Point X="-2.0899609375" Y="-20.857970703125" />
                  <Point X="-2.073386962891" Y="-20.87187890625" />
                  <Point X="-2.065104980469" Y="-20.87809375" />
                  <Point X="-2.047908569336" Y="-20.8895859375" />
                  <Point X="-2.027694580078" Y="-20.90074609375" />
                  <Point X="-1.943772094727" Y="-20.94443359375" />
                  <Point X="-1.934340576172" Y="-20.94870703125" />
                  <Point X="-1.915064208984" Y="-20.956205078125" />
                  <Point X="-1.905219360352" Y="-20.9594296875" />
                  <Point X="-1.88431262207" Y="-20.965033203125" />
                  <Point X="-1.874173950195" Y="-20.967166015625" />
                  <Point X="-1.853724121094" Y="-20.970314453125" />
                  <Point X="-1.833053955078" Y="-20.971216796875" />
                  <Point X="-1.812407348633" Y="-20.96986328125" />
                  <Point X="-1.802119506836" Y="-20.968623046875" />
                  <Point X="-1.780804443359" Y="-20.96486328125" />
                  <Point X="-1.770717407227" Y="-20.962509765625" />
                  <Point X="-1.750860595703" Y="-20.956720703125" />
                  <Point X="-1.72932800293" Y="-20.948412109375" />
                  <Point X="-1.641917114258" Y="-20.912205078125" />
                  <Point X="-1.632578979492" Y="-20.907724609375" />
                  <Point X="-1.61444909668" Y="-20.89777734375" />
                  <Point X="-1.605657592773" Y="-20.892310546875" />
                  <Point X="-1.587927978516" Y="-20.879896484375" />
                  <Point X="-1.579778442383" Y="-20.8735" />
                  <Point X="-1.5642265625" Y="-20.859861328125" />
                  <Point X="-1.550252197266" Y="-20.844611328125" />
                  <Point X="-1.538020751953" Y="-20.8279296875" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.52153894043" Y="-20.80051171875" />
                  <Point X="-1.516856689453" Y="-20.7912734375" />
                  <Point X="-1.508526489258" Y="-20.772341796875" />
                  <Point X="-1.501048950195" Y="-20.750505859375" />
                  <Point X="-1.472598388672" Y="-20.660271484375" />
                  <Point X="-1.470026611328" Y="-20.650236328125" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.43734375" />
                  <Point X="-0.931166931152" Y="-20.2836796875" />
                  <Point X="-0.856119689941" Y="-20.27489453125" />
                  <Point X="-0.365222595215" Y="-20.21744140625" />
                  <Point X="-0.248587020874" Y="-20.652732421875" />
                  <Point X="-0.225666091919" Y="-20.7382734375" />
                  <Point X="-0.220435317993" Y="-20.752892578125" />
                  <Point X="-0.207661911011" Y="-20.781083984375" />
                  <Point X="-0.200119155884" Y="-20.79465625" />
                  <Point X="-0.182260925293" Y="-20.8213828125" />
                  <Point X="-0.172608901978" Y="-20.833544921875" />
                  <Point X="-0.151451400757" Y="-20.856134765625" />
                  <Point X="-0.126896362305" Y="-20.8749765625" />
                  <Point X="-0.09960043335" Y="-20.88956640625" />
                  <Point X="-0.085354034424" Y="-20.8957421875" />
                  <Point X="-0.054915912628" Y="-20.90607421875" />
                  <Point X="-0.03985357666" Y="-20.909845703125" />
                  <Point X="-0.009317661285" Y="-20.91488671875" />
                  <Point X="0.021629491806" Y="-20.91488671875" />
                  <Point X="0.052165405273" Y="-20.909845703125" />
                  <Point X="0.067227592468" Y="-20.90607421875" />
                  <Point X="0.097665863037" Y="-20.8957421875" />
                  <Point X="0.111912406921" Y="-20.88956640625" />
                  <Point X="0.139208343506" Y="-20.8749765625" />
                  <Point X="0.163763076782" Y="-20.856134765625" />
                  <Point X="0.18492074585" Y="-20.833544921875" />
                  <Point X="0.194573059082" Y="-20.8213828125" />
                  <Point X="0.212431289673" Y="-20.79465625" />
                  <Point X="0.219973449707" Y="-20.781083984375" />
                  <Point X="0.232746994019" Y="-20.752892578125" />
                  <Point X="0.237978225708" Y="-20.7382734375" />
                  <Point X="0.378190002441" Y="-20.214994140625" />
                  <Point X="0.827868530273" Y="-20.262087890625" />
                  <Point X="0.889983032227" Y="-20.277083984375" />
                  <Point X="1.453619628906" Y="-20.413166015625" />
                  <Point X="1.49174206543" Y="-20.4269921875" />
                  <Point X="1.858253662109" Y="-20.559927734375" />
                  <Point X="1.897353027344" Y="-20.578212890625" />
                  <Point X="2.25044140625" Y="-20.743341796875" />
                  <Point X="2.288281982422" Y="-20.76538671875" />
                  <Point X="2.629434570312" Y="-20.964142578125" />
                  <Point X="2.665055419922" Y="-20.989474609375" />
                  <Point X="2.817780029297" Y="-21.098083984375" />
                  <Point X="2.167506347656" Y="-22.224390625" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.061223144531" Y="-22.4093125" />
                  <Point X="2.050482910156" Y="-22.4337421875" />
                  <Point X="2.042934814453" Y="-22.455349609375" />
                  <Point X="2.038755004883" Y="-22.468927734375" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017497558594" Y="-22.551255859375" />
                  <Point X="2.014280273438" Y="-22.57458984375" />
                  <Point X="2.013397583008" Y="-22.586359375" />
                  <Point X="2.013052856445" Y="-22.61349609375" />
                  <Point X="2.014403930664" Y="-22.63865625" />
                  <Point X="2.021782348633" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.02914453125" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.045319091797" Y="-22.77611328125" />
                  <Point X="2.055678955078" Y="-22.79615234375" />
                  <Point X="2.066550537109" Y="-22.813373046875" />
                  <Point X="2.104413085937" Y="-22.869171875" />
                  <Point X="2.111697509766" Y="-22.878580078125" />
                  <Point X="2.127387451172" Y="-22.896416015625" />
                  <Point X="2.13579296875" Y="-22.90484375" />
                  <Point X="2.156601318359" Y="-22.923240234375" />
                  <Point X="2.175764404297" Y="-22.938115234375" />
                  <Point X="2.231563964844" Y="-22.975978515625" />
                  <Point X="2.24128125" Y="-22.9817578125" />
                  <Point X="2.261328857422" Y="-22.99212109375" />
                  <Point X="2.271659179688" Y="-22.996705078125" />
                  <Point X="2.293747802734" Y="-23.004970703125" />
                  <Point X="2.304551513672" Y="-23.008294921875" />
                  <Point X="2.326481689453" Y="-23.013638671875" />
                  <Point X="2.345842773438" Y="-23.016650390625" />
                  <Point X="2.407032958984" Y="-23.024029296875" />
                  <Point X="2.419155273438" Y="-23.02470703125" />
                  <Point X="2.4433828125" Y="-23.024513671875" />
                  <Point X="2.455488037109" Y="-23.023642578125" />
                  <Point X="2.483914306641" Y="-23.0197578125" />
                  <Point X="2.507273193359" Y="-23.01505859375" />
                  <Point X="2.578036621094" Y="-22.996134765625" />
                  <Point X="2.584006103516" Y="-22.994326171875" />
                  <Point X="2.604412841797" Y="-22.9869296875" />
                  <Point X="2.627658691406" Y="-22.976423828125" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.940403320312" Y="-22.219046875" />
                  <Point X="4.043955078125" Y="-22.3629609375" />
                  <Point X="4.063812011719" Y="-22.395775390625" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.307504638672" Y="-23.15293359375" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.165996582031" Y="-23.262068359375" />
                  <Point X="3.146329101562" Y="-23.28100390625" />
                  <Point X="3.130230957031" Y="-23.2991953125" />
                  <Point X="3.121723632812" Y="-23.30951953125" />
                  <Point X="3.070794921875" Y="-23.375958984375" />
                  <Point X="3.064079345703" Y="-23.385978515625" />
                  <Point X="3.051971191406" Y="-23.4067890625" />
                  <Point X="3.046578613281" Y="-23.417580078125" />
                  <Point X="3.035555908203" Y="-23.44366015625" />
                  <Point X="3.027587158203" Y="-23.46645703125" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003078125" Y="-23.63624609375" />
                  <Point X="3.006795654297" Y="-23.65758984375" />
                  <Point X="3.022368896484" Y="-23.73306640625" />
                  <Point X="3.025586181641" Y="-23.74480078125" />
                  <Point X="3.033495117188" Y="-23.767765625" />
                  <Point X="3.038186767578" Y="-23.77899609375" />
                  <Point X="3.051085205078" Y="-23.8050390625" />
                  <Point X="3.062619384766" Y="-23.825140625" />
                  <Point X="3.104976806641" Y="-23.889521484375" />
                  <Point X="3.111739746094" Y="-23.898578125" />
                  <Point X="3.126296142578" Y="-23.915826171875" />
                  <Point X="3.134089599609" Y="-23.924017578125" />
                  <Point X="3.151333007812" Y="-23.94010546875" />
                  <Point X="3.160037353516" Y="-23.947306640625" />
                  <Point X="3.178239501953" Y="-23.960626953125" />
                  <Point X="3.195997558594" Y="-23.971396484375" />
                  <Point X="3.257379394531" Y="-24.00594921875" />
                  <Point X="3.268589111328" Y="-24.011326171875" />
                  <Point X="3.291603759766" Y="-24.02056640625" />
                  <Point X="3.303408691406" Y="-24.0244296875" />
                  <Point X="3.332558837891" Y="-24.03190234375" />
                  <Point X="3.354843017578" Y="-24.03621875" />
                  <Point X="3.437829101562" Y="-24.0471875" />
                  <Point X="3.444031738281" Y="-24.04780078125" />
                  <Point X="3.465708007812" Y="-24.04877734375" />
                  <Point X="3.491213623047" Y="-24.04796875" />
                  <Point X="3.500603271484" Y="-24.047203125" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.085763671875" />
                  <Point X="4.758942382812" Y="-24.125966796875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="3.846192382812" Y="-24.537328125" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.683141845703" Y="-24.581490234375" />
                  <Point X="3.657244140625" Y="-24.591712890625" />
                  <Point X="3.63419140625" Y="-24.6030625" />
                  <Point X="3.623034179688" Y="-24.6090234375" />
                  <Point X="3.541496826172" Y="-24.65615234375" />
                  <Point X="3.531259521484" Y="-24.6629921875" />
                  <Point X="3.511760742188" Y="-24.677931640625" />
                  <Point X="3.502499267578" Y="-24.68603125" />
                  <Point X="3.481600341797" Y="-24.706845703125" />
                  <Point X="3.466211181641" Y="-24.724166015625" />
                  <Point X="3.417291992188" Y="-24.7865" />
                  <Point X="3.410854736328" Y="-24.795791015625" />
                  <Point X="3.399130126953" Y="-24.815072265625" />
                  <Point X="3.393842773438" Y="-24.8250625" />
                  <Point X="3.384068603516" Y="-24.84652734375" />
                  <Point X="3.380005126953" Y="-24.857072265625" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.368182128906" Y="-24.900986328125" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350340820313" Y="-24.9984609375" />
                  <Point X="3.348893798828" Y="-25.023201171875" />
                  <Point X="3.34898046875" Y="-25.035619140625" />
                  <Point X="3.351175292969" Y="-25.06588671875" />
                  <Point X="3.354069335938" Y="-25.0878828125" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159667969" Y="-25.18398828125" />
                  <Point X="3.380005859375" Y="-25.20548828125" />
                  <Point X="3.384069335938" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.23749609375" />
                  <Point X="3.399130371094" Y="-25.247486328125" />
                  <Point X="3.410855224609" Y="-25.266767578125" />
                  <Point X="3.423875976562" Y="-25.284447265625" />
                  <Point X="3.472798339844" Y="-25.346787109375" />
                  <Point X="3.481266357422" Y="-25.356208984375" />
                  <Point X="3.499386230469" Y="-25.37384765625" />
                  <Point X="3.509038085938" Y="-25.382064453125" />
                  <Point X="3.534322753906" Y="-25.400828125" />
                  <Point X="3.552465820312" Y="-25.41274609375" />
                  <Point X="3.634003173828" Y="-25.459876953125" />
                  <Point X="3.639487060547" Y="-25.4628125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027832031" Y="-25.481029296875" />
                  <Point X="3.6919921875" Y="-25.483916015625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761613769531" Y="-25.93105078125" />
                  <Point X="4.753594726562" Y="-25.966193359375" />
                  <Point X="4.727802246094" Y="-26.079220703125" />
                  <Point X="3.615611328125" Y="-25.932796875" />
                  <Point X="3.436781738281" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400101318359" Y="-25.90804296875" />
                  <Point X="3.366729003906" Y="-25.91083984375" />
                  <Point X="3.354490234375" Y="-25.912673828125" />
                  <Point X="3.332954833984" Y="-25.917353515625" />
                  <Point X="3.172926025391" Y="-25.95213671875" />
                  <Point X="3.157877929688" Y="-25.956740234375" />
                  <Point X="3.128751953125" Y="-25.9683671875" />
                  <Point X="3.114674072266" Y="-25.975390625" />
                  <Point X="3.086847900391" Y="-25.992283203125" />
                  <Point X="3.074121582031" Y="-26.001533203125" />
                  <Point X="3.050370605469" Y="-26.022005859375" />
                  <Point X="3.039345947266" Y="-26.033228515625" />
                  <Point X="3.026329101562" Y="-26.048884765625" />
                  <Point X="2.929601806641" Y="-26.165216796875" />
                  <Point X="2.921323974609" Y="-26.1768515625" />
                  <Point X="2.906605712891" Y="-26.20123046875" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.873291503906" Y="-26.316935546875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876787353516" Y="-26.576673828125" />
                  <Point X="2.889159423828" Y="-26.605484375" />
                  <Point X="2.896541748047" Y="-26.619373046875" />
                  <Point X="2.908459716797" Y="-26.63791015625" />
                  <Point X="2.997021972656" Y="-26.775662109375" />
                  <Point X="3.001742431641" Y="-26.782353515625" />
                  <Point X="3.0197890625" Y="-26.804443359375" />
                  <Point X="3.043485839844" Y="-26.8281171875" />
                  <Point X="3.052795898438" Y="-26.836279296875" />
                  <Point X="4.087170654297" Y="-27.629984375" />
                  <Point X="4.045495849609" Y="-27.697419921875" />
                  <Point X="4.028904296875" Y="-27.720994140625" />
                  <Point X="4.001273681641" Y="-27.76025390625" />
                  <Point X="3.007726318359" Y="-27.18662890625" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841191650391" Y="-27.09088671875" />
                  <Point X="2.815026611328" Y="-27.079515625" />
                  <Point X="2.783122070312" Y="-27.069328125" />
                  <Point X="2.771108886719" Y="-27.066337890625" />
                  <Point X="2.745478271484" Y="-27.061708984375" />
                  <Point X="2.555018554688" Y="-27.0273125" />
                  <Point X="2.539359619141" Y="-27.02580859375" />
                  <Point X="2.508009277344" Y="-27.025404296875" />
                  <Point X="2.492317871094" Y="-27.02650390625" />
                  <Point X="2.460145019531" Y="-27.0314609375" />
                  <Point X="2.444849609375" Y="-27.03513671875" />
                  <Point X="2.415074951172" Y="-27.044958984375" />
                  <Point X="2.400595703125" Y="-27.05110546875" />
                  <Point X="2.379302978516" Y="-27.062310546875" />
                  <Point X="2.221077636719" Y="-27.145583984375" />
                  <Point X="2.208972412109" Y="-27.15316796875" />
                  <Point X="2.186039306641" Y="-27.1700625" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144940673828" Y="-27.21116015625" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.109257324219" Y="-27.26748828125" />
                  <Point X="2.025984619141" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.44019140625" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337158203" Y="-27.485263671875" />
                  <Point X="2.001379394531" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.00218737793" Y="-27.580140625" />
                  <Point X="2.006816162109" Y="-27.605771484375" />
                  <Point X="2.041213134766" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.723753173828" Y="-29.036083984375" />
                  <Point X="1.955453735352" Y="-28.03481640625" />
                  <Point X="1.833914672852" Y="-27.876423828125" />
                  <Point X="1.828658691406" Y="-27.87015234375" />
                  <Point X="1.808835083008" Y="-27.84962890625" />
                  <Point X="1.783252319336" Y="-27.82800390625" />
                  <Point X="1.773299438477" Y="-27.820646484375" />
                  <Point X="1.748020751953" Y="-27.80439453125" />
                  <Point X="1.56017578125" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517470336914" Y="-27.663873046875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926513672" Y="-27.65115234375" />
                  <Point X="1.455391479492" Y="-27.6486953125" />
                  <Point X="1.424129150391" Y="-27.646376953125" />
                  <Point X="1.408401855469" Y="-27.646515625" />
                  <Point X="1.380755371094" Y="-27.64905859375" />
                  <Point X="1.175315063477" Y="-27.667962890625" />
                  <Point X="1.161225463867" Y="-27.670337890625" />
                  <Point X="1.133571411133" Y="-27.677171875" />
                  <Point X="1.120006835938" Y="-27.681630859375" />
                  <Point X="1.092621337891" Y="-27.692974609375" />
                  <Point X="1.07987902832" Y="-27.6994140625" />
                  <Point X="1.055497802734" Y="-27.7141328125" />
                  <Point X="1.04385925293" Y="-27.722412109375" />
                  <Point X="1.022511169434" Y="-27.740162109375" />
                  <Point X="0.863875366211" Y="-27.872064453125" />
                  <Point X="0.852653320312" Y="-27.88308984375" />
                  <Point X="0.832182434082" Y="-27.90683984375" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041259766" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394287109" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.776409118652" Y="-28.034998046875" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584716797" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091491699" Y="-29.15233984375" />
                  <Point X="0.702578063965" Y="-28.6652578125" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605957031" Y="-28.480123046875" />
                  <Point X="0.642146362305" Y="-28.45358203125" />
                  <Point X="0.626786437988" Y="-28.4238125" />
                  <Point X="0.620404602051" Y="-28.413203125" />
                  <Point X="0.600984802246" Y="-28.385224609375" />
                  <Point X="0.456676971436" Y="-28.177302734375" />
                  <Point X="0.44666885376" Y="-28.16516796875" />
                  <Point X="0.424786682129" Y="-28.14271484375" />
                  <Point X="0.412912872314" Y="-28.132396484375" />
                  <Point X="0.386657318115" Y="-28.11315234375" />
                  <Point X="0.373238739014" Y="-28.10493359375" />
                  <Point X="0.345235961914" Y="-28.090826171875" />
                  <Point X="0.330651885986" Y="-28.0849375" />
                  <Point X="0.300601074219" Y="-28.075611328125" />
                  <Point X="0.077292930603" Y="-28.0063046875" />
                  <Point X="0.063376625061" Y="-28.003109375" />
                  <Point X="0.035218677521" Y="-27.99883984375" />
                  <Point X="0.020976739883" Y="-27.997765625" />
                  <Point X="-0.008664761543" Y="-27.997765625" />
                  <Point X="-0.022906551361" Y="-27.99883984375" />
                  <Point X="-0.051064498901" Y="-28.003109375" />
                  <Point X="-0.064980659485" Y="-28.0063046875" />
                  <Point X="-0.095031616211" Y="-28.015630859375" />
                  <Point X="-0.318339752197" Y="-28.0849375" />
                  <Point X="-0.332928131104" Y="-28.090828125" />
                  <Point X="-0.360931365967" Y="-28.1049375" />
                  <Point X="-0.374346221924" Y="-28.11315625" />
                  <Point X="-0.400601501465" Y="-28.132400390625" />
                  <Point X="-0.412475738525" Y="-28.14271875" />
                  <Point X="-0.43435925293" Y="-28.165173828125" />
                  <Point X="-0.444368408203" Y="-28.177310546875" />
                  <Point X="-0.463788238525" Y="-28.205291015625" />
                  <Point X="-0.608096008301" Y="-28.4132109375" />
                  <Point X="-0.612468933105" Y="-28.42012890625" />
                  <Point X="-0.625977172852" Y="-28.445263671875" />
                  <Point X="-0.638778198242" Y="-28.47621484375" />
                  <Point X="-0.642753112793" Y="-28.487935546875" />
                  <Point X="-0.985424804688" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.179069712473" Y="-27.447057199151" />
                  <Point X="-3.708977160519" Y="-27.96914787048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.961955536308" Y="-28.798799435176" />
                  <Point X="-2.823085994822" Y="-28.953029685879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.106326102758" Y="-27.385871890237" />
                  <Point X="-3.624866674553" Y="-27.920586756584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.912011040125" Y="-28.712293145447" />
                  <Point X="-2.511018479973" Y="-29.157640501105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.0307244765" Y="-27.327860730264" />
                  <Point X="-3.540756188587" Y="-27.872025642689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.862066543942" Y="-28.625786855717" />
                  <Point X="-2.44597618327" Y="-29.08790201758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.955122850241" Y="-27.269849570292" />
                  <Point X="-3.456645702622" Y="-27.823464528794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.812122047759" Y="-28.539280565988" />
                  <Point X="-2.38447942655" Y="-29.014225812976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.879521223982" Y="-27.211838410319" />
                  <Point X="-3.372535216656" Y="-27.774903414899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.762177551576" Y="-28.452774276259" />
                  <Point X="-2.307634756925" Y="-28.957595192523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.69110985298" Y="-26.168502649824" />
                  <Point X="-4.592767527766" Y="-26.277722866944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803919597723" Y="-27.153827250346" />
                  <Point X="-3.28842473069" Y="-27.726342301003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.712233055393" Y="-28.366267986529" />
                  <Point X="-2.227819318671" Y="-28.904263944887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.741227053454" Y="-25.970866587532" />
                  <Point X="-4.478480117594" Y="-26.262676622731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.728317971464" Y="-27.095816090374" />
                  <Point X="-3.204314244724" Y="-27.677781187108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.66228855921" Y="-28.2797616968" />
                  <Point X="-2.148003880417" Y="-28.850932697252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.765367367724" Y="-25.802080780155" />
                  <Point X="-4.364192707422" Y="-26.247630378518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.652716345205" Y="-27.037804930401" />
                  <Point X="-3.120203758759" Y="-27.629220073213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.612344063027" Y="-28.19325540707" />
                  <Point X="-2.065329633118" Y="-28.800776478719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.773417041046" Y="-25.651165439986" />
                  <Point X="-4.249905297251" Y="-26.232584134305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.577114718947" Y="-26.979793770429" />
                  <Point X="-3.036093272793" Y="-27.580658959318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.562399566844" Y="-28.106749117341" />
                  <Point X="-1.959716374337" Y="-28.776096613416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.135936472794" Y="-29.690996881534" />
                  <Point X="-1.087072790956" Y="-29.745265498104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.670429166061" Y="-25.623569790583" />
                  <Point X="-4.135617887079" Y="-26.217537890091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501513092688" Y="-26.921782610456" />
                  <Point X="-2.951982786827" Y="-27.532097845422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.512455070661" Y="-28.020242827611" />
                  <Point X="-1.824906950173" Y="-28.783842374772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120044332177" Y="-29.566671619553" />
                  <Point X="-0.974971278843" Y="-29.727791568151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.567441291076" Y="-25.595974141179" />
                  <Point X="-4.021330476908" Y="-26.202491645878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.425911466429" Y="-26.863771450483" />
                  <Point X="-2.867872300862" Y="-27.483536731527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.462510574478" Y="-27.933736537882" />
                  <Point X="-1.689054414348" Y="-28.792746628994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.142903657385" Y="-29.39930849466" />
                  <Point X="-0.945653694846" Y="-29.618376771605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.464453416092" Y="-25.568378491776" />
                  <Point X="-3.907043066736" Y="-26.187445401665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.35030984017" Y="-26.805760290511" />
                  <Point X="-2.783761814896" Y="-27.434975617632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.412566199141" Y="-27.847230113939" />
                  <Point X="-1.521648204963" Y="-28.83669478796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.179152107509" Y="-29.217075240072" />
                  <Point X="-0.91633611085" Y="-29.508961975059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.361465541107" Y="-25.540782842372" />
                  <Point X="-3.792755656565" Y="-26.172399157452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.274708213911" Y="-26.747749130538" />
                  <Point X="-2.69965132893" Y="-27.386414503737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.362622053453" Y="-27.760723434945" />
                  <Point X="-0.887018526854" Y="-29.399547178513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.258477666122" Y="-25.513187192969" />
                  <Point X="-3.678468246393" Y="-26.157352913239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.199106574518" Y="-26.689737985153" />
                  <Point X="-2.602833109858" Y="-27.351966757265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.319738978235" Y="-27.66637464272" />
                  <Point X="-0.857700942857" Y="-29.290132381967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.155489791137" Y="-25.485591543566" />
                  <Point X="-3.564180836222" Y="-26.142306669025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.123504665192" Y="-26.631727139558" />
                  <Point X="-2.450088128565" Y="-27.379631972828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.329975290059" Y="-27.513030794465" />
                  <Point X="-0.828383358861" Y="-29.180717585421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.052501916152" Y="-25.457995894162" />
                  <Point X="-3.44989342605" Y="-26.127260424812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.047914357321" Y="-26.573703409242" />
                  <Point X="-0.799065774864" Y="-29.071302788875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.949514041168" Y="-25.430400244759" />
                  <Point X="-3.335606015879" Y="-26.112214180599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.98600089511" Y="-26.500490002973" />
                  <Point X="-0.769748190868" Y="-28.961887992328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.77932711911" Y="-24.366824183189" />
                  <Point X="-4.728311348293" Y="-24.423482936713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.846526166183" Y="-25.402804595355" />
                  <Point X="-3.218966856484" Y="-26.099779818505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.951382960935" Y="-26.396961841668" />
                  <Point X="-0.740430606871" Y="-28.852473195782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.761283725994" Y="-24.244888129158" />
                  <Point X="-4.559827442409" Y="-24.468627998897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.743538291198" Y="-25.375208945952" />
                  <Point X="-0.711113022875" Y="-28.743058399236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.743240332877" Y="-24.122952075126" />
                  <Point X="-4.391343536526" Y="-24.513773061082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640550364845" Y="-25.347613353598" />
                  <Point X="-0.681795438878" Y="-28.63364360269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.721276836756" Y="-24.00536973655" />
                  <Point X="-4.222859630642" Y="-24.558918123267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.537562368878" Y="-25.32001783856" />
                  <Point X="-0.652477854882" Y="-28.524228806144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691704195778" Y="-23.896238209479" />
                  <Point X="-4.054375724758" Y="-24.604063185452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.44315327457" Y="-25.282894487975" />
                  <Point X="-0.61467638839" Y="-28.424236315672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.662131522316" Y="-23.787106718487" />
                  <Point X="-3.885891818874" Y="-24.649208247636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.368052478266" Y="-25.224327099987" />
                  <Point X="-0.559636102809" Y="-28.34338947342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.628550918471" Y="-23.682426485135" />
                  <Point X="-3.717407912991" Y="-24.694353309821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.31654005357" Y="-25.139562171286" />
                  <Point X="-0.503990543704" Y="-28.26321485552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.483524149765" Y="-23.701519757208" />
                  <Point X="-3.548924007107" Y="-24.739498372006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296409236543" Y="-25.019944436373" />
                  <Point X="-0.448345048183" Y="-28.183040167002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.338497381059" Y="-23.720613029281" />
                  <Point X="-0.379820729935" Y="-28.117168860182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.193470612353" Y="-23.739706301355" />
                  <Point X="-0.289160951581" Y="-28.075881472376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.048443843646" Y="-23.758799573428" />
                  <Point X="-0.189247022751" Y="-28.044871859904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.903417082694" Y="-23.777892836889" />
                  <Point X="-0.089333021729" Y="-28.013862327608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789595695828" Y="-28.990011560969" />
                  <Point X="0.815509548992" Y="-29.018791810601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.759858533646" Y="-23.795355485836" />
                  <Point X="0.024229457417" Y="-27.998010965926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.735436279548" Y="-28.787886163215" />
                  <Point X="0.793617139967" Y="-28.852502554921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.648456700089" Y="-23.777104484122" />
                  <Point X="0.191427413607" Y="-28.041727836286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.681277057796" Y="-28.585760981507" />
                  <Point X="0.771724730943" Y="-28.686213299241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.555814720279" Y="-23.73801855406" />
                  <Point X="0.379781504009" Y="-28.10894097407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.508278506001" Y="-28.2516513526" />
                  <Point X="0.749832321918" Y="-28.519924043561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.487885652662" Y="-23.671486154439" />
                  <Point X="0.727939912893" Y="-28.353634787882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443036186724" Y="-23.579321260357" />
                  <Point X="0.736052441395" Y="-28.220669391326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.178130347319" Y="-22.620941213785" />
                  <Point X="-3.820563419349" Y="-23.018059518877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421726382229" Y="-23.46101292368" />
                  <Point X="0.760910791598" Y="-28.106302113922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.127853091493" Y="-22.534804491079" />
                  <Point X="0.786674992236" Y="-27.992940885348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.077575835668" Y="-22.448667768373" />
                  <Point X="0.834530991322" Y="-27.904115084606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.027298579842" Y="-22.362531045667" />
                  <Point X="0.903695533793" Y="-27.838954818819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.972508343022" Y="-22.281406496132" />
                  <Point X="0.976799848897" Y="-27.778170113824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.91325278482" Y="-22.205241188408" />
                  <Point X="1.050302928688" Y="-27.717828281882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853996989599" Y="-22.129076143919" />
                  <Point X="1.140082133622" Y="-27.675562918216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.794741194378" Y="-22.052911099431" />
                  <Point X="1.254512352832" Y="-27.660675279508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.735485399157" Y="-21.976746054943" />
                  <Point X="1.372566250847" Y="-27.64981214383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501202989351" Y="-22.094967759039" />
                  <Point X="1.511162685119" Y="-27.661763806007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580450391303" Y="-28.849328114447" />
                  <Point X="2.726625024045" Y="-29.011671490921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.237973242039" Y="-22.245338738442" />
                  <Point X="1.805976922961" Y="-27.847212915867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.843348720372" Y="-27.888718501772" />
                  <Point X="2.498161664237" Y="-28.615961952102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.083322142809" Y="-22.275120912443" />
                  <Point X="2.26969830443" Y="-28.220252413282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.966996464959" Y="-22.262338393822" />
                  <Point X="2.054393528581" Y="-27.839156962486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884561183062" Y="-22.211916777323" />
                  <Point X="2.015529575957" Y="-27.654018898088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.817030048541" Y="-22.144942428226" />
                  <Point X="2.004156854333" Y="-27.499412938888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.764103518008" Y="-22.061748023164" />
                  <Point X="2.040610180778" Y="-27.397923187207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750621688623" Y="-21.934745839365" />
                  <Point X="2.08776723196" Y="-27.308321126176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.818384372046" Y="-21.71751248288" />
                  <Point X="2.137370458125" Y="-27.221435817693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.046847754811" Y="-21.321802918564" />
                  <Point X="2.205824789654" Y="-27.155486782746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.987910795745" Y="-21.245283770652" />
                  <Point X="2.291412928844" Y="-27.108566769014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.912283501127" Y="-21.18730111828" />
                  <Point X="2.378146690033" Y="-27.062919097412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.836656206509" Y="-21.129318465908" />
                  <Point X="2.475523317782" Y="-27.029091526605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761028911891" Y="-21.071335813537" />
                  <Point X="2.610832515727" Y="-27.037392342977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.685401617272" Y="-21.013353161165" />
                  <Point X="2.763491646574" Y="-27.064962211961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.604883817442" Y="-20.960801965087" />
                  <Point X="2.993459281759" Y="-27.178391873366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.519674078588" Y="-20.913461695206" />
                  <Point X="3.259698397756" Y="-27.332105095293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.434464487354" Y="-20.866121261377" />
                  <Point X="3.525937601646" Y="-27.485818414834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.349254896119" Y="-20.818780827548" />
                  <Point X="2.95376940852" Y="-26.708385986723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.104977889794" Y="-26.876320018376" />
                  <Point X="3.792176805536" Y="-27.639531734376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.264045304885" Y="-20.771440393719" />
                  <Point X="2.859925604045" Y="-26.462186610797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518557332584" Y="-27.193671251177" />
                  <Point X="4.013310788809" Y="-27.743150631415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.012802611816" Y="-20.908498400664" />
                  <Point X="2.87177870711" Y="-26.333375543164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932136775373" Y="-27.511022483977" />
                  <Point X="4.067666968366" Y="-27.661544012452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.828749674448" Y="-20.970934624059" />
                  <Point X="2.897625485476" Y="-26.220106026448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.723403532115" Y="-20.945958095886" />
                  <Point X="2.951895886945" Y="-26.138404141267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.630848769982" Y="-20.90677530078" />
                  <Point X="3.013269552726" Y="-26.064591230327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.554657610072" Y="-20.849418884259" />
                  <Point X="3.080290023558" Y="-25.997049731746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.505136509684" Y="-20.762442365861" />
                  <Point X="3.168817088208" Y="-25.95339372541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.471866809425" Y="-20.657416839096" />
                  <Point X="3.275454691479" Y="-25.929851509918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468490859493" Y="-20.519190939102" />
                  <Point X="3.384807400723" Y="-25.909324725098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.427435246081" Y="-20.422812544925" />
                  <Point X="3.522771883947" Y="-25.920574534531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.325366038059" Y="-20.394196612495" />
                  <Point X="3.667798358849" Y="-25.939667480301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.223296830038" Y="-20.365580680065" />
                  <Point X="3.81282513753" Y="-25.958760763453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121227622017" Y="-20.336964747635" />
                  <Point X="3.957851916211" Y="-25.977854046604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.019158413995" Y="-20.308348815205" />
                  <Point X="3.374403110873" Y="-25.187893229396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603338839213" Y="-25.442152114382" />
                  <Point X="4.102878694892" Y="-25.996947329755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.915216687557" Y="-20.281812525164" />
                  <Point X="3.349202135116" Y="-25.017929438098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.793194530199" Y="-25.511032948566" />
                  <Point X="4.247905473573" Y="-26.016040612907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.799569826038" Y="-20.26827610463" />
                  <Point X="3.368886203814" Y="-24.897815538899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.961678210039" Y="-25.556177759704" />
                  <Point X="4.392932252254" Y="-26.035133896058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.683921776562" Y="-20.254741003457" />
                  <Point X="-0.203891125258" Y="-20.787869052297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.124125757471" Y="-20.876457468011" />
                  <Point X="3.408437411569" Y="-24.799766332971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.130161889879" Y="-25.601322570841" />
                  <Point X="4.537959030935" Y="-26.054227179209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.568273727085" Y="-20.241205902284" />
                  <Point X="-0.270131779327" Y="-20.57232608066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.036152465708" Y="-20.91248919629" />
                  <Point X="3.467199814742" Y="-24.723053321099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.298645569719" Y="-25.646467381979" />
                  <Point X="4.682985809616" Y="-26.073320462361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.452625677609" Y="-20.227670801111" />
                  <Point X="-0.324290883387" Y="-20.370201029661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.133134307711" Y="-20.878223171493" />
                  <Point X="3.537313590842" Y="-24.658947286061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.467129249559" Y="-25.691612193117" />
                  <Point X="4.745662338519" Y="-26.000954527509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.201055189219" Y="-20.811681680277" />
                  <Point X="3.621168193372" Y="-24.61010198482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.6356129294" Y="-25.736757004254" />
                  <Point X="4.768633418025" Y="-25.884491223651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.24366926125" Y="-20.717034129745" />
                  <Point X="2.020666161218" Y="-22.690589125663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.307341980192" Y="-23.008974877914" />
                  <Point X="3.715104366663" Y="-24.572453402235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.27298678381" Y="-20.607619264968" />
                  <Point X="2.018513462035" Y="-22.546223038772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.448816233644" Y="-23.024122682087" />
                  <Point X="3.003822446768" Y="-23.64051952819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.360823356081" Y="-24.037009205879" />
                  <Point X="3.818092138025" Y="-24.544857637747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.302304306371" Y="-20.49820440019" />
                  <Point X="2.048969978377" Y="-22.438073154742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.556610173651" Y="-23.001864708644" />
                  <Point X="3.015108346831" Y="-23.511078517804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.498026307652" Y="-24.047413248728" />
                  <Point X="3.92107994803" Y="-24.517261916176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331621828931" Y="-20.388789535412" />
                  <Point X="2.095969483093" Y="-22.348296120634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.650267980686" Y="-22.963906969012" />
                  <Point X="3.050925293489" Y="-23.408881994768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.612417648626" Y="-24.032482431364" />
                  <Point X="4.024067772535" Y="-24.489666210709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.360939351492" Y="-20.279374670634" />
                  <Point X="2.145913935896" Y="-22.261789782726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734378404515" Y="-22.915345786106" />
                  <Point X="3.106810932463" Y="-23.328974012575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726705033408" Y="-24.017436158954" />
                  <Point X="4.12705559704" Y="-24.462070505242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.436284076769" Y="-20.221078193217" />
                  <Point X="2.195858396678" Y="-22.17528345368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.818488828343" Y="-22.866784603201" />
                  <Point X="3.170767117182" Y="-23.258029279488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840992418191" Y="-24.002389886544" />
                  <Point X="4.230043421545" Y="-24.434474799776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.577428723841" Y="-20.235859932419" />
                  <Point X="2.245802863537" Y="-22.088777131383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.902599252172" Y="-22.818223420295" />
                  <Point X="3.246276450424" Y="-23.199915617736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.955279802974" Y="-23.987343614134" />
                  <Point X="4.33303124605" Y="-24.406879094309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.718573370912" Y="-20.250641671621" />
                  <Point X="2.295747330396" Y="-22.002270809086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.986709676001" Y="-22.76966223739" />
                  <Point X="3.32187809152" Y="-23.141904474241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.069567187757" Y="-23.972297341723" />
                  <Point X="4.436019070556" Y="-24.379283388842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.864727065753" Y="-20.270986521962" />
                  <Point X="2.345691797255" Y="-21.915764486788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.070820099829" Y="-22.721101054484" />
                  <Point X="3.397479765993" Y="-23.083893367816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.18385457254" Y="-23.957251069313" />
                  <Point X="4.539006895061" Y="-24.351687683375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.028071334554" Y="-20.31042343888" />
                  <Point X="2.395636264113" Y="-21.829258164491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.154930523658" Y="-22.672539871579" />
                  <Point X="3.473081440466" Y="-23.025882261391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.298141957323" Y="-23.942204796903" />
                  <Point X="4.641994719566" Y="-24.324091977909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.191415869237" Y="-20.349860651092" />
                  <Point X="2.445580730972" Y="-21.742751842194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.239040947487" Y="-22.623978688673" />
                  <Point X="3.54868311494" Y="-22.967871154966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.412429342106" Y="-23.927158524493" />
                  <Point X="4.744982544071" Y="-24.296496272442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.354760403921" Y="-20.389297863303" />
                  <Point X="2.495525197831" Y="-21.656245519896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.323151371315" Y="-22.575417505768" />
                  <Point X="3.624284789413" Y="-22.909860048541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.526716726889" Y="-23.912112252083" />
                  <Point X="4.767236150016" Y="-24.179236133467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.528559511592" Y="-20.440346055111" />
                  <Point X="2.54546966469" Y="-21.569739197599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.407261795144" Y="-22.526856322863" />
                  <Point X="3.699886463886" Y="-22.851848942116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.641004111671" Y="-23.897065979673" />
                  <Point X="4.731107051702" Y="-23.997135432493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.718389467533" Y="-20.509198307632" />
                  <Point X="2.595414131548" Y="-21.483232875302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491372218973" Y="-22.478295139957" />
                  <Point X="3.77548813836" Y="-22.793837835691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.91637601801" Y="-20.587109376123" />
                  <Point X="2.645358598407" Y="-21.396726553004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.575482642801" Y="-22.429733957052" />
                  <Point X="3.851089812833" Y="-22.735826729266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.1371971786" Y="-20.690380848376" />
                  <Point X="2.695303065266" Y="-21.310220230707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.65959306663" Y="-22.381172774146" />
                  <Point X="3.926691487306" Y="-22.677815622841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.381432364387" Y="-20.819656230036" />
                  <Point X="2.745247532125" Y="-21.22371390841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.743703490459" Y="-22.332611591241" />
                  <Point X="4.00229316178" Y="-22.619804516416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.657040342938" Y="-20.983774627964" />
                  <Point X="2.795191998983" Y="-21.137207586112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.827813914287" Y="-22.284050408335" />
                  <Point X="4.077894836253" Y="-22.56179340999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.911924338116" Y="-22.23548922543" />
                  <Point X="4.079316640477" Y="-22.421397211318" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.519052062988" Y="-28.71443359375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.444899047852" Y="-28.493564453125" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.274335754395" Y="-28.2663984375" />
                  <Point X="0.244284881592" Y="-28.257072265625" />
                  <Point X="0.02097677803" Y="-28.187765625" />
                  <Point X="-0.008664756775" Y="-28.187765625" />
                  <Point X="-0.038715789795" Y="-28.197091796875" />
                  <Point X="-0.262023895264" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.307698944092" Y="-28.313623046875" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.812295349121" Y="-29.85478125" />
                  <Point X="-0.84774407959" Y="-29.987078125" />
                  <Point X="-1.100230712891" Y="-29.938068359375" />
                  <Point X="-1.133859741211" Y="-29.929416015625" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.314869750977" Y="-29.594484375" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.316862548828" Y="-29.498666015625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.413949829102" Y="-29.178365234375" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240722656" Y="-28.985763671875" />
                  <Point X="-1.685961181641" Y="-28.983357421875" />
                  <Point X="-1.958829833984" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.020476196289" Y="-28.994234375" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.457094238281" Y="-29.4145" />
                  <Point X="-2.470752685547" Y="-29.40604296875" />
                  <Point X="-2.855837646484" Y="-29.167609375" />
                  <Point X="-2.902380615234" Y="-29.131771484375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.594331298828" Y="-27.782056640625" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499763671875" Y="-27.5975859375" />
                  <Point X="-2.516037109375" Y="-27.56670703125" />
                  <Point X="-2.531327880859" Y="-27.551416015625" />
                  <Point X="-2.560157226562" Y="-27.53719921875" />
                  <Point X="-2.591683837891" Y="-27.543470703125" />
                  <Point X="-3.728147216797" Y="-28.199607421875" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.857468505859" Y="-28.24683203125" />
                  <Point X="-4.161703613281" Y="-27.84712890625" />
                  <Point X="-4.195073242188" Y="-27.791173828125" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.318279052734" Y="-26.54169140625" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535644531" Y="-26.411083984375" />
                  <Point X="-3.144908203125" Y="-26.392486328125" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.14032421875" Y="-26.3346015625" />
                  <Point X="-3.164302246094" Y="-26.3087890625" />
                  <Point X="-3.187643798828" Y="-26.295052734375" />
                  <Point X="-3.219528808594" Y="-26.288572265625" />
                  <Point X="-4.654206054688" Y="-26.477451171875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.808404785156" Y="-26.477025390625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.936221679688" Y="-25.94946484375" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.733453613281" Y="-25.175802734375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.538604980469" Y="-25.119140625" />
                  <Point X="-3.514143798828" Y="-25.1021640625" />
                  <Point X="-3.502324707031" Y="-25.090646484375" />
                  <Point X="-3.493801757812" Y="-25.072373046875" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.486745117188" Y="-25.012923828125" />
                  <Point X="-3.494898925781" Y="-24.98665234375" />
                  <Point X="-3.502328857422" Y="-24.971908203125" />
                  <Point X="-3.517447021484" Y="-24.95810546875" />
                  <Point X="-3.54189453125" Y="-24.94113671875" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.865240234375" Y="-24.58349609375" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.994331542969" Y="-24.521822265625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.899872070312" Y="-23.937994140625" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.879419921875" Y="-23.589412109375" />
                  <Point X="-3.753265869141" Y="-23.60601953125" />
                  <Point X="-3.731704833984" Y="-23.6041328125" />
                  <Point X="-3.724419189453" Y="-23.6018359375" />
                  <Point X="-3.670278808594" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.5739453125" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.636196289062" Y="-23.549158203125" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.619843261719" Y="-23.447712890625" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968017578" Y="-23.380779296875" />
                  <Point X="-4.410111816406" Y="-22.805173828125" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.457991210938" Y="-22.7235" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.112936035156" Y="-22.152482421875" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.236636962891" Y="-22.02832421875" />
                  <Point X="-3.159156738281" Y="-22.073056640625" />
                  <Point X="-3.138519287109" Y="-22.079564453125" />
                  <Point X="-3.128366943359" Y="-22.080455078125" />
                  <Point X="-3.052964599609" Y="-22.08705078125" />
                  <Point X="-3.031506591797" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-3.006050537109" Y="-22.065392578125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.938961914062" Y="-21.96201171875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.284478759766" Y="-21.29021484375" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-3.271857421875" Y="-21.22356640625" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.678729492188" Y="-20.784474609375" />
                  <Point X="-2.141549072266" Y="-20.486029296875" />
                  <Point X="-1.991545410156" Y="-20.681517578125" />
                  <Point X="-1.967826293945" Y="-20.712427734375" />
                  <Point X="-1.951252319336" Y="-20.7263359375" />
                  <Point X="-1.939953125" Y="-20.73221875" />
                  <Point X="-1.856030639648" Y="-20.77590625" />
                  <Point X="-1.835124023438" Y="-20.781509765625" />
                  <Point X="-1.813808837891" Y="-20.77775" />
                  <Point X="-1.802045776367" Y="-20.772876953125" />
                  <Point X="-1.714634887695" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.682254638672" Y="-20.693369140625" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.689137695313" Y="-20.298859375" />
                  <Point X="-1.639947631836" Y="-20.285068359375" />
                  <Point X="-0.968083007812" Y="-20.096703125" />
                  <Point X="-0.878208557129" Y="-20.08618359375" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.06506111145" Y="-20.603556640625" />
                  <Point X="-0.042140228271" Y="-20.68909765625" />
                  <Point X="-0.024282068253" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594009399" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.2247759552" Y="-20.053439453125" />
                  <Point X="0.236648223877" Y="-20.0091328125" />
                  <Point X="0.273580200195" Y="-20.013" />
                  <Point X="0.860210205078" Y="-20.074435546875" />
                  <Point X="0.934572509766" Y="-20.092388671875" />
                  <Point X="1.508456054688" Y="-20.230943359375" />
                  <Point X="1.556526367188" Y="-20.24837890625" />
                  <Point X="1.931044189453" Y="-20.38421875" />
                  <Point X="1.977841308594" Y="-20.406103515625" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.383924316406" Y="-20.60121484375" />
                  <Point X="2.732519775391" Y="-20.804306640625" />
                  <Point X="2.775170898438" Y="-20.834638671875" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.332051269531" Y="-22.319390625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.222305419922" Y="-22.518009765625" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.203037597656" Y="-22.61591015625" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.223777587891" Y="-22.706697265625" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.282448486328" Y="-22.780892578125" />
                  <Point X="2.338248046875" Y="-22.818755859375" />
                  <Point X="2.360336669922" Y="-22.827021484375" />
                  <Point X="2.368571289062" Y="-22.828013671875" />
                  <Point X="2.429761474609" Y="-22.835392578125" />
                  <Point X="2.458187744141" Y="-22.8315078125" />
                  <Point X="2.528951171875" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.856405273438" Y="-22.04815234375" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="3.995814208984" Y="-21.97074609375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.2263671875" Y="-22.29741015625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.423169189453" Y="-23.303671875" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.272518066406" Y="-23.425109375" />
                  <Point X="3.221589355469" Y="-23.491548828125" />
                  <Point X="3.210566650391" Y="-23.51762890625" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.192875244141" Y="-23.61919140625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.221346923828" Y="-23.7207109375" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280947753906" Y="-23.8011796875" />
                  <Point X="3.289208007812" Y="-23.805830078125" />
                  <Point X="3.35058984375" Y="-23.8403828125" />
                  <Point X="3.379739990234" Y="-23.84785546875" />
                  <Point X="3.462726074219" Y="-23.85882421875" />
                  <Point X="3.475803466797" Y="-23.858828125" />
                  <Point X="4.725318359375" Y="-23.694326171875" />
                  <Point X="4.848975097656" Y="-23.678046875" />
                  <Point X="4.85037890625" Y="-23.6838125" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.946680664062" Y="-24.096736328125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="3.895368164062" Y="-24.72085546875" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.718114746094" Y="-24.7735234375" />
                  <Point X="3.636577392578" Y="-24.82065234375" />
                  <Point X="3.615678466797" Y="-24.841466796875" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.554790527344" Y="-24.936724609375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.540677734375" Y="-25.05214453125" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.573342285156" Y="-25.167146484375" />
                  <Point X="3.622264648438" Y="-25.229486328125" />
                  <Point X="3.647549316406" Y="-25.24825" />
                  <Point X="3.729086669922" Y="-25.295380859375" />
                  <Point X="3.741167480469" Y="-25.300388671875" />
                  <Point X="4.887029296875" Y="-25.607419921875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.998019042969" Y="-25.637498046875" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.938833007812" Y="-26.008462890625" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.590811523438" Y="-26.121171875" />
                  <Point X="3.411981933594" Y="-26.09762890625" />
                  <Point X="3.3948359375" Y="-26.098341796875" />
                  <Point X="3.373300537109" Y="-26.103021484375" />
                  <Point X="3.213271728516" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.172428710938" Y="-26.170353515625" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.0624921875" Y="-26.334345703125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.068278564453" Y="-26.53516015625" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.1684609375" Y="-26.685541015625" />
                  <Point X="4.231827148438" Y="-27.5014921875" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.204134277344" Y="-27.802138671875" />
                  <Point X="4.184279296875" Y="-27.830349609375" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="2.912726318359" Y="-27.351171875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.711710205078" Y="-27.248685546875" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.467784912109" Y="-27.230451171875" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.277393554688" Y="-27.3559765625" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.193791748047" Y="-27.572005859375" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.917411621094" Y="-28.962123046875" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.835294433594" Y="-29.19021484375" />
                  <Point X="2.813104492188" Y="-29.204578125" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.804716552734" Y="-28.15048046875" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.645270385742" Y="-27.96421484375" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.398158447266" Y="-27.83826171875" />
                  <Point X="1.192718139648" Y="-27.857166015625" />
                  <Point X="1.165332519531" Y="-27.868509765625" />
                  <Point X="1.143984619141" Y="-27.886259765625" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.962073913574" Y="-28.075353515625" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.107577148438" Y="-29.781619140625" />
                  <Point X="1.127642578125" Y="-29.934029296875" />
                  <Point X="0.994366210938" Y="-29.9632421875" />
                  <Point X="0.973839050293" Y="-29.96697265625" />
                  <Point X="0.860200561523" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#128" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.022920783437" Y="4.440666006273" Z="0.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.3" />
                  <Point X="-0.890176654647" Y="4.994757309884" Z="0.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.3" />
                  <Point X="-1.659601239299" Y="4.794355896753" Z="0.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.3" />
                  <Point X="-1.74543731072" Y="4.73023516653" Z="0.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.3" />
                  <Point X="-1.736096496222" Y="4.352946996444" Z="0.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.3" />
                  <Point X="-1.827336009275" Y="4.304597030641" Z="0.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.3" />
                  <Point X="-1.923021593809" Y="4.343412201105" Z="0.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.3" />
                  <Point X="-1.958034232034" Y="4.380202577259" Z="0.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.3" />
                  <Point X="-2.709168484006" Y="4.290513274447" Z="0.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.3" />
                  <Point X="-3.308435598382" Y="3.84739381736" Z="0.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.3" />
                  <Point X="-3.333936054119" Y="3.716066253979" Z="0.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.3" />
                  <Point X="-2.994927771378" Y="3.064910584831" Z="0.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.3" />
                  <Point X="-3.04756112162" Y="3.001242446267" Z="0.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.3" />
                  <Point X="-3.130165880776" Y="3.000636927795" Z="0.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.3" />
                  <Point X="-3.217793094318" Y="3.046257894626" Z="0.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.3" />
                  <Point X="-4.158555515522" Y="2.909501502997" Z="0.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.3" />
                  <Point X="-4.507329198" Y="2.332986107535" Z="0.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.3" />
                  <Point X="-4.446705943642" Y="2.186439527258" Z="0.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.3" />
                  <Point X="-3.670349884955" Y="1.560480553132" Z="0.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.3" />
                  <Point X="-3.688546464969" Y="1.501257926128" Z="0.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.3" />
                  <Point X="-3.745610298701" Y="1.477129727547" Z="0.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.3" />
                  <Point X="-3.879049898843" Y="1.491441006393" Z="0.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.3" />
                  <Point X="-4.95428785543" Y="1.106363588244" Z="0.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.3" />
                  <Point X="-5.049949098396" Y="0.516776252241" Z="0.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.3" />
                  <Point X="-4.884337177039" Y="0.399486678799" Z="0.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.3" />
                  <Point X="-3.552099634682" Y="0.032091631074" Z="0.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.3" />
                  <Point X="-3.540654018987" Y="0.003535409559" Z="0.3" />
                  <Point X="-3.539556741714" Y="0" Z="0.3" />
                  <Point X="-3.547710582148" Y="-0.026271541502" Z="0.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.3" />
                  <Point X="-3.573268960442" Y="-0.04678435206" Z="0.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.3" />
                  <Point X="-3.752550611733" Y="-0.096225377957" Z="0.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.3" />
                  <Point X="-4.991873627245" Y="-0.925262157864" Z="0.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.3" />
                  <Point X="-4.863005660257" Y="-1.458119841728" Z="0.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.3" />
                  <Point X="-4.653836169148" Y="-1.495742119034" Z="0.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.3" />
                  <Point X="-3.195817411136" Y="-1.320601085424" Z="0.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.3" />
                  <Point X="-3.199466970005" Y="-1.348668827989" Z="0.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.3" />
                  <Point X="-3.354872985421" Y="-1.470743186905" Z="0.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.3" />
                  <Point X="-4.244172405535" Y="-2.785503410855" Z="0.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.3" />
                  <Point X="-3.903410394573" Y="-3.24583515394" Z="0.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.3" />
                  <Point X="-3.709302915994" Y="-3.211628421502" Z="0.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.3" />
                  <Point X="-2.557548683636" Y="-2.570781785496" Z="0.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.3" />
                  <Point X="-2.643788591666" Y="-2.725775466498" Z="0.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.3" />
                  <Point X="-2.93904053648" Y="-4.14010870366" Z="0.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.3" />
                  <Point X="-2.503230082172" Y="-4.417274909183" Z="0.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.3" />
                  <Point X="-2.424442847897" Y="-4.414778166478" Z="0.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.3" />
                  <Point X="-1.998853438567" Y="-4.004528954861" Z="0.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.3" />
                  <Point X="-1.69538729161" Y="-4.001969147028" Z="0.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.3" />
                  <Point X="-1.45307310646" Y="-4.184677053485" Z="0.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.3" />
                  <Point X="-1.372057709795" Y="-4.477140117199" Z="0.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.3" />
                  <Point X="-1.370597982061" Y="-4.556675723872" Z="0.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.3" />
                  <Point X="-1.152474555018" Y="-4.946559667257" Z="0.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.3" />
                  <Point X="-0.853219572698" Y="-5.006862280966" Z="0.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.3" />
                  <Point X="-0.77015503697" Y="-4.836441846383" Z="0.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.3" />
                  <Point X="-0.272778860908" Y="-3.310852879682" Z="0.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.3" />
                  <Point X="-0.030050980702" Y="-3.213566083864" Z="0.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.3" />
                  <Point X="0.22330809866" Y="-3.273545961424" Z="0.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.3" />
                  <Point X="0.397666998524" Y="-3.490792876086" Z="0.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.3" />
                  <Point X="0.464599823431" Y="-3.696094183433" Z="0.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.3" />
                  <Point X="0.976619741722" Y="-4.98488723517" Z="0.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.3" />
                  <Point X="1.155816663665" Y="-4.946410281962" Z="0.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.3" />
                  <Point X="1.150993452488" Y="-4.743813595399" Z="0.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.3" />
                  <Point X="1.004777097153" Y="-3.054692447311" Z="0.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.3" />
                  <Point X="1.169795837491" Y="-2.893425530001" Z="0.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.3" />
                  <Point X="1.396583986774" Y="-2.856770642481" Z="0.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.3" />
                  <Point X="1.612075275066" Y="-2.974993451463" Z="0.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.3" />
                  <Point X="1.758892950868" Y="-3.149638016126" Z="0.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.3" />
                  <Point X="2.834117668655" Y="-4.215272378526" Z="0.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.3" />
                  <Point X="3.024066917953" Y="-4.081147405703" Z="0.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.3" />
                  <Point X="2.954556967612" Y="-3.905843207782" Z="0.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.3" />
                  <Point X="2.23683997045" Y="-2.531838719987" Z="0.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.3" />
                  <Point X="2.31548401035" Y="-2.347982867844" Z="0.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.3" />
                  <Point X="2.484915510528" Y="-2.243417299617" Z="0.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.3" />
                  <Point X="2.696668083526" Y="-2.266608039041" Z="0.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.3" />
                  <Point X="2.881570389325" Y="-2.363192532662" Z="0.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.3" />
                  <Point X="4.219012041221" Y="-2.827846070676" Z="0.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.3" />
                  <Point X="4.380291838421" Y="-2.57095694341" Z="0.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.3" />
                  <Point X="4.25610935937" Y="-2.430542978564" Z="0.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.3" />
                  <Point X="3.104180500925" Y="-1.476840154961" Z="0.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.3" />
                  <Point X="3.106125842423" Y="-1.307646311059" Z="0.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.3" />
                  <Point X="3.204718758452" Y="-1.171039316781" Z="0.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.3" />
                  <Point X="3.377764471483" Y="-1.120601230245" Z="0.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.3" />
                  <Point X="3.578129331931" Y="-1.139463769306" Z="0.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.3" />
                  <Point X="4.981423930931" Y="-0.988307502563" Z="0.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.3" />
                  <Point X="5.041304538375" Y="-0.613671150005" Z="0.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.3" />
                  <Point X="4.893814404841" Y="-0.52784334132" Z="0.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.3" />
                  <Point X="3.666415346025" Y="-0.173680584955" Z="0.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.3" />
                  <Point X="3.60652030251" Y="-0.104999557712" Z="0.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.3" />
                  <Point X="3.583629252982" Y="-0.011458998134" Z="0.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.3" />
                  <Point X="3.597742197442" Y="0.085151533107" Z="0.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.3" />
                  <Point X="3.648859135891" Y="0.158949180863" Z="0.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.3" />
                  <Point X="3.736980068327" Y="0.214468259287" Z="0.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.3" />
                  <Point X="3.902153412466" Y="0.262128591449" Z="0.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.3" />
                  <Point X="4.989929564077" Y="0.942235324539" Z="0.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.3" />
                  <Point X="4.892803934557" Y="1.359294895286" Z="0.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.3" />
                  <Point X="4.712635994749" Y="1.386525849076" Z="0.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.3" />
                  <Point X="3.380129053303" Y="1.232992534642" Z="0.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.3" />
                  <Point X="3.307578814153" Y="1.269021218871" Z="0.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.3" />
                  <Point X="3.256961114038" Y="1.33805247489" Z="0.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.3" />
                  <Point X="3.235687753231" Y="1.422192278104" Z="0.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.3" />
                  <Point X="3.252563024744" Y="1.50018492521" Z="0.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.3" />
                  <Point X="3.306044601717" Y="1.575754095203" Z="0.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.3" />
                  <Point X="3.447451240243" Y="1.687941307763" Z="0.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.3" />
                  <Point X="4.262988685564" Y="2.759757349562" Z="0.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.3" />
                  <Point X="4.030244022852" Y="3.089736756195" Z="0.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.3" />
                  <Point X="3.825249230333" Y="3.026428704552" Z="0.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.3" />
                  <Point X="2.439114428662" Y="2.248075688346" Z="0.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.3" />
                  <Point X="2.368401288174" Y="2.252907599765" Z="0.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.3" />
                  <Point X="2.304367134131" Y="2.291762867725" Z="0.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.3" />
                  <Point X="2.25899573251" Y="2.352657726251" Z="0.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.3" />
                  <Point X="2.246522102994" Y="2.421357155659" Z="0.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.3" />
                  <Point X="2.264452254312" Y="2.50035514722" Z="0.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.3" />
                  <Point X="2.369196648308" Y="2.686889851622" Z="0.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.3" />
                  <Point X="2.797991948819" Y="4.23739024871" Z="0.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.3" />
                  <Point X="2.402938177215" Y="4.473268872164" Z="0.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.3" />
                  <Point X="1.992866807573" Y="4.670467683775" Z="0.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.3" />
                  <Point X="1.567418982799" Y="4.829906493772" Z="0.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.3" />
                  <Point X="0.940150979976" Y="4.98749479282" Z="0.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.3" />
                  <Point X="0.272632066406" Y="5.068009040363" Z="0.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.3" />
                  <Point X="0.170323778116" Y="4.990781499678" Z="0.3" />
                  <Point X="0" Y="4.355124473572" Z="0.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>