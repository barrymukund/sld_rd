<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#131" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="739" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.618700378418" Y="-28.7192734375" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542362426758" Y="-28.467375" />
                  <Point X="0.519719726562" Y="-28.434751953125" />
                  <Point X="0.378634765625" Y="-28.231474609375" />
                  <Point X="0.35674887085" Y="-28.209017578125" />
                  <Point X="0.330493835449" Y="-28.189775390625" />
                  <Point X="0.302491699219" Y="-28.17566796875" />
                  <Point X="0.267453308105" Y="-28.164794921875" />
                  <Point X="0.049132545471" Y="-28.09703515625" />
                  <Point X="0.020983165741" Y="-28.092767578125" />
                  <Point X="-0.008658312798" Y="-28.092765625" />
                  <Point X="-0.036824996948" Y="-28.09703515625" />
                  <Point X="-0.071863235474" Y="-28.10791015625" />
                  <Point X="-0.290184143066" Y="-28.17566796875" />
                  <Point X="-0.318184906006" Y="-28.18977734375" />
                  <Point X="-0.344440093994" Y="-28.209021484375" />
                  <Point X="-0.366324188232" Y="-28.2314765625" />
                  <Point X="-0.388967071533" Y="-28.2641015625" />
                  <Point X="-0.530052001953" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.896172912598" Y="-29.800763671875" />
                  <Point X="-0.916584716797" Y="-29.87694140625" />
                  <Point X="-1.079324707031" Y="-29.845353515625" />
                  <Point X="-1.116008544922" Y="-29.8359140625" />
                  <Point X="-1.246417602539" Y="-29.802361328125" />
                  <Point X="-1.221631713867" Y="-29.61409375" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.224879394531" Y="-29.474142578125" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323646240234" Y="-29.131203125" />
                  <Point X="-1.355905151367" Y="-29.1029140625" />
                  <Point X="-1.556907104492" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886474609" Y="-28.897994140625" />
                  <Point X="-1.643026611328" Y="-28.890966796875" />
                  <Point X="-1.685841186523" Y="-28.88816015625" />
                  <Point X="-1.952615722656" Y="-28.87067578125" />
                  <Point X="-1.983414916992" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042657958984" Y="-28.89480078125" />
                  <Point X="-2.078333496094" Y="-28.918638671875" />
                  <Point X="-2.300624267578" Y="-29.06716796875" />
                  <Point X="-2.312788574219" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.480148925781" Y="-29.288490234375" />
                  <Point X="-2.801706542969" Y="-29.089388671875" />
                  <Point X="-2.852483642578" Y="-29.05029296875" />
                  <Point X="-3.104721679688" Y="-28.856078125" />
                  <Point X="-2.526713134766" Y="-27.8549375" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858154297" Y="-27.6476484375" />
                  <Point X="-2.406587646484" Y="-27.6161171875" />
                  <Point X="-2.405779296875" Y="-27.58373046875" />
                  <Point X="-2.415901855469" Y="-27.552953125" />
                  <Point X="-2.432523193359" Y="-27.521724609375" />
                  <Point X="-2.4492109375" Y="-27.499181640625" />
                  <Point X="-2.464155029297" Y="-27.48423828125" />
                  <Point X="-2.489311279297" Y="-27.466212890625" />
                  <Point X="-2.518140136719" Y="-27.45199609375" />
                  <Point X="-2.547758300781" Y="-27.44301171875" />
                  <Point X="-2.578692382812" Y="-27.444025390625" />
                  <Point X="-2.61021875" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-3.750265380859" Y="-28.102681640625" />
                  <Point X="-3.818023681641" Y="-28.141802734375" />
                  <Point X="-4.082862304687" Y="-27.793857421875" />
                  <Point X="-4.119259277344" Y="-27.732826171875" />
                  <Point X="-4.306143066406" Y="-27.419451171875" />
                  <Point X="-3.286087158203" Y="-26.636734375" />
                  <Point X="-3.105954833984" Y="-26.498515625" />
                  <Point X="-3.083063232422" Y="-26.475880859375" />
                  <Point X="-3.064495849609" Y="-26.446779296875" />
                  <Point X="-3.056718261719" Y="-26.42759765625" />
                  <Point X="-3.052791259766" Y="-26.415720703125" />
                  <Point X="-3.046152099609" Y="-26.390087890625" />
                  <Point X="-3.042037353516" Y="-26.35860546875" />
                  <Point X="-3.042736083984" Y="-26.33573828125" />
                  <Point X="-3.048881103516" Y="-26.313703125" />
                  <Point X="-3.060883300781" Y="-26.28472265625" />
                  <Point X="-3.073079589844" Y="-26.2635078125" />
                  <Point X="-3.090290039062" Y="-26.246115234375" />
                  <Point X="-3.106459472656" Y="-26.233525390625" />
                  <Point X="-3.116640136719" Y="-26.226607421875" />
                  <Point X="-3.139460205078" Y="-26.213177734375" />
                  <Point X="-3.168722167969" Y="-26.20195703125" />
                  <Point X="-3.200607666016" Y="-26.1954765625" />
                  <Point X="-3.231928710938" Y="-26.194384765625" />
                  <Point X="-4.634563964844" Y="-26.379044921875" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.834077636719" Y="-25.99265625" />
                  <Point X="-4.843707519531" Y="-25.92532421875" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.738073730469" Y="-25.275392578125" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.514344482422" Y="-25.213296875" />
                  <Point X="-3.494938964844" Y="-25.203412109375" />
                  <Point X="-3.483892333984" Y="-25.196806640625" />
                  <Point X="-3.459977539063" Y="-25.180208984375" />
                  <Point X="-3.436021728516" Y="-25.158681640625" />
                  <Point X="-3.416070068359" Y="-25.130458984375" />
                  <Point X="-3.407364990234" Y="-25.111599609375" />
                  <Point X="-3.402889648438" Y="-25.099947265625" />
                  <Point X="-3.39491796875" Y="-25.074263671875" />
                  <Point X="-3.390716308594" Y="-25.0425078125" />
                  <Point X="-3.391995361328" Y="-25.0087421875" />
                  <Point X="-3.396196533203" Y="-24.9841796875" />
                  <Point X="-3.404168212891" Y="-24.958494140625" />
                  <Point X="-3.417491943359" Y="-24.92915625" />
                  <Point X="-3.4385625" Y="-24.90148828125" />
                  <Point X="-3.454212890625" Y="-24.88731640625" />
                  <Point X="-3.463809082031" Y="-24.879693359375" />
                  <Point X="-3.487723876953" Y="-24.86309375" />
                  <Point X="-3.501916748047" Y="-24.85495703125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.811444335938" Y="-24.49955859375" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.824488769531" Y="-24.023029296875" />
                  <Point X="-4.8051015625" Y="-23.951486328125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-3.912757080078" Y="-23.68084375" />
                  <Point X="-3.765666015625" Y="-23.70020703125" />
                  <Point X="-3.744984619141" Y="-23.700658203125" />
                  <Point X="-3.723423095703" Y="-23.698771484375" />
                  <Point X="-3.703144042969" Y="-23.69473828125" />
                  <Point X="-3.694649169922" Y="-23.692060546875" />
                  <Point X="-3.641718017578" Y="-23.67537109375" />
                  <Point X="-3.622783691406" Y="-23.667041015625" />
                  <Point X="-3.604039550781" Y="-23.656220703125" />
                  <Point X="-3.587354492188" Y="-23.64398828125" />
                  <Point X="-3.573713378906" Y="-23.62843359375" />
                  <Point X="-3.561299072266" Y="-23.610703125" />
                  <Point X="-3.551352539062" Y="-23.592572265625" />
                  <Point X="-3.547943847656" Y="-23.58434375" />
                  <Point X="-3.526705078125" Y="-23.533068359375" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.532050292969" Y="-23.41062109375" />
                  <Point X="-3.536163085938" Y="-23.402720703125" />
                  <Point X="-3.561790039062" Y="-23.3534921875" />
                  <Point X="-3.573281494141" Y="-23.336294921875" />
                  <Point X="-3.587193847656" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.335525878906" Y="-22.74266015625" />
                  <Point X="-4.351860351563" Y="-22.730126953125" />
                  <Point X="-4.081155761719" Y="-22.266345703125" />
                  <Point X="-4.029802978516" Y="-22.200337890625" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.296996337891" Y="-22.103169921875" />
                  <Point X="-3.206657226562" Y="-22.155328125" />
                  <Point X="-3.187729003906" Y="-22.163658203125" />
                  <Point X="-3.167086425781" Y="-22.17016796875" />
                  <Point X="-3.146794921875" Y="-22.174205078125" />
                  <Point X="-3.134963867188" Y="-22.175240234375" />
                  <Point X="-3.061245605469" Y="-22.181689453125" />
                  <Point X="-3.040560546875" Y="-22.18123828125" />
                  <Point X="-3.019102050781" Y="-22.178412109375" />
                  <Point X="-2.999012939453" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946075927734" Y="-22.13976953125" />
                  <Point X="-2.937677978516" Y="-22.13137109375" />
                  <Point X="-2.885352294922" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.844470947266" Y="-21.952048828125" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854956054688" Y="-21.858041015625" />
                  <Point X="-2.86146484375" Y="-21.8373984375" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-3.183332763672" Y="-21.275404296875" />
                  <Point X="-2.700618896484" Y="-20.9053125" />
                  <Point X="-2.61975390625" Y="-20.86038671875" />
                  <Point X="-2.167036376953" Y="-20.608865234375" />
                  <Point X="-2.070851074219" Y="-20.734216796875" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028892333984" Y="-20.78519921875" />
                  <Point X="-2.012312866211" Y="-20.799111328125" />
                  <Point X="-1.995116943359" Y="-20.8106015625" />
                  <Point X="-1.98194909668" Y="-20.81745703125" />
                  <Point X="-1.899900878906" Y="-20.860169921875" />
                  <Point X="-1.880625244141" Y="-20.86766796875" />
                  <Point X="-1.859718383789" Y="-20.873271484375" />
                  <Point X="-1.839268554688" Y="-20.876419921875" />
                  <Point X="-1.818622192383" Y="-20.87506640625" />
                  <Point X="-1.797306762695" Y="-20.871306640625" />
                  <Point X="-1.777450805664" Y="-20.865517578125" />
                  <Point X="-1.763735473633" Y="-20.8598359375" />
                  <Point X="-1.678276977539" Y="-20.8244375" />
                  <Point X="-1.660146118164" Y="-20.814490234375" />
                  <Point X="-1.642416381836" Y="-20.802076171875" />
                  <Point X="-1.626864501953" Y="-20.7884375" />
                  <Point X="-1.614633056641" Y="-20.771755859375" />
                  <Point X="-1.603810913086" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.591016113281" Y="-20.719919921875" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584202026367" Y="-20.3681015625" />
                  <Point X="-0.949624816895" Y="-20.19019140625" />
                  <Point X="-0.851598876953" Y="-20.178716796875" />
                  <Point X="-0.294711303711" Y="-20.113541015625" />
                  <Point X="-0.160627990723" Y="-20.613947265625" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113975525" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155905724" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425933838" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.307419616699" Y="-20.1120625" />
                  <Point X="0.844031555176" Y="-20.168259765625" />
                  <Point X="0.925154602051" Y="-20.187845703125" />
                  <Point X="1.481039428711" Y="-20.3220546875" />
                  <Point X="1.532545288086" Y="-20.340736328125" />
                  <Point X="1.894645874023" Y="-20.472072265625" />
                  <Point X="1.945701171875" Y="-20.49594921875" />
                  <Point X="2.294568847656" Y="-20.659103515625" />
                  <Point X="2.343932861328" Y="-20.687861328125" />
                  <Point X="2.680972167969" Y="-20.884220703125" />
                  <Point X="2.727497070312" Y="-20.91730859375" />
                  <Point X="2.943259765625" Y="-21.07074609375" />
                  <Point X="2.266739990234" Y="-22.242513671875" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.140013183594" Y="-22.46551953125" />
                  <Point X="2.132042480469" Y="-22.48870703125" />
                  <Point X="2.130107421875" Y="-22.495048828125" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108384277344" Y="-22.588173828125" />
                  <Point X="2.108204345703" Y="-22.61667578125" />
                  <Point X="2.108885498047" Y="-22.6286484375" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.140069580078" Y="-22.75252734375" />
                  <Point X="2.146010498047" Y="-22.761283203125" />
                  <Point X="2.18302734375" Y="-22.8158359375" />
                  <Point X="2.199184326172" Y="-22.834080078125" />
                  <Point X="2.221239013672" Y="-22.853322265625" />
                  <Point X="2.230352783203" Y="-22.860349609375" />
                  <Point X="2.28490625" Y="-22.8973671875" />
                  <Point X="2.304963867188" Y="-22.907734375" />
                  <Point X="2.327060302734" Y="-22.916" />
                  <Point X="2.348975830078" Y="-22.921337890625" />
                  <Point X="2.358568359375" Y="-22.922494140625" />
                  <Point X="2.418392089844" Y="-22.929708984375" />
                  <Point X="2.443263183594" Y="-22.929427734375" />
                  <Point X="2.473269775391" Y="-22.925119140625" />
                  <Point X="2.48430859375" Y="-22.922859375" />
                  <Point X="2.553491699219" Y="-22.904359375" />
                  <Point X="2.565286132812" Y="-22.900361328125" />
                  <Point X="2.588533691406" Y="-22.889853515625" />
                  <Point X="3.87452734375" Y="-22.147384765625" />
                  <Point X="3.967325683594" Y="-22.093806640625" />
                  <Point X="4.123267578125" Y="-22.31053125" />
                  <Point X="4.149206542969" Y="-22.353396484375" />
                  <Point X="4.262197753906" Y="-22.5401171875" />
                  <Point X="3.387667724609" Y="-23.211166015625" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.217159423828" Y="-23.34431640625" />
                  <Point X="3.199923583984" Y="-23.363990234375" />
                  <Point X="3.195982421875" Y="-23.368796875" />
                  <Point X="3.146191162109" Y="-23.43375390625" />
                  <Point X="3.133837402344" Y="-23.45515234375" />
                  <Point X="3.122391601562" Y="-23.482748046875" />
                  <Point X="3.118653564453" Y="-23.49355859375" />
                  <Point X="3.100106201172" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739746094" Y="-23.628234375" />
                  <Point X="3.100183349609" Y="-23.640076171875" />
                  <Point X="3.115408691406" Y="-23.7138671875" />
                  <Point X="3.123606445312" Y="-23.73741015625" />
                  <Point X="3.137450439453" Y="-23.764890625" />
                  <Point X="3.142928710938" Y="-23.77436328125" />
                  <Point X="3.184340332031" Y="-23.837306640625" />
                  <Point X="3.198897216797" Y="-23.8545546875" />
                  <Point X="3.216140869141" Y="-23.870642578125" />
                  <Point X="3.234344238281" Y="-23.883962890625" />
                  <Point X="3.243975341797" Y="-23.889384765625" />
                  <Point X="3.303986328125" Y="-23.923166015625" />
                  <Point X="3.327635253906" Y="-23.932568359375" />
                  <Point X="3.358633544922" Y="-23.940287109375" />
                  <Point X="3.369141845703" Y="-23.942283203125" />
                  <Point X="3.450280517578" Y="-23.953005859375" />
                  <Point X="3.462698486328" Y="-23.95382421875" />
                  <Point X="3.488203369141" Y="-23.953015625" />
                  <Point X="4.709811523438" Y="-23.7921875" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845936035156" Y="-24.067189453125" />
                  <Point X="4.854108886719" Y="-24.11968359375" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.896372070312" Y="-24.622234375" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.698776611328" Y="-24.67715625" />
                  <Point X="3.673902832031" Y="-24.68955859375" />
                  <Point X="3.668753417969" Y="-24.692326171875" />
                  <Point X="3.589037109375" Y="-24.73840234375" />
                  <Point X="3.569077636719" Y="-24.753802734375" />
                  <Point X="3.547088623047" Y="-24.776005859375" />
                  <Point X="3.539853759766" Y="-24.78420703125" />
                  <Point X="3.492024169922" Y="-24.845154296875" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.47052734375" Y="-24.88589453125" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.461121826172" Y="-24.9207578125" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443781982422" Y="-25.02941015625" />
                  <Point X="3.446340820312" Y="-25.061578125" />
                  <Point X="3.447737304688" Y="-25.0719140625" />
                  <Point X="3.463680664062" Y="-25.1551640625" />
                  <Point X="3.470526123047" Y="-25.176662109375" />
                  <Point X="3.480299804688" Y="-25.198126953125" />
                  <Point X="3.492024169922" Y="-25.217408203125" />
                  <Point X="3.499700195312" Y="-25.227189453125" />
                  <Point X="3.547530029297" Y="-25.28813671875" />
                  <Point X="3.566201171875" Y="-25.306181640625" />
                  <Point X="3.593306884766" Y="-25.32599609375" />
                  <Point X="3.601828613281" Y="-25.33155078125" />
                  <Point X="3.681544921875" Y="-25.37762890625" />
                  <Point X="3.692708251953" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.836849609375" Y="-25.692326171875" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.855022949219" Y="-25.948724609375" />
                  <Point X="4.844551757812" Y="-25.994611328125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.632890869141" Y="-26.030892578125" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374660644531" Y="-26.005509765625" />
                  <Point X="3.349551269531" Y="-26.010966796875" />
                  <Point X="3.193096435547" Y="-26.04497265625" />
                  <Point X="3.163973632812" Y="-26.05659765625" />
                  <Point X="3.136146972656" Y="-26.073490234375" />
                  <Point X="3.112396728516" Y="-26.0939609375" />
                  <Point X="3.097219482422" Y="-26.11221484375" />
                  <Point X="3.002652587891" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.967582275391" Y="-26.32900390625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976451660156" Y="-26.568" />
                  <Point X="2.99034765625" Y="-26.58961328125" />
                  <Point X="3.076931884766" Y="-26.7242890625" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="4.150245605469" Y="-27.55863671875" />
                  <Point X="4.213123046875" Y="-27.606884765625" />
                  <Point X="4.124815429687" Y="-27.749779296875" />
                  <Point X="4.103154785156" Y="-27.7805546875" />
                  <Point X="4.028980712891" Y="-27.8859453125" />
                  <Point X="2.986659667969" Y="-27.284162109375" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754222900391" Y="-27.159826171875" />
                  <Point X="2.724338623047" Y="-27.1544296875" />
                  <Point X="2.538132568359" Y="-27.12080078125" />
                  <Point X="2.506783935547" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444832763672" Y="-27.135177734375" />
                  <Point X="2.420006103516" Y="-27.148244140625" />
                  <Point X="2.265314697266" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.191466064453" Y="-27.315265625" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.101072265625" Y="-27.593142578125" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.819877929688" Y="-28.983189453125" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.781857421875" Y="-29.111638671875" />
                  <Point X="2.757637451172" Y="-29.12731640625" />
                  <Point X="2.701763916016" Y="-29.163482421875" />
                  <Point X="1.900256469727" Y="-28.118935546875" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721924316406" Y="-27.900556640625" />
                  <Point X="1.692450439453" Y="-27.881607421875" />
                  <Point X="1.50880078125" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365722656" Y="-27.743435546875" />
                  <Point X="1.417103881836" Y="-27.7411171875" />
                  <Point X="1.384868896484" Y="-27.74408203125" />
                  <Point X="1.184016967773" Y="-27.762564453125" />
                  <Point X="1.156361938477" Y="-27.7693984375" />
                  <Point X="1.12897668457" Y="-27.7807421875" />
                  <Point X="1.104593383789" Y="-27.795462890625" />
                  <Point X="1.079702392578" Y="-27.81616015625" />
                  <Point X="0.924609680176" Y="-27.945115234375" />
                  <Point X="0.904141479492" Y="-27.96886328125" />
                  <Point X="0.887249206543" Y="-27.996689453125" />
                  <Point X="0.875624206543" Y="-28.025810546875" />
                  <Point X="0.868182067871" Y="-28.06005078125" />
                  <Point X="0.821809936523" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="1.009064941406" Y="-29.76116796875" />
                  <Point X="1.022065307617" Y="-29.859916015625" />
                  <Point X="0.97568157959" Y="-29.87008203125" />
                  <Point X="0.953304443359" Y="-29.8741484375" />
                  <Point X="0.929315979004" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.05843737793" Y="-29.752634765625" />
                  <Point X="-1.092334472656" Y="-29.743912109375" />
                  <Point X="-1.14124609375" Y="-29.731328125" />
                  <Point X="-1.127444458008" Y="-29.626494140625" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.541037109375" />
                  <Point X="-1.121759155273" Y="-29.50933203125" />
                  <Point X="-1.123333740234" Y="-29.497693359375" />
                  <Point X="-1.131704711914" Y="-29.455609375" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026367188" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230575439453" Y="-29.094859375" />
                  <Point X="-1.250211914062" Y="-29.07093359375" />
                  <Point X="-1.261009887695" Y="-29.05977734375" />
                  <Point X="-1.293268798828" Y="-29.03148828125" />
                  <Point X="-1.494270751953" Y="-28.855212890625" />
                  <Point X="-1.506739501953" Y="-28.84596484375" />
                  <Point X="-1.533021728516" Y="-28.82962109375" />
                  <Point X="-1.546834838867" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591315307617" Y="-28.8054765625" />
                  <Point X="-1.621455444336" Y="-28.79844921875" />
                  <Point X="-1.63681237793" Y="-28.796169921875" />
                  <Point X="-1.679626953125" Y="-28.79336328125" />
                  <Point X="-1.946401489258" Y="-28.77587890625" />
                  <Point X="-1.961926635742" Y="-28.7761328125" />
                  <Point X="-1.992725952148" Y="-28.779166015625" />
                  <Point X="-2.00799987793" Y="-28.7819453125" />
                  <Point X="-2.039048339844" Y="-28.790263671875" />
                  <Point X="-2.053666992188" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.0954375" Y="-28.815810546875" />
                  <Point X="-2.131113037109" Y="-28.8396484375" />
                  <Point X="-2.353403808594" Y="-28.988177734375" />
                  <Point X="-2.359689453125" Y="-28.99276171875" />
                  <Point X="-2.380446533203" Y="-29.010134765625" />
                  <Point X="-2.402760986328" Y="-29.0327734375" />
                  <Point X="-2.410471679688" Y="-29.041630859375" />
                  <Point X="-2.503202880859" Y="-29.16248046875" />
                  <Point X="-2.747574951172" Y="-29.011169921875" />
                  <Point X="-2.79452734375" Y="-28.97501953125" />
                  <Point X="-2.980862548828" Y="-28.831546875" />
                  <Point X="-2.444440673828" Y="-27.9024375" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849365234" Y="-27.710080078125" />
                  <Point X="-2.323946289062" Y="-27.681109375" />
                  <Point X="-2.319682617188" Y="-27.666177734375" />
                  <Point X="-2.313412109375" Y="-27.634646484375" />
                  <Point X="-2.3116171875" Y="-27.61848828125" />
                  <Point X="-2.310808837891" Y="-27.5861015625" />
                  <Point X="-2.315534912109" Y="-27.554048828125" />
                  <Point X="-2.325657470703" Y="-27.523271484375" />
                  <Point X="-2.332040527344" Y="-27.508318359375" />
                  <Point X="-2.348661865234" Y="-27.47708984375" />
                  <Point X="-2.356167724609" Y="-27.465201171875" />
                  <Point X="-2.37285546875" Y="-27.442658203125" />
                  <Point X="-2.382037353516" Y="-27.43200390625" />
                  <Point X="-2.396981445312" Y="-27.417060546875" />
                  <Point X="-2.408822265625" Y="-27.407015625" />
                  <Point X="-2.433978515625" Y="-27.388990234375" />
                  <Point X="-2.447293945312" Y="-27.381009765625" />
                  <Point X="-2.476122802734" Y="-27.36679296875" />
                  <Point X="-2.490563720703" Y="-27.3610859375" />
                  <Point X="-2.520181884766" Y="-27.3521015625" />
                  <Point X="-2.550869628906" Y="-27.3480625" />
                  <Point X="-2.581803710938" Y="-27.349076171875" />
                  <Point X="-2.597227294922" Y="-27.3508515625" />
                  <Point X="-2.628753662109" Y="-27.357123046875" />
                  <Point X="-2.643684326172" Y="-27.36138671875" />
                  <Point X="-2.672649414062" Y="-27.3722890625" />
                  <Point X="-2.686683837891" Y="-27.378927734375" />
                  <Point X="-3.793088867188" Y="-28.0177109375" />
                  <Point X="-4.004018554688" Y="-27.74058984375" />
                  <Point X="-4.037666992188" Y="-27.68416796875" />
                  <Point X="-4.181266113281" Y="-27.443375" />
                  <Point X="-3.228254882812" Y="-26.712103515625" />
                  <Point X="-3.048122558594" Y="-26.573884765625" />
                  <Point X="-3.039159667969" Y="-26.566068359375" />
                  <Point X="-3.016268066406" Y="-26.54343359375" />
                  <Point X="-3.002975585938" Y="-26.526978515625" />
                  <Point X="-2.984408203125" Y="-26.497876953125" />
                  <Point X="-2.976457519531" Y="-26.4824765625" />
                  <Point X="-2.968679931641" Y="-26.463294921875" />
                  <Point X="-2.960825927734" Y="-26.439541015625" />
                  <Point X="-2.954186767578" Y="-26.413908203125" />
                  <Point X="-2.951953369141" Y="-26.402400390625" />
                  <Point X="-2.947838623047" Y="-26.37091796875" />
                  <Point X="-2.947081787109" Y="-26.355703125" />
                  <Point X="-2.947780517578" Y="-26.3328359375" />
                  <Point X="-2.951227783203" Y="-26.31021875" />
                  <Point X="-2.957372802734" Y="-26.28818359375" />
                  <Point X="-2.961110595703" Y="-26.277353515625" />
                  <Point X="-2.973112792969" Y="-26.248373046875" />
                  <Point X="-2.9785234375" Y="-26.237375" />
                  <Point X="-2.990719726562" Y="-26.21616015625" />
                  <Point X="-3.005551757812" Y="-26.1966875" />
                  <Point X="-3.022762207031" Y="-26.179294921875" />
                  <Point X="-3.031926269531" Y="-26.171158203125" />
                  <Point X="-3.048095703125" Y="-26.158568359375" />
                  <Point X="-3.06845703125" Y="-26.144732421875" />
                  <Point X="-3.091277099609" Y="-26.131302734375" />
                  <Point X="-3.105446777344" Y="-26.124474609375" />
                  <Point X="-3.134708740234" Y="-26.11325390625" />
                  <Point X="-3.149801025391" Y="-26.108861328125" />
                  <Point X="-3.181686523438" Y="-26.102380859375" />
                  <Point X="-3.197298095703" Y="-26.10053515625" />
                  <Point X="-3.228619140625" Y="-26.099443359375" />
                  <Point X="-3.244328613281" Y="-26.100197265625" />
                  <Point X="-4.646963867188" Y="-26.284857421875" />
                  <Point X="-4.660920410156" Y="-26.2866953125" />
                  <Point X="-4.740762695312" Y="-25.9741171875" />
                  <Point X="-4.749664550781" Y="-25.911875" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.713485839844" Y="-25.36715625" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.498832275391" Y="-25.3091015625" />
                  <Point X="-3.471225097656" Y="-25.297947265625" />
                  <Point X="-3.451819580078" Y="-25.2880625" />
                  <Point X="-3.429726318359" Y="-25.2748515625" />
                  <Point X="-3.405811523438" Y="-25.25825390625" />
                  <Point X="-3.396479492188" Y="-25.25087109375" />
                  <Point X="-3.372523681641" Y="-25.22934375" />
                  <Point X="-3.358448486328" Y="-25.213521484375" />
                  <Point X="-3.338496826172" Y="-25.185298828125" />
                  <Point X="-3.329815185547" Y="-25.170271484375" />
                  <Point X="-3.321110107422" Y="-25.151412109375" />
                  <Point X="-3.312159423828" Y="-25.128107421875" />
                  <Point X="-3.304187744141" Y="-25.102423828125" />
                  <Point X="-3.300738769531" Y="-25.086724609375" />
                  <Point X="-3.296537109375" Y="-25.05496875" />
                  <Point X="-3.295784423828" Y="-25.038912109375" />
                  <Point X="-3.297063476562" Y="-25.005146484375" />
                  <Point X="-3.298355224609" Y="-24.9927265625" />
                  <Point X="-3.302556396484" Y="-24.9681640625" />
                  <Point X="-3.305465820312" Y="-24.956021484375" />
                  <Point X="-3.3134375" Y="-24.9303359375" />
                  <Point X="-3.317670410156" Y="-24.9192109375" />
                  <Point X="-3.330994140625" Y="-24.889873046875" />
                  <Point X="-3.341913085938" Y="-24.871599609375" />
                  <Point X="-3.362983642578" Y="-24.843931640625" />
                  <Point X="-3.374796142578" Y="-24.831068359375" />
                  <Point X="-3.390446533203" Y="-24.816896484375" />
                  <Point X="-3.409638916016" Y="-24.801650390625" />
                  <Point X="-3.433553710938" Y="-24.78505078125" />
                  <Point X="-3.440474609375" Y="-24.780677734375" />
                  <Point X="-3.465603271484" Y="-24.767171875" />
                  <Point X="-3.4965625" Y="-24.754365234375" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.7854453125" Y="-24.408173828125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.713408691406" Y="-23.976333984375" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-3.925157226562" Y="-23.77503125" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.767738037109" Y="-23.79518359375" />
                  <Point X="-3.747056640625" Y="-23.795634765625" />
                  <Point X="-3.736703369141" Y="-23.795296875" />
                  <Point X="-3.715141845703" Y="-23.79341015625" />
                  <Point X="-3.704891845703" Y="-23.791947265625" />
                  <Point X="-3.684612792969" Y="-23.7879140625" />
                  <Point X="-3.666088867188" Y="-23.782666015625" />
                  <Point X="-3.613157714844" Y="-23.7659765625" />
                  <Point X="-3.603461914062" Y="-23.762328125" />
                  <Point X="-3.584527587891" Y="-23.753998046875" />
                  <Point X="-3.5752890625" Y="-23.74931640625" />
                  <Point X="-3.556544921875" Y="-23.73849609375" />
                  <Point X="-3.547869873047" Y="-23.7328359375" />
                  <Point X="-3.531184814453" Y="-23.720603515625" />
                  <Point X="-3.5159296875" Y="-23.706626953125" />
                  <Point X="-3.502288574219" Y="-23.691072265625" />
                  <Point X="-3.495892578125" Y="-23.682921875" />
                  <Point X="-3.483478271484" Y="-23.66519140625" />
                  <Point X="-3.478009277344" Y="-23.656396484375" />
                  <Point X="-3.468062744141" Y="-23.638265625" />
                  <Point X="-3.460176513672" Y="-23.620701171875" />
                  <Point X="-3.438937744141" Y="-23.56942578125" />
                  <Point X="-3.435500244141" Y="-23.55965234375" />
                  <Point X="-3.429710693359" Y="-23.5397890625" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436012207031" Y="-23.39546875" />
                  <Point X="-3.443509521484" Y="-23.376189453125" />
                  <Point X="-3.451897460938" Y="-23.358853515625" />
                  <Point X="-3.477524414062" Y="-23.309625" />
                  <Point X="-3.482801757812" Y="-23.3007109375" />
                  <Point X="-3.494293212891" Y="-23.283513671875" />
                  <Point X="-3.500507324219" Y="-23.27523046875" />
                  <Point X="-3.514419677734" Y="-23.258650390625" />
                  <Point X="-3.521497802734" Y="-23.251091796875" />
                  <Point X="-3.536439697266" Y="-23.236787109375" />
                  <Point X="-3.544303466797" Y="-23.230041015625" />
                  <Point X="-4.227614746094" Y="-22.70571875" />
                  <Point X="-4.002298095703" Y="-22.319697265625" />
                  <Point X="-3.954821777344" Y="-22.258671875" />
                  <Point X="-3.726336669922" Y="-21.964986328125" />
                  <Point X="-3.344496337891" Y="-22.18544140625" />
                  <Point X="-3.254157226562" Y="-22.237599609375" />
                  <Point X="-3.244923828125" Y="-22.242279296875" />
                  <Point X="-3.225995605469" Y="-22.250609375" />
                  <Point X="-3.21630078125" Y="-22.254259765625" />
                  <Point X="-3.195658203125" Y="-22.26076953125" />
                  <Point X="-3.185623779297" Y="-22.263341796875" />
                  <Point X="-3.165332275391" Y="-22.26737890625" />
                  <Point X="-3.143244140625" Y="-22.26987890625" />
                  <Point X="-3.069525878906" Y="-22.276328125" />
                  <Point X="-3.059174072266" Y="-22.276666015625" />
                  <Point X="-3.038489013672" Y="-22.27621484375" />
                  <Point X="-3.028155761719" Y="-22.27542578125" />
                  <Point X="-3.006697265625" Y="-22.272599609375" />
                  <Point X="-2.996512451172" Y="-22.2706875" />
                  <Point X="-2.976423339844" Y="-22.26576953125" />
                  <Point X="-2.956999267578" Y="-22.25869921875" />
                  <Point X="-2.938449951172" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.902745605469" Y="-22.226806640625" />
                  <Point X="-2.886611816406" Y="-22.213857421875" />
                  <Point X="-2.870500976562" Y="-22.198544921875" />
                  <Point X="-2.818175292969" Y="-22.146220703125" />
                  <Point X="-2.811263427734" Y="-22.138509765625" />
                  <Point X="-2.798320068359" Y="-22.1223828125" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.749832519531" Y="-21.94376953125" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.76178125" Y="-21.8395078125" />
                  <Point X="-2.764353271484" Y="-21.82947265625" />
                  <Point X="-2.770862060547" Y="-21.808830078125" />
                  <Point X="-2.774510498047" Y="-21.79913671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522705078" Y="-21.770966796875" />
                  <Point X="-3.05938671875" Y="-21.300083984375" />
                  <Point X="-2.648366210938" Y="-20.984958984375" />
                  <Point X="-2.5736171875" Y="-20.943431640625" />
                  <Point X="-2.192524169922" Y="-20.731703125" />
                  <Point X="-2.146219726563" Y="-20.792048828125" />
                  <Point X="-2.118563964844" Y="-20.82808984375" />
                  <Point X="-2.111820556641" Y="-20.835951171875" />
                  <Point X="-2.097517578125" Y="-20.850892578125" />
                  <Point X="-2.089958007813" Y="-20.85797265625" />
                  <Point X="-2.073378417969" Y="-20.871884765625" />
                  <Point X="-2.065093017578" Y="-20.878099609375" />
                  <Point X="-2.047896972656" Y="-20.88958984375" />
                  <Point X="-2.025818847656" Y="-20.901720703125" />
                  <Point X="-1.943770751953" Y="-20.94443359375" />
                  <Point X="-1.934341064453" Y="-20.94870703125" />
                  <Point X="-1.915065429688" Y="-20.956205078125" />
                  <Point X="-1.905219360352" Y="-20.9594296875" />
                  <Point X="-1.8843125" Y="-20.965033203125" />
                  <Point X="-1.874174194336" Y="-20.967166015625" />
                  <Point X="-1.853724365234" Y="-20.970314453125" />
                  <Point X="-1.833053955078" Y="-20.971216796875" />
                  <Point X="-1.812407470703" Y="-20.96986328125" />
                  <Point X="-1.802120117188" Y="-20.968623046875" />
                  <Point X="-1.7808046875" Y="-20.96486328125" />
                  <Point X="-1.770716308594" Y="-20.962509765625" />
                  <Point X="-1.750860351562" Y="-20.956720703125" />
                  <Point X="-1.727377441406" Y="-20.947603515625" />
                  <Point X="-1.641918945312" Y="-20.912205078125" />
                  <Point X="-1.632581787109" Y="-20.9077265625" />
                  <Point X="-1.614451049805" Y="-20.897779296875" />
                  <Point X="-1.605657592773" Y="-20.892310546875" />
                  <Point X="-1.587927734375" Y="-20.879896484375" />
                  <Point X="-1.579778442383" Y="-20.8735" />
                  <Point X="-1.5642265625" Y="-20.859861328125" />
                  <Point X="-1.550252197266" Y="-20.844611328125" />
                  <Point X="-1.538020874023" Y="-20.8279296875" />
                  <Point X="-1.532361083984" Y="-20.819255859375" />
                  <Point X="-1.52153894043" Y="-20.80051171875" />
                  <Point X="-1.516855834961" Y="-20.791271484375" />
                  <Point X="-1.508525146484" Y="-20.772337890625" />
                  <Point X="-1.500413085938" Y="-20.748486328125" />
                  <Point X="-1.472597900391" Y="-20.660267578125" />
                  <Point X="-1.470026489258" Y="-20.650234375" />
                  <Point X="-1.465990966797" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266235352" Y="-20.43734375" />
                  <Point X="-0.931167236328" Y="-20.2836796875" />
                  <Point X="-0.840553894043" Y="-20.273072265625" />
                  <Point X="-0.365222503662" Y="-20.21744140625" />
                  <Point X="-0.252390960693" Y="-20.63853515625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451187134" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.378190795898" Y="-20.2149921875" />
                  <Point X="0.827866149902" Y="-20.2620859375" />
                  <Point X="0.902858886719" Y="-20.28019140625" />
                  <Point X="1.45360559082" Y="-20.41316015625" />
                  <Point X="1.500152954102" Y="-20.43004296875" />
                  <Point X="1.858247436523" Y="-20.55992578125" />
                  <Point X="1.905456298828" Y="-20.58200390625" />
                  <Point X="2.250453857422" Y="-20.74334765625" />
                  <Point X="2.296112060547" Y="-20.769947265625" />
                  <Point X="2.629422607422" Y="-20.964134765625" />
                  <Point X="2.672438476562" Y="-20.9947265625" />
                  <Point X="2.817779541016" Y="-21.098083984375" />
                  <Point X="2.184467529297" Y="-22.195013671875" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.061125" Y="-22.40952734375" />
                  <Point X="2.050172851562" Y="-22.43463671875" />
                  <Point X="2.042202270508" Y="-22.45782421875" />
                  <Point X="2.03833215332" Y="-22.4705078125" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017456054688" Y="-22.55155859375" />
                  <Point X="2.014233398437" Y="-22.5755" />
                  <Point X="2.013386230469" Y="-22.58757421875" />
                  <Point X="2.013206298828" Y="-22.616076171875" />
                  <Point X="2.014568725586" Y="-22.640021484375" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144042969" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045317626953" Y="-22.776109375" />
                  <Point X="2.055678710937" Y="-22.79615234375" />
                  <Point X="2.067397949219" Y="-22.814623046875" />
                  <Point X="2.104414794922" Y="-22.86917578125" />
                  <Point X="2.111907470703" Y="-22.8788203125" />
                  <Point X="2.128064453125" Y="-22.897064453125" />
                  <Point X="2.136728759766" Y="-22.9056640625" />
                  <Point X="2.158783447266" Y="-22.92490625" />
                  <Point X="2.177010986328" Y="-22.9389609375" />
                  <Point X="2.231564453125" Y="-22.975978515625" />
                  <Point X="2.241285888672" Y="-22.981759765625" />
                  <Point X="2.261343505859" Y="-22.992126953125" />
                  <Point X="2.2716796875" Y="-22.996712890625" />
                  <Point X="2.293776123047" Y="-23.004978515625" />
                  <Point X="2.304578613281" Y="-23.00830078125" />
                  <Point X="2.326494140625" Y="-23.013638671875" />
                  <Point X="2.347199707031" Y="-23.016810546875" />
                  <Point X="2.4070234375" Y="-23.024025390625" />
                  <Point X="2.419466308594" Y="-23.024703125" />
                  <Point X="2.444337402344" Y="-23.024421875" />
                  <Point X="2.456765625" Y="-23.023462890625" />
                  <Point X="2.486772216797" Y="-23.019154296875" />
                  <Point X="2.508849853516" Y="-23.014634765625" />
                  <Point X="2.578032958984" Y="-22.996134765625" />
                  <Point X="2.583989990234" Y="-22.994330078125" />
                  <Point X="2.604414550781" Y="-22.9869296875" />
                  <Point X="2.627662109375" Y="-22.976421875" />
                  <Point X="2.636033691406" Y="-22.972125" />
                  <Point X="3.92202734375" Y="-22.22965625" />
                  <Point X="3.940403564453" Y="-22.219046875" />
                  <Point X="4.043948486328" Y="-22.362951171875" />
                  <Point X="4.067929199219" Y="-22.402580078125" />
                  <Point X="4.136883789062" Y="-22.516529296875" />
                  <Point X="3.329835449219" Y="-23.135796875" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.165823242188" Y="-23.26223046875" />
                  <Point X="3.145702880859" Y="-23.28171484375" />
                  <Point X="3.128467041016" Y="-23.301388671875" />
                  <Point X="3.120584716797" Y="-23.311001953125" />
                  <Point X="3.070793457031" Y="-23.375958984375" />
                  <Point X="3.063917724609" Y="-23.386255859375" />
                  <Point X="3.051563964844" Y="-23.407654296875" />
                  <Point X="3.0460859375" Y="-23.418755859375" />
                  <Point X="3.034640136719" Y="-23.4463515625" />
                  <Point X="3.0271640625" Y="-23.46797265625" />
                  <Point X="3.008616699219" Y="-23.53429296875" />
                  <Point X="3.006225585938" Y="-23.545337890625" />
                  <Point X="3.002771972656" Y="-23.56763671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003078125" Y="-23.63624609375" />
                  <Point X="3.007143554688" Y="-23.659275390625" />
                  <Point X="3.022368896484" Y="-23.73306640625" />
                  <Point X="3.025692138672" Y="-23.745107421875" />
                  <Point X="3.033889892578" Y="-23.768650390625" />
                  <Point X="3.038764404297" Y="-23.78015234375" />
                  <Point X="3.052608398438" Y="-23.8076328125" />
                  <Point X="3.063564941406" Y="-23.826578125" />
                  <Point X="3.1049765625" Y="-23.889521484375" />
                  <Point X="3.111740722656" Y="-23.898578125" />
                  <Point X="3.126297607422" Y="-23.915826171875" />
                  <Point X="3.134090332031" Y="-23.924017578125" />
                  <Point X="3.151333984375" Y="-23.94010546875" />
                  <Point X="3.160040283203" Y="-23.94730859375" />
                  <Point X="3.178243652344" Y="-23.96062890625" />
                  <Point X="3.197371826172" Y="-23.97216796875" />
                  <Point X="3.2573828125" Y="-24.00594921875" />
                  <Point X="3.268888427734" Y="-24.0114453125" />
                  <Point X="3.292537353516" Y="-24.02084765625" />
                  <Point X="3.304680664062" Y="-24.02475390625" />
                  <Point X="3.335678955078" Y="-24.03247265625" />
                  <Point X="3.356695556641" Y="-24.03646484375" />
                  <Point X="3.437834228516" Y="-24.0471875" />
                  <Point X="3.444033447266" Y="-24.04780078125" />
                  <Point X="3.465708740234" Y="-24.04877734375" />
                  <Point X="3.491213623047" Y="-24.04796875" />
                  <Point X="3.500603271484" Y="-24.047203125" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.085763671875" />
                  <Point X="4.760239746094" Y="-24.134298828125" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="3.871784179688" Y="-24.530470703125" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.682916992188" Y="-24.58157421875" />
                  <Point X="3.656385742188" Y="-24.592138671875" />
                  <Point X="3.631511962891" Y="-24.604541015625" />
                  <Point X="3.621213134766" Y="-24.610076171875" />
                  <Point X="3.541496826172" Y="-24.65615234375" />
                  <Point X="3.531003417969" Y="-24.663189453125" />
                  <Point X="3.511043945312" Y="-24.67858984375" />
                  <Point X="3.501577880859" Y="-24.686953125" />
                  <Point X="3.479588867188" Y="-24.70915625" />
                  <Point X="3.465119140625" Y="-24.72555859375" />
                  <Point X="3.417289550781" Y="-24.786505859375" />
                  <Point X="3.410854980469" Y="-24.79579296875" />
                  <Point X="3.399131835938" Y="-24.8150703125" />
                  <Point X="3.393843261719" Y="-24.825060546875" />
                  <Point X="3.384069580078" Y="-24.8465234375" />
                  <Point X="3.380005615234" Y="-24.8570703125" />
                  <Point X="3.373158935547" Y="-24.878572265625" />
                  <Point X="3.367817382812" Y="-24.902888671875" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350321777344" Y="-24.99879296875" />
                  <Point X="3.348925292969" Y="-25.0241953125" />
                  <Point X="3.349081054688" Y="-25.036943359375" />
                  <Point X="3.351639892578" Y="-25.069111328125" />
                  <Point X="3.354432861328" Y="-25.089783203125" />
                  <Point X="3.370376220703" Y="-25.173033203125" />
                  <Point X="3.373158935547" Y="-25.18398828125" />
                  <Point X="3.380004394531" Y="-25.205486328125" />
                  <Point X="3.384067138672" Y="-25.216029296875" />
                  <Point X="3.393840820312" Y="-25.237494140625" />
                  <Point X="3.399128417969" Y="-25.247484375" />
                  <Point X="3.410852783203" Y="-25.266765625" />
                  <Point X="3.424965576172" Y="-25.285837890625" />
                  <Point X="3.472795410156" Y="-25.34678515625" />
                  <Point X="3.481510253906" Y="-25.356447265625" />
                  <Point X="3.500181396484" Y="-25.3744921875" />
                  <Point X="3.510137695312" Y="-25.382875" />
                  <Point X="3.537243408203" Y="-25.402689453125" />
                  <Point X="3.554286865234" Y="-25.413798828125" />
                  <Point X="3.634003173828" Y="-25.459876953125" />
                  <Point X="3.639487060547" Y="-25.4628125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027832031" Y="-25.481029296875" />
                  <Point X="3.6919921875" Y="-25.483916015625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761613769531" Y="-25.931052734375" />
                  <Point X="4.751932617188" Y="-25.9734765625" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.645290771484" Y="-25.936705078125" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400096923828" Y="-25.90804296875" />
                  <Point X="3.366721923828" Y="-25.910841796875" />
                  <Point X="3.354485107422" Y="-25.912677734375" />
                  <Point X="3.329375732422" Y="-25.918134765625" />
                  <Point X="3.172920898438" Y="-25.952140625" />
                  <Point X="3.157877441406" Y="-25.9567421875" />
                  <Point X="3.128754638672" Y="-25.9683671875" />
                  <Point X="3.114675292969" Y="-25.975390625" />
                  <Point X="3.086848632812" Y="-25.992283203125" />
                  <Point X="3.074124023438" Y="-26.00153125" />
                  <Point X="3.050373779297" Y="-26.022001953125" />
                  <Point X="3.039348144531" Y="-26.033224609375" />
                  <Point X="3.024170898438" Y="-26.051478515625" />
                  <Point X="2.929604003906" Y="-26.165212890625" />
                  <Point X="2.921325195312" Y="-26.17684765625" />
                  <Point X="2.90660546875" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.872981933594" Y="-26.320298828125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876787109375" Y="-26.576671875" />
                  <Point X="2.889159667969" Y="-26.605484375" />
                  <Point X="2.896542724609" Y="-26.619376953125" />
                  <Point X="2.910438720703" Y="-26.640990234375" />
                  <Point X="2.997022949219" Y="-26.775666015625" />
                  <Point X="3.001743896484" Y="-26.78235546875" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="4.087171386719" Y="-27.629984375" />
                  <Point X="4.045493164062" Y="-27.69742578125" />
                  <Point X="4.025467773437" Y="-27.725876953125" />
                  <Point X="4.001273681641" Y="-27.76025390625" />
                  <Point X="3.034159667969" Y="-27.201890625" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841191650391" Y="-27.09088671875" />
                  <Point X="2.815024902344" Y="-27.079515625" />
                  <Point X="2.783118408203" Y="-27.069328125" />
                  <Point X="2.771104980469" Y="-27.066337890625" />
                  <Point X="2.741220703125" Y="-27.06094140625" />
                  <Point X="2.555014648438" Y="-27.0273125" />
                  <Point X="2.539357666016" Y="-27.02580859375" />
                  <Point X="2.508009033203" Y="-27.025404296875" />
                  <Point X="2.492317382813" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444847900391" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400587646484" Y="-27.051109375" />
                  <Point X="2.375760986328" Y="-27.06417578125" />
                  <Point X="2.221069580078" Y="-27.145587890625" />
                  <Point X="2.208965332031" Y="-27.153171875" />
                  <Point X="2.186036865234" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144941650391" Y="-27.21116015625" />
                  <Point X="2.128047363281" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.107398193359" Y="-27.271021484375" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.0021875" Y="-27.580140625" />
                  <Point X="2.007584472656" Y="-27.610025390625" />
                  <Point X="2.041213134766" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723752685547" Y="-29.036083984375" />
                  <Point X="1.975625" Y="-28.061103515625" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828658813477" Y="-27.87015234375" />
                  <Point X="1.808835327148" Y="-27.84962890625" />
                  <Point X="1.783252929688" Y="-27.82800390625" />
                  <Point X="1.773299560547" Y="-27.820646484375" />
                  <Point X="1.743825683594" Y="-27.801697265625" />
                  <Point X="1.560176025391" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517470092773" Y="-27.663873046875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926513672" Y="-27.65115234375" />
                  <Point X="1.455391601563" Y="-27.6486953125" />
                  <Point X="1.424129760742" Y="-27.646376953125" />
                  <Point X="1.408402832031" Y="-27.646515625" />
                  <Point X="1.37616784668" Y="-27.64948046875" />
                  <Point X="1.175315795898" Y="-27.667962890625" />
                  <Point X="1.1612265625" Y="-27.670337890625" />
                  <Point X="1.133571533203" Y="-27.677171875" />
                  <Point X="1.120005859375" Y="-27.681630859375" />
                  <Point X="1.092620605469" Y="-27.692974609375" />
                  <Point X="1.079877319336" Y="-27.6994140625" />
                  <Point X="1.055494018555" Y="-27.714134765625" />
                  <Point X="1.043854248047" Y="-27.722416015625" />
                  <Point X="1.018963195801" Y="-27.74311328125" />
                  <Point X="0.863870483398" Y="-27.872068359375" />
                  <Point X="0.852649291992" Y="-27.88309375" />
                  <Point X="0.832181091309" Y="-27.906841796875" />
                  <Point X="0.822933776855" Y="-27.919564453125" />
                  <Point X="0.806041625977" Y="-27.947390625" />
                  <Point X="0.799019470215" Y="-27.96146875" />
                  <Point X="0.787394470215" Y="-27.99058984375" />
                  <Point X="0.782791625977" Y="-28.0056328125" />
                  <Point X="0.775349487305" Y="-28.039873046875" />
                  <Point X="0.728977355957" Y="-28.25321875" />
                  <Point X="0.727584594727" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091430664" Y="-29.15233984375" />
                  <Point X="0.710463256836" Y="-28.694685546875" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146057129" Y="-28.45358203125" />
                  <Point X="0.62678692627" Y="-28.423814453125" />
                  <Point X="0.62040625" Y="-28.41320703125" />
                  <Point X="0.597763549805" Y="-28.380583984375" />
                  <Point X="0.456678649902" Y="-28.177306640625" />
                  <Point X="0.446669494629" Y="-28.165169921875" />
                  <Point X="0.424783569336" Y="-28.142712890625" />
                  <Point X="0.412906524658" Y="-28.132392578125" />
                  <Point X="0.386651550293" Y="-28.113150390625" />
                  <Point X="0.373236694336" Y="-28.10493359375" />
                  <Point X="0.345234649658" Y="-28.090826171875" />
                  <Point X="0.330647460938" Y="-28.084935546875" />
                  <Point X="0.295609039307" Y="-28.0740625" />
                  <Point X="0.077288223267" Y="-28.006302734375" />
                  <Point X="0.063372219086" Y="-28.003109375" />
                  <Point X="0.035222892761" Y="-27.998841796875" />
                  <Point X="0.020989425659" Y="-27.997767578125" />
                  <Point X="-0.008652074814" Y="-27.997765625" />
                  <Point X="-0.022895795822" Y="-27.998837890625" />
                  <Point X="-0.051062511444" Y="-28.003107421875" />
                  <Point X="-0.06498550415" Y="-28.0063046875" />
                  <Point X="-0.100023651123" Y="-28.0171796875" />
                  <Point X="-0.318344604492" Y="-28.0849375" />
                  <Point X="-0.332933410645" Y="-28.090830078125" />
                  <Point X="-0.360934112549" Y="-28.104939453125" />
                  <Point X="-0.374346160889" Y="-28.11315625" />
                  <Point X="-0.400601287842" Y="-28.132400390625" />
                  <Point X="-0.412474609375" Y="-28.142716796875" />
                  <Point X="-0.434358734131" Y="-28.165171875" />
                  <Point X="-0.444369384766" Y="-28.177310546875" />
                  <Point X="-0.467012237549" Y="-28.209935546875" />
                  <Point X="-0.608097106934" Y="-28.4132109375" />
                  <Point X="-0.612471130371" Y="-28.4201328125" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.985425231934" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662769005896" Y="-28.956608384526" />
                  <Point X="2.689517047353" Y="-28.947398295251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601785326245" Y="-28.877132784677" />
                  <Point X="2.64112792753" Y="-28.863586040676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.540801646593" Y="-28.797657184828" />
                  <Point X="2.592738807708" Y="-28.779773786101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479817966942" Y="-28.71818158498" />
                  <Point X="2.544349687885" Y="-28.695961531526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.418834287291" Y="-28.638705985131" />
                  <Point X="2.495960568063" Y="-28.612149276951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.08682920946" Y="-29.745328765251" />
                  <Point X="-0.9687786469" Y="-29.704680696797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.816387189522" Y="-29.089998805016" />
                  <Point X="0.824515603003" Y="-29.087199967802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.35785060764" Y="-28.559230385282" />
                  <Point X="2.44757144824" Y="-28.528337022376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.13190503719" Y="-29.660375652718" />
                  <Point X="-0.939120412489" Y="-29.593994583015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791739325591" Y="-28.998011780463" />
                  <Point X="0.811861581099" Y="-28.99108313225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.296866927989" Y="-28.479754785433" />
                  <Point X="2.399182328417" Y="-28.444524767801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119791498396" Y="-29.555730662103" />
                  <Point X="-0.909462178077" Y="-29.483308469233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.767091461661" Y="-28.90602475591" />
                  <Point X="0.799207559195" Y="-28.894966296698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.235883248338" Y="-28.400279185584" />
                  <Point X="2.350793208595" Y="-28.360712513226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.131006725915" Y="-29.459118409915" />
                  <Point X="-0.879803943666" Y="-29.372622355451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.74244359773" Y="-28.814037731358" />
                  <Point X="0.786553537291" Y="-28.798849461146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.174899568687" Y="-28.320803585735" />
                  <Point X="2.302404088772" Y="-28.276900258651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.927146504851" Y="-27.717456580312" />
                  <Point X="4.061758622823" Y="-27.671105911011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.149711152582" Y="-29.365084895794" />
                  <Point X="-0.850145709254" Y="-29.261936241669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.717795733799" Y="-28.722050706805" />
                  <Point X="0.773899515387" Y="-28.702732625594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.113915889036" Y="-28.241327985887" />
                  <Point X="2.25401496895" Y="-28.193088004076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.818134456984" Y="-27.654518473861" />
                  <Point X="4.025908937474" Y="-27.582975982891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.168415583486" Y="-29.271051383133" />
                  <Point X="-0.820487474842" Y="-29.151250127887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.693147931046" Y="-28.630063661187" />
                  <Point X="0.761245493483" Y="-28.606615790042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.052932209384" Y="-28.161852386038" />
                  <Point X="2.205625849127" Y="-28.109275749501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.709122409117" Y="-27.59158036741" />
                  <Point X="3.93552666052" Y="-27.513623131886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.188493327606" Y="-29.177490740133" />
                  <Point X="-0.790829240431" Y="-29.040564014105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.668500154201" Y="-28.538076606648" />
                  <Point X="0.748591471579" Y="-28.51049895449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.991948529733" Y="-28.082376786189" />
                  <Point X="2.157236729305" Y="-28.025463494926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.60011036125" Y="-27.528642260959" />
                  <Point X="3.845144383566" Y="-27.44427028088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.232720720485" Y="-29.092245488052" />
                  <Point X="-0.761171006019" Y="-28.929877900323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.639120797204" Y="-28.44771876581" />
                  <Point X="0.735937449675" Y="-28.414382118938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.930964672382" Y="-28.002901247527" />
                  <Point X="2.108847609482" Y="-27.94165124035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491098313383" Y="-27.465704154508" />
                  <Point X="3.754762106613" Y="-27.374917429875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.308718083487" Y="-29.017939513958" />
                  <Point X="-0.731512771608" Y="-28.819191786541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.587064834205" Y="-28.365169106594" />
                  <Point X="0.724739330526" Y="-28.317763975865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.86998075008" Y="-27.92342573123" />
                  <Point X="2.061932938075" Y="-27.857331292472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382086265517" Y="-27.402766048057" />
                  <Point X="3.664379829659" Y="-27.305564578869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.390985382921" Y="-28.945792452112" />
                  <Point X="-0.701854537196" Y="-28.708505672759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.530781418753" Y="-28.284075075992" />
                  <Point X="0.737761299691" Y="-28.212806187589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.804108599825" Y="-27.845633366797" />
                  <Point X="2.035731403476" Y="-27.765879239632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.27307421765" Y="-27.339827941606" />
                  <Point X="3.573997552705" Y="-27.236211727864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.473252682355" Y="-28.873645390265" />
                  <Point X="-0.672196302785" Y="-28.597819558977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.474498003301" Y="-28.20298104539" />
                  <Point X="0.761366666741" Y="-28.10420424318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.70758197287" Y="-27.778396185162" />
                  <Point X="2.018648294738" Y="-27.671287460979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.164062169783" Y="-27.276889835155" />
                  <Point X="3.483615275751" Y="-27.166858876858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.53918465463" Y="-29.140201237495" />
                  <Point X="-2.46704807011" Y="-29.115362619516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.578772845616" Y="-28.809504931522" />
                  <Point X="-0.642473419742" Y="-28.487111184886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.404850774849" Y="-28.126488544622" />
                  <Point X="0.785965420912" Y="-27.995260248153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.60580810976" Y="-27.712965771829" />
                  <Point X="2.001847217398" Y="-27.576598571127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055050121916" Y="-27.213951728704" />
                  <Point X="3.393232998797" Y="-27.097506025853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.643463912353" Y="-29.075633500709" />
                  <Point X="-2.315052691065" Y="-28.962552448706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.800654991084" Y="-28.78543111639" />
                  <Point X="-0.573066262235" Y="-28.362738419284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.27967342594" Y="-28.069116597697" />
                  <Point X="0.872301144814" Y="-27.865058509688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.48414471681" Y="-27.654383872836" />
                  <Point X="2.009136012568" Y="-27.47361487297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.946038058075" Y="-27.151013627754" />
                  <Point X="3.302850721844" Y="-27.028153174847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.747720410646" Y="-29.011057927224" />
                  <Point X="-0.481432615954" Y="-28.23071245985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.1262067105" Y="-28.021485460831" />
                  <Point X="2.063506150389" Y="-27.354419768467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.835740892956" Y="-27.088518022659" />
                  <Point X="3.21246844489" Y="-26.958800323841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.837889742085" Y="-28.941631753197" />
                  <Point X="2.13038619609" Y="-27.230917157241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.664350297358" Y="-27.047058572669" />
                  <Point X="3.122086167936" Y="-26.889447472836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.928057661808" Y="-28.872205093078" />
                  <Point X="3.034492957245" Y="-26.819134269301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.95086117839" Y="-28.779583008805" />
                  <Point X="2.973807496748" Y="-26.739555984362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.878459101645" Y="-28.654179009809" />
                  <Point X="2.920919739706" Y="-26.657292734804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.635181361787" Y="-26.067025121919" />
                  <Point X="4.738720477965" Y="-26.031373745163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.806057024899" Y="-28.528775010813" />
                  <Point X="2.87539052955" Y="-26.572495734359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.424092100779" Y="-26.03923501864" />
                  <Point X="4.76289161658" Y="-25.922576989981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.733654948153" Y="-28.403371011817" />
                  <Point X="2.859339986549" Y="-26.47754841481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.21300283977" Y="-26.011444915362" />
                  <Point X="4.77886901972" Y="-25.816601564178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.661252871408" Y="-28.277967012822" />
                  <Point X="2.868032946184" Y="-26.374081224054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.001913578762" Y="-25.983654812083" />
                  <Point X="4.682483386517" Y="-25.749315834502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.588850794662" Y="-28.152563013826" />
                  <Point X="2.880837195349" Y="-26.269198402786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.790824317753" Y="-25.955864708804" />
                  <Point X="4.518384057658" Y="-25.705345800037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.516448717917" Y="-28.02715901483" />
                  <Point X="2.945075625122" Y="-26.146605372868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.579735578698" Y="-25.928074425803" />
                  <Point X="4.354284728798" Y="-25.661375765573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.444046642658" Y="-27.901755016346" />
                  <Point X="3.073804790367" Y="-26.001806401926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.295286387784" Y="-25.9255441721" />
                  <Point X="4.190185399938" Y="-25.617405731109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.371644839152" Y="-27.776351111435" />
                  <Point X="4.026086071079" Y="-25.573435696645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.317933617698" Y="-27.657382890032" />
                  <Point X="3.861986742219" Y="-25.529465662181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.315249473859" Y="-27.555984700477" />
                  <Point X="3.697887413359" Y="-25.485495627717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.825257100467" Y="-27.975448057884" />
                  <Point X="-3.564187096508" Y="-27.885554446519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.353886456277" Y="-27.468814515705" />
                  <Point X="3.576695257497" Y="-25.426751468782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.885851643414" Y="-27.895838467522" />
                  <Point X="-3.133010225406" Y="-27.636614378874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.427447783593" Y="-27.393669747257" />
                  <Point X="3.483497656383" Y="-25.358368011625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.946446186362" Y="-27.816228877161" />
                  <Point X="-2.701833354304" Y="-27.387674311229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.593719755374" Y="-27.350447813745" />
                  <Point X="3.4203719404" Y="-25.279629974034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.006497944013" Y="-27.736432390834" />
                  <Point X="3.376437297257" Y="-25.194283920136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056209319007" Y="-27.653075425227" />
                  <Point X="3.356517593251" Y="-25.100668859561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.105920133885" Y="-27.569718266755" />
                  <Point X="3.350123674691" Y="-25.002396497565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.155630948763" Y="-27.486361108284" />
                  <Point X="3.369494806797" Y="-24.895252517168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.066228279982" Y="-27.355103336008" />
                  <Point X="3.426090587918" Y="-24.775291062219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.828700971277" Y="-27.172842159998" />
                  <Point X="3.634468661221" Y="-24.603066772864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.752397459891" Y="-24.56246063108" />
                  <Point X="4.77222907757" Y="-24.211304444208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.591173662572" Y="-26.990580983988" />
                  <Point X="4.75738181968" Y="-24.115942800368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.353646353868" Y="-26.808319807978" />
                  <Point X="4.737257159681" Y="-24.022398311801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.116116557177" Y="-26.626057775285" />
                  <Point X="4.714689090233" Y="-23.929695156578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.974110041877" Y="-26.476687046088" />
                  <Point X="4.441295240892" Y="-23.923358243497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947648791494" Y="-26.367101742186" />
                  <Point X="3.96886487461" Y="-23.985555099251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963318146916" Y="-26.272023169228" />
                  <Point X="3.497229490812" Y="-24.047478220585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.013372274647" Y="-26.188784222852" />
                  <Point X="3.288005462364" Y="-24.01904586623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.11157573045" Y="-26.122124419693" />
                  <Point X="3.174282057725" Y="-23.957730010012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.398725128447" Y="-26.12052392185" />
                  <Point X="3.100469900652" Y="-23.882671609176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.871154028755" Y="-26.182720272829" />
                  <Point X="3.048763901779" Y="-23.800001447648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.343582929063" Y="-26.244916623808" />
                  <Point X="3.01765867224" Y="-23.710237872383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.66866459604" Y="-26.25637725361" />
                  <Point X="3.001312977998" Y="-23.615392181556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692254063707" Y="-26.164025793998" />
                  <Point X="3.015390737465" Y="-23.510070855526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.715843531373" Y="-26.071674334386" />
                  <Point X="3.059139087841" Y="-23.394533125743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.73943299904" Y="-25.979322874773" />
                  <Point X="3.176046515136" Y="-23.253804705614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.753686087677" Y="-25.883756642053" />
                  <Point X="3.413571173654" Y="-23.071544442136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.767381596735" Y="-25.787998419287" />
                  <Point X="3.651100293594" Y="-22.889282642468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781077105793" Y="-25.692240196521" />
                  <Point X="-4.270161172569" Y="-25.516317732642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.348047296175" Y="-25.198808462402" />
                  <Point X="3.888629413534" Y="-22.707020842799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300090030861" Y="-25.081821486984" />
                  <Point X="4.126158533474" Y="-22.524759043131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300289735169" Y="-24.981416285979" />
                  <Point X="2.390258248854" Y="-23.02200348033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.785155650003" Y="-22.886029400698" />
                  <Point X="4.088839584411" Y="-22.437135023079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.330349409887" Y="-24.891292697318" />
                  <Point X="2.231770685378" Y="-22.976101160085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.216332502049" Y="-22.637089339615" />
                  <Point X="4.037701532123" Y="-22.354269301859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.395175263101" Y="-24.813140063922" />
                  <Point X="2.138938394956" Y="-22.90759191637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.647509354094" Y="-22.388149278531" />
                  <Point X="3.979761547322" Y="-22.273745673827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.50649289067" Y="-24.750995832227" />
                  <Point X="2.076819147678" Y="-22.828507323811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.670382681545" Y="-24.706953648049" />
                  <Point X="2.032360625927" Y="-22.743341655784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.834481679945" Y="-24.662983499799" />
                  <Point X="2.01560748055" Y="-22.648636261634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.998580678345" Y="-24.619013351548" />
                  <Point X="2.018323121123" Y="-22.547227226884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.162679676745" Y="-24.575043203298" />
                  <Point X="2.049724552188" Y="-22.435940882359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.326778675145" Y="-24.531073055047" />
                  <Point X="2.116688959984" Y="-22.312409222934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.490877673545" Y="-24.487102906797" />
                  <Point X="2.189091245761" Y="-22.187005151963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.654976671944" Y="-24.443132758546" />
                  <Point X="2.261493176253" Y="-22.061601203327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782234818011" Y="-24.38647728754" />
                  <Point X="2.333895106744" Y="-21.93619725469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766569241915" Y="-24.2806092324" />
                  <Point X="2.406297037236" Y="-21.810793306054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750903665818" Y="-24.174741177259" />
                  <Point X="2.478698967727" Y="-21.685389357417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735238089722" Y="-24.068873122118" />
                  <Point X="-3.893834472559" Y="-23.779154622807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.465027024535" Y="-23.631504377668" />
                  <Point X="2.551100898219" Y="-21.559985408781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708790820591" Y="-23.959292632347" />
                  <Point X="-4.104923154159" Y="-23.751364320022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.425173924405" Y="-23.517307890105" />
                  <Point X="2.62350282871" Y="-21.434581460144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678762535473" Y="-23.848479099889" />
                  <Point X="-4.316011000276" Y="-23.723573729557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429320329368" Y="-23.418261647117" />
                  <Point X="2.695904759202" Y="-21.309177511508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.648734250356" Y="-23.73766556743" />
                  <Point X="-4.527098846394" Y="-23.695783139093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.466593965386" Y="-23.330622024533" />
                  <Point X="2.768306689693" Y="-21.183773562872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.52306410684" Y="-23.24959228885" />
                  <Point X="2.787632388879" Y="-21.076645226283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.610519612038" Y="-23.179231669511" />
                  <Point X="2.692438334317" Y="-21.008949203177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.700902080037" Y="-23.109878884287" />
                  <Point X="2.592783945892" Y="-20.942788996184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.791284548035" Y="-23.040526099064" />
                  <Point X="2.484389599763" Y="-20.879638197968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.881667016034" Y="-22.97117331384" />
                  <Point X="2.375995253634" Y="-20.816487399752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.972049484033" Y="-22.901820528617" />
                  <Point X="2.267600256734" Y="-20.753336825615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.062431952032" Y="-22.832467743393" />
                  <Point X="2.146289516165" Y="-20.694633498668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.15281442003" Y="-22.76311495817" />
                  <Point X="2.022552186959" Y="-20.636765713196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.214960949009" Y="-22.684039759454" />
                  <Point X="-3.028289433" Y="-22.275435988588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.918905662066" Y="-22.23777213581" />
                  <Point X="1.898814880023" Y="-20.578897920056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.141564017135" Y="-22.558293204367" />
                  <Point X="-3.235648217319" Y="-22.246361379175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.776457643554" Y="-22.088249384865" />
                  <Point X="1.762730917537" Y="-20.525281421353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.068167085261" Y="-22.432546649279" />
                  <Point X="-3.346747212568" Y="-22.184141866335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748726271477" Y="-21.978226742992" />
                  <Point X="1.620624454543" Y="-20.473738635876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.991347559407" Y="-22.305621600575" />
                  <Point X="-3.455759396641" Y="-22.121203806783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755406904982" Y="-21.880053104869" />
                  <Point X="1.478517880569" Y="-20.422195888612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.884578609097" Y="-22.168384138028" />
                  <Point X="-3.564771580714" Y="-22.058265747232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.779470358024" Y="-21.78786485151" />
                  <Point X="-0.127315781568" Y="-20.874654796123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.233652129722" Y="-20.750363576755" />
                  <Point X="1.312148212576" Y="-20.379007594603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.777808641116" Y="-22.03114632507" />
                  <Point X="-3.673783764788" Y="-21.995327687681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.826438149505" Y="-21.703563194339" />
                  <Point X="-0.197624382316" Y="-20.7983900241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.264507372004" Y="-20.63926530011" />
                  <Point X="1.140621034983" Y="-20.337595173565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874827147248" Y="-21.619750897729" />
                  <Point X="-0.233197919244" Y="-20.710165010454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.294165567374" Y="-20.528579199771" />
                  <Point X="0.96909385739" Y="-20.296182752528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.923216144992" Y="-21.535938601119" />
                  <Point X="-0.257845651173" Y="-20.618177940449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.323823762744" Y="-20.417893099432" />
                  <Point X="0.788342009534" Y="-20.257946640185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.971605142735" Y="-21.452126304508" />
                  <Point X="-0.282493463927" Y="-20.526190898275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.353481958114" Y="-20.307206999093" />
                  <Point X="0.564597138432" Y="-20.234514212925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.019994140479" Y="-21.368314007898" />
                  <Point X="-1.860907645898" Y="-20.969208521623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.577728272042" Y="-20.87170204369" />
                  <Point X="-0.30714127668" Y="-20.4342038561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.015159242501" Y="-21.266175254304" />
                  <Point X="-1.998304653444" Y="-20.916044140591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.499040739444" Y="-20.744133788682" />
                  <Point X="-0.331789089434" Y="-20.342216813926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.7772731307" Y="-21.08379053248" />
                  <Point X="-2.0983532031" Y="-20.850019654195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.466489088898" Y="-20.632451391828" />
                  <Point X="-0.356436902188" Y="-20.250229771751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.43047506108" Y="-20.863904416162" />
                  <Point X="-2.162013559021" Y="-20.771465707898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.466793723058" Y="-20.532082321069" />
                  <Point X="-0.649704882878" Y="-20.250736070884" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998373046875" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.5269375" Y="-28.743861328125" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.441675994873" Y="-28.488919921875" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.27433605957" Y="-28.266400390625" />
                  <Point X="0.239297592163" Y="-28.25552734375" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="-0.008664604187" Y="-28.187765625" />
                  <Point X="-0.043702919006" Y="-28.198640625" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.310921813965" Y="-28.318267578125" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.804409912109" Y="-29.8253515625" />
                  <Point X="-0.84774432373" Y="-29.987078125" />
                  <Point X="-1.10023046875" Y="-29.938068359375" />
                  <Point X="-1.139680664063" Y="-29.92791796875" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.315818969727" Y="-29.601693359375" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.318054077148" Y="-29.49267578125" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.418541503906" Y="-29.17433984375" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.692055541992" Y="-28.98295703125" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.025553955078" Y="-28.99762890625" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734375" Y="-29.157294921875" />
                  <Point X="-2.453527832031" Y="-29.4098515625" />
                  <Point X="-2.457094970703" Y="-29.4145" />
                  <Point X="-2.479401123047" Y="-29.400689453125" />
                  <Point X="-2.855839111328" Y="-29.167607421875" />
                  <Point X="-2.910440673828" Y="-29.12556640625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.608985595703" Y="-27.8074375" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499763183594" Y="-27.597587890625" />
                  <Point X="-2.516384521484" Y="-27.566359375" />
                  <Point X="-2.531328613281" Y="-27.551416015625" />
                  <Point X="-2.560157470703" Y="-27.53719921875" />
                  <Point X="-2.591683837891" Y="-27.543470703125" />
                  <Point X="-3.702765380859" Y="-28.184953125" />
                  <Point X="-3.842959228516" Y="-28.26589453125" />
                  <Point X="-3.864301513672" Y="-28.23785546875" />
                  <Point X="-4.161703613281" Y="-27.84712890625" />
                  <Point X="-4.2008515625" Y="-27.781484375" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.343919433594" Y="-26.561365234375" />
                  <Point X="-3.163787109375" Y="-26.423146484375" />
                  <Point X="-3.152534179688" Y="-26.41108203125" />
                  <Point X="-3.144756591797" Y="-26.391900390625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.136651611328" Y="-26.350052734375" />
                  <Point X="-3.148653808594" Y="-26.321072265625" />
                  <Point X="-3.164823242188" Y="-26.308482421875" />
                  <Point X="-3.187643310547" Y="-26.295052734375" />
                  <Point X="-3.219528808594" Y="-26.288572265625" />
                  <Point X="-4.6221640625" Y="-26.473232421875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.811077148438" Y="-26.4665625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.937750488281" Y="-25.938775390625" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.762661621094" Y="-25.18362890625" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.538058349609" Y="-25.11876171875" />
                  <Point X="-3.514143554688" Y="-25.1021640625" />
                  <Point X="-3.502324951172" Y="-25.090646484375" />
                  <Point X="-3.493619873047" Y="-25.071787109375" />
                  <Point X="-3.485648193359" Y="-25.046103515625" />
                  <Point X="-3.486927246094" Y="-25.012337890625" />
                  <Point X="-3.494898925781" Y="-24.98665234375" />
                  <Point X="-3.502328857422" Y="-24.971908203125" />
                  <Point X="-3.517979248047" Y="-24.957736328125" />
                  <Point X="-3.541894042969" Y="-24.94113671875" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.836032226562" Y="-24.591322265625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.992609375" Y="-24.51018359375" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.896794433594" Y="-23.926638671875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.900356933594" Y="-23.58665625" />
                  <Point X="-3.753265869141" Y="-23.60601953125" />
                  <Point X="-3.731704345703" Y="-23.6041328125" />
                  <Point X="-3.723209472656" Y="-23.601455078125" />
                  <Point X="-3.670278320312" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.5739453125" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.635711181641" Y="-23.547986328125" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.620428710938" Y="-23.446587890625" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968017578" Y="-23.380779296875" />
                  <Point X="-4.393358398438" Y="-22.818029296875" />
                  <Point X="-4.476105957031" Y="-22.75453515625" />
                  <Point X="-4.451298828125" Y="-22.712033203125" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.104784179688" Y="-22.14200390625" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.249496337891" Y="-22.0208984375" />
                  <Point X="-3.159157226562" Y="-22.073056640625" />
                  <Point X="-3.138514648438" Y="-22.07956640625" />
                  <Point X="-3.12668359375" Y="-22.0806015625" />
                  <Point X="-3.052965332031" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-3.004854980469" Y="-22.064197265625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.939109375" Y="-21.960328125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067626953" Y="-21.865966796875" />
                  <Point X="-3.277054931641" Y="-21.303072265625" />
                  <Point X="-3.307278808594" Y="-21.25072265625" />
                  <Point X="-3.260201171875" Y="-21.21462890625" />
                  <Point X="-2.752873535156" Y="-20.825666015625" />
                  <Point X="-2.665890136719" Y="-20.777341796875" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-1.995482421875" Y="-20.676384765625" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247192383" Y="-20.726337890625" />
                  <Point X="-1.938079345703" Y="-20.733193359375" />
                  <Point X="-1.85603112793" Y="-20.77590625" />
                  <Point X="-1.835124267578" Y="-20.781509765625" />
                  <Point X="-1.813808837891" Y="-20.77775" />
                  <Point X="-1.800093505859" Y="-20.772068359375" />
                  <Point X="-1.714634887695" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.681619140625" Y="-20.691353515625" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.688864990234" Y="-20.300931640625" />
                  <Point X="-1.689137695313" Y="-20.298859375" />
                  <Point X="-1.624857788086" Y="-20.280837890625" />
                  <Point X="-0.968082885742" Y="-20.096703125" />
                  <Point X="-0.862643249512" Y="-20.084361328125" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.068865089417" Y="-20.589359375" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282110214" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594032288" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.220971984863" Y="-20.06763671875" />
                  <Point X="0.2366484375" Y="-20.009130859375" />
                  <Point X="0.286755584717" Y="-20.01437890625" />
                  <Point X="0.860210144043" Y="-20.074435546875" />
                  <Point X="0.947450073242" Y="-20.095498046875" />
                  <Point X="1.508456054688" Y="-20.230943359375" />
                  <Point X="1.56493762207" Y="-20.2514296875" />
                  <Point X="1.931044555664" Y="-20.38421875" />
                  <Point X="1.985946166992" Y="-20.40989453125" />
                  <Point X="2.338685058594" Y="-20.574859375" />
                  <Point X="2.391753662109" Y="-20.605775390625" />
                  <Point X="2.732519775391" Y="-20.804306640625" />
                  <Point X="2.782555175781" Y="-20.839890625" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.349012451172" Y="-22.290013671875" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.2218828125" Y="-22.51958984375" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.203202392578" Y="-22.617275390625" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.224623046875" Y="-22.707943359375" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.283694580078" Y="-22.78173828125" />
                  <Point X="2.338248046875" Y="-22.818755859375" />
                  <Point X="2.360344482422" Y="-22.827021484375" />
                  <Point X="2.369937011719" Y="-22.828177734375" />
                  <Point X="2.429760742188" Y="-22.835392578125" />
                  <Point X="2.459767333984" Y="-22.831083984375" />
                  <Point X="2.528950439453" Y="-22.812583984375" />
                  <Point X="2.541033691406" Y="-22.80758203125" />
                  <Point X="3.82702734375" Y="-22.06511328125" />
                  <Point X="3.994247802734" Y="-21.968568359375" />
                  <Point X="4.000458740234" Y="-21.977201171875" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.230483886719" Y="-22.304212890625" />
                  <Point X="4.387512207031" Y="-22.563705078125" />
                  <Point X="3.4455" Y="-23.28653515625" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.271380126953" Y="-23.426591796875" />
                  <Point X="3.221588867188" Y="-23.491548828125" />
                  <Point X="3.210143066406" Y="-23.51914453125" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.193223144531" Y="-23.620876953125" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.222292480469" Y="-23.7221484375" />
                  <Point X="3.263704101562" Y="-23.785091796875" />
                  <Point X="3.280947753906" Y="-23.8011796875" />
                  <Point X="3.290578857422" Y="-23.8066015625" />
                  <Point X="3.35058984375" Y="-23.8403828125" />
                  <Point X="3.381588134766" Y="-23.8481015625" />
                  <Point X="3.462726806641" Y="-23.85882421875" />
                  <Point X="3.475803466797" Y="-23.858828125" />
                  <Point X="4.697411621094" Y="-23.698" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.852373535156" Y="-23.692005859375" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.947978027344" Y="-24.105068359375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="3.920959960938" Y="-24.713998046875" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.716293701172" Y="-24.774576171875" />
                  <Point X="3.636577392578" Y="-24.82065234375" />
                  <Point X="3.614588378906" Y="-24.84285546875" />
                  <Point X="3.566758789062" Y="-24.903802734375" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.554426269531" Y="-24.938626953125" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.541041748047" Y="-25.054044921875" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.158759765625" />
                  <Point X="3.574434814453" Y="-25.168541015625" />
                  <Point X="3.622264648438" Y="-25.22948828125" />
                  <Point X="3.649370361328" Y="-25.249302734375" />
                  <Point X="3.729086669922" Y="-25.295380859375" />
                  <Point X="3.741167480469" Y="-25.300388671875" />
                  <Point X="4.8614375" Y="-25.6005625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.996905273438" Y="-25.644884765625" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.937170898438" Y="-26.01574609375" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.620490966797" Y="-26.125080078125" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836181641" Y="-26.098341796875" />
                  <Point X="3.369726806641" Y="-26.103798828125" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.170268066406" Y="-26.172951171875" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.062182617188" Y="-26.337708984375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.070256591797" Y="-26.538236328125" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.208078125" Y="-27.483267578125" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.204134277344" Y="-27.802138671875" />
                  <Point X="4.180842285156" Y="-27.835232421875" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="2.939159667969" Y="-27.36643359375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.707456542969" Y="-27.24791796875" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.464251220703" Y="-27.2323125" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.275533935547" Y="-27.359509765625" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.194560058594" Y="-27.576259765625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.902150390625" Y="-28.935689453125" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.835293945312" Y="-29.19021484375" />
                  <Point X="2.809261474609" Y="-29.20706640625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.824887817383" Y="-28.176767578125" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.641075195312" Y="-27.961517578125" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.393569946289" Y="-27.83868359375" />
                  <Point X="1.192718017578" Y="-27.857166015625" />
                  <Point X="1.165332763672" Y="-27.868509765625" />
                  <Point X="1.140441894531" Y="-27.88920703125" />
                  <Point X="0.985349060059" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.961014587402" Y="-28.080228515625" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.103252197266" Y="-29.748767578125" />
                  <Point X="1.127642333984" Y="-29.934029296875" />
                  <Point X="0.994365478516" Y="-29.9632421875" />
                  <Point X="0.970288818359" Y="-29.9676171875" />
                  <Point X="0.860200378418" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#130" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.026724810296" Y="4.45486282851" Z="0.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.35" />
                  <Point X="-0.874611309453" Y="4.996579006201" Z="0.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.35" />
                  <Point X="-1.64451149723" Y="4.79858653125" Z="0.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.35" />
                  <Point X="-1.744593273738" Y="4.723824062682" Z="0.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.35" />
                  <Point X="-1.735461077846" Y="4.35496228114" Z="0.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.35" />
                  <Point X="-1.825383767052" Y="4.305405682645" Z="0.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.35" />
                  <Point X="-1.92114726002" Y="4.342436477748" Z="0.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.35" />
                  <Point X="-1.961970738725" Y="4.385332735032" Z="0.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.35" />
                  <Point X="-2.696329089521" Y="4.297646561323" Z="0.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.35" />
                  <Point X="-3.296779332648" Y="3.85633054839" Z="0.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.35" />
                  <Point X="-3.326511948198" Y="3.703207326067" Z="0.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.35" />
                  <Point X="-2.995075107491" Y="3.066594627268" Z="0.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.35" />
                  <Point X="-3.046365776264" Y="3.002437791622" Z="0.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.35" />
                  <Point X="-3.128481838338" Y="3.000489591682" Z="0.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.35" />
                  <Point X="-3.230652022231" Y="3.053682000547" Z="0.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.35" />
                  <Point X="-4.150403369361" Y="2.919979938693" Z="0.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.35" />
                  <Point X="-4.500636703905" Y="2.344451952853" Z="0.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.35" />
                  <Point X="-4.429952166585" Y="2.173583903414" Z="0.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.35" />
                  <Point X="-3.670935315214" Y="1.561605154344" Z="0.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.35" />
                  <Point X="-3.68806127846" Y="1.502429270054" Z="0.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.35" />
                  <Point X="-3.744401122251" Y="1.477510979042" Z="0.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.35" />
                  <Point X="-3.899986903585" Y="1.494197416192" Z="0.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.35" />
                  <Point X="-4.951210397175" Y="1.117720352367" Z="0.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.35" />
                  <Point X="-5.048226730641" Y="0.528415849207" Z="0.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.35" />
                  <Point X="-4.855129178227" Y="0.391660418985" Z="0.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.35" />
                  <Point X="-3.552645951152" Y="0.032470805903" Z="0.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.35" />
                  <Point X="-3.540836127606" Y="0.004122160573" Z="0.35" />
                  <Point X="-3.539556741714" Y="0" Z="0.35" />
                  <Point X="-3.547528473529" Y="-0.025684790488" Z="0.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.35" />
                  <Point X="-3.572722643971" Y="-0.046405177231" Z="0.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.35" />
                  <Point X="-3.781758610545" Y="-0.104051637771" Z="0.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.35" />
                  <Point X="-4.993402472326" Y="-0.91457263431" Z="0.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.35" />
                  <Point X="-4.865678069745" Y="-1.44765745822" Z="0.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.35" />
                  <Point X="-4.621793955354" Y="-1.49152368237" Z="0.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.35" />
                  <Point X="-3.196338721814" Y="-1.320294264643" Z="0.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.35" />
                  <Point X="-3.199315306688" Y="-1.348083249892" Z="0.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.35" />
                  <Point X="-3.380513139315" Y="-1.490417568134" Z="0.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.35" />
                  <Point X="-4.249950864735" Y="-2.775813816832" Z="0.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.35" />
                  <Point X="-3.910243386673" Y="-3.236858009084" Z="0.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.35" />
                  <Point X="-3.683921037946" Y="-3.196974187582" Z="0.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.35" />
                  <Point X="-2.557890190096" Y="-2.570440279036" Z="0.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.35" />
                  <Point X="-2.658442825586" Y="-2.751157344546" Z="0.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.35" />
                  <Point X="-2.947100586424" Y="-4.133902733659" Z="0.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.35" />
                  <Point X="-2.511878857058" Y="-4.411919802538" Z="0.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.35" />
                  <Point X="-2.420015771506" Y="-4.409008690214" Z="0.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.35" />
                  <Point X="-2.003931515021" Y="-4.007922022062" Z="0.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.35" />
                  <Point X="-1.70148156626" Y="-4.001569706729" Z="0.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.35" />
                  <Point X="-1.457664855938" Y="-4.180650191783" Z="0.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.35" />
                  <Point X="-1.373249194929" Y="-4.47115012252" Z="0.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.35" />
                  <Point X="-1.371547204816" Y="-4.563885783409" Z="0.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.35" />
                  <Point X="-1.15829536637" Y="-4.945062023096" Z="0.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.35" />
                  <Point X="-0.859119843613" Y="-5.005717017592" Z="0.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.35" />
                  <Point X="-0.762269578185" Y="-4.807012911453" Z="0.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.35" />
                  <Point X="-0.276001847191" Y="-3.315496588454" Z="0.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.35" />
                  <Point X="-0.035038364228" Y="-3.215113983659" Z="0.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.35" />
                  <Point X="0.218320715133" Y="-3.271998061629" Z="0.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.35" />
                  <Point X="0.394444012241" Y="-3.486149167314" Z="0.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.35" />
                  <Point X="0.472485282216" Y="-3.725523118363" Z="0.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.35" />
                  <Point X="0.973069700759" Y="-4.98553214805" Z="0.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.35" />
                  <Point X="1.152292198727" Y="-4.947182842649" Z="0.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.35" />
                  <Point X="1.146668507719" Y="-4.710962382299" Z="0.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.35" />
                  <Point X="1.003717761855" Y="-3.059566220629" Z="0.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.35" />
                  <Point X="1.166252850527" Y="-2.896371407481" Z="0.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.35" />
                  <Point X="1.391995665737" Y="-2.857192863956" Z="0.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.35" />
                  <Point X="1.607879931681" Y="-2.972296233301" Z="0.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.35" />
                  <Point X="1.779064068766" Y="-3.175925523405" Z="0.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.35" />
                  <Point X="2.830274619233" Y="-4.217759910361" Z="0.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.35" />
                  <Point X="3.020341740237" Y="-4.083808213405" Z="0.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.35" />
                  <Point X="2.939295634563" Y="-3.879409814613" Z="0.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.35" />
                  <Point X="2.237608194546" Y="-2.536092468884" Z="0.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.35" />
                  <Point X="2.313624185834" Y="-2.351516686487" Z="0.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.35" />
                  <Point X="2.481381691885" Y="-2.245277124133" Z="0.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.35" />
                  <Point X="2.692414334628" Y="-2.265839814944" Z="0.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.35" />
                  <Point X="2.908003782494" Y="-2.378453865711" Z="0.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.35" />
                  <Point X="4.215574890049" Y="-2.832729789052" Z="0.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.35" />
                  <Point X="4.377152344402" Y="-2.57603711776" Z="0.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.35" />
                  <Point X="4.232360033794" Y="-2.412319477663" Z="0.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.35" />
                  <Point X="3.106158459972" Y="-1.479916740266" Z="0.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.35" />
                  <Point X="3.105816217409" Y="-1.311011080133" Z="0.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.35" />
                  <Point X="3.202558442043" Y="-1.173637506752" Z="0.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.35" />
                  <Point X="3.374190370424" Y="-1.121378075896" Z="0.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.35" />
                  <Point X="3.607808601679" Y="-1.143371119" Z="0.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.35" />
                  <Point X="4.979761890586" Y="-0.995590790162" Z="0.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.35" />
                  <Point X="5.040190823224" Y="-0.621058187234" Z="0.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.35" />
                  <Point X="4.868222624575" Y="-0.52098604478" Z="0.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.35" />
                  <Point X="3.668236408749" Y="-0.174733191851" Z="0.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.35" />
                  <Point X="3.607612940144" Y="-0.106391834823" Z="0.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.35" />
                  <Point X="3.583993465527" Y="-0.013360780272" Z="0.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.35" />
                  <Point X="3.597377984898" Y="0.083249750967" Z="0.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.35" />
                  <Point X="3.647766498257" Y="0.157556903752" Z="0.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.35" />
                  <Point X="3.735159005603" Y="0.213415652391" Z="0.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.35" />
                  <Point X="3.927745192732" Y="0.268985887989" Z="0.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.35" />
                  <Point X="4.99122685215" Y="0.933903055015" Z="0.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.35" />
                  <Point X="4.894798568825" Y="1.35110153744" Z="0.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.35" />
                  <Point X="4.684729221671" Y="1.38285185157" Z="0.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.35" />
                  <Point X="3.381982612291" Y="1.232747564241" Z="0.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.35" />
                  <Point X="3.308949721004" Y="1.268249517055" Z="0.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.35" />
                  <Point X="3.257907128937" Y="1.336614573825" Z="0.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.35" />
                  <Point X="3.236035567458" Y="1.420506593018" Z="0.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.35" />
                  <Point X="3.252139324885" Y="1.498669876022" Z="0.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.35" />
                  <Point X="3.304907157347" Y="1.57427020939" Z="0.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.35" />
                  <Point X="3.469782219302" Y="1.705076470074" Z="0.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.35" />
                  <Point X="4.267105376629" Y="2.752954474234" Z="0.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.35" />
                  <Point X="4.034888172792" Y="3.083282441325" Z="0.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.35" />
                  <Point X="3.795871607025" Y="3.009467526794" Z="0.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.35" />
                  <Point X="2.440694866961" Y="2.248498318137" Z="0.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.35" />
                  <Point X="2.369767919447" Y="2.252742809346" Z="0.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.35" />
                  <Point X="2.305613368714" Y="2.290917246761" Z="0.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.35" />
                  <Point X="2.259841353474" Y="2.351411491669" Z="0.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.35" />
                  <Point X="2.246686893413" Y="2.419990524386" Z="0.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.35" />
                  <Point X="2.264029624521" Y="2.498774708921" Z="0.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.35" />
                  <Point X="2.386157826066" Y="2.71626747493" Z="0.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.35" />
                  <Point X="2.805376373065" Y="4.232138854149" Z="0.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.35" />
                  <Point X="2.410767608807" Y="4.468707427189" Z="0.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.35" />
                  <Point X="2.000971758119" Y="4.666677269479" Z="0.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.35" />
                  <Point X="1.57583027541" Y="4.826855655796" Z="0.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.35" />
                  <Point X="0.953028517914" Y="4.984385758182" Z="0.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.35" />
                  <Point X="0.285807541039" Y="5.066629216282" Z="0.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.35" />
                  <Point X="0.166519751257" Y="4.976584677441" Z="0.35" />
                  <Point X="0" Y="4.355124473572" Z="0.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>