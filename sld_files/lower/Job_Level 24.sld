<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#163" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1811" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.744867858887" Y="-29.19013671875" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557719848633" Y="-28.497138671875" />
                  <Point X="0.542362854004" Y="-28.467376953125" />
                  <Point X="0.468152404785" Y="-28.360455078125" />
                  <Point X="0.378635223389" Y="-28.2314765625" />
                  <Point X="0.356755706787" Y="-28.209025390625" />
                  <Point X="0.330500366211" Y="-28.189779296875" />
                  <Point X="0.302494873047" Y="-28.17566796875" />
                  <Point X="0.187658294678" Y="-28.14002734375" />
                  <Point X="0.049135734558" Y="-28.09703515625" />
                  <Point X="0.020976638794" Y="-28.092765625" />
                  <Point X="-0.00866468811" Y="-28.092765625" />
                  <Point X="-0.036823932648" Y="-28.09703515625" />
                  <Point X="-0.151660507202" Y="-28.13267578125" />
                  <Point X="-0.290183074951" Y="-28.17566796875" />
                  <Point X="-0.318185211182" Y="-28.18977734375" />
                  <Point X="-0.34444039917" Y="-28.209021484375" />
                  <Point X="-0.366323577881" Y="-28.2314765625" />
                  <Point X="-0.440534057617" Y="-28.338400390625" />
                  <Point X="-0.530051391602" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.770005432129" Y="-29.329900390625" />
                  <Point X="-0.916584594727" Y="-29.87694140625" />
                  <Point X="-0.919738342285" Y="-29.876330078125" />
                  <Point X="-1.079358276367" Y="-29.845345703125" />
                  <Point X="-1.209142578125" Y="-29.811955078125" />
                  <Point X="-1.24641809082" Y="-29.802365234375" />
                  <Point X="-1.236819458008" Y="-29.729455078125" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.243942993164" Y="-29.378302734375" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287938354492" Y="-29.182966796875" />
                  <Point X="-1.304010620117" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131205078125" />
                  <Point X="-1.429371582031" Y="-29.038484375" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.643027587891" Y="-28.890966796875" />
                  <Point X="-1.783350585938" Y="-28.88176953125" />
                  <Point X="-1.952616699219" Y="-28.87067578125" />
                  <Point X="-1.983414916992" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.159582275391" Y="-28.972927734375" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312790039062" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.458062988281" Y="-29.259708984375" />
                  <Point X="-2.480147460938" Y="-29.288490234375" />
                  <Point X="-2.567770751953" Y="-29.234236328125" />
                  <Point X="-2.80171484375" Y="-29.0893828125" />
                  <Point X="-2.981444580078" Y="-28.950998046875" />
                  <Point X="-3.104721679688" Y="-28.856078125" />
                  <Point X="-2.761180908203" Y="-28.261048828125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405575927734" Y="-27.585189453125" />
                  <Point X="-2.414561279297" Y="-27.555572265625" />
                  <Point X="-2.428779785156" Y="-27.5267421875" />
                  <Point X="-2.446808105469" Y="-27.501583984375" />
                  <Point X="-2.464156738281" Y="-27.484236328125" />
                  <Point X="-2.489316894531" Y="-27.466208984375" />
                  <Point X="-2.518145263672" Y="-27.451994140625" />
                  <Point X="-2.54776171875" Y="-27.44301171875" />
                  <Point X="-2.578694091797" Y="-27.444025390625" />
                  <Point X="-2.610219238281" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.344155029297" Y="-27.86821484375" />
                  <Point X="-3.818022949219" Y="-28.141802734375" />
                  <Point X="-3.898036132812" Y="-28.036681640625" />
                  <Point X="-4.082862792969" Y="-27.793857421875" />
                  <Point X="-4.211714355469" Y="-27.57779296875" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.696329345703" Y="-26.951525390625" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.359658203125" />
                  <Point X="-3.045556640625" Y="-26.327986328125" />
                  <Point X="-3.052558105469" Y="-26.298240234375" />
                  <Point X="-3.068640869141" Y="-26.272255859375" />
                  <Point X="-3.089474365234" Y="-26.248298828125" />
                  <Point X="-3.112974853516" Y="-26.228765625" />
                  <Point X="-3.139457519531" Y="-26.2131796875" />
                  <Point X="-3.168722167969" Y="-26.201955078125" />
                  <Point X="-3.200608886719" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-4.121888671875" Y="-26.311548828125" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.761791015625" Y="-26.27565234375" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.868168945312" Y="-25.754291015625" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.205401855469" Y="-25.40061328125" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.510624755859" Y="-25.211296875" />
                  <Point X="-3.482478027344" Y="-25.195345703125" />
                  <Point X="-3.469218505859" Y="-25.18626953125" />
                  <Point X="-3.442225830078" Y="-25.164220703125" />
                  <Point X="-3.426014404297" Y="-25.14723046875" />
                  <Point X="-3.414465820312" Y="-25.126783203125" />
                  <Point X="-3.402847167969" Y="-25.09853515625" />
                  <Point X="-3.398219970703" Y="-25.084109375" />
                  <Point X="-3.390915039062" Y="-25.0529921875" />
                  <Point X="-3.388401367188" Y="-25.0316171875" />
                  <Point X="-3.390763916016" Y="-25.010224609375" />
                  <Point X="-3.397204101562" Y="-24.981892578125" />
                  <Point X="-3.401703857422" Y="-24.967498046875" />
                  <Point X="-3.414187255859" Y="-24.936462890625" />
                  <Point X="-3.425576171875" Y="-24.91592578125" />
                  <Point X="-3.441654785156" Y="-24.898810546875" />
                  <Point X="-3.466052734375" Y="-24.8785625" />
                  <Point X="-3.479215576172" Y="-24.8693984375" />
                  <Point X="-3.50995703125" Y="-24.851646484375" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.344116210938" Y="-24.624779296875" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.871074707031" Y="-24.33785546875" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.755862304688" Y="-23.76977734375" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.247749511719" Y="-23.636740234375" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985107422" Y="-23.700658203125" />
                  <Point X="-3.723422851562" Y="-23.698771484375" />
                  <Point X="-3.703139892578" Y="-23.694736328125" />
                  <Point X="-3.675298339844" Y="-23.685958984375" />
                  <Point X="-3.641713867188" Y="-23.675369140625" />
                  <Point X="-3.622775878906" Y="-23.667037109375" />
                  <Point X="-3.604032226562" Y="-23.65621484375" />
                  <Point X="-3.587352783203" Y="-23.643984375" />
                  <Point X="-3.573715576172" Y="-23.62843359375" />
                  <Point X="-3.561301513672" Y="-23.610705078125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.540179931641" Y="-23.565599609375" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532050048828" Y="-23.410623046875" />
                  <Point X="-3.545529785156" Y="-23.384728515625" />
                  <Point X="-3.561789794922" Y="-23.353494140625" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-4.067465576172" Y="-22.948349609375" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.262172363281" Y="-22.57646875" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.899368652344" Y="-22.03268359375" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.502739013672" Y="-21.984384765625" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187728759766" Y="-22.163658203125" />
                  <Point X="-3.167086181641" Y="-22.17016796875" />
                  <Point X="-3.146794433594" Y="-22.174205078125" />
                  <Point X="-3.108018554688" Y="-22.17759765625" />
                  <Point X="-3.061245117188" Y="-22.181689453125" />
                  <Point X="-3.040560302734" Y="-22.18123828125" />
                  <Point X="-3.019102050781" Y="-22.178412109375" />
                  <Point X="-2.999012695312" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.918554443359" Y="-22.112248046875" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.846828369141" Y="-21.92510546875" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-3.075996826172" Y="-21.46131640625" />
                  <Point X="-3.183332763672" Y="-21.27540625" />
                  <Point X="-3.015898681641" Y="-21.14703515625" />
                  <Point X="-2.700620605469" Y="-20.9053125" />
                  <Point X="-2.414322753906" Y="-20.74625390625" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.133834960938" Y="-20.652134765625" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028893066406" Y="-20.78519921875" />
                  <Point X="-2.012314086914" Y="-20.799111328125" />
                  <Point X="-1.995114501953" Y="-20.810603515625" />
                  <Point X="-1.951957275391" Y="-20.8330703125" />
                  <Point X="-1.89989831543" Y="-20.860171875" />
                  <Point X="-1.880625976562" Y="-20.86766796875" />
                  <Point X="-1.859719604492" Y="-20.873271484375" />
                  <Point X="-1.83926965332" Y="-20.876419921875" />
                  <Point X="-1.818622924805" Y="-20.87506640625" />
                  <Point X="-1.797307495117" Y="-20.871306640625" />
                  <Point X="-1.777451782227" Y="-20.865517578125" />
                  <Point X="-1.732500610352" Y="-20.846896484375" />
                  <Point X="-1.678277832031" Y="-20.8244375" />
                  <Point X="-1.660147827148" Y="-20.814490234375" />
                  <Point X="-1.642417602539" Y="-20.802076171875" />
                  <Point X="-1.626865356445" Y="-20.7884375" />
                  <Point X="-1.614633789062" Y="-20.7717578125" />
                  <Point X="-1.603811523438" Y="-20.753013671875" />
                  <Point X="-1.595480102539" Y="-20.734078125" />
                  <Point X="-1.580849487305" Y="-20.68767578125" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.581173217773" Y="-20.391107421875" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.357775878906" Y="-20.30462109375" />
                  <Point X="-0.949623352051" Y="-20.19019140625" />
                  <Point X="-0.602554382324" Y="-20.1495703125" />
                  <Point X="-0.294711120605" Y="-20.113541015625" />
                  <Point X="-0.221492446899" Y="-20.386796875" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082114006042" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425956726" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.251870498657" Y="-20.319375" />
                  <Point X="0.307419647217" Y="-20.1120625" />
                  <Point X="0.487668548584" Y="-20.130939453125" />
                  <Point X="0.844031066895" Y="-20.168259765625" />
                  <Point X="1.13119519043" Y="-20.23758984375" />
                  <Point X="1.481039550781" Y="-20.3220546875" />
                  <Point X="1.667126220703" Y="-20.389548828125" />
                  <Point X="1.894645385742" Y="-20.472072265625" />
                  <Point X="2.075380859375" Y="-20.556595703125" />
                  <Point X="2.294556884766" Y="-20.65909765625" />
                  <Point X="2.469201904297" Y="-20.760845703125" />
                  <Point X="2.680975097656" Y="-20.884224609375" />
                  <Point X="2.845649414062" Y="-21.00133203125" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.538118896484" Y="-21.772470703125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.123345458984" Y="-22.5203359375" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.111522460938" Y="-22.650515625" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.140070800781" Y="-22.752529296875" />
                  <Point X="2.159541748047" Y="-22.781224609375" />
                  <Point X="2.183028564453" Y="-22.815837890625" />
                  <Point X="2.19446484375" Y="-22.829671875" />
                  <Point X="2.22159765625" Y="-22.85440625" />
                  <Point X="2.250292480469" Y="-22.873876953125" />
                  <Point X="2.28490625" Y="-22.897365234375" />
                  <Point X="2.3049453125" Y="-22.9077265625" />
                  <Point X="2.327033447266" Y="-22.915994140625" />
                  <Point X="2.348966796875" Y="-22.921337890625" />
                  <Point X="2.380434082031" Y="-22.925130859375" />
                  <Point X="2.418391601562" Y="-22.929708984375" />
                  <Point X="2.436466796875" Y="-22.93015625" />
                  <Point X="2.473208984375" Y="-22.925830078125" />
                  <Point X="2.509599365234" Y="-22.91609765625" />
                  <Point X="2.553495361328" Y="-22.904359375" />
                  <Point X="2.565286376953" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.404485595703" Y="-22.418763671875" />
                  <Point X="3.967325683594" Y="-22.09380859375" />
                  <Point X="3.997652832031" Y="-22.13595703125" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.21507421875" Y="-22.4622421875" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.744963378906" Y="-22.93700390625" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221422119141" Y="-23.33976171875" />
                  <Point X="3.203974365234" Y="-23.358373046875" />
                  <Point X="3.177784179688" Y="-23.3925390625" />
                  <Point X="3.146192138672" Y="-23.43375390625" />
                  <Point X="3.13660546875" Y="-23.44908984375" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.111874023438" Y="-23.517798828125" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.105748046875" Y="-23.667044921875" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120680419922" Y="-23.7310234375" />
                  <Point X="3.136282958984" Y="-23.764259765625" />
                  <Point X="3.158065185547" Y="-23.7973671875" />
                  <Point X="3.184340576172" Y="-23.8373046875" />
                  <Point X="3.198893554688" Y="-23.854548828125" />
                  <Point X="3.216137451172" Y="-23.870638671875" />
                  <Point X="3.234345703125" Y="-23.883962890625" />
                  <Point X="3.265911376953" Y="-23.901732421875" />
                  <Point X="3.303987792969" Y="-23.923166015625" />
                  <Point X="3.320521972656" Y="-23.930498046875" />
                  <Point X="3.356119140625" Y="-23.9405625" />
                  <Point X="3.398797851562" Y="-23.946203125" />
                  <Point X="3.450279785156" Y="-23.953005859375" />
                  <Point X="3.462698974609" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.263303222656" Y="-23.85097265625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.791983398438" Y="-23.8455703125" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.874865722656" Y="-24.253" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.305840820313" Y="-24.512517578125" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704786865234" Y="-24.674416015625" />
                  <Point X="3.681547119141" Y="-24.68493359375" />
                  <Point X="3.639616455078" Y="-24.709169921875" />
                  <Point X="3.589037109375" Y="-24.738404296875" />
                  <Point X="3.574315673828" Y="-24.748900390625" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.522371826172" Y="-24.806482421875" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.455294677734" Y="-24.951185546875" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.453565185547" Y="-25.10234375" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.176662109375" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.517182861328" Y="-25.249466796875" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.559999511719" Y="-25.30123828125" />
                  <Point X="3.589035888672" Y="-25.32415625" />
                  <Point X="3.630966552734" Y="-25.348392578125" />
                  <Point X="3.681545898438" Y="-25.37762890625" />
                  <Point X="3.692708984375" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.427381347656" Y="-25.582611328125" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.885147460938" Y="-25.748916015625" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.817958984375" Y="-26.11114453125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.107758789063" Y="-26.09341015625" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.292364013672" Y="-26.023396484375" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163973632812" Y="-26.05659765625" />
                  <Point X="3.136146972656" Y="-26.073490234375" />
                  <Point X="3.112397460938" Y="-26.0939609375" />
                  <Point X="3.062655273438" Y="-26.15378515625" />
                  <Point X="3.002653320312" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.962628173828" Y="-26.38283984375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.507564453125" />
                  <Point X="2.964078613281" Y="-26.539185546875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.021993896484" Y="-26.6388359375" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086931396484" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.770256591797" Y="-27.267060546875" />
                  <Point X="4.213122070312" Y="-27.6068828125" />
                  <Point X="4.209729003906" Y="-27.612373046875" />
                  <Point X="4.124813964844" Y="-27.74978125" />
                  <Point X="4.048159667969" Y="-27.8586953125" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="3.409593994141" Y="-27.52834375" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786128417969" Y="-27.17001171875" />
                  <Point X="2.754224853516" Y="-27.159826171875" />
                  <Point X="2.656280517578" Y="-27.14213671875" />
                  <Point X="2.538134521484" Y="-27.12080078125" />
                  <Point X="2.506783447266" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.444833984375" Y="-27.135177734375" />
                  <Point X="2.363466308594" Y="-27.178" />
                  <Point X="2.265315917969" Y="-27.22965625" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.161708740234" Y="-27.371806640625" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.113363769531" Y="-27.661203125" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.575696533203" Y="-28.560255859375" />
                  <Point X="2.861283691406" Y="-29.05490625" />
                  <Point X="2.781849609375" Y="-29.11164453125" />
                  <Point X="2.701763916016" Y="-29.163482421875" />
                  <Point X="2.222994384766" Y="-28.539537109375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721923461914" Y="-27.900556640625" />
                  <Point X="1.625323974609" Y="-27.838453125" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365478516" Y="-27.743435546875" />
                  <Point X="1.417100708008" Y="-27.741119140625" />
                  <Point X="1.311452758789" Y="-27.75083984375" />
                  <Point X="1.184013916016" Y="-27.76256640625" />
                  <Point X="1.156362792969" Y="-27.7693984375" />
                  <Point X="1.128976928711" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.7954609375" />
                  <Point X="1.023016601562" Y="-27.863291015625" />
                  <Point X="0.92461151123" Y="-27.94511328125" />
                  <Point X="0.90414074707" Y="-27.968865234375" />
                  <Point X="0.887248779297" Y="-27.99669140625" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.851232849121" Y="-28.138029296875" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.939865844727" Y="-29.235548828125" />
                  <Point X="1.022065490723" Y="-29.859916015625" />
                  <Point X="0.975713684082" Y="-29.870076171875" />
                  <Point X="0.929315612793" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058460327148" Y="-29.75262890625" />
                  <Point X="-1.141246337891" Y="-29.731330078125" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.541037109375" />
                  <Point X="-1.121759155273" Y="-29.50933203125" />
                  <Point X="-1.123333740234" Y="-29.497693359375" />
                  <Point X="-1.150768188477" Y="-29.35976953125" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188125" Y="-29.178470703125" />
                  <Point X="-1.19902734375" Y="-29.14950390625" />
                  <Point X="-1.205666015625" Y="-29.135466796875" />
                  <Point X="-1.22173840332" Y="-29.10762890625" />
                  <Point X="-1.230575195312" Y="-29.094861328125" />
                  <Point X="-1.250209350586" Y="-29.0709375" />
                  <Point X="-1.261006591797" Y="-29.05978125" />
                  <Point X="-1.366733398438" Y="-28.967060546875" />
                  <Point X="-1.494267333984" Y="-28.855216796875" />
                  <Point X="-1.506735839844" Y="-28.84596875" />
                  <Point X="-1.53301953125" Y="-28.829623046875" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576531860352" Y="-28.810224609375" />
                  <Point X="-1.591315917969" Y="-28.805474609375" />
                  <Point X="-1.62145715332" Y="-28.798447265625" />
                  <Point X="-1.636814331055" Y="-28.796169921875" />
                  <Point X="-1.777137329102" Y="-28.78697265625" />
                  <Point X="-1.946403442383" Y="-28.77587890625" />
                  <Point X="-1.961927856445" Y="-28.7761328125" />
                  <Point X="-1.992726074219" Y="-28.779166015625" />
                  <Point X="-2.00799987793" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053667236328" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095437011719" Y="-28.815810546875" />
                  <Point X="-2.212361572266" Y="-28.8939375" />
                  <Point X="-2.353403320312" Y="-28.988177734375" />
                  <Point X="-2.359684082031" Y="-28.9927578125" />
                  <Point X="-2.380450439453" Y="-29.010138671875" />
                  <Point X="-2.402763183594" Y="-29.03277734375" />
                  <Point X="-2.410471679688" Y="-29.041630859375" />
                  <Point X="-2.503201171875" Y="-29.162478515625" />
                  <Point X="-2.517759765625" Y="-29.15346484375" />
                  <Point X="-2.747591308594" Y="-29.011158203125" />
                  <Point X="-2.923487548828" Y="-28.875724609375" />
                  <Point X="-2.980862304688" Y="-28.831546875" />
                  <Point X="-2.678908447266" Y="-28.308548828125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.619232421875" />
                  <Point X="-2.310626708984" Y="-27.588296875" />
                  <Point X="-2.314667480469" Y="-27.557609375" />
                  <Point X="-2.323652832031" Y="-27.5279921875" />
                  <Point X="-2.329359619141" Y="-27.513552734375" />
                  <Point X="-2.343578125" Y="-27.48472265625" />
                  <Point X="-2.351559570312" Y="-27.47140625" />
                  <Point X="-2.369587890625" Y="-27.446248046875" />
                  <Point X="-2.379634765625" Y="-27.43440625" />
                  <Point X="-2.396983398438" Y="-27.41705859375" />
                  <Point X="-2.408825683594" Y="-27.407013671875" />
                  <Point X="-2.433985839844" Y="-27.388986328125" />
                  <Point X="-2.447303710938" Y="-27.38100390625" />
                  <Point X="-2.476132080078" Y="-27.3667890625" />
                  <Point X="-2.490572753906" Y="-27.361083984375" />
                  <Point X="-2.520189208984" Y="-27.3521015625" />
                  <Point X="-2.550873291016" Y="-27.3480625" />
                  <Point X="-2.581805664062" Y="-27.349076171875" />
                  <Point X="-2.597229736328" Y="-27.3508515625" />
                  <Point X="-2.628754882813" Y="-27.357123046875" />
                  <Point X="-2.643685546875" Y="-27.36138671875" />
                  <Point X="-2.672649902344" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.391655029297" Y="-27.785943359375" />
                  <Point X="-3.793086914062" Y="-28.0177109375" />
                  <Point X="-3.822442626953" Y="-27.979142578125" />
                  <Point X="-4.004021972656" Y="-27.740583984375" />
                  <Point X="-4.130121582031" Y="-27.529134765625" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.638497070312" Y="-27.02689453125" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.3988046875" />
                  <Point X="-2.948748535156" Y="-26.368376953125" />
                  <Point X="-2.948577880859" Y="-26.353048828125" />
                  <Point X="-2.950786865234" Y="-26.321376953125" />
                  <Point X="-2.953083740234" Y="-26.306220703125" />
                  <Point X="-2.960085205078" Y="-26.276474609375" />
                  <Point X="-2.971779052734" Y="-26.2482421875" />
                  <Point X="-2.987861816406" Y="-26.2222578125" />
                  <Point X="-2.996955322266" Y="-26.209916015625" />
                  <Point X="-3.017788818359" Y="-26.185958984375" />
                  <Point X="-3.028749511719" Y="-26.175240234375" />
                  <Point X="-3.05225" Y="-26.15570703125" />
                  <Point X="-3.064789794922" Y="-26.146892578125" />
                  <Point X="-3.091272460938" Y="-26.131306640625" />
                  <Point X="-3.105436523438" Y="-26.12448046875" />
                  <Point X="-3.134701171875" Y="-26.113255859375" />
                  <Point X="-3.149801757812" Y="-26.108857421875" />
                  <Point X="-3.181688476562" Y="-26.102376953125" />
                  <Point X="-3.197305175781" Y="-26.10053125" />
                  <Point X="-3.228625488281" Y="-26.09944140625" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-4.134288574219" Y="-26.217361328125" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.66974609375" Y="-26.252140625" />
                  <Point X="-4.740763183594" Y="-25.97411328125" />
                  <Point X="-4.774125976562" Y="-25.740841796875" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.180813964844" Y="-25.492376953125" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.496870361328" Y="-25.308322265625" />
                  <Point X="-3.474619140625" Y="-25.299208984375" />
                  <Point X="-3.463785644531" Y="-25.293947265625" />
                  <Point X="-3.435638916016" Y="-25.27799609375" />
                  <Point X="-3.428817382812" Y="-25.27373828125" />
                  <Point X="-3.409119873047" Y="-25.25984375" />
                  <Point X="-3.382127197266" Y="-25.237794921875" />
                  <Point X="-3.373493896484" Y="-25.229802734375" />
                  <Point X="-3.357282470703" Y="-25.2128125" />
                  <Point X="-3.343296142578" Y="-25.19394921875" />
                  <Point X="-3.331747558594" Y="-25.173501953125" />
                  <Point X="-3.326607177734" Y="-25.162919921875" />
                  <Point X="-3.314988525391" Y="-25.134671875" />
                  <Point X="-3.31238671875" Y="-25.12755078125" />
                  <Point X="-3.305734130859" Y="-25.1058203125" />
                  <Point X="-3.298429199219" Y="-25.074703125" />
                  <Point X="-3.296565185547" Y="-25.064087890625" />
                  <Point X="-3.294051513672" Y="-25.042712890625" />
                  <Point X="-3.293975341797" Y="-25.021189453125" />
                  <Point X="-3.296337890625" Y="-24.999796875" />
                  <Point X="-3.298126953125" Y="-24.98916796875" />
                  <Point X="-3.304567138672" Y="-24.9608359375" />
                  <Point X="-3.30653125" Y="-24.953548828125" />
                  <Point X="-3.313566650391" Y="-24.932046875" />
                  <Point X="-3.326050048828" Y="-24.90101171875" />
                  <Point X="-3.331106933594" Y="-24.890390625" />
                  <Point X="-3.342495849609" Y="-24.869853515625" />
                  <Point X="-3.356336914062" Y="-24.850880859375" />
                  <Point X="-3.372415527344" Y="-24.833765625" />
                  <Point X="-3.380985107422" Y="-24.82570703125" />
                  <Point X="-3.405383056641" Y="-24.805458984375" />
                  <Point X="-3.411772460938" Y="-24.80059765625" />
                  <Point X="-3.431708740234" Y="-24.787130859375" />
                  <Point X="-3.462450195312" Y="-24.76937890625" />
                  <Point X="-3.473593017578" Y="-24.763880859375" />
                  <Point X="-3.496511962891" Y="-24.754384765625" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.319528320312" Y="-24.533015625" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.777098144531" Y="-24.35176171875" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.664169433594" Y="-23.794625" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.260149414062" Y="-23.730927734375" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057128906" Y="-23.795634765625" />
                  <Point X="-3.736704101562" Y="-23.795296875" />
                  <Point X="-3.715141845703" Y="-23.79341015625" />
                  <Point X="-3.704886474609" Y="-23.7919453125" />
                  <Point X="-3.684603515625" Y="-23.78791015625" />
                  <Point X="-3.674575927734" Y="-23.78533984375" />
                  <Point X="-3.646734375" Y="-23.7765625" />
                  <Point X="-3.613149902344" Y="-23.76597265625" />
                  <Point X="-3.603456298828" Y="-23.762326171875" />
                  <Point X="-3.584518310547" Y="-23.753994140625" />
                  <Point X="-3.575273925781" Y="-23.74930859375" />
                  <Point X="-3.556530273438" Y="-23.738486328125" />
                  <Point X="-3.547855957031" Y="-23.732826171875" />
                  <Point X="-3.531176513672" Y="-23.720595703125" />
                  <Point X="-3.515927001953" Y="-23.70662109375" />
                  <Point X="-3.502289794922" Y="-23.6910703125" />
                  <Point X="-3.495896972656" Y="-23.682923828125" />
                  <Point X="-3.483482910156" Y="-23.6651953125" />
                  <Point X="-3.478014160156" Y="-23.65640234375" />
                  <Point X="-3.468064208984" Y="-23.638267578125" />
                  <Point X="-3.463583007813" Y="-23.62892578125" />
                  <Point X="-3.452411376953" Y="-23.601955078125" />
                  <Point X="-3.438935546875" Y="-23.569421875" />
                  <Point X="-3.435499267578" Y="-23.559650390625" />
                  <Point X="-3.4297109375" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436012695312" Y="-23.39546875" />
                  <Point X="-3.443509277344" Y="-23.37619140625" />
                  <Point X="-3.447783935547" Y="-23.3667578125" />
                  <Point X="-3.461263671875" Y="-23.34086328125" />
                  <Point X="-3.477523681641" Y="-23.30962890625" />
                  <Point X="-3.482799804688" Y="-23.30071484375" />
                  <Point X="-3.494291748047" Y="-23.283515625" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521498046875" Y="-23.251091796875" />
                  <Point X="-3.536439941406" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-4.009633300781" Y="-22.87298046875" />
                  <Point X="-4.227614746094" Y="-22.70571875" />
                  <Point X="-4.180125976563" Y="-22.624357421875" />
                  <Point X="-4.002294921875" Y="-22.31969140625" />
                  <Point X="-3.824387939453" Y="-22.091017578125" />
                  <Point X="-3.726336914062" Y="-21.964986328125" />
                  <Point X="-3.550239013672" Y="-22.06665625" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244923583984" Y="-22.242279296875" />
                  <Point X="-3.225995361328" Y="-22.250609375" />
                  <Point X="-3.216300537109" Y="-22.254259765625" />
                  <Point X="-3.195657958984" Y="-22.26076953125" />
                  <Point X="-3.185623535156" Y="-22.263341796875" />
                  <Point X="-3.165331787109" Y="-22.26737890625" />
                  <Point X="-3.155074462891" Y="-22.26884375" />
                  <Point X="-3.116298583984" Y="-22.272236328125" />
                  <Point X="-3.069525146484" Y="-22.276328125" />
                  <Point X="-3.059173583984" Y="-22.276666015625" />
                  <Point X="-3.038488769531" Y="-22.27621484375" />
                  <Point X="-3.028155517578" Y="-22.27542578125" />
                  <Point X="-3.006697265625" Y="-22.272599609375" />
                  <Point X="-2.996512695312" Y="-22.2706875" />
                  <Point X="-2.976423339844" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.938449462891" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.902746826172" Y="-22.226806640625" />
                  <Point X="-2.886614990234" Y="-22.213859375" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.851379394531" Y="-22.179423828125" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.752189941406" Y="-21.916826171875" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.993724609375" Y="-21.41381640625" />
                  <Point X="-3.059387451172" Y="-21.3000859375" />
                  <Point X="-2.958096191406" Y="-21.22242578125" />
                  <Point X="-2.648374755859" Y="-20.984962890625" />
                  <Point X="-2.368185791016" Y="-20.829298828125" />
                  <Point X="-2.192524414062" Y="-20.731703125" />
                  <Point X="-2.118564697266" Y="-20.82808984375" />
                  <Point X="-2.111822021484" Y="-20.83594921875" />
                  <Point X="-2.097519775391" Y="-20.850890625" />
                  <Point X="-2.089959716797" Y="-20.85797265625" />
                  <Point X="-2.073380615234" Y="-20.871884765625" />
                  <Point X="-2.065092529297" Y="-20.8781015625" />
                  <Point X="-2.047893066406" Y="-20.88959375" />
                  <Point X="-2.038981445312" Y="-20.894869140625" />
                  <Point X="-1.99582421875" Y="-20.9173359375" />
                  <Point X="-1.943765258789" Y="-20.9444375" />
                  <Point X="-1.9343359375" Y="-20.9487109375" />
                  <Point X="-1.915063598633" Y="-20.95620703125" />
                  <Point X="-1.905220581055" Y="-20.9594296875" />
                  <Point X="-1.884314208984" Y="-20.965033203125" />
                  <Point X="-1.874175292969" Y="-20.967166015625" />
                  <Point X="-1.853725341797" Y="-20.970314453125" />
                  <Point X="-1.833055053711" Y="-20.971216796875" />
                  <Point X="-1.812408447266" Y="-20.96986328125" />
                  <Point X="-1.802120849609" Y="-20.968623046875" />
                  <Point X="-1.780805419922" Y="-20.96486328125" />
                  <Point X="-1.770716918945" Y="-20.962509765625" />
                  <Point X="-1.750861206055" Y="-20.956720703125" />
                  <Point X="-1.741093994141" Y="-20.95328515625" />
                  <Point X="-1.696142822266" Y="-20.9346640625" />
                  <Point X="-1.641920166016" Y="-20.912205078125" />
                  <Point X="-1.632581054688" Y="-20.907724609375" />
                  <Point X="-1.614451049805" Y="-20.89777734375" />
                  <Point X="-1.605660400391" Y="-20.892310546875" />
                  <Point X="-1.587930175781" Y="-20.879896484375" />
                  <Point X="-1.579780639648" Y="-20.873501953125" />
                  <Point X="-1.564228271484" Y="-20.85986328125" />
                  <Point X="-1.550256469727" Y="-20.8446171875" />
                  <Point X="-1.538024780273" Y="-20.8279375" />
                  <Point X="-1.532361938477" Y="-20.819259765625" />
                  <Point X="-1.521539672852" Y="-20.800515625" />
                  <Point X="-1.516856201172" Y="-20.7912734375" />
                  <Point X="-1.508524780273" Y="-20.772337890625" />
                  <Point X="-1.504877075195" Y="-20.76264453125" />
                  <Point X="-1.490246337891" Y="-20.7162421875" />
                  <Point X="-1.47259765625" Y="-20.660267578125" />
                  <Point X="-1.470026123047" Y="-20.650234375" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-1.332129882812" Y="-20.39609375" />
                  <Point X="-0.931164245605" Y="-20.2836796875" />
                  <Point X="-0.591510986328" Y="-20.24392578125" />
                  <Point X="-0.365222351074" Y="-20.21744140625" />
                  <Point X="-0.313255462646" Y="-20.411384765625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896308899" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.343633392334" Y="-20.343962890625" />
                  <Point X="0.378190765381" Y="-20.2149921875" />
                  <Point X="0.477773590088" Y="-20.225421875" />
                  <Point X="0.827851379395" Y="-20.262083984375" />
                  <Point X="1.108899902344" Y="-20.3299375" />
                  <Point X="1.453622436523" Y="-20.413166015625" />
                  <Point X="1.63473425293" Y="-20.47885546875" />
                  <Point X="1.858248046875" Y="-20.55992578125" />
                  <Point X="2.035136230469" Y="-20.642650390625" />
                  <Point X="2.250428710938" Y="-20.7433359375" />
                  <Point X="2.421379150391" Y="-20.842931640625" />
                  <Point X="2.629431152344" Y="-20.964142578125" />
                  <Point X="2.790592773438" Y="-21.078751953125" />
                  <Point X="2.817778808594" Y="-21.098083984375" />
                  <Point X="2.455846435547" Y="-21.724970703125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.03157019043" Y="-22.495794921875" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017912231445" Y="-22.5485390625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.017205688477" Y="-22.661888671875" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029143920898" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045317871094" Y="-22.776111328125" />
                  <Point X="2.055680175781" Y="-22.79615625" />
                  <Point X="2.061459472656" Y="-22.80587109375" />
                  <Point X="2.080930419922" Y="-22.83456640625" />
                  <Point X="2.104417236328" Y="-22.8691796875" />
                  <Point X="2.10980859375" Y="-22.8763671875" />
                  <Point X="2.130464355469" Y="-22.89987890625" />
                  <Point X="2.157597167969" Y="-22.92461328125" />
                  <Point X="2.168256591797" Y="-22.933017578125" />
                  <Point X="2.196951416016" Y="-22.95248828125" />
                  <Point X="2.231565185547" Y="-22.9759765625" />
                  <Point X="2.2412734375" Y="-22.981751953125" />
                  <Point X="2.2613125" Y="-22.99211328125" />
                  <Point X="2.271643310547" Y="-22.99669921875" />
                  <Point X="2.293731445312" Y="-23.004966796875" />
                  <Point X="2.304545898438" Y="-23.008294921875" />
                  <Point X="2.326479248047" Y="-23.013638671875" />
                  <Point X="2.337598144531" Y="-23.015654296875" />
                  <Point X="2.369065429688" Y="-23.019447265625" />
                  <Point X="2.407022949219" Y="-23.024025390625" />
                  <Point X="2.416041503906" Y="-23.0246796875" />
                  <Point X="2.447575683594" Y="-23.02450390625" />
                  <Point X="2.484317871094" Y="-23.020177734375" />
                  <Point X="2.497753662109" Y="-23.01760546875" />
                  <Point X="2.534144042969" Y="-23.007873046875" />
                  <Point X="2.578040039062" Y="-22.996134765625" />
                  <Point X="2.584001464844" Y="-22.994328125" />
                  <Point X="2.604414794922" Y="-22.9869296875" />
                  <Point X="2.627662353516" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.451985595703" Y="-22.50103515625" />
                  <Point X="3.940403808594" Y="-22.219046875" />
                  <Point X="4.043953369141" Y="-22.36295703125" />
                  <Point X="4.133797363281" Y="-22.51142578125" />
                  <Point X="4.136884765625" Y="-22.51652734375" />
                  <Point X="3.687131103516" Y="-22.861634765625" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.1681328125" Y="-23.260134765625" />
                  <Point X="3.152115478516" Y="-23.274787109375" />
                  <Point X="3.134667724609" Y="-23.2933984375" />
                  <Point X="3.128577636719" Y="-23.300578125" />
                  <Point X="3.102387451172" Y="-23.334744140625" />
                  <Point X="3.070795410156" Y="-23.375958984375" />
                  <Point X="3.065636230469" Y="-23.3833984375" />
                  <Point X="3.049738769531" Y="-23.41062890625" />
                  <Point X="3.034763183594" Y="-23.444453125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.020384277344" Y="-23.492212890625" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.012708007812" Y="-23.6862421875" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024598388672" Y="-23.741767578125" />
                  <Point X="3.034684814453" Y="-23.771392578125" />
                  <Point X="3.050287353516" Y="-23.80462890625" />
                  <Point X="3.056919433594" Y="-23.816474609375" />
                  <Point X="3.078701660156" Y="-23.84958203125" />
                  <Point X="3.104977050781" Y="-23.88951953125" />
                  <Point X="3.111739501953" Y="-23.898576171875" />
                  <Point X="3.126292480469" Y="-23.9158203125" />
                  <Point X="3.134083007812" Y="-23.9240078125" />
                  <Point X="3.151326904297" Y="-23.94009765625" />
                  <Point X="3.160035888672" Y="-23.9473046875" />
                  <Point X="3.178244140625" Y="-23.96062890625" />
                  <Point X="3.187743408203" Y="-23.96674609375" />
                  <Point X="3.219309082031" Y="-23.984515625" />
                  <Point X="3.257385498047" Y="-24.00594921875" />
                  <Point X="3.265477050781" Y="-24.010009765625" />
                  <Point X="3.294675537109" Y="-24.0219140625" />
                  <Point X="3.330272705078" Y="-24.031978515625" />
                  <Point X="3.343671630859" Y="-24.034744140625" />
                  <Point X="3.386350341797" Y="-24.040384765625" />
                  <Point X="3.437832275391" Y="-24.0471875" />
                  <Point X="3.444033447266" Y="-24.04780078125" />
                  <Point X="3.465709228516" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.275703125" Y="-23.94516015625" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.780996582031" Y="-24.267615234375" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="4.281252929687" Y="-24.42075390625" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686024658203" Y="-24.580458984375" />
                  <Point X="3.665617431641" Y="-24.5878671875" />
                  <Point X="3.642377685547" Y="-24.598384765625" />
                  <Point X="3.634006347656" Y="-24.602685546875" />
                  <Point X="3.592075683594" Y="-24.626921875" />
                  <Point X="3.541496337891" Y="-24.65615625" />
                  <Point X="3.533886474609" Y="-24.661052734375" />
                  <Point X="3.508780273438" Y="-24.680125" />
                  <Point X="3.481994873047" Y="-24.7056484375" />
                  <Point X="3.472795654297" Y="-24.715775390625" />
                  <Point X="3.447637207031" Y="-24.747833984375" />
                  <Point X="3.417289794922" Y="-24.78650390625" />
                  <Point X="3.410854736328" Y="-24.795791015625" />
                  <Point X="3.399130615234" Y="-24.8150703125" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.38000390625" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.370376708984" Y="-24.88952734375" />
                  <Point X="3.361990478516" Y="-24.93331640625" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.360260986328" Y="-25.120212890625" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.183986328125" />
                  <Point X="3.380004394531" Y="-25.205484375" />
                  <Point X="3.384067382812" Y="-25.216029296875" />
                  <Point X="3.393841064453" Y="-25.237494140625" />
                  <Point X="3.399128662109" Y="-25.247484375" />
                  <Point X="3.410853027344" Y="-25.266765625" />
                  <Point X="3.417289794922" Y="-25.276056640625" />
                  <Point X="3.442448242188" Y="-25.308115234375" />
                  <Point X="3.472795654297" Y="-25.34678515625" />
                  <Point X="3.478715087891" Y="-25.353630859375" />
                  <Point X="3.501142089844" Y="-25.37580859375" />
                  <Point X="3.530178466797" Y="-25.3987265625" />
                  <Point X="3.541495117188" Y="-25.406404296875" />
                  <Point X="3.58342578125" Y="-25.430640625" />
                  <Point X="3.634005126953" Y="-25.459876953125" />
                  <Point X="3.639487304688" Y="-25.4628125" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026855469" Y="-25.481029296875" />
                  <Point X="3.691991943359" Y="-25.483916015625" />
                  <Point X="4.402793457031" Y="-25.674375" />
                  <Point X="4.784876953125" Y="-25.77675390625" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.727802246094" Y="-26.079220703125" />
                  <Point X="4.120158691406" Y="-25.99922265625" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354482177734" Y="-25.912677734375" />
                  <Point X="3.272187011719" Y="-25.930564453125" />
                  <Point X="3.17291796875" Y="-25.952140625" />
                  <Point X="3.157874267578" Y="-25.9567421875" />
                  <Point X="3.128752929688" Y="-25.9683671875" />
                  <Point X="3.114675292969" Y="-25.975390625" />
                  <Point X="3.086848632812" Y="-25.992283203125" />
                  <Point X="3.074123046875" Y="-26.00153125" />
                  <Point X="3.050373535156" Y="-26.022001953125" />
                  <Point X="3.039349609375" Y="-26.033224609375" />
                  <Point X="2.989607421875" Y="-26.093048828125" />
                  <Point X="2.92960546875" Y="-26.165212890625" />
                  <Point X="2.921327148438" Y="-26.17684765625" />
                  <Point X="2.906606689453" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.868027832031" Y="-26.374134765625" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607177734" Y="-26.51458984375" />
                  <Point X="2.864065429688" Y="-26.530126953125" />
                  <Point X="2.871796875" Y="-26.561748046875" />
                  <Point X="2.876786132812" Y="-26.57666796875" />
                  <Point X="2.889158203125" Y="-26.60548046875" />
                  <Point X="2.896541015625" Y="-26.619373046875" />
                  <Point X="2.942084228516" Y="-26.6902109375" />
                  <Point X="2.997021240234" Y="-26.775662109375" />
                  <Point X="3.001742675781" Y="-26.782353515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486083984" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.712424316406" Y="-27.3424296875" />
                  <Point X="4.087170410156" Y="-27.629982421875" />
                  <Point X="4.04549609375" Y="-27.697419921875" />
                  <Point X="4.001274169922" Y="-27.760251953125" />
                  <Point X="3.45709375" Y="-27.4460703125" />
                  <Point X="2.848454345703" Y="-27.094671875" />
                  <Point X="2.841198730469" Y="-27.090890625" />
                  <Point X="2.815021484375" Y="-27.07951171875" />
                  <Point X="2.783117919922" Y="-27.069326171875" />
                  <Point X="2.771109375" Y="-27.066337890625" />
                  <Point X="2.673165039062" Y="-27.0486484375" />
                  <Point X="2.555019042969" Y="-27.0273125" />
                  <Point X="2.539359619141" Y="-27.02580859375" />
                  <Point X="2.508008544922" Y="-27.025404296875" />
                  <Point X="2.492316894531" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444846191406" Y="-27.03513671875" />
                  <Point X="2.415069091797" Y="-27.0449609375" />
                  <Point X="2.400590332031" Y="-27.051109375" />
                  <Point X="2.31922265625" Y="-27.093931640625" />
                  <Point X="2.221072265625" Y="-27.145587890625" />
                  <Point X="2.208969726562" Y="-27.153169921875" />
                  <Point X="2.186039306641" Y="-27.1700625" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144940429687" Y="-27.21116015625" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.077640869141" Y="-27.3275625" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.002187744141" Y="-27.580140625" />
                  <Point X="2.019876098633" Y="-27.6780859375" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.493424072266" Y="-28.607755859375" />
                  <Point X="2.735893798828" Y="-29.027724609375" />
                  <Point X="2.723752685547" Y="-29.036083984375" />
                  <Point X="2.298363037109" Y="-28.481705078125" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828658813477" Y="-27.87015234375" />
                  <Point X="1.808834228516" Y="-27.84962890625" />
                  <Point X="1.783250854492" Y="-27.82800390625" />
                  <Point X="1.773297729492" Y="-27.820646484375" />
                  <Point X="1.676698242188" Y="-27.75854296875" />
                  <Point X="1.560174438477" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517470703125" Y="-27.663873046875" />
                  <Point X="1.502546875" Y="-27.6588828125" />
                  <Point X="1.470926025391" Y="-27.65115234375" />
                  <Point X="1.455384765625" Y="-27.6486953125" />
                  <Point X="1.424119995117" Y="-27.64637890625" />
                  <Point X="1.408396484375" Y="-27.64651953125" />
                  <Point X="1.302748535156" Y="-27.656240234375" />
                  <Point X="1.175309814453" Y="-27.667966796875" />
                  <Point X="1.1612265625" Y="-27.67033984375" />
                  <Point X="1.133575439453" Y="-27.677171875" />
                  <Point X="1.120007446289" Y="-27.681630859375" />
                  <Point X="1.092621582031" Y="-27.692974609375" />
                  <Point X="1.079880004883" Y="-27.699412109375" />
                  <Point X="1.055498291016" Y="-27.714130859375" />
                  <Point X="1.043858154297" Y="-27.722412109375" />
                  <Point X="0.96227947998" Y="-27.7902421875" />
                  <Point X="0.863874328613" Y="-27.872064453125" />
                  <Point X="0.852650024414" Y="-27.88309375" />
                  <Point X="0.832179138184" Y="-27.906845703125" />
                  <Point X="0.822932739258" Y="-27.919568359375" />
                  <Point X="0.806040710449" Y="-27.94739453125" />
                  <Point X="0.799019287109" Y="-27.961470703125" />
                  <Point X="0.787394775391" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.758400390625" Y="-28.1178515625" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091247559" Y="-29.152337890625" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605224609" Y="-28.48012109375" />
                  <Point X="0.642143371582" Y="-28.453576171875" />
                  <Point X="0.626786315918" Y="-28.423814453125" />
                  <Point X="0.620406982422" Y="-28.413208984375" />
                  <Point X="0.546196594238" Y="-28.306287109375" />
                  <Point X="0.456679382324" Y="-28.17730859375" />
                  <Point X="0.44667098999" Y="-28.165173828125" />
                  <Point X="0.424791442871" Y="-28.14272265625" />
                  <Point X="0.412920349121" Y="-28.13240625" />
                  <Point X="0.38666506958" Y="-28.11316015625" />
                  <Point X="0.373248596191" Y="-28.10494140625" />
                  <Point X="0.345243133545" Y="-28.090830078125" />
                  <Point X="0.330654022217" Y="-28.0849375" />
                  <Point X="0.215817398071" Y="-28.049296875" />
                  <Point X="0.077294914246" Y="-28.0063046875" />
                  <Point X="0.063376972198" Y="-28.003109375" />
                  <Point X="0.035217987061" Y="-27.99883984375" />
                  <Point X="0.020976644516" Y="-27.997765625" />
                  <Point X="-0.008664708138" Y="-27.997765625" />
                  <Point X="-0.022905902863" Y="-27.99883984375" />
                  <Point X="-0.051065185547" Y="-28.003109375" />
                  <Point X="-0.064983123779" Y="-28.0063046875" />
                  <Point X="-0.179819580078" Y="-28.0419453125" />
                  <Point X="-0.318342224121" Y="-28.0849375" />
                  <Point X="-0.332930603027" Y="-28.090828125" />
                  <Point X="-0.360932800293" Y="-28.1049375" />
                  <Point X="-0.374346466064" Y="-28.11315625" />
                  <Point X="-0.400601593018" Y="-28.132400390625" />
                  <Point X="-0.412476409912" Y="-28.14271875" />
                  <Point X="-0.43435949707" Y="-28.165173828125" />
                  <Point X="-0.44436819458" Y="-28.177310546875" />
                  <Point X="-0.518578613281" Y="-28.284234375" />
                  <Point X="-0.608095947266" Y="-28.4132109375" />
                  <Point X="-0.612468994141" Y="-28.42012890625" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.861768310547" Y="-29.3053125" />
                  <Point X="-0.985425048828" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.137055855627" Y="-29.69950064458" />
                  <Point X="-0.958068539315" Y="-29.664709034703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.665627603321" Y="-28.960333858276" />
                  <Point X="2.693821537711" Y="-28.954853512594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.123979978162" Y="-29.600180865455" />
                  <Point X="-0.930712029802" Y="-29.562613381906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601005761865" Y="-28.876116985775" />
                  <Point X="2.643584433209" Y="-28.867840530476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.122597669472" Y="-29.503134085844" />
                  <Point X="-0.903355520289" Y="-29.46051772911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825615945699" Y="-29.124439721061" />
                  <Point X="0.829323502336" Y="-29.123719045055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.53638392041" Y="-28.791900113273" />
                  <Point X="2.593347328707" Y="-28.780827548359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.140797967174" Y="-29.409893779317" />
                  <Point X="-0.875999010776" Y="-29.358422076313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.800968064605" Y="-29.032452697786" />
                  <Point X="0.816900333614" Y="-29.029355778411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.471762078954" Y="-28.707683240772" />
                  <Point X="2.543110224205" Y="-28.693814566242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.159331681585" Y="-29.316718282433" />
                  <Point X="-0.848642543585" Y="-29.256326431743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77632018351" Y="-28.940465674512" />
                  <Point X="0.804477164892" Y="-28.934992511767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.407140237499" Y="-28.62346636827" />
                  <Point X="2.492873120041" Y="-28.606801584058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.177865578812" Y="-29.223542821084" />
                  <Point X="-0.82128612228" Y="-29.154230796092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.751672302416" Y="-28.848478651238" />
                  <Point X="0.792053996171" Y="-28.840629245123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.342518396043" Y="-28.539249495769" />
                  <Point X="2.44263604642" Y="-28.519788595938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.207378112359" Y="-29.132501390458" />
                  <Point X="-0.793929700975" Y="-29.052135160441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.727024421322" Y="-28.756491627964" />
                  <Point X="0.779630827449" Y="-28.746265978479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.277896559404" Y="-28.455032622331" />
                  <Point X="2.392398972799" Y="-28.432775607818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.273732073503" Y="-29.048621207917" />
                  <Point X="-0.76657327967" Y="-28.95003952479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.702376540228" Y="-28.66450460469" />
                  <Point X="0.767207658728" Y="-28.651902711834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.213274733156" Y="-28.370815746873" />
                  <Point X="2.342161899178" Y="-28.345762619698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.364063716745" Y="-28.969401814634" />
                  <Point X="-0.739216858365" Y="-28.847943889139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.677728659134" Y="-28.572517581416" />
                  <Point X="0.754784490006" Y="-28.55753944519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148652906908" Y="-28.286598871416" />
                  <Point X="2.291924825557" Y="-28.258749631578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.587312639955" Y="-29.11039923246" />
                  <Point X="-2.441489444939" Y="-29.082054074733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.454396292358" Y="-28.890182602586" />
                  <Point X="-0.71186043706" Y="-28.745848253488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.652754097365" Y="-28.480594058432" />
                  <Point X="0.742361321284" Y="-28.463176178546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.08403108066" Y="-28.202381995958" />
                  <Point X="2.241687751936" Y="-28.171736643458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.706269181952" Y="-29.036743955846" />
                  <Point X="-2.311140173944" Y="-28.95993865712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.565160196453" Y="-28.814934838485" />
                  <Point X="-0.684504015755" Y="-28.643752617837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.606276763417" Y="-28.392850250953" />
                  <Point X="0.729938152563" Y="-28.368812911902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.019409254411" Y="-28.1181651205" />
                  <Point X="2.191450678315" Y="-28.084723655338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965870081971" Y="-27.739811463115" />
                  <Point X="4.023551683162" Y="-27.728599295644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.813086787323" Y="-28.960729108979" />
                  <Point X="-2.106879711217" Y="-28.82345635921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.883368706676" Y="-28.780010221052" />
                  <Point X="-0.65714759445" Y="-28.541656982186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.547091579862" Y="-28.307576599207" />
                  <Point X="0.726433453727" Y="-28.272716070324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.954787428163" Y="-28.033948245043" />
                  <Point X="2.141213604694" Y="-27.997710667218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840465782056" Y="-27.667409503679" />
                  <Point X="4.076223792855" Y="-27.621582788658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.913443254626" Y="-28.883458344096" />
                  <Point X="-0.622110218051" Y="-28.438068320109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.487907456469" Y="-28.222302741388" />
                  <Point X="0.746628238856" Y="-28.172012515727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.890165601915" Y="-27.949731369585" />
                  <Point X="2.090976531073" Y="-27.910697679097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.71506148214" Y="-27.595007544243" />
                  <Point X="3.97559223727" Y="-27.544365495521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960206530077" Y="-28.795770118014" />
                  <Point X="-0.548208567053" Y="-28.326925208322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.420155919396" Y="-28.138694220088" />
                  <Point X="0.768591471651" Y="-28.070965209726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.824394650606" Y="-27.865737861412" />
                  <Point X="2.048421581901" Y="-27.822191437252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.589657182224" Y="-27.522605584807" />
                  <Point X="3.874960681684" Y="-27.467148202384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.897268226409" Y="-28.68675806507" />
                  <Point X="-0.470564457117" Y="-28.215054636209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.279887341161" Y="-28.069181583667" />
                  <Point X="0.796075523328" Y="-27.968844765244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.723428378241" Y="-27.788585630626" />
                  <Point X="2.029101616666" Y="-27.729168772046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.464252882308" Y="-27.450203625371" />
                  <Point X="3.774329126099" Y="-27.389930909247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.834329922741" Y="-28.577746012126" />
                  <Point X="-0.335176627672" Y="-28.091959822047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.088149528469" Y="-28.009673552951" />
                  <Point X="0.884556261416" Y="-27.854867766001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.607842605639" Y="-27.714275142816" />
                  <Point X="2.0122165348" Y="-27.635672813458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.338848962975" Y="-27.377801591958" />
                  <Point X="3.673697569973" Y="-27.312713616215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.771391619073" Y="-28.468733959181" />
                  <Point X="1.036461342855" Y="-27.728562323291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.449561202682" Y="-27.648263844833" />
                  <Point X="2.00038310274" Y="-27.54119491362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.213445066685" Y="-27.305399554065" />
                  <Point X="3.573066012982" Y="-27.235496323351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.708453315405" Y="-28.359721906237" />
                  <Point X="2.019679998199" Y="-27.440665891094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.088041170394" Y="-27.232997516172" />
                  <Point X="3.472434455992" Y="-27.158279030487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.645515217661" Y="-28.25070989332" />
                  <Point X="2.074675019668" Y="-27.333197855799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.962637274104" Y="-27.160595478279" />
                  <Point X="3.371802899002" Y="-27.081061737623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.582577302109" Y="-28.141697915818" />
                  <Point X="2.134965187976" Y="-27.224700548225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.835685852074" Y="-27.088494248918" />
                  <Point X="3.271171342012" Y="-27.003844444759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.519639386557" Y="-28.032685938316" />
                  <Point X="2.324721897003" Y="-27.091037494443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.605582471776" Y="-27.036443729093" />
                  <Point X="3.170539785021" Y="-26.926627151895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.456701471005" Y="-27.923673960813" />
                  <Point X="3.069908228031" Y="-26.849409859031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.393763555453" Y="-27.814661983311" />
                  <Point X="2.991956408124" Y="-26.767784071862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.81153670768" Y="-27.993471080907" />
                  <Point X="-3.720428490528" Y="-27.975761437492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.333367997349" Y="-27.706144190036" />
                  <Point X="2.936648812064" Y="-26.681756693461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.875705350013" Y="-27.90916611542" />
                  <Point X="-3.467725640382" Y="-27.82986289334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311174941376" Y="-27.605052210934" />
                  <Point X="2.884691431794" Y="-26.595078099079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.939874209231" Y="-27.824861192091" />
                  <Point X="-3.215022019704" Y="-27.683964199412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.330144428567" Y="-27.511961419698" />
                  <Point X="2.860744400632" Y="-26.502954844378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.004038975556" Y="-27.740555473185" />
                  <Point X="-2.962318067179" Y="-27.538065440979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.387675271515" Y="-27.426366196714" />
                  <Point X="2.865158382524" Y="-26.405318767193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.617496844713" Y="-26.064698675199" />
                  <Point X="4.736389770342" Y="-26.041588231561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.055758135558" Y="-27.653830573473" />
                  <Point X="-2.709614114654" Y="-27.392166682546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.51366955557" Y="-27.354078918578" />
                  <Point X="2.874226151076" Y="-26.306778085518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.320661804402" Y="-26.025619476077" />
                  <Point X="4.759499047627" Y="-25.940318157077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.107477295559" Y="-27.567105673762" />
                  <Point X="2.905218175365" Y="-26.203975760235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.023826855923" Y="-25.986540259104" />
                  <Point X="4.775269940761" Y="-25.840474519974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.159196435653" Y="-27.480380770181" />
                  <Point X="2.991843611132" Y="-26.090359395231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726992098584" Y="-25.947461004978" />
                  <Point X="4.709335812721" Y="-25.756512730144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084427825683" Y="-27.369069138641" />
                  <Point X="3.135580445968" Y="-25.96564169882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.429135771098" Y="-25.908580323972" />
                  <Point X="4.50000866087" Y="-25.700423720611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.91551486057" Y="-27.239457698244" />
                  <Point X="4.290681645704" Y="-25.644334684509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.746601895456" Y="-27.109846257847" />
                  <Point X="4.081354749059" Y="-25.588245625369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.577688824187" Y="-26.980234796815" />
                  <Point X="3.872027852415" Y="-25.532156566229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.408775564194" Y="-26.8506232991" />
                  <Point X="3.667442413251" Y="-25.475145861118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.239862304201" Y="-26.721011801384" />
                  <Point X="3.537422315932" Y="-25.403641121808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.070949044208" Y="-26.591400303668" />
                  <Point X="3.454165959439" Y="-25.3230464321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.974853262869" Y="-26.475943089964" />
                  <Point X="3.394081101331" Y="-25.237947659373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949284720801" Y="-26.374194982832" />
                  <Point X="3.365344324613" Y="-25.146755436894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959400592286" Y="-26.279383223038" />
                  <Point X="3.349820215623" Y="-25.052994931978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.011841022789" Y="-26.19279852411" />
                  <Point X="3.357905818331" Y="-24.954645164004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.123201321353" Y="-26.117666687349" />
                  <Point X="3.381462708063" Y="-24.853288082475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.133288627134" Y="-26.217229684082" />
                  <Point X="3.451514470515" Y="-24.742893313213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676416853155" Y="-26.226025030537" />
                  <Point X="3.613265355921" Y="-24.614674040083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.699967751815" Y="-26.133824775478" />
                  <Point X="4.725736575927" Y="-24.301653454411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.723518650474" Y="-26.041624520419" />
                  <Point X="4.769892056284" Y="-24.196292412468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744369307105" Y="-25.94889939148" />
                  <Point X="4.755266914775" Y="-24.102357165975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.757836247586" Y="-25.854739013513" />
                  <Point X="4.734161622331" Y="-24.009681533224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.771303188067" Y="-25.760578635546" />
                  <Point X="4.711665829732" Y="-23.917276186323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784770197033" Y="-25.666418270892" />
                  <Point X="3.639321787777" Y="-24.02894066668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.63528881229" Y="-25.346203637956" />
                  <Point X="3.258037971815" Y="-24.006276646675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.342483223868" Y="-25.192509911141" />
                  <Point X="3.142584590753" Y="-23.931940424556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.30149545495" Y="-25.087764609927" />
                  <Point X="3.077533778863" Y="-23.84780693546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.297937270406" Y="-24.990294882895" />
                  <Point X="3.03084168851" Y="-23.760104872396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.326934847701" Y="-24.899153354913" />
                  <Point X="3.008861257245" Y="-23.6675993494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.393506444741" Y="-24.815315476505" />
                  <Point X="3.002351818669" Y="-23.572086570062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.528918478685" Y="-24.744858823503" />
                  <Point X="3.026420117185" Y="-23.470630080735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73824560198" Y="-24.688769808419" />
                  <Point X="3.080462608099" Y="-23.363347198624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.947572725276" Y="-24.632680793336" />
                  <Point X="3.186196196446" Y="-23.246016585014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.156899848572" Y="-24.576591778252" />
                  <Point X="3.355108541378" Y="-23.116405265168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.366227053045" Y="-24.520502778948" />
                  <Point X="3.524020886309" Y="-22.986793945323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.575554540224" Y="-24.464413834596" />
                  <Point X="3.692933266149" Y="-22.857182618692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784882027402" Y="-24.408324890244" />
                  <Point X="2.363638944124" Y="-23.018793173921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.652432219273" Y="-22.962657447821" />
                  <Point X="3.861846627321" Y="-22.727571101309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770740650253" Y="-24.308797998961" />
                  <Point X="2.199159506622" Y="-22.953986651809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.905136391482" Y="-22.816758646685" />
                  <Point X="4.030759988494" Y="-22.597959583926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.755995949006" Y="-24.209153833354" />
                  <Point X="2.10863640027" Y="-22.874804475185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.157840563692" Y="-22.67085984555" />
                  <Point X="4.11740624295" Y="-22.484339172178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74125124776" Y="-24.109509667747" />
                  <Point X="2.051999107703" Y="-22.789035563602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.410544735901" Y="-22.524961044415" />
                  <Point X="4.065005888845" Y="-22.397746683187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.722272593169" Y="-24.009042504979" />
                  <Point X="2.02158056385" Y="-22.698170243539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.663248981979" Y="-22.379062228921" />
                  <Point X="4.007406199977" Y="-22.312164842495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.694589358653" Y="-23.906883343276" />
                  <Point X="-3.979473954189" Y="-23.767878989887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.488567361547" Y="-23.672456414651" />
                  <Point X="2.013086229849" Y="-22.603043288787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.915953242546" Y="-22.233163410611" />
                  <Point X="3.946314751643" Y="-22.227261731087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.666906124138" Y="-23.804724181572" />
                  <Point X="-4.276310483101" Y="-23.728800080119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.43765556098" Y="-23.565782077098" />
                  <Point X="2.029630250705" Y="-22.503049370878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.639222740366" Y="-23.702564990856" />
                  <Point X="-4.573145390231" Y="-23.689720855109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421514356187" Y="-23.465866458699" />
                  <Point X="2.066652331152" Y="-22.399074921415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.444686855469" Y="-23.373592650253" />
                  <Point X="2.129590421469" Y="-22.290062909942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.49255246308" Y="-23.286118695836" />
                  <Point X="2.192528511785" Y="-22.181050898469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.576164695724" Y="-23.205593181445" />
                  <Point X="2.255466602102" Y="-22.072038886996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.676796101758" Y="-23.128375859238" />
                  <Point X="2.318404692418" Y="-21.963026875523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.777427507792" Y="-23.051158537031" />
                  <Point X="2.381342782735" Y="-21.85401486405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.878058913826" Y="-22.973941214824" />
                  <Point X="2.444280873051" Y="-21.745002852577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.97869031986" Y="-22.896723892617" />
                  <Point X="2.507218850554" Y="-21.635990863033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079322259614" Y="-22.819506674155" />
                  <Point X="2.570156802658" Y="-21.526978878426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.179954436349" Y="-22.742289501757" />
                  <Point X="2.633094754762" Y="-21.417966893818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.194075156922" Y="-22.648256205766" />
                  <Point X="2.696032706866" Y="-21.308954909211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.130357652771" Y="-22.539092691591" />
                  <Point X="2.758970658971" Y="-21.199942924604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.066639760822" Y="-22.429929102036" />
                  <Point X="-3.195936811092" Y="-22.2606815935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.869108092564" Y="-22.197152526158" />
                  <Point X="2.810765645207" Y="-21.093096913147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.002921868873" Y="-22.320765512481" />
                  <Point X="-3.33498721214" Y="-22.190932167421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.773127316317" Y="-22.081717667179" />
                  <Point X="2.703889656912" Y="-21.01709341477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.914459934843" Y="-22.206792168376" />
                  <Point X="-3.460391017117" Y="-22.118530111779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748769428955" Y="-21.980204887483" />
                  <Point X="2.591651544885" Y="-20.942132207662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.825752114452" Y="-22.092771028805" />
                  <Point X="-3.585794936691" Y="-22.046128078412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755005762632" Y="-21.88463902193" />
                  <Point X="2.467094744698" Y="-20.869565510967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73704491847" Y="-21.978750010606" />
                  <Point X="-3.711199145846" Y="-21.973726101334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777544399484" Y="-21.792242003108" />
                  <Point X="2.342538008387" Y="-20.796998801855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.825709364336" Y="-21.704826237845" />
                  <Point X="2.212348464692" Y="-20.725526999585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.875946518862" Y="-21.617813265451" />
                  <Point X="2.066168816607" Y="-20.657163358748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.926183673387" Y="-21.530800293057" />
                  <Point X="1.919988724819" Y="-20.588799804159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.976420827913" Y="-21.443787320663" />
                  <Point X="-0.106427768726" Y="-20.885917182595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.188918457032" Y="-20.82850769193" />
                  <Point X="1.757899757192" Y="-20.523528621774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.026658106341" Y="-21.356774372354" />
                  <Point X="-0.192611916574" Y="-20.805891597876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.242523658619" Y="-20.721309810253" />
                  <Point X="1.584177935174" Y="-20.460518637221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.000456008153" Y="-21.254903114387" />
                  <Point X="-0.231456502966" Y="-20.716664134567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.269880083342" Y="-20.619214173938" />
                  <Point X="1.398443151146" Y="-20.399843735937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.831357784825" Y="-21.125255663441" />
                  <Point X="-1.930596513862" Y="-20.950165409132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.595169114304" Y="-20.884964927513" />
                  <Point X="-0.256104238415" Y="-20.624677082982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.297236508065" Y="-20.517118537623" />
                  <Point X="1.176381634216" Y="-20.346230036225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.662259326186" Y="-20.995608166756" />
                  <Point X="-2.06343565723" Y="-20.879208636865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.508121613653" Y="-20.771266521406" />
                  <Point X="-0.280751973864" Y="-20.532690031397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.324592932788" Y="-20.415022901307" />
                  <Point X="0.954317863827" Y="-20.29261677454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.40243154024" Y="-20.84832467538" />
                  <Point X="-2.141897356429" Y="-20.79768196019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475054879143" Y="-20.668060913309" />
                  <Point X="-0.305399709313" Y="-20.440702979813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.351949306933" Y="-20.312927274823" />
                  <Point X="0.688560582684" Y="-20.247496670984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462714041728" Y="-20.568884011496" />
                  <Point X="-0.33004749896" Y="-20.348715938763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474390920498" Y="-20.474375680781" />
                  <Point X="-0.354695313963" Y="-20.256728902642" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.653104858398" Y="-29.214724609375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.521544921875" />
                  <Point X="0.390108276367" Y="-28.414623046875" />
                  <Point X="0.300591186523" Y="-28.28564453125" />
                  <Point X="0.274335754395" Y="-28.2663984375" />
                  <Point X="0.159499206543" Y="-28.2307578125" />
                  <Point X="0.020976625443" Y="-28.187765625" />
                  <Point X="-0.008664756775" Y="-28.187765625" />
                  <Point X="-0.123501304626" Y="-28.22340625" />
                  <Point X="-0.262023895264" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.362489562988" Y="-28.39256640625" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.678242553711" Y="-29.35448828125" />
                  <Point X="-0.847744140625" Y="-29.987078125" />
                  <Point X="-0.937840576172" Y="-29.96958984375" />
                  <Point X="-1.100258789062" Y="-29.9380625" />
                  <Point X="-1.232813232422" Y="-29.903958984375" />
                  <Point X="-1.35158972168" Y="-29.8733984375" />
                  <Point X="-1.331006591797" Y="-29.7170546875" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.337117675781" Y="-29.3968359375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282958984" Y="-29.20262890625" />
                  <Point X="-1.492009765625" Y="-29.109908203125" />
                  <Point X="-1.619543701172" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.789563842773" Y="-28.97656640625" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878295898" Y="-28.973791015625" />
                  <Point X="-2.106802978516" Y="-29.05191796875" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259733886719" Y="-29.157294921875" />
                  <Point X="-2.382694091797" Y="-29.317541015625" />
                  <Point X="-2.457094238281" Y="-29.4145" />
                  <Point X="-2.617781494141" Y="-29.3150078125" />
                  <Point X="-2.855839355469" Y="-29.167607421875" />
                  <Point X="-3.039401611328" Y="-29.026271484375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.843453369141" Y="-28.213548828125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513981445312" Y="-27.56876171875" />
                  <Point X="-2.531330078125" Y="-27.5514140625" />
                  <Point X="-2.560158447266" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.296655029297" Y="-27.950486328125" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.973629394531" Y="-28.094220703125" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.293307128906" Y="-27.626451171875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.754161621094" Y="-26.87615625" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140326416016" Y="-26.334595703125" />
                  <Point X="-3.161159912109" Y="-26.310638671875" />
                  <Point X="-3.187642578125" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-4.109488769531" Y="-26.405736328125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.8538359375" Y="-26.2991640625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.962211914062" Y="-25.7677421875" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.229989746094" Y="-25.308849609375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.529317138672" Y="-25.1126953125" />
                  <Point X="-3.502324462891" Y="-25.090646484375" />
                  <Point X="-3.490705810547" Y="-25.0623984375" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.489841064453" Y="-25.00294921875" />
                  <Point X="-3.502324462891" Y="-24.9719140625" />
                  <Point X="-3.526722412109" Y="-24.951666015625" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.368704101562" Y="-24.71654296875" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.965051269531" Y="-24.32394921875" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.847555175781" Y="-23.7449296875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.235349609375" Y="-23.542552734375" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731703857422" Y="-23.6041328125" />
                  <Point X="-3.703862304688" Y="-23.59535546875" />
                  <Point X="-3.670277832031" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.573943359375" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.627948486328" Y="-23.529244140625" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.629795898438" Y="-23.42859375" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.125297851563" Y="-23.02371875" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.34421875" Y="-22.528580078125" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.974349365234" Y="-21.974349609375" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.455239013672" Y="-21.90211328125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514404297" Y="-22.07956640625" />
                  <Point X="-3.099738525391" Y="-22.082958984375" />
                  <Point X="-3.052965087891" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.985729492188" Y="-22.045072265625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.941466796875" Y="-21.933384765625" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.158269042969" Y="-21.50881640625" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.073700927734" Y="-21.071642578125" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.460459960938" Y="-20.663208984375" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.058466308594" Y="-20.594302734375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247558594" Y="-20.726337890625" />
                  <Point X="-1.908090332031" Y="-20.7488046875" />
                  <Point X="-1.85603137207" Y="-20.77590625" />
                  <Point X="-1.835125" Y="-20.781509765625" />
                  <Point X="-1.813809570312" Y="-20.77775" />
                  <Point X="-1.768858398438" Y="-20.75912890625" />
                  <Point X="-1.714635620117" Y="-20.736669921875" />
                  <Point X="-1.696905517578" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.671452636719" Y="-20.659109375" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.675360473633" Y="-20.4035078125" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.383421875" Y="-20.2131484375" />
                  <Point X="-0.968083007812" Y="-20.096703125" />
                  <Point X="-0.61359765625" Y="-20.05521484375" />
                  <Point X="-0.224199966431" Y="-20.009640625" />
                  <Point X="-0.129729553223" Y="-20.362208984375" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282131195" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594051361" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.160107513428" Y="-20.294787109375" />
                  <Point X="0.236648422241" Y="-20.0091328125" />
                  <Point X="0.497563232422" Y="-20.03645703125" />
                  <Point X="0.860210021973" Y="-20.074435546875" />
                  <Point X="1.153490478516" Y="-20.1452421875" />
                  <Point X="1.508455810547" Y="-20.230943359375" />
                  <Point X="1.699518188477" Y="-20.3002421875" />
                  <Point X="1.931044311523" Y="-20.38421875" />
                  <Point X="2.115625488281" Y="-20.470541015625" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.517024658203" Y="-20.678759765625" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.900706054688" Y="-20.923912109375" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.620391357422" Y="-21.819970703125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.215120849609" Y="-22.544876953125" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.205839111328" Y="-22.639142578125" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.238153076172" Y="-22.7278828125" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.274938720703" Y="-22.775794921875" />
                  <Point X="2.303633544922" Y="-22.795265625" />
                  <Point X="2.338247314453" Y="-22.81875390625" />
                  <Point X="2.360335449219" Y="-22.827021484375" />
                  <Point X="2.391802734375" Y="-22.830814453125" />
                  <Point X="2.429760253906" Y="-22.835392578125" />
                  <Point X="2.448664306641" Y="-22.8340546875" />
                  <Point X="2.4850546875" Y="-22.824322265625" />
                  <Point X="2.528950683594" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.356985595703" Y="-22.3364921875" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.074765136719" Y="-22.080470703125" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.296351074219" Y="-22.41305859375" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.802795654297" Y="-23.012373046875" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.27937109375" Y="-23.41616796875" />
                  <Point X="3.253180908203" Y="-23.450333984375" />
                  <Point X="3.221588867188" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.203363769531" Y="-23.543384765625" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.198788085938" Y="-23.64784765625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.237428710938" Y="-23.74515234375" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280947998047" Y="-23.8011796875" />
                  <Point X="3.312513671875" Y="-23.81894921875" />
                  <Point X="3.350590087891" Y="-23.8403828125" />
                  <Point X="3.368566650391" Y="-23.846380859375" />
                  <Point X="3.411245361328" Y="-23.852021484375" />
                  <Point X="3.462727294922" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.250903320312" Y="-23.75678515625" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.884287597656" Y="-23.823099609375" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.968734863281" Y="-24.238384765625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.330428710938" Y="-24.60428125" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087890625" Y="-24.767181640625" />
                  <Point X="3.687157226562" Y="-24.79141796875" />
                  <Point X="3.636577880859" Y="-24.82065234375" />
                  <Point X="3.622264892578" Y="-24.833072265625" />
                  <Point X="3.597106445312" Y="-24.865130859375" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.548598876953" Y="-24.9690546875" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.546869384766" Y="-25.084474609375" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.591917480469" Y="-25.190818359375" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.636576660156" Y="-25.241908203125" />
                  <Point X="3.678507324219" Y="-25.26614453125" />
                  <Point X="3.729086669922" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.451969238281" Y="-25.49084765625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.9790859375" Y="-25.763078125" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.910578125" Y="-26.132279296875" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.095359130859" Y="-26.18759765625" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836181641" Y="-26.098341796875" />
                  <Point X="3.312541015625" Y="-26.116228515625" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.135703125" Y="-26.214521484375" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.057228515625" Y="-26.391544921875" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.101903564453" Y="-26.5874609375" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.828088867188" Y="-27.19169140625" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.290542480469" Y="-27.662314453125" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.12584765625" Y="-27.913373046875" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.362093994141" Y="-27.610615234375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340332031" Y="-27.253314453125" />
                  <Point X="2.639395996094" Y="-27.235625" />
                  <Point X="2.52125" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.407709960938" Y="-27.262068359375" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.245776611328" Y="-27.41605078125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.2068515625" Y="-27.6443203125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.657968994141" Y="-28.512755859375" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.937833251953" Y="-29.116974609375" />
                  <Point X="2.835293212891" Y="-29.190216796875" />
                  <Point X="2.747772949219" Y="-29.2468671875" />
                  <Point X="2.679775634766" Y="-29.290880859375" />
                  <Point X="2.147625732422" Y="-28.597369140625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549194336" Y="-27.980466796875" />
                  <Point X="1.573949707031" Y="-27.91836328125" />
                  <Point X="1.45742578125" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.320156860352" Y="-27.845439453125" />
                  <Point X="1.192718139648" Y="-27.857166015625" />
                  <Point X="1.165332275391" Y="-27.868509765625" />
                  <Point X="1.08375378418" Y="-27.93633984375" />
                  <Point X="0.985348754883" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.944065185547" Y="-28.15820703125" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.034053100586" Y="-29.2231484375" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.091351196289" Y="-29.941984375" />
                  <Point X="0.994368469238" Y="-29.9632421875" />
                  <Point X="0.91348815918" Y="-29.977935546875" />
                  <Point X="0.860200378418" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#162" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.087589240046" Y="4.682011984302" Z="1.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.15" />
                  <Point X="-0.625565786334" Y="5.025726147262" Z="1.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.15" />
                  <Point X="-1.403075624132" Y="4.866276683211" Z="1.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.15" />
                  <Point X="-1.731088682022" Y="4.621246401114" Z="1.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.15" />
                  <Point X="-1.725294383833" Y="4.38720683628" Z="1.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.15" />
                  <Point X="-1.794147891476" Y="4.318344114707" Z="1.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.15" />
                  <Point X="-1.891157919395" Y="4.326824904047" Z="1.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.15" />
                  <Point X="-2.02495484578" Y="4.467415259401" Z="1.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.15" />
                  <Point X="-2.490898777771" Y="4.411779151341" Z="1.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.15" />
                  <Point X="-3.1102790809" Y="3.999318244864" Z="1.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.15" />
                  <Point X="-3.207726253463" Y="3.497464479469" Z="1.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.15" />
                  <Point X="-2.997432485308" Y="3.093539306272" Z="1.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.15" />
                  <Point X="-3.02724025057" Y="3.021563317317" Z="1.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.15" />
                  <Point X="-3.101537159334" Y="2.998132213865" Z="1.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.15" />
                  <Point X="-3.436394868828" Y="3.172467695282" Z="1.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.15" />
                  <Point X="-4.019969030777" Y="3.087634909834" Z="1.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.15" />
                  <Point X="-4.393556798386" Y="2.527905477937" Z="1.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.15" />
                  <Point X="-4.161891733669" Y="1.967893921913" Z="1.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.15" />
                  <Point X="-3.680302199361" Y="1.579598773737" Z="1.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.15" />
                  <Point X="-3.680298294316" Y="1.521170772872" Z="1.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.15" />
                  <Point X="-3.725054299057" Y="1.483611002958" Z="1.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.15" />
                  <Point X="-4.234978979452" Y="1.538299972978" Z="1.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.15" />
                  <Point X="-4.901971065093" Y="1.299428578334" Z="1.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.15" />
                  <Point X="-5.020668846551" Y="0.714649400672" Z="1.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.15" />
                  <Point X="-4.387801197235" Y="0.266440261951" Z="1.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.15" />
                  <Point X="-3.561387014679" Y="0.038537603163" Z="1.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.15" />
                  <Point X="-3.54374986551" Y="0.013510176796" Z="1.15" />
                  <Point X="-3.539556741714" Y="0" Z="1.15" />
                  <Point X="-3.544614735625" Y="-0.016296774265" Z="1.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.15" />
                  <Point X="-3.563981580444" Y="-0.040338379971" Z="1.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.15" />
                  <Point X="-4.249086591538" Y="-0.229271794805" Z="1.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.15" />
                  <Point X="-5.017863993632" Y="-0.743540257442" Z="1.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.15" />
                  <Point X="-4.908436621557" Y="-1.28025932209" Z="1.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.15" />
                  <Point X="-4.109118534658" Y="-1.42402869573" Z="1.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.15" />
                  <Point X="-3.204679692677" Y="-1.31538513216" Z="1.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.15" />
                  <Point X="-3.196888693617" Y="-1.338714000332" Z="1.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.15" />
                  <Point X="-3.790755601627" Y="-1.805207667793" Z="1.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.15" />
                  <Point X="-4.342406211938" Y="-2.62078031245" Z="1.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.15" />
                  <Point X="-4.019571260284" Y="-3.093223691376" Z="1.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.15" />
                  <Point X="-3.277810989185" Y="-2.962506444866" Z="1.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.15" />
                  <Point X="-2.563354293456" Y="-2.564976175676" Z="1.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.15" />
                  <Point X="-2.892910568303" Y="-3.157267393307" Z="1.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.15" />
                  <Point X="-3.076061385528" Y="-4.034607213638" Z="1.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.15" />
                  <Point X="-2.65025925522" Y="-4.326238096227" Z="1.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.15" />
                  <Point X="-2.349182549246" Y="-4.316697069993" Z="1.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.15" />
                  <Point X="-2.085180738284" Y="-4.062211097272" Z="1.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.15" />
                  <Point X="-1.798989960659" Y="-3.995178661957" Z="1.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.15" />
                  <Point X="-1.531132847586" Y="-4.116220404554" Z="1.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.15" />
                  <Point X="-1.392312957069" Y="-4.375310207644" Z="1.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.15" />
                  <Point X="-1.386734768903" Y="-4.679246736002" Z="1.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.15" />
                  <Point X="-1.251428347995" Y="-4.92109971651" Z="1.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.15" />
                  <Point X="-0.95352417825" Y="-4.987392803613" Z="1.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.15" />
                  <Point X="-0.636102237626" Y="-4.336149952577" Z="1.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.15" />
                  <Point X="-0.327569627717" Y="-3.389795928804" Z="1.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.15" />
                  <Point X="-0.114836500658" Y="-3.23988038038" Z="1.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.15" />
                  <Point X="0.138522578703" Y="-3.247231664908" Z="1.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.15" />
                  <Point X="0.342876231715" Y="-3.411849826964" Z="1.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.15" />
                  <Point X="0.598652622775" Y="-4.196386077239" Z="1.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.15" />
                  <Point X="0.916269045352" Y="-4.995850754123" Z="1.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.15" />
                  <Point X="1.095900759719" Y="-4.95954381364" Z="1.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.15" />
                  <Point X="1.077469391407" Y="-4.185342972699" Z="1.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.15" />
                  <Point X="0.986768397081" Y="-3.137546593711" Z="1.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.15" />
                  <Point X="1.109565059097" Y="-2.943505447159" Z="1.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.15" />
                  <Point X="1.318582529136" Y="-2.863948407564" Z="1.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.15" />
                  <Point X="1.540754437526" Y="-2.929140742709" Z="1.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.15" />
                  <Point X="2.101801955131" Y="-3.596525639874" Z="1.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.15" />
                  <Point X="2.768785828473" Y="-4.257560419713" Z="1.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.15" />
                  <Point X="2.960738896786" Y="-4.126381136638" Z="1.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.15" />
                  <Point X="2.69511430578" Y="-3.4564755239" Z="1.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.15" />
                  <Point X="2.249899780098" Y="-2.604152451245" Z="1.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.15" />
                  <Point X="2.283866993571" Y="-2.408057784782" Z="1.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.15" />
                  <Point X="2.42484059359" Y="-2.275034316396" Z="1.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.15" />
                  <Point X="2.624354352267" Y="-2.253548229393" Z="1.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.15" />
                  <Point X="3.330938073207" Y="-2.622635194495" Z="1.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.15" />
                  <Point X="4.160580471297" Y="-2.910869283072" Z="1.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.15" />
                  <Point X="4.326920440091" Y="-2.657319907355" Z="1.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.15" />
                  <Point X="3.852370824581" Y="-2.120743463248" Z="1.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.15" />
                  <Point X="3.137805804721" Y="-1.52914210515" Z="1.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.15" />
                  <Point X="3.100862217177" Y="-1.364847385313" Z="1.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.15" />
                  <Point X="3.167993379489" Y="-1.215208546291" Z="1.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.15" />
                  <Point X="3.317004753485" Y="-1.133807606309" Z="1.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.15" />
                  <Point X="4.082676917651" Y="-1.205888714104" Z="1.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.15" />
                  <Point X="4.953169245067" Y="-1.112123391744" Z="1.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.15" />
                  <Point X="5.022371380808" Y="-0.739250782891" Z="1.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.15" />
                  <Point X="4.45875414032" Y="-0.411269300136" Z="1.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.15" />
                  <Point X="3.697373412327" Y="-0.191574902187" Z="1.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.15" />
                  <Point X="3.62509514229" Y="-0.128668268591" Z="1.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.15" />
                  <Point X="3.589820866242" Y="-0.043789294484" Z="1.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.15" />
                  <Point X="3.591550584182" Y="0.05282123673" Z="1.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.15" />
                  <Point X="3.63028429611" Y="0.135280469984" Z="1.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.15" />
                  <Point X="3.706022002026" Y="0.196573942055" Z="1.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.15" />
                  <Point X="4.337213676987" Y="0.378702632633" Z="1.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.15" />
                  <Point X="5.011983461319" Y="0.800586742641" Z="1.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.15" />
                  <Point X="4.926712717109" Y="1.220007811904" Z="1.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.15" />
                  <Point X="4.238220852428" Y="1.324067891471" Z="1.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.15" />
                  <Point X="3.41163955611" Y="1.228828037834" Z="1.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.15" />
                  <Point X="3.33088423061" Y="1.255902287985" Z="1.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.15" />
                  <Point X="3.273043367322" Y="1.313608156799" Z="1.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.15" />
                  <Point X="3.241600595094" Y="1.393535631651" Z="1.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.15" />
                  <Point X="3.245360127151" Y="1.474429089023" Z="1.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.15" />
                  <Point X="3.286708047418" Y="1.550528036371" Z="1.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.15" />
                  <Point X="3.827077884251" Y="1.979239067042" Z="1.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.15" />
                  <Point X="4.332972433677" Y="2.644108468985" Z="1.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.15" />
                  <Point X="4.109194571834" Y="2.980013403414" Z="1.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.15" />
                  <Point X="3.32582963409" Y="2.73808868267" Z="1.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.15" />
                  <Point X="2.465981879757" Y="2.255260394789" Z="1.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.15" />
                  <Point X="2.391634019816" Y="2.250106162645" Z="1.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.15" />
                  <Point X="2.325553122027" Y="2.277387311336" Z="1.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.15" />
                  <Point X="2.273371288899" Y="2.331471738355" Z="1.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.15" />
                  <Point X="2.249323540114" Y="2.398124424016" Z="1.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.15" />
                  <Point X="2.257267547869" Y="2.473487696125" Z="1.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.15" />
                  <Point X="2.65753667019" Y="3.186309447865" Z="1.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.15" />
                  <Point X="2.923527160991" Y="4.148116541173" Z="1.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.15" />
                  <Point X="2.536038514268" Y="4.3957243076" Z="1.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.15" />
                  <Point X="2.130650966854" Y="4.606030640736" Z="1.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.15" />
                  <Point X="1.710410957197" Y="4.778042248186" Z="1.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.15" />
                  <Point X="1.159069124916" Y="4.934641203982" Z="1.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.15" />
                  <Point X="0.496615135178" Y="5.044552030988" Z="1.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.15" />
                  <Point X="0.105655321507" Y="4.749435521649" Z="1.15" />
                  <Point X="0" Y="4.355124473572" Z="1.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>