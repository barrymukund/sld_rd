<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#143" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1141" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.995283203125" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.666013305664" Y="-28.89584765625" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557719848633" Y="-28.497138671875" />
                  <Point X="0.542362548828" Y="-28.467376953125" />
                  <Point X="0.500381896973" Y="-28.406892578125" />
                  <Point X="0.378634918213" Y="-28.2314765625" />
                  <Point X="0.356755554199" Y="-28.209025390625" />
                  <Point X="0.330499908447" Y="-28.189779296875" />
                  <Point X="0.302495178223" Y="-28.17566796875" />
                  <Point X="0.237532531738" Y="-28.155505859375" />
                  <Point X="0.049136188507" Y="-28.09703515625" />
                  <Point X="0.020976638794" Y="-28.092765625" />
                  <Point X="-0.008664536476" Y="-28.092765625" />
                  <Point X="-0.036824085236" Y="-28.09703515625" />
                  <Point X="-0.101786888123" Y="-28.117197265625" />
                  <Point X="-0.290183227539" Y="-28.17566796875" />
                  <Point X="-0.31818460083" Y="-28.189775390625" />
                  <Point X="-0.344440093994" Y="-28.20901953125" />
                  <Point X="-0.366323730469" Y="-28.2314765625" />
                  <Point X="-0.408304260254" Y="-28.291962890625" />
                  <Point X="-0.530051391602" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.848860107422" Y="-29.62419140625" />
                  <Point X="-0.916584533691" Y="-29.87694140625" />
                  <Point X="-1.079359741211" Y="-29.845345703125" />
                  <Point X="-1.150935424805" Y="-29.826931640625" />
                  <Point X="-1.24641809082" Y="-29.802365234375" />
                  <Point X="-1.227326904297" Y="-29.657353515625" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.232028198242" Y="-29.438203125" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.323645019531" Y="-29.131203125" />
                  <Point X="-1.383454467773" Y="-29.078751953125" />
                  <Point X="-1.556905761719" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886474609" Y="-28.897994140625" />
                  <Point X="-1.64302722168" Y="-28.890966796875" />
                  <Point X="-1.722407592773" Y="-28.885763671875" />
                  <Point X="-1.952616333008" Y="-28.87067578125" />
                  <Point X="-1.983414794922" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042657226562" Y="-28.89480078125" />
                  <Point X="-2.108801025391" Y="-28.93899609375" />
                  <Point X="-2.300623535156" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.4801484375" Y="-29.288490234375" />
                  <Point X="-2.481283447266" Y="-29.287787109375" />
                  <Point X="-2.801720947266" Y="-29.089380859375" />
                  <Point X="-2.900843261719" Y="-29.01305859375" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.614638427734" Y="-28.007228515625" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858398438" Y="-27.647650390625" />
                  <Point X="-2.406587890625" Y="-27.61612109375" />
                  <Point X="-2.405576660156" Y="-27.58518359375" />
                  <Point X="-2.414563476562" Y="-27.55556640625" />
                  <Point X="-2.428783691406" Y="-27.526736328125" />
                  <Point X="-2.446810546875" Y="-27.50158203125" />
                  <Point X="-2.464155517578" Y="-27.48423828125" />
                  <Point X="-2.489311035156" Y="-27.466212890625" />
                  <Point X="-2.518139892578" Y="-27.45199609375" />
                  <Point X="-2.547758056641" Y="-27.44301171875" />
                  <Point X="-2.578692138672" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.597973876953" Y="-28.0147578125" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.829706298828" Y="-28.126453125" />
                  <Point X="-4.082863037109" Y="-27.793857421875" />
                  <Point X="-4.153930175781" Y="-27.674689453125" />
                  <Point X="-4.306143066406" Y="-27.419451171875" />
                  <Point X="-3.439927734375" Y="-26.75478125" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083064941406" Y="-26.4758828125" />
                  <Point X="-3.063814941406" Y="-26.445048828125" />
                  <Point X="-3.055125732422" Y="-26.4223515625" />
                  <Point X="-3.051881103516" Y="-26.41220703125" />
                  <Point X="-3.046152099609" Y="-26.390087890625" />
                  <Point X="-3.042037353516" Y="-26.35860546875" />
                  <Point X="-3.042736328125" Y="-26.335736328125" />
                  <Point X="-3.048882568359" Y="-26.31369921875" />
                  <Point X="-3.060887939453" Y="-26.28471484375" />
                  <Point X="-3.073649658203" Y="-26.26276953125" />
                  <Point X="-3.091767578125" Y="-26.244986328125" />
                  <Point X="-3.111060302734" Y="-26.230560546875" />
                  <Point X="-3.119763916016" Y="-26.22476953125" />
                  <Point X="-3.139456298828" Y="-26.2131796875" />
                  <Point X="-3.168721679688" Y="-26.201955078125" />
                  <Point X="-3.200608642578" Y="-26.195474609375" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.442311035156" Y="-26.353734375" />
                  <Point X="-4.732102539062" Y="-26.391884765625" />
                  <Point X="-4.735067382812" Y="-26.38027734375" />
                  <Point X="-4.834077636719" Y="-25.99265625" />
                  <Point X="-4.852880371094" Y="-25.8611875" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.913321777344" Y="-25.322349609375" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.512578125" Y="-25.212373046875" />
                  <Point X="-3.489895263672" Y="-25.200212890625" />
                  <Point X="-3.480616210938" Y="-25.194533203125" />
                  <Point X="-3.459979248047" Y="-25.1802109375" />
                  <Point X="-3.436023193359" Y="-25.15868359375" />
                  <Point X="-3.415299316406" Y="-25.12874609375" />
                  <Point X="-3.405501220703" Y="-25.106365234375" />
                  <Point X="-3.401796142578" Y="-25.09642578125" />
                  <Point X="-3.394917236328" Y="-25.07426171875" />
                  <Point X="-3.389474365234" Y="-25.0455234375" />
                  <Point X="-3.390347412109" Y="-25.0121484375" />
                  <Point X="-3.394966064453" Y="-24.989685546875" />
                  <Point X="-3.397288574219" Y="-24.98066015625" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.417488037109" Y="-24.929162109375" />
                  <Point X="-3.440013427734" Y="-24.900203125" />
                  <Point X="-3.458951904297" Y="-24.88374609375" />
                  <Point X="-3.467096923828" Y="-24.877412109375" />
                  <Point X="-3.4877265625" Y="-24.86309375" />
                  <Point X="-3.501915771484" Y="-24.854958984375" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.636196289062" Y="-24.546517578125" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.888298339844" Y="-24.454251953125" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.786637207031" Y="-23.883345703125" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.038379150391" Y="-23.6643046875" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985107422" Y="-23.700658203125" />
                  <Point X="-3.723423095703" Y="-23.698771484375" />
                  <Point X="-3.703142822266" Y="-23.69473828125" />
                  <Point X="-3.687392822266" Y="-23.6897734375" />
                  <Point X="-3.641716796875" Y="-23.67537109375" />
                  <Point X="-3.622776611328" Y="-23.667037109375" />
                  <Point X="-3.604032714844" Y="-23.65621484375" />
                  <Point X="-3.587353027344" Y="-23.643984375" />
                  <Point X="-3.573715576172" Y="-23.62843359375" />
                  <Point X="-3.561301513672" Y="-23.610705078125" />
                  <Point X="-3.551351318359" Y="-23.592568359375" />
                  <Point X="-3.545031494141" Y="-23.577310546875" />
                  <Point X="-3.526703857422" Y="-23.533064453125" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532050048828" Y="-23.410623046875" />
                  <Point X="-3.539675537109" Y="-23.395974609375" />
                  <Point X="-3.561789794922" Y="-23.353494140625" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.235002929688" Y="-22.81979296875" />
                  <Point X="-4.351860351563" Y="-22.730126953125" />
                  <Point X="-4.329097167969" Y="-22.691126953125" />
                  <Point X="-4.081155761719" Y="-22.266345703125" />
                  <Point X="-3.980890136719" Y="-22.137466796875" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.374149414062" Y="-22.058626953125" />
                  <Point X="-3.206656738281" Y="-22.155328125" />
                  <Point X="-3.187728271484" Y="-22.163658203125" />
                  <Point X="-3.167085449219" Y="-22.17016796875" />
                  <Point X="-3.146797363281" Y="-22.174205078125" />
                  <Point X="-3.124862060547" Y="-22.176125" />
                  <Point X="-3.061248046875" Y="-22.181689453125" />
                  <Point X="-3.040559570312" Y="-22.18123828125" />
                  <Point X="-3.0191015625" Y="-22.178412109375" />
                  <Point X="-2.999012207031" Y="-22.173494140625" />
                  <Point X="-2.980462890625" Y="-22.16434765625" />
                  <Point X="-2.962209228516" Y="-22.15271875" />
                  <Point X="-2.946076660156" Y="-22.13976953125" />
                  <Point X="-2.930506835938" Y="-22.12419921875" />
                  <Point X="-2.885353027344" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.845354980469" Y="-21.9419453125" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.150237548828" Y="-21.3327265625" />
                  <Point X="-3.183333251953" Y="-21.275404296875" />
                  <Point X="-3.132461181641" Y="-21.236400390625" />
                  <Point X="-2.70062890625" Y="-20.905318359375" />
                  <Point X="-2.542716552734" Y="-20.8175859375" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.094469970703" Y="-20.703435546875" />
                  <Point X="-2.04319519043" Y="-20.7702578125" />
                  <Point X="-2.028890869141" Y="-20.785201171875" />
                  <Point X="-2.012310791016" Y="-20.79911328125" />
                  <Point X="-1.995117797852" Y="-20.8106015625" />
                  <Point X="-1.970703979492" Y="-20.8233125" />
                  <Point X="-1.899901977539" Y="-20.860169921875" />
                  <Point X="-1.88062512207" Y="-20.86766796875" />
                  <Point X="-1.859718383789" Y="-20.873271484375" />
                  <Point X="-1.839268432617" Y="-20.876419921875" />
                  <Point X="-1.818621826172" Y="-20.87506640625" />
                  <Point X="-1.797306518555" Y="-20.871306640625" />
                  <Point X="-1.777452880859" Y="-20.865517578125" />
                  <Point X="-1.752024169922" Y="-20.854984375" />
                  <Point X="-1.678278930664" Y="-20.8244375" />
                  <Point X="-1.660145507812" Y="-20.814490234375" />
                  <Point X="-1.642416137695" Y="-20.802076171875" />
                  <Point X="-1.626864013672" Y="-20.7884375" />
                  <Point X="-1.61463269043" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595480102539" Y="-20.734078125" />
                  <Point X="-1.587203491211" Y="-20.707828125" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584201904297" Y="-20.368103515625" />
                  <Point X="-1.508673095703" Y="-20.346927734375" />
                  <Point X="-0.94962310791" Y="-20.19019140625" />
                  <Point X="-0.758207580566" Y="-20.167787109375" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.183452407837" Y="-20.528765625" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113998413" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.0061559062" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425956726" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.289910827637" Y="-20.17740625" />
                  <Point X="0.307419677734" Y="-20.1120625" />
                  <Point X="0.355912963867" Y="-20.117140625" />
                  <Point X="0.84403125" Y="-20.168259765625" />
                  <Point X="1.00241973877" Y="-20.2065" />
                  <Point X="1.48103894043" Y="-20.3220546875" />
                  <Point X="1.583013305664" Y="-20.359041015625" />
                  <Point X="1.894647583008" Y="-20.472072265625" />
                  <Point X="1.994331298828" Y="-20.51869140625" />
                  <Point X="2.294560546875" Y="-20.659099609375" />
                  <Point X="2.390908203125" Y="-20.71523046875" />
                  <Point X="2.680969970703" Y="-20.884220703125" />
                  <Point X="2.7718046875" Y="-20.948818359375" />
                  <Point X="2.943259277344" Y="-21.07074609375" />
                  <Point X="2.368507080078" Y="-22.066248046875" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.127571777344" Y="-22.50453125" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108405273438" Y="-22.5908671875" />
                  <Point X="2.109214111328" Y="-22.6275703125" />
                  <Point X="2.109874267578" Y="-22.636849609375" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.140071533203" Y="-22.752529296875" />
                  <Point X="2.151086181641" Y="-22.76876171875" />
                  <Point X="2.183029296875" Y="-22.815837890625" />
                  <Point X="2.201254394531" Y="-22.835833984375" />
                  <Point X="2.230786865234" Y="-22.860150390625" />
                  <Point X="2.237830810547" Y="-22.865423828125" />
                  <Point X="2.284906738281" Y="-22.8973671875" />
                  <Point X="2.304954345703" Y="-22.90773046875" />
                  <Point X="2.327043457031" Y="-22.91599609375" />
                  <Point X="2.348964599609" Y="-22.921337890625" />
                  <Point X="2.366765625" Y="-22.923484375" />
                  <Point X="2.418389404297" Y="-22.929708984375" />
                  <Point X="2.436469482422" Y="-22.93015625" />
                  <Point X="2.473210205078" Y="-22.925828125" />
                  <Point X="2.493796142578" Y="-22.920322265625" />
                  <Point X="2.553496582031" Y="-22.904357421875" />
                  <Point X="2.565287353516" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.69826171875" Y="-22.24915234375" />
                  <Point X="3.967325439453" Y="-22.09380859375" />
                  <Point X="4.123274414062" Y="-22.310541015625" />
                  <Point X="4.173907226562" Y="-22.394212890625" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.521653564453" Y="-23.10835546875" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221421386719" Y="-23.33976171875" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.189158203125" Y="-23.377701171875" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.136606201172" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.116111083984" Y="-23.5026484375" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739257812" Y="-23.628232421875" />
                  <Point X="3.10226953125" Y="-23.650189453125" />
                  <Point X="3.115408203125" Y="-23.713865234375" />
                  <Point X="3.120680419922" Y="-23.7310234375" />
                  <Point X="3.136283447266" Y="-23.76426171875" />
                  <Point X="3.148605712891" Y="-23.782990234375" />
                  <Point X="3.184341064453" Y="-23.837306640625" />
                  <Point X="3.198894042969" Y="-23.854548828125" />
                  <Point X="3.216138183594" Y="-23.870638671875" />
                  <Point X="3.234343505859" Y="-23.883962890625" />
                  <Point X="3.252199951172" Y="-23.894015625" />
                  <Point X="3.303985595703" Y="-23.923166015625" />
                  <Point X="3.320520019531" Y="-23.930498046875" />
                  <Point X="3.356123535156" Y="-23.9405625" />
                  <Point X="3.380266845703" Y="-23.943751953125" />
                  <Point X="3.450284179688" Y="-23.953005859375" />
                  <Point X="3.462697753906" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.542370605469" Y="-23.814232421875" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.861892578125" Y="-24.169677734375" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.049922851562" Y="-24.58108984375" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704787841797" Y="-24.674416015625" />
                  <Point X="3.681550048828" Y="-24.6849296875" />
                  <Point X="3.657830078125" Y="-24.698638671875" />
                  <Point X="3.589040039062" Y="-24.738400390625" />
                  <Point X="3.5743125" Y="-24.74890234375" />
                  <Point X="3.547528076172" Y="-24.77442578125" />
                  <Point X="3.533296142578" Y="-24.7925625" />
                  <Point X="3.492022216797" Y="-24.845154296875" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.470526855469" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.458936767578" Y="-24.93216796875" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.449923095703" Y="-25.083326171875" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.47052734375" Y="-25.1766640625" />
                  <Point X="3.48030078125" Y="-25.198126953125" />
                  <Point X="3.492024902344" Y="-25.217408203125" />
                  <Point X="3.506256835938" Y="-25.23554296875" />
                  <Point X="3.547530761719" Y="-25.28813671875" />
                  <Point X="3.559994873047" Y="-25.301232421875" />
                  <Point X="3.58903515625" Y="-25.32415625" />
                  <Point X="3.612755371094" Y="-25.3378671875" />
                  <Point X="3.681545166016" Y="-25.37762890625" />
                  <Point X="3.692708496094" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.683299316406" Y="-25.65118359375" />
                  <Point X="4.89147265625" Y="-25.706962890625" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.834579589844" Y="-26.038310546875" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.810966552734" Y="-26.0543359375" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374658203125" Y="-26.005509765625" />
                  <Point X="3.328104003906" Y="-26.01562890625" />
                  <Point X="3.193093994141" Y="-26.04497265625" />
                  <Point X="3.163973876953" Y="-26.05659765625" />
                  <Point X="3.136147216797" Y="-26.073490234375" />
                  <Point X="3.112396728516" Y="-26.0939609375" />
                  <Point X="3.084257568359" Y="-26.1278046875" />
                  <Point X="3.002652587891" Y="-26.22594921875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.965724609375" Y="-26.349193359375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976451416016" Y="-26.568" />
                  <Point X="3.002215087891" Y="-26.608072265625" />
                  <Point X="3.076931640625" Y="-26.7242890625" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="4.007749511719" Y="-27.449296875" />
                  <Point X="4.213121582031" Y="-27.606884765625" />
                  <Point X="4.12480859375" Y="-27.7497890625" />
                  <Point X="4.082531494141" Y="-27.809857421875" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="3.145260009766" Y="-27.37573046875" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754223632813" Y="-27.159826171875" />
                  <Point X="2.698816894531" Y="-27.1498203125" />
                  <Point X="2.538133300781" Y="-27.12080078125" />
                  <Point X="2.506783935547" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444833984375" Y="-27.135177734375" />
                  <Point X="2.398804443359" Y="-27.15940234375" />
                  <Point X="2.265315917969" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.180306884766" Y="-27.33646875" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.563259765625" />
                  <Point X="2.105681884766" Y="-27.618666015625" />
                  <Point X="2.134701171875" Y="-27.7793515625" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.728309814453" Y="-28.82458984375" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.781838134766" Y="-29.11165234375" />
                  <Point X="2.734581298828" Y="-29.142240234375" />
                  <Point X="2.701764648438" Y="-29.163482421875" />
                  <Point X="2.021282958984" Y="-28.276662109375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506225586" Y="-27.922181640625" />
                  <Point X="1.721924316406" Y="-27.900556640625" />
                  <Point X="1.667278320312" Y="-27.865423828125" />
                  <Point X="1.50880090332" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365722656" Y="-27.743435546875" />
                  <Point X="1.417102172852" Y="-27.741119140625" />
                  <Point X="1.357337280273" Y="-27.7466171875" />
                  <Point X="1.184015258789" Y="-27.76256640625" />
                  <Point X="1.1563671875" Y="-27.769396484375" />
                  <Point X="1.128982055664" Y="-27.78073828125" />
                  <Point X="1.104594116211" Y="-27.7954609375" />
                  <Point X="1.0584453125" Y="-27.833833984375" />
                  <Point X="0.924610290527" Y="-27.94511328125" />
                  <Point X="0.904140014648" Y="-27.968865234375" />
                  <Point X="0.887247680664" Y="-27.996693359375" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.86182611084" Y="-28.08929296875" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.98311517334" Y="-29.564060546875" />
                  <Point X="1.022065307617" Y="-29.859916015625" />
                  <Point X="0.975721313477" Y="-29.87007421875" />
                  <Point X="0.932008239746" Y="-29.878015625" />
                  <Point X="0.929315612793" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058464355469" Y="-29.75262890625" />
                  <Point X="-1.127265869141" Y="-29.734927734375" />
                  <Point X="-1.141246337891" Y="-29.731330078125" />
                  <Point X="-1.133139648438" Y="-29.66975390625" />
                  <Point X="-1.120775756836" Y="-29.575841796875" />
                  <Point X="-1.120077392578" Y="-29.5681015625" />
                  <Point X="-1.119451660156" Y="-29.541037109375" />
                  <Point X="-1.121759033203" Y="-29.50933203125" />
                  <Point X="-1.123333862305" Y="-29.497693359375" />
                  <Point X="-1.138853515625" Y="-29.419669921875" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026611328" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230573486328" Y="-29.094861328125" />
                  <Point X="-1.250208496094" Y="-29.070935546875" />
                  <Point X="-1.261007324219" Y="-29.05977734375" />
                  <Point X="-1.320816772461" Y="-29.007326171875" />
                  <Point X="-1.494267944336" Y="-28.855212890625" />
                  <Point X="-1.506740112305" Y="-28.84596484375" />
                  <Point X="-1.53302355957" Y="-28.82962109375" />
                  <Point X="-1.546834838867" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591315673828" Y="-28.805474609375" />
                  <Point X="-1.621456298828" Y="-28.798447265625" />
                  <Point X="-1.636813598633" Y="-28.796169921875" />
                  <Point X="-1.716193969727" Y="-28.790966796875" />
                  <Point X="-1.946402709961" Y="-28.77587890625" />
                  <Point X="-1.961927368164" Y="-28.7761328125" />
                  <Point X="-1.992725952148" Y="-28.779166015625" />
                  <Point X="-2.007999633789" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.05366796875" Y="-28.795494140625" />
                  <Point X="-2.081861816406" Y="-28.808267578125" />
                  <Point X="-2.095436035156" Y="-28.815810546875" />
                  <Point X="-2.161579833984" Y="-28.860005859375" />
                  <Point X="-2.35340234375" Y="-28.988177734375" />
                  <Point X="-2.359681396484" Y="-28.992755859375" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.503201416016" Y="-29.162478515625" />
                  <Point X="-2.747605712891" Y="-29.011150390625" />
                  <Point X="-2.842885253906" Y="-28.937787109375" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.532365966797" Y="-28.054728515625" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947021484" Y="-27.68111328125" />
                  <Point X="-2.319683105469" Y="-27.666181640625" />
                  <Point X="-2.313412597656" Y="-27.63465234375" />
                  <Point X="-2.311638671875" Y="-27.619224609375" />
                  <Point X="-2.310627441406" Y="-27.588287109375" />
                  <Point X="-2.314669433594" Y="-27.557599609375" />
                  <Point X="-2.32365625" Y="-27.527982421875" />
                  <Point X="-2.329363769531" Y="-27.51354296875" />
                  <Point X="-2.343583984375" Y="-27.484712890625" />
                  <Point X="-2.351565429688" Y="-27.4713984375" />
                  <Point X="-2.369592285156" Y="-27.446244140625" />
                  <Point X="-2.379637695313" Y="-27.434404296875" />
                  <Point X="-2.396982666016" Y="-27.417060546875" />
                  <Point X="-2.408821777344" Y="-27.407017578125" />
                  <Point X="-2.433977294922" Y="-27.3889921875" />
                  <Point X="-2.447293701172" Y="-27.381009765625" />
                  <Point X="-2.476122558594" Y="-27.36679296875" />
                  <Point X="-2.490563476562" Y="-27.3610859375" />
                  <Point X="-2.520181640625" Y="-27.3521015625" />
                  <Point X="-2.550869384766" Y="-27.3480625" />
                  <Point X="-2.581803466797" Y="-27.349076171875" />
                  <Point X="-2.597227050781" Y="-27.3508515625" />
                  <Point X="-2.628753417969" Y="-27.357123046875" />
                  <Point X="-2.643684082031" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.645473876953" Y="-27.932486328125" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-4.004020019531" Y="-27.740587890625" />
                  <Point X="-4.072337646484" Y="-27.62603125" />
                  <Point X="-4.181265625" Y="-27.443375" />
                  <Point X="-3.382095458984" Y="-26.830150390625" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039159423828" Y="-26.566068359375" />
                  <Point X="-3.016269775391" Y="-26.543435546875" />
                  <Point X="-3.002480224609" Y="-26.526193359375" />
                  <Point X="-2.983230224609" Y="-26.495359375" />
                  <Point X="-2.975094238281" Y="-26.479013671875" />
                  <Point X="-2.966405029297" Y="-26.45631640625" />
                  <Point X="-2.959915771484" Y="-26.43602734375" />
                  <Point X="-2.954186767578" Y="-26.413908203125" />
                  <Point X="-2.951953369141" Y="-26.402400390625" />
                  <Point X="-2.947838623047" Y="-26.37091796875" />
                  <Point X="-2.947081787109" Y="-26.355703125" />
                  <Point X="-2.947780761719" Y="-26.332833984375" />
                  <Point X="-2.951228759766" Y="-26.31021484375" />
                  <Point X="-2.957375" Y="-26.288177734375" />
                  <Point X="-2.961113525391" Y="-26.277345703125" />
                  <Point X="-2.973118896484" Y="-26.248361328125" />
                  <Point X="-2.978764404297" Y="-26.236958984375" />
                  <Point X="-2.991526123047" Y="-26.215013671875" />
                  <Point X="-3.007103759766" Y="-26.194970703125" />
                  <Point X="-3.025221679688" Y="-26.1771875" />
                  <Point X="-3.034878173828" Y="-26.168904296875" />
                  <Point X="-3.054170898438" Y="-26.154478515625" />
                  <Point X="-3.071578125" Y="-26.142896484375" />
                  <Point X="-3.091270507812" Y="-26.131306640625" />
                  <Point X="-3.105436035156" Y="-26.12448046875" />
                  <Point X="-3.134701416016" Y="-26.113255859375" />
                  <Point X="-3.149801269531" Y="-26.108857421875" />
                  <Point X="-3.181688232422" Y="-26.102376953125" />
                  <Point X="-3.197304931641" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.4547109375" Y="-26.259546875" />
                  <Point X="-4.660921386719" Y="-26.286693359375" />
                  <Point X="-4.740762207031" Y="-25.97412109375" />
                  <Point X="-4.758837402344" Y="-25.84773828125" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.888733886719" Y="-25.41411328125" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.497901855469" Y="-25.30873828125" />
                  <Point X="-3.477604003906" Y="-25.300701171875" />
                  <Point X="-3.467692382812" Y="-25.296099609375" />
                  <Point X="-3.445009521484" Y="-25.283939453125" />
                  <Point X="-3.426451416016" Y="-25.272580078125" />
                  <Point X="-3.405814453125" Y="-25.2582578125" />
                  <Point X="-3.396481445313" Y="-25.250873046875" />
                  <Point X="-3.372525390625" Y="-25.229345703125" />
                  <Point X="-3.357912353516" Y="-25.21275390625" />
                  <Point X="-3.337188476562" Y="-25.18281640625" />
                  <Point X="-3.328273681641" Y="-25.166845703125" />
                  <Point X="-3.318475585938" Y="-25.14446484375" />
                  <Point X="-3.311065429688" Y="-25.1245859375" />
                  <Point X="-3.304186523438" Y="-25.102421875" />
                  <Point X="-3.301576660156" Y="-25.091939453125" />
                  <Point X="-3.296133789062" Y="-25.063201171875" />
                  <Point X="-3.294506835938" Y="-25.0430390625" />
                  <Point X="-3.295379882812" Y="-25.0096640625" />
                  <Point X="-3.297293945312" Y="-24.993015625" />
                  <Point X="-3.301912597656" Y="-24.970552734375" />
                  <Point X="-3.306557617188" Y="-24.952501953125" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.317669189453" Y="-24.919212890625" />
                  <Point X="-3.330989501953" Y="-24.889880859375" />
                  <Point X="-3.342501708984" Y="-24.870833984375" />
                  <Point X="-3.365027099609" Y="-24.841875" />
                  <Point X="-3.377700683594" Y="-24.828494140625" />
                  <Point X="-3.396639160156" Y="-24.812037109375" />
                  <Point X="-3.412929199219" Y="-24.799369140625" />
                  <Point X="-3.433558837891" Y="-24.78505078125" />
                  <Point X="-3.440476806641" Y="-24.780677734375" />
                  <Point X="-3.465598388672" Y="-24.76717578125" />
                  <Point X="-3.49655859375" Y="-24.7543671875" />
                  <Point X="-3.508288330078" Y="-24.75038671875" />
                  <Point X="-4.611608398438" Y="-24.45475390625" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.694944335938" Y="-23.908193359375" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.050779296875" Y="-23.7584921875" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057128906" Y="-23.795634765625" />
                  <Point X="-3.736704101562" Y="-23.795296875" />
                  <Point X="-3.715142089844" Y="-23.79341015625" />
                  <Point X="-3.704893066406" Y="-23.791947265625" />
                  <Point X="-3.684612792969" Y="-23.7879140625" />
                  <Point X="-3.658831542969" Y="-23.78037890625" />
                  <Point X="-3.613155517578" Y="-23.7659765625" />
                  <Point X="-3.603455566406" Y="-23.762326171875" />
                  <Point X="-3.584515380859" Y="-23.7539921875" />
                  <Point X="-3.575275146484" Y="-23.74930859375" />
                  <Point X="-3.55653125" Y="-23.738486328125" />
                  <Point X="-3.547857177734" Y="-23.732826171875" />
                  <Point X="-3.531177490234" Y="-23.720595703125" />
                  <Point X="-3.515927734375" Y="-23.70662109375" />
                  <Point X="-3.502290283203" Y="-23.6910703125" />
                  <Point X="-3.495896972656" Y="-23.682923828125" />
                  <Point X="-3.483482910156" Y="-23.6651953125" />
                  <Point X="-3.478012451172" Y="-23.6563984375" />
                  <Point X="-3.468062255859" Y="-23.63826171875" />
                  <Point X="-3.457262695312" Y="-23.6136640625" />
                  <Point X="-3.438935058594" Y="-23.56941796875" />
                  <Point X="-3.435499267578" Y="-23.5596484375" />
                  <Point X="-3.429711181641" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436012695312" Y="-23.39546875" />
                  <Point X="-3.443509277344" Y="-23.37619140625" />
                  <Point X="-3.455409423828" Y="-23.352109375" />
                  <Point X="-3.477523681641" Y="-23.30962890625" />
                  <Point X="-3.482799804688" Y="-23.30071484375" />
                  <Point X="-3.494291748047" Y="-23.283515625" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521497558594" Y="-23.25109375" />
                  <Point X="-3.536439208984" Y="-23.2367890625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-4.177170410156" Y="-22.744423828125" />
                  <Point X="-4.227614257812" Y="-22.705716796875" />
                  <Point X="-4.002294433594" Y="-22.31969140625" />
                  <Point X="-3.905909179688" Y="-22.19580078125" />
                  <Point X="-3.726337158203" Y="-21.964986328125" />
                  <Point X="-3.421649414062" Y="-22.1408984375" />
                  <Point X="-3.254156738281" Y="-22.237599609375" />
                  <Point X="-3.244922851562" Y="-22.24228125" />
                  <Point X="-3.225994384766" Y="-22.250611328125" />
                  <Point X="-3.216299804688" Y="-22.254259765625" />
                  <Point X="-3.195656982422" Y="-22.26076953125" />
                  <Point X="-3.185625976562" Y="-22.263341796875" />
                  <Point X="-3.165337890625" Y="-22.26737890625" />
                  <Point X="-3.155080810547" Y="-22.26884375" />
                  <Point X="-3.133145507812" Y="-22.270763671875" />
                  <Point X="-3.069531494141" Y="-22.276328125" />
                  <Point X="-3.059176757812" Y="-22.276666015625" />
                  <Point X="-3.03848828125" Y="-22.27621484375" />
                  <Point X="-3.028154541016" Y="-22.27542578125" />
                  <Point X="-3.006696533203" Y="-22.272599609375" />
                  <Point X="-2.996512207031" Y="-22.2706875" />
                  <Point X="-2.976422851562" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.93844921875" Y="-22.249552734375" />
                  <Point X="-2.929419189453" Y="-22.244470703125" />
                  <Point X="-2.911165527344" Y="-22.232841796875" />
                  <Point X="-2.902742431641" Y="-22.2268046875" />
                  <Point X="-2.886609863281" Y="-22.21385546875" />
                  <Point X="-2.878900390625" Y="-22.206943359375" />
                  <Point X="-2.863330566406" Y="-22.191373046875" />
                  <Point X="-2.818176757812" Y="-22.146220703125" />
                  <Point X="-2.811265869141" Y="-22.13851171875" />
                  <Point X="-2.798321777344" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.750716552734" Y="-21.933666015625" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.059387695312" Y="-21.300083984375" />
                  <Point X="-2.648383789062" Y="-20.984970703125" />
                  <Point X="-2.496579101562" Y="-20.900630859375" />
                  <Point X="-2.192523681641" Y="-20.731703125" />
                  <Point X="-2.169838623047" Y="-20.761267578125" />
                  <Point X="-2.118563720703" Y="-20.82808984375" />
                  <Point X="-2.111821777344" Y="-20.83594921875" />
                  <Point X="-2.097517333984" Y="-20.850892578125" />
                  <Point X="-2.089955078125" Y="-20.8579765625" />
                  <Point X="-2.073375" Y="-20.871888671875" />
                  <Point X="-2.065090820313" Y="-20.8781015625" />
                  <Point X="-2.047897949219" Y="-20.88958984375" />
                  <Point X="-2.038989135742" Y="-20.894865234375" />
                  <Point X="-2.014575195313" Y="-20.907576171875" />
                  <Point X="-1.94377331543" Y="-20.94443359375" />
                  <Point X="-1.934340332031" Y="-20.948708984375" />
                  <Point X="-1.915063476562" Y="-20.95620703125" />
                  <Point X="-1.905219360352" Y="-20.9594296875" />
                  <Point X="-1.88431262207" Y="-20.965033203125" />
                  <Point X="-1.874173950195" Y="-20.967166015625" />
                  <Point X="-1.853724121094" Y="-20.970314453125" />
                  <Point X="-1.833053955078" Y="-20.971216796875" />
                  <Point X="-1.812407348633" Y="-20.96986328125" />
                  <Point X="-1.802119750977" Y="-20.968623046875" />
                  <Point X="-1.780804443359" Y="-20.96486328125" />
                  <Point X="-1.770713256836" Y="-20.9625078125" />
                  <Point X="-1.750859619141" Y="-20.95671875" />
                  <Point X="-1.741097167969" Y="-20.95328515625" />
                  <Point X="-1.715668457031" Y="-20.942751953125" />
                  <Point X="-1.641923217773" Y="-20.912205078125" />
                  <Point X="-1.632588867188" Y="-20.907728515625" />
                  <Point X="-1.614455322266" Y="-20.89778125" />
                  <Point X="-1.60565625" Y="-20.892310546875" />
                  <Point X="-1.587926879883" Y="-20.879896484375" />
                  <Point X="-1.579778686523" Y="-20.873501953125" />
                  <Point X="-1.5642265625" Y="-20.85986328125" />
                  <Point X="-1.550251464844" Y="-20.844611328125" />
                  <Point X="-1.538020141602" Y="-20.8279296875" />
                  <Point X="-1.532360229492" Y="-20.819255859375" />
                  <Point X="-1.521538330078" Y="-20.80051171875" />
                  <Point X="-1.516855712891" Y="-20.791271484375" />
                  <Point X="-1.508525146484" Y="-20.772337890625" />
                  <Point X="-1.504876953125" Y="-20.76264453125" />
                  <Point X="-1.496600341797" Y="-20.73639453125" />
                  <Point X="-1.47259753418" Y="-20.660267578125" />
                  <Point X="-1.470026123047" Y="-20.650234375" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-0.93116394043" Y="-20.2836796875" />
                  <Point X="-0.747163696289" Y="-20.262142578125" />
                  <Point X="-0.365222595215" Y="-20.21744140625" />
                  <Point X="-0.275215332031" Y="-20.553353515625" />
                  <Point X="-0.225666244507" Y="-20.7382734375" />
                  <Point X="-0.220435317993" Y="-20.752892578125" />
                  <Point X="-0.207661773682" Y="-20.781083984375" />
                  <Point X="-0.200119155884" Y="-20.79465625" />
                  <Point X="-0.182260925293" Y="-20.8213828125" />
                  <Point X="-0.172608901978" Y="-20.833544921875" />
                  <Point X="-0.151451248169" Y="-20.856134765625" />
                  <Point X="-0.126896362305" Y="-20.8749765625" />
                  <Point X="-0.09960043335" Y="-20.88956640625" />
                  <Point X="-0.085354034424" Y="-20.8957421875" />
                  <Point X="-0.054915912628" Y="-20.90607421875" />
                  <Point X="-0.03985357666" Y="-20.909845703125" />
                  <Point X="-0.009317661285" Y="-20.91488671875" />
                  <Point X="0.021629491806" Y="-20.91488671875" />
                  <Point X="0.052165405273" Y="-20.909845703125" />
                  <Point X="0.067227592468" Y="-20.90607421875" />
                  <Point X="0.097665863037" Y="-20.8957421875" />
                  <Point X="0.111912406921" Y="-20.88956640625" />
                  <Point X="0.139208343506" Y="-20.8749765625" />
                  <Point X="0.16376322937" Y="-20.856134765625" />
                  <Point X="0.184920883179" Y="-20.833544921875" />
                  <Point X="0.194572906494" Y="-20.8213828125" />
                  <Point X="0.212431137085" Y="-20.79465625" />
                  <Point X="0.219973754883" Y="-20.781083984375" />
                  <Point X="0.232747146606" Y="-20.752892578125" />
                  <Point X="0.237978225708" Y="-20.7382734375" />
                  <Point X="0.378190979004" Y="-20.2149921875" />
                  <Point X="0.827865600586" Y="-20.2620859375" />
                  <Point X="0.980124206543" Y="-20.298845703125" />
                  <Point X="1.453606567383" Y="-20.41316015625" />
                  <Point X="1.550621459961" Y="-20.44834765625" />
                  <Point X="1.858246948242" Y="-20.55992578125" />
                  <Point X="1.954086425781" Y="-20.60474609375" />
                  <Point X="2.250439697266" Y="-20.743341796875" />
                  <Point X="2.343086181641" Y="-20.79731640625" />
                  <Point X="2.629419677734" Y="-20.964134765625" />
                  <Point X="2.716747558594" Y="-21.02623828125" />
                  <Point X="2.817778564453" Y="-21.0980859375" />
                  <Point X="2.286234619141" Y="-22.018748046875" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181762695" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.035796630859" Y="-22.479990234375" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017285888672" Y="-22.55289453125" />
                  <Point X="2.014084228516" Y="-22.579529296875" />
                  <Point X="2.013428344727" Y="-22.5929609375" />
                  <Point X="2.014237182617" Y="-22.6296640625" />
                  <Point X="2.015557495117" Y="-22.64822265625" />
                  <Point X="2.021782348633" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144042969" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045319091797" Y="-22.77611328125" />
                  <Point X="2.055682128906" Y="-22.796158203125" />
                  <Point X="2.072475585938" Y="-22.822103515625" />
                  <Point X="2.104418701172" Y="-22.8691796875" />
                  <Point X="2.112816894531" Y="-22.87983203125" />
                  <Point X="2.131041992188" Y="-22.899828125" />
                  <Point X="2.140868896484" Y="-22.909171875" />
                  <Point X="2.170401367188" Y="-22.93348828125" />
                  <Point X="2.184489257812" Y="-22.94403515625" />
                  <Point X="2.231565185547" Y="-22.975978515625" />
                  <Point X="2.241281982422" Y="-22.9817578125" />
                  <Point X="2.261329589844" Y="-22.99212109375" />
                  <Point X="2.271660400391" Y="-22.996705078125" />
                  <Point X="2.293749511719" Y="-23.004970703125" />
                  <Point X="2.304551757812" Y="-23.008294921875" />
                  <Point X="2.326472900391" Y="-23.01363671875" />
                  <Point X="2.355392822266" Y="-23.01780078125" />
                  <Point X="2.407016601562" Y="-23.024025390625" />
                  <Point X="2.416040039062" Y="-23.0246796875" />
                  <Point X="2.447583740234" Y="-23.02450390625" />
                  <Point X="2.484324462891" Y="-23.02017578125" />
                  <Point X="2.497755859375" Y="-23.0176015625" />
                  <Point X="2.518341796875" Y="-23.012095703125" />
                  <Point X="2.578042236328" Y="-22.996130859375" />
                  <Point X="2.584003417969" Y="-22.994326171875" />
                  <Point X="2.604410888672" Y="-22.9869296875" />
                  <Point X="2.627657470703" Y="-22.976423828125" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.74576171875" Y="-22.331423828125" />
                  <Point X="3.940403564453" Y="-22.219046875" />
                  <Point X="4.043955322266" Y="-22.3629609375" />
                  <Point X="4.092630126953" Y="-22.443396484375" />
                  <Point X="4.136884765625" Y="-22.516529296875" />
                  <Point X="3.463821289062" Y="-23.032986328125" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168135742188" Y="-23.2601328125" />
                  <Point X="3.152114257812" Y="-23.2747890625" />
                  <Point X="3.134666748047" Y="-23.293400390625" />
                  <Point X="3.128576416016" Y="-23.300578125" />
                  <Point X="3.113760742188" Y="-23.31990625" />
                  <Point X="3.070794189453" Y="-23.375958984375" />
                  <Point X="3.065635742188" Y="-23.3833984375" />
                  <Point X="3.049739257812" Y="-23.41062890625" />
                  <Point X="3.034762939453" Y="-23.444455078125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.024621337891" Y="-23.4770625" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077636719" Y="-23.636244140625" />
                  <Point X="3.004698974609" Y="-23.6474296875" />
                  <Point X="3.009229248047" Y="-23.66938671875" />
                  <Point X="3.022367919922" Y="-23.7330625" />
                  <Point X="3.024598388672" Y="-23.741767578125" />
                  <Point X="3.034684326172" Y="-23.771392578125" />
                  <Point X="3.050287353516" Y="-23.804630859375" />
                  <Point X="3.056920410156" Y="-23.816478515625" />
                  <Point X="3.069242675781" Y="-23.83520703125" />
                  <Point X="3.104978027344" Y="-23.8895234375" />
                  <Point X="3.111743408203" Y="-23.89858203125" />
                  <Point X="3.126296386719" Y="-23.91582421875" />
                  <Point X="3.134083984375" Y="-23.9240078125" />
                  <Point X="3.151328125" Y="-23.94009765625" />
                  <Point X="3.160030761719" Y="-23.94730078125" />
                  <Point X="3.178236083984" Y="-23.960625" />
                  <Point X="3.187738769531" Y="-23.96674609375" />
                  <Point X="3.205595214844" Y="-23.976798828125" />
                  <Point X="3.257380859375" Y="-24.00594921875" />
                  <Point X="3.265475341797" Y="-24.010009765625" />
                  <Point X="3.294677978516" Y="-24.021916015625" />
                  <Point X="3.330281494141" Y="-24.03198046875" />
                  <Point X="3.343681640625" Y="-24.034744140625" />
                  <Point X="3.367824951172" Y="-24.03793359375" />
                  <Point X="3.437842285156" Y="-24.0471875" />
                  <Point X="3.444034912109" Y="-24.04780078125" />
                  <Point X="3.465708007812" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.554770507812" Y="-23.908419921875" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.085763671875" />
                  <Point X="4.7680234375" Y="-24.18429296875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.025334960938" Y="-24.489326171875" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.686021728516" Y="-24.580458984375" />
                  <Point X="3.665627685547" Y="-24.58786328125" />
                  <Point X="3.642389892578" Y="-24.598376953125" />
                  <Point X="3.634012939453" Y="-24.602677734375" />
                  <Point X="3.61029296875" Y="-24.61638671875" />
                  <Point X="3.541502929688" Y="-24.6561484375" />
                  <Point X="3.533884033203" Y="-24.66105078125" />
                  <Point X="3.508775878906" Y="-24.680126953125" />
                  <Point X="3.481991455078" Y="-24.705650390625" />
                  <Point X="3.472791015625" Y="-24.715779296875" />
                  <Point X="3.458559082031" Y="-24.733916015625" />
                  <Point X="3.41728515625" Y="-24.7865078125" />
                  <Point X="3.410849365234" Y="-24.795798828125" />
                  <Point X="3.399128173828" Y="-24.815076171875" />
                  <Point X="3.393842773438" Y="-24.8250625" />
                  <Point X="3.384068603516" Y="-24.84652734375" />
                  <Point X="3.380005126953" Y="-24.857072265625" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.370376708984" Y="-24.88952734375" />
                  <Point X="3.365632568359" Y="-24.914298828125" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.356618896484" Y="-25.1011953125" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159667969" Y="-25.18398828125" />
                  <Point X="3.380005859375" Y="-25.20548828125" />
                  <Point X="3.384069335938" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.23749609375" />
                  <Point X="3.39912890625" Y="-25.247484375" />
                  <Point X="3.410853027344" Y="-25.266765625" />
                  <Point X="3.417291015625" Y="-25.27605859375" />
                  <Point X="3.431522949219" Y="-25.294193359375" />
                  <Point X="3.472796875" Y="-25.346787109375" />
                  <Point X="3.478716796875" Y="-25.3536328125" />
                  <Point X="3.501133056641" Y="-25.375798828125" />
                  <Point X="3.530173339844" Y="-25.39872265625" />
                  <Point X="3.541493408203" Y="-25.406404296875" />
                  <Point X="3.565213623047" Y="-25.420115234375" />
                  <Point X="3.634003417969" Y="-25.459876953125" />
                  <Point X="3.639487304688" Y="-25.4628125" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.68302734375" Y="-25.481029296875" />
                  <Point X="3.691991943359" Y="-25.483916015625" />
                  <Point X="4.658711425781" Y="-25.742947265625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761613769531" Y="-25.93105078125" />
                  <Point X="4.741960449219" Y="-26.01717578125" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.823366455078" Y="-25.9601484375" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097412109" Y="-25.90804296875" />
                  <Point X="3.366719970703" Y="-25.910841796875" />
                  <Point X="3.354479980469" Y="-25.912677734375" />
                  <Point X="3.30792578125" Y="-25.922796875" />
                  <Point X="3.172915771484" Y="-25.952140625" />
                  <Point X="3.157872070312" Y="-25.956744140625" />
                  <Point X="3.128751953125" Y="-25.968369140625" />
                  <Point X="3.114675537109" Y="-25.975390625" />
                  <Point X="3.086848876953" Y="-25.992283203125" />
                  <Point X="3.074124755859" Y="-26.00153125" />
                  <Point X="3.050374267578" Y="-26.022001953125" />
                  <Point X="3.039347900391" Y="-26.033224609375" />
                  <Point X="3.011208740234" Y="-26.067068359375" />
                  <Point X="2.929603759766" Y="-26.165212890625" />
                  <Point X="2.921324462891" Y="-26.176849609375" />
                  <Point X="2.906605224609" Y="-26.20123046875" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.871124267578" Y="-26.34048828125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889159179688" Y="-26.605484375" />
                  <Point X="2.896542236328" Y="-26.619376953125" />
                  <Point X="2.922305908203" Y="-26.65944921875" />
                  <Point X="2.997022460938" Y="-26.775666015625" />
                  <Point X="3.001744384766" Y="-26.782357421875" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486328125" Y="-26.828119140625" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.949916992188" Y="-27.524666015625" />
                  <Point X="4.087169921875" Y="-27.629984375" />
                  <Point X="4.045489990234" Y="-27.6974296875" />
                  <Point X="4.004843994141" Y="-27.7551796875" />
                  <Point X="4.001274414063" Y="-27.760251953125" />
                  <Point X="3.192760009766" Y="-27.29345703125" />
                  <Point X="2.848454589844" Y="-27.094671875" />
                  <Point X="2.841191650391" Y="-27.09088671875" />
                  <Point X="2.815025634766" Y="-27.079515625" />
                  <Point X="2.783119873047" Y="-27.069328125" />
                  <Point X="2.771106445313" Y="-27.066337890625" />
                  <Point X="2.715699707031" Y="-27.05633203125" />
                  <Point X="2.555016113281" Y="-27.0273125" />
                  <Point X="2.539358398438" Y="-27.02580859375" />
                  <Point X="2.508009033203" Y="-27.025404296875" />
                  <Point X="2.492317382813" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444846679688" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400590087891" Y="-27.051109375" />
                  <Point X="2.354560546875" Y="-27.075333984375" />
                  <Point X="2.221072021484" Y="-27.145587890625" />
                  <Point X="2.208968505859" Y="-27.153171875" />
                  <Point X="2.186038818359" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144941650391" Y="-27.21116015625" />
                  <Point X="2.128047363281" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.096239013672" Y="-27.292224609375" />
                  <Point X="2.025985107422" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337158203" Y="-27.485263671875" />
                  <Point X="2.001379394531" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564484375" />
                  <Point X="2.002187866211" Y="-27.58014453125" />
                  <Point X="2.012194213867" Y="-27.63555078125" />
                  <Point X="2.041213378906" Y="-27.796236328125" />
                  <Point X="2.04301550293" Y="-27.8042265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.646037353516" Y="-28.87208984375" />
                  <Point X="2.735893066406" Y="-29.027724609375" />
                  <Point X="2.723753417969" Y="-29.036083984375" />
                  <Point X="2.096651611328" Y="-28.218830078125" />
                  <Point X="1.833914672852" Y="-27.876423828125" />
                  <Point X="1.828657104492" Y="-27.870150390625" />
                  <Point X="1.808835693359" Y="-27.849630859375" />
                  <Point X="1.78325390625" Y="-27.828005859375" />
                  <Point X="1.773299682617" Y="-27.820646484375" />
                  <Point X="1.718653686523" Y="-27.785513671875" />
                  <Point X="1.560176269531" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517469970703" Y="-27.663873046875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926513672" Y="-27.65115234375" />
                  <Point X="1.455385375977" Y="-27.6486953125" />
                  <Point X="1.424121704102" Y="-27.64637890625" />
                  <Point X="1.408399414062" Y="-27.64651953125" />
                  <Point X="1.348634521484" Y="-27.652017578125" />
                  <Point X="1.1753125" Y="-27.667966796875" />
                  <Point X="1.161231811523" Y="-27.67033984375" />
                  <Point X="1.133583618164" Y="-27.677169921875" />
                  <Point X="1.120016357422" Y="-27.681626953125" />
                  <Point X="1.092631347656" Y="-27.69296875" />
                  <Point X="1.079884765625" Y="-27.699408203125" />
                  <Point X="1.055496826172" Y="-27.714130859375" />
                  <Point X="1.04385534668" Y="-27.7224140625" />
                  <Point X="0.997706604004" Y="-27.760787109375" />
                  <Point X="0.863871520996" Y="-27.87206640625" />
                  <Point X="0.852647949219" Y="-27.88309375" />
                  <Point X="0.83217767334" Y="-27.906845703125" />
                  <Point X="0.822930969238" Y="-27.9195703125" />
                  <Point X="0.806038635254" Y="-27.9473984375" />
                  <Point X="0.799017822266" Y="-27.96147265625" />
                  <Point X="0.787394470215" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.768993591309" Y="-28.069115234375" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584716797" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091247559" Y="-29.152337890625" />
                  <Point X="0.757776306152" Y="-28.871259765625" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605224609" Y="-28.48012109375" />
                  <Point X="0.642142944336" Y="-28.453576171875" />
                  <Point X="0.626785705566" Y="-28.423814453125" />
                  <Point X="0.62040625" Y="-28.413208984375" />
                  <Point X="0.578425598145" Y="-28.352724609375" />
                  <Point X="0.456678619385" Y="-28.17730859375" />
                  <Point X="0.446670806885" Y="-28.165173828125" />
                  <Point X="0.424791564941" Y="-28.14272265625" />
                  <Point X="0.41291986084" Y="-28.13240625" />
                  <Point X="0.386664154053" Y="-28.11316015625" />
                  <Point X="0.373249145508" Y="-28.10494140625" />
                  <Point X="0.345244293213" Y="-28.090830078125" />
                  <Point X="0.330654724121" Y="-28.0849375" />
                  <Point X="0.265692077637" Y="-28.064775390625" />
                  <Point X="0.077295753479" Y="-28.0063046875" />
                  <Point X="0.063377220154" Y="-28.003109375" />
                  <Point X="0.035217639923" Y="-27.99883984375" />
                  <Point X="0.02097659111" Y="-27.997765625" />
                  <Point X="-0.008664463997" Y="-27.997765625" />
                  <Point X="-0.022905660629" Y="-27.99883984375" />
                  <Point X="-0.051065093994" Y="-28.003109375" />
                  <Point X="-0.064983627319" Y="-28.0063046875" />
                  <Point X="-0.129946411133" Y="-28.026466796875" />
                  <Point X="-0.31834274292" Y="-28.0849375" />
                  <Point X="-0.332927093506" Y="-28.090826171875" />
                  <Point X="-0.360928405762" Y="-28.10493359375" />
                  <Point X="-0.374345336914" Y="-28.11315234375" />
                  <Point X="-0.400600921631" Y="-28.132396484375" />
                  <Point X="-0.412478271484" Y="-28.14271875" />
                  <Point X="-0.434361938477" Y="-28.16517578125" />
                  <Point X="-0.444368286133" Y="-28.177310546875" />
                  <Point X="-0.486348754883" Y="-28.237796875" />
                  <Point X="-0.60809588623" Y="-28.4132109375" />
                  <Point X="-0.612468933105" Y="-28.42012890625" />
                  <Point X="-0.625977172852" Y="-28.445263671875" />
                  <Point X="-0.638778198242" Y="-28.47621484375" />
                  <Point X="-0.642753112793" Y="-28.487935546875" />
                  <Point X="-0.94062298584" Y="-29.599603515625" />
                  <Point X="-0.985424865723" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.92935813536" Y="-27.718731166952" />
                  <Point X="4.01096440573" Y="-27.571509558076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.847073143147" Y="-27.671224015184" />
                  <Point X="3.934758877446" Y="-27.513034763052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.764788150933" Y="-27.623716863415" />
                  <Point X="3.858553300273" Y="-27.454560056226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.628186923881" Y="-26.066104245174" />
                  <Point X="4.783494387102" Y="-25.785922164773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.682503158719" Y="-27.576209711647" />
                  <Point X="3.782347723099" Y="-27.3960853494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.526955697735" Y="-26.052777004201" />
                  <Point X="4.693526371785" Y="-25.752275553587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.600218166505" Y="-27.528702559878" />
                  <Point X="3.706142145926" Y="-27.337610642574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.42572447159" Y="-26.039449763227" />
                  <Point X="4.598954039384" Y="-25.7269353503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.684442281733" Y="-28.984852781869" />
                  <Point X="2.697519855757" Y="-28.961260213806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.517933174291" Y="-27.48119540811" />
                  <Point X="3.629936568753" Y="-27.279135935747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.324493245444" Y="-26.026122522254" />
                  <Point X="4.504381867376" Y="-25.701594857659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.621379490744" Y="-28.902667861129" />
                  <Point X="2.642104783682" Y="-28.865278442926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435648182077" Y="-27.433688256341" />
                  <Point X="3.55373099158" Y="-27.220661228921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.223262019299" Y="-26.012795281281" />
                  <Point X="4.409809695367" Y="-25.676254365018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.558316699755" Y="-28.820482940389" />
                  <Point X="2.586689657731" Y="-28.769296769241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353363189863" Y="-27.386181104573" />
                  <Point X="3.477525414407" Y="-27.162186522095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.122030793153" Y="-25.999468040307" />
                  <Point X="4.315237523359" Y="-25.650913872376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.495253908766" Y="-28.738298019649" />
                  <Point X="2.531274531781" Y="-28.673315095555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.271078197649" Y="-27.338673952804" />
                  <Point X="3.401319837234" Y="-27.103711815269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020799567008" Y="-25.986140799334" />
                  <Point X="4.22066535135" Y="-25.625573379735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.432191117777" Y="-28.656113098909" />
                  <Point X="2.475859405831" Y="-28.577333421869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.188793209268" Y="-27.29116679412" />
                  <Point X="3.32511426006" Y="-27.045237108443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.919568340862" Y="-25.97281355836" />
                  <Point X="4.126093179342" Y="-25.600232887094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.369128326788" Y="-28.573928178169" />
                  <Point X="2.42044427988" Y="-28.481351748184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.106508296568" Y="-27.243659498906" />
                  <Point X="3.248908682887" Y="-26.986762401616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.818337115472" Y="-25.959486316024" />
                  <Point X="4.031521007333" Y="-25.574892394452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.739304168951" Y="-24.298017770516" />
                  <Point X="4.775549268246" Y="-24.232629880494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.306065535799" Y="-28.491743257429" />
                  <Point X="2.36502915393" Y="-28.385370074498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.024223383867" Y="-27.196152203691" />
                  <Point X="3.172703105714" Y="-26.92828769479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.717105904533" Y="-25.946159047618" />
                  <Point X="3.936948835324" Y="-25.549551901811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.611738543979" Y="-24.332199042633" />
                  <Point X="4.751357722192" Y="-24.080319377585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243002744811" Y="-28.409558336689" />
                  <Point X="2.309614027979" Y="-28.289388400812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.941938471167" Y="-27.148644908477" />
                  <Point X="3.096497528541" Y="-26.869812987964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.615874693593" Y="-25.932831779212" />
                  <Point X="3.842376663316" Y="-25.524211409169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.484172919007" Y="-24.366380314749" />
                  <Point X="4.718211387111" Y="-23.944163741718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179939953822" Y="-28.327373415949" />
                  <Point X="2.254198902029" Y="-28.193406727127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859653558466" Y="-27.101137613262" />
                  <Point X="3.022571809834" Y="-26.80722530759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.514643482654" Y="-25.919504510806" />
                  <Point X="3.747804491307" Y="-25.498870916528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.356607294034" Y="-24.400561586865" />
                  <Point X="4.635281592307" Y="-23.897819844614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.116877162833" Y="-28.245188495209" />
                  <Point X="2.198783776079" Y="-28.097425053441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.770395854949" Y="-27.066209565669" />
                  <Point X="2.96180821751" Y="-26.72089252266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.412261741016" Y="-25.908252854724" />
                  <Point X="3.65511835225" Y="-25.470127930375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.229041669062" Y="-24.434742858981" />
                  <Point X="4.518112053358" Y="-23.913246081078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053814437836" Y="-28.163003455416" />
                  <Point X="2.143368650128" Y="-28.001443379755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.67166106622" Y="-27.048378632378" />
                  <Point X="2.903479524479" Y="-26.630167063124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.29389027687" Y="-25.92584742164" />
                  <Point X="3.572036237277" Y="-25.424058826131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.10147604409" Y="-24.468924131098" />
                  <Point X="4.400942891819" Y="-23.928671636674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.990751743997" Y="-28.080818359413" />
                  <Point X="2.087953524178" Y="-27.90546170607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.572926587449" Y="-27.030547139907" />
                  <Point X="2.861291156676" Y="-26.510323686094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.170243859902" Y="-25.952958255354" />
                  <Point X="3.494050598632" Y="-25.368795435207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.973910336651" Y="-24.503105551988" />
                  <Point X="4.28377373028" Y="-23.944097192271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.927689050158" Y="-27.99863326341" />
                  <Point X="2.040885591564" Y="-27.794421296981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464142886614" Y="-27.030844923945" />
                  <Point X="2.876926573019" Y="-26.286163441073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.972702699241" Y="-26.113378735555" />
                  <Point X="3.42874598599" Y="-25.290654867787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.846344507108" Y="-24.53728719316" />
                  <Point X="4.166604568741" Y="-23.959522747867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.864626356319" Y="-27.916448167406" />
                  <Point X="2.014193585274" Y="-27.646621743749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.321103069354" Y="-27.092942377922" />
                  <Point X="3.37538310736" Y="-25.19097084193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.718778677565" Y="-24.571468834332" />
                  <Point X="4.049435407203" Y="-23.974948303463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.798083394713" Y="-27.840541640657" />
                  <Point X="2.019594498672" Y="-27.440925030792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.150607774783" Y="-27.204570824112" />
                  <Point X="3.349313964783" Y="-25.042047612814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573460579278" Y="-24.637676416081" />
                  <Point X="3.932266245664" Y="-23.99037385906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.719622130698" Y="-27.786136300614" />
                  <Point X="3.815097084125" Y="-24.005799414656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.639541396056" Y="-27.734652562921" />
                  <Point X="3.697927922587" Y="-24.021224970252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804413224984" Y="-29.045310458042" />
                  <Point X="0.816201099247" Y="-29.024044569938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.559426506782" Y="-27.683230441816" />
                  <Point X="3.580758761048" Y="-24.036650525849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769017593102" Y="-28.91321266102" />
                  <Point X="0.795354561781" Y="-28.865699511795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.468777389884" Y="-27.650812570407" />
                  <Point X="3.465425183622" Y="-24.048764600047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733622030739" Y="-28.781114738585" />
                  <Point X="0.774508024315" Y="-28.707354453652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.360074152846" Y="-27.650965193912" />
                  <Point X="3.36315242703" Y="-24.037316329738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.698226500728" Y="-28.649016757782" />
                  <Point X="0.753661486848" Y="-28.549009395509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.245617485257" Y="-27.661497280887" />
                  <Point X="3.268896818742" Y="-24.011404741026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.06838417558" Y="-22.569091369555" />
                  <Point X="4.116339606322" Y="-22.482577482372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.662830970718" Y="-28.516918776979" />
                  <Point X="0.732814949382" Y="-28.390664337366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.127137544025" Y="-27.679287545648" />
                  <Point X="3.185744822634" Y="-23.965461705686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.87937239508" Y="-22.714124440621" />
                  <Point X="4.059649566143" Y="-22.388895814839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.615565545867" Y="-28.406234653318" />
                  <Point X="0.747422551036" Y="-28.168358319127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.952514345608" Y="-27.798362927505" />
                  <Point X="3.113225106704" Y="-23.90033752916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.69036061458" Y="-22.859157511687" />
                  <Point X="3.999586865991" Y="-22.301298586959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.555176323359" Y="-28.319226487363" />
                  <Point X="3.053990815951" Y="-23.811245811161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.50134883408" Y="-23.004190582753" />
                  <Point X="3.934752614963" Y="-22.222309464725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.494787747514" Y="-28.232217154796" />
                  <Point X="3.013237659662" Y="-23.68881324402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.312335928587" Y="-23.149225683361" />
                  <Point X="3.775011877497" Y="-22.314536176312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.43179383721" Y="-28.149907970011" />
                  <Point X="3.027296113235" Y="-23.467497915146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.096953989019" Y="-23.3418317807" />
                  <Point X="3.615271419974" Y="-22.406762382867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.353591228108" Y="-28.095036004152" />
                  <Point X="3.455531025203" Y="-22.498988476216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.262325495073" Y="-28.063730537704" />
                  <Point X="3.295790630431" Y="-22.591214569566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.16965027812" Y="-28.034967847552" />
                  <Point X="3.13605023566" Y="-22.683440662915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.076962321343" Y="-28.006228140651" />
                  <Point X="2.976309840888" Y="-22.775666756264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.005409803192" Y="-29.762925935022" />
                  <Point X="-0.964712590793" Y="-29.689506220348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.027988062125" Y="-27.999610437097" />
                  <Point X="2.816569446117" Y="-22.867892849613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.10209802025" Y="-29.741402888702" />
                  <Point X="-0.863077124349" Y="-29.310197777989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.155970483886" Y="-28.034543630525" />
                  <Point X="2.656829051345" Y="-22.960118942963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121346134186" Y="-29.580174198177" />
                  <Point X="-0.761442241164" Y="-28.930890387858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.287158022127" Y="-28.075259007143" />
                  <Point X="2.519583247821" Y="-23.011763719465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.139419254166" Y="-29.416825762443" />
                  <Point X="-0.659807357979" Y="-28.551582997726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.483985166732" Y="-28.23439136828" />
                  <Point X="2.404346328038" Y="-23.023703418661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.168103504404" Y="-29.272620312432" />
                  <Point X="2.304309999844" Y="-23.00822052472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.203377783747" Y="-29.140303589635" />
                  <Point X="2.21848369366" Y="-22.96710207247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.265282011512" Y="-29.056028565513" />
                  <Point X="2.141629256817" Y="-22.909797939474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.338371027677" Y="-28.991931433794" />
                  <Point X="2.077508764194" Y="-22.829521162994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.411459855762" Y="-28.927833962773" />
                  <Point X="2.027100680468" Y="-22.724506546021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.484548683848" Y="-28.863736491752" />
                  <Point X="2.019247559728" Y="-22.5427207436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.56593766274" Y="-28.814612889162" />
                  <Point X="2.74147964209" Y="-21.043826369365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.663368376635" Y="-28.794429342593" />
                  <Point X="2.663571985958" Y="-20.988422294264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.76817910197" Y="-28.787559689097" />
                  <Point X="2.583307632678" Y="-20.937269813362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.872990016705" Y="-28.780690377288" />
                  <Point X="2.501203738785" Y="-20.889435951574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.980072942591" Y="-28.777919882097" />
                  <Point X="2.419099844892" Y="-20.841602089786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.118082263867" Y="-28.830942081086" />
                  <Point X="2.336995912914" Y="-20.793768296705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.29059593722" Y="-28.946211778989" />
                  <Point X="2.254891505585" Y="-20.745935361182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.515032582133" Y="-29.155152997179" />
                  <Point X="2.168858998565" Y="-20.705188905087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.595897416553" Y="-29.105083812931" />
                  <Point X="2.082601294357" Y="-20.664848715473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.676762250973" Y="-29.055014628683" />
                  <Point X="1.99634359015" Y="-20.624508525858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.757039949419" Y="-29.003886223098" />
                  <Point X="1.910085685765" Y="-20.584168697374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.83316704424" Y="-28.94526993036" />
                  <Point X="1.822160395945" Y="-20.546836911841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.909294469229" Y="-28.886654233261" />
                  <Point X="1.731724159021" Y="-20.514034994794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311405145866" Y="-27.612080134283" />
                  <Point X="1.641287922097" Y="-20.481233077748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.346617718339" Y="-27.479652089345" />
                  <Point X="1.550851685173" Y="-20.448431160701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413225322973" Y="-27.403862181705" />
                  <Point X="1.460415213974" Y="-20.415629666297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.497043176562" Y="-27.359120385059" />
                  <Point X="1.365020772432" Y="-20.391772587162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.601555632666" Y="-27.351712639626" />
                  <Point X="1.269222672613" Y="-20.368643726835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.743415951158" Y="-27.4116822215" />
                  <Point X="1.173424572794" Y="-20.345514866508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.903156504187" Y="-27.503908600354" />
                  <Point X="1.077626472975" Y="-20.322386006181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.062897057216" Y="-27.596134979207" />
                  <Point X="0.981828373156" Y="-20.299257145854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.222637610246" Y="-27.68836135806" />
                  <Point X="0.886030108289" Y="-20.276128583283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.382378163275" Y="-27.780587736914" />
                  <Point X="0.787536614102" Y="-20.257862343114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.542118716304" Y="-27.872814115767" />
                  <Point X="0.68487753795" Y="-20.247111011739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.701859201723" Y="-27.96504037265" />
                  <Point X="0.582218461799" Y="-20.236359680364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.820043102876" Y="-27.982296566969" />
                  <Point X="-3.029988505944" Y="-26.557000344831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.957270202439" Y="-26.425813052627" />
                  <Point X="0.111397760785" Y="-20.8897895019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.277833505982" Y="-20.589531469379" />
                  <Point X="0.479559385647" Y="-20.225608348989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.882891994118" Y="-27.89972576087" />
                  <Point X="-3.221865202903" Y="-26.707201862006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.970726880207" Y="-26.254136334682" />
                  <Point X="-0.010980341579" Y="-20.914612235498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.94574088536" Y="-27.817154954772" />
                  <Point X="-3.410877262958" Y="-26.852235437401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.032995701548" Y="-26.170519054782" />
                  <Point X="-0.10431809243" Y="-20.887044788138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.008113190062" Y="-27.733724363796" />
                  <Point X="-3.599889733586" Y="-26.997269753492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.11422596842" Y="-26.121109128128" />
                  <Point X="-0.17889245116" Y="-20.825627285342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.06440748001" Y="-27.639328743947" />
                  <Point X="-3.788902204214" Y="-27.142304069582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.211170656853" Y="-26.100048768417" />
                  <Point X="-0.230040657956" Y="-20.721947885734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.120701362454" Y="-27.544932388941" />
                  <Point X="-3.977914674842" Y="-27.287338385673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.325818392925" Y="-26.11092555206" />
                  <Point X="-0.26543617254" Y="-20.589849877101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.17699517808" Y="-27.450535913393" />
                  <Point X="-4.16692714547" Y="-27.432372701763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442987576227" Y="-26.126351146917" />
                  <Point X="-0.300831684601" Y="-20.457751863916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.560156759528" Y="-26.141776741774" />
                  <Point X="-0.336227195698" Y="-20.325653848994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.67732594283" Y="-26.157202336632" />
                  <Point X="-0.386225232701" Y="-20.219899488152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.794495126131" Y="-26.172627931489" />
                  <Point X="-0.502379315144" Y="-20.233493792584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.911664309433" Y="-26.188053526347" />
                  <Point X="-3.387904313293" Y="-25.24316548101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300278253477" Y="-25.085083884496" />
                  <Point X="-0.618533397587" Y="-20.247088097017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.028833492734" Y="-26.203479121204" />
                  <Point X="-3.539395089191" Y="-25.320508867948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.317377710433" Y="-24.919978914169" />
                  <Point X="-0.73468748003" Y="-20.260682401449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146002676035" Y="-26.218904716061" />
                  <Point X="-3.666960455104" Y="-25.35468967271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.376177147998" Y="-24.830102700255" />
                  <Point X="-0.850842332666" Y="-20.274278095346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.263171859337" Y="-26.234330310919" />
                  <Point X="-3.794525821017" Y="-25.388870477471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453514786382" Y="-24.773670285915" />
                  <Point X="-0.970837941596" Y="-20.294802697014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.380341042638" Y="-26.249755905776" />
                  <Point X="-3.922091275741" Y="-25.423051442451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.543932967967" Y="-24.740835796174" />
                  <Point X="-1.09944252466" Y="-20.330858299143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.497510027405" Y="-26.265181142467" />
                  <Point X="-4.049656981284" Y="-25.45723285992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.638505156107" Y="-24.715495332635" />
                  <Point X="-1.228047107723" Y="-20.366913901272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.614678667189" Y="-26.280605756793" />
                  <Point X="-4.177222686826" Y="-25.49141427739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.733077344247" Y="-24.690154869096" />
                  <Point X="-1.637840158133" Y="-20.910246926724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462503133229" Y="-20.593930560531" />
                  <Point X="-1.356651690787" Y="-20.402969503402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.681662237553" Y="-26.205494109283" />
                  <Point X="-4.304788392369" Y="-25.525595694859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.827649532388" Y="-24.664814405557" />
                  <Point X="-1.776128078375" Y="-20.963771731554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.71592583641" Y="-26.071354070624" />
                  <Point X="-4.432354097912" Y="-25.559777112329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.922221720528" Y="-24.639473942018" />
                  <Point X="-1.885299361668" Y="-20.964768732853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.746891536728" Y="-25.93126446551" />
                  <Point X="-4.559919803455" Y="-25.593958529798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.016793908668" Y="-24.614133478479" />
                  <Point X="-3.510013009394" Y="-23.699876534728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442590942047" Y="-23.578243905475" />
                  <Point X="-1.973940953009" Y="-20.928729189473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.769168967896" Y="-25.775500807936" />
                  <Point X="-4.687485508997" Y="-25.628139947267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.111366096808" Y="-24.588793014939" />
                  <Point X="-3.664109998559" Y="-23.781921654861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441736690053" Y="-23.380749586818" />
                  <Point X="-2.057359746023" Y="-20.883267468492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.205938284949" Y="-24.5634525514" />
                  <Point X="-3.779535273904" Y="-23.794201156484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.495537351558" Y="-23.281855342174" />
                  <Point X="-2.91563960031" Y="-22.235692105748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750376734104" Y="-21.93755000294" />
                  <Point X="-2.128334597336" Y="-20.815356282419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.300510473089" Y="-24.538112087861" />
                  <Point X="-3.880766636689" Y="-23.780874162015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.56614480063" Y="-23.213281344914" />
                  <Point X="-3.046821086732" Y="-22.276396564596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.77794522763" Y="-21.791331674538" />
                  <Point X="-2.191397263637" Y="-20.733171136737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.395082661229" Y="-24.512771624322" />
                  <Point X="-3.981997999474" Y="-23.767547167546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.642350368703" Y="-23.15480662167" />
                  <Point X="-3.151430243417" Y="-22.26916327163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.832292368155" Y="-21.693423304142" />
                  <Point X="-2.346675360202" Y="-20.817347031023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.489654849369" Y="-24.487431160783" />
                  <Point X="-4.083229285164" Y="-23.754220033993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.718555936775" Y="-23.096331898426" />
                  <Point X="-3.245098553095" Y="-22.24219216818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.88770754596" Y="-21.597441724004" />
                  <Point X="-2.503630574499" Y="-20.904548525789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.58422703751" Y="-24.462090697244" />
                  <Point X="-4.184460407442" Y="-23.740892605639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.794761504848" Y="-23.037857175182" />
                  <Point X="-3.327651209047" Y="-22.195167894578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.943122723765" Y="-21.501460143866" />
                  <Point X="-2.66306888454" Y="-20.996229643858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678799108176" Y="-24.436750021777" />
                  <Point X="-4.285691529721" Y="-23.727565177284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.870967072921" Y="-22.979382451938" />
                  <Point X="-3.409936313563" Y="-22.147660945408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.998537901569" Y="-21.405478563728" />
                  <Point X="-2.85196565262" Y="-21.141055227026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.77337113097" Y="-24.411409259945" />
                  <Point X="-4.386922651999" Y="-23.71423774893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.947172640993" Y="-22.920907728694" />
                  <Point X="-3.492221227619" Y="-22.100153652638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.053953079374" Y="-21.30949698359" />
                  <Point X="-3.040862420701" Y="-21.285880810195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750940745479" Y="-24.174990566086" />
                  <Point X="-4.488153774278" Y="-23.700910320575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.023378209066" Y="-22.86243300545" />
                  <Point X="-3.574506110063" Y="-22.052646302839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678946203595" Y="-23.849155767143" />
                  <Point X="-4.589384896556" Y="-23.687582892221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.099583777139" Y="-22.803958282206" />
                  <Point X="-3.656790992506" Y="-22.00513895304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.175789345211" Y="-22.745483558962" />
                  <Point X="-3.784823019163" Y="-22.040161636067" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998373046875" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.574250366211" Y="-28.920435546875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.521544921875" />
                  <Point X="0.422338226318" Y="-28.461060546875" />
                  <Point X="0.300591186523" Y="-28.28564453125" />
                  <Point X="0.274335601807" Y="-28.2663984375" />
                  <Point X="0.209372970581" Y="-28.246236328125" />
                  <Point X="0.020976625443" Y="-28.187765625" />
                  <Point X="-0.008664604187" Y="-28.187765625" />
                  <Point X="-0.073627388" Y="-28.207927734375" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.330259765625" Y="-28.34612890625" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.757097167969" Y="-29.648779296875" />
                  <Point X="-0.84774395752" Y="-29.987076171875" />
                  <Point X="-0.878838134766" Y="-29.981041015625" />
                  <Point X="-1.100258300781" Y="-29.9380625" />
                  <Point X="-1.174605102539" Y="-29.918935546875" />
                  <Point X="-1.35158972168" Y="-29.8733984375" />
                  <Point X="-1.321514160156" Y="-29.644953125" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.325202880859" Y="-29.456736328125" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.446092041016" Y="-29.150177734375" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.72862121582" Y="-28.980560546875" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.056022460938" Y="-29.017986328125" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.426965087891" Y="-29.375234375" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.531293945312" Y="-29.36855859375" />
                  <Point X="-2.855837890625" Y="-29.167609375" />
                  <Point X="-2.958801025391" Y="-29.088330078125" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.696910888672" Y="-27.959728515625" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499763183594" Y="-27.59758984375" />
                  <Point X="-2.513983398438" Y="-27.568759765625" />
                  <Point X="-2.531328369141" Y="-27.551416015625" />
                  <Point X="-2.560157226562" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.550473876953" Y="-28.097029296875" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.905299560547" Y="-28.1839921875" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.235522460938" Y="-27.72334765625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.497760009766" Y="-26.679412109375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535644531" Y="-26.411083984375" />
                  <Point X="-3.143846435547" Y="-26.38838671875" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.136651611328" Y="-26.350052734375" />
                  <Point X="-3.148656982422" Y="-26.321068359375" />
                  <Point X="-3.167949707031" Y="-26.306642578125" />
                  <Point X="-3.187642089844" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.429911132812" Y="-26.447921875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.827111816406" Y="-26.4037890625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.946923339844" Y="-25.874638671875" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.937909667969" Y="-25.2305859375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.534781005859" Y="-25.116486328125" />
                  <Point X="-3.514144042969" Y="-25.1021640625" />
                  <Point X="-3.502324951172" Y="-25.090646484375" />
                  <Point X="-3.492526855469" Y="-25.068265625" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.48801953125" Y="-25.008818359375" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.502326171875" Y="-24.971912109375" />
                  <Point X="-3.521264648438" Y="-24.955455078125" />
                  <Point X="-3.541894287109" Y="-24.94113671875" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.660784179688" Y="-24.63828125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.982274902344" Y="-24.440345703125" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.878330078125" Y="-23.858498046875" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.025979003906" Y="-23.5701171875" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731704101562" Y="-23.6041328125" />
                  <Point X="-3.715954101562" Y="-23.59916796875" />
                  <Point X="-3.670278076172" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.573943359375" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.632800292969" Y="-23.54095703125" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.623941650391" Y="-23.43983984375" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.292835449219" Y="-22.895162109375" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.411143554688" Y="-22.64323828125" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.05587109375" Y="-22.0791328125" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.326649414062" Y="-21.97635546875" />
                  <Point X="-3.159156738281" Y="-22.073056640625" />
                  <Point X="-3.138513916016" Y="-22.07956640625" />
                  <Point X="-3.116578613281" Y="-22.081486328125" />
                  <Point X="-3.052964599609" Y="-22.08705078125" />
                  <Point X="-3.031506591797" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.997683105469" Y="-22.057025390625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.939993408203" Y="-21.950224609375" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.232510009766" Y="-21.3802265625" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.190263671875" Y="-21.161009765625" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.588853759766" Y="-20.734541015625" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.01910144043" Y="-20.645603515625" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246582031" Y="-20.726337890625" />
                  <Point X="-1.926832763672" Y="-20.739048828125" />
                  <Point X="-1.856030639648" Y="-20.77590625" />
                  <Point X="-1.835124023438" Y="-20.781509765625" />
                  <Point X="-1.81380859375" Y="-20.77775" />
                  <Point X="-1.788379882812" Y="-20.767216796875" />
                  <Point X="-1.714634643555" Y="-20.736669921875" />
                  <Point X="-1.696905151367" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.677806640625" Y="-20.67926171875" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.68380078125" Y="-20.3393984375" />
                  <Point X="-1.689137695313" Y="-20.298861328125" />
                  <Point X="-1.534319335938" Y="-20.255455078125" />
                  <Point X="-0.968082946777" Y="-20.096703125" />
                  <Point X="-0.769251098633" Y="-20.073431640625" />
                  <Point X="-0.224199996948" Y="-20.009640625" />
                  <Point X="-0.091689376831" Y="-20.504177734375" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282129288" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594051361" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.198147842407" Y="-20.152818359375" />
                  <Point X="0.236648452759" Y="-20.009130859375" />
                  <Point X="0.365808441162" Y="-20.022658203125" />
                  <Point X="0.860210083008" Y="-20.074435546875" />
                  <Point X="1.024715209961" Y="-20.11415234375" />
                  <Point X="1.508456176758" Y="-20.230943359375" />
                  <Point X="1.615405395508" Y="-20.269734375" />
                  <Point X="1.931043823242" Y="-20.38421875" />
                  <Point X="2.034575805664" Y="-20.43263671875" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.438730224609" Y="-20.63314453125" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.826861816406" Y="-20.8713984375" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.450779541016" Y="-22.113748046875" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.219347167969" Y="-22.529072265625" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.204191162109" Y="-22.6254765625" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.229696777344" Y="-22.715419921875" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.291172363281" Y="-22.7868125" />
                  <Point X="2.338248291016" Y="-22.818755859375" />
                  <Point X="2.360337402344" Y="-22.827021484375" />
                  <Point X="2.378138427734" Y="-22.82916796875" />
                  <Point X="2.429762207031" Y="-22.835392578125" />
                  <Point X="2.448664550781" Y="-22.8340546875" />
                  <Point X="2.469250488281" Y="-22.828548828125" />
                  <Point X="2.528950927734" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.65076171875" Y="-22.166880859375" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.028323486328" Y="-22.015927734375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.255184082031" Y="-22.345029296875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.579485839844" Y="-23.183724609375" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.264555664062" Y="-23.43549609375" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.207600830078" Y="-23.528234375" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.195309814453" Y="-23.6309921875" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.22796875" Y="-23.7307734375" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280948242188" Y="-23.8011796875" />
                  <Point X="3.2988046875" Y="-23.811232421875" />
                  <Point X="3.350590332031" Y="-23.8403828125" />
                  <Point X="3.368565429688" Y="-23.846380859375" />
                  <Point X="3.392708740234" Y="-23.8495703125" />
                  <Point X="3.462726074219" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.529970703125" Y="-23.720044921875" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.864341308594" Y="-23.741166015625" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.95576171875" Y="-24.1550625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.074510742188" Y="-24.672853515625" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.7053671875" Y="-24.780890625" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.622265136719" Y="-24.833072265625" />
                  <Point X="3.608033203125" Y="-24.851208984375" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.552240966797" Y="-24.950037109375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.543227294922" Y="-25.06545703125" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.580990722656" Y="-25.176892578125" />
                  <Point X="3.622264648438" Y="-25.229486328125" />
                  <Point X="3.636576904297" Y="-25.241908203125" />
                  <Point X="3.660297119141" Y="-25.255619140625" />
                  <Point X="3.729086914062" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.707887207031" Y="-25.559419921875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.990223144531" Y="-25.68920703125" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.927198730469" Y="-26.0594453125" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.798566650391" Y="-26.1485234375" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836425781" Y="-26.098341796875" />
                  <Point X="3.348282226562" Y="-26.1084609375" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.157306396484" Y="-26.188541015625" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.060324951172" Y="-26.3578984375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.082124267578" Y="-26.5566953125" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.06558203125" Y="-27.373927734375" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.3219375" Y="-27.611513671875" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.160219238281" Y="-27.86453515625" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.097760009766" Y="-27.458001953125" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.681934082031" Y="-27.24330859375" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.443048339844" Y="-27.243470703125" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.264374755859" Y="-27.380712890625" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.199169433594" Y="-27.60178125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.810582275391" Y="-28.77708984375" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.975084960938" Y="-29.090365234375" />
                  <Point X="2.835296142578" Y="-29.190212890625" />
                  <Point X="2.786202880859" Y="-29.221990234375" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="1.945914428711" Y="-28.334494140625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670548950195" Y="-27.980466796875" />
                  <Point X="1.615902954102" Y="-27.945333984375" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.366040039062" Y="-27.841216796875" />
                  <Point X="1.192718017578" Y="-27.857166015625" />
                  <Point X="1.165332885742" Y="-27.8685078125" />
                  <Point X="1.119184082031" Y="-27.906880859375" />
                  <Point X="0.985349060059" Y="-28.01816015625" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.954658569336" Y="-28.109470703125" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.077302490234" Y="-29.55166015625" />
                  <Point X="1.127642578125" Y="-29.93403125" />
                  <Point X="1.126596191406" Y="-29.934259765625" />
                  <Point X="0.994369506836" Y="-29.9632421875" />
                  <Point X="0.948988586426" Y="-29.971486328125" />
                  <Point X="0.860200500488" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#142" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.049548971452" Y="4.540043761932" Z="0.65" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.65" />
                  <Point X="-0.781219238283" Y="5.007509184099" Z="0.65" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.65" />
                  <Point X="-1.553973044818" Y="4.823970338236" Z="0.65" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.65" />
                  <Point X="-1.739529051844" Y="4.685357439594" Z="0.65" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.65" />
                  <Point X="-1.731648567591" Y="4.367053989318" Z="0.65" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.65" />
                  <Point X="-1.813670313711" Y="4.310257594668" Z="0.65" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.65" />
                  <Point X="-1.909901257286" Y="4.33658213761" Z="0.65" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.65" />
                  <Point X="-1.985589778871" Y="4.41611368167" Z="0.65" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.65" />
                  <Point X="-2.619292722615" Y="4.34044628258" Z="0.65" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.65" />
                  <Point X="-3.226841738242" Y="3.909950934568" Z="0.65" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.65" />
                  <Point X="-3.281967312673" Y="3.626053758592" Z="0.65" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.65" />
                  <Point X="-2.995959124172" Y="3.076698881895" Z="0.65" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.65" />
                  <Point X="-3.039193704129" Y="3.009609863758" Z="0.65" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.65" />
                  <Point X="-3.118377583712" Y="2.999605575001" Z="0.65" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.65" />
                  <Point X="-3.307805589705" Y="3.098226636073" Z="0.65" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.65" />
                  <Point X="-4.101490492392" Y="2.982850552871" Z="0.65" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.65" />
                  <Point X="-4.460481739335" Y="2.413247024759" Z="0.65" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.65" />
                  <Point X="-4.329429504242" Y="2.096450160351" Z="0.65" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.65" />
                  <Point X="-3.674447896769" Y="1.568352761616" Z="0.65" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.65" />
                  <Point X="-3.685150159406" Y="1.50945733361" Z="0.65" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.65" />
                  <Point X="-3.737146063554" Y="1.47979848801" Z="0.65" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.65" />
                  <Point X="-4.025608932035" Y="1.510735874986" Z="0.65" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.65" />
                  <Point X="-4.932745647644" Y="1.185860937105" Z="0.65" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.65" />
                  <Point X="-5.037892524107" Y="0.598253431006" Z="0.65" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.65" />
                  <Point X="-4.679881185355" Y="0.344702860097" Z="0.65" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.65" />
                  <Point X="-3.555923849975" Y="0.034745854875" Z="0.65" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.65" />
                  <Point X="-3.54192877932" Y="0.007642666657" Z="0.65" />
                  <Point X="-3.539556741714" Y="0" Z="0.65" />
                  <Point X="-3.546435821815" Y="-0.022164284404" Z="0.65" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.65" />
                  <Point X="-3.569444745149" Y="-0.044130128259" Z="0.65" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.65" />
                  <Point X="-3.957006603418" Y="-0.151009196659" Z="0.65" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.65" />
                  <Point X="-5.002575542816" Y="-0.850435492984" Z="0.65" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.65" />
                  <Point X="-4.881712526674" Y="-1.384883157171" Z="0.65" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.65" />
                  <Point X="-4.429540672593" Y="-1.46621306238" Z="0.65" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.65" />
                  <Point X="-3.199466585888" Y="-1.318453339962" Z="0.65" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.65" />
                  <Point X="-3.198405326786" Y="-1.344569781307" Z="0.65" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.65" />
                  <Point X="-3.534354062682" Y="-1.608463855506" Z="0.65" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.65" />
                  <Point X="-4.284621619936" Y="-2.717676252689" Z="0.65" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.65" />
                  <Point X="-3.951241339277" Y="-3.182995139943" Z="0.65" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.65" />
                  <Point X="-3.531629769661" Y="-3.109048784063" Z="0.65" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.65" />
                  <Point X="-2.559939228856" Y="-2.568391240276" Z="0.65" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.65" />
                  <Point X="-2.746368229105" Y="-2.903448612831" Z="0.65" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.65" />
                  <Point X="-2.995460886088" Y="-4.096666913651" Z="0.65" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.65" />
                  <Point X="-2.563771506369" Y="-4.379789162672" Z="0.65" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.65" />
                  <Point X="-2.393453313158" Y="-4.374391832631" Z="0.65" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.65" />
                  <Point X="-2.034399973745" Y="-4.028280425266" Z="0.65" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.65" />
                  <Point X="-1.738047214159" Y="-3.99917306494" Z="0.65" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.65" />
                  <Point X="-1.485215352806" Y="-4.156489021572" Z="0.65" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.65" />
                  <Point X="-1.380398105731" Y="-4.435210154441" Z="0.65" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.65" />
                  <Point X="-1.377242541349" Y="-4.607146140631" Z="0.65" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.65" />
                  <Point X="-1.193220234479" Y="-4.936076158126" Z="0.65" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.65" />
                  <Point X="-0.894521469102" Y="-4.99884543735" Z="0.65" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.65" />
                  <Point X="-0.714956825476" Y="-4.630439301874" Z="0.65" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.65" />
                  <Point X="-0.295339764888" Y="-3.343358841085" Z="0.65" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.65" />
                  <Point X="-0.06496266539" Y="-3.224401382429" Z="0.65" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.65" />
                  <Point X="0.188396413972" Y="-3.262710662859" Z="0.65" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.65" />
                  <Point X="0.375106094544" Y="-3.458286914683" Z="0.65" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.65" />
                  <Point X="0.519798034926" Y="-3.902096727941" Z="0.65" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.65" />
                  <Point X="0.951769454981" Y="-4.989401625327" Z="0.65" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.65" />
                  <Point X="1.131145409099" Y="-4.951818206771" Z="0.65" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.65" />
                  <Point X="1.120718839102" Y="-4.513855103699" Z="0.65" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.65" />
                  <Point X="0.997361750065" Y="-3.088808860535" Z="0.65" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.65" />
                  <Point X="1.144994928741" Y="-2.91404667236" Z="0.65" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.65" />
                  <Point X="1.364465739511" Y="-2.859726192809" Z="0.65" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.65" />
                  <Point X="1.582707871373" Y="-2.956112924329" Z="0.65" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.65" />
                  <Point X="1.900090776153" Y="-3.333650567081" Z="0.65" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.65" />
                  <Point X="2.807216322698" Y="-4.232685101368" Z="0.65" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.65" />
                  <Point X="2.997990673943" Y="-4.099773059617" Z="0.65" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.65" />
                  <Point X="2.847727636269" Y="-3.720809455595" Z="0.65" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.65" />
                  <Point X="2.242217539128" Y="-2.56161496227" Z="0.65" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.65" />
                  <Point X="2.302465238736" Y="-2.372719598348" Z="0.65" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.65" />
                  <Point X="2.460178780024" Y="-2.256436071231" Z="0.65" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.65" />
                  <Point X="2.666891841243" Y="-2.261230470363" Z="0.65" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.65" />
                  <Point X="3.066604141511" Y="-2.470021864005" Z="0.65" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.65" />
                  <Point X="4.194951983017" Y="-2.86203209931" Z="0.65" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.65" />
                  <Point X="4.358315380285" Y="-2.606518163858" Z="0.65" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.65" />
                  <Point X="4.089864080339" Y="-2.302978472258" Z="0.65" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.65" />
                  <Point X="3.118026214253" Y="-1.498376252097" Z="0.65" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.65" />
                  <Point X="3.103958467322" Y="-1.331199694576" Z="0.65" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.65" />
                  <Point X="3.189596543585" Y="-1.18922664658" Z="0.65" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.65" />
                  <Point X="3.352745764072" Y="-1.126039149801" Z="0.65" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.65" />
                  <Point X="3.785884220169" Y="-1.166815217164" Z="0.65" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.65" />
                  <Point X="4.969789648516" Y="-1.039290515755" Z="0.65" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.65" />
                  <Point X="5.033508532318" Y="-0.665380410605" Z="0.65" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.65" />
                  <Point X="4.714671942979" Y="-0.479842265538" Z="0.65" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.65" />
                  <Point X="3.679162785091" Y="-0.181048833227" Z="0.65" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.65" />
                  <Point X="3.614168765949" Y="-0.114745497486" Z="0.65" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.65" />
                  <Point X="3.586178740795" Y="-0.024771473102" Z="0.65" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.65" />
                  <Point X="3.595192709629" Y="0.071839058128" Z="0.65" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.65" />
                  <Point X="3.641210672452" Y="0.149203241089" Z="0.65" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.65" />
                  <Point X="3.724232629262" Y="0.207100011015" Z="0.65" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.65" />
                  <Point X="4.081295874327" Y="0.310129667231" Z="0.65" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.65" />
                  <Point X="4.999010580588" Y="0.883909437875" Z="0.65" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.65" />
                  <Point X="4.906766374431" Y="1.301941390364" Z="0.65" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.65" />
                  <Point X="4.517288583205" Y="1.360807866533" Z="0.65" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.65" />
                  <Point X="3.393103966223" Y="1.231277741839" Z="0.65" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.65" />
                  <Point X="3.317175162106" Y="1.263619306154" Z="0.65" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.65" />
                  <Point X="3.263583218331" Y="1.32798716744" Z="0.65" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.65" />
                  <Point X="3.238122452822" Y="1.410392482505" Z="0.65" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.65" />
                  <Point X="3.249597125735" Y="1.489579580898" Z="0.65" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.65" />
                  <Point X="3.298082491123" Y="1.565366894508" Z="0.65" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.65" />
                  <Point X="3.603768093658" Y="1.807887443937" Z="0.65" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.65" />
                  <Point X="4.291805523022" Y="2.712137222266" Z="0.65" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.65" />
                  <Point X="4.062753072433" Y="3.044556552108" Z="0.65" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.65" />
                  <Point X="3.619605867174" Y="2.907700460247" Z="0.65" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.65" />
                  <Point X="2.45017749676" Y="2.251034096881" Z="0.65" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.65" />
                  <Point X="2.377967707085" Y="2.251754066833" Z="0.65" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.65" />
                  <Point X="2.313090776206" Y="2.285843520977" Z="0.65" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.65" />
                  <Point X="2.264915079258" Y="2.343934084176" Z="0.65" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.65" />
                  <Point X="2.247675635926" Y="2.411790736747" Z="0.65" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.65" />
                  <Point X="2.261493845776" Y="2.489292079122" Z="0.65" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.65" />
                  <Point X="2.487924892612" Y="2.892533214781" Z="0.65" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.65" />
                  <Point X="2.849682918537" Y="4.200630486783" Z="0.65" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.65" />
                  <Point X="2.457744198355" Y="4.441338757343" Z="0.65" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.65" />
                  <Point X="2.049601461394" Y="4.643934783701" Z="0.65" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.65" />
                  <Point X="1.62629803108" Y="4.808550627943" Z="0.65" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.65" />
                  <Point X="1.03029374554" Y="4.965731550357" Z="0.65" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.65" />
                  <Point X="0.364860388841" Y="5.058350271797" Z="0.65" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.65" />
                  <Point X="0.143695590101" Y="4.891403744019" Z="0.65" />
                  <Point X="0" Y="4.355124473572" Z="0.65" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>