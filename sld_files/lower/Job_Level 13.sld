<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#141" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1074" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.658127807617" Y="-28.866419921875" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542362854004" Y="-28.467375" />
                  <Point X="0.503605163574" Y="-28.411533203125" />
                  <Point X="0.378635070801" Y="-28.231474609375" />
                  <Point X="0.356752349854" Y="-28.209021484375" />
                  <Point X="0.330497161865" Y="-28.18977734375" />
                  <Point X="0.302494110107" Y="-28.17566796875" />
                  <Point X="0.242518844604" Y="-28.1570546875" />
                  <Point X="0.049134975433" Y="-28.09703515625" />
                  <Point X="0.020976791382" Y="-28.092765625" />
                  <Point X="-0.008664384842" Y="-28.092765625" />
                  <Point X="-0.036825450897" Y="-28.09703515625" />
                  <Point X="-0.096800727844" Y="-28.115650390625" />
                  <Point X="-0.290184448242" Y="-28.17566796875" />
                  <Point X="-0.318184143066" Y="-28.189775390625" />
                  <Point X="-0.34443963623" Y="-28.20901953125" />
                  <Point X="-0.366324035645" Y="-28.2314765625" />
                  <Point X="-0.405081726074" Y="-28.2873203125" />
                  <Point X="-0.530051696777" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.856745483398" Y="-29.653619140625" />
                  <Point X="-0.916584411621" Y="-29.87694140625" />
                  <Point X="-1.079326782227" Y="-29.8453515625" />
                  <Point X="-1.145112792969" Y="-29.82842578125" />
                  <Point X="-1.246417480469" Y="-29.802361328125" />
                  <Point X="-1.226377685547" Y="-29.65014453125" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.230836791992" Y="-29.444193359375" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323645141602" Y="-29.131203125" />
                  <Point X="-1.378862915039" Y="-29.082779296875" />
                  <Point X="-1.55690612793" Y="-28.926638671875" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886352539" Y="-28.897994140625" />
                  <Point X="-1.643028320312" Y="-28.890966796875" />
                  <Point X="-1.716314331055" Y="-28.8861640625" />
                  <Point X="-1.952617431641" Y="-28.87067578125" />
                  <Point X="-1.983414672852" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.042657348633" Y="-28.89480078125" />
                  <Point X="-2.103723388672" Y="-28.935603515625" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.480148193359" Y="-29.288490234375" />
                  <Point X="-2.801713623047" Y="-29.08938671875" />
                  <Point X="-2.892783447266" Y="-29.019265625" />
                  <Point X="-3.104722412109" Y="-28.856080078125" />
                  <Point X="-2.599984375" Y="-27.98184765625" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858154297" Y="-27.6476484375" />
                  <Point X="-2.406587646484" Y="-27.6161171875" />
                  <Point X="-2.405576904297" Y="-27.5851796875" />
                  <Point X="-2.414565185547" Y="-27.555560546875" />
                  <Point X="-2.428786865234" Y="-27.52673046875" />
                  <Point X="-2.446812011719" Y="-27.501580078125" />
                  <Point X="-2.464155029297" Y="-27.48423828125" />
                  <Point X="-2.489310791016" Y="-27.466212890625" />
                  <Point X="-2.518139892578" Y="-27.45199609375" />
                  <Point X="-2.5477578125" Y="-27.44301171875" />
                  <Point X="-2.578691894531" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.623355712891" Y="-28.029412109375" />
                  <Point X="-3.818022705078" Y="-28.141802734375" />
                  <Point X="-3.822873291016" Y="-28.1354296875" />
                  <Point X="-4.082862792969" Y="-27.793857421875" />
                  <Point X="-4.148150878906" Y="-27.68437890625" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.414287597656" Y="-26.735107421875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083064941406" Y="-26.4758828125" />
                  <Point X="-3.063912109375" Y="-26.445302734375" />
                  <Point X="-3.055374755859" Y="-26.42319140625" />
                  <Point X="-3.052032958984" Y="-26.41279296875" />
                  <Point X="-3.046152099609" Y="-26.390087890625" />
                  <Point X="-3.042037353516" Y="-26.35860546875" />
                  <Point X="-3.042736328125" Y="-26.335736328125" />
                  <Point X="-3.048882568359" Y="-26.31369921875" />
                  <Point X="-3.060887939453" Y="-26.28471484375" />
                  <Point X="-3.073567382812" Y="-26.262875" />
                  <Point X="-3.091552490234" Y="-26.245146484375" />
                  <Point X="-3.11032421875" Y="-26.23102734375" />
                  <Point X="-3.119243408203" Y="-26.225076171875" />
                  <Point X="-3.13945703125" Y="-26.2131796875" />
                  <Point X="-3.168721923828" Y="-26.201955078125" />
                  <Point X="-3.200608642578" Y="-26.195474609375" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.474352539062" Y="-26.357953125" />
                  <Point X="-4.732102539062" Y="-26.391884765625" />
                  <Point X="-4.732395019531" Y="-26.390740234375" />
                  <Point X="-4.834077636719" Y="-25.99265625" />
                  <Point X="-4.8513515625" Y="-25.871876953125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.884113769531" Y="-25.3145234375" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.512826416016" Y="-25.212505859375" />
                  <Point X="-3.490685791016" Y="-25.200720703125" />
                  <Point X="-3.481160888672" Y="-25.194908203125" />
                  <Point X="-3.459982177734" Y="-25.1802109375" />
                  <Point X="-3.436024169922" Y="-25.15868359375" />
                  <Point X="-3.415408935547" Y="-25.128994140625" />
                  <Point X="-3.40579296875" Y="-25.10719921875" />
                  <Point X="-3.401979003906" Y="-25.09701171875" />
                  <Point X="-3.39491796875" Y="-25.07426171875" />
                  <Point X="-3.389474609375" Y="-25.0455234375" />
                  <Point X="-3.390296386719" Y="-25.0123984375" />
                  <Point X="-3.394733154297" Y="-24.990521484375" />
                  <Point X="-3.397106689453" Y="-24.98124609375" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.417487304688" Y="-24.9291640625" />
                  <Point X="-3.439801757812" Y="-24.90038671875" />
                  <Point X="-3.458194335938" Y="-24.88430859375" />
                  <Point X="-3.466551513672" Y="-24.8777890625" />
                  <Point X="-3.487727539062" Y="-24.863091796875" />
                  <Point X="-3.501915527344" Y="-24.854958984375" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.665404296875" Y="-24.53869140625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.890020996094" Y="-24.465890625" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.789714355469" Y="-23.894701171875" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.017442138672" Y="-23.667060546875" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744984863281" Y="-23.700658203125" />
                  <Point X="-3.723423095703" Y="-23.698771484375" />
                  <Point X="-3.703141357422" Y="-23.694736328125" />
                  <Point X="-3.688600585938" Y="-23.69015234375" />
                  <Point X="-3.641715332031" Y="-23.675369140625" />
                  <Point X="-3.622783203125" Y="-23.667041015625" />
                  <Point X="-3.604039306641" Y="-23.656220703125" />
                  <Point X="-3.587354492188" Y="-23.64398828125" />
                  <Point X="-3.573713623047" Y="-23.62843359375" />
                  <Point X="-3.561299316406" Y="-23.610703125" />
                  <Point X="-3.551351318359" Y="-23.592568359375" />
                  <Point X="-3.545516845703" Y="-23.578482421875" />
                  <Point X="-3.526703857422" Y="-23.533064453125" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.532050292969" Y="-23.41062109375" />
                  <Point X="-3.539090332031" Y="-23.39709765625" />
                  <Point X="-3.561790039062" Y="-23.3534921875" />
                  <Point X="-3.573281982422" Y="-23.33629296875" />
                  <Point X="-3.587194580078" Y="-23.319712890625" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.251756835938" Y="-22.8069375" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.335789550781" Y="-22.70259375" />
                  <Point X="-4.081156738281" Y="-22.26634765625" />
                  <Point X="-3.989042480469" Y="-22.1479453125" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.361290771484" Y="-22.06605078125" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187729492188" Y="-22.163658203125" />
                  <Point X="-3.167087402344" Y="-22.17016796875" />
                  <Point X="-3.146793457031" Y="-22.174205078125" />
                  <Point X="-3.126542236328" Y="-22.1759765625" />
                  <Point X="-3.061244140625" Y="-22.181689453125" />
                  <Point X="-3.040560791016" Y="-22.18123828125" />
                  <Point X="-3.019102294922" Y="-22.178412109375" />
                  <Point X="-2.999013427734" Y="-22.173494140625" />
                  <Point X="-2.980464355469" Y="-22.16434765625" />
                  <Point X="-2.962210205078" Y="-22.15271875" />
                  <Point X="-2.946076660156" Y="-22.13976953125" />
                  <Point X="-2.931702148438" Y="-22.12539453125" />
                  <Point X="-2.885353027344" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.845207519531" Y="-21.94362890625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.157661621094" Y="-21.3198671875" />
                  <Point X="-3.183332763672" Y="-21.275404296875" />
                  <Point X="-3.144117919922" Y="-21.245337890625" />
                  <Point X="-2.700621582031" Y="-20.905314453125" />
                  <Point X="-2.555555664062" Y="-20.82471875" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.090533447266" Y="-20.70856640625" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028891967773" Y="-20.78519921875" />
                  <Point X="-2.01231237793" Y="-20.799111328125" />
                  <Point X="-1.995116210938" Y="-20.8106015625" />
                  <Point X="-1.972576660156" Y="-20.8223359375" />
                  <Point X="-1.899900146484" Y="-20.860169921875" />
                  <Point X="-1.88062512207" Y="-20.86766796875" />
                  <Point X="-1.859718261719" Y="-20.873271484375" />
                  <Point X="-1.839268554688" Y="-20.876419921875" />
                  <Point X="-1.818622192383" Y="-20.87506640625" />
                  <Point X="-1.797306762695" Y="-20.871306640625" />
                  <Point X="-1.777452880859" Y="-20.865517578125" />
                  <Point X="-1.753976196289" Y="-20.85579296875" />
                  <Point X="-1.678278686523" Y="-20.8244375" />
                  <Point X="-1.660145507812" Y="-20.814490234375" />
                  <Point X="-1.642416137695" Y="-20.802076171875" />
                  <Point X="-1.626864013672" Y="-20.7884375" />
                  <Point X="-1.61463269043" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.587839111328" Y="-20.70984375" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.523762817383" Y="-20.351158203125" />
                  <Point X="-0.949623046875" Y="-20.19019140625" />
                  <Point X="-0.773773193359" Y="-20.169609375" />
                  <Point X="-0.294711303711" Y="-20.113541015625" />
                  <Point X="-0.179648239136" Y="-20.542962890625" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113990784" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425941467" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.29371472168" Y="-20.163208984375" />
                  <Point X="0.307419342041" Y="-20.1120625" />
                  <Point X="0.342738311768" Y="-20.11576171875" />
                  <Point X="0.84403137207" Y="-20.168259765625" />
                  <Point X="0.989542419434" Y="-20.203390625" />
                  <Point X="1.481039794922" Y="-20.3220546875" />
                  <Point X="1.574602050781" Y="-20.355990234375" />
                  <Point X="1.894650024414" Y="-20.47207421875" />
                  <Point X="1.986226928711" Y="-20.514900390625" />
                  <Point X="2.294554443359" Y="-20.659095703125" />
                  <Point X="2.383078125" Y="-20.710669921875" />
                  <Point X="2.680974121094" Y="-20.88422265625" />
                  <Point X="2.764420410156" Y="-20.94356640625" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.351545898438" Y="-22.095625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.139487792969" Y="-22.46709375" />
                  <Point X="2.129404052734" Y="-22.49818359375" />
                  <Point X="2.127994628906" Y="-22.502951171875" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108398193359" Y="-22.59050390625" />
                  <Point X="2.109042236328" Y="-22.62583984375" />
                  <Point X="2.109709472656" Y="-22.635482421875" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.140071044922" Y="-22.752529296875" />
                  <Point X="2.150239990234" Y="-22.767515625" />
                  <Point X="2.183028808594" Y="-22.815837890625" />
                  <Point X="2.200976806641" Y="-22.83560546875" />
                  <Point X="2.229262939453" Y="-22.859076171875" />
                  <Point X="2.236584472656" Y="-22.864578125" />
                  <Point X="2.284906738281" Y="-22.8973671875" />
                  <Point X="2.304952880859" Y="-22.90773046875" />
                  <Point X="2.327040771484" Y="-22.91599609375" />
                  <Point X="2.348970214844" Y="-22.92133984375" />
                  <Point X="2.365404541016" Y="-22.9233203125" />
                  <Point X="2.418395019531" Y="-22.9297109375" />
                  <Point X="2.445623291016" Y="-22.92905859375" />
                  <Point X="2.483532958984" Y="-22.922638671875" />
                  <Point X="2.492214355469" Y="-22.920748046875" />
                  <Point X="2.553495361328" Y="-22.904359375" />
                  <Point X="2.565289794922" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.727639404297" Y="-22.23219140625" />
                  <Point X="3.967326416016" Y="-22.09380859375" />
                  <Point X="4.123272460938" Y="-22.3105390625" />
                  <Point X="4.169790527344" Y="-22.38741015625" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.499322509766" Y="-23.125490234375" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221421386719" Y="-23.33976171875" />
                  <Point X="3.203974609375" Y="-23.358373046875" />
                  <Point X="3.190296386719" Y="-23.376216796875" />
                  <Point X="3.146192382812" Y="-23.43375390625" />
                  <Point X="3.132953369141" Y="-23.457361328125" />
                  <Point X="3.119388427734" Y="-23.49253125" />
                  <Point X="3.116534423828" Y="-23.5011328125" />
                  <Point X="3.100105712891" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739257812" Y="-23.628232421875" />
                  <Point X="3.101921875" Y="-23.64850390625" />
                  <Point X="3.115408203125" Y="-23.713865234375" />
                  <Point X="3.120678955078" Y="-23.73101953125" />
                  <Point X="3.136282958984" Y="-23.76426171875" />
                  <Point X="3.147659179688" Y="-23.781552734375" />
                  <Point X="3.184340576172" Y="-23.837306640625" />
                  <Point X="3.198895751953" Y="-23.854552734375" />
                  <Point X="3.216138671875" Y="-23.870640625" />
                  <Point X="3.234348632812" Y="-23.883966796875" />
                  <Point X="3.250834228516" Y="-23.89324609375" />
                  <Point X="3.303990722656" Y="-23.923169921875" />
                  <Point X="3.320521484375" Y="-23.9305" />
                  <Point X="3.35612109375" Y="-23.9405625" />
                  <Point X="3.378410888672" Y="-23.9435078125" />
                  <Point X="3.450281738281" Y="-23.953005859375" />
                  <Point X="3.462698242188" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.570277832031" Y="-23.81055859375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.860595214844" Y="-24.161345703125" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.024331054688" Y="-24.587947265625" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.704788574219" Y="-24.674416015625" />
                  <Point X="3.681550537109" Y="-24.6849296875" />
                  <Point X="3.659651611328" Y="-24.6975859375" />
                  <Point X="3.589040527344" Y="-24.738400390625" />
                  <Point X="3.574311523438" Y="-24.74890234375" />
                  <Point X="3.547528564453" Y="-24.77442578125" />
                  <Point X="3.534389160156" Y="-24.791169921875" />
                  <Point X="3.492022705078" Y="-24.845154296875" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.470526855469" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.459301025391" Y="-24.930265625" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.449558837891" Y="-25.081423828125" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.47052734375" Y="-25.1766640625" />
                  <Point X="3.48030078125" Y="-25.198126953125" />
                  <Point X="3.492025634766" Y="-25.217408203125" />
                  <Point X="3.505165039062" Y="-25.234150390625" />
                  <Point X="3.547531494141" Y="-25.28813671875" />
                  <Point X="3.559994873047" Y="-25.301232421875" />
                  <Point X="3.589035400391" Y="-25.32415625" />
                  <Point X="3.610934570312" Y="-25.336814453125" />
                  <Point X="3.681545410156" Y="-25.37762890625" />
                  <Point X="3.692708496094" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="4.708891113281" Y="-25.658041015625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.855022460938" Y="-25.948728515625" />
                  <Point X="4.836241210938" Y="-26.031029296875" />
                  <Point X="4.801174316406" Y="-26.184697265625" />
                  <Point X="3.781286865234" Y="-26.050427734375" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035400391" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.331679199219" Y="-26.0148515625" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163974365234" Y="-26.05659765625" />
                  <Point X="3.136147460938" Y="-26.073490234375" />
                  <Point X="3.112397705078" Y="-26.093958984375" />
                  <Point X="3.086418945312" Y="-26.125203125" />
                  <Point X="3.002653564453" Y="-26.225947265625" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.966034179688" Y="-26.345828125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450927734" Y="-26.567998046875" />
                  <Point X="3.000236572266" Y="-26.604994140625" />
                  <Point X="3.076931152344" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="4.031499267578" Y="-27.46751953125" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.124811523438" Y="-27.749783203125" />
                  <Point X="4.085968261719" Y="-27.804974609375" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="3.118826660156" Y="-27.36046875" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754224609375" Y="-27.159826171875" />
                  <Point X="2.703071533203" Y="-27.150587890625" />
                  <Point X="2.538134277344" Y="-27.12080078125" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444833251953" Y="-27.135177734375" />
                  <Point X="2.402337646484" Y="-27.15754296875" />
                  <Point X="2.265315185547" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.182166503906" Y="-27.332935546875" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.104913330078" Y="-27.614412109375" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.743571289062" Y="-28.8510234375" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.781839599609" Y="-29.111650390625" />
                  <Point X="2.738423828125" Y="-29.13975390625" />
                  <Point X="2.701764404297" Y="-29.163482421875" />
                  <Point X="2.001111938477" Y="-28.250375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506225586" Y="-27.922181640625" />
                  <Point X="1.721924194336" Y="-27.900556640625" />
                  <Point X="1.671473388672" Y="-27.86812109375" />
                  <Point X="1.50880078125" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365722656" Y="-27.743435546875" />
                  <Point X="1.417101806641" Y="-27.741119140625" />
                  <Point X="1.361925415039" Y="-27.7461953125" />
                  <Point X="1.184014892578" Y="-27.76256640625" />
                  <Point X="1.156362426758" Y="-27.7693984375" />
                  <Point X="1.128976928711" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.795462890625" />
                  <Point X="1.061989379883" Y="-27.830888671875" />
                  <Point X="0.92461151123" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.862885498047" Y="-28.08441796875" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.987440124512" Y="-29.596912109375" />
                  <Point X="1.022065429688" Y="-29.859916015625" />
                  <Point X="0.975725219727" Y="-29.870072265625" />
                  <Point X="0.935556335449" Y="-29.87737109375" />
                  <Point X="0.929315612793" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058424804688" Y="-29.752634765625" />
                  <Point X="-1.121441650391" Y="-29.736421875" />
                  <Point X="-1.141245727539" Y="-29.731326171875" />
                  <Point X="-1.132190429688" Y="-29.662544921875" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.541037109375" />
                  <Point X="-1.121759155273" Y="-29.50933203125" />
                  <Point X="-1.123333984375" Y="-29.497693359375" />
                  <Point X="-1.137662231445" Y="-29.42566015625" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026367188" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230573730469" Y="-29.094861328125" />
                  <Point X="-1.250209106445" Y="-29.070935546875" />
                  <Point X="-1.26100793457" Y="-29.05977734375" />
                  <Point X="-1.316225708008" Y="-29.011353515625" />
                  <Point X="-1.494268920898" Y="-28.855212890625" />
                  <Point X="-1.506739746094" Y="-28.84596484375" />
                  <Point X="-1.533022827148" Y="-28.82962109375" />
                  <Point X="-1.546834716797" Y="-28.822525390625" />
                  <Point X="-1.576531982422" Y="-28.810224609375" />
                  <Point X="-1.59131640625" Y="-28.805474609375" />
                  <Point X="-1.621458251953" Y="-28.798447265625" />
                  <Point X="-1.636815917969" Y="-28.796169921875" />
                  <Point X="-1.710101928711" Y="-28.7913671875" />
                  <Point X="-1.946405029297" Y="-28.77587890625" />
                  <Point X="-1.961928833008" Y="-28.7761328125" />
                  <Point X="-1.992726074219" Y="-28.779166015625" />
                  <Point X="-2.007999511719" Y="-28.7819453125" />
                  <Point X="-2.039048095703" Y="-28.790263671875" />
                  <Point X="-2.053667480469" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436279297" Y="-28.815810546875" />
                  <Point X="-2.156502197266" Y="-28.85661328125" />
                  <Point X="-2.353402832031" Y="-28.988177734375" />
                  <Point X="-2.359683349609" Y="-28.9927578125" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.503201660156" Y="-29.16248046875" />
                  <Point X="-2.747598876953" Y="-29.01115625" />
                  <Point X="-2.834825927734" Y="-28.943994140625" />
                  <Point X="-2.980863525391" Y="-28.83155078125" />
                  <Point X="-2.517711914062" Y="-28.02934765625" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849365234" Y="-27.710080078125" />
                  <Point X="-2.323946289062" Y="-27.681109375" />
                  <Point X="-2.319682617188" Y="-27.666177734375" />
                  <Point X="-2.313412109375" Y="-27.634646484375" />
                  <Point X="-2.311638183594" Y="-27.61921875" />
                  <Point X="-2.310627441406" Y="-27.58828125" />
                  <Point X="-2.314670410156" Y="-27.55759375" />
                  <Point X="-2.323658691406" Y="-27.527974609375" />
                  <Point X="-2.3293671875" Y="-27.513533203125" />
                  <Point X="-2.343588867188" Y="-27.484703125" />
                  <Point X="-2.3515703125" Y="-27.471390625" />
                  <Point X="-2.369595458984" Y="-27.446240234375" />
                  <Point X="-2.379639160156" Y="-27.43440234375" />
                  <Point X="-2.396982177734" Y="-27.417060546875" />
                  <Point X="-2.408821533203" Y="-27.407015625" />
                  <Point X="-2.433977294922" Y="-27.388990234375" />
                  <Point X="-2.447293701172" Y="-27.381009765625" />
                  <Point X="-2.476122802734" Y="-27.36679296875" />
                  <Point X="-2.490563232422" Y="-27.3610859375" />
                  <Point X="-2.520181152344" Y="-27.3521015625" />
                  <Point X="-2.550869140625" Y="-27.3480625" />
                  <Point X="-2.581803222656" Y="-27.349076171875" />
                  <Point X="-2.597226806641" Y="-27.3508515625" />
                  <Point X="-2.628753417969" Y="-27.357123046875" />
                  <Point X="-2.643684082031" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.670855712891" Y="-27.947140625" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-4.004016357422" Y="-27.74059375" />
                  <Point X="-4.066558105469" Y="-27.635720703125" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.356455322266" Y="-26.8104765625" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039159423828" Y="-26.566068359375" />
                  <Point X="-3.016269775391" Y="-26.543435546875" />
                  <Point X="-3.002552734375" Y="-26.52630859375" />
                  <Point X="-2.983399902344" Y="-26.495728515625" />
                  <Point X="-2.975288574219" Y="-26.479521484375" />
                  <Point X="-2.966751220703" Y="-26.45741015625" />
                  <Point X="-2.960067626953" Y="-26.43661328125" />
                  <Point X="-2.954186767578" Y="-26.413908203125" />
                  <Point X="-2.951953369141" Y="-26.402400390625" />
                  <Point X="-2.947838623047" Y="-26.37091796875" />
                  <Point X="-2.947081787109" Y="-26.355703125" />
                  <Point X="-2.947780761719" Y="-26.332833984375" />
                  <Point X="-2.951228759766" Y="-26.31021484375" />
                  <Point X="-2.957375" Y="-26.288177734375" />
                  <Point X="-2.961113525391" Y="-26.277345703125" />
                  <Point X="-2.973118896484" Y="-26.248361328125" />
                  <Point X="-2.978730224609" Y="-26.237017578125" />
                  <Point X="-2.991409667969" Y="-26.215177734375" />
                  <Point X="-3.006876708984" Y="-26.19521875" />
                  <Point X="-3.024861816406" Y="-26.177490234375" />
                  <Point X="-3.034447998047" Y="-26.169224609375" />
                  <Point X="-3.053219726562" Y="-26.15510546875" />
                  <Point X="-3.071058105469" Y="-26.143203125" />
                  <Point X="-3.091271728516" Y="-26.131306640625" />
                  <Point X="-3.105436279297" Y="-26.12448046875" />
                  <Point X="-3.134701171875" Y="-26.113255859375" />
                  <Point X="-3.149801513672" Y="-26.108857421875" />
                  <Point X="-3.181688232422" Y="-26.102376953125" />
                  <Point X="-3.197304931641" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.486752441406" Y="-26.263765625" />
                  <Point X="-4.660920898438" Y="-26.286693359375" />
                  <Point X="-4.740762695312" Y="-25.9741171875" />
                  <Point X="-4.75730859375" Y="-25.858427734375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.859525878906" Y="-25.406287109375" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.498033203125" Y="-25.3087890625" />
                  <Point X="-3.477983642578" Y="-25.300884765625" />
                  <Point X="-3.468188964844" Y="-25.296365234375" />
                  <Point X="-3.446048339844" Y="-25.284580078125" />
                  <Point X="-3.426998535156" Y="-25.272955078125" />
                  <Point X="-3.405819824219" Y="-25.2582578125" />
                  <Point X="-3.396487304688" Y="-25.250875" />
                  <Point X="-3.372529296875" Y="-25.22934765625" />
                  <Point X="-3.357990966797" Y="-25.2128671875" />
                  <Point X="-3.337375732422" Y="-25.183177734375" />
                  <Point X="-3.328492675781" Y="-25.167341796875" />
                  <Point X="-3.318876708984" Y="-25.145546875" />
                  <Point X="-3.311248779297" Y="-25.125171875" />
                  <Point X="-3.304187744141" Y="-25.102421875" />
                  <Point X="-3.301577636719" Y="-25.09194140625" />
                  <Point X="-3.296134277344" Y="-25.063203125" />
                  <Point X="-3.29450390625" Y="-25.04316796875" />
                  <Point X="-3.295325683594" Y="-25.01004296875" />
                  <Point X="-3.297191894531" Y="-24.993515625" />
                  <Point X="-3.301628662109" Y="-24.971638671875" />
                  <Point X="-3.306375732422" Y="-24.953087890625" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.317669189453" Y="-24.919212890625" />
                  <Point X="-3.330988769531" Y="-24.8898828125" />
                  <Point X="-3.342413085938" Y="-24.87094921875" />
                  <Point X="-3.364727539062" Y="-24.842171875" />
                  <Point X="-3.377277587891" Y="-24.828861328125" />
                  <Point X="-3.395670166016" Y="-24.812783203125" />
                  <Point X="-3.412384521484" Y="-24.799744140625" />
                  <Point X="-3.433560546875" Y="-24.785046875" />
                  <Point X="-3.440483154297" Y="-24.780671875" />
                  <Point X="-3.465598388672" Y="-24.767173828125" />
                  <Point X="-3.496558837891" Y="-24.754365234375" />
                  <Point X="-3.508288330078" Y="-24.75038671875" />
                  <Point X="-4.64081640625" Y="-24.446927734375" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.698021484375" Y="-23.919548828125" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.029842041016" Y="-23.761248046875" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747056884766" Y="-23.795634765625" />
                  <Point X="-3.736703613281" Y="-23.795296875" />
                  <Point X="-3.715141845703" Y="-23.79341015625" />
                  <Point X="-3.704885742188" Y="-23.7919453125" />
                  <Point X="-3.684604003906" Y="-23.78791015625" />
                  <Point X="-3.660037597656" Y="-23.780755859375" />
                  <Point X="-3.61315234375" Y="-23.76597265625" />
                  <Point X="-3.603462890625" Y="-23.762328125" />
                  <Point X="-3.584530761719" Y="-23.754" />
                  <Point X="-3.575288085938" Y="-23.74931640625" />
                  <Point X="-3.556544189453" Y="-23.73849609375" />
                  <Point X="-3.547869140625" Y="-23.7328359375" />
                  <Point X="-3.531184326172" Y="-23.720603515625" />
                  <Point X="-3.515929199219" Y="-23.706625" />
                  <Point X="-3.502288330078" Y="-23.6910703125" />
                  <Point X="-3.495892822266" Y="-23.682921875" />
                  <Point X="-3.483478515625" Y="-23.66519140625" />
                  <Point X="-3.478008300781" Y="-23.656392578125" />
                  <Point X="-3.468060302734" Y="-23.6382578125" />
                  <Point X="-3.457748046875" Y="-23.6148359375" />
                  <Point X="-3.438935058594" Y="-23.56941796875" />
                  <Point X="-3.435499267578" Y="-23.5596484375" />
                  <Point X="-3.429711181641" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436012207031" Y="-23.39546875" />
                  <Point X="-3.443509521484" Y="-23.376189453125" />
                  <Point X="-3.454824707031" Y="-23.35323046875" />
                  <Point X="-3.477524414062" Y="-23.309625" />
                  <Point X="-3.482800048828" Y="-23.300712890625" />
                  <Point X="-3.494291992188" Y="-23.283513671875" />
                  <Point X="-3.500508300781" Y="-23.2752265625" />
                  <Point X="-3.514420898438" Y="-23.258646484375" />
                  <Point X="-3.521501708984" Y="-23.251087890625" />
                  <Point X="-3.536442871094" Y="-23.23678515625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-4.193924316406" Y="-22.731568359375" />
                  <Point X="-4.227613769531" Y="-22.705716796875" />
                  <Point X="-4.002296142578" Y="-22.3196953125" />
                  <Point X="-3.914061279297" Y="-22.206279296875" />
                  <Point X="-3.726336914062" Y="-21.964986328125" />
                  <Point X="-3.408790771484" Y="-22.148322265625" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244924804688" Y="-22.242279296875" />
                  <Point X="-3.225997314453" Y="-22.250609375" />
                  <Point X="-3.216302001953" Y="-22.254259765625" />
                  <Point X="-3.195659912109" Y="-22.26076953125" />
                  <Point X="-3.185622802734" Y="-22.263341796875" />
                  <Point X="-3.165328857422" Y="-22.26737890625" />
                  <Point X="-3.155072021484" Y="-22.26884375" />
                  <Point X="-3.134820800781" Y="-22.270615234375" />
                  <Point X="-3.069522705078" Y="-22.276328125" />
                  <Point X="-3.059172363281" Y="-22.276666015625" />
                  <Point X="-3.038489013672" Y="-22.27621484375" />
                  <Point X="-3.028156005859" Y="-22.27542578125" />
                  <Point X="-3.006697509766" Y="-22.272599609375" />
                  <Point X="-2.996512451172" Y="-22.2706875" />
                  <Point X="-2.976423583984" Y="-22.26576953125" />
                  <Point X="-2.956999267578" Y="-22.25869921875" />
                  <Point X="-2.938450195312" Y="-22.249552734375" />
                  <Point X="-2.929421630859" Y="-22.244470703125" />
                  <Point X="-2.911167480469" Y="-22.232841796875" />
                  <Point X="-2.902745605469" Y="-22.226806640625" />
                  <Point X="-2.886612060547" Y="-22.213857421875" />
                  <Point X="-2.878900390625" Y="-22.206943359375" />
                  <Point X="-2.864525878906" Y="-22.192568359375" />
                  <Point X="-2.818176757812" Y="-22.146220703125" />
                  <Point X="-2.811265869141" Y="-22.13851171875" />
                  <Point X="-2.798321777344" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.750569091797" Y="-21.935349609375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.059387207031" Y="-21.300083984375" />
                  <Point X="-2.648376953125" Y="-20.984966796875" />
                  <Point X="-2.50941796875" Y="-20.907763671875" />
                  <Point X="-2.192524169922" Y="-20.731703125" />
                  <Point X="-2.16590234375" Y="-20.7663984375" />
                  <Point X="-2.118564208984" Y="-20.82808984375" />
                  <Point X="-2.111819824219" Y="-20.835951171875" />
                  <Point X="-2.097516357422" Y="-20.850892578125" />
                  <Point X="-2.089957275391" Y="-20.85797265625" />
                  <Point X="-2.073377685547" Y="-20.871884765625" />
                  <Point X="-2.065092041016" Y="-20.8781015625" />
                  <Point X="-2.047895751953" Y="-20.889591796875" />
                  <Point X="-2.038985351562" Y="-20.894865234375" />
                  <Point X="-2.016445800781" Y="-20.906599609375" />
                  <Point X="-1.943769165039" Y="-20.94443359375" />
                  <Point X="-1.934341308594" Y="-20.94870703125" />
                  <Point X="-1.91506628418" Y="-20.956205078125" />
                  <Point X="-1.905219238281" Y="-20.9594296875" />
                  <Point X="-1.88431237793" Y="-20.965033203125" />
                  <Point X="-1.874174194336" Y="-20.967166015625" />
                  <Point X="-1.853724487305" Y="-20.970314453125" />
                  <Point X="-1.833053955078" Y="-20.971216796875" />
                  <Point X="-1.812407470703" Y="-20.96986328125" />
                  <Point X="-1.802120117188" Y="-20.968623046875" />
                  <Point X="-1.7808046875" Y="-20.96486328125" />
                  <Point X="-1.770713745117" Y="-20.9625078125" />
                  <Point X="-1.750859863281" Y="-20.95671875" />
                  <Point X="-1.741097167969" Y="-20.95328515625" />
                  <Point X="-1.717620483398" Y="-20.943560546875" />
                  <Point X="-1.641922973633" Y="-20.912205078125" />
                  <Point X="-1.632588134766" Y="-20.907728515625" />
                  <Point X="-1.614454956055" Y="-20.89778125" />
                  <Point X="-1.60565625" Y="-20.892310546875" />
                  <Point X="-1.587926879883" Y="-20.879896484375" />
                  <Point X="-1.579778686523" Y="-20.873501953125" />
                  <Point X="-1.5642265625" Y="-20.85986328125" />
                  <Point X="-1.550251464844" Y="-20.844611328125" />
                  <Point X="-1.538020019531" Y="-20.8279296875" />
                  <Point X="-1.532360229492" Y="-20.819255859375" />
                  <Point X="-1.521538330078" Y="-20.80051171875" />
                  <Point X="-1.51685546875" Y="-20.791271484375" />
                  <Point X="-1.508525024414" Y="-20.772337890625" />
                  <Point X="-1.504877197266" Y="-20.76264453125" />
                  <Point X="-1.497235961914" Y="-20.73841015625" />
                  <Point X="-1.472597900391" Y="-20.660267578125" />
                  <Point X="-1.470026489258" Y="-20.650234375" />
                  <Point X="-1.465990966797" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-0.931163269043" Y="-20.2836796875" />
                  <Point X="-0.762729431152" Y="-20.26396484375" />
                  <Point X="-0.365222503662" Y="-20.21744140625" />
                  <Point X="-0.271411132812" Y="-20.56755078125" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.378190490723" Y="-20.214994140625" />
                  <Point X="0.827853393555" Y="-20.262083984375" />
                  <Point X="0.967247070312" Y="-20.29573828125" />
                  <Point X="1.453623535156" Y="-20.413166015625" />
                  <Point X="1.542209716797" Y="-20.445296875" />
                  <Point X="1.858256713867" Y="-20.5599296875" />
                  <Point X="1.945983154297" Y="-20.600955078125" />
                  <Point X="2.250421142578" Y="-20.74333203125" />
                  <Point X="2.335255126953" Y="-20.792755859375" />
                  <Point X="2.629443115234" Y="-20.9641484375" />
                  <Point X="2.709363037109" Y="-21.020984375" />
                  <Point X="2.817779541016" Y="-21.098083984375" />
                  <Point X="2.2692734375" Y="-22.048125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.060783447266" Y="-22.410287109375" />
                  <Point X="2.049122070312" Y="-22.43778515625" />
                  <Point X="2.039038330078" Y="-22.468875" />
                  <Point X="2.036219360352" Y="-22.47841015625" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017307983398" Y="-22.55271484375" />
                  <Point X="2.014099121094" Y="-22.578986328125" />
                  <Point X="2.01341394043" Y="-22.592234375" />
                  <Point X="2.014058105469" Y="-22.6275703125" />
                  <Point X="2.015392700195" Y="-22.64685546875" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144042969" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045318359375" Y="-22.776111328125" />
                  <Point X="2.055680908203" Y="-22.79615625" />
                  <Point X="2.07162890625" Y="-22.820857421875" />
                  <Point X="2.104417724609" Y="-22.8691796875" />
                  <Point X="2.112694580078" Y="-22.879697265625" />
                  <Point X="2.130642578125" Y="-22.89946484375" />
                  <Point X="2.140313720703" Y="-22.90871484375" />
                  <Point X="2.168599853516" Y="-22.932185546875" />
                  <Point X="2.183242919922" Y="-22.943189453125" />
                  <Point X="2.231565185547" Y="-22.975978515625" />
                  <Point X="2.241279541016" Y="-22.9817578125" />
                  <Point X="2.261325683594" Y="-22.99212109375" />
                  <Point X="2.271657470703" Y="-22.996705078125" />
                  <Point X="2.293745361328" Y="-23.004970703125" />
                  <Point X="2.304549316406" Y="-23.008294921875" />
                  <Point X="2.326478759766" Y="-23.013638671875" />
                  <Point X="2.354038574219" Y="-23.017638671875" />
                  <Point X="2.407029052734" Y="-23.024029296875" />
                  <Point X="2.420670410156" Y="-23.02468359375" />
                  <Point X="2.447898681641" Y="-23.02403125" />
                  <Point X="2.461485595703" Y="-23.022724609375" />
                  <Point X="2.499395263672" Y="-23.0163046875" />
                  <Point X="2.516758056641" Y="-23.0125234375" />
                  <Point X="2.5780390625" Y="-22.996134765625" />
                  <Point X="2.584007080078" Y="-22.994326171875" />
                  <Point X="2.604416748047" Y="-22.986927734375" />
                  <Point X="2.627660888672" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.775139404297" Y="-22.314462890625" />
                  <Point X="3.940403808594" Y="-22.219046875" />
                  <Point X="4.043956542969" Y="-22.362962890625" />
                  <Point X="4.088513671875" Y="-22.43659375" />
                  <Point X="4.136884765625" Y="-22.516529296875" />
                  <Point X="3.441490234375" Y="-23.05012109375" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168135742188" Y="-23.2601328125" />
                  <Point X="3.152112792969" Y="-23.2747890625" />
                  <Point X="3.134666015625" Y="-23.293400390625" />
                  <Point X="3.128577880859" Y="-23.300578125" />
                  <Point X="3.114899658203" Y="-23.318421875" />
                  <Point X="3.070795654297" Y="-23.375958984375" />
                  <Point X="3.063332519531" Y="-23.387287109375" />
                  <Point X="3.050093505859" Y="-23.41089453125" />
                  <Point X="3.044317626953" Y="-23.423173828125" />
                  <Point X="3.030752685547" Y="-23.45834375" />
                  <Point X="3.025044677734" Y="-23.475546875" />
                  <Point X="3.008615966797" Y="-23.53429296875" />
                  <Point X="3.006224853516" Y="-23.54533984375" />
                  <Point X="3.002771484375" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077636719" Y="-23.636244140625" />
                  <Point X="3.004698974609" Y="-23.6474296875" />
                  <Point X="3.008881591797" Y="-23.667701171875" />
                  <Point X="3.022367919922" Y="-23.7330625" />
                  <Point X="3.024598144531" Y="-23.741767578125" />
                  <Point X="3.034681884766" Y="-23.77138671875" />
                  <Point X="3.050285888672" Y="-23.80462890625" />
                  <Point X="3.056919433594" Y="-23.8164765625" />
                  <Point X="3.068295654297" Y="-23.833767578125" />
                  <Point X="3.104977050781" Y="-23.889521484375" />
                  <Point X="3.111740722656" Y="-23.898578125" />
                  <Point X="3.126295898438" Y="-23.91582421875" />
                  <Point X="3.134087402344" Y="-23.924013671875" />
                  <Point X="3.151330322266" Y="-23.9401015625" />
                  <Point X="3.16003515625" Y="-23.9473046875" />
                  <Point X="3.178245117188" Y="-23.960630859375" />
                  <Point X="3.204235839844" Y="-23.976033203125" />
                  <Point X="3.257392333984" Y="-24.00595703125" />
                  <Point X="3.265481933594" Y="-24.010015625" />
                  <Point X="3.294681396484" Y="-24.02191796875" />
                  <Point X="3.330281005859" Y="-24.03198046875" />
                  <Point X="3.343676269531" Y="-24.034744140625" />
                  <Point X="3.365966064453" Y="-24.037689453125" />
                  <Point X="3.437836914063" Y="-24.0471875" />
                  <Point X="3.444033935547" Y="-24.04780078125" />
                  <Point X="3.465708496094" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.582677734375" Y="-23.90474609375" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.766726074219" Y="-24.1759609375" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="3.999743164062" Y="-24.49618359375" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.686020019531" Y="-24.580458984375" />
                  <Point X="3.66562890625" Y="-24.58786328125" />
                  <Point X="3.642390869141" Y="-24.598376953125" />
                  <Point X="3.634014160156" Y="-24.602677734375" />
                  <Point X="3.612115234375" Y="-24.615333984375" />
                  <Point X="3.541504150391" Y="-24.6561484375" />
                  <Point X="3.533888183594" Y="-24.661048828125" />
                  <Point X="3.508772949219" Y="-24.68012890625" />
                  <Point X="3.481989990234" Y="-24.70565234375" />
                  <Point X="3.472791992188" Y="-24.715779296875" />
                  <Point X="3.459652587891" Y="-24.7325234375" />
                  <Point X="3.417286132812" Y="-24.7865078125" />
                  <Point X="3.410850830078" Y="-24.795796875" />
                  <Point X="3.399129150391" Y="-24.81507421875" />
                  <Point X="3.393842773438" Y="-24.8250625" />
                  <Point X="3.384068603516" Y="-24.84652734375" />
                  <Point X="3.380005126953" Y="-24.857072265625" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.370376708984" Y="-24.88952734375" />
                  <Point X="3.365996826172" Y="-24.912396484375" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.356254638672" Y="-25.09929296875" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159667969" Y="-25.18398828125" />
                  <Point X="3.380005859375" Y="-25.20548828125" />
                  <Point X="3.384069335938" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.23749609375" />
                  <Point X="3.399130371094" Y="-25.247486328125" />
                  <Point X="3.410855224609" Y="-25.266767578125" />
                  <Point X="3.417292480469" Y="-25.27605859375" />
                  <Point X="3.430431884766" Y="-25.29280078125" />
                  <Point X="3.472798339844" Y="-25.346787109375" />
                  <Point X="3.478715576172" Y="-25.353630859375" />
                  <Point X="3.501133300781" Y="-25.37580078125" />
                  <Point X="3.530173828125" Y="-25.398724609375" />
                  <Point X="3.541493896484" Y="-25.406404296875" />
                  <Point X="3.563393066406" Y="-25.4190625" />
                  <Point X="3.63400390625" Y="-25.459876953125" />
                  <Point X="3.639486816406" Y="-25.4628125" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.68302734375" Y="-25.481029296875" />
                  <Point X="3.691991943359" Y="-25.483916015625" />
                  <Point X="4.684303222656" Y="-25.7498046875" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.761614257812" Y="-25.93105078125" />
                  <Point X="4.743622070312" Y="-26.009892578125" />
                  <Point X="4.727802246094" Y="-26.079216796875" />
                  <Point X="3.793686767578" Y="-25.956240234375" />
                  <Point X="3.436781738281" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400096923828" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354481933594" Y="-25.912677734375" />
                  <Point X="3.311501953125" Y="-25.92201953125" />
                  <Point X="3.172917724609" Y="-25.952140625" />
                  <Point X="3.157873535156" Y="-25.9567421875" />
                  <Point X="3.128752929688" Y="-25.9683671875" />
                  <Point X="3.114676513672" Y="-25.975390625" />
                  <Point X="3.086849609375" Y="-25.992283203125" />
                  <Point X="3.074127197266" Y="-26.001529296875" />
                  <Point X="3.050377441406" Y="-26.021998046875" />
                  <Point X="3.039350097656" Y="-26.033220703125" />
                  <Point X="3.013371337891" Y="-26.06446484375" />
                  <Point X="2.929605957031" Y="-26.165208984375" />
                  <Point X="2.921325927734" Y="-26.17684765625" />
                  <Point X="2.906605224609" Y="-26.20123046875" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.871433837891" Y="-26.337123046875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876787109375" Y="-26.576671875" />
                  <Point X="2.889158935547" Y="-26.605482421875" />
                  <Point X="2.896541259766" Y="-26.619373046875" />
                  <Point X="2.920326904297" Y="-26.656369140625" />
                  <Point X="2.997021484375" Y="-26.775662109375" />
                  <Point X="3.001740966797" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.973666992188" Y="-27.542888671875" />
                  <Point X="4.087170410156" Y="-27.629984375" />
                  <Point X="4.045490966797" Y="-27.69742578125" />
                  <Point X="4.008279785156" Y="-27.750298828125" />
                  <Point X="4.001273681641" Y="-27.76025390625" />
                  <Point X="3.166326660156" Y="-27.278197265625" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841199951172" Y="-27.090890625" />
                  <Point X="2.815021240234" Y="-27.07951171875" />
                  <Point X="2.783116943359" Y="-27.069326171875" />
                  <Point X="2.771108642578" Y="-27.066337890625" />
                  <Point X="2.719955566406" Y="-27.057099609375" />
                  <Point X="2.555018310547" Y="-27.0273125" />
                  <Point X="2.539359375" Y="-27.02580859375" />
                  <Point X="2.508008789062" Y="-27.025404296875" />
                  <Point X="2.492317138672" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444847412109" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400588623047" Y="-27.051109375" />
                  <Point X="2.358093017578" Y="-27.073474609375" />
                  <Point X="2.221070556641" Y="-27.145587890625" />
                  <Point X="2.208966552734" Y="-27.153171875" />
                  <Point X="2.186037597656" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144942138672" Y="-27.21116015625" />
                  <Point X="2.128047607422" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.098098388672" Y="-27.28869140625" />
                  <Point X="2.025984619141" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.44019140625" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.0021875" Y="-27.580140625" />
                  <Point X="2.011425537109" Y="-27.631294921875" />
                  <Point X="2.041213134766" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.661298828125" Y="-28.8985234375" />
                  <Point X="2.735893066406" Y="-29.027724609375" />
                  <Point X="2.723752929688" Y="-29.036083984375" />
                  <Point X="2.07648046875" Y="-28.19254296875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657104492" Y="-27.870150390625" />
                  <Point X="1.808835327148" Y="-27.849630859375" />
                  <Point X="1.783253417969" Y="-27.828005859375" />
                  <Point X="1.773299560547" Y="-27.820646484375" />
                  <Point X="1.722848510742" Y="-27.7882109375" />
                  <Point X="1.560176025391" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517470092773" Y="-27.663873046875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926513672" Y="-27.65115234375" />
                  <Point X="1.455385253906" Y="-27.6486953125" />
                  <Point X="1.424121337891" Y="-27.64637890625" />
                  <Point X="1.408398681641" Y="-27.64651953125" />
                  <Point X="1.353222167969" Y="-27.651595703125" />
                  <Point X="1.175311645508" Y="-27.667966796875" />
                  <Point X="1.161228637695" Y="-27.67033984375" />
                  <Point X="1.133576171875" Y="-27.677171875" />
                  <Point X="1.120006713867" Y="-27.681630859375" />
                  <Point X="1.09262121582" Y="-27.692974609375" />
                  <Point X="1.07987512207" Y="-27.699416015625" />
                  <Point X="1.055493408203" Y="-27.71413671875" />
                  <Point X="1.043857910156" Y="-27.722416015625" />
                  <Point X="1.001251831055" Y="-27.757841796875" />
                  <Point X="0.863874023438" Y="-27.872068359375" />
                  <Point X="0.85265435791" Y="-27.88308984375" />
                  <Point X="0.832184082031" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.770053161621" Y="-28.064240234375" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091491699" Y="-29.15233984375" />
                  <Point X="0.749890686035" Y="-28.84183203125" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146484375" Y="-28.453583984375" />
                  <Point X="0.626787841797" Y="-28.42381640625" />
                  <Point X="0.620406982422" Y="-28.41320703125" />
                  <Point X="0.581649230957" Y="-28.357365234375" />
                  <Point X="0.456679229736" Y="-28.177306640625" />
                  <Point X="0.446668884277" Y="-28.165169921875" />
                  <Point X="0.424786102295" Y="-28.142716796875" />
                  <Point X="0.412913513184" Y="-28.132400390625" />
                  <Point X="0.38665838623" Y="-28.11315625" />
                  <Point X="0.373243682861" Y="-28.1049375" />
                  <Point X="0.345240600586" Y="-28.090828125" />
                  <Point X="0.330652374268" Y="-28.0849375" />
                  <Point X="0.270677093506" Y="-28.06632421875" />
                  <Point X="0.077293281555" Y="-28.0063046875" />
                  <Point X="0.063376678467" Y="-28.003109375" />
                  <Point X="0.035218582153" Y="-27.99883984375" />
                  <Point X="0.020976791382" Y="-27.997765625" />
                  <Point X="-0.008664410591" Y="-27.997765625" />
                  <Point X="-0.022904714584" Y="-27.99883984375" />
                  <Point X="-0.05106578064" Y="-28.003109375" />
                  <Point X="-0.06498639679" Y="-28.0063046875" />
                  <Point X="-0.124961685181" Y="-28.024919921875" />
                  <Point X="-0.318345489502" Y="-28.0849375" />
                  <Point X="-0.332930297852" Y="-28.090828125" />
                  <Point X="-0.360929962158" Y="-28.104935546875" />
                  <Point X="-0.374344970703" Y="-28.11315234375" />
                  <Point X="-0.400600402832" Y="-28.132396484375" />
                  <Point X="-0.41247668457" Y="-28.142716796875" />
                  <Point X="-0.434360961914" Y="-28.165173828125" />
                  <Point X="-0.44436907959" Y="-28.177310546875" />
                  <Point X="-0.483126678467" Y="-28.233154296875" />
                  <Point X="-0.608096679688" Y="-28.4132109375" />
                  <Point X="-0.612471130371" Y="-28.4201328125" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.948508361816" Y="-29.62903125" />
                  <Point X="-0.985424804688" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.131321789579" Y="-29.655947009917" />
                  <Point X="-0.960530399308" Y="-29.673897908381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119927690239" Y="-29.561621291456" />
                  <Point X="-0.935636061044" Y="-29.580991122208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.129825600412" Y="-29.465057692618" />
                  <Point X="-0.910741785463" Y="-29.488084329447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.149232028071" Y="-29.367494708321" />
                  <Point X="-0.885847509883" Y="-29.395177536686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.567039372349" Y="-29.122953865088" />
                  <Point X="-2.479899813051" Y="-29.13211260183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.168638461507" Y="-29.269931723417" />
                  <Point X="-0.860953234302" Y="-29.302270743925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751664727629" Y="-29.008025671752" />
                  <Point X="-2.412073085614" Y="-29.043718191589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.190518891712" Y="-29.172108710975" />
                  <Point X="-0.836058958721" Y="-29.209363951164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.895338670545" Y="-28.897401645296" />
                  <Point X="-2.309671857294" Y="-28.958957707823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.250881196234" Y="-29.07024109056" />
                  <Point X="-0.81116468314" Y="-29.116457158403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959819356269" Y="-28.795101165575" />
                  <Point X="-2.186142025974" Y="-28.876417929718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.372586598362" Y="-28.961926050784" />
                  <Point X="-0.786270407559" Y="-29.023550365642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.907824194305" Y="-28.705042790752" />
                  <Point X="-2.052207359162" Y="-28.79497174389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.496781927019" Y="-28.853349309183" />
                  <Point X="-0.761376131978" Y="-28.930643572881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818172248703" Y="-29.096660797497" />
                  <Point X="0.82586768541" Y="-29.097469620487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.85582903234" Y="-28.61498441593" />
                  <Point X="-0.736481856397" Y="-28.83773678012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.79183498091" Y="-28.998369352548" />
                  <Point X="0.813115341289" Y="-29.000606008552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803833870376" Y="-28.524926041107" />
                  <Point X="-0.711587580816" Y="-28.744829987358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765497713117" Y="-28.900077907599" />
                  <Point X="0.800362997168" Y="-28.903742396617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751838708412" Y="-28.434867666284" />
                  <Point X="-0.686693305235" Y="-28.651923194597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.739160572102" Y="-28.801786475975" />
                  <Point X="0.787610653046" Y="-28.806878784682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.702295603995" Y="-29.008120282226" />
                  <Point X="2.726013780585" Y="-29.010613163038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.699843546447" Y="-28.344809291461" />
                  <Point X="-0.661799029654" Y="-28.559016401836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.712823615483" Y="-28.703495063732" />
                  <Point X="0.774858308925" Y="-28.710015172746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.622567943924" Y="-28.904217280926" />
                  <Point X="2.66730062965" Y="-28.908918875651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.647848384483" Y="-28.254750916639" />
                  <Point X="-0.63469424097" Y="-28.466341943364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.686486658864" Y="-28.605203651489" />
                  <Point X="0.762105964804" Y="-28.613151560811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.542840283852" Y="-28.800314279626" />
                  <Point X="2.608587325921" Y="-28.807224572204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.595853222518" Y="-28.164692541816" />
                  <Point X="-0.582482678794" Y="-28.37630631312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.660149702245" Y="-28.506912239246" />
                  <Point X="0.749353620682" Y="-28.516287948876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.463112623781" Y="-28.696411278326" />
                  <Point X="2.549874004794" Y="-28.705530266928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.854621858647" Y="-27.93686734038" />
                  <Point X="-3.68410397447" Y="-27.954789492196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.543858060554" Y="-28.074634166993" />
                  <Point X="-0.520691429256" Y="-28.287277548591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.61591849791" Y="-28.406740065781" />
                  <Point X="0.736601276561" Y="-28.419424336941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.383384963709" Y="-28.592508277026" />
                  <Point X="2.491160683666" Y="-28.603835961653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.933652303335" Y="-27.83303761937" />
                  <Point X="-3.544133915905" Y="-27.873977651603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.491862954029" Y="-27.984575786343" />
                  <Point X="-0.458900925333" Y="-28.198248705694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.544402905618" Y="-28.303700187585" />
                  <Point X="0.724741915149" Y="-28.32265458127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.303657303638" Y="-28.488605275726" />
                  <Point X="2.432447362539" Y="-28.502141656377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.010680912466" Y="-27.729418299756" />
                  <Point X="-3.404163903748" Y="-27.793165806132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.439867903581" Y="-27.8945173998" />
                  <Point X="-0.372199332573" Y="-28.111838123738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.472887932024" Y="-28.200660374417" />
                  <Point X="0.734426662569" Y="-28.228149202683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.223929643566" Y="-28.384702274427" />
                  <Point X="2.373734041412" Y="-28.400447351102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.071456259574" Y="-27.627507266816" />
                  <Point X="-3.264193891591" Y="-27.712353960662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.387872853133" Y="-27.804459013256" />
                  <Point X="-0.16679647675" Y="-28.037903547263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.347479335199" Y="-28.091956113193" />
                  <Point X="0.754725439533" Y="-28.134759403554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.144201983494" Y="-28.280799273127" />
                  <Point X="2.315020720285" Y="-28.298753045826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.132231801745" Y="-27.525596213375" />
                  <Point X="-3.124223879434" Y="-27.631542115191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.336844598886" Y="-27.714299012337" />
                  <Point X="0.775024266103" Y="-28.041369609639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.064474381713" Y="-28.176896277953" />
                  <Point X="2.256307399157" Y="-28.197058740551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.160110810798" Y="-27.42714272489" />
                  <Point X="-2.984253867277" Y="-27.550730269721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311888838504" Y="-27.621398681889" />
                  <Point X="0.805228151435" Y="-27.94902087935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.984747108725" Y="-28.072993317338" />
                  <Point X="2.19759407803" Y="-28.095364435275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.050619843265" Y="-27.343127402742" />
                  <Point X="-2.84428385512" Y="-27.46991842425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.325034642462" Y="-27.524493715659" />
                  <Point X="0.877120957436" Y="-27.861053831188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.905019835737" Y="-27.969090356722" />
                  <Point X="2.138880756903" Y="-27.99367013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.941128875733" Y="-27.259112080595" />
                  <Point X="-2.704313842963" Y="-27.389106578779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.392123250335" Y="-27.421919132275" />
                  <Point X="0.979112356756" Y="-27.776250272658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.823701313626" Y="-27.865020149084" />
                  <Point X="2.080167435776" Y="-27.891975824724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.8316379082" Y="-27.175096758447" />
                  <Point X="1.093232314913" Y="-27.69272147703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.668340516561" Y="-27.75316778476" />
                  <Point X="2.040499928847" Y="-27.792283315185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.722146940668" Y="-27.0910814363" />
                  <Point X="2.022914691191" Y="-27.694911745671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.612655973135" Y="-27.007066114152" />
                  <Point X="2.005329718072" Y="-27.59754020396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.503165005602" Y="-26.923050792005" />
                  <Point X="2.003780567034" Y="-27.501854095066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.898668573719" Y="-27.701014849923" />
                  <Point X="4.033026498389" Y="-27.715136436847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.39367403807" Y="-26.839035469857" />
                  <Point X="2.034486777986" Y="-27.409558161328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696394039828" Y="-27.584231653166" />
                  <Point X="4.080102729553" Y="-27.624561061565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.284182991451" Y="-26.755020156022" />
                  <Point X="2.08212508476" Y="-27.319041862572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.494119505937" Y="-27.467448456409" />
                  <Point X="3.935858368997" Y="-27.513877081798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.174691904104" Y="-26.671004846468" />
                  <Point X="2.131977032564" Y="-27.228758226864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.291844972045" Y="-27.350665259652" />
                  <Point X="3.791611986497" Y="-27.403192889517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.065200816757" Y="-26.586989536913" />
                  <Point X="2.225804291533" Y="-27.143096582607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.089570441308" Y="-27.233882063227" />
                  <Point X="3.647365603997" Y="-27.292508697236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.985947506409" Y="-26.499796108931" />
                  <Point X="2.377093996573" Y="-27.0634744848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887295915728" Y="-27.117098867344" />
                  <Point X="3.503119221498" Y="-27.181824504955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676267300514" Y="-26.226613053058" />
                  <Point X="-4.413961496129" Y="-26.254182504034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.952989060306" Y="-26.407736894645" />
                  <Point X="3.358872838998" Y="-27.071140312674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.701340083063" Y="-26.128454510864" />
                  <Point X="-4.010495781658" Y="-26.201065172851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950890450267" Y="-26.31243418089" />
                  <Point X="3.214626456498" Y="-26.960456120393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726412865612" Y="-26.03029596867" />
                  <Point X="-3.607030067186" Y="-26.147947841668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.993540391931" Y="-26.212428204829" />
                  <Point X="3.070380073999" Y="-26.849771928112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74669461939" Y="-25.932640983891" />
                  <Point X="2.976940624994" Y="-26.744427759723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760564902199" Y="-25.835659871865" />
                  <Point X="2.91107703429" Y="-26.641981930831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.774435351" Y="-25.738678742392" />
                  <Point X="2.866923527927" Y="-26.541817923752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752232675515" Y="-25.645489051061" />
                  <Point X="2.861440992706" Y="-26.445718399522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.496176193415" Y="-25.576878385239" />
                  <Point X="2.870146759648" Y="-26.35111012594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.240119711315" Y="-25.508267719416" />
                  <Point X="2.883844692734" Y="-26.257026550163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.984063229215" Y="-25.439657053593" />
                  <Point X="2.928875199343" Y="-26.166236160566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.728005402087" Y="-25.371046529139" />
                  <Point X="3.001803093524" Y="-26.078377904555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.479819703873" Y="-25.301608610595" />
                  <Point X="3.087537423573" Y="-25.991865659192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.362853236218" Y="-25.218378995171" />
                  <Point X="3.318130871726" Y="-25.920578720659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.333180550007" Y="-26.027264740851" />
                  <Point X="4.730136799913" Y="-26.068986523931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.312368176466" Y="-25.12816190221" />
                  <Point X="4.75142486836" Y="-25.975700703527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.294719103023" Y="-25.034493608019" />
                  <Point X="4.769005390555" Y="-25.88202520431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.311297881485" Y="-24.937227821628" />
                  <Point X="4.783181917387" Y="-25.787991930762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.371093910495" Y="-24.835419719169" />
                  <Point X="4.26839379518" Y="-25.638362232295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.661256749025" Y="-24.709399089364" />
                  <Point X="3.684341976969" Y="-25.481452626028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.24785031431" Y="-24.552222334715" />
                  <Point X="3.490806284496" Y="-25.365587918516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784283293809" Y="-24.400317670075" />
                  <Point X="3.407538197155" Y="-25.261312803315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770364802936" Y="-24.306257275855" />
                  <Point X="3.368197801405" Y="-25.161654674546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.756446312064" Y="-24.212196881636" />
                  <Point X="3.350436551652" Y="-25.064264605415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.742527821192" Y="-24.118136487417" />
                  <Point X="3.355112153984" Y="-24.969232744464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726409084249" Y="-24.024307348378" />
                  <Point X="3.374073015225" Y="-24.875702324726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.701240882648" Y="-23.931429346402" />
                  <Point X="3.418581612715" Y="-24.78485708027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676072609371" Y="-23.838551351959" />
                  <Point X="3.491114145296" Y="-24.69695727008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.650904325582" Y="-23.745673358621" />
                  <Point X="3.613846125373" Y="-24.61433363443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.599632560929" Y="-23.760643186943" />
                  <Point X="3.830369186014" Y="-24.541567838578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.49139380665" Y="-23.676496251879" />
                  <Point X="4.086425109592" Y="-24.472957114052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.445710246003" Y="-23.585774501026" />
                  <Point X="4.342482065284" Y="-24.404346498006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422009412099" Y="-23.49274227249" />
                  <Point X="4.598539020976" Y="-24.33573588196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435917802986" Y="-23.395757155144" />
                  <Point X="4.779694303855" Y="-24.259252782873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.486685376792" Y="-23.294897981564" />
                  <Point X="3.252526118601" Y="-24.003217652081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.576257286883" Y="-24.037243168955" />
                  <Point X="4.764574289897" Y="-24.16214031881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.59975605445" Y="-23.187490487899" />
                  <Point X="3.107136732717" Y="-23.892413325303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979724432912" Y="-23.984125988235" />
                  <Point X="4.747587303953" Y="-24.064831628084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.744002224514" Y="-23.076806317946" />
                  <Point X="3.043514342026" Y="-23.790203056026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.38319157894" Y="-23.931008807514" />
                  <Point X="4.723722054343" Y="-23.966800002716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.888248394578" Y="-22.966122147993" />
                  <Point X="3.013803899242" Y="-23.691557076099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.032494564642" Y="-22.85543797804" />
                  <Point X="3.001161790977" Y="-23.594705050419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.176740734707" Y="-22.744753808087" />
                  <Point X="3.017941817658" Y="-23.500945415732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.193608381471" Y="-22.647457660414" />
                  <Point X="3.051201990321" Y="-23.408917914186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.141075050327" Y="-22.557455849452" />
                  <Point X="3.113716662944" Y="-23.319965184487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.088541719182" Y="-22.467454038489" />
                  <Point X="3.202193608316" Y="-23.23374119961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036008388038" Y="-22.377452227526" />
                  <Point X="3.311683809834" Y="-23.149725796951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.977683939694" Y="-22.288059087508" />
                  <Point X="2.171499113473" Y="-22.93436426982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.617093471034" Y="-22.98119812401" />
                  <Point X="3.421174011351" Y="-23.065710394292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.908986358224" Y="-22.199756207715" />
                  <Point X="2.0770991473" Y="-22.828919147008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.759792397565" Y="-22.900673098998" />
                  <Point X="3.53066525755" Y="-22.981695101434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.84028740263" Y="-22.111453472348" />
                  <Point X="-3.390826851213" Y="-22.158693679886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884447056243" Y="-22.211916340991" />
                  <Point X="2.028009706533" Y="-22.728236352318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.899762720598" Y="-22.819861286202" />
                  <Point X="3.640156741755" Y="-22.897679833591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.771588447036" Y="-22.02315073698" />
                  <Point X="-3.593100664362" Y="-22.041910558882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.800587082071" Y="-22.125207092887" />
                  <Point X="2.014314386906" Y="-22.631273629663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.039733043631" Y="-22.739049473406" />
                  <Point X="3.74964822596" Y="-22.813664565748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755850707401" Y="-22.034385788777" />
                  <Point X="2.02070615472" Y="-22.536422144973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.179703366664" Y="-22.658237660609" />
                  <Point X="3.859139710165" Y="-22.729649297905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750209901667" Y="-21.939455374791" />
                  <Point X="2.047208701922" Y="-22.443684388371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.319673689697" Y="-22.577425847813" />
                  <Point X="3.96863119437" Y="-22.645634030062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761129428904" Y="-21.842784399673" />
                  <Point X="2.093253368725" Y="-22.353000591304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.45964401273" Y="-22.496614035017" />
                  <Point X="4.078122678575" Y="-22.561618762219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803798193646" Y="-21.742776445226" />
                  <Point X="2.145248609796" Y="-22.262942224796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.599614335763" Y="-22.41580222222" />
                  <Point X="4.108284255317" Y="-22.469265585118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.862511520876" Y="-21.641082139309" />
                  <Point X="2.197243850867" Y="-22.172883858288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.739584658796" Y="-22.334990409424" />
                  <Point X="4.046553345372" Y="-22.367254118477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921224848106" Y="-21.539387833392" />
                  <Point X="2.249239091938" Y="-22.082825491779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879554357271" Y="-22.254178530984" />
                  <Point X="3.9727287245" Y="-22.263971551598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.979938175336" Y="-21.437693527475" />
                  <Point X="2.301234303771" Y="-21.992767122198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.038651502566" Y="-21.335999221558" />
                  <Point X="2.353229497275" Y="-21.90270875069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988513252179" Y="-21.245745677464" />
                  <Point X="2.40522469078" Y="-21.812650379183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.878942262727" Y="-21.16173876596" />
                  <Point X="2.457219884284" Y="-21.722592007675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.769371273276" Y="-21.077731854455" />
                  <Point X="2.509215077789" Y="-21.632533636167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.659800283824" Y="-20.99372494295" />
                  <Point X="2.561210271293" Y="-21.542475264659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.518868448293" Y="-20.913014189189" />
                  <Point X="2.613205464798" Y="-21.452416893152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.374286562901" Y="-20.832687071128" />
                  <Point X="-2.08369216993" Y="-20.863229772574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.637572585023" Y="-20.910118830383" />
                  <Point X="2.665200658302" Y="-21.362358521644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.229704678555" Y="-20.752359952957" />
                  <Point X="-2.172022210936" Y="-20.758422624604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.536267040735" Y="-20.825243185584" />
                  <Point X="2.717195851807" Y="-21.272300150136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.495835805945" Y="-20.733969393039" />
                  <Point X="-0.12020306542" Y="-20.878554220239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.084819141686" Y="-20.900102922529" />
                  <Point X="2.769191045312" Y="-21.182241778628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468258137744" Y="-20.641344636208" />
                  <Point X="-0.211108835956" Y="-20.773476352187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.197861698176" Y="-20.816460887423" />
                  <Point X="2.807452415596" Y="-21.090739924134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.464939038979" Y="-20.546170200987" />
                  <Point X="-0.242719288384" Y="-20.674630673199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.241397317774" Y="-20.725513378869" />
                  <Point X="2.649835045321" Y="-20.978650384408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.47769142708" Y="-20.449306584429" />
                  <Point X="-0.269056278057" Y="-20.576339257482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.266291609289" Y="-20.632606587783" />
                  <Point X="2.455273165223" Y="-20.86267782023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.262052835016" Y="-20.376447827183" />
                  <Point X="-0.29539333586" Y="-20.478047834604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.291185900805" Y="-20.539699796696" />
                  <Point X="2.255220583605" Y="-20.746128160067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.014239102817" Y="-20.306970813436" />
                  <Point X="-0.321730400352" Y="-20.379756411023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.316080192321" Y="-20.44679300561" />
                  <Point X="1.993278197863" Y="-20.623073619372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.645315564654" Y="-20.250222953227" />
                  <Point X="-0.348067464845" Y="-20.281464987442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.340974483836" Y="-20.353886214524" />
                  <Point X="1.677472622245" Y="-20.494357829295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.365868775352" Y="-20.260979423438" />
                  <Point X="1.175921843637" Y="-20.346119431704" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.566364807129" Y="-28.8910078125" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.425561096191" Y="-28.465701171875" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.274335906982" Y="-28.2663984375" />
                  <Point X="0.214360549927" Y="-28.24778515625" />
                  <Point X="0.02097677803" Y="-28.187765625" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.068639648438" Y="-28.206380859375" />
                  <Point X="-0.2620234375" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.327036712646" Y="-28.341486328125" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.76498260498" Y="-29.67820703125" />
                  <Point X="-0.84774420166" Y="-29.987078125" />
                  <Point X="-0.872937561035" Y="-29.9821875" />
                  <Point X="-1.10023046875" Y="-29.938068359375" />
                  <Point X="-1.168784790039" Y="-29.9204296875" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.320565063477" Y="-29.637744140625" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.324011352539" Y="-29.4627265625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.441500244141" Y="-29.154205078125" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240722656" Y="-28.985763671875" />
                  <Point X="-1.722526855469" Y="-28.9809609375" />
                  <Point X="-1.958829833984" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.050944335938" Y="-29.01459375" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.431392089844" Y="-29.38100390625" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.522645263672" Y="-29.373912109375" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-2.950740966797" Y="-29.094537109375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.682256835938" Y="-27.93434765625" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499763183594" Y="-27.597587890625" />
                  <Point X="-2.513984863281" Y="-27.5687578125" />
                  <Point X="-2.531327880859" Y="-27.551416015625" />
                  <Point X="-2.560156982422" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.575855712891" Y="-28.11168359375" />
                  <Point X="-3.842958740234" Y="-28.26589453125" />
                  <Point X="-3.898466552734" Y="-28.19296875" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.229743652344" Y="-27.733037109375" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.472119873047" Y="-26.65973828125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535644531" Y="-26.411083984375" />
                  <Point X="-3.143998291016" Y="-26.38897265625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.136651611328" Y="-26.350052734375" />
                  <Point X="-3.148656982422" Y="-26.321068359375" />
                  <Point X="-3.167428710938" Y="-26.30694921875" />
                  <Point X="-3.187642333984" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.461952636719" Y="-26.452140625" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.824439453125" Y="-26.414251953125" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.94539453125" Y="-25.885328125" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.908701660156" Y="-25.222759765625" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.535323242188" Y="-25.116861328125" />
                  <Point X="-3.51414453125" Y="-25.1021640625" />
                  <Point X="-3.502325195312" Y="-25.090646484375" />
                  <Point X="-3.492709228516" Y="-25.0688515625" />
                  <Point X="-3.485648193359" Y="-25.0461015625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.487837646484" Y="-25.009404296875" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.502325927734" Y="-24.971912109375" />
                  <Point X="-3.520718505859" Y="-24.955833984375" />
                  <Point X="-3.54189453125" Y="-24.94113671875" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.6899921875" Y="-24.630455078125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.983997558594" Y="-24.451984375" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.881407226562" Y="-23.869853515625" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.005041992188" Y="-23.572873046875" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731704345703" Y="-23.6041328125" />
                  <Point X="-3.717163574219" Y="-23.599548828125" />
                  <Point X="-3.670278320312" Y="-23.584765625" />
                  <Point X="-3.651534423828" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.633285644531" Y="-23.54212890625" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.623355957031" Y="-23.44096484375" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.309589355469" Y="-22.882306640625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.4178359375" Y="-22.654705078125" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.0640234375" Y="-22.089611328125" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.313790771484" Y="-21.983779296875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514892578" Y="-22.07956640625" />
                  <Point X="-3.118263671875" Y="-22.081337890625" />
                  <Point X="-3.052965576172" Y="-22.08705078125" />
                  <Point X="-3.031507080078" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.998878417969" Y="-22.058220703125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.939845947266" Y="-21.951908203125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.239934082031" Y="-21.3673671875" />
                  <Point X="-3.307278808594" Y="-21.25072265625" />
                  <Point X="-3.201919921875" Y="-21.1699453125" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.601693115234" Y="-20.741673828125" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.015164794922" Y="-20.650734375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247070312" Y="-20.726337890625" />
                  <Point X="-1.928707519531" Y="-20.738072265625" />
                  <Point X="-1.856031005859" Y="-20.77590625" />
                  <Point X="-1.835124145508" Y="-20.781509765625" />
                  <Point X="-1.81380859375" Y="-20.77775" />
                  <Point X="-1.79033203125" Y="-20.768025390625" />
                  <Point X="-1.714634643555" Y="-20.736669921875" />
                  <Point X="-1.696905151367" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.678442138672" Y="-20.68127734375" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.684644897461" Y="-20.332986328125" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.549409057617" Y="-20.259685546875" />
                  <Point X="-0.968083068848" Y="-20.096703125" />
                  <Point X="-0.78481640625" Y="-20.07525390625" />
                  <Point X="-0.224199996948" Y="-20.009640625" />
                  <Point X="-0.087885253906" Y="-20.518375" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282119751" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594039917" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.201951828003" Y="-20.13862109375" />
                  <Point X="0.236648345947" Y="-20.0091328125" />
                  <Point X="0.352633056641" Y="-20.021279296875" />
                  <Point X="0.860210083008" Y="-20.074435546875" />
                  <Point X="1.01183770752" Y="-20.11104296875" />
                  <Point X="1.508456176758" Y="-20.230943359375" />
                  <Point X="1.606994140625" Y="-20.26668359375" />
                  <Point X="1.931044067383" Y="-20.38421875" />
                  <Point X="2.026470947266" Y="-20.428845703125" />
                  <Point X="2.338685302734" Y="-20.574859375" />
                  <Point X="2.430900878906" Y="-20.628583984375" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.819477294922" Y="-20.866146484375" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.433818359375" Y="-22.143125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.219769775391" Y="-22.5274921875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.204026367188" Y="-22.624109375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.228851074219" Y="-22.714173828125" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.289926025391" Y="-22.785966796875" />
                  <Point X="2.338248291016" Y="-22.818755859375" />
                  <Point X="2.360336181641" Y="-22.827021484375" />
                  <Point X="2.376770507812" Y="-22.829001953125" />
                  <Point X="2.429760986328" Y="-22.835392578125" />
                  <Point X="2.467670654297" Y="-22.82897265625" />
                  <Point X="2.528951660156" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.680139404297" Y="-22.149919921875" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.023679443359" Y="-22.00947265625" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.251067382812" Y="-22.3382265625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.557154785156" Y="-23.200859375" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.265693115234" Y="-23.43401171875" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.208024169922" Y="-23.52671875" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.194962158203" Y="-23.629306640625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712046875" />
                  <Point X="3.227022705078" Y="-23.729337890625" />
                  <Point X="3.263704101562" Y="-23.785091796875" />
                  <Point X="3.280947021484" Y="-23.8011796875" />
                  <Point X="3.297432617188" Y="-23.810458984375" />
                  <Point X="3.350589111328" Y="-23.8403828125" />
                  <Point X="3.368565917969" Y="-23.846380859375" />
                  <Point X="3.390855712891" Y="-23.849326171875" />
                  <Point X="3.4627265625" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.557877929688" Y="-23.71637109375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.862346679688" Y="-23.73297265625" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.954464355469" Y="-24.14673046875" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.048918945312" Y="-24.6797109375" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.729086914062" Y="-24.767181640625" />
                  <Point X="3.707187988281" Y="-24.779837890625" />
                  <Point X="3.636576904297" Y="-24.82065234375" />
                  <Point X="3.622265136719" Y="-24.833072265625" />
                  <Point X="3.609125732422" Y="-24.84981640625" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.552605224609" Y="-24.948134765625" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.542863037109" Y="-25.0635546875" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.579898193359" Y="-25.1755" />
                  <Point X="3.622264648438" Y="-25.229486328125" />
                  <Point X="3.636576904297" Y="-25.241908203125" />
                  <Point X="3.658476074219" Y="-25.25456640625" />
                  <Point X="3.729086914062" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.733479003906" Y="-25.56627734375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.991336914062" Y="-25.6818203125" />
                  <Point X="4.948431640625" Y="-25.966400390625" />
                  <Point X="4.928860351562" Y="-26.0521640625" />
                  <Point X="4.874545898438" Y="-26.290177734375" />
                  <Point X="3.768886962891" Y="-26.144615234375" />
                  <Point X="3.411981933594" Y="-26.09762890625" />
                  <Point X="3.394836425781" Y="-26.098341796875" />
                  <Point X="3.351856445312" Y="-26.10768359375" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.159466552734" Y="-26.18594140625" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.060634521484" Y="-26.354533203125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.080146240234" Y="-26.553619140625" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.089331542969" Y="-27.392150390625" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.325077148438" Y="-27.60643359375" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.16365625" Y="-27.85965234375" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.071326660156" Y="-27.442740234375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.6861875" Y="-27.244076171875" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.446582275391" Y="-27.241611328125" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.266234619141" Y="-27.3771796875" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.198401123047" Y="-27.597529296875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.82584375" Y="-28.8035234375" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.978810058594" Y="-29.087705078125" />
                  <Point X="2.835294189453" Y="-29.19021484375" />
                  <Point X="2.790046142578" Y="-29.21950390625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.925743408203" Y="-28.30820703125" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670548950195" Y="-27.980466796875" />
                  <Point X="1.620098266602" Y="-27.94803125" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.370628540039" Y="-27.840794921875" />
                  <Point X="1.192718139648" Y="-27.857166015625" />
                  <Point X="1.165332641602" Y="-27.868509765625" />
                  <Point X="1.122726806641" Y="-27.903935546875" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.955717895508" Y="-28.104595703125" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.081627441406" Y="-29.58451171875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="0.994366638184" Y="-29.9632421875" />
                  <Point X="0.952538696289" Y="-29.970841796875" />
                  <Point X="0.860200500488" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#140" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.045744944593" Y="4.525846939695" Z="0.6" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.6" />
                  <Point X="-0.796784583478" Y="5.005687487782" Z="0.6" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.6" />
                  <Point X="-1.569062786887" Y="4.819739703738" Z="0.6" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.6" />
                  <Point X="-1.740373088827" Y="4.691768543442" Z="0.6" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.6" />
                  <Point X="-1.732283985967" Y="4.365038704621" Z="0.6" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.6" />
                  <Point X="-1.815622555934" Y="4.309448942664" Z="0.6" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.6" />
                  <Point X="-1.911775591075" Y="4.337557860966" Z="0.6" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.6" />
                  <Point X="-1.98165327218" Y="4.410983523897" Z="0.6" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.6" />
                  <Point X="-2.632132117099" Y="4.333312995704" Z="0.6" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.6" />
                  <Point X="-3.238498003977" Y="3.901014203538" Z="0.6" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.6" />
                  <Point X="-3.289391418594" Y="3.638912686505" Z="0.6" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.6" />
                  <Point X="-2.995811788059" Y="3.075014839457" Z="0.6" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.6" />
                  <Point X="-3.040389049484" Y="3.008414518402" Z="0.6" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.6" />
                  <Point X="-3.12006162615" Y="2.999752911114" Z="0.6" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.6" />
                  <Point X="-3.294946661792" Y="3.090802530152" Z="0.6" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.6" />
                  <Point X="-4.109642638554" Y="2.972372117175" Z="0.6" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.6" />
                  <Point X="-4.46717423343" Y="2.401781179442" Z="0.6" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.6" />
                  <Point X="-4.346183281299" Y="2.109305784195" Z="0.6" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.6" />
                  <Point X="-3.67386246651" Y="1.567228160404" Z="0.6" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.6" />
                  <Point X="-3.685635345915" Y="1.508285989684" Z="0.6" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.6" />
                  <Point X="-3.738355240003" Y="1.479417236516" Z="0.6" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.6" />
                  <Point X="-4.004671927293" Y="1.507979465187" Z="0.6" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.6" />
                  <Point X="-4.935823105899" Y="1.174504172982" Z="0.6" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.6" />
                  <Point X="-5.039614891863" Y="0.58661383404" Z="0.6" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.6" />
                  <Point X="-4.709089184167" Y="0.352529119912" Z="0.6" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.6" />
                  <Point X="-3.555377533504" Y="0.034366680047" Z="0.6" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.6" />
                  <Point X="-3.541746670701" Y="0.007055915643" Z="0.6" />
                  <Point X="-3.539556741714" Y="0" Z="0.6" />
                  <Point X="-3.546617930434" Y="-0.022751035418" Z="0.6" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.6" />
                  <Point X="-3.569991061619" Y="-0.044509303087" Z="0.6" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.6" />
                  <Point X="-3.927798604606" Y="-0.143182936844" Z="0.6" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.6" />
                  <Point X="-5.001046697734" Y="-0.861125016538" Z="0.6" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.6" />
                  <Point X="-4.879040117186" Y="-1.395345540679" Z="0.6" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.6" />
                  <Point X="-4.461582886387" Y="-1.470431499045" Z="0.6" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.6" />
                  <Point X="-3.198945275209" Y="-1.318760160742" Z="0.6" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.6" />
                  <Point X="-3.198556990103" Y="-1.345155359404" Z="0.6" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.6" />
                  <Point X="-3.508713908788" Y="-1.588789474277" Z="0.6" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.6" />
                  <Point X="-4.278843160736" Y="-2.727365846712" Z="0.6" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.6" />
                  <Point X="-3.944408347177" Y="-3.1919722848" Z="0.6" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.6" />
                  <Point X="-3.557011647708" Y="-3.123703017983" Z="0.6" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.6" />
                  <Point X="-2.559597722396" Y="-2.568732746736" Z="0.6" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.6" />
                  <Point X="-2.731713995185" Y="-2.878066734784" Z="0.6" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.6" />
                  <Point X="-2.987400836144" Y="-4.102872883653" Z="0.6" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.6" />
                  <Point X="-2.555122731483" Y="-4.385144269316" Z="0.6" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.6" />
                  <Point X="-2.39788038955" Y="-4.380161308895" Z="0.6" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.6" />
                  <Point X="-2.029321897291" Y="-4.024887358065" Z="0.6" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.6" />
                  <Point X="-1.73195293951" Y="-3.999572505238" Z="0.6" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.6" />
                  <Point X="-1.480623603328" Y="-4.160515883274" Z="0.6" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.6" />
                  <Point X="-1.379206620598" Y="-4.441200149121" Z="0.6" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.6" />
                  <Point X="-1.376293318593" Y="-4.599936081094" Z="0.6" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.6" />
                  <Point X="-1.187399423128" Y="-4.937573802288" Z="0.6" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.6" />
                  <Point X="-0.888621198187" Y="-4.999990700724" Z="0.6" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.6" />
                  <Point X="-0.722842284261" Y="-4.659868236804" Z="0.6" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.6" />
                  <Point X="-0.292116778606" Y="-3.338715132313" Z="0.6" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.6" />
                  <Point X="-0.059975281863" Y="-3.222853482634" Z="0.6" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.6" />
                  <Point X="0.193383797498" Y="-3.264258562654" Z="0.6" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.6" />
                  <Point X="0.378329080827" Y="-3.462930623455" Z="0.6" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.6" />
                  <Point X="0.511912576141" Y="-3.872667793011" Z="0.6" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.6" />
                  <Point X="0.955319495944" Y="-4.988756712448" Z="0.6" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.6" />
                  <Point X="1.134669874037" Y="-4.951045646084" Z="0.6" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.6" />
                  <Point X="1.125043783872" Y="-4.546706316799" Z="0.6" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.6" />
                  <Point X="0.998421085363" Y="-3.083935087217" Z="0.6" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.6" />
                  <Point X="1.148537915705" Y="-2.91110079488" Z="0.6" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.6" />
                  <Point X="1.369054060549" Y="-2.859303971334" Z="0.6" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.6" />
                  <Point X="1.586903214758" Y="-2.958810142491" Z="0.6" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.6" />
                  <Point X="1.879919658255" Y="-3.307363059802" Z="0.6" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.6" />
                  <Point X="2.81105937212" Y="-4.230197569533" Z="0.6" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.6" />
                  <Point X="3.001715851659" Y="-4.097112251915" Z="0.6" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.6" />
                  <Point X="2.862988969318" Y="-3.747242848765" Z="0.6" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.6" />
                  <Point X="2.241449315031" Y="-2.557361213372" Z="0.6" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.6" />
                  <Point X="2.304325063252" Y="-2.369185779704" Z="0.6" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.6" />
                  <Point X="2.463712598668" Y="-2.254576246715" Z="0.6" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.6" />
                  <Point X="2.67114559014" Y="-2.26199869446" Z="0.6" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.6" />
                  <Point X="3.040170748342" Y="-2.454760530956" Z="0.6" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.6" />
                  <Point X="4.198389134189" Y="-2.857148380933" Z="0.6" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.6" />
                  <Point X="4.361454874305" Y="-2.601437989508" Z="0.6" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.6" />
                  <Point X="4.113613405915" Y="-2.321201973159" Z="0.6" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.6" />
                  <Point X="3.116048255206" Y="-1.495299666792" Z="0.6" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.6" />
                  <Point X="3.104268092336" Y="-1.327834925502" Z="0.6" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.6" />
                  <Point X="3.191756859995" Y="-1.186628456608" Z="0.6" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.6" />
                  <Point X="3.356319865131" Y="-1.12526230415" Z="0.6" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.6" />
                  <Point X="3.75620495042" Y="-1.16290786747" Z="0.6" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.6" />
                  <Point X="4.971451688861" Y="-1.032007228156" Z="0.6" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.6" />
                  <Point X="5.034622247469" Y="-0.657993373377" Z="0.6" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.6" />
                  <Point X="4.740263723245" Y="-0.486699562078" Z="0.6" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.6" />
                  <Point X="3.677341722367" Y="-0.179996226331" Z="0.6" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.6" />
                  <Point X="3.613076128315" Y="-0.113353220375" Z="0.6" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.6" />
                  <Point X="3.58581452825" Y="-0.022869690964" Z="0.6" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.6" />
                  <Point X="3.595556922174" Y="0.073740840268" Z="0.6" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.6" />
                  <Point X="3.642303310086" Y="0.1505955182" Z="0.6" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.6" />
                  <Point X="3.726053691985" Y="0.208152617911" Z="0.6" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.6" />
                  <Point X="4.055704094061" Y="0.30327237069" Z="0.6" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.6" />
                  <Point X="4.997713292515" Y="0.892241707398" Z="0.6" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.6" />
                  <Point X="4.904771740163" Y="1.31013474821" Z="0.6" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.6" />
                  <Point X="4.545195356283" Y="1.364481864039" Z="0.6" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.6" />
                  <Point X="3.391250407235" Y="1.231522712239" Z="0.6" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.6" />
                  <Point X="3.315804255256" Y="1.26439100797" Z="0.6" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.6" />
                  <Point X="3.262637203432" Y="1.329425068505" Z="0.6" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.6" />
                  <Point X="3.237774638594" Y="1.412078167591" Z="0.6" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.6" />
                  <Point X="3.250020825593" Y="1.491094630085" Z="0.6" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.6" />
                  <Point X="3.299219935494" Y="1.566850780321" Z="0.6" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.6" />
                  <Point X="3.581437114599" Y="1.790752281626" Z="0.6" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.6" />
                  <Point X="4.287688831957" Y="2.718940097594" Z="0.6" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.6" />
                  <Point X="4.058108922492" Y="3.051010866978" Z="0.6" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.6" />
                  <Point X="3.648983490483" Y="2.924661638005" Z="0.6" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.6" />
                  <Point X="2.44859705846" Y="2.25061146709" Z="0.6" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.6" />
                  <Point X="2.376601075812" Y="2.251918857252" Z="0.6" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.6" />
                  <Point X="2.311844541624" Y="2.286689141941" Z="0.6" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.6" />
                  <Point X="2.264069458294" Y="2.345180318758" Z="0.6" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.6" />
                  <Point X="2.247510845507" Y="2.41315736802" Z="0.6" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.6" />
                  <Point X="2.261916475567" Y="2.490872517422" Z="0.6" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.6" />
                  <Point X="2.470963714854" Y="2.863155591472" Z="0.6" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.6" />
                  <Point X="2.842298494292" Y="4.205881881344" Z="0.6" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.6" />
                  <Point X="2.449914766763" Y="4.445900202318" Z="0.6" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.6" />
                  <Point X="2.041496510848" Y="4.647725197997" Z="0.6" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.6" />
                  <Point X="1.617886738469" Y="4.811601465918" Z="0.6" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.6" />
                  <Point X="1.017416207602" Y="4.968840584994" Z="0.6" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.6" />
                  <Point X="0.351684914208" Y="5.059730095877" Z="0.6" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.6" />
                  <Point X="0.14749961696" Y="4.905600566256" Z="0.6" />
                  <Point X="0" Y="4.355124473572" Z="0.6" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>