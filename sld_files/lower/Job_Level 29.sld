<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#173" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2146" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995282226562" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.784295166016" Y="-29.33728125" />
                  <Point X="0.563302001953" Y="-28.5125234375" />
                  <Point X="0.557719726562" Y="-28.497138671875" />
                  <Point X="0.542363098145" Y="-28.467376953125" />
                  <Point X="0.452037628174" Y="-28.337236328125" />
                  <Point X="0.378635437012" Y="-28.2314765625" />
                  <Point X="0.356752746582" Y="-28.2090234375" />
                  <Point X="0.330497711182" Y="-28.189779296875" />
                  <Point X="0.302494659424" Y="-28.175669921875" />
                  <Point X="0.162721191406" Y="-28.132291015625" />
                  <Point X="0.049135520935" Y="-28.097037109375" />
                  <Point X="0.020983409882" Y="-28.092767578125" />
                  <Point X="-0.008658220291" Y="-28.092765625" />
                  <Point X="-0.036824142456" Y="-28.09703515625" />
                  <Point X="-0.176597457886" Y="-28.140416015625" />
                  <Point X="-0.290183258057" Y="-28.17566796875" />
                  <Point X="-0.318184509277" Y="-28.189775390625" />
                  <Point X="-0.344439849854" Y="-28.20901953125" />
                  <Point X="-0.366323608398" Y="-28.2314765625" />
                  <Point X="-0.456649078369" Y="-28.361619140625" />
                  <Point X="-0.530051269531" Y="-28.467376953125" />
                  <Point X="-0.538188781738" Y="-28.481572265625" />
                  <Point X="-0.55099017334" Y="-28.5125234375" />
                  <Point X="-0.730578063965" Y="-29.182755859375" />
                  <Point X="-0.916584960938" Y="-29.87694140625" />
                  <Point X="-0.949240966797" Y="-29.870603515625" />
                  <Point X="-1.079353149414" Y="-29.84534765625" />
                  <Point X="-1.238246337891" Y="-29.804466796875" />
                  <Point X="-1.246418334961" Y="-29.80236328125" />
                  <Point X="-1.241565551758" Y="-29.76550390625" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.249900512695" Y="-29.348353515625" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937744141" Y="-29.182966796875" />
                  <Point X="-1.304009521484" Y="-29.15512890625" />
                  <Point X="-1.323644897461" Y="-29.131203125" />
                  <Point X="-1.452330566406" Y="-29.018349609375" />
                  <Point X="-1.556905761719" Y="-28.926638671875" />
                  <Point X="-1.583188720703" Y="-28.910294921875" />
                  <Point X="-1.612885742188" Y="-28.897994140625" />
                  <Point X="-1.643027709961" Y="-28.890966796875" />
                  <Point X="-1.813822387695" Y="-28.8797734375" />
                  <Point X="-1.952616943359" Y="-28.87067578125" />
                  <Point X="-1.98341418457" Y="-28.873708984375" />
                  <Point X="-2.014463012695" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.184972412109" Y="-28.989892578125" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312789306641" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.435927734375" Y="-29.230861328125" />
                  <Point X="-2.480147216797" Y="-29.28848828125" />
                  <Point X="-2.611014404297" Y="-29.207458984375" />
                  <Point X="-2.801728759766" Y="-29.089375" />
                  <Point X="-3.021744384766" Y="-28.91996875" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.834452148438" Y="-28.38795703125" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406587890625" Y="-27.616125" />
                  <Point X="-2.405575439453" Y="-27.58519140625" />
                  <Point X="-2.414560546875" Y="-27.555572265625" />
                  <Point X="-2.428778564453" Y="-27.5267421875" />
                  <Point X="-2.446807373047" Y="-27.501583984375" />
                  <Point X="-2.464156005859" Y="-27.484236328125" />
                  <Point X="-2.489316650391" Y="-27.466208984375" />
                  <Point X="-2.518145507812" Y="-27.451994140625" />
                  <Point X="-2.547761962891" Y="-27.44301171875" />
                  <Point X="-2.578693847656" Y="-27.444025390625" />
                  <Point X="-2.610219238281" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.217245605469" Y="-27.794943359375" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.932200927734" Y="-27.991796875" />
                  <Point X="-4.082862792969" Y="-27.793857421875" />
                  <Point X="-4.240606445313" Y="-27.529345703125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.824530029297" Y="-27.049896484375" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.359658203125" />
                  <Point X="-3.045556152344" Y="-26.32798828125" />
                  <Point X="-3.052556640625" Y="-26.298244140625" />
                  <Point X="-3.068637451172" Y="-26.27226171875" />
                  <Point X="-3.089468994141" Y="-26.2483046875" />
                  <Point X="-3.112970458984" Y="-26.22876953125" />
                  <Point X="-3.139458251953" Y="-26.2131796875" />
                  <Point X="-3.168728759766" Y="-26.201953125" />
                  <Point X="-3.200612792969" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-3.961677734375" Y="-26.29045703125" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.775152832031" Y="-26.223341796875" />
                  <Point X="-4.834078125" Y="-25.992650390625" />
                  <Point X="-4.875812988281" Y="-25.70084375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.351441894531" Y="-25.439744140625" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.509926513672" Y="-25.210896484375" />
                  <Point X="-3.479049316406" Y="-25.19305078125" />
                  <Point X="-3.465885498047" Y="-25.18387890625" />
                  <Point X="-3.441624267578" Y="-25.1637265625" />
                  <Point X="-3.4255546875" Y="-25.14660546875" />
                  <Point X="-3.414175048828" Y="-25.12606640625" />
                  <Point X="-3.401645507812" Y="-25.0948828125" />
                  <Point X="-3.397150878906" Y="-25.080486328125" />
                  <Point X="-3.390755859375" Y="-25.052302734375" />
                  <Point X="-3.388401611328" Y="-25.030912109375" />
                  <Point X="-3.390922363281" Y="-25.0095390625" />
                  <Point X="-3.398272949219" Y="-24.9782734375" />
                  <Point X="-3.40291015625" Y="-24.963837890625" />
                  <Point X="-3.414484130859" Y="-24.935736328125" />
                  <Point X="-3.426039794922" Y="-24.915296875" />
                  <Point X="-3.442255126953" Y="-24.89831640625" />
                  <Point X="-3.469386230469" Y="-24.876171875" />
                  <Point X="-3.482654541016" Y="-24.86709765625" />
                  <Point X="-3.510661865234" Y="-24.8512421875" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.198076660156" Y="-24.66391015625" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.862462890625" Y="-24.279658203125" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.740475097656" Y="-23.712994140625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.352434570312" Y="-23.622958984375" />
                  <Point X="-3.765666503906" Y="-23.70020703125" />
                  <Point X="-3.744985351562" Y="-23.700658203125" />
                  <Point X="-3.723422851562" Y="-23.698771484375" />
                  <Point X="-3.703139160156" Y="-23.694736328125" />
                  <Point X="-3.669251708984" Y="-23.684052734375" />
                  <Point X="-3.641713134766" Y="-23.675369140625" />
                  <Point X="-3.622776123047" Y="-23.667037109375" />
                  <Point X="-3.604032226562" Y="-23.65621484375" />
                  <Point X="-3.587350585938" Y="-23.643982421875" />
                  <Point X="-3.573712402344" Y="-23.6284296875" />
                  <Point X="-3.561298339844" Y="-23.61069921875" />
                  <Point X="-3.5513515625" Y="-23.592568359375" />
                  <Point X="-3.53775390625" Y="-23.5597421875" />
                  <Point X="-3.526704101563" Y="-23.533064453125" />
                  <Point X="-3.520915283203" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532050048828" Y="-23.410623046875" />
                  <Point X="-3.54845703125" Y="-23.37910546875" />
                  <Point X="-3.561789794922" Y="-23.353494140625" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-3.983696289063" Y="-23.012626953125" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.228709960938" Y="-22.519138671875" />
                  <Point X="-4.08115625" Y="-22.266345703125" />
                  <Point X="-3.858607910156" Y="-21.980291015625" />
                  <Point X="-3.750504638672" Y="-21.84133984375" />
                  <Point X="-3.567033203125" Y="-21.947265625" />
                  <Point X="-3.206656738281" Y="-22.155330078125" />
                  <Point X="-3.187729003906" Y="-22.163658203125" />
                  <Point X="-3.167086669922" Y="-22.17016796875" />
                  <Point X="-3.146793945312" Y="-22.174205078125" />
                  <Point X="-3.099597900391" Y="-22.178333984375" />
                  <Point X="-3.061244628906" Y="-22.181689453125" />
                  <Point X="-3.040560546875" Y="-22.18123828125" />
                  <Point X="-3.019102294922" Y="-22.178412109375" />
                  <Point X="-2.999013183594" Y="-22.173494140625" />
                  <Point X="-2.980464355469" Y="-22.16434765625" />
                  <Point X="-2.962210205078" Y="-22.15271875" />
                  <Point X="-2.946077880859" Y="-22.139771484375" />
                  <Point X="-2.912577880859" Y="-22.106271484375" />
                  <Point X="-2.885354248047" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.847564941406" Y="-21.91668359375" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-3.038876220703" Y="-21.525611328125" />
                  <Point X="-3.183332763672" Y="-21.27540625" />
                  <Point X="-2.957617431641" Y="-21.1023515625" />
                  <Point X="-2.700620849609" Y="-20.905314453125" />
                  <Point X="-2.350125488281" Y="-20.7105859375" />
                  <Point X="-2.167036621094" Y="-20.608865234375" />
                  <Point X="-2.153517822266" Y="-20.626484375" />
                  <Point X="-2.043195556641" Y="-20.7702578125" />
                  <Point X="-2.028892944336" Y="-20.78519921875" />
                  <Point X="-2.012313964844" Y="-20.799111328125" />
                  <Point X="-1.995114746094" Y="-20.810603515625" />
                  <Point X="-1.9425859375" Y="-20.83794921875" />
                  <Point X="-1.899898681641" Y="-20.860171875" />
                  <Point X="-1.880625854492" Y="-20.86766796875" />
                  <Point X="-1.859719360352" Y="-20.873271484375" />
                  <Point X="-1.83926940918" Y="-20.876419921875" />
                  <Point X="-1.818622802734" Y="-20.87506640625" />
                  <Point X="-1.797307495117" Y="-20.871306640625" />
                  <Point X="-1.777452636719" Y="-20.865517578125" />
                  <Point X="-1.722740112305" Y="-20.842853515625" />
                  <Point X="-1.678278686523" Y="-20.8244375" />
                  <Point X="-1.660147705078" Y="-20.814490234375" />
                  <Point X="-1.642417602539" Y="-20.802076171875" />
                  <Point X="-1.626865112305" Y="-20.7884375" />
                  <Point X="-1.614633422852" Y="-20.771755859375" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734080078125" />
                  <Point X="-1.577672363281" Y="-20.6776015625" />
                  <Point X="-1.563200927734" Y="-20.631703125" />
                  <Point X="-1.559165405273" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.557730224609" Y="-20.569173828125" />
                  <Point X="-1.57695300293" Y="-20.4231640625" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.282327270508" Y="-20.28346875" />
                  <Point X="-0.949623718262" Y="-20.19019140625" />
                  <Point X="-0.524727539062" Y="-20.1404609375" />
                  <Point X="-0.294711578369" Y="-20.113541015625" />
                  <Point X="-0.240512741089" Y="-20.315814453125" />
                  <Point X="-0.133903366089" Y="-20.713685546875" />
                  <Point X="-0.121129745483" Y="-20.741876953125" />
                  <Point X="-0.103271453857" Y="-20.768603515625" />
                  <Point X="-0.082113960266" Y="-20.791193359375" />
                  <Point X="-0.054818138123" Y="-20.805783203125" />
                  <Point X="-0.024380064011" Y="-20.816115234375" />
                  <Point X="0.006155897141" Y="-20.82115625" />
                  <Point X="0.036691894531" Y="-20.816115234375" />
                  <Point X="0.067129966736" Y="-20.805783203125" />
                  <Point X="0.094425964355" Y="-20.791193359375" />
                  <Point X="0.115583587646" Y="-20.768603515625" />
                  <Point X="0.133441726685" Y="-20.741876953125" />
                  <Point X="0.146215194702" Y="-20.713685546875" />
                  <Point X="0.232850479126" Y="-20.390359375" />
                  <Point X="0.307419616699" Y="-20.112060546875" />
                  <Point X="0.553545349121" Y="-20.137837890625" />
                  <Point X="0.84403125" Y="-20.168259765625" />
                  <Point X="1.195583129883" Y="-20.253134765625" />
                  <Point X="1.481039794922" Y="-20.3220546875" />
                  <Point X="1.709182861328" Y="-20.404802734375" />
                  <Point X="1.894646850586" Y="-20.472072265625" />
                  <Point X="2.115905761719" Y="-20.575546875" />
                  <Point X="2.294555908203" Y="-20.65909765625" />
                  <Point X="2.508348876953" Y="-20.78365234375" />
                  <Point X="2.680974609375" Y="-20.884224609375" />
                  <Point X="2.882571289062" Y="-21.02758984375" />
                  <Point X="2.943259033203" Y="-21.070748046875" />
                  <Point X="2.622924804688" Y="-21.62558203125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.4839453125" />
                  <Point X="2.121232421875" Y="-22.52823828125" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.112346435547" Y="-22.65734765625" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442138672" Y="-22.710396484375" />
                  <Point X="2.129708007812" Y="-22.732484375" />
                  <Point X="2.140071044922" Y="-22.752529296875" />
                  <Point X="2.163770019531" Y="-22.787455078125" />
                  <Point X="2.183028808594" Y="-22.815837890625" />
                  <Point X="2.194465820313" Y="-22.829671875" />
                  <Point X="2.221597167969" Y="-22.85440625" />
                  <Point X="2.256523193359" Y="-22.87810546875" />
                  <Point X="2.284905761719" Y="-22.897365234375" />
                  <Point X="2.304945556641" Y="-22.9077265625" />
                  <Point X="2.327033691406" Y="-22.915994140625" />
                  <Point X="2.348965576172" Y="-22.921337890625" />
                  <Point X="2.387265869141" Y="-22.925955078125" />
                  <Point X="2.418390380859" Y="-22.929708984375" />
                  <Point X="2.436467285156" Y="-22.93015625" />
                  <Point X="2.473208496094" Y="-22.925830078125" />
                  <Point X="2.517500976563" Y="-22.913984375" />
                  <Point X="2.553494873047" Y="-22.904359375" />
                  <Point X="2.565285644531" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.257597412109" Y="-22.5035703125" />
                  <Point X="3.967325683594" Y="-22.09380859375" />
                  <Point X="4.020873535156" Y="-22.168228515625" />
                  <Point X="4.123273925781" Y="-22.310541015625" />
                  <Point X="4.235657714844" Y="-22.496255859375" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.856618164062" Y="-22.851328125" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221421386719" Y="-23.33976171875" />
                  <Point X="3.203974365234" Y="-23.358373046875" />
                  <Point X="3.172096923828" Y="-23.399958984375" />
                  <Point X="3.146192138672" Y="-23.43375390625" />
                  <Point X="3.136606689453" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.109755615234" Y="-23.525373046875" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.107487060547" Y="-23.675474609375" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120680419922" Y="-23.7310234375" />
                  <Point X="3.136283203125" Y="-23.764259765625" />
                  <Point X="3.162795654297" Y="-23.804556640625" />
                  <Point X="3.184340820312" Y="-23.8373046875" />
                  <Point X="3.198891113281" Y="-23.854546875" />
                  <Point X="3.216133789062" Y="-23.87063671875" />
                  <Point X="3.234346435547" Y="-23.88396484375" />
                  <Point X="3.272766601562" Y="-23.905591796875" />
                  <Point X="3.303988525391" Y="-23.92316796875" />
                  <Point X="3.320522705078" Y="-23.9305" />
                  <Point X="3.356119628906" Y="-23.9405625" />
                  <Point X="3.408066162109" Y="-23.947427734375" />
                  <Point X="3.450280273438" Y="-23.953005859375" />
                  <Point X="3.462698974609" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.123769042969" Y="-23.869341796875" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.801956542969" Y="-23.886537109375" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.881352050781" Y="-24.294662109375" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.433799804688" Y="-24.47823046875" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704788818359" Y="-24.674416015625" />
                  <Point X="3.681547851562" Y="-24.684931640625" />
                  <Point X="3.630511962891" Y="-24.7144296875" />
                  <Point X="3.589037841797" Y="-24.73840234375" />
                  <Point X="3.574308105469" Y="-24.74890625" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.516908691406" Y="-24.813443359375" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.48030078125" Y="-24.86443359375" />
                  <Point X="3.47052734375" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.453473632812" Y="-24.9606953125" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443483154297" Y="-25.021876953125" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.455386230469" Y="-25.1118515625" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.47052734375" Y="-25.1766640625" />
                  <Point X="3.48030078125" Y="-25.198126953125" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.522645996094" Y="-25.256427734375" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.559994873047" Y="-25.301232421875" />
                  <Point X="3.589035400391" Y="-25.32415625" />
                  <Point X="3.640071533203" Y="-25.35365625" />
                  <Point X="3.681545410156" Y="-25.37762890625" />
                  <Point X="3.692708496094" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.299422363281" Y="-25.54832421875" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.879579101562" Y="-25.785849609375" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.809648925781" Y="-26.147560546875" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.256155273438" Y="-26.1129453125" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035400391" Y="-26.0027109375" />
                  <Point X="3.374659179688" Y="-26.005509765625" />
                  <Point X="3.274493652344" Y="-26.02728125" />
                  <Point X="3.193094970703" Y="-26.04497265625" />
                  <Point X="3.163974609375" Y="-26.05659765625" />
                  <Point X="3.136147705078" Y="-26.073490234375" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.051853271484" Y="-26.16677734375" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.961080078125" Y="-26.3996640625" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.507564453125" />
                  <Point X="2.964078613281" Y="-26.539185546875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.031883789062" Y="-26.65421875" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086931396484" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.651510009766" Y="-27.175943359375" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.194031738281" Y="-27.637775390625" />
                  <Point X="4.124815429687" Y="-27.749779296875" />
                  <Point X="4.030974121094" Y="-27.88311328125" />
                  <Point X="4.028980224609" Y="-27.8859453125" />
                  <Point X="3.541760986328" Y="-27.604650390625" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224121094" Y="-27.159826171875" />
                  <Point X="2.635010986328" Y="-27.138296875" />
                  <Point X="2.538133789062" Y="-27.12080078125" />
                  <Point X="2.506784179688" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.345796875" Y="-27.187298828125" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242385009766" Y="-27.246548828125" />
                  <Point X="2.221425537109" Y="-27.2675078125" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.152409423828" Y="-27.3894765625" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.563259765625" />
                  <Point X="2.117205322266" Y="-27.68247265625" />
                  <Point X="2.134701171875" Y="-27.7793515625" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.499389892578" Y="-28.42808984375" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.781837402344" Y="-29.11165234375" />
                  <Point X="2.701764648438" Y="-29.163482421875" />
                  <Point X="2.323849853516" Y="-28.670974609375" />
                  <Point X="1.758546264648" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721923706055" Y="-27.900556640625" />
                  <Point X="1.60434753418" Y="-27.824966796875" />
                  <Point X="1.508800292969" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365722656" Y="-27.743435546875" />
                  <Point X="1.417100585938" Y="-27.741119140625" />
                  <Point X="1.288510864258" Y="-27.752951171875" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.156362670898" Y="-27.7693984375" />
                  <Point X="1.128977050781" Y="-27.7807421875" />
                  <Point X="1.104595092773" Y="-27.795462890625" />
                  <Point X="1.005301513672" Y="-27.8780234375" />
                  <Point X="0.92461138916" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887248962402" Y="-27.996689453125" />
                  <Point X="0.875624267578" Y="-28.025810546875" />
                  <Point X="0.845936035156" Y="-28.1623984375" />
                  <Point X="0.821809997559" Y="-28.273396484375" />
                  <Point X="0.81972454834" Y="-28.289626953125" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="0.918241210938" Y="-29.07129296875" />
                  <Point X="1.022065429688" Y="-29.859916015625" />
                  <Point X="0.97570916748" Y="-29.870076171875" />
                  <Point X="0.929315551758" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058442993164" Y="-29.7526328125" />
                  <Point X="-1.141246459961" Y="-29.731330078125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.156725952148" Y="-29.3298203125" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.17847265625" />
                  <Point X="-1.199026000977" Y="-29.149505859375" />
                  <Point X="-1.205664794922" Y="-29.135466796875" />
                  <Point X="-1.221736572266" Y="-29.10762890625" />
                  <Point X="-1.230573486328" Y="-29.094861328125" />
                  <Point X="-1.250208862305" Y="-29.070935546875" />
                  <Point X="-1.261007324219" Y="-29.05977734375" />
                  <Point X="-1.389692871094" Y="-28.946923828125" />
                  <Point X="-1.494268188477" Y="-28.855212890625" />
                  <Point X="-1.506739379883" Y="-28.84596484375" />
                  <Point X="-1.533022338867" Y="-28.82962109375" />
                  <Point X="-1.546834106445" Y="-28.822525390625" />
                  <Point X="-1.57653112793" Y="-28.810224609375" />
                  <Point X="-1.591315795898" Y="-28.805474609375" />
                  <Point X="-1.621457763672" Y="-28.798447265625" />
                  <Point X="-1.636815063477" Y="-28.796169921875" />
                  <Point X="-1.807609619141" Y="-28.7849765625" />
                  <Point X="-1.946404296875" Y="-28.77587890625" />
                  <Point X="-1.961928344727" Y="-28.7761328125" />
                  <Point X="-1.992725585938" Y="-28.779166015625" />
                  <Point X="-2.007998779297" Y="-28.7819453125" />
                  <Point X="-2.039047607422" Y="-28.790263671875" />
                  <Point X="-2.053666748047" Y="-28.795494140625" />
                  <Point X="-2.081861328125" Y="-28.808267578125" />
                  <Point X="-2.095436767578" Y="-28.815810546875" />
                  <Point X="-2.237751708984" Y="-28.91090234375" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359685302734" Y="-28.9927578125" />
                  <Point X="-2.380448242188" Y="-29.01013671875" />
                  <Point X="-2.402761962891" Y="-29.032775390625" />
                  <Point X="-2.410471923828" Y="-29.041630859375" />
                  <Point X="-2.503200195312" Y="-29.162478515625" />
                  <Point X="-2.561003417969" Y="-29.1266875" />
                  <Point X="-2.747615234375" Y="-29.01114453125" />
                  <Point X="-2.963786621094" Y="-28.844697265625" />
                  <Point X="-2.980862792969" Y="-28.831548828125" />
                  <Point X="-2.7521796875" Y="-28.43545703125" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413085938" Y="-27.634658203125" />
                  <Point X="-2.311638671875" Y="-27.619232421875" />
                  <Point X="-2.310626220703" Y="-27.588298828125" />
                  <Point X="-2.314666259766" Y="-27.55761328125" />
                  <Point X="-2.323651367188" Y="-27.527994140625" />
                  <Point X="-2.329358398438" Y="-27.513552734375" />
                  <Point X="-2.343576416016" Y="-27.48472265625" />
                  <Point X="-2.351559082031" Y="-27.47140625" />
                  <Point X="-2.369587890625" Y="-27.446248046875" />
                  <Point X="-2.379634033203" Y="-27.43440625" />
                  <Point X="-2.396982666016" Y="-27.41705859375" />
                  <Point X="-2.408825683594" Y="-27.40701171875" />
                  <Point X="-2.433986328125" Y="-27.388984375" />
                  <Point X="-2.447303955078" Y="-27.38100390625" />
                  <Point X="-2.4761328125" Y="-27.3667890625" />
                  <Point X="-2.490572998047" Y="-27.361083984375" />
                  <Point X="-2.520189453125" Y="-27.3521015625" />
                  <Point X="-2.550873535156" Y="-27.3480625" />
                  <Point X="-2.581805419922" Y="-27.349076171875" />
                  <Point X="-2.597229492188" Y="-27.3508515625" />
                  <Point X="-2.628754882813" Y="-27.357123046875" />
                  <Point X="-2.643685546875" Y="-27.36138671875" />
                  <Point X="-2.672649902344" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.264745605469" Y="-27.712671875" />
                  <Point X="-3.793087402344" Y="-28.0177109375" />
                  <Point X="-3.856607666016" Y="-27.9342578125" />
                  <Point X="-4.004022705078" Y="-27.740583984375" />
                  <Point X="-4.159013671875" Y="-27.4806875" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.766697753906" Y="-27.125265625" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.3988046875" />
                  <Point X="-2.948748535156" Y="-26.368376953125" />
                  <Point X="-2.948577880859" Y="-26.353048828125" />
                  <Point X="-2.950786376953" Y="-26.32137890625" />
                  <Point X="-2.953082763672" Y="-26.306224609375" />
                  <Point X="-2.960083251953" Y="-26.27648046875" />
                  <Point X="-2.971776611328" Y="-26.248248046875" />
                  <Point X="-2.987857421875" Y="-26.222265625" />
                  <Point X="-2.996948974609" Y="-26.20992578125" />
                  <Point X="-3.017780517578" Y="-26.18596875" />
                  <Point X="-3.0287421875" Y="-26.175248046875" />
                  <Point X="-3.052243652344" Y="-26.155712890625" />
                  <Point X="-3.064783447266" Y="-26.1468984375" />
                  <Point X="-3.091271240234" Y="-26.13130859375" />
                  <Point X="-3.105437988281" Y="-26.12448046875" />
                  <Point X="-3.134708496094" Y="-26.11325390625" />
                  <Point X="-3.149812255859" Y="-26.10885546875" />
                  <Point X="-3.181696289062" Y="-26.102376953125" />
                  <Point X="-3.19730859375" Y="-26.10053125" />
                  <Point X="-3.228625" Y="-26.09944140625" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-3.974077636719" Y="-26.19626953125" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.683107910156" Y="-26.199830078125" />
                  <Point X="-4.740762695312" Y="-25.97411328125" />
                  <Point X="-4.781770019531" Y="-25.687392578125" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.326854003906" Y="-25.5315078125" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.496495849609" Y="-25.30816796875" />
                  <Point X="-3.473546386719" Y="-25.298654296875" />
                  <Point X="-3.462389160156" Y="-25.293146484375" />
                  <Point X="-3.431511962891" Y="-25.27530078125" />
                  <Point X="-3.424740722656" Y="-25.27099609375" />
                  <Point X="-3.405184326172" Y="-25.25695703125" />
                  <Point X="-3.380923095703" Y="-25.2368046875" />
                  <Point X="-3.372355712891" Y="-25.228740234375" />
                  <Point X="-3.356286132812" Y="-25.211619140625" />
                  <Point X="-3.342456542969" Y="-25.192646484375" />
                  <Point X="-3.331076904297" Y="-25.172107421875" />
                  <Point X="-3.326024658203" Y="-25.161484375" />
                  <Point X="-3.313495117188" Y="-25.13030078125" />
                  <Point X="-3.310962158203" Y="-25.1231953125" />
                  <Point X="-3.304505859375" Y="-25.1015078125" />
                  <Point X="-3.298110839844" Y="-25.07332421875" />
                  <Point X="-3.296326171875" Y="-25.0626953125" />
                  <Point X="-3.293971923828" Y="-25.0413046875" />
                  <Point X="-3.294055419922" Y="-25.01978515625" />
                  <Point X="-3.296576171875" Y="-24.998412109375" />
                  <Point X="-3.298443847656" Y="-24.987796875" />
                  <Point X="-3.305794433594" Y="-24.95653125" />
                  <Point X="-3.307825195312" Y="-24.94921875" />
                  <Point X="-3.315068847656" Y="-24.92766015625" />
                  <Point X="-3.326642822266" Y="-24.89955859375" />
                  <Point X="-3.331785644531" Y="-24.888982421875" />
                  <Point X="-3.343341308594" Y="-24.86854296875" />
                  <Point X="-3.357334472656" Y="-24.8496875" />
                  <Point X="-3.373549804688" Y="-24.83270703125" />
                  <Point X="-3.382184814453" Y="-24.82471875" />
                  <Point X="-3.409315917969" Y="-24.80257421875" />
                  <Point X="-3.4157578125" Y="-24.797755859375" />
                  <Point X="-3.435852539062" Y="-24.78442578125" />
                  <Point X="-3.463859863281" Y="-24.7685703125" />
                  <Point X="-3.474677490234" Y="-24.7633203125" />
                  <Point X="-3.496891601562" Y="-24.754228515625" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.173488769531" Y="-24.572146484375" />
                  <Point X="-4.7854453125" Y="-24.408173828125" />
                  <Point X="-4.768486328125" Y="-24.293564453125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.648782226562" Y="-23.737841796875" />
                  <Point X="-4.633586425781" Y="-23.681763671875" />
                  <Point X="-4.364834960938" Y="-23.717146484375" />
                  <Point X="-3.778066894531" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747057373047" Y="-23.795634765625" />
                  <Point X="-3.736704589844" Y="-23.795296875" />
                  <Point X="-3.715142089844" Y="-23.79341015625" />
                  <Point X="-3.704887207031" Y="-23.7919453125" />
                  <Point X="-3.684603515625" Y="-23.78791015625" />
                  <Point X="-3.674574707031" Y="-23.78533984375" />
                  <Point X="-3.640687255859" Y="-23.77465625" />
                  <Point X="-3.613148681641" Y="-23.76597265625" />
                  <Point X="-3.603453857422" Y="-23.76232421875" />
                  <Point X="-3.584516845703" Y="-23.7539921875" />
                  <Point X="-3.575274658203" Y="-23.74930859375" />
                  <Point X="-3.556530761719" Y="-23.738486328125" />
                  <Point X="-3.547854980469" Y="-23.73282421875" />
                  <Point X="-3.531173339844" Y="-23.720591796875" />
                  <Point X="-3.515923095703" Y="-23.7066171875" />
                  <Point X="-3.502284912109" Y="-23.691064453125" />
                  <Point X="-3.495891113281" Y="-23.682916015625" />
                  <Point X="-3.483477050781" Y="-23.665185546875" />
                  <Point X="-3.478009033203" Y="-23.656392578125" />
                  <Point X="-3.468062255859" Y="-23.63826171875" />
                  <Point X="-3.463583496094" Y="-23.628923828125" />
                  <Point X="-3.449985839844" Y="-23.59609765625" />
                  <Point X="-3.438936035156" Y="-23.569419921875" />
                  <Point X="-3.435499755859" Y="-23.559650390625" />
                  <Point X="-3.4297109375" Y="-23.539791015625" />
                  <Point X="-3.427358398438" Y="-23.529701171875" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436012695312" Y="-23.39546875" />
                  <Point X="-3.443509277344" Y="-23.37619140625" />
                  <Point X="-3.447783935547" Y="-23.3667578125" />
                  <Point X="-3.464190917969" Y="-23.335240234375" />
                  <Point X="-3.477523681641" Y="-23.30962890625" />
                  <Point X="-3.482799804688" Y="-23.30071484375" />
                  <Point X="-3.494291748047" Y="-23.283515625" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521497558594" Y="-23.25109375" />
                  <Point X="-3.536439208984" Y="-23.2367890625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.925863769531" Y="-22.9372578125" />
                  <Point X="-4.227614746094" Y="-22.705716796875" />
                  <Point X="-4.146663574219" Y="-22.56702734375" />
                  <Point X="-4.002295654297" Y="-22.319693359375" />
                  <Point X="-3.783627197266" Y="-22.038625" />
                  <Point X="-3.726338623047" Y="-21.96498828125" />
                  <Point X="-3.614532714844" Y="-22.0295390625" />
                  <Point X="-3.25415625" Y="-22.237603515625" />
                  <Point X="-3.244916503906" Y="-22.24228515625" />
                  <Point X="-3.225988769531" Y="-22.25061328125" />
                  <Point X="-3.216301025391" Y="-22.254259765625" />
                  <Point X="-3.195658691406" Y="-22.26076953125" />
                  <Point X="-3.185623046875" Y="-22.263341796875" />
                  <Point X="-3.165330322266" Y="-22.26737890625" />
                  <Point X="-3.155073242188" Y="-22.26884375" />
                  <Point X="-3.107877197266" Y="-22.27297265625" />
                  <Point X="-3.069523925781" Y="-22.276328125" />
                  <Point X="-3.059172851562" Y="-22.276666015625" />
                  <Point X="-3.038488769531" Y="-22.27621484375" />
                  <Point X="-3.028155761719" Y="-22.27542578125" />
                  <Point X="-3.006697509766" Y="-22.272599609375" />
                  <Point X="-2.996512695312" Y="-22.2706875" />
                  <Point X="-2.976423583984" Y="-22.26576953125" />
                  <Point X="-2.956998779297" Y="-22.25869921875" />
                  <Point X="-2.938449951172" Y="-22.249552734375" />
                  <Point X="-2.929421630859" Y="-22.244470703125" />
                  <Point X="-2.911167480469" Y="-22.232841796875" />
                  <Point X="-2.902748291016" Y="-22.22680859375" />
                  <Point X="-2.886615966797" Y="-22.213861328125" />
                  <Point X="-2.878902832031" Y="-22.206947265625" />
                  <Point X="-2.845402832031" Y="-22.173447265625" />
                  <Point X="-2.818179199219" Y="-22.146224609375" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321044922" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.752926513672" Y="-21.908404296875" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.956604003906" Y="-21.478111328125" />
                  <Point X="-3.059387695312" Y="-21.3000859375" />
                  <Point X="-2.899815185547" Y="-21.1777421875" />
                  <Point X="-2.648378662109" Y="-20.98496875" />
                  <Point X="-2.303987792969" Y="-20.793630859375" />
                  <Point X="-2.192524658203" Y="-20.731703125" />
                  <Point X="-2.118565917969" Y="-20.828087890625" />
                  <Point X="-2.111821777344" Y="-20.83594921875" />
                  <Point X="-2.097519042969" Y="-20.850890625" />
                  <Point X="-2.089959472656" Y="-20.85797265625" />
                  <Point X="-2.073380615234" Y="-20.871884765625" />
                  <Point X="-2.065093261719" Y="-20.8781015625" />
                  <Point X="-2.047893920898" Y="-20.88959375" />
                  <Point X="-2.038982055664" Y="-20.894869140625" />
                  <Point X="-1.98645324707" Y="-20.92221484375" />
                  <Point X="-1.943765991211" Y="-20.9444375" />
                  <Point X="-1.934335571289" Y="-20.9487109375" />
                  <Point X="-1.9150625" Y="-20.95620703125" />
                  <Point X="-1.905220336914" Y="-20.9594296875" />
                  <Point X="-1.884313842773" Y="-20.965033203125" />
                  <Point X="-1.874175048828" Y="-20.967166015625" />
                  <Point X="-1.853725097656" Y="-20.970314453125" />
                  <Point X="-1.833054931641" Y="-20.971216796875" />
                  <Point X="-1.812408325195" Y="-20.96986328125" />
                  <Point X="-1.802120727539" Y="-20.968623046875" />
                  <Point X="-1.780805419922" Y="-20.96486328125" />
                  <Point X="-1.770715698242" Y="-20.962509765625" />
                  <Point X="-1.750860839844" Y="-20.956720703125" />
                  <Point X="-1.741095825195" Y="-20.95328515625" />
                  <Point X="-1.686383300781" Y="-20.93062109375" />
                  <Point X="-1.641921875" Y="-20.912205078125" />
                  <Point X="-1.632583862305" Y="-20.9077265625" />
                  <Point X="-1.614452758789" Y="-20.897779296875" />
                  <Point X="-1.605659790039" Y="-20.892310546875" />
                  <Point X="-1.587929931641" Y="-20.879896484375" />
                  <Point X="-1.579781005859" Y="-20.873501953125" />
                  <Point X="-1.564228515625" Y="-20.85986328125" />
                  <Point X="-1.550253173828" Y="-20.84461328125" />
                  <Point X="-1.538021606445" Y="-20.827931640625" />
                  <Point X="-1.532361450195" Y="-20.819255859375" />
                  <Point X="-1.521539306641" Y="-20.80051171875" />
                  <Point X="-1.516858276367" Y="-20.791275390625" />
                  <Point X="-1.50852722168" Y="-20.77234375" />
                  <Point X="-1.504877197266" Y="-20.7626484375" />
                  <Point X="-1.487069335938" Y="-20.706169921875" />
                  <Point X="-1.472597900391" Y="-20.660271484375" />
                  <Point X="-1.470026123047" Y="-20.650236328125" />
                  <Point X="-1.465990600586" Y="-20.629947265625" />
                  <Point X="-1.464526733398" Y="-20.619693359375" />
                  <Point X="-1.462640625" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752807617" Y="-20.5671015625" />
                  <Point X="-1.46354284668" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-1.256681518555" Y="-20.37494140625" />
                  <Point X="-0.931165039062" Y="-20.2836796875" />
                  <Point X="-0.513683959961" Y="-20.23481640625" />
                  <Point X="-0.365222930908" Y="-20.21744140625" />
                  <Point X="-0.332275756836" Y="-20.34040234375" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.22043510437" Y="-20.752892578125" />
                  <Point X="-0.207661560059" Y="-20.781083984375" />
                  <Point X="-0.200119094849" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600669861" Y="-20.88956640625" />
                  <Point X="-0.085354125977" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227790833" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912307739" Y="-20.88956640625" />
                  <Point X="0.139208236694" Y="-20.8749765625" />
                  <Point X="0.163763122559" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.1945730896" Y="-20.8213828125" />
                  <Point X="0.21243132019" Y="-20.79465625" />
                  <Point X="0.219973648071" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978118896" Y="-20.7382734375" />
                  <Point X="0.324613372803" Y="-20.414947265625" />
                  <Point X="0.378190856934" Y="-20.2149921875" />
                  <Point X="0.543649963379" Y="-20.2323203125" />
                  <Point X="0.827852050781" Y="-20.262083984375" />
                  <Point X="1.173287841797" Y="-20.345482421875" />
                  <Point X="1.453622436523" Y="-20.413166015625" />
                  <Point X="1.676790893555" Y="-20.494109375" />
                  <Point X="1.858249633789" Y="-20.55992578125" />
                  <Point X="2.075661132812" Y="-20.6616015625" />
                  <Point X="2.250428955078" Y="-20.7433359375" />
                  <Point X="2.460526367188" Y="-20.86573828125" />
                  <Point X="2.6294296875" Y="-20.964142578125" />
                  <Point X="2.817778808594" Y="-21.0980859375" />
                  <Point X="2.54065234375" Y="-21.57808203125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.0531796875" Y="-22.42656640625" />
                  <Point X="2.044181640625" Y="-22.450439453125" />
                  <Point X="2.041301635742" Y="-22.459404296875" />
                  <Point X="2.02945715332" Y="-22.503697265625" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017912719727" Y="-22.548537109375" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.018029663086" Y="-22.668720703125" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.023800537109" Y="-22.71096484375" />
                  <Point X="2.029143310547" Y="-22.732888671875" />
                  <Point X="2.032468261719" Y="-22.743693359375" />
                  <Point X="2.040734130859" Y="-22.76578125" />
                  <Point X="2.045318847656" Y="-22.77611328125" />
                  <Point X="2.055681640625" Y="-22.796158203125" />
                  <Point X="2.061460205078" Y="-22.80587109375" />
                  <Point X="2.085159179688" Y="-22.840796875" />
                  <Point X="2.10441796875" Y="-22.8691796875" />
                  <Point X="2.109810791016" Y="-22.876369140625" />
                  <Point X="2.130463378906" Y="-22.899876953125" />
                  <Point X="2.157594726562" Y="-22.924611328125" />
                  <Point X="2.168255371094" Y="-22.933017578125" />
                  <Point X="2.203181396484" Y="-22.956716796875" />
                  <Point X="2.231563964844" Y="-22.9759765625" />
                  <Point X="2.241274169922" Y="-22.981751953125" />
                  <Point X="2.261313964844" Y="-22.99211328125" />
                  <Point X="2.271643554688" Y="-22.99669921875" />
                  <Point X="2.293731689453" Y="-23.004966796875" />
                  <Point X="2.304544677734" Y="-23.00829296875" />
                  <Point X="2.3264765625" Y="-23.01363671875" />
                  <Point X="2.337595458984" Y="-23.015654296875" />
                  <Point X="2.375895751953" Y="-23.020271484375" />
                  <Point X="2.407020263672" Y="-23.024025390625" />
                  <Point X="2.416040527344" Y="-23.0246796875" />
                  <Point X="2.447576416016" Y="-23.02450390625" />
                  <Point X="2.484317626953" Y="-23.020177734375" />
                  <Point X="2.497752929688" Y="-23.01760546875" />
                  <Point X="2.542045410156" Y="-23.005759765625" />
                  <Point X="2.578039306641" Y="-22.996134765625" />
                  <Point X="2.584001708984" Y="-22.994328125" />
                  <Point X="2.604412841797" Y="-22.9869296875" />
                  <Point X="2.627661132812" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.305097412109" Y="-22.585841796875" />
                  <Point X="3.940402832031" Y="-22.219048828125" />
                  <Point X="3.943760742188" Y="-22.22371484375" />
                  <Point X="4.043958984375" Y="-22.362966796875" />
                  <Point X="4.136885253906" Y="-22.51652734375" />
                  <Point X="3.798786132812" Y="-22.775958984375" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168135742188" Y="-23.2601328125" />
                  <Point X="3.15211328125" Y="-23.2747890625" />
                  <Point X="3.134666259766" Y="-23.293400390625" />
                  <Point X="3.128577392578" Y="-23.300578125" />
                  <Point X="3.096699951172" Y="-23.3421640625" />
                  <Point X="3.070795166016" Y="-23.375958984375" />
                  <Point X="3.065636230469" Y="-23.3833984375" />
                  <Point X="3.049740234375" Y="-23.410626953125" />
                  <Point X="3.034763427734" Y="-23.444453125" />
                  <Point X="3.030140380859" Y="-23.457328125" />
                  <Point X="3.018266113281" Y="-23.499787109375" />
                  <Point X="3.008616455078" Y="-23.53429296875" />
                  <Point X="3.006225341797" Y="-23.545337890625" />
                  <Point X="3.002771728516" Y="-23.56763671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.014447021484" Y="-23.694671875" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024598388672" Y="-23.741767578125" />
                  <Point X="3.034685058594" Y="-23.77139453125" />
                  <Point X="3.050287841797" Y="-23.804630859375" />
                  <Point X="3.056919921875" Y="-23.816474609375" />
                  <Point X="3.083432373047" Y="-23.856771484375" />
                  <Point X="3.104977539062" Y="-23.88951953125" />
                  <Point X="3.111737792969" Y="-23.898572265625" />
                  <Point X="3.126288085938" Y="-23.915814453125" />
                  <Point X="3.134078125" Y="-23.92400390625" />
                  <Point X="3.151320800781" Y="-23.94009375" />
                  <Point X="3.160030517578" Y="-23.94730078125" />
                  <Point X="3.178243164062" Y="-23.96062890625" />
                  <Point X="3.18774609375" Y="-23.96675" />
                  <Point X="3.226166259766" Y="-23.988376953125" />
                  <Point X="3.257388183594" Y="-24.005953125" />
                  <Point X="3.265477783203" Y="-24.01001171875" />
                  <Point X="3.294680908203" Y="-24.02191796875" />
                  <Point X="3.330277832031" Y="-24.03198046875" />
                  <Point X="3.343672607422" Y="-24.034744140625" />
                  <Point X="3.395619140625" Y="-24.041609375" />
                  <Point X="3.437833251953" Y="-24.0471875" />
                  <Point X="3.444033691406" Y="-24.04780078125" />
                  <Point X="3.465709228516" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.136168945312" Y="-23.963529296875" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.70965234375" Y="-23.9090078125" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.409211914062" Y="-24.386466796875" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686020263672" Y="-24.580458984375" />
                  <Point X="3.665627197266" Y="-24.58786328125" />
                  <Point X="3.642386230469" Y="-24.59837890625" />
                  <Point X="3.634008544922" Y="-24.602681640625" />
                  <Point X="3.58297265625" Y="-24.6321796875" />
                  <Point X="3.541498535156" Y="-24.65615234375" />
                  <Point X="3.533880615234" Y="-24.6610546875" />
                  <Point X="3.508770751953" Y="-24.6801328125" />
                  <Point X="3.481992919922" Y="-24.705650390625" />
                  <Point X="3.472795898438" Y="-24.7157734375" />
                  <Point X="3.442174316406" Y="-24.75479296875" />
                  <Point X="3.417290039063" Y="-24.786501953125" />
                  <Point X="3.4108515625" Y="-24.795796875" />
                  <Point X="3.399127929688" Y="-24.815078125" />
                  <Point X="3.393842773438" Y="-24.825064453125" />
                  <Point X="3.384069335938" Y="-24.84652734375" />
                  <Point X="3.380006103516" Y="-24.8570703125" />
                  <Point X="3.373159667969" Y="-24.8785703125" />
                  <Point X="3.370376464844" Y="-24.88952734375" />
                  <Point X="3.360169189453" Y="-24.942826171875" />
                  <Point X="3.351874267578" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584472656" Y="-25.026263671875" />
                  <Point X="3.350280273438" Y="-25.06294140625" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.36208203125" Y="-25.129720703125" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159667969" Y="-25.18398828125" />
                  <Point X="3.380005859375" Y="-25.20548828125" />
                  <Point X="3.384069335938" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.23749609375" />
                  <Point X="3.399127929688" Y="-25.247482421875" />
                  <Point X="3.4108515625" Y="-25.266763671875" />
                  <Point X="3.417290039063" Y="-25.27605859375" />
                  <Point X="3.447911621094" Y="-25.315078125" />
                  <Point X="3.472795898438" Y="-25.346787109375" />
                  <Point X="3.478717529297" Y="-25.3536328125" />
                  <Point X="3.501133300781" Y="-25.37580078125" />
                  <Point X="3.530173828125" Y="-25.398724609375" />
                  <Point X="3.541493896484" Y="-25.406404296875" />
                  <Point X="3.592530029297" Y="-25.435904296875" />
                  <Point X="3.63400390625" Y="-25.459876953125" />
                  <Point X="3.639486816406" Y="-25.4628125" />
                  <Point X="3.659156494141" Y="-25.472017578125" />
                  <Point X="3.683028076172" Y="-25.481029296875" />
                  <Point X="3.6919921875" Y="-25.483916015625" />
                  <Point X="4.274834472656" Y="-25.640087890625" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.76161328125" Y="-25.931052734375" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="4.268555175781" Y="-26.0187578125" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400096923828" Y="-25.90804296875" />
                  <Point X="3.366720703125" Y="-25.910841796875" />
                  <Point X="3.354481689453" Y="-25.912677734375" />
                  <Point X="3.254316162109" Y="-25.93444921875" />
                  <Point X="3.172917480469" Y="-25.952140625" />
                  <Point X="3.157873291016" Y="-25.956744140625" />
                  <Point X="3.128752929688" Y="-25.968369140625" />
                  <Point X="3.114676757812" Y="-25.975390625" />
                  <Point X="3.086849853516" Y="-25.992283203125" />
                  <Point X="3.074125244141" Y="-26.00153125" />
                  <Point X="3.050374755859" Y="-26.022001953125" />
                  <Point X="3.039348876953" Y="-26.033224609375" />
                  <Point X="2.978804931641" Y="-26.106041015625" />
                  <Point X="2.929604736328" Y="-26.165212890625" />
                  <Point X="2.921325683594" Y="-26.17684765625" />
                  <Point X="2.906605957031" Y="-26.201228515625" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.866479736328" Y="-26.390958984375" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607177734" Y="-26.51458984375" />
                  <Point X="2.864065429688" Y="-26.530126953125" />
                  <Point X="2.871796875" Y="-26.561748046875" />
                  <Point X="2.876786132812" Y="-26.57666796875" />
                  <Point X="2.889158203125" Y="-26.60548046875" />
                  <Point X="2.896541015625" Y="-26.619373046875" />
                  <Point X="2.951974121094" Y="-26.70559375" />
                  <Point X="2.997021240234" Y="-26.775662109375" />
                  <Point X="3.001742675781" Y="-26.782353515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486083984" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.593677734375" Y="-27.2513125" />
                  <Point X="4.087170654297" Y="-27.629984375" />
                  <Point X="4.045500488281" Y="-27.6974140625" />
                  <Point X="4.001274414063" Y="-27.760251953125" />
                  <Point X="3.589260742188" Y="-27.522376953125" />
                  <Point X="2.848454345703" Y="-27.094671875" />
                  <Point X="2.841191650391" Y="-27.09088671875" />
                  <Point X="2.815025878906" Y="-27.079515625" />
                  <Point X="2.783120605469" Y="-27.069328125" />
                  <Point X="2.771107421875" Y="-27.066337890625" />
                  <Point X="2.651894287109" Y="-27.04480859375" />
                  <Point X="2.555017089844" Y="-27.0273125" />
                  <Point X="2.539358886719" Y="-27.02580859375" />
                  <Point X="2.508009277344" Y="-27.025404296875" />
                  <Point X="2.492317871094" Y="-27.02650390625" />
                  <Point X="2.460144775391" Y="-27.0314609375" />
                  <Point X="2.444846435547" Y="-27.03513671875" />
                  <Point X="2.415069091797" Y="-27.0449609375" />
                  <Point X="2.400590087891" Y="-27.051109375" />
                  <Point X="2.301553222656" Y="-27.10323046875" />
                  <Point X="2.221072021484" Y="-27.145587890625" />
                  <Point X="2.208969726562" Y="-27.153169921875" />
                  <Point X="2.1860390625" Y="-27.1700625" />
                  <Point X="2.175210693359" Y="-27.179373046875" />
                  <Point X="2.154251220703" Y="-27.20033203125" />
                  <Point X="2.144940185547" Y="-27.21116015625" />
                  <Point X="2.128046386719" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.068341308594" Y="-27.345232421875" />
                  <Point X="2.025984619141" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.44019140625" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564484375" />
                  <Point X="2.002187866211" Y="-27.58014453125" />
                  <Point X="2.023717651367" Y="-27.699357421875" />
                  <Point X="2.041213500977" Y="-27.796236328125" />
                  <Point X="2.043015380859" Y="-27.8042265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.417117431641" Y="-28.47558984375" />
                  <Point X="2.735893066406" Y="-29.027724609375" />
                  <Point X="2.72375390625" Y="-29.036083984375" />
                  <Point X="2.399218505859" Y="-28.613142578125" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828658569336" Y="-27.87015234375" />
                  <Point X="1.808834594727" Y="-27.84962890625" />
                  <Point X="1.783251464844" Y="-27.82800390625" />
                  <Point X="1.773298217773" Y="-27.820646484375" />
                  <Point X="1.655722045898" Y="-27.745056640625" />
                  <Point X="1.560174804688" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517470581055" Y="-27.663873046875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926513672" Y="-27.65115234375" />
                  <Point X="1.455385009766" Y="-27.6486953125" />
                  <Point X="1.424119750977" Y="-27.64637890625" />
                  <Point X="1.408395996094" Y="-27.64651953125" />
                  <Point X="1.279806274414" Y="-27.6583515625" />
                  <Point X="1.175308959961" Y="-27.667966796875" />
                  <Point X="1.161226196289" Y="-27.67033984375" />
                  <Point X="1.133575317383" Y="-27.677171875" />
                  <Point X="1.120007080078" Y="-27.681630859375" />
                  <Point X="1.092621459961" Y="-27.692974609375" />
                  <Point X="1.079875610352" Y="-27.699416015625" />
                  <Point X="1.055493652344" Y="-27.71413671875" />
                  <Point X="1.043857543945" Y="-27.722416015625" />
                  <Point X="0.944563964844" Y="-27.8049765625" />
                  <Point X="0.863873901367" Y="-27.872068359375" />
                  <Point X="0.852654052734" Y="-27.88308984375" />
                  <Point X="0.832183776855" Y="-27.906837890625" />
                  <Point X="0.822933654785" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799018859863" Y="-27.96146875" />
                  <Point X="0.787394165039" Y="-27.99058984375" />
                  <Point X="0.782791748047" Y="-28.0056328125" />
                  <Point X="0.75310357666" Y="-28.142220703125" />
                  <Point X="0.728977478027" Y="-28.25321875" />
                  <Point X="0.727584594727" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.724742370605" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.824053955078" Y="-29.083693359375" />
                  <Point X="0.833091308594" Y="-29.15233984375" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605102539" Y="-28.48012109375" />
                  <Point X="0.642143676758" Y="-28.453576171875" />
                  <Point X="0.62678692627" Y="-28.423814453125" />
                  <Point X="0.620407287598" Y="-28.413208984375" />
                  <Point X="0.530081726074" Y="-28.283068359375" />
                  <Point X="0.4566796875" Y="-28.17730859375" />
                  <Point X="0.446669189453" Y="-28.165171875" />
                  <Point X="0.424786560059" Y="-28.14271875" />
                  <Point X="0.412914123535" Y="-28.13240234375" />
                  <Point X="0.386659118652" Y="-28.113158203125" />
                  <Point X="0.373244140625" Y="-28.104939453125" />
                  <Point X="0.345241210938" Y="-28.090830078125" />
                  <Point X="0.330653106689" Y="-28.084939453125" />
                  <Point X="0.190879669189" Y="-28.041560546875" />
                  <Point X="0.077294021606" Y="-28.006306640625" />
                  <Point X="0.063380245209" Y="-28.003111328125" />
                  <Point X="0.035228092194" Y="-27.998841796875" />
                  <Point X="0.020989723206" Y="-27.997767578125" />
                  <Point X="-0.008651926041" Y="-27.997765625" />
                  <Point X="-0.022896093369" Y="-27.998837890625" />
                  <Point X="-0.051062065125" Y="-28.003107421875" />
                  <Point X="-0.06498387146" Y="-28.0063046875" />
                  <Point X="-0.204757171631" Y="-28.049685546875" />
                  <Point X="-0.318342956543" Y="-28.0849375" />
                  <Point X="-0.332927185059" Y="-28.090828125" />
                  <Point X="-0.360928466797" Y="-28.104935546875" />
                  <Point X="-0.374345550537" Y="-28.11315234375" />
                  <Point X="-0.400600830078" Y="-28.132396484375" />
                  <Point X="-0.412477874756" Y="-28.14271875" />
                  <Point X="-0.434361572266" Y="-28.16517578125" />
                  <Point X="-0.44436819458" Y="-28.177310546875" />
                  <Point X="-0.534693603516" Y="-28.307453125" />
                  <Point X="-0.60809576416" Y="-28.4132109375" />
                  <Point X="-0.612469482422" Y="-28.420130859375" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777832031" Y="-28.47621484375" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.822341064453" Y="-29.15816796875" />
                  <Point X="-0.985425720215" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128849972531" Y="-29.63717008616" />
                  <Point X="-0.961846678743" Y="-29.678808683868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119464926043" Y="-29.541601746269" />
                  <Point X="-0.937255065365" Y="-29.58703176693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.675676346236" Y="-29.055686366082" />
                  <Point X="-2.462114194756" Y="-29.108933390794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.134837697889" Y="-29.439860588978" />
                  <Point X="-0.912663451987" Y="-29.495254849992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884564827539" Y="-28.905696323434" />
                  <Point X="-2.397261392001" Y="-29.027194715795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.155329013435" Y="-29.33684323541" />
                  <Point X="-0.888071838609" Y="-29.403477933054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956753082575" Y="-28.789789475189" />
                  <Point X="-2.301134920546" Y="-28.953253442155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.175820354356" Y="-29.233825875514" />
                  <Point X="-0.863480225231" Y="-29.311701016116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.907338972722" Y="-28.704201501723" />
                  <Point X="-2.194423424323" Y="-28.881951311601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.210379128053" Y="-29.127301110699" />
                  <Point X="-0.838888611853" Y="-29.219924099178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.857924862868" Y="-28.618613528257" />
                  <Point X="-2.08663214186" Y="-28.810918401993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.329529625718" Y="-28.99968526029" />
                  <Point X="-0.814297032178" Y="-29.128147173837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808510753014" Y="-28.533025554791" />
                  <Point X="-1.794560412928" Y="-28.785831768066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.485521146689" Y="-28.862883911118" />
                  <Point X="-0.789705521836" Y="-29.03637023121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75909664316" Y="-28.447437581324" />
                  <Point X="-0.765114011493" Y="-28.944593288582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.709682454337" Y="-28.361849627548" />
                  <Point X="-0.74052250115" Y="-28.852816345954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.815533014041" Y="-27.988221821265" />
                  <Point X="-3.764185283313" Y="-28.001024248418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.66026825266" Y="-28.276261676975" />
                  <Point X="-0.715930990808" Y="-28.761039403327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.831631015176" Y="-29.146889947555" />
                  <Point X="0.832399035939" Y="-29.147081436637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.907511179161" Y="-27.867380794262" />
                  <Point X="-3.645749705182" Y="-27.932645259791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.610854050983" Y="-28.190673726403" />
                  <Point X="-0.691339480465" Y="-28.669262460699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.803518481926" Y="-29.041972410996" />
                  <Point X="0.819071820514" Y="-29.045850293844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.999489480487" Y="-27.7465397333" />
                  <Point X="-3.527314127051" Y="-27.864266271164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.561439849307" Y="-28.105085775831" />
                  <Point X="-0.666747970122" Y="-28.577485518071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.775405948675" Y="-28.937054874438" />
                  <Point X="0.805744482448" Y="-28.944619120472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.069228604288" Y="-27.631243522054" />
                  <Point X="-3.408878548919" Y="-27.795887282537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.51202564763" Y="-28.019497825259" />
                  <Point X="-0.642010213412" Y="-28.485745038759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.747293415424" Y="-28.832137337879" />
                  <Point X="0.792417144381" Y="-28.843387947101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.137814830863" Y="-27.516234760371" />
                  <Point X="-3.290442970788" Y="-27.727508293909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.462611445954" Y="-27.933909874687" />
                  <Point X="-0.598085649894" Y="-28.398788367668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.719180882174" Y="-28.727219801321" />
                  <Point X="0.779089806315" Y="-28.742156773729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.145971100136" Y="-27.416292879255" />
                  <Point X="-3.172007203431" Y="-27.659129352462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413197244277" Y="-27.848321924115" />
                  <Point X="-0.540156125711" Y="-28.31532352545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.691068348923" Y="-28.622302264762" />
                  <Point X="0.765762468249" Y="-28.640925600358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.049666898187" Y="-27.342395918804" />
                  <Point X="-3.05357138364" Y="-27.590750424087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.363783042601" Y="-27.762733973543" />
                  <Point X="-0.482227204419" Y="-28.231858532915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.662955815673" Y="-28.517384728204" />
                  <Point X="0.752435130182" Y="-28.539694426986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.719119106341" Y="-29.030043814986" />
                  <Point X="2.728961591822" Y="-29.032497822234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.953362696238" Y="-27.268498958353" />
                  <Point X="-2.935135563849" Y="-27.522371495712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.322252840793" Y="-27.675180321029" />
                  <Point X="-0.419237393231" Y="-28.149655361949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.616763804234" Y="-28.407959471456" />
                  <Point X="0.739107792116" Y="-28.438463253615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.626217570157" Y="-28.90897256572" />
                  <Point X="2.674244974505" Y="-28.920947142527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.857058494289" Y="-27.194601997902" />
                  <Point X="-2.816699744057" Y="-27.453992567338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311732663076" Y="-27.57989500114" />
                  <Point X="-0.303987294957" Y="-28.080482143991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.534589234463" Y="-28.289562755303" />
                  <Point X="0.725780454049" Y="-28.337232080243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.533316033973" Y="-28.787901316454" />
                  <Point X="2.608212087975" Y="-28.806575000019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.760754271457" Y="-27.120705042658" />
                  <Point X="-2.698263924266" Y="-27.385613638963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.351100527082" Y="-27.472171195443" />
                  <Point X="-0.129053554653" Y="-28.026189729303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.451405762204" Y="-28.170914491506" />
                  <Point X="0.731678025995" Y="-28.24079421529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44041449779" Y="-28.666830067188" />
                  <Point X="2.542179201445" Y="-28.69220285751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.664449731131" Y="-27.046808166574" />
                  <Point X="0.751864996098" Y="-28.147919097441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347513220481" Y="-28.545758882466" />
                  <Point X="2.476146314916" Y="-28.577830715001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568145190806" Y="-26.97291129049" />
                  <Point X="0.772051945026" Y="-28.055043974312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.254612149428" Y="-28.424687749171" />
                  <Point X="2.41011343361" Y="-28.463458573795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.47184065048" Y="-26.899014414405" />
                  <Point X="0.798150973224" Y="-27.963642898101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.161711078376" Y="-28.303616615875" />
                  <Point X="2.344080596337" Y="-28.349086443568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.375536110155" Y="-26.825117538321" />
                  <Point X="0.855711822275" Y="-27.880086134848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.068810007323" Y="-28.182545482579" />
                  <Point X="2.278047759064" Y="-28.23471431334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.279231569829" Y="-26.751220662237" />
                  <Point X="0.945161106662" Y="-27.804480051492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.975908936271" Y="-28.061474349283" />
                  <Point X="2.212014921791" Y="-28.120342183113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.182927029504" Y="-26.677323786153" />
                  <Point X="1.03574922772" Y="-27.729157912008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.883007865218" Y="-27.940403215988" />
                  <Point X="2.145982084518" Y="-28.005970052885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.681594309468" Y="-26.205755771525" />
                  <Point X="-4.462004141214" Y="-26.26050574962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.086622489178" Y="-26.603426910069" />
                  <Point X="1.18093683267" Y="-27.6674489528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.759310342899" Y="-27.811653665003" />
                  <Point X="2.079949247245" Y="-27.891597922657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708304032927" Y="-26.10118799473" />
                  <Point X="-4.205013111549" Y="-26.226672515006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.004079787855" Y="-26.526098822151" />
                  <Point X="2.038905485691" Y="-27.783456268772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735013753929" Y="-25.996620218548" />
                  <Point X="-3.948022066227" Y="-26.192839284297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960688327662" Y="-26.439009233473" />
                  <Point X="2.020389945243" Y="-27.680931531262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752158092639" Y="-25.894437360029" />
                  <Point X="-3.691030882136" Y="-26.159006088186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949211519249" Y="-26.343962428405" />
                  <Point X="2.002024525938" Y="-27.578444223157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766678921007" Y="-25.792908616104" />
                  <Point X="-3.434039698045" Y="-26.125172892076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.977499642138" Y="-26.239001112433" />
                  <Point X="2.007165005644" Y="-27.481817593908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781199749375" Y="-25.691379872179" />
                  <Point X="2.043250578658" Y="-27.39290644297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665635266874" Y="-25.622285039013" />
                  <Point X="2.088801937398" Y="-27.306355377483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.476360148849" Y="-25.571568331389" />
                  <Point X="2.137917363693" Y="-27.220692933842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.772714943777" Y="-27.628293749537" />
                  <Point X="4.046093385319" Y="-27.696454650387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.287084781448" Y="-25.520851685943" />
                  <Point X="2.223741421394" Y="-27.144182979956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.474232817331" Y="-27.455965502278" />
                  <Point X="4.046251294916" Y="-27.598585726884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.097808476549" Y="-25.47013527424" />
                  <Point X="2.349973476152" Y="-27.077747871276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.175752556496" Y="-27.283637720167" />
                  <Point X="3.85723983662" Y="-27.453551582684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.908532171651" Y="-25.419418862538" />
                  <Point X="2.534071654097" Y="-27.025740407522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.87727229566" Y="-27.111309938057" />
                  <Point X="3.668228378324" Y="-27.308517438484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.719255866752" Y="-25.368702450836" />
                  <Point X="3.479216341168" Y="-27.163483149959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.529979561853" Y="-25.317986039133" />
                  <Point X="3.290203926989" Y="-27.018448767431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.399846815197" Y="-25.252523482174" />
                  <Point X="3.10119151281" Y="-26.873414384903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.330929806132" Y="-25.171798127617" />
                  <Point X="2.977016473299" Y="-26.74454577551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.299989591342" Y="-25.08160409479" />
                  <Point X="2.902053455723" Y="-26.627947101262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.299371802166" Y="-24.983849832143" />
                  <Point X="2.862490807916" Y="-26.520174730509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.339105748672" Y="-24.876034751828" />
                  <Point X="2.863574069177" Y="-26.422536523088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.283857050785" Y="-24.5425735017" />
                  <Point X="2.87238149844" Y="-26.326824167048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772805943484" Y="-24.322756556003" />
                  <Point X="2.891954720343" Y="-26.233796024586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.758833682569" Y="-24.228331937124" />
                  <Point X="2.943336077857" Y="-26.14869854105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744861397348" Y="-24.133907324305" />
                  <Point X="3.010765254387" Y="-26.067602228179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.73054459974" Y="-24.039568608071" />
                  <Point X="3.091076036012" Y="-25.98971766018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.705692364824" Y="-23.947856671381" />
                  <Point X="3.258472127649" Y="-25.933545898604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.680840129907" Y="-23.856144734691" />
                  <Point X="3.684567884026" Y="-25.941875207773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.65598789499" Y="-23.764432798001" />
                  <Point X="4.51658018061" Y="-26.051410877233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.551545871436" Y="-23.692564824358" />
                  <Point X="4.743575209044" Y="-26.01009879954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.745650373923" Y="-23.795588844465" />
                  <Point X="4.763700580055" Y="-25.917208323312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.564208287144" Y="-23.742919142805" />
                  <Point X="4.77792725886" Y="-25.822847137937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.483452209112" Y="-23.66514559967" />
                  <Point X="3.572699486169" Y="-25.424441809613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.44228351983" Y="-23.57750181196" />
                  <Point X="3.428731886716" Y="-25.290638360779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421487563994" Y="-23.484778531308" />
                  <Point X="3.371772841831" Y="-25.178528581085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441275060705" Y="-23.381936659483" />
                  <Point X="3.351781572857" Y="-25.07563590313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.50684274822" Y="-23.267680504116" />
                  <Point X="3.353407462851" Y="-24.978132988247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.678682296778" Y="-23.126927797876" />
                  <Point X="3.371588026434" Y="-24.884757617067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.86769338669" Y="-22.981893745525" />
                  <Point X="3.410407023456" Y="-24.796527985279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.0567060143" Y="-22.836859309782" />
                  <Point X="3.473979924377" Y="-24.714470194912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.222836274603" Y="-22.697530088981" />
                  <Point X="3.569003655011" Y="-24.640253977106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.172948635889" Y="-22.61206017952" />
                  <Point X="3.702888151317" Y="-24.575726836393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.123060655541" Y="-22.526590355238" />
                  <Point X="3.89216381522" Y="-24.525010264872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.073172294731" Y="-22.441120625815" />
                  <Point X="4.081439479122" Y="-24.474293693352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.023283933922" Y="-22.355650896393" />
                  <Point X="4.270715143025" Y="-24.423577121831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.965338514903" Y="-22.272190017203" />
                  <Point X="4.459990671436" Y="-24.372860516529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.901541770084" Y="-22.190188037388" />
                  <Point X="3.167357216746" Y="-23.952662504074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.530665380477" Y="-24.043245402954" />
                  <Point X="4.649265830302" Y="-24.322143819088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.837745025266" Y="-22.108186057573" />
                  <Point X="3.065376190583" Y="-23.829327483705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.787655629797" Y="-24.009411973779" />
                  <Point X="4.779289810149" Y="-24.256654143517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.773948291575" Y="-22.026184074984" />
                  <Point X="-3.503590640291" Y="-22.093591808232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.920709281448" Y="-22.238920453327" />
                  <Point X="3.019678388519" Y="-23.720025447194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.044645879118" Y="-23.975578544604" />
                  <Point X="4.763430569674" Y="-24.154791695975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.834469360097" Y="-22.162514185894" />
                  <Point X="3.001498521729" Y="-23.617584402527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.301635705283" Y="-23.941745009925" />
                  <Point X="4.744503843114" Y="-24.052164438253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.77232911252" Y="-22.080099194931" />
                  <Point X="3.011973714788" Y="-23.522287866703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.558625297393" Y="-23.907911416889" />
                  <Point X="4.719127754306" Y="-23.947929173922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.749014545913" Y="-21.988003874472" />
                  <Point X="3.040501873744" Y="-23.431492440812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75465125967" Y="-21.8886901891" />
                  <Point X="3.093204357998" Y="-23.346724351168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.781087088263" Y="-21.784190701965" />
                  <Point X="3.161808113639" Y="-23.265920893761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.845719632389" Y="-21.670167704031" />
                  <Point X="2.147060249969" Y="-22.915007540735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.52708977249" Y="-23.009759542607" />
                  <Point X="3.256941365523" Y="-23.191731982669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.911752800311" Y="-21.555795491363" />
                  <Point X="2.054588337434" Y="-22.794043408675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.676086963564" Y="-22.949000419898" />
                  <Point X="3.353245557572" Y="-23.11783501975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.977786005486" Y="-21.441423269407" />
                  <Point X="2.020304510323" Y="-22.687587195744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.794522920235" Y="-22.880621525651" />
                  <Point X="3.449549749621" Y="-23.043938056831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.043819289543" Y="-21.327051027784" />
                  <Point X="2.013449553916" Y="-22.587969768365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.912958876906" Y="-22.812242631405" />
                  <Point X="3.545853941669" Y="-22.970041093911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.985743001025" Y="-21.243622778025" />
                  <Point X="2.031875041915" Y="-22.494655463701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.031394833577" Y="-22.743863737158" />
                  <Point X="3.642158133718" Y="-22.896144130992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.889378843388" Y="-21.169740766206" />
                  <Point X="2.063572670158" Y="-22.404650275257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.149830790248" Y="-22.675484842912" />
                  <Point X="3.738462325767" Y="-22.822247168073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.79301395658" Y="-21.09585893619" />
                  <Point X="2.112865108306" Y="-22.319031965628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.26826674692" Y="-22.607105948665" />
                  <Point X="3.834766673978" Y="-22.748350244089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.696649069771" Y="-21.021977106174" />
                  <Point X="2.162279290248" Y="-22.233444010135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386702711422" Y="-22.538727056371" />
                  <Point X="3.931071284008" Y="-22.674453385384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.587670406588" Y="-20.951240243829" />
                  <Point X="2.21169347219" Y="-22.147856054643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.505138679458" Y="-22.470348164958" />
                  <Point X="4.027375894037" Y="-22.600556526679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466031826123" Y="-20.883659853377" />
                  <Point X="2.261107654133" Y="-22.06226809915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.623574647495" Y="-22.401969273545" />
                  <Point X="4.123680504067" Y="-22.526659667974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.344393245659" Y="-20.816079462925" />
                  <Point X="-2.046693484854" Y="-20.890304349733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.764436452654" Y="-20.96067893186" />
                  <Point X="2.310521836075" Y="-21.976680143658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.742010615531" Y="-22.333590382132" />
                  <Point X="4.076676148832" Y="-22.417031871171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.222755257195" Y="-20.74849892487" />
                  <Point X="-2.169435902588" Y="-20.761792933067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.61744348677" Y="-20.899420099687" />
                  <Point X="2.359936018017" Y="-21.891092188165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.860446583567" Y="-22.265211490719" />
                  <Point X="3.998361944116" Y="-22.299597652126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.534308176984" Y="-20.822239765654" />
                  <Point X="2.409350199959" Y="-21.805504232673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.495821334192" Y="-20.733927318515" />
                  <Point X="2.458764381902" Y="-21.719916277181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468550679303" Y="-20.642818361646" />
                  <Point X="2.508178563844" Y="-21.634328321688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.464987765874" Y="-20.545798400948" />
                  <Point X="-0.111308206189" Y="-20.883308622053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.015344621959" Y="-20.91488671875" />
                  <Point X="2.557592791508" Y="-21.548740377595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.478315371774" Y="-20.444567160798" />
                  <Point X="-0.217728156288" Y="-20.758866853644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.165396700178" Y="-20.854390608947" />
                  <Point X="2.607007106817" Y="-21.463152455355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.30761126549" Y="-20.389220179906" />
                  <Point X="-0.248434221725" Y="-20.653302676886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.224449451312" Y="-20.771205818661" />
                  <Point X="2.656421422126" Y="-21.377564533115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.122769966536" Y="-20.337397997029" />
                  <Point X="-0.276546845065" Y="-20.548385117865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.253450903333" Y="-20.680528397985" />
                  <Point X="2.705835737435" Y="-21.291976610874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.937928436224" Y="-20.285575871836" />
                  <Point X="-0.304659468405" Y="-20.443467558845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.278042529143" Y="-20.588751484146" />
                  <Point X="2.755250052745" Y="-21.206388688634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.673705309157" Y="-20.253545801624" />
                  <Point X="-0.332772089061" Y="-20.338550000494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.302634154952" Y="-20.496974570308" />
                  <Point X="2.804664368054" Y="-21.120800766394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.406464632933" Y="-20.222268090918" />
                  <Point X="-0.360884560403" Y="-20.233632479371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.327225758692" Y="-20.405197650967" />
                  <Point X="2.662037866003" Y="-20.987331690697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.351817176755" Y="-20.313420685331" />
                  <Point X="2.380839204529" Y="-20.819312695241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.376408594818" Y="-20.221643719696" />
                  <Point X="2.001067315819" Y="-20.626716633905" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998373046875" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.692532226562" Y="-29.361869140625" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.521544921875" />
                  <Point X="0.373993377686" Y="-28.391404296875" />
                  <Point X="0.300591186523" Y="-28.28564453125" />
                  <Point X="0.274336212158" Y="-28.266400390625" />
                  <Point X="0.134562774658" Y="-28.223021484375" />
                  <Point X="0.020977081299" Y="-28.187767578125" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.14843788147" Y="-28.231146484375" />
                  <Point X="-0.262023590088" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.37860446167" Y="-28.41578515625" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.638815185547" Y="-29.20734375" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-0.96734185791" Y="-29.96386328125" />
                  <Point X="-1.100258056641" Y="-29.9380625" />
                  <Point X="-1.261917236328" Y="-29.896470703125" />
                  <Point X="-1.35158996582" Y="-29.8733984375" />
                  <Point X="-1.335752807617" Y="-29.753103515625" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.343075073242" Y="-29.36688671875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282470703" Y="-29.20262890625" />
                  <Point X="-1.514968139648" Y="-29.089775390625" />
                  <Point X="-1.619543334961" Y="-28.998064453125" />
                  <Point X="-1.649240356445" Y="-28.985763671875" />
                  <Point X="-1.82003503418" Y="-28.9745703125" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.132193359375" Y="-29.0688828125" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.360558837891" Y="-29.288693359375" />
                  <Point X="-2.457094238281" Y="-29.4145" />
                  <Point X="-2.661025634766" Y="-29.28823046875" />
                  <Point X="-2.855839599609" Y="-29.167607421875" />
                  <Point X="-3.079701904297" Y="-28.995240234375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.916724609375" Y="-28.34045703125" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.597591796875" />
                  <Point X="-2.513980712891" Y="-27.56876171875" />
                  <Point X="-2.531329345703" Y="-27.5514140625" />
                  <Point X="-2.560158203125" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.169745605469" Y="-27.87721484375" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.007794189453" Y="-28.0493359375" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.32219921875" Y="-27.57800390625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.882362304688" Y="-26.97452734375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140325927734" Y="-26.33459765625" />
                  <Point X="-3.161157470703" Y="-26.310640625" />
                  <Point X="-3.187645263672" Y="-26.29505078125" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.949277832031" Y="-26.38464453125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.867197753906" Y="-26.246853515625" />
                  <Point X="-4.927393066406" Y="-26.01119140625" />
                  <Point X="-4.969855957031" Y="-25.71429296875" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-4.376029785156" Y="-25.34798046875" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.526586669922" Y="-25.11080078125" />
                  <Point X="-3.502325439453" Y="-25.0906484375" />
                  <Point X="-3.489795898438" Y="-25.05946484375" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.490751464844" Y="-25.000015625" />
                  <Point X="-3.502325439453" Y="-24.9719140625" />
                  <Point X="-3.529456542969" Y="-24.94976953125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.222664550781" Y="-24.755673828125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.956439453125" Y="-24.265751953125" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.83216796875" Y="-23.688146484375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.340034179687" Y="-23.528771484375" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731703613281" Y="-23.6041328125" />
                  <Point X="-3.697816162109" Y="-23.59344921875" />
                  <Point X="-3.670277587891" Y="-23.584765625" />
                  <Point X="-3.651533691406" Y="-23.573943359375" />
                  <Point X="-3.639119628906" Y="-23.556212890625" />
                  <Point X="-3.625521972656" Y="-23.52338671875" />
                  <Point X="-3.614472167969" Y="-23.496708984375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.632723144531" Y="-23.422970703125" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.041528808594" Y="-23.08799609375" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.310756347656" Y="-22.47125" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.933588623047" Y="-21.92195703125" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.519533447266" Y="-21.8649921875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514648438" Y="-22.07956640625" />
                  <Point X="-3.091318603516" Y="-22.0836953125" />
                  <Point X="-3.052965332031" Y="-22.08705078125" />
                  <Point X="-3.031507080078" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.979752929688" Y="-22.039095703125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.942203369141" Y="-21.924962890625" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.1211484375" Y="-21.573111328125" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-3.015419677734" Y="-21.026958984375" />
                  <Point X="-2.752872314453" Y="-20.825666015625" />
                  <Point X="-2.396263183594" Y="-20.627541015625" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.078148925781" Y="-20.56865234375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247558594" Y="-20.726337890625" />
                  <Point X="-1.89871862793" Y="-20.75368359375" />
                  <Point X="-1.85603137207" Y="-20.77590625" />
                  <Point X="-1.83512487793" Y="-20.781509765625" />
                  <Point X="-1.813809448242" Y="-20.77775" />
                  <Point X="-1.759096923828" Y="-20.7550859375" />
                  <Point X="-1.714635498047" Y="-20.736669921875" />
                  <Point X="-1.696905395508" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.668275390625" Y="-20.649033203125" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.671140258789" Y="-20.435564453125" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-1.307973144531" Y="-20.19199609375" />
                  <Point X="-0.968082885742" Y="-20.096703125" />
                  <Point X="-0.535770996094" Y="-20.04610546875" />
                  <Point X="-0.224200180054" Y="-20.009640625" />
                  <Point X="-0.148749725342" Y="-20.2912265625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282100677" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.03659405899" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.141087509155" Y="-20.365771484375" />
                  <Point X="0.236648452759" Y="-20.009130859375" />
                  <Point X="0.563440551758" Y="-20.04335546875" />
                  <Point X="0.860209777832" Y="-20.074435546875" />
                  <Point X="1.217878173828" Y="-20.160787109375" />
                  <Point X="1.508455688477" Y="-20.230943359375" />
                  <Point X="1.741574707031" Y="-20.31549609375" />
                  <Point X="1.931043701172" Y="-20.38421875" />
                  <Point X="2.156150146484" Y="-20.4894921875" />
                  <Point X="2.338685302734" Y="-20.574859375" />
                  <Point X="2.556171630859" Y="-20.70156640625" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.937628173828" Y="-20.950169921875" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.705197265625" Y="-21.67308203125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.213007568359" Y="-22.552779296875" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.206663085938" Y="-22.645974609375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218681884766" Y="-22.6991875" />
                  <Point X="2.242380859375" Y="-22.73411328125" />
                  <Point X="2.261639648438" Y="-22.76249609375" />
                  <Point X="2.274938964844" Y="-22.775794921875" />
                  <Point X="2.309864990234" Y="-22.799494140625" />
                  <Point X="2.338247558594" Y="-22.81875390625" />
                  <Point X="2.360335693359" Y="-22.827021484375" />
                  <Point X="2.398635986328" Y="-22.831638671875" />
                  <Point X="2.429760498047" Y="-22.835392578125" />
                  <Point X="2.4486640625" Y="-22.8340546875" />
                  <Point X="2.492956542969" Y="-22.822208984375" />
                  <Point X="2.528950439453" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.210097412109" Y="-22.421298828125" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.097985839844" Y="-22.1127421875" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.316934570312" Y="-22.447072265625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.914450439453" Y="-22.926697265625" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.247493896484" Y="-23.45775390625" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.201245117188" Y="-23.550958984375" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.200527099609" Y="-23.65627734375" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.242158935547" Y="-23.752341796875" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280946777344" Y="-23.8011796875" />
                  <Point X="3.319366943359" Y="-23.822806640625" />
                  <Point X="3.350588867188" Y="-23.8403828125" />
                  <Point X="3.368566650391" Y="-23.846380859375" />
                  <Point X="3.420513183594" Y="-23.85324609375" />
                  <Point X="3.462727294922" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.111369140625" Y="-23.775154296875" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.894260742188" Y="-23.86406640625" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.975221191406" Y="-24.280046875" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.458387695312" Y="-24.569994140625" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.678051269531" Y="-24.7966796875" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.622264648438" Y="-24.83307421875" />
                  <Point X="3.591643066406" Y="-24.87209375" />
                  <Point X="3.566758789062" Y="-24.903802734375" />
                  <Point X="3.556985351562" Y="-24.925265625" />
                  <Point X="3.546778076172" Y="-24.978564453125" />
                  <Point X="3.538483154297" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.548690429688" Y="-25.093982421875" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.597380371094" Y="-25.19777734375" />
                  <Point X="3.622264648438" Y="-25.229486328125" />
                  <Point X="3.636576904297" Y="-25.241908203125" />
                  <Point X="3.687613037109" Y="-25.271408203125" />
                  <Point X="3.729086914062" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.324010253906" Y="-25.456560546875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.973517578125" Y="-25.80001171875" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.902268066406" Y="-26.1686953125" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.243755371094" Y="-26.2071328125" />
                  <Point X="3.411981933594" Y="-26.09762890625" />
                  <Point X="3.394836669922" Y="-26.098341796875" />
                  <Point X="3.294671142578" Y="-26.12011328125" />
                  <Point X="3.213272460938" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.124901611328" Y="-26.227513671875" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.055680419922" Y="-26.408369140625" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.111793457031" Y="-26.60284375" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.709342285156" Y="-27.10057421875" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.274845214844" Y="-27.687716796875" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.108662109375" Y="-27.937791015625" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.494260986328" Y="-27.686921875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.618127685547" Y="-27.23178515625" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489077392578" Y="-27.21924609375" />
                  <Point X="2.390040527344" Y="-27.2713671875" />
                  <Point X="2.309559326172" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.236477539062" Y="-27.433720703125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.210692871094" Y="-27.665587890625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.581662353516" Y="-28.38058984375" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.919207275391" Y="-29.13027734375" />
                  <Point X="2.835291259766" Y="-29.190216796875" />
                  <Point X="2.728557617188" Y="-29.2593046875" />
                  <Point X="2.679775634766" Y="-29.290880859375" />
                  <Point X="2.248481201172" Y="-28.728806640625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549194336" Y="-27.980466796875" />
                  <Point X="1.552973022461" Y="-27.904876953125" />
                  <Point X="1.45742578125" Y="-27.84344921875" />
                  <Point X="1.425805053711" Y="-27.83571875" />
                  <Point X="1.297215454102" Y="-27.84755078125" />
                  <Point X="1.192718261719" Y="-27.857166015625" />
                  <Point X="1.165332641602" Y="-27.868509765625" />
                  <Point X="1.0660390625" Y="-27.9510703125" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.938768493652" Y="-28.182576171875" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.012428405762" Y="-29.058892578125" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.073729248047" Y="-29.94584765625" />
                  <Point X="0.994369689941" Y="-29.9632421875" />
                  <Point X="0.895737976074" Y="-29.98116015625" />
                  <Point X="0.860200378418" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#172" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.106609374343" Y="4.752996095487" Z="1.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.4" />
                  <Point X="-0.547739060359" Y="5.034834628843" Z="1.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.4" />
                  <Point X="-1.327626913789" Y="4.887429855699" Z="1.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.4" />
                  <Point X="-1.72686849711" Y="4.589190881874" Z="1.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.4" />
                  <Point X="-1.722117291954" Y="4.397283259761" Z="1.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.4" />
                  <Point X="-1.784386680358" Y="4.322387374726" Z="1.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.4" />
                  <Point X="-1.88178625045" Y="4.321946287265" Z="1.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.4" />
                  <Point X="-2.044637379235" Y="4.493066048266" Z="1.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.4" />
                  <Point X="-2.426701805349" Y="4.447445585722" Z="1.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.4" />
                  <Point X="-3.051997752229" Y="4.044001900013" Z="1.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.4" />
                  <Point X="-3.170605723859" Y="3.433169839907" Z="1.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.4" />
                  <Point X="-2.998169165875" Y="3.101959518461" Z="1.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.4" />
                  <Point X="-3.02126352379" Y="3.027540044096" Z="1.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.4" />
                  <Point X="-3.093116947145" Y="2.997395533298" Z="1.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.4" />
                  <Point X="-3.50068950839" Y="3.209588224887" Z="1.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.4" />
                  <Point X="-3.97920829997" Y="3.140027088315" Z="1.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.4" />
                  <Point X="-4.360094327911" Y="2.585234704526" Z="1.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.4" />
                  <Point X="-4.078122848383" Y="1.903615802694" Z="1.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.4" />
                  <Point X="-3.683229350657" Y="1.585221779797" Z="1.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.4" />
                  <Point X="-3.67787236177" Y="1.527027492502" Z="1.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.4" />
                  <Point X="-3.719008416809" Y="1.485517260433" Z="1.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.4" />
                  <Point X="-4.339664003161" Y="1.552082021973" Z="1.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.4" />
                  <Point X="-4.886583773817" Y="1.356212398948" Z="1.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.4" />
                  <Point X="-5.012057007773" Y="0.772847385504" Z="1.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.4" />
                  <Point X="-4.241761203174" Y="0.227308962878" Z="1.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.4" />
                  <Point X="-3.564118597031" Y="0.040433477307" Z="1.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.4" />
                  <Point X="-3.544660408605" Y="0.016443931866" Z="1.4" />
                  <Point X="-3.539556741714" Y="0" Z="1.4" />
                  <Point X="-3.54370419253" Y="-0.013363019195" Z="1.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.4" />
                  <Point X="-3.561249998092" Y="-0.038442505827" Z="1.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.4" />
                  <Point X="-4.395126585598" Y="-0.268403093878" Z="1.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.4" />
                  <Point X="-5.02550821904" Y="-0.69009263967" Z="1.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.4" />
                  <Point X="-4.921798668998" Y="-1.227947404549" Z="1.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.4" />
                  <Point X="-3.948907465691" Y="-1.402936512406" Z="1.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.4" />
                  <Point X="-3.207286246072" Y="-1.313851028259" Z="1.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.4" />
                  <Point X="-3.196130377033" Y="-1.335786109845" Z="1.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.4" />
                  <Point X="-3.9189563711" Y="-1.903579573936" Z="1.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.4" />
                  <Point X="-4.371298507939" Y="-2.572332342331" Z="1.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.4" />
                  <Point X="-4.053736220787" Y="-3.048337967092" Z="1.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.4" />
                  <Point X="-3.150901598947" Y="-2.889235275267" Z="1.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.4" />
                  <Point X="-2.565061825756" Y="-2.563268643375" Z="1.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.4" />
                  <Point X="-2.966181737902" Y="-3.284176783545" Z="1.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.4" />
                  <Point X="-3.116361635248" Y="-4.003577363632" Z="1.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.4" />
                  <Point X="-2.693503129646" Y="-4.299462563005" Z="1.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.4" />
                  <Point X="-2.327047167291" Y="-4.287849688674" Z="1.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.4" />
                  <Point X="-2.110571120553" Y="-4.079176433275" Z="1.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.4" />
                  <Point X="-1.829461333909" Y="-3.993181460465" Z="1.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.4" />
                  <Point X="-1.554091594976" Y="-4.096086096045" Z="1.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.4" />
                  <Point X="-1.398270382738" Y="-4.345360234246" Z="1.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.4" />
                  <Point X="-1.39148088268" Y="-4.715297033687" Z="1.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.4" />
                  <Point X="-1.280532404753" Y="-4.913611495702" Z="1.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.4" />
                  <Point X="-0.983025532825" Y="-4.981666486745" Z="1.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.4" />
                  <Point X="-0.596674943702" Y="-4.189005277928" Z="1.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.4" />
                  <Point X="-0.343684559131" Y="-3.413014472663" Z="1.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.4" />
                  <Point X="-0.139773418293" Y="-3.247619879356" Z="1.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.4" />
                  <Point X="0.113585661068" Y="-3.239492165932" Z="1.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.4" />
                  <Point X="0.326761300301" Y="-3.388631283105" Z="1.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.4" />
                  <Point X="0.6380799167" Y="-4.343530751887" Z="1.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.4" />
                  <Point X="0.898518840537" Y="-4.999075318521" Z="1.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.4" />
                  <Point X="1.078278435028" Y="-4.963406617075" Z="1.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.4" />
                  <Point X="1.05584466756" Y="-4.021086907199" Z="1.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.4" />
                  <Point X="0.981471720589" Y="-3.161915460299" Z="1.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.4" />
                  <Point X="1.091850124276" Y="-2.958234834558" Z="1.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.4" />
                  <Point X="1.295640923948" Y="-2.866059514941" Z="1.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.4" />
                  <Point X="1.519777720602" Y="-2.915654651899" Z="1.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.4" />
                  <Point X="2.20265754462" Y="-3.72796317627" Z="1.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.4" />
                  <Point X="2.74957058136" Y="-4.269998078885" Z="1.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.4" />
                  <Point X="2.942113008207" Y="-4.139685175149" Z="1.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.4" />
                  <Point X="2.618807640535" Y="-3.324308558053" Z="1.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.4" />
                  <Point X="2.253740900582" Y="-2.625421195733" Z="1.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.4" />
                  <Point X="2.274567870989" Y="-2.425726877999" Z="1.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.4" />
                  <Point X="2.407171500373" Y="-2.284333438978" Z="1.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.4" />
                  <Point X="2.60308560778" Y="-2.249707108909" Z="1.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.4" />
                  <Point X="3.463105039054" Y="-2.69894185974" Z="1.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.4" />
                  <Point X="4.143394715437" Y="-2.935287874953" Z="1.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.4" />
                  <Point X="4.311222969993" Y="-2.682720779103" Z="1.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.4" />
                  <Point X="3.733624196702" Y="-2.029625958744" Z="1.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.4" />
                  <Point X="3.147695599955" Y="-1.544525031676" Z="1.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.4" />
                  <Point X="3.099314092104" Y="-1.381671230682" Z="1.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.4" />
                  <Point X="3.157191797441" Y="-1.228199496147" Z="1.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.4" />
                  <Point X="3.299134248192" Y="-1.137691834563" Z="1.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.4" />
                  <Point X="4.231073266393" Y="-1.225425462574" Z="1.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.4" />
                  <Point X="4.944859043342" Y="-1.148539829739" Z="1.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.4" />
                  <Point X="5.016802805053" Y="-0.776185969034" Z="1.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.4" />
                  <Point X="4.33079523899" Y="-0.376982817434" Z="1.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.4" />
                  <Point X="3.706478725945" Y="-0.196837936667" Z="1.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.4" />
                  <Point X="3.630558330461" Y="-0.135629654144" Z="1.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.4" />
                  <Point X="3.591641928966" Y="-0.053298205175" Z="1.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.4" />
                  <Point X="3.589729521458" Y="0.043312326031" Z="1.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.4" />
                  <Point X="3.624821107939" Y="0.128319084431" Z="1.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.4" />
                  <Point X="3.696916688408" Y="0.191310907575" Z="1.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.4" />
                  <Point X="4.465172578316" Y="0.412989115334" Z="1.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.4" />
                  <Point X="5.018469901685" Y="0.758925395024" Z="1.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.4" />
                  <Point X="4.936685888448" Y="1.179041022674" Z="1.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.4" />
                  <Point X="4.098686987039" Y="1.30569790394" Z="1.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.4" />
                  <Point X="3.420907351054" Y="1.227603185832" Z="1.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.4" />
                  <Point X="3.337738764863" Y="1.252043778901" Z="1.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.4" />
                  <Point X="3.277773441818" Y="1.306418651478" Z="1.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.4" />
                  <Point X="3.24333966623" Y="1.385107206223" Z="1.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.4" />
                  <Point X="3.243241627859" Y="1.466853843086" Z="1.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.4" />
                  <Point X="3.281020825565" Y="1.543108607303" Z="1.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.4" />
                  <Point X="3.938732779548" Y="2.064914878594" Z="1.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.4" />
                  <Point X="4.353555889004" Y="2.610094092344" Z="1.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.4" />
                  <Point X="4.132415321535" Y="2.947741829066" Z="1.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.4" />
                  <Point X="3.178941517548" Y="2.653282793881" Z="1.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.4" />
                  <Point X="2.473884071255" Y="2.257373543742" Z="1.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.4" />
                  <Point X="2.398467176181" Y="2.249282210551" Z="1.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.4" />
                  <Point X="2.331784294938" Y="2.273159206516" Z="1.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.4" />
                  <Point X="2.277599393719" Y="2.325240565444" Z="1.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.4" />
                  <Point X="2.250147492208" Y="2.391291267651" Z="1.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.4" />
                  <Point X="2.255154398915" Y="2.465585504627" Z="1.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.4" />
                  <Point X="2.742342558979" Y="3.333197564407" Z="1.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.4" />
                  <Point X="2.960449282218" Y="4.121859568368" Z="1.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.4" />
                  <Point X="2.575185672225" Y="4.372917082729" Z="1.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.4" />
                  <Point X="2.171175719584" Y="4.587078569254" Z="1.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.4" />
                  <Point X="1.752467420256" Y="4.762788058308" Z="1.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.4" />
                  <Point X="1.223456814604" Y="4.919096030794" Z="1.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.4" />
                  <Point X="0.562492508346" Y="5.037652910583" Z="1.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.4" />
                  <Point X="0.08663518721" Y="4.678451410464" Z="1.4" />
                  <Point X="0" Y="4.355124473572" Z="1.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>