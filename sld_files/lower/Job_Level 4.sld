<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#123" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="471" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.587158630371" Y="-28.60155859375" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.553965576172" Y="-28.48987890625" />
                  <Point X="0.536994506836" Y="-28.46026171875" />
                  <Point X="0.532612304688" Y="-28.453326171875" />
                  <Point X="0.378635528564" Y="-28.231474609375" />
                  <Point X="0.357108612061" Y="-28.20751953125" />
                  <Point X="0.324800323486" Y="-28.185798828125" />
                  <Point X="0.294973968506" Y="-28.173693359375" />
                  <Point X="0.287406921387" Y="-28.17098828125" />
                  <Point X="0.049136642456" Y="-28.097037109375" />
                  <Point X="0.020403022766" Y="-28.09159375" />
                  <Point X="-0.015286241531" Y="-28.092970703125" />
                  <Point X="-0.045195781708" Y="-28.099900390625" />
                  <Point X="-0.051912509918" Y="-28.10171875" />
                  <Point X="-0.290182769775" Y="-28.17566796875" />
                  <Point X="-0.319508972168" Y="-28.188984375" />
                  <Point X="-0.350144073486" Y="-28.2134921875" />
                  <Point X="-0.371412506104" Y="-28.239361328125" />
                  <Point X="-0.376073974609" Y="-28.245525390625" />
                  <Point X="-0.53005078125" Y="-28.467375" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.91658416748" Y="-29.87694140625" />
                  <Point X="-1.079317993164" Y="-29.845353515625" />
                  <Point X="-1.092730712891" Y="-29.841904296875" />
                  <Point X="-1.246417724609" Y="-29.802361328125" />
                  <Point X="-1.217834960938" Y="-29.58525390625" />
                  <Point X="-1.214963134766" Y="-29.56344140625" />
                  <Point X="-1.214830078125" Y="-29.539697265625" />
                  <Point X="-1.218967651367" Y="-29.50529296875" />
                  <Point X="-1.22011340332" Y="-29.498103515625" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287037231445" Y="-29.1817578125" />
                  <Point X="-1.308026977539" Y="-29.14880078125" />
                  <Point X="-1.332252563477" Y="-29.12402734375" />
                  <Point X="-1.337536499023" Y="-29.1190234375" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583204223633" Y="-28.908791015625" />
                  <Point X="-1.619925537109" Y="-28.89541796875" />
                  <Point X="-1.654227294922" Y="-28.89051171875" />
                  <Point X="-1.661465576172" Y="-28.8897578125" />
                  <Point X="-1.952616943359" Y="-28.87067578125" />
                  <Point X="-1.984347045898" Y="-28.872525390625" />
                  <Point X="-2.021616699219" Y="-28.88426953125" />
                  <Point X="-2.051817382813" Y="-28.9012578125" />
                  <Point X="-2.058020751953" Y="-28.90506640625" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312788574219" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.480148193359" Y="-29.28848828125" />
                  <Point X="-2.801719726562" Y="-29.089380859375" />
                  <Point X="-2.820241943359" Y="-29.075119140625" />
                  <Point X="-3.104721923828" Y="-28.856080078125" />
                  <Point X="-2.468096191406" Y="-27.75341015625" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406587890625" Y="-27.616125" />
                  <Point X="-2.405664794922" Y="-27.584529296875" />
                  <Point X="-2.415158447266" Y="-27.554380859375" />
                  <Point X="-2.430409667969" Y="-27.52451953125" />
                  <Point X="-2.447840820312" Y="-27.500552734375" />
                  <Point X="-2.46415625" Y="-27.48423828125" />
                  <Point X="-2.489312011719" Y="-27.466212890625" />
                  <Point X="-2.518140869141" Y="-27.45199609375" />
                  <Point X="-2.547759277344" Y="-27.44301171875" />
                  <Point X="-2.578693603516" Y="-27.444025390625" />
                  <Point X="-2.610219482422" Y="-27.450296875" />
                  <Point X="-2.639184082031" Y="-27.46119921875" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.082863037109" Y="-27.793857421875" />
                  <Point X="-4.096145996094" Y="-27.771583984375" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.183526855469" Y="-26.558037109375" />
                  <Point X="-3.105955078125" Y="-26.498515625" />
                  <Point X="-3.083679199219" Y="-26.474212890625" />
                  <Point X="-3.065255126953" Y="-26.445310546875" />
                  <Point X="-3.053397705078" Y="-26.4180625" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.043348144531" Y="-26.35965234375" />
                  <Point X="-3.045559326172" Y="-26.3279765625" />
                  <Point X="-3.052883544922" Y="-26.297466796875" />
                  <Point X="-3.069747314453" Y="-26.271005859375" />
                  <Point X="-3.092166015625" Y="-26.24612109375" />
                  <Point X="-3.114559570312" Y="-26.2278359375" />
                  <Point X="-3.139454101562" Y="-26.21318359375" />
                  <Point X="-3.168716064453" Y="-26.201958984375" />
                  <Point X="-3.200603027344" Y="-26.1954765625" />
                  <Point X="-3.231928710938" Y="-26.194384765625" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.837592285156" Y="-25.96808203125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.621241943359" Y="-25.244087890625" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.515947509766" Y="-25.21409375" />
                  <Point X="-3.486074951172" Y="-25.198322265625" />
                  <Point X="-3.459974853516" Y="-25.18020703125" />
                  <Point X="-3.436541015625" Y="-25.156962890625" />
                  <Point X="-3.416746337891" Y="-25.128931640625" />
                  <Point X="-3.403617431641" Y="-25.10229296875" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.390662597656" Y="-25.0444375" />
                  <Point X="-3.391213134766" Y="-25.013021484375" />
                  <Point X="-3.395467529297" Y="-24.98652734375" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.419357666016" Y="-24.929044921875" />
                  <Point X="-3.440254394531" Y="-24.901642578125" />
                  <Point X="-3.461627685547" Y="-24.88120703125" />
                  <Point X="-3.487727783203" Y="-24.863091796875" />
                  <Point X="-3.501923339844" Y="-24.854953125" />
                  <Point X="-3.532875732422" Y="-24.842150390625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.817411132812" Y="-23.99691015625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-3.829008300781" Y="-23.691869140625" />
                  <Point X="-3.765665283203" Y="-23.70020703125" />
                  <Point X="-3.744995605469" Y="-23.700658203125" />
                  <Point X="-3.723447998047" Y="-23.698775390625" />
                  <Point X="-3.703182128906" Y="-23.69475" />
                  <Point X="-3.699477783203" Y="-23.693583984375" />
                  <Point X="-3.641709960938" Y="-23.675369140625" />
                  <Point X="-3.622778808594" Y="-23.6670390625" />
                  <Point X="-3.604037109375" Y="-23.65621875" />
                  <Point X="-3.587358398438" Y="-23.643990234375" />
                  <Point X="-3.573721191406" Y="-23.62844140625" />
                  <Point X="-3.561306884766" Y="-23.61071484375" />
                  <Point X="-3.551362792969" Y="-23.592595703125" />
                  <Point X="-3.549883056641" Y="-23.589025390625" />
                  <Point X="-3.526703613281" Y="-23.533064453125" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.53205078125" Y="-23.41062109375" />
                  <Point X="-3.533822021484" Y="-23.40721875" />
                  <Point X="-3.561790527344" Y="-23.3534921875" />
                  <Point X="-3.573282470703" Y="-23.33629296875" />
                  <Point X="-3.5871953125" Y="-23.319712890625" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-4.351859375" Y="-22.730125" />
                  <Point X="-4.081154541016" Y="-22.26634375" />
                  <Point X="-4.062411132812" Y="-22.242251953125" />
                  <Point X="-3.750503173828" Y="-21.841337890625" />
                  <Point X="-3.245560302734" Y="-22.1328671875" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187729492188" Y="-22.163658203125" />
                  <Point X="-3.167087402344" Y="-22.17016796875" />
                  <Point X="-3.146786621094" Y="-22.174205078125" />
                  <Point X="-3.141691650391" Y="-22.174650390625" />
                  <Point X="-3.061237304688" Y="-22.181689453125" />
                  <Point X="-3.040567138672" Y="-22.18123828125" />
                  <Point X="-3.019112304688" Y="-22.1784140625" />
                  <Point X="-2.999024902344" Y="-22.1735" />
                  <Point X="-2.980476806641" Y="-22.16435546875" />
                  <Point X="-2.962224365234" Y="-22.15273046875" />
                  <Point X="-2.946096679688" Y="-22.139791015625" />
                  <Point X="-2.942460449219" Y="-22.13615625" />
                  <Point X="-2.885353271484" Y="-22.079048828125" />
                  <Point X="-2.872406738281" Y="-22.062916015625" />
                  <Point X="-2.860777587891" Y="-22.044662109375" />
                  <Point X="-2.851628662109" Y="-22.026111328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.843881591797" Y="-21.95878515625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854956054688" Y="-21.858041015625" />
                  <Point X="-2.86146484375" Y="-21.8373984375" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-3.183332519531" Y="-21.275404296875" />
                  <Point X="-2.700622558594" Y="-20.90531640625" />
                  <Point X="-2.671110351562" Y="-20.888919921875" />
                  <Point X="-2.167036621094" Y="-20.608865234375" />
                  <Point X="-2.055104003906" Y="-20.754740234375" />
                  <Point X="-2.043194458008" Y="-20.770259765625" />
                  <Point X="-2.028893310547" Y="-20.78519921875" />
                  <Point X="-2.012321777344" Y="-20.79910546875" />
                  <Point X="-1.995135742188" Y="-20.810591796875" />
                  <Point X="-1.989442993164" Y="-20.813556640625" />
                  <Point X="-1.899897460938" Y="-20.860171875" />
                  <Point X="-1.880614257812" Y="-20.867669921875" />
                  <Point X="-1.859704956055" Y="-20.873271484375" />
                  <Point X="-1.839261108398" Y="-20.87641796875" />
                  <Point X="-1.81862109375" Y="-20.875064453125" />
                  <Point X="-1.797303466797" Y="-20.8713046875" />
                  <Point X="-1.777439208984" Y="-20.86551171875" />
                  <Point X="-1.771532836914" Y="-20.863064453125" />
                  <Point X="-1.678279785156" Y="-20.8244375" />
                  <Point X="-1.660146118164" Y="-20.814490234375" />
                  <Point X="-1.642416381836" Y="-20.802076171875" />
                  <Point X="-1.626864135742" Y="-20.7884375" />
                  <Point X="-1.614632568359" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595479980469" Y="-20.734078125" />
                  <Point X="-1.593557495117" Y="-20.72798046875" />
                  <Point X="-1.563200561523" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584201904297" Y="-20.368103515625" />
                  <Point X="-0.949625366211" Y="-20.19019140625" />
                  <Point X="-0.913858764648" Y="-20.18600390625" />
                  <Point X="-0.294711273193" Y="-20.113541015625" />
                  <Point X="-0.145412063599" Y="-20.670734375" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113998413" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425949097" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.307419464111" Y="-20.1120625" />
                  <Point X="0.844041748047" Y="-20.16826171875" />
                  <Point X="0.873642700195" Y="-20.175408203125" />
                  <Point X="1.481026855469" Y="-20.32205078125" />
                  <Point X="1.498905029297" Y="-20.328533203125" />
                  <Point X="1.894653442383" Y="-20.47207421875" />
                  <Point X="1.913282592773" Y="-20.480787109375" />
                  <Point X="2.294550048828" Y="-20.659091796875" />
                  <Point X="2.312609619141" Y="-20.669615234375" />
                  <Point X="2.680980957031" Y="-20.884228515625" />
                  <Point X="2.697958984375" Y="-20.896302734375" />
                  <Point X="2.943259521484" Y="-21.07074609375" />
                  <Point X="2.198895263672" Y="-22.3600234375" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.140825439453" Y="-22.463251953125" />
                  <Point X="2.131798095703" Y="-22.4887265625" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.1084453125" Y="-22.585314453125" />
                  <Point X="2.107606201172" Y="-22.6083515625" />
                  <Point X="2.1082265625" Y="-22.62318359375" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121440185547" Y="-22.710390625" />
                  <Point X="2.129702392578" Y="-22.73247265625" />
                  <Point X="2.140061279297" Y="-22.752513671875" />
                  <Point X="2.142630126953" Y="-22.75630078125" />
                  <Point X="2.183029541016" Y="-22.815837890625" />
                  <Point X="2.196981689453" Y="-22.83209765625" />
                  <Point X="2.214051269531" Y="-22.847955078125" />
                  <Point X="2.225367675781" Y="-22.85696484375" />
                  <Point X="2.284906005859" Y="-22.897365234375" />
                  <Point X="2.304955078125" Y="-22.907728515625" />
                  <Point X="2.327050048828" Y="-22.91599609375" />
                  <Point X="2.34896484375" Y="-22.921337890625" />
                  <Point X="2.353093261719" Y="-22.9218359375" />
                  <Point X="2.418383544922" Y="-22.929708984375" />
                  <Point X="2.440196044922" Y="-22.929818359375" />
                  <Point X="2.463880371094" Y="-22.927201171875" />
                  <Point X="2.477988525391" Y="-22.92455078125" />
                  <Point X="2.553493408203" Y="-22.904359375" />
                  <Point X="2.565287353516" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.967325195312" Y="-22.09380859375" />
                  <Point X="4.123274414062" Y="-22.310541015625" />
                  <Point X="4.132739257812" Y="-22.32618359375" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.298343505859" Y="-23.27970703125" />
                  <Point X="3.230783447266" Y="-23.331548828125" />
                  <Point X="3.218820800781" Y="-23.34246875" />
                  <Point X="3.200531982422" Y="-23.362861328125" />
                  <Point X="3.146191162109" Y="-23.43375390625" />
                  <Point X="3.135047363281" Y="-23.45236328125" />
                  <Point X="3.125296142578" Y="-23.4738984375" />
                  <Point X="3.120347900391" Y="-23.487498046875" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.098791748047" Y="-23.63333203125" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.122241455078" Y="-23.734583984375" />
                  <Point X="3.132301513672" Y="-23.756310546875" />
                  <Point X="3.13914453125" Y="-23.768609375" />
                  <Point X="3.184340087891" Y="-23.8373046875" />
                  <Point X="3.198895751953" Y="-23.85455078125" />
                  <Point X="3.216145019531" Y="-23.87064453125" />
                  <Point X="3.234364990234" Y="-23.8839765625" />
                  <Point X="3.238512695312" Y="-23.886310546875" />
                  <Point X="3.303987548828" Y="-23.92316796875" />
                  <Point X="3.324498535156" Y="-23.93173046875" />
                  <Point X="3.348090087891" Y="-23.93846875" />
                  <Point X="3.361732910156" Y="-23.941302734375" />
                  <Point X="3.450278320312" Y="-23.953005859375" />
                  <Point X="3.462697998047" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.848919921875" Y="-24.08635546875" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.794005126953" Y="-24.6496640625" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.701063232422" Y="-24.6760546875" />
                  <Point X="3.676036865234" Y="-24.6881171875" />
                  <Point X="3.589036376953" Y="-24.738404296875" />
                  <Point X="3.571361083984" Y="-24.75157421875" />
                  <Point X="3.553743164062" Y="-24.76820703125" />
                  <Point X="3.544225097656" Y="-24.778634765625" />
                  <Point X="3.492024902344" Y="-24.845150390625" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463681152344" Y="-24.90739453125" />
                  <Point X="3.462579101562" Y="-24.9131484375" />
                  <Point X="3.445178955078" Y="-25.004005859375" />
                  <Point X="3.443578369141" Y="-25.026134765625" />
                  <Point X="3.444680419922" Y="-25.0506953125" />
                  <Point X="3.446280517578" Y="-25.064306640625" />
                  <Point X="3.463680664062" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.1766640625" />
                  <Point X="3.480300292969" Y="-25.19812890625" />
                  <Point X="3.492020263672" Y="-25.217404296875" />
                  <Point X="3.495325927734" Y="-25.2216171875" />
                  <Point X="3.547526123047" Y="-25.2881328125" />
                  <Point X="3.563505371094" Y="-25.30413671875" />
                  <Point X="3.583333007812" Y="-25.319744140625" />
                  <Point X="3.594550537109" Y="-25.32734375" />
                  <Point X="3.681544433594" Y="-25.37762890625" />
                  <Point X="3.692709228516" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.851199707031" Y="-25.965478515625" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.514173339844" Y="-26.01526171875" />
                  <Point X="3.424381591797" Y="-26.00344140625" />
                  <Point X="3.401637451172" Y="-26.003193359375" />
                  <Point X="3.373678955078" Y="-26.006255859375" />
                  <Point X="3.363845703125" Y="-26.007859375" />
                  <Point X="3.193094482422" Y="-26.04497265625" />
                  <Point X="3.162729980469" Y="-26.055689453125" />
                  <Point X="3.131485595703" Y="-26.0760390625" />
                  <Point X="3.112449707031" Y="-26.09467578125" />
                  <Point X="3.105861572266" Y="-26.101822265625" />
                  <Point X="3.002653320312" Y="-26.22594921875" />
                  <Point X="2.986627197266" Y="-26.250416015625" />
                  <Point X="2.974509765625" Y="-26.282392578125" />
                  <Point X="2.969977294922" Y="-26.307125" />
                  <Point X="2.968820800781" Y="-26.315544921875" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.955109130859" Y="-26.508484375" />
                  <Point X="2.965241455078" Y="-26.544662109375" />
                  <Point X="2.977737060547" Y="-26.5691328125" />
                  <Point X="2.982434570312" Y="-26.5773046875" />
                  <Point X="3.076930664062" Y="-26.724287109375" />
                  <Point X="3.086931640625" Y="-26.737236328125" />
                  <Point X="3.110628662109" Y="-26.76091015625" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.124814941406" Y="-27.749779296875" />
                  <Point X="4.116907226562" Y="-27.761013671875" />
                  <Point X="4.028980224609" Y="-27.8859453125" />
                  <Point X="2.880926269531" Y="-27.2231171875" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.77939453125" Y="-27.167826171875" />
                  <Point X="2.750411376953" Y="-27.159599609375" />
                  <Point X="2.741354980469" Y="-27.157501953125" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.505974853516" Y="-27.119083984375" />
                  <Point X="2.468744628906" Y="-27.1261640625" />
                  <Point X="2.442381835938" Y="-27.1369609375" />
                  <Point X="2.434142333984" Y="-27.1408046875" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.241146240234" Y="-27.24612890625" />
                  <Point X="2.217397949219" Y="-27.271525390625" />
                  <Point X="2.202880126953" Y="-27.29428515625" />
                  <Point X="2.198905273438" Y="-27.301130859375" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.098733398438" Y="-27.500109375" />
                  <Point X="2.094302734375" Y="-27.538046875" />
                  <Point X="2.096839355469" Y="-27.56741796875" />
                  <Point X="2.097999511719" Y="-27.576126953125" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138986328125" Y="-27.795146484375" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.781839599609" Y="-29.11165234375" />
                  <Point X="2.773014892578" Y="-29.11736328125" />
                  <Point X="2.701764404297" Y="-29.163482421875" />
                  <Point X="1.819571655273" Y="-28.013787109375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.741662109375" Y="-27.917224609375" />
                  <Point X="1.716340820313" Y="-27.897443359375" />
                  <Point X="1.709230957031" Y="-27.892396484375" />
                  <Point X="1.508800170898" Y="-27.7635390625" />
                  <Point X="1.479748535156" Y="-27.749646484375" />
                  <Point X="1.442078735352" Y="-27.7419375" />
                  <Point X="1.411740234375" Y="-27.74199609375" />
                  <Point X="1.403219116211" Y="-27.742396484375" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.155378173828" Y="-27.76853515625" />
                  <Point X="1.123302978516" Y="-27.783205078125" />
                  <Point X="1.099751220703" Y="-27.799865234375" />
                  <Point X="1.093876831055" Y="-27.804373046875" />
                  <Point X="0.924611938477" Y="-27.945111328125" />
                  <Point X="0.90261114502" Y="-27.96864453125" />
                  <Point X="0.88383605957" Y="-28.002529296875" />
                  <Point X="0.874513061523" Y="-28.032599609375" />
                  <Point X="0.872419433594" Y="-28.0405546875" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.32312109375" />
                  <Point X="1.022065002441" Y="-29.859916015625" />
                  <Point X="0.97567364502" Y="-29.870083984375" />
                  <Point X="0.967506103516" Y="-29.871568359375" />
                  <Point X="0.929315917969" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058434570312" Y="-29.7526328125" />
                  <Point X="-1.06905859375" Y="-29.749900390625" />
                  <Point X="-1.14124609375" Y="-29.731326171875" />
                  <Point X="-1.123647705078" Y="-29.597654296875" />
                  <Point X="-1.120775878906" Y="-29.575841796875" />
                  <Point X="-1.119964599609" Y="-29.563974609375" />
                  <Point X="-1.119831542969" Y="-29.54023046875" />
                  <Point X="-1.120509765625" Y="-29.528353515625" />
                  <Point X="-1.124647338867" Y="-29.49394921875" />
                  <Point X="-1.126938842773" Y="-29.4795703125" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.186859863281" Y="-29.182046875" />
                  <Point X="-1.196860961914" Y="-29.15187109375" />
                  <Point X="-1.206908203125" Y="-29.130724609375" />
                  <Point X="-1.227897827148" Y="-29.097767578125" />
                  <Point X="-1.240104980469" Y="-29.082380859375" />
                  <Point X="-1.264330566406" Y="-29.057607421875" />
                  <Point X="-1.2748984375" Y="-29.047599609375" />
                  <Point X="-1.494267456055" Y="-28.855216796875" />
                  <Point X="-1.50355456543" Y="-28.84803515625" />
                  <Point X="-1.529853271484" Y="-28.830185546875" />
                  <Point X="-1.550695922852" Y="-28.819525390625" />
                  <Point X="-1.587417480469" Y="-28.80615234375" />
                  <Point X="-1.606474243164" Y="-28.801375" />
                  <Point X="-1.640776123047" Y="-28.79646875" />
                  <Point X="-1.655252685547" Y="-28.7949609375" />
                  <Point X="-1.946404052734" Y="-28.77587890625" />
                  <Point X="-1.958145263672" Y="-28.7758359375" />
                  <Point X="-1.989875366211" Y="-28.777685546875" />
                  <Point X="-2.012898803711" Y="-28.78191796875" />
                  <Point X="-2.050168457031" Y="-28.793662109375" />
                  <Point X="-2.068192382813" Y="-28.801470703125" />
                  <Point X="-2.098393066406" Y="-28.818458984375" />
                  <Point X="-2.110799804687" Y="-28.826076171875" />
                  <Point X="-2.353402832031" Y="-28.988177734375" />
                  <Point X="-2.3596875" Y="-28.992759765625" />
                  <Point X="-2.380446533203" Y="-29.010134765625" />
                  <Point X="-2.402760986328" Y="-29.0327734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.503201171875" Y="-29.162478515625" />
                  <Point X="-2.747603271484" Y="-29.01115234375" />
                  <Point X="-2.762283935547" Y="-28.99984765625" />
                  <Point X="-2.980862792969" Y="-28.83155078125" />
                  <Point X="-2.385823730469" Y="-27.80091015625" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413085938" Y="-27.634658203125" />
                  <Point X="-2.311628417969" Y="-27.6188984375" />
                  <Point X="-2.310705322266" Y="-27.587302734375" />
                  <Point X="-2.315051269531" Y="-27.55599609375" />
                  <Point X="-2.324544921875" Y="-27.52584765625" />
                  <Point X="-2.330554199219" Y="-27.511169921875" />
                  <Point X="-2.345805419922" Y="-27.48130859375" />
                  <Point X="-2.353580810547" Y="-27.468642578125" />
                  <Point X="-2.371011962891" Y="-27.44467578125" />
                  <Point X="-2.380667724609" Y="-27.433375" />
                  <Point X="-2.396983154297" Y="-27.417060546875" />
                  <Point X="-2.408822753906" Y="-27.407015625" />
                  <Point X="-2.433978515625" Y="-27.388990234375" />
                  <Point X="-2.447294677734" Y="-27.381009765625" />
                  <Point X="-2.476123535156" Y="-27.36679296875" />
                  <Point X="-2.490564697266" Y="-27.3610859375" />
                  <Point X="-2.520183105469" Y="-27.3521015625" />
                  <Point X="-2.550870605469" Y="-27.3480625" />
                  <Point X="-2.581804931641" Y="-27.349076171875" />
                  <Point X="-2.597229003906" Y="-27.3508515625" />
                  <Point X="-2.628754882813" Y="-27.357123046875" />
                  <Point X="-2.643685546875" Y="-27.36138671875" />
                  <Point X="-2.672650146484" Y="-27.3722890625" />
                  <Point X="-2.686684082031" Y="-27.378927734375" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-4.004022216797" Y="-27.7405859375" />
                  <Point X="-4.014553466797" Y="-27.72292578125" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.125694580078" Y="-26.63340625" />
                  <Point X="-3.048122802734" Y="-26.573884765625" />
                  <Point X="-3.035923095703" Y="-26.56270703125" />
                  <Point X="-3.013647216797" Y="-26.538404296875" />
                  <Point X="-3.003571044922" Y="-26.525279296875" />
                  <Point X="-2.985146972656" Y="-26.496376953125" />
                  <Point X="-2.978145751953" Y="-26.483216796875" />
                  <Point X="-2.966288330078" Y="-26.45596875" />
                  <Point X="-2.961432128906" Y="-26.441880859375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552490234" Y="-26.39880078125" />
                  <Point X="-2.948748779297" Y="-26.3683671875" />
                  <Point X="-2.948578857422" Y="-26.353037109375" />
                  <Point X="-2.950790039062" Y="-26.321361328125" />
                  <Point X="-2.953183837891" Y="-26.30580078125" />
                  <Point X="-2.960508056641" Y="-26.275291015625" />
                  <Point X="-2.972770019531" Y="-26.24641015625" />
                  <Point X="-2.989633789062" Y="-26.21994921875" />
                  <Point X="-2.999166015625" Y="-26.207419921875" />
                  <Point X="-3.021584716797" Y="-26.18253515625" />
                  <Point X="-3.032081054688" Y="-26.17253515625" />
                  <Point X="-3.054474609375" Y="-26.15425" />
                  <Point X="-3.066371826172" Y="-26.14596484375" />
                  <Point X="-3.091266357422" Y="-26.1313125" />
                  <Point X="-3.105430175781" Y="-26.124484375" />
                  <Point X="-3.134692138672" Y="-26.113259765625" />
                  <Point X="-3.149790283203" Y="-26.10886328125" />
                  <Point X="-3.181677246094" Y="-26.102380859375" />
                  <Point X="-3.197293945312" Y="-26.10053515625" />
                  <Point X="-3.228619628906" Y="-26.099443359375" />
                  <Point X="-3.244328613281" Y="-26.100197265625" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.740762695312" Y="-25.974115234375" />
                  <Point X="-4.743549316406" Y="-25.954630859375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.596654052734" Y="-25.3358515625" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.499665771484" Y="-25.309416015625" />
                  <Point X="-3.471593505859" Y="-25.298103515625" />
                  <Point X="-3.441720947266" Y="-25.28233203125" />
                  <Point X="-3.431907226562" Y="-25.276365234375" />
                  <Point X="-3.405807128906" Y="-25.25825" />
                  <Point X="-3.393073242188" Y="-25.247654296875" />
                  <Point X="-3.369639404297" Y="-25.22441015625" />
                  <Point X="-3.358939453125" Y="-25.21176171875" />
                  <Point X="-3.339144775391" Y="-25.18373046875" />
                  <Point X="-3.331533447266" Y="-25.1709296875" />
                  <Point X="-3.318404541016" Y="-25.144291015625" />
                  <Point X="-3.312886962891" Y="-25.130453125" />
                  <Point X="-3.304187011719" Y="-25.102421875" />
                  <Point X="-3.300869873047" Y="-25.0876796875" />
                  <Point X="-3.296614990234" Y="-25.05785546875" />
                  <Point X="-3.295677246094" Y="-25.0427734375" />
                  <Point X="-3.296227783203" Y="-25.011357421875" />
                  <Point X="-3.297414794922" Y="-24.997958984375" />
                  <Point X="-3.301669189453" Y="-24.97146484375" />
                  <Point X="-3.304736572266" Y="-24.958369140625" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.319737548828" Y="-24.9149453125" />
                  <Point X="-3.334927490234" Y="-24.88549609375" />
                  <Point X="-3.343816650391" Y="-24.8714375" />
                  <Point X="-3.364713378906" Y="-24.84403515625" />
                  <Point X="-3.374602539062" Y="-24.832978515625" />
                  <Point X="-3.395975830078" Y="-24.81254296875" />
                  <Point X="-3.407459960938" Y="-24.8031640625" />
                  <Point X="-3.433560058594" Y="-24.785048828125" />
                  <Point X="-3.440476806641" Y="-24.78067578125" />
                  <Point X="-3.465612304688" Y="-24.767166015625" />
                  <Point X="-3.496564697266" Y="-24.75436328125" />
                  <Point X="-3.508287841797" Y="-24.75038671875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.73133203125" Y="-24.0424765625" />
                  <Point X="-4.725717773438" Y="-24.0217578125" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-3.841408447266" Y="-23.786056640625" />
                  <Point X="-3.778065429688" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747068847656" Y="-23.795634765625" />
                  <Point X="-3.736726074219" Y="-23.795296875" />
                  <Point X="-3.715178466797" Y="-23.7934140625" />
                  <Point X="-3.704939697266" Y="-23.791955078125" />
                  <Point X="-3.684673828125" Y="-23.7879296875" />
                  <Point X="-3.670909667969" Y="-23.7841875" />
                  <Point X="-3.613141845703" Y="-23.76597265625" />
                  <Point X="-3.603448486328" Y="-23.76232421875" />
                  <Point X="-3.584517333984" Y="-23.753994140625" />
                  <Point X="-3.575279541016" Y="-23.7493125" />
                  <Point X="-3.556537841797" Y="-23.7384921875" />
                  <Point X="-3.547865234375" Y="-23.73283203125" />
                  <Point X="-3.531186523438" Y="-23.720603515625" />
                  <Point X="-3.515936523438" Y="-23.706630859375" />
                  <Point X="-3.502299316406" Y="-23.69108203125" />
                  <Point X="-3.495906005859" Y="-23.6829375" />
                  <Point X="-3.483491699219" Y="-23.6652109375" />
                  <Point X="-3.478024902344" Y="-23.656421875" />
                  <Point X="-3.468080810547" Y="-23.638302734375" />
                  <Point X="-3.462114257812" Y="-23.62537890625" />
                  <Point X="-3.438934814453" Y="-23.56941796875" />
                  <Point X="-3.435498291016" Y="-23.559646484375" />
                  <Point X="-3.429710205078" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436012939453" Y="-23.395466796875" />
                  <Point X="-3.443510742188" Y="-23.3761875" />
                  <Point X="-3.449556884766" Y="-23.3633515625" />
                  <Point X="-3.477525390625" Y="-23.309625" />
                  <Point X="-3.482800537109" Y="-23.300712890625" />
                  <Point X="-3.494292480469" Y="-23.283513671875" />
                  <Point X="-3.500509277344" Y="-23.2752265625" />
                  <Point X="-3.514422119141" Y="-23.258646484375" />
                  <Point X="-3.521501220703" Y="-23.251087890625" />
                  <Point X="-3.536441894531" Y="-23.23678515625" />
                  <Point X="-3.544303466797" Y="-23.230041015625" />
                  <Point X="-4.227613769531" Y="-22.705716796875" />
                  <Point X="-4.002294677734" Y="-22.31969140625" />
                  <Point X="-3.987430664062" Y="-22.3005859375" />
                  <Point X="-3.726336669922" Y="-21.964986328125" />
                  <Point X="-3.293060302734" Y="-22.215138671875" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244924804688" Y="-22.242279296875" />
                  <Point X="-3.225997314453" Y="-22.250609375" />
                  <Point X="-3.216302001953" Y="-22.254259765625" />
                  <Point X="-3.195659912109" Y="-22.26076953125" />
                  <Point X="-3.185616699219" Y="-22.26334375" />
                  <Point X="-3.165315917969" Y="-22.267380859375" />
                  <Point X="-3.149963378906" Y="-22.2692890625" />
                  <Point X="-3.069509033203" Y="-22.276328125" />
                  <Point X="-3.059164306641" Y="-22.276666015625" />
                  <Point X="-3.038494140625" Y="-22.27621484375" />
                  <Point X="-3.028168701172" Y="-22.27542578125" />
                  <Point X="-3.006713867188" Y="-22.2726015625" />
                  <Point X="-2.996537841797" Y="-22.270693359375" />
                  <Point X="-2.976450439453" Y="-22.265779296875" />
                  <Point X="-2.957016357422" Y="-22.25870703125" />
                  <Point X="-2.938468261719" Y="-22.2495625" />
                  <Point X="-2.929442871094" Y="-22.244484375" />
                  <Point X="-2.911190429688" Y="-22.232859375" />
                  <Point X="-2.902773925781" Y="-22.226830078125" />
                  <Point X="-2.886646240234" Y="-22.213890625" />
                  <Point X="-2.87528515625" Y="-22.20333203125" />
                  <Point X="-2.818177978516" Y="-22.146224609375" />
                  <Point X="-2.811260986328" Y="-22.1385078125" />
                  <Point X="-2.798314453125" Y="-22.122375" />
                  <Point X="-2.792284912109" Y="-22.113958984375" />
                  <Point X="-2.780655761719" Y="-22.095705078125" />
                  <Point X="-2.775575927734" Y="-22.086681640625" />
                  <Point X="-2.766427001953" Y="-22.068130859375" />
                  <Point X="-2.759351318359" Y="-22.04869140625" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.749243164062" Y="-21.950505859375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.76178125" Y="-21.8395078125" />
                  <Point X="-2.764353271484" Y="-21.82947265625" />
                  <Point X="-2.770862060547" Y="-21.808830078125" />
                  <Point X="-2.774510498047" Y="-21.79913671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522705078" Y="-21.770966796875" />
                  <Point X="-3.059386474609" Y="-21.3000859375" />
                  <Point X="-2.648372070312" Y="-20.98496484375" />
                  <Point X="-2.624972412109" Y="-20.97196484375" />
                  <Point X="-2.192524414062" Y="-20.731705078125" />
                  <Point X="-2.130472900391" Y="-20.812572265625" />
                  <Point X="-2.111819580078" Y="-20.835953125" />
                  <Point X="-2.097518554688" Y="-20.850892578125" />
                  <Point X="-2.0899609375" Y="-20.857970703125" />
                  <Point X="-2.073389404297" Y="-20.871876953125" />
                  <Point X="-2.065110351563" Y="-20.878087890625" />
                  <Point X="-2.047924438477" Y="-20.88957421875" />
                  <Point X="-2.033309814453" Y="-20.897822265625" />
                  <Point X="-1.943764160156" Y="-20.9444375" />
                  <Point X="-1.934325805664" Y="-20.948712890625" />
                  <Point X="-1.915042724609" Y="-20.9562109375" />
                  <Point X="-1.905197753906" Y="-20.95943359375" />
                  <Point X="-1.884288452148" Y="-20.96503515625" />
                  <Point X="-1.87415612793" Y="-20.967166015625" />
                  <Point X="-1.853712158203" Y="-20.9703125" />
                  <Point X="-1.833044677734" Y="-20.97121484375" />
                  <Point X="-1.812404663086" Y="-20.969861328125" />
                  <Point X="-1.802120727539" Y="-20.96862109375" />
                  <Point X="-1.780803100586" Y="-20.964861328125" />
                  <Point X="-1.770706787109" Y="-20.962505859375" />
                  <Point X="-1.750842529297" Y="-20.956712890625" />
                  <Point X="-1.735177734375" Y="-20.95083203125" />
                  <Point X="-1.641924682617" Y="-20.912205078125" />
                  <Point X="-1.63258996582" Y="-20.907728515625" />
                  <Point X="-1.614456420898" Y="-20.89778125" />
                  <Point X="-1.605657592773" Y="-20.892310546875" />
                  <Point X="-1.587927734375" Y="-20.879896484375" />
                  <Point X="-1.579779296875" Y="-20.873501953125" />
                  <Point X="-1.564227050781" Y="-20.85986328125" />
                  <Point X="-1.550252075195" Y="-20.844611328125" />
                  <Point X="-1.538020507812" Y="-20.8279296875" />
                  <Point X="-1.532359863281" Y="-20.819255859375" />
                  <Point X="-1.521538085938" Y="-20.80051171875" />
                  <Point X="-1.516856079102" Y="-20.791271484375" />
                  <Point X="-1.508525146484" Y="-20.772337890625" />
                  <Point X="-1.502953979492" Y="-20.756546875" />
                  <Point X="-1.472597045898" Y="-20.660267578125" />
                  <Point X="-1.470025878906" Y="-20.650234375" />
                  <Point X="-1.465990966797" Y="-20.62994921875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-0.931167358398" Y="-20.2836796875" />
                  <Point X="-0.902811767578" Y="-20.280359375" />
                  <Point X="-0.365222473145" Y="-20.21744140625" />
                  <Point X="-0.237175094604" Y="-20.695322265625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.378190612793" Y="-20.2149921875" />
                  <Point X="0.827863647461" Y="-20.2620859375" />
                  <Point X="0.85134765625" Y="-20.267755859375" />
                  <Point X="1.453608886719" Y="-20.413162109375" />
                  <Point X="1.466522216797" Y="-20.41784375" />
                  <Point X="1.858260986328" Y="-20.5599296875" />
                  <Point X="1.87303527832" Y="-20.56683984375" />
                  <Point X="2.250414306641" Y="-20.743326171875" />
                  <Point X="2.264780273438" Y="-20.751697265625" />
                  <Point X="2.629445800781" Y="-20.96415234375" />
                  <Point X="2.642901367188" Y="-20.973720703125" />
                  <Point X="2.817779541016" Y="-21.098083984375" />
                  <Point X="2.116622802734" Y="-22.3125234375" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.061629882812" Y="-22.4084375" />
                  <Point X="2.051281494141" Y="-22.431521484375" />
                  <Point X="2.042254150391" Y="-22.45699609375" />
                  <Point X="2.040022827148" Y="-22.464185546875" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017657592773" Y="-22.550142578125" />
                  <Point X="2.01449609375" Y="-22.571224609375" />
                  <Point X="2.013508300781" Y="-22.58185546875" />
                  <Point X="2.012669189453" Y="-22.604892578125" />
                  <Point X="2.012689208984" Y="-22.612322265625" />
                  <Point X="2.013909667969" Y="-22.634556640625" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.023799926758" Y="-22.710962890625" />
                  <Point X="2.029140869141" Y="-22.732880859375" />
                  <Point X="2.032464355469" Y="-22.743681640625" />
                  <Point X="2.04072644043" Y="-22.765763671875" />
                  <Point X="2.045309448242" Y="-22.77609375" />
                  <Point X="2.055668212891" Y="-22.796134765625" />
                  <Point X="2.06401953125" Y="-22.809642578125" />
                  <Point X="2.104418945313" Y="-22.8691796875" />
                  <Point X="2.11093359375" Y="-22.877701171875" />
                  <Point X="2.124885742188" Y="-22.8939609375" />
                  <Point X="2.132323242188" Y="-22.90169921875" />
                  <Point X="2.149392822266" Y="-22.917556640625" />
                  <Point X="2.154879150391" Y="-22.922275390625" />
                  <Point X="2.172025634766" Y="-22.935576171875" />
                  <Point X="2.231563964844" Y="-22.9759765625" />
                  <Point X="2.241283935547" Y="-22.9817578125" />
                  <Point X="2.261333007812" Y="-22.99212109375" />
                  <Point X="2.271662109375" Y="-22.996703125" />
                  <Point X="2.293757080078" Y="-23.004970703125" />
                  <Point X="2.304552246094" Y="-23.00829296875" />
                  <Point X="2.326467041016" Y="-23.013634765625" />
                  <Point X="2.341715087891" Y="-23.01615234375" />
                  <Point X="2.407005371094" Y="-23.024025390625" />
                  <Point X="2.417907226562" Y="-23.02470703125" />
                  <Point X="2.439719726562" Y="-23.02481640625" />
                  <Point X="2.450630371094" Y="-23.024244140625" />
                  <Point X="2.474314697266" Y="-23.021626953125" />
                  <Point X="2.481420410156" Y="-23.020568359375" />
                  <Point X="2.502531005859" Y="-23.016326171875" />
                  <Point X="2.578035888672" Y="-22.996134765625" />
                  <Point X="2.584006103516" Y="-22.994326171875" />
                  <Point X="2.604410888672" Y="-22.9869296875" />
                  <Point X="2.627657470703" Y="-22.976423828125" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.940403076172" Y="-22.219046875" />
                  <Point X="4.043955078125" Y="-22.3629609375" />
                  <Point X="4.051459716797" Y="-22.37536328125" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.240511230469" Y="-23.204337890625" />
                  <Point X="3.172951171875" Y="-23.2561796875" />
                  <Point X="3.166735839844" Y="-23.261384765625" />
                  <Point X="3.148096679688" Y="-23.279041015625" />
                  <Point X="3.129807861328" Y="-23.29943359375" />
                  <Point X="3.125134277344" Y="-23.30506640625" />
                  <Point X="3.070793457031" Y="-23.375958984375" />
                  <Point X="3.064687255859" Y="-23.384947265625" />
                  <Point X="3.053543457031" Y="-23.403556640625" />
                  <Point X="3.048505859375" Y="-23.413177734375" />
                  <Point X="3.038754638672" Y="-23.434712890625" />
                  <Point X="3.036021972656" Y="-23.441416015625" />
                  <Point X="3.028858154297" Y="-23.461912109375" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.005751708984" Y="-23.652529296875" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.025188232422" Y="-23.743619140625" />
                  <Point X="3.032021240234" Y="-23.764337890625" />
                  <Point X="3.036034423828" Y="-23.7745" />
                  <Point X="3.046094482422" Y="-23.7962265625" />
                  <Point X="3.049286132812" Y="-23.8025" />
                  <Point X="3.059780517578" Y="-23.82082421875" />
                  <Point X="3.104976074219" Y="-23.88951953125" />
                  <Point X="3.111741210938" Y="-23.898578125" />
                  <Point X="3.126296875" Y="-23.91582421875" />
                  <Point X="3.134087402344" Y="-23.92401171875" />
                  <Point X="3.151336669922" Y="-23.94010546875" />
                  <Point X="3.160045654297" Y="-23.9473125" />
                  <Point X="3.178265625" Y="-23.96064453125" />
                  <Point X="3.191924560547" Y="-23.969103515625" />
                  <Point X="3.257385986328" Y="-24.005953125" />
                  <Point X="3.267389892578" Y="-24.0108359375" />
                  <Point X="3.287900878906" Y="-24.0193984375" />
                  <Point X="3.298407714844" Y="-24.023078125" />
                  <Point X="3.321999267578" Y="-24.02981640625" />
                  <Point X="3.328768554688" Y="-24.031482421875" />
                  <Point X="3.349284912109" Y="-24.035484375" />
                  <Point X="3.437830322266" Y="-24.0471875" />
                  <Point X="3.444032226562" Y="-24.04780078125" />
                  <Point X="3.465708251953" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.75505078125" Y="-24.100970703125" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="3.769417236328" Y="-24.557900390625" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.684103515625" Y="-24.5811328125" />
                  <Point X="3.659815429688" Y="-24.5904765625" />
                  <Point X="3.6347890625" Y="-24.6025390625" />
                  <Point X="3.62849609375" Y="-24.605869140625" />
                  <Point X="3.541495605469" Y="-24.65615625" />
                  <Point X="3.532275390625" Y="-24.6622265625" />
                  <Point X="3.514600097656" Y="-24.675396484375" />
                  <Point X="3.506145019531" Y="-24.68249609375" />
                  <Point X="3.488527099609" Y="-24.69912890625" />
                  <Point X="3.483577392578" Y="-24.704162109375" />
                  <Point X="3.469490966797" Y="-24.719984375" />
                  <Point X="3.417290771484" Y="-24.7865" />
                  <Point X="3.410854003906" Y="-24.795791015625" />
                  <Point X="3.399129394531" Y="-24.815072265625" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.380004394531" Y="-24.85707421875" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.369275146484" Y="-24.89527734375" />
                  <Point X="3.351875" Y="-24.986134765625" />
                  <Point X="3.350426513672" Y="-24.99715234375" />
                  <Point X="3.348825927734" Y="-25.01928125" />
                  <Point X="3.348673828125" Y="-25.030392578125" />
                  <Point X="3.349775878906" Y="-25.054953125" />
                  <Point X="3.350330078125" Y="-25.061787109375" />
                  <Point X="3.352976074219" Y="-25.08217578125" />
                  <Point X="3.370376220703" Y="-25.173033203125" />
                  <Point X="3.373158447266" Y="-25.183986328125" />
                  <Point X="3.380004150391" Y="-25.205486328125" />
                  <Point X="3.384067626953" Y="-25.216033203125" />
                  <Point X="3.393841552734" Y="-25.237498046875" />
                  <Point X="3.399127441406" Y="-25.247484375" />
                  <Point X="3.410847412109" Y="-25.266759765625" />
                  <Point X="3.420587158203" Y="-25.28026171875" />
                  <Point X="3.472787353516" Y="-25.34677734375" />
                  <Point X="3.480299316406" Y="-25.355255859375" />
                  <Point X="3.496278564453" Y="-25.371259765625" />
                  <Point X="3.504745849609" Y="-25.37878515625" />
                  <Point X="3.524573486328" Y="-25.394392578125" />
                  <Point X="3.530049316406" Y="-25.39839453125" />
                  <Point X="3.547008544922" Y="-25.409591796875" />
                  <Point X="3.634002441406" Y="-25.459876953125" />
                  <Point X="3.639491210938" Y="-25.462814453125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027099609" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.76161328125" Y="-25.9310546875" />
                  <Point X="4.758580566406" Y="-25.94434375" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.526573486328" Y="-25.92107421875" />
                  <Point X="3.436781738281" Y="-25.90925390625" />
                  <Point X="3.425417480469" Y="-25.908447265625" />
                  <Point X="3.402673339844" Y="-25.90819921875" />
                  <Point X="3.391293212891" Y="-25.9087578125" />
                  <Point X="3.363334716797" Y="-25.9118203125" />
                  <Point X="3.343668212891" Y="-25.91502734375" />
                  <Point X="3.172916992188" Y="-25.952140625" />
                  <Point X="3.161476806641" Y="-25.955388671875" />
                  <Point X="3.131112304688" Y="-25.96610546875" />
                  <Point X="3.110883056641" Y="-25.976083984375" />
                  <Point X="3.079638671875" Y="-25.99643359375" />
                  <Point X="3.065025878906" Y="-26.00815625" />
                  <Point X="3.045989990234" Y="-26.02679296875" />
                  <Point X="3.032813720703" Y="-26.0410859375" />
                  <Point X="2.92960546875" Y="-26.165212890625" />
                  <Point X="2.923183837891" Y="-26.17389453125" />
                  <Point X="2.907157714844" Y="-26.198361328125" />
                  <Point X="2.897791748047" Y="-26.216751953125" />
                  <Point X="2.885674316406" Y="-26.248728515625" />
                  <Point X="2.881065917969" Y="-26.265267578125" />
                  <Point X="2.876533447266" Y="-26.29" />
                  <Point X="2.874220458984" Y="-26.30683984375" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.85908203125" Y="-26.479484375" />
                  <Point X="2.860162597656" Y="-26.511671875" />
                  <Point X="2.863629150391" Y="-26.53410546875" />
                  <Point X="2.873761474609" Y="-26.570283203125" />
                  <Point X="2.880633789062" Y="-26.587865234375" />
                  <Point X="2.893129394531" Y="-26.6123359375" />
                  <Point X="2.902524414062" Y="-26.6286796875" />
                  <Point X="2.997020507812" Y="-26.775662109375" />
                  <Point X="3.001743896484" Y="-26.78235546875" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486328125" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="4.087170410156" Y="-27.629984375" />
                  <Point X="4.045499023438" Y="-27.6974140625" />
                  <Point X="4.039222167969" Y="-27.70633203125" />
                  <Point X="4.001272705078" Y="-27.760251953125" />
                  <Point X="2.928426269531" Y="-27.140845703125" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.837962158203" Y="-27.08944921875" />
                  <Point X="2.816402099609" Y="-27.080330078125" />
                  <Point X="2.805334472656" Y="-27.076435546875" />
                  <Point X="2.776351318359" Y="-27.068208984375" />
                  <Point X="2.758238525391" Y="-27.064013671875" />
                  <Point X="2.555017578125" Y="-27.0273125" />
                  <Point X="2.543198242188" Y="-27.025935546875" />
                  <Point X="2.5110390625" Y="-27.02421875" />
                  <Point X="2.488226806641" Y="-27.025755859375" />
                  <Point X="2.450996582031" Y="-27.0328359375" />
                  <Point X="2.432739990234" Y="-27.038251953125" />
                  <Point X="2.406377197266" Y="-27.049048828125" />
                  <Point X="2.389898193359" Y="-27.056736328125" />
                  <Point X="2.221071533203" Y="-27.145587890625" />
                  <Point X="2.211812988281" Y="-27.151154296875" />
                  <Point X="2.187643554688" Y="-27.167626953125" />
                  <Point X="2.171757324219" Y="-27.1812421875" />
                  <Point X="2.148009033203" Y="-27.206638671875" />
                  <Point X="2.137304931641" Y="-27.220435546875" />
                  <Point X="2.122787109375" Y="-27.2431953125" />
                  <Point X="2.114837402344" Y="-27.25688671875" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.021113769531" Y="-27.436568359375" />
                  <Point X="2.009794189453" Y="-27.466720703125" />
                  <Point X="2.00437487793" Y="-27.48908984375" />
                  <Point X="1.999944091797" Y="-27.52702734375" />
                  <Point X="1.999655029297" Y="-27.546220703125" />
                  <Point X="2.002191650391" Y="-27.575591796875" />
                  <Point X="2.00451184082" Y="-27.593009765625" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051237792969" Y="-27.831548828125" />
                  <Point X="2.064070800781" Y="-27.862482421875" />
                  <Point X="2.069546875" Y="-27.873580078125" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723753417969" Y="-29.036083984375" />
                  <Point X="1.894940185547" Y="-27.955955078125" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.826012207031" Y="-27.867373046875" />
                  <Point X="1.809128173828" Y="-27.850341796875" />
                  <Point X="1.800146484375" Y="-27.842361328125" />
                  <Point X="1.774825317383" Y="-27.822580078125" />
                  <Point X="1.76060546875" Y="-27.812486328125" />
                  <Point X="1.560174682617" Y="-27.68362890625" />
                  <Point X="1.549784423828" Y="-27.677833984375" />
                  <Point X="1.520732666016" Y="-27.66394140625" />
                  <Point X="1.498795288086" Y="-27.656576171875" />
                  <Point X="1.461125366211" Y="-27.6488671875" />
                  <Point X="1.441895263672" Y="-27.6469375" />
                  <Point X="1.411556762695" Y="-27.64699609375" />
                  <Point X="1.394514526367" Y="-27.647796875" />
                  <Point X="1.175308959961" Y="-27.667966796875" />
                  <Point X="1.164628662109" Y="-27.669564453125" />
                  <Point X="1.135993164062" Y="-27.675533203125" />
                  <Point X="1.115865478516" Y="-27.682142578125" />
                  <Point X="1.083790283203" Y="-27.6968125" />
                  <Point X="1.068440185547" Y="-27.7056484375" />
                  <Point X="1.044888549805" Y="-27.72230859375" />
                  <Point X="1.033139770508" Y="-27.73132421875" />
                  <Point X="0.863874938965" Y="-27.8720625" />
                  <Point X="0.855215270996" Y="-27.880234375" />
                  <Point X="0.833214477539" Y="-27.903767578125" />
                  <Point X="0.819514404297" Y="-27.9226015625" />
                  <Point X="0.800739318848" Y="-27.956486328125" />
                  <Point X="0.793097167969" Y="-27.974396484375" />
                  <Point X="0.783774169922" Y="-28.004466796875" />
                  <Point X="0.779587036133" Y="-28.020376953125" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323171875" />
                  <Point X="0.725554992676" Y="-28.335521484375" />
                  <Point X="0.833090820312" Y="-29.152337890625" />
                  <Point X="0.678921630859" Y="-28.576970703125" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.651129699707" Y="-28.4763125" />
                  <Point X="0.641793212891" Y="-28.45366796875" />
                  <Point X="0.636392272949" Y="-28.442646484375" />
                  <Point X="0.619421325684" Y="-28.413029296875" />
                  <Point X="0.610656738281" Y="-28.399158203125" />
                  <Point X="0.456679992676" Y="-28.177306640625" />
                  <Point X="0.449296386719" Y="-28.1679765625" />
                  <Point X="0.42776940918" Y="-28.144021484375" />
                  <Point X="0.410111968994" Y="-28.1286796875" />
                  <Point X="0.377803588867" Y="-28.106958984375" />
                  <Point X="0.360527069092" Y="-28.0977734375" />
                  <Point X="0.330700683594" Y="-28.08566796875" />
                  <Point X="0.315566711426" Y="-28.0802578125" />
                  <Point X="0.077296401978" Y="-28.006306640625" />
                  <Point X="0.066819076538" Y="-28.003697265625" />
                  <Point X="0.038085517883" Y="-27.99825390625" />
                  <Point X="0.016740444183" Y="-27.9966640625" />
                  <Point X="-0.018948818207" Y="-27.998041015625" />
                  <Point X="-0.03672857666" Y="-28.000421875" />
                  <Point X="-0.066638191223" Y="-28.0073515625" />
                  <Point X="-0.080071624756" Y="-28.01098828125" />
                  <Point X="-0.318341766357" Y="-28.0849375" />
                  <Point X="-0.329460540771" Y="-28.08916796875" />
                  <Point X="-0.358786834717" Y="-28.102484375" />
                  <Point X="-0.378854644775" Y="-28.11480078125" />
                  <Point X="-0.409489685059" Y="-28.13930859375" />
                  <Point X="-0.423526947021" Y="-28.15316015625" />
                  <Point X="-0.444795349121" Y="-28.179029296875" />
                  <Point X="-0.454118164062" Y="-28.191357421875" />
                  <Point X="-0.608095031738" Y="-28.41320703125" />
                  <Point X="-0.612470825195" Y="-28.420130859375" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.985424560547" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.758137848789" Y="-24.292971300516" />
                  <Point X="4.607840021339" Y="-23.901432073778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.090027290064" Y="-22.552483789974" />
                  <Point X="3.986767654227" Y="-22.283483241832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.665869510765" Y="-24.317694732526" />
                  <Point X="4.510976429397" Y="-23.914184460057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.011421656159" Y="-22.612799783081" />
                  <Point X="3.874810884942" Y="-22.256916556862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.57360117274" Y="-24.342418164535" />
                  <Point X="4.414112837454" Y="-23.926936846336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932816022255" Y="-22.673115776188" />
                  <Point X="3.791512940628" Y="-22.305008663434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.481332834716" Y="-24.367141596545" />
                  <Point X="4.317249245512" Y="-23.939689232614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854210388351" Y="-22.733431769295" />
                  <Point X="3.708214996315" Y="-22.353100770006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.389064496692" Y="-24.391865028555" />
                  <Point X="4.22038565357" Y="-23.952441618893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.775604754447" Y="-22.793747762402" />
                  <Point X="3.624917052001" Y="-22.401192876578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.296796158667" Y="-24.416588460564" />
                  <Point X="4.123522061627" Y="-23.965194005171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696999120542" Y="-22.854063755509" />
                  <Point X="3.541619107688" Y="-22.44928498315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765779284041" Y="-25.903421942416" />
                  <Point X="4.709391147233" Y="-25.756525823839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204527820643" Y="-24.441311892574" />
                  <Point X="4.026658469685" Y="-23.97794639145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.618393486638" Y="-22.914379748616" />
                  <Point X="3.458321163374" Y="-22.497377089721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729181862999" Y="-26.073173071477" />
                  <Point X="4.595965963578" Y="-25.726133788653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.112259482618" Y="-24.466035324583" />
                  <Point X="3.929794877743" Y="-23.990698777728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.539787852734" Y="-22.974695741723" />
                  <Point X="3.375023219061" Y="-22.545469196293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.624524502828" Y="-26.065621997371" />
                  <Point X="4.482540779923" Y="-25.695741753466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.019991144594" Y="-24.490758756593" />
                  <Point X="3.8329312858" Y="-24.003451164007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.461182218829" Y="-23.03501173483" />
                  <Point X="3.291725274747" Y="-22.593561302865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.75764626258" Y="-21.202237908585" />
                  <Point X="2.680074254593" Y="-21.000155918853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.517349468319" Y="-26.051512157379" />
                  <Point X="4.369115596268" Y="-25.66534971828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.92772280657" Y="-24.515482188602" />
                  <Point X="3.736067693858" Y="-24.016203550285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382576584925" Y="-23.095327727937" />
                  <Point X="3.208427330434" Y="-22.641653409437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.696525190402" Y="-21.308102742248" />
                  <Point X="2.545785111005" Y="-20.915411409799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.410174433811" Y="-26.037402317387" />
                  <Point X="4.255690412613" Y="-25.634957683094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.835454468545" Y="-24.540205620612" />
                  <Point X="3.639204101916" Y="-24.028955936564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.303970951021" Y="-23.155643721044" />
                  <Point X="3.12512938612" Y="-22.689745516009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.635404118225" Y="-21.41396757591" />
                  <Point X="2.414713446101" Y="-20.839048719281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.302999399302" Y="-26.023292477395" />
                  <Point X="4.142265228958" Y="-25.604565647907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.743186115982" Y="-24.564929014748" />
                  <Point X="3.542340509973" Y="-24.041708322842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.225365406148" Y="-23.215959946086" />
                  <Point X="3.041831441807" Y="-22.737837622581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574283046047" Y="-21.519832409573" />
                  <Point X="2.283641781197" Y="-20.762686028763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.195824364794" Y="-26.009182637403" />
                  <Point X="4.028840045302" Y="-25.574173612721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.652573941191" Y="-24.593966899485" />
                  <Point X="3.442876542924" Y="-24.047686500366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147903759367" Y="-23.279256127538" />
                  <Point X="2.958533497493" Y="-22.785929729153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.51316197387" Y="-21.625697243236" />
                  <Point X="2.157830975082" Y="-20.700028343945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.088649330285" Y="-25.995072797411" />
                  <Point X="3.915414861647" Y="-25.543781577534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.568660217515" Y="-24.640454845973" />
                  <Point X="3.335393675586" Y="-24.032774728435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.079103954623" Y="-23.365117178962" />
                  <Point X="2.87523555318" Y="-22.834021835724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.452040901692" Y="-21.731562076898" />
                  <Point X="2.033807639678" Y="-20.642027179534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.981474295777" Y="-25.980962957419" />
                  <Point X="3.801989677992" Y="-25.513389542348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.489185646936" Y="-24.698507181651" />
                  <Point X="3.213954059307" Y="-23.981504382462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.022807076719" Y="-23.483549468371" />
                  <Point X="2.791937608866" Y="-22.882113942296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390919829515" Y="-21.837426910561" />
                  <Point X="1.909784304275" Y="-20.584026015122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.874299261268" Y="-25.966853117427" />
                  <Point X="3.688483358169" Y="-25.482786140217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419918725749" Y="-24.783151353135" />
                  <Point X="2.708639664553" Y="-22.930206048868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.329798757338" Y="-21.943291744224" />
                  <Point X="1.789154080861" Y="-20.534864209649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.76712422676" Y="-25.952743277435" />
                  <Point X="3.561941256276" Y="-25.418223364767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.366587736916" Y="-24.909310047733" />
                  <Point X="2.625070944467" Y="-22.977592760438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.26867768516" Y="-22.049156577886" />
                  <Point X="1.670935808934" Y="-20.491985752619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.659949192251" Y="-25.938633437443" />
                  <Point X="2.534861710438" Y="-23.007680341749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.207556612983" Y="-22.155021411549" />
                  <Point X="1.552717537006" Y="-20.449107295589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.552774157743" Y="-25.924523597451" />
                  <Point X="2.439680779942" Y="-23.024816210959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.146435540805" Y="-22.260886245211" />
                  <Point X="1.435480610076" Y="-20.408785329678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.445599479739" Y="-25.910414686186" />
                  <Point X="2.334114542702" Y="-23.01489743114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085314477478" Y="-22.36675110193" />
                  <Point X="1.323327759179" Y="-20.381707834647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.035343977747" Y="-27.711842299324" />
                  <Point X="3.969162424645" Y="-27.539433459055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.345496870241" Y="-25.914729143251" />
                  <Point X="2.212432060481" Y="-22.962994397756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032194573671" Y="-22.49345969182" />
                  <Point X="1.211174908282" Y="-20.354630339615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.938186570091" Y="-27.723829269501" />
                  <Point X="3.824915830939" Y="-27.428748905585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251539250294" Y="-25.935051845397" />
                  <Point X="1.099022057385" Y="-20.327552844584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.807454527078" Y="-27.648351324257" />
                  <Point X="3.680669237233" Y="-27.318064352116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.158051159933" Y="-25.956597713933" />
                  <Point X="0.986869206488" Y="-20.300475349552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.676722484065" Y="-27.572873379012" />
                  <Point X="3.536422643526" Y="-27.207379798647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.073480356668" Y="-26.001373909568" />
                  <Point X="0.874716355591" Y="-20.27339785452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.545990441052" Y="-27.497395433768" />
                  <Point X="3.39217604982" Y="-27.096695245178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001446719651" Y="-26.078810539899" />
                  <Point X="0.766133697381" Y="-20.255621029417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.415258398039" Y="-27.421917488524" />
                  <Point X="3.247929456114" Y="-26.986010691709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.931828431207" Y="-26.162539368387" />
                  <Point X="0.660112704381" Y="-20.244517570338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.284526355025" Y="-27.34643954328" />
                  <Point X="3.103682862407" Y="-26.875326138239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.877329586127" Y="-26.285655693444" />
                  <Point X="0.554091711381" Y="-20.23341411126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.153794312012" Y="-27.270961598036" />
                  <Point X="2.914241168859" Y="-26.646904324394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859954852271" Y="-26.505483634688" />
                  <Point X="0.448070718381" Y="-20.222310652182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.023062268999" Y="-27.195483652792" />
                  <Point X="0.363931014877" Y="-20.268209901093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.892330148535" Y="-27.120005505781" />
                  <Point X="0.322099813735" Y="-20.424326566848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.770135935478" Y="-27.066769367992" />
                  <Point X="0.280268612592" Y="-20.580443232603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.660546891427" Y="-27.046370818138" />
                  <Point X="0.23843741145" Y="-20.736559898358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.551306347231" Y="-27.026880141448" />
                  <Point X="0.177109259546" Y="-20.841885270889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.451776830967" Y="-27.032687557428" />
                  <Point X="0.096213470666" Y="-20.896235206315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.364400618906" Y="-27.070155413286" />
                  <Point X="0.001614341094" Y="-20.91488671875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.27974437784" Y="-27.114709035843" />
                  <Point X="-0.11251343122" Y="-20.882664377532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.359105729393" Y="-20.240269478123" />
                  <Point X="-0.36775483529" Y="-20.217737786933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.196089295452" Y="-27.161870765921" />
                  <Point X="-0.465138512781" Y="-20.229135304035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.124510795515" Y="-27.240493068883" />
                  <Point X="-0.562522190272" Y="-20.240532821137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.70026392773" Y="-29.005471927995" />
                  <Point X="2.65506610584" Y="-28.88772757644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.065228328477" Y="-27.351147632688" />
                  <Point X="-0.659905867764" Y="-20.251930338239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.496640232889" Y="-28.740104737665" />
                  <Point X="2.351425503451" Y="-28.361807433975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.009035699657" Y="-27.469851500247" />
                  <Point X="-0.757289545255" Y="-20.263327855341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293016538048" Y="-28.474737547336" />
                  <Point X="-0.854673222746" Y="-20.274725372443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.089392843206" Y="-28.209370357007" />
                  <Point X="-0.950873920132" Y="-20.289204658084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.885769009048" Y="-27.944002803746" />
                  <Point X="-1.042745404587" Y="-20.314961928985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.72465324509" Y="-27.789372559323" />
                  <Point X="-1.134616889043" Y="-20.340719199886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.589553630263" Y="-27.702516700506" />
                  <Point X="-1.226488373499" Y="-20.366476470787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.467718682487" Y="-27.650216480772" />
                  <Point X="-1.318359857955" Y="-20.392233741688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.36603695787" Y="-27.650417202308" />
                  <Point X="-1.410231342411" Y="-20.417991012589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.267749762105" Y="-27.659460973835" />
                  <Point X="-1.466062501604" Y="-20.537636540721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.169584761497" Y="-27.668823074629" />
                  <Point X="-1.494311405258" Y="-20.729136301138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.07951483527" Y="-27.699273565172" />
                  <Point X="-1.551303137716" Y="-20.845758432547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.000482421158" Y="-27.758477757827" />
                  <Point X="-1.629850646915" Y="-20.906225845689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.923343974205" Y="-27.822615903616" />
                  <Point X="-1.717325298551" Y="-20.943437257688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.847053918965" Y="-27.888964185381" />
                  <Point X="-1.80909408169" Y="-20.969462074665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.786391677156" Y="-27.996024313019" />
                  <Point X="-1.916096828715" Y="-20.955801058914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.748638399167" Y="-28.162764331787" />
                  <Point X="-2.041992631986" Y="-20.892921948935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.73200686745" Y="-28.384528380797" />
                  <Point X="-2.203331883456" Y="-20.73770949964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.785124101626" Y="-28.787994177111" />
                  <Point X="-2.287203559191" Y="-20.784306984761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.442429666261" Y="-28.160335321423" />
                  <Point X="-2.371075234925" Y="-20.830904469882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.30916991011" Y="-28.078272458325" />
                  <Point X="-2.45494691066" Y="-20.877501955002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.193648017104" Y="-28.042418308537" />
                  <Point X="-2.538818586394" Y="-20.924099440123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.078126124098" Y="-28.006564158749" />
                  <Point X="-2.622690262129" Y="-20.970696925244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.026515434458" Y="-27.999054249257" />
                  <Point X="-2.702918732373" Y="-21.026785285149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.11904951107" Y="-28.023085408578" />
                  <Point X="-2.781539121584" Y="-21.087062839366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.209975738876" Y="-28.051305157242" />
                  <Point X="-2.860159510796" Y="-21.147340393584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.300901966681" Y="-28.079524905906" />
                  <Point X="-2.938779900007" Y="-21.207617947802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.386707943653" Y="-28.121083364025" />
                  <Point X="-2.748511338173" Y="-21.968375168005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.897231392639" Y="-21.580946180414" />
                  <Point X="-3.017400289218" Y="-21.26789550202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.458865450082" Y="-28.198197303507" />
                  <Point X="-2.793652807893" Y="-22.115868289288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.524386470921" Y="-28.292599879026" />
                  <Point X="-2.865567624337" Y="-22.193614457794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.58990749176" Y="-28.387002454545" />
                  <Point X="-2.944675271087" Y="-22.252622662727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.646933161855" Y="-28.503536175389" />
                  <Point X="-3.037409668251" Y="-22.276131969168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.688764413962" Y="-28.659652708376" />
                  <Point X="-3.14151132799" Y="-22.270028544179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.73059566607" Y="-28.815769241362" />
                  <Point X="-3.256162984173" Y="-22.236441438823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.772426918177" Y="-28.971885774349" />
                  <Point X="-3.38689519997" Y="-22.160963043462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.814258170285" Y="-29.128002307335" />
                  <Point X="-3.517627376205" Y="-22.085484751163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.856089422392" Y="-29.284118840322" />
                  <Point X="-3.64835955244" Y="-22.010006458864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.8979206745" Y="-29.440235373309" />
                  <Point X="-3.753833105656" Y="-22.00032912918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.939751926607" Y="-29.596351906295" />
                  <Point X="-3.821971952759" Y="-22.087912034127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.981583178715" Y="-29.752468439282" />
                  <Point X="-1.170603124824" Y="-29.260054644663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.237587737958" Y="-29.085553761486" />
                  <Point X="-3.890110799861" Y="-22.175494939073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.086001154964" Y="-29.745540981611" />
                  <Point X="-1.128531065131" Y="-29.634746777713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.39401559347" Y="-28.943135936093" />
                  <Point X="-3.448203221991" Y="-23.591794208202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604922659698" Y="-23.183526114808" />
                  <Point X="-3.958249646964" Y="-22.26307784402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.541398676517" Y="-28.82428054854" />
                  <Point X="-3.508941756156" Y="-23.69865558746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.749169322536" Y="-23.072841381243" />
                  <Point X="-4.024001200494" Y="-22.356879861346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.654377191529" Y="-28.79505212495" />
                  <Point X="-3.58874411447" Y="-23.755854006893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.893415985375" Y="-22.962156647679" />
                  <Point X="-4.085388543957" Y="-22.462051034595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.758775437358" Y="-28.788176066784" />
                  <Point X="-3.678802905251" Y="-23.786333506263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.037662648213" Y="-22.851471914114" />
                  <Point X="-4.14677588742" Y="-22.567222207844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.863160370721" Y="-28.781334688776" />
                  <Point X="-2.311469241601" Y="-27.613450151641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.380533162179" Y="-27.433532487377" />
                  <Point X="-3.296037212918" Y="-25.048562896115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.381221671346" Y="-24.826649794982" />
                  <Point X="-3.777449269825" Y="-23.794441611055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.181909311052" Y="-22.74078718055" />
                  <Point X="-4.208163230883" Y="-22.672393381092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.966835464463" Y="-28.776342506202" />
                  <Point X="-2.359062077669" Y="-27.754557245256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.512676237481" Y="-27.354378677347" />
                  <Point X="-3.343527572711" Y="-25.189936949554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.512709936392" Y="-24.749201823989" />
                  <Point X="-3.88461001283" Y="-23.780369001702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.06026652422" Y="-28.798036944541" />
                  <Point X="-2.420183035621" Y="-27.860422376486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.614472208878" Y="-27.354281775846" />
                  <Point X="-2.962983031284" Y="-26.446380043469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.081783674351" Y="-26.136893787337" />
                  <Point X="-3.41627460038" Y="-25.2655151337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.626135163714" Y="-24.718809675048" />
                  <Point X="-3.991785100872" Y="-23.766259022252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.143002433885" Y="-28.84759320143" />
                  <Point X="-2.481304050197" Y="-27.966287360205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703126291643" Y="-27.388420664708" />
                  <Point X="-3.024762161577" Y="-26.550530577132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.197502003158" Y="-26.100527904791" />
                  <Point X="-3.501015668323" Y="-25.309847774684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739560391035" Y="-24.688417526106" />
                  <Point X="-4.098960188913" Y="-23.752149042802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.22398904851" Y="-28.901706527698" />
                  <Point X="-2.542425064774" Y="-28.072152343924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.786424215215" Y="-27.436512825313" />
                  <Point X="-3.101758214173" Y="-26.615039672902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296739072536" Y="-26.107097170956" />
                  <Point X="-3.593152664952" Y="-25.334913362728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.852985618357" Y="-24.658025377164" />
                  <Point X="-4.206135276954" Y="-23.738039063352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.304975663135" Y="-28.955819853966" />
                  <Point X="-2.60354607935" Y="-28.178017327642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.869722138788" Y="-27.484604985918" />
                  <Point X="-3.18036389576" Y="-26.675355541791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.393602725879" Y="-26.11984939728" />
                  <Point X="-3.685421041245" Y="-25.359636695043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.966410845679" Y="-24.627633228223" />
                  <Point X="-4.313310364996" Y="-23.723929083902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.384360597634" Y="-29.014105699614" />
                  <Point X="-2.664667093926" Y="-28.283882311361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.95302006236" Y="-27.532697146523" />
                  <Point X="-3.258969439599" Y="-26.735671769527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.490466379223" Y="-26.132601623603" />
                  <Point X="-3.777689418307" Y="-25.384360025357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079836073001" Y="-24.597241079281" />
                  <Point X="-4.420485453037" Y="-23.709819104453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.453851899596" Y="-29.098165339197" />
                  <Point X="-2.725788108503" Y="-28.38974729508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.036317985932" Y="-27.580789307128" />
                  <Point X="-3.337574983438" Y="-26.795987997263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.587330032566" Y="-26.145353849927" />
                  <Point X="-3.869957795369" Y="-25.409083355671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.193261300322" Y="-24.566848930339" />
                  <Point X="-4.527660541079" Y="-23.695709125003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.539566301058" Y="-29.139962359676" />
                  <Point X="-2.786909123079" Y="-28.495612278799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.119615909504" Y="-27.628881467733" />
                  <Point X="-3.416180527276" Y="-26.856304225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.684193685909" Y="-26.15810607625" />
                  <Point X="-3.96222617243" Y="-25.433806685985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.306686527644" Y="-24.536456781397" />
                  <Point X="-4.634076936782" Y="-23.683575606666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.673051358081" Y="-29.057312567739" />
                  <Point X="-2.848030137655" Y="-28.601477262517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.202913833076" Y="-27.676973628338" />
                  <Point X="-3.494786071115" Y="-26.916620452736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.781057339253" Y="-26.170858302573" />
                  <Point X="-4.054494549492" Y="-25.458530016299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.420111754966" Y="-24.506064632456" />
                  <Point X="-4.676185638181" Y="-23.838969359536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.81137952721" Y="-28.962046037417" />
                  <Point X="-2.909151152231" Y="-28.707342246236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.286211756648" Y="-27.725065788943" />
                  <Point X="-3.573391614953" Y="-26.976936680472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.877920992596" Y="-26.183610528897" />
                  <Point X="-4.146762926554" Y="-25.483253346613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.533536982287" Y="-24.475672483514" />
                  <Point X="-4.718294339581" Y="-23.994363112407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.955832910934" Y="-28.850822777535" />
                  <Point X="-2.970272166808" Y="-28.813207229955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.36950968022" Y="-27.773157949548" />
                  <Point X="-3.651997158792" Y="-27.037252908208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.97478464594" Y="-26.19636275522" />
                  <Point X="-4.239031303615" Y="-25.507976676926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.646962209609" Y="-24.445280334572" />
                  <Point X="-4.7508783762" Y="-24.174569465342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.452807603793" Y="-27.821250110153" />
                  <Point X="-3.73060270263" Y="-27.097569135945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.071648299283" Y="-26.209114981543" />
                  <Point X="-4.331299680677" Y="-25.53270000724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760387436931" Y="-24.414888185631" />
                  <Point X="-4.779190885703" Y="-24.365903526856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.536105527365" Y="-27.869342270758" />
                  <Point X="-3.809208246469" Y="-27.157885363681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.168511952626" Y="-26.221867207867" />
                  <Point X="-4.423568057739" Y="-25.557423337554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.619403450937" Y="-27.917434431363" />
                  <Point X="-3.887813790308" Y="-27.218201591417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.26537560597" Y="-26.23461943419" />
                  <Point X="-4.515836434801" Y="-25.582146667868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.702701374509" Y="-27.965526591968" />
                  <Point X="-3.966419334146" Y="-27.278517819153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.362239259313" Y="-26.247371660513" />
                  <Point X="-4.608104811862" Y="-25.606869998182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.785999298081" Y="-28.013618752572" />
                  <Point X="-4.045024877985" Y="-27.33883404689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.459102912656" Y="-26.260123886837" />
                  <Point X="-4.700373188924" Y="-25.631593328496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.98090928681" Y="-27.77095154275" />
                  <Point X="-4.123630421823" Y="-27.399150274626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.555966566" Y="-26.27287611316" />
                  <Point X="-4.782398541541" Y="-25.683000649781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.652830219343" Y="-26.285628339483" />
                  <Point X="-4.677822411638" Y="-26.220521452633" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.495395721436" Y="-28.626146484375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.454567871094" Y="-28.507494140625" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.289073425293" Y="-28.27382421875" />
                  <Point X="0.259247192383" Y="-28.26171875" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="0.006156158924" Y="-28.18551953125" />
                  <Point X="-0.023753477097" Y="-28.19244921875" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.276761291504" Y="-28.27382421875" />
                  <Point X="-0.298029815674" Y="-28.299693359375" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.835951660156" Y="-29.943068359375" />
                  <Point X="-0.847743896484" Y="-29.987076171875" />
                  <Point X="-1.100231933594" Y="-29.938068359375" />
                  <Point X="-1.116397216797" Y="-29.93391015625" />
                  <Point X="-1.351589477539" Y="-29.873396484375" />
                  <Point X="-1.312022216797" Y="-29.572853515625" />
                  <Point X="-1.309150390625" Y="-29.551041015625" />
                  <Point X="-1.313287963867" Y="-29.51663671875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.375948730469" Y="-29.215220703125" />
                  <Point X="-1.400174438477" Y="-29.190447265625" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.633376708984" Y="-28.9894609375" />
                  <Point X="-1.667678466797" Y="-28.9845546875" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.975041015625" Y="-28.967068359375" />
                  <Point X="-2.005241699219" Y="-28.984056640625" />
                  <Point X="-2.247844726562" Y="-29.146158203125" />
                  <Point X="-2.259734375" Y="-29.157294921875" />
                  <Point X="-2.457094970703" Y="-29.4145" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-2.878200439453" Y="-29.150390625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.550368652344" Y="-27.70591015625" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.597591796875" />
                  <Point X="-2.515013916016" Y="-27.56773046875" />
                  <Point X="-2.531329345703" Y="-27.551416015625" />
                  <Point X="-2.560158203125" Y="-27.53719921875" />
                  <Point X="-2.591684082031" Y="-27.543470703125" />
                  <Point X="-3.80429296875" Y="-28.2435703125" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.177737792969" Y="-27.820244140625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.241359130859" Y="-26.48266796875" />
                  <Point X="-3.163787353516" Y="-26.423146484375" />
                  <Point X="-3.14536328125" Y="-26.394244140625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140328613281" Y="-26.334591796875" />
                  <Point X="-3.162747314453" Y="-26.30970703125" />
                  <Point X="-3.187641845703" Y="-26.2950546875" />
                  <Point X="-3.219528808594" Y="-26.288572265625" />
                  <Point X="-4.750332519531" Y="-26.49010546875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.931635253906" Y="-25.981533203125" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.645829833984" Y="-25.15232421875" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.540242675781" Y="-25.120279296875" />
                  <Point X="-3.514142578125" Y="-25.1021640625" />
                  <Point X="-3.494347900391" Y="-25.0741328125" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.486198486328" Y="-25.014685546875" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.515795410156" Y="-24.95925" />
                  <Point X="-3.541895507813" Y="-24.941134765625" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.952864257812" Y="-24.560017578125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.909104492188" Y="-23.972064453125" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.816608154297" Y="-23.597681640625" />
                  <Point X="-3.753265136719" Y="-23.60601953125" />
                  <Point X="-3.731717529297" Y="-23.60413671875" />
                  <Point X="-3.728045898438" Y="-23.60298046875" />
                  <Point X="-3.670278076172" Y="-23.584765625" />
                  <Point X="-3.651536376953" Y="-23.5739453125" />
                  <Point X="-3.639122070312" Y="-23.55621875" />
                  <Point X="-3.637651855469" Y="-23.552671875" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.618087158203" Y="-23.4510859375" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968505859" Y="-23.380779296875" />
                  <Point X="-4.460373535156" Y="-22.76660546875" />
                  <Point X="-4.47610546875" Y="-22.754533203125" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.137392578125" Y="-22.18391796875" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.198060302734" Y="-22.050595703125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514892578" Y="-22.07956640625" />
                  <Point X="-3.133419921875" Y="-22.08001171875" />
                  <Point X="-3.052965576172" Y="-22.08705078125" />
                  <Point X="-3.031510742188" Y="-22.0842265625" />
                  <Point X="-3.013258300781" Y="-22.0726015625" />
                  <Point X="-3.009635742188" Y="-22.06898046875" />
                  <Point X="-2.952528564453" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.938520019531" Y="-21.967064453125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067626953" Y="-21.865966796875" />
                  <Point X="-3.306751464844" Y="-21.25163671875" />
                  <Point X="-3.306826416016" Y="-21.250376953125" />
                  <Point X="-2.752873779297" Y="-20.82566796875" />
                  <Point X="-2.717247802734" Y="-20.805875" />
                  <Point X="-2.141548828125" Y="-20.48602734375" />
                  <Point X="-1.979735229492" Y="-20.696908203125" />
                  <Point X="-1.967825683594" Y="-20.712427734375" />
                  <Point X="-1.951254150391" Y="-20.726333984375" />
                  <Point X="-1.945576293945" Y="-20.729291015625" />
                  <Point X="-1.856030761719" Y="-20.77590625" />
                  <Point X="-1.835121459961" Y="-20.7815078125" />
                  <Point X="-1.813803833008" Y="-20.777748046875" />
                  <Point X="-1.807897460938" Y="-20.77530078125" />
                  <Point X="-1.714634887695" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.684161010742" Y="-20.6994140625" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.689137695313" Y="-20.298861328125" />
                  <Point X="-1.685216674805" Y="-20.29776171875" />
                  <Point X="-0.968082763672" Y="-20.096703125" />
                  <Point X="-0.924904602051" Y="-20.0916484375" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.053649044037" Y="-20.646146484375" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.02428212738" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594047546" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.236188186646" Y="-20.010849609375" />
                  <Point X="0.236648147583" Y="-20.0091328125" />
                  <Point X="0.860210083008" Y="-20.074435546875" />
                  <Point X="0.895939819336" Y="-20.0830625" />
                  <Point X="1.508456665039" Y="-20.230943359375" />
                  <Point X="1.531292358398" Y="-20.239224609375" />
                  <Point X="1.931044799805" Y="-20.38421875" />
                  <Point X="1.953526489258" Y="-20.394732421875" />
                  <Point X="2.338684326172" Y="-20.574857421875" />
                  <Point X="2.360436035156" Y="-20.58753125" />
                  <Point X="2.732519287109" Y="-20.804306640625" />
                  <Point X="2.753017578125" Y="-20.818884765625" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.281167724609" Y="-22.4075234375" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.223573486328" Y="-22.513267578125" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202543212891" Y="-22.611810546875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218678222656" Y="-22.699181640625" />
                  <Point X="2.221240722656" Y="-22.702958984375" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.278709716797" Y="-22.778353515625" />
                  <Point X="2.338248046875" Y="-22.81875390625" />
                  <Point X="2.360343017578" Y="-22.827021484375" />
                  <Point X="2.364471435547" Y="-22.82751953125" />
                  <Point X="2.42976171875" Y="-22.835392578125" />
                  <Point X="2.453446044922" Y="-22.832775390625" />
                  <Point X="2.528950927734" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.944538085938" Y="-21.99726953125" />
                  <Point X="3.994247558594" Y="-21.9685703125" />
                  <Point X="4.202590820313" Y="-22.258119140625" />
                  <Point X="4.214017089844" Y="-22.277001953125" />
                  <Point X="4.387512207031" Y="-22.563705078125" />
                  <Point X="3.35617578125" Y="-23.355076171875" />
                  <Point X="3.288615722656" Y="-23.40691796875" />
                  <Point X="3.2759296875" Y="-23.42065625" />
                  <Point X="3.221588867188" Y="-23.491548828125" />
                  <Point X="3.211837646484" Y="-23.513083984375" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.191831787109" Y="-23.614134765625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.218508544922" Y="-23.71639453125" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280953369141" Y="-23.80118359375" />
                  <Point X="3.285101074219" Y="-23.803517578125" />
                  <Point X="3.350589355469" Y="-23.8403828125" />
                  <Point X="3.374180908203" Y="-23.84712109375" />
                  <Point X="3.462726318359" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.809038574219" Y="-23.6833046875" />
                  <Point X="4.848975097656" Y="-23.678046875" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.9427890625" Y="-24.071740234375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="3.818593017578" Y="-24.741427734375" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.723577636719" Y="-24.770365234375" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.618959228516" Y="-24.83728515625" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.555883056641" Y="-24.93101953125" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.539584960938" Y="-25.0464375" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.570064697266" Y="-25.16297265625" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.642092529297" Y="-25.245095703125" />
                  <Point X="3.729086425781" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.9638046875" Y="-25.6279921875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.943818847656" Y="-25.98661328125" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.5017734375" Y="-26.10944921875" />
                  <Point X="3.411981689453" Y="-26.09762890625" />
                  <Point X="3.384023193359" Y="-26.10069140625" />
                  <Point X="3.213271972656" Y="-26.1378046875" />
                  <Point X="3.1979453125" Y="-26.143921875" />
                  <Point X="3.178909423828" Y="-26.16255859375" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.067953613281" Y="-26.299517578125" />
                  <Point X="3.063421142578" Y="-26.32425" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.049849121094" Y="-26.501458984375" />
                  <Point X="3.062344726562" Y="-26.5259296875" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.1684609375" Y="-26.685541015625" />
                  <Point X="4.303075195312" Y="-27.556162109375" />
                  <Point X="4.339073730469" Y="-27.58378515625" />
                  <Point X="4.204130859375" Y="-27.80214453125" />
                  <Point X="4.194590820313" Y="-27.815697265625" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="2.833426269531" Y="-27.305388671875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.724471435547" Y="-27.250990234375" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.504749267578" Y="-27.214076171875" />
                  <Point X="2.478386474609" Y="-27.224873046875" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.297490966797" Y="-27.322615234375" />
                  <Point X="2.282973144531" Y="-27.345375" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.188950439453" Y="-27.529873046875" />
                  <Point X="2.191487060547" Y="-27.559244140625" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091796875" Y="-27.778580078125" />
                  <Point X="2.963195800781" Y="-29.041423828125" />
                  <Point X="2.986673828125" Y="-29.082087890625" />
                  <Point X="2.835294921875" Y="-29.19021484375" />
                  <Point X="2.824633544922" Y="-29.197115234375" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.74420324707" Y="-28.071619140625" />
                  <Point X="1.683177734375" Y="-27.992087890625" />
                  <Point X="1.657856445312" Y="-27.972306640625" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.442262207031" Y="-27.8369375" />
                  <Point X="1.411923706055" Y="-27.83699609375" />
                  <Point X="1.192718383789" Y="-27.857166015625" />
                  <Point X="1.178165649414" Y="-27.86076171875" />
                  <Point X="1.154613891602" Y="-27.877421875" />
                  <Point X="0.985349060059" Y="-28.01816015625" />
                  <Point X="0.974574951172" Y="-28.030662109375" />
                  <Point X="0.965251953125" Y="-28.060732421875" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.310720703125" />
                  <Point X="1.120552001953" Y="-29.88017578125" />
                  <Point X="1.127642089844" Y="-29.934029296875" />
                  <Point X="0.994367736816" Y="-29.9632421875" />
                  <Point X="0.984488891602" Y="-29.965037109375" />
                  <Point X="0.860200439453" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#122" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.011508702859" Y="4.398075539562" Z="0.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.15" />
                  <Point X="-0.936872690232" Y="4.989292220936" Z="0.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.15" />
                  <Point X="-1.704870465505" Y="4.78166399326" Z="0.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.15" />
                  <Point X="-1.747969421667" Y="4.749468478074" Z="0.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.15" />
                  <Point X="-1.738002751349" Y="4.346901142356" Z="0.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.15" />
                  <Point X="-1.833192735946" Y="4.30217107463" Z="0.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.15" />
                  <Point X="-1.928644595176" Y="4.346339371174" Z="0.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.15" />
                  <Point X="-1.946224711962" Y="4.36481210394" Z="0.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.15" />
                  <Point X="-2.747686667459" Y="4.269113413819" Z="0.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.15" />
                  <Point X="-3.343404395585" Y="3.820583624271" Z="0.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.15" />
                  <Point X="-3.356208371882" Y="3.754643037716" Z="0.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.15" />
                  <Point X="-2.994485763037" Y="3.059858457517" Z="0.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.15" />
                  <Point X="-3.051147157687" Y="2.997656410199" Z="0.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.15" />
                  <Point X="-3.135218008089" Y="3.001078936136" Z="0.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.15" />
                  <Point X="-3.179216310581" Y="3.023985576864" Z="0.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.15" />
                  <Point X="-4.183011954007" Y="2.878066195908" Z="0.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.15" />
                  <Point X="-4.527406680284" Y="2.298588571582" Z="0.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.15" />
                  <Point X="-4.496967274814" Y="2.225006398789" Z="0.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.15" />
                  <Point X="-3.668593594178" Y="1.557106749496" Z="0.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.15" />
                  <Point X="-3.690002024496" Y="1.497743894349" Z="0.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.15" />
                  <Point X="-3.74923782805" Y="1.475985973062" Z="0.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.15" />
                  <Point X="-3.816238884618" Y="1.483171776995" Z="0.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.15" />
                  <Point X="-4.963520230196" Y="1.072293295875" Z="0.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.15" />
                  <Point X="-5.055116201663" Y="0.481857461341" Z="0.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.15" />
                  <Point X="-4.971961173475" Y="0.422965458243" Z="0.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.15" />
                  <Point X="-3.55046068527" Y="0.030954106588" Z="0.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.15" />
                  <Point X="-3.54010769313" Y="0.001775156517" Z="0.15" />
                  <Point X="-3.539556741714" Y="0" Z="0.15" />
                  <Point X="-3.548256908005" Y="-0.028031794544" Z="0.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.15" />
                  <Point X="-3.574907909853" Y="-0.047921876546" Z="0.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.15" />
                  <Point X="-3.664926615297" Y="-0.072746598513" Z="0.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.15" />
                  <Point X="-4.987287092" Y="-0.957330728527" Z="0.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.15" />
                  <Point X="-4.854988431792" Y="-1.489506992252" Z="0.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.15" />
                  <Point X="-4.749962810529" Y="-1.508397429029" Z="0.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.15" />
                  <Point X="-3.194253479099" Y="-1.321521547764" Z="0.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.15" />
                  <Point X="-3.199921959956" Y="-1.350425562282" Z="0.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.15" />
                  <Point X="-3.277952523737" Y="-1.411720043219" Z="0.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.15" />
                  <Point X="-4.226837027934" Y="-2.814572192927" Z="0.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.15" />
                  <Point X="-3.882911418271" Y="-3.272766588511" Z="0.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.15" />
                  <Point X="-3.785448550137" Y="-3.255591123261" Z="0.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.15" />
                  <Point X="-2.556524164256" Y="-2.571806304876" Z="0.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.15" />
                  <Point X="-2.599825889907" Y="-2.649629832355" Z="0.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.15" />
                  <Point X="-2.914860386648" Y="-4.158726613664" Z="0.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.15" />
                  <Point X="-2.477283757517" Y="-4.433340229116" Z="0.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.15" />
                  <Point X="-2.43772407707" Y="-4.43208659527" Z="0.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.15" />
                  <Point X="-1.983619209206" Y="-3.99434975326" Z="0.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.15" />
                  <Point X="-1.67710446766" Y="-4.003167467923" Z="0.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.15" />
                  <Point X="-1.439297858026" Y="-4.19675763859" Z="0.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.15" />
                  <Point X="-1.368483254394" Y="-4.495110101238" Z="0.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.15" />
                  <Point X="-1.367750313795" Y="-4.535045545261" Z="0.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.15" />
                  <Point X="-1.135012120963" Y="-4.951052599742" Z="0.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.15" />
                  <Point X="-0.835518759953" Y="-5.010298071087" Z="0.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.15" />
                  <Point X="-0.793811413325" Y="-4.924728651172" Z="0.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.15" />
                  <Point X="-0.26310990206" Y="-3.296921753366" Z="0.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.15" />
                  <Point X="-0.015088830121" Y="-3.208922384478" Z="0.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.15" />
                  <Point X="0.238270249241" Y="-3.27818966081" Z="0.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.15" />
                  <Point X="0.407335957372" Y="-3.504724002402" Z="0.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.15" />
                  <Point X="0.440943447076" Y="-3.607807378644" Z="0.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.15" />
                  <Point X="0.987269864611" Y="-4.982952496531" Z="0.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.15" />
                  <Point X="1.166390058479" Y="-4.944092599902" Z="0.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.15" />
                  <Point X="1.163968286797" Y="-4.8423672347" Z="0.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.15" />
                  <Point X="1.007955103049" Y="-3.040071127358" Z="0.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.15" />
                  <Point X="1.180424798384" Y="-2.884587897561" Z="0.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.15" />
                  <Point X="1.410348949887" Y="-2.855503978055" Z="0.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.15" />
                  <Point X="1.62466130522" Y="-2.983085105949" Z="0.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.15" />
                  <Point X="1.698379597175" Y="-3.070775494288" Z="0.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.15" />
                  <Point X="2.845646816922" Y="-4.207809783023" Z="0.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.15" />
                  <Point X="3.0352424511" Y="-4.073164982596" Z="0.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.15" />
                  <Point X="3.000340966759" Y="-3.985143387291" Z="0.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.15" />
                  <Point X="2.234535298159" Y="-2.519077473294" Z="0.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.15" />
                  <Point X="2.3210634839" Y="-2.337381411913" Z="0.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.15" />
                  <Point X="2.495516966459" Y="-2.237837826067" Z="0.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.15" />
                  <Point X="2.709429330218" Y="-2.268912711332" Z="0.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.15" />
                  <Point X="2.802270209816" Y="-2.317408533515" Z="0.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.15" />
                  <Point X="4.229323494738" Y="-2.813194915548" Z="0.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.15" />
                  <Point X="4.38971032048" Y="-2.555716420361" Z="0.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.15" />
                  <Point X="4.327357336097" Y="-2.485213481267" Z="0.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.15" />
                  <Point X="3.098246623784" Y="-1.467610399045" Z="0.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.15" />
                  <Point X="3.107054717467" Y="-1.297552003838" Z="0.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.15" />
                  <Point X="3.211199707681" Y="-1.163244746868" Z="0.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.15" />
                  <Point X="3.388486774659" Y="-1.118270693293" Z="0.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.15" />
                  <Point X="3.489091522686" Y="-1.127741720224" Z="0.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.15" />
                  <Point X="4.986410051966" Y="-0.966457639766" Z="0.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.15" />
                  <Point X="5.044645683828" Y="-0.591510038319" Z="0.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.15" />
                  <Point X="4.970589745638" Y="-0.548415230941" Z="0.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.15" />
                  <Point X="3.660952157854" Y="-0.170522764267" Z="0.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.15" />
                  <Point X="3.603242389607" Y="-0.100822726381" Z="0.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.15" />
                  <Point X="3.582536615348" Y="-0.005753651719" Z="0.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.15" />
                  <Point X="3.598834835076" Y="0.090856879526" Z="0.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.15" />
                  <Point X="3.652137048793" Y="0.163126012194" Z="0.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.15" />
                  <Point X="3.742443256498" Y="0.217626079975" Z="0.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.15" />
                  <Point X="3.825378071668" Y="0.241556701828" Z="0.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.15" />
                  <Point X="4.986037699857" Y="0.967232133109" Z="0.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.15" />
                  <Point X="4.886820031753" Y="1.383874968824" Z="0.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.15" />
                  <Point X="4.796356313982" Y="1.397547841595" Z="0.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.15" />
                  <Point X="3.374568376336" Y="1.233727445843" Z="0.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.15" />
                  <Point X="3.303466093602" Y="1.271336324322" Z="0.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.15" />
                  <Point X="3.25412306934" Y="1.342366178082" Z="0.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.15" />
                  <Point X="3.234644310549" Y="1.42724933336" Z="0.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.15" />
                  <Point X="3.253834124319" Y="1.504730072772" Z="0.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.15" />
                  <Point X="3.309456934829" Y="1.580205752644" Z="0.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.15" />
                  <Point X="3.380458303065" Y="1.636535820832" Z="0.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.15" />
                  <Point X="4.250638612368" Y="2.780165975547" Z="0.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.15" />
                  <Point X="4.016311573031" Y="3.109099700803" Z="0.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.15" />
                  <Point X="3.913382100258" Y="3.077312237825" Z="0.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.15" />
                  <Point X="2.434373113763" Y="2.246807798973" Z="0.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.15" />
                  <Point X="2.364301394354" Y="2.253401971021" Z="0.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.15" />
                  <Point X="2.300628430385" Y="2.294299730617" Z="0.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.15" />
                  <Point X="2.256458869618" Y="2.356396429997" Z="0.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.15" />
                  <Point X="2.246027731738" Y="2.425457049478" Z="0.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.15" />
                  <Point X="2.265720143684" Y="2.505096462119" Z="0.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.15" />
                  <Point X="2.318313115035" Y="2.598756981697" Z="0.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.15" />
                  <Point X="2.775838676083" Y="4.253144432393" Z="0.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.15" />
                  <Point X="2.379449882441" Y="4.486953207086" Z="0.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.15" />
                  <Point X="1.968551955935" Y="4.681838926665" Z="0.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.15" />
                  <Point X="1.542185104964" Y="4.839059007699" Z="0.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.15" />
                  <Point X="0.901518366163" Y="4.996821896732" Z="0.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.15" />
                  <Point X="0.233105642505" Y="5.072148512605" Z="0.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.15" />
                  <Point X="0.181735858694" Y="5.033371966389" Z="0.15" />
                  <Point X="0" Y="4.355124473572" Z="0.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>