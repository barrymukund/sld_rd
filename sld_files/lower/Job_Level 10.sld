<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#135" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="873" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.634471435547" Y="-28.7781328125" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542362426758" Y="-28.467375" />
                  <Point X="0.513273742676" Y="-28.42546484375" />
                  <Point X="0.378634918213" Y="-28.231474609375" />
                  <Point X="0.356748718262" Y="-28.209017578125" />
                  <Point X="0.330493682861" Y="-28.189775390625" />
                  <Point X="0.302492431641" Y="-28.17566796875" />
                  <Point X="0.257479156494" Y="-28.16169921875" />
                  <Point X="0.049133304596" Y="-28.09703515625" />
                  <Point X="0.020983165741" Y="-28.092767578125" />
                  <Point X="-0.008658312798" Y="-28.092765625" />
                  <Point X="-0.036824542999" Y="-28.09703515625" />
                  <Point X="-0.081837684631" Y="-28.111005859375" />
                  <Point X="-0.290183685303" Y="-28.17566796875" />
                  <Point X="-0.318184906006" Y="-28.18977734375" />
                  <Point X="-0.344440093994" Y="-28.209021484375" />
                  <Point X="-0.366323883057" Y="-28.2314765625" />
                  <Point X="-0.395412536621" Y="-28.273388671875" />
                  <Point X="-0.530051696777" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.880401855469" Y="-29.74190625" />
                  <Point X="-0.916584533691" Y="-29.87694140625" />
                  <Point X="-1.079327636719" Y="-29.8453515625" />
                  <Point X="-1.127652099609" Y="-29.832919921875" />
                  <Point X="-1.24641784668" Y="-29.80236328125" />
                  <Point X="-1.223530151367" Y="-29.628513671875" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.227262329102" Y="-29.4621640625" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.323645141602" Y="-29.131203125" />
                  <Point X="-1.365087768555" Y="-29.094859375" />
                  <Point X="-1.55690612793" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886474609" Y="-28.897994140625" />
                  <Point X="-1.643026977539" Y="-28.890966796875" />
                  <Point X="-1.698030273438" Y="-28.887361328125" />
                  <Point X="-1.952616088867" Y="-28.87067578125" />
                  <Point X="-1.983414794922" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042656738281" Y="-28.89480078125" />
                  <Point X="-2.088488525391" Y="-28.925423828125" />
                  <Point X="-2.300623291016" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102783203" Y="-29.099462890625" />
                  <Point X="-2.480148193359" Y="-29.288490234375" />
                  <Point X="-2.801720947266" Y="-29.089380859375" />
                  <Point X="-2.868603027344" Y="-29.0378828125" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.556021484375" Y="-27.905701171875" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.411279052734" Y="-27.646970703125" />
                  <Point X="-2.405863525391" Y="-27.624439453125" />
                  <Point X="-2.406064208984" Y="-27.601267578125" />
                  <Point X="-2.410209472656" Y="-27.569787109375" />
                  <Point X="-2.416627929688" Y="-27.545833984375" />
                  <Point X="-2.429026611328" Y="-27.524359375" />
                  <Point X="-2.441692871094" Y="-27.5078515625" />
                  <Point X="-2.449889404297" Y="-27.49850390625" />
                  <Point X="-2.464155761719" Y="-27.48423828125" />
                  <Point X="-2.489311767578" Y="-27.466212890625" />
                  <Point X="-2.518140625" Y="-27.45199609375" />
                  <Point X="-2.547758789062" Y="-27.44301171875" />
                  <Point X="-2.578693359375" Y="-27.444025390625" />
                  <Point X="-2.610219238281" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-3.699501464844" Y="-28.073373046875" />
                  <Point X="-3.818022949219" Y="-28.141802734375" />
                  <Point X="-4.082864013672" Y="-27.79385546875" />
                  <Point X="-4.130815917969" Y="-27.713447265625" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.3373671875" Y="-26.676083984375" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083060302734" Y="-26.475875" />
                  <Point X="-3.0642421875" Y="-26.44614453125" />
                  <Point X="-3.056159667969" Y="-26.425791015625" />
                  <Point X="-3.052487304688" Y="-26.414546875" />
                  <Point X="-3.046151611328" Y="-26.390083984375" />
                  <Point X="-3.042037353516" Y="-26.3586015625" />
                  <Point X="-3.042737060547" Y="-26.335732421875" />
                  <Point X="-3.048884277344" Y="-26.313693359375" />
                  <Point X="-3.060890136719" Y="-26.2847109375" />
                  <Point X="-3.073294921875" Y="-26.263228515625" />
                  <Point X="-3.090838623047" Y="-26.245689453125" />
                  <Point X="-3.108046386719" Y="-26.232490234375" />
                  <Point X="-3.117681640625" Y="-26.22599609375" />
                  <Point X="-3.139459228516" Y="-26.2131796875" />
                  <Point X="-3.168722167969" Y="-26.201955078125" />
                  <Point X="-3.200608154297" Y="-26.195474609375" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.570479980469" Y="-26.370607421875" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.846765136719" Y="-25.9039453125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-3.796489746094" Y="-25.291044921875" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.513689697266" Y="-25.2129609375" />
                  <Point X="-3.493190917969" Y="-25.202318359375" />
                  <Point X="-3.482798095703" Y="-25.196048828125" />
                  <Point X="-3.459975830078" Y="-25.180208984375" />
                  <Point X="-3.436020263672" Y="-25.158681640625" />
                  <Point X="-3.415780029297" Y="-25.129826171875" />
                  <Point X="-3.4067109375" Y="-25.10979296875" />
                  <Point X="-3.402525390625" Y="-25.0987734375" />
                  <Point X="-3.39491796875" Y="-25.074263671875" />
                  <Point X="-3.389474365234" Y="-25.045521484375" />
                  <Point X="-3.390129150391" Y="-25.0132421875" />
                  <Point X="-3.394019775391" Y="-24.993125" />
                  <Point X="-3.396560791016" Y="-24.983005859375" />
                  <Point X="-3.404168212891" Y="-24.958494140625" />
                  <Point X="-3.417483398438" Y="-24.929169921875" />
                  <Point X="-3.439088623047" Y="-24.901017578125" />
                  <Point X="-3.455837890625" Y="-24.886078125" />
                  <Point X="-3.464906494141" Y="-24.8789296875" />
                  <Point X="-3.487728759766" Y="-24.86308984375" />
                  <Point X="-3.501923095703" Y="-24.854953125" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.753027832031" Y="-24.515212890625" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.824488769531" Y="-24.02303125" />
                  <Point X="-4.798946289062" Y="-23.928771484375" />
                  <Point X="-4.703550292969" Y="-23.576732421875" />
                  <Point X="-3.954631103516" Y="-23.675330078125" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744991210938" Y="-23.700658203125" />
                  <Point X="-3.723435546875" Y="-23.6987734375" />
                  <Point X="-3.703155029297" Y="-23.6947421875" />
                  <Point X="-3.692221923828" Y="-23.691296875" />
                  <Point X="-3.641708984375" Y="-23.675369140625" />
                  <Point X="-3.622775878906" Y="-23.667037109375" />
                  <Point X="-3.604032226562" Y="-23.65621484375" />
                  <Point X="-3.587352783203" Y="-23.643984375" />
                  <Point X="-3.573715576172" Y="-23.62843359375" />
                  <Point X="-3.561301513672" Y="-23.610705078125" />
                  <Point X="-3.551350830078" Y="-23.592568359375" />
                  <Point X="-3.546971923828" Y="-23.58199609375" />
                  <Point X="-3.526703369141" Y="-23.533064453125" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532049316406" Y="-23.410623046875" />
                  <Point X="-3.537333007813" Y="-23.40047265625" />
                  <Point X="-3.5617890625" Y="-23.353494140625" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-4.302018066406" Y="-22.76837109375" />
                  <Point X="-4.351860351563" Y="-22.730126953125" />
                  <Point X="-4.081155029297" Y="-22.266345703125" />
                  <Point X="-4.013498291016" Y="-22.179380859375" />
                  <Point X="-3.750503417969" Y="-21.841337890625" />
                  <Point X="-3.322713867188" Y="-22.088322265625" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187728759766" Y="-22.163658203125" />
                  <Point X="-3.167086181641" Y="-22.17016796875" />
                  <Point X="-3.146796142578" Y="-22.174205078125" />
                  <Point X="-3.131596923828" Y="-22.17553515625" />
                  <Point X="-3.061246826172" Y="-22.181689453125" />
                  <Point X="-3.040560302734" Y="-22.18123828125" />
                  <Point X="-3.019102050781" Y="-22.178412109375" />
                  <Point X="-2.999012695312" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946076171875" Y="-22.13976953125" />
                  <Point X="-2.935287597656" Y="-22.12898046875" />
                  <Point X="-2.885352539062" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.844765625" Y="-21.948681640625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.179934082031" Y="-21.281291015625" />
                  <Point X="-3.179086914062" Y="-21.2721484375" />
                  <Point X="-2.700622558594" Y="-20.905314453125" />
                  <Point X="-2.59407421875" Y="-20.846119140625" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.078723876953" Y="-20.72395703125" />
                  <Point X="-2.04319519043" Y="-20.7702578125" />
                  <Point X="-2.028892333984" Y="-20.78519921875" />
                  <Point X="-2.012312988281" Y="-20.799111328125" />
                  <Point X="-1.995115356445" Y="-20.810603515625" />
                  <Point X="-1.978198974609" Y="-20.81941015625" />
                  <Point X="-1.899899414062" Y="-20.860171875" />
                  <Point X="-1.880625366211" Y="-20.86766796875" />
                  <Point X="-1.859718505859" Y="-20.873271484375" />
                  <Point X="-1.839268920898" Y="-20.876419921875" />
                  <Point X="-1.818622680664" Y="-20.87506640625" />
                  <Point X="-1.797306884766" Y="-20.871306640625" />
                  <Point X="-1.777451416016" Y="-20.865517578125" />
                  <Point X="-1.759831787109" Y="-20.85821875" />
                  <Point X="-1.678277587891" Y="-20.8244375" />
                  <Point X="-1.660145751953" Y="-20.814490234375" />
                  <Point X="-1.642416259766" Y="-20.802076171875" />
                  <Point X="-1.626864013672" Y="-20.7884375" />
                  <Point X="-1.614632568359" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.589745117188" Y="-20.715888671875" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584202026367" Y="-20.3681015625" />
                  <Point X="-1.569033081055" Y="-20.363849609375" />
                  <Point X="-0.949624938965" Y="-20.19019140625" />
                  <Point X="-0.820468933105" Y="-20.17507421875" />
                  <Point X="-0.294711303711" Y="-20.113541015625" />
                  <Point X="-0.168236175537" Y="-20.585552734375" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082114021301" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155905724" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425994873" Y="-20.791193359375" />
                  <Point X="0.115583656311" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.305126922607" Y="-20.120619140625" />
                  <Point X="0.307419769287" Y="-20.1120625" />
                  <Point X="0.844044799805" Y="-20.16826171875" />
                  <Point X="0.950909179688" Y="-20.1940625" />
                  <Point X="1.48102331543" Y="-20.322048828125" />
                  <Point X="1.549368041992" Y="-20.346837890625" />
                  <Point X="1.894650756836" Y="-20.47207421875" />
                  <Point X="1.961912353516" Y="-20.503529296875" />
                  <Point X="2.294562011719" Y="-20.659099609375" />
                  <Point X="2.359591064453" Y="-20.696984375" />
                  <Point X="2.680968017578" Y="-20.88421875" />
                  <Point X="2.742267089844" Y="-20.9278125" />
                  <Point X="2.943259277344" Y="-21.07074609375" />
                  <Point X="2.300662353516" Y="-22.1837578125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.139762939453" Y="-22.4662578125" />
                  <Point X="2.130947021484" Y="-22.49260546875" />
                  <Point X="2.129262207031" Y="-22.498208984375" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108383300781" Y="-22.589228515625" />
                  <Point X="2.108532958984" Y="-22.62046484375" />
                  <Point X="2.109215087891" Y="-22.6313828125" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129708496094" Y="-22.732484375" />
                  <Point X="2.1400703125" Y="-22.75252734375" />
                  <Point X="2.147702392578" Y="-22.763775390625" />
                  <Point X="2.183028076172" Y="-22.8158359375" />
                  <Point X="2.199996582031" Y="-22.83478125" />
                  <Point X="2.224543945312" Y="-22.85571484375" />
                  <Point X="2.232845703125" Y="-22.862041015625" />
                  <Point X="2.284906738281" Y="-22.8973671875" />
                  <Point X="2.304953125" Y="-22.90773046875" />
                  <Point X="2.327041259766" Y="-22.91599609375" />
                  <Point X="2.348970947266" Y="-22.92133984375" />
                  <Point X="2.361305419922" Y="-22.922826171875" />
                  <Point X="2.418395751953" Y="-22.9297109375" />
                  <Point X="2.44434375" Y="-22.929267578125" />
                  <Point X="2.47751171875" Y="-22.924115234375" />
                  <Point X="2.487472412109" Y="-22.922015625" />
                  <Point X="2.553494628906" Y="-22.904359375" />
                  <Point X="2.565289550781" Y="-22.900359375" />
                  <Point X="2.588533691406" Y="-22.889853515625" />
                  <Point X="3.815771972656" Y="-22.181306640625" />
                  <Point X="3.967325439453" Y="-22.093806640625" />
                  <Point X="4.123270019531" Y="-22.31053515625" />
                  <Point X="4.157439941406" Y="-22.367001953125" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.432329589844" Y="-23.176896484375" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.216647949219" Y="-23.344904296875" />
                  <Point X="3.197137451172" Y="-23.367546875" />
                  <Point X="3.193708251953" Y="-23.371765625" />
                  <Point X="3.146191894531" Y="-23.43375390625" />
                  <Point X="3.133427001953" Y="-23.456158203125" />
                  <Point X="3.121133544922" Y="-23.486783203125" />
                  <Point X="3.117805908203" Y="-23.496587890625" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739746094" Y="-23.628234375" />
                  <Point X="3.10087890625" Y="-23.643447265625" />
                  <Point X="3.115408691406" Y="-23.7138671875" />
                  <Point X="3.124108642578" Y="-23.738392578125" />
                  <Point X="3.139844726562" Y="-23.76874609375" />
                  <Point X="3.144820556641" Y="-23.777236328125" />
                  <Point X="3.184340087891" Y="-23.8373046875" />
                  <Point X="3.198892578125" Y="-23.854548828125" />
                  <Point X="3.216135986328" Y="-23.870638671875" />
                  <Point X="3.234346679688" Y="-23.88396484375" />
                  <Point X="3.246719482422" Y="-23.8909296875" />
                  <Point X="3.303988769531" Y="-23.92316796875" />
                  <Point X="3.328722412109" Y="-23.93283203125" />
                  <Point X="3.363427978516" Y="-23.941041015625" />
                  <Point X="3.372849365234" Y="-23.9427734375" />
                  <Point X="3.450281005859" Y="-23.953005859375" />
                  <Point X="3.462698486328" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.653998046875" Y="-23.799537109375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.856703613281" Y="-24.136349609375" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="3.947555664062" Y="-24.60851953125" />
                  <Point X="3.716579589844" Y="-24.67041015625" />
                  <Point X="3.698089355469" Y="-24.677501953125" />
                  <Point X="3.669573730469" Y="-24.692009765625" />
                  <Point X="3.665112060547" Y="-24.694431640625" />
                  <Point X="3.589037841797" Y="-24.73840234375" />
                  <Point X="3.568302001953" Y="-24.754595703125" />
                  <Point X="3.544127929688" Y="-24.77958203125" />
                  <Point X="3.537669433594" Y="-24.78698828125" />
                  <Point X="3.492024902344" Y="-24.845150390625" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.460393554688" Y="-24.924560546875" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443877197266" Y="-25.0305234375" />
                  <Point X="3.447164794922" Y="-25.06649609375" />
                  <Point X="3.448466064453" Y="-25.07571875" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.176662109375" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.4920234375" Y="-25.217408203125" />
                  <Point X="3.501884765625" Y="-25.229974609375" />
                  <Point X="3.547529296875" Y="-25.28813671875" />
                  <Point X="3.567118652344" Y="-25.30684375" />
                  <Point X="3.597866455078" Y="-25.328763671875" />
                  <Point X="3.605471191406" Y="-25.33365625" />
                  <Point X="3.681545410156" Y="-25.37762890625" />
                  <Point X="3.692709228516" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.785666015625" Y="-25.67861328125" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.841227539062" Y="-26.009177734375" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.692249267578" Y="-26.03870703125" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374657714844" Y="-26.005509765625" />
                  <Point X="3.342400146484" Y="-26.012521484375" />
                  <Point X="3.193093505859" Y="-26.04497265625" />
                  <Point X="3.163973876953" Y="-26.05659765625" />
                  <Point X="3.136147216797" Y="-26.073490234375" />
                  <Point X="3.112395996094" Y="-26.093962890625" />
                  <Point X="3.092898193359" Y="-26.1174140625" />
                  <Point X="3.002651855469" Y="-26.225951171875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.966963134766" Y="-26.335734375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.507564453125" />
                  <Point X="2.964078613281" Y="-26.539185546875" />
                  <Point X="2.976451660156" Y="-26.568" />
                  <Point X="2.994303466797" Y="-26.595765625" />
                  <Point X="3.076931884766" Y="-26.7242890625" />
                  <Point X="3.086931396484" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="4.102747070312" Y="-27.522189453125" />
                  <Point X="4.213122558594" Y="-27.606884765625" />
                  <Point X="4.124811035156" Y="-27.74978515625" />
                  <Point X="4.096280273438" Y="-27.790322265625" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="3.039526611328" Y="-27.314685546875" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754224609375" Y="-27.159826171875" />
                  <Point X="2.715832763672" Y="-27.152892578125" />
                  <Point X="2.538134277344" Y="-27.12080078125" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.444834472656" Y="-27.135177734375" />
                  <Point X="2.412940185547" Y="-27.151962890625" />
                  <Point X="2.26531640625" Y="-27.22965625" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531494141" Y="-27.290439453125" />
                  <Point X="2.187745849609" Y="-27.322333984375" />
                  <Point X="2.110052490234" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.102608642578" Y="-27.601650390625" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.789355224609" Y="-28.93032421875" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.781839111328" Y="-29.11165234375" />
                  <Point X="2.749953369141" Y="-29.132291015625" />
                  <Point X="2.701764648438" Y="-29.163482421875" />
                  <Point X="1.940598510742" Y="-28.17151171875" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747506469727" Y="-27.922181640625" />
                  <Point X="1.721924316406" Y="-27.900556640625" />
                  <Point X="1.684059814453" Y="-27.876212890625" />
                  <Point X="1.50880078125" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.75116796875" />
                  <Point X="1.448366088867" Y="-27.7434375" />
                  <Point X="1.417100341797" Y="-27.741119140625" />
                  <Point X="1.375688842773" Y="-27.7449296875" />
                  <Point X="1.184013549805" Y="-27.76256640625" />
                  <Point X="1.156363037109" Y="-27.7693984375" />
                  <Point X="1.128977050781" Y="-27.7807421875" />
                  <Point X="1.104595336914" Y="-27.795462890625" />
                  <Point X="1.072618530273" Y="-27.82205078125" />
                  <Point X="0.924611633301" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.866063537598" Y="-28.069796875" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="1.000415161133" Y="-29.695466796875" />
                  <Point X="1.022065307617" Y="-29.859916015625" />
                  <Point X="0.975716430664" Y="-29.87007421875" />
                  <Point X="0.946208251953" Y="-29.875435546875" />
                  <Point X="0.929315429688" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058437988281" Y="-29.7526328125" />
                  <Point X="-1.103983642578" Y="-29.740916015625" />
                  <Point X="-1.14124621582" Y="-29.731328125" />
                  <Point X="-1.129342895508" Y="-29.6409140625" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.54103515625" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.134087646484" Y="-29.443630859375" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124633789" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230573486328" Y="-29.094861328125" />
                  <Point X="-1.250208740234" Y="-29.070935546875" />
                  <Point X="-1.26100769043" Y="-29.05977734375" />
                  <Point X="-1.302450195312" Y="-29.02343359375" />
                  <Point X="-1.494268554688" Y="-28.855212890625" />
                  <Point X="-1.506739990234" Y="-28.84596484375" />
                  <Point X="-1.533023071289" Y="-28.82962109375" />
                  <Point X="-1.546834838867" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591315551758" Y="-28.805474609375" />
                  <Point X="-1.621456054688" Y="-28.798447265625" />
                  <Point X="-1.636813110352" Y="-28.796169921875" />
                  <Point X="-1.69181640625" Y="-28.792564453125" />
                  <Point X="-1.94640222168" Y="-28.77587890625" />
                  <Point X="-1.961927124023" Y="-28.7761328125" />
                  <Point X="-1.992725952148" Y="-28.779166015625" />
                  <Point X="-2.007999511719" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053668457031" Y="-28.795494140625" />
                  <Point X="-2.081861816406" Y="-28.808267578125" />
                  <Point X="-2.095435058594" Y="-28.815810546875" />
                  <Point X="-2.141266845703" Y="-28.84643359375" />
                  <Point X="-2.353401611328" Y="-28.988177734375" />
                  <Point X="-2.359680419922" Y="-28.992755859375" />
                  <Point X="-2.380451171875" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471435547" Y="-29.041630859375" />
                  <Point X="-2.503201416016" Y="-29.162478515625" />
                  <Point X="-2.74760546875" Y="-29.011150390625" />
                  <Point X="-2.810645019531" Y="-28.962611328125" />
                  <Point X="-2.980863037109" Y="-28.83155078125" />
                  <Point X="-2.473749023438" Y="-27.953201171875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.336204345703" Y="-27.71348046875" />
                  <Point X="-2.323722167969" Y="-27.68383203125" />
                  <Point X="-2.318909667969" Y="-27.669171875" />
                  <Point X="-2.313494140625" Y="-27.646640625" />
                  <Point X="-2.3108671875" Y="-27.6236171875" />
                  <Point X="-2.311067871094" Y="-27.6004453125" />
                  <Point X="-2.311877197266" Y="-27.588865234375" />
                  <Point X="-2.316022460938" Y="-27.557384765625" />
                  <Point X="-2.318446777344" Y="-27.54519921875" />
                  <Point X="-2.324865234375" Y="-27.52124609375" />
                  <Point X="-2.334355957031" Y="-27.498333984375" />
                  <Point X="-2.346754638672" Y="-27.476859375" />
                  <Point X="-2.353656738281" Y="-27.466529296875" />
                  <Point X="-2.366322998047" Y="-27.450021484375" />
                  <Point X="-2.382716064453" Y="-27.431326171875" />
                  <Point X="-2.396982421875" Y="-27.417060546875" />
                  <Point X="-2.408822753906" Y="-27.407015625" />
                  <Point X="-2.433978759766" Y="-27.388990234375" />
                  <Point X="-2.447294433594" Y="-27.381009765625" />
                  <Point X="-2.476123291016" Y="-27.36679296875" />
                  <Point X="-2.490564208984" Y="-27.3610859375" />
                  <Point X="-2.520182373047" Y="-27.3521015625" />
                  <Point X="-2.550870117188" Y="-27.3480625" />
                  <Point X="-2.5818046875" Y="-27.349076171875" />
                  <Point X="-2.597228759766" Y="-27.3508515625" />
                  <Point X="-2.628754638672" Y="-27.357123046875" />
                  <Point X="-2.643685302734" Y="-27.36138671875" />
                  <Point X="-2.672649902344" Y="-27.3722890625" />
                  <Point X="-2.686683837891" Y="-27.378927734375" />
                  <Point X="-3.747001464844" Y="-27.9911015625" />
                  <Point X="-3.793087158203" Y="-28.017708984375" />
                  <Point X="-4.004022705078" Y="-27.740583984375" />
                  <Point X="-4.049223144531" Y="-27.6647890625" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.279534912109" Y="-26.751453125" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039154785156" Y="-26.566064453125" />
                  <Point X="-3.016260498047" Y="-26.543423828125" />
                  <Point X="-3.002788818359" Y="-26.52668359375" />
                  <Point X="-2.983970703125" Y="-26.496953125" />
                  <Point X="-2.975948974609" Y="-26.48120703125" />
                  <Point X="-2.967866455078" Y="-26.460853515625" />
                  <Point X="-2.960521728516" Y="-26.438365234375" />
                  <Point X="-2.954186035156" Y="-26.41390234375" />
                  <Point X="-2.951952636719" Y="-26.40239453125" />
                  <Point X="-2.947838378906" Y="-26.370912109375" />
                  <Point X="-2.947081787109" Y="-26.355697265625" />
                  <Point X="-2.947781494141" Y="-26.332828125" />
                  <Point X="-2.951229980469" Y="-26.310208984375" />
                  <Point X="-2.957377197266" Y="-26.288169921875" />
                  <Point X="-2.961116699219" Y="-26.2773359375" />
                  <Point X="-2.973122558594" Y="-26.248353515625" />
                  <Point X="-2.978620849609" Y="-26.237205078125" />
                  <Point X="-2.991025634766" Y="-26.21572265625" />
                  <Point X="-3.006128662109" Y="-26.196044921875" />
                  <Point X="-3.023672363281" Y="-26.178505859375" />
                  <Point X="-3.03301953125" Y="-26.170310546875" />
                  <Point X="-3.050227294922" Y="-26.157111328125" />
                  <Point X="-3.069497802734" Y="-26.144123046875" />
                  <Point X="-3.091275390625" Y="-26.131306640625" />
                  <Point X="-3.105436523438" Y="-26.12448046875" />
                  <Point X="-3.134699462891" Y="-26.113255859375" />
                  <Point X="-3.149801269531" Y="-26.108857421875" />
                  <Point X="-3.181687255859" Y="-26.102376953125" />
                  <Point X="-3.197304443359" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.582879882812" Y="-26.276419921875" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.740763183594" Y="-25.97411328125" />
                  <Point X="-4.752722167969" Y="-25.89049609375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-3.771901855469" Y="-25.38280859375" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.4984921875" Y="-25.30896875" />
                  <Point X="-3.479305908203" Y="-25.30151953125" />
                  <Point X="-3.469915527344" Y="-25.297275390625" />
                  <Point X="-3.449416748047" Y="-25.2866328125" />
                  <Point X="-3.428631103516" Y="-25.27409375" />
                  <Point X="-3.405808837891" Y="-25.25825390625" />
                  <Point X="-3.396477539062" Y="-25.250869140625" />
                  <Point X="-3.372521972656" Y="-25.229341796875" />
                  <Point X="-3.358245605469" Y="-25.213236328125" />
                  <Point X="-3.338005371094" Y="-25.184380859375" />
                  <Point X="-3.329235351562" Y="-25.169005859375" />
                  <Point X="-3.320166259766" Y="-25.14897265625" />
                  <Point X="-3.311795166016" Y="-25.12693359375" />
                  <Point X="-3.304187744141" Y="-25.102423828125" />
                  <Point X="-3.301577392578" Y="-25.09194140625" />
                  <Point X="-3.296133789062" Y="-25.06319921875" />
                  <Point X="-3.294493896484" Y="-25.043595703125" />
                  <Point X="-3.295148681641" Y="-25.01131640625" />
                  <Point X="-3.296857421875" Y="-24.995203125" />
                  <Point X="-3.300748046875" Y="-24.9750859375" />
                  <Point X="-3.305830078125" Y="-24.95484765625" />
                  <Point X="-3.3134375" Y="-24.9303359375" />
                  <Point X="-3.317667724609" Y="-24.919216796875" />
                  <Point X="-3.330982910156" Y="-24.889892578125" />
                  <Point X="-3.342118896484" Y="-24.87133203125" />
                  <Point X="-3.363724121094" Y="-24.8431796875" />
                  <Point X="-3.375853027344" Y="-24.83012109375" />
                  <Point X="-3.392602294922" Y="-24.815181640625" />
                  <Point X="-3.410739501953" Y="-24.800884765625" />
                  <Point X="-3.433561767578" Y="-24.785044921875" />
                  <Point X="-3.440483154297" Y="-24.780671875" />
                  <Point X="-3.465612548828" Y="-24.767166015625" />
                  <Point X="-3.496565429688" Y="-24.75436328125" />
                  <Point X="-3.508288330078" Y="-24.75038671875" />
                  <Point X="-4.728439941406" Y="-24.42344921875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731332519531" Y="-24.04248046875" />
                  <Point X="-4.707252929688" Y="-23.953619140625" />
                  <Point X="-4.633584960938" Y="-23.681763671875" />
                  <Point X="-3.96703125" Y="-23.769517578125" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.767738769531" Y="-23.79518359375" />
                  <Point X="-3.747063720703" Y="-23.795634765625" />
                  <Point X="-3.736716308594" Y="-23.795296875" />
                  <Point X="-3.715160644531" Y="-23.793412109375" />
                  <Point X="-3.704914306641" Y="-23.791951171875" />
                  <Point X="-3.684633789062" Y="-23.787919921875" />
                  <Point X="-3.663653076172" Y="-23.781900390625" />
                  <Point X="-3.613140136719" Y="-23.76597265625" />
                  <Point X="-3.603443115234" Y="-23.762322265625" />
                  <Point X="-3.584510009766" Y="-23.753990234375" />
                  <Point X="-3.575273925781" Y="-23.74930859375" />
                  <Point X="-3.556530273438" Y="-23.738486328125" />
                  <Point X="-3.547855957031" Y="-23.732826171875" />
                  <Point X="-3.531176513672" Y="-23.720595703125" />
                  <Point X="-3.515927001953" Y="-23.70662109375" />
                  <Point X="-3.502289794922" Y="-23.6910703125" />
                  <Point X="-3.495896972656" Y="-23.682923828125" />
                  <Point X="-3.483482910156" Y="-23.6651953125" />
                  <Point X="-3.478013427734" Y="-23.656400390625" />
                  <Point X="-3.468062744141" Y="-23.638263671875" />
                  <Point X="-3.459202636719" Y="-23.618349609375" />
                  <Point X="-3.438934082031" Y="-23.56941796875" />
                  <Point X="-3.435498291016" Y="-23.559646484375" />
                  <Point X="-3.429710693359" Y="-23.5397890625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.43601171875" Y="-23.395470703125" />
                  <Point X="-3.443507568359" Y="-23.376193359375" />
                  <Point X="-3.453066162109" Y="-23.356607421875" />
                  <Point X="-3.477522216797" Y="-23.30962890625" />
                  <Point X="-3.482800537109" Y="-23.300712890625" />
                  <Point X="-3.494293212891" Y="-23.283513671875" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521498046875" Y="-23.251091796875" />
                  <Point X="-3.536439941406" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-4.22761328125" Y="-22.70571875" />
                  <Point X="-4.00229296875" Y="-22.31969140625" />
                  <Point X="-3.938517089844" Y="-22.23771484375" />
                  <Point X="-3.726336425781" Y="-21.964986328125" />
                  <Point X="-3.370213867188" Y="-22.17059375" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244923583984" Y="-22.242279296875" />
                  <Point X="-3.225995361328" Y="-22.250609375" />
                  <Point X="-3.216300537109" Y="-22.254259765625" />
                  <Point X="-3.195657958984" Y="-22.26076953125" />
                  <Point X="-3.185625" Y="-22.263341796875" />
                  <Point X="-3.165334960938" Y="-22.26737890625" />
                  <Point X="-3.139878662109" Y="-22.270173828125" />
                  <Point X="-3.069528564453" Y="-22.276328125" />
                  <Point X="-3.059175292969" Y="-22.276666015625" />
                  <Point X="-3.038488769531" Y="-22.27621484375" />
                  <Point X="-3.028155517578" Y="-22.27542578125" />
                  <Point X="-3.006697265625" Y="-22.272599609375" />
                  <Point X="-2.996512695312" Y="-22.2706875" />
                  <Point X="-2.976423339844" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.938449462891" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.902745117188" Y="-22.226806640625" />
                  <Point X="-2.886611572266" Y="-22.213857421875" />
                  <Point X="-2.868110839844" Y="-22.196154296875" />
                  <Point X="-2.81817578125" Y="-22.146220703125" />
                  <Point X="-2.811264404297" Y="-22.138509765625" />
                  <Point X="-2.798320800781" Y="-22.1223828125" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.750127197266" Y="-21.94040234375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.059387207031" Y="-21.300083984375" />
                  <Point X="-2.648370361328" Y="-20.984962890625" />
                  <Point X="-2.547937011719" Y="-20.9291640625" />
                  <Point X="-2.192524169922" Y="-20.731703125" />
                  <Point X="-2.154092773438" Y="-20.7817890625" />
                  <Point X="-2.118563964844" Y="-20.82808984375" />
                  <Point X="-2.111820800781" Y="-20.835951171875" />
                  <Point X="-2.097517822266" Y="-20.850892578125" />
                  <Point X="-2.089958251953" Y="-20.85797265625" />
                  <Point X="-2.07337890625" Y="-20.871884765625" />
                  <Point X="-2.065095703125" Y="-20.87809765625" />
                  <Point X="-2.047898193359" Y="-20.88958984375" />
                  <Point X="-2.022067138672" Y="-20.90367578125" />
                  <Point X="-1.943767578125" Y="-20.9444375" />
                  <Point X="-1.934334350586" Y="-20.9487109375" />
                  <Point X="-1.915060302734" Y="-20.95620703125" />
                  <Point X="-1.905219604492" Y="-20.9594296875" />
                  <Point X="-1.88431262207" Y="-20.965033203125" />
                  <Point X="-1.874174438477" Y="-20.967166015625" />
                  <Point X="-1.853724853516" Y="-20.970314453125" />
                  <Point X="-1.833054321289" Y="-20.971216796875" />
                  <Point X="-1.812408081055" Y="-20.96986328125" />
                  <Point X="-1.80212109375" Y="-20.968623046875" />
                  <Point X="-1.780805297852" Y="-20.96486328125" />
                  <Point X="-1.770715820312" Y="-20.962509765625" />
                  <Point X="-1.750860351562" Y="-20.956720703125" />
                  <Point X="-1.723474609375" Y="-20.945986328125" />
                  <Point X="-1.641920410156" Y="-20.912205078125" />
                  <Point X="-1.632584350586" Y="-20.9077265625" />
                  <Point X="-1.614452514648" Y="-20.897779296875" />
                  <Point X="-1.605656738281" Y="-20.892310546875" />
                  <Point X="-1.587927246094" Y="-20.879896484375" />
                  <Point X="-1.579779296875" Y="-20.873501953125" />
                  <Point X="-1.564226928711" Y="-20.85986328125" />
                  <Point X="-1.550251708984" Y="-20.844611328125" />
                  <Point X="-1.538020263672" Y="-20.8279296875" />
                  <Point X="-1.532359863281" Y="-20.819255859375" />
                  <Point X="-1.521538085938" Y="-20.80051171875" />
                  <Point X="-1.51685546875" Y="-20.791271484375" />
                  <Point X="-1.508525024414" Y="-20.772337890625" />
                  <Point X="-1.499141967773" Y="-20.744455078125" />
                  <Point X="-1.47259765625" Y="-20.660267578125" />
                  <Point X="-1.470026489258" Y="-20.650236328125" />
                  <Point X="-1.465991210938" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479265991211" Y="-20.437345703125" />
                  <Point X="-0.931162841797" Y="-20.283677734375" />
                  <Point X="-0.809424987793" Y="-20.2694296875" />
                  <Point X="-0.365222503662" Y="-20.21744140625" />
                  <Point X="-0.25999911499" Y="-20.610140625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896308899" Y="-20.8749765625" />
                  <Point X="-0.099600372314" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912307739" Y="-20.88956640625" />
                  <Point X="0.139208236694" Y="-20.8749765625" />
                  <Point X="0.163763122559" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194573242188" Y="-20.8213828125" />
                  <Point X="0.21243132019" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.378190856934" Y="-20.2149921875" />
                  <Point X="0.827879333496" Y="-20.262087890625" />
                  <Point X="0.92861340332" Y="-20.286408203125" />
                  <Point X="1.453590209961" Y="-20.413154296875" />
                  <Point X="1.516975830078" Y="-20.43614453125" />
                  <Point X="1.858257568359" Y="-20.5599296875" />
                  <Point X="1.921668579102" Y="-20.589583984375" />
                  <Point X="2.250437988281" Y="-20.74333984375" />
                  <Point X="2.311769287109" Y="-20.7790703125" />
                  <Point X="2.629428710938" Y="-20.964138671875" />
                  <Point X="2.687209472656" Y="-21.00523046875" />
                  <Point X="2.817779296875" Y="-21.098083984375" />
                  <Point X="2.218389892578" Y="-22.1362578125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.060963867188" Y="-22.409884765625" />
                  <Point X="2.049672363281" Y="-22.43611328125" />
                  <Point X="2.040856445312" Y="-22.4624609375" />
                  <Point X="2.037486816406" Y="-22.47366796875" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017387207031" Y="-22.55208203125" />
                  <Point X="2.014163574219" Y="-22.577078125" />
                  <Point X="2.013384399414" Y="-22.58968359375" />
                  <Point X="2.013534057617" Y="-22.620919921875" />
                  <Point X="2.01489831543" Y="-22.642755859375" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144042969" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040734985352" Y="-22.76578125" />
                  <Point X="2.045318847656" Y="-22.776111328125" />
                  <Point X="2.055680664062" Y="-22.796154296875" />
                  <Point X="2.069090576172" Y="-22.817115234375" />
                  <Point X="2.104416259766" Y="-22.86917578125" />
                  <Point X="2.112262451172" Y="-22.879216796875" />
                  <Point X="2.129230957031" Y="-22.898162109375" />
                  <Point X="2.138353271484" Y="-22.90706640625" />
                  <Point X="2.162900634766" Y="-22.928" />
                  <Point X="2.179504150391" Y="-22.94065234375" />
                  <Point X="2.231565185547" Y="-22.975978515625" />
                  <Point X="2.241280029297" Y="-22.9817578125" />
                  <Point X="2.261326416016" Y="-22.99212109375" />
                  <Point X="2.271657958984" Y="-22.996705078125" />
                  <Point X="2.29374609375" Y="-23.004970703125" />
                  <Point X="2.304550048828" Y="-23.008294921875" />
                  <Point X="2.326479736328" Y="-23.013638671875" />
                  <Point X="2.349939941406" Y="-23.01714453125" />
                  <Point X="2.407030273438" Y="-23.024029296875" />
                  <Point X="2.420018798828" Y="-23.024697265625" />
                  <Point X="2.445966796875" Y="-23.02425390625" />
                  <Point X="2.458926269531" Y="-23.023142578125" />
                  <Point X="2.492094238281" Y="-23.017990234375" />
                  <Point X="2.512015625" Y="-23.013791015625" />
                  <Point X="2.578037841797" Y="-22.996134765625" />
                  <Point X="2.584005126953" Y="-22.994326171875" />
                  <Point X="2.604416503906" Y="-22.986927734375" />
                  <Point X="2.627660644531" Y="-22.976421875" />
                  <Point X="2.636033691406" Y="-22.972125" />
                  <Point X="3.863271972656" Y="-22.263578125" />
                  <Point X="3.940403076172" Y="-22.219046875" />
                  <Point X="4.043952392578" Y="-22.36295703125" />
                  <Point X="4.076162841797" Y="-22.416185546875" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.374497314453" Y="-23.10152734375" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.165541503906" Y="-23.262494140625" />
                  <Point X="3.144679931641" Y="-23.282890625" />
                  <Point X="3.125169433594" Y="-23.305533203125" />
                  <Point X="3.118311035156" Y="-23.313970703125" />
                  <Point X="3.070794677734" Y="-23.375958984375" />
                  <Point X="3.063649169922" Y="-23.386724609375" />
                  <Point X="3.050884277344" Y="-23.40912890625" />
                  <Point X="3.045264892578" Y="-23.420767578125" />
                  <Point X="3.032971435547" Y="-23.451392578125" />
                  <Point X="3.026316162109" Y="-23.471001953125" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003078125" Y="-23.63624609375" />
                  <Point X="3.007839111328" Y="-23.662646484375" />
                  <Point X="3.022368896484" Y="-23.73306640625" />
                  <Point X="3.025875" Y="-23.745626953125" />
                  <Point X="3.034574951172" Y="-23.77015234375" />
                  <Point X="3.039768798828" Y="-23.7821171875" />
                  <Point X="3.055504882812" Y="-23.812470703125" />
                  <Point X="3.065456542969" Y="-23.829451171875" />
                  <Point X="3.104976074219" Y="-23.88951953125" />
                  <Point X="3.111738037109" Y="-23.89857421875" />
                  <Point X="3.126290527344" Y="-23.915818359375" />
                  <Point X="3.134081054688" Y="-23.9240078125" />
                  <Point X="3.151324462891" Y="-23.94009765625" />
                  <Point X="3.160033935547" Y="-23.9473046875" />
                  <Point X="3.178244628906" Y="-23.960630859375" />
                  <Point X="3.200118652344" Y="-23.97371484375" />
                  <Point X="3.257387939453" Y="-24.005953125" />
                  <Point X="3.269415283203" Y="-24.011654296875" />
                  <Point X="3.294148925781" Y="-24.021318359375" />
                  <Point X="3.306855224609" Y="-24.02528125" />
                  <Point X="3.341560791016" Y="-24.033490234375" />
                  <Point X="3.360403564453" Y="-24.036955078125" />
                  <Point X="3.437835205078" Y="-24.0471875" />
                  <Point X="3.444033691406" Y="-24.04780078125" />
                  <Point X="3.465708740234" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.666397949219" Y="-23.893724609375" />
                  <Point X="4.704704589844" Y="-23.888681640625" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.762834472656" Y="-24.15096484375" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="3.922967773438" Y="-24.516755859375" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.682559570313" Y="-24.5817109375" />
                  <Point X="3.655011230469" Y="-24.592830078125" />
                  <Point X="3.626495605469" Y="-24.607337890625" />
                  <Point X="3.617572265625" Y="-24.612181640625" />
                  <Point X="3.541498046875" Y="-24.65615234375" />
                  <Point X="3.530566162109" Y="-24.663529296875" />
                  <Point X="3.509830322266" Y="-24.67972265625" />
                  <Point X="3.500026367188" Y="-24.6885390625" />
                  <Point X="3.475852294922" Y="-24.713525390625" />
                  <Point X="3.462935302734" Y="-24.728337890625" />
                  <Point X="3.417290771484" Y="-24.7865" />
                  <Point X="3.410854003906" Y="-24.795791015625" />
                  <Point X="3.399129394531" Y="-24.815072265625" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.38000390625" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.367089355469" Y="-24.90669140625" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.35029296875" Y="-24.999349609375" />
                  <Point X="3.348991455078" Y="-25.025865234375" />
                  <Point X="3.349271484375" Y="-25.039169921875" />
                  <Point X="3.352559082031" Y="-25.075142578125" />
                  <Point X="3.355161621094" Y="-25.093587890625" />
                  <Point X="3.370376464844" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.18398828125" />
                  <Point X="3.380004638672" Y="-25.205486328125" />
                  <Point X="3.384067382812" Y="-25.216029296875" />
                  <Point X="3.393841064453" Y="-25.237494140625" />
                  <Point X="3.399126953125" Y="-25.247482421875" />
                  <Point X="3.410850341797" Y="-25.266763671875" />
                  <Point X="3.427149169922" Y="-25.288623046875" />
                  <Point X="3.472793701172" Y="-25.34678515625" />
                  <Point X="3.481919189453" Y="-25.356841796875" />
                  <Point X="3.501508544922" Y="-25.375548828125" />
                  <Point X="3.511972412109" Y="-25.38419921875" />
                  <Point X="3.542720214844" Y="-25.406119140625" />
                  <Point X="3.5579296875" Y="-25.415904296875" />
                  <Point X="3.63400390625" Y="-25.459876953125" />
                  <Point X="3.639489013672" Y="-25.4628125" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027099609" Y="-25.481029296875" />
                  <Point X="3.6919921875" Y="-25.483916015625" />
                  <Point X="4.761078125" Y="-25.770376953125" />
                  <Point X="4.784876464844" Y="-25.77675390625" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.748608398438" Y="-25.98804296875" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.704649169922" Y="-25.94451953125" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097412109" Y="-25.90804296875" />
                  <Point X="3.366719482422" Y="-25.910841796875" />
                  <Point X="3.354479003906" Y="-25.912677734375" />
                  <Point X="3.322221435547" Y="-25.919689453125" />
                  <Point X="3.172914794922" Y="-25.952140625" />
                  <Point X="3.15787109375" Y="-25.956744140625" />
                  <Point X="3.128751464844" Y="-25.968369140625" />
                  <Point X="3.114675537109" Y="-25.975390625" />
                  <Point X="3.086848876953" Y="-25.992283203125" />
                  <Point X="3.074122314453" Y="-26.001533203125" />
                  <Point X="3.05037109375" Y="-26.022005859375" />
                  <Point X="3.039346435547" Y="-26.033228515625" />
                  <Point X="3.019848632813" Y="-26.0566796875" />
                  <Point X="2.929602294922" Y="-26.165216796875" />
                  <Point X="2.921324462891" Y="-26.1768515625" />
                  <Point X="2.906605957031" Y="-26.20123046875" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.872362792969" Y="-26.327029296875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607177734" Y="-26.51458984375" />
                  <Point X="2.864065429688" Y="-26.530126953125" />
                  <Point X="2.871796875" Y="-26.561748046875" />
                  <Point X="2.876786132812" Y="-26.576669921875" />
                  <Point X="2.889159179688" Y="-26.605484375" />
                  <Point X="2.89654296875" Y="-26.619376953125" />
                  <Point X="2.914394775391" Y="-26.647142578125" />
                  <Point X="2.997023193359" Y="-26.775666015625" />
                  <Point X="3.001745117188" Y="-26.782357421875" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486083984" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="4.044914794922" Y="-27.59755859375" />
                  <Point X="4.087170898438" Y="-27.629982421875" />
                  <Point X="4.045494873047" Y="-27.697421875" />
                  <Point X="4.018592773438" Y="-27.73564453125" />
                  <Point X="4.001274169922" Y="-27.760251953125" />
                  <Point X="3.087026611328" Y="-27.232412109375" />
                  <Point X="2.848454589844" Y="-27.094671875" />
                  <Point X="2.841199951172" Y="-27.090890625" />
                  <Point X="2.815021240234" Y="-27.07951171875" />
                  <Point X="2.783116943359" Y="-27.069326171875" />
                  <Point X="2.771108642578" Y="-27.066337890625" />
                  <Point X="2.732716796875" Y="-27.059404296875" />
                  <Point X="2.555018310547" Y="-27.0273125" />
                  <Point X="2.539359375" Y="-27.02580859375" />
                  <Point X="2.508008789062" Y="-27.025404296875" />
                  <Point X="2.492317138672" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444845947266" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400591308594" Y="-27.051109375" />
                  <Point X="2.368697021484" Y="-27.06789453125" />
                  <Point X="2.221073242188" Y="-27.145587890625" />
                  <Point X="2.208970947266" Y="-27.153169921875" />
                  <Point X="2.186040039062" Y="-27.1700625" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144941162109" Y="-27.21116015625" />
                  <Point X="2.128046875" Y="-27.234091796875" />
                  <Point X="2.120463134766" Y="-27.2461953125" />
                  <Point X="2.103677490234" Y="-27.27808984375" />
                  <Point X="2.02598425293" Y="-27.425712890625" />
                  <Point X="2.019835571289" Y="-27.440193359375" />
                  <Point X="2.010012084961" Y="-27.46996875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.0021875" Y="-27.580140625" />
                  <Point X="2.009120849609" Y="-27.618533203125" />
                  <Point X="2.041213134766" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.707082763672" Y="-28.97782421875" />
                  <Point X="2.735893066406" Y="-29.027724609375" />
                  <Point X="2.723753662109" Y="-29.036083984375" />
                  <Point X="2.015967163086" Y="-28.1136796875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828657714844" Y="-27.87015234375" />
                  <Point X="1.808835571289" Y="-27.849630859375" />
                  <Point X="1.783253417969" Y="-27.828005859375" />
                  <Point X="1.773299682617" Y="-27.820646484375" />
                  <Point X="1.735435180664" Y="-27.796302734375" />
                  <Point X="1.560176147461" Y="-27.68362890625" />
                  <Point X="1.546279663086" Y="-27.676244140625" />
                  <Point X="1.517465332031" Y="-27.663873046875" />
                  <Point X="1.502547363281" Y="-27.65888671875" />
                  <Point X="1.470927001953" Y="-27.65115625" />
                  <Point X="1.455391113281" Y="-27.648697265625" />
                  <Point X="1.424125244141" Y="-27.64637890625" />
                  <Point X="1.408395507812" Y="-27.64651953125" />
                  <Point X="1.366984008789" Y="-27.650330078125" />
                  <Point X="1.17530871582" Y="-27.667966796875" />
                  <Point X="1.161225708008" Y="-27.67033984375" />
                  <Point X="1.133575317383" Y="-27.677171875" />
                  <Point X="1.1200078125" Y="-27.681630859375" />
                  <Point X="1.092621826172" Y="-27.692974609375" />
                  <Point X="1.079875244141" Y="-27.699416015625" />
                  <Point X="1.055493530273" Y="-27.71413671875" />
                  <Point X="1.043858032227" Y="-27.722416015625" />
                  <Point X="1.011881225586" Y="-27.74900390625" />
                  <Point X="0.863874328613" Y="-27.872068359375" />
                  <Point X="0.852654663086" Y="-27.88308984375" />
                  <Point X="0.832184204102" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.773231140137" Y="-28.049619140625" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091369629" Y="-29.15233984375" />
                  <Point X="0.726234375" Y="-28.753544921875" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146057129" Y="-28.45358203125" />
                  <Point X="0.62678692627" Y="-28.423814453125" />
                  <Point X="0.62040625" Y="-28.41320703125" />
                  <Point X="0.591317443848" Y="-28.371296875" />
                  <Point X="0.456678649902" Y="-28.177306640625" />
                  <Point X="0.446669189453" Y="-28.165169921875" />
                  <Point X="0.424782989502" Y="-28.142712890625" />
                  <Point X="0.41290637207" Y="-28.132392578125" />
                  <Point X="0.386651397705" Y="-28.113150390625" />
                  <Point X="0.373237579346" Y="-28.104935546875" />
                  <Point X="0.345236450195" Y="-28.090828125" />
                  <Point X="0.330648651123" Y="-28.084935546875" />
                  <Point X="0.285635375977" Y="-28.070966796875" />
                  <Point X="0.07728956604" Y="-28.006302734375" />
                  <Point X="0.063372665405" Y="-28.003107421875" />
                  <Point X="0.035222595215" Y="-27.99883984375" />
                  <Point X="0.020989425659" Y="-27.997767578125" />
                  <Point X="-0.008652074814" Y="-27.997765625" />
                  <Point X="-0.022896093369" Y="-27.998837890625" />
                  <Point X="-0.051062362671" Y="-28.003107421875" />
                  <Point X="-0.064984466553" Y="-28.0063046875" />
                  <Point X="-0.109997581482" Y="-28.020275390625" />
                  <Point X="-0.318343719482" Y="-28.0849375" />
                  <Point X="-0.332932373047" Y="-28.090830078125" />
                  <Point X="-0.360933685303" Y="-28.104939453125" />
                  <Point X="-0.374346160889" Y="-28.11315625" />
                  <Point X="-0.400601287842" Y="-28.132400390625" />
                  <Point X="-0.412475067139" Y="-28.142716796875" />
                  <Point X="-0.434358886719" Y="-28.165171875" />
                  <Point X="-0.444368804932" Y="-28.177310546875" />
                  <Point X="-0.473457427979" Y="-28.21922265625" />
                  <Point X="-0.608096679688" Y="-28.4132109375" />
                  <Point X="-0.612470092773" Y="-28.420130859375" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.972164855957" Y="-29.717318359375" />
                  <Point X="-0.985424865723" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.799510161991" Y="-21.12972708244" />
                  <Point X="1.338733641283" Y="-20.385424269115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.751945008351" Y="-21.212112418726" />
                  <Point X="0.94103309322" Y="-20.289406711174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.704379854711" Y="-21.294497755013" />
                  <Point X="0.639426075778" Y="-20.242351252617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.656814701071" Y="-21.3768830913" />
                  <Point X="0.377984786308" Y="-20.215761254658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.609249547432" Y="-21.459268427586" />
                  <Point X="0.352847702744" Y="-20.309574263431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.968916775921" Y="-22.25867447592" />
                  <Point X="3.917310031979" Y="-22.232379526516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.561684393792" Y="-21.541653763873" />
                  <Point X="0.327710619179" Y="-20.403387272205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079447636012" Y="-22.421613754667" />
                  <Point X="3.819211177087" Y="-22.289016655958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.514119240152" Y="-21.624039100159" />
                  <Point X="0.302573535615" Y="-20.497200280978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.104796805269" Y="-22.541150794102" />
                  <Point X="3.721112609845" Y="-22.345653931965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.466554086512" Y="-21.706424436446" />
                  <Point X="0.277436452051" Y="-20.591013289751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.350910509252" Y="-20.270854521855" />
                  <Point X="-0.43883170982" Y="-20.226056432615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021293642777" Y="-22.605224800274" />
                  <Point X="3.623014042604" Y="-22.402291207971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.418988932872" Y="-21.788809772733" />
                  <Point X="0.252299368487" Y="-20.684826298524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.317824366486" Y="-20.394333746195" />
                  <Point X="-0.608999796399" Y="-20.245972454387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.937790480285" Y="-22.669298806446" />
                  <Point X="3.524915475362" Y="-22.458928483978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371423779232" Y="-21.871195109019" />
                  <Point X="0.222221992958" Y="-20.776122102814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.284738223721" Y="-20.517812970535" />
                  <Point X="-0.779167882978" Y="-20.265888476158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854287317793" Y="-22.733372812619" />
                  <Point X="3.42681690812" Y="-22.515565759985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.323858625592" Y="-21.953580445306" />
                  <Point X="0.165778034189" Y="-20.853983461926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.251652162166" Y="-20.641292153496" />
                  <Point X="-0.945578081509" Y="-20.287719237526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.7707841553" Y="-22.797446818791" />
                  <Point X="3.328718340879" Y="-22.572203035991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.276293471953" Y="-22.035965781593" />
                  <Point X="0.061547696439" Y="-20.907496444808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.213998562437" Y="-20.767098613398" />
                  <Point X="-1.080560384066" Y="-20.325563311718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.687280992808" Y="-22.861520824963" />
                  <Point X="3.230619773637" Y="-22.628840311998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.228728318313" Y="-22.118351117879" />
                  <Point X="-1.215542686622" Y="-20.363407385909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603777830316" Y="-22.925594831135" />
                  <Point X="3.132521206395" Y="-22.685477588005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.18116313293" Y="-22.200736437992" />
                  <Point X="-1.350524989178" Y="-20.401251460101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.520274667824" Y="-22.989668837307" />
                  <Point X="3.034422639153" Y="-22.742114864011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.133597938731" Y="-22.283121753613" />
                  <Point X="-1.478570287899" Y="-20.44263011429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.436771505331" Y="-23.053742843479" />
                  <Point X="2.936324071912" Y="-22.798752140018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086032744532" Y="-22.365507069234" />
                  <Point X="-1.463532390669" Y="-20.55691329821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353268448197" Y="-23.117816903334" />
                  <Point X="2.83822550467" Y="-22.855389416025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.044673110904" Y="-22.451054275894" />
                  <Point X="-1.472291085881" Y="-20.659071512671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.711471148494" Y="-23.916476737283" />
                  <Point X="4.666731422823" Y="-23.89368070845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.269765700127" Y="-23.181891120665" />
                  <Point X="2.740126937428" Y="-22.912026692031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.018876817144" Y="-22.544531400295" />
                  <Point X="-1.501312585145" Y="-20.750905312789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.741102990142" Y="-24.038195907293" />
                  <Point X="4.500441907838" Y="-23.915572961157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.186262952057" Y="-23.245965337996" />
                  <Point X="2.642028370187" Y="-22.968663968038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015716613107" Y="-22.649542188488" />
                  <Point X="-1.543835021296" Y="-20.83586004197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.763667307575" Y="-24.156313993851" />
                  <Point X="4.334152394967" Y="-23.93746521494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.116110267489" Y="-23.316841752434" />
                  <Point X="2.518130343755" Y="-23.012155763005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.042529331263" Y="-22.769824943334" />
                  <Point X="-1.622693752778" Y="-20.902300503941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.781698028502" Y="-24.272122097611" />
                  <Point X="4.167862882096" Y="-23.959357468723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.059230049815" Y="-23.394480826531" />
                  <Point X="2.296339401037" Y="-23.005768625797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212550528016" Y="-22.963076062608" />
                  <Point X="-1.736325216531" Y="-20.951023373871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.663257416669" Y="-24.318394584204" />
                  <Point X="4.001573369225" Y="-23.981249722507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.023053389213" Y="-23.482668889852" />
                  <Point X="-2.280305242693" Y="-20.7804726991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.526120135479" Y="-24.355140641938" />
                  <Point X="3.835283856354" Y="-24.00314197629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0017523487" Y="-23.578436460185" />
                  <Point X="-2.380408761717" Y="-20.836088401148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.388982854289" Y="-24.391886699673" />
                  <Point X="3.668994343482" Y="-24.025034230073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.01372150582" Y="-23.691156042922" />
                  <Point X="-2.480512280741" Y="-20.891704103197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.251845573098" Y="-24.428632757407" />
                  <Point X="3.502704830611" Y="-24.046926483857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.061016408301" Y="-23.821874991943" />
                  <Point X="-2.580615836981" Y="-20.947319786283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.114708291908" Y="-24.465378815142" />
                  <Point X="-2.675368409739" Y="-21.005661931633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.977571010718" Y="-24.502124872876" />
                  <Point X="-2.758913299876" Y="-21.069714676508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840433903414" Y="-24.53887101921" />
                  <Point X="-2.842458190013" Y="-21.133767421384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.703296911151" Y="-24.57561722416" />
                  <Point X="-2.92600308015" Y="-21.197820166259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.593317431083" Y="-24.626200872718" />
                  <Point X="-3.009547970287" Y="-21.261872911134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.502334273302" Y="-24.686463630929" />
                  <Point X="-3.02420130648" Y="-21.361027655999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.437891683518" Y="-24.760249483978" />
                  <Point X="-2.93698738537" Y="-21.51208636093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386684954008" Y="-24.840779344682" />
                  <Point X="-2.84977346426" Y="-21.663145065861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361728459319" Y="-24.934684368083" />
                  <Point X="-2.77043258008" Y="-21.810192258111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349181877867" Y="-25.034912558105" />
                  <Point X="-2.751339829291" Y="-21.926541493114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.771393210763" Y="-25.86618641935" />
                  <Point X="4.386232736125" Y="-25.669937355383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365983363748" Y="-25.150094335326" />
                  <Point X="-2.755084471788" Y="-22.031254495038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.754096248557" Y="-25.963994169482" />
                  <Point X="3.944876924473" Y="-25.551676329639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.426256732286" Y="-25.287426143098" />
                  <Point X="-2.794888590422" Y="-22.117594276175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.73229989313" Y="-26.059509364261" />
                  <Point X="-2.861984538765" Y="-22.190028175511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.503865534568" Y="-26.04973723761" />
                  <Point X="-2.946563795625" Y="-22.253553884217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.221705630934" Y="-26.012590578457" />
                  <Point X="-3.119743826603" Y="-22.271935243665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.9395457273" Y="-25.975443919304" />
                  <Point X="-3.784554930826" Y="-22.039818059532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.657385535322" Y="-25.938297113232" />
                  <Point X="-3.843957606897" Y="-22.116171876881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.390356084692" Y="-25.908859804947" />
                  <Point X="-3.903360282967" Y="-22.19252569423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.238196917646" Y="-25.937951829538" />
                  <Point X="-3.962762618957" Y="-22.268879684859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.109082696105" Y="-25.978785840347" />
                  <Point X="-4.018339458251" Y="-22.347182863412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.029624972073" Y="-26.044921100369" />
                  <Point X="-4.066307220126" Y="-22.429363060557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.96735416198" Y="-26.119813530441" />
                  <Point X="-4.114274982" Y="-22.511543257702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.909260181015" Y="-26.196834161252" />
                  <Point X="-4.162242743875" Y="-22.593723454847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876799763247" Y="-26.286915744873" />
                  <Point X="-4.210210505749" Y="-22.675903651992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866715586409" Y="-26.388398592713" />
                  <Point X="-3.964082710055" Y="-22.907933020301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859898397754" Y="-26.491546054174" />
                  <Point X="-3.550505830575" Y="-23.225281958294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.894692664655" Y="-26.615895611232" />
                  <Point X="-3.438547875556" Y="-23.388948378224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.9960581007" Y="-26.774164873171" />
                  <Point X="-3.423009679372" Y="-23.503486477195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.337573650651" Y="-27.054796729844" />
                  <Point X="-3.450076030965" Y="-23.596316474809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751151643124" Y="-27.372146234936" />
                  <Point X="-3.494176672364" Y="-23.680467068252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.07777338731" Y="-27.645189318532" />
                  <Point X="-3.572015465947" Y="-23.747427214539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.025839327527" Y="-27.725348585952" />
                  <Point X="3.302107091689" Y="-27.356588593173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.711236598393" Y="-27.055525039484" />
                  <Point X="-3.696982206398" Y="-23.790374472514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.456480467316" Y="-27.03234129986" />
                  <Point X="-3.940252756401" Y="-23.77304292875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.343267988144" Y="-27.081277653097" />
                  <Point X="-4.22241388426" Y="-23.735895645823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.240333932246" Y="-27.135451124573" />
                  <Point X="-4.504575337186" Y="-23.698748197266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.155656817261" Y="-27.198926972074" />
                  <Point X="-4.647364605067" Y="-23.732614423941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103273551659" Y="-27.278857357697" />
                  <Point X="-4.672751728613" Y="-23.826300030981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059025156329" Y="-27.362932666752" />
                  <Point X="-4.698138852159" Y="-23.91998563802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017182829436" Y="-27.44823392891" />
                  <Point X="-4.723525832942" Y="-24.0136713178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000449266548" Y="-27.546328745332" />
                  <Point X="-4.74149208819" Y="-24.111138046094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.016845186574" Y="-27.661303876429" />
                  <Point X="-3.307888022886" Y="-24.948216794441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.904522009687" Y="-24.644216594132" />
                  <Point X="-4.756163124534" Y="-24.210283772282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038052294117" Y="-27.778730430008" />
                  <Point X="-3.295943447843" Y="-25.060923851984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.345875733387" Y="-24.525956632253" />
                  <Point X="-4.770834160877" Y="-24.30942949847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.092301611027" Y="-27.912992830166" />
                  <Point X="-3.322458000487" Y="-25.154035005205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179515431037" Y="-28.064051483584" />
                  <Point X="1.848447989984" Y="-27.895364196869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.36745618305" Y="-27.650286630237" />
                  <Point X="-3.376644604596" Y="-25.233046543965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266729251048" Y="-28.215110137002" />
                  <Point X="1.982781868457" Y="-28.070431719255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.190209161343" Y="-27.666595754406" />
                  <Point X="-3.46492696857" Y="-25.294685425354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.353943071058" Y="-28.366168790421" />
                  <Point X="2.117116124965" Y="-28.24549943426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.064081637127" Y="-27.708951563511" />
                  <Point X="-3.594514827817" Y="-25.335278105698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.441156891069" Y="-28.517227443839" />
                  <Point X="2.2514505055" Y="-28.42056721246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.982074280015" Y="-27.773787720592" />
                  <Point X="-3.731652631466" Y="-25.372023897226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528370711079" Y="-28.668286097257" />
                  <Point X="2.385784886036" Y="-28.59563499066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.902565904147" Y="-27.839897172215" />
                  <Point X="-3.86879007255" Y="-25.40876987349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.61558453109" Y="-28.819344750675" />
                  <Point X="2.520119266572" Y="-28.770702768861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.830158486397" Y="-27.909624742714" />
                  <Point X="-4.005927363017" Y="-25.445515926498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.7027983511" Y="-28.970403404094" />
                  <Point X="2.654453647108" Y="-28.945770547061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.786370934787" Y="-27.993934863373" />
                  <Point X="-4.143064653484" Y="-25.482261979506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764573764496" Y="-28.089449642958" />
                  <Point X="-4.280201943951" Y="-25.519008032513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.743709762629" Y="-28.185439895604" />
                  <Point X="-2.953647303297" Y="-26.301542374647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.327356537003" Y="-26.111128009362" />
                  <Point X="-4.417339234418" Y="-25.555754085521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725423154123" Y="-28.28274339576" />
                  <Point X="-2.953123983033" Y="-26.408430012215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.493645955595" Y="-26.133020311183" />
                  <Point X="-4.554476524886" Y="-25.592500138528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.73316298897" Y="-28.393308031165" />
                  <Point X="0.537346579249" Y="-28.293534586984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.036870797463" Y="-28.000956220007" />
                  <Point X="-2.98511011076" Y="-26.498753258682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.659935374187" Y="-26.154912613004" />
                  <Point X="-4.691613815353" Y="-25.629246191536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.748209205622" Y="-28.507595454043" />
                  <Point X="0.642126141782" Y="-28.453543433257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.171032842566" Y="-28.039218236246" />
                  <Point X="-3.047675848147" Y="-26.573495415792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.826224792779" Y="-26.176804914825" />
                  <Point X="-4.781378968863" Y="-25.690129553919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.763255422274" Y="-28.621882876921" />
                  <Point X="0.679524340617" Y="-28.579219759903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.301076760574" Y="-28.079578543044" />
                  <Point X="-3.131142427587" Y="-26.637588061961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.992514211371" Y="-26.198697216646" />
                  <Point X="-4.764931232468" Y="-25.805131086775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.778301638926" Y="-28.736170299799" />
                  <Point X="0.712610294829" Y="-28.70269888817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.402842251617" Y="-28.134347428053" />
                  <Point X="-3.214645632871" Y="-26.701662046329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.158803629963" Y="-26.220589518467" />
                  <Point X="-4.748483535709" Y="-25.920132599434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.793347855578" Y="-28.850457722677" />
                  <Point X="0.745696416481" Y="-28.826178101753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.466159077501" Y="-28.208706886459" />
                  <Point X="-3.298148790519" Y="-26.76573605497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.325093048556" Y="-26.242481820288" />
                  <Point X="-4.724150128679" Y="-26.039152082164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.80839407223" Y="-28.964745145555" />
                  <Point X="0.778782655349" Y="-28.949657375059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.520827219942" Y="-28.287473069184" />
                  <Point X="-3.3816517821" Y="-26.829810148226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.491382467148" Y="-26.264374122109" />
                  <Point X="-4.692840700838" Y="-26.161726025034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.823440288882" Y="-29.079032568433" />
                  <Point X="0.811868894217" Y="-29.073136648366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.57549551818" Y="-28.366239172526" />
                  <Point X="-2.361604331213" Y="-27.45617127672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.572395430795" Y="-27.348767846956" />
                  <Point X="-3.465154773681" Y="-26.893884241481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.657672893746" Y="-26.286265910325" />
                  <Point X="-4.661531272998" Y="-26.284299967903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.626617651075" Y="-28.44681213736" />
                  <Point X="-2.311981042839" Y="-27.588076597609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703455089764" Y="-27.388610607884" />
                  <Point X="-3.548657765263" Y="-26.957958334737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.656256502771" Y="-28.538331380702" />
                  <Point X="-2.325414894948" Y="-27.68785270065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.801553766904" Y="-27.445247827895" />
                  <Point X="-3.632160756844" Y="-27.022032427993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.681393600444" Y="-28.632144382286" />
                  <Point X="-2.369221213859" Y="-27.772153258892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.899652444044" Y="-27.501885047906" />
                  <Point X="-3.715663748425" Y="-27.086106521248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.706530698117" Y="-28.725957383871" />
                  <Point X="-2.416786311787" Y="-27.854538623565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.997751121184" Y="-27.558522267916" />
                  <Point X="-3.799166740007" Y="-27.150180614504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.73166779579" Y="-28.819770385455" />
                  <Point X="-2.464351409714" Y="-27.936923988239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.095849798324" Y="-27.615159487927" />
                  <Point X="-3.882669731588" Y="-27.21425470776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.756804893463" Y="-28.91358338704" />
                  <Point X="-2.511916499004" Y="-28.019309357313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.193948475465" Y="-27.671796707938" />
                  <Point X="-3.966172723169" Y="-27.278328801015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.781941991136" Y="-29.007396388624" />
                  <Point X="-2.559481586166" Y="-28.101694727472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.292047152605" Y="-27.728433927948" />
                  <Point X="-4.049675714751" Y="-27.342402894271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.807079088809" Y="-29.101209390209" />
                  <Point X="-2.607046673328" Y="-28.184080097631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.390145829745" Y="-27.785071147959" />
                  <Point X="-4.133178706332" Y="-27.406476987527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.832216186481" Y="-29.195022391793" />
                  <Point X="-1.487527649323" Y="-28.86112452413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.601328617395" Y="-28.80314003472" />
                  <Point X="-2.654611760491" Y="-28.266465467789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.488244506885" Y="-27.841708367969" />
                  <Point X="-4.142525368926" Y="-27.508335617642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.857353284154" Y="-29.288835393378" />
                  <Point X="-1.226501543849" Y="-29.100744960426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.851935928779" Y="-28.782070224316" />
                  <Point X="-2.702176847653" Y="-28.348850837948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.586343184025" Y="-27.89834558798" />
                  <Point X="-4.051187125045" Y="-27.661495769987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.882490381827" Y="-29.382648394962" />
                  <Point X="-1.175961242214" Y="-29.23311752291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.042610007539" Y="-28.791537921204" />
                  <Point X="-2.749741934815" Y="-28.431236208106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.684441861165" Y="-27.954982807991" />
                  <Point X="-3.939909156712" Y="-27.824815719396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.9076274795" Y="-29.476461396547" />
                  <Point X="-1.152361182809" Y="-29.351763346362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.142504125033" Y="-28.847260318661" />
                  <Point X="-2.797307021978" Y="-28.513621578265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.782540629915" Y="-28.011619981324" />
                  <Point X="-3.807339593462" Y="-27.998984278276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.932764577173" Y="-29.570274398131" />
                  <Point X="-1.128761195868" Y="-29.470409132891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.233036966878" Y="-28.907752524302" />
                  <Point X="-2.84487210914" Y="-28.596006948424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.957901674846" Y="-29.664087399716" />
                  <Point X="-1.121424269707" Y="-29.580768476066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.323569808722" Y="-28.968244729942" />
                  <Point X="-2.892437196303" Y="-28.678392318582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.983038915991" Y="-29.757900328197" />
                  <Point X="-1.134579105109" Y="-29.680686745221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.403811263681" Y="-29.033980659111" />
                  <Point X="-2.940002283465" Y="-28.760777688741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.463196551185" Y="-29.110343336378" />
                  <Point X="-2.923157629913" Y="-28.875981460989" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998373046875" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.542708435059" Y="-28.802720703125" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.435230072021" Y="-28.4796328125" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.27433605957" Y="-28.266400390625" />
                  <Point X="0.229322860718" Y="-28.252431640625" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="-0.008664604187" Y="-28.187765625" />
                  <Point X="-0.05367779541" Y="-28.201736328125" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.317367736816" Y="-28.3275546875" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.788638977051" Y="-29.766494140625" />
                  <Point X="-0.847744140625" Y="-29.987078125" />
                  <Point X="-0.855236816406" Y="-29.985623046875" />
                  <Point X="-1.10023034668" Y="-29.938068359375" />
                  <Point X="-1.151322143555" Y="-29.924923828125" />
                  <Point X="-1.351589477539" Y="-29.873396484375" />
                  <Point X="-1.317717407227" Y="-29.61611328125" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.320436889648" Y="-29.480697265625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.427725219727" Y="-29.16628515625" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.704244140625" Y="-28.982158203125" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.035710327148" Y="-29.0044140625" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.444673339844" Y="-29.3983125" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.496698974609" Y="-29.389978515625" />
                  <Point X="-2.855837890625" Y="-29.167609375" />
                  <Point X="-2.926560791016" Y="-29.113154296875" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.638293945312" Y="-27.858201171875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.500251220703" Y="-27.613669921875" />
                  <Point X="-2.504396484375" Y="-27.582189453125" />
                  <Point X="-2.517062744141" Y="-27.565681640625" />
                  <Point X="-2.531329101562" Y="-27.551416015625" />
                  <Point X="-2.560157958984" Y="-27.53719921875" />
                  <Point X="-2.591683837891" Y="-27.543470703125" />
                  <Point X="-3.652001464844" Y="-28.15564453125" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.877967529297" Y="-28.219900390625" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.212408691406" Y="-27.76210546875" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.395199462891" Y="-26.60071484375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535400391" Y="-26.41108203125" />
                  <Point X="-3.144452880859" Y="-26.390728515625" />
                  <Point X="-3.1381171875" Y="-26.366265625" />
                  <Point X="-3.136651855469" Y="-26.35005078125" />
                  <Point X="-3.148657714844" Y="-26.321068359375" />
                  <Point X="-3.165865478516" Y="-26.307869140625" />
                  <Point X="-3.187643066406" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.558080078125" Y="-26.464794921875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.816421875" Y="-26.445638671875" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.940808105469" Y="-25.917396484375" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-3.821077636719" Y="-25.19928125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.536965087891" Y="-25.11800390625" />
                  <Point X="-3.514142822266" Y="-25.1021640625" />
                  <Point X="-3.502324707031" Y="-25.090646484375" />
                  <Point X="-3.493255615234" Y="-25.07061328125" />
                  <Point X="-3.485648193359" Y="-25.046103515625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.487291503906" Y="-25.0111640625" />
                  <Point X="-3.494898925781" Y="-24.98665234375" />
                  <Point X="-3.50232421875" Y="-24.9719140625" />
                  <Point X="-3.519073486328" Y="-24.956974609375" />
                  <Point X="-3.541895751953" Y="-24.941134765625" />
                  <Point X="-3.557463623047" Y="-24.9339140625" />
                  <Point X="-4.777615722656" Y="-24.6069765625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.989164550781" Y="-24.486904296875" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.890639648438" Y="-23.903923828125" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-3.942230957031" Y="-23.581142578125" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731710449219" Y="-23.604134765625" />
                  <Point X="-3.720790771484" Y="-23.600693359375" />
                  <Point X="-3.670277832031" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.573943359375" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.634741210938" Y="-23.545642578125" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.621599853516" Y="-23.444337890625" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.359850585938" Y="-22.843740234375" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.437913574219" Y="-22.6891015625" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.088479248047" Y="-22.121046875" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.275213867188" Y="-22.00605078125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514404297" Y="-22.07956640625" />
                  <Point X="-3.123315185547" Y="-22.080896484375" />
                  <Point X="-3.052965087891" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-3.002464355469" Y="-22.061806640625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.939404052734" Y="-21.9569609375" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.262206542969" Y="-21.328791015625" />
                  <Point X="-3.307278808594" Y="-21.25072265625" />
                  <Point X="-3.236888671875" Y="-21.196755859375" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.640211425781" Y="-20.76307421875" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.003355102539" Y="-20.666125" />
                  <Point X="-1.967826538086" Y="-20.71242578125" />
                  <Point X="-1.951247192383" Y="-20.726337890625" />
                  <Point X="-1.934330810547" Y="-20.73514453125" />
                  <Point X="-1.85603125" Y="-20.77590625" />
                  <Point X="-1.835124389648" Y="-20.781509765625" />
                  <Point X="-1.81380859375" Y="-20.77775" />
                  <Point X="-1.796188964844" Y="-20.770451171875" />
                  <Point X="-1.714634765625" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.680348510742" Y="-20.687322265625" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.687177001953" Y="-20.31375390625" />
                  <Point X="-1.689137695313" Y="-20.298859375" />
                  <Point X="-1.594678222656" Y="-20.272376953125" />
                  <Point X="-0.968083007812" Y="-20.096703125" />
                  <Point X="-0.831512451172" Y="-20.08071875" />
                  <Point X="-0.224199981689" Y="-20.009640625" />
                  <Point X="-0.076473182678" Y="-20.56096484375" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282142639" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.03659406662" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.213364044189" Y="-20.09603125" />
                  <Point X="0.236648529053" Y="-20.009130859375" />
                  <Point X="0.313106536865" Y="-20.017138671875" />
                  <Point X="0.860210205078" Y="-20.074435546875" />
                  <Point X="0.973205200195" Y="-20.101716796875" />
                  <Point X="1.508455932617" Y="-20.230943359375" />
                  <Point X="1.581760253906" Y="-20.25753125" />
                  <Point X="1.931044067383" Y="-20.38421875" />
                  <Point X="2.00215625" Y="-20.417474609375" />
                  <Point X="2.338685546875" Y="-20.574859375" />
                  <Point X="2.407412597656" Y="-20.6148984375" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.79732421875" Y="-20.850392578125" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.382934814453" Y="-22.2312578125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.221037597656" Y="-22.52275" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.203531982422" Y="-22.620009765625" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682128906" Y="-22.6991875" />
                  <Point X="2.226314208984" Y="-22.710435546875" />
                  <Point X="2.261639892578" Y="-22.76249609375" />
                  <Point X="2.286187255859" Y="-22.7834296875" />
                  <Point X="2.338248291016" Y="-22.818755859375" />
                  <Point X="2.360336425781" Y="-22.827021484375" />
                  <Point X="2.372670898438" Y="-22.8285078125" />
                  <Point X="2.429761230469" Y="-22.835392578125" />
                  <Point X="2.462929199219" Y="-22.830240234375" />
                  <Point X="2.528951416016" Y="-22.812583984375" />
                  <Point X="2.541033691406" Y="-22.80758203125" />
                  <Point X="3.768271972656" Y="-22.09903515625" />
                  <Point X="3.994247558594" Y="-21.968568359375" />
                  <Point X="4.009746826172" Y="-21.990109375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.238717285156" Y="-22.317818359375" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.490161865234" Y="-23.252265625" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.26910546875" Y="-23.429560546875" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.209295654297" Y="-23.522173828125" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.193918701172" Y="-23.624248046875" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.224184570312" Y="-23.725021484375" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280947509766" Y="-23.8011796875" />
                  <Point X="3.2933203125" Y="-23.80814453125" />
                  <Point X="3.350589599609" Y="-23.8403828125" />
                  <Point X="3.385295166016" Y="-23.848591796875" />
                  <Point X="3.462726806641" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.641598144531" Y="-23.705349609375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.856362792969" Y="-23.708392578125" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.950572753906" Y="-24.121734375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="3.972143554688" Y="-24.700283203125" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.712651855469" Y="-24.776681640625" />
                  <Point X="3.636577636719" Y="-24.82065234375" />
                  <Point X="3.612403564453" Y="-24.845638671875" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.553697753906" Y="-24.9424296875" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.541770507812" Y="-25.057849609375" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.576620361328" Y="-25.171326171875" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.653012695312" Y="-25.251408203125" />
                  <Point X="3.729086914062" Y="-25.295380859375" />
                  <Point X="3.74116796875" Y="-25.300388671875" />
                  <Point X="4.81025390625" Y="-25.586849609375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.994677734375" Y="-25.65966015625" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.933846679688" Y="-26.0303125" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.679849365234" Y="-26.13289453125" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836425781" Y="-26.098341796875" />
                  <Point X="3.362578857422" Y="-26.105353515625" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.165947753906" Y="-26.1781484375" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.061563476562" Y="-26.344439453125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.074212158203" Y="-26.544388671875" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="4.160579589844" Y="-27.4468203125" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.33449609375" Y="-27.591193359375" />
                  <Point X="4.204134277344" Y="-27.802138671875" />
                  <Point X="4.173967773438" Y="-27.845" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="2.992026611328" Y="-27.39695703125" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.698948730469" Y="-27.246380859375" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.457183349609" Y="-27.23603125" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.271814208984" Y="-27.366578125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.196096435547" Y="-27.584767578125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.871627685547" Y="-28.88282421875" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.835294433594" Y="-29.19021484375" />
                  <Point X="2.801575195312" Y="-29.212041015625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="1.865229980469" Y="-28.22934375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549072266" Y="-27.980466796875" />
                  <Point X="1.632684448242" Y="-27.956123046875" />
                  <Point X="1.457425537109" Y="-27.84344921875" />
                  <Point X="1.425805175781" Y="-27.83571875" />
                  <Point X="1.384393676758" Y="-27.839529296875" />
                  <Point X="1.192718383789" Y="-27.857166015625" />
                  <Point X="1.165332641602" Y="-27.868509765625" />
                  <Point X="1.133355834961" Y="-27.89509765625" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.958895935059" Y="-28.089974609375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.094602416992" Y="-29.68306640625" />
                  <Point X="1.127642333984" Y="-29.934029296875" />
                  <Point X="0.994366821289" Y="-29.9632421875" />
                  <Point X="0.963188659668" Y="-29.96890625" />
                  <Point X="0.860200439453" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#134" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.034332864015" Y="4.483256472984" Z="0.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.45" />
                  <Point X="-0.843480619063" Y="5.000222398833" Z="0.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.45" />
                  <Point X="-1.614332013093" Y="4.807047800245" Z="0.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.45" />
                  <Point X="-1.742905199774" Y="4.711001854986" Z="0.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.45" />
                  <Point X="-1.734190241094" Y="4.358992850533" Z="0.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.45" />
                  <Point X="-1.821479282605" Y="4.307022986653" Z="0.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.45" />
                  <Point X="-1.917398592442" Y="4.340485031035" Z="0.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.45" />
                  <Point X="-1.969843752107" Y="4.395593050578" Z="0.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.45" />
                  <Point X="-2.670650300553" Y="4.311913135075" Z="0.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.45" />
                  <Point X="-3.273466801179" Y="3.874204010449" Z="0.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.45" />
                  <Point X="-3.311663736356" Y="3.677489470242" Z="0.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.45" />
                  <Point X="-2.995369779718" Y="3.069962712144" Z="0.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.45" />
                  <Point X="-3.043975085552" Y="3.004828482334" Z="0.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.45" />
                  <Point X="-3.125113753463" Y="3.000194919455" Z="0.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.45" />
                  <Point X="-3.256369878055" Y="3.068530212389" Z="0.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.45" />
                  <Point X="-4.134099077038" Y="2.940936810086" Z="0.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.45" />
                  <Point X="-4.487251715715" Y="2.367383643488" Z="0.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.45" />
                  <Point X="-4.396444612471" Y="2.147872655727" Z="0.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.45" />
                  <Point X="-3.672106175733" Y="1.563854356768" Z="0.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.45" />
                  <Point X="-3.687090905442" Y="1.504771957906" Z="0.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.45" />
                  <Point X="-3.741982769352" Y="1.478273482031" Z="0.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.45" />
                  <Point X="-3.941860913068" Y="1.49971023579" Z="0.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.45" />
                  <Point X="-4.945055480665" Y="1.140433880613" Z="0.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.45" />
                  <Point X="-5.044781995129" Y="0.55169504314" Z="0.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.45" />
                  <Point X="-4.796713180603" Y="0.376007899355" Z="0.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.45" />
                  <Point X="-3.553738584093" Y="0.03322915556" Z="0.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.45" />
                  <Point X="-3.541200344844" Y="0.005295662601" Z="0.45" />
                  <Point X="-3.539556741714" Y="0" Z="0.45" />
                  <Point X="-3.547164256291" Y="-0.02451128846" Z="0.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.45" />
                  <Point X="-3.57163001103" Y="-0.045646827574" Z="0.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.45" />
                  <Point X="-3.84017460817" Y="-0.119704157401" Z="0.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.45" />
                  <Point X="-4.99646016249" Y="-0.893193587201" Z="0.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.45" />
                  <Point X="-4.871022888721" Y="-1.426732691204" Z="0.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.45" />
                  <Point X="-4.557709527767" Y="-1.48308680904" Z="0.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.45" />
                  <Point X="-3.197381343172" Y="-1.319680623083" Z="0.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.45" />
                  <Point X="-3.199011980054" Y="-1.346912093697" Z="0.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.45" />
                  <Point X="-3.431793447104" Y="-1.529766330591" Z="0.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.45" />
                  <Point X="-4.261507783135" Y="-2.756434628784" Z="0.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.45" />
                  <Point X="-3.923909370875" Y="-3.21890371937" Z="0.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.45" />
                  <Point X="-3.633157281851" Y="-3.167665719743" Z="0.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.45" />
                  <Point X="-2.558573203016" Y="-2.569757266116" Z="0.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.45" />
                  <Point X="-2.687751293426" Y="-2.801921100641" Z="0.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.45" />
                  <Point X="-2.963220686312" Y="-4.121490793656" Z="0.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.45" />
                  <Point X="-2.529176406828" Y="-4.401209589249" Z="0.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.45" />
                  <Point X="-2.411161618723" Y="-4.397469737687" Z="0.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.45" />
                  <Point X="-2.014087667929" Y="-4.014708156463" Z="0.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.45" />
                  <Point X="-1.71367011556" Y="-4.000770826133" Z="0.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.45" />
                  <Point X="-1.466848354894" Y="-4.172596468379" Z="0.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.45" />
                  <Point X="-1.375632165196" Y="-4.45917013316" Z="0.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.45" />
                  <Point X="-1.373445650327" Y="-4.578305902483" Z="0.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.45" />
                  <Point X="-1.169936989073" Y="-4.942066734772" Z="0.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.45" />
                  <Point X="-0.870920385443" Y="-5.003426490845" Z="0.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.45" />
                  <Point X="-0.746498660615" Y="-4.748155041593" Z="0.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.45" />
                  <Point X="-0.282447819757" Y="-3.324784005997" Z="0.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.45" />
                  <Point X="-0.045013131282" Y="-3.218209783249" Z="0.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.45" />
                  <Point X="0.208345948079" Y="-3.268902262039" Z="0.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.45" />
                  <Point X="0.387998039675" Y="-3.47686174977" Z="0.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.45" />
                  <Point X="0.488256199786" Y="-3.784380988222" Z="0.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.45" />
                  <Point X="0.965969618833" Y="-4.986821973809" Z="0.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.45" />
                  <Point X="1.145243268851" Y="-4.948727964023" Z="0.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.45" />
                  <Point X="1.13801861818" Y="-4.645259956099" Z="0.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.45" />
                  <Point X="1.001599091258" Y="-3.069313767264" Z="0.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.45" />
                  <Point X="1.159166876598" Y="-2.90226316244" Z="0.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.45" />
                  <Point X="1.382819023662" Y="-2.858037306907" Z="0.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.45" />
                  <Point X="1.599489244912" Y="-2.966901796977" Z="0.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.45" />
                  <Point X="1.819406304562" Y="-3.228500537964" Z="0.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.45" />
                  <Point X="2.822588520388" Y="-4.22273497403" Z="0.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.45" />
                  <Point X="3.012891384806" Y="-4.089129828809" Z="0.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.45" />
                  <Point X="2.908772968465" Y="-3.826543028274" Z="0.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.45" />
                  <Point X="2.23914464274" Y="-2.54459996668" Z="0.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.45" />
                  <Point X="2.309904536801" Y="-2.358584323774" Z="0.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.45" />
                  <Point X="2.474314054598" Y="-2.248996773166" Z="0.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.45" />
                  <Point X="2.683906836833" Y="-2.264303366751" Z="0.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.45" />
                  <Point X="2.960870568833" Y="-2.408976531809" Z="0.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.45" />
                  <Point X="4.208700587705" Y="-2.842497225805" Z="0.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.45" />
                  <Point X="4.370873356363" Y="-2.586197466459" Z="0.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.45" />
                  <Point X="4.184861382642" Y="-2.375872475861" Z="0.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.45" />
                  <Point X="3.110114378065" Y="-1.486069910876" Z="0.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.45" />
                  <Point X="3.10519696738" Y="-1.317740618281" Z="0.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.45" />
                  <Point X="3.198237809223" Y="-1.178833886695" Z="0.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.45" />
                  <Point X="3.367042168307" Y="-1.122931767198" Z="0.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.45" />
                  <Point X="3.667167141175" Y="-1.151185818388" Z="0.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.45" />
                  <Point X="4.976437809896" Y="-1.01015736536" Z="0.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.45" />
                  <Point X="5.037963392922" Y="-0.635832261691" Z="0.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.45" />
                  <Point X="4.817039064043" Y="-0.507271451699" Z="0.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.45" />
                  <Point X="3.671878534196" Y="-0.176838405643" Z="0.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.45" />
                  <Point X="3.609798215412" Y="-0.109176389044" Z="0.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.45" />
                  <Point X="3.584721890616" Y="-0.017164344549" Z="0.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.45" />
                  <Point X="3.596649559808" Y="0.079446186687" Z="0.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.45" />
                  <Point X="3.645581222988" Y="0.154772349531" Z="0.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.45" />
                  <Point X="3.731516880156" Y="0.211310438599" Z="0.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.45" />
                  <Point X="3.978928753264" Y="0.28270048107" Z="0.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.45" />
                  <Point X="4.993821428296" Y="0.917238515969" Z="0.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.45" />
                  <Point X="4.89878783736" Y="1.334714821748" Z="0.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.45" />
                  <Point X="4.628915675516" Y="1.375503856558" Z="0.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.45" />
                  <Point X="3.385689730269" Y="1.23225762344" Z="0.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.45" />
                  <Point X="3.311691534704" Y="1.266706113421" Z="0.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.45" />
                  <Point X="3.259799158735" Y="1.333738771697" Z="0.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.45" />
                  <Point X="3.236731195913" Y="1.417135222847" Z="0.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.45" />
                  <Point X="3.251291925168" Y="1.495639777648" Z="0.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.45" />
                  <Point X="3.302632268605" Y="1.571302437762" Z="0.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.45" />
                  <Point X="3.514444177421" Y="1.739346794695" Z="0.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.45" />
                  <Point X="4.27533875876" Y="2.739348723578" Z="0.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.45" />
                  <Point X="4.044176472672" Y="3.070373811586" Z="0.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.45" />
                  <Point X="3.737116360408" Y="2.975545171278" Z="0.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.45" />
                  <Point X="2.443855743561" Y="2.249343577718" Z="0.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.45" />
                  <Point X="2.372501181993" Y="2.252413228508" Z="0.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.45" />
                  <Point X="2.308105837878" Y="2.289226004833" Z="0.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.45" />
                  <Point X="2.261532595402" Y="2.348919022504" Z="0.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.45" />
                  <Point X="2.24701647425" Y="2.41725726184" Z="0.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.45" />
                  <Point X="2.263184364939" Y="2.495613832321" Z="0.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.45" />
                  <Point X="2.420080181581" Y="2.775022721547" Z="0.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.45" />
                  <Point X="2.820145221556" Y="4.221636065027" Z="0.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.45" />
                  <Point X="2.426426471989" Y="4.459584537241" Z="0.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.45" />
                  <Point X="2.01718165921" Y="4.659096440886" Z="0.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.45" />
                  <Point X="1.592652860634" Y="4.820753979845" Z="0.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.45" />
                  <Point X="0.978783593789" Y="4.978167688907" Z="0.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.45" />
                  <Point X="0.312158490307" Y="5.06386956812" Z="0.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.45" />
                  <Point X="0.158911697538" Y="4.948191032967" Z="0.45" />
                  <Point X="0" Y="4.355124473572" Z="0.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>