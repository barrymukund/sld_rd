<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#205" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3218" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.995282226562" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.910462402344" Y="-29.80814453125" />
                  <Point X="0.563302001953" Y="-28.5125234375" />
                  <Point X="0.557721130371" Y="-28.497140625" />
                  <Point X="0.542363708496" Y="-28.467376953125" />
                  <Point X="0.400470489502" Y="-28.2629375" />
                  <Point X="0.378636047363" Y="-28.2314765625" />
                  <Point X="0.356755462646" Y="-28.209025390625" />
                  <Point X="0.330499511719" Y="-28.189779296875" />
                  <Point X="0.3024949646" Y="-28.17566796875" />
                  <Point X="0.082923301697" Y="-28.107521484375" />
                  <Point X="0.049135822296" Y="-28.09703515625" />
                  <Point X="0.020976577759" Y="-28.092765625" />
                  <Point X="-0.008664747238" Y="-28.092765625" />
                  <Point X="-0.036823993683" Y="-28.09703515625" />
                  <Point X="-0.256395477295" Y="-28.165181640625" />
                  <Point X="-0.290182983398" Y="-28.17566796875" />
                  <Point X="-0.318184814453" Y="-28.18977734375" />
                  <Point X="-0.344440155029" Y="-28.209021484375" />
                  <Point X="-0.366323455811" Y="-28.231474609375" />
                  <Point X="-0.508216827393" Y="-28.435916015625" />
                  <Point X="-0.530051147461" Y="-28.467375" />
                  <Point X="-0.538188781738" Y="-28.481572265625" />
                  <Point X="-0.55099017334" Y="-28.5125234375" />
                  <Point X="-0.604410949707" Y="-28.711892578125" />
                  <Point X="-0.916584655762" Y="-29.87694140625" />
                  <Point X="-1.043643798828" Y="-29.852279296875" />
                  <Point X="-1.079353881836" Y="-29.84534765625" />
                  <Point X="-1.24641796875" Y="-29.80236328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.268964233398" Y="-29.252513671875" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937255859" Y="-29.18296875" />
                  <Point X="-1.304008666992" Y="-29.155130859375" />
                  <Point X="-1.32364440918" Y="-29.131203125" />
                  <Point X="-1.525797973633" Y="-28.953919921875" />
                  <Point X="-1.556905151367" Y="-28.926638671875" />
                  <Point X="-1.583190429688" Y="-28.910294921875" />
                  <Point X="-1.612889282227" Y="-28.897994140625" />
                  <Point X="-1.643028320312" Y="-28.890966796875" />
                  <Point X="-1.911331176758" Y="-28.873380859375" />
                  <Point X="-1.952617553711" Y="-28.87067578125" />
                  <Point X="-1.983416870117" Y="-28.873708984375" />
                  <Point X="-2.014464111328" Y="-28.88202734375" />
                  <Point X="-2.042657470703" Y="-28.89480078125" />
                  <Point X="-2.266221679688" Y="-29.044181640625" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312788330078" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.365094482422" Y="-29.138548828125" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.749395507812" Y="-29.12177734375" />
                  <Point X="-2.801723876953" Y="-29.08937890625" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-3.068919433594" Y="-28.79406640625" />
                  <Point X="-2.423760986328" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405575927734" Y="-27.585189453125" />
                  <Point X="-2.414561523438" Y="-27.5555703125" />
                  <Point X="-2.428780273438" Y="-27.526740234375" />
                  <Point X="-2.446810302734" Y="-27.50158203125" />
                  <Point X="-2.461845214844" Y="-27.486548828125" />
                  <Point X="-2.464140380859" Y="-27.48425390625" />
                  <Point X="-2.489298095703" Y="-27.46622265625" />
                  <Point X="-2.518128662109" Y="-27.452001953125" />
                  <Point X="-2.54774609375" Y="-27.443015625" />
                  <Point X="-2.578680664062" Y="-27.444025390625" />
                  <Point X="-2.6102109375" Y="-27.450294921875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-2.811135742188" Y="-27.5604765625" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.041528808594" Y="-27.848162109375" />
                  <Point X="-4.082859619141" Y="-27.793861328125" />
                  <Point X="-4.306142089844" Y="-27.419451171875" />
                  <Point X="-4.234772949219" Y="-27.3646875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084578613281" Y="-26.47559765625" />
                  <Point X="-3.066614257812" Y="-26.44846875" />
                  <Point X="-3.053856689453" Y="-26.4198359375" />
                  <Point X="-3.0471796875" Y="-26.3940546875" />
                  <Point X="-3.046152099609" Y="-26.390087890625" />
                  <Point X="-3.043347900391" Y="-26.359662109375" />
                  <Point X="-3.045555908203" Y="-26.327990234375" />
                  <Point X="-3.052556884766" Y="-26.298244140625" />
                  <Point X="-3.068639892578" Y="-26.272259765625" />
                  <Point X="-3.089473144531" Y="-26.248302734375" />
                  <Point X="-3.112972900391" Y="-26.228767578125" />
                  <Point X="-3.135923828125" Y="-26.215259765625" />
                  <Point X="-3.139445800781" Y="-26.2131875" />
                  <Point X="-3.168729248047" Y="-26.201953125" />
                  <Point X="-3.200612304688" Y="-26.195474609375" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-3.449002441406" Y="-26.222962890625" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.817911621094" Y="-26.055943359375" />
                  <Point X="-4.834076660156" Y="-25.99265625" />
                  <Point X="-4.892423828125" Y="-25.584701171875" />
                  <Point X="-4.81876953125" Y="-25.56496484375" />
                  <Point X="-3.532875732422" Y="-25.22041015625" />
                  <Point X="-3.517493408203" Y="-25.214828125" />
                  <Point X="-3.487731933594" Y="-25.19947265625" />
                  <Point X="-3.463680175781" Y="-25.182779296875" />
                  <Point X="-3.459979003906" Y="-25.1802109375" />
                  <Point X="-3.437527832031" Y="-25.15833203125" />
                  <Point X="-3.418280761719" Y="-25.132076171875" />
                  <Point X="-3.404168457031" Y="-25.1040703125" />
                  <Point X="-3.396151123047" Y="-25.07823828125" />
                  <Point X="-3.394917480469" Y="-25.074263671875" />
                  <Point X="-3.390648193359" Y="-25.046103515625" />
                  <Point X="-3.390647949219" Y="-25.0164609375" />
                  <Point X="-3.394917236328" Y="-24.988302734375" />
                  <Point X="-3.4029296875" Y="-24.962486328125" />
                  <Point X="-3.404162841797" Y="-24.958509765625" />
                  <Point X="-3.4182734375" Y="-24.9305" />
                  <Point X="-3.437522705078" Y="-24.904236328125" />
                  <Point X="-3.459979003906" Y="-24.882349609375" />
                  <Point X="-3.484030761719" Y="-24.86565625" />
                  <Point X="-3.495171386719" Y="-24.85900390625" />
                  <Point X="-3.514437255859" Y="-24.849216796875" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-3.730748779297" Y="-24.789130859375" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.834905273438" Y="-24.093423828125" />
                  <Point X="-4.824487792969" Y="-24.0230234375" />
                  <Point X="-4.703551757812" Y="-23.576732421875" />
                  <Point X="-4.687426757812" Y="-23.57885546875" />
                  <Point X="-3.765666748047" Y="-23.70020703125" />
                  <Point X="-3.744984619141" Y="-23.700658203125" />
                  <Point X="-3.723424316406" Y="-23.698771484375" />
                  <Point X="-3.703141357422" Y="-23.694736328125" />
                  <Point X="-3.649906982422" Y="-23.677953125" />
                  <Point X="-3.641697998047" Y="-23.675365234375" />
                  <Point X="-3.622764892578" Y="-23.66703125" />
                  <Point X="-3.604024169922" Y="-23.656208984375" />
                  <Point X="-3.587345458984" Y="-23.643978515625" />
                  <Point X="-3.573709228516" Y="-23.62842578125" />
                  <Point X="-3.561297119141" Y="-23.610697265625" />
                  <Point X="-3.551351318359" Y="-23.592568359375" />
                  <Point X="-3.529990722656" Y="-23.541" />
                  <Point X="-3.526703857422" Y="-23.533064453125" />
                  <Point X="-3.520915039062" Y="-23.513203125" />
                  <Point X="-3.517156982422" Y="-23.49188671875" />
                  <Point X="-3.5158046875" Y="-23.471244140625" />
                  <Point X="-3.518952636719" Y="-23.450798828125" />
                  <Point X="-3.524556396484" Y="-23.429890625" />
                  <Point X="-3.532051513672" Y="-23.410619140625" />
                  <Point X="-3.557825195312" Y="-23.361107421875" />
                  <Point X="-3.561791259766" Y="-23.353490234375" />
                  <Point X="-3.573286865234" Y="-23.336287109375" />
                  <Point X="-3.587197509766" Y="-23.3197109375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-3.715636230469" Y="-23.218318359375" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.121629882812" Y="-22.335685546875" />
                  <Point X="-4.08115625" Y="-22.266345703125" />
                  <Point X="-3.750505126953" Y="-21.841337890625" />
                  <Point X="-3.206656494141" Y="-22.155330078125" />
                  <Point X="-3.187728759766" Y="-22.163658203125" />
                  <Point X="-3.167086181641" Y="-22.17016796875" />
                  <Point X="-3.146793945312" Y="-22.174205078125" />
                  <Point X="-3.072653320312" Y="-22.18069140625" />
                  <Point X="-3.061244628906" Y="-22.181689453125" />
                  <Point X="-3.040560302734" Y="-22.18123828125" />
                  <Point X="-3.019102050781" Y="-22.178412109375" />
                  <Point X="-2.999012695312" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946078369141" Y="-22.139771484375" />
                  <Point X="-2.893452636719" Y="-22.087146484375" />
                  <Point X="-2.885354736328" Y="-22.079048828125" />
                  <Point X="-2.872408447266" Y="-22.06291796875" />
                  <Point X="-2.860778808594" Y="-22.0446640625" />
                  <Point X="-2.851629394531" Y="-22.02611328125" />
                  <Point X="-2.846712646484" Y="-22.00601953125" />
                  <Point X="-2.843887207031" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.849922363281" Y="-21.889740234375" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-2.920090576172" Y="-21.731353515625" />
                  <Point X="-3.183332763672" Y="-21.27540625" />
                  <Point X="-2.7711171875" Y="-20.95936328125" />
                  <Point X="-2.700621826172" Y="-20.90531640625" />
                  <Point X="-2.167035644531" Y="-20.6088671875" />
                  <Point X="-2.043194946289" Y="-20.7702578125" />
                  <Point X="-2.02889831543" Y="-20.785193359375" />
                  <Point X="-2.012321533203" Y="-20.79910546875" />
                  <Point X="-1.995116699219" Y="-20.810603515625" />
                  <Point X="-1.912598510742" Y="-20.853560546875" />
                  <Point X="-1.899900756836" Y="-20.860171875" />
                  <Point X="-1.880626464844" Y="-20.86766796875" />
                  <Point X="-1.859718017578" Y="-20.873271484375" />
                  <Point X="-1.839269775391" Y="-20.876419921875" />
                  <Point X="-1.818625" Y="-20.87506640625" />
                  <Point X="-1.797307617188" Y="-20.871306640625" />
                  <Point X="-1.777452758789" Y="-20.865517578125" />
                  <Point X="-1.691504638672" Y="-20.829916015625" />
                  <Point X="-1.678278930664" Y="-20.8244375" />
                  <Point X="-1.660144287109" Y="-20.81448828125" />
                  <Point X="-1.642415405273" Y="-20.80207421875" />
                  <Point X="-1.626863525391" Y="-20.788435546875" />
                  <Point X="-1.614632446289" Y="-20.771755859375" />
                  <Point X="-1.603810668945" Y="-20.75301171875" />
                  <Point X="-1.59548034668" Y="-20.734078125" />
                  <Point X="-1.567505981445" Y="-20.645353515625" />
                  <Point X="-1.563201049805" Y="-20.631701171875" />
                  <Point X="-1.559166015625" Y="-20.611416015625" />
                  <Point X="-1.557279296875" Y="-20.58985546875" />
                  <Point X="-1.557730224609" Y="-20.569173828125" />
                  <Point X="-1.563448364258" Y="-20.5257421875" />
                  <Point X="-1.584201904297" Y="-20.3681015625" />
                  <Point X="-1.040891845703" Y="-20.21577734375" />
                  <Point X="-0.949625366211" Y="-20.19019140625" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.133903366089" Y="-20.713685546875" />
                  <Point X="-0.121129745483" Y="-20.741876953125" />
                  <Point X="-0.103271606445" Y="-20.768603515625" />
                  <Point X="-0.082114006042" Y="-20.791193359375" />
                  <Point X="-0.054818138123" Y="-20.805783203125" />
                  <Point X="-0.024380064011" Y="-20.816115234375" />
                  <Point X="0.006155910492" Y="-20.82115625" />
                  <Point X="0.036691894531" Y="-20.816115234375" />
                  <Point X="0.067129966736" Y="-20.805783203125" />
                  <Point X="0.094425949097" Y="-20.791193359375" />
                  <Point X="0.115583587646" Y="-20.768603515625" />
                  <Point X="0.133441726685" Y="-20.741876953125" />
                  <Point X="0.146215194702" Y="-20.713685546875" />
                  <Point X="0.171986022949" Y="-20.6175078125" />
                  <Point X="0.307419525146" Y="-20.112060546875" />
                  <Point X="0.764352905273" Y="-20.159916015625" />
                  <Point X="0.844031494141" Y="-20.168259765625" />
                  <Point X="1.401623535156" Y="-20.302880859375" />
                  <Point X="1.481039794922" Y="-20.3220546875" />
                  <Point X="1.843763305664" Y="-20.4536171875" />
                  <Point X="1.89464453125" Y="-20.472072265625" />
                  <Point X="2.245583984375" Y="-20.636193359375" />
                  <Point X="2.294558105469" Y="-20.65909765625" />
                  <Point X="2.633619628906" Y="-20.856634765625" />
                  <Point X="2.680991455078" Y="-20.884234375" />
                  <Point X="2.943260742188" Y="-21.07074609375" />
                  <Point X="2.894303955078" Y="-21.155541015625" />
                  <Point X="2.147581298828" Y="-22.44890234375" />
                  <Point X="2.142077148438" Y="-22.46006640625" />
                  <Point X="2.133076660156" Y="-22.483943359375" />
                  <Point X="2.114470214844" Y="-22.5535234375" />
                  <Point X="2.1121875" Y="-22.56471875" />
                  <Point X="2.107986572266" Y="-22.594328125" />
                  <Point X="2.107728027344" Y="-22.619046875" />
                  <Point X="2.114983154297" Y="-22.679212890625" />
                  <Point X="2.116099365234" Y="-22.688470703125" />
                  <Point X="2.121442138672" Y="-22.71039453125" />
                  <Point X="2.129710205078" Y="-22.73248828125" />
                  <Point X="2.140072265625" Y="-22.75253125" />
                  <Point X="2.177301269531" Y="-22.8073984375" />
                  <Point X="2.184428222656" Y="-22.816626953125" />
                  <Point X="2.203455322266" Y="-22.838365234375" />
                  <Point X="2.221599365234" Y="-22.854408203125" />
                  <Point X="2.276447021484" Y="-22.891625" />
                  <Point X="2.284888916016" Y="-22.89735546875" />
                  <Point X="2.304943115234" Y="-22.907724609375" />
                  <Point X="2.327038574219" Y="-22.915994140625" />
                  <Point X="2.348965820312" Y="-22.921337890625" />
                  <Point X="2.409127197266" Y="-22.928591796875" />
                  <Point X="2.421243652344" Y="-22.9292734375" />
                  <Point X="2.449409423828" Y="-22.929052734375" />
                  <Point X="2.473208007812" Y="-22.925830078125" />
                  <Point X="2.542787597656" Y="-22.90722265625" />
                  <Point X="2.549237792969" Y="-22.90525" />
                  <Point X="2.572027099609" Y="-22.897384765625" />
                  <Point X="2.588533691406" Y="-22.88985546875" />
                  <Point X="2.787555175781" Y="-22.774951171875" />
                  <Point X="3.967326171875" Y="-22.093810546875" />
                  <Point X="4.095179931641" Y="-22.271498046875" />
                  <Point X="4.123274414062" Y="-22.310541015625" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="4.213914550781" Y="-22.577166015625" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221422363281" Y="-23.33976171875" />
                  <Point X="3.203974121094" Y="-23.358373046875" />
                  <Point X="3.153897705078" Y="-23.423701171875" />
                  <Point X="3.147797119141" Y="-23.4326796875" />
                  <Point X="3.131621826172" Y="-23.45968359375" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.102976318359" Y="-23.549615234375" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836669922" Y="-23.605748046875" />
                  <Point X="3.097739746094" Y="-23.628232421875" />
                  <Point X="3.113052246094" Y="-23.7024453125" />
                  <Point X="3.115925537109" Y="-23.7131640625" />
                  <Point X="3.125481201172" Y="-23.74196484375" />
                  <Point X="3.136283935547" Y="-23.764263671875" />
                  <Point X="3.177932617188" Y="-23.827568359375" />
                  <Point X="3.184341552734" Y="-23.83730859375" />
                  <Point X="3.198896484375" Y="-23.8545546875" />
                  <Point X="3.216136962891" Y="-23.870640625" />
                  <Point X="3.234345214844" Y="-23.88396484375" />
                  <Point X="3.294699951172" Y="-23.917939453125" />
                  <Point X="3.305129638672" Y="-23.922998046875" />
                  <Point X="3.332394287109" Y="-23.934224609375" />
                  <Point X="3.356117919922" Y="-23.9405625" />
                  <Point X="3.437721435547" Y="-23.95134765625" />
                  <Point X="3.444022216797" Y="-23.951966796875" />
                  <Point X="3.469656982422" Y="-23.95362890625" />
                  <Point X="3.488203125" Y="-23.953015625" />
                  <Point X="3.677260253906" Y="-23.928126953125" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.833870605469" Y="-24.0176328125" />
                  <Point X="4.845937011719" Y="-24.0671953125" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.843268066406" Y="-24.368513671875" />
                  <Point X="3.716580078125" Y="-24.67041015625" />
                  <Point X="3.704788818359" Y="-24.674416015625" />
                  <Point X="3.681547363281" Y="-24.684931640625" />
                  <Point X="3.601374267578" Y="-24.731271484375" />
                  <Point X="3.592718505859" Y="-24.736923828125" />
                  <Point X="3.566070068359" Y="-24.756474609375" />
                  <Point X="3.547531005859" Y="-24.774423828125" />
                  <Point X="3.499427246094" Y="-24.835720703125" />
                  <Point X="3.492025146484" Y="-24.84515234375" />
                  <Point X="3.480302246094" Y="-24.8644296875" />
                  <Point X="3.470527587891" Y="-24.88589453125" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.447645996094" Y="-24.991123046875" />
                  <Point X="3.446237060547" Y="-25.001619140625" />
                  <Point X="3.443769775391" Y="-25.033314453125" />
                  <Point X="3.445178710938" Y="-25.058556640625" />
                  <Point X="3.461213378906" Y="-25.142283203125" />
                  <Point X="3.463680908203" Y="-25.155166015625" />
                  <Point X="3.470528808594" Y="-25.176669921875" />
                  <Point X="3.480303222656" Y="-25.1981328125" />
                  <Point X="3.492025146484" Y="-25.217408203125" />
                  <Point X="3.54012890625" Y="-25.278705078125" />
                  <Point X="3.547476074219" Y="-25.28701953125" />
                  <Point X="3.569189697266" Y="-25.30887109375" />
                  <Point X="3.589035644531" Y="-25.32415625" />
                  <Point X="3.669208740234" Y="-25.370498046875" />
                  <Point X="3.674460449219" Y="-25.373318359375" />
                  <Point X="3.698877929688" Y="-25.38545703125" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="3.889954101562" Y="-25.438607421875" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.861759277344" Y="-25.90404296875" />
                  <Point X="4.855021972656" Y="-25.94873046875" />
                  <Point X="4.801174316406" Y="-26.18469921875" />
                  <Point X="4.731023925781" Y="-26.175462890625" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.40803515625" Y="-26.0027109375" />
                  <Point X="3.374660400391" Y="-26.005509765625" />
                  <Point X="3.217309082031" Y="-26.0397109375" />
                  <Point X="3.193096191406" Y="-26.04497265625" />
                  <Point X="3.163978271484" Y="-26.056595703125" />
                  <Point X="3.136149658203" Y="-26.07348828125" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.017288330078" Y="-26.20834765625" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.956126220703" Y="-26.4535" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.95634765625" Y="-26.50756640625" />
                  <Point X="2.964079589844" Y="-26.5391875" />
                  <Point X="2.976450927734" Y="-26.567998046875" />
                  <Point X="3.063531494141" Y="-26.7034453125" />
                  <Point X="3.076931152344" Y="-26.724287109375" />
                  <Point X="3.086930419922" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.271520751953" Y="-26.8843671875" />
                  <Point X="4.213123046875" Y="-27.606884765625" />
                  <Point X="4.143799804688" Y="-27.71905859375" />
                  <Point X="4.124823730469" Y="-27.749767578125" />
                  <Point X="4.028980712891" Y="-27.8859453125" />
                  <Point X="3.964695068359" Y="-27.848830078125" />
                  <Point X="2.800954345703" Y="-27.176943359375" />
                  <Point X="2.786135009766" Y="-27.170015625" />
                  <Point X="2.754222900391" Y="-27.15982421875" />
                  <Point X="2.566949951172" Y="-27.126001953125" />
                  <Point X="2.538132568359" Y="-27.120798828125" />
                  <Point X="2.506775390625" Y="-27.120396484375" />
                  <Point X="2.474604736328" Y="-27.12535546875" />
                  <Point X="2.444833496094" Y="-27.135177734375" />
                  <Point X="2.289255615234" Y="-27.217056640625" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242385253906" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.12265234375" Y="-27.446017578125" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229492188" Y="-27.499732421875" />
                  <Point X="2.095271484375" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.129496826172" Y="-27.75053125" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.255208740234" Y="-28.005154296875" />
                  <Point X="2.861283691406" Y="-29.05490625" />
                  <Point X="2.804387207031" Y="-29.095546875" />
                  <Point X="2.781836914062" Y="-29.11165234375" />
                  <Point X="2.701764892578" Y="-29.163482421875" />
                  <Point X="2.646587890625" Y="-29.09157421875" />
                  <Point X="1.758546386719" Y="-27.934255859375" />
                  <Point X="1.74750378418" Y="-27.9221796875" />
                  <Point X="1.721922973633" Y="-27.900556640625" />
                  <Point X="1.537221435547" Y="-27.781810546875" />
                  <Point X="1.508799560547" Y="-27.7635390625" />
                  <Point X="1.479986938477" Y="-27.75116796875" />
                  <Point X="1.448368774414" Y="-27.7434375" />
                  <Point X="1.41710144043" Y="-27.741119140625" />
                  <Point X="1.215098754883" Y="-27.75970703125" />
                  <Point X="1.184014648438" Y="-27.76256640625" />
                  <Point X="1.15636706543" Y="-27.769396484375" />
                  <Point X="1.128978759766" Y="-27.780740234375" />
                  <Point X="1.104594116211" Y="-27.7954609375" />
                  <Point X="0.948612792969" Y="-27.925154296875" />
                  <Point X="0.924610351562" Y="-27.94511328125" />
                  <Point X="0.904137756348" Y="-27.9688671875" />
                  <Point X="0.887247009277" Y="-27.996693359375" />
                  <Point X="0.875624267578" Y="-28.025810546875" />
                  <Point X="0.828986633301" Y="-28.24037890625" />
                  <Point X="0.821809997559" Y="-28.273396484375" />
                  <Point X="0.81972454834" Y="-28.289626953125" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="0.849042114258" Y="-28.545673828125" />
                  <Point X="1.022065490723" Y="-29.8599140625" />
                  <Point X="0.996997680664" Y="-29.86541015625" />
                  <Point X="0.975678833008" Y="-29.87008203125" />
                  <Point X="0.929315307617" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.025542358398" Y="-29.75901953125" />
                  <Point X="-1.05844543457" Y="-29.7526328125" />
                  <Point X="-1.14124621582" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.175789672852" Y="-29.23398046875" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124633789" Y="-29.178470703125" />
                  <Point X="-1.199025756836" Y="-29.149505859375" />
                  <Point X="-1.20566394043" Y="-29.135470703125" />
                  <Point X="-1.221735229492" Y="-29.1076328125" />
                  <Point X="-1.230570800781" Y="-29.094865234375" />
                  <Point X="-1.250206665039" Y="-29.0709375" />
                  <Point X="-1.261006713867" Y="-29.05977734375" />
                  <Point X="-1.46316027832" Y="-28.882494140625" />
                  <Point X="-1.494267456055" Y="-28.855212890625" />
                  <Point X="-1.506741943359" Y="-28.845962890625" />
                  <Point X="-1.53302722168" Y="-28.829619140625" />
                  <Point X="-1.546837646484" Y="-28.822525390625" />
                  <Point X="-1.576536621094" Y="-28.810224609375" />
                  <Point X="-1.591317260742" Y="-28.8054765625" />
                  <Point X="-1.621456298828" Y="-28.79844921875" />
                  <Point X="-1.636814941406" Y="-28.796169921875" />
                  <Point X="-1.905117675781" Y="-28.778583984375" />
                  <Point X="-1.946404052734" Y="-28.77587890625" />
                  <Point X="-1.961928344727" Y="-28.7761328125" />
                  <Point X="-1.992727661133" Y="-28.779166015625" />
                  <Point X="-2.008002685547" Y="-28.7819453125" />
                  <Point X="-2.039049926758" Y="-28.790263671875" />
                  <Point X="-2.053669189453" Y="-28.795494140625" />
                  <Point X="-2.081862548828" Y="-28.808267578125" />
                  <Point X="-2.095436767578" Y="-28.815810546875" />
                  <Point X="-2.319000976562" Y="-28.96519140625" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359688232422" Y="-28.99276171875" />
                  <Point X="-2.380445800781" Y="-29.010134765625" />
                  <Point X="-2.402760498047" Y="-29.0327734375" />
                  <Point X="-2.410471923828" Y="-29.041630859375" />
                  <Point X="-2.440463378906" Y="-29.080716796875" />
                  <Point X="-2.503200927734" Y="-29.1624765625" />
                  <Point X="-2.699384521484" Y="-29.041005859375" />
                  <Point X="-2.747601074219" Y="-29.01115234375" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.341488037109" Y="-27.724119140625" />
                  <Point X="-2.334849609375" Y="-27.71008203125" />
                  <Point X="-2.323947509766" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.619232421875" />
                  <Point X="-2.310626708984" Y="-27.588296875" />
                  <Point X="-2.314667236328" Y="-27.557611328125" />
                  <Point X="-2.323652832031" Y="-27.5279921875" />
                  <Point X="-2.329360107422" Y="-27.513548828125" />
                  <Point X="-2.343578857422" Y="-27.48471875" />
                  <Point X="-2.351562744141" Y="-27.471400390625" />
                  <Point X="-2.369592773438" Y="-27.4462421875" />
                  <Point X="-2.379638916016" Y="-27.43440234375" />
                  <Point X="-2.394673828125" Y="-27.419369140625" />
                  <Point X="-2.408797851562" Y="-27.4070390625" />
                  <Point X="-2.433955566406" Y="-27.3890078125" />
                  <Point X="-2.4472734375" Y="-27.3810234375" />
                  <Point X="-2.476104003906" Y="-27.366802734375" />
                  <Point X="-2.490546142578" Y="-27.36109375" />
                  <Point X="-2.520163574219" Y="-27.352107421875" />
                  <Point X="-2.550845458984" Y="-27.34806640625" />
                  <Point X="-2.581780029297" Y="-27.349076171875" />
                  <Point X="-2.597208007812" Y="-27.350849609375" />
                  <Point X="-2.62873828125" Y="-27.357119140625" />
                  <Point X="-2.643673828125" Y="-27.3613828125" />
                  <Point X="-2.672646728516" Y="-27.372287109375" />
                  <Point X="-2.686684082031" Y="-27.378927734375" />
                  <Point X="-2.858635986328" Y="-27.478205078125" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-3.965935546875" Y="-27.790623046875" />
                  <Point X="-4.004018554688" Y="-27.74058984375" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-4.176940429688" Y="-27.440056640625" />
                  <Point X="-3.048122070312" Y="-26.573884765625" />
                  <Point X="-3.036482910156" Y="-26.5633125" />
                  <Point X="-3.015106933594" Y="-26.54039453125" />
                  <Point X="-3.005370361328" Y="-26.528048828125" />
                  <Point X="-2.987406005859" Y="-26.500919921875" />
                  <Point X="-2.979838134766" Y="-26.4871328125" />
                  <Point X="-2.967080566406" Y="-26.4585" />
                  <Point X="-2.961890869141" Y="-26.443654296875" />
                  <Point X="-2.955213867188" Y="-26.417873046875" />
                  <Point X="-2.951552978516" Y="-26.398806640625" />
                  <Point X="-2.948748779297" Y="-26.368380859375" />
                  <Point X="-2.948577880859" Y="-26.3530546875" />
                  <Point X="-2.950785888672" Y="-26.3213828125" />
                  <Point X="-2.953082519531" Y="-26.3062265625" />
                  <Point X="-2.960083496094" Y="-26.27648046875" />
                  <Point X="-2.971778076172" Y="-26.24824609375" />
                  <Point X="-2.987861083984" Y="-26.22226171875" />
                  <Point X="-2.996953857422" Y="-26.209921875" />
                  <Point X="-3.017787109375" Y="-26.18596484375" />
                  <Point X="-3.028743652344" Y="-26.175248046875" />
                  <Point X="-3.052243408203" Y="-26.155712890625" />
                  <Point X="-3.064786621094" Y="-26.14689453125" />
                  <Point X="-3.087737548828" Y="-26.13338671875" />
                  <Point X="-3.10541796875" Y="-26.124490234375" />
                  <Point X="-3.134701416016" Y="-26.113255859375" />
                  <Point X="-3.149812255859" Y="-26.10885546875" />
                  <Point X="-3.1816953125" Y="-26.102376953125" />
                  <Point X="-3.197308349609" Y="-26.10053125" />
                  <Point X="-3.228625244141" Y="-26.09944140625" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-3.46140234375" Y="-26.128775390625" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.725866699219" Y="-26.032431640625" />
                  <Point X="-4.740761230469" Y="-25.974119140625" />
                  <Point X="-4.786451660156" Y="-25.654658203125" />
                  <Point X="-3.508287109375" Y="-25.312173828125" />
                  <Point X="-3.500469238281" Y="-25.309712890625" />
                  <Point X="-3.473934082031" Y="-25.29925390625" />
                  <Point X="-3.444172607422" Y="-25.2838984375" />
                  <Point X="-3.433564697266" Y="-25.277517578125" />
                  <Point X="-3.409512939453" Y="-25.26082421875" />
                  <Point X="-3.393676513672" Y="-25.248248046875" />
                  <Point X="-3.371225341797" Y="-25.226369140625" />
                  <Point X="-3.360909423828" Y="-25.214498046875" />
                  <Point X="-3.341662353516" Y="-25.1882421875" />
                  <Point X="-3.333443115234" Y="-25.174826171875" />
                  <Point X="-3.319330810547" Y="-25.1468203125" />
                  <Point X="-3.313437744141" Y="-25.13223046875" />
                  <Point X="-3.305420410156" Y="-25.1063984375" />
                  <Point X="-3.300990722656" Y="-25.08850390625" />
                  <Point X="-3.296721435547" Y="-25.06034375" />
                  <Point X="-3.295648193359" Y="-25.046103515625" />
                  <Point X="-3.295647949219" Y="-25.0164609375" />
                  <Point X="-3.296721435547" Y="-25.002220703125" />
                  <Point X="-3.300990722656" Y="-24.9740625" />
                  <Point X="-3.304186523438" Y="-24.960142578125" />
                  <Point X="-3.312192382812" Y="-24.93434765625" />
                  <Point X="-3.319320800781" Y="-24.915767578125" />
                  <Point X="-3.333431396484" Y="-24.8877578125" />
                  <Point X="-3.341649902344" Y="-24.874341796875" />
                  <Point X="-3.360899169922" Y="-24.848078125" />
                  <Point X="-3.371215820312" Y="-24.836203125" />
                  <Point X="-3.393672119141" Y="-24.81431640625" />
                  <Point X="-3.405811767578" Y="-24.8043046875" />
                  <Point X="-3.429863525391" Y="-24.787611328125" />
                  <Point X="-3.452144775391" Y="-24.774306640625" />
                  <Point X="-3.471410644531" Y="-24.76451953125" />
                  <Point X="-3.480441162109" Y="-24.7605078125" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.706161132812" Y="-24.6973671875" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.740928710938" Y="-24.107330078125" />
                  <Point X="-4.731331054688" Y="-24.04246875" />
                  <Point X="-4.633586914062" Y="-23.681763671875" />
                  <Point X="-3.778067626953" Y="-23.79439453125" />
                  <Point X="-3.767738525391" Y="-23.79518359375" />
                  <Point X="-3.747056396484" Y="-23.795634765625" />
                  <Point X="-3.736702880859" Y="-23.795296875" />
                  <Point X="-3.715142578125" Y="-23.79341015625" />
                  <Point X="-3.704887939453" Y="-23.7919453125" />
                  <Point X="-3.684604980469" Y="-23.78791015625" />
                  <Point X="-3.674576660156" Y="-23.78533984375" />
                  <Point X="-3.621342285156" Y="-23.768556640625" />
                  <Point X="-3.603424560547" Y="-23.762314453125" />
                  <Point X="-3.584491455078" Y="-23.75398046875" />
                  <Point X="-3.575257324219" Y="-23.749298828125" />
                  <Point X="-3.556516601562" Y="-23.7384765625" />
                  <Point X="-3.547846435547" Y="-23.732818359375" />
                  <Point X="-3.531167724609" Y="-23.720587890625" />
                  <Point X="-3.515913574219" Y="-23.706607421875" />
                  <Point X="-3.50227734375" Y="-23.6910546875" />
                  <Point X="-3.49588671875" Y="-23.68291015625" />
                  <Point X="-3.483474609375" Y="-23.665181640625" />
                  <Point X="-3.478008056641" Y="-23.656390625" />
                  <Point X="-3.468062255859" Y="-23.63826171875" />
                  <Point X="-3.463583007813" Y="-23.628923828125" />
                  <Point X="-3.442222412109" Y="-23.57735546875" />
                  <Point X="-3.435498779297" Y="-23.559646484375" />
                  <Point X="-3.429709960938" Y="-23.53978515625" />
                  <Point X="-3.427357910156" Y="-23.529697265625" />
                  <Point X="-3.423599853516" Y="-23.508380859375" />
                  <Point X="-3.422360107422" Y="-23.49809765625" />
                  <Point X="-3.4210078125" Y="-23.477455078125" />
                  <Point X="-3.421911132813" Y="-23.456787109375" />
                  <Point X="-3.425059082031" Y="-23.436341796875" />
                  <Point X="-3.427191162109" Y="-23.426205078125" />
                  <Point X="-3.432794921875" Y="-23.405296875" />
                  <Point X="-3.436016845703" Y="-23.395455078125" />
                  <Point X="-3.443511962891" Y="-23.37618359375" />
                  <Point X="-3.44778515625" Y="-23.36675390625" />
                  <Point X="-3.473558837891" Y="-23.3172421875" />
                  <Point X="-3.482803466797" Y="-23.300708984375" />
                  <Point X="-3.494299072266" Y="-23.283505859375" />
                  <Point X="-3.500516113281" Y="-23.27521875" />
                  <Point X="-3.514426757812" Y="-23.258642578125" />
                  <Point X="-3.521502929688" Y="-23.251087890625" />
                  <Point X="-3.53644140625" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-3.657803955078" Y="-23.14294921875" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.039583496094" Y="-22.38357421875" />
                  <Point X="-4.002296386719" Y="-22.319693359375" />
                  <Point X="-3.726338378906" Y="-21.964986328125" />
                  <Point X="-3.254156494141" Y="-22.2376015625" />
                  <Point X="-3.244916259766" Y="-22.24228515625" />
                  <Point X="-3.225988525391" Y="-22.25061328125" />
                  <Point X="-3.216300537109" Y="-22.254259765625" />
                  <Point X="-3.195657958984" Y="-22.26076953125" />
                  <Point X="-3.185623046875" Y="-22.263341796875" />
                  <Point X="-3.165330810547" Y="-22.26737890625" />
                  <Point X="-3.155073486328" Y="-22.26884375" />
                  <Point X="-3.080932861328" Y="-22.275330078125" />
                  <Point X="-3.059172851562" Y="-22.276666015625" />
                  <Point X="-3.038488525391" Y="-22.27621484375" />
                  <Point X="-3.028155517578" Y="-22.27542578125" />
                  <Point X="-3.006697265625" Y="-22.272599609375" />
                  <Point X="-2.996512695312" Y="-22.2706875" />
                  <Point X="-2.976423339844" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.938449462891" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.902745605469" Y="-22.226806640625" />
                  <Point X="-2.886614257812" Y="-22.213859375" />
                  <Point X="-2.878903808594" Y="-22.206947265625" />
                  <Point X="-2.826278076172" Y="-22.154322265625" />
                  <Point X="-2.811265380859" Y="-22.13851171875" />
                  <Point X="-2.798319091797" Y="-22.122380859375" />
                  <Point X="-2.792287597656" Y="-22.113962890625" />
                  <Point X="-2.780657958984" Y="-22.095708984375" />
                  <Point X="-2.775578125" Y="-22.086685546875" />
                  <Point X="-2.766428710938" Y="-22.068134765625" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754435058594" Y="-22.028599609375" />
                  <Point X="-2.752525634766" Y="-22.018419921875" />
                  <Point X="-2.749700195312" Y="-21.9969609375" />
                  <Point X="-2.748909912109" Y="-21.986634765625" />
                  <Point X="-2.748458496094" Y="-21.965955078125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.755283935547" Y="-21.8814609375" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.837818359375" Y="-21.683853515625" />
                  <Point X="-3.059386962891" Y="-21.3000859375" />
                  <Point X="-2.713314941406" Y="-21.034755859375" />
                  <Point X="-2.648369628906" Y="-20.98496484375" />
                  <Point X="-2.192522460938" Y="-20.731705078125" />
                  <Point X="-2.118563232422" Y="-20.82808984375" />
                  <Point X="-2.111822021484" Y="-20.83594921875" />
                  <Point X="-2.097525390625" Y="-20.850884765625" />
                  <Point X="-2.089969726562" Y="-20.857962890625" />
                  <Point X="-2.073392822266" Y="-20.871875" />
                  <Point X="-2.065107421875" Y="-20.87808984375" />
                  <Point X="-2.047902709961" Y="-20.889587890625" />
                  <Point X="-2.038983398437" Y="-20.894869140625" />
                  <Point X="-1.956465332031" Y="-20.937826171875" />
                  <Point X="-1.934335327148" Y="-20.9487109375" />
                  <Point X="-1.915061035156" Y="-20.95620703125" />
                  <Point X="-1.90521887207" Y="-20.9594296875" />
                  <Point X="-1.884310302734" Y="-20.965033203125" />
                  <Point X="-1.874174926758" Y="-20.9671640625" />
                  <Point X="-1.85372668457" Y="-20.9703125" />
                  <Point X="-1.8330546875" Y="-20.971216796875" />
                  <Point X="-1.812409912109" Y="-20.96986328125" />
                  <Point X="-1.802124389648" Y="-20.968623046875" />
                  <Point X="-1.780807006836" Y="-20.96486328125" />
                  <Point X="-1.770715820312" Y="-20.962509765625" />
                  <Point X="-1.750860961914" Y="-20.956720703125" />
                  <Point X="-1.741097290039" Y="-20.95328515625" />
                  <Point X="-1.655149169922" Y="-20.91768359375" />
                  <Point X="-1.632584228516" Y="-20.9077265625" />
                  <Point X="-1.614449462891" Y="-20.89777734375" />
                  <Point X="-1.605654052734" Y="-20.892306640625" />
                  <Point X="-1.587925292969" Y="-20.879892578125" />
                  <Point X="-1.57977746582" Y="-20.873498046875" />
                  <Point X="-1.564225585938" Y="-20.859859375" />
                  <Point X="-1.550253540039" Y="-20.84461328125" />
                  <Point X="-1.538022460938" Y="-20.82793359375" />
                  <Point X="-1.532359741211" Y="-20.819255859375" />
                  <Point X="-1.521537963867" Y="-20.80051171875" />
                  <Point X="-1.516854980469" Y="-20.79126953125" />
                  <Point X="-1.508524658203" Y="-20.7723359375" />
                  <Point X="-1.504877197266" Y="-20.76264453125" />
                  <Point X="-1.476902709961" Y="-20.673919921875" />
                  <Point X="-1.470026611328" Y="-20.650234375" />
                  <Point X="-1.465991455078" Y="-20.62994921875" />
                  <Point X="-1.464527709961" Y="-20.619697265625" />
                  <Point X="-1.462640991211" Y="-20.59813671875" />
                  <Point X="-1.462301879883" Y="-20.58778515625" />
                  <Point X="-1.462752807617" Y="-20.567103515625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.469261352539" Y="-20.513341796875" />
                  <Point X="-1.479266357422" Y="-20.43734375" />
                  <Point X="-1.015246154785" Y="-20.30725" />
                  <Point X="-0.931167724609" Y="-20.2836796875" />
                  <Point X="-0.365222564697" Y="-20.21744140625" />
                  <Point X="-0.225666366577" Y="-20.7382734375" />
                  <Point X="-0.220435150146" Y="-20.752892578125" />
                  <Point X="-0.207661453247" Y="-20.781083984375" />
                  <Point X="-0.200119293213" Y="-20.79465625" />
                  <Point X="-0.182261199951" Y="-20.8213828125" />
                  <Point X="-0.172608886719" Y="-20.833544921875" />
                  <Point X="-0.15145123291" Y="-20.856134765625" />
                  <Point X="-0.126896499634" Y="-20.8749765625" />
                  <Point X="-0.099600570679" Y="-20.88956640625" />
                  <Point X="-0.085354026794" Y="-20.8957421875" />
                  <Point X="-0.054916057587" Y="-20.90607421875" />
                  <Point X="-0.039853721619" Y="-20.909845703125" />
                  <Point X="-0.009317661285" Y="-20.91488671875" />
                  <Point X="0.021629489899" Y="-20.91488671875" />
                  <Point X="0.052165550232" Y="-20.909845703125" />
                  <Point X="0.067227882385" Y="-20.90607421875" />
                  <Point X="0.097665855408" Y="-20.8957421875" />
                  <Point X="0.111912246704" Y="-20.88956640625" />
                  <Point X="0.139208175659" Y="-20.8749765625" />
                  <Point X="0.163763061523" Y="-20.856134765625" />
                  <Point X="0.184920715332" Y="-20.833544921875" />
                  <Point X="0.194573181152" Y="-20.8213828125" />
                  <Point X="0.212431259155" Y="-20.79465625" />
                  <Point X="0.219973724365" Y="-20.781083984375" />
                  <Point X="0.232747116089" Y="-20.752892578125" />
                  <Point X="0.23797819519" Y="-20.7382734375" />
                  <Point X="0.263748962402" Y="-20.642095703125" />
                  <Point X="0.378190673828" Y="-20.2149921875" />
                  <Point X="0.754457458496" Y="-20.2543984375" />
                  <Point X="0.827851989746" Y="-20.262083984375" />
                  <Point X="1.37932800293" Y="-20.395228515625" />
                  <Point X="1.453622070312" Y="-20.413166015625" />
                  <Point X="1.811370849609" Y="-20.542923828125" />
                  <Point X="1.858249023438" Y="-20.559927734375" />
                  <Point X="2.205339599609" Y="-20.722248046875" />
                  <Point X="2.250428466797" Y="-20.7433359375" />
                  <Point X="2.585796875" Y="-20.938720703125" />
                  <Point X="2.629447021484" Y="-20.96415234375" />
                  <Point X="2.817780273438" Y="-21.098083984375" />
                  <Point X="2.812031738281" Y="-21.108041015625" />
                  <Point X="2.065309082031" Y="-22.40140234375" />
                  <Point X="2.062374267578" Y="-22.406892578125" />
                  <Point X="2.053183105469" Y="-22.426556640625" />
                  <Point X="2.044182617188" Y="-22.45043359375" />
                  <Point X="2.041301391602" Y="-22.45940234375" />
                  <Point X="2.022694946289" Y="-22.528982421875" />
                  <Point X="2.018129394531" Y="-22.551373046875" />
                  <Point X="2.013928466797" Y="-22.580982421875" />
                  <Point X="2.012991821289" Y="-22.593333984375" />
                  <Point X="2.012733276367" Y="-22.618052734375" />
                  <Point X="2.013411254883" Y="-22.630419921875" />
                  <Point X="2.020666381836" Y="-22.6905859375" />
                  <Point X="2.023800537109" Y="-22.710962890625" />
                  <Point X="2.029143310547" Y="-22.73288671875" />
                  <Point X="2.032468261719" Y="-22.74369140625" />
                  <Point X="2.040736328125" Y="-22.76578515625" />
                  <Point X="2.045320922852" Y="-22.7761171875" />
                  <Point X="2.055683105469" Y="-22.79616015625" />
                  <Point X="2.061460449219" Y="-22.80587109375" />
                  <Point X="2.098689453125" Y="-22.86073828125" />
                  <Point X="2.112943359375" Y="-22.8791953125" />
                  <Point X="2.131970458984" Y="-22.90093359375" />
                  <Point X="2.14052734375" Y="-22.90953515625" />
                  <Point X="2.158671386719" Y="-22.925578125" />
                  <Point X="2.168258056641" Y="-22.93301953125" />
                  <Point X="2.223105712891" Y="-22.970236328125" />
                  <Point X="2.241256103516" Y="-22.9817421875" />
                  <Point X="2.261310302734" Y="-22.992111328125" />
                  <Point X="2.271643798828" Y="-22.996697265625" />
                  <Point X="2.293739257812" Y="-23.004966796875" />
                  <Point X="2.304545166016" Y="-23.00829296875" />
                  <Point X="2.326472412109" Y="-23.01363671875" />
                  <Point X="2.33759375" Y="-23.015654296875" />
                  <Point X="2.397755126953" Y="-23.022908203125" />
                  <Point X="2.421988037109" Y="-23.024271484375" />
                  <Point X="2.450153808594" Y="-23.02405078125" />
                  <Point X="2.462157470703" Y="-23.023193359375" />
                  <Point X="2.485956054688" Y="-23.019970703125" />
                  <Point X="2.497750976562" Y="-23.01760546875" />
                  <Point X="2.567330566406" Y="-22.998998046875" />
                  <Point X="2.580230957031" Y="-22.995052734375" />
                  <Point X="2.603020263672" Y="-22.9871875" />
                  <Point X="2.611452392578" Y="-22.983818359375" />
                  <Point X="2.636033447266" Y="-22.97212890625" />
                  <Point X="2.835054931641" Y="-22.857224609375" />
                  <Point X="3.940404785156" Y="-22.21905078125" />
                  <Point X="4.018067382812" Y="-22.326984375" />
                  <Point X="4.043958007812" Y="-22.36296484375" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.172952392578" Y="-23.2561796875" />
                  <Point X="3.168133056641" Y="-23.260134765625" />
                  <Point X="3.152116455078" Y="-23.274787109375" />
                  <Point X="3.134668212891" Y="-23.2933984375" />
                  <Point X="3.128576904297" Y="-23.300578125" />
                  <Point X="3.078500488281" Y="-23.36590625" />
                  <Point X="3.066299316406" Y="-23.38386328125" />
                  <Point X="3.050124023438" Y="-23.4108671875" />
                  <Point X="3.044352050781" Y="-23.422146484375" />
                  <Point X="3.034360107422" Y="-23.445376953125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.011486572266" Y="-23.524029296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893554688" Y="-23.6024609375" />
                  <Point X="3.001175048828" Y="-23.613759765625" />
                  <Point X="3.003078125" Y="-23.636244140625" />
                  <Point X="3.004699707031" Y="-23.6474296875" />
                  <Point X="3.020012207031" Y="-23.721642578125" />
                  <Point X="3.025758789062" Y="-23.743080078125" />
                  <Point X="3.035314453125" Y="-23.771880859375" />
                  <Point X="3.039985595703" Y="-23.7833828125" />
                  <Point X="3.050788330078" Y="-23.805681640625" />
                  <Point X="3.056919921875" Y="-23.816478515625" />
                  <Point X="3.098568603516" Y="-23.879783203125" />
                  <Point X="3.111741210938" Y="-23.898580078125" />
                  <Point X="3.126296142578" Y="-23.915826171875" />
                  <Point X="3.134087402344" Y="-23.924015625" />
                  <Point X="3.151327880859" Y="-23.9401015625" />
                  <Point X="3.160035400391" Y="-23.947306640625" />
                  <Point X="3.178243652344" Y="-23.960630859375" />
                  <Point X="3.187744384766" Y="-23.96675" />
                  <Point X="3.248099121094" Y="-24.000724609375" />
                  <Point X="3.268958496094" Y="-24.010841796875" />
                  <Point X="3.296223144531" Y="-24.022068359375" />
                  <Point X="3.307874511719" Y="-24.026005859375" />
                  <Point X="3.331598144531" Y="-24.03234375" />
                  <Point X="3.343670410156" Y="-24.034744140625" />
                  <Point X="3.425273925781" Y="-24.045529296875" />
                  <Point X="3.437875488281" Y="-24.046767578125" />
                  <Point X="3.463510253906" Y="-24.0484296875" />
                  <Point X="3.472796630859" Y="-24.048576171875" />
                  <Point X="3.500602539062" Y="-24.047203125" />
                  <Point X="3.689659667969" Y="-24.022314453125" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.74156640625" Y="-24.040103515625" />
                  <Point X="4.752684570312" Y="-24.085771484375" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="3.691991455078" Y="-24.578646484375" />
                  <Point X="3.686020996094" Y="-24.580458984375" />
                  <Point X="3.665627929688" Y="-24.58786328125" />
                  <Point X="3.642386474609" Y="-24.59837890625" />
                  <Point X="3.634007568359" Y="-24.602681640625" />
                  <Point X="3.553834472656" Y="-24.649021484375" />
                  <Point X="3.536522949219" Y="-24.660326171875" />
                  <Point X="3.509874511719" Y="-24.679876953125" />
                  <Point X="3.499989501953" Y="-24.68822265625" />
                  <Point X="3.481450439453" Y="-24.706171875" />
                  <Point X="3.472796386719" Y="-24.715775390625" />
                  <Point X="3.424692626953" Y="-24.777072265625" />
                  <Point X="3.41085546875" Y="-24.795791015625" />
                  <Point X="3.399132568359" Y="-24.815068359375" />
                  <Point X="3.393844726562" Y="-24.82505859375" />
                  <Point X="3.384070068359" Y="-24.8465234375" />
                  <Point X="3.380006103516" Y="-24.8570703125" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.354341552734" Y="-24.97325390625" />
                  <Point X="3.351523681641" Y="-24.99424609375" />
                  <Point X="3.349056396484" Y="-25.02594140625" />
                  <Point X="3.348917480469" Y="-25.038609375" />
                  <Point X="3.350326416016" Y="-25.0638515625" />
                  <Point X="3.351874267578" Y="-25.07642578125" />
                  <Point X="3.367908935547" Y="-25.16015234375" />
                  <Point X="3.373159912109" Y="-25.1839921875" />
                  <Point X="3.3800078125" Y="-25.20549609375" />
                  <Point X="3.384072265625" Y="-25.21604296875" />
                  <Point X="3.393846679688" Y="-25.237505859375" />
                  <Point X="3.399134033203" Y="-25.247494140625" />
                  <Point X="3.410855957031" Y="-25.26676953125" />
                  <Point X="3.417290527344" Y="-25.276056640625" />
                  <Point X="3.465394287109" Y="-25.337353515625" />
                  <Point X="3.480088623047" Y="-25.353982421875" />
                  <Point X="3.501802246094" Y="-25.375833984375" />
                  <Point X="3.511221923828" Y="-25.384134765625" />
                  <Point X="3.531067871094" Y="-25.399419921875" />
                  <Point X="3.541494140625" Y="-25.406404296875" />
                  <Point X="3.621667236328" Y="-25.45274609375" />
                  <Point X="3.632170654297" Y="-25.45838671875" />
                  <Point X="3.656588134766" Y="-25.470525390625" />
                  <Point X="3.665270507812" Y="-25.474314453125" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="3.865366455078" Y="-25.53037109375" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.767820800781" Y="-25.889880859375" />
                  <Point X="4.761612792969" Y="-25.93105859375" />
                  <Point X="4.727802734375" Y="-26.07921875" />
                  <Point X="3.436783203125" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400096191406" Y="-25.90804296875" />
                  <Point X="3.366721435547" Y="-25.910841796875" />
                  <Point X="3.354482666016" Y="-25.912677734375" />
                  <Point X="3.197131347656" Y="-25.94687890625" />
                  <Point X="3.172918457031" Y="-25.952140625" />
                  <Point X="3.157877197266" Y="-25.9567421875" />
                  <Point X="3.128759277344" Y="-25.968365234375" />
                  <Point X="3.114682617188" Y="-25.97538671875" />
                  <Point X="3.086854003906" Y="-25.992279296875" />
                  <Point X="3.074126708984" Y="-26.001529296875" />
                  <Point X="3.050374267578" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.944240234375" Y="-26.147611328125" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606201172" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.861525878906" Y="-26.444794921875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607910156" Y="-26.514591796875" />
                  <Point X="2.86406640625" Y="-26.530130859375" />
                  <Point X="2.871798339844" Y="-26.561751953125" />
                  <Point X="2.876787109375" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896541015625" Y="-26.619373046875" />
                  <Point X="2.983621582031" Y="-26.7548203125" />
                  <Point X="2.997021240234" Y="-26.775662109375" />
                  <Point X="3.001739501953" Y="-26.782349609375" />
                  <Point X="3.019789550781" Y="-26.8044453125" />
                  <Point X="3.043487548828" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.213688476562" Y="-26.959736328125" />
                  <Point X="4.087171142578" Y="-27.629984375" />
                  <Point X="4.062986572266" Y="-27.6691171875" />
                  <Point X="4.045506591797" Y="-27.697404296875" />
                  <Point X="4.001274658203" Y="-27.760251953125" />
                  <Point X="2.848454345703" Y="-27.094669921875" />
                  <Point X="2.841185791016" Y="-27.0908828125" />
                  <Point X="2.815036132812" Y="-27.079517578125" />
                  <Point X="2.783124023438" Y="-27.069326171875" />
                  <Point X="2.771107177734" Y="-27.0663359375" />
                  <Point X="2.583834228516" Y="-27.032513671875" />
                  <Point X="2.555016845703" Y="-27.027310546875" />
                  <Point X="2.539351318359" Y="-27.025806640625" />
                  <Point X="2.507994140625" Y="-27.025404296875" />
                  <Point X="2.492302490234" Y="-27.026505859375" />
                  <Point X="2.460131835938" Y="-27.03146484375" />
                  <Point X="2.444840087891" Y="-27.035138671875" />
                  <Point X="2.415068847656" Y="-27.0449609375" />
                  <Point X="2.400589355469" Y="-27.051109375" />
                  <Point X="2.245011474609" Y="-27.13298828125" />
                  <Point X="2.221071289062" Y="-27.145587890625" />
                  <Point X="2.20896875" Y="-27.153169921875" />
                  <Point X="2.186038574219" Y="-27.1700625" />
                  <Point X="2.1752109375" Y="-27.179373046875" />
                  <Point X="2.154251464844" Y="-27.20033203125" />
                  <Point X="2.144940429687" Y="-27.21116015625" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120463867188" Y="-27.2461953125" />
                  <Point X="2.038584350586" Y="-27.4017734375" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.440193359375" />
                  <Point X="2.010012573242" Y="-27.46996875" />
                  <Point X="2.006337890625" Y="-27.485263671875" />
                  <Point X="2.001379760742" Y="-27.5174375" />
                  <Point X="2.000279418945" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.036009155273" Y="-27.7674140625" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.804220703125" />
                  <Point X="2.051236816406" Y="-27.831546875" />
                  <Point X="2.0640703125" Y="-27.862482421875" />
                  <Point X="2.069547119141" Y="-27.873580078125" />
                  <Point X="2.172936523438" Y="-28.052654296875" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.721956542969" Y="-29.0337421875" />
                  <Point X="1.833915039062" Y="-27.876423828125" />
                  <Point X="1.828654663086" Y="-27.8701484375" />
                  <Point X="1.808831542969" Y="-27.849626953125" />
                  <Point X="1.783250732422" Y="-27.82800390625" />
                  <Point X="1.773297851562" Y="-27.820646484375" />
                  <Point X="1.588596313477" Y="-27.701900390625" />
                  <Point X="1.560174438477" Y="-27.68362890625" />
                  <Point X="1.546280273438" Y="-27.67624609375" />
                  <Point X="1.517467651367" Y="-27.663875" />
                  <Point X="1.502549316406" Y="-27.65888671875" />
                  <Point X="1.470931152344" Y="-27.65115625" />
                  <Point X="1.455393310547" Y="-27.648697265625" />
                  <Point X="1.424126098633" Y="-27.64637890625" />
                  <Point X="1.408396362305" Y="-27.64651953125" />
                  <Point X="1.206393798828" Y="-27.665107421875" />
                  <Point X="1.175309692383" Y="-27.667966796875" />
                  <Point X="1.161230712891" Y="-27.67033984375" />
                  <Point X="1.133583129883" Y="-27.677169921875" />
                  <Point X="1.120014526367" Y="-27.681626953125" />
                  <Point X="1.092626220703" Y="-27.692970703125" />
                  <Point X="1.079881469727" Y="-27.69941015625" />
                  <Point X="1.055496704102" Y="-27.714130859375" />
                  <Point X="1.043857055664" Y="-27.722412109375" />
                  <Point X="0.887875793457" Y="-27.85210546875" />
                  <Point X="0.863873352051" Y="-27.872064453125" />
                  <Point X="0.852649047852" Y="-27.883091796875" />
                  <Point X="0.832176391602" Y="-27.906845703125" />
                  <Point X="0.822928161621" Y="-27.919572265625" />
                  <Point X="0.806037353516" Y="-27.9473984375" />
                  <Point X="0.799016540527" Y="-27.961474609375" />
                  <Point X="0.787393798828" Y="-27.990591796875" />
                  <Point X="0.782791809082" Y="-28.0056328125" />
                  <Point X="0.73615411377" Y="-28.220201171875" />
                  <Point X="0.728977539063" Y="-28.25321875" />
                  <Point X="0.727584655762" Y="-28.2612890625" />
                  <Point X="0.72472454834" Y="-28.289677734375" />
                  <Point X="0.724742248535" Y="-28.323169921875" />
                  <Point X="0.725555053711" Y="-28.33551953125" />
                  <Point X="0.754854919434" Y="-28.55807421875" />
                  <Point X="0.833091918945" Y="-29.152341796875" />
                  <Point X="0.655064880371" Y="-28.487935546875" />
                  <Point X="0.652606323242" Y="-28.480123046875" />
                  <Point X="0.642145202637" Y="-28.453580078125" />
                  <Point X="0.626787902832" Y="-28.42381640625" />
                  <Point X="0.620407958984" Y="-28.413208984375" />
                  <Point X="0.478514709473" Y="-28.20876953125" />
                  <Point X="0.456680206299" Y="-28.17730859375" />
                  <Point X="0.446670166016" Y="-28.165171875" />
                  <Point X="0.424789611816" Y="-28.142720703125" />
                  <Point X="0.412919250488" Y="-28.13240625" />
                  <Point X="0.386663360596" Y="-28.11316015625" />
                  <Point X="0.373248962402" Y="-28.10494140625" />
                  <Point X="0.345244384766" Y="-28.090830078125" />
                  <Point X="0.33065423584" Y="-28.0849375" />
                  <Point X="0.111082633972" Y="-28.016791015625" />
                  <Point X="0.077295150757" Y="-28.0063046875" />
                  <Point X="0.063377067566" Y="-28.003109375" />
                  <Point X="0.035217784882" Y="-27.99883984375" />
                  <Point X="0.020976589203" Y="-27.997765625" />
                  <Point X="-0.00866476059" Y="-27.997765625" />
                  <Point X="-0.022905954361" Y="-27.99883984375" />
                  <Point X="-0.051065238953" Y="-28.003109375" />
                  <Point X="-0.064983322144" Y="-28.0063046875" />
                  <Point X="-0.284554779053" Y="-28.074451171875" />
                  <Point X="-0.318342254639" Y="-28.0849375" />
                  <Point X="-0.332930938721" Y="-28.090830078125" />
                  <Point X="-0.360932830811" Y="-28.104939453125" />
                  <Point X="-0.374345733643" Y="-28.113154296875" />
                  <Point X="-0.400601165771" Y="-28.1323984375" />
                  <Point X="-0.412473022461" Y="-28.14271484375" />
                  <Point X="-0.434356262207" Y="-28.16516796875" />
                  <Point X="-0.444367919922" Y="-28.177306640625" />
                  <Point X="-0.586261169434" Y="-28.381748046875" />
                  <Point X="-0.60809552002" Y="-28.41320703125" />
                  <Point X="-0.612471862793" Y="-28.4201328125" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777832031" Y="-28.47621484375" />
                  <Point X="-0.642753051758" Y="-28.487935546875" />
                  <Point X="-0.69617388916" Y="-28.6873046875" />
                  <Point X="-0.985425231934" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.833119537692" Y="-28.945306131618" />
                  <Point X="-2.454071814995" Y="-29.098451352431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128438168735" Y="-29.63404211137" />
                  <Point X="-0.967296102938" Y="-29.699147732037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.958206665026" Y="-28.792306851107" />
                  <Point X="-2.391444838926" Y="-29.021293492653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119888308877" Y="-29.535035678425" />
                  <Point X="-0.942523669291" Y="-29.606695644354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.91023984488" Y="-28.709225903862" />
                  <Point X="-2.30306233252" Y="-28.954541542592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.137719982917" Y="-29.425370413908" />
                  <Point X="-0.917751235645" Y="-29.51424355667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.862273024734" Y="-28.626144956616" />
                  <Point X="-2.207501501858" Y="-28.890689823788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.159881665086" Y="-29.313955712549" />
                  <Point X="-0.892978801999" Y="-29.421791468987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.814306204587" Y="-28.543064009371" />
                  <Point X="-2.111940671196" Y="-28.826838104983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.182043285768" Y="-29.202541036032" />
                  <Point X="-0.868206368353" Y="-29.329339381303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.766339384441" Y="-28.459983062125" />
                  <Point X="-1.979546746482" Y="-28.777867922155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.248238769977" Y="-29.073335523825" />
                  <Point X="-0.843433934707" Y="-29.236887293619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.875549260093" Y="-27.909372381852" />
                  <Point X="-3.716641792003" Y="-27.973575166442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.718372564295" Y="-28.37690211488" />
                  <Point X="-1.689136437648" Y="-28.792740502622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.459890022337" Y="-28.885362066587" />
                  <Point X="-0.818661501061" Y="-29.144435205936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.988171891692" Y="-27.761409084509" />
                  <Point X="-3.612236626325" Y="-27.913296790934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.670405744148" Y="-28.293821167635" />
                  <Point X="-0.793889067415" Y="-29.051983118252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.073190842188" Y="-27.624598398262" />
                  <Point X="-3.507831460648" Y="-27.853018415426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.622438924002" Y="-28.210740220389" />
                  <Point X="-0.769116633768" Y="-28.959531030569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153689749108" Y="-27.489613928161" />
                  <Point X="-3.40342629497" Y="-27.792740039918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.574472103856" Y="-28.127659273144" />
                  <Point X="-0.744344200122" Y="-28.867078942885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.12375626611" Y="-27.399247039768" />
                  <Point X="-3.299021129292" Y="-27.73246166441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.526505283709" Y="-28.044578325899" />
                  <Point X="-0.719571766476" Y="-28.774626855202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036284041354" Y="-27.332127312047" />
                  <Point X="-3.194615963615" Y="-27.672183288902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.478538463563" Y="-27.961497378653" />
                  <Point X="-0.694799330536" Y="-28.682174768445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.948811816598" Y="-27.265007584326" />
                  <Point X="-3.090210797937" Y="-27.611904913394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.430571643417" Y="-27.878416431408" />
                  <Point X="-0.670026855539" Y="-28.589722697468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.861339591842" Y="-27.197887856606" />
                  <Point X="-2.985805632259" Y="-27.551626537886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.382604823271" Y="-27.795335484163" />
                  <Point X="-0.645254380543" Y="-28.497270626491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.815730128445" Y="-29.087546683661" />
                  <Point X="0.82505759916" Y="-29.091315226451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.773867367086" Y="-27.130768128885" />
                  <Point X="-2.881400466582" Y="-27.491348162378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.335678265419" Y="-27.711834243669" />
                  <Point X="-0.60626117556" Y="-28.410564103379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784942871113" Y="-28.972647023723" />
                  <Point X="0.810810527655" Y="-28.983098235367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.686395142329" Y="-27.063648401164" />
                  <Point X="-2.776995699718" Y="-27.431069625739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311634186674" Y="-27.619087881503" />
                  <Point X="-0.550721879986" Y="-28.330542634801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.75415561378" Y="-28.857747363785" />
                  <Point X="0.796563456151" Y="-28.874881244283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.598922917573" Y="-26.996528673443" />
                  <Point X="-2.670700821004" Y="-27.371554743857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.331875212818" Y="-27.508449175549" />
                  <Point X="-0.495182681757" Y="-28.250521126893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.723368356448" Y="-28.742847703846" />
                  <Point X="0.782316384646" Y="-28.7666642532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.511450692817" Y="-26.929408945723" />
                  <Point X="-0.438976069722" Y="-28.170769271666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692581099116" Y="-28.627948043908" />
                  <Point X="0.768069313141" Y="-28.658447262116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423978468061" Y="-26.862289218002" />
                  <Point X="-0.355316049689" Y="-28.102109313259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.661793841783" Y="-28.51304838397" />
                  <Point X="0.753822244814" Y="-28.550230272316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.668502519918" Y="-26.257008061815" />
                  <Point X="-4.61122261125" Y="-26.28015064713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.336506243305" Y="-26.795169490281" />
                  <Point X="-0.219865450184" Y="-28.05437410721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.601751596067" Y="-28.386328941488" />
                  <Point X="0.739575217154" Y="-28.442013298947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.697686163968" Y="-26.142756303699" />
                  <Point X="-4.419949301591" Y="-26.25496927998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.249034018549" Y="-26.72804976256" />
                  <Point X="-0.076440774043" Y="-28.009860637248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.502924792717" Y="-28.243939520565" />
                  <Point X="0.725444737946" Y="-28.333843414209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726869778531" Y="-26.028504557496" />
                  <Point X="-4.228675991932" Y="-26.229787912831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.161561793793" Y="-26.66093003484" />
                  <Point X="0.733055407557" Y="-28.234457523774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.748910786638" Y="-25.917138611623" />
                  <Point X="-4.037402682273" Y="-26.204606545681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.074089569037" Y="-26.593810307119" />
                  <Point X="0.75352801674" Y="-28.140268194241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.612773579245" Y="-28.89145216176" />
                  <Point X="2.670737582022" Y="-28.914871139037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.764463828077" Y="-25.808393974435" />
                  <Point X="-3.846129372614" Y="-26.179425178531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.000702582962" Y="-26.520999773574" />
                  <Point X="0.774000622016" Y="-28.046078863129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.498826871204" Y="-28.74295390281" />
                  <Point X="2.593584806473" Y="-28.781238593764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.780016869517" Y="-25.699649337248" />
                  <Point X="-3.654856062955" Y="-26.154243811381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959678941855" Y="-26.435113599906" />
                  <Point X="0.802233489902" Y="-27.955024881631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.384880163163" Y="-28.594455643859" />
                  <Point X="2.516432030924" Y="-28.647606048492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.697059393799" Y="-25.630705532513" />
                  <Point X="-3.463582753296" Y="-26.129062444231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949719680661" Y="-26.336676602064" />
                  <Point X="0.859975889075" Y="-27.875893524685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.270933455123" Y="-28.445957384909" />
                  <Point X="2.439279255375" Y="-28.51397350322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.544582602641" Y="-25.589849354417" />
                  <Point X="-3.272309711806" Y="-26.103880968734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.991487275628" Y="-26.217340597753" />
                  <Point X="0.942429245983" Y="-27.80674604273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.156986747082" Y="-28.297459125959" />
                  <Point X="2.362126479826" Y="-28.380340957948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.392105811483" Y="-25.548993176322" />
                  <Point X="1.025360298969" Y="-27.737791562518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043040039042" Y="-28.148960867008" />
                  <Point X="2.284973704277" Y="-28.246708412676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.239629020325" Y="-25.508136998227" />
                  <Point X="1.131008818785" Y="-27.67801553469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.929093331001" Y="-28.000462608058" />
                  <Point X="2.207820928728" Y="-28.113075867404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.087152229166" Y="-25.467280820132" />
                  <Point X="1.325526659647" Y="-27.654145043237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.80834642515" Y="-27.849216890851" />
                  <Point X="2.130667689349" Y="-27.979443134732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.934675438008" Y="-25.426424642036" />
                  <Point X="2.05788683791" Y="-27.847576961457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.78219864685" Y="-25.385568463941" />
                  <Point X="2.029943242815" Y="-27.733826215641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.629721855692" Y="-25.344712285846" />
                  <Point X="2.009982518724" Y="-27.623300759067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.481375900383" Y="-25.302187141733" />
                  <Point X="2.001390391806" Y="-27.517368513902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.384181770629" Y="-25.238995318596" />
                  <Point X="2.026361845973" Y="-27.424996835728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.32599420758" Y="-25.160043819531" />
                  <Point X="2.070830691874" Y="-27.340502615151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.298016610015" Y="-25.068886702129" />
                  <Point X="2.115299358989" Y="-27.256008322339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.779482586629" Y="-24.36787479434" />
                  <Point X="-4.471591950774" Y="-24.492270685914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.303227303342" Y="-24.964320644816" />
                  <Point X="2.176492414562" Y="-27.178271121075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76517634439" Y="-24.271194090843" />
                  <Point X="-3.718630696776" Y="-24.694025979013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.37439211438" Y="-24.833107394245" />
                  <Point X="2.276715210052" Y="-27.116302958325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.75087010215" Y="-24.174513387347" />
                  <Point X="2.386850799193" Y="-27.058339824181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.714861288347" Y="-27.594890889984" />
                  <Point X="4.028477660159" Y="-27.721600129047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.736563920781" Y="-24.077832659257" />
                  <Point X="2.570625260052" Y="-27.030128725453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.123712304388" Y="-27.253590396534" />
                  <Point X="4.080807186236" Y="-27.640281829413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.715459652969" Y="-23.983898536376" />
                  <Point X="3.840566112443" Y="-27.440757334524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.690434575177" Y="-23.891548523553" />
                  <Point X="3.55853971414" Y="-27.224350472677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665409497385" Y="-23.79919851073" />
                  <Point X="3.276513315837" Y="-27.00794361083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.640384419593" Y="-23.706848497908" />
                  <Point X="3.016514820662" Y="-26.800436599548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.359590302459" Y="-23.717835884735" />
                  <Point X="2.922709356665" Y="-26.660075931412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.983413682689" Y="-23.767360304114" />
                  <Point X="2.865094985483" Y="-26.534337413915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.681145960111" Y="-23.787023590705" />
                  <Point X="2.862799946488" Y="-26.430949357417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.553147176891" Y="-23.736277655446" />
                  <Point X="2.871890410608" Y="-26.332161342773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.481876713834" Y="-23.662611991094" />
                  <Point X="2.890530993712" Y="-26.237231826657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.44183302706" Y="-23.576329890176" />
                  <Point X="2.938726079814" Y="-26.154243104844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421315935718" Y="-23.482158532601" />
                  <Point X="3.002496340558" Y="-26.077547162059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.446537255884" Y="-23.36950765725" />
                  <Point X="3.072153784716" Y="-26.003229795769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551169567703" Y="-23.224772658651" />
                  <Point X="3.190075107673" Y="-25.948412302274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.833197363262" Y="-23.008365232276" />
                  <Point X="3.355026434382" Y="-25.912596163697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115225332162" Y="-22.791957735867" />
                  <Point X="3.679414743687" Y="-25.941196747456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.198506218389" Y="-22.655849273166" />
                  <Point X="4.05559081662" Y="-25.990720945898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150113577875" Y="-22.572940368517" />
                  <Point X="4.431766889554" Y="-26.040245144341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.101720937361" Y="-22.490031463868" />
                  <Point X="4.732363419136" Y="-26.059233225133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.053328296846" Y="-22.407122559219" />
                  <Point X="4.753771150605" Y="-25.965421709527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.004935022104" Y="-22.324213910814" />
                  <Point X="3.472361243112" Y="-25.345237700301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.058997573699" Y="-25.582254162886" />
                  <Point X="4.770839654921" Y="-25.869857032352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.944953768649" Y="-22.245987109714" />
                  <Point X="3.379995970492" Y="-25.205458907251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.884304263426" Y="-22.168030299854" />
                  <Point X="3.355031548076" Y="-25.092911825328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.823654758203" Y="-22.090073489994" />
                  <Point X="3.352187362041" Y="-24.989301899024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.763005252979" Y="-22.012116680133" />
                  <Point X="-3.368949690597" Y="-22.171325461772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.11998561826" Y="-22.271913476287" />
                  <Point X="3.369547418088" Y="-24.893855016395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.931463389203" Y="-22.245620600425" />
                  <Point X="3.404854456736" Y="-24.805659185411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.848596370034" Y="-22.176640248871" />
                  <Point X="3.463934201746" Y="-24.727068151256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.783657188079" Y="-22.100416580911" />
                  <Point X="3.542569130088" Y="-24.656377924019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751539732612" Y="-22.010932074672" />
                  <Point X="3.647212004767" Y="-24.596195589182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752971405117" Y="-21.907892840879" />
                  <Point X="3.791428778798" Y="-24.552002147541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.77579878412" Y="-21.796209180541" />
                  <Point X="3.9439053608" Y="-24.511145884941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.849235841068" Y="-21.664077883031" />
                  <Point X="4.096381942801" Y="-24.470289622341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.926388761468" Y="-21.530445279235" />
                  <Point X="4.248858524803" Y="-24.429433359741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.003541681868" Y="-21.39681267544" />
                  <Point X="3.082834870152" Y="-23.855868422764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.542690954133" Y="-24.041662340802" />
                  <Point X="4.401335106805" Y="-24.388577097141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.035216278541" Y="-21.281554507137" />
                  <Point X="3.021931080449" Y="-23.728800893917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.733965374141" Y="-24.016481422262" />
                  <Point X="4.553811688806" Y="-24.347720834541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947696546643" Y="-21.214453973547" />
                  <Point X="3.001542628097" Y="-23.618102623908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.925237636153" Y="-23.991299631836" />
                  <Point X="4.706288270808" Y="-24.306864571941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.860176814745" Y="-21.147353439958" />
                  <Point X="3.012584578409" Y="-23.520103060864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.116509898166" Y="-23.966117841409" />
                  <Point X="4.775509434361" Y="-24.232370936845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.772657082847" Y="-21.080252906368" />
                  <Point X="3.041299345717" Y="-23.429243779371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.307782160178" Y="-23.940936050982" />
                  <Point X="4.758485889815" Y="-24.123032177837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.685136636219" Y="-21.013152661548" />
                  <Point X="3.0926015684" Y="-23.347510422224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.499054422191" Y="-23.915754260556" />
                  <Point X="4.7344471064" Y="-24.010859078346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586450354805" Y="-20.950563706815" />
                  <Point X="2.082559296748" Y="-22.83696605472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.516949778398" Y="-23.01247120156" />
                  <Point X="3.156432275046" Y="-23.270838901168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.690326684203" Y="-23.890572470129" />
                  <Point X="4.706782980036" Y="-23.897221245226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.479676605452" Y="-20.89124230123" />
                  <Point X="2.023768111576" Y="-22.710752073508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.650304869251" Y="-22.963889355059" />
                  <Point X="3.242211660719" Y="-23.203035222062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.3729028561" Y="-20.831920895645" />
                  <Point X="2.012881382253" Y="-22.603892748794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.754710493764" Y="-22.903611164932" />
                  <Point X="3.329683868149" Y="-23.135915487341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.266129106747" Y="-20.77259949006" />
                  <Point X="-2.113142002776" Y="-20.834410292279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.787387096734" Y="-20.966023817514" />
                  <Point X="2.028386993303" Y="-22.507696621751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859116023004" Y="-22.843332936313" />
                  <Point X="3.417156075579" Y="-23.06879575262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.654329346563" Y="-20.917321837579" />
                  <Point X="2.057626836786" Y="-22.417049484803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.963521234112" Y="-22.78305457916" />
                  <Point X="3.504628283009" Y="-23.001676017899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.558477867708" Y="-20.853587548267" />
                  <Point X="2.104497670667" Y="-22.333525730363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.06792644522" Y="-22.722776222007" />
                  <Point X="3.59210049044" Y="-22.934556283178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.508189223752" Y="-20.771444678733" />
                  <Point X="2.15246448793" Y="-22.250444781953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.172331656328" Y="-22.662497864854" />
                  <Point X="3.67957269787" Y="-22.867436548457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.479057429729" Y="-20.680753886969" />
                  <Point X="2.200431305192" Y="-22.167363833543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.276736867436" Y="-22.602219507702" />
                  <Point X="3.7670449053" Y="-22.800316813736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462361764423" Y="-20.585038573057" />
                  <Point X="2.248398122455" Y="-22.084282885132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.381142078545" Y="-22.541941150549" />
                  <Point X="3.85451711273" Y="-22.733197079015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.473926500951" Y="-20.47790531565" />
                  <Point X="2.296364939718" Y="-22.001201936722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.485547289653" Y="-22.481662793396" />
                  <Point X="3.94198932016" Y="-22.666077344294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.3856694036" Y="-20.411102697041" />
                  <Point X="2.34433175698" Y="-21.918120988311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.589952500761" Y="-22.421384436243" />
                  <Point X="4.02946152759" Y="-22.598957609573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.235957940674" Y="-20.369129253818" />
                  <Point X="-0.205150500426" Y="-20.785602493464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.081248208527" Y="-20.901315082926" />
                  <Point X="2.392298574243" Y="-21.835040039901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.694357711869" Y="-22.36110607909" />
                  <Point X="4.11693373502" Y="-22.531837874852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.086246477747" Y="-20.327155810594" />
                  <Point X="-0.244722874685" Y="-20.66715341589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.180031345041" Y="-20.838765260193" />
                  <Point X="2.440265391506" Y="-21.751959091491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.798762922977" Y="-22.300827721937" />
                  <Point X="4.073535654119" Y="-22.411843111463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.936532134347" Y="-20.285183531157" />
                  <Point X="-0.27551006728" Y="-20.552253782107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.23094308956" Y="-20.756874139628" />
                  <Point X="2.488232208768" Y="-21.66887814308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.903168134085" Y="-22.240549364785" />
                  <Point X="3.97747623869" Y="-22.270571787837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.74157651764" Y="-20.261489912626" />
                  <Point X="-0.306297259876" Y="-20.437354148324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257566271051" Y="-20.665169802611" />
                  <Point X="2.536199026031" Y="-21.58579719467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.544939747265" Y="-20.238475524266" />
                  <Point X="-0.337084452472" Y="-20.322454514541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.282338686562" Y="-20.5727177076" />
                  <Point X="2.584165843294" Y="-21.502716246259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.307111081417" Y="-20.480265604244" />
                  <Point X="2.632132660556" Y="-21.419635297849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331883476272" Y="-20.387813500888" />
                  <Point X="2.680099477819" Y="-21.336554349439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.356655871127" Y="-20.295361397532" />
                  <Point X="2.728066295082" Y="-21.253473401028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.422932117422" Y="-20.219677938631" />
                  <Point X="2.776033112344" Y="-21.170392452618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765270192864" Y="-20.255530698656" />
                  <Point X="2.774521685661" Y="-21.067320996045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.342814529431" Y="-20.386412956658" />
                  <Point X="2.305178917697" Y="-20.775233408327" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998373046875" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.818699462891" Y="-29.832732421875" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.46431942749" Y="-28.521544921875" />
                  <Point X="0.322426269531" Y="-28.31710546875" />
                  <Point X="0.300591796875" Y="-28.28564453125" />
                  <Point X="0.274335754395" Y="-28.2663984375" />
                  <Point X="0.054764087677" Y="-28.198251953125" />
                  <Point X="0.020976625443" Y="-28.187765625" />
                  <Point X="-0.008664756775" Y="-28.187765625" />
                  <Point X="-0.22823626709" Y="-28.255912109375" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.430172332764" Y="-28.490083984375" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.512647949219" Y="-28.73648046875" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-1.061745239258" Y="-29.9455390625" />
                  <Point X="-1.100260131836" Y="-29.9380625" />
                  <Point X="-1.35158972168" Y="-29.8733984375" />
                  <Point X="-1.350940185547" Y="-29.86846484375" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.362138793945" Y="-29.271046875" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282104492" Y="-29.20262890625" />
                  <Point X="-1.588435668945" Y="-29.025345703125" />
                  <Point X="-1.61954284668" Y="-28.998064453125" />
                  <Point X="-1.649241821289" Y="-28.985763671875" />
                  <Point X="-1.917544677734" Y="-28.968177734375" />
                  <Point X="-1.958831054688" Y="-28.96547265625" />
                  <Point X="-1.989878295898" Y="-28.973791015625" />
                  <Point X="-2.213442382812" Y="-29.123171875" />
                  <Point X="-2.247844482422" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.289725585938" Y="-29.196380859375" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.799406738281" Y="-29.202548828125" />
                  <Point X="-2.855835205078" Y="-29.167611328125" />
                  <Point X="-3.208662597656" Y="-28.8959453125" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-3.151192138672" Y="-28.74656640625" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513981689453" Y="-27.56876171875" />
                  <Point X="-2.529016601562" Y="-27.553728515625" />
                  <Point X="-2.531322753906" Y="-27.551421875" />
                  <Point X="-2.560153320312" Y="-27.537201171875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.763635498047" Y="-27.642748046875" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.117122070312" Y="-27.905701171875" />
                  <Point X="-4.161701660156" Y="-27.8471328125" />
                  <Point X="-4.414653808594" Y="-27.422970703125" />
                  <Point X="-4.43101953125" Y="-27.39552734375" />
                  <Point X="-4.292604980469" Y="-27.289318359375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822509766" Y="-26.396017578125" />
                  <Point X="-3.139145507812" Y="-26.370236328125" />
                  <Point X="-3.138117919922" Y="-26.36626953125" />
                  <Point X="-3.140325927734" Y="-26.33459765625" />
                  <Point X="-3.161159179688" Y="-26.310640625" />
                  <Point X="-3.184110107422" Y="-26.2971328125" />
                  <Point X="-3.187646240234" Y="-26.29505078125" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.436602539062" Y="-26.317150390625" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.909956542969" Y="-26.079455078125" />
                  <Point X="-4.927392578125" Y="-26.011193359375" />
                  <Point X="-4.994317382812" Y="-25.54326171875" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-4.843357910156" Y="-25.473201171875" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541899169922" Y="-25.121427734375" />
                  <Point X="-3.517847412109" Y="-25.104734375" />
                  <Point X="-3.514146240234" Y="-25.102166015625" />
                  <Point X="-3.494899169922" Y="-25.07591015625" />
                  <Point X="-3.486881835938" Y="-25.050078125" />
                  <Point X="-3.485648193359" Y="-25.046103515625" />
                  <Point X="-3.485647949219" Y="-25.0164609375" />
                  <Point X="-3.493663330078" Y="-24.990634765625" />
                  <Point X="-3.494896972656" Y="-24.986658203125" />
                  <Point X="-3.514146240234" Y="-24.96039453125" />
                  <Point X="-3.538197998047" Y="-24.943701171875" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.755336425781" Y="-24.88089453125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.928881835938" Y="-24.079517578125" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.782929199219" Y="-23.5064375" />
                  <Point X="-4.773516113281" Y="-23.471701171875" />
                  <Point X="-4.675026367188" Y="-23.48466796875" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731706054688" Y="-23.6041328125" />
                  <Point X="-3.678471679688" Y="-23.587349609375" />
                  <Point X="-3.670272460938" Y="-23.584763671875" />
                  <Point X="-3.651531738281" Y="-23.57394140625" />
                  <Point X="-3.639119628906" Y="-23.556212890625" />
                  <Point X="-3.617759033203" Y="-23.50464453125" />
                  <Point X="-3.614472167969" Y="-23.496708984375" />
                  <Point X="-3.610714111328" Y="-23.475392578125" />
                  <Point X="-3.616317871094" Y="-23.454484375" />
                  <Point X="-3.642091552734" Y="-23.40497265625" />
                  <Point X="-3.646057617188" Y="-23.39735546875" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.773468505859" Y="-23.2936875" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.203676269531" Y="-22.287796875" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.803154541016" Y="-21.75430078125" />
                  <Point X="-3.774670898438" Y="-21.717689453125" />
                  <Point X="-3.725276367188" Y="-21.74620703125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514404297" Y="-22.07956640625" />
                  <Point X="-3.064373779297" Y="-22.086052734375" />
                  <Point X="-3.052965087891" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.960627197266" Y="-22.019970703125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899658203" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.944560791016" Y="-21.89801953125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.002362792969" Y="-21.778853515625" />
                  <Point X="-3.307278808594" Y="-21.250724609375" />
                  <Point X="-2.828919433594" Y="-20.883970703125" />
                  <Point X="-2.752873779297" Y="-20.82566796875" />
                  <Point X="-2.190832275391" Y="-20.51341015625" />
                  <Point X="-2.141548828125" Y="-20.486029296875" />
                  <Point X="-2.141133056641" Y="-20.4865703125" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.95125" Y="-20.726337890625" />
                  <Point X="-1.868731811523" Y="-20.769294921875" />
                  <Point X="-1.856034057617" Y="-20.77590625" />
                  <Point X="-1.835125610352" Y="-20.781509765625" />
                  <Point X="-1.813808227539" Y="-20.77775" />
                  <Point X="-1.727860107422" Y="-20.7421484375" />
                  <Point X="-1.714634277344" Y="-20.736669921875" />
                  <Point X="-1.696905273438" Y="-20.724255859375" />
                  <Point X="-1.686083618164" Y="-20.70551171875" />
                  <Point X="-1.658109130859" Y="-20.616787109375" />
                  <Point X="-1.653804321289" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.657635620117" Y="-20.538142578125" />
                  <Point X="-1.689137573242" Y="-20.298859375" />
                  <Point X="-1.066537597656" Y="-20.1243046875" />
                  <Point X="-0.968083862305" Y="-20.096703125" />
                  <Point X="-0.286725280762" Y="-20.016958984375" />
                  <Point X="-0.224199981689" Y="-20.009640625" />
                  <Point X="-0.209614196777" Y="-20.064076171875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282119751" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594039917" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.08022303009" Y="-20.592919921875" />
                  <Point X="0.236648406982" Y="-20.009130859375" />
                  <Point X="0.774248168945" Y="-20.06543359375" />
                  <Point X="0.860210205078" Y="-20.074435546875" />
                  <Point X="1.423919067383" Y="-20.210533203125" />
                  <Point X="1.508456420898" Y="-20.230943359375" />
                  <Point X="1.876155639648" Y="-20.364310546875" />
                  <Point X="1.931040527344" Y="-20.384216796875" />
                  <Point X="2.285828613281" Y="-20.550138671875" />
                  <Point X="2.338686523438" Y="-20.574859375" />
                  <Point X="2.681442382812" Y="-20.774548828125" />
                  <Point X="2.732534912109" Y="-20.80431640625" />
                  <Point X="3.055778808594" Y="-21.034189453125" />
                  <Point X="3.068740722656" Y="-21.043408203125" />
                  <Point X="2.976576171875" Y="-21.203041015625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508484375" />
                  <Point X="2.206245605469" Y="-22.578064453125" />
                  <Point X="2.202044677734" Y="-22.607673828125" />
                  <Point X="2.209299804688" Y="-22.66783984375" />
                  <Point X="2.210416015625" Y="-22.67709765625" />
                  <Point X="2.218684082031" Y="-22.69919140625" />
                  <Point X="2.255913085938" Y="-22.75405859375" />
                  <Point X="2.274940185547" Y="-22.775796875" />
                  <Point X="2.329800048828" Y="-22.813021484375" />
                  <Point X="2.338242431641" Y="-22.818751953125" />
                  <Point X="2.360337890625" Y="-22.827021484375" />
                  <Point X="2.420499267578" Y="-22.834275390625" />
                  <Point X="2.448665039062" Y="-22.8340546875" />
                  <Point X="2.518244628906" Y="-22.815447265625" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.740055419922" Y="-22.692677734375" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.172292480469" Y="-22.21601171875" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.382801757813" Y="-22.555919921875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.271746582031" Y="-22.65253515625" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.229294921875" Y="-23.48149609375" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.194466064453" Y="-23.575201171875" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779785156" Y="-23.60903515625" />
                  <Point X="3.206092285156" Y="-23.683248046875" />
                  <Point X="3.215647949219" Y="-23.712048828125" />
                  <Point X="3.257296630859" Y="-23.775353515625" />
                  <Point X="3.263705566406" Y="-23.78509375" />
                  <Point X="3.280946044922" Y="-23.8011796875" />
                  <Point X="3.34130078125" Y="-23.835154296875" />
                  <Point X="3.368565429688" Y="-23.846380859375" />
                  <Point X="3.450168945312" Y="-23.857166015625" />
                  <Point X="3.450174316406" Y="-23.85716796875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.664860839844" Y="-23.833939453125" />
                  <Point X="4.848975097656" Y="-23.678046875" />
                  <Point X="4.926174804688" Y="-23.995162109375" />
                  <Point X="4.939189453125" Y="-24.048619140625" />
                  <Point X="4.995977539062" Y="-24.41336328125" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.867855957031" Y="-24.46027734375" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.6489140625" Y="-24.813521484375" />
                  <Point X="3.622265625" Y="-24.833072265625" />
                  <Point X="3.574161865234" Y="-24.894369140625" />
                  <Point X="3.566759765625" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.540950439453" Y="-25.0089921875" />
                  <Point X="3.538483154297" Y="-25.0406875" />
                  <Point X="3.554517822266" Y="-25.1244140625" />
                  <Point X="3.556985351562" Y="-25.137296875" />
                  <Point X="3.566759765625" Y="-25.158759765625" />
                  <Point X="3.614863525391" Y="-25.220056640625" />
                  <Point X="3.636577148438" Y="-25.241908203125" />
                  <Point X="3.716750244141" Y="-25.28825" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="3.914541748047" Y="-25.34684375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.955697753906" Y="-25.918205078125" />
                  <Point X="4.948431152344" Y="-25.96640234375" />
                  <Point X="4.87567578125" Y="-26.285228515625" />
                  <Point X="4.874546386719" Y="-26.290177734375" />
                  <Point X="4.718624023438" Y="-26.269650390625" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394838134766" Y="-26.098341796875" />
                  <Point X="3.237486816406" Y="-26.13254296875" />
                  <Point X="3.213273925781" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.090336425781" Y="-26.269083984375" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.0507265625" Y="-26.462205078125" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360839844" Y="-26.516623046875" />
                  <Point X="3.14344140625" Y="-26.6520703125" />
                  <Point X="3.156841064453" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.329353027344" Y="-26.808998046875" />
                  <Point X="4.339074707031" Y="-27.58378515625" />
                  <Point X="4.224612792969" Y="-27.769" />
                  <Point X="4.204136230469" Y="-27.80213671875" />
                  <Point X="4.056687255859" Y="-28.011638671875" />
                  <Point X="3.9171953125" Y="-27.931103515625" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737338623047" Y="-27.2533125" />
                  <Point X="2.550065673828" Y="-27.219490234375" />
                  <Point X="2.521248291016" Y="-27.214287109375" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.333499755859" Y="-27.301125" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288600097656" Y="-27.33468359375" />
                  <Point X="2.206720458984" Y="-27.49026171875" />
                  <Point X="2.19412109375" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.222984375" Y="-27.7336484375" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.337480957031" Y="-27.957654296875" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.859604980469" Y="-29.1728515625" />
                  <Point X="2.835293945312" Y="-29.19021484375" />
                  <Point X="2.679775878906" Y="-29.290880859375" />
                  <Point X="2.571219238281" Y="-29.14940625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670548095703" Y="-27.980466796875" />
                  <Point X="1.485846435547" Y="-27.861720703125" />
                  <Point X="1.457424682617" Y="-27.84344921875" />
                  <Point X="1.425806396484" Y="-27.83571875" />
                  <Point X="1.223803710938" Y="-27.854306640625" />
                  <Point X="1.192719604492" Y="-27.857166015625" />
                  <Point X="1.165331176758" Y="-27.868509765625" />
                  <Point X="1.009349853516" Y="-27.998203125" />
                  <Point X="0.985347412109" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.921819091797" Y="-28.260556640625" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.943229309082" Y="-28.5332734375" />
                  <Point X="1.127642333984" Y="-29.93402734375" />
                  <Point X="1.017339904785" Y="-29.95820703125" />
                  <Point X="0.994335083008" Y="-29.963248046875" />
                  <Point X="0.860200317383" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#204" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.167473804092" Y="4.980145251278" Z="2.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.2" />
                  <Point X="-0.298693537241" Y="5.063981769904" Z="2.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.2" />
                  <Point X="-1.08619104069" Y="4.95512000766" Z="2.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.2" />
                  <Point X="-1.713363905394" Y="4.486613220306" Z="2.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.2" />
                  <Point X="-1.711950597941" Y="4.4295278149" Z="2.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.2" />
                  <Point X="-1.753150804782" Y="4.335325806788" Z="2.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.2" />
                  <Point X="-1.851796909826" Y="4.306334713563" Z="2.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.2" />
                  <Point X="-2.10762148629" Y="4.575148572634" Z="2.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.2" />
                  <Point X="-2.221271493599" Y="4.56157817574" Z="2.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.2" />
                  <Point X="-2.865497500482" Y="4.186989596487" Z="2.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.2" />
                  <Point X="-3.051820029124" Y="3.227426993309" Z="2.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.2" />
                  <Point X="-3.000526543691" Y="3.128904197465" Z="2.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.2" />
                  <Point X="-3.002137998096" Y="3.04666556979" Z="2.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.2" />
                  <Point X="-3.066172268141" Y="2.995038155482" Z="2.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.2" />
                  <Point X="-3.706432354988" Y="3.328373919622" Z="2.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.2" />
                  <Point X="-3.848773961386" Y="3.307682059455" Z="2.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.2" />
                  <Point X="-4.253014422393" Y="2.768688229611" Z="2.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.2" />
                  <Point X="-3.810062415468" Y="1.697925821193" Z="2.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.2" />
                  <Point X="-3.692596234804" Y="1.60321539919" Z="2.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.2" />
                  <Point X="-3.670109377626" Y="1.54576899532" Z="2.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.2" />
                  <Point X="-3.699661593614" Y="1.491617284349" Z="2.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.2" />
                  <Point X="-4.674656079028" Y="1.596184578759" Z="2.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.2" />
                  <Point X="-4.837344441734" Y="1.537920624915" Z="2.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.2" />
                  <Point X="-4.984499123684" Y="0.959080936969" Z="2.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.2" />
                  <Point X="-3.774433222182" Y="0.102088805845" Z="2.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.2" />
                  <Point X="-3.572859660558" Y="0.046500274567" Z="2.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.2" />
                  <Point X="-3.547574146509" Y="0.025831948089" Z="2.2" />
                  <Point X="-3.539556741714" Y="0" Z="2.2" />
                  <Point X="-3.540790454626" Y="-0.003975002972" Z="2.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.2" />
                  <Point X="-3.552508934565" Y="-0.032375708567" Z="2.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.2" />
                  <Point X="-4.862454566591" Y="-0.393623250911" Z="2.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.2" />
                  <Point X="-5.049969740346" Y="-0.519060262802" Z="2.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.2" />
                  <Point X="-4.964557220809" Y="-1.06054926842" Z="2.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.2" />
                  <Point X="-3.436232044995" Y="-1.335441525766" Z="2.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.2" />
                  <Point X="-3.215627216935" Y="-1.308941895776" Z="2.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.2" />
                  <Point X="-3.193703763962" Y="-1.326416860285" Z="2.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.2" />
                  <Point X="-4.329198833412" Y="-2.218369673595" Z="2.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.2" />
                  <Point X="-4.463753855142" Y="-2.417298837949" Z="2.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.2" />
                  <Point X="-4.163064094397" Y="-2.904703649384" Z="2.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.2" />
                  <Point X="-2.744791550186" Y="-2.65476753255" Z="2.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.2" />
                  <Point X="-2.570525929116" Y="-2.557804540015" Z="2.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.2" />
                  <Point X="-3.200649480618" Y="-3.690286832306" Z="2.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.2" />
                  <Point X="-3.245322434352" Y="-3.904281843611" Z="2.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.2" />
                  <Point X="-2.831883527809" Y="-4.213780856694" Z="2.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.2" />
                  <Point X="-2.256213945031" Y="-4.195538068452" Z="2.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.2" />
                  <Point X="-2.191820343815" Y="-4.133465508485" Z="2.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.2" />
                  <Point X="-1.926969728308" Y="-3.986790415692" Z="2.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.2" />
                  <Point X="-1.627559586624" Y="-4.031656308815" Z="2.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.2" />
                  <Point X="-1.417334144878" Y="-4.249520319371" Z="2.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.2" />
                  <Point X="-1.406668446767" Y="-4.83065798628" Z="2.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.2" />
                  <Point X="-1.373665386378" Y="-4.889649189117" Z="2.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.2" />
                  <Point X="-1.077429867462" Y="-4.963342272766" Z="2.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.2" />
                  <Point X="-0.470507603143" Y="-3.718142319052" Z="2.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.2" />
                  <Point X="-0.395252339657" Y="-3.487313813014" Z="2.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.2" />
                  <Point X="-0.219571554723" Y="-3.272386276078" Z="2.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.2" />
                  <Point X="0.033787524638" Y="-3.21472576921" Z="2.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.2" />
                  <Point X="0.275193519775" Y="-3.314331942754" Z="2.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.2" />
                  <Point X="0.764247257259" Y="-4.814393710763" Z="2.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.2" />
                  <Point X="0.84171818513" Y="-5.009393924595" Z="2.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.2" />
                  <Point X="1.02188699602" Y="-4.975767588065" Z="2.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.2" />
                  <Point X="0.986645551248" Y="-3.495467497598" Z="2.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.2" />
                  <Point X="0.964522355815" Y="-3.239895833381" Z="2.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.2" />
                  <Point X="1.035162332847" Y="-3.005368874236" Z="2.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.2" />
                  <Point X="1.222227787347" Y="-2.872815058549" Z="2.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.2" />
                  <Point X="1.452652226447" Y="-2.872499161306" Z="2.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.2" />
                  <Point X="2.525395430985" Y="-4.148563292738" Z="2.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.2" />
                  <Point X="2.688081790601" Y="-4.309798588238" Z="2.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.2" />
                  <Point X="2.882510164756" Y="-4.182258098382" Z="2.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.2" />
                  <Point X="2.374626311751" Y="-2.901374267341" Z="2.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.2" />
                  <Point X="2.266032486134" Y="-2.693481178094" Z="2.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.2" />
                  <Point X="2.244810678727" Y="-2.482267976294" Z="2.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.2" />
                  <Point X="2.350630402078" Y="-2.31409063124" Z="2.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.2" />
                  <Point X="2.535025625419" Y="-2.237415523357" Z="2.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.2" />
                  <Point X="3.886039329766" Y="-2.943123188524" Z="2.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.2" />
                  <Point X="4.088400296685" Y="-3.013427368972" Z="2.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.2" />
                  <Point X="4.260991065682" Y="-2.764003568699" Z="2.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.2" />
                  <Point X="3.35363498749" Y="-1.738049944329" Z="2.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.2" />
                  <Point X="3.179342944704" Y="-1.593750396559" Z="2.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.2" />
                  <Point X="3.094360091872" Y="-1.435507535862" Z="2.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.2" />
                  <Point X="3.122626734887" Y="-1.269770535686" Z="2.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.2" />
                  <Point X="3.241948631253" Y="-1.150121364976" Z="2.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.2" />
                  <Point X="4.705941582365" Y="-1.287943057677" Z="2.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.2" />
                  <Point X="4.918266397823" Y="-1.265072431321" Z="2.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.2" />
                  <Point X="4.998983362637" Y="-0.894378564692" Z="2.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.2" />
                  <Point X="3.921326754736" Y="-0.26726607279" Z="2.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.2" />
                  <Point X="3.735615729523" Y="-0.213679647003" Z="2.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.2" />
                  <Point X="3.648040532608" Y="-0.157906087913" Z="2.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.2" />
                  <Point X="3.597469329681" Y="-0.083726719387" Z="2.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.2" />
                  <Point X="3.583902120743" Y="0.012883811795" Z="2.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.2" />
                  <Point X="3.607338905792" Y="0.106042650662" Z="2.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.2" />
                  <Point X="3.66777968483" Y="0.174469197239" Z="2.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.2" />
                  <Point X="4.874641062571" Y="0.522705859978" Z="2.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.2" />
                  <Point X="5.039226510854" Y="0.62560908265" Z="2.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.2" />
                  <Point X="4.968600036732" Y="1.047947297137" Z="2.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.2" />
                  <Point X="3.652178617796" Y="1.246913943841" Z="2.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.2" />
                  <Point X="3.450564294873" Y="1.223683659425" Z="2.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.2" />
                  <Point X="3.359673274469" Y="1.239696549832" Z="2.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.2" />
                  <Point X="3.292909680203" Y="1.283412234451" Z="2.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.2" />
                  <Point X="3.248904693866" Y="1.358136244856" Z="2.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.2" />
                  <Point X="3.236462430124" Y="1.442613056087" Z="2.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.2" />
                  <Point X="3.262821715636" Y="1.519366434285" Z="2.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.2" />
                  <Point X="4.296028444498" Y="2.339077475562" Z="2.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.2" />
                  <Point X="4.419422946051" Y="2.501248087094" Z="2.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.2" />
                  <Point X="4.206721720577" Y="2.844472791155" Z="2.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.2" />
                  <Point X="2.708899544614" Y="2.381903949757" Z="2.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.2" />
                  <Point X="2.499171084051" Y="2.264135620395" Z="2.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.2" />
                  <Point X="2.420333276551" Y="2.24664556385" Z="2.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.2" />
                  <Point X="2.351724048252" Y="2.259629271092" Z="2.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.2" />
                  <Point X="2.291129329143" Y="2.305300812131" Z="2.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.2" />
                  <Point X="2.252784138909" Y="2.369425167282" Z="2.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.2" />
                  <Point X="2.248392322263" Y="2.440298491831" Z="2.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.2" />
                  <Point X="3.013721403103" Y="3.803239537341" Z="2.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.2" />
                  <Point X="3.078600070145" Y="4.037837255393" Z="2.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.2" />
                  <Point X="2.700456577686" Y="4.29993396314" Z="2.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.2" />
                  <Point X="2.30085492832" Y="4.526431940512" Z="2.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.2" />
                  <Point X="1.887048102043" Y="4.713974650699" Z="2.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.2" />
                  <Point X="1.429497421607" Y="4.869351476594" Z="2.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.2" />
                  <Point X="0.773300102485" Y="5.015575725289" Z="2.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.2" />
                  <Point X="0.025770757461" Y="4.451302254673" Z="2.2" />
                  <Point X="0" Y="4.355124473572" Z="2.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>