<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#153" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1476" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.705440429688" Y="-29.0429921875" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557719848633" Y="-28.497138671875" />
                  <Point X="0.542362731934" Y="-28.467376953125" />
                  <Point X="0.484267242432" Y="-28.383673828125" />
                  <Point X="0.378635223389" Y="-28.2314765625" />
                  <Point X="0.356752197266" Y="-28.2090234375" />
                  <Point X="0.330497009277" Y="-28.189779296875" />
                  <Point X="0.302494262695" Y="-28.175669921875" />
                  <Point X="0.21259487915" Y="-28.14776953125" />
                  <Point X="0.049135276794" Y="-28.097037109375" />
                  <Point X="0.020983165741" Y="-28.092767578125" />
                  <Point X="-0.008658161163" Y="-28.092765625" />
                  <Point X="-0.036824691772" Y="-28.09703515625" />
                  <Point X="-0.126724235535" Y="-28.1249375" />
                  <Point X="-0.290183685303" Y="-28.17566796875" />
                  <Point X="-0.318183990479" Y="-28.189775390625" />
                  <Point X="-0.34443963623" Y="-28.20901953125" />
                  <Point X="-0.366323730469" Y="-28.2314765625" />
                  <Point X="-0.424419219971" Y="-28.315181640625" />
                  <Point X="-0.530051391602" Y="-28.467376953125" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.809432861328" Y="-29.477044921875" />
                  <Point X="-0.916584655762" Y="-29.87694140625" />
                  <Point X="-1.079360229492" Y="-29.845345703125" />
                  <Point X="-1.180038818359" Y="-29.819443359375" />
                  <Point X="-1.24641809082" Y="-29.802365234375" />
                  <Point X="-1.232073120117" Y="-29.693404296875" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.237985473633" Y="-29.40825390625" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937988281" Y="-29.182966796875" />
                  <Point X="-1.304010009766" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131205078125" />
                  <Point X="-1.406412963867" Y="-29.058619140625" />
                  <Point X="-1.556905517578" Y="-28.926640625" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886108398" Y="-28.897994140625" />
                  <Point X="-1.643028198242" Y="-28.890966796875" />
                  <Point X="-1.752879882812" Y="-28.883767578125" />
                  <Point X="-1.9526171875" Y="-28.87067578125" />
                  <Point X="-1.98341418457" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042657104492" Y="-28.89480078125" />
                  <Point X="-2.13419140625" Y="-28.9559609375" />
                  <Point X="-2.300623535156" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102539062" Y="-29.099462890625" />
                  <Point X="-2.480147949219" Y="-29.288490234375" />
                  <Point X="-2.524526855469" Y="-29.26101171875" />
                  <Point X="-2.801713134766" Y="-29.089384765625" />
                  <Point X="-2.941144042969" Y="-28.98202734375" />
                  <Point X="-3.104721435547" Y="-28.856078125" />
                  <Point X="-2.687909667969" Y="-28.134138671875" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858398438" Y="-27.647650390625" />
                  <Point X="-2.406587890625" Y="-27.61612109375" />
                  <Point X="-2.405576660156" Y="-27.58518359375" />
                  <Point X="-2.414564208984" Y="-27.555564453125" />
                  <Point X="-2.428784912109" Y="-27.526734375" />
                  <Point X="-2.446808349609" Y="-27.5015859375" />
                  <Point X="-2.464151855469" Y="-27.4842421875" />
                  <Point X="-2.489310546875" Y="-27.466212890625" />
                  <Point X="-2.518139892578" Y="-27.45199609375" />
                  <Point X="-2.547757568359" Y="-27.44301171875" />
                  <Point X="-2.57869140625" Y="-27.444025390625" />
                  <Point X="-2.610218261719" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.471064453125" Y="-27.941486328125" />
                  <Point X="-3.8180234375" Y="-28.141802734375" />
                  <Point X="-3.863871337891" Y="-28.081568359375" />
                  <Point X="-4.082862304687" Y="-27.793857421875" />
                  <Point X="-4.182822265625" Y="-27.6262421875" />
                  <Point X="-4.306143066406" Y="-27.419451171875" />
                  <Point X="-3.568128662109" Y="-26.853154296875" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.043347412109" Y="-26.35966015625" />
                  <Point X="-3.045555664062" Y="-26.327990234375" />
                  <Point X="-3.052555908203" Y="-26.29824609375" />
                  <Point X="-3.06863671875" Y="-26.27226171875" />
                  <Point X="-3.089468017578" Y="-26.2483046875" />
                  <Point X="-3.112973144531" Y="-26.228767578125" />
                  <Point X="-3.139459960938" Y="-26.2131796875" />
                  <Point X="-3.168722412109" Y="-26.20195703125" />
                  <Point X="-3.200607666016" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.282100097656" Y="-26.332642578125" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.748428710938" Y="-26.32796484375" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.860524902344" Y="-25.80773828125" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.059361816406" Y="-25.361482421875" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.511491943359" Y="-25.211783203125" />
                  <Point X="-3.486075439453" Y="-25.197728515625" />
                  <Point X="-3.47787890625" Y="-25.192634765625" />
                  <Point X="-3.459973632812" Y="-25.18020703125" />
                  <Point X="-3.436019042969" Y="-25.1586796875" />
                  <Point X="-3.414829589844" Y="-25.12765625" />
                  <Point X="-3.404121826172" Y="-25.102341796875" />
                  <Point X="-3.400885986328" Y="-25.0934921875" />
                  <Point X="-3.394917480469" Y="-25.07426171875" />
                  <Point X="-3.389474365234" Y="-25.0455234375" />
                  <Point X="-3.390575195312" Y="-25.011072265625" />
                  <Point X="-3.396104736328" Y="-24.985673828125" />
                  <Point X="-3.398199951172" Y="-24.97772265625" />
                  <Point X="-3.404168457031" Y="-24.9584921875" />
                  <Point X="-3.417483398438" Y="-24.929169921875" />
                  <Point X="-3.440922363281" Y="-24.899423828125" />
                  <Point X="-3.462587890625" Y="-24.881072265625" />
                  <Point X="-3.469821289062" Y="-24.87551953125" />
                  <Point X="-3.4877265625" Y="-24.863091796875" />
                  <Point X="-3.501924560547" Y="-24.854953125" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-4.49015625" Y="-24.5856484375" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.879686523438" Y="-24.3960546875" />
                  <Point X="-4.82448828125" Y="-24.023029296875" />
                  <Point X="-4.771249511719" Y="-23.8265625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.143064453125" Y="-23.650521484375" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985595703" Y="-23.700658203125" />
                  <Point X="-3.723423095703" Y="-23.698771484375" />
                  <Point X="-3.703141113281" Y="-23.694736328125" />
                  <Point X="-3.681345214844" Y="-23.687865234375" />
                  <Point X="-3.641715087891" Y="-23.675369140625" />
                  <Point X="-3.622775878906" Y="-23.667037109375" />
                  <Point X="-3.604032226562" Y="-23.65621484375" />
                  <Point X="-3.587353271484" Y="-23.643984375" />
                  <Point X="-3.573716064453" Y="-23.628435546875" />
                  <Point X="-3.561301757812" Y="-23.61070703125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.542605957031" Y="-23.57145703125" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532049804688" Y="-23.410623046875" />
                  <Point X="-3.542602294922" Y="-23.3903515625" />
                  <Point X="-3.561789550781" Y="-23.353494140625" />
                  <Point X="-3.57328125" Y="-23.336294921875" />
                  <Point X="-3.587193359375" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.151234375" Y="-22.884072265625" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.295634765625" Y="-22.633798828125" />
                  <Point X="-4.081156738281" Y="-22.26634765625" />
                  <Point X="-3.940129394531" Y="-22.08507421875" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.438444335938" Y="-22.021505859375" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187728515625" Y="-22.163658203125" />
                  <Point X="-3.167085693359" Y="-22.17016796875" />
                  <Point X="-3.146795410156" Y="-22.174205078125" />
                  <Point X="-3.116439941406" Y="-22.176861328125" />
                  <Point X="-3.06124609375" Y="-22.181689453125" />
                  <Point X="-3.040559814453" Y="-22.18123828125" />
                  <Point X="-3.019101806641" Y="-22.178412109375" />
                  <Point X="-2.999012695312" Y="-22.173494140625" />
                  <Point X="-2.980463623047" Y="-22.16434765625" />
                  <Point X="-2.962209716797" Y="-22.15271875" />
                  <Point X="-2.946077392578" Y="-22.13976953125" />
                  <Point X="-2.924530761719" Y="-22.11822265625" />
                  <Point X="-2.885353759766" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.846091552734" Y="-21.933525390625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.1131171875" Y="-21.397021484375" />
                  <Point X="-3.183332519531" Y="-21.275404296875" />
                  <Point X="-3.074180175781" Y="-21.19171875" />
                  <Point X="-2.700620605469" Y="-20.905314453125" />
                  <Point X="-2.47851953125" Y="-20.781919921875" />
                  <Point X="-2.167036132812" Y="-20.608865234375" />
                  <Point X="-2.114152587891" Y="-20.67778515625" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028893066406" Y="-20.78519921875" />
                  <Point X="-2.012314086914" Y="-20.799111328125" />
                  <Point X="-1.995114257812" Y="-20.810603515625" />
                  <Point X="-1.961328491211" Y="-20.82819140625" />
                  <Point X="-1.899897949219" Y="-20.860171875" />
                  <Point X="-1.880625366211" Y="-20.86766796875" />
                  <Point X="-1.859718383789" Y="-20.873271484375" />
                  <Point X="-1.839268798828" Y="-20.876419921875" />
                  <Point X="-1.818622680664" Y="-20.87506640625" />
                  <Point X="-1.797306762695" Y="-20.871306640625" />
                  <Point X="-1.777453491211" Y="-20.865517578125" />
                  <Point X="-1.742263549805" Y="-20.85094140625" />
                  <Point X="-1.678279296875" Y="-20.8244375" />
                  <Point X="-1.660145141602" Y="-20.814490234375" />
                  <Point X="-1.642415771484" Y="-20.802076171875" />
                  <Point X="-1.626863647461" Y="-20.7884375" />
                  <Point X="-1.614632324219" Y="-20.771755859375" />
                  <Point X="-1.603810546875" Y="-20.75301171875" />
                  <Point X="-1.595480102539" Y="-20.734078125" />
                  <Point X="-1.584026489258" Y="-20.697751953125" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584202026367" Y="-20.3681015625" />
                  <Point X="-1.433224975586" Y="-20.3257734375" />
                  <Point X="-0.949624206543" Y="-20.19019140625" />
                  <Point X="-0.68038079834" Y="-20.158677734375" />
                  <Point X="-0.294711303711" Y="-20.113541015625" />
                  <Point X="-0.202472351074" Y="-20.45778125" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082114006042" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155906677" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425956726" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.270890594482" Y="-20.248390625" />
                  <Point X="0.307419433594" Y="-20.1120625" />
                  <Point X="0.421790893555" Y="-20.124041015625" />
                  <Point X="0.844031311035" Y="-20.168259765625" />
                  <Point X="1.066807495117" Y="-20.222044921875" />
                  <Point X="1.481040283203" Y="-20.3220546875" />
                  <Point X="1.625069946289" Y="-20.374294921875" />
                  <Point X="1.894648681641" Y="-20.47207421875" />
                  <Point X="2.034856445313" Y="-20.537642578125" />
                  <Point X="2.294552978516" Y="-20.65909375" />
                  <Point X="2.430054443359" Y="-20.738037109375" />
                  <Point X="2.680994140625" Y="-20.884236328125" />
                  <Point X="2.808727539062" Y="-20.975072265625" />
                  <Point X="2.943260742188" Y="-21.07074609375" />
                  <Point X="2.453312988281" Y="-21.919359375" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.4839453125" />
                  <Point X="2.125458740234" Y="-22.51243359375" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.110698486328" Y="-22.643681640625" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140070068359" Y="-22.75252734375" />
                  <Point X="2.155312744141" Y="-22.7749921875" />
                  <Point X="2.183027832031" Y="-22.8158359375" />
                  <Point X="2.194462646484" Y="-22.82966796875" />
                  <Point X="2.221595458984" Y="-22.85440625" />
                  <Point X="2.244059326172" Y="-22.869650390625" />
                  <Point X="2.284904052734" Y="-22.897365234375" />
                  <Point X="2.304951660156" Y="-22.90773046875" />
                  <Point X="2.327038574219" Y="-22.91599609375" />
                  <Point X="2.34896875" Y="-22.92133984375" />
                  <Point X="2.373602783203" Y="-22.92430859375" />
                  <Point X="2.418393554688" Y="-22.9297109375" />
                  <Point X="2.436466796875" Y="-22.93015625" />
                  <Point X="2.473209472656" Y="-22.925828125" />
                  <Point X="2.501697509766" Y="-22.918208984375" />
                  <Point X="2.553495849609" Y="-22.904357421875" />
                  <Point X="2.565286376953" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.88985546875" />
                  <Point X="3.551373779297" Y="-22.3339609375" />
                  <Point X="3.967326660156" Y="-22.093810546875" />
                  <Point X="3.974432128906" Y="-22.103685546875" />
                  <Point X="4.123271972656" Y="-22.310537109375" />
                  <Point X="4.194490234375" Y="-22.428228515625" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.633308349609" Y="-23.0226796875" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221420410156" Y="-23.339763671875" />
                  <Point X="3.203973632812" Y="-23.358373046875" />
                  <Point X="3.183470703125" Y="-23.38512109375" />
                  <Point X="3.14619140625" Y="-23.43375390625" />
                  <Point X="3.136607421875" Y="-23.4490859375" />
                  <Point X="3.121630126953" Y="-23.482912109375" />
                  <Point X="3.113992675781" Y="-23.510220703125" />
                  <Point X="3.100106201172" Y="-23.559876953125" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.104009033203" Y="-23.6586171875" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120680419922" Y="-23.7310234375" />
                  <Point X="3.136283203125" Y="-23.764259765625" />
                  <Point X="3.153335449219" Y="-23.790177734375" />
                  <Point X="3.184340820312" Y="-23.8373046875" />
                  <Point X="3.198892578125" Y="-23.854548828125" />
                  <Point X="3.216135986328" Y="-23.870638671875" />
                  <Point X="3.234346679688" Y="-23.88396484375" />
                  <Point X="3.259057617188" Y="-23.897875" />
                  <Point X="3.303988769531" Y="-23.92316796875" />
                  <Point X="3.320522216797" Y="-23.9305" />
                  <Point X="3.356118652344" Y="-23.9405625" />
                  <Point X="3.389529785156" Y="-23.944978515625" />
                  <Point X="3.450279296875" Y="-23.953005859375" />
                  <Point X="3.462698974609" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.402836914062" Y="-23.8326015625" />
                  <Point X="4.776839355469" Y="-23.78336328125" />
                  <Point X="4.782010253906" Y="-23.804603515625" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.86837890625" Y="-24.21133984375" />
                  <Point X="4.890864257812" Y="-24.355759765625" />
                  <Point X="4.177881835938" Y="-24.5468046875" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704788085938" Y="-24.674416015625" />
                  <Point X="3.681548095703" Y="-24.684931640625" />
                  <Point X="3.64872265625" Y="-24.703904296875" />
                  <Point X="3.589038085938" Y="-24.73840234375" />
                  <Point X="3.574313232422" Y="-24.74890234375" />
                  <Point X="3.547529541016" Y="-24.774423828125" />
                  <Point X="3.527834472656" Y="-24.799521484375" />
                  <Point X="3.492023681641" Y="-24.84515234375" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.470526855469" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.457115478516" Y="-24.941677734375" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.021876953125" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.451743896484" Y="-25.092833984375" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470526855469" Y="-25.1766640625" />
                  <Point X="3.480301025391" Y="-25.19812890625" />
                  <Point X="3.492023681641" Y="-25.217408203125" />
                  <Point X="3.51171875" Y="-25.242505859375" />
                  <Point X="3.547529541016" Y="-25.28813671875" />
                  <Point X="3.559998046875" Y="-25.301236328125" />
                  <Point X="3.589036865234" Y="-25.324158203125" />
                  <Point X="3.621862304688" Y="-25.343130859375" />
                  <Point X="3.681546875" Y="-25.377630859375" />
                  <Point X="3.692709716797" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.555340332031" Y="-25.616896484375" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.890715820312" Y="-25.71198046875" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.826269042969" Y="-26.074728515625" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.959362792969" Y="-26.073873046875" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374659667969" Y="-26.005509765625" />
                  <Point X="3.310235107422" Y="-26.01951171875" />
                  <Point X="3.193095458984" Y="-26.04497265625" />
                  <Point X="3.163973144531" Y="-26.05659765625" />
                  <Point X="3.136146728516" Y="-26.073490234375" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.073456542969" Y="-26.140794921875" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.964176513672" Y="-26.366017578125" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347167969" Y="-26.507564453125" />
                  <Point X="2.964078613281" Y="-26.539185546875" />
                  <Point X="2.976450927734" Y="-26.567998046875" />
                  <Point X="3.012104492188" Y="-26.623453125" />
                  <Point X="3.076931152344" Y="-26.724287109375" />
                  <Point X="3.086931396484" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.889003173828" Y="-27.358177734375" />
                  <Point X="4.213122558594" Y="-27.6068828125" />
                  <Point X="4.124814453125" Y="-27.749779296875" />
                  <Point X="4.065345947266" Y="-27.834275390625" />
                  <Point X="4.028980224609" Y="-27.8859453125" />
                  <Point X="3.277427001953" Y="-27.452037109375" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.78612890625" Y="-27.17001171875" />
                  <Point X="2.754224609375" Y="-27.159826171875" />
                  <Point X="2.677549072266" Y="-27.145978515625" />
                  <Point X="2.538134277344" Y="-27.12080078125" />
                  <Point X="2.506783691406" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.444833984375" Y="-27.135177734375" />
                  <Point X="2.381135498047" Y="-27.168701171875" />
                  <Point X="2.265315917969" Y="-27.22965625" />
                  <Point X="2.242385498047" Y="-27.246548828125" />
                  <Point X="2.22142578125" Y="-27.2675078125" />
                  <Point X="2.204531738281" Y="-27.290439453125" />
                  <Point X="2.171007568359" Y="-27.354138671875" />
                  <Point X="2.110052734375" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.109522949219" Y="-27.63993359375" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.652003173828" Y="-28.692423828125" />
                  <Point X="2.861283203125" Y="-29.05490625" />
                  <Point X="2.781858398438" Y="-29.11163671875" />
                  <Point X="2.715366210938" Y="-29.15467578125" />
                  <Point X="2.701763671875" Y="-29.16348046875" />
                  <Point X="2.122138671875" Y="-28.408099609375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747507202148" Y="-27.92218359375" />
                  <Point X="1.721923217773" Y="-27.900556640625" />
                  <Point X="1.646300415039" Y="-27.851939453125" />
                  <Point X="1.508799804688" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.751166015625" />
                  <Point X="1.448365478516" Y="-27.743435546875" />
                  <Point X="1.417101318359" Y="-27.741119140625" />
                  <Point X="1.334394775391" Y="-27.748728515625" />
                  <Point X="1.184014282227" Y="-27.76256640625" />
                  <Point X="1.156362060547" Y="-27.7693984375" />
                  <Point X="1.12897668457" Y="-27.7807421875" />
                  <Point X="1.104594970703" Y="-27.795462890625" />
                  <Point X="1.040731201172" Y="-27.848564453125" />
                  <Point X="0.924611206055" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.856529418945" Y="-28.113662109375" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.961490661621" Y="-29.3998046875" />
                  <Point X="1.022065490723" Y="-29.859916015625" />
                  <Point X="0.975727905273" Y="-29.870072265625" />
                  <Point X="0.929315429688" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058460327148" Y="-29.75262890625" />
                  <Point X="-1.141246337891" Y="-29.731330078125" />
                  <Point X="-1.137885864258" Y="-29.7058046875" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.541037109375" />
                  <Point X="-1.121759155273" Y="-29.50933203125" />
                  <Point X="-1.123333740234" Y="-29.497693359375" />
                  <Point X="-1.144810791016" Y="-29.389720703125" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188124633789" Y="-29.178470703125" />
                  <Point X="-1.199026489258" Y="-29.14950390625" />
                  <Point X="-1.205665283203" Y="-29.135466796875" />
                  <Point X="-1.221737304688" Y="-29.10762890625" />
                  <Point X="-1.230575561523" Y="-29.094859375" />
                  <Point X="-1.250210327148" Y="-29.070935546875" />
                  <Point X="-1.261006958008" Y="-29.05978125" />
                  <Point X="-1.343775024414" Y="-28.9871953125" />
                  <Point X="-1.494267578125" Y="-28.855216796875" />
                  <Point X="-1.506735717773" Y="-28.84596875" />
                  <Point X="-1.53301940918" Y="-28.829623046875" />
                  <Point X="-1.546834472656" Y="-28.822525390625" />
                  <Point X="-1.576531494141" Y="-28.810224609375" />
                  <Point X="-1.591316040039" Y="-28.805474609375" />
                  <Point X="-1.621458251953" Y="-28.798447265625" />
                  <Point X="-1.636815673828" Y="-28.796169921875" />
                  <Point X="-1.746667358398" Y="-28.788970703125" />
                  <Point X="-1.946404541016" Y="-28.77587890625" />
                  <Point X="-1.961928710938" Y="-28.7761328125" />
                  <Point X="-1.992725708008" Y="-28.779166015625" />
                  <Point X="-2.007998779297" Y="-28.7819453125" />
                  <Point X="-2.039047729492" Y="-28.790263671875" />
                  <Point X="-2.053667480469" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095435546875" Y="-28.815810546875" />
                  <Point X="-2.186969970703" Y="-28.876970703125" />
                  <Point X="-2.353402099609" Y="-28.988177734375" />
                  <Point X="-2.359681396484" Y="-28.992755859375" />
                  <Point X="-2.380451416016" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471191406" Y="-29.041630859375" />
                  <Point X="-2.503201904297" Y="-29.16248046875" />
                  <Point X="-2.747588378906" Y="-29.011162109375" />
                  <Point X="-2.883186523438" Y="-28.906755859375" />
                  <Point X="-2.980862548828" Y="-28.831548828125" />
                  <Point X="-2.605637207031" Y="-28.181638671875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947021484" Y="-27.68111328125" />
                  <Point X="-2.319683105469" Y="-27.666181640625" />
                  <Point X="-2.313412597656" Y="-27.63465234375" />
                  <Point X="-2.311638671875" Y="-27.619224609375" />
                  <Point X="-2.310627441406" Y="-27.588287109375" />
                  <Point X="-2.314669677734" Y="-27.557599609375" />
                  <Point X="-2.323657226562" Y="-27.52798046875" />
                  <Point X="-2.329365234375" Y="-27.5135390625" />
                  <Point X="-2.3435859375" Y="-27.484708984375" />
                  <Point X="-2.351567871094" Y="-27.47139453125" />
                  <Point X="-2.369591308594" Y="-27.44624609375" />
                  <Point X="-2.3796328125" Y="-27.434412109375" />
                  <Point X="-2.396976318359" Y="-27.417068359375" />
                  <Point X="-2.408814697266" Y="-27.4070234375" />
                  <Point X="-2.433973388672" Y="-27.388994140625" />
                  <Point X="-2.447293701172" Y="-27.381009765625" />
                  <Point X="-2.476123046875" Y="-27.36679296875" />
                  <Point X="-2.490562988281" Y="-27.3610859375" />
                  <Point X="-2.520180664062" Y="-27.3521015625" />
                  <Point X="-2.550868896484" Y="-27.3480625" />
                  <Point X="-2.581802734375" Y="-27.349076171875" />
                  <Point X="-2.597226074219" Y="-27.3508515625" />
                  <Point X="-2.628752929688" Y="-27.357123046875" />
                  <Point X="-2.64368359375" Y="-27.36138671875" />
                  <Point X="-2.672648925781" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.518564453125" Y="-27.85921484375" />
                  <Point X="-3.793087646484" Y="-28.0177109375" />
                  <Point X="-4.004022705078" Y="-27.740583984375" />
                  <Point X="-4.101229980469" Y="-27.577583984375" />
                  <Point X="-4.181266113281" Y="-27.443375" />
                  <Point X="-3.510296386719" Y="-26.9285234375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552978516" Y="-26.3988046875" />
                  <Point X="-2.948748535156" Y="-26.36837890625" />
                  <Point X="-2.948577392578" Y="-26.353052734375" />
                  <Point X="-2.950785644531" Y="-26.3213828125" />
                  <Point X="-2.953082275391" Y="-26.3062265625" />
                  <Point X="-2.960082519531" Y="-26.276482421875" />
                  <Point X="-2.971774169922" Y="-26.24825390625" />
                  <Point X="-2.987854980469" Y="-26.22226953125" />
                  <Point X="-2.996947753906" Y="-26.20992578125" />
                  <Point X="-3.017779052734" Y="-26.18596875" />
                  <Point X="-3.028743164062" Y="-26.17524609375" />
                  <Point X="-3.052248291016" Y="-26.155708984375" />
                  <Point X="-3.064789306641" Y="-26.14689453125" />
                  <Point X="-3.091276123047" Y="-26.131306640625" />
                  <Point X="-3.105441894531" Y="-26.124478515625" />
                  <Point X="-3.134704345703" Y="-26.113255859375" />
                  <Point X="-3.149801025391" Y="-26.108861328125" />
                  <Point X="-3.181686279297" Y="-26.102380859375" />
                  <Point X="-3.197298095703" Y="-26.10053515625" />
                  <Point X="-3.228619384766" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.2945" Y="-26.238455078125" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.740762207031" Y="-25.974115234375" />
                  <Point X="-4.766481933594" Y="-25.794287109375" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.034773925781" Y="-25.45324609375" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.497333496094" Y="-25.30851171875" />
                  <Point X="-3.475949462891" Y="-25.299884765625" />
                  <Point X="-3.465520019531" Y="-25.294919921875" />
                  <Point X="-3.440103515625" Y="-25.280865234375" />
                  <Point X="-3.423710449219" Y="-25.270677734375" />
                  <Point X="-3.405805175781" Y="-25.25825" />
                  <Point X="-3.396473876953" Y="-25.2508671875" />
                  <Point X="-3.372519287109" Y="-25.22933984375" />
                  <Point X="-3.357571044922" Y="-25.212259765625" />
                  <Point X="-3.336381591797" Y="-25.181236328125" />
                  <Point X="-3.327334960938" Y="-25.164666015625" />
                  <Point X="-3.316627197266" Y="-25.1393515625" />
                  <Point X="-3.310155517578" Y="-25.12165234375" />
                  <Point X="-3.304187011719" Y="-25.102421875" />
                  <Point X="-3.301576904297" Y="-25.09194140625" />
                  <Point X="-3.296133789062" Y="-25.063203125" />
                  <Point X="-3.294522705078" Y="-25.042490234375" />
                  <Point X="-3.295623535156" Y="-25.0080390625" />
                  <Point X="-3.297749511719" Y="-24.99086328125" />
                  <Point X="-3.303279052734" Y="-24.96546484375" />
                  <Point X="-3.307469482422" Y="-24.9495625" />
                  <Point X="-3.313437988281" Y="-24.93033203125" />
                  <Point X="-3.317668701172" Y="-24.919212890625" />
                  <Point X="-3.330983642578" Y="-24.889890625" />
                  <Point X="-3.342864990234" Y="-24.870373046875" />
                  <Point X="-3.366303955078" Y="-24.840626953125" />
                  <Point X="-3.379520507812" Y="-24.82693359375" />
                  <Point X="-3.401186035156" Y="-24.80858203125" />
                  <Point X="-3.415652832031" Y="-24.7974765625" />
                  <Point X="-3.433558105469" Y="-24.785048828125" />
                  <Point X="-3.440481689453" Y="-24.780671875" />
                  <Point X="-3.465612792969" Y="-24.767166015625" />
                  <Point X="-3.496564453125" Y="-24.75436328125" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-4.465568359375" Y="-24.493884765625" />
                  <Point X="-4.7854453125" Y="-24.408173828125" />
                  <Point X="-4.731331542969" Y="-24.042474609375" />
                  <Point X="-4.679556640625" Y="-23.85141015625" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.155464355469" Y="-23.744708984375" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057617188" Y="-23.795634765625" />
                  <Point X="-3.736704833984" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704885986328" Y="-23.7919453125" />
                  <Point X="-3.684604003906" Y="-23.78791015625" />
                  <Point X="-3.674578369141" Y="-23.78533984375" />
                  <Point X="-3.652782470703" Y="-23.77846875" />
                  <Point X="-3.61315234375" Y="-23.76597265625" />
                  <Point X="-3.603459716797" Y="-23.762326171875" />
                  <Point X="-3.584520507812" Y="-23.753994140625" />
                  <Point X="-3.575273925781" Y="-23.74930859375" />
                  <Point X="-3.556530273438" Y="-23.738486328125" />
                  <Point X="-3.547854980469" Y="-23.73282421875" />
                  <Point X="-3.531176025391" Y="-23.72059375" />
                  <Point X="-3.515931396484" Y="-23.706625" />
                  <Point X="-3.502294189453" Y="-23.691076171875" />
                  <Point X="-3.495897949219" Y="-23.682927734375" />
                  <Point X="-3.483483642578" Y="-23.66519921875" />
                  <Point X="-3.478012695312" Y="-23.656400390625" />
                  <Point X="-3.4680625" Y="-23.638263671875" />
                  <Point X="-3.463583251953" Y="-23.62892578125" />
                  <Point X="-3.454837646484" Y="-23.6078125" />
                  <Point X="-3.438935791016" Y="-23.569421875" />
                  <Point X="-3.435498779297" Y="-23.5596484375" />
                  <Point X="-3.429710205078" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436012451172" Y="-23.39546875" />
                  <Point X="-3.443508789062" Y="-23.37619140625" />
                  <Point X="-3.447783447266" Y="-23.3667578125" />
                  <Point X="-3.4583359375" Y="-23.346486328125" />
                  <Point X="-3.477523193359" Y="-23.30962890625" />
                  <Point X="-3.482799072266" Y="-23.300716796875" />
                  <Point X="-3.494290771484" Y="-23.283517578125" />
                  <Point X="-3.500506591797" Y="-23.27523046875" />
                  <Point X="-3.514418701172" Y="-23.258650390625" />
                  <Point X="-3.521498535156" Y="-23.251091796875" />
                  <Point X="-3.536440917969" Y="-23.236787109375" />
                  <Point X="-3.544303466797" Y="-23.230041015625" />
                  <Point X="-4.093402099609" Y="-22.808703125" />
                  <Point X="-4.227614746094" Y="-22.70571875" />
                  <Point X="-4.213588378906" Y="-22.6816875" />
                  <Point X="-4.002297607422" Y="-22.319697265625" />
                  <Point X="-3.8651484375" Y="-22.143408203125" />
                  <Point X="-3.726337158203" Y="-21.964986328125" />
                  <Point X="-3.485944335938" Y="-22.10377734375" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244923095703" Y="-22.24228125" />
                  <Point X="-3.225994628906" Y="-22.250611328125" />
                  <Point X="-3.216300048828" Y="-22.254259765625" />
                  <Point X="-3.195657226562" Y="-22.26076953125" />
                  <Point X="-3.185624267578" Y="-22.263341796875" />
                  <Point X="-3.165333984375" Y="-22.26737890625" />
                  <Point X="-3.155076660156" Y="-22.26884375" />
                  <Point X="-3.124721191406" Y="-22.2715" />
                  <Point X="-3.06952734375" Y="-22.276328125" />
                  <Point X="-3.059174560547" Y="-22.276666015625" />
                  <Point X="-3.03848828125" Y="-22.27621484375" />
                  <Point X="-3.028154785156" Y="-22.27542578125" />
                  <Point X="-3.006696777344" Y="-22.272599609375" />
                  <Point X="-2.996512207031" Y="-22.2706875" />
                  <Point X="-2.976423095703" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.938449462891" Y="-22.249552734375" />
                  <Point X="-2.929420410156" Y="-22.244470703125" />
                  <Point X="-2.911166503906" Y="-22.232841796875" />
                  <Point X="-2.902742431641" Y="-22.2268046875" />
                  <Point X="-2.886610107422" Y="-22.21385546875" />
                  <Point X="-2.878901855469" Y="-22.206943359375" />
                  <Point X="-2.857355224609" Y="-22.185396484375" />
                  <Point X="-2.818178222656" Y="-22.146220703125" />
                  <Point X="-2.811268310547" Y="-22.138513671875" />
                  <Point X="-2.798323486328" Y="-22.12238671875" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.751453125" Y="-21.92524609375" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.030844726562" Y="-21.349521484375" />
                  <Point X="-3.059386474609" Y="-21.3000859375" />
                  <Point X="-3.016378173828" Y="-21.267111328125" />
                  <Point X="-2.648368896484" Y="-20.984962890625" />
                  <Point X="-2.432382080078" Y="-20.86496484375" />
                  <Point X="-2.192524169922" Y="-20.731703125" />
                  <Point X="-2.189521484375" Y="-20.7356171875" />
                  <Point X="-2.118564208984" Y="-20.82808984375" />
                  <Point X="-2.111822265625" Y="-20.83594921875" />
                  <Point X="-2.097520019531" Y="-20.850890625" />
                  <Point X="-2.089959716797" Y="-20.85797265625" />
                  <Point X="-2.073380615234" Y="-20.871884765625" />
                  <Point X="-2.065092041016" Y="-20.8781015625" />
                  <Point X="-2.047892211914" Y="-20.88959375" />
                  <Point X="-2.03898059082" Y="-20.894869140625" />
                  <Point X="-2.005194702148" Y="-20.91245703125" />
                  <Point X="-1.943764282227" Y="-20.9444375" />
                  <Point X="-1.934335083008" Y="-20.9487109375" />
                  <Point X="-1.9150625" Y="-20.95620703125" />
                  <Point X="-1.905219360352" Y="-20.9594296875" />
                  <Point X="-1.88431237793" Y="-20.965033203125" />
                  <Point X="-1.874174316406" Y="-20.967166015625" />
                  <Point X="-1.853724731445" Y="-20.970314453125" />
                  <Point X="-1.833054199219" Y="-20.971216796875" />
                  <Point X="-1.812408081055" Y="-20.96986328125" />
                  <Point X="-1.80212109375" Y="-20.968623046875" />
                  <Point X="-1.780805053711" Y="-20.96486328125" />
                  <Point X="-1.770713012695" Y="-20.9625078125" />
                  <Point X="-1.750859863281" Y="-20.95671875" />
                  <Point X="-1.741098510742" Y="-20.95328515625" />
                  <Point X="-1.705908569336" Y="-20.938708984375" />
                  <Point X="-1.641924316406" Y="-20.912205078125" />
                  <Point X="-1.632590576172" Y="-20.907728515625" />
                  <Point X="-1.614456420898" Y="-20.89778125" />
                  <Point X="-1.605655883789" Y="-20.892310546875" />
                  <Point X="-1.587926513672" Y="-20.879896484375" />
                  <Point X="-1.579778320312" Y="-20.873501953125" />
                  <Point X="-1.564226196289" Y="-20.85986328125" />
                  <Point X="-1.550251098633" Y="-20.844611328125" />
                  <Point X="-1.538019775391" Y="-20.8279296875" />
                  <Point X="-1.532359619141" Y="-20.819255859375" />
                  <Point X="-1.521537841797" Y="-20.80051171875" />
                  <Point X="-1.516855102539" Y="-20.791271484375" />
                  <Point X="-1.508524658203" Y="-20.772337890625" />
                  <Point X="-1.504877075195" Y="-20.76264453125" />
                  <Point X="-1.493423339844" Y="-20.726318359375" />
                  <Point X="-1.47259765625" Y="-20.660267578125" />
                  <Point X="-1.470026123047" Y="-20.650234375" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266235352" Y="-20.43734375" />
                  <Point X="-1.407579345703" Y="-20.41724609375" />
                  <Point X="-0.93116607666" Y="-20.2836796875" />
                  <Point X="-0.669336914063" Y="-20.253033203125" />
                  <Point X="-0.36522253418" Y="-20.21744140625" />
                  <Point X="-0.294235290527" Y="-20.482369140625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896308899" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.362653564453" Y="-20.272978515625" />
                  <Point X="0.378190551758" Y="-20.214994140625" />
                  <Point X="0.411895294189" Y="-20.2185234375" />
                  <Point X="0.827852966309" Y="-20.262083984375" />
                  <Point X="1.044512084961" Y="-20.314392578125" />
                  <Point X="1.453624145508" Y="-20.413166015625" />
                  <Point X="1.592677734375" Y="-20.4636015625" />
                  <Point X="1.858255493164" Y="-20.5599296875" />
                  <Point X="1.994612792969" Y="-20.623697265625" />
                  <Point X="2.250419921875" Y="-20.743330078125" />
                  <Point X="2.382231445312" Y="-20.820123046875" />
                  <Point X="2.629453857422" Y="-20.96415625" />
                  <Point X="2.753671386719" Y="-21.0524921875" />
                  <Point X="2.817780761719" Y="-21.098083984375" />
                  <Point X="2.371040527344" Y="-21.871859375" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.0531796875" Y="-22.42656640625" />
                  <Point X="2.044181640625" Y="-22.450439453125" />
                  <Point X="2.041301635742" Y="-22.459404296875" />
                  <Point X="2.033683349609" Y="-22.487892578125" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017912719727" Y="-22.548537109375" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411376953" Y="-22.630421875" />
                  <Point X="2.016381713867" Y="-22.6550546875" />
                  <Point X="2.021782714844" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144287109" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.045319580078" Y="-22.77611328125" />
                  <Point X="2.055680419922" Y="-22.796154296875" />
                  <Point X="2.061457763672" Y="-22.8058671875" />
                  <Point X="2.076700439453" Y="-22.82833203125" />
                  <Point X="2.104415527344" Y="-22.86917578125" />
                  <Point X="2.109808105469" Y="-22.876365234375" />
                  <Point X="2.130456787109" Y="-22.899869140625" />
                  <Point X="2.157589599609" Y="-22.924607421875" />
                  <Point X="2.168250976563" Y="-22.933015625" />
                  <Point X="2.19071484375" Y="-22.948259765625" />
                  <Point X="2.231559570312" Y="-22.975974609375" />
                  <Point X="2.241272949219" Y="-22.98175390625" />
                  <Point X="2.261320556641" Y="-22.992119140625" />
                  <Point X="2.271654785156" Y="-22.996705078125" />
                  <Point X="2.293741699219" Y="-23.004970703125" />
                  <Point X="2.304547851562" Y="-23.008294921875" />
                  <Point X="2.326478027344" Y="-23.013638671875" />
                  <Point X="2.337602050781" Y="-23.015658203125" />
                  <Point X="2.362236083984" Y="-23.018626953125" />
                  <Point X="2.407026855469" Y="-23.024029296875" />
                  <Point X="2.416053466797" Y="-23.024681640625" />
                  <Point X="2.447580566406" Y="-23.02450390625" />
                  <Point X="2.484323242188" Y="-23.02017578125" />
                  <Point X="2.497754638672" Y="-23.0176015625" />
                  <Point X="2.526242675781" Y="-23.009982421875" />
                  <Point X="2.578041015625" Y="-22.996130859375" />
                  <Point X="2.583989746094" Y="-22.994330078125" />
                  <Point X="2.604408691406" Y="-22.986931640625" />
                  <Point X="2.62765625" Y="-22.97642578125" />
                  <Point X="2.636033935547" Y="-22.97212890625" />
                  <Point X="3.598873779297" Y="-22.416234375" />
                  <Point X="3.940405517578" Y="-22.21905078125" />
                  <Point X="4.043953369141" Y="-22.36295703125" />
                  <Point X="4.113212890625" Y="-22.477412109375" />
                  <Point X="4.136883789062" Y="-22.516529296875" />
                  <Point X="3.575476074219" Y="-22.947310546875" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168130859375" Y="-23.26013671875" />
                  <Point X="3.152115234375" Y="-23.2747890625" />
                  <Point X="3.134668457031" Y="-23.2933984375" />
                  <Point X="3.128575683594" Y="-23.300578125" />
                  <Point X="3.108072753906" Y="-23.327326171875" />
                  <Point X="3.070793457031" Y="-23.375958984375" />
                  <Point X="3.065635009766" Y="-23.3833984375" />
                  <Point X="3.049741455078" Y="-23.410625" />
                  <Point X="3.034764160156" Y="-23.444451171875" />
                  <Point X="3.030140869141" Y="-23.45732421875" />
                  <Point X="3.022503417969" Y="-23.4846328125" />
                  <Point X="3.008616943359" Y="-23.5342890625" />
                  <Point X="3.006225585938" Y="-23.545337890625" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.010968994141" Y="-23.677814453125" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024598388672" Y="-23.741767578125" />
                  <Point X="3.034685058594" Y="-23.77139453125" />
                  <Point X="3.050287841797" Y="-23.804630859375" />
                  <Point X="3.056919921875" Y="-23.816474609375" />
                  <Point X="3.073972167969" Y="-23.842392578125" />
                  <Point X="3.104977539062" Y="-23.88951953125" />
                  <Point X="3.111737304688" Y="-23.898572265625" />
                  <Point X="3.1262890625" Y="-23.91581640625" />
                  <Point X="3.134081054688" Y="-23.9240078125" />
                  <Point X="3.151324462891" Y="-23.94009765625" />
                  <Point X="3.160033935547" Y="-23.9473046875" />
                  <Point X="3.178244628906" Y="-23.960630859375" />
                  <Point X="3.187745849609" Y="-23.96675" />
                  <Point X="3.212456787109" Y="-23.98066015625" />
                  <Point X="3.257387939453" Y="-24.005953125" />
                  <Point X="3.2654765625" Y="-24.01001171875" />
                  <Point X="3.294679931641" Y="-24.02191796875" />
                  <Point X="3.330276367188" Y="-24.03198046875" />
                  <Point X="3.343670654297" Y="-24.034744140625" />
                  <Point X="3.377081787109" Y="-24.03916015625" />
                  <Point X="3.437831298828" Y="-24.0471875" />
                  <Point X="3.444033203125" Y="-24.04780078125" />
                  <Point X="3.465709228516" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.415236816406" Y="-23.9267890625" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.774509765625" Y="-24.225955078125" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.153293945312" Y="-24.455041015625" />
                  <Point X="3.691991699219" Y="-24.578646484375" />
                  <Point X="3.686021972656" Y="-24.580458984375" />
                  <Point X="3.665625" Y="-24.58786328125" />
                  <Point X="3.642385009766" Y="-24.59837890625" />
                  <Point X="3.634008789062" Y="-24.602681640625" />
                  <Point X="3.601183349609" Y="-24.621654296875" />
                  <Point X="3.541498779297" Y="-24.65615234375" />
                  <Point X="3.533882324219" Y="-24.661052734375" />
                  <Point X="3.508778320312" Y="-24.680126953125" />
                  <Point X="3.481994628906" Y="-24.7056484375" />
                  <Point X="3.472793945312" Y="-24.715775390625" />
                  <Point X="3.453098876953" Y="-24.740873046875" />
                  <Point X="3.417288085938" Y="-24.78650390625" />
                  <Point X="3.410851318359" Y="-24.795796875" />
                  <Point X="3.399128662109" Y="-24.815076171875" />
                  <Point X="3.393842773438" Y="-24.8250625" />
                  <Point X="3.384068603516" Y="-24.84652734375" />
                  <Point X="3.380005371094" Y="-24.857072265625" />
                  <Point X="3.373159179688" Y="-24.878572265625" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.363811035156" Y="-24.92380859375" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026263671875" />
                  <Point X="3.350280029297" Y="-25.06294140625" />
                  <Point X="3.351874511719" Y="-25.076423828125" />
                  <Point X="3.358439697266" Y="-25.110703125" />
                  <Point X="3.370376708984" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.18398828125" />
                  <Point X="3.380005126953" Y="-25.20548828125" />
                  <Point X="3.384068603516" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.237498046875" />
                  <Point X="3.399128662109" Y="-25.247484375" />
                  <Point X="3.410851318359" Y="-25.266763671875" />
                  <Point X="3.417288085938" Y="-25.276056640625" />
                  <Point X="3.436983154297" Y="-25.301154296875" />
                  <Point X="3.472793945312" Y="-25.34678515625" />
                  <Point X="3.478717285156" Y="-25.353634765625" />
                  <Point X="3.501137451172" Y="-25.3758046875" />
                  <Point X="3.530176269531" Y="-25.3987265625" />
                  <Point X="3.541497558594" Y="-25.406408203125" />
                  <Point X="3.574322998047" Y="-25.425380859375" />
                  <Point X="3.634007568359" Y="-25.459880859375" />
                  <Point X="3.639499511719" Y="-25.462818359375" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026367188" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.530752441406" Y="-25.70866015625" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.76161328125" Y="-25.931052734375" />
                  <Point X="4.733649902344" Y="-26.05359375" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.971762695312" Y="-25.979685546875" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400096923828" Y="-25.90804296875" />
                  <Point X="3.366720947266" Y="-25.910841796875" />
                  <Point X="3.354483398438" Y="-25.912677734375" />
                  <Point X="3.290058837891" Y="-25.9266796875" />
                  <Point X="3.172919189453" Y="-25.952140625" />
                  <Point X="3.157875732422" Y="-25.9567421875" />
                  <Point X="3.128753417969" Y="-25.9683671875" />
                  <Point X="3.114674560547" Y="-25.975390625" />
                  <Point X="3.086848144531" Y="-25.992283203125" />
                  <Point X="3.074122802734" Y="-26.00153125" />
                  <Point X="3.050373291016" Y="-26.022001953125" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="3.000408447266" Y="-26.08005859375" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606201172" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.869576171875" Y="-26.3573125" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607177734" Y="-26.51458984375" />
                  <Point X="2.864065429688" Y="-26.530126953125" />
                  <Point X="2.871796875" Y="-26.561748046875" />
                  <Point X="2.876786376953" Y="-26.576669921875" />
                  <Point X="2.889158691406" Y="-26.605482421875" />
                  <Point X="2.896541503906" Y="-26.619373046875" />
                  <Point X="2.932195068359" Y="-26.674828125" />
                  <Point X="2.997021728516" Y="-26.775662109375" />
                  <Point X="3.0017421875" Y="-26.782353515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486083984" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.831170898438" Y="-27.433546875" />
                  <Point X="4.087170898438" Y="-27.629982421875" />
                  <Point X="4.045498046875" Y="-27.697416015625" />
                  <Point X="4.001273681641" Y="-27.760251953125" />
                  <Point X="3.324927001953" Y="-27.369763671875" />
                  <Point X="2.848454589844" Y="-27.094671875" />
                  <Point X="2.841199951172" Y="-27.090890625" />
                  <Point X="2.815021240234" Y="-27.07951171875" />
                  <Point X="2.783116943359" Y="-27.069326171875" />
                  <Point X="2.771108642578" Y="-27.066337890625" />
                  <Point X="2.694433105469" Y="-27.052490234375" />
                  <Point X="2.555018310547" Y="-27.0273125" />
                  <Point X="2.539359375" Y="-27.02580859375" />
                  <Point X="2.508008789062" Y="-27.025404296875" />
                  <Point X="2.492317138672" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444846191406" Y="-27.03513671875" />
                  <Point X="2.415069091797" Y="-27.0449609375" />
                  <Point X="2.400590332031" Y="-27.051109375" />
                  <Point X="2.336891845703" Y="-27.0846328125" />
                  <Point X="2.221072265625" Y="-27.145587890625" />
                  <Point X="2.208969726562" Y="-27.153169921875" />
                  <Point X="2.186039306641" Y="-27.1700625" />
                  <Point X="2.175211425781" Y="-27.179373046875" />
                  <Point X="2.154251708984" Y="-27.20033203125" />
                  <Point X="2.144940673828" Y="-27.21116015625" />
                  <Point X="2.128046630859" Y="-27.234091796875" />
                  <Point X="2.120463623047" Y="-27.2461953125" />
                  <Point X="2.086939453125" Y="-27.30989453125" />
                  <Point X="2.025984619141" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.44019140625" />
                  <Point X="2.010012329102" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.01603527832" Y="-27.65681640625" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.569730712891" Y="-28.739923828125" />
                  <Point X="2.735892822266" Y="-29.027724609375" />
                  <Point X="2.723751953125" Y="-29.03608203125" />
                  <Point X="2.197507080078" Y="-28.350267578125" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828654663086" Y="-27.8701484375" />
                  <Point X="1.808836914062" Y="-27.8496328125" />
                  <Point X="1.783252929688" Y="-27.828005859375" />
                  <Point X="1.773297119141" Y="-27.820646484375" />
                  <Point X="1.697674316406" Y="-27.772029296875" />
                  <Point X="1.560173706055" Y="-27.68362890625" />
                  <Point X="1.546284912109" Y="-27.676248046875" />
                  <Point X="1.517471435547" Y="-27.663875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926025391" Y="-27.65115234375" />
                  <Point X="1.455384887695" Y="-27.6486953125" />
                  <Point X="1.424120727539" Y="-27.64637890625" />
                  <Point X="1.408397705078" Y="-27.64651953125" />
                  <Point X="1.325691162109" Y="-27.65412890625" />
                  <Point X="1.175310668945" Y="-27.667966796875" />
                  <Point X="1.161227783203" Y="-27.67033984375" />
                  <Point X="1.133575561523" Y="-27.677171875" />
                  <Point X="1.120006103516" Y="-27.681630859375" />
                  <Point X="1.092620727539" Y="-27.692974609375" />
                  <Point X="1.07987487793" Y="-27.699416015625" />
                  <Point X="1.055493041992" Y="-27.71413671875" />
                  <Point X="1.043857299805" Y="-27.722416015625" />
                  <Point X="0.979993530273" Y="-27.775517578125" />
                  <Point X="0.863873596191" Y="-27.872068359375" />
                  <Point X="0.852653625488" Y="-27.88308984375" />
                  <Point X="0.83218359375" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.763697021484" Y="-28.093484375" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091491699" Y="-29.15233984375" />
                  <Point X="0.797203430176" Y="-29.018404296875" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605224609" Y="-28.48012109375" />
                  <Point X="0.642143249512" Y="-28.453576171875" />
                  <Point X="0.626786010742" Y="-28.423814453125" />
                  <Point X="0.620406677246" Y="-28.413208984375" />
                  <Point X="0.562311157227" Y="-28.329505859375" />
                  <Point X="0.456679229736" Y="-28.17730859375" />
                  <Point X="0.446668457031" Y="-28.165169921875" />
                  <Point X="0.424785522461" Y="-28.142716796875" />
                  <Point X="0.412913360596" Y="-28.13240234375" />
                  <Point X="0.386658233643" Y="-28.113158203125" />
                  <Point X="0.373243835449" Y="-28.104939453125" />
                  <Point X="0.34524105835" Y="-28.090830078125" />
                  <Point X="0.330652679443" Y="-28.084939453125" />
                  <Point X="0.240753356934" Y="-28.0570390625" />
                  <Point X="0.07729372406" Y="-28.006306640625" />
                  <Point X="0.063380096436" Y="-28.003111328125" />
                  <Point X="0.03522794342" Y="-27.998841796875" />
                  <Point X="0.020989425659" Y="-27.997767578125" />
                  <Point X="-0.008651926041" Y="-27.997765625" />
                  <Point X="-0.022895795822" Y="-27.998837890625" />
                  <Point X="-0.051062362671" Y="-28.003107421875" />
                  <Point X="-0.064984909058" Y="-28.0063046875" />
                  <Point X="-0.154884384155" Y="-28.03420703125" />
                  <Point X="-0.318343841553" Y="-28.0849375" />
                  <Point X="-0.33292880249" Y="-28.090828125" />
                  <Point X="-0.360929077148" Y="-28.104935546875" />
                  <Point X="-0.374344512939" Y="-28.11315234375" />
                  <Point X="-0.400600250244" Y="-28.132396484375" />
                  <Point X="-0.412477142334" Y="-28.14271875" />
                  <Point X="-0.434361114502" Y="-28.16517578125" />
                  <Point X="-0.444368347168" Y="-28.177310546875" />
                  <Point X="-0.50246383667" Y="-28.261015625" />
                  <Point X="-0.608095947266" Y="-28.4132109375" />
                  <Point X="-0.612468994141" Y="-28.42012890625" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.901195739746" Y="-29.45245703125" />
                  <Point X="-0.985425048828" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.666128046827" Y="-28.960985227825" />
                  <Point X="2.68813112819" Y="-28.944999053468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.608275820626" Y="-28.885590872638" />
                  <Point X="2.640369434115" Y="-28.862273497561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550423594425" Y="-28.81019651745" />
                  <Point X="2.592607740039" Y="-28.779547941654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.998083443224" Y="-27.758410071212" />
                  <Point X="4.007264917632" Y="-27.751739339584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.492571368224" Y="-28.734802162262" />
                  <Point X="2.544846062449" Y="-28.696822373769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.908025007815" Y="-27.70641489668" />
                  <Point X="4.051207478848" Y="-27.602386742209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.434719142023" Y="-28.659407807075" />
                  <Point X="2.497084400014" Y="-28.614096794874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.817966572406" Y="-27.654419722147" />
                  <Point X="3.972601880191" Y="-27.54207059471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.376866915822" Y="-28.584013451887" />
                  <Point X="2.449322737579" Y="-28.531371215979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.727908136998" Y="-27.602424547615" />
                  <Point X="3.893996281534" Y="-27.481754447212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.319014689621" Y="-28.5086190967" />
                  <Point X="2.401561075144" Y="-28.448645637084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.637849701589" Y="-27.550429373082" />
                  <Point X="3.815390684422" Y="-27.42143829859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.26116246342" Y="-28.433224741512" />
                  <Point X="2.353799412709" Y="-28.365920058189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.54779126618" Y="-27.49843419855" />
                  <Point X="3.736785093465" Y="-27.361122145497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.203310237219" Y="-28.357830386324" />
                  <Point X="2.306037750274" Y="-28.283194479294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.457732830772" Y="-27.446439024017" />
                  <Point X="3.658179502508" Y="-27.300805992404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.145458122577" Y="-28.282435950084" />
                  <Point X="2.258276087839" Y="-28.200468900398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.367674395363" Y="-27.394443849485" />
                  <Point X="3.579573911551" Y="-27.240489839311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825501588397" Y="-29.124014049422" />
                  <Point X="0.829025282342" Y="-29.121453935915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.087606020374" Y="-28.207041504807" />
                  <Point X="2.210514425404" Y="-28.117743321503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.277616019647" Y="-27.342448631583" />
                  <Point X="3.500968320593" Y="-27.180173686218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.799164393638" Y="-29.02572268362" />
                  <Point X="0.814915412493" Y="-29.014278898563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029753918171" Y="-28.131647059529" />
                  <Point X="2.162752762969" Y="-28.035017742608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.187557697865" Y="-27.290453374496" />
                  <Point X="3.422362729636" Y="-27.119857533125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.772827304891" Y="-28.927431240796" />
                  <Point X="0.800805542644" Y="-28.90710386121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.971901815968" Y="-28.056252614252" />
                  <Point X="2.114991100534" Y="-27.952292163713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.097499376083" Y="-27.238458117409" />
                  <Point X="3.343757138679" Y="-27.059541380032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.698385795636" Y="-26.075346051097" />
                  <Point X="4.734708145924" Y="-26.048956318896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746490224671" Y="-28.829139791777" />
                  <Point X="0.786695672794" Y="-28.799928823858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.914049713765" Y="-27.980858168974" />
                  <Point X="2.067477122263" Y="-27.869386631739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007441054301" Y="-27.186462860322" />
                  <Point X="3.265151547721" Y="-26.999225226939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.56155595138" Y="-26.057332294188" />
                  <Point X="4.764842144224" Y="-25.90963622973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.720153144452" Y="-28.730848342757" />
                  <Point X="0.772585802945" Y="-28.692753786506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.856197611561" Y="-27.905463723697" />
                  <Point X="2.037191824786" Y="-27.773963730466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.917382732519" Y="-27.134467603235" />
                  <Point X="3.186545956764" Y="-26.938909073846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.424726107123" Y="-26.039318537278" />
                  <Point X="4.784723740727" Y="-25.777764946483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.693816064233" Y="-28.632556893738" />
                  <Point X="0.758475933096" Y="-28.585578749153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.791541646279" Y="-27.835012574301" />
                  <Point X="2.018444712826" Y="-27.67015784672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.825268701111" Y="-27.083965906616" />
                  <Point X="3.107940365807" Y="-26.878592920753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.287896262867" Y="-26.021304780368" />
                  <Point X="4.667706389883" Y="-25.745356570524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.01978620375" Y="-29.760135359651" />
                  <Point X="-0.974899634873" Y="-29.727523358426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.667478984013" Y="-28.534265444718" />
                  <Point X="0.744366063247" Y="-28.478403711801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.707718930216" Y="-27.778486884472" />
                  <Point X="2.000787044145" Y="-27.5655604361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.704472136168" Y="-27.054303290422" />
                  <Point X="3.031823866055" Y="-26.816468337042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.15106641861" Y="-26.003291023458" />
                  <Point X="4.549629284059" Y="-25.713718151626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.141191714087" Y="-29.730915168183" />
                  <Point X="-0.935829156712" Y="-29.58171053659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.635264031232" Y="-28.440244520089" />
                  <Point X="0.730256193398" Y="-28.371228674448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.621971728085" Y="-27.723359415615" />
                  <Point X="2.023457976975" Y="-27.431662581387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.575025062709" Y="-27.030925636554" />
                  <Point X="2.974525940247" Y="-26.740671259047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.014236574354" Y="-25.985277266549" />
                  <Point X="4.431552276065" Y="-25.682079661651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.124096858951" Y="-29.601068571054" />
                  <Point X="-0.896758685726" Y="-29.435897719966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.583681060186" Y="-28.360295284412" />
                  <Point X="0.728679210462" Y="-28.254947961754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.533027202793" Y="-27.670554938011" />
                  <Point X="2.123267134058" Y="-27.241720526219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.346281045456" Y="-27.079691435252" />
                  <Point X="2.923067639973" Y="-26.660631444752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.877406937226" Y="-25.967263359151" />
                  <Point X="4.313475286687" Y="-25.65044115815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.125872140756" Y="-29.484931930922" />
                  <Point X="-0.857688270742" Y="-29.290084944031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.52950138041" Y="-28.28223266806" />
                  <Point X="0.75889725278" Y="-28.115566811035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.403918192913" Y="-27.646931666574" />
                  <Point X="2.87683886582" Y="-26.576792157329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.740577393337" Y="-25.949249384011" />
                  <Point X="4.195398297309" Y="-25.618802654649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.146280319763" Y="-29.382332883027" />
                  <Point X="-0.818617855758" Y="-29.144272168096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.475322113331" Y="-28.204169751867" />
                  <Point X="0.794790224879" Y="-27.972062582486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.218857530672" Y="-27.66395965009" />
                  <Point X="2.859388697985" Y="-26.47204398852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603747849448" Y="-25.931235408872" />
                  <Point X="4.077321307932" Y="-25.587164151149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.166688533693" Y="-29.279733860505" />
                  <Point X="-0.779547440774" Y="-28.998459392161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.412696550246" Y="-28.132243428926" />
                  <Point X="2.870571793938" Y="-26.346492535852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.466918305559" Y="-25.913221433732" />
                  <Point X="3.959244318554" Y="-25.555525647648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.188298727382" Y="-29.178008127396" />
                  <Point X="-0.74047702579" Y="-28.852646616226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.320512664784" Y="-28.081792484249" />
                  <Point X="2.905282858821" Y="-26.20384701316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.285367464421" Y="-25.927699382951" />
                  <Point X="3.841167329176" Y="-25.523887144147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.23240682907" Y="-29.092628081239" />
                  <Point X="-0.701406610806" Y="-28.706833840291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.207264882692" Y="-28.046645356278" />
                  <Point X="3.723090339799" Y="-25.492248640646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.300794623096" Y="-29.024888264133" />
                  <Point X="-0.662336195822" Y="-28.561021064356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.094018266566" Y="-28.011497381184" />
                  <Point X="3.618419230927" Y="-25.450870194833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.374024992984" Y="-28.960666784336" />
                  <Point X="-0.595461693116" Y="-28.395007436238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.05836620904" Y="-28.004784725457" />
                  <Point X="3.529404078211" Y="-25.398117031055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.447255441529" Y="-28.896445361687" />
                  <Point X="-0.418871971087" Y="-28.149281035313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.350837469409" Y="-28.099851076472" />
                  <Point X="3.460316251405" Y="-25.330885817535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.524337490804" Y="-28.835022290768" />
                  <Point X="3.403527507457" Y="-25.254718797263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.709217066997" Y="-24.306079803884" />
                  <Point X="4.779081883654" Y="-24.255320043371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.633219446753" Y="-28.796703204435" />
                  <Point X="3.368417200578" Y="-25.162801470519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.453157072874" Y="-24.374691821473" />
                  <Point X="4.762657123933" Y="-24.149826871959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.781094387052" Y="-28.786714179526" />
                  <Point X="3.35008369977" Y="-25.05869508068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.197097078751" Y="-24.443303839061" />
                  <Point X="4.743143446553" Y="-24.046577930591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.929343711099" Y="-28.776997160332" />
                  <Point X="3.362145524276" Y="-24.932505194349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.941038895786" Y="-24.511914540767" />
                  <Point X="4.718852748054" Y="-23.946799698222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.567091455365" Y="-29.122921560818" />
                  <Point X="3.438264969278" Y="-24.759774722485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.684123213557" Y="-24.581148252155" />
                  <Point X="4.622290737578" Y="-23.899529647561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.654350724466" Y="-29.06889267292" />
                  <Point X="4.424898464524" Y="-23.925517070772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.741609993566" Y="-29.014863785021" />
                  <Point X="4.227506466182" Y="-23.951504294392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.820679225478" Y="-28.9548844868" />
                  <Point X="4.03011448198" Y="-23.97749150774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.899146105571" Y="-28.894467554364" />
                  <Point X="3.832722497777" Y="-24.003478721088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.977613154801" Y="-28.834050744815" />
                  <Point X="3.635330513574" Y="-24.029465934437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868915703793" Y="-28.637650966109" />
                  <Point X="3.448211983564" Y="-24.047989046405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752132767967" Y="-28.435376738824" />
                  <Point X="3.314687539858" Y="-24.027573775423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.635349832142" Y="-28.233102511539" />
                  <Point X="3.215374382387" Y="-23.982302550053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.518567045016" Y="-28.030828392291" />
                  <Point X="3.134042433689" Y="-23.923967211806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.401784308634" Y="-27.82855430991" />
                  <Point X="3.07744057059" Y="-23.847664414649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316264924731" Y="-27.648994382673" />
                  <Point X="3.031930287719" Y="-23.763303112754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.321429972434" Y="-27.535320551626" />
                  <Point X="3.007970851845" Y="-23.663284204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.366462329574" Y="-27.450612016362" />
                  <Point X="3.005913580214" Y="-23.54735244147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.439070787447" Y="-27.385938691037" />
                  <Point X="3.062454733838" Y="-23.388846430917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.548916094199" Y="-27.348319520032" />
                  <Point X="4.126069667039" Y="-22.498658490662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.00802001717" Y="-27.564451586982" />
                  <Point X="4.076712020712" Y="-22.417092461939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.793235066826" Y="-28.017517256324" />
                  <Point X="4.025293207182" Y="-22.337023958845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.850787329329" Y="-27.941904964753" />
                  <Point X="3.969806490523" Y="-22.259910960375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.908339591832" Y="-27.866292673182" />
                  <Point X="3.570377368256" Y="-22.432686746763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.965891854335" Y="-27.790680381611" />
                  <Point X="2.783303115722" Y="-22.887103206065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.020510531369" Y="-27.712936715437" />
                  <Point X="2.432446189333" Y="-23.024589226469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.069369580264" Y="-27.631008434475" />
                  <Point X="2.29661046335" Y="-23.005853200356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.118228447291" Y="-27.549080021378" />
                  <Point X="2.202888242055" Y="-22.956519922084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167086973446" Y="-27.467151360622" />
                  <Point X="2.126085776038" Y="-22.894893722038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.979845090028" Y="-26.487143183428" />
                  <Point X="2.069890528941" Y="-22.818295501063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948979121631" Y="-26.347291286857" />
                  <Point X="2.028643302122" Y="-22.730836907647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.972554353282" Y="-26.246993237396" />
                  <Point X="2.013135651214" Y="-22.624677417679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.031860367452" Y="-26.172655120994" />
                  <Point X="2.032213212727" Y="-22.493390300046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.119666122274" Y="-26.119023278214" />
                  <Point X="2.114521264026" Y="-22.316163542518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.25782323144" Y="-26.101973835707" />
                  <Point X="2.231304551452" Y="-22.11388905978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.455215155468" Y="-26.127961005336" />
                  <Point X="2.348087838878" Y="-21.911614577042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.652607079496" Y="-26.153948174964" />
                  <Point X="2.464871333751" Y="-21.709339943585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.849999003524" Y="-26.179935344593" />
                  <Point X="2.58165487937" Y="-21.50706527326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.047390927552" Y="-26.205922514221" />
                  <Point X="2.698438424988" Y="-21.304790602935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.24478285158" Y="-26.23190968385" />
                  <Point X="2.815221970606" Y="-21.102515932609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.442173507394" Y="-26.257895932068" />
                  <Point X="2.737893487034" Y="-21.041271906688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.639563736244" Y="-26.283881870077" />
                  <Point X="2.656215834258" Y="-20.983187737155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.68348203756" Y="-26.198363925879" />
                  <Point X="2.569146513571" Y="-20.929020843656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708781355221" Y="-26.099318498226" />
                  <Point X="-3.693779290021" Y="-25.361876331846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.298278835585" Y="-25.074528431852" />
                  <Point X="2.479449679167" Y="-20.876762950615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.734080672881" Y="-26.000273070573" />
                  <Point X="-3.94983807393" Y="-25.430487470162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.304325174369" Y="-24.961494896255" />
                  <Point X="2.389752844764" Y="-20.824505057574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.751958107329" Y="-25.895835329129" />
                  <Point X="-4.205896550142" Y="-25.499098384924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.342149064607" Y="-24.871549103225" />
                  <Point X="2.300055446039" Y="-20.772247574537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.767171980793" Y="-25.789462397353" />
                  <Point X="-4.461954873631" Y="-25.567709188726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.40892700471" Y="-24.80263965878" />
                  <Point X="2.20650250505" Y="-20.722791306923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782385813965" Y="-25.683089436305" />
                  <Point X="-4.71801319712" Y="-25.636319992528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501704400703" Y="-24.752619924744" />
                  <Point X="2.10817272691" Y="-20.676805614649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.619309725043" Y="-24.720638736534" />
                  <Point X="2.009842948769" Y="-20.630819922374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737386631899" Y="-24.689000173078" />
                  <Point X="1.91151196128" Y="-20.584835108744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.855463538756" Y="-24.657361609622" />
                  <Point X="1.808838237909" Y="-20.542005477419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.973540445612" Y="-24.625723046166" />
                  <Point X="1.701033770226" Y="-20.502903550037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091617352469" Y="-24.594084482711" />
                  <Point X="1.593229302544" Y="-20.463801622655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.209694259326" Y="-24.562445919255" />
                  <Point X="1.485424269639" Y="-20.42470010593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.327771166182" Y="-24.530807355799" />
                  <Point X="1.368097019126" Y="-20.39251688526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.445848073039" Y="-24.499168792343" />
                  <Point X="1.24678571544" Y="-20.363228248653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.563924887974" Y="-24.467530162103" />
                  <Point X="-3.57561695451" Y="-23.749482417676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514706453395" Y="-23.705228348213" />
                  <Point X="1.125474411753" Y="-20.333939612047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.68200168448" Y="-24.435891518472" />
                  <Point X="-3.795836722038" Y="-23.79205498643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.426274936454" Y="-23.523552632478" />
                  <Point X="1.004163066052" Y="-20.304651005966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.783032521643" Y="-24.391868260449" />
                  <Point X="-3.932666238004" Y="-23.774040991003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.431546454561" Y="-23.409956156707" />
                  <Point X="0.002622010163" Y="-20.91488671875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.235262313289" Y="-20.745863644801" />
                  <Point X="0.882851636046" Y="-20.275362461136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763563422024" Y="-24.260296673731" />
                  <Point X="-4.069495753969" Y="-23.756026995576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.471364552036" Y="-23.321459240045" />
                  <Point X="-0.113745169369" Y="-20.882005965682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.275179763448" Y="-20.599435461788" />
                  <Point X="0.75063447581" Y="-20.253997393117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744094322405" Y="-24.128725087013" />
                  <Point X="-4.20632536356" Y="-23.738013068171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.527814089077" Y="-23.245045771528" />
                  <Point X="-0.185420261548" Y="-20.816654510486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.314250138057" Y="-20.453622715188" />
                  <Point X="0.609372289356" Y="-20.239203921312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.717683730599" Y="-23.992110211014" />
                  <Point X="-4.343155131403" Y="-23.719999255744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604845234957" Y="-23.183585717129" />
                  <Point X="-0.227865427144" Y="-20.730066270537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.353320512665" Y="-20.307809968587" />
                  <Point X="0.468110102903" Y="-20.224410449507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678062817489" Y="-23.845897474779" />
                  <Point X="-4.479984899246" Y="-23.701985443317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683450829955" Y="-23.123269566971" />
                  <Point X="-0.254202408899" Y="-20.631774749979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.638442244396" Y="-23.69968498558" />
                  <Point X="-4.61681466709" Y="-23.683971630889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.762056424953" Y="-23.062953416814" />
                  <Point X="-0.280539390654" Y="-20.53348322942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.840662019951" Y="-23.002637266657" />
                  <Point X="-0.306876444005" Y="-20.43519176088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.919267614949" Y="-22.9423211165" />
                  <Point X="-2.993995117585" Y="-22.270071297171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808955163276" Y="-22.135631900985" />
                  <Point X="-0.333213574929" Y="-20.336900348699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.997873209948" Y="-22.882004966343" />
                  <Point X="-3.15405254226" Y="-22.268933365258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748642471017" Y="-21.974385707218" />
                  <Point X="-0.359550705852" Y="-20.238608936518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.076478804946" Y="-22.821688816185" />
                  <Point X="-3.26440520141" Y="-22.231682807346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.757257245052" Y="-21.863218249061" />
                  <Point X="-0.516390460338" Y="-20.235133230372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.155084564583" Y="-22.761372785646" />
                  <Point X="-3.354463636936" Y="-22.179687632899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.788818342463" Y="-21.768722270698" />
                  <Point X="-0.709049022848" Y="-20.257681411557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.218416697992" Y="-22.689959816094" />
                  <Point X="-3.444522072461" Y="-22.127692458451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.83658008268" Y="-21.685996748315" />
                  <Point X="-1.851652423469" Y="-20.970404916889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.486805052554" Y="-20.705327785689" />
                  <Point X="-0.901710945528" Y="-20.280232034049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.0994065036" Y="-22.48606739074" />
                  <Point X="-3.53458044768" Y="-22.075697240188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884341822897" Y="-21.603271225932" />
                  <Point X="-1.963437995752" Y="-20.934195431308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462681522267" Y="-20.570374557147" />
                  <Point X="-1.154109192779" Y="-20.346183636809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963628042926" Y="-22.269992106811" />
                  <Point X="-3.624638771535" Y="-22.023701984608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.932103563114" Y="-21.520545703549" />
                  <Point X="-2.056112761214" Y="-20.884101131826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475943386613" Y="-20.462583407733" />
                  <Point X="-1.417288385895" Y="-20.419968055231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.753497220213" Y="-21.999896669802" />
                  <Point X="-3.714697095391" Y="-21.971706729027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.979865303331" Y="-21.437820181165" />
                  <Point X="-2.126467384098" Y="-20.817790299531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027627043548" Y="-21.355094658782" />
                  <Point X="-2.184319811631" Y="-20.74239609062" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.61367755127" Y="-29.067580078125" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.521544921875" />
                  <Point X="0.406223327637" Y="-28.437841796875" />
                  <Point X="0.300591186523" Y="-28.28564453125" />
                  <Point X="0.274335906982" Y="-28.266400390625" />
                  <Point X="0.184436386108" Y="-28.2385" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.098563964844" Y="-28.21566796875" />
                  <Point X="-0.2620234375" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.346374664307" Y="-28.36934765625" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.71766986084" Y="-29.5016328125" />
                  <Point X="-0.84774420166" Y="-29.987078125" />
                  <Point X="-0.908339111328" Y="-29.97531640625" />
                  <Point X="-1.100258789062" Y="-29.9380625" />
                  <Point X="-1.203709228516" Y="-29.911447265625" />
                  <Point X="-1.35158972168" Y="-29.8733984375" />
                  <Point X="-1.326260375977" Y="-29.68100390625" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.33116015625" Y="-29.426787109375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282714844" Y="-29.20262890625" />
                  <Point X="-1.46905090332" Y="-29.13004296875" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240600586" Y="-28.985763671875" />
                  <Point X="-1.759092407227" Y="-28.978564453125" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.081412841797" Y="-29.034951171875" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.404829589844" Y="-29.34638671875" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.574537597656" Y="-29.341783203125" />
                  <Point X="-2.855839355469" Y="-29.167607421875" />
                  <Point X="-2.999101318359" Y="-29.05730078125" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.770182128906" Y="-28.086638671875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499763183594" Y="-27.59758984375" />
                  <Point X="-2.513983886719" Y="-27.568759765625" />
                  <Point X="-2.531327392578" Y="-27.551416015625" />
                  <Point X="-2.560156738281" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.423564453125" Y="-28.0237578125" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.939464355469" Y="-28.139107421875" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.264414550781" Y="-27.674900390625" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.6259609375" Y="-26.77778515625" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140325683594" Y="-26.33459765625" />
                  <Point X="-3.161156982422" Y="-26.310640625" />
                  <Point X="-3.187643798828" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.269700195312" Y="-26.426830078125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.840473632812" Y="-26.3514765625" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.954567871094" Y="-25.821189453125" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.083949707031" Y="-25.26971875" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.532047363281" Y="-25.114591796875" />
                  <Point X="-3.514142089844" Y="-25.1021640625" />
                  <Point X="-3.50232421875" Y="-25.090646484375" />
                  <Point X="-3.491616455078" Y="-25.06533203125" />
                  <Point X="-3.485647949219" Y="-25.0461015625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.488930419922" Y="-25.0058828125" />
                  <Point X="-3.494898925781" Y="-24.98665234375" />
                  <Point X="-3.50232421875" Y="-24.9719140625" />
                  <Point X="-3.523989746094" Y="-24.9535625" />
                  <Point X="-3.541895019531" Y="-24.941134765625" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.514744140625" Y="-24.677412109375" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.973663085938" Y="-24.3821484375" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.862942382812" Y="-23.80171484375" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.130664550781" Y="-23.556333984375" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731703857422" Y="-23.6041328125" />
                  <Point X="-3.709907958984" Y="-23.59726171875" />
                  <Point X="-3.670277832031" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.573943359375" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.630374267578" Y="-23.5351015625" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.626868652344" Y="-23.434216796875" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968017578" Y="-23.380779296875" />
                  <Point X="-4.209066894531" Y="-22.95944140625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.377681152344" Y="-22.58591015625" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.015110351562" Y="-22.026740234375" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.390944335938" Y="-21.939234375" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138514160156" Y="-22.07956640625" />
                  <Point X="-3.108158691406" Y="-22.08222265625" />
                  <Point X="-3.05296484375" Y="-22.08705078125" />
                  <Point X="-3.031506835938" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.991706298828" Y="-22.051048828125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.940729980469" Y="-21.9418046875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.195389648438" Y="-21.444521484375" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.131982421875" Y="-21.116326171875" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.524656738281" Y="-20.698875" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.038783935547" Y="-20.619953125" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951247558594" Y="-20.726337890625" />
                  <Point X="-1.917462036133" Y="-20.74392578125" />
                  <Point X="-1.85603137207" Y="-20.77590625" />
                  <Point X="-1.835124389648" Y="-20.781509765625" />
                  <Point X="-1.81380847168" Y="-20.77775" />
                  <Point X="-1.778618530273" Y="-20.763173828125" />
                  <Point X="-1.714634399414" Y="-20.736669921875" />
                  <Point X="-1.696905029297" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.674629638672" Y="-20.669185546875" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.679580566406" Y="-20.371453125" />
                  <Point X="-1.689137573242" Y="-20.298859375" />
                  <Point X="-1.458870727539" Y="-20.23430078125" />
                  <Point X="-0.968083190918" Y="-20.096703125" />
                  <Point X="-0.691424438477" Y="-20.064322265625" />
                  <Point X="-0.224199996948" Y="-20.009640625" />
                  <Point X="-0.110709388733" Y="-20.433193359375" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282131195" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594051361" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.179127685547" Y="-20.223802734375" />
                  <Point X="0.236648376465" Y="-20.0091328125" />
                  <Point X="0.431685913086" Y="-20.02955859375" />
                  <Point X="0.860210021973" Y="-20.074435546875" />
                  <Point X="1.089102905273" Y="-20.129697265625" />
                  <Point X="1.508456176758" Y="-20.230943359375" />
                  <Point X="1.657461914062" Y="-20.28498828125" />
                  <Point X="1.931043334961" Y="-20.38421875" />
                  <Point X="2.075100585938" Y="-20.451587890625" />
                  <Point X="2.338683837891" Y="-20.574857421875" />
                  <Point X="2.477877197266" Y="-20.655951171875" />
                  <Point X="2.73253515625" Y="-20.80431640625" />
                  <Point X="2.863783691406" Y="-20.89765234375" />
                  <Point X="3.068740722656" Y="-21.043408203125" />
                  <Point X="2.535585449219" Y="-21.966859375" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.217233886719" Y="-22.536974609375" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.205015136719" Y="-22.63230859375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.233925048828" Y="-22.72165234375" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.274939941406" Y="-22.775796875" />
                  <Point X="2.297403808594" Y="-22.791041015625" />
                  <Point X="2.338248535156" Y="-22.818755859375" />
                  <Point X="2.360335449219" Y="-22.827021484375" />
                  <Point X="2.384969482422" Y="-22.829990234375" />
                  <Point X="2.429760253906" Y="-22.835392578125" />
                  <Point X="2.448664306641" Y="-22.8340546875" />
                  <Point X="2.47715234375" Y="-22.826435546875" />
                  <Point X="2.528950683594" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.503873779297" Y="-22.2516875" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.051544433594" Y="-22.04819921875" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.275767578125" Y="-22.379044921875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.691140625" Y="-23.098048828125" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.279371582031" Y="-23.41616796875" />
                  <Point X="3.258868652344" Y="-23.442916015625" />
                  <Point X="3.221589355469" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.205481933594" Y="-23.53580859375" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.197049072266" Y="-23.639419921875" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.232698730469" Y="-23.737962890625" />
                  <Point X="3.263704101562" Y="-23.78508984375" />
                  <Point X="3.280947509766" Y="-23.8011796875" />
                  <Point X="3.305658447266" Y="-23.81508984375" />
                  <Point X="3.350589599609" Y="-23.8403828125" />
                  <Point X="3.368566650391" Y="-23.846380859375" />
                  <Point X="3.401977783203" Y="-23.850796875" />
                  <Point X="3.462727294922" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.390437011719" Y="-23.7384140625" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.874314453125" Y="-23.7821328125" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.962248046875" Y="-24.196724609375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.202469726562" Y="-24.638568359375" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087402344" Y="-24.767181640625" />
                  <Point X="3.696261962891" Y="-24.786154296875" />
                  <Point X="3.636577392578" Y="-24.82065234375" />
                  <Point X="3.622265136719" Y="-24.833072265625" />
                  <Point X="3.602570068359" Y="-24.858169921875" />
                  <Point X="3.566759277344" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.550419921875" Y="-24.959546875" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538482910156" Y="-25.040685546875" />
                  <Point X="3.545048095703" Y="-25.07496484375" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566759277344" Y="-25.158759765625" />
                  <Point X="3.586454345703" Y="-25.183857421875" />
                  <Point X="3.622265136719" Y="-25.22948828125" />
                  <Point X="3.636576171875" Y="-25.241908203125" />
                  <Point X="3.669401611328" Y="-25.260880859375" />
                  <Point X="3.729086181641" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.579928222656" Y="-25.5251328125" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.984654296875" Y="-25.726142578125" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.918888183594" Y="-26.09586328125" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.946962890625" Y="-26.168060546875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.3948359375" Y="-26.098341796875" />
                  <Point X="3.330411376953" Y="-26.11234375" />
                  <Point X="3.213271728516" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.146504638672" Y="-26.20153125" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.058776855469" Y="-26.37472265625" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360351562" Y="-26.516623046875" />
                  <Point X="3.092013916016" Y="-26.572078125" />
                  <Point X="3.156840576172" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.946835449219" Y="-27.28280859375" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.306240234375" Y="-27.6369140625" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.143033691406" Y="-27.888953125" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="3.229927001953" Y="-27.53430859375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340576172" Y="-27.253314453125" />
                  <Point X="2.660665039062" Y="-27.239466796875" />
                  <Point X="2.521250244141" Y="-27.2142890625" />
                  <Point X="2.489077636719" Y="-27.21924609375" />
                  <Point X="2.425379150391" Y="-27.25276953125" />
                  <Point X="2.309559570312" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.255075683594" Y="-27.3983828125" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.203010498047" Y="-27.62305078125" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.734275634766" Y="-28.644923828125" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.956458984375" Y="-29.103669921875" />
                  <Point X="2.835296142578" Y="-29.190212890625" />
                  <Point X="2.766987548828" Y="-29.234427734375" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="2.046770019531" Y="-28.465931640625" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549316406" Y="-27.980466796875" />
                  <Point X="1.594926513672" Y="-27.931849609375" />
                  <Point X="1.45742590332" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.343098388672" Y="-27.843328125" />
                  <Point X="1.192718017578" Y="-27.857166015625" />
                  <Point X="1.165332641602" Y="-27.868509765625" />
                  <Point X="1.10146887207" Y="-27.921611328125" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.949361877441" Y="-28.13383984375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.055677978516" Y="-29.387404296875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.108973266602" Y="-29.93812109375" />
                  <Point X="0.994366638184" Y="-29.9632421875" />
                  <Point X="0.93123828125" Y="-29.9747109375" />
                  <Point X="0.860200317383" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#152" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.068569105749" Y="4.611027873117" Z="0.9" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.9" />
                  <Point X="-0.703392512309" Y="5.01661766568" Z="0.9" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.9" />
                  <Point X="-1.478524334475" Y="4.845123510723" Z="0.9" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.9" />
                  <Point X="-1.735308866933" Y="4.653301920354" Z="0.9" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.9" />
                  <Point X="-1.728471475712" Y="4.377130412799" Z="0.9" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.9" />
                  <Point X="-1.803909102593" Y="4.314300854688" Z="0.9" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.9" />
                  <Point X="-1.90052958834" Y="4.331703520828" Z="0.9" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.9" />
                  <Point X="-2.005272312325" Y="4.441764470535" Z="0.9" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.9" />
                  <Point X="-2.555095750193" Y="4.376112716961" Z="0.9" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.9" />
                  <Point X="-3.168560409571" Y="3.954634589716" Z="0.9" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.9" />
                  <Point X="-3.244846783068" Y="3.561759119031" Z="0.9" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.9" />
                  <Point X="-2.99669580474" Y="3.085119094084" Z="0.9" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.9" />
                  <Point X="-3.033216977349" Y="3.015586590537" Z="0.9" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.9" />
                  <Point X="-3.109957371523" Y="2.998868894433" Z="0.9" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.9" />
                  <Point X="-3.372100229266" Y="3.135347165678" Z="0.9" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.9" />
                  <Point X="-4.060729761585" Y="3.035242731352" Z="0.9" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.9" />
                  <Point X="-4.427019268861" Y="2.470576251348" Z="0.9" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.9" />
                  <Point X="-4.245660618956" Y="2.032172041132" Z="0.9" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.9" />
                  <Point X="-3.677375048065" Y="1.573975767677" Z="0.9" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.9" />
                  <Point X="-3.682724226861" Y="1.515314053241" Z="0.9" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.9" />
                  <Point X="-3.731100181305" Y="1.481704745484" Z="0.9" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.9" />
                  <Point X="-4.130293955744" Y="1.524517923982" Z="0.9" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.9" />
                  <Point X="-4.917358356369" Y="1.242644757719" Z="0.9" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.9" />
                  <Point X="-5.029280685329" Y="0.656451415839" Z="0.9" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.9" />
                  <Point X="-4.533841191295" Y="0.305571561024" Z="0.9" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.9" />
                  <Point X="-3.558655432327" Y="0.036641729019" Z="0.9" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.9" />
                  <Point X="-3.542839322415" Y="0.010576421727" Z="0.9" />
                  <Point X="-3.539556741714" Y="0" Z="0.9" />
                  <Point X="-3.54552527872" Y="-0.019230529334" Z="0.9" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.9" />
                  <Point X="-3.566713162796" Y="-0.042234254115" Z="0.9" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.9" />
                  <Point X="-4.103046597478" Y="-0.190140495732" Z="0.9" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.9" />
                  <Point X="-5.010219768224" Y="-0.796987875213" Z="0.9" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.9" />
                  <Point X="-4.895074574115" Y="-1.332571239631" Z="0.9" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.9" />
                  <Point X="-4.269329603626" Y="-1.445120879055" Z="0.9" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.9" />
                  <Point X="-3.202073139283" Y="-1.316919236061" Z="0.9" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.9" />
                  <Point X="-3.197647010202" Y="-1.341641890819" Z="0.9" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.9" />
                  <Point X="-3.662554832155" Y="-1.70683576165" Z="0.9" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.9" />
                  <Point X="-4.313513915937" Y="-2.669228282569" Z="0.9" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.9" />
                  <Point X="-3.985406299781" Y="-3.138109415659" Z="0.9" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.9" />
                  <Point X="-3.404720379423" Y="-3.035777614465" Z="0.9" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.9" />
                  <Point X="-2.561646761156" Y="-2.566683707976" Z="0.9" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.9" />
                  <Point X="-2.819639398704" Y="-3.030358003069" Z="0.9" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.9" />
                  <Point X="-3.035761135808" Y="-4.065637063645" Z="0.9" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.9" />
                  <Point X="-2.607015380794" Y="-4.353013629449" Z="0.9" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.9" />
                  <Point X="-2.371317931202" Y="-4.345544451312" Z="0.9" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.9" />
                  <Point X="-2.059790356014" Y="-4.045245761269" Z="0.9" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.9" />
                  <Point X="-1.768518587409" Y="-3.997175863448" Z="0.9" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.9" />
                  <Point X="-1.508174100196" Y="-4.136354713063" Z="0.9" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.9" />
                  <Point X="-1.3863555314" Y="-4.405260181043" Z="0.9" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.9" />
                  <Point X="-1.381988655126" Y="-4.643196438317" Z="0.9" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.9" />
                  <Point X="-1.222324291237" Y="-4.928587937318" Z="0.9" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.9" />
                  <Point X="-0.924022823676" Y="-4.993119120482" Z="0.9" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.9" />
                  <Point X="-0.675529531551" Y="-4.483294627226" Z="0.9" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.9" />
                  <Point X="-0.311454696303" Y="-3.366577384944" Z="0.9" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.9" />
                  <Point X="-0.089899583024" Y="-3.232140881405" Z="0.9" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.9" />
                  <Point X="0.163459496337" Y="-3.254971163883" Z="0.9" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.9" />
                  <Point X="0.358991163129" Y="-3.435068370823" Z="0.9" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.9" />
                  <Point X="0.55922532885" Y="-4.04924140259" Z="0.9" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.9" />
                  <Point X="0.934019250167" Y="-4.992626189725" Z="0.9" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.9" />
                  <Point X="1.113523084409" Y="-4.955681010205" Z="0.9" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.9" />
                  <Point X="1.099094115255" Y="-4.349599038199" Z="0.9" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.9" />
                  <Point X="0.992065073573" Y="-3.113177727123" Z="0.9" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.9" />
                  <Point X="1.127279993919" Y="-2.928776059759" Z="0.9" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.9" />
                  <Point X="1.341524134323" Y="-2.861837300187" Z="0.9" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.9" />
                  <Point X="1.56173115445" Y="-2.942626833519" Z="0.9" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.9" />
                  <Point X="2.000946365642" Y="-3.465088103477" Z="0.9" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.9" />
                  <Point X="2.788001075585" Y="-4.24512276054" Z="0.9" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.9" />
                  <Point X="2.979364785364" Y="-4.113077098128" Z="0.9" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.9" />
                  <Point X="2.771420971024" Y="-3.588642489748" Z="0.9" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.9" />
                  <Point X="2.246058659613" Y="-2.582883706757" Z="0.9" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.9" />
                  <Point X="2.293166116154" Y="-2.390388691565" Z="0.9" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.9" />
                  <Point X="2.442509686807" Y="-2.265735193814" Z="0.9" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.9" />
                  <Point X="2.645623096755" Y="-2.257389349878" Z="0.9" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.9" />
                  <Point X="3.198771107359" Y="-2.54632852925" Z="0.9" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.9" />
                  <Point X="4.177766227157" Y="-2.886450691191" Z="0.9" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.9" />
                  <Point X="4.342617910188" Y="-2.631919035607" Z="0.9" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.9" />
                  <Point X="3.97111745246" Y="-2.211860967753" Z="0.9" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.9" />
                  <Point X="3.127916009487" Y="-1.513759178623" Z="0.9" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.9" />
                  <Point X="3.102410342249" Y="-1.348023539944" Z="0.9" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.9" />
                  <Point X="3.178794961537" Y="-1.202217596435" Z="0.9" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.9" />
                  <Point X="3.334875258779" Y="-1.129923378055" Z="0.9" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.9" />
                  <Point X="3.93428056891" Y="-1.186351965634" Z="0.9" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.9" />
                  <Point X="4.961479446792" Y="-1.07570695375" Z="0.9" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.9" />
                  <Point X="5.027939956563" Y="-0.702315596748" Z="0.9" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.9" />
                  <Point X="4.58671304165" Y="-0.445555782837" Z="0.9" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.9" />
                  <Point X="3.688268098709" Y="-0.186311867707" Z="0.9" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.9" />
                  <Point X="3.61963195412" Y="-0.121706883039" Z="0.9" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.9" />
                  <Point X="3.587999803519" Y="-0.034280383793" Z="0.9" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.9" />
                  <Point X="3.593371646906" Y="0.062330147429" Z="0.9" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.9" />
                  <Point X="3.635747484281" Y="0.142241855536" Z="0.9" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.9" />
                  <Point X="3.715127315644" Y="0.201836976535" Z="0.9" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.9" />
                  <Point X="4.209254775657" Y="0.344416149932" Z="0.9" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.9" />
                  <Point X="5.005497020954" Y="0.842248090258" Z="0.9" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.9" />
                  <Point X="4.91673954577" Y="1.260974601134" Z="0.9" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.9" />
                  <Point X="4.377754717816" Y="1.342437879002" Z="0.9" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.9" />
                  <Point X="3.402371761167" Y="1.230052889837" Z="0.9" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.9" />
                  <Point X="3.324029696358" Y="1.25976079707" Z="0.9" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.9" />
                  <Point X="3.268313292827" Y="1.32079766212" Z="0.9" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.9" />
                  <Point X="3.239861523958" Y="1.401964057078" Z="0.9" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.9" />
                  <Point X="3.247478626443" Y="1.482004334961" Z="0.9" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.9" />
                  <Point X="3.29239526927" Y="1.55794746544" Z="0.9" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.9" />
                  <Point X="3.715422988955" Y="1.893563255489" Z="0.9" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.9" />
                  <Point X="4.31238897835" Y="2.678122845625" Z="0.9" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.9" />
                  <Point X="4.085973822133" Y="3.012284977761" Z="0.9" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.9" />
                  <Point X="3.472717750632" Y="2.822894571458" Z="0.9" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.9" />
                  <Point X="2.458079688258" Y="2.253147245835" Z="0.9" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.9" />
                  <Point X="2.384800863451" Y="2.250930114739" Z="0.9" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.9" />
                  <Point X="2.319321949117" Y="2.281615416156" Z="0.9" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.9" />
                  <Point X="2.269143184079" Y="2.337702911265" Z="0.9" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.9" />
                  <Point X="2.24849958802" Y="2.404957580382" Z="0.9" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.9" />
                  <Point X="2.259380696823" Y="2.481389887624" Z="0.9" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.9" />
                  <Point X="2.572730781401" Y="3.039421331323" Z="0.9" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.9" />
                  <Point X="2.886605039764" Y="4.174373513978" Z="0.9" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.9" />
                  <Point X="2.496891356311" Y="4.418531532472" Z="0.9" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.9" />
                  <Point X="2.090126214124" Y="4.624982712218" Z="0.9" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.9" />
                  <Point X="1.668354494139" Y="4.793296438064" Z="0.9" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.9" />
                  <Point X="1.094681435228" Y="4.950186377169" Z="0.9" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.9" />
                  <Point X="0.43073776201" Y="5.051451151392" Z="0.9" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.9" />
                  <Point X="0.124675455804" Y="4.820419632834" Z="0.9" />
                  <Point X="0" Y="4.355124473572" Z="0.9" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>