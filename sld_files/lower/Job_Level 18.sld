<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#151" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1409" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.697555053711" Y="-29.013564453125" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.542363037109" Y="-28.467375" />
                  <Point X="0.487490509033" Y="-28.388314453125" />
                  <Point X="0.378635375977" Y="-28.231474609375" />
                  <Point X="0.356752044678" Y="-28.209021484375" />
                  <Point X="0.330496856689" Y="-28.18977734375" />
                  <Point X="0.302495025635" Y="-28.17566796875" />
                  <Point X="0.217582870483" Y="-28.149314453125" />
                  <Point X="0.049135883331" Y="-28.09703515625" />
                  <Point X="0.020976638794" Y="-28.092765625" />
                  <Point X="-0.00866468811" Y="-28.092765625" />
                  <Point X="-0.036824085236" Y="-28.09703515625" />
                  <Point X="-0.121736251831" Y="-28.123388671875" />
                  <Point X="-0.290183074951" Y="-28.17566796875" />
                  <Point X="-0.318184906006" Y="-28.18977734375" />
                  <Point X="-0.344440093994" Y="-28.209021484375" />
                  <Point X="-0.366323883057" Y="-28.2314765625" />
                  <Point X="-0.421196563721" Y="-28.3105390625" />
                  <Point X="-0.530051513672" Y="-28.467376953125" />
                  <Point X="-0.538188720703" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.817318237305" Y="-29.506474609375" />
                  <Point X="-0.916584960938" Y="-29.87694140625" />
                  <Point X="-1.079317504883" Y="-29.84535546875" />
                  <Point X="-1.174216796875" Y="-29.8209375" />
                  <Point X="-1.246417724609" Y="-29.802361328125" />
                  <Point X="-1.231123901367" Y="-29.686193359375" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.236793945312" Y="-29.414244140625" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323644897461" Y="-29.131203125" />
                  <Point X="-1.401821411133" Y="-29.06264453125" />
                  <Point X="-1.556905761719" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.612886474609" Y="-28.897994140625" />
                  <Point X="-1.64302734375" Y="-28.890966796875" />
                  <Point X="-1.746784790039" Y="-28.884166015625" />
                  <Point X="-1.952616577148" Y="-28.87067578125" />
                  <Point X="-1.983414794922" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042657226562" Y="-28.89480078125" />
                  <Point X="-2.129113769531" Y="-28.952568359375" />
                  <Point X="-2.300623779297" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102539062" Y="-29.099462890625" />
                  <Point X="-2.4801484375" Y="-29.288490234375" />
                  <Point X="-2.515877929688" Y="-29.2663671875" />
                  <Point X="-2.801711181641" Y="-29.089388671875" />
                  <Point X="-2.933083496094" Y="-28.988234375" />
                  <Point X="-3.104721923828" Y="-28.856080078125" />
                  <Point X="-2.673255371094" Y="-28.108755859375" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858154297" Y="-27.6476484375" />
                  <Point X="-2.406587646484" Y="-27.6161171875" />
                  <Point X="-2.405576904297" Y="-27.5851796875" />
                  <Point X="-2.414565185547" Y="-27.555560546875" />
                  <Point X="-2.428786865234" Y="-27.52673046875" />
                  <Point X="-2.446814453125" Y="-27.501578125" />
                  <Point X="-2.464156738281" Y="-27.48423828125" />
                  <Point X="-2.489305664062" Y="-27.466216796875" />
                  <Point X="-2.518135009766" Y="-27.451998046875" />
                  <Point X="-2.547754882812" Y="-27.443013671875" />
                  <Point X="-2.578691162109" Y="-27.444025390625" />
                  <Point X="-2.610218261719" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.496446289062" Y="-27.956140625" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-3.857038085938" Y="-28.090544921875" />
                  <Point X="-4.082863037109" Y="-27.793857421875" />
                  <Point X="-4.177043945312" Y="-27.635931640625" />
                  <Point X="-4.306143066406" Y="-27.419451171875" />
                  <Point X="-3.542488525391" Y="-26.833478515625" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.083064941406" Y="-26.4758828125" />
                  <Point X="-3.063474853516" Y="-26.444146484375" />
                  <Point X="-3.054179199219" Y="-26.419107421875" />
                  <Point X="-3.051274658203" Y="-26.409865234375" />
                  <Point X="-3.046152099609" Y="-26.390087890625" />
                  <Point X="-3.042037353516" Y="-26.35860546875" />
                  <Point X="-3.042736328125" Y="-26.33573828125" />
                  <Point X="-3.048882080078" Y="-26.313701171875" />
                  <Point X="-3.060885253906" Y="-26.284720703125" />
                  <Point X="-3.073938476562" Y="-26.262400390625" />
                  <Point X="-3.092526855469" Y="-26.24442578125" />
                  <Point X="-3.113909179688" Y="-26.228767578125" />
                  <Point X="-3.121855712891" Y="-26.2235390625" />
                  <Point X="-3.139462890625" Y="-26.213177734375" />
                  <Point X="-3.168722900391" Y="-26.20195703125" />
                  <Point X="-3.200607910156" Y="-26.1954765625" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-4.314142089844" Y="-26.336859375" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.745756347656" Y="-26.338427734375" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.85899609375" Y="-25.818427734375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.030153808594" Y="-25.35365625" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.511687988281" Y="-25.211890625" />
                  <Point X="-3.486818359375" Y="-25.19821484375" />
                  <Point X="-3.478426757812" Y="-25.193015625" />
                  <Point X="-3.459975097656" Y="-25.180208984375" />
                  <Point X="-3.436020019531" Y="-25.158681640625" />
                  <Point X="-3.414914550781" Y="-25.12785546875" />
                  <Point X="-3.404388916016" Y="-25.10312890625" />
                  <Point X="-3.401068603516" Y="-25.094080078125" />
                  <Point X="-3.39491796875" Y="-25.074263671875" />
                  <Point X="-3.389474365234" Y="-25.045521484375" />
                  <Point X="-3.390533203125" Y="-25.011265625" />
                  <Point X="-3.395880371094" Y="-24.986455078125" />
                  <Point X="-3.398017089844" Y="-24.9783125" />
                  <Point X="-3.404167724609" Y="-24.958494140625" />
                  <Point X="-3.417483886719" Y="-24.929169921875" />
                  <Point X="-3.440755859375" Y="-24.899564453125" />
                  <Point X="-3.461875488281" Y="-24.881591796875" />
                  <Point X="-3.469276367188" Y="-24.875896484375" />
                  <Point X="-3.487728027344" Y="-24.86308984375" />
                  <Point X="-3.501923828125" Y="-24.854953125" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-4.519364257812" Y="-24.577822265625" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.881409179688" Y="-24.407693359375" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.774327148438" Y="-23.83791796875" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.122127929688" Y="-23.653279296875" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744984863281" Y="-23.700658203125" />
                  <Point X="-3.723423095703" Y="-23.698771484375" />
                  <Point X="-3.703140136719" Y="-23.694736328125" />
                  <Point X="-3.682553466797" Y="-23.68824609375" />
                  <Point X="-3.641714111328" Y="-23.675369140625" />
                  <Point X="-3.622783691406" Y="-23.667041015625" />
                  <Point X="-3.604039550781" Y="-23.656220703125" />
                  <Point X="-3.587354492188" Y="-23.64398828125" />
                  <Point X="-3.573713378906" Y="-23.62843359375" />
                  <Point X="-3.561299072266" Y="-23.610703125" />
                  <Point X="-3.551352050781" Y="-23.592572265625" />
                  <Point X="-3.543091552734" Y="-23.572630859375" />
                  <Point X="-3.526704589844" Y="-23.533068359375" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951416016" Y="-23.450806640625" />
                  <Point X="-3.524553466797" Y="-23.429900390625" />
                  <Point X="-3.532050048828" Y="-23.410623046875" />
                  <Point X="-3.542017089844" Y="-23.3914765625" />
                  <Point X="-3.561789794922" Y="-23.353494140625" />
                  <Point X="-3.573281738281" Y="-23.336294921875" />
                  <Point X="-3.587194091797" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-4.167987792969" Y="-22.87121484375" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.302327148438" Y="-22.645263671875" />
                  <Point X="-4.081156982422" Y="-22.26634765625" />
                  <Point X="-3.948281494141" Y="-22.095552734375" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.425585449219" Y="-22.0289296875" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187729736328" Y="-22.163658203125" />
                  <Point X="-3.167087890625" Y="-22.17016796875" />
                  <Point X="-3.14679296875" Y="-22.174205078125" />
                  <Point X="-3.118121337891" Y="-22.176712890625" />
                  <Point X="-3.061243652344" Y="-22.181689453125" />
                  <Point X="-3.040561035156" Y="-22.18123828125" />
                  <Point X="-3.019102539062" Y="-22.178412109375" />
                  <Point X="-2.999013916016" Y="-22.17349609375" />
                  <Point X="-2.980465087891" Y="-22.16434765625" />
                  <Point X="-2.962210693359" Y="-22.15271875" />
                  <Point X="-2.946077392578" Y="-22.13976953125" />
                  <Point X="-2.925726074219" Y="-22.11941796875" />
                  <Point X="-2.885353759766" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.845944335938" Y="-21.935208984375" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.120541259766" Y="-21.384162109375" />
                  <Point X="-3.183332763672" Y="-21.275404296875" />
                  <Point X="-3.085836669922" Y="-21.200654296875" />
                  <Point X="-2.700621582031" Y="-20.905314453125" />
                  <Point X="-2.491358886719" Y="-20.789052734375" />
                  <Point X="-2.167036376953" Y="-20.608865234375" />
                  <Point X="-2.110216064453" Y="-20.682916015625" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028891723633" Y="-20.78519921875" />
                  <Point X="-2.012312011719" Y="-20.799111328125" />
                  <Point X="-1.995115844727" Y="-20.8106015625" />
                  <Point X="-1.963204467773" Y="-20.82721484375" />
                  <Point X="-1.899899780273" Y="-20.860169921875" />
                  <Point X="-1.88062512207" Y="-20.86766796875" />
                  <Point X="-1.859718261719" Y="-20.873271484375" />
                  <Point X="-1.839268554688" Y="-20.876419921875" />
                  <Point X="-1.818622070312" Y="-20.87506640625" />
                  <Point X="-1.797306518555" Y="-20.871306640625" />
                  <Point X="-1.777453491211" Y="-20.865517578125" />
                  <Point X="-1.744215698242" Y="-20.85175" />
                  <Point X="-1.678279296875" Y="-20.8244375" />
                  <Point X="-1.660145141602" Y="-20.814490234375" />
                  <Point X="-1.642415771484" Y="-20.802076171875" />
                  <Point X="-1.626863647461" Y="-20.7884375" />
                  <Point X="-1.614632324219" Y="-20.771755859375" />
                  <Point X="-1.603810546875" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.584662109375" Y="-20.699767578125" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.584202148438" Y="-20.3681015625" />
                  <Point X="-1.448314697266" Y="-20.33000390625" />
                  <Point X="-0.949624084473" Y="-20.19019140625" />
                  <Point X="-0.695946166992" Y="-20.1605" />
                  <Point X="-0.294711303711" Y="-20.113541015625" />
                  <Point X="-0.198668334961" Y="-20.471978515625" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113990784" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907154" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425949097" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.274694763184" Y="-20.234193359375" />
                  <Point X="0.307419616699" Y="-20.1120625" />
                  <Point X="0.408614898682" Y="-20.12266015625" />
                  <Point X="0.844031921387" Y="-20.168259765625" />
                  <Point X="1.053930053711" Y="-20.218935546875" />
                  <Point X="1.481038696289" Y="-20.3220546875" />
                  <Point X="1.616658691406" Y="-20.371244140625" />
                  <Point X="1.894644897461" Y="-20.472072265625" />
                  <Point X="2.026751098633" Y="-20.533853515625" />
                  <Point X="2.294559082031" Y="-20.659099609375" />
                  <Point X="2.422225341797" Y="-20.7334765625" />
                  <Point X="2.680972167969" Y="-20.88422265625" />
                  <Point X="2.801342285156" Y="-20.96982421875" />
                  <Point X="2.943259033203" Y="-21.070748046875" />
                  <Point X="2.436351806641" Y="-21.948736328125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076660156" Y="-22.4839453125" />
                  <Point X="2.125881103516" Y="-22.510853515625" />
                  <Point X="2.111606933594" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107727783203" Y="-22.619048828125" />
                  <Point X="2.110533447266" Y="-22.64231640625" />
                  <Point X="2.116099121094" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140070068359" Y="-22.75252734375" />
                  <Point X="2.154467285156" Y="-22.77374609375" />
                  <Point X="2.183027832031" Y="-22.8158359375" />
                  <Point X="2.194462646484" Y="-22.82966796875" />
                  <Point X="2.221595214844" Y="-22.85440625" />
                  <Point X="2.242812744141" Y="-22.8688046875" />
                  <Point X="2.284903808594" Y="-22.897365234375" />
                  <Point X="2.304953125" Y="-22.90773046875" />
                  <Point X="2.327041015625" Y="-22.91599609375" />
                  <Point X="2.348967285156" Y="-22.921337890625" />
                  <Point X="2.372234619141" Y="-22.924142578125" />
                  <Point X="2.418392089844" Y="-22.929708984375" />
                  <Point X="2.436467773438" Y="-22.93015625" />
                  <Point X="2.473206787109" Y="-22.925830078125" />
                  <Point X="2.500114501953" Y="-22.918634765625" />
                  <Point X="2.553493164062" Y="-22.904359375" />
                  <Point X="2.565289794922" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.580751220703" Y="-22.31699609375" />
                  <Point X="3.967325927734" Y="-22.09380859375" />
                  <Point X="3.969788085938" Y="-22.09723046875" />
                  <Point X="4.123270996094" Y="-22.310537109375" />
                  <Point X="4.190373535156" Y="-22.42142578125" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.610977539062" Y="-23.039814453125" />
                  <Point X="3.230783691406" Y="-23.331548828125" />
                  <Point X="3.221421386719" Y="-23.33976171875" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.184608398438" Y="-23.38363671875" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.136606201172" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.114416259766" Y="-23.508708984375" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.103661132812" Y="-23.656931640625" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120678955078" Y="-23.73101953125" />
                  <Point X="3.136282714844" Y="-23.76426171875" />
                  <Point X="3.152388916016" Y="-23.7887421875" />
                  <Point X="3.184340332031" Y="-23.837306640625" />
                  <Point X="3.198896728516" Y="-23.8545546875" />
                  <Point X="3.216140136719" Y="-23.870642578125" />
                  <Point X="3.234346191406" Y="-23.88396484375" />
                  <Point X="3.257686279297" Y="-23.897103515625" />
                  <Point X="3.30398828125" Y="-23.92316796875" />
                  <Point X="3.320521240234" Y="-23.930498046875" />
                  <Point X="3.35612109375" Y="-23.9405625" />
                  <Point X="3.387678466797" Y="-23.944732421875" />
                  <Point X="3.450281738281" Y="-23.953005859375" />
                  <Point X="3.462698242188" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.430743652344" Y="-23.828927734375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.780015625" Y="-23.79641015625" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.867081542969" Y="-24.203005859375" />
                  <Point X="4.890864257812" Y="-24.355759765625" />
                  <Point X="4.152290039062" Y="-24.55366015625" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704787841797" Y="-24.674416015625" />
                  <Point X="3.681548339844" Y="-24.684931640625" />
                  <Point X="3.650543945312" Y="-24.7028515625" />
                  <Point X="3.589038330078" Y="-24.73840234375" />
                  <Point X="3.574312988281" Y="-24.74890234375" />
                  <Point X="3.547529541016" Y="-24.774423828125" />
                  <Point X="3.528927001953" Y="-24.79812890625" />
                  <Point X="3.492023681641" Y="-24.84515234375" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.457479736328" Y="-24.939775390625" />
                  <Point X="3.445178466797" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.021876953125" />
                  <Point X="3.445178710938" Y="-25.0585546875" />
                  <Point X="3.451379638672" Y="-25.090931640625" />
                  <Point X="3.463680908203" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.1766640625" />
                  <Point X="3.480300292969" Y="-25.19812890625" />
                  <Point X="3.492023681641" Y="-25.217408203125" />
                  <Point X="3.510626220703" Y="-25.24111328125" />
                  <Point X="3.547529541016" Y="-25.28813671875" />
                  <Point X="3.559998291016" Y="-25.301236328125" />
                  <Point X="3.589037353516" Y="-25.324158203125" />
                  <Point X="3.620041748047" Y="-25.342078125" />
                  <Point X="3.681547363281" Y="-25.377630859375" />
                  <Point X="3.692709716797" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.580932128906" Y="-25.62375390625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.8550234375" Y="-25.948720703125" />
                  <Point X="4.827931152344" Y="-26.0674453125" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="3.929683349609" Y="-26.06996484375" />
                  <Point X="3.424381835938" Y="-26.00344140625" />
                  <Point X="3.408035400391" Y="-26.0027109375" />
                  <Point X="3.374658447266" Y="-26.005509765625" />
                  <Point X="3.313807861328" Y="-26.018736328125" />
                  <Point X="3.193094238281" Y="-26.04497265625" />
                  <Point X="3.163974609375" Y="-26.05659765625" />
                  <Point X="3.136147705078" Y="-26.073490234375" />
                  <Point X="3.112396972656" Y="-26.0939609375" />
                  <Point X="3.075616699219" Y="-26.138197265625" />
                  <Point X="3.002652832031" Y="-26.22594921875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.964486083984" Y="-26.36265234375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450927734" Y="-26.567998046875" />
                  <Point X="3.010126464844" Y="-26.620376953125" />
                  <Point X="3.076931152344" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.912752441406" Y="-27.37640234375" />
                  <Point X="4.213121582031" Y="-27.606884765625" />
                  <Point X="4.124808105469" Y="-27.7497890625" />
                  <Point X="4.068782714844" Y="-27.829392578125" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="3.250993652344" Y="-27.436775390625" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786128417969" Y="-27.17001171875" />
                  <Point X="2.754225097656" Y="-27.159826171875" />
                  <Point X="2.681803222656" Y="-27.14674609375" />
                  <Point X="2.538134765625" Y="-27.12080078125" />
                  <Point X="2.506783203125" Y="-27.120396484375" />
                  <Point X="2.474611083984" Y="-27.125353515625" />
                  <Point X="2.444833740234" Y="-27.135177734375" />
                  <Point X="2.384668945312" Y="-27.166841796875" />
                  <Point X="2.265315673828" Y="-27.22965625" />
                  <Point X="2.242386230469" Y="-27.246548828125" />
                  <Point X="2.221426269531" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.172867675781" Y="-27.350603515625" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.108754394531" Y="-27.6356796875" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.667264648438" Y="-28.71885546875" />
                  <Point X="2.861284423828" Y="-29.05490625" />
                  <Point X="2.781868896484" Y="-29.1116328125" />
                  <Point X="2.719209960938" Y="-29.152189453125" />
                  <Point X="2.701763671875" Y="-29.163482421875" />
                  <Point X="2.101967529297" Y="-28.3818125" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747507202148" Y="-27.92218359375" />
                  <Point X="1.721923095703" Y="-27.900556640625" />
                  <Point X="1.650495727539" Y="-27.85463671875" />
                  <Point X="1.508799682617" Y="-27.7635390625" />
                  <Point X="1.479986450195" Y="-27.751166015625" />
                  <Point X="1.448365478516" Y="-27.743435546875" />
                  <Point X="1.417100952148" Y="-27.741119140625" />
                  <Point X="1.338982910156" Y="-27.748306640625" />
                  <Point X="1.184014160156" Y="-27.76256640625" />
                  <Point X="1.156367553711" Y="-27.769396484375" />
                  <Point X="1.128982055664" Y="-27.78073828125" />
                  <Point X="1.104594604492" Y="-27.7954609375" />
                  <Point X="1.044273925781" Y="-27.8456171875" />
                  <Point X="0.924610900879" Y="-27.94511328125" />
                  <Point X="0.904140014648" Y="-27.968865234375" />
                  <Point X="0.887247680664" Y="-27.996693359375" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.857588745117" Y="-28.108787109375" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.965815612793" Y="-29.43265625" />
                  <Point X="1.022065612793" Y="-29.859916015625" />
                  <Point X="0.975746765137" Y="-29.8700703125" />
                  <Point X="0.929315612793" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058404907227" Y="-29.752640625" />
                  <Point X="-1.14124597168" Y="-29.731326171875" />
                  <Point X="-1.136936645508" Y="-29.69859375" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759155273" Y="-29.509330078125" />
                  <Point X="-1.123333740234" Y="-29.497693359375" />
                  <Point X="-1.143619384766" Y="-29.3957109375" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026367188" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230573364258" Y="-29.094861328125" />
                  <Point X="-1.250208496094" Y="-29.070935546875" />
                  <Point X="-1.261007202148" Y="-29.05977734375" />
                  <Point X="-1.33918371582" Y="-28.99121875" />
                  <Point X="-1.494268066406" Y="-28.855212890625" />
                  <Point X="-1.506740112305" Y="-28.84596484375" />
                  <Point X="-1.53302355957" Y="-28.82962109375" />
                  <Point X="-1.546834838867" Y="-28.822525390625" />
                  <Point X="-1.576532104492" Y="-28.810224609375" />
                  <Point X="-1.591315795898" Y="-28.805474609375" />
                  <Point X="-1.621456665039" Y="-28.798447265625" />
                  <Point X="-1.636813842773" Y="-28.796169921875" />
                  <Point X="-1.740571289062" Y="-28.789369140625" />
                  <Point X="-1.946403076172" Y="-28.77587890625" />
                  <Point X="-1.961927612305" Y="-28.7761328125" />
                  <Point X="-1.992725952148" Y="-28.779166015625" />
                  <Point X="-2.007999511719" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.05366796875" Y="-28.795494140625" />
                  <Point X="-2.081861816406" Y="-28.808267578125" />
                  <Point X="-2.095435791016" Y="-28.815810546875" />
                  <Point X="-2.181892333984" Y="-28.873578125" />
                  <Point X="-2.35340234375" Y="-28.988177734375" />
                  <Point X="-2.359682373047" Y="-28.992755859375" />
                  <Point X="-2.380451416016" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410471191406" Y="-29.041630859375" />
                  <Point X="-2.503202392578" Y="-29.16248046875" />
                  <Point X="-2.747585693359" Y="-29.011166015625" />
                  <Point X="-2.875125488281" Y="-28.912962890625" />
                  <Point X="-2.980862792969" Y="-28.831548828125" />
                  <Point X="-2.590982910156" Y="-28.156255859375" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849365234" Y="-27.710080078125" />
                  <Point X="-2.323946289062" Y="-27.681109375" />
                  <Point X="-2.319682617188" Y="-27.666177734375" />
                  <Point X="-2.313412109375" Y="-27.634646484375" />
                  <Point X="-2.311638183594" Y="-27.61921875" />
                  <Point X="-2.310627441406" Y="-27.58828125" />
                  <Point X="-2.314670410156" Y="-27.55759375" />
                  <Point X="-2.323658691406" Y="-27.527974609375" />
                  <Point X="-2.3293671875" Y="-27.513533203125" />
                  <Point X="-2.343588867188" Y="-27.484703125" />
                  <Point X="-2.351571777344" Y="-27.47138671875" />
                  <Point X="-2.369599365234" Y="-27.446234375" />
                  <Point X="-2.379644042969" Y="-27.4343984375" />
                  <Point X="-2.396986328125" Y="-27.41705859375" />
                  <Point X="-2.408821289063" Y="-27.407017578125" />
                  <Point X="-2.433970214844" Y="-27.38899609375" />
                  <Point X="-2.447284179688" Y="-27.381015625" />
                  <Point X="-2.476113525391" Y="-27.366796875" />
                  <Point X="-2.490560058594" Y="-27.361087890625" />
                  <Point X="-2.520179931641" Y="-27.352103515625" />
                  <Point X="-2.550860107422" Y="-27.348064453125" />
                  <Point X="-2.581796386719" Y="-27.349076171875" />
                  <Point X="-2.597225830078" Y="-27.3508515625" />
                  <Point X="-2.628752929688" Y="-27.357123046875" />
                  <Point X="-2.64368359375" Y="-27.36138671875" />
                  <Point X="-2.672648925781" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.543946289062" Y="-27.873869140625" />
                  <Point X="-3.793087890625" Y="-28.0177109375" />
                  <Point X="-4.004017822266" Y="-27.740591796875" />
                  <Point X="-4.095451416016" Y="-27.5872734375" />
                  <Point X="-4.181266113281" Y="-27.443375" />
                  <Point X="-3.48465625" Y="-26.90884765625" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.039159423828" Y="-26.566068359375" />
                  <Point X="-3.016269775391" Y="-26.543435546875" />
                  <Point X="-3.002225830078" Y="-26.525783203125" />
                  <Point X="-2.982635742188" Y="-26.494046875" />
                  <Point X="-2.9744140625" Y="-26.477208984375" />
                  <Point X="-2.965118408203" Y="-26.452169921875" />
                  <Point X="-2.959309326172" Y="-26.433685546875" />
                  <Point X="-2.954186767578" Y="-26.413908203125" />
                  <Point X="-2.951953369141" Y="-26.402400390625" />
                  <Point X="-2.947838623047" Y="-26.37091796875" />
                  <Point X="-2.947081787109" Y="-26.355703125" />
                  <Point X="-2.947780761719" Y="-26.3328359375" />
                  <Point X="-2.951228271484" Y="-26.31021875" />
                  <Point X="-2.957374023438" Y="-26.288181640625" />
                  <Point X="-2.961112548828" Y="-26.277349609375" />
                  <Point X="-2.973115722656" Y="-26.248369140625" />
                  <Point X="-2.978879150391" Y="-26.23676171875" />
                  <Point X="-2.991932373047" Y="-26.21444140625" />
                  <Point X="-3.007900390625" Y="-26.194107421875" />
                  <Point X="-3.026488769531" Y="-26.1761328125" />
                  <Point X="-3.036398925781" Y="-26.167779296875" />
                  <Point X="-3.05778125" Y="-26.15212109375" />
                  <Point X="-3.073674316406" Y="-26.1416640625" />
                  <Point X="-3.091281494141" Y="-26.131302734375" />
                  <Point X="-3.105447509766" Y="-26.1244765625" />
                  <Point X="-3.134707519531" Y="-26.113255859375" />
                  <Point X="-3.149801513672" Y="-26.108861328125" />
                  <Point X="-3.181686523438" Y="-26.102380859375" />
                  <Point X="-3.197298339844" Y="-26.10053515625" />
                  <Point X="-3.228619628906" Y="-26.099443359375" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-4.326541992188" Y="-26.242671875" />
                  <Point X="-4.660920898438" Y="-26.286693359375" />
                  <Point X="-4.74076171875" Y="-25.97412109375" />
                  <Point X="-4.764953125" Y="-25.8049765625" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.005565917969" Y="-25.445419921875" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.497434814453" Y="-25.30855078125" />
                  <Point X="-3.476246826172" Y="-25.30003125" />
                  <Point X="-3.465912109375" Y="-25.295134765625" />
                  <Point X="-3.441042480469" Y="-25.281458984375" />
                  <Point X="-3.424259277344" Y="-25.271060546875" />
                  <Point X="-3.405807617188" Y="-25.25825390625" />
                  <Point X="-3.396476074219" Y="-25.250869140625" />
                  <Point X="-3.372520996094" Y="-25.229341796875" />
                  <Point X="-3.357632324219" Y="-25.2123515625" />
                  <Point X="-3.336526855469" Y="-25.181525390625" />
                  <Point X="-3.327504638672" Y="-25.165064453125" />
                  <Point X="-3.316979003906" Y="-25.140337890625" />
                  <Point X="-3.310338378906" Y="-25.122240234375" />
                  <Point X="-3.304187744141" Y="-25.102423828125" />
                  <Point X="-3.301577392578" Y="-25.09194140625" />
                  <Point X="-3.296133789062" Y="-25.06319921875" />
                  <Point X="-3.294519775391" Y="-25.0425859375" />
                  <Point X="-3.295578613281" Y="-25.008330078125" />
                  <Point X="-3.297665527344" Y="-24.99125" />
                  <Point X="-3.303012695312" Y="-24.966439453125" />
                  <Point X="-3.307286132812" Y="-24.950154296875" />
                  <Point X="-3.313436767578" Y="-24.9303359375" />
                  <Point X="-3.317668457031" Y="-24.91921484375" />
                  <Point X="-3.330984619141" Y="-24.889890625" />
                  <Point X="-3.342796630859" Y="-24.8704609375" />
                  <Point X="-3.366068603516" Y="-24.84085546875" />
                  <Point X="-3.3791875" Y="-24.82721484375" />
                  <Point X="-3.400307128906" Y="-24.8092421875" />
                  <Point X="-3.415108886719" Y="-24.7978515625" />
                  <Point X="-3.433560546875" Y="-24.785044921875" />
                  <Point X="-3.440486083984" Y="-24.78066796875" />
                  <Point X="-3.465612792969" Y="-24.767166015625" />
                  <Point X="-3.496565185547" Y="-24.75436328125" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-4.494776367188" Y="-24.48605859375" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.682634277344" Y="-23.862765625" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.134528320313" Y="-23.747466796875" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747056884766" Y="-23.795634765625" />
                  <Point X="-3.736703613281" Y="-23.795296875" />
                  <Point X="-3.715141845703" Y="-23.79341015625" />
                  <Point X="-3.70488671875" Y="-23.7919453125" />
                  <Point X="-3.684603759766" Y="-23.78791015625" />
                  <Point X="-3.674575927734" Y="-23.78533984375" />
                  <Point X="-3.653989257812" Y="-23.778849609375" />
                  <Point X="-3.613149902344" Y="-23.76597265625" />
                  <Point X="-3.603458740234" Y="-23.762326171875" />
                  <Point X="-3.584528320312" Y="-23.753998046875" />
                  <Point X="-3.5752890625" Y="-23.74931640625" />
                  <Point X="-3.556544921875" Y="-23.73849609375" />
                  <Point X="-3.547869873047" Y="-23.7328359375" />
                  <Point X="-3.531184814453" Y="-23.720603515625" />
                  <Point X="-3.5159296875" Y="-23.706626953125" />
                  <Point X="-3.502288574219" Y="-23.691072265625" />
                  <Point X="-3.495892578125" Y="-23.682921875" />
                  <Point X="-3.483478271484" Y="-23.66519140625" />
                  <Point X="-3.478010253906" Y="-23.656396484375" />
                  <Point X="-3.468063232422" Y="-23.638265625" />
                  <Point X="-3.463584228516" Y="-23.6289296875" />
                  <Point X="-3.455323730469" Y="-23.60898828125" />
                  <Point X="-3.438936767578" Y="-23.56942578125" />
                  <Point X="-3.435499267578" Y="-23.559650390625" />
                  <Point X="-3.429710205078" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057128906" Y="-23.436353515625" />
                  <Point X="-3.427188720703" Y="-23.42621875" />
                  <Point X="-3.432790771484" Y="-23.4053125" />
                  <Point X="-3.436012695312" Y="-23.39546875" />
                  <Point X="-3.443509277344" Y="-23.37619140625" />
                  <Point X="-3.447783935547" Y="-23.3667578125" />
                  <Point X="-3.457750976562" Y="-23.347611328125" />
                  <Point X="-3.477523681641" Y="-23.30962890625" />
                  <Point X="-3.482799804688" Y="-23.30071484375" />
                  <Point X="-3.494291748047" Y="-23.283515625" />
                  <Point X="-3.500507568359" Y="-23.27523046875" />
                  <Point X="-3.514419921875" Y="-23.258650390625" />
                  <Point X="-3.521497558594" Y="-23.25109375" />
                  <Point X="-3.536439208984" Y="-23.2367890625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-4.110155273438" Y="-22.795845703125" />
                  <Point X="-4.227614257812" Y="-22.705716796875" />
                  <Point X="-4.220280273438" Y="-22.69315234375" />
                  <Point X="-4.002296630859" Y="-22.3196953125" />
                  <Point X="-3.873300537109" Y="-22.15388671875" />
                  <Point X="-3.726337158203" Y="-21.964986328125" />
                  <Point X="-3.473085449219" Y="-22.111201171875" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244925292969" Y="-22.242279296875" />
                  <Point X="-3.225998046875" Y="-22.250609375" />
                  <Point X="-3.216302490234" Y="-22.254259765625" />
                  <Point X="-3.195660644531" Y="-22.26076953125" />
                  <Point X="-3.185622314453" Y="-22.263341796875" />
                  <Point X="-3.165327392578" Y="-22.26737890625" />
                  <Point X="-3.155070800781" Y="-22.26884375" />
                  <Point X="-3.126399169922" Y="-22.2713515625" />
                  <Point X="-3.069521484375" Y="-22.276328125" />
                  <Point X="-3.059171875" Y="-22.276666015625" />
                  <Point X="-3.038489257812" Y="-22.27621484375" />
                  <Point X="-3.02815625" Y="-22.27542578125" />
                  <Point X="-3.006697753906" Y="-22.272599609375" />
                  <Point X="-2.996520751953" Y="-22.270689453125" />
                  <Point X="-2.976432128906" Y="-22.2657734375" />
                  <Point X="-2.9569921875" Y="-22.258697265625" />
                  <Point X="-2.938443359375" Y="-22.249548828125" />
                  <Point X="-2.929422851563" Y="-22.244470703125" />
                  <Point X="-2.911168457031" Y="-22.232841796875" />
                  <Point X="-2.902745605469" Y="-22.226806640625" />
                  <Point X="-2.886612304688" Y="-22.213857421875" />
                  <Point X="-2.878901855469" Y="-22.206943359375" />
                  <Point X="-2.858550537109" Y="-22.186591796875" />
                  <Point X="-2.818178222656" Y="-22.146220703125" />
                  <Point X="-2.811268310547" Y="-22.138513671875" />
                  <Point X="-2.798323486328" Y="-22.12238671875" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.751305908203" Y="-21.9269296875" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.038268798828" Y="-21.336662109375" />
                  <Point X="-3.059387695312" Y="-21.300083984375" />
                  <Point X="-3.028034179688" Y="-21.276044921875" />
                  <Point X="-2.648377441406" Y="-20.984966796875" />
                  <Point X="-2.445221435547" Y="-20.87209765625" />
                  <Point X="-2.192524658203" Y="-20.731703125" />
                  <Point X="-2.185584960938" Y="-20.740748046875" />
                  <Point X="-2.118564453125" Y="-20.82808984375" />
                  <Point X="-2.111819091797" Y="-20.835953125" />
                  <Point X="-2.097515380859" Y="-20.85089453125" />
                  <Point X="-2.089956787109" Y="-20.85797265625" />
                  <Point X="-2.073376953125" Y="-20.871884765625" />
                  <Point X="-2.065091552734" Y="-20.8781015625" />
                  <Point X="-2.047895507812" Y="-20.889591796875" />
                  <Point X="-2.038984619141" Y="-20.8948671875" />
                  <Point X="-2.007073242188" Y="-20.91148046875" />
                  <Point X="-1.943768676758" Y="-20.944435546875" />
                  <Point X="-1.934341674805" Y="-20.94870703125" />
                  <Point X="-1.915066894531" Y="-20.956205078125" />
                  <Point X="-1.905219238281" Y="-20.9594296875" />
                  <Point X="-1.88431237793" Y="-20.965033203125" />
                  <Point X="-1.874174194336" Y="-20.967166015625" />
                  <Point X="-1.853724487305" Y="-20.970314453125" />
                  <Point X="-1.833053955078" Y="-20.971216796875" />
                  <Point X="-1.812407470703" Y="-20.96986328125" />
                  <Point X="-1.802120117188" Y="-20.968623046875" />
                  <Point X="-1.780804443359" Y="-20.96486328125" />
                  <Point X="-1.770712402344" Y="-20.9625078125" />
                  <Point X="-1.750859375" Y="-20.95671875" />
                  <Point X="-1.741098510742" Y="-20.95328515625" />
                  <Point X="-1.707860717773" Y="-20.939517578125" />
                  <Point X="-1.641924316406" Y="-20.912205078125" />
                  <Point X="-1.632590576172" Y="-20.907728515625" />
                  <Point X="-1.614456420898" Y="-20.89778125" />
                  <Point X="-1.605655883789" Y="-20.892310546875" />
                  <Point X="-1.587926513672" Y="-20.879896484375" />
                  <Point X="-1.579778320312" Y="-20.873501953125" />
                  <Point X="-1.564226196289" Y="-20.85986328125" />
                  <Point X="-1.550251098633" Y="-20.844611328125" />
                  <Point X="-1.538019775391" Y="-20.8279296875" />
                  <Point X="-1.532359619141" Y="-20.819255859375" />
                  <Point X="-1.521537841797" Y="-20.80051171875" />
                  <Point X="-1.516854858398" Y="-20.79126953125" />
                  <Point X="-1.508524536133" Y="-20.7723359375" />
                  <Point X="-1.504877197266" Y="-20.76264453125" />
                  <Point X="-1.494059082031" Y="-20.728333984375" />
                  <Point X="-1.472597900391" Y="-20.660267578125" />
                  <Point X="-1.470026489258" Y="-20.650234375" />
                  <Point X="-1.465990966797" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266357422" Y="-20.43734375" />
                  <Point X="-1.422669189453" Y="-20.4214765625" />
                  <Point X="-0.931165039062" Y="-20.2836796875" />
                  <Point X="-0.68490246582" Y="-20.25485546875" />
                  <Point X="-0.36522253418" Y="-20.21744140625" />
                  <Point X="-0.290431274414" Y="-20.49656640625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.366457702637" Y="-20.25878125" />
                  <Point X="0.378190826416" Y="-20.2149921875" />
                  <Point X="0.398720184326" Y="-20.217142578125" />
                  <Point X="0.827853210449" Y="-20.262083984375" />
                  <Point X="1.031634765625" Y="-20.311283203125" />
                  <Point X="1.453622436523" Y="-20.413166015625" />
                  <Point X="1.584266967773" Y="-20.46055078125" />
                  <Point X="1.858247192383" Y="-20.55992578125" />
                  <Point X="1.986506591797" Y="-20.619908203125" />
                  <Point X="2.250434082031" Y="-20.74333984375" />
                  <Point X="2.374403076172" Y="-20.8155625" />
                  <Point X="2.629425048828" Y="-20.964138671875" />
                  <Point X="2.746285400391" Y="-21.047244140625" />
                  <Point X="2.817778564453" Y="-21.0980859375" />
                  <Point X="2.354079345703" Y="-21.901236328125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053179931641" Y="-22.42656640625" />
                  <Point X="2.044181884766" Y="-22.450439453125" />
                  <Point X="2.041301391602" Y="-22.459404296875" />
                  <Point X="2.034105834961" Y="-22.4863125" />
                  <Point X="2.019831665039" Y="-22.53969140625" />
                  <Point X="2.017912231445" Y="-22.5485390625" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755371094" Y="-22.616759765625" />
                  <Point X="2.013410888672" Y="-22.630421875" />
                  <Point X="2.016216674805" Y="-22.653689453125" />
                  <Point X="2.021782226562" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.02914440918" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.045319580078" Y="-22.77611328125" />
                  <Point X="2.055680419922" Y="-22.796154296875" />
                  <Point X="2.061457763672" Y="-22.8058671875" />
                  <Point X="2.075854980469" Y="-22.8270859375" />
                  <Point X="2.104415527344" Y="-22.86917578125" />
                  <Point X="2.109808105469" Y="-22.876365234375" />
                  <Point X="2.130456298828" Y="-22.899869140625" />
                  <Point X="2.157588867188" Y="-22.924607421875" />
                  <Point X="2.168250488281" Y="-22.933015625" />
                  <Point X="2.189468017578" Y="-22.9474140625" />
                  <Point X="2.231559082031" Y="-22.975974609375" />
                  <Point X="2.241275634766" Y="-22.98175390625" />
                  <Point X="2.261324951172" Y="-22.992119140625" />
                  <Point X="2.271657714844" Y="-22.996705078125" />
                  <Point X="2.293745605469" Y="-23.004970703125" />
                  <Point X="2.304554199219" Y="-23.008296875" />
                  <Point X="2.32648046875" Y="-23.013638671875" />
                  <Point X="2.337598144531" Y="-23.015654296875" />
                  <Point X="2.360865478516" Y="-23.018458984375" />
                  <Point X="2.407022949219" Y="-23.024025390625" />
                  <Point X="2.416042236328" Y="-23.0246796875" />
                  <Point X="2.447577636719" Y="-23.02450390625" />
                  <Point X="2.484316650391" Y="-23.020177734375" />
                  <Point X="2.497748291016" Y="-23.01760546875" />
                  <Point X="2.524656005859" Y="-23.01041015625" />
                  <Point X="2.578034667969" Y="-22.996134765625" />
                  <Point X="2.583999755859" Y="-22.994328125" />
                  <Point X="2.604416748047" Y="-22.986927734375" />
                  <Point X="2.627660888672" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.628251220703" Y="-22.399267578125" />
                  <Point X="3.940404296875" Y="-22.219046875" />
                  <Point X="4.043947998047" Y="-22.36294921875" />
                  <Point X="4.109096191406" Y="-22.470609375" />
                  <Point X="4.136883789062" Y="-22.516529296875" />
                  <Point X="3.553145263672" Y="-22.9644453125" />
                  <Point X="3.172951416016" Y="-23.2561796875" />
                  <Point X="3.168135742188" Y="-23.2601328125" />
                  <Point X="3.152114257812" Y="-23.2747890625" />
                  <Point X="3.134666748047" Y="-23.293400390625" />
                  <Point X="3.128576416016" Y="-23.300578125" />
                  <Point X="3.1092109375" Y="-23.325841796875" />
                  <Point X="3.070794189453" Y="-23.375958984375" />
                  <Point X="3.065635742188" Y="-23.3833984375" />
                  <Point X="3.049739257812" Y="-23.41062890625" />
                  <Point X="3.034762939453" Y="-23.444455078125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.022926513672" Y="-23.483123046875" />
                  <Point X="3.008616210938" Y="-23.53429296875" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.01062109375" Y="-23.67612890625" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.024597900391" Y="-23.741765625" />
                  <Point X="3.034681640625" Y="-23.77138671875" />
                  <Point X="3.050285400391" Y="-23.80462890625" />
                  <Point X="3.056918945312" Y="-23.8164765625" />
                  <Point X="3.073025146484" Y="-23.84095703125" />
                  <Point X="3.1049765625" Y="-23.889521484375" />
                  <Point X="3.111739501953" Y="-23.898578125" />
                  <Point X="3.126295898438" Y="-23.915826171875" />
                  <Point X="3.134089355469" Y="-23.924017578125" />
                  <Point X="3.151332763672" Y="-23.94010546875" />
                  <Point X="3.160039550781" Y="-23.94730859375" />
                  <Point X="3.178245605469" Y="-23.960630859375" />
                  <Point X="3.187744873047" Y="-23.96675" />
                  <Point X="3.211084960938" Y="-23.979888671875" />
                  <Point X="3.257386962891" Y="-24.005953125" />
                  <Point X="3.265483642578" Y="-24.010015625" />
                  <Point X="3.294676757813" Y="-24.0219140625" />
                  <Point X="3.330276611328" Y="-24.031978515625" />
                  <Point X="3.343676269531" Y="-24.034744140625" />
                  <Point X="3.375233642578" Y="-24.0389140625" />
                  <Point X="3.437836914063" Y="-24.0471875" />
                  <Point X="3.444033935547" Y="-24.04780078125" />
                  <Point X="3.465708496094" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.443143554688" Y="-23.923115234375" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.752683105469" Y="-24.085763671875" />
                  <Point X="4.773212402344" Y="-24.21762109375" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.127702148438" Y="-24.461896484375" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686022460938" Y="-24.580458984375" />
                  <Point X="3.665624023438" Y="-24.587865234375" />
                  <Point X="3.642384521484" Y="-24.598380859375" />
                  <Point X="3.634009521484" Y="-24.602681640625" />
                  <Point X="3.603005126953" Y="-24.6206015625" />
                  <Point X="3.541499511719" Y="-24.65615234375" />
                  <Point X="3.533883789062" Y="-24.661052734375" />
                  <Point X="3.508777587891" Y="-24.680126953125" />
                  <Point X="3.481994140625" Y="-24.7056484375" />
                  <Point X="3.472794189453" Y="-24.715775390625" />
                  <Point X="3.454191650391" Y="-24.73948046875" />
                  <Point X="3.417288330078" Y="-24.78650390625" />
                  <Point X="3.410852783203" Y="-24.79579296875" />
                  <Point X="3.399129394531" Y="-24.815072265625" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.380004150391" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.364175292969" Y="-24.92190625" />
                  <Point X="3.351874023438" Y="-24.986138671875" />
                  <Point X="3.350603271484" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026263671875" />
                  <Point X="3.350280029297" Y="-25.06294140625" />
                  <Point X="3.351874511719" Y="-25.076423828125" />
                  <Point X="3.358075439453" Y="-25.10880078125" />
                  <Point X="3.370376708984" Y="-25.173033203125" />
                  <Point X="3.373158447266" Y="-25.183986328125" />
                  <Point X="3.38000390625" Y="-25.205486328125" />
                  <Point X="3.384067626953" Y="-25.216033203125" />
                  <Point X="3.393841552734" Y="-25.237498046875" />
                  <Point X="3.399129394531" Y="-25.24748828125" />
                  <Point X="3.410852783203" Y="-25.266767578125" />
                  <Point X="3.417288330078" Y="-25.276056640625" />
                  <Point X="3.435890869141" Y="-25.29976171875" />
                  <Point X="3.472794189453" Y="-25.34678515625" />
                  <Point X="3.478718017578" Y="-25.353634765625" />
                  <Point X="3.501137939453" Y="-25.3758046875" />
                  <Point X="3.530177001953" Y="-25.3987265625" />
                  <Point X="3.541498535156" Y="-25.406408203125" />
                  <Point X="3.572502929688" Y="-25.424328125" />
                  <Point X="3.634008544922" Y="-25.459880859375" />
                  <Point X="3.639498535156" Y="-25.462818359375" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026367188" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.556344238281" Y="-25.715517578125" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.761614257812" Y="-25.931048828125" />
                  <Point X="4.735312011719" Y="-26.046310546875" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="3.942083496094" Y="-25.97577734375" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428622802734" Y="-25.90853515625" />
                  <Point X="3.400096923828" Y="-25.90804296875" />
                  <Point X="3.366719970703" Y="-25.910841796875" />
                  <Point X="3.354480224609" Y="-25.912677734375" />
                  <Point X="3.293629638672" Y="-25.925904296875" />
                  <Point X="3.172916015625" Y="-25.952140625" />
                  <Point X="3.157871826172" Y="-25.956744140625" />
                  <Point X="3.128752197266" Y="-25.968369140625" />
                  <Point X="3.114676757812" Y="-25.975390625" />
                  <Point X="3.086849853516" Y="-25.992283203125" />
                  <Point X="3.074125488281" Y="-26.001529296875" />
                  <Point X="3.050374755859" Y="-26.022" />
                  <Point X="3.039348388672" Y="-26.033224609375" />
                  <Point X="3.002568115234" Y="-26.0774609375" />
                  <Point X="2.929604248047" Y="-26.165212890625" />
                  <Point X="2.921325195312" Y="-26.176849609375" />
                  <Point X="2.906605712891" Y="-26.20123046875" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.869885742188" Y="-26.353947265625" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876787109375" Y="-26.576671875" />
                  <Point X="2.889158935547" Y="-26.605482421875" />
                  <Point X="2.896541259766" Y="-26.619373046875" />
                  <Point X="2.930216796875" Y="-26.671751953125" />
                  <Point X="2.997021484375" Y="-26.775662109375" />
                  <Point X="3.001740966797" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.854920166016" Y="-27.451771484375" />
                  <Point X="4.087169921875" Y="-27.629984375" />
                  <Point X="4.045492919922" Y="-27.697423828125" />
                  <Point X="4.001273193359" Y="-27.760251953125" />
                  <Point X="3.298493652344" Y="-27.35450390625" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841198730469" Y="-27.090890625" />
                  <Point X="2.815021484375" Y="-27.07951171875" />
                  <Point X="2.783118164062" Y="-27.069326171875" />
                  <Point X="2.771109863281" Y="-27.066337890625" />
                  <Point X="2.698687988281" Y="-27.0532578125" />
                  <Point X="2.55501953125" Y="-27.0273125" />
                  <Point X="2.539359863281" Y="-27.02580859375" />
                  <Point X="2.508008300781" Y="-27.025404296875" />
                  <Point X="2.49231640625" Y="-27.02650390625" />
                  <Point X="2.460144287109" Y="-27.0314609375" />
                  <Point X="2.444846435547" Y="-27.03513671875" />
                  <Point X="2.415069091797" Y="-27.0449609375" />
                  <Point X="2.400589599609" Y="-27.051109375" />
                  <Point X="2.340424804688" Y="-27.0827734375" />
                  <Point X="2.221071533203" Y="-27.145587890625" />
                  <Point X="2.208967773438" Y="-27.153171875" />
                  <Point X="2.186038330078" Y="-27.170064453125" />
                  <Point X="2.175212646484" Y="-27.179373046875" />
                  <Point X="2.154252685547" Y="-27.20033203125" />
                  <Point X="2.144941650391" Y="-27.21116015625" />
                  <Point X="2.128047363281" Y="-27.234091796875" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.088799804688" Y="-27.306359375" />
                  <Point X="2.025985229492" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.002187744141" Y="-27.580140625" />
                  <Point X="2.015266601563" Y="-27.6525625" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.5849921875" Y="-28.76635546875" />
                  <Point X="2.73589453125" Y="-29.027724609375" />
                  <Point X="2.723753173828" Y="-29.0360859375" />
                  <Point X="2.177336181641" Y="-28.32398046875" />
                  <Point X="1.833914550781" Y="-27.876423828125" />
                  <Point X="1.828654663086" Y="-27.8701484375" />
                  <Point X="1.808836669922" Y="-27.8496328125" />
                  <Point X="1.783252685547" Y="-27.828005859375" />
                  <Point X="1.773296875" Y="-27.820646484375" />
                  <Point X="1.701869506836" Y="-27.7747265625" />
                  <Point X="1.560173461914" Y="-27.68362890625" />
                  <Point X="1.546284912109" Y="-27.676248046875" />
                  <Point X="1.517471435547" Y="-27.663875" />
                  <Point X="1.502547119141" Y="-27.6588828125" />
                  <Point X="1.470926025391" Y="-27.65115234375" />
                  <Point X="1.455384887695" Y="-27.6486953125" />
                  <Point X="1.424120361328" Y="-27.64637890625" />
                  <Point X="1.408396972656" Y="-27.64651953125" />
                  <Point X="1.330278808594" Y="-27.65370703125" />
                  <Point X="1.175310180664" Y="-27.667966796875" />
                  <Point X="1.161229492188" Y="-27.67033984375" />
                  <Point X="1.133582885742" Y="-27.677169921875" />
                  <Point X="1.120017089844" Y="-27.681626953125" />
                  <Point X="1.092631591797" Y="-27.69296875" />
                  <Point X="1.079884033203" Y="-27.69941015625" />
                  <Point X="1.055496459961" Y="-27.7141328125" />
                  <Point X="1.043856323242" Y="-27.7224140625" />
                  <Point X="0.983535827637" Y="-27.7725703125" />
                  <Point X="0.863872680664" Y="-27.87206640625" />
                  <Point X="0.852649597168" Y="-27.883091796875" />
                  <Point X="0.832178710938" Y="-27.90684375" />
                  <Point X="0.822930969238" Y="-27.9195703125" />
                  <Point X="0.806038635254" Y="-27.9473984375" />
                  <Point X="0.799017822266" Y="-27.96147265625" />
                  <Point X="0.787394470215" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.764756408691" Y="-28.088609375" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.83309173584" Y="-29.152341796875" />
                  <Point X="0.789317932129" Y="-28.9889765625" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146789551" Y="-28.453583984375" />
                  <Point X="0.626788085938" Y="-28.42381640625" />
                  <Point X="0.620407287598" Y="-28.41320703125" />
                  <Point X="0.565534790039" Y="-28.334146484375" />
                  <Point X="0.4566796875" Y="-28.177306640625" />
                  <Point X="0.446668151855" Y="-28.16516796875" />
                  <Point X="0.424784912109" Y="-28.14271484375" />
                  <Point X="0.412913208008" Y="-28.132400390625" />
                  <Point X="0.386658081055" Y="-28.11315625" />
                  <Point X="0.373244873047" Y="-28.104939453125" />
                  <Point X="0.345242980957" Y="-28.090830078125" />
                  <Point X="0.330654449463" Y="-28.0849375" />
                  <Point X="0.245742172241" Y="-28.058583984375" />
                  <Point X="0.077295211792" Y="-28.0063046875" />
                  <Point X="0.063377120972" Y="-28.003109375" />
                  <Point X="0.035217838287" Y="-27.99883984375" />
                  <Point X="0.020976644516" Y="-27.997765625" />
                  <Point X="-0.008664708138" Y="-27.997765625" />
                  <Point X="-0.022905902863" Y="-27.99883984375" />
                  <Point X="-0.051065185547" Y="-28.003109375" />
                  <Point X="-0.064983421326" Y="-28.0063046875" />
                  <Point X="-0.149895553589" Y="-28.032658203125" />
                  <Point X="-0.318342376709" Y="-28.0849375" />
                  <Point X="-0.332931030273" Y="-28.090830078125" />
                  <Point X="-0.360932922363" Y="-28.104939453125" />
                  <Point X="-0.374346160889" Y="-28.11315625" />
                  <Point X="-0.400601287842" Y="-28.132400390625" />
                  <Point X="-0.412475067139" Y="-28.142716796875" />
                  <Point X="-0.434358886719" Y="-28.165171875" />
                  <Point X="-0.444368804932" Y="-28.177310546875" />
                  <Point X="-0.499241424561" Y="-28.256373046875" />
                  <Point X="-0.608096374512" Y="-28.4132109375" />
                  <Point X="-0.61247052002" Y="-28.420130859375" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.909081237793" Y="-29.48188671875" />
                  <Point X="-0.985425598145" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.952183706944" Y="-28.781875105533" />
                  <Point X="-2.444995882285" Y="-29.086624295821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.904676534189" Y="-28.699590122027" />
                  <Point X="-2.383198371416" Y="-29.01292581372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.857169361434" Y="-28.617305138522" />
                  <Point X="-2.299677650664" Y="-28.952279952958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.131120960004" Y="-29.654419649476" />
                  <Point X="-0.979691700831" Y="-29.745407527882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.896312450607" Y="-27.882094805968" />
                  <Point X="-3.730626575045" Y="-27.981648923722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.809662188679" Y="-28.535020155016" />
                  <Point X="-2.212343845048" Y="-28.893925224726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.11966994465" Y="-29.550469940915" />
                  <Point X="-0.954112930885" Y="-29.64994663069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.035657347868" Y="-27.687537772006" />
                  <Point X="-3.636560020844" Y="-27.927339638973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.762155015925" Y="-28.45273517151" />
                  <Point X="-2.125009531097" Y="-28.835570801934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.136946205542" Y="-29.429259143366" />
                  <Point X="-0.928534160939" Y="-29.554485733498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.138661607899" Y="-27.514816395825" />
                  <Point X="-3.542493468307" Y="-27.873030353225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.71464784317" Y="-28.370450188004" />
                  <Point X="-2.023080570234" Y="-28.785985727719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.161984168526" Y="-29.303384644692" />
                  <Point X="-0.902955414849" Y="-29.459024821972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.133766662186" Y="-27.406927403201" />
                  <Point X="-3.448427021819" Y="-27.818721003756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.667140670415" Y="-28.288165204498" />
                  <Point X="-1.8443130908" Y="-28.782569893339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.188913803015" Y="-29.176373515107" />
                  <Point X="-0.877376744515" Y="-29.363563864928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.05276154568" Y="-27.344770014914" />
                  <Point X="-3.35436057533" Y="-27.764411654288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.61963349766" Y="-28.205880220993" />
                  <Point X="-1.637276981404" Y="-28.796139565456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.397009015048" Y="-28.940507124453" />
                  <Point X="-0.851798074182" Y="-29.268102907883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.971756429173" Y="-27.282612626627" />
                  <Point X="-3.260294128841" Y="-27.710102304819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.572126306156" Y="-28.123595248753" />
                  <Point X="-0.826219403848" Y="-29.172641950838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.890751312667" Y="-27.22045523834" />
                  <Point X="-3.166227682353" Y="-27.655792955351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.524619086163" Y="-28.04131029363" />
                  <Point X="-0.800640733514" Y="-29.077180993793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.80974619616" Y="-27.158297850053" />
                  <Point X="-3.072161235864" Y="-27.601483605882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.47711186617" Y="-27.959025338508" />
                  <Point X="-0.77506206318" Y="-28.981720036749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.728741079653" Y="-27.096140461766" />
                  <Point X="-2.978094789375" Y="-27.547174256414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.429604646178" Y="-27.876740383386" />
                  <Point X="-0.749483392846" Y="-28.886259079704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.647735963147" Y="-27.033983073479" />
                  <Point X="-2.884028342886" Y="-27.492864906945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.382097426185" Y="-27.794455428263" />
                  <Point X="-0.723904722512" Y="-28.790798122659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.56673084664" Y="-26.971825685193" />
                  <Point X="-2.789961896398" Y="-27.438555557476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.335561856982" Y="-27.711586646446" />
                  <Point X="-0.698326052178" Y="-28.695337165614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.686011873143" Y="-26.188463622006" />
                  <Point X="-4.547402559719" Y="-26.271748499874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.485725730133" Y="-26.909668296906" />
                  <Point X="-2.695895449909" Y="-27.384246208008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311507214635" Y="-27.615209961002" />
                  <Point X="-0.672747381844" Y="-28.59987620857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.719454062531" Y="-26.057539354654" />
                  <Point X="-4.396101072757" Y="-26.251829432254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.404720446172" Y="-26.847511009235" />
                  <Point X="-2.570585990212" Y="-27.348709554796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.343308384262" Y="-27.485271717798" />
                  <Point X="-0.64716871151" Y="-28.504415251525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.747054072" Y="-25.930125423144" />
                  <Point X="-4.244799550701" Y="-26.231910385721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.323715159971" Y="-26.785353722911" />
                  <Point X="-0.609860646724" Y="-28.416002025692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.764395494711" Y="-25.808875472424" />
                  <Point X="-4.093497998781" Y="-26.211991357132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.24270987377" Y="-26.723196436588" />
                  <Point X="-0.555698264612" Y="-28.3377158954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781737067607" Y="-25.687625431462" />
                  <Point X="-3.942196446861" Y="-26.192072328543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.161704587568" Y="-26.661039150264" />
                  <Point X="-0.501413859108" Y="-28.259503084159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804033327664" Y="-29.043894888911" />
                  <Point X="0.820084117851" Y="-29.053539176639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.693571063738" Y="-25.629770738389" />
                  <Point X="-3.790894894941" Y="-26.172153299954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.080699301367" Y="-26.59888186394" />
                  <Point X="-0.447130473013" Y="-28.181289660396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.768637797449" Y="-28.91179693598" />
                  <Point X="0.804239669208" Y="-28.933188698684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.566005630719" Y="-25.595589610704" />
                  <Point X="-3.639593343021" Y="-26.152234271365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.007296409562" Y="-26.532156598212" />
                  <Point X="-0.375123221218" Y="-28.113725809548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733242426992" Y="-28.779699079041" />
                  <Point X="0.788395220566" Y="-28.812838220729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.438440197701" Y="-25.561408483018" />
                  <Point X="-3.4882917911" Y="-26.132315242776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963664369054" Y="-26.447543200346" />
                  <Point X="-0.265748696509" Y="-28.068614481436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.697847056536" Y="-28.647601222102" />
                  <Point X="0.772550771924" Y="-28.692487742774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.310874764683" Y="-25.527227355332" />
                  <Point X="-3.33699023918" Y="-26.112396214187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947362852813" Y="-26.34650796675" />
                  <Point X="-0.144120562905" Y="-28.030865864349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.662451686079" Y="-28.515503365163" />
                  <Point X="0.756706323282" Y="-28.57213726482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.183309331665" Y="-25.493046227647" />
                  <Point X="-3.162826773593" Y="-26.106214009197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.000175044747" Y="-26.203945027677" />
                  <Point X="-0.014076809374" Y="-27.998173861861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.580170482643" Y="-28.355233657597" />
                  <Point X="0.74086187464" Y="-28.451786786865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.055743898647" Y="-25.458865099961" />
                  <Point X="0.297808800513" Y="-28.074743469747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.44436142619" Y="-28.162801171132" />
                  <Point X="0.725297318989" Y="-28.331604485586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.92817857596" Y="-25.424683905982" />
                  <Point X="0.734789902305" Y="-28.226478032338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.800613324812" Y="-25.390502669018" />
                  <Point X="0.756096762989" Y="-28.128450313102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.673048073664" Y="-25.356321432053" />
                  <Point X="0.777403693745" Y="-28.030422635971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.545482822516" Y="-25.322140195089" />
                  <Point X="0.810753591743" Y="-27.939631103591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.434983267713" Y="-25.277704853255" />
                  <Point X="0.871795656939" Y="-27.865478703936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.641404585734" Y="-28.928767020328" />
                  <Point X="2.698604034253" Y="-28.963135916373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.778279026071" Y="-24.359741159615" />
                  <Point X="-4.627028379647" Y="-24.450621716654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358206518646" Y="-25.213006805491" />
                  <Point X="0.949173264806" Y="-27.801141688562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483609239345" Y="-28.723123838082" />
                  <Point X="2.600626635243" Y="-28.793434983018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763218134374" Y="-24.257960483588" />
                  <Point X="-4.294116611221" Y="-24.539825115176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.31295355443" Y="-25.129367356847" />
                  <Point X="1.026550036292" Y="-27.73680417064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.325813892956" Y="-28.517480655835" />
                  <Point X="2.502649551348" Y="-28.623734239003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.748157242678" Y="-24.15617980756" />
                  <Point X="-3.961205208783" Y="-24.629028293791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.29492834315" Y="-25.029367823719" />
                  <Point X="1.119516883511" Y="-27.681834115274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.168018555897" Y="-28.311837479195" />
                  <Point X="2.404672527284" Y="-28.454033530938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.733096350981" Y="-24.054399131533" />
                  <Point X="-3.628293806344" Y="-24.718231472406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.326624246082" Y="-24.899492831127" />
                  <Point X="1.266868700171" Y="-27.659541846312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.010223367521" Y="-28.106194391892" />
                  <Point X="2.306695503221" Y="-28.284332822873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708530363745" Y="-23.958329693095" />
                  <Point X="1.430158863612" Y="-27.646826302263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.852428179144" Y="-27.90055130459" />
                  <Point X="2.208718479157" Y="-28.114632114809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.682702751721" Y="-23.863018315308" />
                  <Point X="2.110741455093" Y="-27.944931406744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.656875060905" Y="-23.767706984865" />
                  <Point X="2.040421576663" Y="-27.791848788325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.610369403038" Y="-23.684820230503" />
                  <Point X="2.01796934915" Y="-27.667527956267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.374161600739" Y="-23.715918024077" />
                  <Point X="2.000447213221" Y="-27.546169422091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.137953798439" Y="-23.74701581765" />
                  <Point X="2.017964687522" Y="-27.445864809808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.901748836253" Y="-23.778111904711" />
                  <Point X="2.060315748648" Y="-27.360481721877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.696913654869" Y="-23.79035912586" />
                  <Point X="2.10463113016" Y="-27.276278916709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.578261305926" Y="-23.75082247696" />
                  <Point X="2.157418355452" Y="-27.197166508839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.499392315062" Y="-23.687381574897" />
                  <Point X="2.23970689859" Y="-27.135780281072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453347760204" Y="-23.604217761897" />
                  <Point X="2.33803462382" Y="-27.084031366187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.424059288934" Y="-23.510985878139" />
                  <Point X="2.442436114902" Y="-27.03593193801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.437278982554" Y="-23.392212512111" />
                  <Point X="2.637261594351" Y="-27.042164723459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.622187331418" Y="-23.170278194414" />
                  <Point X="4.005891412436" Y="-27.753690310438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.223272950075" Y="-22.698279344764" />
                  <Point X="4.059358904279" Y="-27.674986647948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.175379351754" Y="-22.616226549163" />
                  <Point X="3.792119882384" Y="-27.403583071089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.127485820391" Y="-22.53417371333" />
                  <Point X="3.126342591551" Y="-26.892713543249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079592289029" Y="-22.452120877496" />
                  <Point X="2.922136611855" Y="-26.659184039144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.031698757666" Y="-22.370068041663" />
                  <Point X="2.86140529304" Y="-26.511862808591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.979610800487" Y="-22.290535471122" />
                  <Point X="2.865335653929" Y="-26.403394234933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.920853526255" Y="-22.215010230553" />
                  <Point X="2.87499980873" Y="-26.298370872233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.862096133368" Y="-22.139485061281" />
                  <Point X="2.904550779074" Y="-26.205296713831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803338236881" Y="-22.063960194601" />
                  <Point X="2.960483954158" Y="-26.128074583301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.744580340394" Y="-21.988435327922" />
                  <Point X="3.021935053306" Y="-26.05416795604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.082949193013" Y="-22.27515325597" />
                  <Point X="3.095079433548" Y="-25.987287360895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.939907727492" Y="-22.250271066754" />
                  <Point X="3.208254994358" Y="-25.944459925687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.85963333964" Y="-22.187674612395" />
                  <Point X="3.343709306926" Y="-25.915018915051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.793963100896" Y="-22.116303099963" />
                  <Point X="3.541514396307" Y="-25.923042030767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754568001563" Y="-22.029143890999" />
                  <Point X="3.777720481169" Y="-25.9541387924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75192051033" Y="-21.919904491485" />
                  <Point X="4.013926715279" Y="-25.98523564371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.776685900802" Y="-21.7941937709" />
                  <Point X="4.250133290839" Y="-26.016332700184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.870722171583" Y="-21.626860906292" />
                  <Point X="4.4863398664" Y="-26.047429756659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.968699020308" Y="-21.457160303582" />
                  <Point X="3.444302898715" Y="-25.310480606471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.765911354735" Y="-25.503722462439" />
                  <Point X="4.72254644196" Y="-26.078526813133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.053358679205" Y="-21.295461475794" />
                  <Point X="3.36652406362" Y="-25.152916194732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.098822313685" Y="-25.59292537458" />
                  <Point X="4.749548958675" Y="-25.983921389306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.972316127767" Y="-21.233326580683" />
                  <Point X="3.348824055462" Y="-25.031450784138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.431733272636" Y="-25.68212828672" />
                  <Point X="4.768624132096" Y="-25.88455273708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.891273157042" Y="-21.171191937506" />
                  <Point X="3.362811899812" Y="-24.929025356218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.764643033968" Y="-25.771330479259" />
                  <Point X="4.783945281331" Y="-25.782928439558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.810230186317" Y="-21.109057294328" />
                  <Point X="3.389630896457" Y="-24.834309662409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.729187215592" Y="-21.04692265115" />
                  <Point X="3.442037039913" Y="-24.754968277471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.64810167464" Y="-20.984813586746" />
                  <Point X="3.506130484955" Y="-24.682649331799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.552264240577" Y="-20.931568353967" />
                  <Point X="3.595009538554" Y="-24.625223082227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.456426806514" Y="-20.878323121188" />
                  <Point X="3.698875657772" Y="-24.576801970181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.360589834493" Y="-20.825077610788" />
                  <Point X="3.826440567344" Y="-24.542620527977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.264752923646" Y="-20.771832063629" />
                  <Point X="3.954005476916" Y="-24.508439085773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.765494862041" Y="-20.960986398844" />
                  <Point X="3.102734541647" Y="-23.886113731913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351907174443" Y="-24.035831754299" />
                  <Point X="4.081570386487" Y="-24.47425764357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.653942833235" Y="-20.917183447191" />
                  <Point X="3.020939234298" Y="-23.72613598017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.545456993877" Y="-24.041298045881" />
                  <Point X="4.209135611751" Y="-24.440076391052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.564494960853" Y="-20.860098978425" />
                  <Point X="3.000913545906" Y="-23.603273159911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696758243892" Y="-24.02137883589" />
                  <Point X="4.336701015854" Y="-24.405895245993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.512200370085" Y="-20.780690565871" />
                  <Point X="3.017528184312" Y="-23.502426069092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.848059493908" Y="-24.001459625898" />
                  <Point X="4.464266419956" Y="-24.371714100933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.481448444451" Y="-20.688338014208" />
                  <Point X="2.074360751125" Y="-22.824883728606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.405478067039" Y="-23.023839084017" />
                  <Point X="3.049626876225" Y="-23.41088273625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.999360743924" Y="-23.981540415907" />
                  <Point X="4.591831824059" Y="-24.337532955873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462341294865" Y="-20.588988575201" />
                  <Point X="2.019522250156" Y="-22.681103260232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.554360033382" Y="-23.00246622174" />
                  <Point X="3.10395621821" Y="-23.332696925571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.150661993939" Y="-23.961621205916" />
                  <Point X="4.719397228161" Y="-24.303351810813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474885282191" Y="-20.470621214476" />
                  <Point X="2.015291905292" Y="-22.567731239862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662696654692" Y="-22.956731258348" />
                  <Point X="3.167939859725" Y="-23.260312003284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.301963243955" Y="-23.941701995924" />
                  <Point X="4.77445528942" Y="-24.225603858848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.388272566995" Y="-20.411833211408" />
                  <Point X="2.038277525306" Y="-22.470712220997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.75676311253" Y="-22.902421915699" />
                  <Point X="3.248735128368" Y="-23.19802852568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.453264468989" Y="-23.921782770922" />
                  <Point X="4.755418915344" Y="-24.103335478601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.262503282068" Y="-20.376572849069" />
                  <Point X="2.076144142677" Y="-22.382634607416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.850829570368" Y="-22.84811257305" />
                  <Point X="3.329740013371" Y="-23.135870998292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.604565345539" Y="-23.90186333653" />
                  <Point X="4.725620850243" Y="-23.974600822023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.13673399714" Y="-20.34131248673" />
                  <Point X="2.123651349945" Y="-22.300349644648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.944896028206" Y="-22.793803230401" />
                  <Point X="3.410744898375" Y="-23.073713470904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.010964712212" Y="-20.306052124391" />
                  <Point X="-0.20128650254" Y="-20.792555874668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.002306211325" Y="-20.91488671875" />
                  <Point X="2.171158557213" Y="-22.21806468188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.038962486044" Y="-22.739493887752" />
                  <Point X="3.491749783378" Y="-23.011555943516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.874738179492" Y="-20.277075110433" />
                  <Point X="-0.24840410199" Y="-20.65341459196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.129221211095" Y="-20.88031477134" />
                  <Point X="2.218665764482" Y="-22.135779719112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.133028943882" Y="-22.685184545103" />
                  <Point X="3.572754776905" Y="-22.949398481335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.720358677373" Y="-20.259005500906" />
                  <Point X="-0.283799470712" Y="-20.521316736063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.200594971553" Y="-20.812370280496" />
                  <Point X="2.26617297175" Y="-22.053494756344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.22709540172" Y="-22.630875202453" />
                  <Point X="3.653760110212" Y="-22.887241223315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.565977405662" Y="-20.240936954658" />
                  <Point X="-0.319194953114" Y="-20.389218811861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.241271938204" Y="-20.725981295122" />
                  <Point X="2.313680179019" Y="-21.971209793576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.321161859558" Y="-22.576565859804" />
                  <Point X="3.734765443519" Y="-22.825083965295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.411595606358" Y="-20.222868725419" />
                  <Point X="-0.354590461726" Y="-20.25712087191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.266850602909" Y="-20.630520334696" />
                  <Point X="2.361187391941" Y="-21.888924834205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.415228317396" Y="-22.522256517155" />
                  <Point X="3.815770776825" Y="-22.762926707274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.292429267615" Y="-20.535059374269" />
                  <Point X="2.408694636995" Y="-21.806639894141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.509294775234" Y="-22.467947174506" />
                  <Point X="3.896776110132" Y="-22.700769449254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.31800793232" Y="-20.439598413842" />
                  <Point X="2.456201882048" Y="-21.724354954077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603361233072" Y="-22.413637831857" />
                  <Point X="3.977781443439" Y="-22.638612191234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.343586597025" Y="-20.344137453416" />
                  <Point X="2.503709127102" Y="-21.642070014013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.697427901265" Y="-22.359328615602" />
                  <Point X="4.058786776745" Y="-22.576454933213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.369165239905" Y="-20.248676479874" />
                  <Point X="2.551216372156" Y="-21.559785073949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.791494645145" Y="-22.305019444824" />
                  <Point X="4.133100179892" Y="-22.510276757895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.522754816223" Y="-20.230132235042" />
                  <Point X="2.59872361721" Y="-21.477500133885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.885561389025" Y="-22.250710274046" />
                  <Point X="4.022309793937" Y="-22.332877005272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746142137716" Y="-20.253526706582" />
                  <Point X="2.646230862264" Y="-21.395215193821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.023414493634" Y="-20.309298573262" />
                  <Point X="2.693738107318" Y="-21.312930253757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.331768409575" Y="-20.383746125339" />
                  <Point X="2.741245352372" Y="-21.230645313693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.735088270007" Y="-20.515254973608" />
                  <Point X="2.788752597426" Y="-21.148360373629" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998373046875" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.605792175293" Y="-29.03815234375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.40944619751" Y="-28.442482421875" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.274335754395" Y="-28.2663984375" />
                  <Point X="0.189423522949" Y="-28.240044921875" />
                  <Point X="0.020976625443" Y="-28.187765625" />
                  <Point X="-0.008664756775" Y="-28.187765625" />
                  <Point X="-0.093576835632" Y="-28.214119140625" />
                  <Point X="-0.262023742676" Y="-28.2663984375" />
                  <Point X="-0.288278991699" Y="-28.285642578125" />
                  <Point X="-0.343151611328" Y="-28.364705078125" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.725555236816" Y="-29.5310625" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-0.902438964844" Y="-29.9764609375" />
                  <Point X="-1.10023046875" Y="-29.938068359375" />
                  <Point X="-1.197888793945" Y="-29.91294140625" />
                  <Point X="-1.351589599609" Y="-29.873396484375" />
                  <Point X="-1.325311157227" Y="-29.67379296875" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.32996862793" Y="-29.43277734375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.464458984375" Y="-29.1340703125" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.752998291016" Y="-28.978962890625" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.076334960938" Y="-29.03155859375" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.409256835938" Y="-29.35215625" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.565889160156" Y="-29.34713671875" />
                  <Point X="-2.855837890625" Y="-29.167609375" />
                  <Point X="-2.991041259766" Y="-29.063505859375" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.755527832031" Y="-28.061255859375" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499763183594" Y="-27.597587890625" />
                  <Point X="-2.513984863281" Y="-27.5687578125" />
                  <Point X="-2.531327148438" Y="-27.55141796875" />
                  <Point X="-2.560156494141" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.448946289062" Y="-28.038412109375" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.932631347656" Y="-28.148083984375" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.258636230469" Y="-27.68458984375" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.600320800781" Y="-26.758109375" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.152535644531" Y="-26.411083984375" />
                  <Point X="-3.143239990234" Y="-26.386044921875" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.136651611328" Y="-26.350052734375" />
                  <Point X="-3.148654785156" Y="-26.321072265625" />
                  <Point X="-3.170037109375" Y="-26.3054140625" />
                  <Point X="-3.187644287109" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-4.3017421875" Y="-26.431046875" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.837801269531" Y="-26.361939453125" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.9530390625" Y="-25.83187890625" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.054741699219" Y="-25.261892578125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.532594238281" Y="-25.114970703125" />
                  <Point X="-3.514142578125" Y="-25.1021640625" />
                  <Point X="-3.502324462891" Y="-25.090646484375" />
                  <Point X="-3.491798828125" Y="-25.065919921875" />
                  <Point X="-3.485648193359" Y="-25.046103515625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.488748046875" Y="-25.006470703125" />
                  <Point X="-3.494898681641" Y="-24.98665234375" />
                  <Point X="-3.50232421875" Y="-24.9719140625" />
                  <Point X="-3.523443847656" Y="-24.95394140625" />
                  <Point X="-3.541895507813" Y="-24.941134765625" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.543952148438" Y="-24.6695859375" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.975385742188" Y="-24.393787109375" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.866020019531" Y="-23.8130703125" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.109727539062" Y="-23.559091796875" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731704345703" Y="-23.6041328125" />
                  <Point X="-3.711117675781" Y="-23.597642578125" />
                  <Point X="-3.670278320312" Y="-23.584765625" />
                  <Point X="-3.651534179688" Y="-23.5739453125" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.630859375" Y="-23.5362734375" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316162109" Y="-23.45448828125" />
                  <Point X="-3.626283203125" Y="-23.435341796875" />
                  <Point X="-3.646055908203" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.2258203125" Y="-22.946583984375" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.384373535156" Y="-22.597375" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-4.023262451172" Y="-22.03721875" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.378085449219" Y="-21.946658203125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138515136719" Y="-22.07956640625" />
                  <Point X="-3.109843505859" Y="-22.08207421875" />
                  <Point X="-3.052965820312" Y="-22.08705078125" />
                  <Point X="-3.031507324219" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.992901611328" Y="-22.052244140625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.940582763672" Y="-21.94348828125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.202813720703" Y="-21.431662109375" />
                  <Point X="-3.307278808594" Y="-21.25072265625" />
                  <Point X="-3.143638671875" Y="-21.12526171875" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.537496337891" Y="-20.7060078125" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.034847290039" Y="-20.625083984375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246948242" Y="-20.726337890625" />
                  <Point X="-1.919335693359" Y="-20.742951171875" />
                  <Point X="-1.856030761719" Y="-20.77590625" />
                  <Point X="-1.835124023438" Y="-20.781509765625" />
                  <Point X="-1.81380847168" Y="-20.77775" />
                  <Point X="-1.780570678711" Y="-20.763982421875" />
                  <Point X="-1.714634399414" Y="-20.736669921875" />
                  <Point X="-1.696905029297" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.675265136719" Y="-20.671201171875" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.680424682617" Y="-20.36504296875" />
                  <Point X="-1.689137695313" Y="-20.298859375" />
                  <Point X="-1.473960449219" Y="-20.23853125" />
                  <Point X="-0.968083251953" Y="-20.096703125" />
                  <Point X="-0.706989746094" Y="-20.06614453125" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.106905418396" Y="-20.447390625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282123566" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594043732" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.182931808472" Y="-20.20960546875" />
                  <Point X="0.2366484375" Y="-20.009130859375" />
                  <Point X="0.418510375977" Y="-20.028177734375" />
                  <Point X="0.860210021973" Y="-20.074435546875" />
                  <Point X="1.076225341797" Y="-20.126587890625" />
                  <Point X="1.508456176758" Y="-20.230943359375" />
                  <Point X="1.64905065918" Y="-20.2819375" />
                  <Point X="1.931044433594" Y="-20.38421875" />
                  <Point X="2.066995849609" Y="-20.447798828125" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.470047851562" Y="-20.651390625" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.856399414062" Y="-20.892404296875" />
                  <Point X="3.068739746094" Y="-21.043408203125" />
                  <Point X="2.518624267578" Y="-21.996236328125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.217656494141" Y="-22.53539453125" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.204850341797" Y="-22.630943359375" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.233079589844" Y="-22.72040625" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.274939941406" Y="-22.775796875" />
                  <Point X="2.296157470703" Y="-22.7901953125" />
                  <Point X="2.338248535156" Y="-22.818755859375" />
                  <Point X="2.360336425781" Y="-22.827021484375" />
                  <Point X="2.383603759766" Y="-22.829826171875" />
                  <Point X="2.429761230469" Y="-22.835392578125" />
                  <Point X="2.448665283203" Y="-22.8340546875" />
                  <Point X="2.475572998047" Y="-22.826859375" />
                  <Point X="2.528951660156" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.533251220703" Y="-22.234724609375" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.046900146484" Y="-22.041744140625" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.271650878906" Y="-22.3722421875" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.668809814453" Y="-23.11518359375" />
                  <Point X="3.288615966797" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.260005859375" Y="-23.441431640625" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.205906005859" Y="-23.534294921875" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.196701171875" Y="-23.637734375" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646484375" Y="-23.712046875" />
                  <Point X="3.231752685547" Y="-23.73652734375" />
                  <Point X="3.263704101562" Y="-23.785091796875" />
                  <Point X="3.280947509766" Y="-23.8011796875" />
                  <Point X="3.304287597656" Y="-23.814318359375" />
                  <Point X="3.350589599609" Y="-23.8403828125" />
                  <Point X="3.368565917969" Y="-23.846380859375" />
                  <Point X="3.400123291016" Y="-23.85055078125" />
                  <Point X="3.4627265625" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.41834375" Y="-23.734740234375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.872319824219" Y="-23.773939453125" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.960950683594" Y="-24.188390625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.176877929688" Y="-24.645423828125" />
                  <Point X="3.741167480469" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.698082763672" Y="-24.7851015625" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.622264892578" Y="-24.833072265625" />
                  <Point X="3.603662353516" Y="-24.85677734375" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.550784179688" Y="-24.95764453125" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538482910156" Y="-25.040685546875" />
                  <Point X="3.544683837891" Y="-25.0730625" />
                  <Point X="3.556985107422" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.585361572266" Y="-25.18246484375" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.636576171875" Y="-25.241908203125" />
                  <Point X="3.667580566406" Y="-25.259828125" />
                  <Point X="3.729086181641" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.605520019531" Y="-25.531990234375" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.985768066406" Y="-25.718755859375" />
                  <Point X="4.948432617188" Y="-25.96639453125" />
                  <Point X="4.920550292969" Y="-26.088580078125" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="3.917283447266" Y="-26.16415234375" />
                  <Point X="3.411981933594" Y="-26.09762890625" />
                  <Point X="3.394836669922" Y="-26.098341796875" />
                  <Point X="3.333986083984" Y="-26.111568359375" />
                  <Point X="3.213272460938" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.148665283203" Y="-26.19893359375" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.059086425781" Y="-26.371357421875" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.090036132812" Y="-26.569001953125" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.970584716797" Y="-27.301033203125" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.309379882813" Y="-27.631833984375" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.146470703125" Y="-27.8840703125" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.203493652344" Y="-27.519046875" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340332031" Y="-27.253314453125" />
                  <Point X="2.664918457031" Y="-27.240234375" />
                  <Point X="2.52125" Y="-27.2142890625" />
                  <Point X="2.489077880859" Y="-27.21924609375" />
                  <Point X="2.428913085938" Y="-27.25091015625" />
                  <Point X="2.309559814453" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.256935546875" Y="-27.39484765625" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.2022421875" Y="-27.618796875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.749537109375" Y="-28.67135546875" />
                  <Point X="2.986673828125" Y="-29.082087890625" />
                  <Point X="2.960184326172" Y="-29.101009765625" />
                  <Point X="2.835296142578" Y="-29.19021484375" />
                  <Point X="2.770830810547" Y="-29.23194140625" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="2.026598999023" Y="-28.43964453125" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549316406" Y="-27.980466796875" />
                  <Point X="1.599121948242" Y="-27.934546875" />
                  <Point X="1.45742590332" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.347686889648" Y="-27.84290625" />
                  <Point X="1.192718139648" Y="-27.857166015625" />
                  <Point X="1.165332763672" Y="-27.8685078125" />
                  <Point X="1.105012084961" Y="-27.9186640625" />
                  <Point X="0.985349060059" Y="-28.01816015625" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.950421203613" Y="-28.12896484375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.060002807617" Y="-29.420255859375" />
                  <Point X="1.127642578125" Y="-29.934029296875" />
                  <Point X="1.112498291016" Y="-29.937349609375" />
                  <Point X="0.994367858887" Y="-29.9632421875" />
                  <Point X="0.934788513184" Y="-29.97406640625" />
                  <Point X="0.860200500488" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#150" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.06476507889" Y="4.59683105088" Z="0.85" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.85" />
                  <Point X="-0.718957857503" Y="5.014795969364" Z="0.85" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.85" />
                  <Point X="-1.493614076544" Y="4.840892876226" Z="0.85" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.85" />
                  <Point X="-1.736152903915" Y="4.659713024202" Z="0.85" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.85" />
                  <Point X="-1.729106894088" Y="4.375115128102" Z="0.85" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.85" />
                  <Point X="-1.805861344817" Y="4.313492202684" Z="0.85" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.85" />
                  <Point X="-1.90240392213" Y="4.332679244185" Z="0.85" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.85" />
                  <Point X="-2.001335805635" Y="4.436634312762" Z="0.85" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.85" />
                  <Point X="-2.567935144678" Y="4.368979430084" Z="0.85" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.85" />
                  <Point X="-3.180216675305" Y="3.945697858686" Z="0.85" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.85" />
                  <Point X="-3.252270888989" Y="3.574618046943" Z="0.85" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.85" />
                  <Point X="-2.996548468626" Y="3.083435051646" Z="0.85" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.85" />
                  <Point X="-3.034412322705" Y="3.014391245181" Z="0.85" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.85" />
                  <Point X="-3.111641413961" Y="2.999016230547" Z="0.85" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.85" />
                  <Point X="-3.359241301354" Y="3.127923059757" Z="0.85" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.85" />
                  <Point X="-4.068881907746" Y="3.024764295656" Z="0.85" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.85" />
                  <Point X="-4.433711762955" Y="2.459110406031" Z="0.85" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.85" />
                  <Point X="-4.262414396013" Y="2.045027664976" Z="0.85" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.85" />
                  <Point X="-3.676789617806" Y="1.572851166465" Z="0.85" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.85" />
                  <Point X="-3.68320941337" Y="1.514142709315" Z="0.85" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.85" />
                  <Point X="-3.732309357755" Y="1.48132349399" Z="0.85" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.85" />
                  <Point X="-4.109356951002" Y="1.521761514183" Z="0.85" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.85" />
                  <Point X="-4.920435814624" Y="1.231287993596" Z="0.85" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.85" />
                  <Point X="-5.031003053085" Y="0.644811818872" Z="0.85" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.85" />
                  <Point X="-4.563049190107" Y="0.313397820839" Z="0.85" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.85" />
                  <Point X="-3.558109115856" Y="0.03626255419" Z="0.85" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.85" />
                  <Point X="-3.542657213796" Y="0.009989670713" Z="0.85" />
                  <Point X="-3.539556741714" Y="0" Z="0.85" />
                  <Point X="-3.545707387339" Y="-0.019817280348" Z="0.85" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.85" />
                  <Point X="-3.567259479267" Y="-0.042613428943" Z="0.85" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.85" />
                  <Point X="-4.073838598666" Y="-0.182314235917" Z="0.85" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.85" />
                  <Point X="-5.008690923142" Y="-0.807677398767" Z="0.85" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.85" />
                  <Point X="-4.892402164627" Y="-1.343033623139" Z="0.85" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.85" />
                  <Point X="-4.301371817419" Y="-1.44933931572" Z="0.85" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.85" />
                  <Point X="-3.201551828604" Y="-1.317226056841" Z="0.85" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.85" />
                  <Point X="-3.197798673519" Y="-1.342227468917" Z="0.85" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.85" />
                  <Point X="-3.63691467826" Y="-1.687161380421" Z="0.85" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.85" />
                  <Point X="-4.307735456737" Y="-2.678917876593" Z="0.85" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.85" />
                  <Point X="-3.97857330768" Y="-3.147086560516" Z="0.85" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.85" />
                  <Point X="-3.430102257471" Y="-3.050431848384" Z="0.85" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.85" />
                  <Point X="-2.561305254696" Y="-2.567025214436" Z="0.85" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.85" />
                  <Point X="-2.804985164784" Y="-3.004976125021" Z="0.85" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.85" />
                  <Point X="-3.027701085864" Y="-4.071843033646" Z="0.85" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.85" />
                  <Point X="-2.598366605909" Y="-4.358368736094" Z="0.85" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.85" />
                  <Point X="-2.375745007594" Y="-4.351313927576" Z="0.85" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.85" />
                  <Point X="-2.05471227956" Y="-4.041852694068" Z="0.85" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.85" />
                  <Point X="-1.762424312759" Y="-3.997575303746" Z="0.85" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.85" />
                  <Point X="-1.503582350718" Y="-4.140381574765" Z="0.85" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.85" />
                  <Point X="-1.385164046266" Y="-4.411250175723" Z="0.85" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.85" />
                  <Point X="-1.38103943237" Y="-4.63598637878" Z="0.85" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.85" />
                  <Point X="-1.216503479886" Y="-4.93008558148" Z="0.85" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.85" />
                  <Point X="-0.918122552761" Y="-4.994264383855" Z="0.85" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.85" />
                  <Point X="-0.683414990336" Y="-4.512723562155" Z="0.85" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.85" />
                  <Point X="-0.30823171002" Y="-3.361933676173" Z="0.85" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.85" />
                  <Point X="-0.084912199497" Y="-3.23059298161" Z="0.85" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.85" />
                  <Point X="0.168446879864" Y="-3.256519063678" Z="0.85" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.85" />
                  <Point X="0.362214149412" Y="-3.439712079595" Z="0.85" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.85" />
                  <Point X="0.551339870065" Y="-4.01981246766" Z="0.85" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.85" />
                  <Point X="0.93756929113" Y="-4.991981276846" Z="0.85" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.85" />
                  <Point X="1.117047549347" Y="-4.954908449519" Z="0.85" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.85" />
                  <Point X="1.103419060024" Y="-4.382450251299" Z="0.85" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.85" />
                  <Point X="0.993124408871" Y="-3.108303953805" Z="0.85" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.85" />
                  <Point X="1.130822980883" Y="-2.925830182279" Z="0.85" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.85" />
                  <Point X="1.346112455361" Y="-2.861415078711" Z="0.85" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.85" />
                  <Point X="1.565926497834" Y="-2.945324051681" Z="0.85" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.85" />
                  <Point X="1.980775247744" Y="-3.438800596198" Z="0.85" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.85" />
                  <Point X="2.791844125008" Y="-4.242635228706" Z="0.85" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.85" />
                  <Point X="2.98308996308" Y="-4.110416290426" Z="0.85" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.85" />
                  <Point X="2.786682304073" Y="-3.615075882917" Z="0.85" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.85" />
                  <Point X="2.245290435516" Y="-2.57862995786" Z="0.85" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.85" />
                  <Point X="2.29502594067" Y="-2.386854872922" Z="0.85" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.85" />
                  <Point X="2.446043505451" Y="-2.263875369297" Z="0.85" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.85" />
                  <Point X="2.649876845653" Y="-2.258157573975" Z="0.85" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.85" />
                  <Point X="3.17233771419" Y="-2.531067196201" Z="0.85" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.85" />
                  <Point X="4.181203378329" Y="-2.881566972815" Z="0.85" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.85" />
                  <Point X="4.345757404207" Y="-2.626838861257" Z="0.85" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.85" />
                  <Point X="3.994866778036" Y="-2.230084468654" Z="0.85" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.85" />
                  <Point X="3.12593805044" Y="-1.510682593318" Z="0.85" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.85" />
                  <Point X="3.102719967264" Y="-1.344658770871" Z="0.85" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.85" />
                  <Point X="3.180955277946" Y="-1.199619406464" Z="0.85" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.85" />
                  <Point X="3.338449359837" Y="-1.129146532404" Z="0.85" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.85" />
                  <Point X="3.904601299162" Y="-1.18244461594" Z="0.85" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.85" />
                  <Point X="4.963141487137" Y="-1.068423666151" Z="0.85" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.85" />
                  <Point X="5.029053671714" Y="-0.69492855952" Z="0.85" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.85" />
                  <Point X="4.612304821916" Y="-0.452413079377" Z="0.85" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.85" />
                  <Point X="3.686447035985" Y="-0.185259260811" Z="0.85" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.85" />
                  <Point X="3.618539316485" Y="-0.120314605928" Z="0.85" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.85" />
                  <Point X="3.587635590974" Y="-0.032378601655" Z="0.85" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.85" />
                  <Point X="3.59373585945" Y="0.064231929569" Z="0.85" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.85" />
                  <Point X="3.636840121915" Y="0.143634132647" Z="0.85" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.85" />
                  <Point X="3.716948378367" Y="0.202889583431" Z="0.85" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.85" />
                  <Point X="4.183662995391" Y="0.337558853392" Z="0.85" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.85" />
                  <Point X="5.004199732881" Y="0.850580359781" Z="0.85" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.85" />
                  <Point X="4.914744911502" Y="1.26916795898" Z="0.85" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.85" />
                  <Point X="4.405661490894" Y="1.346111876508" Z="0.85" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.85" />
                  <Point X="3.400518202178" Y="1.230297860237" Z="0.85" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.85" />
                  <Point X="3.322658789508" Y="1.260532498886" Z="0.85" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.85" />
                  <Point X="3.267367277928" Y="1.322235563184" Z="0.85" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.85" />
                  <Point X="3.239513709731" Y="1.403649742164" Z="0.85" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.85" />
                  <Point X="3.247902326301" Y="1.483519384148" Z="0.85" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.85" />
                  <Point X="3.293532713641" Y="1.559431351253" Z="0.85" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.85" />
                  <Point X="3.693092009895" Y="1.876428093179" Z="0.85" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.85" />
                  <Point X="4.308272287284" Y="2.684925720953" Z="0.85" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.85" />
                  <Point X="4.081329672193" Y="3.01873929263" Z="0.85" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.85" />
                  <Point X="3.502095373941" Y="2.839855749216" Z="0.85" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.85" />
                  <Point X="2.456499249959" Y="2.252724616044" Z="0.85" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.85" />
                  <Point X="2.383434232177" Y="2.251094905158" Z="0.85" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.85" />
                  <Point X="2.318075714535" Y="2.28246103712" Z="0.85" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.85" />
                  <Point X="2.268297563115" Y="2.338949145847" Z="0.85" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.85" />
                  <Point X="2.248334797601" Y="2.406324211655" Z="0.85" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.85" />
                  <Point X="2.259803326613" Y="2.482970325923" Z="0.85" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.85" />
                  <Point X="2.555769603643" Y="3.010043708014" Z="0.85" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.85" />
                  <Point X="2.879220615519" Y="4.179624908539" Z="0.85" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.85" />
                  <Point X="2.48906192472" Y="4.423092977446" Z="0.85" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.85" />
                  <Point X="2.082021263578" Y="4.628773126515" Z="0.85" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.85" />
                  <Point X="1.659943201527" Y="4.79634727604" Z="0.85" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.85" />
                  <Point X="1.08180389729" Y="4.953295411807" Z="0.85" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.85" />
                  <Point X="0.417562287376" Y="5.052830975473" Z="0.85" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.85" />
                  <Point X="0.128479482663" Y="4.834616455071" Z="0.85" />
                  <Point X="0" Y="4.355124473572" Z="0.85" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>