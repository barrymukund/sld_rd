<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#215" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3553" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563301513672" Y="-28.512521484375" />
                  <Point X="0.55772277832" Y="-28.49714453125" />
                  <Point X="0.542365356445" Y="-28.46737890625" />
                  <Point X="0.384357025146" Y="-28.239720703125" />
                  <Point X="0.37863772583" Y="-28.231478515625" />
                  <Point X="0.356758209229" Y="-28.20902734375" />
                  <Point X="0.33050012207" Y="-28.189779296875" />
                  <Point X="0.302494354248" Y="-28.17566796875" />
                  <Point X="0.057995937347" Y="-28.099787109375" />
                  <Point X="0.049151611328" Y="-28.0970390625" />
                  <Point X="0.020988721848" Y="-28.092767578125" />
                  <Point X="-0.008654577255" Y="-28.092765625" />
                  <Point X="-0.036820350647" Y="-28.09703515625" />
                  <Point X="-0.281328887939" Y="-28.172921875" />
                  <Point X="-0.290179473877" Y="-28.17566796875" />
                  <Point X="-0.318176757812" Y="-28.1897734375" />
                  <Point X="-0.344435150146" Y="-28.209017578125" />
                  <Point X="-0.366322692871" Y="-28.2314765625" />
                  <Point X="-0.524330871582" Y="-28.45913671875" />
                  <Point X="-0.529754089355" Y="-28.467935546875" />
                  <Point X="-0.542694641113" Y="-28.491744140625" />
                  <Point X="-0.55099017334" Y="-28.5125234375" />
                  <Point X="-0.564983703613" Y="-28.564748046875" />
                  <Point X="-0.916584533691" Y="-29.87694140625" />
                  <Point X="-1.073142456055" Y="-29.846552734375" />
                  <Point X="-1.079356079102" Y="-29.845345703125" />
                  <Point X="-1.24641796875" Y="-29.80236328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.274921630859" Y="-29.222564453125" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287936401367" Y="-29.182970703125" />
                  <Point X="-1.304007202148" Y="-29.1551328125" />
                  <Point X="-1.323643554688" Y="-29.131203125" />
                  <Point X="-1.548755859375" Y="-28.93378515625" />
                  <Point X="-1.556904418945" Y="-28.926638671875" />
                  <Point X="-1.583188354492" Y="-28.910294921875" />
                  <Point X="-1.612886108398" Y="-28.897994140625" />
                  <Point X="-1.643026977539" Y="-28.890966796875" />
                  <Point X="-1.941801147461" Y="-28.871384765625" />
                  <Point X="-1.952615844727" Y="-28.87067578125" />
                  <Point X="-1.983412963867" Y="-28.873708984375" />
                  <Point X="-2.014462524414" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.291612304688" Y="-29.061146484375" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.317627441406" Y="-29.0816953125" />
                  <Point X="-2.337372802734" Y="-29.1030703125" />
                  <Point X="-2.342958984375" Y="-29.109701171875" />
                  <Point X="-2.480149414062" Y="-29.2884921875" />
                  <Point X="-2.792641113281" Y="-29.09500390625" />
                  <Point X="-2.801688964844" Y="-29.08940234375" />
                  <Point X="-3.104721923828" Y="-28.856078125" />
                  <Point X="-2.423759033203" Y="-27.676615234375" />
                  <Point X="-2.412859130859" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616126953125" />
                  <Point X="-2.405575439453" Y="-27.585193359375" />
                  <Point X="-2.414559570312" Y="-27.555576171875" />
                  <Point X="-2.428776855469" Y="-27.52674609375" />
                  <Point X="-2.446805419922" Y="-27.501587890625" />
                  <Point X="-2.463547851562" Y="-27.484845703125" />
                  <Point X="-2.4641875" Y="-27.48420703125" />
                  <Point X="-2.489371582031" Y="-27.466171875" />
                  <Point X="-2.518194335938" Y="-27.451974609375" />
                  <Point X="-2.547802001953" Y="-27.443005859375" />
                  <Point X="-2.578721435547" Y="-27.444025390625" />
                  <Point X="-2.610232666016" Y="-27.450298828125" />
                  <Point X="-2.639184326172" Y="-27.46119921875" />
                  <Point X="-2.684226806641" Y="-27.487205078125" />
                  <Point X="-3.818024414062" Y="-28.1418046875" />
                  <Point X="-4.075694580078" Y="-27.80327734375" />
                  <Point X="-4.082852783203" Y="-27.793873046875" />
                  <Point X="-4.306142089844" Y="-27.419451171875" />
                  <Point X="-3.105952148438" Y="-26.498513671875" />
                  <Point X="-3.084576171875" Y="-26.47559375" />
                  <Point X="-3.066611083984" Y="-26.4484609375" />
                  <Point X="-3.053855957031" Y="-26.41983203125" />
                  <Point X="-3.046420654297" Y="-26.391123046875" />
                  <Point X="-3.046153564453" Y="-26.390091796875" />
                  <Point X="-3.043348876953" Y="-26.35967578125" />
                  <Point X="-3.045554199219" Y="-26.328001953125" />
                  <Point X="-3.052553466797" Y="-26.298251953125" />
                  <Point X="-3.068636230469" Y="-26.272263671875" />
                  <Point X="-3.089469482422" Y="-26.2483046875" />
                  <Point X="-3.112969482422" Y="-26.22876953125" />
                  <Point X="-3.138527099609" Y="-26.2137265625" />
                  <Point X="-3.139557861328" Y="-26.21312109375" />
                  <Point X="-3.168825927734" Y="-26.201916015625" />
                  <Point X="-3.200662597656" Y="-26.19546484375" />
                  <Point X="-3.231926513672" Y="-26.194384765625" />
                  <Point X="-3.288788574219" Y="-26.201869140625" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.831273925781" Y="-26.003630859375" />
                  <Point X="-4.834076660156" Y="-25.992658203125" />
                  <Point X="-4.892424316406" Y="-25.58469921875" />
                  <Point X="-3.532866943359" Y="-25.22040625" />
                  <Point X="-3.517493408203" Y="-25.214828125" />
                  <Point X="-3.4877265625" Y="-25.19946875" />
                  <Point X="-3.460956054688" Y="-25.180888671875" />
                  <Point X="-3.460015380859" Y="-25.180236328125" />
                  <Point X="-3.437565917969" Y="-25.1583671875" />
                  <Point X="-3.418292236328" Y="-25.132087890625" />
                  <Point X="-3.404167236328" Y="-25.104064453125" />
                  <Point X="-3.395239257812" Y="-25.075298828125" />
                  <Point X="-3.394916259766" Y="-25.0742578125" />
                  <Point X="-3.390646728516" Y="-25.04609375" />
                  <Point X="-3.390647949219" Y="-25.01645703125" />
                  <Point X="-3.394917236328" Y="-24.98830078125" />
                  <Point X="-3.403844970703" Y="-24.95953515625" />
                  <Point X="-3.404156005859" Y="-24.958533203125" />
                  <Point X="-3.418260498047" Y="-24.930525390625" />
                  <Point X="-3.43751171875" Y="-24.90425" />
                  <Point X="-3.459979248047" Y="-24.8823515625" />
                  <Point X="-3.486734130859" Y="-24.863783203125" />
                  <Point X="-3.4999453125" Y="-24.856109375" />
                  <Point X="-3.532875732422" Y="-24.842150390625" />
                  <Point X="-3.584708251953" Y="-24.82826171875" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.826293457031" Y="-24.0352265625" />
                  <Point X="-4.82448828125" Y="-24.023029296875" />
                  <Point X="-4.703551269531" Y="-23.576734375" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744984863281" Y="-23.700658203125" />
                  <Point X="-3.723424316406" Y="-23.698771484375" />
                  <Point X="-3.703140869141" Y="-23.694736328125" />
                  <Point X="-3.643860595703" Y="-23.676046875" />
                  <Point X="-3.64164453125" Y="-23.67534765625" />
                  <Point X="-3.622726318359" Y="-23.667013671875" />
                  <Point X="-3.603996582031" Y="-23.65619140625" />
                  <Point X="-3.587328125" Y="-23.643962890625" />
                  <Point X="-3.573699707031" Y="-23.628416015625" />
                  <Point X="-3.561294433594" Y="-23.6106953125" />
                  <Point X="-3.551351318359" Y="-23.5925703125" />
                  <Point X="-3.527567382813" Y="-23.535150390625" />
                  <Point X="-3.526710693359" Y="-23.53308203125" />
                  <Point X="-3.520919677734" Y="-23.51322265625" />
                  <Point X="-3.517158691406" Y="-23.491904296875" />
                  <Point X="-3.515803955078" Y="-23.471259765625" />
                  <Point X="-3.518949951172" Y="-23.450814453125" />
                  <Point X="-3.524552001953" Y="-23.429904296875" />
                  <Point X="-3.532049560547" Y="-23.410623046875" />
                  <Point X="-3.560750488281" Y="-23.35548828125" />
                  <Point X="-3.561821777344" Y="-23.35343359375" />
                  <Point X="-3.573295410156" Y="-23.336275390625" />
                  <Point X="-3.587201171875" Y="-23.31970703125" />
                  <Point X="-3.602134521484" Y="-23.305412109375" />
                  <Point X="-3.631865722656" Y="-23.28259765625" />
                  <Point X="-4.351860351563" Y="-22.730126953125" />
                  <Point X="-4.088166992188" Y="-22.278357421875" />
                  <Point X="-4.081149414062" Y="-22.266333984375" />
                  <Point X="-3.75050390625" Y="-21.841337890625" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187726806641" Y="-22.163658203125" />
                  <Point X="-3.167088867188" Y="-22.170166015625" />
                  <Point X="-3.146798339844" Y="-22.174203125" />
                  <Point X="-3.064237548828" Y="-22.18142578125" />
                  <Point X="-3.061249023438" Y="-22.1816875" />
                  <Point X="-3.040582519531" Y="-22.18123828125" />
                  <Point X="-3.019122802734" Y="-22.178416015625" />
                  <Point X="-2.999026367188" Y="-22.1735" />
                  <Point X="-2.980470214844" Y="-22.1643515625" />
                  <Point X="-2.962214599609" Y="-22.15272265625" />
                  <Point X="-2.946079589844" Y="-22.1397734375" />
                  <Point X="-2.887477294922" Y="-22.081171875" />
                  <Point X="-2.885355957031" Y="-22.07905078125" />
                  <Point X="-2.872411376953" Y="-22.062921875" />
                  <Point X="-2.860781005859" Y="-22.04466796875" />
                  <Point X="-2.851631347656" Y="-22.0261171875" />
                  <Point X="-2.846713623047" Y="-22.006025390625" />
                  <Point X="-2.843887451172" Y="-21.98456640625" />
                  <Point X="-2.843435791016" Y="-21.9638828125" />
                  <Point X="-2.850658935547" Y="-21.881322265625" />
                  <Point X="-2.850920410156" Y="-21.87833203125" />
                  <Point X="-2.854954833984" Y="-21.858046875" />
                  <Point X="-2.861463378906" Y="-21.83740234375" />
                  <Point X="-2.869795898438" Y="-21.81846484375" />
                  <Point X="-2.882970703125" Y="-21.795646484375" />
                  <Point X="-3.183332519531" Y="-21.27540625" />
                  <Point X="-2.712835693359" Y="-20.9146796875" />
                  <Point X="-2.700616699219" Y="-20.905310546875" />
                  <Point X="-2.167036376953" Y="-20.608865234375" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028891723633" Y="-20.78519921875" />
                  <Point X="-2.012311889648" Y="-20.799111328125" />
                  <Point X="-1.995114501953" Y="-20.810603515625" />
                  <Point X="-1.903233520508" Y="-20.858435546875" />
                  <Point X="-1.899928710938" Y="-20.86015625" />
                  <Point X="-1.880637939453" Y="-20.867662109375" />
                  <Point X="-1.859727050781" Y="-20.873267578125" />
                  <Point X="-1.839281738281" Y="-20.87641796875" />
                  <Point X="-1.818639404297" Y="-20.87506640625" />
                  <Point X="-1.797318969727" Y="-20.87130859375" />
                  <Point X="-1.777453613281" Y="-20.865517578125" />
                  <Point X="-1.681744140625" Y="-20.825873046875" />
                  <Point X="-1.678273803711" Y="-20.824435546875" />
                  <Point X="-1.660131835938" Y="-20.814482421875" />
                  <Point X="-1.64240637207" Y="-20.802068359375" />
                  <Point X="-1.626859619141" Y="-20.788431640625" />
                  <Point X="-1.614631835938" Y="-20.77175390625" />
                  <Point X="-1.603811889648" Y="-20.753013671875" />
                  <Point X="-1.59548059082" Y="-20.734080078125" />
                  <Point X="-1.564328857422" Y="-20.635279296875" />
                  <Point X="-1.563201416016" Y="-20.631703125" />
                  <Point X="-1.559166259766" Y="-20.61141796875" />
                  <Point X="-1.557279296875" Y="-20.58985546875" />
                  <Point X="-1.55773046875" Y="-20.569173828125" />
                  <Point X="-1.559228393555" Y="-20.557796875" />
                  <Point X="-1.584202026367" Y="-20.368103515625" />
                  <Point X="-0.965442443848" Y="-20.194625" />
                  <Point X="-0.949620666504" Y="-20.1901875" />
                  <Point X="-0.294711425781" Y="-20.113541015625" />
                  <Point X="-0.133903366089" Y="-20.713685546875" />
                  <Point X="-0.121129745483" Y="-20.741876953125" />
                  <Point X="-0.103271606445" Y="-20.768603515625" />
                  <Point X="-0.082114006042" Y="-20.791193359375" />
                  <Point X="-0.054818138123" Y="-20.805783203125" />
                  <Point X="-0.024380064011" Y="-20.816115234375" />
                  <Point X="0.006155911922" Y="-20.82115625" />
                  <Point X="0.036691894531" Y="-20.816115234375" />
                  <Point X="0.067129966736" Y="-20.805783203125" />
                  <Point X="0.094425949097" Y="-20.791193359375" />
                  <Point X="0.115583587646" Y="-20.768603515625" />
                  <Point X="0.133441726685" Y="-20.741876953125" />
                  <Point X="0.146215194702" Y="-20.713685546875" />
                  <Point X="0.152965774536" Y="-20.6884921875" />
                  <Point X="0.307419586182" Y="-20.1120625" />
                  <Point X="0.830229675293" Y="-20.166814453125" />
                  <Point X="0.844029968262" Y="-20.168259765625" />
                  <Point X="1.466010742188" Y="-20.31842578125" />
                  <Point X="1.481038452148" Y="-20.3220546875" />
                  <Point X="1.885819580078" Y="-20.46887109375" />
                  <Point X="1.894645263672" Y="-20.472072265625" />
                  <Point X="2.286109375" Y="-20.655146484375" />
                  <Point X="2.294568115234" Y="-20.6591015625" />
                  <Point X="2.672769042969" Y="-20.879443359375" />
                  <Point X="2.680978027344" Y="-20.884224609375" />
                  <Point X="2.943259765625" Y="-21.07074609375" />
                  <Point X="2.147581298828" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.4839453125" />
                  <Point X="2.112357177734" Y="-22.561427734375" />
                  <Point X="2.109568603516" Y="-22.576873046875" />
                  <Point X="2.107480957031" Y="-22.598578125" />
                  <Point X="2.107727783203" Y="-22.619046875" />
                  <Point X="2.115806884766" Y="-22.686046875" />
                  <Point X="2.116099121094" Y="-22.688470703125" />
                  <Point X="2.121441162109" Y="-22.710390625" />
                  <Point X="2.12970703125" Y="-22.73248046875" />
                  <Point X="2.140070556641" Y="-22.75252734375" />
                  <Point X="2.181527587891" Y="-22.813625" />
                  <Point X="2.191407714844" Y="-22.8258671875" />
                  <Point X="2.206208740234" Y="-22.84137890625" />
                  <Point X="2.221598632812" Y="-22.854408203125" />
                  <Point X="2.282695556641" Y="-22.895865234375" />
                  <Point X="2.284891357422" Y="-22.89735546875" />
                  <Point X="2.304918457031" Y="-22.9077109375" />
                  <Point X="2.3270234375" Y="-22.91598828125" />
                  <Point X="2.348965820312" Y="-22.921337890625" />
                  <Point X="2.415965576172" Y="-22.929416015625" />
                  <Point X="2.431986328125" Y="-22.929986328125" />
                  <Point X="2.453313232422" Y="-22.92894140625" />
                  <Point X="2.473207519531" Y="-22.925830078125" />
                  <Point X="2.550689208984" Y="-22.905109375" />
                  <Point X="2.5603828125" Y="-22.901951171875" />
                  <Point X="2.588534179688" Y="-22.889853515625" />
                  <Point X="2.640667724609" Y="-22.85975390625" />
                  <Point X="3.967325683594" Y="-22.09380859375" />
                  <Point X="4.118401367188" Y="-22.30376953125" />
                  <Point X="4.123276855469" Y="-22.310544921875" />
                  <Point X="4.26219921875" Y="-22.540115234375" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221422363281" Y="-23.33976171875" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.148213134766" Y="-23.4311171875" />
                  <Point X="3.139863525391" Y="-23.4440625" />
                  <Point X="3.129371582031" Y="-23.463654296875" />
                  <Point X="3.121629150391" Y="-23.48291796875" />
                  <Point X="3.100857177734" Y="-23.557193359375" />
                  <Point X="3.100105224609" Y="-23.5598828125" />
                  <Point X="3.096651611328" Y="-23.582181640625" />
                  <Point X="3.095835693359" Y="-23.60574609375" />
                  <Point X="3.097738769531" Y="-23.62823046875" />
                  <Point X="3.114790527344" Y="-23.71087109375" />
                  <Point X="3.119134765625" Y="-23.725705078125" />
                  <Point X="3.126950683594" Y="-23.746076171875" />
                  <Point X="3.136282226562" Y="-23.764259765625" />
                  <Point X="3.182660888672" Y="-23.83475390625" />
                  <Point X="3.184345703125" Y="-23.837314453125" />
                  <Point X="3.198907226562" Y="-23.85456640625" />
                  <Point X="3.21614453125" Y="-23.870646484375" />
                  <Point X="3.234345458984" Y="-23.88396484375" />
                  <Point X="3.3015546875" Y="-23.921798828125" />
                  <Point X="3.315900634766" Y="-23.928369140625" />
                  <Point X="3.336309570312" Y="-23.935736328125" />
                  <Point X="3.356118164062" Y="-23.9405625" />
                  <Point X="3.446989501953" Y="-23.952572265625" />
                  <Point X="3.456898193359" Y="-23.953357421875" />
                  <Point X="3.488203125" Y="-23.953015625" />
                  <Point X="3.5377265625" Y="-23.94649609375" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.843844238281" Y="-24.058599609375" />
                  <Point X="4.845935546875" Y="-24.067189453125" />
                  <Point X="4.890864746094" Y="-24.35576171875" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704788085938" Y="-24.674416015625" />
                  <Point X="3.681546875" Y="-24.68493359375" />
                  <Point X="3.592268554688" Y="-24.736537109375" />
                  <Point X="3.579820800781" Y="-24.74512109375" />
                  <Point X="3.562276611328" Y="-24.759408203125" />
                  <Point X="3.547530517578" Y="-24.774421875" />
                  <Point X="3.493963623047" Y="-24.8426796875" />
                  <Point X="3.492024658203" Y="-24.845150390625" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.470527587891" Y="-24.88589453125" />
                  <Point X="3.463681152344" Y="-24.90739453125" />
                  <Point X="3.445825439453" Y="-25.000630859375" />
                  <Point X="3.444170166016" Y="-25.015732421875" />
                  <Point X="3.443523681641" Y="-25.037919921875" />
                  <Point X="3.445179199219" Y="-25.058556640625" />
                  <Point X="3.463034912109" Y="-25.151791015625" />
                  <Point X="3.463681396484" Y="-25.155166015625" />
                  <Point X="3.470527587891" Y="-25.176666015625" />
                  <Point X="3.480301025391" Y="-25.19812890625" />
                  <Point X="3.492024658203" Y="-25.21741015625" />
                  <Point X="3.545591552734" Y="-25.28566796875" />
                  <Point X="3.556147460938" Y="-25.297060546875" />
                  <Point X="3.572398925781" Y="-25.311951171875" />
                  <Point X="3.589036865234" Y="-25.32415625" />
                  <Point X="3.678315185547" Y="-25.375759765625" />
                  <Point X="3.686934326172" Y="-25.380171875" />
                  <Point X="3.716581787109" Y="-25.39215234375" />
                  <Point X="3.761997070312" Y="-25.4043203125" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.856190917969" Y="-25.9409765625" />
                  <Point X="4.855021484375" Y="-25.948734375" />
                  <Point X="4.801174316406" Y="-26.18469921875" />
                  <Point X="3.424375244141" Y="-26.00344140625" />
                  <Point X="3.408036376953" Y="-26.0027109375" />
                  <Point X="3.374656738281" Y="-26.005509765625" />
                  <Point X="3.199435058594" Y="-26.04359375" />
                  <Point X="3.193092529297" Y="-26.04497265625" />
                  <Point X="3.163968505859" Y="-26.056599609375" />
                  <Point X="3.136144042969" Y="-26.0734921875" />
                  <Point X="3.112396728516" Y="-26.0939609375" />
                  <Point X="3.006494140625" Y="-26.221330078125" />
                  <Point X="3.002665283203" Y="-26.22593359375" />
                  <Point X="2.987940673828" Y="-26.25031640625" />
                  <Point X="2.976592285156" Y="-26.27770703125" />
                  <Point X="2.969757324219" Y="-26.305365234375" />
                  <Point X="2.954577880859" Y="-26.47032421875" />
                  <Point X="2.954028320312" Y="-26.476296875" />
                  <Point X="2.956347900391" Y="-26.507568359375" />
                  <Point X="2.964081054688" Y="-26.53919140625" />
                  <Point X="2.976451904297" Y="-26.568" />
                  <Point X="3.073422119141" Y="-26.718830078125" />
                  <Point X="3.080464111328" Y="-26.728408203125" />
                  <Point X="3.095592773438" Y="-26.746494140625" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.152773925781" Y="-26.79325" />
                  <Point X="4.213122070312" Y="-27.6068828125" />
                  <Point X="4.128102050781" Y="-27.744458984375" />
                  <Point X="4.124807617188" Y="-27.749791015625" />
                  <Point X="4.028980712891" Y="-27.8859453125" />
                  <Point X="2.800946533203" Y="-27.176939453125" />
                  <Point X="2.786129394531" Y="-27.170013671875" />
                  <Point X="2.754224365234" Y="-27.159826171875" />
                  <Point X="2.545682617188" Y="-27.1221640625" />
                  <Point X="2.538134033203" Y="-27.12080078125" />
                  <Point X="2.506784912109" Y="-27.120396484375" />
                  <Point X="2.474609863281" Y="-27.125353515625" />
                  <Point X="2.444831298828" Y="-27.135177734375" />
                  <Point X="2.271584228516" Y="-27.22635546875" />
                  <Point X="2.265313232422" Y="-27.22965625" />
                  <Point X="2.242378417969" Y="-27.246552734375" />
                  <Point X="2.221419921875" Y="-27.267513671875" />
                  <Point X="2.204531005859" Y="-27.29044140625" />
                  <Point X="2.113352294922" Y="-27.4636875" />
                  <Point X="2.110052001953" Y="-27.469958984375" />
                  <Point X="2.100227783203" Y="-27.499736328125" />
                  <Point X="2.095270996094" Y="-27.531908203125" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.133337890625" Y="-27.77180078125" />
                  <Point X="2.136010742188" Y="-27.7828046875" />
                  <Point X="2.143276855469" Y="-27.806466796875" />
                  <Point X="2.151819335938" Y="-27.826080078125" />
                  <Point X="2.178902099609" Y="-27.87298828125" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.785758544922" Y="-29.1088515625" />
                  <Point X="2.781849365234" Y="-29.11164453125" />
                  <Point X="2.701764648438" Y="-29.163482421875" />
                  <Point X="1.758547363281" Y="-27.9342578125" />
                  <Point X="1.747507202148" Y="-27.92218359375" />
                  <Point X="1.721924194336" Y="-27.900556640625" />
                  <Point X="1.516245605469" Y="-27.76832421875" />
                  <Point X="1.508776489258" Y="-27.763525390625" />
                  <Point X="1.479979370117" Y="-27.7511640625" />
                  <Point X="1.448363525391" Y="-27.743435546875" />
                  <Point X="1.417099609375" Y="-27.741119140625" />
                  <Point X="1.192199584961" Y="-27.761814453125" />
                  <Point X="1.184057006836" Y="-27.762560546875" />
                  <Point X="1.156370117188" Y="-27.769396484375" />
                  <Point X="1.128974853516" Y="-27.780744140625" />
                  <Point X="1.104592895508" Y="-27.79546484375" />
                  <Point X="0.930896606445" Y="-27.939888671875" />
                  <Point X="0.92460925293" Y="-27.9451171875" />
                  <Point X="0.904138366699" Y="-27.9688671875" />
                  <Point X="0.887248779297" Y="-27.996689453125" />
                  <Point X="0.875624389648" Y="-28.02580859375" />
                  <Point X="0.823690063477" Y="-28.26474609375" />
                  <Point X="0.821998962402" Y="-28.275421875" />
                  <Point X="0.819405944824" Y="-28.301216796875" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="0.827417297363" Y="-28.38141796875" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.979373596191" Y="-29.8692734375" />
                  <Point X="0.975706604004" Y="-29.870078125" />
                  <Point X="0.929315612793" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.055040405273" Y="-29.75329296875" />
                  <Point X="-1.058427124023" Y="-29.75263671875" />
                  <Point X="-1.14124621582" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759155273" Y="-29.509330078125" />
                  <Point X="-1.123333984375" Y="-29.497693359375" />
                  <Point X="-1.181747070312" Y="-29.20403125" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199024902344" Y="-29.1495078125" />
                  <Point X="-1.205662231445" Y="-29.135474609375" />
                  <Point X="-1.221733032227" Y="-29.10763671875" />
                  <Point X="-1.230567871094" Y="-29.094869140625" />
                  <Point X="-1.250204223633" Y="-29.070939453125" />
                  <Point X="-1.261005737305" Y="-29.05977734375" />
                  <Point X="-1.486118041992" Y="-28.862359375" />
                  <Point X="-1.506739379883" Y="-28.845962890625" />
                  <Point X="-1.53302331543" Y="-28.829619140625" />
                  <Point X="-1.546834472656" Y="-28.822525390625" />
                  <Point X="-1.576532226563" Y="-28.810224609375" />
                  <Point X="-1.591315551758" Y="-28.805474609375" />
                  <Point X="-1.621456176758" Y="-28.798447265625" />
                  <Point X="-1.636813842773" Y="-28.796169921875" />
                  <Point X="-1.935588012695" Y="-28.776587890625" />
                  <Point X="-1.961927368164" Y="-28.7761328125" />
                  <Point X="-1.992724609375" Y="-28.779166015625" />
                  <Point X="-2.007997070312" Y="-28.7819453125" />
                  <Point X="-2.039046630859" Y="-28.790263671875" />
                  <Point X="-2.053665771484" Y="-28.795494140625" />
                  <Point X="-2.081860839844" Y="-28.808267578125" />
                  <Point X="-2.095436767578" Y="-28.815810546875" />
                  <Point X="-2.344391357422" Y="-28.98215625" />
                  <Point X="-2.362333984375" Y="-28.994939453125" />
                  <Point X="-2.379337402344" Y="-29.009466796875" />
                  <Point X="-2.387409912109" Y="-29.017232421875" />
                  <Point X="-2.407155273438" Y="-29.038607421875" />
                  <Point X="-2.418327636719" Y="-29.051869140625" />
                  <Point X="-2.503203369141" Y="-29.162482421875" />
                  <Point X="-2.742629638672" Y="-29.014234375" />
                  <Point X="-2.747556396484" Y="-29.01118359375" />
                  <Point X="-2.980862304688" Y="-28.831546875" />
                  <Point X="-2.341486572266" Y="-27.724115234375" />
                  <Point X="-2.334846923828" Y="-27.710076171875" />
                  <Point X="-2.323947021484" Y="-27.68111328125" />
                  <Point X="-2.319684570312" Y="-27.666185546875" />
                  <Point X="-2.313413574219" Y="-27.63466015625" />
                  <Point X="-2.311638916016" Y="-27.619236328125" />
                  <Point X="-2.310626220703" Y="-27.588302734375" />
                  <Point X="-2.314666015625" Y="-27.5576171875" />
                  <Point X="-2.323650146484" Y="-27.528" />
                  <Point X="-2.329356445312" Y="-27.51355859375" />
                  <Point X="-2.343573730469" Y="-27.484728515625" />
                  <Point X="-2.351557128906" Y="-27.47141015625" />
                  <Point X="-2.369585693359" Y="-27.446251953125" />
                  <Point X="-2.379630859375" Y="-27.434412109375" />
                  <Point X="-2.396373291016" Y="-27.417669921875" />
                  <Point X="-2.408875488281" Y="-27.406970703125" />
                  <Point X="-2.434059570312" Y="-27.388935546875" />
                  <Point X="-2.447393554688" Y="-27.38094921875" />
                  <Point X="-2.476216308594" Y="-27.366751953125" />
                  <Point X="-2.490652832031" Y="-27.3610546875" />
                  <Point X="-2.520260498047" Y="-27.3520859375" />
                  <Point X="-2.550932861328" Y="-27.348056640625" />
                  <Point X="-2.581852294922" Y="-27.349076171875" />
                  <Point X="-2.597270507812" Y="-27.350853515625" />
                  <Point X="-2.628781738281" Y="-27.357126953125" />
                  <Point X="-2.643706542969" Y="-27.361390625" />
                  <Point X="-2.672658203125" Y="-27.372291015625" />
                  <Point X="-2.686685058594" Y="-27.378927734375" />
                  <Point X="-2.731727539062" Y="-27.40493359375" />
                  <Point X="-3.793089355469" Y="-28.017712890625" />
                  <Point X="-4.000101074219" Y="-27.745740234375" />
                  <Point X="-4.004007080078" Y="-27.740609375" />
                  <Point X="-4.181264648438" Y="-27.443375" />
                  <Point X="-3.048119873047" Y="-26.5738828125" />
                  <Point X="-3.036477783203" Y="-26.56330859375" />
                  <Point X="-3.015101806641" Y="-26.540388671875" />
                  <Point X="-3.005365478516" Y="-26.528041015625" />
                  <Point X="-2.987400390625" Y="-26.500908203125" />
                  <Point X="-2.979833984375" Y="-26.487123046875" />
                  <Point X="-2.967078857422" Y="-26.458494140625" />
                  <Point X="-2.961890136719" Y="-26.443650390625" />
                  <Point X="-2.954454833984" Y="-26.41494140625" />
                  <Point X="-2.951554931641" Y="-26.398814453125" />
                  <Point X="-2.948750244141" Y="-26.3683984375" />
                  <Point X="-2.948578369141" Y="-26.353078125" />
                  <Point X="-2.950783691406" Y="-26.321404296875" />
                  <Point X="-2.953079101562" Y="-26.30624609375" />
                  <Point X="-2.960078369141" Y="-26.27649609375" />
                  <Point X="-2.971770996094" Y="-26.248259765625" />
                  <Point X="-2.987853759766" Y="-26.222271484375" />
                  <Point X="-2.996947753906" Y="-26.209927734375" />
                  <Point X="-3.017781005859" Y="-26.18596875" />
                  <Point X="-3.028740478516" Y="-26.17525" />
                  <Point X="-3.052240478516" Y="-26.15571484375" />
                  <Point X="-3.064781005859" Y="-26.1468984375" />
                  <Point X="-3.090338623047" Y="-26.13185546875" />
                  <Point X="-3.105591796875" Y="-26.124400390625" />
                  <Point X="-3.134859863281" Y="-26.1131953125" />
                  <Point X="-3.149959228516" Y="-26.10880859375" />
                  <Point X="-3.181795898438" Y="-26.102357421875" />
                  <Point X="-3.197382568359" Y="-26.100521484375" />
                  <Point X="-3.228646484375" Y="-26.09944140625" />
                  <Point X="-3.244323730469" Y="-26.100197265625" />
                  <Point X="-3.301185791016" Y="-26.107681640625" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.739229003906" Y="-25.980119140625" />
                  <Point X="-4.740761230469" Y="-25.974119140625" />
                  <Point X="-4.786452148438" Y="-25.65465625" />
                  <Point X="-3.508279052734" Y="-25.312169921875" />
                  <Point X="-3.500464111328" Y="-25.309708984375" />
                  <Point X="-3.473931640625" Y="-25.299251953125" />
                  <Point X="-3.444164794922" Y="-25.283892578125" />
                  <Point X="-3.433559814453" Y="-25.277513671875" />
                  <Point X="-3.406789306641" Y="-25.25893359375" />
                  <Point X="-3.393725585938" Y="-25.24828515625" />
                  <Point X="-3.371276123047" Y="-25.226416015625" />
                  <Point X="-3.360960449219" Y="-25.21455078125" />
                  <Point X="-3.341686767578" Y="-25.188271484375" />
                  <Point X="-3.333459228516" Y="-25.17484765625" />
                  <Point X="-3.319334228516" Y="-25.14682421875" />
                  <Point X="-3.313436767578" Y="-25.132224609375" />
                  <Point X="-3.304508789062" Y="-25.103458984375" />
                  <Point X="-3.300989501953" Y="-25.08849609375" />
                  <Point X="-3.296719970703" Y="-25.06033203125" />
                  <Point X="-3.295646728516" Y="-25.04608984375" />
                  <Point X="-3.295647949219" Y="-25.016453125" />
                  <Point X="-3.296721435547" Y="-25.00221484375" />
                  <Point X="-3.300990722656" Y="-24.97405859375" />
                  <Point X="-3.304186523438" Y="-24.960140625" />
                  <Point X="-3.313114257812" Y="-24.931375" />
                  <Point X="-3.319307617188" Y="-24.9158046875" />
                  <Point X="-3.333412109375" Y="-24.887796875" />
                  <Point X="-3.341627929688" Y="-24.87437890625" />
                  <Point X="-3.360879150391" Y="-24.848103515625" />
                  <Point X="-3.371203613281" Y="-24.83621875" />
                  <Point X="-3.393671142578" Y="-24.8143203125" />
                  <Point X="-3.405814208984" Y="-24.804306640625" />
                  <Point X="-3.432569091797" Y="-24.78573828125" />
                  <Point X="-3.439018066406" Y="-24.78163671875" />
                  <Point X="-3.462868896484" Y="-24.768642578125" />
                  <Point X="-3.495799316406" Y="-24.75468359375" />
                  <Point X="-3.508287597656" Y="-24.75038671875" />
                  <Point X="-3.560120117188" Y="-24.736498046875" />
                  <Point X="-4.7854453125" Y="-24.408173828125" />
                  <Point X="-4.732316894531" Y="-24.0491328125" />
                  <Point X="-4.731331542969" Y="-24.042474609375" />
                  <Point X="-4.633586425781" Y="-23.681765625" />
                  <Point X="-3.778065917969" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747056884766" Y="-23.795634765625" />
                  <Point X="-3.736703369141" Y="-23.795296875" />
                  <Point X="-3.715142822266" Y="-23.79341015625" />
                  <Point X="-3.704888427734" Y="-23.7919453125" />
                  <Point X="-3.684604980469" Y="-23.78791015625" />
                  <Point X="-3.674575927734" Y="-23.78533984375" />
                  <Point X="-3.615295654297" Y="-23.766650390625" />
                  <Point X="-3.603345947266" Y="-23.76228515625" />
                  <Point X="-3.584427734375" Y="-23.753951171875" />
                  <Point X="-3.575197753906" Y="-23.74926953125" />
                  <Point X="-3.556468017578" Y="-23.738447265625" />
                  <Point X="-3.547802246094" Y="-23.7327890625" />
                  <Point X="-3.531133789062" Y="-23.720560546875" />
                  <Point X="-3.515890136719" Y="-23.7065859375" />
                  <Point X="-3.50226171875" Y="-23.6910390625" />
                  <Point X="-3.495874267578" Y="-23.682896484375" />
                  <Point X="-3.483468994141" Y="-23.66517578125" />
                  <Point X="-3.478004150391" Y="-23.65638671875" />
                  <Point X="-3.468061035156" Y="-23.63826171875" />
                  <Point X="-3.463582763672" Y="-23.62892578125" />
                  <Point X="-3.439798828125" Y="-23.571505859375" />
                  <Point X="-3.435509033203" Y="-23.55967578125" />
                  <Point X="-3.429718017578" Y="-23.53981640625" />
                  <Point X="-3.427364501953" Y="-23.529728515625" />
                  <Point X="-3.423603515625" Y="-23.50841015625" />
                  <Point X="-3.422362548828" Y="-23.498125" />
                  <Point X="-3.4210078125" Y="-23.47748046875" />
                  <Point X="-3.421908935547" Y="-23.4568125" />
                  <Point X="-3.425054931641" Y="-23.4363671875" />
                  <Point X="-3.427186035156" Y="-23.42623046875" />
                  <Point X="-3.432788085938" Y="-23.4053203125" />
                  <Point X="-3.436010498047" Y="-23.395474609375" />
                  <Point X="-3.443508056641" Y="-23.376193359375" />
                  <Point X="-3.447783203125" Y="-23.3667578125" />
                  <Point X="-3.476484130859" Y="-23.311623046875" />
                  <Point X="-3.482851074219" Y="-23.300626953125" />
                  <Point X="-3.494324707031" Y="-23.28346875" />
                  <Point X="-3.500528320312" Y="-23.275203125" />
                  <Point X="-3.514434082031" Y="-23.258634765625" />
                  <Point X="-3.521509033203" Y="-23.251080078125" />
                  <Point X="-3.536442382812" Y="-23.23678515625" />
                  <Point X="-3.54430078125" Y="-23.230044921875" />
                  <Point X="-3.574031982422" Y="-23.20723046875" />
                  <Point X="-4.227615234375" Y="-22.70571875" />
                  <Point X="-4.006120849609" Y="-22.32624609375" />
                  <Point X="-4.0022890625" Y="-22.319681640625" />
                  <Point X="-3.726337646484" Y="-21.964986328125" />
                  <Point X="-3.254156982422" Y="-22.237599609375" />
                  <Point X="-3.244920166016" Y="-22.24228125" />
                  <Point X="-3.225989990234" Y="-22.250611328125" />
                  <Point X="-3.216296630859" Y="-22.254259765625" />
                  <Point X="-3.195658691406" Y="-22.260767578125" />
                  <Point X="-3.185627197266" Y="-22.26333984375" />
                  <Point X="-3.165336669922" Y="-22.267376953125" />
                  <Point X="-3.155077636719" Y="-22.268841796875" />
                  <Point X="-3.072516845703" Y="-22.276064453125" />
                  <Point X="-3.059184570312" Y="-22.276666015625" />
                  <Point X="-3.038518066406" Y="-22.276216796875" />
                  <Point X="-3.0281953125" Y="-22.275427734375" />
                  <Point X="-3.006735595703" Y="-22.27260546875" />
                  <Point X="-2.996549316406" Y="-22.2706953125" />
                  <Point X="-2.976452880859" Y="-22.265779296875" />
                  <Point X="-2.957018066406" Y="-22.25870703125" />
                  <Point X="-2.938461914062" Y="-22.24955859375" />
                  <Point X="-2.929430419922" Y="-22.2444765625" />
                  <Point X="-2.911174804688" Y="-22.23284765625" />
                  <Point X="-2.902753173828" Y="-22.2268125" />
                  <Point X="-2.886618164062" Y="-22.21386328125" />
                  <Point X="-2.878904785156" Y="-22.20694921875" />
                  <Point X="-2.820302490234" Y="-22.14834765625" />
                  <Point X="-2.811266357422" Y="-22.138513671875" />
                  <Point X="-2.798321777344" Y="-22.122384765625" />
                  <Point X="-2.792291992188" Y="-22.11396875" />
                  <Point X="-2.780661621094" Y="-22.09571484375" />
                  <Point X="-2.775580810547" Y="-22.08669140625" />
                  <Point X="-2.766431152344" Y="-22.068140625" />
                  <Point X="-2.759355224609" Y="-22.048703125" />
                  <Point X="-2.7544375" Y="-22.028611328125" />
                  <Point X="-2.752526855469" Y="-22.0184296875" />
                  <Point X="-2.749700683594" Y="-21.996970703125" />
                  <Point X="-2.74891015625" Y="-21.986640625" />
                  <Point X="-2.748458496094" Y="-21.96595703125" />
                  <Point X="-2.748797363281" Y="-21.955603515625" />
                  <Point X="-2.756020507812" Y="-21.87304296875" />
                  <Point X="-2.757745361328" Y="-21.85980078125" />
                  <Point X="-2.761779785156" Y="-21.839515625" />
                  <Point X="-2.764350830078" Y="-21.829482421875" />
                  <Point X="-2.770859375" Y="-21.808837890625" />
                  <Point X="-2.774508544922" Y="-21.799142578125" />
                  <Point X="-2.782841064453" Y="-21.780205078125" />
                  <Point X="-2.787524414062" Y="-21.770962890625" />
                  <Point X="-2.80069921875" Y="-21.74814453125" />
                  <Point X="-3.059386230469" Y="-21.3000859375" />
                  <Point X="-2.655033447266" Y="-20.990072265625" />
                  <Point X="-2.648362060547" Y="-20.98495703125" />
                  <Point X="-2.192523925781" Y="-20.731703125" />
                  <Point X="-2.118563964844" Y="-20.82808984375" />
                  <Point X="-2.111819091797" Y="-20.835953125" />
                  <Point X="-2.097515380859" Y="-20.85089453125" />
                  <Point X="-2.089956542969" Y="-20.85797265625" />
                  <Point X="-2.073376708984" Y="-20.871884765625" />
                  <Point X="-2.065094970703" Y="-20.87809765625" />
                  <Point X="-2.047897705078" Y="-20.88958984375" />
                  <Point X="-2.038981933594" Y="-20.894869140625" />
                  <Point X="-1.947100952148" Y="-20.942701171875" />
                  <Point X="-1.934376586914" Y="-20.94869140625" />
                  <Point X="-1.9150859375" Y="-20.956197265625" />
                  <Point X="-1.905235595703" Y="-20.959421875" />
                  <Point X="-1.884324707031" Y="-20.96502734375" />
                  <Point X="-1.874194702148" Y="-20.96716015625" />
                  <Point X="-1.853749389648" Y="-20.970310546875" />
                  <Point X="-1.833074829102" Y="-20.97121484375" />
                  <Point X="-1.812432495117" Y="-20.96986328125" />
                  <Point X="-1.802149414062" Y="-20.968625" />
                  <Point X="-1.780828979492" Y="-20.9648671875" />
                  <Point X="-1.770731933594" Y="-20.96251171875" />
                  <Point X="-1.750866455078" Y="-20.956720703125" />
                  <Point X="-1.741098388672" Y="-20.95328515625" />
                  <Point X="-1.645388916016" Y="-20.913640625" />
                  <Point X="-1.632579467773" Y="-20.907724609375" />
                  <Point X="-1.6144375" Y="-20.897771484375" />
                  <Point X="-1.605634521484" Y="-20.892296875" />
                  <Point X="-1.587909057617" Y="-20.8798828125" />
                  <Point X="-1.57976184082" Y="-20.873486328125" />
                  <Point X="-1.564215209961" Y="-20.859849609375" />
                  <Point X="-1.550245605469" Y="-20.844603515625" />
                  <Point X="-1.538017822266" Y="-20.82792578125" />
                  <Point X="-1.532359863281" Y="-20.81925390625" />
                  <Point X="-1.521540039062" Y="-20.800513671875" />
                  <Point X="-1.516857788086" Y="-20.791275390625" />
                  <Point X="-1.508526489258" Y="-20.772341796875" />
                  <Point X="-1.504877441406" Y="-20.762646484375" />
                  <Point X="-1.473725708008" Y="-20.663845703125" />
                  <Point X="-1.470026977539" Y="-20.65023828125" />
                  <Point X="-1.465991821289" Y="-20.629953125" />
                  <Point X="-1.464527954102" Y="-20.61969921875" />
                  <Point X="-1.462641113281" Y="-20.59813671875" />
                  <Point X="-1.462301879883" Y="-20.587783203125" />
                  <Point X="-1.462753173828" Y="-20.5671015625" />
                  <Point X="-1.465041381836" Y="-20.545396484375" />
                  <Point X="-1.479266357422" Y="-20.437345703125" />
                  <Point X="-0.939796691895" Y="-20.28609765625" />
                  <Point X="-0.931156799316" Y="-20.283673828125" />
                  <Point X="-0.36522277832" Y="-20.21744140625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.22043510437" Y="-20.752892578125" />
                  <Point X="-0.207661560059" Y="-20.781083984375" />
                  <Point X="-0.200119384766" Y="-20.79465625" />
                  <Point X="-0.182261154175" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451187134" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085354125977" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.00931760788" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227790833" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912307739" Y="-20.88956640625" />
                  <Point X="0.139208236694" Y="-20.8749765625" />
                  <Point X="0.163763122559" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.1945730896" Y="-20.8213828125" />
                  <Point X="0.21243132019" Y="-20.79465625" />
                  <Point X="0.219973648071" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978118896" Y="-20.7382734375" />
                  <Point X="0.244728713989" Y="-20.713080078125" />
                  <Point X="0.378190765381" Y="-20.2149921875" />
                  <Point X="0.820334838867" Y="-20.261296875" />
                  <Point X="0.827850646973" Y="-20.262083984375" />
                  <Point X="1.443715332031" Y="-20.4107734375" />
                  <Point X="1.453618896484" Y="-20.4131640625" />
                  <Point X="1.853427368164" Y="-20.558177734375" />
                  <Point X="1.858248779297" Y="-20.55992578125" />
                  <Point X="2.245864746094" Y="-20.741201171875" />
                  <Point X="2.250440917969" Y="-20.743341796875" />
                  <Point X="2.624945800781" Y="-20.961529296875" />
                  <Point X="2.629440917969" Y="-20.964146484375" />
                  <Point X="2.817779541016" Y="-21.098083984375" />
                  <Point X="2.065308837891" Y="-22.40140234375" />
                  <Point X="2.062372070313" Y="-22.4068984375" />
                  <Point X="2.0531796875" Y="-22.42656640625" />
                  <Point X="2.044181640625" Y="-22.450439453125" />
                  <Point X="2.041301635742" Y="-22.459404296875" />
                  <Point X="2.02058190918" Y="-22.53688671875" />
                  <Point X="2.018868652344" Y="-22.544548828125" />
                  <Point X="2.015005126953" Y="-22.56777734375" />
                  <Point X="2.012917358398" Y="-22.589482421875" />
                  <Point X="2.012487792969" Y="-22.59972265625" />
                  <Point X="2.012734741211" Y="-22.62019140625" />
                  <Point X="2.013410888672" Y="-22.630419921875" />
                  <Point X="2.021490112305" Y="-22.697419921875" />
                  <Point X="2.023800537109" Y="-22.71096484375" />
                  <Point X="2.029142578125" Y="-22.732884765625" />
                  <Point X="2.032466308594" Y="-22.74368359375" />
                  <Point X="2.040732177734" Y="-22.7657734375" />
                  <Point X="2.045316772461" Y="-22.776107421875" />
                  <Point X="2.055680419922" Y="-22.796154296875" />
                  <Point X="2.061459228516" Y="-22.8058671875" />
                  <Point X="2.102916259766" Y="-22.86696484375" />
                  <Point X="2.107600097656" Y="-22.8732890625" />
                  <Point X="2.122676513672" Y="-22.89144921875" />
                  <Point X="2.137477539062" Y="-22.9069609375" />
                  <Point X="2.144824707031" Y="-22.913884765625" />
                  <Point X="2.160214599609" Y="-22.9269140625" />
                  <Point X="2.168257324219" Y="-22.93301953125" />
                  <Point X="2.229354248047" Y="-22.9744765625" />
                  <Point X="2.241257324219" Y="-22.9817421875" />
                  <Point X="2.261284423828" Y="-22.99209765625" />
                  <Point X="2.271604248047" Y="-22.996677734375" />
                  <Point X="2.293709228516" Y="-23.004955078125" />
                  <Point X="2.304521240234" Y="-23.00828515625" />
                  <Point X="2.326463623047" Y="-23.013634765625" />
                  <Point X="2.337593994141" Y="-23.015654296875" />
                  <Point X="2.40459375" Y="-23.023732421875" />
                  <Point X="2.4125859375" Y="-23.02435546875" />
                  <Point X="2.436635253906" Y="-23.024873046875" />
                  <Point X="2.457962158203" Y="-23.023828125" />
                  <Point X="2.4679921875" Y="-23.02280078125" />
                  <Point X="2.487886474609" Y="-23.019689453125" />
                  <Point X="2.497750732422" Y="-23.01760546875" />
                  <Point X="2.575232421875" Y="-22.996884765625" />
                  <Point X="2.597891113281" Y="-22.989232421875" />
                  <Point X="2.626042480469" Y="-22.977134765625" />
                  <Point X="2.636034423828" Y="-22.972125" />
                  <Point X="2.68816796875" Y="-22.942025390625" />
                  <Point X="3.940403320312" Y="-22.219048828125" />
                  <Point X="4.041288818359" Y="-22.359255859375" />
                  <Point X="4.043961914062" Y="-22.362970703125" />
                  <Point X="4.136885253906" Y="-22.51652734375" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168133056641" Y="-23.260134765625" />
                  <Point X="3.152116943359" Y="-23.274787109375" />
                  <Point X="3.134668457031" Y="-23.2933984375" />
                  <Point X="3.128576416016" Y="-23.300578125" />
                  <Point X="3.072815673828" Y="-23.373322265625" />
                  <Point X="3.068378662109" Y="-23.379625" />
                  <Point X="3.056116455078" Y="-23.399212890625" />
                  <Point X="3.045624511719" Y="-23.4188046875" />
                  <Point X="3.041224853516" Y="-23.4282265625" />
                  <Point X="3.033482421875" Y="-23.447490234375" />
                  <Point X="3.030139648438" Y="-23.45733203125" />
                  <Point X="3.009367675781" Y="-23.531607421875" />
                  <Point X="3.006224609375" Y="-23.545341796875" />
                  <Point X="3.002770996094" Y="-23.567640625" />
                  <Point X="3.001708496094" Y="-23.57889453125" />
                  <Point X="3.000892578125" Y="-23.602458984375" />
                  <Point X="3.001174072266" Y="-23.6137578125" />
                  <Point X="3.003077148438" Y="-23.6362421875" />
                  <Point X="3.004698730469" Y="-23.647427734375" />
                  <Point X="3.021750488281" Y="-23.730068359375" />
                  <Point X="3.023619873047" Y="-23.7375703125" />
                  <Point X="3.030438964844" Y="-23.759736328125" />
                  <Point X="3.038254882812" Y="-23.780107421875" />
                  <Point X="3.042430419922" Y="-23.789451171875" />
                  <Point X="3.051761962891" Y="-23.807634765625" />
                  <Point X="3.05691796875" Y="-23.816474609375" />
                  <Point X="3.103296630859" Y="-23.88696875" />
                  <Point X="3.111748779297" Y="-23.89858984375" />
                  <Point X="3.126310302734" Y="-23.915841796875" />
                  <Point X="3.134104492188" Y="-23.924033203125" />
                  <Point X="3.151341796875" Y="-23.94011328125" />
                  <Point X="3.160044433594" Y="-23.9473125" />
                  <Point X="3.178245361328" Y="-23.960630859375" />
                  <Point X="3.187743652344" Y="-23.96675" />
                  <Point X="3.254952880859" Y="-24.004583984375" />
                  <Point X="3.261996826172" Y="-24.008171875" />
                  <Point X="3.283644775391" Y="-24.017724609375" />
                  <Point X="3.304053710938" Y="-24.025091796875" />
                  <Point X="3.313821533203" Y="-24.028037109375" />
                  <Point X="3.333630126953" Y="-24.03286328125" />
                  <Point X="3.343670898438" Y="-24.034744140625" />
                  <Point X="3.434542236328" Y="-24.04675390625" />
                  <Point X="3.457935302734" Y="-24.0483515625" />
                  <Point X="3.489240234375" Y="-24.048009765625" />
                  <Point X="3.500602539062" Y="-24.047203125" />
                  <Point X="3.550125976562" Y="-24.04068359375" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.751540039062" Y="-24.0810703125" />
                  <Point X="4.752683105469" Y="-24.085765625" />
                  <Point X="4.783870605469" Y="-24.286078125" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686021972656" Y="-24.580458984375" />
                  <Point X="3.665620605469" Y="-24.587865234375" />
                  <Point X="3.642379394531" Y="-24.5983828125" />
                  <Point X="3.634006347656" Y="-24.602685546875" />
                  <Point X="3.544728027344" Y="-24.6542890625" />
                  <Point X="3.538336669922" Y="-24.658330078125" />
                  <Point X="3.519832519531" Y="-24.67145703125" />
                  <Point X="3.502288330078" Y="-24.685744140625" />
                  <Point X="3.494500244141" Y="-24.69283984375" />
                  <Point X="3.479754150391" Y="-24.707853515625" />
                  <Point X="3.472796142578" Y="-24.715771484375" />
                  <Point X="3.419229248047" Y="-24.784029296875" />
                  <Point X="3.410851806641" Y="-24.795794921875" />
                  <Point X="3.399128173828" Y="-24.815076171875" />
                  <Point X="3.393843017578" Y="-24.8250625" />
                  <Point X="3.384069580078" Y="-24.846525390625" />
                  <Point X="3.380006347656" Y="-24.857068359375" />
                  <Point X="3.373159912109" Y="-24.878568359375" />
                  <Point X="3.370376708984" Y="-24.889525390625" />
                  <Point X="3.352520996094" Y="-24.98276171875" />
                  <Point X="3.351391113281" Y="-24.990279296875" />
                  <Point X="3.349210449219" Y="-25.01296484375" />
                  <Point X="3.348563964844" Y="-25.03515234375" />
                  <Point X="3.348827880859" Y="-25.045515625" />
                  <Point X="3.350483398438" Y="-25.06615234375" />
                  <Point X="3.351875" Y="-25.07642578125" />
                  <Point X="3.369730712891" Y="-25.16966015625" />
                  <Point X="3.373159912109" Y="-25.183990234375" />
                  <Point X="3.380006103516" Y="-25.205490234375" />
                  <Point X="3.384069580078" Y="-25.21603515625" />
                  <Point X="3.393843017578" Y="-25.237498046875" />
                  <Point X="3.399128173828" Y="-25.247484375" />
                  <Point X="3.410851806641" Y="-25.266765625" />
                  <Point X="3.417290283203" Y="-25.276060546875" />
                  <Point X="3.470857177734" Y="-25.344318359375" />
                  <Point X="3.47590625" Y="-25.350236328125" />
                  <Point X="3.491968994141" Y="-25.367103515625" />
                  <Point X="3.508220458984" Y="-25.381994140625" />
                  <Point X="3.516207519531" Y="-25.38855078125" />
                  <Point X="3.532845458984" Y="-25.400755859375" />
                  <Point X="3.541496337891" Y="-25.406404296875" />
                  <Point X="3.630774658203" Y="-25.4580078125" />
                  <Point X="3.651341308594" Y="-25.468251953125" />
                  <Point X="3.680988769531" Y="-25.480232421875" />
                  <Point X="3.691995849609" Y="-25.483916015625" />
                  <Point X="3.737411132812" Y="-25.496083984375" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.762252441406" Y="-25.926814453125" />
                  <Point X="4.761611816406" Y="-25.9310625" />
                  <Point X="4.727802734375" Y="-26.079220703125" />
                  <Point X="3.436775146484" Y="-25.90925390625" />
                  <Point X="3.428618164062" Y="-25.908537109375" />
                  <Point X="3.400098632812" Y="-25.90804296875" />
                  <Point X="3.366718994141" Y="-25.910841796875" />
                  <Point X="3.354479736328" Y="-25.912677734375" />
                  <Point X="3.179258056641" Y="-25.95076171875" />
                  <Point X="3.157869628906" Y="-25.956744140625" />
                  <Point X="3.128745605469" Y="-25.96837109375" />
                  <Point X="3.114667480469" Y="-25.97539453125" />
                  <Point X="3.086843017578" Y="-25.992287109375" />
                  <Point X="3.074120117188" Y="-26.001533203125" />
                  <Point X="3.050372802734" Y="-26.022001953125" />
                  <Point X="3.039348632812" Y="-26.033224609375" />
                  <Point X="2.933455566406" Y="-26.16058203125" />
                  <Point X="2.933455322266" Y="-26.16058203125" />
                  <Point X="2.921343505859" Y="-26.17682421875" />
                  <Point X="2.906618896484" Y="-26.20120703125" />
                  <Point X="2.900175292969" Y="-26.213953125" />
                  <Point X="2.888826904297" Y="-26.24134375" />
                  <Point X="2.884366699219" Y="-26.254916015625" />
                  <Point X="2.877531738281" Y="-26.28257421875" />
                  <Point X="2.875156982422" Y="-26.29666015625" />
                  <Point X="2.859977539062" Y="-26.461619140625" />
                  <Point X="2.859288574219" Y="-26.48332421875" />
                  <Point X="2.861608154297" Y="-26.514595703125" />
                  <Point X="2.864067138672" Y="-26.530134765625" />
                  <Point X="2.871800292969" Y="-26.5617578125" />
                  <Point X="2.8767890625" Y="-26.57667578125" />
                  <Point X="2.889159912109" Y="-26.605484375" />
                  <Point X="2.896541992188" Y="-26.619375" />
                  <Point X="2.993512207031" Y="-26.770205078125" />
                  <Point X="3.007596191406" Y="-26.789361328125" />
                  <Point X="3.022724853516" Y="-26.807447265625" />
                  <Point X="3.029844970703" Y="-26.81506640625" />
                  <Point X="3.044880371094" Y="-26.829482421875" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.09494140625" Y="-26.868619140625" />
                  <Point X="4.087170410156" Y="-27.629982421875" />
                  <Point X="4.047288574219" Y="-27.694517578125" />
                  <Point X="4.045487060547" Y="-27.69743359375" />
                  <Point X="4.001273681641" Y="-27.76025390625" />
                  <Point X="2.848446533203" Y="-27.09466796875" />
                  <Point X="2.841173583984" Y="-27.090876953125" />
                  <Point X="2.815026123047" Y="-27.079515625" />
                  <Point X="2.78312109375" Y="-27.069328125" />
                  <Point X="2.771107910156" Y="-27.066337890625" />
                  <Point X="2.562566162109" Y="-27.02867578125" />
                  <Point X="2.539359130859" Y="-27.02580859375" />
                  <Point X="2.508010009766" Y="-27.025404296875" />
                  <Point X="2.492319335938" Y="-27.02650390625" />
                  <Point X="2.460144287109" Y="-27.0314609375" />
                  <Point X="2.444846435547" Y="-27.03513671875" />
                  <Point X="2.415067871094" Y="-27.0449609375" />
                  <Point X="2.400587158203" Y="-27.051109375" />
                  <Point X="2.227340087891" Y="-27.142287109375" />
                  <Point X="2.208965576172" Y="-27.153171875" />
                  <Point X="2.186030761719" Y="-27.170068359375" />
                  <Point X="2.175199462891" Y="-27.179380859375" />
                  <Point X="2.154240966797" Y="-27.200341796875" />
                  <Point X="2.144931396484" Y="-27.211171875" />
                  <Point X="2.128042480469" Y="-27.234099609375" />
                  <Point X="2.120463134766" Y="-27.246197265625" />
                  <Point X="2.029284423828" Y="-27.419443359375" />
                  <Point X="2.019835205078" Y="-27.4401953125" />
                  <Point X="2.010011108398" Y="-27.46997265625" />
                  <Point X="2.006335693359" Y="-27.48526953125" />
                  <Point X="2.00137890625" Y="-27.51744140625" />
                  <Point X="2.000278930664" Y="-27.533134765625" />
                  <Point X="2.00068347168" Y="-27.564484375" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.039850219727" Y="-27.78868359375" />
                  <Point X="2.045196044922" Y="-27.81069140625" />
                  <Point X="2.052462158203" Y="-27.834353515625" />
                  <Point X="2.056179443359" Y="-27.84440234375" />
                  <Point X="2.064721923828" Y="-27.864015625" />
                  <Point X="2.069547119141" Y="-27.873580078125" />
                  <Point X="2.096629882812" Y="-27.92048828125" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.72375390625" Y="-29.036083984375" />
                  <Point X="1.833915893555" Y="-27.87642578125" />
                  <Point X="1.828657470703" Y="-27.87015234375" />
                  <Point X="1.808838256836" Y="-27.8496328125" />
                  <Point X="1.783255249023" Y="-27.828005859375" />
                  <Point X="1.773299072266" Y="-27.820646484375" />
                  <Point X="1.567620361328" Y="-27.6884140625" />
                  <Point X="1.567596801758" Y="-27.6883984375" />
                  <Point X="1.546249389648" Y="-27.676228515625" />
                  <Point X="1.517452148438" Y="-27.6638671875" />
                  <Point X="1.502537963867" Y="-27.658880859375" />
                  <Point X="1.47092199707" Y="-27.65115234375" />
                  <Point X="1.455383056641" Y="-27.6486953125" />
                  <Point X="1.424119262695" Y="-27.64637890625" />
                  <Point X="1.40839440918" Y="-27.64651953125" />
                  <Point X="1.18353112793" Y="-27.6672109375" />
                  <Point X="1.16128515625" Y="-27.670330078125" />
                  <Point X="1.133598266602" Y="-27.677166015625" />
                  <Point X="1.120014770508" Y="-27.68162890625" />
                  <Point X="1.092619506836" Y="-27.6929765625" />
                  <Point X="1.079873291016" Y="-27.69941796875" />
                  <Point X="1.055491455078" Y="-27.714138671875" />
                  <Point X="1.04385559082" Y="-27.72241796875" />
                  <Point X="0.870159301758" Y="-27.866841796875" />
                  <Point X="0.852650512695" Y="-27.88309375" />
                  <Point X="0.832179626465" Y="-27.90684375" />
                  <Point X="0.822930358887" Y="-27.9195703125" />
                  <Point X="0.806040710449" Y="-27.947392578125" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394775391" Y="-27.990587890625" />
                  <Point X="0.782791931152" Y="-28.005630859375" />
                  <Point X="0.73085760498" Y="-28.244568359375" />
                  <Point X="0.727475341797" Y="-28.265919921875" />
                  <Point X="0.724882324219" Y="-28.29171484375" />
                  <Point X="0.724417175293" Y="-28.30267578125" />
                  <Point X="0.724753479004" Y="-28.324578125" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.733230041504" Y="-28.393818359375" />
                  <Point X="0.833091186523" Y="-29.152337890625" />
                  <Point X="0.65506451416" Y="-28.48793359375" />
                  <Point X="0.652605834961" Y="-28.48012109375" />
                  <Point X="0.642148132324" Y="-28.4535859375" />
                  <Point X="0.626790649414" Y="-28.4238203125" />
                  <Point X="0.620409667969" Y="-28.4132109375" />
                  <Point X="0.462406677246" Y="-28.185560546875" />
                  <Point X="0.462406860352" Y="-28.185560546875" />
                  <Point X="0.446673492432" Y="-28.16517578125" />
                  <Point X="0.42479397583" Y="-28.142724609375" />
                  <Point X="0.412922729492" Y="-28.132408203125" />
                  <Point X="0.386664642334" Y="-28.11316015625" />
                  <Point X="0.37324798584" Y="-28.10494140625" />
                  <Point X="0.345242248535" Y="-28.090830078125" />
                  <Point X="0.330652954102" Y="-28.0849375" />
                  <Point X="0.086154624939" Y="-28.009056640625" />
                  <Point X="0.086154502869" Y="-28.009056640625" />
                  <Point X="0.063397335052" Y="-28.00311328125" />
                  <Point X="0.035234485626" Y="-27.998841796875" />
                  <Point X="0.020994924545" Y="-27.997767578125" />
                  <Point X="-0.008648359299" Y="-27.997765625" />
                  <Point X="-0.022892526627" Y="-27.998837890625" />
                  <Point X="-0.051058349609" Y="-28.003107421875" />
                  <Point X="-0.064979858398" Y="-28.0063046875" />
                  <Point X="-0.309488464355" Y="-28.08219140625" />
                  <Point X="-0.332923614502" Y="-28.090828125" />
                  <Point X="-0.360920898438" Y="-28.10493359375" />
                  <Point X="-0.374333526611" Y="-28.1131484375" />
                  <Point X="-0.400591918945" Y="-28.132392578125" />
                  <Point X="-0.412470306396" Y="-28.142712890625" />
                  <Point X="-0.434357849121" Y="-28.165171875" />
                  <Point X="-0.444367156982" Y="-28.177310546875" />
                  <Point X="-0.602375305176" Y="-28.404970703125" />
                  <Point X="-0.613221801758" Y="-28.422568359375" />
                  <Point X="-0.626162414551" Y="-28.446376953125" />
                  <Point X="-0.630923522949" Y="-28.456521484375" />
                  <Point X="-0.639219055176" Y="-28.47730078125" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.656746582031" Y="-28.54016015625" />
                  <Point X="-0.985425048828" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150206075824" Y="-27.495455571254" />
                  <Point X="-3.708652924864" Y="-27.968963354155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.951578663606" Y="-28.780826103094" />
                  <Point X="-2.722024793371" Y="-29.026992490799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115752610263" Y="-27.393105867037" />
                  <Point X="-3.62421649426" Y="-27.920213817686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.901908315892" Y="-28.694794507163" />
                  <Point X="-2.486311177003" Y="-29.140467874883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.040035442378" Y="-27.335006066054" />
                  <Point X="-3.539780063656" Y="-27.871464281216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.852237968177" Y="-28.608762911232" />
                  <Point X="-2.427674989024" Y="-29.064050965511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.964318274492" Y="-27.27690626507" />
                  <Point X="-3.455343633052" Y="-27.822714744747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.802567620463" Y="-28.522731315302" />
                  <Point X="-2.362269349542" Y="-28.994893404115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.888601106607" Y="-27.218806464086" />
                  <Point X="-3.370907202447" Y="-27.773965208277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752897272749" Y="-28.436699719371" />
                  <Point X="-2.282693709692" Y="-28.940931307735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.673371936158" Y="-26.237946259299" />
                  <Point X="-4.631523511686" Y="-26.282823200267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.812883938721" Y="-27.160706663102" />
                  <Point X="-3.286470771843" Y="-27.725215671808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703226925034" Y="-28.35066812344" />
                  <Point X="-2.20266333141" Y="-28.88745685862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.722375470237" Y="-26.046099880037" />
                  <Point X="-4.515830711002" Y="-26.26759201706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737166770836" Y="-27.102606862119" />
                  <Point X="-3.202034341239" Y="-27.676466135338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.65355657732" Y="-28.264636527509" />
                  <Point X="-2.122632953128" Y="-28.833982409505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.755463949228" Y="-25.871320307869" />
                  <Point X="-4.400137910319" Y="-26.252360833852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.661449602951" Y="-27.044507061135" />
                  <Point X="-3.117597910635" Y="-27.627716598868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.603886229606" Y="-28.178604931578" />
                  <Point X="-2.034612891289" Y="-28.78907584704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.778995929521" Y="-25.706788825882" />
                  <Point X="-4.284445109635" Y="-26.237129650645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.585732435065" Y="-26.986407260151" />
                  <Point X="-3.03316148003" Y="-27.578967062399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.554215881891" Y="-28.092573335647" />
                  <Point X="-1.915110437707" Y="-28.777930016397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.126995012601" Y="-29.623080338168" />
                  <Point X="-0.994637226952" Y="-29.765016686027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.715454280248" Y="-25.63563237971" />
                  <Point X="-4.168752308951" Y="-26.221898467438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.51001526718" Y="-26.928307459167" />
                  <Point X="-2.948725049426" Y="-27.530217525929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.504545534177" Y="-28.006541739716" />
                  <Point X="-1.77675850795" Y="-28.786997774204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.125858297557" Y="-29.485002793177" />
                  <Point X="-0.958115674784" Y="-29.664884733177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.611526360699" Y="-25.607784906096" />
                  <Point X="-4.053059508267" Y="-26.20666728423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.434298099294" Y="-26.870207658184" />
                  <Point X="-2.864288618822" Y="-27.48146798946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.454875186463" Y="-27.920510143786" />
                  <Point X="-1.638406578192" Y="-28.796065532012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.161078963868" Y="-29.307936730044" />
                  <Point X="-0.929122257304" Y="-29.556679844244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.50759844115" Y="-25.579937432483" />
                  <Point X="-3.937366707583" Y="-26.191436101023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358580931409" Y="-26.8121078572" />
                  <Point X="-2.779852188218" Y="-27.43271845299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.405204838748" Y="-27.834478547855" />
                  <Point X="-1.269729265883" Y="-29.052127023193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.235672440026" Y="-29.088648497604" />
                  <Point X="-0.900128839824" Y="-29.448474955311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.403670521601" Y="-25.55208995887" />
                  <Point X="-3.821673906899" Y="-26.176204917816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.282863763523" Y="-26.754008056216" />
                  <Point X="-2.695416000082" Y="-27.383968656504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.355534491034" Y="-27.748446951924" />
                  <Point X="-0.871135422343" Y="-29.340270066377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.299742602052" Y="-25.524242485256" />
                  <Point X="-3.705981106216" Y="-26.160973734609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.207146595638" Y="-26.695908255232" />
                  <Point X="-2.59648472809" Y="-27.350762934396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316636672428" Y="-27.650863232849" />
                  <Point X="-0.842142004863" Y="-29.232065177444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.195814682503" Y="-25.496395011643" />
                  <Point X="-3.590288305532" Y="-26.145742551401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.131429427753" Y="-26.637808454248" />
                  <Point X="-2.424825572767" Y="-27.395548318718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347011059309" Y="-27.478994168136" />
                  <Point X="-0.813148587383" Y="-29.123860288511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091886762954" Y="-25.468547538029" />
                  <Point X="-3.474595504848" Y="-26.130511368194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.055712259867" Y="-26.579708653265" />
                  <Point X="-0.784155169902" Y="-29.015655399578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.987958843405" Y="-25.440700064416" />
                  <Point X="-3.358902704164" Y="-26.115280184987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.99234078425" Y="-26.508369718189" />
                  <Point X="-0.755161752422" Y="-28.907450510645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.884030923856" Y="-25.412852590802" />
                  <Point X="-3.243125516984" Y="-26.100139495217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953664837092" Y="-26.410548071116" />
                  <Point X="-0.726168334942" Y="-28.799245621712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772512773608" Y="-24.320775933067" />
                  <Point X="-4.659557920094" Y="-24.441905183622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.780103004307" Y="-25.385005117189" />
                  <Point X="-3.075520653694" Y="-26.140577183622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.969107855717" Y="-26.254690938518" />
                  <Point X="-0.697174917461" Y="-28.691040732778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.754723470612" Y="-24.200556102338" />
                  <Point X="-4.486394012702" Y="-24.488304216978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.676175084758" Y="-25.357157643575" />
                  <Point X="-0.668181499981" Y="-28.582835843845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.736934167616" Y="-24.080336271609" />
                  <Point X="-4.31323010531" Y="-24.534703250335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.572247165209" Y="-25.329310169962" />
                  <Point X="-0.638463434828" Y="-28.475408044401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.711295141053" Y="-23.968534238814" />
                  <Point X="-4.140066197919" Y="-24.581102283692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.471534313693" Y="-25.298014957989" />
                  <Point X="-0.590374818675" Y="-28.387680249037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.682047574672" Y="-23.86060189121" />
                  <Point X="-3.966902290527" Y="-24.627501317048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.39073168398" Y="-25.245368647145" />
                  <Point X="-0.534948450048" Y="-28.307821229827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.652800008291" Y="-23.752669543606" />
                  <Point X="-3.793738383135" Y="-24.673900350405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.331051653014" Y="-25.170071122331" />
                  <Point X="-0.479522081421" Y="-28.227962210617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.582786397098" Y="-23.688453426889" />
                  <Point X="-3.620574475744" Y="-24.720299383762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.297666494696" Y="-25.066575798854" />
                  <Point X="-0.420979822897" Y="-28.151444574237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.434711890497" Y="-23.707947371885" />
                  <Point X="-3.424311341458" Y="-24.791469305265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.332153118358" Y="-24.890296900089" />
                  <Point X="-0.342915243643" Y="-28.095862063754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.286637383895" Y="-23.72744131688" />
                  <Point X="-0.244559053863" Y="-28.062039641475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.138562877294" Y="-23.746935261876" />
                  <Point X="-0.143819047411" Y="-28.030773549607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.805284698324" Y="-29.0485627091" />
                  <Point X="0.821754050649" Y="-29.066223927208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990488370693" Y="-23.766429206872" />
                  <Point X="-0.041126013377" Y="-28.001601823407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.752911293274" Y="-28.853102585651" />
                  <Point X="0.800400602311" Y="-28.904028634723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842413864092" Y="-23.785923151867" />
                  <Point X="0.099618349663" Y="-28.013235151807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.700537888223" Y="-28.657642462202" />
                  <Point X="0.779047153972" Y="-28.741833342238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.706665302046" Y="-23.7921991396" />
                  <Point X="0.282418484501" Y="-28.06996777396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.643612050156" Y="-28.457300452031" />
                  <Point X="0.757693705634" Y="-28.579638049753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604329416812" Y="-23.762644418202" />
                  <Point X="0.736340257295" Y="-28.417442757268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.521722608218" Y="-23.711932852338" />
                  <Point X="0.727228535676" Y="-28.268375109473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.465501588605" Y="-23.632925991981" />
                  <Point X="0.750528941348" Y="-28.154065212811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.217076738425" Y="-22.687663795446" />
                  <Point X="-4.131379033559" Y="-22.779563332665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.428259867217" Y="-23.533566326069" />
                  <Point X="0.775082669726" Y="-28.041099340202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167070932255" Y="-22.601991934666" />
                  <Point X="-3.674732389924" Y="-23.129960382202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442357679254" Y="-23.379151750925" />
                  <Point X="0.810596905258" Y="-27.939887172511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.117065126086" Y="-22.516320073886" />
                  <Point X="0.871408510818" Y="-27.865803112885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.067059319916" Y="-22.430648213106" />
                  <Point X="0.944574520018" Y="-27.804967529152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.017053513746" Y="-22.344976352326" />
                  <Point X="1.017740529218" Y="-27.74413194542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.960651907074" Y="-22.266163147882" />
                  <Point X="1.097895524962" Y="-27.690791132172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.901570951528" Y="-22.190223193332" />
                  <Point X="1.204042692029" Y="-27.665323510156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842489995982" Y="-22.114283238782" />
                  <Point X="1.323673528623" Y="-27.654315353439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.783409040436" Y="-22.038343284232" />
                  <Point X="1.447805300772" Y="-27.648133859175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.638580332875" Y="-28.925083744281" />
                  <Point X="2.734919779293" Y="-29.028395152161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716766321687" Y="-21.970512327933" />
                  <Point X="1.686585296166" Y="-27.76489753218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.035186937286" Y="-28.13872702438" />
                  <Point X="2.527335515583" Y="-28.666491760429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435370112537" Y="-22.13297629511" />
                  <Point X="2.316178054067" Y="-28.300756583175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.183516393468" Y="-22.263759820306" />
                  <Point X="2.105020592551" Y="-27.935021405922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.041934711803" Y="-22.276291063001" />
                  <Point X="2.025774490004" Y="-27.710743842523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.937481489382" Y="-22.249006907751" />
                  <Point X="2.000422500956" Y="-27.544260640095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.862294219516" Y="-22.190338860712" />
                  <Point X="2.024245029794" Y="-27.430510651979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.797211460913" Y="-22.120835051964" />
                  <Point X="2.070605736331" Y="-27.340929900409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754249515262" Y="-22.027609575566" />
                  <Point X="2.117468415012" Y="-27.251887448058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754729582703" Y="-21.887798243628" />
                  <Point X="2.17772565176" Y="-27.177208900663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874885466779" Y="-21.619650310584" />
                  <Point X="2.259348075773" Y="-27.125441711575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.04982428169" Y="-21.292754876668" />
                  <Point X="2.346481593556" Y="-27.079584447004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.974080946196" Y="-21.234683137009" />
                  <Point X="2.437262642646" Y="-27.037638680876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.898337610702" Y="-21.17661139735" />
                  <Point X="2.558310403956" Y="-27.028149989887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.822594275208" Y="-21.118539657691" />
                  <Point X="2.714240228366" Y="-27.056067731908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.746850939714" Y="-21.060467918032" />
                  <Point X="2.917086742656" Y="-27.134297464135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.67110760422" Y="-21.002396178373" />
                  <Point X="3.198483806367" Y="-27.296762347716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.588491715581" Y="-20.951694349665" />
                  <Point X="3.479880870079" Y="-27.459227231297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.502925970918" Y="-20.904155854255" />
                  <Point X="3.76127793379" Y="-27.621692114879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.417360226255" Y="-20.856617358846" />
                  <Point X="2.86218414195" Y="-26.518235542496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.425328082246" Y="-27.122133483309" />
                  <Point X="4.009493751952" Y="-27.748574468973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.331794481593" Y="-20.809078863436" />
                  <Point X="2.867100574642" Y="-26.384211248444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.88197430145" Y="-27.472530077699" />
                  <Point X="4.063906000217" Y="-27.667627938818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.24622873693" Y="-20.761540368027" />
                  <Point X="2.882703395757" Y="-26.26164670296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.947677176633" Y="-20.942401196982" />
                  <Point X="2.926745699706" Y="-26.169579768997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.794572340272" Y="-20.967289510214" />
                  <Point X="2.987565325664" Y="-26.095504310194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.695390726934" Y="-20.934352246332" />
                  <Point X="3.049627190669" Y="-26.022760989675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.605076393465" Y="-20.891905988975" />
                  <Point X="3.128788086227" Y="-25.968354134493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.536710824261" Y="-20.825922563596" />
                  <Point X="3.231658493408" Y="-25.939372617706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.495008533213" Y="-20.731346273016" />
                  <Point X="3.339664058897" Y="-25.915897884009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465151567687" Y="-20.624067425987" />
                  <Point X="3.467085720338" Y="-25.913244364082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474316708641" Y="-20.474942492969" />
                  <Point X="3.615160662348" Y="-25.932738775996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.4001616274" Y="-20.415167559146" />
                  <Point X="3.763235604357" Y="-25.95223318791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.297187485892" Y="-20.386297283805" />
                  <Point X="3.911310546367" Y="-25.971727599824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.194213344383" Y="-20.357427008464" />
                  <Point X="4.059385488377" Y="-25.991222011738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.091239202875" Y="-20.328556733124" />
                  <Point X="3.355921009254" Y="-25.097552193077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.724276213481" Y="-25.492564788266" />
                  <Point X="4.207460430386" Y="-26.010716423653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.988265061367" Y="-20.299686457783" />
                  <Point X="3.356993830714" Y="-24.959406130607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.897439939438" Y="-25.538963627057" />
                  <Point X="4.355535372396" Y="-26.030210835567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.878993742542" Y="-20.277569078358" />
                  <Point X="3.383355280329" Y="-24.84837880169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.070604037444" Y="-25.585362864823" />
                  <Point X="4.503610314406" Y="-26.049705247481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.761878867011" Y="-20.26386288372" />
                  <Point X="3.434773673723" Y="-24.764221755249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.24376813545" Y="-25.631762102588" />
                  <Point X="4.651685256415" Y="-26.069199659395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.64476399148" Y="-20.250156689083" />
                  <Point X="-0.240213001337" Y="-20.683984512522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.027727252841" Y="-20.911847580535" />
                  <Point X="3.496449209276" Y="-24.691064147114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.416932233456" Y="-25.678161340353" />
                  <Point X="4.740212798712" Y="-26.024837303093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.527649115949" Y="-20.236450494445" />
                  <Point X="-0.292586332289" Y="-20.488524468533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.089678606595" Y="-20.898453427931" />
                  <Point X="3.575477835671" Y="-24.636515450621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.590096331461" Y="-25.724560578119" />
                  <Point X="4.764541708244" Y="-25.911630341789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.410534240418" Y="-20.222744299807" />
                  <Point X="-0.344959663241" Y="-20.293064424544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.171955389645" Y="-20.847387953" />
                  <Point X="3.661672841746" Y="-24.58965175546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.763260429467" Y="-25.770959815884" />
                  <Point X="4.782620171633" Y="-25.791720597616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.226529536878" Y="-20.766615038233" />
                  <Point X="2.020666717692" Y="-22.69059161243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.320569247802" Y="-23.012197701777" />
                  <Point X="3.039725576588" Y="-23.783398446383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.236074341576" Y="-23.993956718209" />
                  <Point X="3.763451089242" Y="-24.5594990408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.258517114102" Y="-20.661620992522" />
                  <Point X="2.018188578134" Y="-22.548637610473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.461018935107" Y="-23.02351502914" />
                  <Point X="3.000892589913" Y="-23.602458643921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.412486388245" Y="-24.043838954492" />
                  <Point X="3.867379037023" Y="-24.531651597462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.28751049461" Y="-20.553416063941" />
                  <Point X="2.047731284911" Y="-22.441021762195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.567908387634" Y="-22.998843410826" />
                  <Point X="3.022137762265" Y="-23.485944779354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.540608458396" Y="-24.04193653095" />
                  <Point X="3.971306984804" Y="-24.503804154123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.316503875117" Y="-20.445211135359" />
                  <Point X="2.094130293644" Y="-22.3514820847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.659991078283" Y="-22.958293484378" />
                  <Point X="3.062156988885" Y="-23.389563623146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.656301201083" Y="-24.026705285548" />
                  <Point X="4.075234932585" Y="-24.475956710785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.345497255625" Y="-20.337006206778" />
                  <Point X="2.143800631591" Y="-22.265450478295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.744427548534" Y="-22.909543990424" />
                  <Point X="3.119777267888" Y="-23.312057284775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.771993881865" Y="-24.011473973762" />
                  <Point X="4.179162880366" Y="-24.448109267447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.374490636133" Y="-20.228801278196" />
                  <Point X="2.193470969538" Y="-22.17941887189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.828864140135" Y="-22.860794626601" />
                  <Point X="3.187299886179" Y="-23.245169905213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.887686562647" Y="-23.996242661976" />
                  <Point X="4.283090828147" Y="-24.420261824108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.503774029469" Y="-20.2281442213" />
                  <Point X="2.243141307485" Y="-22.093387265485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.913300731735" Y="-22.812045262779" />
                  <Point X="3.263017036735" Y="-23.187070085647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.003379243429" Y="-23.98101135019" />
                  <Point X="4.387018775928" Y="-24.39241438077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.647728767581" Y="-20.243220255475" />
                  <Point X="2.292811645432" Y="-22.00735565908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.997737323336" Y="-22.763295898957" />
                  <Point X="3.338734187292" Y="-23.12897026608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.119071924211" Y="-23.965780038404" />
                  <Point X="4.490946723709" Y="-24.364566937432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791683505693" Y="-20.258296289651" />
                  <Point X="2.342481983379" Y="-21.921324052675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.082173914936" Y="-22.714546535135" />
                  <Point X="3.414451337848" Y="-23.070870446513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.234764604993" Y="-23.950548726617" />
                  <Point X="4.59487467149" Y="-24.336719494093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.953371288435" Y="-20.292388686021" />
                  <Point X="2.392152321326" Y="-21.83529244627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.166610506536" Y="-22.665797171312" />
                  <Point X="3.490168488405" Y="-23.012770626946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.350457285775" Y="-23.935317414831" />
                  <Point X="4.698802619271" Y="-24.308872050755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.12100923704" Y="-20.332861854082" />
                  <Point X="2.441822659273" Y="-21.749260839865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251047098137" Y="-22.61704780749" />
                  <Point X="3.565885638961" Y="-22.95467080738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.466149966557" Y="-23.920086103045" />
                  <Point X="4.779146101402" Y="-24.255733364411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.288647185645" Y="-20.373335022142" />
                  <Point X="2.49149299722" Y="-21.663229233459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.335483689737" Y="-22.568298443668" />
                  <Point X="3.641602789518" Y="-22.896570987813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.581842647339" Y="-23.904854791259" />
                  <Point X="4.753111689307" Y="-24.088518352861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.456740193402" Y="-20.4142961814" />
                  <Point X="2.541163335167" Y="-21.577197627054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419920281338" Y="-22.519549079845" />
                  <Point X="3.717319940074" Y="-22.838471168246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.697535328121" Y="-23.889623479473" />
                  <Point X="4.707547679056" Y="-23.900360411329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.653026237661" Y="-20.485490670842" />
                  <Point X="2.590833673114" Y="-21.491166020649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.504356872938" Y="-22.470799716023" />
                  <Point X="3.793037090631" Y="-22.780371348679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.84931228192" Y="-20.556685160284" />
                  <Point X="2.64050401106" Y="-21.405134414244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.588793464539" Y="-22.422050352201" />
                  <Point X="3.868754241187" Y="-22.722271529113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.078115916161" Y="-20.662750495748" />
                  <Point X="2.690174349007" Y="-21.319102807839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.673230056139" Y="-22.373300988378" />
                  <Point X="3.944471391744" Y="-22.664171709546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.322091200876" Y="-20.78508543446" />
                  <Point X="2.739844686954" Y="-21.233071201434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.75766664774" Y="-22.324551624556" />
                  <Point X="4.0201885423" Y="-22.606071889979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.606505548421" Y="-20.950785958813" />
                  <Point X="2.789515024901" Y="-21.147039595029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.84210323934" Y="-22.275802260734" />
                  <Point X="4.095905692857" Y="-22.547972070412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.92653983094" Y="-22.227052896911" />
                  <Point X="4.01246123419" Y="-22.319192321278" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998374023438" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.85812677002" Y="-29.979876953125" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464321105957" Y="-28.521546875" />
                  <Point X="0.306312866211" Y="-28.293888671875" />
                  <Point X="0.300593475342" Y="-28.285646484375" />
                  <Point X="0.274335449219" Y="-28.2663984375" />
                  <Point X="0.029827056885" Y="-28.190513671875" />
                  <Point X="0.020982406616" Y="-28.187767578125" />
                  <Point X="-0.008660801888" Y="-28.187765625" />
                  <Point X="-0.253169342041" Y="-28.26365234375" />
                  <Point X="-0.262019927979" Y="-28.2663984375" />
                  <Point X="-0.288278259277" Y="-28.285642578125" />
                  <Point X="-0.446286468506" Y="-28.513302734375" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.47322064209" Y="-28.5893359375" />
                  <Point X="-0.847743896484" Y="-29.987076171875" />
                  <Point X="-1.091244262695" Y="-29.9398125" />
                  <Point X="-1.100260253906" Y="-29.9380625" />
                  <Point X="-1.35158972168" Y="-29.8733984375" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683105469" Y="-29.534759765625" />
                  <Point X="-1.368096191406" Y="-29.24109765625" />
                  <Point X="-1.370210571289" Y="-29.230466796875" />
                  <Point X="-1.38628137207" Y="-29.20262890625" />
                  <Point X="-1.611393676758" Y="-29.0052109375" />
                  <Point X="-1.619542236328" Y="-28.998064453125" />
                  <Point X="-1.649239990234" Y="-28.985763671875" />
                  <Point X="-1.948014282227" Y="-28.966181640625" />
                  <Point X="-1.958828979492" Y="-28.96547265625" />
                  <Point X="-1.989878662109" Y="-28.973791015625" />
                  <Point X="-2.238833251953" Y="-29.14013671875" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.267590332031" Y="-29.167533203125" />
                  <Point X="-2.457095214844" Y="-29.414501953125" />
                  <Point X="-2.84265234375" Y="-29.1757734375" />
                  <Point X="-2.855822998047" Y="-29.167619140625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-3.224463378906" Y="-28.8734765625" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.59759375" />
                  <Point X="-2.513979980469" Y="-27.568763671875" />
                  <Point X="-2.530722412109" Y="-27.552021484375" />
                  <Point X="-2.531349609375" Y="-27.55139453125" />
                  <Point X="-2.560172363281" Y="-27.537197265625" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.636726074219" Y="-27.5694765625" />
                  <Point X="-3.842959472656" Y="-28.265896484375" />
                  <Point X="-4.151288085938" Y="-27.860814453125" />
                  <Point X="-4.161698242187" Y="-27.847138671875" />
                  <Point X="-4.43101953125" Y="-27.39552734375" />
                  <Point X="-4.420805664062" Y="-27.387689453125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145821777344" Y="-26.396013671875" />
                  <Point X="-3.138386474609" Y="-26.3673046875" />
                  <Point X="-3.138119384766" Y="-26.3662734375" />
                  <Point X="-3.140324707031" Y="-26.334599609375" />
                  <Point X="-3.161157958984" Y="-26.310640625" />
                  <Point X="-3.186715576172" Y="-26.29559765625" />
                  <Point X="-3.187692626953" Y="-26.2950234375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.276391357422" Y="-26.296056640625" />
                  <Point X="-4.803283691406" Y="-26.497076171875" />
                  <Point X="-4.923318847656" Y="-26.027142578125" />
                  <Point X="-4.927392089844" Y="-26.011197265625" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-4.989397460938" Y="-25.51233203125" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541895019531" Y="-25.12142578125" />
                  <Point X="-3.515111572266" Y="-25.1028359375" />
                  <Point X="-3.514171386719" Y="-25.10218359375" />
                  <Point X="-3.494897705078" Y="-25.075904296875" />
                  <Point X="-3.485969726563" Y="-25.047138671875" />
                  <Point X="-3.485646728516" Y="-25.04609765625" />
                  <Point X="-3.485647949219" Y="-25.0164609375" />
                  <Point X="-3.494575683594" Y="-24.9876953125" />
                  <Point X="-3.494893066406" Y="-24.986671875" />
                  <Point X="-3.514144287109" Y="-24.960396484375" />
                  <Point X="-3.540899169922" Y="-24.941828125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.609296386719" Y="-24.920025390625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.920270019531" Y="-24.0213203125" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.773516113281" Y="-23.471703125" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731705810547" Y="-23.6041328125" />
                  <Point X="-3.672425537109" Y="-23.585443359375" />
                  <Point X="-3.670254882812" Y="-23.5847578125" />
                  <Point X="-3.651525146484" Y="-23.573935546875" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.6153359375" Y="-23.498794921875" />
                  <Point X="-3.614474853516" Y="-23.496716796875" />
                  <Point X="-3.610713867188" Y="-23.4753984375" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.645016845703" Y="-23.399353515625" />
                  <Point X="-3.6460625" Y="-23.39734765625" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.689699462891" Y="-23.35796484375" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.170213378906" Y="-22.23046875" />
                  <Point X="-4.160010253906" Y="-22.21298828125" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138519042969" Y="-22.079564453125" />
                  <Point X="-3.055958251953" Y="-22.086787109375" />
                  <Point X="-3.052969726562" Y="-22.087048828125" />
                  <Point X="-3.031510009766" Y="-22.0842265625" />
                  <Point X="-3.013254394531" Y="-22.07259765625" />
                  <Point X="-2.954652099609" Y="-22.01399609375" />
                  <Point X="-2.952530761719" Y="-22.011875" />
                  <Point X="-2.940900390625" Y="-21.99362109375" />
                  <Point X="-2.93807421875" Y="-21.972162109375" />
                  <Point X="-2.945297363281" Y="-21.8896015625" />
                  <Point X="-2.945558837891" Y="-21.886611328125" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-2.9652421875" Y="-21.8431484375" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.770637939453" Y="-20.839287109375" />
                  <Point X="-2.752870117188" Y="-20.8256640625" />
                  <Point X="-2.141548828125" Y="-20.48602734375" />
                  <Point X="-1.967826904297" Y="-20.71242578125" />
                  <Point X="-1.951247070312" Y="-20.726337890625" />
                  <Point X="-1.859366088867" Y="-20.774169921875" />
                  <Point X="-1.856040283203" Y="-20.77590234375" />
                  <Point X="-1.835129394531" Y="-20.7815078125" />
                  <Point X="-1.813808837891" Y="-20.77775" />
                  <Point X="-1.718099365234" Y="-20.73810546875" />
                  <Point X="-1.714629150391" Y="-20.73666796875" />
                  <Point X="-1.696903686523" Y="-20.72425390625" />
                  <Point X="-1.686083740234" Y="-20.705513671875" />
                  <Point X="-1.654932128906" Y="-20.606712890625" />
                  <Point X="-1.65380456543" Y="-20.60313671875" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.653415527344" Y="-20.570197265625" />
                  <Point X="-1.689137573242" Y="-20.298861328125" />
                  <Point X="-0.991088378906" Y="-20.10315234375" />
                  <Point X="-0.968082092285" Y="-20.096701171875" />
                  <Point X="-0.224200073242" Y="-20.009640625" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282121658" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594039917" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.061202869415" Y="-20.663904296875" />
                  <Point X="0.236648391724" Y="-20.0091328125" />
                  <Point X="0.840124389648" Y="-20.07233203125" />
                  <Point X="0.860209472656" Y="-20.074435546875" />
                  <Point X="1.488306152344" Y="-20.226078125" />
                  <Point X="1.50845690918" Y="-20.230943359375" />
                  <Point X="1.918211914062" Y="-20.379564453125" />
                  <Point X="1.93104309082" Y="-20.38421875" />
                  <Point X="2.326354003906" Y="-20.569091796875" />
                  <Point X="2.338696044922" Y="-20.57486328125" />
                  <Point X="2.720592041016" Y="-20.797357421875" />
                  <Point X="2.732518310547" Y="-20.8043046875" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="3.061382080078" Y="-21.05615234375" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.204132324219" Y="-22.58596875" />
                  <Point X="2.202044677734" Y="-22.607673828125" />
                  <Point X="2.210123779297" Y="-22.674673828125" />
                  <Point X="2.210416015625" Y="-22.67709765625" />
                  <Point X="2.218681884766" Y="-22.6991875" />
                  <Point X="2.260138916016" Y="-22.76028515625" />
                  <Point X="2.260143554688" Y="-22.760291015625" />
                  <Point X="2.274939941406" Y="-22.775796875" />
                  <Point X="2.336036865234" Y="-22.81725390625" />
                  <Point X="2.338232666016" Y="-22.818744140625" />
                  <Point X="2.360337646484" Y="-22.827021484375" />
                  <Point X="2.427337402344" Y="-22.835099609375" />
                  <Point X="2.448664306641" Y="-22.8340546875" />
                  <Point X="2.526145996094" Y="-22.813333984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.593167480469" Y="-22.777482421875" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.195513671875" Y="-22.248283203125" />
                  <Point X="4.202592773438" Y="-22.25812109375" />
                  <Point X="4.387513183594" Y="-22.563703125" />
                  <Point X="4.383401367188" Y="-22.566859375" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.223610595703" Y="-23.488912109375" />
                  <Point X="3.213118652344" Y="-23.50850390625" />
                  <Point X="3.192346679688" Y="-23.582779296875" />
                  <Point X="3.191594726562" Y="-23.58546875" />
                  <Point X="3.190778808594" Y="-23.609033203125" />
                  <Point X="3.207830566406" Y="-23.691673828125" />
                  <Point X="3.215646484375" Y="-23.712044921875" />
                  <Point X="3.262025146484" Y="-23.7825390625" />
                  <Point X="3.263709960938" Y="-23.785099609375" />
                  <Point X="3.280947265625" Y="-23.8011796875" />
                  <Point X="3.348156494141" Y="-23.839013671875" />
                  <Point X="3.348161865234" Y="-23.839017578125" />
                  <Point X="3.368565429688" Y="-23.846380859375" />
                  <Point X="3.459436767578" Y="-23.858390625" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.525327148438" Y="-23.85230859375" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.9361484375" Y="-24.03612890625" />
                  <Point X="4.939187988281" Y="-24.04861328125" />
                  <Point X="4.997858886719" Y="-24.4254453125" />
                  <Point X="4.995814941406" Y="-24.4259921875" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087402344" Y="-24.767181640625" />
                  <Point X="3.639809082031" Y="-24.81878515625" />
                  <Point X="3.639809326172" Y="-24.81878515625" />
                  <Point X="3.622264892578" Y="-24.833072265625" />
                  <Point X="3.568697998047" Y="-24.901330078125" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985595703" Y="-24.925263671875" />
                  <Point X="3.539129882812" Y="-25.0185" />
                  <Point X="3.538483398438" Y="-25.0406875" />
                  <Point X="3.556339111328" Y="-25.133921875" />
                  <Point X="3.556985595703" Y="-25.137296875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.620325927734" Y="-25.227017578125" />
                  <Point X="3.636577392578" Y="-25.241908203125" />
                  <Point X="3.725855712891" Y="-25.29351171875" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="3.786583007812" Y="-25.312556640625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.950129394531" Y="-25.955138671875" />
                  <Point X="4.948430664062" Y="-25.96640625" />
                  <Point X="4.874546386719" Y="-26.290177734375" />
                  <Point X="4.867020507812" Y="-26.2891875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394833740234" Y="-26.098341796875" />
                  <Point X="3.219612060547" Y="-26.13642578125" />
                  <Point X="3.21326953125" Y="-26.1378046875" />
                  <Point X="3.185445068359" Y="-26.154697265625" />
                  <Point X="3.079534667969" Y="-26.282076171875" />
                  <Point X="3.075706054688" Y="-26.2866796875" />
                  <Point X="3.064357666016" Y="-26.3140703125" />
                  <Point X="3.049178222656" Y="-26.479029296875" />
                  <Point X="3.048628662109" Y="-26.485001953125" />
                  <Point X="3.056361816406" Y="-26.516625" />
                  <Point X="3.15333203125" Y="-26.667455078125" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.210606445312" Y="-26.717880859375" />
                  <Point X="4.339073730469" Y="-27.583783203125" />
                  <Point X="4.208915527344" Y="-27.794400390625" />
                  <Point X="4.204125488281" Y="-27.80215234375" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="4.049362304688" Y="-28.00741015625" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340820313" Y="-27.253314453125" />
                  <Point X="2.528799072266" Y="-27.21565234375" />
                  <Point X="2.521250488281" Y="-27.2142890625" />
                  <Point X="2.489075439453" Y="-27.21924609375" />
                  <Point X="2.315828369141" Y="-27.310423828125" />
                  <Point X="2.309557373047" Y="-27.313724609375" />
                  <Point X="2.288598876953" Y="-27.334685546875" />
                  <Point X="2.197420166016" Y="-27.507931640625" />
                  <Point X="2.194119873047" Y="-27.514203125" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.226825439453" Y="-27.75491796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.261174316406" Y="-27.82548828125" />
                  <Point X="2.986673339844" Y="-29.082087890625" />
                  <Point X="2.840975830078" Y="-29.18615625" />
                  <Point X="2.835281738281" Y="-29.190224609375" />
                  <Point X="2.679776123047" Y="-29.290880859375" />
                  <Point X="2.672074707031" Y="-29.28084375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549316406" Y="-27.980466796875" />
                  <Point X="1.464870727539" Y="-27.848234375" />
                  <Point X="1.457420776367" Y="-27.843447265625" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.200868041992" Y="-27.85641796875" />
                  <Point X="1.192725463867" Y="-27.8571640625" />
                  <Point X="1.165330200195" Y="-27.86851171875" />
                  <Point X="0.991633789062" Y="-28.012935546875" />
                  <Point X="0.985346496582" Y="-28.0181640625" />
                  <Point X="0.96845690918" Y="-28.045986328125" />
                  <Point X="0.916522521973" Y="-28.284923828125" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.921604614258" Y="-28.369017578125" />
                  <Point X="1.127642333984" Y="-29.934029296875" />
                  <Point X="0.999714050293" Y="-29.9620703125" />
                  <Point X="0.99436340332" Y="-29.963244140625" />
                  <Point X="0.860200378418" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#214" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.186493938389" Y="5.051129362463" Z="2.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.45" />
                  <Point X="-0.220866811266" Y="5.073090251485" Z="2.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.45" />
                  <Point X="-1.010742330347" Y="4.976273180148" Z="2.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.45" />
                  <Point X="-1.709143720482" Y="4.454557701065" Z="2.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.45" />
                  <Point X="-1.708773506062" Y="4.439604238381" Z="2.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.45" />
                  <Point X="-1.743389593664" Y="4.339369066807" Z="2.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.45" />
                  <Point X="-1.842425240881" Y="4.301456096782" Z="2.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.45" />
                  <Point X="-2.127304019744" Y="4.600799361499" Z="2.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.45" />
                  <Point X="-2.157074521177" Y="4.59724461012" Z="2.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.45" />
                  <Point X="-2.80721617181" Y="4.231673251636" Z="2.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.45" />
                  <Point X="-3.014699499519" Y="3.163132353748" Z="2.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.45" />
                  <Point X="-3.001263224259" Y="3.137324409654" Z="2.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.45" />
                  <Point X="-2.996161271317" Y="3.05264229657" Z="2.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.45" />
                  <Point X="-3.057752055953" Y="2.994301474914" Z="2.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.45" />
                  <Point X="-3.77072699455" Y="3.365494449226" Z="2.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.45" />
                  <Point X="-3.808013230579" Y="3.360074237936" Z="2.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.45" />
                  <Point X="-4.219551951918" Y="2.826017456199" Z="2.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.45" />
                  <Point X="-3.726293530182" Y="1.633647701974" Z="2.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.45" />
                  <Point X="-3.6955233861" Y="1.608838405251" Z="2.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.45" />
                  <Point X="-3.667683445081" Y="1.551625714951" Z="2.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.45" />
                  <Point X="-3.693615711366" Y="1.493523541824" Z="2.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.45" />
                  <Point X="-4.779341102737" Y="1.609966627755" Z="2.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.45" />
                  <Point X="-4.821957150459" Y="1.59470444553" Z="2.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.45" />
                  <Point X="-4.975887284906" Y="1.017278921801" Z="2.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.45" />
                  <Point X="-3.628393228122" Y="0.062957506772" Z="2.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.45" />
                  <Point X="-3.575591242911" Y="0.048396148711" Z="2.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.45" />
                  <Point X="-3.548484689604" Y="0.028765703159" Z="2.45" />
                  <Point X="-3.539556741714" Y="0" Z="2.45" />
                  <Point X="-3.539879911531" Y="-0.001041247902" Z="2.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.45" />
                  <Point X="-3.549777352213" Y="-0.030479834423" Z="2.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.45" />
                  <Point X="-5.008494560651" Y="-0.432754549984" Z="2.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.45" />
                  <Point X="-5.057613965754" Y="-0.465612645031" Z="2.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.45" />
                  <Point X="-4.977919268251" Y="-1.008237350879" Z="2.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.45" />
                  <Point X="-3.276020976027" Y="-1.314349342442" Z="2.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.45" />
                  <Point X="-3.21823377033" Y="-1.307407791875" Z="2.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.45" />
                  <Point X="-3.192945447377" Y="-1.323488969798" Z="2.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.45" />
                  <Point X="-4.457399602884" Y="-2.316741579739" Z="2.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.45" />
                  <Point X="-4.492646151143" Y="-2.36885086783" Z="2.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.45" />
                  <Point X="-4.197229054901" Y="-2.8598179251" Z="2.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.45" />
                  <Point X="-2.617882159948" Y="-2.581496362951" Z="2.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.45" />
                  <Point X="-2.572233461416" Y="-2.556097007715" Z="2.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.45" />
                  <Point X="-3.273920650217" Y="-3.817196222544" Z="2.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.45" />
                  <Point X="-3.285622684072" Y="-3.873251993604" Z="2.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.45" />
                  <Point X="-2.875127402235" Y="-4.187005323472" Z="2.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.45" />
                  <Point X="-2.234078563076" Y="-4.166690687133" Z="2.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.45" />
                  <Point X="-2.217210726085" Y="-4.150430844488" Z="2.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.45" />
                  <Point X="-1.957441101558" Y="-3.984793214201" Z="2.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.45" />
                  <Point X="-1.650518334014" Y="-4.011522000306" Z="2.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.45" />
                  <Point X="-1.423291570547" Y="-4.219570345972" Z="2.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.45" />
                  <Point X="-1.411414560544" Y="-4.866708283966" Z="2.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.45" />
                  <Point X="-1.402769443136" Y="-4.882160968309" Z="2.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.45" />
                  <Point X="-1.106931222036" Y="-4.957615955897" Z="2.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.45" />
                  <Point X="-0.431080309218" Y="-3.570997644404" Z="2.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.45" />
                  <Point X="-0.411367271071" Y="-3.510532356873" Z="2.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.45" />
                  <Point X="-0.244508472357" Y="-3.280125775053" Z="2.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.45" />
                  <Point X="0.008850607003" Y="-3.206986270235" Z="2.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.45" />
                  <Point X="0.259078588361" Y="-3.291113398895" Z="2.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.45" />
                  <Point X="0.803674551183" Y="-4.961538385412" Z="2.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.45" />
                  <Point X="0.823967980316" Y="-5.012618488993" Z="2.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.45" />
                  <Point X="1.00426467133" Y="-4.9796303915" Z="2.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.45" />
                  <Point X="0.9650208274" Y="-3.331211432098" Z="2.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.45" />
                  <Point X="0.959225679323" Y="-3.264264699969" Z="2.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.45" />
                  <Point X="1.017447398025" Y="-3.020098261636" Z="2.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.45" />
                  <Point X="1.199286182159" Y="-2.874926165926" Z="2.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.45" />
                  <Point X="1.431675509524" Y="-2.859013070496" Z="2.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.45" />
                  <Point X="2.626251020474" Y="-4.280000829135" Z="2.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.45" />
                  <Point X="2.668866543488" Y="-4.32223624741" Z="2.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.45" />
                  <Point X="2.863884276177" Y="-4.195562136892" Z="2.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.45" />
                  <Point X="2.298319646506" Y="-2.769207301493" Z="2.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.45" />
                  <Point X="2.269873606618" Y="-2.714749922581" Z="2.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.45" />
                  <Point X="2.235511556145" Y="-2.499937069511" Z="2.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.45" />
                  <Point X="2.332961308861" Y="-2.323389753822" Z="2.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.45" />
                  <Point X="2.513756880931" Y="-2.233574402873" Z="2.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.45" />
                  <Point X="4.018206295614" Y="-3.019429853769" Z="2.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.45" />
                  <Point X="4.071214540824" Y="-3.037845960853" Z="2.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.45" />
                  <Point X="4.245293595585" Y="-2.789404440447" Z="2.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.45" />
                  <Point X="3.234888359611" Y="-1.646932439825" Z="2.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.45" />
                  <Point X="3.189232739938" Y="-1.609133323086" Z="2.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.45" />
                  <Point X="3.0928119668" Y="-1.452331381231" Z="2.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.45" />
                  <Point X="3.111825152839" Y="-1.282761485542" Z="2.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.45" />
                  <Point X="3.224078125959" Y="-1.15400559323" Z="2.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.45" />
                  <Point X="4.854337931107" Y="-1.307479806147" Z="2.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.45" />
                  <Point X="4.909956196099" Y="-1.301488869315" Z="2.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.45" />
                  <Point X="4.993414786882" Y="-0.931313750835" Z="2.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.45" />
                  <Point X="3.793367853406" Y="-0.232979590089" Z="2.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.45" />
                  <Point X="3.744721043141" Y="-0.218942681483" Z="2.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.45" />
                  <Point X="3.653503720779" Y="-0.164867473465" Z="2.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.45" />
                  <Point X="3.599290392405" Y="-0.093235630079" Z="2.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.45" />
                  <Point X="3.582081058019" Y="0.003374901096" Z="2.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.45" />
                  <Point X="3.601875717621" Y="0.09908126511" Z="2.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.45" />
                  <Point X="3.658674371212" Y="0.169206162759" Z="2.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.45" />
                  <Point X="5.002599963901" Y="0.55699234268" Z="2.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.45" />
                  <Point X="5.04571295122" Y="0.583947735033" Z="2.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.45" />
                  <Point X="4.978573208071" Y="1.006980507907" Z="2.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.45" />
                  <Point X="3.512644752408" Y="1.22854395631" Z="2.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.45" />
                  <Point X="3.459832089817" Y="1.222458807423" Z="2.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.45" />
                  <Point X="3.366527808721" Y="1.235838040748" Z="2.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.45" />
                  <Point X="3.297639754699" Y="1.27622272913" Z="2.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.45" />
                  <Point X="3.250643765002" Y="1.349707819428" Z="2.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.45" />
                  <Point X="3.234343930832" Y="1.43503781015" Z="2.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.45" />
                  <Point X="3.257134493783" Y="1.511947005217" Z="2.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.45" />
                  <Point X="4.407683339794" Y="2.424753287114" Z="2.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.45" />
                  <Point X="4.440006401379" Y="2.467233710454" Z="2.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.45" />
                  <Point X="4.229942470277" Y="2.812201216807" Z="2.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.45" />
                  <Point X="2.562011428072" Y="2.297098060968" Z="2.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.45" />
                  <Point X="2.507073275549" Y="2.266248769348" Z="2.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.45" />
                  <Point X="2.427166432916" Y="2.245821611756" Z="2.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.45" />
                  <Point X="2.357955221162" Y="2.255401166272" Z="2.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.45" />
                  <Point X="2.295357433963" Y="2.29906963922" Z="2.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.45" />
                  <Point X="2.253608091003" Y="2.362592010916" Z="2.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.45" />
                  <Point X="2.246279173309" Y="2.432396300333" Z="2.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.45" />
                  <Point X="3.098527291892" Y="3.950127653883" Z="2.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.45" />
                  <Point X="3.115522191372" Y="4.011580282588" Z="2.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.45" />
                  <Point X="2.739603735642" Y="4.277126738268" Z="2.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.45" />
                  <Point X="2.34137968105" Y="4.50747986903" Z="2.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.45" />
                  <Point X="1.929104565101" Y="4.698720460821" Z="2.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.45" />
                  <Point X="1.493885111295" Y="4.853806303406" Z="2.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.45" />
                  <Point X="0.839177475654" Y="5.008676604885" Z="2.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.45" />
                  <Point X="0.006750623164" Y="4.380318143488" Z="2.45" />
                  <Point X="0" Y="4.355124473572" Z="2.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>