<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#175" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2213" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="-24.995283203125" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.792180480957" Y="-29.3667109375" />
                  <Point X="0.563302001953" Y="-28.5125234375" />
                  <Point X="0.557721679688" Y="-28.497142578125" />
                  <Point X="0.542363098145" Y="-28.467375" />
                  <Point X="0.448814666748" Y="-28.33258984375" />
                  <Point X="0.378635437012" Y="-28.231474609375" />
                  <Point X="0.356752288818" Y="-28.209021484375" />
                  <Point X="0.330497253418" Y="-28.18977734375" />
                  <Point X="0.3024949646" Y="-28.17566796875" />
                  <Point X="0.157734115601" Y="-28.130740234375" />
                  <Point X="0.049135822296" Y="-28.09703515625" />
                  <Point X="0.020976730347" Y="-28.092765625" />
                  <Point X="-0.008664140701" Y="-28.092765625" />
                  <Point X="-0.036824295044" Y="-28.09703515625" />
                  <Point X="-0.181584976196" Y="-28.14196484375" />
                  <Point X="-0.290183258057" Y="-28.17566796875" />
                  <Point X="-0.31818359375" Y="-28.189775390625" />
                  <Point X="-0.34443939209" Y="-28.20901953125" />
                  <Point X="-0.366323608398" Y="-28.2314765625" />
                  <Point X="-0.459872039795" Y="-28.366263671875" />
                  <Point X="-0.530051269531" Y="-28.467376953125" />
                  <Point X="-0.538188781738" Y="-28.481572265625" />
                  <Point X="-0.55099017334" Y="-28.5125234375" />
                  <Point X="-0.722692687988" Y="-29.153328125" />
                  <Point X="-0.916584533691" Y="-29.87694140625" />
                  <Point X="-0.95514074707" Y="-29.86945703125" />
                  <Point X="-1.079354858398" Y="-29.845345703125" />
                  <Point X="-1.244067138672" Y="-29.80296875" />
                  <Point X="-1.24641796875" Y="-29.80236328125" />
                  <Point X="-1.242514526367" Y="-29.77271484375" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.251092163086" Y="-29.34236328125" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323644775391" Y="-29.131203125" />
                  <Point X="-1.456922119141" Y="-29.014322265625" />
                  <Point X="-1.556905639648" Y="-28.926638671875" />
                  <Point X="-1.583189208984" Y="-28.910294921875" />
                  <Point X="-1.61288659668" Y="-28.897994140625" />
                  <Point X="-1.64302746582" Y="-28.890966796875" />
                  <Point X="-1.819916259766" Y="-28.879373046875" />
                  <Point X="-1.952616699219" Y="-28.87067578125" />
                  <Point X="-1.983414794922" Y="-28.873708984375" />
                  <Point X="-2.014463378906" Y="-28.88202734375" />
                  <Point X="-2.042657470703" Y="-28.89480078125" />
                  <Point X="-2.19005078125" Y="-28.99328515625" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.431500732422" Y="-29.225091796875" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.619663085938" Y="-29.20210546875" />
                  <Point X="-2.801714111328" Y="-29.0893828125" />
                  <Point X="-3.029804443359" Y="-28.91376171875" />
                  <Point X="-3.104721435547" Y="-28.856078125" />
                  <Point X="-2.849106201172" Y="-28.41333984375" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405575927734" Y="-27.585189453125" />
                  <Point X="-2.414561279297" Y="-27.555572265625" />
                  <Point X="-2.428779785156" Y="-27.5267421875" />
                  <Point X="-2.446808105469" Y="-27.501583984375" />
                  <Point X="-2.464156738281" Y="-27.484236328125" />
                  <Point X="-2.489316894531" Y="-27.466208984375" />
                  <Point X="-2.518145263672" Y="-27.451994140625" />
                  <Point X="-2.54776171875" Y="-27.44301171875" />
                  <Point X="-2.578694091797" Y="-27.444025390625" />
                  <Point X="-2.610219238281" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.191863769531" Y="-27.7802890625" />
                  <Point X="-3.818022949219" Y="-28.141802734375" />
                  <Point X="-3.939034179688" Y="-27.982818359375" />
                  <Point X="-4.082862792969" Y="-27.793857421875" />
                  <Point X="-4.246384765625" Y="-27.51965625" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.850170410156" Y="-27.069572265625" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577148438" Y="-26.475595703125" />
                  <Point X="-3.066612548828" Y="-26.44846484375" />
                  <Point X="-3.053856689453" Y="-26.419833984375" />
                  <Point X="-3.046152099609" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.35966015625" />
                  <Point X="-3.045555908203" Y="-26.327990234375" />
                  <Point X="-3.052556396484" Y="-26.298244140625" />
                  <Point X="-3.068637695312" Y="-26.27226171875" />
                  <Point X="-3.089469482422" Y="-26.2483046875" />
                  <Point X="-3.112974365234" Y="-26.228765625" />
                  <Point X="-3.139461669922" Y="-26.213177734375" />
                  <Point X="-3.168723144531" Y="-26.20195703125" />
                  <Point X="-3.200607421875" Y="-26.1954765625" />
                  <Point X="-3.231929199219" Y="-26.194384765625" />
                  <Point X="-3.929635498047" Y="-26.28623828125" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.777825195312" Y="-26.21287890625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.877341796875" Y="-25.690154296875" />
                  <Point X="-4.892423828125" Y="-25.584701171875" />
                  <Point X="-4.380649902344" Y="-25.4475703125" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.509799804688" Y="-25.21082421875" />
                  <Point X="-3.478375488281" Y="-25.19259765625" />
                  <Point X="-3.465201660156" Y="-25.183384765625" />
                  <Point X="-3.441487060547" Y="-25.163611328125" />
                  <Point X="-3.425461914062" Y="-25.1464765625" />
                  <Point X="-3.414123779297" Y="-25.125939453125" />
                  <Point X="-3.401412353516" Y="-25.094171875" />
                  <Point X="-3.396932861328" Y="-25.0797421875" />
                  <Point X="-3.390720214844" Y="-25.05214453125" />
                  <Point X="-3.38840234375" Y="-25.03076953125" />
                  <Point X="-3.390950927734" Y="-25.00941796875" />
                  <Point X="-3.398483398438" Y="-24.97756640625" />
                  <Point X="-3.403157714844" Y="-24.963091796875" />
                  <Point X="-3.414549560547" Y="-24.93557421875" />
                  <Point X="-3.426132568359" Y="-24.915169921875" />
                  <Point X="-3.442363525391" Y="-24.8982265625" />
                  <Point X="-3.470034912109" Y="-24.875708984375" />
                  <Point X="-3.483352783203" Y="-24.8666328125" />
                  <Point X="-3.510820068359" Y="-24.85115234375" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.168868652344" Y="-24.671736328125" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.860740722656" Y="-24.268017578125" />
                  <Point X="-4.824488769531" Y="-24.02302734375" />
                  <Point X="-4.737397949219" Y="-23.70163671875" />
                  <Point X="-4.703551269531" Y="-23.576732421875" />
                  <Point X="-4.37337109375" Y="-23.620201171875" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744985839844" Y="-23.700658203125" />
                  <Point X="-3.723422363281" Y="-23.698771484375" />
                  <Point X="-3.703139404297" Y="-23.694736328125" />
                  <Point X="-3.668042724609" Y="-23.683671875" />
                  <Point X="-3.641713378906" Y="-23.675369140625" />
                  <Point X="-3.622773925781" Y="-23.667037109375" />
                  <Point X="-3.604030761719" Y="-23.65621484375" />
                  <Point X="-3.5873515625" Y="-23.643984375" />
                  <Point X="-3.573714599609" Y="-23.62843359375" />
                  <Point X="-3.56130078125" Y="-23.610705078125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.537268798828" Y="-23.558572265625" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915527344" Y="-23.513205078125" />
                  <Point X="-3.517157226562" Y="-23.491888671875" />
                  <Point X="-3.5158046875" Y="-23.471248046875" />
                  <Point X="-3.518951904297" Y="-23.4508046875" />
                  <Point X="-3.524554443359" Y="-23.4298984375" />
                  <Point X="-3.532050048828" Y="-23.410623046875" />
                  <Point X="-3.549042236328" Y="-23.37798046875" />
                  <Point X="-3.561789794922" Y="-23.353494140625" />
                  <Point X="-3.57328125" Y="-23.336294921875" />
                  <Point X="-3.587193115234" Y="-23.31971484375" />
                  <Point X="-3.602135742188" Y="-23.30541015625" />
                  <Point X="-3.966942626953" Y="-23.025482421875" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.222017578125" Y="-22.507673828125" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.850455810547" Y="-21.9698125" />
                  <Point X="-3.750503662109" Y="-21.841337890625" />
                  <Point X="-3.579892578125" Y="-21.93983984375" />
                  <Point X="-3.206656982422" Y="-22.155328125" />
                  <Point X="-3.187727294922" Y="-22.163658203125" />
                  <Point X="-3.167083496094" Y="-22.17016796875" />
                  <Point X="-3.146794677734" Y="-22.174205078125" />
                  <Point X="-3.097914550781" Y="-22.178482421875" />
                  <Point X="-3.061245361328" Y="-22.181689453125" />
                  <Point X="-3.04055859375" Y="-22.18123828125" />
                  <Point X="-3.019101074219" Y="-22.178412109375" />
                  <Point X="-2.999011474609" Y="-22.173494140625" />
                  <Point X="-2.980462158203" Y="-22.164345703125" />
                  <Point X="-2.962208740234" Y="-22.152716796875" />
                  <Point X="-2.946078125" Y="-22.139771484375" />
                  <Point X="-2.911382568359" Y="-22.105076171875" />
                  <Point X="-2.885354492188" Y="-22.079048828125" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.847712402344" Y="-21.915" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.031451904297" Y="-21.53846875" />
                  <Point X="-3.183332519531" Y="-21.275404296875" />
                  <Point X="-2.945961181641" Y="-21.0934140625" />
                  <Point X="-2.700620849609" Y="-20.905314453125" />
                  <Point X="-2.337286132812" Y="-20.703453125" />
                  <Point X="-2.167036376953" Y="-20.608865234375" />
                  <Point X="-2.157454345703" Y="-20.621353515625" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028891479492" Y="-20.78519921875" />
                  <Point X="-2.012311767578" Y="-20.799111328125" />
                  <Point X="-1.995115112305" Y="-20.810603515625" />
                  <Point X="-1.940711914062" Y="-20.83892578125" />
                  <Point X="-1.899899047852" Y="-20.860171875" />
                  <Point X="-1.880625488281" Y="-20.86766796875" />
                  <Point X="-1.859719238281" Y="-20.873271484375" />
                  <Point X="-1.839269042969" Y="-20.876419921875" />
                  <Point X="-1.818622192383" Y="-20.87506640625" />
                  <Point X="-1.797307128906" Y="-20.871306640625" />
                  <Point X="-1.777452636719" Y="-20.865517578125" />
                  <Point X="-1.720787963867" Y="-20.842044921875" />
                  <Point X="-1.678278686523" Y="-20.8244375" />
                  <Point X="-1.660147705078" Y="-20.814490234375" />
                  <Point X="-1.642417602539" Y="-20.802076171875" />
                  <Point X="-1.626865112305" Y="-20.7884375" />
                  <Point X="-1.614633422852" Y="-20.771755859375" />
                  <Point X="-1.603811279297" Y="-20.75301171875" />
                  <Point X="-1.595480224609" Y="-20.734078125" />
                  <Point X="-1.577036743164" Y="-20.675583984375" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165405273" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.557730224609" Y="-20.569173828125" />
                  <Point X="-1.576108886719" Y="-20.42957421875" />
                  <Point X="-1.584201904297" Y="-20.3681015625" />
                  <Point X="-1.267237915039" Y="-20.279236328125" />
                  <Point X="-0.949624511719" Y="-20.19019140625" />
                  <Point X="-0.509162475586" Y="-20.138640625" />
                  <Point X="-0.294711212158" Y="-20.113541015625" />
                  <Point X="-0.244316619873" Y="-20.3016171875" />
                  <Point X="-0.133903366089" Y="-20.713685546875" />
                  <Point X="-0.121129745483" Y="-20.741876953125" />
                  <Point X="-0.103271453857" Y="-20.768603515625" />
                  <Point X="-0.082113975525" Y="-20.791193359375" />
                  <Point X="-0.054818138123" Y="-20.805783203125" />
                  <Point X="-0.024380064011" Y="-20.816115234375" />
                  <Point X="0.006155911446" Y="-20.82115625" />
                  <Point X="0.036691894531" Y="-20.816115234375" />
                  <Point X="0.067129966736" Y="-20.805783203125" />
                  <Point X="0.094425918579" Y="-20.791193359375" />
                  <Point X="0.115583435059" Y="-20.768603515625" />
                  <Point X="0.133441726685" Y="-20.741876953125" />
                  <Point X="0.146215194702" Y="-20.713685546875" />
                  <Point X="0.229046310425" Y="-20.4045546875" />
                  <Point X="0.307419494629" Y="-20.1120625" />
                  <Point X="0.566721191406" Y="-20.13921875" />
                  <Point X="0.844031005859" Y="-20.168259765625" />
                  <Point X="1.208460571289" Y="-20.256244140625" />
                  <Point X="1.481038574219" Y="-20.3220546875" />
                  <Point X="1.717594116211" Y="-20.407853515625" />
                  <Point X="1.894645996094" Y="-20.472072265625" />
                  <Point X="2.124010253906" Y="-20.579337890625" />
                  <Point X="2.294557128906" Y="-20.65909765625" />
                  <Point X="2.516178222656" Y="-20.78821484375" />
                  <Point X="2.680993408203" Y="-20.884234375" />
                  <Point X="2.889956054688" Y="-21.032837890625" />
                  <Point X="2.943260742188" Y="-21.07074609375" />
                  <Point X="2.639885986328" Y="-21.596205078125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.4839453125" />
                  <Point X="2.120809814453" Y="-22.529818359375" />
                  <Point X="2.111607177734" Y="-22.564232421875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.112511230469" Y="-22.65871484375" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442382812" Y="-22.710396484375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140070800781" Y="-22.752529296875" />
                  <Point X="2.164615478516" Y="-22.788703125" />
                  <Point X="2.183028564453" Y="-22.815837890625" />
                  <Point X="2.194469238281" Y="-22.829677734375" />
                  <Point X="2.221597167969" Y="-22.85440625" />
                  <Point X="2.25776953125" Y="-22.878951171875" />
                  <Point X="2.284905761719" Y="-22.897365234375" />
                  <Point X="2.304947998047" Y="-22.9077265625" />
                  <Point X="2.327038085938" Y="-22.915994140625" />
                  <Point X="2.348964599609" Y="-22.921337890625" />
                  <Point X="2.388631591797" Y="-22.92612109375" />
                  <Point X="2.418389404297" Y="-22.929708984375" />
                  <Point X="2.436469482422" Y="-22.93015625" />
                  <Point X="2.473208007812" Y="-22.925830078125" />
                  <Point X="2.519080810547" Y="-22.9135625" />
                  <Point X="2.553494384766" Y="-22.904359375" />
                  <Point X="2.5652890625" Y="-22.900359375" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.228219970703" Y="-22.52053125" />
                  <Point X="3.967325927734" Y="-22.09380859375" />
                  <Point X="4.025517578125" Y="-22.174681640625" />
                  <Point X="4.123270507813" Y="-22.31053515625" />
                  <Point X="4.239773925781" Y="-22.503060546875" />
                  <Point X="4.262198242188" Y="-22.5401171875" />
                  <Point X="3.878949462891" Y="-22.834193359375" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221422363281" Y="-23.33976171875" />
                  <Point X="3.203973876953" Y="-23.358373046875" />
                  <Point X="3.170958984375" Y="-23.401443359375" />
                  <Point X="3.146191650391" Y="-23.43375390625" />
                  <Point X="3.136606689453" Y="-23.449087890625" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.109331787109" Y="-23.526888671875" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.107834960938" Y="-23.67716015625" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679931641" Y="-23.7310234375" />
                  <Point X="3.136282958984" Y="-23.764259765625" />
                  <Point X="3.163741455078" Y="-23.805994140625" />
                  <Point X="3.184340576172" Y="-23.8373046875" />
                  <Point X="3.198890869141" Y="-23.854546875" />
                  <Point X="3.216133544922" Y="-23.87063671875" />
                  <Point X="3.234346435547" Y="-23.88396484375" />
                  <Point X="3.274137451172" Y="-23.90636328125" />
                  <Point X="3.303988525391" Y="-23.92316796875" />
                  <Point X="3.320521240234" Y="-23.9305" />
                  <Point X="3.356120117188" Y="-23.9405625" />
                  <Point X="3.409920410156" Y="-23.947671875" />
                  <Point X="3.450280761719" Y="-23.953005859375" />
                  <Point X="3.462697998047" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.095862548828" Y="-23.873015625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.803951171875" Y="-23.89473046875" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.882649414063" Y="-24.302994140625" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.459391601562" Y="-24.471373046875" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704788818359" Y="-24.674416015625" />
                  <Point X="3.681547607422" Y="-24.684931640625" />
                  <Point X="3.628690673828" Y="-24.715482421875" />
                  <Point X="3.589037597656" Y="-24.73840234375" />
                  <Point X="3.574312988281" Y="-24.74890234375" />
                  <Point X="3.547530273438" Y="-24.774423828125" />
                  <Point X="3.515816162109" Y="-24.8148359375" />
                  <Point X="3.492024414062" Y="-24.84515234375" />
                  <Point X="3.480300292969" Y="-24.864431640625" />
                  <Point X="3.470526367188" Y="-24.885896484375" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.453109375" Y="-24.962595703125" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.455750488281" Y="-25.11375390625" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.470526367188" Y="-25.176662109375" />
                  <Point X="3.480300048828" Y="-25.198126953125" />
                  <Point X="3.492024414062" Y="-25.217408203125" />
                  <Point X="3.523738525391" Y="-25.2578203125" />
                  <Point X="3.547530273438" Y="-25.28813671875" />
                  <Point X="3.560000732422" Y="-25.30123828125" />
                  <Point X="3.589035644531" Y="-25.32415625" />
                  <Point X="3.641892822266" Y="-25.354708984375" />
                  <Point X="3.681545654297" Y="-25.37762890625" />
                  <Point X="3.692708251953" Y="-25.383138671875" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.273830566406" Y="-25.541466796875" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.878465332031" Y="-25.793236328125" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.807986816406" Y="-26.15484375" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.285834960937" Y="-26.116853515625" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374658935547" Y="-26.005509765625" />
                  <Point X="3.270919189453" Y="-26.02805859375" />
                  <Point X="3.193094726563" Y="-26.04497265625" />
                  <Point X="3.163975341797" Y="-26.056595703125" />
                  <Point X="3.136147949219" Y="-26.07348828125" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.049693115234" Y="-26.169375" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932861328" Y="-26.250330078125" />
                  <Point X="2.976589599609" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.960770507812" Y="-26.403029296875" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.033861816406" Y="-26.657296875" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.627760253906" Y="-27.157720703125" />
                  <Point X="4.213122070312" Y="-27.606884765625" />
                  <Point X="4.190892089844" Y="-27.64285546875" />
                  <Point X="4.124815429687" Y="-27.749779296875" />
                  <Point X="4.028980957031" Y="-27.8859453125" />
                  <Point X="3.568194335938" Y="-27.61991015625" />
                  <Point X="2.800954589844" Y="-27.176943359375" />
                  <Point X="2.786137451172" Y="-27.170015625" />
                  <Point X="2.754224365234" Y="-27.15982421875" />
                  <Point X="2.630757568359" Y="-27.137525390625" />
                  <Point X="2.538134033203" Y="-27.120798828125" />
                  <Point X="2.50677734375" Y="-27.120396484375" />
                  <Point X="2.474605712891" Y="-27.12535546875" />
                  <Point X="2.444833496094" Y="-27.135177734375" />
                  <Point X="2.342262939453" Y="-27.18916015625" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242386962891" Y="-27.246546875" />
                  <Point X="2.221426757812" Y="-27.267505859375" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.150549804688" Y="-27.393009765625" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.117973388672" Y="-27.686724609375" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.484128662109" Y="-28.40165625" />
                  <Point X="2.861283691406" Y="-29.05490625" />
                  <Point X="2.860264160156" Y="-29.055634765625" />
                  <Point X="2.781857177734" Y="-29.111638671875" />
                  <Point X="2.701764160156" Y="-29.16348046875" />
                  <Point X="2.344020996094" Y="-28.69726171875" />
                  <Point X="1.758546264648" Y="-27.934255859375" />
                  <Point X="1.747506835938" Y="-27.922181640625" />
                  <Point X="1.721923706055" Y="-27.900556640625" />
                  <Point X="1.60015222168" Y="-27.82226953125" />
                  <Point X="1.508800292969" Y="-27.7635390625" />
                  <Point X="1.479986328125" Y="-27.751166015625" />
                  <Point X="1.448365600586" Y="-27.743435546875" />
                  <Point X="1.417100585938" Y="-27.741119140625" />
                  <Point X="1.283922607422" Y="-27.753373046875" />
                  <Point X="1.184013793945" Y="-27.76256640625" />
                  <Point X="1.156362548828" Y="-27.7693984375" />
                  <Point X="1.128977050781" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.795462890625" />
                  <Point X="1.001758728027" Y="-27.88096875" />
                  <Point X="0.92461138916" Y="-27.945115234375" />
                  <Point X="0.904140808105" Y="-27.968865234375" />
                  <Point X="0.887248779297" Y="-27.99669140625" />
                  <Point X="0.875624267578" Y="-28.025810546875" />
                  <Point X="0.844876647949" Y="-28.1672734375" />
                  <Point X="0.821809997559" Y="-28.273396484375" />
                  <Point X="0.81972454834" Y="-28.289626953125" />
                  <Point X="0.81974230957" Y="-28.323119140625" />
                  <Point X="0.91391607666" Y="-29.03844140625" />
                  <Point X="1.022065307617" Y="-29.859916015625" />
                  <Point X="0.975721130371" Y="-29.87007421875" />
                  <Point X="0.929315368652" Y="-29.87850390625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058456787109" Y="-29.75262890625" />
                  <Point X="-1.14124609375" Y="-29.731328125" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.54103515625" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.157917602539" Y="-29.323830078125" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026245117" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221737060547" Y="-29.10762890625" />
                  <Point X="-1.230573242188" Y="-29.094861328125" />
                  <Point X="-1.250208251953" Y="-29.070935546875" />
                  <Point X="-1.261006835938" Y="-29.05977734375" />
                  <Point X="-1.394284301758" Y="-28.942896484375" />
                  <Point X="-1.494267822266" Y="-28.855212890625" />
                  <Point X="-1.506740112305" Y="-28.84596484375" />
                  <Point X="-1.533023681641" Y="-28.82962109375" />
                  <Point X="-1.546835083008" Y="-28.822525390625" />
                  <Point X="-1.576532348633" Y="-28.810224609375" />
                  <Point X="-1.591315917969" Y="-28.805474609375" />
                  <Point X="-1.621456787109" Y="-28.798447265625" />
                  <Point X="-1.636814086914" Y="-28.796169921875" />
                  <Point X="-1.81370300293" Y="-28.784576171875" />
                  <Point X="-1.946403564453" Y="-28.77587890625" />
                  <Point X="-1.961927856445" Y="-28.7761328125" />
                  <Point X="-1.992725952148" Y="-28.779166015625" />
                  <Point X="-2.007999633789" Y="-28.7819453125" />
                  <Point X="-2.039048217773" Y="-28.790263671875" />
                  <Point X="-2.053667724609" Y="-28.795494140625" />
                  <Point X="-2.081861816406" Y="-28.808267578125" />
                  <Point X="-2.095436279297" Y="-28.815810546875" />
                  <Point X="-2.242829589844" Y="-28.914294921875" />
                  <Point X="-2.353402832031" Y="-28.988177734375" />
                  <Point X="-2.359683349609" Y="-28.9927578125" />
                  <Point X="-2.380450683594" Y="-29.010138671875" />
                  <Point X="-2.402763427734" Y="-29.03277734375" />
                  <Point X="-2.410471923828" Y="-29.041630859375" />
                  <Point X="-2.503200927734" Y="-29.162478515625" />
                  <Point X="-2.569652587891" Y="-29.121333984375" />
                  <Point X="-2.747587890625" Y="-29.01116015625" />
                  <Point X="-2.971847412109" Y="-28.83848828125" />
                  <Point X="-2.980862060547" Y="-28.831546875" />
                  <Point X="-2.766833740234" Y="-28.46083984375" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.619232421875" />
                  <Point X="-2.310626708984" Y="-27.588296875" />
                  <Point X="-2.314667480469" Y="-27.557609375" />
                  <Point X="-2.323652832031" Y="-27.5279921875" />
                  <Point X="-2.329359619141" Y="-27.513552734375" />
                  <Point X="-2.343578125" Y="-27.48472265625" />
                  <Point X="-2.351559570312" Y="-27.47140625" />
                  <Point X="-2.369587890625" Y="-27.446248046875" />
                  <Point X="-2.379634765625" Y="-27.43440625" />
                  <Point X="-2.396983398438" Y="-27.41705859375" />
                  <Point X="-2.408825683594" Y="-27.407013671875" />
                  <Point X="-2.433985839844" Y="-27.388986328125" />
                  <Point X="-2.447303710938" Y="-27.38100390625" />
                  <Point X="-2.476132080078" Y="-27.3667890625" />
                  <Point X="-2.490572753906" Y="-27.361083984375" />
                  <Point X="-2.520189208984" Y="-27.3521015625" />
                  <Point X="-2.550873291016" Y="-27.3480625" />
                  <Point X="-2.581805664062" Y="-27.349076171875" />
                  <Point X="-2.597229736328" Y="-27.3508515625" />
                  <Point X="-2.628754882813" Y="-27.357123046875" />
                  <Point X="-2.643685546875" Y="-27.36138671875" />
                  <Point X="-2.672649902344" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.239363769531" Y="-27.698017578125" />
                  <Point X="-3.793087890625" Y="-28.0177109375" />
                  <Point X="-3.863440673828" Y="-27.92528125" />
                  <Point X="-4.004017578125" Y="-27.740591796875" />
                  <Point X="-4.164791992188" Y="-27.470998046875" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.792338134766" Y="-27.14494140625" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036482421875" Y="-26.5633125" />
                  <Point X="-3.015104980469" Y="-26.540392578125" />
                  <Point X="-3.005367431641" Y="-26.528044921875" />
                  <Point X="-2.987402832031" Y="-26.5009140625" />
                  <Point X="-2.979835449219" Y="-26.487126953125" />
                  <Point X="-2.967079589844" Y="-26.45849609375" />
                  <Point X="-2.961891113281" Y="-26.44365234375" />
                  <Point X="-2.954186523438" Y="-26.413904296875" />
                  <Point X="-2.951553222656" Y="-26.3988046875" />
                  <Point X="-2.948748779297" Y="-26.36837890625" />
                  <Point X="-2.948577636719" Y="-26.353052734375" />
                  <Point X="-2.950785888672" Y="-26.3213828125" />
                  <Point X="-2.953082275391" Y="-26.3062265625" />
                  <Point X="-2.960082763672" Y="-26.27648046875" />
                  <Point X="-2.971776855469" Y="-26.248248046875" />
                  <Point X="-2.987858154297" Y="-26.222265625" />
                  <Point X="-2.996949462891" Y="-26.20992578125" />
                  <Point X="-3.01778125" Y="-26.18596875" />
                  <Point X="-3.028740722656" Y="-26.17525" />
                  <Point X="-3.052245605469" Y="-26.1557109375" />
                  <Point X="-3.064791015625" Y="-26.146890625" />
                  <Point X="-3.091278320312" Y="-26.131302734375" />
                  <Point X="-3.105447753906" Y="-26.1244765625" />
                  <Point X="-3.134709228516" Y="-26.113255859375" />
                  <Point X="-3.149801269531" Y="-26.108861328125" />
                  <Point X="-3.181685546875" Y="-26.102380859375" />
                  <Point X="-3.197298095703" Y="-26.10053515625" />
                  <Point X="-3.228619873047" Y="-26.099443359375" />
                  <Point X="-3.244329101562" Y="-26.100197265625" />
                  <Point X="-3.942035400391" Y="-26.19205078125" />
                  <Point X="-4.660919921875" Y="-26.286693359375" />
                  <Point X="-4.685780273438" Y="-26.1893671875" />
                  <Point X="-4.740763183594" Y="-25.974111328125" />
                  <Point X="-4.783298828125" Y="-25.676705078125" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.356062011719" Y="-25.539333984375" />
                  <Point X="-3.508287841797" Y="-25.312173828125" />
                  <Point X="-3.496431884766" Y="-25.308142578125" />
                  <Point X="-3.473355712891" Y="-25.298556640625" />
                  <Point X="-3.462135742188" Y="-25.293001953125" />
                  <Point X="-3.430711425781" Y="-25.274775390625" />
                  <Point X="-3.423931396484" Y="-25.27044921875" />
                  <Point X="-3.404363769531" Y="-25.256349609375" />
                  <Point X="-3.380649169922" Y="-25.236576171875" />
                  <Point X="-3.372102783203" Y="-25.228501953125" />
                  <Point X="-3.356077636719" Y="-25.2113671875" />
                  <Point X="-3.342294677734" Y="-25.192392578125" />
                  <Point X="-3.330956542969" Y="-25.17185546875" />
                  <Point X="-3.325922607422" Y="-25.161232421875" />
                  <Point X="-3.313211181641" Y="-25.12946484375" />
                  <Point X="-3.31068359375" Y="-25.122337890625" />
                  <Point X="-3.304252197266" Y="-25.10060546875" />
                  <Point X="-3.298039550781" Y="-25.0730078125" />
                  <Point X="-3.296273925781" Y="-25.06238671875" />
                  <Point X="-3.293956054688" Y="-25.04101171875" />
                  <Point X="-3.294072021484" Y="-25.019509765625" />
                  <Point X="-3.296620605469" Y="-24.998158203125" />
                  <Point X="-3.298500976562" Y="-24.9875546875" />
                  <Point X="-3.306033447266" Y="-24.955703125" />
                  <Point X="-3.308080322266" Y="-24.948373046875" />
                  <Point X="-3.315382080078" Y="-24.92675390625" />
                  <Point X="-3.326773925781" Y="-24.899236328125" />
                  <Point X="-3.331933105469" Y="-24.88867578125" />
                  <Point X="-3.343516113281" Y="-24.868271484375" />
                  <Point X="-3.357530761719" Y="-24.849453125" />
                  <Point X="-3.37376171875" Y="-24.832509765625" />
                  <Point X="-3.382401855469" Y="-24.824541015625" />
                  <Point X="-3.410073242188" Y="-24.8020234375" />
                  <Point X="-3.416534912109" Y="-24.797205078125" />
                  <Point X="-3.436708984375" Y="-24.78387109375" />
                  <Point X="-3.464176269531" Y="-24.768390625" />
                  <Point X="-3.474921386719" Y="-24.7631953125" />
                  <Point X="-3.496977294922" Y="-24.754193359375" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.144280761719" Y="-24.57997265625" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.766764160156" Y="-24.281923828125" />
                  <Point X="-4.731331542969" Y="-24.042470703125" />
                  <Point X="-4.645705078125" Y="-23.726484375" />
                  <Point X="-4.633586425781" Y="-23.681763671875" />
                  <Point X="-4.385770996094" Y="-23.714388671875" />
                  <Point X="-3.778066162109" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747057861328" Y="-23.795634765625" />
                  <Point X="-3.736705322266" Y="-23.795296875" />
                  <Point X="-3.715141845703" Y="-23.79341015625" />
                  <Point X="-3.704885986328" Y="-23.7919453125" />
                  <Point X="-3.684603027344" Y="-23.78791015625" />
                  <Point X="-3.674575927734" Y="-23.78533984375" />
                  <Point X="-3.639479248047" Y="-23.774275390625" />
                  <Point X="-3.613149902344" Y="-23.76597265625" />
                  <Point X="-3.603458251953" Y="-23.762326171875" />
                  <Point X="-3.584518798828" Y="-23.753994140625" />
                  <Point X="-3.575270996094" Y="-23.74930859375" />
                  <Point X="-3.556527832031" Y="-23.738486328125" />
                  <Point X="-3.547854003906" Y="-23.732826171875" />
                  <Point X="-3.531174804688" Y="-23.720595703125" />
                  <Point X="-3.515925292969" Y="-23.70662109375" />
                  <Point X="-3.502288330078" Y="-23.6910703125" />
                  <Point X="-3.495895507812" Y="-23.682923828125" />
                  <Point X="-3.483481689453" Y="-23.6651953125" />
                  <Point X="-3.478011962891" Y="-23.656400390625" />
                  <Point X="-3.468062744141" Y="-23.638265625" />
                  <Point X="-3.463583251953" Y="-23.62892578125" />
                  <Point X="-3.449500488281" Y="-23.594927734375" />
                  <Point X="-3.438935791016" Y="-23.569421875" />
                  <Point X="-3.435498779297" Y="-23.5596484375" />
                  <Point X="-3.429710205078" Y="-23.539787109375" />
                  <Point X="-3.427358642578" Y="-23.52969921875" />
                  <Point X="-3.423600341797" Y="-23.5083828125" />
                  <Point X="-3.422360595703" Y="-23.498099609375" />
                  <Point X="-3.421008056641" Y="-23.477458984375" />
                  <Point X="-3.421910888672" Y="-23.45679296875" />
                  <Point X="-3.425058105469" Y="-23.436349609375" />
                  <Point X="-3.427189697266" Y="-23.42621484375" />
                  <Point X="-3.432792236328" Y="-23.40530859375" />
                  <Point X="-3.436013427734" Y="-23.395466796875" />
                  <Point X="-3.443509033203" Y="-23.37619140625" />
                  <Point X="-3.447783447266" Y="-23.3667578125" />
                  <Point X="-3.464775634766" Y="-23.334115234375" />
                  <Point X="-3.477523193359" Y="-23.30962890625" />
                  <Point X="-3.482798828125" Y="-23.300716796875" />
                  <Point X="-3.494290283203" Y="-23.283517578125" />
                  <Point X="-3.500506103516" Y="-23.27523046875" />
                  <Point X="-3.51441796875" Y="-23.258650390625" />
                  <Point X="-3.521498779297" Y="-23.25108984375" />
                  <Point X="-3.53644140625" Y="-23.23678515625" />
                  <Point X="-3.544303222656" Y="-23.230041015625" />
                  <Point X="-3.909110107422" Y="-22.95011328125" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.139971191406" Y="-22.5555625" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.775475097656" Y="-22.028146484375" />
                  <Point X="-3.726337890625" Y="-21.964986328125" />
                  <Point X="-3.627392333984" Y="-22.02211328125" />
                  <Point X="-3.254156738281" Y="-22.2376015625" />
                  <Point X="-3.244921142578" Y="-22.24228125" />
                  <Point X="-3.225991455078" Y="-22.250611328125" />
                  <Point X="-3.216297607422" Y="-22.254259765625" />
                  <Point X="-3.195653808594" Y="-22.26076953125" />
                  <Point X="-3.185623291016" Y="-22.263341796875" />
                  <Point X="-3.165334472656" Y="-22.26737890625" />
                  <Point X="-3.155076171875" Y="-22.26884375" />
                  <Point X="-3.106196044922" Y="-22.27312109375" />
                  <Point X="-3.069526855469" Y="-22.276328125" />
                  <Point X="-3.059173828125" Y="-22.276666015625" />
                  <Point X="-3.038487060547" Y="-22.27621484375" />
                  <Point X="-3.028153320312" Y="-22.27542578125" />
                  <Point X="-3.006695800781" Y="-22.272599609375" />
                  <Point X="-2.996511962891" Y="-22.2706875" />
                  <Point X="-2.976422363281" Y="-22.26576953125" />
                  <Point X="-2.956990722656" Y="-22.2586953125" />
                  <Point X="-2.93844140625" Y="-22.249546875" />
                  <Point X="-2.92941796875" Y="-22.244466796875" />
                  <Point X="-2.911164550781" Y="-22.232837890625" />
                  <Point X="-2.902748535156" Y="-22.22680859375" />
                  <Point X="-2.886617919922" Y="-22.21386328125" />
                  <Point X="-2.878903320312" Y="-22.206947265625" />
                  <Point X="-2.844207763672" Y="-22.172251953125" />
                  <Point X="-2.8181796875" Y="-22.146224609375" />
                  <Point X="-2.811267089844" Y="-22.138513671875" />
                  <Point X="-2.798321533203" Y="-22.122384765625" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.753073974609" Y="-21.906720703125" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-2.949179443359" Y="-21.49096875" />
                  <Point X="-3.059386474609" Y="-21.300083984375" />
                  <Point X="-2.888159179688" Y="-21.168806640625" />
                  <Point X="-2.648370605469" Y="-20.984962890625" />
                  <Point X="-2.2911484375" Y="-20.786498046875" />
                  <Point X="-2.192523925781" Y="-20.731703125" />
                  <Point X="-2.118565673828" Y="-20.828087890625" />
                  <Point X="-2.111818359375" Y="-20.835953125" />
                  <Point X="-2.097514404297" Y="-20.85089453125" />
                  <Point X="-2.089956542969" Y="-20.85797265625" />
                  <Point X="-2.073376708984" Y="-20.871884765625" />
                  <Point X="-2.065096435547" Y="-20.87809765625" />
                  <Point X="-2.047899780273" Y="-20.88958984375" />
                  <Point X="-2.038983398437" Y="-20.894869140625" />
                  <Point X="-1.984580078125" Y="-20.92319140625" />
                  <Point X="-1.943767333984" Y="-20.9444375" />
                  <Point X="-1.934334716797" Y="-20.9487109375" />
                  <Point X="-1.915061157227" Y="-20.95620703125" />
                  <Point X="-1.905220214844" Y="-20.9594296875" />
                  <Point X="-1.884313964844" Y="-20.965033203125" />
                  <Point X="-1.874174926758" Y="-20.967166015625" />
                  <Point X="-1.853724609375" Y="-20.970314453125" />
                  <Point X="-1.8330546875" Y="-20.971216796875" />
                  <Point X="-1.812407836914" Y="-20.96986328125" />
                  <Point X="-1.802119750977" Y="-20.968623046875" />
                  <Point X="-1.78080480957" Y="-20.96486328125" />
                  <Point X="-1.77071484375" Y="-20.962509765625" />
                  <Point X="-1.750860351562" Y="-20.956720703125" />
                  <Point X="-1.741095825195" Y="-20.95328515625" />
                  <Point X="-1.684431152344" Y="-20.9298125" />
                  <Point X="-1.641921875" Y="-20.912205078125" />
                  <Point X="-1.632583862305" Y="-20.9077265625" />
                  <Point X="-1.614452880859" Y="-20.897779296875" />
                  <Point X="-1.605659912109" Y="-20.892310546875" />
                  <Point X="-1.58792980957" Y="-20.879896484375" />
                  <Point X="-1.579781005859" Y="-20.873501953125" />
                  <Point X="-1.564228515625" Y="-20.85986328125" />
                  <Point X="-1.550253295898" Y="-20.84461328125" />
                  <Point X="-1.538021484375" Y="-20.827931640625" />
                  <Point X="-1.532361328125" Y="-20.819255859375" />
                  <Point X="-1.521539306641" Y="-20.80051171875" />
                  <Point X="-1.516856689453" Y="-20.7912734375" />
                  <Point X="-1.508525634766" Y="-20.77233984375" />
                  <Point X="-1.504877319336" Y="-20.762646484375" />
                  <Point X="-1.486433837891" Y="-20.70415234375" />
                  <Point X="-1.472598022461" Y="-20.66026953125" />
                  <Point X="-1.470026611328" Y="-20.650236328125" />
                  <Point X="-1.465990966797" Y="-20.62994921875" />
                  <Point X="-1.464526977539" Y="-20.619693359375" />
                  <Point X="-1.462640625" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752807617" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.43734375" />
                  <Point X="-1.241592163086" Y="-20.370708984375" />
                  <Point X="-0.931165893555" Y="-20.2836796875" />
                  <Point X="-0.49811920166" Y="-20.23299609375" />
                  <Point X="-0.365222564697" Y="-20.21744140625" />
                  <Point X="-0.336079650879" Y="-20.326205078125" />
                  <Point X="-0.225666366577" Y="-20.7382734375" />
                  <Point X="-0.220435150146" Y="-20.752892578125" />
                  <Point X="-0.207661453247" Y="-20.781083984375" />
                  <Point X="-0.200119140625" Y="-20.79465625" />
                  <Point X="-0.182260910034" Y="-20.8213828125" />
                  <Point X="-0.172608886719" Y="-20.833544921875" />
                  <Point X="-0.151451385498" Y="-20.856134765625" />
                  <Point X="-0.126896499634" Y="-20.8749765625" />
                  <Point X="-0.099600570679" Y="-20.88956640625" />
                  <Point X="-0.085354026794" Y="-20.8957421875" />
                  <Point X="-0.054916057587" Y="-20.90607421875" />
                  <Point X="-0.039853721619" Y="-20.909845703125" />
                  <Point X="-0.009317661285" Y="-20.91488671875" />
                  <Point X="0.021629489899" Y="-20.91488671875" />
                  <Point X="0.052165550232" Y="-20.909845703125" />
                  <Point X="0.067227882385" Y="-20.90607421875" />
                  <Point X="0.097665855408" Y="-20.8957421875" />
                  <Point X="0.111912246704" Y="-20.88956640625" />
                  <Point X="0.139208175659" Y="-20.8749765625" />
                  <Point X="0.163763214111" Y="-20.856134765625" />
                  <Point X="0.184920715332" Y="-20.833544921875" />
                  <Point X="0.194572891235" Y="-20.8213828125" />
                  <Point X="0.212431121826" Y="-20.79465625" />
                  <Point X="0.219973724365" Y="-20.781083984375" />
                  <Point X="0.232747116089" Y="-20.752892578125" />
                  <Point X="0.23797819519" Y="-20.7382734375" />
                  <Point X="0.320809295654" Y="-20.429142578125" />
                  <Point X="0.378190612793" Y="-20.2149921875" />
                  <Point X="0.556826171875" Y="-20.233701171875" />
                  <Point X="0.827852661133" Y="-20.262083984375" />
                  <Point X="1.186165161133" Y="-20.348591796875" />
                  <Point X="1.453622314453" Y="-20.413166015625" />
                  <Point X="1.685202392578" Y="-20.49716015625" />
                  <Point X="1.858247558594" Y="-20.55992578125" />
                  <Point X="2.083765625" Y="-20.665392578125" />
                  <Point X="2.250427978516" Y="-20.7433359375" />
                  <Point X="2.468355224609" Y="-20.87030078125" />
                  <Point X="2.629450927734" Y="-20.964154296875" />
                  <Point X="2.817780517578" Y="-21.098083984375" />
                  <Point X="2.557613525391" Y="-21.548705078125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.0531796875" Y="-22.42656640625" />
                  <Point X="2.044181640625" Y="-22.450439453125" />
                  <Point X="2.041301635742" Y="-22.459404296875" />
                  <Point X="2.029034423828" Y="-22.50527734375" />
                  <Point X="2.01983190918" Y="-22.53969140625" />
                  <Point X="2.017912719727" Y="-22.548537109375" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.018194580078" Y="-22.670087890625" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029143920898" Y="-22.732890625" />
                  <Point X="2.03246887207" Y="-22.743693359375" />
                  <Point X="2.040735717773" Y="-22.765783203125" />
                  <Point X="2.045319091797" Y="-22.77611328125" />
                  <Point X="2.055680664062" Y="-22.79615625" />
                  <Point X="2.061458740234" Y="-22.805869140625" />
                  <Point X="2.086003417969" Y="-22.84204296875" />
                  <Point X="2.104416503906" Y="-22.869177734375" />
                  <Point X="2.109807373047" Y="-22.876365234375" />
                  <Point X="2.130470703125" Y="-22.89988671875" />
                  <Point X="2.157598632812" Y="-22.924615234375" />
                  <Point X="2.168255371094" Y="-22.933017578125" />
                  <Point X="2.204427734375" Y="-22.9575625" />
                  <Point X="2.231563964844" Y="-22.9759765625" />
                  <Point X="2.241278320312" Y="-22.981755859375" />
                  <Point X="2.261320556641" Y="-22.9921171875" />
                  <Point X="2.2716484375" Y="-22.99669921875" />
                  <Point X="2.293738525391" Y="-23.004966796875" />
                  <Point X="2.304543945312" Y="-23.00829296875" />
                  <Point X="2.326470458984" Y="-23.01363671875" />
                  <Point X="2.337591552734" Y="-23.015654296875" />
                  <Point X="2.377258544922" Y="-23.0204375" />
                  <Point X="2.407016357422" Y="-23.024025390625" />
                  <Point X="2.416040039062" Y="-23.0246796875" />
                  <Point X="2.447579589844" Y="-23.02450390625" />
                  <Point X="2.484318115234" Y="-23.020177734375" />
                  <Point X="2.497750976562" Y="-23.01760546875" />
                  <Point X="2.543623779297" Y="-23.005337890625" />
                  <Point X="2.578037353516" Y="-22.996134765625" />
                  <Point X="2.584005371094" Y="-22.994326171875" />
                  <Point X="2.604415039063" Y="-22.986927734375" />
                  <Point X="2.627659912109" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.275719970703" Y="-22.602802734375" />
                  <Point X="3.940403564453" Y="-22.219046875" />
                  <Point X="3.948405029297" Y="-22.23016796875" />
                  <Point X="4.043952880859" Y="-22.36295703125" />
                  <Point X="4.136884277344" Y="-22.516529296875" />
                  <Point X="3.8211171875" Y="-22.75882421875" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168133056641" Y="-23.260134765625" />
                  <Point X="3.152116943359" Y="-23.274787109375" />
                  <Point X="3.134668457031" Y="-23.2933984375" />
                  <Point X="3.128576416016" Y="-23.300578125" />
                  <Point X="3.095561523438" Y="-23.3436484375" />
                  <Point X="3.070794189453" Y="-23.375958984375" />
                  <Point X="3.065634765625" Y="-23.3833984375" />
                  <Point X="3.049740234375" Y="-23.410626953125" />
                  <Point X="3.034763427734" Y="-23.444453125" />
                  <Point X="3.030140380859" Y="-23.457328125" />
                  <Point X="3.017842285156" Y="-23.501302734375" />
                  <Point X="3.008616455078" Y="-23.53429296875" />
                  <Point X="3.006225341797" Y="-23.545337890625" />
                  <Point X="3.002771728516" Y="-23.56763671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.014794921875" Y="-23.696357421875" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034684814453" Y="-23.77139453125" />
                  <Point X="3.050287841797" Y="-23.804630859375" />
                  <Point X="3.056919677734" Y="-23.816474609375" />
                  <Point X="3.084378173828" Y="-23.858208984375" />
                  <Point X="3.104977294922" Y="-23.88951953125" />
                  <Point X="3.111737548828" Y="-23.898572265625" />
                  <Point X="3.126287841797" Y="-23.915814453125" />
                  <Point X="3.134077880859" Y="-23.92400390625" />
                  <Point X="3.151320556641" Y="-23.94009375" />
                  <Point X="3.160030761719" Y="-23.94730078125" />
                  <Point X="3.178243652344" Y="-23.96062890625" />
                  <Point X="3.187746337891" Y="-23.96675" />
                  <Point X="3.227537353516" Y="-23.9891484375" />
                  <Point X="3.257388427734" Y="-24.005953125" />
                  <Point X="3.265474853516" Y="-24.01001171875" />
                  <Point X="3.294680664062" Y="-24.02191796875" />
                  <Point X="3.330279541016" Y="-24.03198046875" />
                  <Point X="3.343674560547" Y="-24.034744140625" />
                  <Point X="3.397474853516" Y="-24.041853515625" />
                  <Point X="3.437835205078" Y="-24.0471875" />
                  <Point X="3.444033447266" Y="-24.04780078125" />
                  <Point X="3.465708251953" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.108262695312" Y="-23.967203125" />
                  <Point X="4.704703613281" Y="-23.8886796875" />
                  <Point X="4.711646972656" Y="-23.917201171875" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="4.434803710938" Y="-24.379609375" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686020263672" Y="-24.580458984375" />
                  <Point X="3.665627441406" Y="-24.58786328125" />
                  <Point X="3.642386230469" Y="-24.59837890625" />
                  <Point X="3.634008056641" Y="-24.602681640625" />
                  <Point X="3.581151123047" Y="-24.633232421875" />
                  <Point X="3.541498046875" Y="-24.65615234375" />
                  <Point X="3.533881103516" Y="-24.6610546875" />
                  <Point X="3.508776611328" Y="-24.680126953125" />
                  <Point X="3.481993896484" Y="-24.7056484375" />
                  <Point X="3.472795654297" Y="-24.715775390625" />
                  <Point X="3.441081542969" Y="-24.7561875" />
                  <Point X="3.417289794922" Y="-24.78650390625" />
                  <Point X="3.410854736328" Y="-24.795791015625" />
                  <Point X="3.399130615234" Y="-24.8150703125" />
                  <Point X="3.393841552734" Y="-24.8250625" />
                  <Point X="3.384067626953" Y="-24.84652734375" />
                  <Point X="3.38000390625" Y="-24.85707421875" />
                  <Point X="3.373158447266" Y="-24.87857421875" />
                  <Point X="3.370376708984" Y="-24.88952734375" />
                  <Point X="3.359805175781" Y="-24.9447265625" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.362446289062" Y="-25.131623046875" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159179688" Y="-25.183986328125" />
                  <Point X="3.380004394531" Y="-25.205484375" />
                  <Point X="3.384067382812" Y="-25.216029296875" />
                  <Point X="3.393841064453" Y="-25.237494140625" />
                  <Point X="3.399128662109" Y="-25.247484375" />
                  <Point X="3.410853027344" Y="-25.266765625" />
                  <Point X="3.417289794922" Y="-25.276056640625" />
                  <Point X="3.44900390625" Y="-25.31646875" />
                  <Point X="3.472795654297" Y="-25.34678515625" />
                  <Point X="3.478718261719" Y="-25.353634765625" />
                  <Point X="3.501141357422" Y="-25.375806640625" />
                  <Point X="3.530176269531" Y="-25.398724609375" />
                  <Point X="3.541494140625" Y="-25.406404296875" />
                  <Point X="3.594351318359" Y="-25.43695703125" />
                  <Point X="3.634004150391" Y="-25.459876953125" />
                  <Point X="3.639497558594" Y="-25.46281640625" />
                  <Point X="3.659150146484" Y="-25.472013671875" />
                  <Point X="3.683021972656" Y="-25.48102734375" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.249242675781" Y="-25.63323046875" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.784526855469" Y="-25.77907421875" />
                  <Point X="4.76161328125" Y="-25.9310546875" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="4.298234863281" Y="-26.022666015625" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720458984" Y="-25.910841796875" />
                  <Point X="3.354480957031" Y="-25.912677734375" />
                  <Point X="3.250741210938" Y="-25.9352265625" />
                  <Point X="3.172916748047" Y="-25.952140625" />
                  <Point X="3.157877197266" Y="-25.9567421875" />
                  <Point X="3.1287578125" Y="-25.968365234375" />
                  <Point X="3.114677978516" Y="-25.97538671875" />
                  <Point X="3.086850585938" Y="-25.992279296875" />
                  <Point X="3.074122314453" Y="-26.00153125" />
                  <Point X="3.050371582031" Y="-26.02200390625" />
                  <Point X="3.039349121094" Y="-26.033224609375" />
                  <Point X="2.976645019531" Y="-26.108638671875" />
                  <Point X="2.929604980469" Y="-26.165212890625" />
                  <Point X="2.921326416016" Y="-26.17684765625" />
                  <Point X="2.906606201172" Y="-26.201228515625" />
                  <Point X="2.900164550781" Y="-26.213974609375" />
                  <Point X="2.888821289062" Y="-26.241359375" />
                  <Point X="2.88436328125" Y="-26.254927734375" />
                  <Point X="2.87753125" Y="-26.282578125" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.866170166016" Y="-26.39432421875" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.953951904297" Y="-26.708671875" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486328125" Y="-26.828119140625" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.569927734375" Y="-27.23308984375" />
                  <Point X="4.087169677734" Y="-27.629984375" />
                  <Point X="4.045491699219" Y="-27.69742578125" />
                  <Point X="4.001273681641" Y="-27.76025390625" />
                  <Point X="3.615694335938" Y="-27.537638671875" />
                  <Point X="2.848454589844" Y="-27.094671875" />
                  <Point X="2.841190917969" Y="-27.090884765625" />
                  <Point X="2.815037597656" Y="-27.079517578125" />
                  <Point X="2.783124511719" Y="-27.069326171875" />
                  <Point X="2.771108642578" Y="-27.0663359375" />
                  <Point X="2.647641845703" Y="-27.044037109375" />
                  <Point X="2.555018310547" Y="-27.027310546875" />
                  <Point X="2.539352783203" Y="-27.025806640625" />
                  <Point X="2.50799609375" Y="-27.025404296875" />
                  <Point X="2.492304931641" Y="-27.026505859375" />
                  <Point X="2.460133300781" Y="-27.03146484375" />
                  <Point X="2.444841796875" Y="-27.035138671875" />
                  <Point X="2.415069580078" Y="-27.0449609375" />
                  <Point X="2.400588867188" Y="-27.051109375" />
                  <Point X="2.298018310547" Y="-27.105091796875" />
                  <Point X="2.221070800781" Y="-27.145587890625" />
                  <Point X="2.208970214844" Y="-27.153169921875" />
                  <Point X="2.186041748047" Y="-27.170060546875" />
                  <Point X="2.175213867187" Y="-27.179369140625" />
                  <Point X="2.154253662109" Y="-27.200328125" />
                  <Point X="2.144940673828" Y="-27.21116015625" />
                  <Point X="2.128045898438" Y="-27.23409375" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.066481933594" Y="-27.348765625" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.024485717773" Y="-27.703607421875" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.401856201172" Y="-28.44915625" />
                  <Point X="2.735893554688" Y="-29.027724609375" />
                  <Point X="2.723753173828" Y="-29.036083984375" />
                  <Point X="2.419389404297" Y="-28.6394296875" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828658569336" Y="-27.87015234375" />
                  <Point X="1.808834594727" Y="-27.84962890625" />
                  <Point X="1.783251464844" Y="-27.82800390625" />
                  <Point X="1.773298217773" Y="-27.820646484375" />
                  <Point X="1.651526733398" Y="-27.742359375" />
                  <Point X="1.560174926758" Y="-27.68362890625" />
                  <Point X="1.546284545898" Y="-27.67624609375" />
                  <Point X="1.517470581055" Y="-27.663873046875" />
                  <Point X="1.50254699707" Y="-27.6588828125" />
                  <Point X="1.470926269531" Y="-27.65115234375" />
                  <Point X="1.455384887695" Y="-27.6486953125" />
                  <Point X="1.424119873047" Y="-27.64637890625" />
                  <Point X="1.408396240234" Y="-27.64651953125" />
                  <Point X="1.275218261719" Y="-27.6587734375" />
                  <Point X="1.175309448242" Y="-27.667966796875" />
                  <Point X="1.1612265625" Y="-27.67033984375" />
                  <Point X="1.133575317383" Y="-27.677171875" />
                  <Point X="1.120006713867" Y="-27.681630859375" />
                  <Point X="1.092621337891" Y="-27.692974609375" />
                  <Point X="1.079875488281" Y="-27.699416015625" />
                  <Point X="1.055493530273" Y="-27.71413671875" />
                  <Point X="1.043857910156" Y="-27.722416015625" />
                  <Point X="0.94102142334" Y="-27.807921875" />
                  <Point X="0.863874084473" Y="-27.872068359375" />
                  <Point X="0.852652160645" Y="-27.883091796875" />
                  <Point X="0.83218145752" Y="-27.906841796875" />
                  <Point X="0.822932800293" Y="-27.919568359375" />
                  <Point X="0.806040771484" Y="-27.94739453125" />
                  <Point X="0.799019348145" Y="-27.961470703125" />
                  <Point X="0.787394836426" Y="-27.99058984375" />
                  <Point X="0.782791809082" Y="-28.0056328125" />
                  <Point X="0.752044128418" Y="-28.147095703125" />
                  <Point X="0.728977539063" Y="-28.25321875" />
                  <Point X="0.727584655762" Y="-28.2612890625" />
                  <Point X="0.72472454834" Y="-28.289677734375" />
                  <Point X="0.724742248535" Y="-28.323169921875" />
                  <Point X="0.725555053711" Y="-28.33551953125" />
                  <Point X="0.819728759766" Y="-29.050841796875" />
                  <Point X="0.833091308594" Y="-29.15233984375" />
                  <Point X="0.655064880371" Y="-28.487935546875" />
                  <Point X="0.652606079102" Y="-28.480123046875" />
                  <Point X="0.642146728516" Y="-28.453583984375" />
                  <Point X="0.626788146973" Y="-28.42381640625" />
                  <Point X="0.620407470703" Y="-28.41320703125" />
                  <Point X="0.526859069824" Y="-28.278421875" />
                  <Point X="0.456679748535" Y="-28.177306640625" />
                  <Point X="0.446668518066" Y="-28.16516796875" />
                  <Point X="0.424785430908" Y="-28.14271484375" />
                  <Point X="0.412913726807" Y="-28.132400390625" />
                  <Point X="0.386658599854" Y="-28.11315625" />
                  <Point X="0.373244659424" Y="-28.1049375" />
                  <Point X="0.34524230957" Y="-28.090828125" />
                  <Point X="0.330653930664" Y="-28.0849375" />
                  <Point X="0.185893157959" Y="-28.040009765625" />
                  <Point X="0.07729486084" Y="-28.0063046875" />
                  <Point X="0.063377067566" Y="-28.003109375" />
                  <Point X="0.035218082428" Y="-27.99883984375" />
                  <Point X="0.020976737976" Y="-27.997765625" />
                  <Point X="-0.008664166451" Y="-27.997765625" />
                  <Point X="-0.022904914856" Y="-27.99883984375" />
                  <Point X="-0.051065090179" Y="-28.003109375" />
                  <Point X="-0.064984512329" Y="-28.0063046875" />
                  <Point X="-0.209745147705" Y="-28.051234375" />
                  <Point X="-0.318343444824" Y="-28.0849375" />
                  <Point X="-0.332928405762" Y="-28.090828125" />
                  <Point X="-0.360928649902" Y="-28.104935546875" />
                  <Point X="-0.374343963623" Y="-28.11315234375" />
                  <Point X="-0.400599822998" Y="-28.132396484375" />
                  <Point X="-0.412476593018" Y="-28.142716796875" />
                  <Point X="-0.434360870361" Y="-28.165173828125" />
                  <Point X="-0.444368377686" Y="-28.177310546875" />
                  <Point X="-0.537916687012" Y="-28.31209765625" />
                  <Point X="-0.608095947266" Y="-28.4132109375" />
                  <Point X="-0.612469482422" Y="-28.420130859375" />
                  <Point X="-0.625976318359" Y="-28.445263671875" />
                  <Point X="-0.638777832031" Y="-28.47621484375" />
                  <Point X="-0.642753173828" Y="-28.487935546875" />
                  <Point X="-0.814455749512" Y="-29.128740234375" />
                  <Point X="-0.985424804688" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.918383517886" Y="-27.712397055417" />
                  <Point X="4.019572315123" Y="-27.578114986038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.835493354132" Y="-27.664540204584" />
                  <Point X="3.944200935546" Y="-27.520280371597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.703196136133" Y="-29.009293550858" />
                  <Point X="2.715684288567" Y="-28.992721212838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.752603190377" Y="-27.616683353751" />
                  <Point X="3.868829555969" Y="-27.462445757157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.643181058233" Y="-28.931080435798" />
                  <Point X="2.664082104561" Y="-28.903343810501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.669713026623" Y="-27.568826502918" />
                  <Point X="3.793458176392" Y="-27.404611142716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.583165980333" Y="-28.852867320738" />
                  <Point X="2.612479920555" Y="-28.813966408164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.58682284195" Y="-27.520969679845" />
                  <Point X="3.718086796815" Y="-27.346776528275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.678182642897" Y="-26.072686307474" />
                  <Point X="4.751493920228" Y="-25.975398956525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.523150902432" Y="-28.774654205678" />
                  <Point X="2.560877736549" Y="-28.724589005828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.503932618138" Y="-27.47311290871" />
                  <Point X="3.642715417239" Y="-27.288941913834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.569965513722" Y="-26.058439474949" />
                  <Point X="4.782676571053" Y="-25.776162367816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.463135824532" Y="-28.696441090618" />
                  <Point X="2.509275552542" Y="-28.635211603491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.421042394326" Y="-27.425256137576" />
                  <Point X="3.567344038418" Y="-27.23110729839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.461748384547" Y="-26.044192642424" />
                  <Point X="4.683706888446" Y="-25.749643759211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.403120757739" Y="-28.618227960818" />
                  <Point X="2.457673368536" Y="-28.545834201154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.338152170515" Y="-27.377399366442" />
                  <Point X="3.491972680891" Y="-27.173272654687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.353531255373" Y="-26.029945809899" />
                  <Point X="4.584737205838" Y="-25.723125150607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.343105720815" Y="-28.540014791381" />
                  <Point X="2.40607118453" Y="-28.456456798817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.255261946703" Y="-27.329542595307" />
                  <Point X="3.416601323365" Y="-27.115438010984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.245314187674" Y="-26.015698895793" />
                  <Point X="4.48576752323" Y="-25.696606542002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.283090683891" Y="-28.461801621944" />
                  <Point X="2.354469019823" Y="-28.367079370869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.172371722892" Y="-27.281685824173" />
                  <Point X="3.341229965839" Y="-27.057603367281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.13709718421" Y="-26.001451896444" />
                  <Point X="4.386797840622" Y="-25.670087933398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.223075646966" Y="-28.383588452507" />
                  <Point X="2.302866856833" Y="-28.277701940642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.08948149908" Y="-27.233829053038" />
                  <Point X="3.265858608313" Y="-26.999768723578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.028880180747" Y="-25.987204897095" />
                  <Point X="4.287828158014" Y="-25.643569324793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.163060610042" Y="-28.305375283071" />
                  <Point X="2.251264693843" Y="-28.188324510416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.006591275268" Y="-27.185972281904" />
                  <Point X="3.190487250787" Y="-26.941934079875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.920663177284" Y="-25.972957897746" />
                  <Point X="4.188858546053" Y="-25.617050622438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103045573117" Y="-28.227162113634" />
                  <Point X="2.199662530853" Y="-28.098947080189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.923701051457" Y="-27.138115510769" />
                  <Point X="3.11511589326" Y="-26.884099436172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.81244617382" Y="-25.958710898396" />
                  <Point X="4.089888979235" Y="-25.590531860174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043030536193" Y="-28.148948944197" />
                  <Point X="2.148060367864" Y="-28.009569649963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.840549245224" Y="-27.090605871232" />
                  <Point X="3.040554380224" Y="-26.825190092533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.704229170357" Y="-25.944463899047" />
                  <Point X="3.990919412417" Y="-25.564013097911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.983015499268" Y="-28.070735774761" />
                  <Point X="2.096458204874" Y="-27.920192219736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.743625427942" Y="-27.061372307642" />
                  <Point X="2.979481424764" Y="-26.74838082841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.596012166893" Y="-25.930216899698" />
                  <Point X="3.8919498456" Y="-25.537494335648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.923000462344" Y="-27.992522605324" />
                  <Point X="2.04925852579" Y="-27.824972496039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.638922141523" Y="-27.042462448284" />
                  <Point X="2.924717259092" Y="-26.66319951747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48779516343" Y="-25.915969900349" />
                  <Point X="3.792980278782" Y="-25.510975573385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.698838793437" Y="-24.308860722392" />
                  <Point X="4.772250598735" Y="-24.211439966325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.86298542542" Y="-27.914309435887" />
                  <Point X="2.023958910642" Y="-27.700690405905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.532585735883" Y="-27.025719811331" />
                  <Point X="2.874998220783" Y="-26.571323096387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.373110355462" Y="-25.910305967474" />
                  <Point X="3.694010711964" Y="-25.484456811122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.549790695494" Y="-24.348798415533" />
                  <Point X="4.75154032416" Y="-24.081067615548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.827325347892" Y="-29.130820965322" />
                  <Point X="0.829822075377" Y="-29.127507696042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.799022678559" Y="-27.841335054478" />
                  <Point X="2.001470812353" Y="-27.572677306881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390498798131" Y="-27.056419732888" />
                  <Point X="2.862935674187" Y="-26.429474822975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.232369661883" Y="-25.939219362674" />
                  <Point X="3.6058451577" Y="-25.443600639947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.400742622492" Y="-24.388736075575" />
                  <Point X="4.722494186823" Y="-23.961757328282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.796122991578" Y="-29.014372077285" />
                  <Point X="0.812130754347" Y="-28.993129058596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.720997445529" Y="-27.78702222252" />
                  <Point X="2.101762310856" Y="-27.281730179732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.185671671823" Y="-27.170378696776" />
                  <Point X="2.892585040626" Y="-26.232272971372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052262183902" Y="-26.020374245256" />
                  <Point X="3.524239793206" Y="-25.394038802909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.251694633691" Y="-24.42867362388" />
                  <Point X="4.653532691497" Y="-23.895416510138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764920635264" Y="-28.897923189247" />
                  <Point X="0.794439506551" Y="-28.858750323965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.640865434112" Y="-27.73550517991" />
                  <Point X="3.456521587748" Y="-25.326048083385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.10264664489" Y="-24.468611172185" />
                  <Point X="4.521479037519" Y="-23.91280181442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733718278949" Y="-28.78147430121" />
                  <Point X="0.776748258755" Y="-28.724371589334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.560733509811" Y="-27.683988021694" />
                  <Point X="3.398155802784" Y="-25.245646282674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953598656089" Y="-24.50854872049" />
                  <Point X="4.389425383541" Y="-23.930187118701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.702515922635" Y="-28.665025413173" />
                  <Point X="0.759057010959" Y="-28.589992854703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.466992701764" Y="-27.65053046218" />
                  <Point X="3.362989239364" Y="-25.134458075148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.804550667287" Y="-24.548486268795" />
                  <Point X="4.257371729562" Y="-23.947572422983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.671313566321" Y="-28.548576525135" />
                  <Point X="0.741365763163" Y="-28.455614120072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.346790815813" Y="-27.652187939074" />
                  <Point X="3.350955220574" Y="-24.99257194406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.650905691228" Y="-24.594524225236" />
                  <Point X="4.125318075584" Y="-23.964957727264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.634748270685" Y="-28.439244497953" />
                  <Point X="0.724740478569" Y="-28.319820804494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.218975518413" Y="-27.663948754206" />
                  <Point X="3.993264478456" Y="-23.982342956103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.579658368266" Y="-28.354495454275" />
                  <Point X="0.758569998082" Y="-28.1170717024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.067804134514" Y="-27.70670414298" />
                  <Point X="3.861210889759" Y="-23.999728173753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.522626482039" Y="-28.272323510153" />
                  <Point X="3.729157301062" Y="-24.017113391402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.465594705339" Y="-28.190151420684" />
                  <Point X="3.597103712365" Y="-24.034498609052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.39825615842" Y="-28.121656877262" />
                  <Point X="3.467432211185" Y="-24.048722689798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.311461805115" Y="-28.078981060955" />
                  <Point X="3.357623877942" Y="-24.036587456393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.067015769011" Y="-29.750426774011" />
                  <Point X="-0.933594161659" Y="-29.573370320883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.215055615761" Y="-28.049060581902" />
                  <Point X="3.260557165541" Y="-24.00754352103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.134825170618" Y="-29.682557075865" />
                  <Point X="-0.867957692882" Y="-29.328411971476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.118649681646" Y="-28.019139764136" />
                  <Point X="3.177414313345" Y="-23.960021999084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121985044624" Y="-29.507661839749" />
                  <Point X="-0.802321303496" Y="-29.083453727425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.015803363235" Y="-27.997765625" />
                  <Point X="3.108268108834" Y="-23.893926298309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.146325612337" Y="-29.382107050681" />
                  <Point X="-0.736685264156" Y="-28.8384959479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.123199661438" Y="-28.024373055675" />
                  <Point X="3.052980487421" Y="-23.809439636599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.876972633283" Y="-22.715965126376" />
                  <Point X="4.088059059223" Y="-22.435843977918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.171167534384" Y="-29.257217581286" />
                  <Point X="-0.671049224816" Y="-28.593538168376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.278465048962" Y="-28.072561370758" />
                  <Point X="3.01575066711" Y="-23.700989463445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.594945327474" Y="-22.932372188699" />
                  <Point X="4.034221617866" Y="-22.349432862274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.202853810626" Y="-29.141410876683" />
                  <Point X="3.004190826521" Y="-23.558474076631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.312917400892" Y="-23.148780074815" />
                  <Point X="3.976118245821" Y="-22.268682827858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.260604610958" Y="-29.060192963801" />
                  <Point X="3.859270586294" Y="-22.265889095945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.332197892996" Y="-28.997344644586" />
                  <Point X="3.648710158221" Y="-22.38745640825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.403819676028" Y="-28.934534147467" />
                  <Point X="3.438149730149" Y="-22.509023720555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475441274366" Y="-28.871723405251" />
                  <Point X="3.227589424892" Y="-22.630590869879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.554826473867" Y="-28.819215309755" />
                  <Point X="3.017029534106" Y="-22.752157469181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.655490967655" Y="-28.794945791551" />
                  <Point X="2.80646964332" Y="-22.873724068482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.768845301482" Y="-28.787516259857" />
                  <Point X="2.601383606" Y="-22.988026618888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.88219973431" Y="-28.780086859543" />
                  <Point X="2.45566014104" Y="-23.023552375045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.001687543133" Y="-28.780796724082" />
                  <Point X="2.342236786057" Y="-23.016214437519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.199342624915" Y="-28.885238063421" />
                  <Point X="2.247015338854" Y="-22.984721752531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.519572585447" Y="-29.152341760866" />
                  <Point X="2.167483150415" Y="-22.932408717944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.600681714099" Y="-29.102121396624" />
                  <Point X="2.100513010058" Y="-22.863425282502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.68179047462" Y="-29.051900543855" />
                  <Point X="2.046147260877" Y="-22.77771525502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761798273485" Y="-29.00021866562" />
                  <Point X="2.016829244632" Y="-22.65876576325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.837074911606" Y="-28.942258325021" />
                  <Point X="2.037558663951" Y="-22.473401081281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.912351549726" Y="-28.884297984421" />
                  <Point X="2.368223642777" Y="-21.876738020032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945829660624" Y="-28.770869124719" />
                  <Point X="2.757987793979" Y="-21.201647708119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.556068096593" Y="-28.095782246098" />
                  <Point X="2.752212679844" Y="-21.05145573002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.31144482041" Y="-27.613300380786" />
                  <Point X="2.674763663695" Y="-20.996378232434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.339594374349" Y="-27.492800287165" />
                  <Point X="2.595151342513" Y="-20.944171537589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.399721593398" Y="-27.414735988434" />
                  <Point X="2.512488722131" Y="-20.896012726503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.481072950638" Y="-27.364837072386" />
                  <Point X="2.429826246905" Y="-20.847853722786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.588751626464" Y="-27.349875688132" />
                  <Point X="2.347163937952" Y="-20.799694498419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.762644825291" Y="-27.422783943745" />
                  <Point X="2.264501628999" Y="-20.751535274052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.973204892694" Y="-27.544350777425" />
                  <Point X="2.177447053964" Y="-20.709204783644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.183764960096" Y="-27.665917611105" />
                  <Point X="2.08949123346" Y="-20.668070286368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.394325323855" Y="-27.787484838063" />
                  <Point X="2.00153505808" Y="-20.626936260028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604885793944" Y="-27.909052206126" />
                  <Point X="1.913578857991" Y="-20.58580226648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.799435067368" Y="-28.009371998567" />
                  <Point X="1.823596300067" Y="-20.547357340602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.859209743032" Y="-27.930839858964" />
                  <Point X="1.730177072309" Y="-20.513473029632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.91898455114" Y="-27.85230789512" />
                  <Point X="1.636757495187" Y="-20.479589182283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.978759369337" Y="-27.773775944665" />
                  <Point X="-3.11084299771" Y="-26.622012018098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.954272377875" Y="-26.414235787828" />
                  <Point X="1.543337593724" Y="-20.445705765349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.03436253086" Y="-27.689708018824" />
                  <Point X="-3.392870318912" Y="-26.838419100847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963394414758" Y="-26.268485326229" />
                  <Point X="1.449631280357" Y="-20.412202429849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.086913000447" Y="-27.601589033955" />
                  <Point X="-3.674897640113" Y="-27.054826183597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.019154507795" Y="-26.184625655541" />
                  <Point X="1.348988895628" Y="-20.387903571933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.139463470033" Y="-27.513470049087" />
                  <Point X="-3.956924610415" Y="-27.271232800687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.096155238335" Y="-26.128953262858" />
                  <Point X="1.248346510898" Y="-20.363604714017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.193988390193" Y="-26.100926427007" />
                  <Point X="1.147703968426" Y="-20.339306065433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.319887671989" Y="-26.110144603553" />
                  <Point X="1.047061170925" Y="-20.315007755283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.451941033491" Y="-26.127529519706" />
                  <Point X="0.946418373425" Y="-20.290709445133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.583994394993" Y="-26.144914435858" />
                  <Point X="0.845775575924" Y="-20.266411134983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716047756495" Y="-26.16229935201" />
                  <Point X="0.737234535544" Y="-20.252594147146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.848101117997" Y="-26.179684268163" />
                  <Point X="0.163094509532" Y="-20.856647882144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.219798797112" Y="-20.781398750947" />
                  <Point X="0.626982183088" Y="-20.241048147137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.980154520969" Y="-26.197069239347" />
                  <Point X="0.000255510809" Y="-20.91488671875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.295713889395" Y="-20.522800207443" />
                  <Point X="0.516730091373" Y="-20.229501801114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11220802613" Y="-26.214454346141" />
                  <Point X="-3.357364510044" Y="-25.212743166986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.349392565946" Y="-25.202164039852" />
                  <Point X="-0.099612391499" Y="-20.889560087948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.361349997514" Y="-20.277842336646" />
                  <Point X="0.406478455871" Y="-20.217954849676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.244261531291" Y="-26.231839452936" />
                  <Point X="-3.562111530016" Y="-25.326595826176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300441695272" Y="-24.979348227004" />
                  <Point X="-0.174530351254" Y="-20.83112376508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.376315036452" Y="-26.24922455973" />
                  <Point X="-3.711159232098" Y="-25.366532993991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.340154539236" Y="-24.874193137532" />
                  <Point X="-0.224973851609" Y="-20.740208737604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.508368541614" Y="-26.266609666525" />
                  <Point X="-3.86020693418" Y="-25.406470161806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.4067571183" Y="-24.804721931778" />
                  <Point X="-0.256304532544" Y="-20.62393014209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.640422046775" Y="-26.283994773319" />
                  <Point X="-4.009254636261" Y="-25.446407329621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.489831681788" Y="-24.757109787657" />
                  <Point X="-0.287506889586" Y="-20.507481255018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.686359408376" Y="-26.187099897744" />
                  <Point X="-4.158302338343" Y="-25.486344497436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.587686645152" Y="-24.729111896653" />
                  <Point X="-0.318709246628" Y="-20.391032367946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.716473045756" Y="-26.069206230882" />
                  <Point X="-4.307350040424" Y="-25.526281665252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.686656145552" Y="-24.702593046249" />
                  <Point X="-0.349911534387" Y="-20.274583388933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744432723361" Y="-25.948454162856" />
                  <Point X="-4.456398106818" Y="-25.566219316525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.785625645951" Y="-24.676074195846" />
                  <Point X="-0.431664977433" Y="-20.225218058771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763408115928" Y="-25.815779545893" />
                  <Point X="-4.605446350081" Y="-25.606157202512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.88459514635" Y="-24.649555345443" />
                  <Point X="-0.562123976374" Y="-20.240487184342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782383508494" Y="-25.68310492893" />
                  <Point X="-4.754494593344" Y="-25.6460950885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.98356464675" Y="-24.623036495039" />
                  <Point X="-0.692582758872" Y="-20.255756022685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.082534147149" Y="-24.596517644636" />
                  <Point X="-0.82304154137" Y="-20.271024861027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.181503690051" Y="-24.569998850635" />
                  <Point X="-3.553352767354" Y="-23.736414421475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.45287644835" Y="-23.603077842644" />
                  <Point X="-0.956985153516" Y="-20.290918224508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.280473303456" Y="-24.543480150196" />
                  <Point X="-3.715263010409" Y="-23.793420757682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.430170808486" Y="-23.415090627435" />
                  <Point X="-1.107799352594" Y="-20.333199613014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.379442916861" Y="-24.516961449756" />
                  <Point X="-3.829815753145" Y="-23.787581568325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.47430738742" Y="-23.315806032547" />
                  <Point X="-1.672657447626" Y="-20.92493580957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.472278948618" Y="-20.659024560097" />
                  <Point X="-1.258613680601" Y="-20.375481172616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.478412530267" Y="-24.490442749317" />
                  <Point X="-3.93803272955" Y="-23.773334533067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.534856847277" Y="-23.238302066295" />
                  <Point X="-1.826144184133" Y="-20.970763775033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.471074007933" Y="-20.499569736394" />
                  <Point X="-1.409429022032" Y="-20.417764077076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.577382143672" Y="-24.463924048878" />
                  <Point X="-4.046249705954" Y="-23.759087497809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.609744667965" Y="-23.179825747535" />
                  <Point X="-2.869671585329" Y="-22.197715595602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.76119854069" Y="-22.053767003429" />
                  <Point X="-1.929806268293" Y="-20.950472193609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676351757077" Y="-24.437405348439" />
                  <Point X="-4.154466682358" Y="-23.744840462551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.685115950694" Y="-23.121991004573" />
                  <Point X="-3.047933143484" Y="-22.276420859839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754766366111" Y="-21.887375406055" />
                  <Point X="-2.015910658521" Y="-20.906880765373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.775321370483" Y="-24.410886647999" />
                  <Point X="-4.262683658762" Y="-23.730593427294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.760487233423" Y="-23.064156261611" />
                  <Point X="-3.160583640952" Y="-22.268057305751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.786909938461" Y="-21.772175553884" />
                  <Point X="-2.094677182614" Y="-20.853551659881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.759353143337" Y="-24.231840281448" />
                  <Point X="-4.370900635167" Y="-23.716346392036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.835858516152" Y="-23.006321518648" />
                  <Point X="-3.255849860235" Y="-22.236624035319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.838463735562" Y="-21.682733939955" />
                  <Point X="-2.15684717415" Y="-20.778198211802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.728931386079" Y="-24.033613432608" />
                  <Point X="-4.479117745937" Y="-23.702099535087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.911229810532" Y="-22.948486791148" />
                  <Point X="-3.338740053969" Y="-22.18876722427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890065859758" Y="-21.593356458247" />
                  <Point X="-2.275505048186" Y="-20.77780671568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.662135126957" Y="-23.78711598943" />
                  <Point X="-4.587334878111" Y="-23.687852706543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.986601507552" Y="-22.890652597967" />
                  <Point X="-3.421630247703" Y="-22.140910413221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.941667983953" Y="-21.503978976539" />
                  <Point X="-2.480123124045" Y="-20.891488260251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.061973204571" Y="-22.832818404787" />
                  <Point X="-3.504520441437" Y="-22.093053602173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.993270025874" Y="-21.414601385648" />
                  <Point X="-2.698443426327" Y="-21.023353273443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.13734490159" Y="-22.774984211606" />
                  <Point X="-3.587410635171" Y="-22.045196791124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.044872053778" Y="-21.325223776157" />
                  <Point X="-2.980149791882" Y="-21.239334433664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.212716598609" Y="-22.717150018425" />
                  <Point X="-3.670300704174" Y="-21.997339814551" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="-24.998374023438" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.700417602539" Y="-29.391298828125" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.370770324707" Y="-28.3867578125" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.274335906982" Y="-28.2663984375" />
                  <Point X="0.129575042725" Y="-28.221470703125" />
                  <Point X="0.02097677803" Y="-28.187765625" />
                  <Point X="-0.008664148331" Y="-28.187765625" />
                  <Point X="-0.153424865723" Y="-28.2326953125" />
                  <Point X="-0.262023132324" Y="-28.2663984375" />
                  <Point X="-0.288278839111" Y="-28.285642578125" />
                  <Point X="-0.381827331543" Y="-28.4204296875" />
                  <Point X="-0.452006652832" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.63092980957" Y="-29.177916015625" />
                  <Point X="-0.847744018555" Y="-29.987076171875" />
                  <Point X="-0.973242553711" Y="-29.962716796875" />
                  <Point X="-1.10025793457" Y="-29.9380625" />
                  <Point X="-1.26773815918" Y="-29.89497265625" />
                  <Point X="-1.35158972168" Y="-29.8733984375" />
                  <Point X="-1.336701904297" Y="-29.760314453125" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.344266601562" Y="-29.360896484375" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.519559936523" Y="-29.085748046875" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240844727" Y="-28.985763671875" />
                  <Point X="-1.826129638672" Y="-28.974169921875" />
                  <Point X="-1.958829956055" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.137271728516" Y="-29.072275390625" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.356131835938" Y="-29.282923828125" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.669674072266" Y="-29.282876953125" />
                  <Point X="-2.855839111328" Y="-29.167607421875" />
                  <Point X="-3.08776171875" Y="-28.98903515625" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.931378662109" Y="-28.36583984375" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513981445312" Y="-27.56876171875" />
                  <Point X="-2.531330078125" Y="-27.5514140625" />
                  <Point X="-2.560158447266" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.144363769531" Y="-27.862560546875" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.014627441406" Y="-28.040357421875" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.327977539062" Y="-27.568314453125" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.908002685547" Y="-26.994203125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822265625" Y="-26.396015625" />
                  <Point X="-3.138117675781" Y="-26.366267578125" />
                  <Point X="-3.140325927734" Y="-26.33459765625" />
                  <Point X="-3.161157714844" Y="-26.310640625" />
                  <Point X="-3.187645019531" Y="-26.295052734375" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.917235595703" Y="-26.38042578125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.869870117188" Y="-26.236390625" />
                  <Point X="-4.927393066406" Y="-26.01119140625" />
                  <Point X="-4.971384765625" Y="-25.703603515625" />
                  <Point X="-4.998395996094" Y="-25.514744140625" />
                  <Point X="-4.405237792969" Y="-25.355806640625" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.526039550781" Y="-25.110419921875" />
                  <Point X="-3.502324951172" Y="-25.090646484375" />
                  <Point X="-3.489613525391" Y="-25.05887890625" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.490933349609" Y="-24.9994296875" />
                  <Point X="-3.502325195312" Y="-24.971912109375" />
                  <Point X="-3.529996582031" Y="-24.94939453125" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.193456542969" Y="-24.7635" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.954717285156" Y="-24.254111328125" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.829091308594" Y="-23.6767890625" />
                  <Point X="-4.773516113281" Y="-23.471701171875" />
                  <Point X="-4.360971191406" Y="-23.526013671875" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731702880859" Y="-23.6041328125" />
                  <Point X="-3.696606201172" Y="-23.593068359375" />
                  <Point X="-3.670276855469" Y="-23.584765625" />
                  <Point X="-3.651533691406" Y="-23.573943359375" />
                  <Point X="-3.639119873047" Y="-23.55621484375" />
                  <Point X="-3.625037109375" Y="-23.522216796875" />
                  <Point X="-3.614472412109" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616316650391" Y="-23.45448828125" />
                  <Point X="-3.633308837891" Y="-23.421845703125" />
                  <Point X="-3.646056396484" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.024775146484" Y="-23.1008515625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.304063964844" Y="-22.45978515625" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.925436523438" Y="-21.911478515625" />
                  <Point X="-3.774670410156" Y="-21.717689453125" />
                  <Point X="-3.532392578125" Y="-21.857568359375" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138513183594" Y="-22.07956640625" />
                  <Point X="-3.089633056641" Y="-22.08384375" />
                  <Point X="-3.052963867188" Y="-22.08705078125" />
                  <Point X="-3.031506347656" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.978557373047" Y="-22.037900390625" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.942350830078" Y="-21.923279296875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.113724365234" Y="-21.58596875" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.003763427734" Y="-21.018021484375" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.383423583984" Y="-20.620408203125" />
                  <Point X="-2.141548339844" Y="-20.48602734375" />
                  <Point X="-2.082085449219" Y="-20.563521484375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246826172" Y="-20.726337890625" />
                  <Point X="-1.89684362793" Y="-20.75466015625" />
                  <Point X="-1.856030761719" Y="-20.77590625" />
                  <Point X="-1.835124511719" Y="-20.781509765625" />
                  <Point X="-1.813809448242" Y="-20.77775" />
                  <Point X="-1.757144775391" Y="-20.75427734375" />
                  <Point X="-1.714635498047" Y="-20.736669921875" />
                  <Point X="-1.696905395508" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.667639892578" Y="-20.647017578125" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.670296142578" Y="-20.441974609375" />
                  <Point X="-1.689137573242" Y="-20.298859375" />
                  <Point X="-1.292883666992" Y="-20.187763671875" />
                  <Point X="-0.968083496094" Y="-20.096703125" />
                  <Point X="-0.520205566406" Y="-20.04428515625" />
                  <Point X="-0.224199981689" Y="-20.009640625" />
                  <Point X="-0.152553695679" Y="-20.277029296875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282114029" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594032288" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.13728338623" Y="-20.379966796875" />
                  <Point X="0.236648376465" Y="-20.0091328125" />
                  <Point X="0.576616088867" Y="-20.044736328125" />
                  <Point X="0.860209777832" Y="-20.074435546875" />
                  <Point X="1.230755737305" Y="-20.163896484375" />
                  <Point X="1.508455322266" Y="-20.230943359375" />
                  <Point X="1.749985839844" Y="-20.318546875" />
                  <Point X="1.931044799805" Y="-20.38421875" />
                  <Point X="2.164254882812" Y="-20.493283203125" />
                  <Point X="2.338686767578" Y="-20.574859375" />
                  <Point X="2.564001464844" Y="-20.70612890625" />
                  <Point X="2.732533203125" Y="-20.804314453125" />
                  <Point X="2.945012451172" Y="-20.95541796875" />
                  <Point X="3.068740722656" Y="-21.043408203125" />
                  <Point X="2.722158447266" Y="-21.643705078125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.212584960938" Y="-22.554359375" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.206827880859" Y="-22.647341796875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682861328" Y="-22.699189453125" />
                  <Point X="2.243227539062" Y="-22.73536328125" />
                  <Point X="2.261640625" Y="-22.762498046875" />
                  <Point X="2.274938964844" Y="-22.775794921875" />
                  <Point X="2.311111328125" Y="-22.80033984375" />
                  <Point X="2.338247558594" Y="-22.81875390625" />
                  <Point X="2.360337646484" Y="-22.827021484375" />
                  <Point X="2.400004638672" Y="-22.8318046875" />
                  <Point X="2.429762451172" Y="-22.835392578125" />
                  <Point X="2.448665039062" Y="-22.8340546875" />
                  <Point X="2.494537841797" Y="-22.821787109375" />
                  <Point X="2.528951416016" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.180719970703" Y="-22.438259765625" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.102629882812" Y="-22.1191953125" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.321051269531" Y="-22.453876953125" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.936781738281" Y="-22.9095625" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.246356445312" Y="-23.45923828125" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.200821289062" Y="-23.552474609375" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.200875" Y="-23.657962890625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646240234" Y="-23.712044921875" />
                  <Point X="3.243104736328" Y="-23.753779296875" />
                  <Point X="3.263703857422" Y="-23.78508984375" />
                  <Point X="3.280946533203" Y="-23.8011796875" />
                  <Point X="3.320737548828" Y="-23.823578125" />
                  <Point X="3.350588623047" Y="-23.8403828125" />
                  <Point X="3.368565673828" Y="-23.846380859375" />
                  <Point X="3.422365966797" Y="-23.853490234375" />
                  <Point X="3.462726318359" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.083462646484" Y="-23.778828125" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.896255371094" Y="-23.872259765625" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.976518554688" Y="-24.28837890625" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.483979492188" Y="-24.56313671875" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087158203" Y="-24.767181640625" />
                  <Point X="3.676230224609" Y="-24.797732421875" />
                  <Point X="3.636577148438" Y="-24.82065234375" />
                  <Point X="3.622264892578" Y="-24.833072265625" />
                  <Point X="3.59055078125" Y="-24.873484375" />
                  <Point X="3.566759033203" Y="-24.90380078125" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.546413574219" Y="-24.98046484375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.5490546875" Y="-25.095884765625" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566759033203" Y="-25.158759765625" />
                  <Point X="3.598473144531" Y="-25.199171875" />
                  <Point X="3.622264892578" Y="-25.22948828125" />
                  <Point X="3.636577148438" Y="-25.241908203125" />
                  <Point X="3.689434326172" Y="-25.2724609375" />
                  <Point X="3.729087158203" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.298418457031" Y="-25.449703125" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.972403808594" Y="-25.8073984375" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.900605957031" Y="-26.175978515625" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.273435058594" Y="-26.211041015625" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836914062" Y="-26.098341796875" />
                  <Point X="3.291097167969" Y="-26.120890625" />
                  <Point X="3.213272705078" Y="-26.1378046875" />
                  <Point X="3.1854453125" Y="-26.154697265625" />
                  <Point X="3.122741210938" Y="-26.230111328125" />
                  <Point X="3.075701171875" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.055370849609" Y="-26.411734375" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.113771728516" Y="-26.605921875" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.685592773438" Y="-27.0823515625" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.271705566406" Y="-27.692796875" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.105225097656" Y="-27.942673828125" />
                  <Point X="4.056687744141" Y="-28.011638671875" />
                  <Point X="3.520694335938" Y="-27.70218359375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340087891" Y="-27.2533125" />
                  <Point X="2.613873291016" Y="-27.231013671875" />
                  <Point X="2.521249755859" Y="-27.214287109375" />
                  <Point X="2.489078125" Y="-27.21924609375" />
                  <Point X="2.386507568359" Y="-27.273228515625" />
                  <Point X="2.309560058594" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.234617675781" Y="-27.43725390625" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.2114609375" Y="-27.669841796875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.566401123047" Y="-28.35415625" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.915482177734" Y="-29.132939453125" />
                  <Point X="2.835297851563" Y="-29.190212890625" />
                  <Point X="2.724714111328" Y="-29.261791015625" />
                  <Point X="2.679775146484" Y="-29.29087890625" />
                  <Point X="2.26865234375" Y="-28.75509375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549194336" Y="-27.980466796875" />
                  <Point X="1.548777709961" Y="-27.9021796875" />
                  <Point X="1.45742578125" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.292627075195" Y="-27.84797265625" />
                  <Point X="1.192718139648" Y="-27.857166015625" />
                  <Point X="1.165332519531" Y="-27.868509765625" />
                  <Point X="1.06249597168" Y="-27.954015625" />
                  <Point X="0.985348754883" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.93770916748" Y="-28.187451171875" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.008103393555" Y="-29.026041015625" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.070204101563" Y="-29.946619140625" />
                  <Point X="0.994367370605" Y="-29.9632421875" />
                  <Point X="0.892187744141" Y="-29.9818046875" />
                  <Point X="0.860200317383" Y="-29.987615234375" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#174" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.110413401202" Y="4.767192917724" Z="1.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.45" />
                  <Point X="-0.532173715165" Y="5.036656325159" Z="1.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.45" />
                  <Point X="-1.31253717172" Y="4.891660490197" Z="1.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.45" />
                  <Point X="-1.726024460128" Y="4.582779778026" Z="1.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.45" />
                  <Point X="-1.721481873578" Y="4.399298544457" Z="1.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.45" />
                  <Point X="-1.782434438134" Y="4.32319602673" Z="1.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.45" />
                  <Point X="-1.879911916661" Y="4.320970563909" Z="1.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.45" />
                  <Point X="-2.048573885926" Y="4.498196206039" Z="1.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.45" />
                  <Point X="-2.413862410865" Y="4.454578872598" Z="1.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.45" />
                  <Point X="-3.040341486495" Y="4.052938631042" Z="1.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.45" />
                  <Point X="-3.163181617938" Y="3.420310911995" Z="1.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.45" />
                  <Point X="-2.998316501989" Y="3.103643560899" Z="1.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.45" />
                  <Point X="-3.020068178434" Y="3.028735389452" Z="1.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.45" />
                  <Point X="-3.091432904708" Y="2.997248197184" Z="1.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.45" />
                  <Point X="-3.513548436302" Y="3.217012330808" Z="1.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.45" />
                  <Point X="-3.971056153808" Y="3.150505524011" Z="1.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.45" />
                  <Point X="-4.353401833816" Y="2.596700549844" Z="1.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.45" />
                  <Point X="-4.061369071326" Y="1.89076017885" Z="1.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.45" />
                  <Point X="-3.683814780916" Y="1.586346381009" Z="1.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.45" />
                  <Point X="-3.677387175261" Y="1.528198836428" Z="1.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.45" />
                  <Point X="-3.717799240359" Y="1.485898511927" Z="1.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.45" />
                  <Point X="-4.360601007903" Y="1.554838431772" Z="1.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.45" />
                  <Point X="-4.883506315562" Y="1.367569163071" Z="1.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.45" />
                  <Point X="-5.010334640017" Y="0.784486982471" Z="1.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.45" />
                  <Point X="-4.212553204362" Y="0.219482703064" Z="1.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.45" />
                  <Point X="-3.564664913502" Y="0.040812652136" Z="1.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.45" />
                  <Point X="-3.544842517224" Y="0.01703068288" Z="1.45" />
                  <Point X="-3.539556741714" Y="0" Z="1.45" />
                  <Point X="-3.543522083911" Y="-0.012776268181" Z="1.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.45" />
                  <Point X="-3.560703681622" Y="-0.038063330998" Z="1.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.45" />
                  <Point X="-4.42433458441" Y="-0.276229353692" Z="1.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.45" />
                  <Point X="-5.027037064122" Y="-0.679403116116" Z="1.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.45" />
                  <Point X="-4.924471078486" Y="-1.217485021041" Z="1.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.45" />
                  <Point X="-3.916865251897" Y="-1.398718075741" Z="1.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.45" />
                  <Point X="-3.207807556751" Y="-1.313544207479" Z="1.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.45" />
                  <Point X="-3.195978713716" Y="-1.335200531747" Z="1.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.45" />
                  <Point X="-3.944596524994" Y="-1.923253955165" Z="1.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.45" />
                  <Point X="-4.377076967139" Y="-2.562642748307" Z="1.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.45" />
                  <Point X="-4.060569212888" Y="-3.039360822235" Z="1.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.45" />
                  <Point X="-3.1255197209" Y="-2.874581041347" Z="1.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.45" />
                  <Point X="-2.565403332216" Y="-2.562927136915" Z="1.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.45" />
                  <Point X="-2.980835971822" Y="-3.309558661592" Z="1.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.45" />
                  <Point X="-3.124421685192" Y="-3.99737139363" Z="1.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.45" />
                  <Point X="-2.702151904531" Y="-4.29410745636" Z="1.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.45" />
                  <Point X="-2.322620090899" Y="-4.28208021241" Z="1.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.45" />
                  <Point X="-2.115649197007" Y="-4.082569500476" Z="1.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.45" />
                  <Point X="-1.835555608559" Y="-3.992782020167" Z="1.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.45" />
                  <Point X="-1.558683344454" Y="-4.092059234343" Z="1.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.45" />
                  <Point X="-1.399461867872" Y="-4.339370239566" Z="1.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.45" />
                  <Point X="-1.392430105436" Y="-4.722507093224" Z="1.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.45" />
                  <Point X="-1.286353216104" Y="-4.912113851541" Z="1.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.45" />
                  <Point X="-0.988925803739" Y="-4.980521223371" Z="1.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.45" />
                  <Point X="-0.588789484917" Y="-4.159576342999" Z="1.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.45" />
                  <Point X="-0.346907545414" Y="-3.417658181435" Z="1.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.45" />
                  <Point X="-0.14476080182" Y="-3.249167779151" Z="1.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.45" />
                  <Point X="0.108598277541" Y="-3.237944266137" Z="1.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.45" />
                  <Point X="0.323538314018" Y="-3.383987574333" Z="1.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.45" />
                  <Point X="0.645965375485" Y="-4.372959686817" Z="1.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.45" />
                  <Point X="0.894968799574" Y="-4.999720231401" Z="1.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.45" />
                  <Point X="1.07475397009" Y="-4.964179177762" Z="1.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.45" />
                  <Point X="1.05151972279" Y="-3.988235694099" Z="1.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.45" />
                  <Point X="0.980412385291" Y="-3.166789233616" Z="1.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.45" />
                  <Point X="1.088307137311" Y="-2.961180712038" Z="1.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.45" />
                  <Point X="1.29105260291" Y="-2.866481736417" Z="1.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.45" />
                  <Point X="1.515582377218" Y="-2.912957433737" Z="1.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.45" />
                  <Point X="2.222828662518" Y="-3.754250683549" Z="1.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.45" />
                  <Point X="2.745727531938" Y="-4.27248561072" Z="1.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.45" />
                  <Point X="2.938387830492" Y="-4.142345982851" Z="1.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.45" />
                  <Point X="2.603546307486" Y="-3.297875164883" Z="1.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.45" />
                  <Point X="2.254509124679" Y="-2.629674944631" Z="1.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.45" />
                  <Point X="2.272708046473" Y="-2.429260696643" Z="1.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.45" />
                  <Point X="2.40363768173" Y="-2.286193263494" Z="1.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.45" />
                  <Point X="2.598831858882" Y="-2.248938884812" Z="1.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.45" />
                  <Point X="3.489538432224" Y="-2.714203192789" Z="1.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.45" />
                  <Point X="4.139957564265" Y="-2.940171593329" Z="1.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.45" />
                  <Point X="4.308083475974" Y="-2.687800953453" Z="1.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.45" />
                  <Point X="3.709874871127" Y="-2.011402457843" Z="1.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.45" />
                  <Point X="3.149673559002" Y="-1.547601616981" Z="1.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.45" />
                  <Point X="3.09900446709" Y="-1.385035999756" Z="1.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.45" />
                  <Point X="3.155031481031" Y="-1.230797686118" Z="1.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.45" />
                  <Point X="3.295560147133" Y="-1.138468680214" Z="1.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.45" />
                  <Point X="4.260752536141" Y="-1.229332812268" Z="1.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.45" />
                  <Point X="4.943197002997" Y="-1.155823117338" Z="1.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.45" />
                  <Point X="5.015689089902" Y="-0.783573006263" Z="1.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.45" />
                  <Point X="4.305203458724" Y="-0.370125520894" Z="1.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.45" />
                  <Point X="3.708299788668" Y="-0.197890543563" Z="1.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.45" />
                  <Point X="3.631650968095" Y="-0.137021931255" Z="1.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.45" />
                  <Point X="3.592006141511" Y="-0.055199987314" Z="1.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.45" />
                  <Point X="3.589365308914" Y="0.041410543892" Z="1.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.45" />
                  <Point X="3.623728470305" Y="0.12692680732" Z="1.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.45" />
                  <Point X="3.695095625684" Y="0.190258300679" Z="1.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.45" />
                  <Point X="4.490764358582" Y="0.419846411875" Z="1.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.45" />
                  <Point X="5.019767189758" Y="0.750593125501" Z="1.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.45" />
                  <Point X="4.938680522715" Y="1.170847664828" Z="1.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.45" />
                  <Point X="4.070780213962" Y="1.302023906434" Z="1.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.45" />
                  <Point X="3.422760910043" Y="1.227358215432" Z="1.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.45" />
                  <Point X="3.339109671713" Y="1.251272077084" Z="1.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.45" />
                  <Point X="3.278719456717" Y="1.304980750414" Z="1.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.45" />
                  <Point X="3.243687480458" Y="1.383421521138" Z="1.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.45" />
                  <Point X="3.242817928" Y="1.465338793899" Z="1.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.45" />
                  <Point X="3.279883381194" Y="1.54162472149" Z="1.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.45" />
                  <Point X="3.961063758608" Y="2.082050040905" Z="1.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.45" />
                  <Point X="4.35767258007" Y="2.603291217016" Z="1.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.45" />
                  <Point X="4.137059471475" Y="2.941287514197" Z="1.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.45" />
                  <Point X="3.14956389424" Y="2.636321616123" Z="1.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.45" />
                  <Point X="2.475464509555" Y="2.257796173533" Z="1.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.45" />
                  <Point X="2.399833807454" Y="2.249117420132" Z="1.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.45" />
                  <Point X="2.33303052952" Y="2.272313585552" Z="1.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.45" />
                  <Point X="2.278445014683" Y="2.323994330862" Z="1.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.45" />
                  <Point X="2.250312282627" Y="2.389924636378" Z="1.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.45" />
                  <Point X="2.254731769124" Y="2.464005066327" Z="1.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.45" />
                  <Point X="2.759303736736" Y="3.362575187715" Z="1.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.45" />
                  <Point X="2.967833706464" Y="4.116608173807" Z="1.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.45" />
                  <Point X="2.583015103816" Y="4.368355637755" Z="1.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.45" />
                  <Point X="2.17928067013" Y="4.583288154958" Z="1.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.45" />
                  <Point X="1.760878712868" Y="4.759737220333" Z="1.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.45" />
                  <Point X="1.236334352542" Y="4.915986996156" Z="1.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.45" />
                  <Point X="0.57566798298" Y="5.036273086503" Z="1.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.45" />
                  <Point X="0.082831160351" Y="4.664254588227" Z="1.45" />
                  <Point X="0" Y="4.355124473572" Z="1.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>